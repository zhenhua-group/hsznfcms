﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_CorperationListBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_CorperationListBymaster" %>

<%@ Register TagPrefix="cc1" Namespace="Geekees.Common.Controls" Assembly="ASTreeView, Version=1.5.9.0, Culture=neutral, PublicKeyToken=521e0b4262a9001c" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
        <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>    
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
     <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/Coperation/cpr_CorperationList_jq.js"></script>   
    <script type="text/javascript" src="../js/Coperation/cpr_CorperationListSearch.js"></script>
     <script type="text/javascript" src="../js/Coperation/ProjectCharge.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化
            var tabs = $.trim($.cookie('tabs'));
          
            coperationList(tabs);
           
            //初始化选择查询字段的列和事件
            InitCheckBoxSelectColumns();

        });
        var divcbx = "tbl_cbx";//查询选择列div的ID
        var tblsch = "tbl_sch";//查询按钮table的ID
        var divsch = "tbl_id2";//点击选择列显示的字段输入值得div的ID
        function coperationList(coperationtype) {
            //数据全部恢复
           // BackCheckBoxSelectColumns(divcbx, divsch); 
            ////只显示点击列表的查询字段和查询按钮
            $("#" + divcbx, "#selectChoose").css("display", "");
            $("#" + tblsch, "#selectColumnsParam").show();
            //加载数据
            coperation();
            //初始化显示cookies字段列
            InitColumnsCookies("columnslist", "jqGrid", "columnsid");
        }

    </script>
    <style type="text/css">
        .borderClass {
            border: 1px solid #9BA0A6;
        }

        .tdvalue {
            min-width: 120px;
            width: 200px;
        }

        .tdtext {
            min-width: 70px;
            width: 90px;
        }

        .tdbetween {
            min-width: 50px;
            width: 90px;
        }

        #tbl_columns > tbody > tr > td {
            padding-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同信息列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right"> </i><a>合同信息列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询合同
                    </div>
                    <div class="actions">
                        <div class="btn-group" data-toggle="buttons" id="CommSelectWay">
                            <span class="btn blue">查询方式：</span>
                            <label rel="and" class="btn yellow btn-sm active" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option1" value="and">必须
                            </label>
                            <label rel="or" class="btn default btn-sm" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option2" value="or">或
                            </label>

                        </div>
                        <%--<input type="button" class="btn red btn-sm" id="btn_export" value="导出" />--%>
                    </div>
                    <div class="actions">
                        <div class="btn-group" id="selectChoose">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择查询字段
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="tbl_cbx" rel="tbl_id" for="tbl_id2">
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="portlet-body" id="selectColumnsParam">
                    <table border="0" cellspacing="0" cellpadding="0" style="display: none;" id="TempTbl">
                    </table>
                    <table class="table-responsive" style="width: 100%; margin: 0 auto;" id="tbl_columns">
                        <tr>
                            <td class="tdtext">生产部门：</td>
                            <td>
                                <cc1:ASDropDownTreeView ID="drp_unit" runat="server" Width="200" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全院部门-------" />
                            </td>
                            <td class="tdtext">年份：</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control borderClass tdvalue" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td class="tdtext">工程名称：</td>
                            <td>
                                <input type="text" class="form-control input-sm tdvalue" id="txt_keyname" runat="server" style="border: 1px solid #9BA0A6;" /></td>
                        </tr>
                        <%-- <tr>
                            <td colspan="2">录入时间：<input type="text" name="txt_date" id="txt_startdate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>
                            <td colspan="4">截止时间：<input type="text" name="txt_date" id="txt_enddate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>

                        </tr>--%>
                        <tr>
                            <td class="tdtext">合同分类：</td>
                            <td>
                                <asp:DropDownList ID="drp_type" CssClass="form-control borderClass tdvalue" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="0">-----选择合同分类-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdtext">建设单位：</td>
                            <td>
                                <input type="text" class="form-control input-sm tdvalue" id="txt_cprbuildunit" runat="server" style="border: 1px solid #9BA0A6;" /></td>
                            <td class="tdtext">合同额：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm tdbetween" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" id="txt_account1" runat="server" />
                                    －
                                    <input type="text" class="form-control  input-sm tdbetween" id="txt_account2" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" runat="server" />
                                    万元
                                </div>


                            </td>
                        </tr>
                    </table>
                    <table id="tbl_sch" class="table-responsive" style="width: 100%; margin: 0 auto; display: ;">
                        <tr>
                            <td colspan="2" align="center">
                                <input type="button" class="btn blue" id="btn_search" value="查询" />&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                   
                </div>
            </div>
            <div class="portlet box blue" id="div_cbx" style="display: none;">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>高级查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_id">
                                    <tr>
                                        <td align="left">
                                            <input id="chk_all" name="bb" class="cls_audit" type="checkbox" /><span for="chk_All">全选</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_No" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_No">合同编号</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Type" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Type">合同类型</span>
                                        </td>
                                        <%-- <td align="left">
                                            <input id="cpr_Name" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Name">合同名称</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="BuildArea" name="bb" class="cls_audit" type="checkbox" /><span for="BuildArea">建筑规模</span>
                                        </td>
                                        <%--<td align="left">
                                            <input id="BuildUnit" name="bb" class="cls_audit" type="checkbox" /><span for="BuildUnit">建设单位</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="StructType" name="bb" class="cls_audit" type="checkbox" /><span for="StructType">结构形式</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildStructType" name="bb" class="cls_audit" type="checkbox" /><span for="BuildStructType">建筑分类</span>
                                        </td>
                                        <%--<td align="left">
                                            <input id="BuildType" name="bb" class="cls_audit" type="checkbox" /><span for="BuildType">建筑类别</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="ChgPeople" name="bb" class="cls_audit" type="checkbox" /><span for="ChgPeople">工程负责人</span>
                                        </td>
                                        <td align="left">
                                            <input id="ChgJia" name="bb" class="cls_audit" type="checkbox" /><span for="ChgJia">甲方负责人</span>
                                        </td>

                                    </tr>
                                    <tr>

                                        <%-- <td align="left">
                                            <input id="cpr_Acount" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Acount">合同额</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="BuildPosition" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="BuildPosition">工程地点</span>
                                        </td>
                                       <%-- <td align="left">

                                            <input id="Industry" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="Industry">行业性质</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Process" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Process">合同阶段</span>
                                        </td>

                                        <td align="left">
                                            <input id="BuildSrc" name="bb" class="cls_audit" type="checkbox" /><span for="BuildSrc">工程来源</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="cpr_SignDate2" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_SignDate2">合同签约日期</span>
                                        </td>
                                       <%-- <td align="left">
                                            <input id="cpr_DoneDate" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_DoneDate">合同完成日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_SignDate" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_SignDate">合同统计年份</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="ssze" name="bb" class="cls_audit" type="checkbox" /><span for="ssze">合同收费</span>
                                        </td>
                                       <%-- <td align="left">
                                            <input id="cstName" name="bb" class="cls_audit" type="checkbox" /><span for="cstName">按照客户</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="InsertDate" name="bb" class="cls_audit" type="checkbox" /><span for="InsertDate">录入时间</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue" id="div_sch" style="display: none;">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>合同高级查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="tbl_id2">
                                    <tr for="cpr_No" style="display: none;">
                                        <td class="tdtext" for="cpr_No">合同编号:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm tdvalue" id="txt_proName" style="border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="cpr_Type" style="display: none;">
                                        <td class="tdtext" for="cpr_Type">合同类型:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="txt_ddcpr_Type" runat="Server" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm tdvalue">
                                                <asp:ListItem Value="-1">----选择合同类型----</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_Name" style="display: none;">
                                        <td class="tdtext" for="cpr_Name">合同名称:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm tdvalue" id="txt_cprName" style="border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="BuildArea" style="display: none;">
                                        <td class="tdtext" for="BuildArea">建筑规模:
                                        </td>
                                        <td>

                                            <div class="input-group">
                                                <input type="text" id="txt_buildArea" class="form-control input-sm tdbetween" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" />
                                                至 
                                                        <input type="text" id="txt_buildArea2" class="form-control input-sm tdbetween" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" />
                                                平米
                                            </div>

                                        </td>
                                    </tr>
                                    <tr for="BuildUnit" style="display: none;">
                                        <td class="tdtext" for="BuildUnit">建设单位:
                                        </td>
                                        <td>
                                            <input type="text" id="txt_BuildUnit" class="form-control input-sm tdvalue " style="border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="StructType" style="display: none;">
                                        <td class="tdtext" for="StructType">结构形式：
                                        </td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="asTreeviewStruct" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择结构形式--------"
                                                Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr for="BuildStructType" style="display: none;">
                                        <td class="tdtext" for="BuildStructType">建筑分类：
                                        </td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="asTreeviewStructType" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择建筑分类--------"
                                                Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr for="BuildType" style="display: none;">
                                        <td class="tdtext" for="BuildType">建筑类别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_buildtype" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm tdvalue">
                                                <asp:ListItem Value="-1">------选择类别------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="ChgPeople" style="display: none;">
                                        <td class="tdtext" for="ChgPeople">工程负责人:
                                        </td>
                                        <td>
                                            <input type="text" id="txt_proFuze" class="form-control input-sm tdvalue " style="border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="ChgJia" style="display: none;">
                                        <td class="tdtext" for="ChgJia">甲方负责人:
                                        </td>
                                        <td>
                                            <input type="text" id="txtFParty" class="form-control input-sm tdvalue " style="border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <%-- <tr style="display:none;" for="cpr_Unit">
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm"
                                                >
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>--%>
                                    <tr for="cpr_Acount" style="display: none;">
                                        <td class="tdtext" for="cpr_Acount">合同额:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txtproAcount" class="form-control input-sm tdbetween" style="border: 1px solid #9BA0A6;" />
                                                        至 
                                                        <input type="text" id="txtproAcount2" class="form-control input-sm tdbetween" style="border: 1px solid #9BA0A6;" />
                                                        万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="BuildPosition" style="display: none;">
                                        <td class="tdtext" for="BuildPosition">工程地点:
                                        </td>
                                        <td>
                                            <input type="text" id="txtaddress" class="form-control input-sm tdvalue " style="border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="Industry" style="display: none;">
                                        <td class="tdtext" for="Industry">行业性质:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddType" runat="server" CssClass="form-control input-sm tdvalue"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_Process" style="display: none;">
                                        <td class="tdtext" for="cpr_Process">合同阶段:
                                        </td>
                                        <td>
                                            <asp:CheckBoxList ID="chk_cprjd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr for="BuildSrc" style="display: none;">
                                        <td class="tdtext" for="BuildSrc">工程来源:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddsource" runat="server" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm tdvalue">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_SignDate2" style="display: none;">
                                        <td class="tdtext" for="cpr_SignDate2">合同签约日期:
                                        </td>
                                        <td>

                                            <div class="input-group">
                                                <input type="text" id="txt_signdate_1" onclick="WdatePicker({ readOnly: true })" class="Wdate tdbetween"
                                                    runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                                至 
                                                        <input type="text" id="txt_signdate2_1" onclick="WdatePicker({ readOnly: true })" class="Wdate tdbetween"
                                                            runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                            </div>

                                        </td>
                                    </tr>
                                    <tr for="cpr_DoneDate" style="display: none;">
                                        <td class="tdtext" for="cpr_DoneDate">合同完成日期:
                                        </td>
                                        <td>

                                            <div class="input-group">
                                                <input type="text" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate tdbetween" runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                                至 
                                                        <input type="text" id="txt_finishdate2" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate tdbetween" runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                            </div>

                                        </td>
                                    </tr>
                                    <tr for="cpr_SignDate" style="display: none;">
                                        <td class="tdtext" for="cpr_SignDate">合同统计年份:
                                        </td>
                                        <td>

                                            <div class="input-group">
                                                <input type="text" id="txt_signdate" onclick="WdatePicker({ readOnly: true })" class="Wdate tdbetween"
                                                    runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                                至 
                                                        <input type="text" id="txt_signdate2" onclick="WdatePicker({ readOnly: true })" class="Wdate tdbetween"
                                                            runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                            </div>

                                        </td>
                                    </tr>
                                    <tr for="ssze" style="display: none;">
                                        <td class="tdtext" for="ssze">合同收费:
                                        </td>
                                        <td>

                                            <div class="input-group">
                                                <input type="text" id="projcharge1" class="form-control input-sm tdbetween" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" />
                                                至 
                                                        <input type="text" id="projcharge2" class="form-control input-sm tdbetween" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" />
                                                万元
                                            </div>

                                        </td>
                                    </tr>
                                    <tr for="cstName" style="display: none;">
                                        <td class="tdtext" for="cstName">按照客户:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm tdvalue" id="txt_cstname" style="border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="InsertDate" style="display: none;">
                                        <td class="tdtext" for="InsertDate">录入时间：</td>
                                        <td>

                                            <div class="input-group">
                                                <input type="text" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate tdbetween" runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                                至
                                                        <input type="text" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate tdbetween" runat="Server" style="height: 25px; border: 1px solid #9BA0A6;" />
                                            </div>

                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同信息列表
                    </div>
                    <div class="actions">
                        <div class="btn-group" id="choose">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <%=ColumnsContent %>
                            </div>
                          
                        </div>
                        <asp:Button Text="导出Excel" runat="server" CssClass="btn red btn-sm" ID="btn_export" OnClick="btn_export_Click" />
                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">
                   
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_1" style="width: 100%">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--添加收费-->
    <div id="addCharge" class="modal
    fade yellow"
        tabindex="-1" data-width="900" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">收费列表详细
            </h4>
        </div>
        <div class="modal-body">
            <div id="addChargeDialogDiv" style="color: #222222;">
                <table id="addChargeListTable" style="text-align: center;" class="table table-striped table-bordered table-advance table-hover">
                    <tr class="trBackColor">
                        <th style="width: 50px;" align="center">序号
                        </th>
                        <th style="width: 100px;" align="center">入账额(万元)
                        </th>
                        <th style="width: 60px;" align="center">比例
                        </th>
                        <th style="width: 120px;" align="center">合同信息
                        </th>
                        <th style="width: 70px;" align="center">入账人
                        </th>
                        <th style="width: 80px;" align="center">收款时间
                        </th>
                        <th style="width: 100px;" align="center">备注
                        </th>
                       <%-- <th style="width: 100px;" align="center">状态
                        </th>--%>
                        <th style="width: 90px;" align="center">操作
                        </th>
                    </tr>
                </table>
                <br />
                <table class="table table-bordered">
                    <tr>
                        <td>项目名称:
                        </td>
                        <td>
                            <label id="lbl_projname">
                            </label>
                            <span id="txt_inputtimes"></span>
                            <input type="hidden" id="hide_cprName" value="" />
                        </td>
                        <td>承接负责人:
                        </td>
                        <td>
                            <label id="lbl_pmname">
                            </label>
                            <input type="hidden" id="hide_cprpmname" value="" />
                        </td>
                        <td>合同总金额:
                        </td>
                        <td>
                            <label id="lbl_allcount">
                            </label>
                            (万元)
                        </td>
                    </tr>
                    <tr>  
                      <%--  <td>未支付金额:
                        </td>
                        <td>
                            <label id="lbl_nopaycount">
                            </label>
                            (万元)
                            <input type="hidden" id="hide_noPayCount" class="cls_input_text" value="" />
                        </td>      --%>               
                        <td>入账金额:
                        </td>
                        <td>
                            <input type="text" id="txt_payCount" maxlength="15" class="form-control input-sm"
                                placeholder="万元" />
                            <span style="color: red; display: none;" id="txt_paycount_valide"></span>
                        </td>
                        <td>合同信息:
                        </td>
                        <td>
                            <select id="drp_subcprname" class="form-control input-sm" style="width:220px;"></select>                          
                            <span style="color: red; display: none;" id="txt_remitter_valide">请选择合同信息!</span>
                        </td>
                         <td>入账时间:
                        </td>
                        <td>
                            <input id="txt_times" type="text" class="Wdate
    "
                                onclick="WdatePicker({ readOnly: true })" style="height: 22px; border: 1px solid
    #e5e5e5;" />
                            <span style="color: red; display: none;" id="txt_time_valide">请输入入账时间!</span>
                        </td>
                    </tr>
                    <tr>
                        <td>备注:
                        </td>
                        <td colspan="5">
                            <textarea rows="3" cols="35" id="txt_chargeRemark" style="max-height: 100px; min-height: 10px;" ></textarea>
                            <span style="color: red; display: none;" id="txt_chargeRemark_valide">请输入备注!</span>
                        </td>
                    </tr>
                </table>
             
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn red btn-default" id="btn_addcount">
                收款</button>
            <button type="button" onclick="AddOrEditCharge('add')" style="display: none;" class="btn
    btn-default"
                id="btn_returnCharge">
                返回收费</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_cols" runat="server" Value="" />
    <asp:HiddenField ID="hid_colsvalue" runat="server" Value="" />
    <input type="hidden" id="hid_cxtype" value="and" />
     <!-- 是否已经收完款 -->
    <asp:HiddenField ID="hid_iscomplete" runat="server" Value="0" />
    <!-- 当前用户 -->
    <asp:HiddenField ID="hid_curuser" runat="server" Value='0' />
    <!-- 修改收款，收款ID -->
    <input type="hidden" id="hid_chgidThis" value="0" />
    <input type="hidden" id="hid_chgidNow" value="0" />
    <!-- 修改前的付款额度 -->
    <input type="hidden" id="hid_tempnopay" value="0" />
</asp:Content>
