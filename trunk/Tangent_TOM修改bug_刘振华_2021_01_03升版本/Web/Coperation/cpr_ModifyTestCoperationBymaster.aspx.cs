﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Text;
using System.Xml;
using System.Data;
using TG.Model;
using TG.BLL;

namespace TG.Web.Coperation
{
    public partial class cpr_ModifyTestCoperationBymaster : PageBase
    {
        public string asTreeviewStructObjID;

        public string asTreeviewStructTypeObjID;


        //是否具有修改权限
        public string HasAudit { get; set; }
        //审批流程获取的修改权限
        public string HasEditAudit
        {
            get
            {
                return Request["audit"] ?? "0";
            }
        }
        ////审批流程获取的修改权限ID
        public int AuditEditSysNo
        {
            get
            {
                int auditid = 0;
                int.TryParse(Request["auditeditsysno"] ?? "0", out auditid);
                return auditid;
            }
        }
        public int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["msgno"] ?? "0", out msgid);
                return msgid;
            }
        }
        //建筑结构值  qpl 20140115
        public string StructString { get; set; }
        public string StructTypeString { get; set; }
        public string BuildTypeString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string str_cprid = Request.QueryString["cprid"] ?? "";
            if (!IsPostBack)
            {
                //保存合同编号
                this.hid_cprid.Value = str_cprid;
                //绑定合同类别
                BindCorpType();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //设计等级
                BindBuildType();
                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
                //如果是审批修改流程
                //HasAudit = CheckAudit(int.Parse(str_cprid)) == true ? "1" : "0";
                //if (HasAudit == "1")
                //{
                //    Response.Write("<script language='javascript' >alert('该合同已提交审批，不能被修改！');window.history.back();</script>");
                //}

                //获取权限
                SetUserSysNoAndRole();
                //绑定权限
                BindPreviewPower();

            }
            else
            {
                EditADD();
            }
        }

        private void EditADD()
        {
            if (UpdateCoperation())
            {
                //弹出提示
                TG.Common.MessageBox.ResponseScriptBack(this, "合同信息更新成功！");
            }
        }
        //修改合同信息
        protected bool UpdateCoperation()
        {
            TG.BLL.cm_MeasureCoperation bll_cpr = new TG.BLL.cm_MeasureCoperation();
            TG.Model.cm_MeasureCoperation model_cpr = bll_cpr.GetModel(int.Parse(this.hid_cprid.Value));
            TG.Model.cm_Coperation model_cprMain = new TG.Model.cm_Coperation();

            //更新ad
            bool affectRow = false;
            //合同ID
            model_cpr.cpr_Id = int.Parse(this.hid_cprid.Value);
            //客户ID
            model_cprMain.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            //合同编号
            model_cprMain.cpr_No = this.txtcpr_No.Value.Trim();
            model_cpr.cpr_No = this.txtcpr_No.Value.Trim();
            //项目类型
            model_cprMain.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            //去掉标准合同类型
            model_cprMain.cpr_Type2 = this.txt_cprType.Value;
            model_cpr.cpr_Type2 = this.txt_cprType.Value;
            //合同名称
            model_cprMain.cpr_Name = this.txt_cprName.Value;
            model_cpr.cpr_Name = this.txt_cprName.Value;
            //建设单位
            model_cprMain.BuildUnit = this.txt_cprBuildUnit.Value;
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            //合同额
            model_cprMain.cpr_Acount = Convert.ToDecimal(this.hidtxtcpr_Account.Value);
            model_cpr.cpr_Acount = Convert.ToDecimal(this.hidtxtcpr_Account.Value);

            //实际合同额
            model_cprMain.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            model_cpr.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);

            //合同统计时间
            model_cprMain.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            //签订时间
            model_cprMain.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            //合同备注
            model_cprMain.cpr_Mark = this.txtcpr_Remark.Value;
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            if (!string.IsNullOrEmpty(this.hidtxt_buildArea.Value))
            {
                //建筑面积
                model_cprMain.BuildArea = this.hidtxt_buildArea.Value;
                model_cpr.BuildArea = Convert.ToDecimal(this.hidtxt_buildArea.Value);
            }
            else
            {
                model_cprMain.BuildArea = "0";
                model_cpr.BuildArea = 0;
            }

            //负责人
            model_cprMain.ChgPeople = this.txt_proFuze.Value;
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            //更新项目经理ID qpl 20131225
            model_cprMain.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cpr.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            //项目经理
            model_cprMain.ChgPhone = this.txt_fzphone.Value;
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            //甲方负责人
            model_cprMain.ChgJia = this.txtFParty.Value;
            model_cpr.ChgJia = this.txtFParty.Value;
            //甲方负责人电话
            model_cprMain.ChgJiaPhone = this.txt_jiafphone.Value;
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            //工程地点
            model_cprMain.BuildPosition = this.txt_ProjectPosition.Value;
            model_cpr.BuildPosition = this.txt_ProjectPosition.Value;

            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                model_cprMain.Industry = this.ddProfessionType.SelectedItem.Text;
                model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            else
            {
                model_cprMain.Industry = this.ddProfessionType.Items[0].Value;
                model_cpr.Industry = this.ddProfessionType.Items[0].Value;
            }
            //工程来源
            if (this.ddSourceWay.SelectedIndex != 0)
            {
                model_cprMain.BuildSrc = this.ddSourceWay.SelectedItem.Text;
                model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            }
            else
            {
                model_cprMain.BuildSrc = this.ddSourceWay.Items[0].Value;
                model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value;
            }
            //承接部门
            model_cprMain.cpr_Unit = this.hid_cjbm.Value;
            model_cpr.cpr_Unit = this.hid_cjbm.Value;
            //制表人
            model_cprMain.TableMaker = this.txt_tbcreate.Value;
            model_cpr.TableMaker = this.txt_tbcreate.Value;
            //时间
            model_cprMain.RegTime = Convert.ToDateTime(this.hid_cprtime.Value);
            model_cpr.RegTime = Convert.ToDateTime(this.hid_cprtime.Value);
            //更新人
            model_cprMain.UpdateBy = UserSysNo.ToString();
            model_cpr.UpdateBy = UserSysNo.ToString();
            //更新时间
            model_cprMain.LastUpdate = DateTime.Now;
            model_cpr.LastUpdate = DateTime.Now;
            //建筑类别
            model_cprMain.BuildType = this.drp_buildtype.SelectedItem.Text.Trim();
            model_cpr.BuildType = this.drp_buildtype.SelectedItem.Text.Trim();
            //楼层信息
            model_cprMain.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //多吨建筑
            model_cprMain.MultiBuild = this.txt_MultiBuild.Value;
            model_cpr.MultiBuild = this.txt_MultiBuild.Value;
            //完成时间
            model_cprMain.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);

            //建筑数量、观测数量、观测总点次、每点次单价
            if (!string.IsNullOrEmpty(this.txt_BuildNumber.Value.Trim()))
            {
                model_cpr.BuildNumber = Convert.ToInt32(this.txt_BuildNumber.Value);
            }
            else
            {
                model_cpr.BuildNumber = 0;
            }
            if (!string.IsNullOrEmpty(this.txt_ObserveNumber.Value.Trim()))
            {
                model_cpr.ObserveNumber = Convert.ToInt32(this.txt_ObserveNumber.Value);
            }
            else
            {
                model_cpr.ObserveNumber = 0;
            }
            if (!string.IsNullOrEmpty(this.txt_ObserveTotal.Value.Trim()))
            {
                model_cpr.ObserveTotal = Convert.ToInt32(this.txt_ObserveTotal.Value);
            }
            else
            {
                model_cpr.ObserveTotal = 0;
            }
            if (!string.IsNullOrEmpty(this.txt_ObservePoint.Value.Trim()))
            {
                model_cpr.ObservePoint = Convert.ToDecimal(this.txt_ObservePoint.Value);
            }
            else
            {
                model_cpr.ObservePoint = 0;
            }
            //工期
            if (!string.IsNullOrEmpty(this.txt_ProjectDate.Value.Trim()))
            {
                model_cpr.ProjectDate = Convert.ToInt32(this.txt_ProjectDate.Value);
            }
            else
            {
                model_cpr.ProjectDate = 0;
            }

            //规划面积
            // model_cpr.cpr_Area = this.hidtxt_area.Value;
            try
            {
                //更新主合同
                model_cprMain.cpr_Id = int.Parse(hid_cprFid.Value);
                new TG.BLL.cm_Coperation().Update(model_cprMain);
                //更新字表合同信息
                model_cpr.Cpr_FID = int.Parse(hid_cprFid.Value);
                affectRow = bll_cpr.Update(model_cpr);
            }
            catch (System.Exception ex)
            {
            }

            return affectRow;
        }

        //设置选择合同额方法
        public void SetUserSysNoAndRole()
        {
            //用户ID
            this.ChooseCustomer1.UserSysNo = int.Parse(GetCurMemID());
            //权限
            this.ChooseCustomer1.PreviewPower = GetPreviewPower();
        }
        //获取页面权限
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationListBymaster.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            return this.hid_cprid.Value;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();
        }
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }

        //工程来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }

        //合同设计等级
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_MeasureCoperation] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    //保存客户ID
                    this.hid_cstid.Value = str_cstid;

                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Value = model_cst.Cst_No.Trim();
                        this.txtCst_Name.Value = model_cst.Cst_Name.Trim();
                        this.txtCpy_Address.Value = model_cst.Cpy_Address.Trim();
                        this.txtCst_Brief.Value = model_cst.Cst_Brief;
                        this.txtCode.Value = model_cst.Code ?? "";
                        this.txtLinkman.Value = model_cst.Linkman ?? "";
                        this.txtCpy_Phone.Value = model_cst.Cpy_Phone ?? "";
                        this.txtCpy_Fax.Value = model_cst.Cpy_Fax ?? "";
                    }
                }
            }
        }
        //显示合同信息
        protected void ShowCoperation(string cprid)
        {

            TG.BLL.cm_MeasureCoperation bll_cpr = new TG.BLL.cm_MeasureCoperation();
            TG.Model.cm_MeasureCoperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            if (model_cpr != null)
            {
                //赋值
                this.txtcpr_No.Value = model_cpr.cpr_No == null ? "" : model_cpr.cpr_No.Trim();
                this.ddcpr_Type.Items.FindByText(model_cpr.cpr_Type.Trim()).Selected = true;
                this.txt_cprType.Value = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();

                this.txt_cprName.Value = model_cpr.cpr_Name.Trim();
                this.txt_cprBuildUnit.Value = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
                //建筑类别
                if (!string.IsNullOrEmpty(model_cpr.BuildType))
                {
                    this.drp_buildtype.Items.FindByText(model_cpr.BuildType.Trim()).Selected = true;

                }


                this.txtcpr_Account.Value = model_cpr.cpr_Acount.ToString();
                //实际合同额
                this.txtcpr_Account0.Value = model_cpr.cpr_ShijiAcount.ToString();


                //签订日期
                this.txtSingnDate.Value = Convert.ToDateTime(model_cpr.cpr_SignDate).ToString("yyyy-MM-dd");
                this.txtSingnDate2.Value = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToString("yyyy-MM-dd");
                this.txtCompleteDate.Value = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToString("yyyy-MM-dd");
                this.txtcpr_Remark.Value = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
                //建筑面积
                this.txt_buildArea.Value = Convert.ToString(model_cpr.BuildArea ?? 0).Trim();
                //项目经理
                this.txt_proFuze.Value = model_cpr.ChgPeople.Trim();
                //添加项目经理ID  qpl 20131225
                this.HiddenPMUserID.Value = model_cpr.PMUserID.ToString();
                //经理电话
                this.txt_fzphone.Value = model_cpr.ChgPhone.Trim();
                //甲方负责人
                this.txtFParty.Value = model_cpr.ChgJia.Trim();
                //甲方负责人电话
                this.txt_jiafphone.Value = model_cpr.ChgJiaPhone.Trim();

                this.txt_ProjectPosition.Value = model_cpr.BuildPosition.Trim();
                if (model_cpr.Industry.Trim() != "" && model_cpr.Industry.Trim() != "-1")
                {
                    this.ddProfessionType.Items.FindByText(model_cpr.Industry.Trim()).Selected = true;
                }
                if (model_cpr.BuildSrc.Trim() != "" && model_cpr.BuildSrc.Trim() != "-1")
                {
                    this.ddSourceWay.Items.FindByText(model_cpr.BuildSrc.Trim()).Selected = true;
                }

                this.txt_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                //承接部门
                this.hid_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                //制表人
                this.txt_tbcreate.Value = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
                //录入时间
                this.hid_cprtime.Value = model_cpr.RegTime.ToString();

                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.txt_upfloor.Value = floors[0].ToString().Trim();
                this.txt_downfloor.Value = floors[1].ToString().Trim();
                //多吨建筑
                this.txt_MultiBuild.Value = model_cpr.MultiBuild;
                //建筑数量、观测数量、观测总点次、每点次单价\工期
                this.txt_BuildNumber.Value = (model_cpr.BuildNumber ?? 0) + "";
                this.txt_ObserveNumber.Value = (model_cpr.ObserveNumber ?? 0) + "";
                this.txt_ObservePoint.Value = (model_cpr.ObservePoint ?? 0) + "";
                this.txt_ObserveTotal.Value = (model_cpr.ObserveTotal ?? 0) + "";
                this.txt_ProjectDate.Value = (model_cpr.ProjectDate ?? 0) + "";

                hid_cprFid.Value = model_cpr.Cpr_FID == null ? "0" : model_cpr.Cpr_FID.ToString();
            }
        }


        // 检查是否在审核队列中
        private bool CheckAudit(int coperationSysNo)
        {
            return new TG.BLL.cm_CoperationAudit().IsExist(coperationSysNo);

        }

        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                //string NotShowUnitList = base.NotShowUnitList;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;
                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
                //this.NotShowUnitList.Value = NotShowUnitList;
            }
        }
    }
}