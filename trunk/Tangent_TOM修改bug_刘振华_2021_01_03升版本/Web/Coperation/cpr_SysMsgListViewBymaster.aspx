﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_SysMsgListViewBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_SysMsgListViewBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />     
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
     <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"
        type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    
    <script src="../js/Coperation/cpr_SysMsgListViewBymaster.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页 <small>消息审批列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>消息审批列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>查询消息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>名称:</td>
                            <td>
                                <input type="text" class="form-control" id="txtCoperationName" /></td>
                            <td>消息类型:</td>
                            <td>
                                <asp:DropDownList ID="MsgTypeDropDownList" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="-1">-----消息类型-----</asp:ListItem>
                                    <asp:ListItem Value="1">合同审核</asp:ListItem>
                                    <asp:ListItem Value="2">项目审核</asp:ListItem>
                                    <asp:ListItem Value="3">工号申请</asp:ListItem>
                                    <asp:ListItem Value="4">策划审核</asp:ListItem>
                                    <asp:ListItem Value="6">合同入账</asp:ListItem>
                                    <asp:ListItem Value="7">项目入账</asp:ListItem>
                                    <asp:ListItem Value="8">项目策划</asp:ListItem>
                                    <asp:ListItem Value="9">工程出图</asp:ListItem>
                                    <asp:ListItem Value="11">项目修改审核</asp:ListItem>
                                    <asp:ListItem Value="12">合同修改审核</asp:ListItem>
                                    <asp:ListItem Value="13">策划修改审核</asp:ListItem>
                                    <asp:ListItem Value="23">监理公司合同审核</asp:ListItem>
                                    <asp:ListItem Value="33">自评互评考核</asp:ListItem>
                                    <asp:ListItem Value="34">项目比例分配</asp:ListItem>
                                    <asp:ListItem Value="35">考勤申请审批</asp:ListItem>
                                    <asp:ListItem Value="36">考勤申请删除</asp:ListItem>
                                    <asp:ListItem Value="24">监理公司合同修改审核</asp:ListItem>
                                    <asp:ListItem Value="100">合同额修改</asp:ListItem>
                                    <asp:ListItem Value="101">合同删除消息</asp:ListItem>
                                    <asp:ListItem Value="102">合同开票</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                                <button type="button" class="btn blue " id="btn_Search">
                                    <i class="fa fa-search"></i>
                                </button>
                            </td>
                            <td>
                                <input type="button" class="btn red " value="未办事项" id="ButtonDone" /></td>
                            <td>
                                <input type="button" class="btn blue " value="未读消息" id="ButtonUnread" /></td>
                            <td>
                                <input type="button" class="btn yellow" value="已读消息" id="ButtonRead" /></td>
                            <td>
                                <input type="button" class="btn default " value="已办事项" id="ButtonUnDone" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>消息审批列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hiddenMessageType" runat="server" />
    <input type="hidden" id="hiddenMessageDone" runat="server" />
    <input type="hidden" id="hiddenMessageIsDoneStatus" runat="server" />
    <input type="hidden" id="userSysNum" runat="server" />
    <input type="hidden" id="hiddenMessageIsRead" runat="server" />
    <div id="AuditDel" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                <label id="lbl_title"></label>
            </h4>
        </div>
        <div class="modal-body" id="auditShow">
            <table class="table table-striped table-bordered" width="100%">
              <%--  <tr>
                    <td style="font-weight: bold;">申请状态：
                    </td>
                    <td>
                        <label id="applycontent"></label>
                    </td>

                </tr>--%>
                <tr>
                    <td style="font-weight: bold;">申请类型：
                    </td>
                    <td>
                        <label id="applysort"></label>
                    </td>

                </tr>
                <tr>
                    <td style="font-weight: bold;">申请原因：
                    </td>
                    <td>
                        <label id="reason"></label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">时 间：
                    </td>
                    <td>
                        <label id="applytime"></label>
                    </td>
                </tr>
                <tr id="kqtj">
                    <td style="font-weight: bold;">考勤统计：
                    </td>
                    <td>
                        <label id="totaltime"></label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 100px;">申请人：
                    </td>
                    <td>
                        <label id="applyname"></label>
                        <input type="hidden" id="applyid" value="0">
                    </td>

                </tr>

                <tr>
                    <td style="font-weight: bold;">备 注：
                    </td>
                    <td>
                        <label id="remark"></label>
                    </td>
                </tr>

            </table>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="text-align: center; line-height: 30px;">
                <tr>
                    <td>是否通过删除申请？</td>
                </tr>
                <tr>
                    <td>                       
                        <input type="button" name="btn_save" id="btn_yes" value="Y" class="btn  green" />&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="btn_save" id="btn_no" value="N" class="btn  red" /></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
             <input type="hidden" id="hidmsgID" value="0"/>
            <input type="hidden" id="hidapplyID" value="0"/>
            <button type="button" data-dismiss="modal" id="btn_CanceAudit" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
