﻿using AjaxPro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class CprMiddlePage : PageBase
    {
        protected string CprID
        {
            get
            {
                return Request.Params["cprid"].ToString();
            }
        }       
        protected void Page_Load(object sender, EventArgs e)
        {   //注册页面
            Utility.RegisterTypeForAjax(typeof(CprMiddlePage));
            if (!IsPostBack)
            {
                this.hidcprId.Value = CprID;
                this.hiduserid.Value = UserSysNo+"";
            }

        }
    
        protected void btn_ProList_Click(object sender, EventArgs e)
        {
            Response.Redirect("cpr_CorperationListBymaster.aspx");
        }
        protected void btn_ShowPro_Click(object sender, EventArgs e)
        {
            Response.Redirect("cpr_ShowCoprationBymaster.aspx?cprid=" + CprID);
        }   

    }
}