﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.Coperation
{
    public partial class cpr_ShowRelativeContractInfoBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定合同信息
                BindCoperation();
            }
        }
        // 是否检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //检查权限
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 显示相关合同
        /// </summary>
        /// <param name="RC_Id">相关合同ID</param>
        protected void BindCoperation()
        {
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            StringBuilder sb = new StringBuilder("");
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();
            //所有记录数
            //  this.AspNetPager1.RecordCount = int.Parse(bll_cpr.GetListPageProcCount(sb.ToString()).ToString());
            //排序
            // string orderString = " Order by " + OrderColumn + " " + Order;
            // sb.Append(orderString);

            //this.gv_Coperation.DataSource = bll_cpr.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            // this.gv_Coperation.DataBind();
        }
    }
}