﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;

namespace TG.Web.Coperation
{
    public partial class cpr_CoperationAuditBymaster : PageBase
    {
        #region QueryString

        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 合同系统号
        /// </summary>
        public int CoperationSysNo { get; set; }

        /// <summary>
        /// 合同审核记录系统编号
        /// </summary>
        public int CoperationAuditSysNo
        {
            get
            {
                int coperationAuditSysNo = 0;
                int.TryParse(Request["CoperationAuditSysNo"], out coperationAuditSysNo);
                return coperationAuditSysNo;
            }
        }

        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_CoperationAudit coperationAuditRecordEntity { get; set; }

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        /// <summary>
        /// 消息审批状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }

        }
        #endregion

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion

        #region Property
        /// <summary>
        /// 合同基本信息实体
        /// </summary>
        public TG.Model.cm_Coperation Coperation { get; set; }

        /// <summary>
        /// 合同审核状态
        /// </summary>
        public string AuditStatus { get; set; }
        /// <summary>
        /// 合同名称
        /// </summary>
        public string CoperationName { get; set; }

        /// <summary>
        /// 是否有权限
        /// </summary>
        public string HasPower { get; set; }

        public string CoperationTypeHTMLString { get; set; }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(cpr_CoperationAuditBymaster));
            if (!IsPostBack)
            {
                //查询合同系统号
                CoperationSysNo = new TG.BLL.cm_CoperationAudit().GetModel(CoperationAuditSysNo).CoperationSysNo;

                GetConperationBaseInfo();
                GetCoperationAuditRecord();
                CoperationName = Coperation.cpr_Name;
            }
        }
        /// <summary>
        /// 得到合同基本信息
        /// </summary>
        private void GetConperationBaseInfo()
        {
            TG.BLL.cm_Coperation coperationBLL = new TG.BLL.cm_Coperation();
            Coperation = coperationBLL.GetModel(CoperationSysNo);
            //建设单位
            ConstructionCompany.Text = Coperation.BuildUnit;
            //承接部门
            UndertakeDepartment.Text = Coperation.cpr_Unit;
            //项目名称
            ProjectName.Text = Coperation.cpr_Name;
            //建设规模
            BuildScope.Text = Coperation.BuildArea;
            //建设地点
            BuildAddress.Text = Coperation.BuildPosition;
            //投资额
            Investment.Text = Coperation.cpr_Touzi.ToString();
            //建设性质
            HiddenBuildNature.Value = Coperation.Industry;
            //合同额 
            CoperationAmount.Text = Coperation.cpr_Acount.ToString();
            //合同分类
            HiddenCoperationType.Value = Coperation.cpr_Type;
            //建筑类别
            buildType.Text = Coperation.BuildType.Trim();
            //层数
            string[] floorArray = Coperation.Floor.Split('|');
            floorNum.Text = string.Format("地上：{0}层 地下：{1}层", floorArray[0].Trim().Length == 0 ? "0" : floorArray[0], floorArray[1].Trim().Length == 0 ? "0" : floorArray[1]);
            //保密情况
            //SecretSituation.Text =;

            this.RepeaterCoperationBuildType.DataSource = TG.DBUtility.DbHelperSQL.Query("SELECT [dic_Name] FROM [dbo].[cm_Dictionary] where dic_Type =N'cpr_fl'");
            this.RepeaterCoperationBuildType.DataBind();
        }

        /// <summary>
        /// 查询消息审核记录
        /// </summary>
        private void GetCoperationAuditRecord()
        {
            TG.BLL.cm_CoperationAudit coperationAuditBLL = new TG.BLL.cm_CoperationAudit();
            coperationAuditRecordEntity = coperationAuditBLL.GetModel(CoperationAuditSysNo);
            AuditStatus = coperationAuditRecordEntity == null ? "" : coperationAuditRecordEntity.Status;
            if (!string.IsNullOrEmpty(coperationAuditRecordEntity.AuditUser))
            {
                string[] tempArr = coperationAuditRecordEntity.AuditUser.Split(',');
                if (tempArr != null && tempArr.Length > 0)
                {
                    foreach (var item in tempArr)
                    {
                        //查询联系人姓名
                        TG.Model.tg_member user = new TG.BLL.tg_member().GetModel(int.Parse(item));
                        coperationAuditRecordEntity.AuditUserString = string.IsNullOrEmpty(coperationAuditRecordEntity.AuditUser) ? user.mem_Name : coperationAuditRecordEntity.AuditUserString + user.mem_Name + ",";
                    }
                    coperationAuditRecordEntity.AuditUserString = coperationAuditRecordEntity.AuditUserString.Substring(0, coperationAuditRecordEntity.AuditUserString.Length - 1);
                }
            }
        }


        [AjaxMethod]
        public string CheckPower(string auditStatus, int userSysNo, int needLegalAdviser)
        {
            //得到用户所有的角色集合
            List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList(userSysNo);

            int position = 0;
            switch (auditStatus)
            {
                case "A":
                    position = 1;
                    break;
                case "B":
                    position = 2;
                    break;
                case "D":
                    if (needLegalAdviser == 1)
                    {
                        position = 3;
                    }
                    else
                    {
                        position = 4;
                    }
                    break;
                case "F":
                    position = 4;
                    break;
                case "H":
                    position = 5;
                    break;
            }
            //得到该审核记录当前的审核角色
            TG.Model.cm_CoperationAuditConfig coperationAuditConfig = new TG.BLL.cm_CoperationAuditConfig().GetCoperationAuditConfigByPostion(position);

            string hasPower = (from role in roleList where role.SysNo == coperationAuditConfig.RoleSysNo select role).Count() > 0 ? "1" : "0";

            return hasPower;
        }
    }
}