﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectUserControl.ascx.cs"
    Inherits="TG.Web.Coperation.SelectUserControl" %>
<%--<link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
<link href="/css/Corperation.css" rel="stylesheet" type="text/css" />--%>
<style type="text/css">
    #selectUserMain table td
    {
        padding: 2px 0px;
        vertical-align: middle;
    }
    #divDataTable td
    {
        padding: 0px;
    }
</style>
<div id="selectUserMain">
    <table width="100%">
        <tr>
            <td>
                <table style="width: 100%; margin: 0 auto;" class="table-bordered">
                    <tr>
                        <td>
                            姓名简称：
                            <input type="text" id="UserShortName" class="form-control input-sm" style="display: inline;
                                width: 150px; height: 25px; padding: 0px; font-size: 12px;" />
                            生产部门：
                            <select id="UserDepartment" class="form-control" style="display: inline;
                                        width: 150px; padding: 0px; font-size: 12px;">
                                <option value="0">----请选择----</option>
                                <asp:Repeater ID="RepeaterDepartment" runat="server">
                                    <ItemTemplate>
                                        <option value="<%#Eval("unit_ID") %>">
                                            <%#Eval("unit_Name")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                            <a href="###" class="btn blue btn-sm" id="btnSearch"><i class="fa fa-search"></i></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div style="overflow: auto; height: 300px;">
                    <table style="width: 98%; margin: 0 auto;" id="divDataTable">
                        <%=HTML%>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <!--HiddenArea-->
    <input type="hidden" id="defaultDepartmentSysNo" value="<%=DefaultDeparmentSysNo %>" />
</div>
