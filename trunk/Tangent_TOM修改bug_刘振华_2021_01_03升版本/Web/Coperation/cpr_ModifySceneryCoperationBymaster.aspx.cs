﻿using Geekees.Common.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TG.BLL;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class cpr_ModifySceneryCoperationBymaster : PageBase
    {
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }

        //是否具有修改权限
        public string HasAudit { get; set; }
        //审批流程获取的修改权限
        public string HasEditAudit
        {
            get
            {
                return Request["audit"] ?? "0";
            }
        }
        ////审批流程获取的修改权限ID
        public int AuditEditSysNo
        {
            get
            {
                int auditid = 0;
                int.TryParse(Request["auditeditsysno"] ?? "0", out auditid);
                return auditid;
            }
        }
        public int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["msgno"] ?? "0", out msgid);
                return msgid;
            }
        }
        //建筑结构值  qpl 20140115
        public string StructString { get; set; }
        public string StructTypeString { get; set; }
        public string BuildTypeString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string str_cprid = Request.QueryString["cprid"] ?? "";
            if (!IsPostBack)
            {
                //保存合同编号
                this.hid_cprid.Value = str_cprid;
                //绑定合同类别
                BindCorpType();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //合同阶段
                BindCorpProc();
                //设计等级
                BindBuildType();
                //设置下拉树的样式
                SetDropDownTreeThem();
                //绑定建筑结构样式
                BindStructType();
                //绑定建筑分类
                BindBuildStuctType();
                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
                //如果是审批修改流程
                HasAudit = CheckAudit(int.Parse(str_cprid)) == true ? "1" : "0";
                //if (HasAudit == "1")
                //{
                //    Response.Write("<script language='javascript' >alert('该合同已提交审批，不能被修改！');window.history.back();</script>");
                //}

                //获取权限
                SetUserSysNoAndRole();
                //绑定权限
                BindPreviewPower();

            }
            else
            {
                EditADD();
            }
        }

        private void EditADD()
        {
            if (UpdateCoperation())
            {
                //弹出提示
                //TG.Common.MessageBox.ShowAndRedirect(this, "合同信息更新成功！", "CprMiddlePage.aspx?cprid=" + this.hid_cprid.Value);
                TG.Common.MessageBox.ResponseScriptBack(this, "合同信息更新成功！");
            }
        }
        //修改合同信息
        protected bool UpdateCoperation()
        {
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            TG.BLL.cm_SceneryCoperation bll_cop = new TG.BLL.cm_SceneryCoperation();
            TG.Model.cm_SceneryCoperation model_cop = bll_cop.GetModel(int.Parse(this.hid_cprid.Value));
            TG.Model.cm_Coperation model_cpr = new TG.Model.cm_Coperation();
            if (model_cop.cpr_FID != null)
            {
                model_cpr = bll_cpr.GetModel(int.Parse(model_cop.cpr_FID.ToString()));
            }
            //更新ad
            bool affectRow = false;
            //合同ID
            model_cop.cpr_Id = int.Parse(this.hid_cprid.Value);
            //客户ID
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cop.cst_Id = int.Parse(this.hid_cstid.Value);
            //合同编号
            model_cpr.cpr_No = this.txtcpr_No.Value.Trim();
            model_cop.cpr_No = this.txtcpr_No.Value.Trim();
            //项目类型
            model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            model_cop.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            //去掉标准合同类型
            model_cpr.cpr_Type2 = this.txt_cprType.InnerText;
            model_cop.cpr_Type2 = this.txt_cprType.InnerText;
            //合同名称
            model_cpr.cpr_Name = this.txt_cprName.Value;
            model_cop.cpr_Name = this.txt_cprName.Value;
            //建设单位
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            model_cop.BuildUnit = this.txt_cprBuildUnit.Value;
            //合同额
            model_cpr.cpr_Acount = Convert.ToDecimal(this.hidtxtcpr_Account.Value);
            model_cop.cpr_Acount = Convert.ToDecimal(this.hidtxtcpr_Account.Value);
            //实际合同额
            model_cpr.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            model_cop.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            //投资额
            model_cpr.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
            model_cop.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
            //实际投资额
            model_cpr.cpr_ShijiTouzi = Convert.ToDecimal(txtInvestAccount0.Value);
            model_cop.cpr_ShijiTouzi = Convert.ToDecimal(this.txtInvestAccount0.Value);
            //合同阶段
            string str_process = "";
            foreach (ListItem chk in this.chk_cprjd.Items)
            {
                if (chk.Selected)
                {
                    str_process += chk.Value + ",";
                }
            }
            str_process = str_process.IndexOf(",") > -1 ? str_process.Remove(str_process.Length - 1) : "";
            model_cpr.cpr_Process = str_process;
            model_cop.cpr_Process = str_process;
            //统计年份
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            model_cop.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            //签订时间
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            model_cop.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            //合同备注
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            model_cop.cpr_Mark = this.txtcpr_Remark.Value;
            //建筑面积
            model_cpr.BuildArea = this.hidtxt_buildArea.Value;
            model_cop.BuildArea = Convert.ToDecimal(this.txt_buildArea.Value);
            model_cop.BuildAreaunit = this.areaType.SelectedItem.Text;
            if (this.areaType.SelectedItem.Value != "1")
            {
                model_cpr.BuildArea = "0";
            }
            //负责人
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            model_cop.ChgPeople = this.txt_proFuze.Value;
            //更新项目经理ID qpl 20131225
            model_cpr.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cop.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            //项目经理
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            model_cop.ChgPhone = this.txt_fzphone.Value;
            //甲方负责人
            model_cpr.ChgJia = this.txtFParty.Value;
            model_cop.ChgJia = this.txtFParty.Value;
            //甲方负责人电话
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            model_cop.ChgJiaPhone = this.txt_jiafphone.Value;
            //工程地点
            if (this.txt_ProjectPosition.Value.Trim() != "")
            {
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
                model_cop.BuildPosition = this.txt_ProjectPosition.Value;
            }
            else
            {
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
                model_cop.BuildPosition = this.txt_ProjectPosition.Value;
            }
            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
                model_cop.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            else
            {
                model_cpr.Industry = this.ddProfessionType.Items[0].Value;
                model_cop.Industry = this.ddProfessionType.Items[0].Value;
            }
            //工程来源
            if (this.ddSourceWay.SelectedIndex != 0)
            {
                model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
                model_cop.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            }
            else
            {
                model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value;
                model_cop.BuildSrc = this.ddSourceWay.Items[0].Value;
            }
            //承接部门
            model_cpr.cpr_Unit = this.hid_cjbm.Value;
            model_cop.cpr_Unit = this.hid_cjbm.Value;
            //制表人
            model_cpr.TableMaker = this.txt_tbcreate.Value;
            model_cop.TableMaker = this.txt_tbcreate.Value;
            model_cpr.RegTime = Convert.ToDateTime(this.hid_cprtime.Value);
            model_cop.RegTime = Convert.ToDateTime(this.hid_cprtime.Value);
            model_cpr.UpdateBy = UserSysNo.ToString();
            model_cop.UpdateBy = UserSysNo.ToString();
            model_cpr.LastUpdate = DateTime.Now;
            model_cop.LastUpdate = DateTime.Now;
            //结构样式
            //update by 20130530 qpl
            model_cpr.StructType = GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            model_cop.StructType = GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            //建筑分类
            //update by 20130530 qpl
            model_cpr.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            model_cop.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            //设计等级
            //update 20131225 sgq
            model_cpr.BuildType = this.drp_buildtype.SelectedItem.Text;
            model_cop.BuildType = this.drp_buildtype.SelectedItem.Text;
            //楼层信息
            model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            model_cop.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //多吨建筑
            model_cpr.MultiBuild = this.txt_MultiBuild.Value;
            model_cop.MultiBuild = this.txt_MultiBuild.Value;
            //完成时间
            model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cop.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            //专业特征
            model_cpr.JieGou = "";
            model_cpr.Geips = "";
            model_cpr.Nuant = "";
            model_cpr.Dianq = "";
            //规划面积
            // model_cpr.cpr_Area = this.hidtxt_area.Value;
            try
            {
                affectRow = bll_cpr.Update(model_cpr);
                bll_cop.Update(model_cop);

                //修改合同状态
                if (HasEditAudit == "1")
                {
                    UpdateAuditEditStatus();
                }
            }
            catch (System.Exception ex)
            {
            }

            return affectRow;
        }

        //设置选择合同额方法
        public void SetUserSysNoAndRole()
        {
            //用户ID
            this.ChooseCustomer1.UserSysNo = int.Parse(GetCurMemID());
            //权限
            this.ChooseCustomer1.PreviewPower = GetPreviewPower();
        }
        //获取页面权限
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            return this.hid_cprid.Value;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();
        }
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }

        //工程来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }
        //合同阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_jd'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
        }
        //合同设计等级
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_SceneryCoperation] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    //保存客户ID
                    this.hid_cstid.Value = str_cstid;

                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Value = model_cst.Cst_No.Trim();
                        this.txtCst_Name.Value = model_cst.Cst_Name.Trim();
                        this.txtCpy_Address.Value = model_cst.Cpy_Address.Trim();
                        this.txtCst_Brief.Value = model_cst.Cst_Brief;
                        this.txtCode.Value = model_cst.Code ?? "";
                        this.txtLinkman.Value = model_cst.Linkman ?? "";
                        this.txtCpy_Phone.Value = model_cst.Cpy_Phone ?? "";
                        this.txtCpy_Fax.Value = model_cst.Cpy_Fax ?? "";
                    }
                }
            }
        }
        //显示合同信息
        protected void ShowCoperation(string cprid)
        {

            TG.BLL.cm_SceneryCoperation bll_cpr = new TG.BLL.cm_SceneryCoperation();
            TG.Model.cm_SceneryCoperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            //赋值
            this.txtcpr_No.Value = model_cpr.cpr_No == null ? "" : model_cpr.cpr_No.Trim();
            this.ddcpr_Type.Items.FindByText(model_cpr.cpr_Type.Trim()).Selected = true;
            //this.txt_cprType.Value = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();

            this.txt_cprName.Value = model_cpr.cpr_Name.Trim();
            this.txt_cprBuildUnit.Value = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
            //建筑类别
            this.drp_buildtype.Items.FindByText(model_cpr.BuildType.Trim()).Selected = true;

            this.txtcpr_Account.Value = model_cpr.cpr_Acount.ToString();
            //实际合同额
            this.txtcpr_Account0.Value = model_cpr.cpr_ShijiAcount.ToString();
            //投资额
            this.txtInvestAccount.Value = model_cpr.cpr_Touzi.ToString();
            //实际投资额
            this.txtInvestAccount0.Value = model_cpr.cpr_ShijiTouzi.ToString();

            if (model_cpr.cpr_Process.Trim() != "")
            {
                string[] array = model_cpr.cpr_Process.Split(new char[] { ',' }, StringSplitOptions.None);

                for (int i = 0; i < array.Length; i++)
                {
                    for (int j = 0; j < this.chk_cprjd.Items.Count; j++)
                    {
                        if (array[i].Trim() == this.chk_cprjd.Items[j].Value)
                        {
                            this.chk_cprjd.Items[j].Selected = true;
                        }
                    }
                }
            }
            //签订日期
            this.txtSingnDate.Value = Convert.ToDateTime(model_cpr.cpr_SignDate).ToString("yyyy-MM-dd");

            this.txtSingnDate2.Value = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToString("yyyy-MM-dd");
            this.txtCompleteDate.Value = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToString("yyyy-MM-dd");
            this.txtcpr_Remark.Value = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
            //建筑面积
            this.txt_buildArea.Value = Convert.ToString(model_cpr.BuildArea).Trim();
            //建筑面积单位
            if (this.areaType.Items.FindByText(model_cpr.BuildAreaunit) != null)
            {
                this.areaType.Items.FindByText(model_cpr.BuildAreaunit).Selected = true;
            }

            this.areaType1.Text = model_cpr.BuildAreaunit;
            areaType2.Text = model_cpr.BuildAreaunit;
            //项目经理
            this.txt_proFuze.Value = model_cpr.ChgPeople.Trim();
            //添加项目经理ID  qpl 20131225
            this.HiddenPMUserID.Value = model_cpr.PMUserID.ToString();
            //经理电话
            this.txt_fzphone.Value = model_cpr.ChgPhone.Trim();
            //甲方负责人
            this.txtFParty.Value = model_cpr.ChgJia.Trim();
            //甲方负责人电话
            this.txt_jiafphone.Value = model_cpr.ChgJiaPhone.Trim();
            //if (model_cpr.BuildPosition.Trim() != "" && model_cpr.BuildPosition.Trim() != "-1")
            //{
            //    this.ddProjectPosition.Items.FindByText(model_cpr.BuildPosition.Trim()).Selected = true;
            //}
            this.txt_ProjectPosition.Value = model_cpr.BuildPosition.Trim();
            if (model_cpr.Industry.Trim() != "" && model_cpr.Industry.Trim() != "-1")
            {
                this.ddProfessionType.Items.FindByText(model_cpr.Industry.Trim()).Selected = true;
            }
            if (model_cpr.BuildSrc.Trim() != "" && model_cpr.BuildSrc.Trim() != "-1")
            {
                this.ddSourceWay.Items.FindByText(model_cpr.BuildSrc.Trim()).Selected = true;
            }
            //if (o != null)
            //{
            //    if (o.ToString().Split(',')[1].Length > 0)
            //    {
            //        this.txt_cjbm.Enabled = true;
            //    }

            //}
            this.txt_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
            //承接部门
            this.hid_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
            //制表人
            this.txt_tbcreate.Value = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
            //录入时间
            this.hid_cprtime.Value = model_cpr.RegTime.ToString();
            //结构形式
            string StrStruct = model_cpr.StructType == null ? "" : model_cpr.StructType.Trim();
            //注销  20140115
            //LoadStructData(StrStruct, this.asTreeviewStruct);
            //结构形式值 qpl  20140115
            StructString = StrStruct;
            //建筑分类
            string structtype = model_cpr.BuildStructType == null ? "" : model_cpr.BuildStructType.Trim();
            //注销  20140115
            //LoadStructData(structtype, this.asTreeviewStructType);
            //建筑分类值  qpl  20140115
            StructTypeString = structtype;
            //层数
            string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
            this.txt_upfloor.Value = floors[0].ToString().Trim();
            this.txt_downfloor.Value = floors[1].ToString().Trim();
            //多吨建筑
            this.txt_MultiBuild.Value = model_cpr.MultiBuild;
        }

        //更新合同修改流程状态
        protected void UpdateAuditEditStatus()
        {
            //TG.BLL.cm_CoperationEditRecord bll = new TG.BLL.cm_CoperationEditRecord();
            ////TG.Model.cm_CoperationEditRecord model = bll.GetModel(AuditEditSysNo);
            //model.Status = "F";
            //bll.Update(model);
            ////消息状态
            //string strSql = " Update cm_SysMsg Set IsDone='D' Where [SysNo]=" + MessageID;
            //TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);Cst_Id
        }
        // 检查是否在审核队列中
        private bool CheckAudit(int coperationSysNo)
        {
            return new TG.BLL.cm_CoperationAudit().IsExist(coperationSysNo);

        }
        //建筑分类
        protected void BindBuildStuctType()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
            //子项建筑分类 qpl  20131225
            //BindProInfoConfig("BuildType", this.asTreeviewStructType_sub.RootNode);
            //this.asTreeviewStructType_sub.CollapseAll();
        }
        //结构形式
        protected void BindStructType()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();

        }

        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="infokey"></param>
        /// <param name="rootnode"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode, bool showsubbox)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root, showsubbox);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="xmlroot"></param>
        /// <param name="root"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root, bool showsubbox)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                //标示子节点是否显示CheckBox qpl 20140108
                switch (name)
                {
                    case "特级":
                    case "一级":
                    case "二级":
                    case "三级":
                        linknode.EnableCheckbox = !showsubbox;
                        break;
                    default:
                        linknode.EnableCheckbox = showsubbox;
                        break;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);

                        //子节点的checkbox 不可用  qpl 20140108
                        sublinknode.EnableCheckbox = showsubbox;

                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode, showsubbox);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //获取所有树文字数据
        protected void GetAllNodeTex(ASTreeViewNode nodes, ref List<string> alltext)
        {
            foreach (ASTreeViewNode node in nodes.ChildNodes)
            {
                alltext.Add(node.NodeText);
                if (node.ChildNodes.Count > 0)
                {
                    GetAllNodeTex(node, ref alltext);
                }
            }
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
            //qpl 20131225
            //this.asTreeViewBuildType.Theme = macOS;
            //qpl 20131225
            //this.asTreeviewStructType_sub.Theme = macOS;
            //this.asTreeviewStruct_sub.Theme = macOS;
        }

        //update by 20130530 qpl
        //获取选中树所有节点值并返回值
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        //获取等级选中节点 qpl 20140108
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes, bool flag)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                }
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //加载存储的树节点
        protected void LoadStructData(string strData, ASDropDownTreeView contrl)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                //加载所有选中节点
                List<string> nodelist = new List<string>();
                //第一级
                string[] strarray = strData.Split('+');

                for (int i = 0; i < strarray.Length; i++)
                {
                    //特殊第二级
                    string[] strsubarray = strarray[i].Split('^');
                    for (int j = 0; j < strsubarray.Length; j++)
                    {
                        //递归的三级以后
                        string[] strendarray = strsubarray[j].Split('*');
                        nodelist.Add(strendarray[strendarray.Length - 1]);
                    }
                }
                //选中节点
                foreach (string nodevalue in nodelist)
                {
                    if (contrl.FindByValue(nodevalue) != null)
                    {
                        contrl.FindByValue(nodevalue).CheckedState = ASTreeViewCheckboxState.Checked;
                    }

                }
            }
        }
        //加载存储的树节点  qpl   20140108  
        protected void LoadStructData(string strData, ASDropDownTreeView contrl, bool flag)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                //加载所有选中节点
                List<string> nodelist = new List<string>();

                if (strData.IndexOf('+') > -1)
                {
                    //第一级
                    string[] strarray = strData.Split('+');
                    for (int i = 0; i < strarray.Length; i++)
                    {
                        nodelist.Add(strarray[i].ToString());
                    }
                }
                else
                {
                    nodelist.Add(strData);
                }
                //选中节点
                foreach (string nodevalue in nodelist)
                {
                    if (contrl.FindByValue(nodevalue) != null)
                    {
                        contrl.FindByValue(nodevalue).CheckedState = ASTreeViewCheckboxState.Checked;
                    }
                }
            }
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                //string NotShowUnitList = base.NotShowUnitList;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;
                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
                //this.NotShowUnitList.Value = NotShowUnitList;
            }
        }
    }
}