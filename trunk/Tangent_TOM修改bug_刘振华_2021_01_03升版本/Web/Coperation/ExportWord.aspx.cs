﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;

using System.IO;
using Aspose.Words;
using TG.Common;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;



namespace TG.Web.Coperation
{
    public partial class ExportWord : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 合同系统自增号
        /// </summary>
        public int CoperationSysNo
        {
            get
            {
                int coperationSysNo = 0;
                int.TryParse(Request["coperationSysNo"], out coperationSysNo);
                return coperationSysNo;
            }
        }

        /// <summary>
        /// 客户基本信息
        /// </summary>
        public TG.Model.cm_CustomerInfo CustomerInfo { get; set; }

        /// <summary>
        /// 合同基本信息
        /// </summary>
        public TG.Model.cm_CoperationAuditListView CoperationAuditEntity { get; set; }

        /// <summary>
        /// 审核记录
        /// </summary>
        public TG.Model.cm_CoperationAudit coper { get; set; }
        //word程序类

        //数组

        protected void Page_Load(object sender, EventArgs e)
        {
            string action = Request["action"] ?? "0";
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb1 = new StringBuilder("");
            //权限
            string strwhere = Request["strwhere"];
            //合同年份
            string year = Request["drp_year"];
            //合同编号
            string txtcpr_No = Request["txtcpr_No"];
            //合同分类
            string drp_type = Request["drp_type"];
            //合同类型
            string cprtype = Request["cprtype"];
            //合同名称
            string txt_cprname = Request["txt_cprname"];
            //建筑规模1
            string txt_Area = Request["txt_Area"];
            //建筑规模2
            string txt_Area2 = Request["txt_Area2"];
            //规模单位
           // string drp_AreaUnit = Request["drp_AreaUnit"];
            //建设单位
            string txt_cprBuildUnit = Request["txt_cprBuildUnit"];
            //结构形式
            string asTreeviewStruct = Request["asTreeviewStruct"];
            //建筑分类
            string asTreeviewStructType = Request["asTreeviewStructType"];
            //建筑类别
            string drp_buildtype = Request["drp_buildtype"];
            //工程负责人
            string txt_proFuze = Request["txt_proFuze"];
            //甲方负责人
            string txtFParty = Request["txtFParty"];
            //承接部门
            string drp_unit = Request["drp_unit"];
            //合同额1
            string txtcpr_Account = Request["txtcpr_Account"];
            //合同额2
            string txtcpr_Account2 = Request["txtcpr_Account2"];
            //工程地点
            string txt_Address = Request["txt_Address"];
            //行业性质
            string ddType = Request["ddType"];
            //合同阶段
            string chk_cprjd = Request["chk_cprjd"];
            //工程来源
            string ddSourceWay = Request["ddSourceWay"];
            //合同签订日期1
            string txt_signdate_1 = Request["txt_signdate_1"];
            //合同签订日期2
            string txt_signdate2_1 = Request["txt_signdate2_1"];
            //合同完成日期1
            string txt_finishdate = Request["txt_finishdate"];
            //合同完成日期2
            string txt_finishdate2 = Request["txt_finishdate2"];
            //合同统计日期
             string txt_signdate = Request["txt_signdate"];
            //合同统计日期2
            string txt_signdate2 = Request["txt_signdate2"];
            //合同收费1
            string projcharge1 = Request["projcharge1"];
            //合同收费2
            string projcharge2 = Request["projcharge2"];
            //按照客户
            string txt_cst = Request["txt_cst"];

            string startTime = Request["startTime"];//录入时间
            string endTime = Request["endTime"];//录入时间  
            //字段名称
            string str_columnsname = Request["str_columnsname"];
            //字段名称标题
            string str_columnschinaname = Request["str_columnschinaname"];

            //查询方式or或and
            string andor = Request["andor"] ?? "and";

            //标签导出word模板

            //用npoi导出word模板
            // ExporTotWord();
            //生成word文档
            //exword();
            if (action == "search")
            {
                //合同编号
                if (!string.IsNullOrEmpty(txtcpr_No))
                {
                    sb.Append(" AND ( cpr_No like '%" + txtcpr_No.Trim() + "%' ");
                }
                //合同分类
                if (!string.IsNullOrEmpty(drp_type) && !drp_type.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  cpr_Type=N'" + drp_type.Trim() + "' ");
                }
                //合同类型
                if (!string.IsNullOrEmpty(cprtype) && !cprtype.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  cpr_Type2=N'" + cprtype.Trim() + "' ");
                }
                //合同名称

                if (!string.IsNullOrEmpty(txt_cprname))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_cprname.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append(" cpr_Name LIKE '%" + keyname + "%' ");
                }
                //建筑规模1
                //建筑规模2
                if (!string.IsNullOrEmpty(txt_Area) && string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                
                        sb.Append(" Convert(float,BuildArea) >=" + txt_Area.Trim() + " ");
                    
                    
                }
                else if (string.IsNullOrEmpty(txt_Area) && !string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                  
                        sb.Append(" Convert(float,BuildArea) <=" + txt_Area2.Trim() + " ");
                    

                }
                else if (!string.IsNullOrEmpty(txt_Area) && !string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
               
                        sb.Append(" (Convert(float,BuildArea) >=" + txt_Area.Trim() + " AND Convert(float,BuildArea) <=" + txt_Area2.Trim() + ") ");
                   
                }
                //建设单位
                if (!string.IsNullOrEmpty(txt_cprBuildUnit))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildUnit like '%" + txt_cprBuildUnit.Trim() + "%' ");
                }
                //结构形式
                if (!string.IsNullOrEmpty(asTreeviewStruct) && !asTreeviewStruct.Contains("请选择结构形式"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  replace(replace(replace(StructType,'+',','),'*',','),'^',',')  LIKE '%" + asTreeviewStruct + "%' ");
                }
                //建筑分类
                if (!string.IsNullOrEmpty(asTreeviewStructType) && !asTreeviewStructType.Contains("请选择建筑分类"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  replace(replace(replace(BuildStructType,'+',','),'*',','),'^',',') LIKE '%" + asTreeviewStructType + "%' ");
                }
                //建筑类别
                if (!string.IsNullOrEmpty(drp_buildtype) && !drp_buildtype.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("   BuildType =N'" + drp_buildtype.Trim() + "' ");
                }
                //工程负责人
                if (!string.IsNullOrEmpty(txt_proFuze))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_proFuze.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("   ChgPeople='" + keyname + "' ");
                }
                //甲方负责人
                if (!string.IsNullOrEmpty(txtFParty))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txtFParty.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("   ChgJia='" + keyname + "' ");
                }
                //年份
                if (year != "-1" && !string.IsNullOrEmpty(year))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.AppendFormat(" year(cpr_SignDate)={0} ", year);
                }
                //承接部门
                if (!string.IsNullOrEmpty(drp_unit) && !drp_unit.Contains("全院部门"))
                {
                    drp_unit = drp_unit.Substring(0, drp_unit.Length - 1);

                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  (cpr_Unit in (" + drp_unit.Trim() + ")) ");
                }
                //合同额1
                //合同额2
                if (!string.IsNullOrEmpty(txtcpr_Account) && string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  cpr_Acount >= " + txtcpr_Account.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtcpr_Account) && !string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  cpr_Acount <= " + txtcpr_Account2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtcpr_Account) && !string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  (cpr_Acount>=" + txtcpr_Account.Trim() + " AND cpr_Acount <= " + txtcpr_Account2.Trim() + ") ");
                }
                //工程地点
                if (!string.IsNullOrEmpty(txt_Address))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  BuildPosition like '%" + txt_Address.Trim() + "%' ");
                }
                //行业性质
                if (!string.IsNullOrEmpty(ddType) && !ddType.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  Industry='" + ddType.Trim() + "' ");
                }
                //合同阶段
                string str_process = chk_cprjd;
                if (!string.IsNullOrEmpty(str_process))
                {
                    str_process = str_process.IndexOf(",") > -1 ? str_process.Remove(str_process.Length - 1) : "";
                    str_process = str_process.Replace("方案", "27").Replace("初设", "28").Replace("施工图", "29").Replace("其他", "30");
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  cpr_Process like '%" + str_process + "%' ");
                }

                //工程来源
                if (!string.IsNullOrEmpty(ddSourceWay) && !ddSourceWay.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append("  BuildSrc='" + ddSourceWay.Trim() + "' ");
                }
                //合同签订时间1
                //合同签订时间2
                if (!string.IsNullOrEmpty(txt_signdate_1) && string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate >= '" + txt_signdate_1.Trim() + "' ");
                }
                else if (string.IsNullOrEmpty(txt_signdate_1) && !string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate <= '" + txt_signdate2_1.Trim() + "' ");
                }
                else if (!string.IsNullOrEmpty(txt_signdate_1) && !string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_SignDate BETWEEN '" + txt_signdate_1.Trim() + "' AND '" + txt_signdate2_1.Trim() + "') ");
                }
                ////合同完成日期1
                ////合同完成日期2
                //if (!string.IsNullOrEmpty(txt_finishdate) && string.IsNullOrEmpty(txt_finishdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_DoneDate >= '" + txt_finishdate.Trim() + "' ");
                //}
                //else if (string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_DoneDate <= '" + txt_finishdate2.Trim() + "' ");
                //}
                //else if (!string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" (cpr_DoneDate BETWEEN '" + txt_finishdate.Trim() + "' AND '" + txt_finishdate2.Trim() + "') ");
                //}
                ////合同统计年份1
                ////合同统计年份2
                //if (!string.IsNullOrEmpty(txt_signdate) && string.IsNullOrEmpty(txt_signdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_SignDate >= '" + txt_signdate.Trim() + "' ");
                //}
                //else if (string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_SignDate <= '" + txt_signdate2.Trim() + "' ");
                //}
                //else if (!string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" (cpr_SignDate BETWEEN '" + txt_signdate.Trim() + "' AND '" + txt_signdate2.Trim() + "') ");
                //}
                //合同收费1
                //合同收费2
                if (!string.IsNullOrEmpty(projcharge1.Trim()) && string.IsNullOrEmpty(projcharge2.Trim()))
                {
                    int Acount = 0;
                    //等于false：acount=0不需要加条件，等于true：acount=projcharge1
                    if (int.TryParse(projcharge1, out Acount))
                    {
                        if (sb.ToString() != "")
                        {
                            sb.Append(" " + andor + " ");
                        }
                        else
                        {
                            sb.Append(" and ( ");
                        }
                        sb.Append(" ssze >= " + projcharge1.Trim() + " ");
                    }
                }
                else if (string.IsNullOrEmpty(projcharge1.Trim()) && !string.IsNullOrEmpty(projcharge2.Trim()))
                {
                    int Acount = 0;
                    //等于false：acount=0不需要加条件，等于true：acount=projcharge2
                    if (int.TryParse(projcharge2, out Acount))
                    {
                        if (sb.ToString() != "")
                        {
                            sb.Append(" " + andor + " ");
                        }
                        else
                        {
                            sb.Append(" and ( ");
                        }
                        sb.Append(" ssze <= " + projcharge2.Trim() + " ");
                    }
                }
                else if (!string.IsNullOrEmpty(projcharge1.Trim()) && !string.IsNullOrEmpty(projcharge2.Trim()))
                {
                    int Acount = 0;
                    if (int.TryParse(projcharge1, out Acount) && int.TryParse(projcharge2, out Acount))
                    {
                        if (sb.ToString() != "")
                        {
                            sb.Append(" " + andor + " ");
                        }
                        else
                        {
                            sb.Append(" and ( ");
                        }
                        sb.Append(" ssze BETWEEN " + projcharge1.Trim() + " AND " + projcharge2.Trim() + " ");
                    }

                }
                //按照客户
                if (!string.IsNullOrEmpty(txt_cst))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_cst.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  cst_Id IN (Select Cst_Id From cm_CustomerInfo Where Cst_Name like '%" + keyname + "%') ");
                }

                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat("  InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat("  InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat("  (InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59)");
                }
                if (sb.ToString() != "")
                {
                    sb.Append(")");
                }
               

                //加上权限
                sb.Append(strwhere);               

                ToExcelExport(sb.ToString(), str_columnsname, str_columnschinaname);
            }
            else
            {
                ToWordExport();
            }

        }

        private void ToExcelExport(string strWhere, string str_columnsname, string str_columnschinaname)
        {

            DataTable dt = new TG.BLL.cm_Coperation().GetCoperationExportInfo(strWhere).Tables[0];
            if (dt.Rows.Count > 0)
            {

                string modelPath = " ~/TemplateXls/Scr_Coperation.xls";

                HSSFWorkbook wb = null;

                //如果没有模板路径，则创建一个空的workbook和一个空的sheet
                if (string.IsNullOrEmpty(modelPath))
                {
                    wb = new HSSFWorkbook();
                    wb.CreateSheet();
                    wb.GetSheetAt(0).CreateRow(0);
                }
                else
                {
                    using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        wb = new HSSFWorkbook(fileStream);
                        fileStream.Close();
                    }
                }

                ICellStyle style0 = wb.CreateCellStyle();
                style0.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style0.VerticalAlignment = VerticalAlignment.CENTER;
                style0.WrapText = true;
                //style0.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                //style0.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                //style0.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                //style0.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font0 = wb.CreateFont();
                font0.FontHeightInPoints = 12;//字号
                font0.FontName = "宋体";//字体
                font0.Boldweight = (short)700;
                style0.SetFont(font0);


                //标题样式
                ICellStyle style1 = wb.CreateCellStyle();
                style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style1.VerticalAlignment = VerticalAlignment.CENTER;
                style1.WrapText = true;
                style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font1 = wb.CreateFont();
                font1.FontHeightInPoints = 12;//字号
                font1.FontName = "宋体";//字体
                font1.Boldweight = (short)700;
                style1.SetFont(font1);


                //内容样式
                ICellStyle style2 = wb.CreateCellStyle();
                style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style2.VerticalAlignment = VerticalAlignment.CENTER;
                style2.WrapText = true;
                style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font2 = wb.CreateFont();
                font2.FontHeightInPoints = 10;//字号
                font2.FontName = "宋体";//字体
                style2.SetFont(font2);

                //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
                var ws = wb.GetSheet("Sheet1");
                if (ws == null)
                    ws = wb.GetSheetAt(0);

                //读取标题
                IRow dataRowTitle = ws.GetRow(0);
                if (!string.IsNullOrEmpty(str_columnschinaname))
                {
                    string[] columnsnamelist = str_columnschinaname.Split(',');
                    for (int j = 0; j < columnsnamelist.Length; j++)
                    {
                        ICell celltitle = dataRowTitle.CreateCell(j + 1);
                        celltitle.CellStyle = style1;
                        celltitle.SetCellValue(columnsnamelist[j].Trim());
                    }
                }
                int row = 1;
                decimal sumarea = 0, sumaccount = 0, sumssze = 0,sumshijiaccount=0;
                int areaIndex = 4, acountIndex = 5,shijiacountIndex=6, sszeIndex = 10;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(i + row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(i + row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(i + 1);

                    //cell = dataRow.CreateCell(1);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dt.Rows[i]["cpr_No"].ToString());

                    //cell = dataRow.CreateCell(2);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dt.Rows[i]["cpr_Name"].ToString());

                    //cell = dataRow.CreateCell(3);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dt.Rows[i]["cpr_Type"].ToString());                   

                    //cell = dataRow.CreateCell(4);
                    //cell.CellStyle = style2;
                    //decimal buildare = Convert.ToDecimal(dt.Rows[i]["BuildArea"]);
                    //cell.SetCellValue(buildare.ToString());
                    //sumarea += buildare;

                    //cell = dataRow.CreateCell(5);
                    //cell.CellStyle = style2;
                    //decimal account = Convert.ToDecimal(dt.Rows[i]["cpr_Acount"]);
                    //cell.SetCellValue(account.ToString("f4"));
                    //sumaccount += account;

                    //cell = dataRow.CreateCell(6);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dt.Rows[i]["cpr_Unit"].ToString());

                    //cell = dataRow.CreateCell(7);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_SignDate"]).ToString("yyyy-MM-dd"));

                    //cell = dataRow.CreateCell(8);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_DoneDate"]).ToString("yyyy-MM-dd"));

                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dt.Rows[i]["ChgPeople"].ToString());

                    //cell = dataRow.CreateCell(10);
                    //cell.CellStyle = style2;
                    //decimal ssze = 0;
                    //if (!string.IsNullOrEmpty(dt.Rows[i]["ssze"].ToString()))
                    //{
                    //    ssze = Convert.ToDecimal(dt.Rows[i]["ssze"]);
                    //}
                    //cell.SetCellValue(ssze.ToString());
                    //sumssze += ssze;

                    //cell = dataRow.CreateCell(11);
                    //cell.CellStyle = style2;
                    //if (!string.IsNullOrEmpty(dt.Rows[i]["InsertDate"].ToString()))
                    //{
                    //    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["InsertDate"]).ToString("yyyy-MM-dd"));
                    //}

                    if (!string.IsNullOrEmpty(str_columnsname))
                    {
                        decimal buildare = Convert.ToDecimal(dt.Rows[i]["BuildArea"]);
                        sumarea += buildare;
                        decimal ssze = 0;
                        if (!string.IsNullOrEmpty(dt.Rows[i]["ssze"].ToString()))
                        {
                            ssze = Convert.ToDecimal(dt.Rows[i]["ssze"]);
                        }
                        sumssze += ssze;
                        decimal account = Convert.ToDecimal(dt.Rows[i]["cpr_Acount"]);
                        sumaccount += account;

                        decimal shijiaccount = Convert.ToDecimal(dt.Rows[i]["cpr_ShijiAcount"]);
                        sumshijiaccount += shijiaccount;

                        string[] columnslist = str_columnsname.Split(',');
                        for (int j = 0; j < columnslist.Length; j++)
                        {
                            cell = dataRow.CreateCell(j + 1);
                            cell.CellStyle = style2;
                            string cellvalue = "";
                            if (columnslist[j].Trim() == "Floor")
                            {
                                if (!string.IsNullOrEmpty(cellvalue))
                                {
                                    string cellvalueitem = dt.Rows[i][columnslist[j].Trim()].ToString();
                                    string[] arr = cellvalueitem.Split('|');
                                    cellvalue = "地上:" + arr[0] + " 地下:" + arr[1];
                                }

                            }
                            else if (columnslist[j].Trim() == "cpr_Process")
                            {
                                string cellvalueitem = dt.Rows[i][columnslist[j].Trim()].ToString();
                                if (!string.IsNullOrEmpty(cellvalueitem))
                                {
                                    string temp = "";
                                    string[] arr = cellvalueitem.Split(',');
                                    for (int k = 0; k < arr.Length; k++)
                                    {
                                        if (arr[k] == "27")
                                        {
                                            temp = "方案,";
                                        }
                                        if (arr[k] == "28")
                                        {
                                            temp += "初设,";
                                        }
                                        if (arr[k] == "29")
                                        {
                                            temp += "施工图,";
                                        }
                                        if (arr[k] == "30")
                                        {
                                            temp += "其他,";
                                        }
                                    }
                                    cellvalue = temp;
                                }

                            }
                            else
                            {
                                cellvalue = dt.Rows[i][columnslist[j].Trim()].ToString();
                            }
                            if (columnslist[j].Trim() == "BuildArea")
                            {
                                areaIndex = j + 1;
                            }
                            if (columnslist[j].Trim() == "cpr_Acount")
                            {
                                acountIndex = j + 1;
                            }
                            if (columnslist[j].Trim() == "cpr_ShijiAcount")
                            {
                                shijiacountIndex = j + 1;
                            }
                            if (columnslist[j].Trim() == "ssze")
                            {
                                sszeIndex = j + 1;
                            }
                            cell.SetCellValue(cellvalue);
                        }
                    }
                }
                IRow dataRowbottom = ws.CreateRow(dt.Rows.Count + 1);//最后一个
                ICell cellbottom = dataRowbottom.CreateCell(1);
                cellbottom.SetCellValue("合计：");
                cellbottom.CellStyle = style0;

                if (str_columnsname.Contains("BuildArea"))
                {
                    cellbottom = dataRowbottom.CreateCell(areaIndex);
                    cellbottom.SetCellValue(sumarea.ToString("f2"));
                    cellbottom.CellStyle = style0;
                }
                if (str_columnsname.Contains("cpr_Acount"))
                {
                    cellbottom = dataRowbottom.CreateCell(acountIndex);
                    cellbottom.SetCellValue(sumaccount.ToString("f4"));
                    cellbottom.CellStyle = style0;
                }
                if (str_columnsname.Contains("cpr_ShijiAcount"))
                {
                    cellbottom = dataRowbottom.CreateCell(shijiacountIndex);
                    cellbottom.SetCellValue(sumshijiaccount.ToString("f4"));
                    cellbottom.CellStyle = style0;
                }
                if (str_columnsname.Contains("ssze"))
                {
                    cellbottom = dataRowbottom.CreateCell(sszeIndex);
                    cellbottom.SetCellValue(sumssze.ToString("f4"));
                    cellbottom.CellStyle = style0;
                }
                using (MemoryStream memoryStream = new MemoryStream())
                {

                    wb.Write(memoryStream);

                    string name = System.Web.HttpContext.Current.Server.UrlEncode("合同高级查询列表.xls");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                    Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.ContentEncoding = Encoding.UTF8;
                    wb = null;
                    Response.End();
                }
            }
            else
            {
                Response.Write("<script type='javascript'>alert('导出没有数据');</script>");
            }

        }

        private void ToWordExport()
        {
            //throw new NotImplementedException();
            TG.BLL.cm_Coperation copbll = new BLL.cm_Coperation();
            TG.BLL.cm_CoperationAudit auditBll = new BLL.cm_CoperationAudit();
            TG.BLL.cm_SubCoperation subBll = new BLL.cm_SubCoperation();
            TG.BLL.cm_Project cm_proBll = new TG.BLL.cm_Project();
            coper = new TG.BLL.cm_CoperationAudit().GetModelByCoperationSysNo(CoperationSysNo);
            //判断导出那个iso表单，0为合同审核表，1为勘察设计表
            string option = Request.QueryString["option"];
            //审核合同项目的id
            string copid = Request.QueryString["coperationSysNo"];
            //管理级别
            string level = Request.QueryString["level"];
            //审核结果的id
            string auditno = Request.QueryString["auditNo"];
            TG.Model.cm_CoperationAudit modelAudit = auditBll.GetModel(int.Parse(auditno));

            List<TG.Model.cm_CoperationAuditListView> listCop = auditBll.GetCoperationListView("AND cpr_Id=" + copid);
            List<TG.Model.cm_SubCoperation> listSubCop = subBll.GetModelList("cpr_Id=" + copid);
            ReportWord report = new ReportWord();

            ///合同审批表
            #region 合同审批
            if (option == "0")
            {
                //合同信息
                GetCoperationInfo();
                //客户信息
                GetCustomerInfo();

                //审核记录

                string path = Server.MapPath(@"~\TemplateWord\QR-7.1-01.doc");

                Aspose.Words.Document doc = new Aspose.Words.Document(path); //载入模板

                //BuildUnit cpr_Id cpr_Name cpr_No cpr_Unit DataTime fourTime fourUnit fristTime fristUnit leaderAdvice leaderTime level productAdvice productTime secondTime secondUnit thirdTime thirdUnit
                if (listCop.Count > 0 && modelAudit != null)
                {
                    TG.Model.cm_CoperationAuditListView modelCop = new Model.cm_CoperationAuditListView();
                    foreach (TG.Model.cm_CoperationAuditListView item in listCop)
                    {
                        if (item.AuditStatus == "J" || item.AuditStatus == "H")
                        {
                            modelCop = item;
                        }
                    }
                    //  2013-06-09 10:15:21,2013-06-09 10:16:34,2013-06-09 10:17:07,2013-06-09 10:17:33,2013-06-09 10:18:22
                    //report.InsertValue("BuildUnit", modelCopCop.BuildUnit.Trim());
                    if (modelCop.AuditStatus == "J")
                    {
                        if (modelAudit.LegalAdviserProposal.Length > 0)
                        {
                            if (doc.Range.Bookmarks["BuildUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                                mark.Text = modelCop.BuildUnit == null ? "" : modelCop.BuildUnit.Trim();
                            }
                            // report.InsertValue("cpr_Id", modelCop.cpr_Id.ToString().Trim());
                            if (doc.Range.Bookmarks["cpr_Id"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_Id"];
                                mark.Text = modelCop.cpr_Id.ToString().Trim();
                            }
                            // report.InsertValue("cpr_Name", modelCop.cpr_Name.Trim());
                            if (doc.Range.Bookmarks["cpr_Name"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                                mark.Text = modelCop.cpr_Name == null ? "" : modelCop.cpr_Name.Trim();
                            }
                            // report.InsertValue("cpr_No", modelCop.cpr_No.Trim());
                            if (doc.Range.Bookmarks["cpr_No"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                                mark.Text = modelCop.cpr_No == null ? "" : modelCop.cpr_No.Trim();
                            }
                            // report.InsertValue("cpr_Unit", modelCop.cpr_Unit.Trim());
                            if (doc.Range.Bookmarks["cpr_Unit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                                mark.Text = modelCop.cpr_Unit == null ? "" : modelCop.cpr_Unit.Trim();
                            }
                            //report.InsertValue("DataTime", modelAudit.InDate.ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["DataTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["DataTime"];
                                mark.Text = modelAudit.InDate.ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("fourTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["fourTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["fourTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd");
                            }
                            //report.InsertValue("fourUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[4]));
                            if (doc.Range.Bookmarks["fourUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["fourUnit"];
                                mark.Text = RoleToCompany(4);
                            }
                            // report.InsertValue("fristTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["fristTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["fristTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("fristUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[1]));
                            if (doc.Range.Bookmarks["fristUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["fristUnit"];
                                mark.Text = RoleToCompany(1);
                            }
                            // report.InsertValue("leaderAdvice", modelAudit.GeneralManagerProposal);
                            if (doc.Range.Bookmarks["leaderAdvice"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["leaderAdvice"];
                                mark.Text = modelAudit.GeneralManagerProposal == null ? "" : modelAudit.GeneralManagerProposal;
                            }
                            // report.InsertValue("leaderTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[4]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["leaderTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["leaderTime"];
                                if (modelAudit.AuditDate.Split(',').Length > 4)
                                {
                                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[4]).ToString("yyyy-MM-dd");
                                }
                                else
                                {
                                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd");
                                }

                            }
                            //report.InsertValue("level", level == "0" ? "一级" : "二级");
                            if (doc.Range.Bookmarks["level"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["level"];
                                mark.Text = level == "0" ? "一级" : "二级";
                            }
                            //report.InsertValue("productAdvice", modelAudit.OperateDepartmentProposal.Replace('|', ' '));
                            if (doc.Range.Bookmarks["productAdvice"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["productAdvice"];
                                mark.Text = modelAudit.OperateDepartmentProposal.Replace('|', ' ');
                            }
                            //report.InsertValue("productTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["productTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["productTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                            }
                            //   report.InsertValue("secondTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["secondTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["secondTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("secondUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[2]));
                            if (doc.Range.Bookmarks["secondUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["secondUnit"];
                                mark.Text = RoleToCompany(2);
                            }
                            //report.InsertValue("thirdTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["thirdTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["thirdTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("thirdUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[3]));
                            if (doc.Range.Bookmarks["thirdUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["thirdUnit"];
                                mark.Text = RoleToCompany(3);
                            }
                            //report.InsertValue("tableMarker", modelCop.TableMaker.Trim());
                            if (doc.Range.Bookmarks["tableMarker"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["tableMarker"];
                                mark.Text = modelCop.TableMaker == null ? "" : modelCop.TableMaker.Trim();
                            }
                            //report.InsertValue("JingBan", modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim());
                            if (doc.Range.Bookmarks["JingBan"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["JingBan"];
                                mark.Text = modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim();
                            }
                            doc.Save("《合同评审表》.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                            // string wordPath = context.Server.MapPath(@"~\TemplateWord\repordWord1.doc");
                            //下载
                            //if (System.IO.File.Exists(wordPath))
                            //{
                            //    report.SaveDocument(wordPath);
                            //    context.Response.ContentType = "application/x-zip-compressed";
                            //    context.Response.AddHeader("Content-Disposition", "attachment;filename=合同评审表 .doc");
                            //    context.Response.TransmitFile(wordPath);
                            //    context.Response.BufferOutput = true;
                            //}
                        }
                        else
                        {
                            if (doc.Range.Bookmarks["BuildUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                                mark.Text = modelCop.BuildUnit == null ? "" : modelCop.BuildUnit.Trim();
                            }
                            // report.InsertValue("cpr_Id", modelCop.cpr_Id.ToString().Trim());
                            if (doc.Range.Bookmarks["cpr_Id"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_Id"];
                                mark.Text = modelCop.cpr_Id.ToString().Trim();
                            }
                            // report.InsertValue("cpr_Name", modelCop.cpr_Name.Trim());
                            if (doc.Range.Bookmarks["cpr_Name"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                                mark.Text = modelCop.cpr_Name == null ? "" : modelCop.cpr_Name.Trim();
                            }
                            // report.InsertValue("cpr_No", modelCop.cpr_No.Trim());
                            if (doc.Range.Bookmarks["cpr_No"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                                mark.Text = modelCop.cpr_No == null ? "" : modelCop.cpr_No.Trim();
                            }
                            // report.InsertValue("cpr_Unit", modelCop.cpr_Unit.Trim());
                            if (doc.Range.Bookmarks["cpr_Unit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                                mark.Text = modelCop.cpr_Unit == null ? "" : modelCop.cpr_Unit.Trim();
                            }
                            //report.InsertValue("DataTime", modelAudit.InDate.ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["DataTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["DataTime"];
                                mark.Text = modelAudit.InDate.ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("fourTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd"));

                            //report.InsertValue("fourUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[4]));

                            // report.InsertValue("fristTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["fristTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["fristTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("fristUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[1]));
                            if (doc.Range.Bookmarks["fristUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["fristUnit"];
                                mark.Text = RoleToCompany(1);
                            }
                            // report.InsertValue("leaderAdvice", modelAudit.GeneralManagerProposal);
                            if (doc.Range.Bookmarks["leaderAdvice"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["leaderAdvice"];
                                mark.Text = modelAudit.GeneralManagerProposal == null ? "" : modelAudit.GeneralManagerProposal;
                            }
                            // report.InsertValue("leaderTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[4]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["leaderTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["leaderTime"];
                                if (modelAudit.AuditDate.Split(',').Length > 4)
                                {
                                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[4]).ToString("yyyy-MM-dd");
                                }
                                else
                                {
                                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd");
                                }

                            }
                            //report.InsertValue("level", level == "0" ? "一级" : "二级");
                            if (doc.Range.Bookmarks["level"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["level"];
                                mark.Text = level == "0" ? "一级" : "二级";
                            }
                            //report.InsertValue("productAdvice", modelAudit.OperateDepartmentProposal.Replace('|', ' '));
                            if (doc.Range.Bookmarks["productAdvice"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["productAdvice"];
                                mark.Text = modelAudit.OperateDepartmentProposal.Replace('|', ' ');
                            }
                            //report.InsertValue("productTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["productTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["productTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                            }
                            //   report.InsertValue("secondTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["secondTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["secondTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("secondUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[2]));
                            if (doc.Range.Bookmarks["secondUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["secondUnit"];
                                mark.Text = RoleToCompany(2);
                            }
                            //report.InsertValue("thirdTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd"));
                            if (doc.Range.Bookmarks["thirdTime"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["thirdTime"];
                                mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd");
                            }
                            // report.InsertValue("thirdUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[3]));
                            if (doc.Range.Bookmarks["thirdUnit"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["thirdUnit"];
                                mark.Text = RoleToCompany(4);
                            }
                            //report.InsertValue("tableMarker", modelCop.TableMaker.Trim());
                            if (doc.Range.Bookmarks["tableMarker"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["tableMarker"];
                                mark.Text = modelCop.TableMaker == null ? "" : modelCop.TableMaker.Trim();
                            }
                            //report.InsertValue("JingBan", modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim());
                            if (doc.Range.Bookmarks["JingBan"] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks["JingBan"];
                                mark.Text = modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim();
                            }
                            doc.Save("《合同评审表》.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                        }
                    }
                    else
                    {
                        if (doc.Range.Bookmarks["BuildUnit"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                            mark.Text = modelCop.BuildUnit == null ? "" : modelCop.BuildUnit.Trim();
                        }
                        // report.InsertValue("cpr_Id", modelCop.cpr_Id.ToString().Trim());
                        if (doc.Range.Bookmarks["cpr_Id"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["cpr_Id"];
                            mark.Text = modelCop.cpr_Id.ToString().Trim();
                        }
                        // report.InsertValue("cpr_Name", modelCop.cpr_Name.Trim());
                        if (doc.Range.Bookmarks["cpr_Name"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                            mark.Text = modelCop.cpr_Name == null ? "" : modelCop.cpr_Name.Trim();
                        }
                        // report.InsertValue("cpr_No", modelCop.cpr_No.Trim());
                        if (doc.Range.Bookmarks["cpr_No"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                            mark.Text = modelCop.cpr_No == null ? "" : modelCop.cpr_No.Trim();
                        }
                        // report.InsertValue("cpr_Unit", modelCop.cpr_Unit.Trim());
                        if (doc.Range.Bookmarks["cpr_Unit"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                            mark.Text = modelCop.cpr_Unit == null ? "" : modelCop.cpr_Unit.Trim();
                        }
                        //report.InsertValue("DataTime", modelAudit.InDate.ToString("yyyy-MM-dd"));
                        if (doc.Range.Bookmarks["DataTime"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["DataTime"];
                            mark.Text = modelAudit.InDate.ToString("yyyy-MM-dd");
                        }
                        // report.InsertValue("fourTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd"));

                        //report.InsertValue("fourUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[4]));

                        // report.InsertValue("fristTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                        if (doc.Range.Bookmarks["fristTime"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["fristTime"];
                            mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                        }
                        // report.InsertValue("fristUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[1]));
                        if (doc.Range.Bookmarks["fristUnit"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["fristUnit"];
                            mark.Text = RoleToCompany(1);
                        }
                        // report.InsertValue("leaderAdvice", modelAudit.GeneralManagerProposal);
                        if (doc.Range.Bookmarks["leaderAdvice"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["leaderAdvice"];
                            mark.Text = modelAudit.GeneralManagerProposal == null ? "" : modelAudit.GeneralManagerProposal;
                        }
                        // report.InsertValue("leaderTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[4]).ToString("yyyy-MM-dd"));

                        //report.InsertValue("level", level == "0" ? "一级" : "二级");
                        if (doc.Range.Bookmarks["level"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["level"];
                            mark.Text = level == "0" ? "一级" : "二级";
                        }
                        //report.InsertValue("productAdvice", modelAudit.OperateDepartmentProposal.Replace('|', ' '));
                        if (doc.Range.Bookmarks["productAdvice"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["productAdvice"];
                            mark.Text = modelAudit.OperateDepartmentProposal.Replace('|', ' ');
                        }
                        //report.InsertValue("productTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                        if (doc.Range.Bookmarks["productTime"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["productTime"];
                            mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                        }
                        //   report.InsertValue("secondTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd"));
                        if (doc.Range.Bookmarks["secondTime"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["secondTime"];
                            mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd");
                        }
                        // report.InsertValue("secondUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[2]));
                        if (doc.Range.Bookmarks["secondUnit"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["secondUnit"];
                            mark.Text = RoleToCompany(2);
                        }
                        //report.InsertValue("thirdTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd"));
                        if (doc.Range.Bookmarks["thirdTime"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["thirdTime"];
                            mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd");
                        }
                        // report.InsertValue("thirdUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[3]));
                        if (doc.Range.Bookmarks["thirdUnit"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["thirdUnit"];
                            mark.Text = RoleToCompany(4);
                        }
                        //report.InsertValue("tableMarker", modelCop.TableMaker.Trim());
                        if (doc.Range.Bookmarks["tableMarker"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["tableMarker"];
                            mark.Text = modelCop.TableMaker == null ? "" : modelCop.TableMaker.Trim();
                        }
                        //report.InsertValue("JingBan", modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim());
                        if (doc.Range.Bookmarks["JingBan"] != null)
                        {
                            Bookmark mark = doc.Range.Bookmarks["JingBan"];
                            mark.Text = modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim();
                        }
                        doc.Save("《合同评审表》.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                    }
                }

            }
            #endregion
            //项目审批
            #region 项目审批
            else if (option == "1")
            {

                string path2 = Server.MapPath(@"~\TemplateWord\QR-7.1-04.doc");
                Aspose.Words.Document doc = new Aspose.Words.Document(path2); //载入模板
                //BuildUnit cpr_Id cpr_Name cpr_No cpr_Unit DataTime fourTime fourUnit fristTime fristUnit leaderAdvice leaderTime level productAdvice productTime secondTime secondUnit thirdTime thirdUnit

                if (TG.BLL.tg_ProjectAuditBP.GetProjectAuditEntity(int.Parse(copid)) != null)
                {
                    //得到工程号
                    string sqlByNum = @"
SELECT n.ProNumber FROM cm_Project c LEFT JOIN 
cm_ProjectNumber n ON c.pro_ID=n.Pro_id WHERE c.pro_ID=" + copid;
                    object num = TG.DBUtility.DbHelperSQL.GetSingle(sqlByNum);
                    TG.Model.cm_CoperationAuditListView modelCop = new Model.cm_CoperationAuditListView();
                    foreach (TG.Model.cm_CoperationAuditListView item in listCop)
                    {
                        if (item.AuditStatus == "J")
                        {
                            modelCop = item;
                        }
                    }
                    string subname = "";
                    foreach (TG.Model.cm_SubCoperation item in listSubCop)
                    {
                        subname += item.Item_Name;
                    }
                    TG.Model.ProjectAuditDataEntity Modeldata = TG.BLL.tg_ProjectAuditBP.GetProjectAuditEntity(int.Parse(copid));
                    TG.Model.cm_Project ProjectObj = cm_proBll.GetModel(int.Parse(copid));
                    //  2013-06-09 10:15:21,2013-06-09 10:16:34,2013-06-09 10:17:07,2013-06-09 10:17:33,2013-06-09 10:18:22
                    //KanChamoney  levle GaiYuSuanmoney cpr_Unit cpr_No cpr_Name cpr_childName buildArea
                    //report.InsertValue("KanChamoney", modelCop.cpr_ShijiAcount.ToString().Trim());
                    if (doc.Range.Bookmarks["KanChamoney"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["KanChamoney"];
                        mark.Text = "";
                    }
                    //report.InsertValue("levle", level == "0" ? "一级" : "二级");
                    if (doc.Range.Bookmarks["levle"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["levle"];
                        mark.Text = level == "0" ? "院管" : "所管";
                    }
                    //report.InsertValue("GaiYuSuanmoney", modelCop.cpr_Acount.ToString().Trim());
                    if (doc.Range.Bookmarks["GaiYuSuanmoney"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["GaiYuSuanmoney"];
                        mark.Text = "";
                    }
                    /////
                    //if (doc.Range.Bookmarks["SCJYSign"] != null)
                    //{
                    //    Bookmark mark = doc.Range.Bookmarks["SCJYSign"];
                    //    mark.Text = Modeldata.AuditUser.Split(',')[1];
                    //}
                    if (doc.Range.Bookmarks["year"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["year"];
                        mark.Text = Convert.ToDateTime(Modeldata.InDate).ToString("yyyy-MM-dd").Split('-')[0];
                    }
                    if (doc.Range.Bookmarks["month"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["month"];
                        mark.Text = Convert.ToDateTime(Modeldata.InDate).ToString("yyyy-MM-dd").Split('-')[1];
                    }
                    if (doc.Range.Bookmarks["day"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["day"];
                        mark.Text = Convert.ToDateTime(Modeldata.InDate).ToString("yyyy-MM-dd").Split('-')[2];
                    }
                    /////////////
                    //report.InsertValue("cpr_Unit", modelCop.cpr_Unit.ToString().Trim());
                    if (doc.Range.Bookmarks["cpr_Unit"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                        mark.Text = ProjectObj.Unit == null ? "" : ProjectObj.Unit.ToString().Trim();
                    }
                    if (doc.Range.Bookmarks["KanChamoney"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["KanChamoney"];
                        mark.Text = ProjectObj.Cpr_Acount == null ? "" : ProjectObj.Cpr_Acount.ToString().Trim();
                    }
                    if (doc.Range.Bookmarks["shengchanjihua"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["shengchanjihua"];
                        mark.Text = ProjectObj.Unit == null ? "" : ProjectObj.Unit.ToString().Trim();
                    }
                    //report.InsertValue("cpr_No", modelCop.cpr_No.Trim());
                    if (doc.Range.Bookmarks["cpr_No"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                        mark.Text = num == null ? "" : num.ToString().Trim();
                    }
                    //report.InsertValue("cpr_Name", modelCop.cpr_Name.ToString().Trim());
                    if (doc.Range.Bookmarks["cpr_Name"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                        mark.Text = ProjectObj.pro_name == null ? "" : ProjectObj.pro_name.ToString().Trim();
                    }
                    //report.InsertValue("cpr_childName", subname);
                    if (doc.Range.Bookmarks["cpr_childName"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["cpr_childName"];
                        mark.Text = "";
                    }

                    //report.InsertValue("buildArea", modelCop.BuildArea.ToString().Trim());
                    if (doc.Range.Bookmarks["buildArea"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["buildArea"];
                        mark.Text = ProjectObj.ProjectScale == null ? "" : ProjectObj.ProjectScale.ToString().Trim();
                    }
                    doc.Save("勘察设计任务通知单.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                    //string wordPath = context.Server.MapPath(@"~\TemplateWord\repordWord1.doc");
                    ////下载
                    //if (System.IO.File.Exists(wordPath))
                    //{
                    //    report.SaveDocument(wordPath);
                    //    context.Response.ContentType = "application/x-zip-compressed";
                    //    context.Response.AddHeader("Content-Disposition", "attachment;filename=勘察任务通知单 .doc");
                    //    context.Response.TransmitFile(wordPath);
                    //}
                }
            }
            #endregion
            ///项目进岗人员审批单
            #region 工程项目进岗人员审批表
            else if (option == "3")
            {
                //先判断有没有所对应的的tcd的存的值。
                string status = Request.QueryString["status"];

                string path3 = Server.MapPath(@"~\TemplateWord\QR-7.2-01.doc");
                Aspose.Words.Document doc = new Aspose.Words.Document(path3); //载入模板
                //书签列表  copid  项目id   level 级别   auditno 审核id
                //得到项目的信息
                TG.Model.cm_Project ProjectModel = cm_proBll.GetModel(int.Parse(copid));
                TG.Model.cm_Coperation CoperModel = copbll.GetModel(ProjectModel.CoperationSysNo);
                //进行判断所对应的的表单id和关键的东西
                int index = -1;
                string[] arrayproject = ProjectModel.pro_status.Split(',');
                for (int i = 0; i < arrayproject.Length; i++)
                {
                    if (arrayproject[i].Trim() != "")
                    {
                        index++;
                        if (status == arrayproject[i])
                        {
                            break;
                        }

                    }
                }
                string biaoshi = "0::" + index;
                string sqlProbii = "SELECT bill_title FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
                object Probii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii);
                //得到工程号
                string sqlByNum = @"
SELECT n.ProNumber FROM cm_Project c LEFT JOIN 
cm_ProjectNumber n ON c.pro_ID=n.Pro_id WHERE c.pro_ID=" + copid;
                object num = TG.DBUtility.DbHelperSQL.GetSingle(sqlByNum);
                TG.BLL.cm_ProjectPlanBP proplan = new TG.BLL.cm_ProjectPlanBP();
                //得到参与人员的信息
                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
                List<TG.Model.ProjectPlanRole> ProjectPlanRoleList = projectPlanBP.GetProjectPlanRoleAndUsers(int.Parse(copid));
                //设计人员
                TG.Model.ProjectDesignPlanRole ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(int.Parse(copid));
                //审核信息
                TG.Model.cm_ProjectPlanAuditEntity projectPlanAudit = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditEntity(new TG.Model.cm_ProjectPlanAuditQueryEntity { ProjectPlanAuditSysNo = int.Parse(auditno) });
                // RepeaterAllUsers.DataSource = new TG.BLL.cm_ProjectPlanBP().GetPlanUsers(ProjectModel.ReferenceSysNo);
                //绑定书签 
                //项目名称
                #region tcd没有进岗信息
                if (Probii == null)
                {
                    if (doc.Range.Bookmarks["BM_3002"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3002"];
                        mark.Text = ProjectModel.pro_name == null ? "" : ProjectModel.pro_name.ToString().Trim();
                    }
                    //工程号
                    if (doc.Range.Bookmarks["BM_3003"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3003"];
                        mark.Text = num.ToString().Trim();
                    }
                    //子项名称
                    if (doc.Range.Bookmarks["BM_3004"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3004"];
                        mark.Text = "";
                    }
                    //管理级别
                    if (doc.Range.Bookmarks["BM_3153"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3153"];
                        mark.Text = level == "0" ? "院管" : "所管";
                    }
                    //功能
                    if (doc.Range.Bookmarks["BM_3006"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3006"];
                        mark.Text = "";
                    }

                    //其他
                    if (doc.Range.Bookmarks["BM_3010"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3010"];
                        mark.Text = "";
                    }
                    //面积
                    if (doc.Range.Bookmarks["BM_3007"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3007"];
                        mark.Text = ProjectModel.ProjectScale == null ? "" : ProjectModel.ProjectScale.ToString().Trim();
                    }
                    //层数
                    if (doc.Range.Bookmarks["BM_3008"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3008"];
                        string floor = CoperModel.Floor.Split('|')[0] == null ? "" : "地上：" + CoperModel.Floor.Split('|')[0].Trim() + "";
                        floor += CoperModel.Floor.Split('|')[1] == null ? "" : "地下：" + CoperModel.Floor.Split('|')[1].Trim();
                        mark.Text = floor;
                    }
                    //结构类型
                    if (doc.Range.Bookmarks["BM_3009"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3009"];
                        mark.Text = ProjectModel.pro_StruType == null ? "" : ProjectModel.pro_StruType.ToString().Trim();
                    }
                    //设计总负责
                    if (doc.Range.Bookmarks["BM_3011"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3011"];
                        string usestring = "";
                        ProjectPlanRoleList[0].Users.ForEach((user) =>
                        {
                            usestring += user.UserName + " ";
                        });
                        mark.Text = usestring;
                    }
                    //设计阶段
                    if (doc.Range.Bookmarks["BM_3154"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3154"];

                        mark.Text = status;
                    }
                    #region 各专业的人
                    //建筑专业负责人
                    if (doc.Range.Bookmarks["BM_3013"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3013"];
                        string usestring = "";
                        ProjectPlanRoleList[1].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "建筑")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //结构专业负责人
                    if (doc.Range.Bookmarks["BM_3014"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3014"];
                        string usestring = "";
                        ProjectPlanRoleList[1].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "结构")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //给排水专业负责人
                    if (doc.Range.Bookmarks["BM_3015"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3015"];
                        string usestring = "";
                        ProjectPlanRoleList[1].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "给排水")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //暖通专业负责人
                    if (doc.Range.Bookmarks["BM_3016"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3016"];
                        string usestring = "";
                        ProjectPlanRoleList[1].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "暖通")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //电气专业负责人
                    if (doc.Range.Bookmarks["BM_3017"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3017"];
                        string usestring = "";
                        ProjectPlanRoleList[1].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "电气")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //概预算专业负责人
                    if (doc.Range.Bookmarks["BM_3018"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3018"];
                        string usestring = "";
                        ProjectPlanRoleList[1].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "预算")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }

                    //建筑专业设计人
                    if (doc.Range.Bookmarks["BM_3019"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3019"];
                        string usestring = "";
                        ProjectDesignPlanRoleList.Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "建筑")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //结构专业设计人
                    if (doc.Range.Bookmarks["BM_3020"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3020"];
                        string usestring = "";
                        ProjectDesignPlanRoleList.Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "结构")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //给排水专业设计人
                    if (doc.Range.Bookmarks["BM_3021"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3021"];
                        string usestring = "";
                        ProjectDesignPlanRoleList.Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "给排水")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //暖通专业设计人
                    if (doc.Range.Bookmarks["BM_3022"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3022"];
                        string usestring = "";
                        ProjectDesignPlanRoleList.Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "暖通")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //电气专业设计人
                    if (doc.Range.Bookmarks["BM_3023"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3023"];
                        string usestring = "";
                        ProjectDesignPlanRoleList.Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "电气")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //概预算专业设计人
                    if (doc.Range.Bookmarks["BM_3024"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3024"];
                        string usestring = "";
                        ProjectDesignPlanRoleList.Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "预算")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }

                    //建筑专业校对人
                    if (doc.Range.Bookmarks["BM_3025"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3025"];
                        string usestring = "";
                        ProjectPlanRoleList[2].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "建筑")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //结构专业校对人
                    if (doc.Range.Bookmarks["BM_3026"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3026"];
                        string usestring = "";
                        ProjectPlanRoleList[2].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "结构")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //给排水专业校对人
                    if (doc.Range.Bookmarks["BM_3027"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3027"];
                        string usestring = "";
                        ProjectPlanRoleList[2].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "给排水")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //暖通专业校对人
                    if (doc.Range.Bookmarks["BM_3028"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3028"];
                        string usestring = "";
                        ProjectPlanRoleList[2].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "暖通")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //电气专业校对人
                    if (doc.Range.Bookmarks["BM_3029"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3029"];
                        string usestring = "";
                        ProjectPlanRoleList[2].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "电气")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    //概预算专业校对人
                    if (doc.Range.Bookmarks["BM_3030"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3030"];
                        string usestring = "";
                        ProjectPlanRoleList[2].Users.ForEach((user) =>
                        {
                            if (user.SpecialtyName == "预算")
                            {
                                usestring += user.UserName + " ";
                            }
                        });
                        mark.Text = usestring;
                    }
                    #endregion
                    #region 院审批一部分
                    //院审批意见
                    if (doc.Range.Bookmarks["BM_3158"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3158"];
                        string usestring = "";
                        try
                        {
                            usestring = projectPlanAudit.Suggestion.Split('|')[2];
                        }
                        catch (System.Exception ex)
                        {
                            usestring = "";
                        }


                        mark.Text = usestring;
                    }
                    //原审批的年
                    if (doc.Range.Bookmarks["BM_3157"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3157"];
                        string usestring = "";
                        try
                        {
                            usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd").Split('-')[0];
                        }
                        catch (System.Exception ex)
                        {
                            usestring = "";
                        }
                        mark.Text = usestring;
                    }
                    //原审批的月
                    if (doc.Range.Bookmarks["BM_3156"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3156"];
                        string usestring = "";
                        try
                        {
                            usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd").Split('-')[1];
                        }
                        catch (System.Exception ex)
                        {
                            usestring = "";
                        }
                        mark.Text = usestring;
                    }
                    //原审批的日
                    if (doc.Range.Bookmarks["BM_3155"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3155"];
                        string usestring = "";
                        try
                        {
                            usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd").Split('-')[2];
                        }
                        catch (System.Exception ex)
                        {
                            usestring = "";
                        }
                        mark.Text = usestring;
                    }
                    #endregion

                    //经济所审批人签字&  时间
                    if (doc.Range.Bookmarks["BM_3037"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3037"];
                        string usestring = "";
                        try
                        {
                            usestring = GetRoleName(projectPlanAudit.AuditUser.Split(',')[1]); //projectPlanAudit.Suggestion.Split('|')[2];
                        }
                        catch (System.Exception ex)
                        {
                            usestring = "";
                        }
                        mark.Text = usestring;
                    }
                    if (doc.Range.Bookmarks["BM_3038"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3038"];
                        string usestring = "";
                        try
                        {
                            usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd"); //projectPlanAudit.Suggestion.Split('|')[2];
                        }
                        catch (System.Exception ex)
                        {
                            usestring = "";
                        }
                        mark.Text = usestring;
                    }
                    doc.Save("工程项目进岗人员审批表.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                }
                #endregion
                #region tcd有进岗信息
                else
                {
                    string sqlProbii2 = "SELECT item_val FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
                    object ProObjBii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii2);
                    if (ProObjBii != null)
                    {
                        string[] arrayprobii = ProObjBii.ToString().Split('\n');

                        for (int i = 0; i < arrayprobii.Length; i++)
                        {
                            for (int numb = 3002; numb < 3100; numb++)
                            {
                                if (arrayprobii[i].Split('=')[0] == numb.ToString())
                                {
                                    //就给对应的书签赋值
                                    if (doc.Range.Bookmarks["BM_" + numb] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_" + numb];
                                        mark.Text = arrayprobii[i].Split('=')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3154")
                                {
                                    if (doc.Range.Bookmarks["BM_3154"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3154"];
                                        mark.Text = arrayprobii[i].Split('=')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3032")
                                {
                                    if (doc.Range.Bookmarks["BM_3155"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3155"];
                                        mark.Text = Convert.ToDateTime(arrayprobii[i].Split('=')[1]).ToString("yyyy-MM-dd").Split('-')[2];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3032")
                                {
                                    if (doc.Range.Bookmarks["BM_3156"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3156"];
                                        mark.Text = Convert.ToDateTime(arrayprobii[i].Split('=')[1]).ToString("yyyy-MM-dd").Split('-')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3032")
                                {
                                    if (doc.Range.Bookmarks["BM_3157"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3157"];
                                        mark.Text = Convert.ToDateTime(arrayprobii[i].Split('=')[1]).ToString("yyyy-MM-dd").Split('-')[0];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3031")
                                {
                                    if (doc.Range.Bookmarks["BM_3158"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3158"];
                                        mark.Text = arrayprobii[i].Split('=')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3031")
                                {
                                    if (doc.Range.Bookmarks["BM_3152"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3152"];
                                        mark.Text = arrayprobii[i].Split('=')[1] == "0" ? "院管" : "所管";
                                    }
                                }
                            }

                        }
                    }
                    doc.Save("工程项目进岗人员审批表.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                }
                #endregion
            }
            #endregion
            ///项目进度计划表
            #region
            else if (option == "4")
            {
                string status = Request.QueryString["status"];
                string path4 = Server.MapPath(@"~\TemplateWord\QR-7.2-02.doc");
                Aspose.Words.Document doc = new Aspose.Words.Document(path4); //载入模板
                //书签列表  copid  项目id   level 级别   auditno 审核id
                //得到项目的信息
                TG.Model.cm_Project ProjectModel = cm_proBll.GetModel(int.Parse(copid));
                TG.Model.cm_Coperation CoperModel = copbll.GetModel(ProjectModel.CoperationSysNo);
                //得到工程号
                string sqlByNum = @"
SELECT n.ProNumber FROM cm_Project c LEFT JOIN 
cm_ProjectNumber n ON c.pro_ID=n.Pro_id WHERE c.pro_ID=" + copid;
                object num = TG.DBUtility.DbHelperSQL.GetSingle(sqlByNum);
                TG.BLL.cm_ProjectPlanBP proplan = new TG.BLL.cm_ProjectPlanBP();
                //得到参与人员的信息
                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
                List<TG.Model.ProjectPlanRole> ProjectPlanRoleList = projectPlanBP.GetProjectPlanRoleAndUsers(int.Parse(copid));
                //设计人员
                TG.Model.ProjectDesignPlanRole ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(int.Parse(copid));
                //审核信息
                TG.Model.cm_ProjectPlanAuditEntity projectPlanAudit = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditEntity(new TG.Model.cm_ProjectPlanAuditQueryEntity { ProjectPlanAuditSysNo = int.Parse(auditno) });
                int index = -1;
                string[] arrayproject = ProjectModel.pro_status.Split(',');
                for (int i = 0; i < arrayproject.Length; i++)
                {
                    if (arrayproject[i].Trim() != "")
                    {
                        index++;
                        if (status == arrayproject[i])
                        {
                            break;
                        }

                    }
                }
                string biaoshi = "1::" + index;
                string sqlProbii = "SELECT bill_title FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
                object Probii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii);
                #region tcd没有进度
                if (Probii == null)
                {
                    //项目名称
                    if (doc.Range.Bookmarks["BM_3002"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3002"];
                        mark.Text = ProjectModel.pro_name == null ? "" : ProjectModel.pro_name.ToString().Trim();
                    }
                    //设计阶段
                    if (doc.Range.Bookmarks["BM_3154"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3154"];

                        mark.Text = ProjectModel.pro_status == null ? "" : ProjectModel.pro_status.ToString().Trim().Replace(',', ' ');
                    }
                    //管理级别
                    if (doc.Range.Bookmarks["BM_3153"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3153"];
                        mark.Text = level == "0" ? "院管" : "所管";
                    }
                    //建筑
                    if (doc.Range.Bookmarks["BM_3100"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-建筑") > 0)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-建筑", "1");
                        }
                        Bookmark mark = doc.Range.Bookmarks["BM_3100"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3102"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-建筑", "2");
                        Bookmark mark = doc.Range.Bookmarks["BM_3102"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3104"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-建筑") > 2)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-建筑", "3");
                        }
                        Bookmark mark = doc.Range.Bookmarks["BM_3104"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //结构
                    if (doc.Range.Bookmarks["BM_3107"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-结构") > 0)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-结构", "1");
                        }

                        Bookmark mark = doc.Range.Bookmarks["BM_3107"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3109"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-结构", "2");
                        Bookmark mark = doc.Range.Bookmarks["BM_3109"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3111"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-结构") > 2)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-结构", "3");
                        }
                        Bookmark mark = doc.Range.Bookmarks["BM_3111"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //给排水141618
                    if (doc.Range.Bookmarks["BM_3114"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-给排水") > 0)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-给排水", "1");
                        }
                        Bookmark mark = doc.Range.Bookmarks["BM_3114"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3116"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-给排水", "2");
                        Bookmark mark = doc.Range.Bookmarks["BM_3116"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3118"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-给排水") > 2)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-给排水", "3");
                        }
                        Bookmark mark = doc.Range.Bookmarks["BM_3118"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //电气283032
                    if (doc.Range.Bookmarks["BM_3128"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-电气") > 0)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-电气", "1");
                        }
                        Bookmark mark = doc.Range.Bookmarks["BM_3128"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3130"] != null)
                    {

                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-电气", "2");
                        Bookmark mark = doc.Range.Bookmarks["BM_3130"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3132"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-电气") > 2)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-电气", "3");
                        }

                        Bookmark mark = doc.Range.Bookmarks["BM_3132"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //暖通212325
                    if (doc.Range.Bookmarks["BM_3121"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-暖通") > 0)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-暖通", "1");
                        }
                        Bookmark mark = doc.Range.Bookmarks["BM_3121"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3123"] != null)
                    {

                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-暖通", "2");
                        Bookmark mark = doc.Range.Bookmarks["BM_3123"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3125"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-暖通") > 2)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-暖通", "3");
                        }
                        // object itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-暖通", "3");
                        Bookmark mark = doc.Range.Bookmarks["BM_3125"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //概算3537
                    if (doc.Range.Bookmarks["BM_3135"] != null)
                    {
                        object itemsub = null;
                        if (GetCount(ProjectModel.ReferenceSysNo, "提资-概算") > 0)
                        {
                            itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-概算", "1");
                        }

                        Bookmark mark = doc.Range.Bookmarks["BM_3135"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    if (doc.Range.Bookmarks["BM_3137"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "提资-概算", "2");
                        Bookmark mark = doc.Range.Bookmarks["BM_3137"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //评审时间40
                    if (doc.Range.Bookmarks["BM_3140"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3140"];
                        mark.Text = projectPlanAudit.InDate == null ? "" : projectPlanAudit.InDate.ToString("yyyy-MM-dd");
                    }
                    //校对时间41
                    if (doc.Range.Bookmarks["BM_3141"] != null)
                    {

                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "校对", "1");
                        Bookmark mark = doc.Range.Bookmarks["BM_3141"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //会签时间42
                    if (doc.Range.Bookmarks["BM_3142"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "会签", "1");
                        Bookmark mark = doc.Range.Bookmarks["BM_3142"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //审核时间43
                    if (doc.Range.Bookmarks["BM_3143"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "审核", "1");
                        Bookmark mark = doc.Range.Bookmarks["BM_3143"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //审定时间44
                    if (doc.Range.Bookmarks["BM_3144"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "审定", "1");
                        Bookmark mark = doc.Range.Bookmarks["BM_3144"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //出图时间45
                    if (doc.Range.Bookmarks["BM_3145"] != null)
                    {
                        object itemsub = GetItem(ProjectModel.ReferenceSysNo, "盖章", "1");
                        Bookmark mark = doc.Range.Bookmarks["BM_3145"];
                        mark.Text = itemsub == null ? "" : Convert.ToDateTime(itemsub).ToString("yyyy-MM-dd");
                    }
                    //设总3011
                    if (doc.Range.Bookmarks["BM_3011"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3011"];
                        string usestring = "";
                        ProjectPlanRoleList[0].Users.ForEach((user) =>
                        {
                            usestring += user.UserName + " ";
                        });
                        mark.Text = usestring;
                    }
                    //生产经营部3146
                    string[] array = projectPlanAudit.AuditUser.Split(',');
                    if (doc.Range.Bookmarks["BM_3146"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3146"];
                        if (array.Length > 1)
                        {
                            mark.Text = projectPlanAudit.AuditUser == null ? "" : GetUser(projectPlanAudit.AuditUser.Split(',')[0]);
                        }
                        else
                        {
                            mark.Text = "";
                        }

                    }
                    //技术质量部3099
                    if (doc.Range.Bookmarks["BM_3099"] != null)
                    {
                        Bookmark mark = doc.Range.Bookmarks["BM_3099"];
                        if (array.Length > 2)
                        {
                            mark.Text = projectPlanAudit.AuditUser == null ? "" : GetUser(projectPlanAudit.AuditUser.Split(',')[1]);
                        }
                        else
                        {
                            mark.Text = "";
                        }
                    }
                    doc.Save("项目进度计划表.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                }
                #endregion
                #region tcd有进度
                else
                {
                    string sqlProbii2 = "SELECT item_val FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
                    string ProObjBii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii2).ToString();
                    if (ProObjBii != null)
                    {
                        string[] arrayprobii = ProObjBii.Split('\n');

                        for (int i = 0; i < arrayprobii.Length; i++)
                        {
                            for (int numb = 3099; numb < 3150; numb++)
                            {
                                if (arrayprobii[i].Split('=')[0] == numb.ToString())
                                {
                                    //就给对应的书签赋值
                                    if (doc.Range.Bookmarks["BM_" + numb] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_" + numb];
                                        mark.Text = arrayprobii[i].Split('=')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3002")
                                {
                                    if (doc.Range.Bookmarks["BM_3002"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3002"];
                                        mark.Text = arrayprobii[i].Split('=')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3011")
                                {
                                    if (doc.Range.Bookmarks["BM_3011"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3011"];
                                        mark.Text = arrayprobii[i].Split('=')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3154")
                                {
                                    if (doc.Range.Bookmarks["BM_3154"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3154"];
                                        mark.Text = arrayprobii[i].Split('=')[1];
                                    }
                                }
                                if (arrayprobii[i].Split('=')[0] == "3153")
                                {
                                    if (doc.Range.Bookmarks["BM_3152"] != null)
                                    {
                                        Bookmark mark = doc.Range.Bookmarks["BM_3153"];
                                        mark.Text = arrayprobii[i].Split('=')[1] == "0" ? "院管" : "所管";
                                    }
                                }
                            }

                        }
                    }
                    doc.Save("项目进度计划表.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
                }
                #endregion
            }
            #endregion
            //资料卡
            else if (option == "5")
            {
                string GKZiLiaoPath = Server.MapPath(@"~\TemplateWord\QR-7.7-01.doc");
                Aspose.Words.Document doc = new Aspose.Words.Document(GKZiLiaoPath); //载入模板
                ToWordByMenZL(copid.ToString(), GKZiLiaoPath, out doc);
                doc.Save("顾客提供资料记录卡.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
            }
        }
        public string GetUser(string memId)
        {
            string sqlMem = @"SELECT mem_Name FROM tg_member WHERE mem_ID=" + memId;
            return TG.DBUtility.DbHelperSQL.GetSingle(sqlMem).ToString();
        }
        public int GetCount(int proid, string ZHYEname)
        {
            int itemcount = 0;
            string sqlitemcount = @"SELECT count(*) FROM cm_ProjectPlanSubItem WHERE ProjectSysNo=" + proid + " AND DesignLevel='" + ZHYEname + "'";
            object obj = TG.DBUtility.DbHelperSQL.GetSingle(sqlitemcount);
            if (obj != null)
            {
                itemcount = int.Parse(obj.ToString());
            }
            return itemcount;
        }
        public object GetItem(int proid, string ZHYEname, string flag)
        {
            object ItemDate = null;
            if (flag == "1")
            {
                string sqlItem1 = @"SELECT TOP 1 StartDate FROM cm_ProjectPlanSubItem WHERE ProjectSysNo=" + proid + " AND DesignLevel='" + ZHYEname + "'";
                ItemDate = TG.DBUtility.DbHelperSQL.GetSingle(sqlItem1);
            }
            if (flag == "2")
            {
                string sqlItem1 = @"select top 1 StartDate from cm_ProjectPlanSubItem WHERE ProjectSysNo=" + proid + " AND DesignLevel='" + ZHYEname + "' and  SysNo > (select min(SysNo) from cm_ProjectPlanSubItem WHERE ProjectSysNo=" + proid + " AND DesignLevel='" + ZHYEname + "')";
                ItemDate = TG.DBUtility.DbHelperSQL.GetSingle(sqlItem1);
            }
            if (flag == "3")
            {
                string sqlItem1 = @"select top 1 StartDate from cm_ProjectPlanSubItem WHERE ProjectSysNo=" + proid + " AND DesignLevel='" + ZHYEname + "' and  SysNo in (select max(SysNo) from cm_ProjectPlanSubItem WHERE ProjectSysNo=" + proid + " AND DesignLevel='" + ZHYEname + "')";
                ItemDate = TG.DBUtility.DbHelperSQL.GetSingle(sqlItem1);
            }
            return ItemDate;

        }
        public void ToWordByMenZL(string proid, string path, out Aspose.Words.Document doc)
        {
            TG.BLL.cm_Project cm_proBll = new TG.BLL.cm_Project();
            doc = new Aspose.Words.Document(path); //载入模板
            //得到项目的信息
            TG.Model.cm_Project ProjectModel = cm_proBll.GetModel(int.Parse(proid));
            //标示
            string biaoshi = "2";
            string sqlProbii2 = "SELECT item_val FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
            object ProObjBii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii2);
            if (doc.Range.Bookmarks["BM_proName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BM_proName"];
                mark.Text = ProjectModel.pro_name == null ? "" : ProjectModel.pro_name.Trim();
            }
            if (ProObjBii != null)
            {
                string arrayprobii = ProObjBii.ToString().Split(new char[] { '=' }, 3)[2];
                string[] arrayprobiichild = arrayprobii.Split('\r');
                for (int i = 0; i < arrayprobiichild.Length; i++)
                {
                    string[] arrchild = arrayprobiichild[i].Split('\n');
                    for (int j = 2; j < arrchild.Length; j++)
                    {

                        //BM_3149_0_2
                        string e = "BM_3149_" + i + "_" + j;
                        if (i == 0)
                        {
                            if (doc.Range.Bookmarks[e] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks[e];
                                mark.Text = arrchild[j];
                            }
                        }
                        else
                        {
                            if (doc.Range.Bookmarks[e] != null)
                            {
                                Bookmark mark = doc.Range.Bookmarks[e];
                                mark.Text = arrchild[j + 1];
                            }
                        }


                    }
                }
            }
        }
        public DateTime Gettime(string strtime)
        {
            return Convert.ToDateTime(strtime);
        }

        //根据id得到人员名册
        public string GetRoleName(string roleid)
        {
            string rolesql = @"SELECT mem_Name FROM tg_member WHERE mem_ID=" + roleid;
            return TG.DBUtility.DbHelperSQL.GetSingle(rolesql).ToString();
        }
        //根据用户编号，判断所在部门
        public string RoleToCompany(string str)
        {
            string rolename = "";
            TG.BLL.cm_Role bllrole = new BLL.cm_Role();
            List<TG.Model.cm_Role> listrole = bllrole.GetRoleList();
            foreach (TG.Model.cm_Role item in listrole)
            {
                string[] strArray = item.Users.Split(',');
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (strArray[i] == str)
                    {
                        rolename = item.RoleName;
                        break;
                    }
                }
                if (rolename != "")
                {
                    break;
                }

            }
            return rolename;
        }


        public string RoleToCompany(int position)
        {
            string sql = @"SELECT c.RoleName FROM cm_Role c LEFT JOIN cm_CoperationAuditConfig a ON c.SysNo=a.RoleSysNo WHERE
a.Position=" + position;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return o.ToString();
        }
        protected string GetProfession(string num)
        {
            string result = "";
            switch (num)
            {
                case "-1":
                    result = "";
                    break;
                case "27":
                    result = "方案";
                    break;
                case "28":
                    result = "初设";
                    break;
                case "29":
                    result = "施工图";
                    break;
                case "30":
                    result = "其他";
                    break;
                case "31":
                    result = "公开招标";
                    break;
                case "32":
                    result = "邀请招标";
                    break;
                case "36":
                    result = "计算机行业";
                    break;
                case "33":
                    result = "自行委托";
                    break;
                case "37":
                    result = "教育行业";
                    break;
                case "38":
                    result = "建筑行业";
                    break;
                case "47":
                    result = "科教行业";
                    break;
                case "34":
                    result = "普通客户";
                    break;
                case "35":
                    result = "VIP客户";
                    break;
                case "39":
                    result = "一般";
                    break;
                case "40":
                    result = "密切";
                    break;
                case "41":
                    result = "很密切";
                    break;
                case "43":
                    result = "一级";
                    break;
                case "44":
                    result = "二级";
                    break;
                case "45":
                    result = "三级";
                    break;
                case "46":
                    result = "四级";
                    break;
                default:
                    result = "";
                    break;
            }
            return result;
        }

        //public void exword()
        //{
        //    // try
        //    //{
        //    string tmppath = Server.MapPath(@"~\Template\sc_template.doc");
        //    // string strSaveFileName = string.Format("合同审查审批表{0:yyyyMMddhhmmss}.doc", DateTime.Now);
        //    string strSaveFileName = "合同审查审批表.doc";
        //    string newpath = Server.MapPath(@"~\Template\" + strSaveFileName);
        //    object fileName = tmppath;
        //    object readOnly = false;
        //    object isVisible = true;

        //    Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
        //    doc = App.Documents.Open(ref fileName, ref missing, ref readOnly,
        //        ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
        //        ref missing, ref missing, ref isVisible, ref missing, ref missing, ref missing, ref missing);
        //    doc.Activate();

        //    ReplaceText("{BuildUnit}", CoperationAuditEntity.BuildUnit);
        //    ReplaceText("{Cpy_Address}", CustomerInfo.Cpy_Address);
        //    ReplaceText("{Linkma}", CustomerInfo.Linkman);
        //    ReplaceText("{Cpy_Phone}", CustomerInfo.Cpy_Phone);
        //    ReplaceText("{cpr_Name}", CoperationAuditEntity.cpr_Name);
        //    ReplaceText("{cpr_No}", CoperationAuditEntity.cpr_No);
        //    ReplaceText("{cpr_Acount}", CoperationAuditEntity.cpr_Acount.ToString());
        //    ReplaceText("{ChgPeople}", CoperationAuditEntity.ChgPeople);
        //    ReplaceText("{ChgPhone}", CoperationAuditEntity.ChgPhone);
        //    ReplaceText("{Cpy_Fax}", CustomerInfo.Cpy_Fax);
        //    //项目经理
        //    arr = coper.UndertakeProposal.Split('|');
        //    if (arr.Length > 0)
        //    {
        //        for (int i = 0; i < arr.Length; i++)
        //        {
        //            str = str + (i + 1) + "、";
        //            if (string.IsNullOrEmpty(arr[i]))
        //            {
        //                str = str + "通过";
        //            }
        //            else
        //            {
        //                str = str + "不通过。原因：" + arr[i];
        //            }
        //            str = str + (char)13;
        //        }
        //    }
        //    ReplaceText("{UndertakeProposal}", str);
        //    //经营部长
        //    arr = coper.OperateDepartmentProposal.Split('|');
        //    str = "";
        //    if (arr.Length > 0)
        //    {
        //        for (int i = 0; i < arr.Length; i++)
        //        {
        //            str = str + (i + 1) + "、";
        //            if (string.IsNullOrEmpty(arr[i]))
        //            {
        //                str = str + "通过";
        //            }
        //            else
        //            {
        //                str = str + "不通过。原因：" + arr[i];
        //            }
        //            str = str + (char)13;
        //        }
        //    }
        //    ReplaceText("{OperateDepartmentProposal}", str);
        //    //法律顾问
        //    ReplaceText("{LegalAdviserProposal}", coper.LegalAdviserProposal);
        //    //经营院长
        //    arr = coper.TechnologyDepartmentProposal.Split('|');
        //    str = "";
        //    if (arr.Length > 0)
        //    {
        //        for (int i = 0; i < arr.Length; i++)
        //        {
        //            str = str + (i + 1) + "、";
        //            if ((i + 1) == arr.Length)
        //            {
        //                if (arr[i] == "0")
        //                {
        //                    str = str + "一级";
        //                }
        //                else
        //                {
        //                    str = str + "二级";
        //                }
        //            }
        //            else
        //            {
        //                if (string.IsNullOrEmpty(arr[i]))
        //                {
        //                    str = str + "通过";
        //                }
        //                else
        //                {
        //                    str = str + "不通过。原因：" + arr[i];
        //                }

        //            }
        //            str = str + (char)13;
        //        }
        //    }
        //    ReplaceText("{TechnologyDepartmentProposal}", str);
        //    //院长               
        //    ReplaceText("{GeneralManagerProposal}", coper.GeneralManagerProposal);
        //    arr = coper.AuditDate.Split(',');
        //    str = "";
        //    if (arr.Length > 0)
        //    {
        //        for (int i = 0; i < arr.Length; i++)
        //        {
        //            ReplaceText("{AuditDate" + (i + 1) + "}", Convert.ToDateTime(arr[i]).ToString("yyyy年MM月dd日"));
        //        }
        //    }

        //    ReplaceText("{TableMaker}", CoperationAuditEntity.TableMaker);
        //    // if (System.IO.File.Exists(newpath))
        //    // {
        //    // fileName = newpath;
        //    object format = WdSaveFormat.wdFormatDocument;//保存格式
        //    // doc.SaveAs(ref fileName, ref format, ref missing, ref missing, ref missing, ref missing, ref missing,
        //    //  ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
        //    object SaveChanges = WdSaveOptions.wdSaveChanges;
        //    object OriginalFormat = WdOriginalFormat.wdOriginalDocumentFormat;
        //    object RouteDocument = false;
        //    doc.Close(ref SaveChanges, ref OriginalFormat, ref RouteDocument);
        //    App.Application.Quit(ref SaveChanges, ref OriginalFormat, ref RouteDocument);


        //    // Response.ContentType = "application/ms-word";
        //    //  Response.Charset = "gb2312";
        //    //  Response.AddHeader("Content-Disposition", "attachment;filename= " + HttpUtility.UrlEncode(CoperationAuditEntity.cpr_No+strSaveFileName, Encoding.UTF8).ToString());
        //    //  Page.EnableViewState = false;
        //    //  Response.TransmitFile(newpath);


        //    FileInfo fileInfo = new FileInfo(fileName.ToString());
        //    Response.Clear();
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(CoperationAuditEntity.cpr_No + strSaveFileName, Encoding.UTF8).ToString());
        //    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
        //    Response.AddHeader("Content-Transfer-Encoding", "binary");
        //    Response.ContentType = "application/octet-stream";
        //    Response.ContentEncoding = System.Text.Encoding.GetEncoding("gb2312");
        //    Response.WriteFile(fileInfo.FullName);
        //    Response.Flush();
        //    Response.End();
        //    // }
        //    // 
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    App.Application.Quit(ref missing, ref missing, ref missing);
        //    //    TG.Common.MessageBox.Show(this, ex.ToString());
        //    //}

        //}

        //public bool ReplaceText(string findStr, string replaceStr)
        //{
        //    object replaceAll = Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll;

        //    object missing = System.Reflection.Missing.Value;

        //    App.Selection.Find.ClearFormatting();

        //    object findText = findStr;

        //    App.Selection.Find.Replacement.ClearFormatting();
        //    App.Selection.Find.Replacement.Text = replaceStr;

        //    if (App.Selection.Find.Execute(ref findText, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
        // ref missing, ref missing, ref missing, ref replaceAll, ref missing, ref missing, ref missing, ref missing))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        #region

        /// <summary>
        /// 模板导出word
        /// </summary>
        public void ExporTotWord()
        {
            //IWorkbook workbook = null;
            //ISheet sheet = null;
            //IRow row = null;
            //ICell cell = null;
            //PackagePart pPart = null;
            //IEnumerator<PackagePart> pIter = null;
            //FileStream fs = new FileStream(Server.MapPath("/Template/sc_template.doc"), FileMode.Open, FileAccess.ReadWrite);
            //XWPFDocument doc = new XWPFDocument(fs);
            //List<PackagePart> embeddedDocs = doc.GetAllEmbedds();
            //if (embeddedDocs != null && embeddedDocs.Count != 0)
            //{
            //    pIter = embeddedDocs.GetEnumerator();
            //    while (pIter.MoveNext())
            //    {
            //        pPart = pIter.Current;
            //       // if (pPart.PartName.Extension.Equals(BINARY_EXTENSION) ||
            //             //   pPart.PartName.Extension.Equals(OPENXML_EXTENSION))
            //    //    {

            //            workbook = WorkbookFactory.Create(pPart.GetInputStream());
            //            sheet = workbook.GetSheetAt(0);
            //            row = sheet.GetRow(0);
            //            cell = row.GetCell(1);
            //            cell.SetCellValue(2);
            //            workbook.Write(pPart.GetOutputStream());
            //       // }
            //    }
            //}

            //using (MemoryStream memoryStream = new MemoryStream())
            //{

            //    doc.Write(memoryStream);
            //    string name = System.Web.HttpContext.Current.Server.UrlEncode("合同");
            //    Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", HttpUtility.UrlEncode(name, Encoding.UTF8)));
            //    Response.ContentType = "application/msword;charset=UTF-8";
            //    Response.BinaryWrite(memoryStream.ToArray());
            //    Response.ContentEncoding = Encoding.UTF8;
            //    doc = null;
            //    Response.End();
            //}



        }
        #endregion
        /// <summary>
        /// 查询客户信息
        /// </summary>
        private void GetCustomerInfo()
        {
            CustomerInfo = new TG.BLL.cm_CustomerInfo().GetModel(Convert.ToInt32(CoperationAuditEntity.cst_Id));
        }

        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        private void GetCoperationInfo()
        {
            List<TG.Model.cm_CoperationAuditListView> coperationListView = new TG.BLL.cm_CoperationAudit().GetCoperationListView(" and cpr_Id=" + CoperationSysNo);
            if (coperationListView != null && coperationListView.Count > 0)
            {
                CoperationAuditEntity = coperationListView[0];

            }
        }
    }
}