﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using TG.BLL;
using NPOI.SS.Util;

namespace TG.Web.Coperation
{
    public partial class ProjectIndustryRpt : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectedCurYear();
                LoadUnitTree();
                SelectedCurUnit();
                //统计数据
                GetAllData();
            }
        }

        #region 基本设置 2015年6月9日
        /// <summary>
        /// 统计语言
        /// </summary>
        public string CountTip
        {
            get;
            set;
        }
        /// <summary>
        /// 当前年
        /// </summary>
        public string CurYear { get; set; }
        public string CurMonth { get; set; }
        /// <summary>
        /// 前一年
        /// </summary>
        public string BeforeYear { get; set; }
        public string BeforeMonth { get; set; }
        /// <summary>
        /// 表格数据
        /// </summary>
        public string HtmlTable { get; set; }
        /// <summary>
        /// 绑定多选年
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();

            if (list.Count > 0)
            {
                foreach (string str in list)
                {
                    //加载年
                    drpYear.Items.Add(new ListItem(str, str));
                    drpYear2.Items.Add(new ListItem(str, str));
                }
            }
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void SelectedCurYear()
        {
            int curyear = DateTime.Now.Year;
            string curmonth = DateTime.Now.Month.ToString();
            //当前年
            if (drpYear2.Items.FindByValue(curyear.ToString()) != null)
            {
                drpYear2.Items.FindByValue(curyear.ToString()).Selected = true;
            }

            //前一年
            string beforeyear = (curyear - 1).ToString();
            if (drpYear.Items.FindByValue(beforeyear) != null)
            {
                drpYear.Items.FindByValue(beforeyear).Selected = true;
            }
            else
            {
                var select = drpYear.Items.FindByValue(curyear.ToString());
                if (select != null) drpYear.Items.FindByValue(curyear.ToString()).Selected = true;
            }

            //当前月
            drpMonth.Items.FindByValue(curmonth).Selected = true;
            drpMonth2.Items.FindByValue(curmonth).Selected = true;
        }

        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
        protected DataTable GetUnit()
        {
            string strWhere = " 1=1 ";

            if (base.RolePowerParameterEntity != null)
            {

                //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void GetSelectYear()
        {
            this.BeforeYear = this.drpYear.SelectedValue;
            this.BeforeMonth = this.drpMonth.SelectedValue;
            this.CurYear = this.drpYear2.SelectedValue;
            this.CurMonth = this.drpMonth2.SelectedValue;
        }
        #endregion

        #region 统计表


        /// <summary>
        /// 统计部门数据
        /// </summary>
        protected void GetCountDataByUnit()
        {
            StringBuilder sbTable = new StringBuilder();
            //选中的部门
            List<ASTreeViewNode> checkedNods = this.drp_unit.GetCheckedNodes(false);
            //不显示
            string strUnitIDArry = "0";

            //全没选
            if (checkedNods.Count > 0)
            {
                foreach (ASTreeViewNode node in checkedNods)
                {
                    if (node.NodeValue == "0")
                        continue;
                    //部门名称
                    string unitID = node.NodeValue.Trim();
                    strUnitIDArry += "," + unitID;

                }
            }
            else
            {
                //循环部门
                DataTable dt = GetUnit();
                //部门名称

                foreach (DataRow dr in dt.Rows)
                {
                    //部门名称
                    string unitID = dr["unit_ID"].ToString().Trim();
                    strUnitIDArry += "," + unitID;
                }
            }

            sbTable.Append(CreateTableRowByUnit(strUnitIDArry));

            HtmlTable = sbTable.ToString();

            //保存导出列表对象
            ViewState["ExcelList"] = this.ImportExcelList;
        }
        /// <summary>
        /// 创建统计行
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        private string CreateTableRowByUnit(string unitarr)
        {
            StringBuilder sbTable = new StringBuilder();
            //加载上年数据
            DataTable dtDataBef = GetCountData(unitarr, BeforeYear, BeforeMonth);
            //加载今年数据
            DataTable dtDataCur = GetCountData(unitarr, CurYear, CurMonth);

            for (int i = 0; i < dtDataBef.Rows.Count; i++)
            {
                //BefRow
                DataRow drbef = dtDataBef.Rows[i];
                //CurRow
                DataRow drcur = dtDataCur.Rows[i];

                sbTable.Append("<tr>");
                //上年
                sbTable.AppendFormat("<td>{0}</td>", drbef["unit_Name"].ToString().Trim());
                sbTable.AppendFormat("<td>{0}</td>", drbef["GongJian"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drbef["ShiZheng"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drbef["FangDiChan"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drbef["ZongHe"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drbef["HeJi"].ToString());
                //今年
                sbTable.AppendFormat("<td>{0}</td>", drcur["GongJian"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drcur["ShiZheng"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drcur["FangDiChan"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drcur["ZongHe"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", drcur["HeJi"].ToString());
                //同比率
                string compareGongjian = GetCompareValue(drcur["GongJian"].ToString(), drbef["GongJian"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", compareGongjian);
                compareGongjian = GetCompareValueExcel(drcur["GongJian"].ToString(), drbef["GongJian"].ToString());
                string compareShizheng = GetCompareValue(drcur["ShiZheng"].ToString(), drbef["ShiZheng"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", compareShizheng);
                compareShizheng = GetCompareValueExcel(drcur["ShiZheng"].ToString(), drbef["ShiZheng"].ToString());
                string compareFangdichan = GetCompareValue(drcur["FangDiChan"].ToString(), drbef["FangDiChan"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", compareFangdichan);
                compareFangdichan = GetCompareValueExcel(drcur["FangDiChan"].ToString(), drbef["FangDiChan"].ToString());
                string compareZonghe = GetCompareValue(drcur["ZongHe"].ToString(), drbef["ZongHe"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", compareZonghe);
                compareZonghe = GetCompareValueExcel(drcur["ZongHe"].ToString(), drbef["ZongHe"].ToString());
                string compareHeji = GetCompareValue(drcur["HeJi"].ToString(), drbef["HeJi"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", compareHeji);
                compareHeji = GetCompareValueExcel(drcur["HeJi"].ToString(), drbef["HeJi"].ToString());
                sbTable.Append("</tr>");

                //创建对象列表
                TG.Model.ProjectIndustryExcel excel = new TG.Model.ProjectIndustryExcel()
                {
                    UnitID = drcur["unit_ID"].ToString(),
                    UnitName = drbef["unit_Name"].ToString().Trim(),
                    GongJian = drbef["GongJian"].ToString(),
                    ShiZheng = drbef["ShiZheng"].ToString(),
                    FangDiChan = drbef["FangDiChan"].ToString(),
                    ZongHe = drbef["ZongHe"].ToString(),
                    HeJi = drbef["HeJi"].ToString(),
                    GongJianTwo = drcur["GongJian"].ToString(),
                    ShiZhengTwo = drcur["ShiZheng"].ToString(),
                    FangDiChanTwo = drcur["FangDiChan"].ToString(),
                    ZongHeTwo = drcur["ZongHe"].ToString(),
                    HeJiTwo = drcur["HeJi"].ToString(),
                    CompareGongJian = compareGongjian,
                    CompareShiZheng = compareShizheng,
                    CompareFangDiChan = compareFangdichan,
                    CompareZongHe = compareZonghe,
                    CompareHeJi = compareHeji
                };

                this.ImportExcelList.Add(excel);
            }

            return sbTable.ToString();
        }

        /// <summary>
        /// 获取同比值
        /// </summary>
        /// <param name="curValue"></param>
        /// <param name="beforeValue"></param>
        /// <returns></returns>
        public string GetCompareValue(string curValue, string beforeValue)
        {
            //本年度值
            decimal curCount = Convert.ToDecimal(curValue);
            //上年度值
            decimal beforeCount = Convert.ToDecimal(beforeValue);
            //增长率
            //返回结果
            string result = "";
            if (beforeCount != 0)
            {
                //同比增长
                decimal growPersent = ((curCount - beforeCount) / beforeCount) * 100;

                if (growPersent > 0)
                {
                    result = string.Format("<span style=\"color:red;font-weight:500;\">{0}%</span>",
                        growPersent.ToString("f2"));
                }
                else if (growPersent < 0)
                {
                    result = string.Format("<span style=\"color:green;font-weight:500;\">{0}%</span>",
                        growPersent.ToString("f2"));
                }
                else
                {
                    result = string.Format("<span style=\"font-weight:500;\">{0}%</span>",
                        growPersent.ToString("f2"));
                }
            }
            else
            {
                result = "------";
            }


            return result;
        }
        /// <summary>
        /// 获取同比值
        /// </summary>
        /// <param name="curValue"></param>
        /// <param name="beforeValue"></param>
        /// <returns></returns>
        public string GetCompareValueExcel(string curValue, string beforeValue)
        {
            //本年度值
            decimal curCount = Convert.ToDecimal(curValue);
            //上年度值
            decimal beforeCount = Convert.ToDecimal(beforeValue);
            //增长率
            //返回结果
            string result = "";
            if (beforeCount != 0)
            {
                //同比增长
                decimal growPersent = ((curCount - beforeCount) / beforeCount) * 100;

                if (growPersent > 0)
                {
                    result = string.Format("{0}%",
                        growPersent.ToString("f2"));
                }
                else if (growPersent < 0)
                {
                    result = string.Format("{0}%",
                        growPersent.ToString("f2"));
                }
                else
                {
                    result = string.Format("{0}%",
                        growPersent.ToString("f2"));
                }
            }
            else
            {
                result = "";
            }


            return result;
        }
        protected DataTable GetCountData(string unitarr, string year, string month)
        {
            StringBuilder sbSql = new StringBuilder();
            //获取月最后一天
            string days = DateTime.DaysInMonth(int.Parse(year), int.Parse(month)).ToString();

            sbSql.AppendFormat(@"Select unit_ID, unit_Name
		                                ,(
			                                Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit=U.unit_Name 
			                                AND Industry='公建' 
			                                AND pro_startTime BETWEEN '{0}-01-01' AND '{0}-{1}-{2}'
		                                ) AS GongJian
		                                ,(
			                                Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit=U.unit_Name 
			                                AND Industry='市政' 
			                                AND pro_startTime BETWEEN '{0}-01-01' AND '{0}-{1}-{2}'
		                                ) AS ShiZheng
		                                ,(
			                                Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit=U.unit_Name 
			                                AND Industry='房地产' 
			                                AND pro_startTime BETWEEN '{0}-01-01' AND '{0}-{1}-{2}'
		                                ) AS FangDiChan
		                                ,(
			                                Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit=U.unit_Name 
			                                AND Industry NOT IN ('公建','市政','房地产') 
			                                AND pro_startTime BETWEEN '{0}-01-01' AND '{0}-{1}-{2}'
		                                ) AS ZongHe
		                                ,(
			                                Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit=U.unit_Name
			                                AND pro_startTime BETWEEN '{0}-01-01' AND '{0}-{1}-{2}'
		                                ) AS HeJi
                                From tg_unit U 
                                Where unit_ID IN ({3}) order By unit_ID ", year, month, days, unitarr);

            DataTable dt = DBUtility.DbHelperSQL.Query(sbSql.ToString()).Tables[0];
            return dt;
        }

        /// <summary>
        /// 统计提示
        /// </summary>
        protected void GetCountCondition()
        {
            string strTip = "";
            strTip += this.drpYear.SelectedValue + "年";
            strTip += "(1-";
            strTip += this.drpMonth.SelectedValue + ")月";
            strTip += "已签合同额[项目分类]与";
            strTip += this.drpYear2.SelectedValue + "年";
            strTip += "(1-";
            strTip += this.drpMonth2.SelectedValue + ")月";
            strTip += "同期对比统计表";

            this.CountTip = strTip;
        }
        #endregion

        #region 统计图表基本设置 2015年6月10日
        /// <summary>
        /// Y坐标数据
        /// </summary>
        public string xAxis { get; set; }
        /// <summary>
        /// y坐标数据
        /// </summary>
        public string yAxis { get; set; }
        /// <summary>
        /// 图例
        /// </summary>
        public string LegendData { get; set; }
        /// <summary>
        /// 统计数据
        /// </summary>
        public string SeriesData { get; set; }
        #endregion

        #region 图表统计数据 2015年6月10日
        /// <summary>
        /// 获取图例数据
        /// </summary>
        protected void GetLegendValue()
        {
            //范围图
            StringBuilder legendData = new StringBuilder();
            //数据图例
            legendData.Append("[");

            string beforetime = this.drpYear.SelectedValue + "年" + this.drpMonth.SelectedValue + "月";
            legendData.AppendFormat("\"{0}\",", beforetime + "(公建)");
            legendData.AppendFormat("\"{0}\",", beforetime + "(市政)");
            legendData.AppendFormat("\"{0}\",", beforetime + "(房地产)");
            legendData.AppendFormat("\"{0}\",", beforetime + "(综合)");
            legendData.AppendFormat("\"{0}\",", beforetime + "(总计)");
            string curtime = this.drpYear2.SelectedValue + "年" + this.drpMonth2.SelectedValue + "月";
            legendData.AppendFormat("\"{0}\",", curtime + "(公建)");
            legendData.AppendFormat("\"{0}\",", curtime + "(市政)");
            legendData.AppendFormat("\"{0}\",", curtime + "(房地产)");
            legendData.AppendFormat("\"{0}\",", curtime + "(综合)");
            legendData.AppendFormat("\"{0}\",", curtime + "(总计)");
            //x坐标
            legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
            legendData.Append("]");

            LegendData = legendData.ToString();

        }

        /// <summary>
        ///  获取X坐标数据
        /// </summary>
        private void GetxAxisValue()
        {
            //横向坐标
            StringBuilder sbxAxis = new StringBuilder();

            sbxAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                int index = 0;
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    //X坐标数据
                    if (index % 2 == 0)
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                    else
                    {
                        sbxAxis.Append("\"\\n" + unitname + "\",");
                    }

                    index++;
                }

                index = 0;
            }
            else
            {
                int index = 0;
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    if (nodes.Count >= 10)
                    {

                        if (index % 2 == 0)
                        {
                            sbxAxis.Append("\"" + unitname + "\",");
                        }
                        else
                        {
                            sbxAxis.Append("\"\\n" + unitname + "\",");
                        }
                        index++;
                    }
                    else
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                }

                index = 0;
            }
            //x坐标
            sbxAxis.Remove(sbxAxis.ToString().LastIndexOf(','), 1);
            sbxAxis.Append("]");

            xAxis = sbxAxis.ToString();
        }

        /// <summary>
        /// 设置Y坐标数据
        /// </summary>
        private void SetyAxisValue()
        {
            StringBuilder sbyAxis = new StringBuilder();

            sbyAxis.Append(@"{
                            type : 'value',
                            name : '合同额',
                            axisLabel : {
                                formatter: '{value} 万元'}
                        }");

            yAxis = sbyAxis.ToString();
        }

        /// <summary>
        /// 获取实际数据
        /// </summary>
        private void GetSeriesData()
        {
            StringBuilder sbSeries = new StringBuilder();
            //年
            string beforeyear = this.drpYear.SelectedValue;
            string curyear = this.drpYear2.SelectedValue;
            //月
            string beforemonth = this.drpMonth.SelectedValue;
            string curmonth = this.drpMonth2.SelectedValue;
            //日
            string curdays = DateTime.DaysInMonth(int.Parse(curyear), int.Parse(curmonth)).ToString();
            string beforedays = DateTime.DaysInMonth(int.Parse(beforeyear), int.Parse(beforemonth)).ToString();

            //加载去年数据
            //公建
            string strData = GetCountDataByYear(beforeyear, beforemonth, beforedays, "0");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(公建)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", beforeyear, strData, beforemonth);
            //市政
            strData = GetCountDataByYear(beforeyear, beforemonth, beforedays, "1");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(市政)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", beforeyear, strData, beforemonth);
            //房地产
            strData = GetCountDataByYear(beforeyear, beforemonth, beforedays, "2");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(房地产)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", beforeyear, strData, beforemonth);
            //综合
            strData = GetCountDataByYear(beforeyear, beforemonth, beforedays, "4");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(综合)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", beforeyear, strData, beforemonth);
            //总计
            strData = GetCountDataByYear(beforeyear, beforemonth, beforedays, "");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(总计)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", beforeyear, strData, beforemonth);
            //加载今年数据
            //公建
            strData = GetCountDataByYear(curyear, curmonth, curdays, "0");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(公建)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", curyear, strData, curmonth);
            //市政
            strData = GetCountDataByYear(curyear, curmonth, curdays, "1");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(市政)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", curyear, strData, curmonth);
            //房地产
            strData = GetCountDataByYear(curyear, curmonth, curdays, "2");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(房地产)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", curyear, strData, curmonth);
            //综合
            strData = GetCountDataByYear(curyear, curmonth, curdays, "3");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(综合)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", curyear, strData, curmonth);
            //总计
            strData = GetCountDataByYear(curyear, curmonth, curdays, "");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(总计)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", curyear, strData, curmonth);
            sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
            //返回数据
            SeriesData = sbSeries.ToString();

        }
        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="slttype"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        private string GetCountDataByYear(string year, string month, string days, string type)
        {
            //横向坐标
            //统计值
            StringBuilder sbyAxis = new StringBuilder();
            sbyAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    sbyAxis.Append(GetCprCountByUnit(unitname, year, month, days, type) + ",");
                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    sbyAxis.Append(GetCprCountByUnit(unitname, year, month, days, type) + ",");
                }
            }
            //y坐标
            sbyAxis.Remove(sbyAxis.ToString().LastIndexOf(','), 1);
            sbyAxis.Append("]");

            return sbyAxis.ToString();
        }

        #endregion
        protected void GetAllData()
        {
            //选中当前年
            GetSelectYear();
            //组织话术
            GetCountCondition();
            //获取数据
            GetCountDataByUnit();

            //图表开始
            GetLegendValue();
            //设置横坐标
            GetxAxisValue();
            //设置y单位
            SetyAxisValue();
            //统计值
            GetSeriesData();
        }
        /// <summary>
        /// 查询时间
        /// </summary>
        /// <param name="unitid"></param>
        /// <returns></returns>
        protected string GetCprCountByUnit(string unitname, string year, string month, string days, string type)
        {
            //总计
            string strSql = string.Format(@" Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit='{0}' ", unitname);

            //当月
            if (type == "0")
            {
                strSql = string.Format(@" Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit='{0}'
			                                AND Industry='公建' ", unitname);
            }
            else if (type == "1")
            {
                strSql = string.Format(@" Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit='{0}'
			                                AND Industry='市政' ", unitname);
            }
            else if (type == "2")
            {
                strSql = string.Format(@" Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit='{0}'
			                                AND Industry='房地产' ", unitname);
            }
            else if (type == "3")
            {
                strSql = string.Format(@" Select ISNULL(SUM(Cpr_Acount),0)
			                                From cm_Project
			                                Where Unit='{0}'
			                                AND Industry NOT IN ('公建','市政','房地产') ", unitname);
            }
            //时间
            strSql += string.Format(" AND (pro_startTime BETWEEN '{0}-01-01' AND '{0}-{1}-{2}')", year, month, days);

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetAllData();
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcelByData();
        }

        /// <summary>
        /// 导出列表列表集合
        /// </summary>
        protected List<TG.Model.ProjectIndustryExcel> ImportExcelList =
            new List<TG.Model.ProjectIndustryExcel>();
        /// <summary>
        /// 导出Excel
        /// </summary>
        protected void ExportExcelByData()
        {
            GetCountCondition();
            //导出列表
            List<TG.Model.ProjectIndustryExcel> excellist = ViewState["ExcelList"] as List<TG.Model.ProjectIndustryExcel>;
            //模板路径
            string pathModel = "~/TemplateXls/ProjectIndustryTlt.xls";

            //导出Excel
            ExportDataToExcel(excellist, pathModel);
        }
        private void ExportDataToExcel(List<TG.Model.ProjectIndustryExcel> excellist, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";

            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            string title = this.drpYear.SelectedValue + "年（1-" + this.drpMonth.SelectedValue + "月)已签订合同额【项目分类】与" +
                           this.drpYear2.SelectedValue + "年(1-" + this.drpMonth2.SelectedValue + "月)同期对比表";

            ws.GetRow(0).GetCell(0).SetCellValue(title);
            ws.GetRow(1).GetCell(0).SetCellValue(DateTime.Now.ToString("单位：万元          yyyy年-MM月-dd日") + "制表");
            ws.GetRow(2).GetCell(2).SetCellValue(this.drpYear.SelectedValue + "年按类别划分");
            ws.GetRow(2).GetCell(7).SetCellValue(this.drpYear2.SelectedValue + "年按类别划分");

            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style.VerticalAlignment = VerticalAlignment.CENTER;
            int index = 4;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            // ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            //进行切割数据集合
            List<TG.Model.ProjectIndustryExcel> listGL = new List<TG.Model.ProjectIndustryExcel>();
            List<TG.Model.ProjectIndustryExcel> listSJ = new List<TG.Model.ProjectIndustryExcel>();
            List<TG.Model.ProjectIndustryExcel> listKC = new List<TG.Model.ProjectIndustryExcel>();
            List<TG.Model.ProjectIndustryExcel> listJL = new List<TG.Model.ProjectIndustryExcel>();
            List<TG.Model.ProjectIndustryExcel> listDJ = new List<TG.Model.ProjectIndustryExcel>();
            foreach (TG.Model.ProjectIndustryExcel item in excellist)
            {
                if (unitparenrID(item.UnitID.ToString()) == "238")
                {
                    if (item.UnitID.ToString() == "250")//勘察
                    {
                        listKC.Add(item);
                    }
                    else if (item.UnitID.ToString() == "253")//监理
                    {
                        listJL.Add(item);
                    }
                    else//其他
                    {
                        listSJ.Add(item);
                    }
                }
                else if (unitparenrID(item.UnitID.ToString()) == "254")
                {
                    listDJ.Add(item);
                }
                else
                {
                    if (unitparenrID(item.UnitID.ToString()) != "0" || item.UnitID.ToString() == "230")
                    {
                        listGL.Add(item);
                    }

                }
            }
            if (excellist.Count > 0)
            {
                //管理部门表头
                if (listGL.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("管理部门部分");
                    index = index + 1;
                    ws.AddMergedRegion(new CellRangeAddress(index - 1, index - 1, 0, 16));
                }
                //遍历管理部门列表
                foreach (TG.Model.ProjectIndustryExcel item in listGL)
                {
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.UnitName);
                    cell0.CellStyle = style;
                    ICell cell00 = row.CreateCell(1);
                    cell00.SetCellValue("");
                    cell00.CellStyle = style;
                    ICell cell1 = row.CreateCell(2);
                    cell1.SetCellValue(Convert.ToDouble(item.GongJian));
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(3);
                    cell2.SetCellValue(Convert.ToDouble(item.ShiZheng));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(4);
                    cell3.SetCellValue(Convert.ToDouble(item.FangDiChan));
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(5);
                    cell4.SetCellValue(Convert.ToDouble(item.ZongHe)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(6);
                    cell5.SetCellValue(Convert.ToDouble(item.HeJi)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(7);
                    cell6.SetCellValue(Convert.ToDouble(item.GongJianTwo)); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(8);
                    cell7.SetCellValue(Convert.ToDouble(item.ShiZhengTwo)); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(9);
                    cell8.SetCellValue(Convert.ToDouble(item.FangDiChanTwo)); cell8.CellStyle = style;
                    ICell cell9 = row.CreateCell(10);
                    cell9.SetCellValue(Convert.ToDouble(item.ZongHeTwo)); cell9.CellStyle = style;
                    ICell cell10 = row.CreateCell(11);
                    cell10.SetCellValue(Convert.ToDouble(item.HeJiTwo)); cell10.CellStyle = style;
                    ICell cell11 = row.CreateCell(12);
                    cell11.SetCellValue(item.CompareGongJian); cell11.CellStyle = style;
                    ICell cell12 = row.CreateCell(13);
                    cell12.SetCellValue(item.CompareShiZheng); cell12.CellStyle = style;
                    ICell cell13 = row.CreateCell(14);
                    cell13.SetCellValue(item.CompareFangDiChan); cell13.CellStyle = style;
                    ICell cell14 = row.CreateCell(15);
                    cell14.SetCellValue(item.CompareZongHe); cell14.CellStyle = style;
                    ICell cell15 = row.CreateCell(16);
                    cell15.SetCellValue(item.CompareHeJi); cell15.CellStyle = style;
                    index = index + 1;
                }
                //增加管理部门小合计
                if (listGL.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("管理合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C3:C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(D3:D" + (index - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E3:E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F3:F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(G3:G" + (index - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(H3:H" + (index - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(I3:I" + (index - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(J3:J" + (index - 1) + ")");
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(K3:K" + (index - 1) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(L3:L" + (index - 1) + ")");

                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(H" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(13);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    cell = dataRow.CreateCell(14);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(J" + index + "-E" + index + ")/E" + index + "*100)");
                    cell = dataRow.CreateCell(15);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(F" + index + "=0,\" \",(K" + index + "-F" + index + ")/F" + index + "*100)");
                    cell = dataRow.CreateCell(16);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(G" + index + "=0,\" \",(L" + index + "-G" + index + ")/G" + index + "*100)");
                }
                //设计所表头
                if (listSJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("设计、监理、勘察部分");
                    index = index + 1;
                    ws.AddMergedRegion(new CellRangeAddress(index - 1, index - 1, 0, 16));
                }
                //遍历设计设计所的部门列表
                foreach (TG.Model.ProjectIndustryExcel item in listSJ)
                {
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.UnitName);
                    cell0.CellStyle = style;
                    ICell cell00 = row.CreateCell(1);
                    cell00.SetCellValue("");
                    cell00.CellStyle = style;
                    ICell cell1 = row.CreateCell(2);
                    cell1.SetCellValue(Convert.ToDouble(item.GongJian));
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(3);
                    cell2.SetCellValue(Convert.ToDouble(item.ShiZheng));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(4);
                    cell3.SetCellValue(Convert.ToDouble(item.FangDiChan));
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(5);
                    cell4.SetCellValue(Convert.ToDouble(item.ZongHe)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(6);
                    cell5.SetCellValue(Convert.ToDouble(item.HeJi)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(7);
                    cell6.SetCellValue(Convert.ToDouble(item.GongJianTwo)); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(8);
                    cell7.SetCellValue(Convert.ToDouble(item.ShiZhengTwo)); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(9);
                    cell8.SetCellValue(Convert.ToDouble(item.FangDiChanTwo)); cell8.CellStyle = style;
                    ICell cell9 = row.CreateCell(10);
                    cell9.SetCellValue(Convert.ToDouble(item.ZongHeTwo)); cell9.CellStyle = style;
                    ICell cell10 = row.CreateCell(11);
                    cell10.SetCellValue(Convert.ToDouble(item.HeJiTwo)); cell10.CellStyle = style;
                    ICell cell11 = row.CreateCell(12);
                    cell11.SetCellValue(item.CompareGongJian); cell11.CellStyle = style;
                    ICell cell12 = row.CreateCell(13);
                    cell12.SetCellValue(item.CompareShiZheng); cell12.CellStyle = style;
                    ICell cell13 = row.CreateCell(14);
                    cell13.SetCellValue(item.CompareFangDiChan); cell13.CellStyle = style;
                    ICell cell14 = row.CreateCell(15);
                    cell14.SetCellValue(item.CompareZongHe); cell14.CellStyle = style;
                    ICell cell15 = row.CreateCell(16);
                    cell15.SetCellValue(item.CompareHeJi); cell15.CellStyle = style;

                    index = index + 1;
                }
                //设计所小计
                if (listSJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("设计所合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - listSJ.Count) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(D" + (index - listSJ.Count) + ":D" + (index - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - listSJ.Count) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - listSJ.Count) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(G" + (index - listSJ.Count) + ":G" + (index - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(H" + (index - listSJ.Count) + ":H" + (index - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(I" + (index - listSJ.Count) + ":I" + (index - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(J" + (index - listSJ.Count) + ":J" + (index - 1) + ")");
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(K" + (index - listSJ.Count) + ":K" + (index - 1) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(L" + (index - listSJ.Count) + ":L" + (index - 1) + ")");
                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(H" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(13);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    cell = dataRow.CreateCell(14);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(J" + index + "-E" + index + ")/E" + index + "*100)");
                    cell = dataRow.CreateCell(15);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(F" + index + "=0,\" \",(K" + index + "-F" + index + ")/F" + index + "*100)");
                    cell = dataRow.CreateCell(16);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(G" + index + "=0,\" \",(K" + index + "-G" + index + ")/G" + index + "*100)");
                }
                //勘察和设计所小合计
                if (listKC.Count > 0)
                {
                    foreach (TG.Model.ProjectIndustryExcel item in listKC)
                    {
                        IRow row = ws.CreateRow(index);
                        ICell cell0 = row.CreateCell(0);
                        cell0.SetCellValue(item.UnitName);
                        cell0.CellStyle = style;
                        ICell cell00 = row.CreateCell(1);
                        cell00.SetCellValue("");
                        cell00.CellStyle = style;
                        ICell cell1 = row.CreateCell(2);
                        cell1.SetCellValue(Convert.ToDouble(item.GongJian));
                        cell1.CellStyle = style;
                        ICell cell2 = row.CreateCell(3);
                        cell2.SetCellValue(Convert.ToDouble(item.ShiZheng));
                        cell2.CellStyle = style;
                        ICell cell3 = row.CreateCell(4);
                        cell3.SetCellValue(Convert.ToDouble(item.FangDiChan));
                        cell3.CellStyle = style;
                        ICell cell4 = row.CreateCell(5);
                        cell4.SetCellValue(Convert.ToDouble(item.ZongHe)); cell4.CellStyle = style;
                        ICell cell5 = row.CreateCell(6);
                        cell5.SetCellValue(Convert.ToDouble(item.HeJi)); cell5.CellStyle = style;
                        ICell cell6 = row.CreateCell(7);
                        cell6.SetCellValue(Convert.ToDouble(item.GongJianTwo)); cell6.CellStyle = style;
                        ICell cell7 = row.CreateCell(8);
                        cell7.SetCellValue(Convert.ToDouble(item.ShiZhengTwo)); cell7.CellStyle = style;
                        ICell cell8 = row.CreateCell(9);
                        cell8.SetCellValue(Convert.ToDouble(item.FangDiChanTwo)); cell8.CellStyle = style;
                        ICell cell9 = row.CreateCell(10);
                        cell9.SetCellValue(Convert.ToDouble(item.ZongHeTwo)); cell9.CellStyle = style;
                        ICell cell10 = row.CreateCell(11);
                        cell10.SetCellValue(Convert.ToDouble(item.HeJiTwo)); cell10.CellStyle = style;
                        ICell cell11 = row.CreateCell(12);
                        cell11.SetCellValue(item.CompareGongJian); cell11.CellStyle = style;
                        ICell cell12 = row.CreateCell(13);
                        cell12.SetCellValue(item.CompareShiZheng); cell12.CellStyle = style;
                        ICell cell13 = row.CreateCell(14);
                        cell13.SetCellValue(item.CompareFangDiChan); cell13.CellStyle = style;
                        ICell cell14 = row.CreateCell(15);
                        cell14.SetCellValue(item.CompareZongHe); cell14.CellStyle = style;
                        ICell cell15 = row.CreateCell(16);
                        cell15.SetCellValue(item.CompareHeJi); cell15.CellStyle = style;

                        index = index + 1;
                    }
                    //顺便把合计给加上
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("勘察、设计合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - 2) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(D" + (index - 2) + ":D" + (index - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - 2) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - 2) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(G" + (index - 2) + ":G" + (index - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(H" + (index - 2) + ":H" + (index - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(I" + (index - 2) + ":I" + (index - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(J" + (index - 2) + ":J" + (index - 1) + ")");
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(K" + (index - 2) + ":K" + (index - 1) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(L" + (index - 2) + ":L" + (index - 1) + ")");
                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(H" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(13);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    cell = dataRow.CreateCell(14);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(J" + index + "-E" + index + ")/E" + index + "*100)");
                    cell = dataRow.CreateCell(15);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(F" + index + "=0,\" \",(K" + index + "-F" + index + ")/F" + index + "*100)");
                    cell = dataRow.CreateCell(16);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(G" + index + "=0,\" \",(L" + index + "-G" + index + ")/G" + index + "*100)");
                }
                //接下来是监理合计
                if (listJL.Count > 0)
                {
                    //遍历把监理加上
                    foreach (TG.Model.ProjectIndustryExcel item in listJL)
                    {
                        IRow row = ws.CreateRow(index);
                        ICell cell0 = row.CreateCell(0);
                        cell0.SetCellValue(item.UnitName);
                        cell0.CellStyle = style;
                        ICell cell00 = row.CreateCell(1);
                        cell00.SetCellValue("");
                        cell00.CellStyle = style;
                        ICell cell1 = row.CreateCell(2);
                        cell1.SetCellValue(Convert.ToDouble(item.GongJian));
                        cell1.CellStyle = style;
                        ICell cell2 = row.CreateCell(3);
                        cell2.SetCellValue(Convert.ToDouble(item.ShiZheng));
                        cell2.CellStyle = style;
                        ICell cell3 = row.CreateCell(4);
                        cell3.SetCellValue(Convert.ToDouble(item.FangDiChan));
                        cell3.CellStyle = style;
                        ICell cell4 = row.CreateCell(5);
                        cell4.SetCellValue(Convert.ToDouble(item.ZongHe)); cell4.CellStyle = style;
                        ICell cell5 = row.CreateCell(6);
                        cell5.SetCellValue(Convert.ToDouble(item.HeJi)); cell5.CellStyle = style;
                        ICell cell6 = row.CreateCell(7);
                        cell6.SetCellValue(Convert.ToDouble(item.GongJianTwo)); cell6.CellStyle = style;
                        ICell cell7 = row.CreateCell(8);
                        cell7.SetCellValue(Convert.ToDouble(item.ShiZhengTwo)); cell7.CellStyle = style;
                        ICell cell8 = row.CreateCell(9);
                        cell8.SetCellValue(Convert.ToDouble(item.FangDiChanTwo)); cell8.CellStyle = style;
                        ICell cell9 = row.CreateCell(10);
                        cell9.SetCellValue(Convert.ToDouble(item.ZongHeTwo)); cell9.CellStyle = style;
                        ICell cell10 = row.CreateCell(11);
                        cell10.SetCellValue(Convert.ToDouble(item.HeJiTwo)); cell10.CellStyle = style;
                        ICell cell11 = row.CreateCell(12);
                        cell11.SetCellValue(item.CompareGongJian); cell11.CellStyle = style;
                        ICell cell12 = row.CreateCell(13);
                        cell12.SetCellValue(item.CompareShiZheng); cell12.CellStyle = style;
                        ICell cell13 = row.CreateCell(14);
                        cell13.SetCellValue(item.CompareFangDiChan); cell13.CellStyle = style;
                        ICell cell14 = row.CreateCell(15);
                        cell14.SetCellValue(item.CompareZongHe); cell14.CellStyle = style;
                        ICell cell15 = row.CreateCell(16);
                        cell15.SetCellValue(item.CompareHeJi); cell15.CellStyle = style;
                        index = index + 1;
                    }
                    //顺便把合计给加上
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("主营合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - 2) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(D" + (index - 2) + ":D" + (index - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - 2) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - 2) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(G" + (index - 2) + ":G" + (index - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(H" + (index - 2) + ":H" + (index - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(I" + (index - 2) + ":I" + (index - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(J" + (index - 2) + ":J" + (index - 1) + ")");
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(K" + (index - 2) + ":K" + (index - 1) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(L" + (index - 2) + ":L" + (index - 1) + ")");
                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(H" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(13);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    cell = dataRow.CreateCell(14);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(J" + index + "-E" + index + ")/E" + index + "*100)");
                    cell = dataRow.CreateCell(15);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(F" + index + "=0,\" \",(K" + index + "-F" + index + ")/F" + index + "*100)");
                    cell = dataRow.CreateCell(16);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(G" + index + "=0,\" \",(L" + index + "-G" + index + ")/G" + index + "*100)");
                }
                //多经部门头
                //多经部门
                if (listDJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("多种经营部分");
                    index = index + 1;
                    ws.AddMergedRegion(new CellRangeAddress(index - 1, index - 1, 0, 16));
                }
                //遍历多经部门列表
                foreach (TG.Model.ProjectIndustryExcel item in listDJ)
                {

                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.UnitName);
                    cell0.CellStyle = style;
                    ICell cell00 = row.CreateCell(1);
                    cell00.SetCellValue("");
                    cell00.CellStyle = style;
                    ICell cell1 = row.CreateCell(2);
                    cell1.SetCellValue(Convert.ToDouble(item.GongJian));
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(3);
                    cell2.SetCellValue(Convert.ToDouble(item.ShiZheng));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(4);
                    cell3.SetCellValue(Convert.ToDouble(item.FangDiChan));
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(5);
                    cell4.SetCellValue(Convert.ToDouble(item.ZongHe)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(6);
                    cell5.SetCellValue(Convert.ToDouble(item.HeJi)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(7);
                    cell6.SetCellValue(Convert.ToDouble(item.GongJianTwo)); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(8);
                    cell7.SetCellValue(Convert.ToDouble(item.ShiZhengTwo)); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(9);
                    cell8.SetCellValue(Convert.ToDouble(item.FangDiChanTwo)); cell8.CellStyle = style;
                    ICell cell9 = row.CreateCell(10);
                    cell9.SetCellValue(Convert.ToDouble(item.ZongHeTwo)); cell9.CellStyle = style;
                    ICell cell10 = row.CreateCell(11);
                    cell10.SetCellValue(Convert.ToDouble(item.HeJiTwo)); cell10.CellStyle = style;
                    ICell cell11 = row.CreateCell(12);
                    cell11.SetCellValue(item.CompareGongJian); cell11.CellStyle = style;
                    ICell cell12 = row.CreateCell(13);
                    cell12.SetCellValue(item.CompareShiZheng); cell12.CellStyle = style;
                    ICell cell13 = row.CreateCell(14);
                    cell13.SetCellValue(item.CompareFangDiChan); cell13.CellStyle = style;
                    ICell cell14 = row.CreateCell(15);
                    cell14.SetCellValue(item.CompareZongHe); cell14.CellStyle = style;
                    ICell cell15 = row.CreateCell(16);
                    cell15.SetCellValue(item.CompareHeJi); cell15.CellStyle = style;

                    index = index + 1;
                }
                //多经营合计
                if (listDJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("多经营所合计");
                    index = index + 1;

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - listDJ.Count) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(D" + (index - listDJ.Count) + ":D" + (index - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - listDJ.Count) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - listDJ.Count) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(G" + (index - listDJ.Count) + ":G" + (index - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(H" + (index - listDJ.Count) + ":H" + (index - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(I" + (index - listDJ.Count) + ":I" + (index - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(J" + (index - listDJ.Count) + ":J" + (index - 1) + ")");
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(K" + (index - listDJ.Count) + ":K" + (index - 1) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(L" + (index - listDJ.Count) + ":L" + (index - 1) + ")");
                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(H" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(13);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    cell = dataRow.CreateCell(14);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(J" + index + "-E" + index + ")/E" + index + "*100)");
                    cell = dataRow.CreateCell(15);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(F" + index + "=0,\" \",(K" + index + "-F" + index + ")/F" + index + "*100)");
                    cell = dataRow.CreateCell(16);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(G" + index + "=0,\" \",(L" + index + "-G" + index + ")/G" + index + "*100)");
                }
                //全院总合计
                if (listDJ.Count != 0 && listSJ.Count != 0 && listGL.Count != 0 && listKC.Count != 0 && listJL.Count != 0)
                {

                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("全院总合计");


                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - listDJ.Count - 2) + ",C" + (index) + ",C" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(D" + (index - listDJ.Count - 2) + ",D" + (index) + ",D" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - listDJ.Count - 2) + ",E" + (index) + ",E" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - listDJ.Count - 2) + ",F" + (index) + ",F" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(G" + (index - listDJ.Count - 2) + ",G" + (index) + ",G" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(H" + (index - listDJ.Count - 2) + ",H" + (index) + ",H" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(I" + (index - listDJ.Count - 2) + ",I" + (index) + ",I" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(J" + (index - listDJ.Count - 2) + ",J" + (index) + ",J" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(K" + (index - listDJ.Count - 2) + ",K" + (index) + ",K" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(L" + (index - listDJ.Count - 2) + ",L" + (index) + ",L" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + (index + 1) + "=0,\" \",(H" + (index + 1) + "-C" + (index + 1) + ")/C" + (index + 1) + "*100)");
                    cell = dataRow.CreateCell(13);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(D" + (index + 1) + "=0,\" \",(I" + (index + 1) + "-D" + (index + 1) + ")/D" + (index + 1) + "*100)");
                    cell = dataRow.CreateCell(14);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + (index + 1) + "=0,\" \",(J" + (index + 1) + "-E" + (index + 1) + ")/E" + (index + 1) + "*100)");
                    cell = dataRow.CreateCell(15);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(F" + (index + 1) + "=0,\" \",(K" + (index + 1) + "-F" + (index + 1) + ")/F" + (index + 1) + "*100)");
                    cell = dataRow.CreateCell(16);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(G" + (index + 1) + "=0,\" \",(L" + (index + 1) + "-G" + (index + 1) + ")/G" + (index + 1) + "*100)");
                }
                //foreach (TG.Model.ProjectIndustryExcel item in excellist)
                //{

                //    IRow row = ws.CreateRow(index);
                //    ICell cell0 = row.CreateCell(0);
                //    cell0.SetCellValue(item.UnitName);
                //    cell0.CellStyle = style;
                //    ICell cell00 = row.CreateCell(1);
                //    cell00.SetCellValue("");
                //    cell00.CellStyle = style;
                //    ICell cell1 = row.CreateCell(2);
                //    cell1.SetCellValue(item.GongJian);
                //    cell1.CellStyle = style;
                //    ICell cell2 = row.CreateCell(3);
                //    cell2.SetCellValue(item.ShiZheng);
                //    cell2.CellStyle = style;
                //    ICell cell3 = row.CreateCell(4);
                //    cell3.SetCellValue(item.FangDiChan);
                //    cell3.CellStyle = style;
                //    ICell cell4 = row.CreateCell(5);
                //    cell4.SetCellValue(item.ZongHe); cell4.CellStyle = style;
                //    ICell cell5 = row.CreateCell(6);
                //    cell5.SetCellValue(item.HeJi); cell5.CellStyle = style;
                //    ICell cell6 = row.CreateCell(7);
                //    cell6.SetCellValue(item.GongJianTwo); cell6.CellStyle = style;
                //    ICell cell7 = row.CreateCell(8);
                //    cell7.SetCellValue(item.ShiZhengTwo); cell7.CellStyle = style;
                //    ICell cell8 = row.CreateCell(9);
                //    cell8.SetCellValue(item.FangDiChanTwo); cell8.CellStyle = style;
                //    ICell cell9 = row.CreateCell(10);
                //    cell9.SetCellValue(item.ZongHeTwo); cell9.CellStyle = style;
                //    ICell cell10 = row.CreateCell(11);
                //    cell10.SetCellValue(item.HeJiTwo); cell10.CellStyle = style;
                //    ICell cell11 = row.CreateCell(12);
                //    cell11.SetCellValue(item.CompareGongJian); cell11.CellStyle = style;
                //    ICell cell12 = row.CreateCell(13);
                //    cell12.SetCellValue(item.CompareShiZheng); cell12.CellStyle = style;
                //    ICell cell13 = row.CreateCell(14);
                //    cell13.SetCellValue(item.CompareFangDiChan); cell13.CellStyle = style;
                //    ICell cell14 = row.CreateCell(15);
                //    cell14.SetCellValue(item.CompareZongHe); cell14.CellStyle = style;
                //    ICell cell15 = row.CreateCell(16);
                //    cell15.SetCellValue(item.CompareHeJi); cell15.CellStyle = style;
                //    //ICell cell16 = row.CreateCell(16);
                //    //cell16.SetCellValue(item.TotalContrast); cell16.CellStyle = style;

                //    index++;
                //}
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(title + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        public string unitparenrID(string unitid)
        {
            return bllUnit.GetModel(int.Parse(unitid)).unit_ParentID.ToString();
        }
    }
}