﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SearchCoperationBymaster.aspx.cs" Inherits="TG.Web.Coperation.SearchCoperationBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/Coperation/cpr_SearchCpr_jq.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同高级查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同高级查询</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>请选择查询项
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_id">
                                    <tr>
                                        <td align="left">
                                            <input id="cpr_No" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_No">合同编号</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Type" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Type">合同分类</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Name" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Name">合同名称</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildArea" name="bb" class="cls_audit" type="checkbox" /><span for="BuildArea">建筑规模</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildUnit" name="bb" class="cls_audit" type="checkbox" /><span for="BuildUnit">建设单位</span>
                                        </td>
                                        <td align="left">
                                            <input id="StructType" name="bb" class="cls_audit" type="checkbox" /><span for="StructType">结构形式</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildStructType" name="bb" class="cls_audit" type="checkbox" /><span for="BuildStructType">建筑分类</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildType" name="bb" class="cls_audit" type="checkbox" /><span for="BuildType">建筑类别</span>
                                        </td>
                                        <td align="left">
                                            <input id="ChgPeople" name="bb" class="cls_audit" type="checkbox" /><span for="ChgPeople">工程负责人</span>
                                        </td>
                                        <td align="left">
                                            <input id="ChgJia" name="bb" class="cls_audit" type="checkbox" /><span for="ChgJia">甲方负责人</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Unit" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Unit">承接部门</span>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td align="left">
                                            <input id="cpr_Acount" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Acount">合同额</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildPosition" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="BuildPosition">工程地点</span>
                                        </td>
                                        <td align="left">
                                            <!---->
                                            <input id="Industry" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="Industry">行业性质</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Process" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Process">合同阶段</span>
                                        </td>

                                        <td align="left">
                                            <input id="BuildSrc" name="bb" class="cls_audit" type="checkbox" /><span for="BuildSrc">工程来源</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_SignDate" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_SignDate">合同签订日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_DoneDate" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_DoneDate">合同完成日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="ssze" name="bb" class="cls_audit" type="checkbox" /><span for="ssze">合同收费</span>
                                        </td>
                                        <td align="left">
                                            <input id="cstName" name="bb" class="cls_audit" type="checkbox" /><span for="cstName">按照客户</span>
                                        </td>
                                        <td align="left">
                                            <input id="InsertDate" name="bb" class="cls_audit" type="checkbox" /><span for="InsertDate">录入时间</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>合同高级查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <div class="btn-group" data-toggle="buttons">
                            <span class="btn blue">查询方式：</span>
                            <label rel="and" class="btn yellow btn-sm active" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option1" value="and">必须
                            </label>
                            <label rel="or" class="btn default btn-sm" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option2" value="or">或
                            </label>

                        </div>
                        <%--<input type="button" class="btn red btn-sm" id="btn_export" value="导出" />--%>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="tbl_id2">
                                    <tr for="cpr_No">
                                        <td>合同编号:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="txt_proName" style="width: 390px;" />
                                        </td>
                                    </tr>
                                    <tr for="cpr_Type">
                                        <td>合同分类:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="txt_ddcpr_Type" runat="Server" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm" Width="180px">
                                                <asp:ListItem Value="-1">----选择合同类别----</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_Name">
                                        <td>合同名称:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="txt_cprName" style="width: 390px;" />
                                        </td>
                                    </tr>
                                    <tr for="BuildArea">
                                        <td>建筑规模:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_buildArea" class="form-control input-sm" style="width: 180px;" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_buildArea2" class="form-control input-sm" style="width: 180px;" />
                                                        &nbsp;平米
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="BuildUnit">
                                        <td>建设单位:
                                        </td>
                                        <td>
                                            <input type="text" id="txt_BuildUnit" class="form-control input-sm " style="width: 390px;" />
                                        </td>
                                    </tr>
                                    <tr for="StructType">
                                        <td>结构形式：
                                        </td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="asTreeviewStruct" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择结构形式--------"
                                                Width="180px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr for="BuildStructType">
                                        <td>建筑分类：
                                        </td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="asTreeviewStructType" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择建筑分类--------"
                                                Width="180px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr for="BuildType">
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_buildtype" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm" Width="180px">
                                                <asp:ListItem Value="-1">------选择类别------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="ChgPeople">
                                        <td>工程负责人:
                                        </td>
                                        <td>
                                            <input type="text" id="txt_proFuze" class="form-control input-sm " style="width: 390px;" />
                                        </td>
                                    </tr>
                                    <tr for="ChgJia">
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <input type="text" id="txtFParty" class="form-control input-sm " style="width: 390px;" />
                                        </td>
                                    </tr>
                                    <tr for="cpr_Unit">
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm"
                                                Width="180">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_Acount">
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txtproAcount" class="form-control input-sm" style="width: 180px;" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtproAcount2" class="form-control input-sm" style="width: 180px;" />
                                                        &nbsp;万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="BuildPosition">
                                        <td>工程地点:
                                        </td>
                                        <td>
                                            <input type="text" id="txtaddress" class="form-control input-sm " style="width: 390px;" />
                                        </td>
                                    </tr>
                                    <tr for="Industry">
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddType" runat="server" CssClass="form-control input-sm" Width="180"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_Process">
                                        <td>合同阶段:
                                        </td>
                                        <td>
                                            <asp:CheckBoxList ID="chk_cprjd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr for="BuildSrc">
                                        <td>工程来源:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddsource" runat="server" AppendDataBoundItems="True" Width="180"
                                                CssClass="form-control input-sm">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_SignDate">
                                        <td>合同签订日期:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_signdate" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                                            runat="Server" style="width: 180px; height: 30px; border: 1px solid #e5e5e5" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_signdate2" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                                            runat="Server" style="width: 180px; height: 30px; border: 1px solid
                #e5e5e5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="cpr_DoneDate">
                                        <td>合同完成日期:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid #e5e5e5" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_finishdate2" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid
                #e5e5e5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr for="ssze">
                                        <td>合同收费:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="projcharge1" class="form-control input-sm" style="width: 180px;" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="projcharge2" class="form-control input-sm" style="width: 180px;" />
                                                        &nbsp;万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="cstName">
                                        <td>按照客户:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="txt_cstname" style="width: 390px;" />
                                        </td>
                                    </tr>
                                    <tr for="InsertDate">
                                        <td>录入时间：</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid #e5e5e5" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid
                #e5e5e5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <input type="button" class="btn
                blue"
                                                id="btn_search" value="查询" />&nbsp;&nbsp;
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>合同列表
                    </div>
                    <div class="actions">
                        <div class="btn-group" id="choose">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <%=ColumnsContent %>
                            </div>
                        </div>

                        <input type="button" class="btn red btn-sm" id="btn_export" value="导出Excel" />
                    </div>
                </div>
                <div class="portlet-body form">

                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
</asp:Content>
