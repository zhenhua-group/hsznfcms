﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_ShowRelativeContractInfoBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_ShowRelativeContractInfoBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ShowRelation_jq.js"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        合同信息管理 <small>合同关联客户</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right">
    </i><a>合同关联客户</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同关联信息</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
