﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Aspose.Words;
using System.Text;
using TG.Common;

namespace TG.Web.Coperation
{
    public partial class cpr_ShowReconnCoprationBymaster : System.Web.UI.Page
    {
        //返回标示
        public string BackFlag
        {
            get
            {
                return Request["flag"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //合同ID
            string str_cprid = Request.QueryString["cprid"] ?? "";
            string str_flag = Request.QueryString["flag"] ?? "";
            if (!IsPostBack)
            {
                this.hid_cprid.Value = str_cprid;
                this.hid_flag.Value = str_flag;
                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
            }
            else
            {
                OutputLocation();
            }
        }

        private void OutputLocation()
        {
            TG.BLL.cm_ReconnCoperation bllcop = new TG.BLL.cm_ReconnCoperation();
            string cprid = Request.QueryString["cprid"];
            TG.Model.cm_ReconnCoperation model = bllcop.GetModel(Convert.ToInt32(cprid));
            int cstid = this.GetCstId(Convert.ToInt32(cprid));
            TG.Model.cm_CustomerInfo conmodel = this.GetConst(cstid);

            string tmppath = Server.MapPath("~/TemplateWord/ReconnCoprationReport.doc");
            Document doc = new Document(tmppath); //载入模板
            //占地面积
            if (doc.Range.Bookmarks["BuildArea"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildArea"];
                mark.Text = model.BuildArea + "亩";
            }
            //建筑物数量
            if (doc.Range.Bookmarks["BuildNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildNumber"];
                mark.Text = model.BuildNumber + "个";
            }
            //工程地点
            if (doc.Range.Bookmarks["BuildPosition"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildPosition"];
                mark.Text = model.BuildPosition == null ? "" : model.BuildPosition;
            }
            if (doc.Range.Bookmarks["BuildSrc"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildSrc"];
                mark.Text = model.BuildSrc == null ? "" : model.BuildSrc;
            }
            //建设单位
            if (doc.Range.Bookmarks["BuildUnit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                mark.Text = model.BuildUnit == null ? "" : model.BuildUnit;
            }
            if (doc.Range.Bookmarks["ChgJia"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJia"];
                mark.Text = model.ChgJia == null ? "" : model.ChgJia;
            }
            if (doc.Range.Bookmarks["ChgJiaPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJiaPhone"];
                mark.Text = model.ChgJiaPhone == null ? "" : model.ChgJiaPhone;
            }
            if (doc.Range.Bookmarks["ChgPeople"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPeople"];
                mark.Text = model.ChgPeople == null ? "" : model.ChgPeople;
            }
            if (doc.Range.Bookmarks["ChgPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPhone"];
                mark.Text = model.ChgPhone == null ? "" : model.ChgPhone;
            }
            //控制性钻孔深度
            if (doc.Range.Bookmarks["ControlHeight"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ControlHeight"];
                mark.Text = model.ControlHeight + "m";
            }
            //控制性钻孔数量
            if (doc.Range.Bookmarks["ControlNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ControlNumber"];
                mark.Text = model.ControlNumber + "个";
            }
            //合同额
            if (doc.Range.Bookmarks["cpr_Acount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Acount"];
                mark.Text = model.cpr_Acount.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元";
            }
            //合同完成日期
            if (doc.Range.Bookmarks["cpr_DoneDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_DoneDate"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_DoneDate));
            }
            //备注
            if (doc.Range.Bookmarks["cpr_Mark"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Mark"];
                mark.Text = model.cpr_Mark == null ? "" : model.cpr_Mark;
            }
            //建筑分类
            if (doc.Range.Bookmarks["BuildStructType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildStructType"];
                mark.Text = model.BuildStructType == null ? "" : model.BuildStructType;
            }

            //合同名称
            if (doc.Range.Bookmarks["cpr_Name"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                mark.Text = model.cpr_Name == null ? "" : model.cpr_Name;
            }
            //合同编号
            if (doc.Range.Bookmarks["cpr_No"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                mark.Text = model.cpr_No == null ? "" : model.cpr_No;
            }
            //勘察阶段
            if (doc.Range.Bookmarks["cpr_Process"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Process"];
                mark.Text = GetProfession(model.cpr_Process);
            }
            //合同签订时间
            if (doc.Range.Bookmarks["cpr_SignDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_SignDate"];
                mark.Text = model.cpr_SignDate2 == null ? "" : CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_SignDate2));
            }
            //合同签订时间
            if (doc.Range.Bookmarks["cpr_SignDate2"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_SignDate2"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_SignDate));
            }
            //合同开工时间
            if (doc.Range.Bookmarks["cpr_StartDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_StartDate"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_StartDate));
            }
            //合同分类
            if (doc.Range.Bookmarks["cpr_Type"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type"];
                mark.Text = model.cpr_Type == null ? "" : model.cpr_Type;
            }
            //合同类型
            if (doc.Range.Bookmarks["cpr_Type2"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type2"];
                mark.Text = "岩土工程勘察合同";
            }
            //承接部门
            if (doc.Range.Bookmarks["cpr_Unit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                mark.Text = model.cpr_Unit == null ? "" : model.cpr_Unit;
            }
            //建筑类型
            if (doc.Range.Bookmarks["ddcpr_BuildType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ddcpr_BuildType"];
                mark.Text = model.BuildType == null ? "" : model.BuildType;
            }
            //勘察等级
            if (doc.Range.Bookmarks["ddcpr_SurveyClass"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ddcpr_SurveyClass"];
                mark.Text = model.SurveyClass == null ? "" : model.SurveyClass;
            }
            //工程特征
            if (doc.Range.Bookmarks["Features"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Features"];
                mark.Text = model.Features == null ? "" : model.Features;
            }
            //层数
            if (doc.Range.Bookmarks["Floor"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Floor"];
                mark.Text = GetFloor(model.Floor);
            }
            //一般性钻孔深度.
            if (doc.Range.Bookmarks["GenerHeight"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["GenerHeight"];
                mark.Text = model.GenerHeight == null ? "" : model.GenerHeight.ToString() + "m";
            }
            //一般性钻孔数量.
            if (doc.Range.Bookmarks["GenerNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["GenerNumber"];
                mark.Text = model.GenerNumber == null ? "" : model.GenerNumber.ToString() + "个";
            }
            //钻孔深度
            if (doc.Range.Bookmarks["HoleHeight"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["HoleHeight"];
                mark.Text = model.HoleHeight == null ? "" : model.HoleHeight.ToString() + "m";
            }
            //钻孔数量
            if (doc.Range.Bookmarks["HoleNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["HoleNumber"];
                mark.Text = model.HoleNumber == null ? "" : Convert.ToInt32(model.HoleNumber).ToString() + "个";
            }
            //行业性质
            if (doc.Range.Bookmarks["Industry"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Industry"];
                mark.Text = model.Industry == null ? "" : model.Industry;
            }
            //多栋楼也叫地基处理方案
            if (doc.Range.Bookmarks["MultiBuild"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["MultiBuild"];
                mark.Text = model.MultiBuild == null ? "" : model.MultiBuild;
            }
            //勘探点数量
            if (doc.Range.Bookmarks["PointNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["PointNumber"];
                mark.Text = model.PointNumber == null ? "" : Convert.ToInt32(model.PointNumber).ToString() + "个";
            }
            //工程规模
            if (doc.Range.Bookmarks["ProjectArea"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjectArea"];
                mark.Text = model.ProjectArea == null ? "" : Convert.ToDecimal(model.ProjectArea).ToString("f2") + "㎡";
            }
            //工期
            if (doc.Range.Bookmarks["ProjectDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjectDate"];
                mark.Text = model.ProjectDate == null ? "" : Convert.ToInt32(model.ProjectDate).ToString() + "天";
            }
            //实际合同额
            if (doc.Range.Bookmarks["ShijiAcount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ShijiAcount"];
                mark.Text = model.cpr_ShijiAcounnt == null ? "" : Convert.ToDecimal(model.cpr_ShijiAcounnt) + "万元";
            }
            //制表人
            if (doc.Range.Bookmarks["TableMaker"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TableMaker"];
                mark.Text = model.TableMaker == null ? "" : model.TableMaker;
            }
            //总进尺
            if (doc.Range.Bookmarks["TotalLength"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TotalLength"];
                mark.Text = model.TotalLength == null ? "" : Convert.ToDecimal(model.TotalLength).ToString("f2") + "m";
            }
            //地貌单元
            if (doc.Range.Bookmarks["txt_EarthUnit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["txt_EarthUnit"];
                mark.Text = model.EarthUnit == null ? "" : model.EarthUnit;
            }
            //波速试验钻孔深度
            if (doc.Range.Bookmarks["WaveHeight"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["WaveHeight"];
                mark.Text = model.WaveHeight == null ? "" : Convert.ToDecimal(model.WaveHeight).ToString("f2") + "m";
            }
            //波速试验钻孔高度
            if (doc.Range.Bookmarks["WaveNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["WaveNumber"];
                mark.Text = model.WaveNumber == null ? "" : Convert.ToInt32(model.WaveNumber).ToString() + "个";
            }
            //探井深度
            if (doc.Range.Bookmarks["WellsHeight"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["WellsHeight"];
                mark.Text = model.WellsHeight == null ? "" : Convert.ToDecimal(model.WellsHeight).ToString("f2") + "m";
            }
            //探井数量
            if (doc.Range.Bookmarks["WellsNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["WellsNumber"];
                mark.Text = model.WellsNumber == null ? "" : Convert.ToInt32(model.WellsNumber).ToString() + "个";
            }
            //chargeplan  收费计划
            if (doc.Range.Bookmarks["chargeplan"] != null)
            {
                string sqlcharge = @"SELECT * From cm_CoperationChargeType Where paytype='reconn' and cpr_ID=" + Convert.ToInt32(cprid);
                StringBuilder builder = new StringBuilder();
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(sqlcharge).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int index = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        index++;
                        builder.Append(index + "、  ");
                        builder.Append(dr["Times"] + "   ");
                        builder.Append("百分之");
                        builder.Append(dr["persent"] + "   ");
                        builder.Append("金额");
                        builder.Append(dr["payCount"] + "万元   ");
                        builder.Append("收款时间是");
                        builder.Append(Convert.ToDateTime(dr["paytime"]).ToString("yyyy-MM-dd") + "。        ");
                    }
                }
                Bookmark mark = doc.Range.Bookmarks["chargeplan"];
                mark.Text = builder.ToString();
            }
            //childproj 工程子项
            //if (doc.Range.Bookmarks["childproj"] != null)
            //{
            //    string sqlcharge = @"SELECT * From cm_SubCoperation Where cpr_ID=" + Convert.ToInt32(cprid);
            //    StringBuilder builder = new StringBuilder();
            //    DataTable dt = TG.DBUtility.DbHelperSQL.Query(sqlcharge).Tables[0];
            //    if (dt.Rows.Count > 0)
            //    {
            //        int index = 0;
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            index++;
            //            builder.Append(index + "、  ");
            //            builder.Append("子项名称：");
            //            builder.Append(dr["Item_Name"] + "   ");
            //            builder.Append(".面积");
            //            builder.Append(dr["Item_Area"] + "㎡   ");
            //            builder.Append("金额：");
            //            builder.Append(dr["Money"] + "万元   ");
            //            builder.Append("备注：");
            //            builder.Append(dr["Remark"] + "。  ");
            //        }
            //    }
            //    Bookmark mark = doc.Range.Bookmarks["childproj"];
            //    mark.Text = builder.ToString();
            //}
            doc.Save(model.cpr_Name + "-岩土工程勘察合同.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  //保存为doc，并打开
        }

        //根据合同id获得用户id。
        public int GetCstId(int cpr_Id)
        {
            string sql = "select  cst_Id from cm_Coperation where cpr_Id=" + cpr_Id;
            return Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(sql));
        }
        //根据用户id获得用户model
        public TG.Model.cm_CustomerInfo GetConst(int cstid)
        {
            TG.BLL.cm_CustomerInfo dalcust = new TG.BLL.cm_CustomerInfo();
            return dalcust.GetModel(cstid);
        }
        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_ReconnCoperation] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Text = model_cst.Cst_No;
                        this.txtCode.Text = model_cst.Code;
                        this.txtCst_Name.Text = model_cst.Cst_Name;
                        this.txtLinkman.Text = model_cst.Linkman;
                        this.txtCpy_Address.Text = model_cst.Cpy_Address;
                        this.txtCpy_Phone.Text = model_cst.Cpy_Phone;
                        this.txt_JC.Text = model_cst.Cst_Brief;
                        this.txt_Fax.Text = model_cst.Cpy_Fax;
                    }
                }
            }
        }
        //显示合同信息
        protected void ShowCoperation(string cprid)
        {
            TG.BLL.cm_ReconnCoperation bll_cpr = new TG.BLL.cm_ReconnCoperation();
            TG.Model.cm_ReconnCoperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            if (model_cpr != null)
            {
                //合同编号
                this.txtcpr_No.Text = model_cpr.cpr_No == null ? "" : model_cpr.cpr_No.Trim();
                //合同分类
                this.ddcpr_Type.Text = model_cpr.cpr_Type.Trim();
                //this.txt_cprType.Value = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
                //合同名称
                this.txt_cprName.Text = model_cpr.cpr_Name.Trim();
                //勘察等级
                this.ddcpr_SurveyClass.Text = model_cpr.SurveyClass.Trim();
                //建设单位
                this.txt_cprBuildUnit.Text = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
                //占地面积
                this.txt_buildArea.Text = model_cpr.BuildArea.ToString();
                //建筑类型
                this.ddcpr_BuildType.Text = model_cpr.BuildType.Trim();
                //地貌单元
                this.txt_EarthUnit.Text = model_cpr.EarthUnit.Trim();
                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.lbl_upfloor.Text = floors[0].ToString().Trim();
                this.lbl_downfloor.Text = floors[1].ToString().Trim();
                //项目经理
                this.txt_proFuze.Text = model_cpr.ChgPeople.Trim();
                //添加项目经理ID  qpl 20131225
                //this.HiddenPMUserID.Value = model_cpr.PMUserID.ToString();
                //经理电话
                this.txt_fzphone.Text = model_cpr.ChgPhone.Trim();
                //承接部门
                //this.hid_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                this.txt_cjbm.Text = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                //甲方负责人
                this.txtFParty.Text = model_cpr.ChgJia.Trim();
                //甲方负责人电话
                this.txt_jiafphone.Text = model_cpr.ChgJiaPhone.Trim();
                this.ddProjectPosition.Text = model_cpr.BuildPosition.Trim();
                //建筑类别
                //this.drp_buildtype.Items.FindByText(model_cpr.BuildType.Trim()).Selected = true;
                //合同额
                this.txtcpr_Account.Text = model_cpr.cpr_Acount.ToString();
                //实际合同额
                this.txtcpr_Account0.Text = model_cpr.cpr_ShijiAcounnt.ToString();
                //投资额
                //this.txtInvestAccount.Value = model_cpr.cpr_Touzi.ToString();
                ////实际投资额
                //this.txtInvestAccount0.Value = model_cpr.cpr_ShijiTouzi.ToString();
                //建筑分类
                if (model_cpr.BuildStructType != null)
                {
                    this.lbl_BuildStructType.Text = TG.Common.StringPlus.ResolveStructString(model_cpr.BuildStructType.ToString());
                }
                //行业性质
                if (model_cpr.Industry.Trim() != "" && model_cpr.Industry.Trim() != "-1")
                {
                    this.ddProfessionType.Text = model_cpr.Industry.Trim();
                }

                //钻孔数量
                this.txt_HoleNumber.Text = model_cpr.HoleNumber == null ? "" : model_cpr.HoleNumber.ToString();
                //钻孔深度
                this.txt_HoleHeight.Text = model_cpr.HoleHeight == null ? "" : model_cpr.HoleHeight.ToString();
                //建筑物数量、
                this.txt_BuildNumber.Text = model_cpr.BuildNumber == null ? "" : model_cpr.BuildNumber.ToString();
                //探井数量
                this.txt_WellsNumber.Text = model_cpr.WellsNumber == null ? "" : model_cpr.WellsNumber.ToString();
                //探井深度
                this.txt_WellsHeight.Text = model_cpr.WellsHeight == null ? "" : model_cpr.WellsHeight.ToString();
                //总进尺
                this.txt_TotalLength.Text = model_cpr.TotalLength == null ? "" : model_cpr.TotalLength.ToString();
                string result = "";
                if (model_cpr.cpr_Process.Trim() != "")
                {
                    string[] array = model_cpr.cpr_Process.Split(new char[] { ',' }, StringSplitOptions.None);
                    TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                    for (int i = 0; i < array.Length; i++)
                    {
                        result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                    }
                    result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
                }
                this.ddcpr_Stage.Text = result;
                //工程来源
                if (model_cpr.BuildSrc.Trim() != "" && model_cpr.BuildSrc.Trim() != "-1")
                {
                    this.ddSourceWay.Text = model_cpr.BuildSrc.Trim();
                }
                //合同签订日期
                this.txtSingnDate.Text = Convert.ToDateTime(model_cpr.cpr_SignDate).ToString("yyyy-MM-dd");
                this.txtSingnDate2.Text = model_cpr.cpr_SignDate2 == null ? "" : Convert.ToDateTime(model_cpr.cpr_SignDate2).ToString("yyyy-MM-dd");
                //合同开工日期
                this.txtStartDate.Text = model_cpr.cpr_StartDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_StartDate).ToString("yyyy-MM-dd");
                //合同完成日期
                this.txtCompleteDate.Text = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToString("yyyy-MM-dd");
                //制表人
                this.txt_tbcreate.Text = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
                //勘探点数量
                this.txt_PointNumber.Text = model_cpr.PointNumber == null ? "" : model_cpr.PointNumber.ToString();
                //波速试验钻孔数量及深度
                this.txt_WaveNumber.Text = model_cpr.WaveNumber == null ? "" : model_cpr.WaveNumber.ToString();
                this.txt_WaveHeight.Text = model_cpr.WaveHeight == null ? "" : model_cpr.WaveHeight.ToString();
                //控制性钻孔数量及深度
                this.txt_ControlNumber.Text = model_cpr.ControlNumber == null ? "" : model_cpr.ControlNumber.ToString();
                this.txt_ControlHeight.Text = model_cpr.ControlHeight == null ? "" : model_cpr.ControlHeight.ToString();
                //一般性钻孔数量和深度
                this.txt_GenerNumber.Text = model_cpr.GenerNumber == null ? "" : model_cpr.GenerNumber.ToString();
                this.txt_GenerHeight.Text = model_cpr.GenerHeight == null ? "" : model_cpr.GenerHeight.ToString();
                //工程规模
                this.txt_ProjectArea.Text = model_cpr.ProjectArea == null ? "" : model_cpr.ProjectArea.ToString();
                //工程特征
                this.txt_Features.Text = model_cpr.Features == null ? "" : model_cpr.Features.ToString();
                //工期
                this.txt_ProjectDate.Text = model_cpr.ProjectDate == null ? "" : model_cpr.ProjectDate.ToString();
                //多栋楼--拟采用基础选型及地基处理方案
                this.txt_multibuild.Text = model_cpr.MultiBuild == null ? "" : model_cpr.MultiBuild.Trim();
                //备注
                this.txtcpr_Remark.Text = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
            }
            //this.txt_area.Text = model_cpr.cpr_Area;
        }
        //  转换地上和地下楼层数
        protected string GetFloor(string floor)
        {
            string[] floors = floor.Split('|');
            if (floors.Length > 0)
            {
                return "地上:" + floors[0] + "地下:" + floors[1];
            }
            else
            {
                return "";
            }
        }
        protected string GetProfession(string num)
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_kcjd'";

            List<TG.Model.cm_Dictionary> JDList = bll_dic.GetModelList(str_where);
            string result = "";
            string[] str = num.Split(',');
            if (str.Length > 1)
            {
                for (int i = 0; i < str.Length; i++)
                {
                    foreach (TG.Model.cm_Dictionary item in JDList)
                    {
                        if (str[i].Trim() == item.ID.ToString())
                        {
                            result += item.dic_Name + "，";
                        }
                    }
                }
                result = result.Substring(0, result.Length - 1);
            }
            foreach (TG.Model.cm_Dictionary item in JDList)
            {
                if (num.Trim() == item.ID.ToString())
                {
                    result = item.dic_Name;
                }
                else
                {
                    result += "";
                }
            }
            //switch (num)
            //{
            //    case "-1":
            //        result = "";
            //        break;
            //    case "27":
            //        result = "方案";
            //        break;
            //    case "28":
            //        result = "初设";
            //        break;
            //    case "29":
            //        result = "施工图";
            //        break;
            //    case "30":
            //        result = "其他";
            //        break;
            //    case "31":
            //        result = "公开招标";
            //        break;
            //    case "32":
            //        result = "邀请招标";
            //        break;
            //    case "36":
            //        result = "计算机行业";
            //        break;
            //    case "33":
            //        result = "自行委托";
            //        break;
            //    case "37":
            //        result = "教育行业";
            //        break;
            //    case "38":
            //        result = "建筑行业";
            //        break;
            //    case "47":
            //        result = "科教行业";
            //        break;
            //    case "34":
            //        result = "普通客户";
            //        break;
            //    case "35":
            //        result = "VIP客户";
            //        break;
            //    case "39":
            //        result = "一般";
            //        break;
            //    case "40":
            //        result = "密切";
            //        break;
            //    case "41":
            //        result = "很密切";
            //        break;
            //    case "43":
            //        result = "一级";
            //        break;
            //    case "44":
            //        result = "二级";
            //        break;
            //    case "45":
            //        result = "三级";
            //        break;
            //    case "46":
            //        result = "四级";
            //        break;
            //    default:
            //        result += "";
            //        break;
            //}
            return result;
        }
    }
}