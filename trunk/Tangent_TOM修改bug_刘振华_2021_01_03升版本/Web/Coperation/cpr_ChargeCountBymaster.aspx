﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_ChargeCountBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_ChargeCountBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script type="text/javascript">

        $(function () {
            //年份选择
            $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
                var index = $(this).val();
                if (index != "-1" && $("#ctl00_ContentPlaceHolder1_drpMonth").val() == "0" && $("#ctl00_ContentPlaceHolder1_drpJidu").val() == "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
                else if (index == "-1") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                }
            });
            //季度选择
            $("#ctl00_ContentPlaceHolder1_drpJidu").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                }
            });
            //月份选择
            $("#ctl00_ContentPlaceHolder1_drpMonth").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
            });
            //时间段
            $("#ctl00_ContentPlaceHolder1_cbx_time").click(function () {
                if($(this).is(":checked"))
                {                    
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().hide();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                  //  $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();
                   // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();
                   // $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "");
                    //时间段选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("1");

                }
                else
                {
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().show();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                   // $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().show();
                   // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().show();
                  //  $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "none");
                    //时间段没有选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("0");

                }
            });
            //初始化数据显示
            Init();
        });
        function Init()
        {                       
            //时间段隐藏域的值
            if ($("#ctl00_ContentPlaceHolder1_hid_time").val() == "1") {
                //隐藏年
                $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drp_year").parent().hide();
    
                //隐藏季度
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();

                //隐藏月份
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();

                //显示合同时间段
                $("#ctl00_ContentPlaceHolder1_cbx_time").parents("td").prev().css("display", "");
            }
            if($("#ctl00_ContentPlaceHolder1_drp_year").val()=="-1")
            {
                $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同目标额统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>收费管理</a><i class="fa fa-angle-right">
    </i><a>合同目标额统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询合同目标额
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:   </td>
                            <td>
                                <cc1:ASDropDownTreeView ID="drpunit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全体部门-------" />
                                        </td>
                            <td>合同年份:    </td>
                            <td>
                                <asp:DropDownList ID="drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="year">季度：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpJidu" runat="server">
                                    <asp:ListItem Value="0">--全部--</asp:ListItem>
                                    <asp:ListItem Value="1" Text="一季度"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二季度"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三季度"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四季度"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="year">月份：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpMonth" runat="server">
                                    <asp:ListItem Value="0" Text="--全部--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="一月"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二月"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三月"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四月"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="五月"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="六月"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="七月"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="八月"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="九月"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="十月"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="十一月"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="十二月"></asp:ListItem>
                                </asp:DropDownList>

                            </td>   
                             <td style="display:none;">
                                <div class="input-group">
                                    合同时间：
                                    <input type="text" name="txt_year1" id="txt_year1" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />至<input
                                            type="text" name="txt_year2" id="txt_year2" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />
                                </div>
                            </td>                        
                            <td>
                                <div class="input-group">
                                    <input type="checkbox" id="cbx_time" runat="server"/>时间段
                                    <input type="hidden" id="hid_time" value="0" runat="server"/>
                                </div>
                            </td>
                            
                            <td>收费时间：    </td>
                            <td>
                                <input type="text" name="txt_date2" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                    class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />至<input
                                        type="text" name="txt_date1" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />
                            </td>
                            <td>
                                 <asp:Button Text="查询" runat="server" CssClass="btn blue" ID="btn_search" OnClick="btn_search_Click" />
                              
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同目标额统计
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button Text="导出Excel" runat="server" CssClass="btn red btn-sm" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cls_Container_Report">
                                <div class="cls_Container_Tip">
                                    <p class="bg-purper">
                                        <%= ChartTip %>合同综合目标值统计与<%= ChargeStartTime%>到<%=
        ChargeEndTime%>合同收款统计
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" CssClass="table table-bordered table-grid" runat="server" AutoGenerateColumns="False" Font-Size="11pt"
                                    EnableModelValidation="True" Border="0">
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="cpryear" HeaderText="合同年份">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="cprTarget" HeaderText="合同目标值(万元)">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="cprAllCount" HeaderText="已签合同额(万元)">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Cprprt" HeaderText="完成比例">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Starttime" HeaderText="收费开始时间">
                                            <ItemStyle Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="EndTime" HeaderText="收费结束时间">
                                            <ItemStyle Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AllotTarget" HeaderText="目标产值(万元)">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AllotCount" HeaderText="完成收费(万元)">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AllotPrt" HeaderText="完成比例">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundField>
                                       
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hiddenischeckallpower" name="name" value="<%= IsCheckAllPower %>" />
</asp:Content>
