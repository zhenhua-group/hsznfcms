﻿using AjaxPro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Coperation
{
    public partial class CoperationAuditEditConfigBymaster : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return base.IsAuth;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(CoperationAuditEditConfigBymaster));
        }
    }
}