﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using InfoSoftGlobal;

namespace TG.Web.Coperation
{
    public partial class cpr_ChargeAndCoperationRatioListBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitDropDownByYear();
                InitDropDownByMonth();
                BindUnit();
                BindPreviewPower();
                this.hidnowmonth.Value = DateTime.Now.Month.ToString();

                this.labTime.Text = DateTime.Now.ToString("yyyy年MM月dd日") + "制表";
                GetTuAboutCharge();
                GetTuAboutCoper();
            }
            else
            {
                GetTuAboutCharge();
                GetTuAboutCoper();
            }
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
        /// <summary>
        ///初始年份
        /// </summary>
        protected void InitDropDownByYear()
        {
            this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
        }
        /// <summary>
        ///初始月份
        /// </summary>
        protected void InitDropDownByMonth()
        {
            this.drp_month.Items.FindByText(DateTime.Now.AddMonths(-1).Month.ToString()).Selected = true;
            this.drp_month2.Items.FindByText(DateTime.Now.Month.ToString()).Selected = true;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        protected void btn_report_Click(object sender, EventArgs e)
        {
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string title = year + "年（" + month + "月-" + DateTime.Now.Month + "月）【收费与合同额】环比表";

            ToExcel(title);
        }
        public void ToExcel(string title)
        {
            TG.BLL.ChargeAndCoper chargebll = new BLL.ChargeAndCoper();

            string unitId = this.drp_unit.SelectedItem.Value;
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string endmonth = this.drp_month2.SelectedItem.Value;
            string beginTime = month + "-01";
            string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
            TG.BLL.CopContrast bll = new BLL.CopContrast();
            List<TG.Model.ChargeAndCoper> listSC = chargebll.GetList(unitId, year, beginTime, endTime, endmonth);

            ExportDataToExcel(listSC, "~/TemplateXls/cpr_ChargeAndCoperationRatioList.xls", year, month);
            //获取excel的文件名称（Guid是一个全球表示，使excel的文件名不同）


        }
        private void ExportDataToExcel(List<TG.Model.ChargeAndCoper> listSC, string modelPath, string year, string month)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            ws.GetRow(0).GetCell(0).SetCellValue(this.drp_year.SelectedItem.Value + "年（" + this.drp_month.SelectedItem.Value + "月-" + DateTime.Now.Month + "月)【收费与合同额】环比表");
            ws.GetRow(1).GetCell(6).SetCellValue(DateTime.Now.ToString("yyyy年-MM月-dd日") + "制表");
            ws.GetRow(3).GetCell(1).SetCellValue(int.Parse(year) + "年");
            ws.GetRow(3).GetCell(2).SetCellValue(int.Parse(year) + "年");
            ws.GetRow(3).GetCell(4).SetCellValue(int.Parse(year) + "年");
            ws.GetRow(3).GetCell(5).SetCellValue(int.Parse(year) + "年");

            ws.GetRow(4).GetCell(1).SetCellValue(int.Parse(month) + "月");
            ws.GetRow(4).GetCell(2).SetCellValue(DateTime.Now.Month + "月");
            ws.GetRow(4).GetCell(4).SetCellValue(int.Parse(month) + "月");
            ws.GetRow(4).GetCell(5).SetCellValue(DateTime.Now.Month + "月");
            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 5;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            if (listSC.Count > 0)
            {
                foreach (TG.Model.ChargeAndCoper item in listSC)
                {
                    //     Unit  Frsitmonthcharge Secondmonthcharge ContrastCharge Frsitmonthcoper Secondmonthcoper ContrastCoper UnitAllotLast

                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.Unit);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(Convert.ToDouble(item.Frsitmonthcharge));
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(Convert.ToDouble(item.Secondmonthcharge));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.ContrastCharge);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.Frsitmonthcoper)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(Convert.ToDouble(item.Secondmonthcoper)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.ContrastCoper); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.UnitAllotLast); cell7.CellStyle = style;
                    //ICell cell8 = row.CreateCell(8);
                    //cell8.SetCellValue(item.AllotUnitLast); cell8.CellStyle = style;

                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(this.drp_year.SelectedItem.Value + "年（" + this.drp_month.SelectedItem.Value + "月-" + DateTime.Now.Month + "月)【收费与合同额】环比表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        private void GetTuAboutCoper()
        {
            string unitId = this.drp_unit.SelectedItem.Value;
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string endmonth = this.drp_month2.SelectedItem.Value;
            string beginTime = month + "-01";
            string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
            TG.BLL.ChargeAndCoper bll = new BLL.ChargeAndCoper();
            if (unitId == "-1")
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    //
                    unitId = base.UserUnitNo.ToString();
                }
            }
            List<TG.Model.ChargeAndCoper> list = bll.GetList(unitId, year, beginTime, endTime, endmonth);
            StringBuilder xmlData1 = new StringBuilder();
            xmlData1.Append("<chart caption='" + year + "年" + month + "-" + DateTime.Now.Month.ToString() + "月【合同额环比表】' numberPrefix='' formatNumberScale='0' rotateValues='1' placeValuesInside='1' decimals='0' numberSuffix='万元' baseFontSize='12' xAxisName='单位' yAxisName='金额'><categories>");
            foreach (TG.Model.ChargeAndCoper item in list)
            {
                xmlData1.Append("<category name='" + item.Unit + "' />");
            }
            xmlData1.Append("</categories>");
            xmlData1.Append("<dataset seriesName='" + month + "月'>");
            foreach (TG.Model.ChargeAndCoper item in list)
            {
                xmlData1.Append("<set value='" + item.Frsitmonthcoper + "' />");
            }
            xmlData1.Append("</dataset>");
            xmlData1.Append("<dataset seriesName='" + DateTime.Now.Month + "月'>");
            foreach (TG.Model.ChargeAndCoper item in list)
            {
                xmlData1.Append("<set value='" + item.Secondmonthcoper + "' />");
            }
            xmlData1.Append("</dataset>");
            xmlData1.Append("</chart>");
            Literal5.Text = FusionCharts.RenderChart("../js/FunctionChart/MSColumn2D.swf", "", xmlData1.ToString(), "productSalesCop", "100%", "500", false, true);
        }
        private void GetTuAboutCharge()
        {
            string unitId = this.drp_unit.SelectedItem.Value;
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string endmonth = this.drp_month2.SelectedItem.Value;
            string beginTime = month + "-01";
            string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
            TG.BLL.ChargeAndCoper bll = new BLL.ChargeAndCoper();
            if (unitId == "-1")
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    //
                    unitId = base.UserUnitNo.ToString();
                }
            }
            List<TG.Model.ChargeAndCoper> list = bll.GetList(unitId, year, beginTime, endTime, endmonth);


            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<chart caption='" + year + "年" + month + "-" + DateTime.Now.Month.ToString() + "月【收费环比表】' numberPrefix='' formatNumberScale='0' rotateValues='1' placeValuesInside='1' decimals='0' numberSuffix='万元' baseFontSize='12' xAxisName='' yAxisName=''><categories>");
            foreach (TG.Model.ChargeAndCoper item in list)
            {
                xmlData.Append("<category name='" + item.Unit + "' />");
            }
            xmlData.Append("</categories>");
            xmlData.Append("<dataset seriesName='" + month + "'>");
            foreach (TG.Model.ChargeAndCoper item in list)
            {
                xmlData.Append("<set value='" + item.Frsitmonthcharge + "' />");
            }
            xmlData.Append("</dataset>");
            xmlData.Append("<dataset seriesName='" + DateTime.Now.Month + "'>");
            foreach (TG.Model.ChargeAndCoper item in list)
            {
                xmlData.Append("<set value='" + item.Secondmonthcharge + "' />");
            }
            xmlData.Append("</dataset>");
            xmlData.Append("</chart>");
            Literal4.Text = FusionCharts.RenderChart("../js/FunctionChart/MSColumn2D.swf", "", xmlData.ToString(), "productSalesCharge", "100%", "300", false, true);



        }
    }
}