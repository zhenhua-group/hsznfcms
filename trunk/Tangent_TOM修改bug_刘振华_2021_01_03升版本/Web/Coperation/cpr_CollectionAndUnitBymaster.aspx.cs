﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Text;

namespace TG.Web.Coperation
{
    public partial class cpr_CollectionAndUnitBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitDropDownByYear();
                InitDropDownByMonth();
            }
        }
        /// <summary>
        ///初始年份
        /// </summary>
        protected void InitDropDownByYear()
        {
            this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
        }
        /// <summary>
        ///初始月份
        /// </summary>
        protected void InitDropDownByMonth()
        {
            this.drp_month.Items.FindByText(DateTime.Now.Month.ToString()).Selected = true;
        }
        protected void btn_report_Click(object sender, EventArgs e)
        {
            string title = this.drp_year.SelectedItem.Value + "年" + this.drp_month.SelectedItem.Value + "财务报表";
            ToExcel(title);
        }
        public void ToExcel(string title)
        {
            TG.BLL.StandBookBp standbll = new BLL.StandBookBp();


            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;

            TG.BLL.CopContrast bll = new BLL.CopContrast();
            string beginmonth = year + "-" + month + "-01";
            string endmonth = year + "-" + month + "-" + TG.Common.TimeParser.GetMonthLastDate(int.Parse(year), int.Parse(month));
            List<TG.Model.Collection> listSC = standbll.GetListCollection(beginmonth, endmonth);

            ExportDataToExcel(listSC, "~/TemplateXls/cpr_CollectionAndUnit.xls", year, month);
            //获取excel的文件名称（Guid是一个全球表示，使excel的文件名不同）


        }
        private void ExportDataToExcel(List<TG.Model.Collection> listSC, string modelPath, string year, string month)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            ws.GetRow(0).GetCell(0).SetCellValue(this.drp_year.SelectedItem.Value + "年" + month + "月财务报表");
            //ws.GetRow(1).GetCell(6).SetCellValue(DateTime.Now.ToString("yyyy年-MM月-dd日") + "制表");
            //ws.GetRow(3).GetCell(1).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(2).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(4).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(5).SetCellValue(int.Parse(year) + "年");

            //ws.GetRow(4).GetCell(1).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(2).SetCellValue(DateTime.Now.Month + "月");
            //ws.GetRow(4).GetCell(4).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(5).SetCellValue(DateTime.Now.Month + "月");
            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 2;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            // ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            if (listSC.Count > 0)
            {
                int i = 0;
                foreach (TG.Model.Collection item in listSC)
                {
                    //     month  XX CprNo Unit BuildUnit Acount FourthTime FifthTime Mark
                    i++;
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(month);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.CprNo);
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.Unit + "预收" + item.BuildUnit);
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(Convert.ToDouble(item.Acount));
                    cell3.CellStyle = style;
                    //ICell cell4 = row.CreateCell(4);
                    //cell4.SetCellValue(item.FristTime); cell4.CellStyle = style;
                    //ICell cell5 = row.CreateCell(5);
                    //cell5.SetCellValue(item.SecondTime); cell5.CellStyle = style;
                    //ICell cell6 = row.CreateCell(6);
                    //cell6.SetCellValue(item.ThirdTime); cell6.CellStyle = style;
                    //ICell cell7 = row.CreateCell(7);
                    //cell7.SetCellValue(item.FourthTime); cell7.CellStyle = style;
                    //ICell cell8 = row.CreateCell(8);
                    //cell8.SetCellValue(item.FifthTime); cell8.CellStyle = style;
                    //ICell cell9 = row.CreateCell(9);
                    //cell9.SetCellValue(item.Mark); cell9.CellStyle = style;
                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(this.drp_year.SelectedItem.Value + "年" + month + "月财务报表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}