﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words;

namespace TG.Web.Coperation
{
    public partial class ShowSingleCoperationForReportBymaster : PageBase
    {
        //合同报备ID
        protected int CoperationBakID
        {
            get
            {
                int cprSysNo = 0;
                int.TryParse(Request["cprid"], out cprSysNo);
                return cprSysNo;
            }
        }
        //客户ID
        protected int CustomerID
        {
            get
            {
                int cstSysNo = 0;
                int.TryParse(Request["cstid"], out cstSysNo);
                return cstSysNo;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string cprid = Request.QueryString["cprid"];

            if (!IsPostBack)
            {
                //绑定联系人
                ShowContractInfo();
                //合同报备信息
                ShowCoperationBack();
            }
            else
            {
                OutputCopback();
            }
        }

        private void OutputCopback()
        {
            TG.Model.cm_Coperation_back model = new TG.BLL.cm_Coperation_back().GetModel(CoperationBakID);
            TG.Model.cm_CustomerInfo modelconst = new TG.BLL.cm_CustomerInfo().GetModel(CustomerID);
            // TG.Common.ReportWord report = new Common.ReportWord();
            string path = Server.MapPath(@"~\TemplateWord\CoperationbackReport.doc");
            Aspose.Words.Document doc = new Aspose.Words.Document(path); //载入模板


            // report.InsertValue("proapp", model.proapproval == 0 ? "是" : "否");
            if (doc.Range.Bookmarks["proapproval"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["proapproval"];
                mark.Text = model.proapproval == 0 ? "是" : "否";
            }
            //  report.InsertValue("cpr_Name", model.cpr_Name);
            if (doc.Range.Bookmarks["cpr_Name"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                mark.Text = model.cpr_Name == null ? "" : model.cpr_Name;
            }
            //  report.InsertValue("BuildArea", model.BuildArea);
            if (doc.Range.Bookmarks["BuildArea"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildArea"];
                mark.Text = model.BuildArea == null ? "" : model.BuildArea + "㎡";
            }
            // report.InsertValue("BuildPosition", model.BuildPosition);
            if (doc.Range.Bookmarks["BuildPosition"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildPosition"];
                mark.Text = model.BuildPosition == null ? "" : model.BuildPosition;
            }
            // report.InsertValue("BuildSrc", model.BuildSrc);
            if (doc.Range.Bookmarks["BuildSrc"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildSrc"];
                mark.Text = GetProfession(model.BuildSrc);
            }
            //  report.InsertValue("BuildStructType", model.BuildStructType);
            if (doc.Range.Bookmarks["BuildStructType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildStructType"];
                mark.Text = model.BuildStructType == null ? "" : model.BuildStructType;
            }
            //   report.InsertValue("BuildType", model.BuildType);
            if (doc.Range.Bookmarks["BuildType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildType"];
                mark.Text = model.BuildType == null ? "" : model.BuildType;
            }
            //   report.InsertValue("BuildUnit", model.cpr_Mark);
            if (doc.Range.Bookmarks["BuildUnit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                mark.Text = model.cpr_Mark == null ? "" : model.cpr_Mark;
            }
            // report.InsertValue("ChgJiaPhone", model.ChgJiaPhone);
            if (doc.Range.Bookmarks["ChgJiaPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJiaPhone"];
                mark.Text = model.ChgJiaPhone == null ? "" : model.ChgJiaPhone;
            }
            //  report.InsertValue("ChgPeople", model.ChgPeople);
            if (doc.Range.Bookmarks["ChgPeople"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPeople"];
                mark.Text = model.ChgPeople == null ? "" : model.ChgPeople;
            }
            //report.InsertValue("ChgJia", model.ChgJia);
            if (doc.Range.Bookmarks["ChgJia"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJia"];
                mark.Text = model.ChgJia == null ? "" : model.ChgJia;
            }
            //report.InsertValue("ChgPhone", model.ChgPhone);
            if (doc.Range.Bookmarks["ChgPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPhone"];
                mark.Text = model.ChgPhone == null ? "" : model.ChgPhone;
            }
            // report.InsertValue("cpr_Address", model.cpr_Address);
            if (doc.Range.Bookmarks["cpr_Address"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Address"];
                mark.Text = model.cpr_Address == null ? "" : model.cpr_Address;
            }
            // report.InsertValue("jiapeople", model.ChgJia);
            if (doc.Range.Bookmarks["ChgJia"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJia"];
                mark.Text = model.ChgJia == null ? "" : model.ChgJia;
            }
            // report.InsertValue("jiaphone", model.ChgJiaPhone);
            if (doc.Range.Bookmarks["ChgJiaPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJiaPhone"];
                mark.Text = model.ChgJiaPhone == null ? "" : model.ChgJiaPhone;
            }
            //  report.InsertValue("cpr_Type", model.cpr_Type);
            if (doc.Range.Bookmarks["cpr_Type"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type"];
                mark.Text = model.cpr_Type == null ? "" : model.cpr_Type;
            }
            //  report.InsertValue("cpr_Acount", model.cpr_Acount.ToString() + "万元");
            if (doc.Range.Bookmarks["cpr_Acount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Acount"];
                mark.Text = model.cpr_Acount.ToString() + "万元";
            }
            //  report.InsertValue("cpr_Touzi", model.cpr_Touzi.ToString() + "万元");
            if (doc.Range.Bookmarks["cpr_Touzi"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Touzi"];
                mark.Text = model.cpr_Touzi.ToString() + "万元";
            }
            // report.InsertValue("cpr_Type2", model.cpr_Type2);
            if (doc.Range.Bookmarks["cpr_Type2"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type2"];
                mark.Text = model.cpr_Type2 == null ? "" : model.cpr_Type2;
            }
            //report.InsertValue("cpr_ShijiAcount", model.cpr_ShijiAcount.ToString() + "万元");
            if (doc.Range.Bookmarks["cpr_ShijiAcount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_ShijiAcount"];
                mark.Text = model.cpr_ShijiAcount.ToString() + "万元";
            }
            //   report.InsertValue("cpr_ShijiTouzi", model.cpr_ShijiTouzi.ToString() + "万元");
            if (doc.Range.Bookmarks["cpr_ShijiTouzi"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_ShijiTouzi"];
                mark.Text = model.cpr_ShijiTouzi.ToString() + "万元";
            }
            //  report.InsertValue("cpr_Unit", model.cpr_Unit);
            if (doc.Range.Bookmarks["cpr_Unit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                mark.Text = model.cpr_Unit == null ? "" : model.cpr_Unit;
            }
            //   report.InsertValue("Floor", GetFloor(model.Floor));
            if (doc.Range.Bookmarks["Floor"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Floor"];
                mark.Text = GetFloor(model.Floor);
            }
            //   report.InsertValue("Industry", model.Industry);
            if (doc.Range.Bookmarks["Industry"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Industry"];
                mark.Text = model.Industry == null ? "" : model.Industry;
            }
            //  report.InsertValue("MultiBuild", model.MultiBuild);
            if (doc.Range.Bookmarks["MultiBuild"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["MultiBuild"];
                mark.Text = model.MultiBuild == null ? "" : model.MultiBuild;
            }
            //report.InsertValue("StructType", model.StructType);
            if (doc.Range.Bookmarks["StructType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["StructType"];
                mark.Text = model.StructType == null ? "" : model.StructType;
            }
            //report.InsertValue("RegTime", TG.Common.CommCoperation.GetEasyTime(Convert.ToDateTime(model.RegTime)));
            if (doc.Range.Bookmarks["RegTime"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["RegTime"];
                mark.Text = TG.Common.CommCoperation.GetEasyTime(Convert.ToDateTime(model.RegTime));
            }
            if (doc.Range.Bookmarks["cpr_Mark"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Mark"];
                mark.Text = model.cpr_Mark == null ? "" : model.cpr_Mark;
            }
            doc.Save(model.cpr_Name + ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  //保存为doc，并打开
        }
        // 绑定报备合同
        private void ShowCoperationBack()
        {
            TG.Model.cm_Coperation_back model = new TG.BLL.cm_Coperation_back().GetModel(CoperationBakID);
            //合同名
            this.txt_cprName.Text = model.cpr_Name.Trim();
            //合同类型
            this.ddcpr_Type.Text = model.cpr_Type.Trim();
            //合同类别  
            this.txt_cprType.Text = model.cpr_Type2 ?? "";
            //建筑分类
            this.txt_buildType.Text = model.BuildType.Trim();
            //合同额
            this.txtcpr_Account.Text = model.cpr_Acount.ToString();
            //实际合同额
            this.txtcpr_Account0.Text = model.cpr_ShijiAcount.ToString();
            //投资额
            this.txtInvestAccount.Text = model.cpr_Touzi.ToString();
            //实际投资额
            this.txtInvestAccount0.Text = model.cpr_ShijiTouzi.ToString();
            //备注
            this.txtcpr_Remark.Value = (model.cpr_Mark == null) ? "" : model.cpr_Mark.Trim();
            //建筑规模
            this.txt_buildArea.Text = Convert.ToString(model.BuildArea ?? "").Trim();
            //甲方
            this.txt_proFuze.Text = model.ChgPeople.Trim();
            this.txt_fzphone.Text = model.ChgPhone.Trim();
            //负责人
            this.txtFParty.Text = model.ChgJia ?? "";
            this.txt_jiafphone.Text = model.ChgJiaPhone ?? "";
            this.ddProjectPosition.Text = model.BuildPosition.Trim();
            this.ddProfessionType.Text = model.Industry.Trim();
            this.ddSourceWay.Text = GetProfession(model.BuildSrc ?? "");
            this.drp_proapproval.Text = model.proapproval == 0 ? "是" : "否";
            //承接部门
            this.txt_cjbm.Text = (model.cpr_Unit == null) ? "" : model.cpr_Unit.Trim();
            if (model.StructType != null)
            {
                this.lbl_StructType.Text = TG.Common.StringPlus.ResolveStructString(model.StructType.ToString());
            }
            //建筑分类
            if (model.BuildStructType != null)
            {
                this.lbl_BuildStructType.Text = TG.Common.StringPlus.ResolveStructString(model.BuildStructType.ToString());
            }
            string[] textArray2 = model.Floor.Split('|');
            this.txt_upfloor.Text = textArray2[0].ToString().Trim();
            this.txt_downfloor.Text = textArray2[1].ToString().Trim();
            this.txt_cprAddress.Text = model.cpr_Address.ToString();
            this.txtcpr_recorpt.Value = (model.MultiBuild == null) ? "" : model.MultiBuild.ToString();
        }
        //显示联系人
        protected void ShowContractInfo()
        {
            TG.Model.cm_CustomerInfo model_cst = new TG.BLL.cm_CustomerInfo().GetModel(CustomerID);
            if (model_cst != null)
            {
                this.txtCst_No.Text = model_cst.Cst_No;
                this.txtCode.Text = model_cst.Code;
                this.txtCst_Name.Text = model_cst.Cst_Name;
                this.txtLinkman.Text = model_cst.Linkman;
                this.txtCpy_Address.Text = model_cst.Cpy_Address;
                this.txtCpy_Phone.Text = model_cst.Cpy_Phone;
                this.txt_JC.Text = model_cst.Cst_Brief;
                this.txt_Fax.Text = model_cst.Cpy_Fax;
            }
        }
        //分割楼层
        protected string GetFloor(string floor)
        {
            string[] floors = floor.Split('|');
            if (floors.Length > 0)
            {
                return "地上:" + floors[0] + "层，地下:" + floors[1] + "层";
            }
            else
            {
                return "";
            }
        }
        protected string GetProfession(string num)
        {
            string result = "";
            switch (num)
            {
                case "-1":
                    result = "";
                    break;
                case "27":
                    result = "方案";
                    break;
                case "28":
                    result = "初设";
                    break;
                case "29":
                    result = "施工图";
                    break;
                case "30":
                    result = "其他";
                    break;
                case "31":
                    result = "公开招标";
                    break;
                case "32":
                    result = "邀请招标";
                    break;
                case "36":
                    result = "计算机行业";
                    break;
                case "33":
                    result = "自行委托";
                    break;
                case "37":
                    result = "教育行业";
                    break;
                case "38":
                    result = "建筑行业";
                    break;
                case "47":
                    result = "科教行业";
                    break;
                case "34":
                    result = "普通客户";
                    break;
                case "35":
                    result = "VIP客户";
                    break;
                case "39":
                    result = "一般";
                    break;
                case "40":
                    result = "密切";
                    break;
                case "41":
                    result = "很密切";
                    break;
                case "43":
                    result = "一级";
                    break;
                case "44":
                    result = "二级";
                    break;
                case "45":
                    result = "三级";
                    break;
                case "46":
                    result = "四级";
                    break;
                default:
                    result = "";
                    break;
            }
            return result;
        }

    }
}