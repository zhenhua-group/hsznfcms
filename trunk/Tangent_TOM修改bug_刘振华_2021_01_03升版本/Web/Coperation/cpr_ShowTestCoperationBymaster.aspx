﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="cpr_ShowTestCoperationBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_ShowTestCoperationBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/Coperation/cpr_ShowTestCoperationBymaster.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>测量及沉降观测合同信息查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right">
    </i><a>测量及沉降观测合同信息查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>测量及沉降观测合同信息查看
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">客户信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 13%">客户编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCst_No" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%">客户简称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_JC" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>客户名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCst_Name" runat="server"></asp:Label>
                                        </td>
                                        <td>公司地址:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCpy_Address" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>邮政编码:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCode" runat="server"></asp:Label>
                                        </td>
                                        <td>联系人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtLinkman" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>公司电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCpy_Phone" runat="server"></asp:Label>
                                        </td>
                                        <td>传真号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_Fax" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                    <h4 class="form-section">合同信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td>合同编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_No" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_cprid" runat="server" />
                                        </td>
                                        <td>合同分类:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddcpr_Type" runat="Server">
                                            </asp:Label>
                                        </td>
                                        <td>合同类型:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_cprType" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同名称:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txt_cprName" runat="server"></asp:Label>
                                        </td>
                                        <td>建筑类型:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_buildType" runat="Server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设单位:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txt_cprBuildUnit" runat="server"></asp:Label>
                                        </td>
                                        <td>测量占地面积:</td>
                                        <td>
                                            <asp:Label ID="txt_buildArea" runat="Server">
                                            </asp:Label>㎡
                                        </td>
                                    </tr>
                                    <%--   <tr>
                                        <td>
                                            结构形式:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="lbl_StructType" runat="server"></asp:Literal></div>
                                        </td>
                                        <td>
                                            建筑分类:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="lbl_BuildStructType" runat="server"></asp:Literal></div>
                                        </td>
                                        </tr>--%>

                                    <tr>
                                        <td>工程负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_proFuze" runat="server" Width="120px"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_fzphone" runat="server"></asp:Label>
                                        </td>
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_cjbm" runat="server" Width="120px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtFParty" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_jiafphone" runat="server" Width="120px"></asp:Label>
                                        </td>
                                        <td>工程地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProjectPosition" runat="Server" Width="60px">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_Account" runat="server"></asp:Label>
                                            万元
                                        </td>
                                        <td>实际合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_Account0" runat="server"></asp:Label>
                                            万元
                                        </td>
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProfessionType" runat="Server" Width="60px">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>层数:
                                        </td>
                                        <td colspan="5">地上：<asp:Label ID="lbl_upfloor" runat="server" Text="0"></asp:Label>
                                            层 地下：<asp:Label ID="lbl_downfloor" runat="server" Text="0"></asp:Label>
                                            层
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>建筑数量:
                                        </td>
                                        <td>
                                            <asp:Label ID="BuildNumber" runat="server"></asp:Label>个
                                        </td>
                                        <td>观测数量:
                                        </td>
                                        <td>
                                            <asp:Label ID="ObserveNumber" runat="server"></asp:Label>个                                         
                                        </td>
                                        <td>工程来源:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddSourceWay" runat="Server" Width="60px">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>观测总点次:
                                        </td>
                                        <td>
                                            <asp:Label ID="ObserveTotal" runat="Server"></asp:Label>次
                                        </td>
                                        <td>每点次单价:</td>
                                        <td>
                                            <asp:Label ID="ObservePoint" runat="Server"></asp:Label>元</td>
                                        <td>制表人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_tbcreate" runat="server" Width="120px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同开工日期:</td>
                                        <td>
                                            <asp:Label ID="txtSingnDate2" runat="server"></asp:Label>
                                        </td>
                                        <td>合同完成日期:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCompleteDate" runat="server"></asp:Label>
                                        </td>
                                        <td>合同统计年份:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtSingnDate" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>工期: 
                                        </td>
                                        <td>
                                            <asp:Label ID="ProjectDate" runat="server"></asp:Label>天
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>多栋楼:
                                        </td>
                                        <td colspan="5">
                                            <asp:Literal ID="txt_multibuild" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同备注:
                                        </td>
                                        <td colspan="5">
                                            <asp:Literal ID="txtcpr_Remark" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                    <h4 class="form-section">收费计划</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="sf_datas" style="width: 98%;" align="center">
                                        <tr id="sf_row">
                                            <td align="center" id="sf_id">收费次数
                                            </td>
                                            <td align="center" id="sf_bfb">百分比
                                            </td>
                                            <td align="center" id="sf_edu">付费额(万元)
                                            </td>
                                            <%--                                                <td align="center" id="sf_type">
                                                    收费类型
                                                </td>--%>
                                            <td align="center" id="sf_time">付款时间
                                            </td>
                                            <td align="center" id="sf_mark">备注
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                    <%--<h4 class="form-section">
                        合同子项</h4>
                    <div class="row">
                        <div class="col-md-12">--%>
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <%-- <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="datas"  style="width: 98%;" align="center">
                                        <tr id="sub_row">
                                            <td align="center" id="sub_id">
                                                序号
                                            </td>
                                            <td align="center" id="sub_name">
                                                子项名称
                                            </td>--%>
                    <%-- <td align="center" id="struct_sub">
                                                    结构形式
                                                </td>
                                                <td align="center" id="type_sub">
                                                    建筑分类
                                                </td>--%>
                    <%--  <td align="center" id="sub_area">
                                               建筑面积(<span style="font-size: 10pt">㎡</span>)
                                            </td>
                                            <td align="center" id="Money">
                                                金额(万元)
                                            </td>--%>
                    <%-- <td align="center" id="sub_livearea">
                                                    规划面积(<span style="font-size: 10pt">公顷</span>)
                                                </td>
                                                <td align="center" id="sub_moneyunit">
                                                    平米单价(元)
                                                </td>--%>
                    <%--  <td align="center" id="Remark">
                                                备注
                                            </td>
                                        </tr>
                                    </table>
                                </div>--%>
                    <!-- END FORM-->
                    <%--  </div>
                        </div>
                    </div>--%>
                    <h4 class="form-section">合同附件</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="datas_att" style="width: 98%;" align="center">
                                        <tr id="att_row">
                                            <td align="center" id="att_id">序号
                                            </td>
                                            <td align="center" id="att_filename">文件名称
                                            </td>
                                            <td align="center" id="att_filesize">文件大小
                                            </td>
                                            <td align="center" id="att_filetype">文件类型
                                            </td>
                                            <td align="center" id="att_uptime">上传时间
                                            </td>
                                            <td align="center" id="att_oper">操作
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-offset-12 col-md-12">
                                    <button type="submit" class="btn green" id="btn_output">
                                        导出</button>
                                    <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                        返回</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_flag" runat="server" />
</asp:Content>
