﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCoperationCharge.aspx.cs"
    Inherits="TG.Web.Coperation.AddCoperationCharge" %>

<%@ Register Assembly="xgo.Components" Namespace="xgo.Components" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/CprChargStatus.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript" src="../js/Coperation/AddCharge.js"></script>

    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="cls_data">
            <table style="width: 560px;" class="cls_content_head">
                <tr>
                    <td style="width: 90px;" align="center">计划收费次数
                    </td>
                    <td style="width: 50px;" align="center">百分比
                    </td>
                    <td style="width: 100px;" align="center">计划付款(万元)
                    </td>
                    <td style="width: 100px;" align="center">计划付款时间
                    </td>
                    <td style="width: 100px;" align="center">实际收款(万元)
                    </td>
                    <td style="width: 60px;" align="center">状态
                    </td>
                    <td style="width: 60px;" align="center">录入
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                Font-Size="12px" Width="560px" OnRowDataBound="gv_Coperation_RowDataBound" OnRowCommand="gv_Coperation_RowCommand">
                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                <Columns>
                    <asp:BoundField DataField="Times" HeaderText="收款次数">
                        <ItemStyle Width="90px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="persent" HeaderText="百分比">
                        <ItemStyle Width="50px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="payCount" HeaderText="计划收款">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="paytime" HeaderText="计划时间">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="实际收款">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("payShiCount") %>'></asp:Label>
                            <asp:HiddenField ID="Hidden1" runat="server" Value='<%# Eval("ID") %>' />
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="paytime" HeaderText="状态">
                        <ItemStyle Width="60px" />
                    </asp:BoundField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <a href="###" class="cls_inputcount">入账</a>
                            <asp:HiddenField ID="Hidden2" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:HiddenField ID="HiddenPartialAcceptance" runat="server" Value='<%# Bind("payShiCount") %>' />
                            <asp:HiddenField ID="HiddenSumOfAccount" runat="server" Value='<%# Bind("payCount") %>' />
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    无收款记录！
                </EmptyDataTemplate>
            </asp:GridView>
            <table class="cls_show_cst_jiben" style="width: 560px; margin: 0 auto; display: none;"
                id="td_input">
                <tr>
                    <td style="width: 100px;">入账收费批次：
                    </td>
                    <td>
                        <span id="txt_inputtimes"></span>
                    </td>
                </tr>

                <tr>
                    <td>未支付金额：
                    </td>
                    <td>
                        <input id="txt_partialAcceptance" maxlength="15" type="text" class="TextBoxBorder" disabled="disabled" />(万元)<span
                            style="color: red; display: none;" id="txt_partialAcceptance_valide">请输入数字！</span>
                    </td>
                </tr>

                <tr>
                    <td>入账金额：
                    </td>
                    <td>
                        <input id="txt_payCount" maxlength="15" type="text" class="TextBoxBorder" />(万元)<span
                            style="color: red; display: none;" id="txt_paycount_valide">请输入数字！</span>
                    </td>
                </tr>
                <tr>
                    <td>汇款人：
                    </td>
                    <td>
                        <input id="txt_remitter" maxlength="50" type="text" class="TextBoxBorder" /><span
                            style="color: red; display: none;" id="txt_remitter_valide">请填写汇款人姓名！</span>
                    </td>
                </tr>
                <tr>
                    <td>入账单据号：
                    </td>
                    <td class="TextBoxBorder">
                        <input id="txt_BillNo" type="text" maxlength="50" class="TextBoxBorder" /><span style="color: red; display: none;"
                            id="txt_BillNo_valide">请填写入账单据号！</span>
                    </td>
                </tr>
                <tr>
                    <td>入 账 人：
                    </td>
                    <td class="TextBoxBorder">
                        <input id="txt_user" type="text" maxlength="15" class="TextBoxBorder" /><span style="color: red; display: none;"
                            id="txt_user_valide">请填写入账人！</span>
                    </td>
                </tr>
                <tr>
                    <td>入账时间：
                    </td>
                    <td>
                        <input id="txt_times" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" /><span
                            style="color: red; display: none;" id="txt_time_valide">请选择入账时间！</span>
                    </td>
                </tr>
                <tr>
                    <td>备注：
                    </td>
                    <td>
                        <textarea maxlength="150" id="txt_mark" class="TextBoxBorder" style="width: 400px; height: 60px;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <a href="###" id="btn_addcount">
                            <img src="../Images/buttons/btn_shoukuan.gif" style="border: none;" /></a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="cls_data_bottom" style="text-align: center;">
            <a href="#" id="btn_close" style="color: Black; font-size: 10pt;">关闭</a>
        </div>
        <!-- 隐藏控件 -->
        <input id="hid_chgid" type="hidden" value="" />
        <input id="hid_plantime" type="hidden" value="" />
        <input id="hid_isinput" type="hidden" value="<%= IsSave %>" />
        <input id="hid_cprid" type="hidden" value="<%= CprId %>" />
        <a href="AddCoperationCharge.aspx?math=1" id="reload" style="display: none;"></a>
    </form>
</body>
</html>
