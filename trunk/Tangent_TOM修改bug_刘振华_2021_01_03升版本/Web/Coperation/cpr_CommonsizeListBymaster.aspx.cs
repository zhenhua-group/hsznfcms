﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.HSSF.UserModel;
using System.Text;
using InfoSoftGlobal;

namespace TG.Web.Coperation
{
    public partial class cpr_CommonsizeListBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitDropDownByYear();
                InitDropDownByMonth();
                BindUnit();
                BindPreviewPower();
                this.labYear.Text = DateTime.Now.Year.ToString();
                this.labTime.Text = DateTime.Now.ToString("yyyy年MM月dd日") + "制表";
                this.labYear2.Text = DateTime.Now.Year.ToString();
                GetTu();
            }
            else
            {
                GetTu();
            }
        }
        private void GetTu()
        {
            string unitId = this.drp_unit.SelectedItem.Value;
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string beginTime = month + "-01";
            string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
            TG.BLL.CopContrast bll = new BLL.CopContrast();
            if (unitId == "-1")
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    //
                    unitId = base.UserUnitNo.ToString();
                }
            }
            List<TG.Model.CopContrast> list = bll.GetList("unitcomm", unitId, year, beginTime, endTime);
            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<chart caption='" + year + "年（" + month + "月）【已签订合同额】与" + (Convert.ToInt32(year) - 1).ToString() + "同期对比图' numberPrefix='' formatNumberScale='0' rotateValues='1' placeValuesInside='1' decimals='0' numberSuffix='万元' baseFontSize='12' xAxisName='' yAxisName=''><categories>");
            foreach (TG.Model.CopContrast item in list)
            {
                xmlData.Append("<category name='" + item.Unit + "' />");
            }
            xmlData.Append("</categories>");
            xmlData.Append("<dataset seriesName='" + (int.Parse(year) - 1) + "'>");
            foreach (TG.Model.CopContrast item in list)
            {
                xmlData.Append("<set value='" + item.AcountMonth + "' />");
            }
            xmlData.Append("</dataset>");
            xmlData.Append("<dataset seriesName='" + year + "'>");
            foreach (TG.Model.CopContrast item in list)
            {
                xmlData.Append("<set value='" + item.AcountMonthNow + "' />");
            }
            xmlData.Append("</dataset>");
            xmlData.Append("</chart>");
            Literal4.Text = FusionCharts.RenderChart("../js/FunctionChart/MSColumn2D.swf", "", xmlData.ToString(), "productSales", "100%", "500", false, true);
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
        /// <summary>
        ///初始年份
        /// </summary>
        protected void InitDropDownByYear()
        {
            this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
        }
        /// <summary>
        ///初始月份
        /// </summary>
        protected void InitDropDownByMonth()
        {
            this.drp_month.Items.FindByText(DateTime.Now.Month.ToString()).Selected = true;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        protected void btn_report_Click(object sender, EventArgs e)
        {
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string title = year + "年（" + month + "月）【已签订合同额】与" + (Convert.ToInt32(year) - 1) + "同期对比表";
            ToExcel(title);
        }
        public void ToExcel(string title)
        {
            TG.BLL.CopContrast copcon = new BLL.CopContrast();

            string unitId = this.drp_unit.SelectedItem.Value;
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string beginTime = month + "-01";
            string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
            TG.BLL.CopContrast bll = new BLL.CopContrast();
            List<TG.Model.CopContrast> listSC = copcon.GetList("unitcomm", unitId, year, beginTime, endTime);

            ExportDataToExcel(listSC, "~/TemplateXls/cpr_CommonsizeList.xls", year, month);
            //获取excel的文件名称（Guid是一个全球表示，使excel的文件名不同）


        }
        private void ExportDataToExcel(List<TG.Model.CopContrast> listSC, string modelPath, string year, string month)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            ws.GetRow(0).GetCell(0).SetCellValue(this.drp_year.SelectedItem.Value + "年（" + this.drp_month.SelectedItem.Value + "月)【已签订合同额】与" + (Convert.ToInt32(this.drp_year.SelectedItem.Value) - 1) + "年同期对比表.xls");
            ws.GetRow(1).GetCell(6).SetCellValue(DateTime.Now.ToString("yyyy年-MM月-dd日") + "制表");
            ws.GetRow(2).GetCell(2).SetCellValue(int.Parse(year) - 1 + "年");
            ws.GetRow(3).GetCell(2).SetCellValue(int.Parse(month) + "月");
            ws.GetRow(2).GetCell(4).SetCellValue(int.Parse(year) + "年");
            ws.GetRow(3).GetCell(4).SetCellValue(int.Parse(month) + "月");
            ws.GetRow(2).GetCell(8).SetCellValue(int.Parse(year) + "年目标值");
            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 4;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            if (listSC.Count > 0)
            {
                foreach (TG.Model.CopContrast item in listSC)
                {
                    //    Unit AllotUnitNow AcountMonth beforeMubiao AcountMonthNow TongBiMonthNow nowMubiao TongBiMuBiao AllotUnitLast

                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.Unit);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.AllotUnitNow);
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(Convert.ToDouble(item.AcountMonth));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.beforeMubiao);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.AcountMonthNow)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.TongBiMonthNow); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.nowMubiao2); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.TongBiMuBiao2); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.AllotUnitLast); cell8.CellStyle = style;

                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(this.drp_year.SelectedItem.Value + "年（" + this.drp_month.SelectedItem.Value + "月)【已签订合同额】与" + (Convert.ToInt32(this.drp_year.SelectedItem.Value) - 1) + "年同期对比表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }

    }
}