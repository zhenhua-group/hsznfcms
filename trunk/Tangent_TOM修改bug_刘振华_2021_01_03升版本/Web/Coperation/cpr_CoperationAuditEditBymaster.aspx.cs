﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using AjaxPro;

namespace TG.Web.Coperation
{
    public partial class cpr_CoperationAuditEditBymaster : PageBase
    { 
        #region QueryString
        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 合同系统号
        /// </summary>
        public int CoperationSysNo { get; set; }
        public string Creattable { get; set; }
        /// <summary>
        /// 合同审核记录系统编号
        /// </summary>
        public int CoperationAuditSysNo
        {
            get
            {
                int coperationAuditSysNo = 0;
                int.TryParse(Request["CoperationAuditSysNo"], out coperationAuditSysNo);
                return coperationAuditSysNo;
            }
        }

        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_CoperationAuditEdit coperationAuditRecordEntity { get; set; }

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        //类型
        public string MsgType
        {
            get
            {
                return Request["msyType"];
            }
        }
        /// <summary>
        /// 消息审批状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }

        }
        #endregion

        #region Property
        /// <summary>
        /// 合同基本信息实体
        /// </summary>
        public TG.Model.cm_Coperation Coperation { get; set; }

        /// <summary>
        /// 合同审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        /// <summary>
        /// 是否有权限
        /// </summary>
        public string HasPower { get; set; }

        public string CoperationTypeHTMLString { get; set; }

        #endregion

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        TG.BLL.cm_Coperation coperationBLL = new TG.BLL.cm_Coperation();
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(cpr_CoperationAuditEditBymaster));
            string sql = @"SELECT CoperationSysNo FROM cm_AuditRecordEdit WHERE SysNo=" + CoperationAuditSysNo;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            if (o != null)
            {
                TG.BLL.cm_AuditRecordEdit auditBll = new TG.BLL.cm_AuditRecordEdit();
                string optionsValue = auditBll.GetModel(CoperationAuditSysNo).Options;
                string ChangeDateil = auditBll.GetModel(CoperationAuditSysNo).ChangeDetails;

                if (!IsPostBack)
                {
                    //查询合同系统号
                    CoperationSysNo = new TG.BLL.cm_AuditRecordEdit().GetModel(CoperationAuditSysNo).CoperationSysNo;
                    GetConperationBaseInfo();
                    GetCoperationAuditRecord();
                }
            }
            else
            {
                Response.Redirect("../Error.aspx?msgtype=" + MsgType);
            }
        }

        /// <summary>
        /// 得到合同基本信息
        /// </summary>
        private void GetConperationBaseInfo()
        {
            TG.BLL.cm_Coperation coperationBLL = new TG.BLL.cm_Coperation();
            Coperation = coperationBLL.GetModel(CoperationSysNo);
            //建设单位
            ConstructionCompany.Text = Coperation.BuildUnit;
            //承接部门
            UndertakeDepartment.Text = Coperation.cpr_Unit;
            //项目名称
            ProjectName.Text = Coperation.cpr_Name;
            //建设规模
            BuildScope.Text = Coperation.BuildArea;
            //建设地点
            BuildAddress.Text = Coperation.BuildPosition;
            //投资额
            Investment.Text = Coperation.cpr_Touzi.ToString();
            //建设性质
            HiddenBuildNature.Value = Coperation.Industry;
            //合同额 
            CoperationAmount.Text = Coperation.cpr_Acount.ToString();
            //合同分类
            HiddenCoperationType.Value = Coperation.cpr_Type;
            //建筑类别
            buildType.Text = Coperation.BuildType.Trim();
            //层数
            string[] floorArray = Coperation.Floor.Split('|');
            floorNum.Text = string.Format("地上：{0}层 地下：{1}层", floorArray[0].Trim().Length == 0 ? "0" : floorArray[0], floorArray[1].Trim().Length == 0 ? "0" : floorArray[1]);
            //保密情况
            //SecretSituation.Text =;

            this.RepeaterCoperationBuildType.DataSource = TG.DBUtility.DbHelperSQL.Query("SELECT [dic_Name] FROM [dbo].[cm_Dictionary] where dic_Type =N'cpr_fl'");
            this.RepeaterCoperationBuildType.DataBind();
        }

        /// <summary>
        /// 查询消息审核记录
        /// </summary>
        private void GetCoperationAuditRecord()
        {
            TG.BLL.cm_AuditRecordEdit coperationAuditBLL = new TG.BLL.cm_AuditRecordEdit();
            coperationAuditRecordEntity = coperationAuditBLL.GetModel(CoperationAuditSysNo);
            AuditStatus = coperationAuditRecordEntity == null ? "" : coperationAuditRecordEntity.Status;
            TG.Model.cm_Coperation model_cpr = coperationBLL.GetModel(CoperationSysNo);
            Creattable = CreatTableByContent(coperationAuditRecordEntity.Options, model_cpr);
            if (!string.IsNullOrEmpty(coperationAuditRecordEntity.AuditUser))
            {
                string[] tempArr = coperationAuditRecordEntity.AuditUser.Split(',');
                if (tempArr != null && tempArr.Length > 0)
                {
                    foreach (var item in tempArr)
                    {
                        //查询联系人姓名
                        TG.Model.tg_member user = new TG.BLL.tg_member().GetModel(int.Parse(item));
                        coperationAuditRecordEntity.AuditUserString = string.IsNullOrEmpty(coperationAuditRecordEntity.AuditUser) ? user.mem_Name : coperationAuditRecordEntity.AuditUserString + user.mem_Name + ",";
                    }
                    coperationAuditRecordEntity.AuditUserString = coperationAuditRecordEntity.AuditUserString.Substring(0, coperationAuditRecordEntity.AuditUserString.Length - 1);
                }
            }
        }

        [AjaxMethod]
        public string CheckPower(string auditStatus, int userSysNo, int needLegalAdviser)
        {
            //得到用户所有的角色集合
            List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList(userSysNo);

            int position = 0;
            switch (auditStatus)
            {
                case "A":
                    position = 1;
                    break;
                case "B":
                    position = 2;
                    break;
                case "D":
                    if (needLegalAdviser == 1)
                    {
                        position = 3;
                    }
                    else
                    {
                        position = 4;
                    }
                    break;
                case "F":
                    position = 4;
                    break;
                case "H":
                    position = 5;
                    break;
            }
            //得到该审核记录当前的审核角色
            TG.Model.cm_CoperationAuditConfig coperationAuditConfig = new TG.BLL.cm_CoperationAuditConfig().GetCoperationAuditConfigByPostion(position);

            string hasPower = (from role in roleList where role.SysNo == coperationAuditConfig.RoleSysNo select role).Count() > 0 ? "1" : "0";

            return hasPower;
        }

        protected string CreatTableByContent(string options, TG.Model.cm_Coperation copModel)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder builder1 = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            if (options != null && options.Length > 0)
            {
                options = options.Replace("|*|", "\n");
                string[] arratList = options.Split('\n');
                for (int i = 0; i < arratList.Length; i++)
                {
                    string typestr = arratList[i].Split(':')[0];
                    switch (typestr)
                    {
                        case "cpr_No":
                            builder.Append("<tr><td style=\" width:60px;\">合同编号</td><td style=\" width:300px;\">" + copModel.cpr_No + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_Type":
                            builder.Append("<tr><td style=\" width:60px;\">合同分类</td><td style=\" width:300px;\">" + copModel.cpr_Type + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + returnPross(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "cpr_Name":
                            builder.Append("<tr><td style=\" width:60px;\">合同名称</td><td style=\" width:300px;\">" + copModel.cpr_Name + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "BuildArea":                      
                            builder.Append("<tr><td style=\" width:60px;\">建筑面积</td><td style=\" width:300px;\">" + copModel.BuildArea + "(㎡)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(㎡)</td></tr>");
                            break;
                        case "BuildUnit":
                            builder.Append("<tr><td style=\" width:60px;\">建设单位</td><td style=\" width:300px;\">" + copModel.BuildUnit + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        //case "cpr_Area":
                        //    builder.Append("<tr><td style=\" width:60px;\">规划面积</td><td style=\" width:300px;\">" + copModel.cpr_Area + "(公顷)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(公顷)</td></tr>");
                        //    break;
                        ////
                        case "cpr_Type2":
                            builder.Append("<tr><td style=\" width:60px;\">合同类型</td><td style=\" width:300px;\">" + copModel.cpr_Type2 + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "StructType":
                            builder.Append("<tr><td style=\" width:60px;\">结构形式</td><td style=\" width:300px;\">" + copModel.StructType + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetXML("StructType", arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "BuildStructType":
                            builder.Append("<tr><td style=\" width:60px;\">建筑分类</td><td style=\" width:300px;\">" + copModel.BuildStructType + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetXML("BuildType", arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "BuildTypelevel":
                            builder.Append("<tr><td style=\" width:60px;\">建筑类别</td><td style=\" width:300px;\">" + copModel.BuildType + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetProfession(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        //////
                        case "ChgPeople":
                            builder.Append("<tr><td style=\" width:60px;\">工程负责人</td><td style=\" width:300px;\">" + copModel.ChgPeople + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "ChgPhone":
                            builder.Append("<tr><td style=\" width:60px;\">电话</td><td style=\" width:300px;\">" + copModel.ChgPhone + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "floor":
                            builder.Append("<tr><td style=\" width:60px;\">层数</td><td style=\" width:300px;\">地上：" + copModel.Floor.Split(new char[] { '|' }, StringSplitOptions.None)[0] + "层  地下：" + copModel.Floor.Split(new char[] { '|' }, StringSplitOptions.None)[1] + "层</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">地上：" + arratList[i].Split(':')[1].Split(new char[] { '|' }, StringSplitOptions.None)[0] + "层  地下：" + arratList[i].Split(':')[1].Split(new char[] { '|' }, StringSplitOptions.None)[1] + "层</td></tr>");
                            break;
                        case "ChgJia":
                            builder.Append("<tr><td style=\" width:60px;\">甲方负责人</td><td style=\" width:300px;\">" + copModel.ChgJia + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "ChgJiaPhone":
                            builder.Append("<tr><td style=\" width:60px;\">电话</td><td style=\" width:300px;\">" + copModel.ChgJiaPhone + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_Unit":
                            builder.Append("<tr><td style=\" width:60px;\">承接部门</td><td style=\" width:300px;\">" + copModel.cpr_Unit + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_Acount":
                            builder.Append("<tr><td style=\" width:60px;\">合同额</td><td style=\" width:300px;\">" + copModel.cpr_Acount + "(万元)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(万元)</td></tr>");
                            break;
                        case "cpr_Touzi":
                            builder.Append("<tr><td style=\" width:60px;\">投资额</td><td style=\" width:300px;\">" + copModel.cpr_Touzi + "(万元)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(万元)</td></tr>");
                            break;
                        case "BuildPosition":
                            builder.Append("<tr><td style=\" width:60px;\">工程地点</td><td style=\" width:300px;\">" + copModel.BuildPosition + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_ShijiAcount":
                            builder.Append("<tr><td style=\" width:60px;\">实际合同额</td><td style=\" width:300px;\">" + copModel.cpr_ShijiAcount + "(万元)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(万元)</td></tr>");
                            break;
                        case "cpr_ShijiTouzi":
                            builder.Append("<tr><td style=\" width:60px;\">实际投资额</td><td style=\" width:300px;\">" + copModel.cpr_ShijiTouzi + "(万元)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(万元)</td></tr>");
                            break;
                        case "Industry":
                            builder.Append("<tr><td style=\" width:60px;\">行业性质</td><td style=\" width:300px;\">" + copModel.Industry + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetProfession(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        //
                        case "cpr_Process":
                            builder.Append("<tr><td style=\" width:60px;\">合同阶段</td><td style=\" width:300px;\">" + returnPross(copModel.cpr_Process) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        //
                        case "TableMaker":
                            builder.Append("<tr><td style=\" width:60px;\">制表人</td><td style=\" width:300px;\">" + copModel.TableMaker + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "BuildSrc":
                            builder.Append("<tr><td style=\" width:60px;\">工程来源</td><td style=\" width:300px;\">" + copModel.BuildSrc + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetProfession(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "cpr_SignDate2":
                            builder.Append("<tr><td style=\" width:60px;\">合同签订日期</td><td style=\" width:300px;\">" + Convert.ToDateTime(copModel.cpr_SignDate2).ToString("yyyy-MM-dd") + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_DoneDate":
                            builder.Append("<tr><td style=\" width:60px;\">合同完成日期</td><td style=\" width:300px;\">" + Convert.ToDateTime(copModel.cpr_DoneDate).ToString("yyyy-MM-dd") + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_SignDate":
                            builder.Append("<tr><td style=\" width:60px;\">合同统计年份</td><td style=\" width:300px;\">" + Convert.ToDateTime(copModel.cpr_SignDate).ToString("yyyy-MM-dd") + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;                        
                        case "MultiBuild":
                            builder.Append("<tr><td style=\" width:60px;\">多栋楼</td><td style=\" width:300px;\">" + copModel.MultiBuild + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_Mark":
                            builder.Append("<tr><td style=\" width:60px;\">合同备注</td><td style=\" width:300px;\">" + copModel.cpr_Mark + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                    }
                }
            }
            return builder.ToString();
        }
        protected string GetProfession(string num)
        {
            string sql = "  SELECT dic_Name FROM cm_Dictionary WHERE id=" + num;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            if (o != null)
            {
                return o.ToString().Trim();
            }
            else
            {
                return "";
            }

        }
        public string returnPross(string pross)
        {
            string result = "";
            if (pross.Trim() != "" && pross != "-1")
            {
                string[] array = pross.Split(new char[] { ',' }, StringSplitOptions.None);
                TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                for (int i = 0; i < array.Length; i++)
                {
                    result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                }
                result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
            }
            return result;
        }
        public string GetXML(string type, string p)
        {
            StringBuilder builder = new StringBuilder();
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + type + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                //一级
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                Readxml(nodes, out builder, p);
            }
            if (builder.ToString().Length > 0)
            {
                return builder.ToString().Substring(1);
            }
            else
            {
                return builder.ToString();
            }
            //公共建筑^办公+工业建筑^重工厂房^轻工厂房
        }
        //第一级别
        public void Readxml(XmlNodeList xmlnl, out StringBuilder sb_, string p)
        {
            sb_ = new StringBuilder();
            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("+" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        //第二级别和一下的级别
        public void ReadxmlChild(XmlNodeList xmlnl, StringBuilder sb_, string p)
        {

            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
    }
}