﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Aspose.Words;
using System.Text;
using TG.Common;

namespace TG.Web.Coperation
{
    public partial class cpr_ShowTestCoperationBymaster : System.Web.UI.Page
    {
        //返回标示
        public string BackFlag
        {
            get
            {
                return Request["flag"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //合同ID
            string str_cprid = Request.QueryString["cprid"] ?? "";
            string str_flag = Request.QueryString["flag"] ?? "";
            if (!IsPostBack)
            {
                this.hid_cprid.Value = str_cprid;
                this.hid_flag.Value = str_flag;
                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
            }
            else
            {
                OutputLocation();
            }
        }

        private void OutputLocation()
        {
            TG.BLL.cm_MeasureCoperation bllcop = new TG.BLL.cm_MeasureCoperation();
            string cprid = Request.QueryString["cprid"];
            TG.Model.cm_MeasureCoperation model = bllcop.GetModel(Convert.ToInt32(cprid));
            int cstid = this.GetCstId(Convert.ToInt32(cprid));
            TG.Model.cm_CustomerInfo conmodel = this.GetConst(cstid);

            string tmppath = Server.MapPath("~/TemplateWord/TestCoprationReport.doc");
            Document doc = new Document(tmppath); //载入模板
            if (doc.Range.Bookmarks["BuildArea"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildArea"];
                mark.Text = model.BuildArea + "";
            }
            if (doc.Range.Bookmarks["BuildPosition"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildPosition"];
                mark.Text = model.BuildPosition == null ? "" : model.BuildPosition;
            }
            if (doc.Range.Bookmarks["BuildSrc"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildSrc"];
                mark.Text = model.BuildSrc == null ? "" : model.BuildSrc;
            }
            //if (doc.Range.Bookmarks["BuildStructType"] != null)
            //{
            //    Bookmark mark = doc.Range.Bookmarks["BuildStructType"];
            //    mark.Text = model.BuildStructType == null ? "" : model.BuildStructType;
            //}
            if (doc.Range.Bookmarks["BuildType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildType"];
                mark.Text = model.BuildType == null ? "" : model.BuildType;
            }
            if (doc.Range.Bookmarks["BuildUnit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                mark.Text = model.BuildUnit == null ? "" : model.BuildUnit;
            }
            if (doc.Range.Bookmarks["ChgJia"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJia"];
                mark.Text = model.ChgJia == null ? "" : model.ChgJia;
            }
            if (doc.Range.Bookmarks["ChgJiaPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJiaPhone"];
                mark.Text = model.ChgJiaPhone == null ? "" : model.ChgJiaPhone;
            }
            if (doc.Range.Bookmarks["ChgPeople"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPeople"];
                mark.Text = model.ChgPeople == null ? "" : model.ChgPeople;
            }
            if (doc.Range.Bookmarks["ChgPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPhone"];
                mark.Text = model.ChgPhone == null ? "" : model.ChgPhone;
            }

            //
            if (doc.Range.Bookmarks["BuildNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildNumber"];
                mark.Text = (model.BuildNumber == null ? "0" : model.BuildNumber + "") + "个";
            }
            if (doc.Range.Bookmarks["ObserveNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ObserveNumber"];
                mark.Text = (model.ObserveNumber == null ? "0" : model.ObserveNumber + "") + "个";
            }
            if (doc.Range.Bookmarks["ObserveTotal"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ObserveTotal"];
                mark.Text = (model.ObserveTotal == null ? "0" : model.ObserveTotal + "") + "次";
            }
            if (doc.Range.Bookmarks["ObservePoint"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ObservePoint"];
                mark.Text = (model.ObservePoint == null ? "0" : model.ObservePoint + "") + "元";
            }
            if (doc.Range.Bookmarks["ProjectDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjectDate"];
                mark.Text = (model.ProjectDate == null ? "0" : model.ProjectDate + "") + "天";
            }
            //

            if (doc.Range.Bookmarks["cpr_Acount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Acount"];
                mark.Text = model.cpr_Acount.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元";
            }
            if (doc.Range.Bookmarks["cpr_DoneDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_DoneDate"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_DoneDate));
            }
            if (doc.Range.Bookmarks["cpr_Mark"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Mark"];
                mark.Text = model.cpr_Mark == null ? "" : model.cpr_Mark;
            }
            if (doc.Range.Bookmarks["cpr_Name"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                mark.Text = model.cpr_Name == null ? "" : model.cpr_Name;
            }
            if (doc.Range.Bookmarks["cpr_No"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                mark.Text = model.cpr_No == null ? "" : model.cpr_No;
            }
            //if (doc.Range.Bookmarks["cpr_Process"] != null)
            //{
            //    Bookmark mark = doc.Range.Bookmarks["cpr_Process"];
            //    mark.Text = GetProfession(model.cpr_Process);
            //}
            if (doc.Range.Bookmarks["cpr_ShijiAcount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_ShijiAcount"];
                mark.Text = model.cpr_ShijiAcount.ToString() == "0.00" ? "" : model.cpr_ShijiAcount.ToString() + "万元";
            }
            //if (doc.Range.Bookmarks["cpr_ShijiTouzi"] != null)
            //{
            //    Bookmark mark = doc.Range.Bookmarks["cpr_ShijiTouzi"];
            //    mark.Text = model.cpr_ShijiTouzi.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元";
            //}
            if (doc.Range.Bookmarks["cpr_SignDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_SignDate"];
                mark.Text = model.cpr_SignDate2 == null ? "" : CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_SignDate2));
            }
            if (doc.Range.Bookmarks["cpr_SignDate2"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_SignDate2"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_SignDate));
            }
            //if (doc.Range.Bookmarks["cpr_Touzi"] != null)
            //{
            //    Bookmark mark = doc.Range.Bookmarks["cpr_Touzi"];
            //    mark.Text = model.cpr_Touzi.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元";
            //}
            if (doc.Range.Bookmarks["cpr_Type"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type"];
                mark.Text = model.cpr_Type == null ? "" : model.cpr_Type;
            }
            if (doc.Range.Bookmarks["cpr_Type2"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type2"];
                mark.Text = model.cpr_Type2 == null ? "" : model.cpr_Type2;
            }
            if (doc.Range.Bookmarks["cpr_Unit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                mark.Text = model.cpr_Unit == null ? "" : model.cpr_Unit;
            }
            if (doc.Range.Bookmarks["TableMaker"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TableMaker"];
                mark.Text = model.TableMaker == null ? "" : model.TableMaker;
            }
            if (doc.Range.Bookmarks["Floor"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Floor"];
                mark.Text = GetFloor(model.Floor);
            }
            if (doc.Range.Bookmarks["Industry"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Industry"];
                mark.Text = model.Industry == null ? "" : model.Industry;
            }
            if (doc.Range.Bookmarks["MultiBuild"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["MultiBuild"];
                mark.Text = model.MultiBuild == null ? "" : model.MultiBuild;
            }

            //if (doc.Range.Bookmarks["StructType"] != null)
            //{
            //    Bookmark mark = doc.Range.Bookmarks["StructType"];
            //    mark.Text = model.StructType == null ? "" : model.StructType;
            //}
            //chargeplan  收费计划
            if (doc.Range.Bookmarks["chargeplan"] != null)
            {
                string sqlcharge = @"SELECT * From cm_CoperationChargeType Where cpr_ID=" + Convert.ToInt32(cprid) + " and paytype='measurecharge'";
                StringBuilder builder = new StringBuilder();
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(sqlcharge).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int index = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        index++;
                        builder.Append(index + "、  ");
                        builder.Append(dr["Times"] + "   ");
                        builder.Append("百分之");
                        builder.Append(dr["persent"] + "   ");
                        builder.Append("金额");
                        builder.Append(dr["payCount"] + "万元   ");
                        builder.Append("收款时间是");
                        builder.Append(Convert.ToDateTime(dr["paytime"]).ToString("yyyy-MM-dd") + "。        ");
                    }
                }
                Bookmark mark = doc.Range.Bookmarks["chargeplan"];
                mark.Text = builder.ToString();
            }
            //childproj 工程子项
            //if (doc.Range.Bookmarks["childproj"] != null)
            //{
            //    string sqlcharge = @"SELECT * From cm_SubCoperation Where cpr_ID=" + Convert.ToInt32(cprid);
            //    StringBuilder builder = new StringBuilder();
            //    DataTable dt = TG.DBUtility.DbHelperSQL.Query(sqlcharge).Tables[0];
            //    if (dt.Rows.Count > 0)
            //    {
            //        int index = 0;
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            index++;
            //            builder.Append(index + "、  ");
            //            builder.Append("子项名称：");
            //            builder.Append(dr["Item_Name"] + "   ");
            //            builder.Append(".面积");
            //            builder.Append(dr["Item_Area"] + "㎡   ");
            //            builder.Append("金额：");
            //            builder.Append(dr["Money"] + "万元   ");
            //            builder.Append("备注：");
            //            builder.Append(dr["Remark"] + "。  ");
            //        }
            //    }
            //    Bookmark mark = doc.Range.Bookmarks["childproj"];
            //    mark.Text = builder.ToString();
            //}
            doc.Save(model.cpr_Name + ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  //保存为doc，并打开
        }

        //根据合同id获得用户id。
        public int GetCstId(int cpr_Id)
        {
            string sql = "select  cst_Id from cm_MeasureCoperation where cpr_Id=" + cpr_Id;
            return Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(sql));
        }
        //根据用户id获得用户model
        public TG.Model.cm_CustomerInfo GetConst(int cstid)
        {
            TG.BLL.cm_CustomerInfo dalcust = new TG.BLL.cm_CustomerInfo();
            return dalcust.GetModel(cstid);
        }
        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_MeasureCoperation] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Text = model_cst.Cst_No;
                        this.txtCode.Text = model_cst.Code;
                        this.txtCst_Name.Text = model_cst.Cst_Name;
                        this.txtLinkman.Text = model_cst.Linkman;
                        this.txtCpy_Address.Text = model_cst.Cpy_Address;
                        this.txtCpy_Phone.Text = model_cst.Cpy_Phone;
                        this.txt_JC.Text = model_cst.Cst_Brief;
                        this.txt_Fax.Text = model_cst.Cpy_Fax;
                    }
                }
            }
        }
        //显示合同信息
        protected void ShowCoperation(string cprid)
        {
            TG.BLL.cm_MeasureCoperation bll_cpr = new TG.BLL.cm_MeasureCoperation();
            TG.Model.cm_MeasureCoperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            if (model_cpr != null)
            {
                //赋值
                this.txtcpr_No.Text = model_cpr.cpr_No ?? "";
                this.ddcpr_Type.Text = model_cpr.cpr_Type.Trim();
                this.txt_cprType.Text = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
                this.txt_cprName.Text = model_cpr.cpr_Name.Trim();
                this.txt_cprBuildUnit.Text = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();

                this.txtcpr_Account.Text = model_cpr.cpr_Acount.ToString();
                this.txtcpr_Account0.Text = model_cpr.cpr_ShijiAcount.ToString();
                // this.txtInvestAccount.Text = model_cpr.cpr_Touzi.ToString();
                // this.txtInvestAccount0.Text = model_cpr.cpr_ShijiTouzi.ToString();
                //  string result = "";
                //  if (model_cpr.cpr_Process.Trim() != "")
                //  {
                //    string[] array = model_cpr.cpr_Process.Split(new char[] { ',' }, StringSplitOptions.None);
                //    TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                //   for (int i = 0; i < array.Length; i++)
                //    {
                //        result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                //    }
                //    result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
                //   }
                //   this.ddcpr_Stage.Text = result;
                this.txtSingnDate.Text = Convert.ToDateTime(model_cpr.cpr_SignDate).ToShortDateString();
                this.txtSingnDate2.Text = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToShortDateString();
                this.txtCompleteDate.Text = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToShortDateString();
                //建筑数量、观测数量、观测总点次、每点次单价\工期
                this.BuildNumber.Text = (model_cpr.BuildNumber ?? 0) + "";
                this.ObserveNumber.Text = (model_cpr.ObserveNumber ?? 0) + "";
                this.ObservePoint.Text = (model_cpr.ObservePoint ?? 0) + "";
                this.ObserveTotal.Text = (model_cpr.ObserveTotal ?? 0) + "";
                this.ProjectDate.Text = (model_cpr.ProjectDate ?? 0) + "";

                this.txtcpr_Remark.Text = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
                this.txt_buildArea.Text = Convert.ToString(model_cpr.BuildArea ?? 0).Trim();
                this.txt_proFuze.Text = model_cpr.ChgPeople.Trim();
                this.txt_fzphone.Text = model_cpr.ChgPhone.Trim();
                this.txtFParty.Text = model_cpr.ChgJia.Trim();
                this.txt_jiafphone.Text = model_cpr.ChgJiaPhone.Trim();
                if (!string.IsNullOrEmpty(model_cpr.BuildPosition))
                {
                    if (model_cpr.BuildPosition == "-1")
                    {
                        this.ddProjectPosition.Text = "";
                    }
                    else
                    {
                        this.ddProjectPosition.Text = model_cpr.BuildPosition.Trim();
                    }
                }
                if (model_cpr.Industry.Trim() == "-1")
                {
                    this.ddProfessionType.Text = "";
                }
                else
                {
                    this.ddProfessionType.Text = model_cpr.Industry.Trim();
                }
                if (model_cpr.BuildSrc.Trim() == "-1")
                {
                    this.ddSourceWay.Text = "";
                }
                else
                {
                    this.ddSourceWay.Text = model_cpr.BuildSrc.Trim();
                }
                this.txt_cjbm.Text = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit;
                this.txt_tbcreate.Text = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
                //if (model_cpr.StructType != null)
                //{
                //    this.lbl_StructType.Text = TG.Common.StringPlus.ResolveStructString(model_cpr.StructType.ToString());
                //}
                ////建筑分类
                //if (model_cpr.BuildStructType != null)
                //{
                //    this.lbl_BuildStructType.Text = TG.Common.StringPlus.ResolveStructString(model_cpr.BuildStructType.ToString());
                //}
                ////设计等级
                //if (model_cpr.BuildType != null)
                //{
                //    this.txt_buildType.Text = model_cpr.BuildType.Trim().Replace('+', ',');
                //}

                this.txt_buildType.Text = model_cpr.BuildType == null ? "" : model_cpr.BuildType.Trim();
                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.lbl_upfloor.Text = floors[0].ToString();
                this.lbl_downfloor.Text = floors[1].ToString();
                //多栋楼
                this.txt_multibuild.Text = model_cpr.MultiBuild;
            }
            //this.txt_area.Text = model_cpr.cpr_Area;
        }
        //  转换地上和地下楼层数
        protected string GetFloor(string floor)
        {
            string[] floors = floor.Split('|');
            if (floors.Length > 0)
            {
                return "地上:" + floors[0] + "地下:" + floors[1];
            }
            else
            {
                return "";
            }
        }
        protected string GetProfession(string num)
        {
            string result = "";
            string[] str = num.Split(',');
            if (str.Length > 1)
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == "27")
                    {
                        result += "方案，";
                    }
                    else if (str[i] == "28")
                    {
                        result += "初设，";
                    }
                    else if (str[i] == "29")
                    {
                        result += "施工图，";
                    }
                    else
                    {
                        result += "其他";
                    }
                }
            }
            switch (num)
            {
                case "-1":
                    result = "";
                    break;
                case "27":
                    result = "方案";
                    break;
                case "28":
                    result = "初设";
                    break;
                case "29":
                    result = "施工图";
                    break;
                case "30":
                    result = "其他";
                    break;
                case "31":
                    result = "公开招标";
                    break;
                case "32":
                    result = "邀请招标";
                    break;
                case "36":
                    result = "计算机行业";
                    break;
                case "33":
                    result = "自行委托";
                    break;
                case "37":
                    result = "教育行业";
                    break;
                case "38":
                    result = "建筑行业";
                    break;
                case "47":
                    result = "科教行业";
                    break;
                case "34":
                    result = "普通客户";
                    break;
                case "35":
                    result = "VIP客户";
                    break;
                case "39":
                    result = "一般";
                    break;
                case "40":
                    result = "密切";
                    break;
                case "41":
                    result = "很密切";
                    break;
                case "43":
                    result = "一级";
                    break;
                case "44":
                    result = "二级";
                    break;
                case "45":
                    result = "三级";
                    break;
                case "46":
                    result = "四级";
                    break;
                default:
                    result += "";
                    break;
            }
            return result;
        }
    }
}