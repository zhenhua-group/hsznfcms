﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Text;
using System.Xml;
using TG.Model;
using TG.BLL;

namespace TG.Web.Coperation
{
    public partial class AddCoperationForReportBymaster : PageBase
    {
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定合同类别
                BindCorpType();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //绑定客户选择数据
                BindChooseCustomerData();
                //设置样式
                SetDropDownTreeThem();
                //建筑类别
                BindBuildType();
                //绑定建筑结构样式
                BindStructType();
                //绑定建筑分类
                BindBuildStuctType();
                //设置选择用户的属性
                SetUserSysNoAndRole();
                //设置用户权限
                BindPreviewPower();
            }
            else
            {
                ADDCopBack();
            }
        }

        private void ADDCopBack()
        {
            if (this.hid_cstid.Value.Trim() != "")
            {
                if (AddCoperationBak() > 0)
                {
                    TG.Common.MessageBox.ShowAndRedirect(this, "合同报备信息保存成功！", "CoperationForReportListBymaster.aspx");
                }
            }
        }

        //添加报备信息
        protected int AddCoperationBak()
        {
            //影响的行数
            int affectRow = 0;
            //客户关联ID
            int cst_id = int.Parse(this.hid_cstid.Value);
            string cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            string cpr_Type2 = this.txt_cprType.Value;
            string cpr_Unit = this.hid_cjbm.Value;
            decimal cpr_Acount = decimal.Parse(this.txtcpr_Account.Value);
            if (this.txtcpr_Account0.Value.Trim() == "")
            {
                this.txtcpr_Account0.Value = "0";
            }
            decimal cpr_ShijiAcount = decimal.Parse(this.txtcpr_Account0.Value);
            if (this.txtInvestAccount.Value.Trim() == "")
            {
                this.txtInvestAccount.Value = "0";
            }
            decimal cpr_Touzi = decimal.Parse(this.txtInvestAccount.Value);
            if (this.txtInvestAccount0.Value.Trim() == "")
            {
                this.txtInvestAccount0.Value = "0";
            }
            decimal cpr_ShijiTouzi = decimal.Parse(this.txtInvestAccount0.Value);
            string cpr_Mark = this.txtcpr_Remark.Value;
            string cpr_Address = this.txt_cprAddress.Value;
            string BuildArea = this.txt_buildArea.Value;
            string ChgPeople = this.txt_proFuze.Value;
            string ChgPhone = this.txt_fzphone.Value;
            string ChgJia = this.txtFParty.Value;
            string ChgJiaPhone = this.txt_jiafphone.Value;
            string UpdateBy = UserSysNo.ToString();
            string BuildPosition = this.txt_ProjectPosition.Value;
            string Industry = this.ddProfessionType.SelectedItem.Text;
            string BuildUnit = this.txtcpr_recorpt.Value;
            string BuildSrc = this.ddSourceWay.SelectedItem.Value;
            DateTime RegTime = DateTime.Now;
            string BuildType = this.drp_buildtype.SelectedItem.Text;
            string StructType = this.GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            string Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            string BuildStructType = this.GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            string MultiBuild = this.txtcpr_recorpt.Value;
            string cpr_Name = this.txt_cprName.Value;
            int InsertUserID = UserSysNo;
            DateTime InsertDate = DateTime.Now;
            int proapproval = int.Parse(this.drp_proapproval.SelectedItem.Value);

            TG.Model.cm_Coperation_back model = new TG.Model.cm_Coperation_back();
            model.cst_Id = cst_id;
            model.cpr_Type = cpr_Type;
            model.cpr_Type2 = cpr_Type2;
            model.cpr_Unit = cpr_Unit;
            model.cpr_Acount = cpr_Acount;
            model.cpr_ShijiAcount = cpr_ShijiAcount;
            model.cpr_Touzi = cpr_Touzi;
            model.cpr_ShijiTouzi = cpr_ShijiTouzi;
            model.cpr_Mark = cpr_Mark;
            model.cpr_Address = cpr_Address;
            model.BuildArea = BuildArea;
            model.ChgPeople = ChgPeople;
            model.ChgPhone = ChgPhone;
            model.ChgJia = ChgJia;
            model.ChgJiaPhone = ChgJiaPhone;
            model.UpdateBy = UpdateBy;
            model.BuildPosition = BuildPosition;
            model.Industry = Industry;
            model.BuildUnit = BuildUnit;
            model.BuildSrc = BuildSrc;
            model.RegTime = RegTime;
            model.BuildType = BuildType;
            model.StructType = StructType;
            model.Floor = Floor;
            model.BuildStructType = BuildStructType;
            model.MultiBuild = MultiBuild;
            model.cpr_Name = cpr_Name;
            model.InsertUserID = InsertUserID;
            model.InsertDate = InsertDate;
            model.proapproval = proapproval;

            TG.BLL.cm_Coperation_back bll = new TG.BLL.cm_Coperation_back();
            affectRow = bll.Add(model);

            return affectRow;
        }
        //返回临时合同ID
        protected string GetCoperationID()
        {
            string tempid = DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second.ToString();
            return tempid;
        }

        // 得到用户id
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }

        // update by 20130530 qpl
        //获取选中树所有节点值并返回值
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        //获取第二级的节点拼接
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();
        }
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }

        //来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }

        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
        }
        //合同建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //结构形式
        protected void BindStructType()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
        }
        //建筑分类
        protected void BindBuildStuctType()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
        }
        /// 初始化各项
        private void BindChooseCustomerData()
        {
            //选择客户
            this.ChooseCustomer1.DataBind();
            this.ChooseCustomer1.EnableViewState = true;
        }

        //设置选择合同额方法
        private void SetUserSysNoAndRole()
        {
            //用户id
            this.ChooseCustomer1.UserSysNo = int.Parse(this.GetCurMemID());
            //权限id
            this.ChooseCustomer1.PreviewPower = this.GetPreviewPower();
        }
        // 查找用户权限 by long 20130510
        private int GetPreviewPower()
        {
            string PageName = HttpContext.Current.Request.Path.ToString().Substring(HttpContext.Current.Request.Path.LastIndexOf('/'));
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);
            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
    }
}