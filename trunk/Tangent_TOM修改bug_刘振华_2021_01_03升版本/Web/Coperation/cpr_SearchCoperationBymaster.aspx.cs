﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.Coperation
{
    public partial class cpr_SearchCoperationBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDropList();
                BindPreviewPower();
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //是否检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //检查浏览权限
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定下拉列表
        protected void BindDropList()
        {
            //合同类型
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.drp_type.DataSource = bll_dic.GetList(str_where);
            this.drp_type.DataTextField = "dic_Name";
            this.drp_type.DataValueField = "ID";
            this.drp_type.DataBind();
            //行业性质
            str_where = " dic_Type='cpr_hyxz'";
            this.drp_xz.DataSource = bll_dic.GetList(str_where);
            this.drp_xz.DataTextField = "dic_Name";
            this.drp_xz.DataValueField = "ID";
            this.drp_xz.DataBind();
            //工程地点
            //str_where = " dic_Type='cpr_dd'";
            //this.drp_position.DataSource = bll_dic.GetList(str_where);
            //this.drp_position.DataTextField = "dic_Name";
            //this.drp_position.DataValueField = "ID";
            //this.drp_position.DataBind();
            //工程来源
            str_where = " dic_Type='cpr_src'";
            this.drp_src.DataSource = bll_dic.GetList(str_where);
            this.drp_src.DataTextField = "dic_Name";
            this.drp_src.DataValueField = "ID";
            this.drp_src.DataBind();
            //承接部门 
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
            //建筑类型
            this.drp_buildtype.DataSource = bll_dic.GetList(" dic_Type='cpr_buildtype'");
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();

            //阶段

            str_where = " dic_Type='cpr_jd'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataBind();

            foreach (ListItem item in chk_cprjd.Items)
            {
                item.Attributes.Add("val", item.Value);
            }
        }
        //查询数据
        protected void SearchBindData()
        {
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();

            StringBuilder sb = new StringBuilder("");

            //检查权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();


        }
    }
}