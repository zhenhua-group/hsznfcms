﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace TG.Web.Coperation
{
    public partial class ChargeCollectionRpt : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectedCurYear();
                LoadUnitTree();
                SelectedCurUnit();
                //统计数据
                GetAllData();
            }
        }

        #region 基本设置 2015年6月9日
        /// <summary>
        /// 统计语言
        /// </summary>
        public string CountTip
        {
            get;
            set;
        }
        /// <summary>
        /// 当前年
        /// </summary>
        public string CurYear { get; set; }
        public string CurMonth { get; set; }
        /// <summary>
        /// 前一年
        /// </summary>
        public string BeforeYear { get; set; }
        public string BeforeMonth { get; set; }
        /// <summary>
        /// 表格数据
        /// </summary>
        public string HtmlTable { get; set; }
        /// <summary>
        /// 绑定多选年
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();

            if (list.Count > 0)
            {
                foreach (string str in list)
                {
                    //加载年
                    drpYear.Items.Add(new ListItem(str, str));
                    //drpYear2.Items.Add(new ListItem(str, str));
                }
            }
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void SelectedCurYear()
        {
            int curyear = DateTime.Now.Year;
            int curmonth = DateTime.Now.Month;
            //当前年
            //if (drpYear2.Items.FindByValue(curyear.ToString()) != null)
            //{
            //    drpYear2.Items.FindByValue(curyear.ToString()).Selected = true;
            //}
            if (drpYear.Items.FindByValue(curyear.ToString()) != null)
            {
                drpYear.Items.FindByValue(curyear.ToString()).Selected = true;
            }

            //当前月
            if (curmonth == 1)
            {
                curmonth = 1;
            }
            drpMonth.Items.FindByValue((curmonth - 1).ToString()).Selected = true;
            //drpMonth2.Items.FindByValue(curmonth.ToString()).Selected = true;
        }

        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { UserUnitNo.ToString() };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";

            if (base.RolePowerParameterEntity != null)
            {

                //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void GetSelectYear()
        {
            this.BeforeYear = this.drpYear.SelectedValue;//年
            this.BeforeMonth = this.drpMonth.SelectedValue;//月
            //this.CurYear = this.drpYear2.SelectedValue;//当年
            //this.CurMonth = this.drpMonth2.SelectedValue;//当月
        }
        #endregion

        #region 统计表
        /// <summary>
        /// 获取统计表
        /// </summary>
        protected DataTable GetCountData()
        {
            //每月的天数
            string days = "30";
            if (BeforeMonth != "0")
            {
                days = DateTime.DaysInMonth(int.Parse(BeforeYear), int.Parse(BeforeMonth)).ToString();
            }
            //查询
            string strSql = string.Format(@"SELECT 
		                                P.Acount as 'Acount'
		                                ,P.InAcountCode as 'CprNo'
		                                ,C.cpr_Unit as 'Unit'
		                                ,C.BuildUnit as 'BuildUnit'
		                                ,CONVERT(nvarchar(100), P.InAcountTime,23) as InAcountTime
                                        FROM cm_Coperation AS C RIGHT JOIN cm_ProjectCharge AS P ON P.cprID=c.cpr_Id 
                                        WHERE P.Status!='B' ");
            //部门
            if (this.drp_unit.GetCheckedNodes(false).Count != 0)
            {
                string strUnitArr = "''";

                foreach (ASTreeViewNode node in this.drp_unit.GetCheckedNodes(false))
                {
                    strUnitArr += ",'" + node.NodeText.Trim().ToString() + "'";
                }

                strSql += string.Format(" AND cpr_Unit IN ({0})", strUnitArr);
            }
            //时间
            if (BeforeMonth != "0")
            {
                strSql += string.Format(" AND P.InAcountTime BETWEEN '{0}-{1}-01'AND '{0}-{1}-{2}' ", BeforeYear,
                    BeforeMonth, days);
            }
            else
            {
                strSql += string.Format(" AND year(P.InAcountTime)={0} ", BeforeYear);
            }

            //排序
            strSql += " ORDER BY P.InAcountTime DESC ";
            //查询语句


            DataTable dtData = DBUtility.DbHelperSQL.Query(strSql.ToString()).Tables[0];
            return dtData;
        }
        /// <summary>
        /// 创建统计行
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        private void CreateTableRowByUnit()
        {
            StringBuilder sbTable = new StringBuilder();
            //获取数据表
            DataTable dtData = GetCountData();

            foreach (DataRow dr in dtData.Rows)
            {
                sbTable.Append("<tr>");
                sbTable.AppendFormat("<td>{0}</td>", this.drpMonth.SelectedItem.Text);
                sbTable.AppendFormat("<td>{0}</td>", dr["CprNo"].ToString());
                string substr = dr["Unit"].ToString() + dr["BuildUnit"].ToString() + "项目费用";
                sbTable.AppendFormat("<td>{0}</td>", substr);
                sbTable.AppendFormat("<td>{0}</td>", dr["Acount"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", dr["InAcountTime"].ToString());
                sbTable.AppendFormat("<td></td>");
                sbTable.Append("</tr>");

                //创建对象列表
                TG.Model.ChargeCollectionExcel excel = new TG.Model.ChargeCollectionExcel()
                {
                    Month = this.drpMonth.SelectedItem.Text,
                    ChargeNo = dr["CprNo"].ToString(),
                    ChargeDetails = substr,
                    ChgDate = dr["InAcountTime"].ToString(),
                    ChgAcount = dr["Acount"].ToString(),
                    ChgSub = ""
                    
                };

                this.ImportExcelList.Add(excel);
            }

            this.HtmlTable = sbTable.ToString();

            //保存导出列表对象
            ViewState["ExcelList"] = this.ImportExcelList;
        }
        /// <summary>
        /// 统计提示
        /// </summary>
        protected void GetCountCondition()
        {
            string strTip = "";
            strTip += this.drpYear.SelectedValue + "年";
            strTip += this.drpMonth.SelectedValue + "月";
            strTip += "财务对账报表";

            this.CountTip = strTip;
        }
        #endregion

        protected void GetAllData()
        {
            ////选中当前年
            GetSelectYear();
            ////组织话术
            GetCountCondition();
            ////获取数据
            CreateTableRowByUnit();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetAllData();
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcelByData();
        }
        /// <summary>
        /// 导出列表列表集合
        /// </summary>
        protected List<TG.Model.ChargeCollectionExcel> ImportExcelList =
            new List<TG.Model.ChargeCollectionExcel>();
        /// <summary>
        /// 导出Excel
        /// </summary>
        protected void ExportExcelByData()
        {
            GetCountCondition();
            //导出列表
            List<TG.Model.ChargeCollectionExcel> excellist = ViewState["ExcelList"] as List<TG.Model.ChargeCollectionExcel>;
            //模板路径
            string pathModel = "~/TemplateXls/ChargeCollectionRptTlt.xls";

            //导出Excel
            ExportDataToExcel(excellist, pathModel);
        }
        private void ExportDataToExcel(List<TG.Model.ChargeCollectionExcel> excellist, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);
            string title = this.drpYear.SelectedValue + "年" + this.drpMonth.SelectedValue + "月财务收款对账报表";
            ws.GetRow(0).GetCell(0).SetCellValue(title);

            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 2;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            if (excellist.Count > 0)
            {
                int i = 0;
                foreach (TG.Model.ChargeCollectionExcel item in excellist)
                {
                    //     month  XX CprNo Unit BuildUnit Acount FourthTime FifthTime Mark
                    i++;
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.Month);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.ChargeNo);
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.ChargeDetails);
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.ChgAcount);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(item.ChgDate); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(item.ChgSub); cell5.CellStyle = style;
                    
                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(title + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}