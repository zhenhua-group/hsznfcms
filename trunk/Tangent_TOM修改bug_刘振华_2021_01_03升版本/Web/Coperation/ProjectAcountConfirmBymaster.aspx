﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectAcountConfirmBymaster.aspx.cs" Inherits="TG.Web.Coperation.ProjChgWin.ProjectAcountConfirmBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            //记录是否存在
            var flag = $("#ctl00_ContentPlaceHolder1_hid_flag").val();
            if (flag == "0") {
                $("#td_input").show();
                $("#tb_noDetails").hide();
            }
            else {
                $("#td_input").hide();
                $("#tb_noDetails").show();
            }

            //只能选择控制
            //            $(".cls_input_text_valid").focus(function () {
            //                $(this).blur();
            //            });

            //查询
            $("#ctl00_ContentPlaceHolder1_btn_cprType").click(function () {

                $("#customerCompactTable tr").remove();
                LoadAjaxData();

            });

            //审批状态
            var status = $.trim($("#HiddenAuditStatus").val());


            $("#ctl00_ContentPlaceHolder1_sureButton").click(function () {
                //入账单号是否为空
                var billno = $("#ctl00_ContentPlaceHolder1_lbl_BillCode").val();

                if (!$("#ctl00_ContentPlaceHolder1_chk_no").get(0).checked) {
                    if ($.trim(billno) == "") {
                        alert("请输入单据号！");
                        return false;
                    }
                }

                $("#ctl00_ContentPlaceHolder1_lbl_BillCode").val(billno);


                //如果不通过填写原因
                if ($("#ctl00_ContentPlaceHolder1_chk_no").get(0).checked) {
                    if (status == "A") {
                        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_caiwu").val()) == "") {
                            alert("不通过原因不能为空！");
                            return false;
                        }
                    }
                    else if (status == "C") {
                        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_suozhang").val()) == "") {
                            alert("不通过原因不能为空！");
                            return false;
                        }
                    }
                }
            });
            //通过按钮
            $("#ctl00_ContentPlaceHolder1_chk_yes").click(function () {
                if ($(this).attr("checked") == "checked") {
                    $("#Caiwu").hide();
                    $("#Suozhang").hide();

                    $("#DanJu").show();
                    if (status == "A") {

                        $("#ctl00_ContentPlaceHolder1_lbl_BillCode").val("");
                        //$("#lbl_BillCode").attr("disabled", true);
                    }
                    else {
                        $("#ctl00_ContentPlaceHolder1_lbl_BillCode").attr("disabled", true);
                    }
                }
            });
            $("#ctl00_ContentPlaceHolder1_chk_no").click(function () {
                if ($(this).attr("checked") == "checked") {
                    if (status == "A") {
                        $("#Caiwu").show();
                        $("#Suozhang").hide();
                        $("#ctl00_ContentPlaceHolder1_lbl_BillCode").val("");
                        $("#DanJu").hide();
                    }
                    else if (status == "C") {
                        $("#Caiwu").hide();
                        $("#Suozhang").show();
                        // $("#lbl_BillCode").attr("disabled", true);
                    }
                }
            });
            //判断审批状态
            if (status == "B") {
                $("#Caiwu").show();
            }
            else if (status == "D") {
                $("#Suozhang").show();
            }

            //弹层实现-加载数据
            function LoadAjaxData() {

                var name = $("#HiddenCprName").val();
                var account = $("#ctl00_ContentPlaceHolder1_lbl_acount").text();
                var inTime = $("#ctl00_ContentPlaceHolder1_lbl_times").text();
                var unit = $("#HiddenCprUnit").val();
                var data = "action=getFinancial&name=" + name + "&inTime=" + inTime + "&unit=" + unit + "&account=" + account;

                $.ajax({
                    type: "Post",
                    dataType: "json",
                    url: "/HttpHandler/CommHandler.ashx",
                    data: data,
                    success: function (result) {
                        AjaxSuccess_Financial(result);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统错误!");
                    }
                });
            }

            //返回成功的 执行方法
            function AjaxSuccess_Financial(result) {
                var data = result == null ? "" : result.ds;

                if (data != "") {
                    $.each(data, function (i, n) {
                        var name = n.Name;
                        if (name.length > 20) {
                            name = name.substr(0, 20) + "...";
                        }
                        var oper = "<span  style=\"cursor: pointer; color: Blue;\" rel='" + n.ID + "' style=\"color:blue;\">选择</span>";
                        var trHtml = "<tr><td width=\"100px\">" + n.FinancialNum + "</td><td width=\"260px\" title=" + n.Name + ">" + name + "</td><td width=\"100px\">" + n.Account + "</td><td width=\"100px\">" + oper + "</td></tr>";
                        $("#customerCompactTable").append(trHtml);
                        $("#customerCompactTable span:last").click(function () {
                            $("#ctl00_ContentPlaceHolder1_lbl_BillCode").val(n.FinancialNum);
                            $("#ctl00_ContentPlaceHolder1_Hiddencm_FinancialNum").val(n.ID);
                            $("#chooseCustomerCompact").modal("hide");
                        });
                    });
                }
                else {
                    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='4'>无数据!</td></tr>";
                    $("#customerCompactTable").append(trHtml);
                }
            }
        });
    </script>
    <style type="text/css">
        /* 表格基本样式*/
        .cls_show_cst_jiben1
        {
            margin: 0 auto;
        }
        /* 表格基本样式*/
        .cls_show_cst_jiben
        {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        .cls_show_cst_jiben > tbody > tr > td
        {
            border: solid 1px #CCC;
            height: 18px;
        }
        .cls_content_head
        {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }
        .cls_content_head td
        {
            height: 20px;
            background-color: #E6E6E6;
            border: 1px solid Gray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        收费管理 <small>确认项目入账</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../../mainpage/WelcomePage.aspx">首页</a> <i
        class="fa fa-angle-right"></i><a>收费管理</a><i class="fa fa-angle-right"> </i><a>确认项目入账</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>
                        <asp:Label ID="lbl_status" runat="server" Text=""></asp:Label>确认项目入账
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="cls_data">
                        <table style="width: 800px; margin: 0 auto;" class="table table-bordered">
                            <tr>
                                <td style="width: 450px;">
                                    合同名称：<%=CoperationName%>
                                </td>
                                <td>
                                    承接部门：<%=CoperationUnit%>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 800px; margin: 0 auto;" class="table table-bordered">
                            <tr>
                                <th style="width: 50px;" align="center">
                                    序号
                                </th>
                                <th style="width: 100px;" align="center">
                                    入账额(万元)
                                </th>
                                <th style="width: 200px;" align="center">
                                    汇款人
                                </th>
                                <th style="width: 100px;" align="center">
                                    入账人
                                </th>
                                <th style="width: 150px;" align="center">
                                    汇款单据号
                                </th>
                                <th style="width: 100px;" align="center">
                                    入账时间
                                </th>
                                <th style="width: 100px;" align="center">
                                    状态
                                </th>
                            </tr>
                        </table>
                        <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            CssClass="table table-bordered  cls_show_cst_jiben1" Font-Size="12px" Width="800px"
                            OnRowDataBound="gv_Coperation_RowDataBound" EnableModelValidation="True">
                            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="序号">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex+1%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Acount" HeaderText="入账额">
                                    <ItemStyle Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FromUser" HeaderText="汇款人">
                                    <ItemStyle Width="200px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="InAcountUser" HeaderText="入账人">
                                    <ItemStyle Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="InAcountCode" HeaderText="汇款单据号">
                                    <ItemStyle Width="150px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="InAcountTime" HeaderText="入账时间" DataFormatString="{0:d}">
                                    <ItemStyle Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="状态">
                                    <ItemStyle Width="100px" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <table class="table table-bordered" style="width: 800px; margin: 0 auto;" id="td_input">
                            <tr>
                                <td style="width: 150px;">
                                    入账金额：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_acount" runat="server"></asp:Label>(万元)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    汇款人：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_remitter" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    入账时间：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_times" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    入 账 人：
                                </td>
                                <td class="TextBoxBorder">
                                    <asp:Label ID="lbl_user" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    备注：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_mark" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="DanJu">
                                <td>
                                    入账单据号：
                                </td>
                                <td class="TextBoxBorder">
                                    <%-- <input id="lbl_BillCode" name="txt_keyname" type="text" runat="server" value="" class="cls_input_text_valid"
                        width="250px" />--%>
                                    <asp:TextBox ID="lbl_BillCode" runat="server" CssClass="cls_input_text_valid" Width="200px"></asp:TextBox>
                                    <a href="#chooseCustomerCompact" data-toggle="modal" id="btn_cprType" runat="Server"
                                        style="cursor: pointer; color: Blue;">查询</a>
                                    <%--<a
                        href="#" id="btn_cprType">查询</a>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    确定入账：
                                </td>
                                <td class="TextBoxBorder">
                                    <%-- <asp:RadioButton ID="chk_yes" runat="server" Text="是" Checked="true" GroupName="A" />
                                    <asp:RadioButton ID="chk_no" runat="server" Text="否" GroupName="A" />--%>
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="1" class="yes" id="chk_yes" runat="server" checked /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="">
                                                <input type="radio" name="1" class="no" runat="server" id="chk_no" /></span>
                                            否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                </td>
                            </tr>
                            <tr id="Caiwu" style="display: none;">
                                <td>
                                    不通过原因：
                                </td>
                                <td class="TextBoxBorder">
                                    <asp:TextBox ID="txt_caiwu" runat="server" MaxLength="100" TextMode="MultiLine" Height="50"
                                        Width="300"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="Suozhang" style="display: none;">
                                <td>
                                    不通过原因：
                                </td>
                                <td class="TextBoxBorder">
                                    <asp:TextBox ID="txt_suozhang" runat="server" MaxLength="100" TextMode="MultiLine"
                                        Height="50" Width="300"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="sureButton" runat="server" Text="保存" OnClick=" sureButton_Click"
                                        CssClass="btn green" />
                                    <input type="button" value="返回" onclick="javascript:history.back();" class=" btn default" />
                                </td>
                            </tr>
                        </table>
                        <table class="cls_show_cst_jiben" style="width: 800px; margin: 0 auto; font-size: 10pt;
                            display: none; color: red" id="tb_noDetails">
                            <tr>
                                <td>
                                    入账不通过或消息被删除！
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input type="button" value="返回" onclick="javascript:history.back();" class=" btn default" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--合同类型层-->
                    <div id="chooseCustomerCompact" class="modal fade yellow" tabindex="-1" data-width="620"
                        aria-hidden="true" style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            </button>
                            <h4 class="modal-title">
                                选取入账单据号</h4>
                        </div>
                        <div class="modal-body">
                            <table style="width: 560px;" class="cls_content_head">
                                <tr>
                                    <td style="width: 100px;" align="center">
                                        入账单据号
                                    </td>
                                    <td style="width: 260px;" align="center">
                                        汇款单位
                                    </td>
                                    <td style="width: 100px;" align="center">
                                        金额
                                    </td>
                                    <td style="width: 100px;" align="center">
                                        操作
                                    </td>
                                </tr>
                            </table>
                            <table id="customerCompactTable" class="cls_show_cst_jiben" align="center" style="width: 560px;">
                            </table>
                        </div>
                        <!--入账单据号-->
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default">
                                关闭</button>
                        </div>
                    </div>
                    <input id="HiddenAuditStatus" type="hidden" value='<%= ChargeAuditStatus %>' />
                    <input id="HiddenCprName" type="hidden" value='<%=CoperationName %>' />
                    <input id="HiddenCprUnit" type="hidden" value='<%=CoperationUnit %>' />
                    <input id="Hiddencm_FinancialNum" type="hidden" runat="server" />
                    <input type="hidden" id="hid_flag" runat="Server" value="0" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
