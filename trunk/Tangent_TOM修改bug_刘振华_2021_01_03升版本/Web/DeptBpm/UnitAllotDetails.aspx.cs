﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.DeptBpm
{
    public partial class UnitAllotDetails : PageBase
    {
        /// <summary>
        /// 考核任务
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public string RenwuNo
        {
            get
            {
                return this.drpRenwu.SelectedValue;
            }
        }
        /// <summary>
        /// 考核任务
        /// </summary>
        public TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));
                return model;
            }
        }
        /// <summary>
        /// 任务总奖金数
        /// </summary>
        public decimal? RenwuAllCount
        {
            get
            {
                decimal? result = 0;
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));

                if (model != null)
                {
                    result = model.JianAllCount;
                }
                return result;
            }
        }
        /// <summary>
        /// 是否已经保存
        /// </summary>
        public bool IsSave
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0} ", this.RenwuNo);
                var list = new TG.BLL.cm_KaoHeUnitAllotReport().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        /// <summary>
        /// 是否已经发起总分配调整
        /// </summary>
        public bool IsSendAllAllot
        {
            get {

                string strWhere = string.Format(" RenwuID={0}", this.RenwuNo);

                var list = new TG.BLL.cm_KaoHeAllMemsAllotRecord().GetModelList(strWhere);

                if(list.Count>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 比例
        /// </summary>
        public DataTable ProjAllotUnitList
        {

            get
            {
                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = string.Format(@"Select unit.unit_ID,unit.unit_Name
                                            ,isnull((select COUNT(*) From cm_KaoHeMemsMngAllot Where RenwuID={0} AND  UnitID=unit.unit_ID AND Stat=1),0) AS memCount
                                            ,(Select ID From cm_KaoHeRenwuUnit Where RenWuNo='{0}' AND UnitID=unit.unit_ID) AS KaoHeUnitID
                                            ,(Select top 1 insertUserID From dbo.cm_KaoHeMemsMngAllot Where RenwuID={0} And UnitID=unit.unit_ID) AS FromUserID
                                            From tg_unit unit join cm_DropUnitList DU on unit.unit_ID=DU.unit_ID join dbo.tg_unitExt ET on unit.unit_ID=ET.unit_ID
                                            Where unit.unit_ParentID<>0 AND unit.unit_ID<>230 AND DU.flag=0 AND ET.unit_Type<>3
                                            Order By ET.unit_Order ", renwuid);
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    strSql = string.Format(@"Select unit.unit_ID,unit.unit_Name
                                            ,isnull((select COUNT(*) From cm_KaoHeMemsMngAllot Where RenwuID={0} AND  UnitID=unit.unit_ID AND Stat=1),0) AS memCount
                                            ,(Select ID From cm_KaoHeRenwuUnit Where RenWuNo='{0}' AND UnitID=unit.unit_ID) AS KaoHeUnitID
                                            ,(Select top 1 insertUserID From dbo.cm_KaoHeMemsMngAllot Where RenwuID={0} And UnitID=unit.unit_ID) AS FromUserID
                                            From tg_unit unit join cm_DropUnitList DU on unit.unit_ID=DU.unit_ID join dbo.tg_unitExt ET on unit.unit_ID=ET.unit_ID
                                            Where unit.unit_ID={1} AND unit.unit_ParentID<>0 AND unit.unit_ID<>230 AND DU.flag=0 AND ET.unit_Type<>3 Order By unit.unit_ID", renwuid, this.drp_unit3.SelectedValue);
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }
        public DataTable ProjAllotUnitListOther
        {

            get
            {
                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = string.Format(@"
                                            Select unit.unit_ID,unit.unit_Name
                                            ,isnull((select COUNT(*) From cm_KaoHeMemsMngAllot Where RenwuID={0} AND  UnitID=unit.unit_ID AND Stat=1),0) AS memCount
                                            ,(Select ID From cm_KaoHeRenwuUnit Where RenWuNo='{0}' AND UnitID=unit.unit_ID) AS KaoHeUnitID
                                            ,(Select top 1 insertUserID From dbo.cm_KaoHeMemsMngAllot Where RenwuID={0} And UnitID=unit.unit_ID) AS FromUserID
                                            From tg_unit unit join cm_DropUnitList DU on unit.unit_ID=DU.unit_ID join dbo.tg_unitExt ET on unit.unit_ID=ET.unit_ID
                                            Where unit.unit_ParentID<>0 AND unit.unit_ID<>230 AND DU.flag=0 AND ET.unit_Type=3
                                            Order By ET.unit_Order ", renwuid);
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    strSql = string.Format(@"Select unit.unit_ID,unit.unit_Name
                                            ,isnull((select COUNT(*) From cm_KaoHeMemsMngAllot Where RenwuID={0} AND  UnitID=unit.unit_ID AND Stat=1),0) AS memCount
                                            ,(Select ID From cm_KaoHeRenwuUnit Where RenWuNo='{0}' AND UnitID=unit.unit_ID) AS KaoHeUnitID
                                            ,(Select top 1 insertUserID From dbo.cm_KaoHeMemsMngAllot Where RenwuID={0} And UnitID=unit.unit_ID) AS FromUserID
                                            From tg_unit unit join cm_DropUnitList DU on unit.unit_ID=DU.unit_ID join dbo.tg_unitExt ET on unit.unit_ID=ET.unit_ID
                                            Where unit.unit_ID={1} AND unit.unit_ParentID<>0 AND unit.unit_ID<>230 AND DU.flag=0 AND ET.unit_Type=3 Order By unit.unit_ID", renwuid, this.drp_unit3.SelectedValue);
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindRenwu();
            }
        }

        /// <summary>
        /// 绑定考核任务
        /// </summary>
        protected void BindRenwu()
        {
            if (RenwuList.Count > 0)
            {
                RenwuList.OrderByDescending(c=>c.StartDate).ToList().ForEach(c =>
                {
                    this.drpRenwu.Items.Add(new ListItem(c.RenWuNo, c.ID.ToString()));
                });
            }

        }

        protected void BindUnit()
        {

            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var list = bll_unit.GetModelList(" unit_ParentID<>0 AND unit_ID<>230 ");
            TG.BLL.cm_DropUnitList bll_drop = new BLL.cm_DropUnitList();
            var listShow = bll_drop.GetModelList(" flag=0 ");
            var listExt = new TG.BLL.tg_unitExt().GetModelList("");
            //查询不显示部门
            var query = from u in list
                        join p in listShow on u.unit_ID equals p.unit_ID
                        select u;

            var showList = query.ToList();
            //部门排序
            var result = from u in showList
                         join p in listExt on u.unit_ID equals p.unit_ID
                         orderby p.unit_Order ascending
                         select u;

            this.drp_unit3.DataSource = result.ToList();
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }

    }
}