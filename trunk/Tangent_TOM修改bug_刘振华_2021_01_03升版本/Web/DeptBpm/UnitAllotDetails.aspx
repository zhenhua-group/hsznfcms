﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitAllotDetails.aspx.cs" Inherits="TG.Web.DeptBpm.UnitAllotDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/DeptBpm/UnitAllotDetails.js?v=20170120"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">绩效考核 <small>绩效考核分配详情</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>绩效考核</a><i class="fa fa-angle-right"> </i><a>绩效考核分配详情</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>绩效考核
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">部门名称:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">考核任务:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drpRenwu" runat="server" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 150px;">
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                                    <%--<asp:Button CssClass="btn btn-sm red" ID="btn_Export" runat="server" Text="导出数据" />--%>
                                </td>
                                <td></td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th colspan="4">部门奖金详情列表
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="4">年度总奖金调整发起状态：
                                        <% if (!IsSendAllAllot)
                                           { %>
                                        <span class="badge badge-danger">未发起</span><a href="#" class="btn btn-sm blue" id="btnSave">发起调整年度总奖金</a>
                                        <%}
                                           else
                                           { %>
                                        <span class="badge badge-success">已发起</span><a href="#" class="btn btn-sm blue disabled">发起调整年度总奖金</a>
                                        <%} %>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 150px;">部门名称</th>
                                    <th style="width: 200px;">部门奖金调整是否完成</th>
                                    <th style="width: 100px;">操作</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--优先排序施工图部门-->
                                <% if (ProjAllotUnitList.Rows.Count > 0)
                                   {
                                       foreach (System.Data.DataRow dr in ProjAllotUnitList.Rows)
                                       {     
                                %>
                                <tr>
                                    <td><span unitid="<%= dr["unit_ID"].ToString() %>"><%= dr["unit_Name"].ToString() %></span></td>
                                    <td>
                                        <% if (dr["memCount"].ToString() != "0")
                                           { %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else
                                           { %>
                                        <span class="badge badge-danger">未完成</span>
                                        <%} %>
                                    </td>
                                    <td>
                                        <% if (dr["memCount"].ToString() != "0")
                                           { %>
                                        <a fromuserid="<%= dr["FromUserID"].ToString() %>" kaoheid="<%= dr["KaoHeUnitID"].ToString() %>" unitid="<%= dr["unit_ID"].ToString() %>" href="#" class="btn default btn-xs red-stripe">重新发起部门奖金调整</a>
                                        <%}
                                           else
                                           { %>
                                        <a kaoheid="<%= dr["KaoHeUnitID"].ToString() %>" unitid="<%= dr["unit_ID"].ToString() %>" href="#" class="btn default btn-xs red-stripe disabled">重新发起部门奖金调整</a>

                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                   } %>

                                <!--新加部门的类型区分加排序--->
                                <% if (ProjAllotUnitListOther.Rows.Count > 0)
                                   {
                                       foreach (System.Data.DataRow dr in ProjAllotUnitListOther.Rows)
                                       {     
                                %>
                                <tr>
                                    <td><span unitid="<%= dr["unit_ID"].ToString() %>"><%= dr["unit_Name"].ToString() %></span></td>
                                    <td>
                                        <% if (dr["memCount"].ToString() != "0")
                                           { %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else
                                           { %>
                                        <span class="badge badge-danger">未完成</span>
                                        <%} %>
                                    </td>
                                    <td>
                                        <% if (dr["memCount"].ToString() != "0")
                                           { %>
                                        <a fromuserid="<%= dr["FromUserID"].ToString() %>" kaoheid="<%= dr["KaoHeUnitID"].ToString() %>" unitid="<%= dr["unit_ID"].ToString() %>" href="#" class="btn default btn-xs red-stripe">重新发起部门奖金调整</a>
                                        <%}
                                           else
                                           { %>
                                        <a kaoheid="<%= dr["KaoHeUnitID"].ToString() %>" unitid="<%= dr["unit_ID"].ToString() %>" href="#" class="btn default btn-xs red-stripe disabled">重新发起部门奖金调整</a>

                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                   } %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%= RenwuNo %>" id="hidRenwuID" />
    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />

    <script>

        unitAllot.init();
    </script>
</asp:Content>
