﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ShowAllJudgeCountDetails.aspx.cs" Inherits="TG.Web.DeptBpm.ShowAllJudgeCountDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="../js/assets/plugins/data-tables/DT_bootstrap.css" />

    <script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/DT_bootstrap.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>考核汇总表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>部门考核考核汇总表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="table table-bordered table-hover" id="tbData">
        <thead>
            <tr>
                <th colspan="17" align="center"><span><%= KaoHeUnit.unit_Name %>&nbsp;
                                    <%if (RenwuModel.RenWuNo.ToString().ToCharArray()[RenwuModel.RenWuNo.ToString().Length - 1] == '1')
                                      { %>
                    <b>(<%= RenwuModel.RenWuNo.ToString()%>) </b>
                    <%}
                                      else
                                      {%>
                    <b>(<%= RenwuModel.RenWuNo.ToString()%>) </b>
                    <%} %>
                                    &nbsp;互评与自评打分表（部门汇总表） </span>
                    <a href="#" id="btnOrder" class="btn btn-sm red">排序</a>
                </th>

            </tr>
            <tr class="success">
                <th style="width: 75px;"><a href="#" id="btnDefault" title="点击恢复默认排序">评价人</a></th>
                <th style="width: 75px;">权重</th>
                <th style="width: 75px;">劳动强度</th>
                <th style="width: 75px;">工作效率</th>
                <th style="width: 85px;">工作主动性</th>
                <th style="width: 75px;">工作质量</th>
                <th style="width: 75px;">工作难度</th>
                <th style="width: 75px;">劳动纪律</th>
                <th style="width: 100px;">内部沟通能力</th>
                <th style="width: 110px;">合作与互助精神</th>
                <th style="width: 100px;">外部沟通能力</th>
                <th style="width: 110px;">组织与配合能力</th>
                <th style="width: 75px;">学习意愿</th>
                <th style="width: 110px;">公共事务积极性</th>
                <th style="width: 50px;">得分</th>
                <th style="width: 50px;">排名</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <%
                decimal? avg1 = 0;
                decimal? avg2 = 0;
                decimal? avg3 = 0;
                decimal? avg4 = 0;
                decimal? avg5 = 0;
                decimal? avg6 = 0;
                decimal? avg7 = 0;
                decimal? avg8 = 0;
                decimal? avg9 = 0;
                decimal? avg10 = 0;
                decimal? avg11 = 0;
                decimal? avg12 = 0;
                decimal? JudgeAllCount = 0;
                int userCount = JudgeReport.Count;
                //权重与得分乘
                decimal? AllCount = 0;
                decimal? AllWeights = 0;
                //临时值
                decimal? tempval = 0;

                int index = 1;
                int i = 0;
                JudgeReport.ForEach(c =>
                {
                    //avg1 += Convert.ToDecimal(dr["Judge1"].ToString());
                    //avg2 += Convert.ToDecimal(dr["Judge2"].ToString());
                    //avg3 += Convert.ToDecimal(dr["Judge3"].ToString());
                    //avg4 += Convert.ToDecimal(dr["Judge4"].ToString());
                    //avg5 += Convert.ToDecimal(dr["Judge5"].ToString());
                    //avg6 += Convert.ToDecimal(dr["Judge6"].ToString());
                    //avg7 += Convert.ToDecimal(dr["Judge7"].ToString());
                    //avg8 += Convert.ToDecimal(dr["Judge8"].ToString());
                    //avg9 += Convert.ToDecimal(dr["Judge9"].ToString());
                    //avg10 += Convert.ToDecimal(dr["Judge10"].ToString());
                    //avg11 += Convert.ToDecimal(dr["Judge11"].ToString());
                    //avg12 += Convert.ToDecimal(dr["Judge12"].ToString());
                    //JudgeAllCount += Convert.ToDecimal(dr["JudgeCount"].ToString());

                    avg1 += c.Judge1;
                    avg2 += c.Judge2;
                    avg3 += c.Judge3;
                    avg4 += c.Judge4;
                    avg5 += c.Judge5;
                    avg6 += c.Judge6;
                    avg7 += c.Judge7;
                    avg8 += c.Judge8;
                    avg9 += c.Judge9;
                    avg10 += c.Judge10;
                    avg11 += c.Judge11;
                    avg12 += c.Judge12;
                    JudgeAllCount += c.JudgeCount;

                    AllCount += c.Weights * c.JudgeCount;
                    AllWeights += c.Weights;
            %>
            <tr>
                <td><span class="badge badge-info"><%= c.MemName%></span></td>
                <td><span><%=c.Weights %></span></td>
                <td>
                    <span><%= c.Judge1%></span></td>
                <td>
                    <span><%= c.Judge2%></span></td>
                <td>
                    <span><%= c.Judge3%></span></td>
                <td>
                    <span><%= c.Judge4%></span></td>
                <td>
                    <span><%= c.Judge5%></span></td>
                <td>
                    <span><%= c.Judge6%></span></td>
                <td>
                    <span><%= c.Judge7%></span></td>
                <td>
                    <span><%= c.Judge8%></span></td>
                <td>
                    <span><%= c.Judge9%></span></td>
                <td>
                    <span><%= c.Judge10%></span></td>
                <td>
                    <span><%= c.Judge11%></span></td>
                <td>
                    <span><%= c.Judge12%></span></td>
                <td>
                    <span><%= c.JudgeCount%></span></td>
                <td>
                    <%  
                    if (i == 0)
                    {
                        tempval = c.JudgeCount;
                        i++;
                    }
                    else
                    {
                        if (tempval != c.JudgeCount)
                        {
                            tempval = c.JudgeCount;
                            index++;
                        }
                    }                    
                    %>
                   
                        <%--<b><%= index%></span></b>--%>
                         <b><span><%= c.IndexID%></span></b>

                </td>
                <td></td>
            </tr>
            <% });%>
        </tbody>
        <tfoot>
            <% if (userCount > 0)
               { %>
            <tr class="success">
                <td>平均值</td>
                <td></td>
                <td><%= Convert.ToDecimal(avg1 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg2 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg3 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg4 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg5 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg6 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg7 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg8 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg9 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg10 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg11 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(avg12 / userCount).ToString("f2") %></td>
                <td><%= Convert.ToDecimal(JudgeAllCount/userCount).ToString("f2") %></td>
                <td></td>
                <td>
                    <%if (AllWeights > 0)
                      { %>
                    最后得分：<span style="font-weight: bold; color: red;"><%= Convert.ToDecimal(AllCount/AllWeights).ToString("f2") %></span>
                    <%}
                      else
                      {%>
                    最后得分：<span style="font-weight: bold; color: red;">0.00</span>
                    <%} %>
                </td>
            </tr>
            <%} %>
            <tr>
                <td colspan="17" align="center">
                    <a href="/DeptBpm/ShowUserPress.aspx?uid=<%= KaoHeUnitSysNo%>&renwuno=<%=RenwuNo %>&id=<%= KaoHeUnitID%>" class="btn btn-xs default">返回</a>
                </td>
            </tr>
        </tfoot>
    </table>


    <script>

        $(function () {

            $("#btnOrder").click(function () {

                initOrderTable();
            });

            $("#btnDefault").click(function () {

                window.location.reload();
            });
        });

        //给table排序
        var initOrderTable = function () {

            $("#tbData").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [0] },
                    { "bSortable": false, "aTargets": [1] },
                    { "bSortable": true, "aTargets": [2] },
                    { "bSortable": true, "aTargets": [3] },
                    { "bSortable": true, "aTargets": [4] },
                    { "bSortable": true, "aTargets": [5] },
                    { "bSortable": true, "aTargets": [6] },
                    { "bSortable": true, "aTargets": [7] },
                    { "bSortable": true, "aTargets": [8] },
                    { "bSortable": true, "aTargets": [9] },
                    { "bSortable": true, "aTargets": [10] },
                    { "bSortable": true, "aTargets": [11] },
                    { "bSortable": true, "aTargets": [12] },
                    { "bSortable": true, "aTargets": [13] },
                    { "bSortable": true, "aTargets": [14] },
                    { "bSortable": true, "aTargets": [15] },
                    { "bSortable": false, "aTargets": [16] }
                ],
                "iDisplayLength": 50
            });


            jQuery('#tbData_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
            jQuery('#tbData_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
            //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
        }

        // 2016年10月20日 
        //initOrderTable();
    </script>
</asp:Content>
