﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="StartTesting.aspx.cs" Inherits="TG.Web.DeptBpm.StartTesting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/DeptBpm/StartTesting.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>部门考核列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>考核信息列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>任务名称：<%= RenwuNo %>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <td width="100">
                                    <b>选择任务计划:</b>
                                </td>
                                <td width="420">
                                    <%--<select id="selRenwu_1">
                                        <option value="0">--请选择--</option>
                                        <% RenwuList.ForEach(c =>
                                           { 
                                        %>
                                        <option value="<%=c.ID %>"><%=c.RenWuNo %></option>
                                        <%     }); %>
                                    </select>--%>

                                    <!--兼容历史数据-->
                                    <asp:DropDownList runat="server" ID="selRenwu" OnSelectedIndexChanged="selRenwu_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="--请选择--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>

                                    <%-- <% RenwuList.ForEach(c =>
                                           {
                                               if (c.ID == RenwuId)
                                               {%>
                                    <input type="checkbox" name="name" action="renwu" value="<%= c.ID %>" checked="checked" /><span style="font-weight: bold; color: blue;"><%=c.RenWuNo %></span>
                                    <%    }
                                               else
                                               {%>
                                    <input type="checkbox" name="name" action="renwu" value="<%= c.ID %>" /><span style="font-weight: bold; color: blue;"><%=c.RenWuNo %></span>
                                    <% 
                                               }
                                           }); %>--%>
                                </td>
                                <td>
                                    <% if(!CurRenwuIsBak){ %>
                                    <a href="#" class="btn btn-sm default disabled" id="btnSetUnit">设置考核部门</a>
                                    <%} %>
                                    <a href="/DeptBpm/DeptBpmList.aspx" class="btn btn-sm blue">返回</a>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                    <th style="width: 30px;">
                                        <input type="checkbox" name="name" value="0" id="chkAll" /></th>
                                    <th style="width: 50px;">序号</th>
                                    <th style="width: 150px;">部门名称</th>
                                    <th style="width: 150px;">是否参与</th>
                                    <th style="width: 150px;">考核人员</th>
                                    <th style="width: 150px;">考核状态</th>
                                    <th style="width: 120px;">操作</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;
                                   UnitList.ForEach(c =>
                                  { %>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="name" value="0" class="chkitem" uname="<%= c.unit_Name.Trim() %>" uid="<%=c.unit_ID %>" /></td>
                                    <td><%= index++ %></td>
                                    <td><%= c.unit_Name.Trim() %></td>
                                    <td>
                                        <% if (!SpecialUnitList.Contains(c.unit_ID))
                                           { %>
                                        <select>
                                            <option value="0">是</option>
                                            <option value="1">否</option>
                                        </select>
                                        <%}
                                           else
                                           { %>
                                        <select>
                                            <option value="1">否</option>
                                            <option value="0">是</option>
                                        </select>
                                        <%} %>
                                    </td>
                                    <td>
                                        <a href="#" class="btn default btn-xs green-stripe disabled" asetmem="setusers">人员设置</a>
                                    </td>
                                    <td><span class="badge badge-sm badge-danger">未提交</span></td>
                                    <td>
                                        <% if (!SpecialUnitList.Contains(c.unit_ID))
                                           { %>
                                        <a href="#" class="btn default btn-xs red-stripe disabled" action="sendmsg" uid="<%=c.unit_ID %>" ukaoheid="0" urenwuno="0" senduserid="<% =UserSysNo %>">提交考核</a>
                                        <%}
                                           else
                                           { %>
                                        <a href="#" class="btn default btn-xs blue-stripe" action="sendallot" uid="<%=c.unit_ID %>" ukaoheid="0" urenwuno="0" senduserid="<% =UserSysNo %>">发起奖金调整</a>
                                        <%} %>
                                    </td>
                                    <td>
                                        <% if (!SpecialUnitList.Contains(c.unit_ID))
                                           { %>
                                        <a href="#" style="display: none;" class="btn default btn-xs yellow-stripe" action="resendmsg" uid="<%=c.unit_ID %>" ukaoheid="0" urenwuno="0" senduserid="<% =UserSysNo %>">重新提交</a>
                                        <a href="#" style="display: none;" class="btn default btn-xs blue-stripe" action="sendonce" uid="<%=c.unit_ID %>" ukaoheid="0" urenwuno="0" senduserid="<% =UserSysNo %>">一键提交</a>
                                        <%}%>
                                                                    
                                    </td>
                                </tr>
                                <%}); %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%= RenwuId %>" id="hidRenwuId" />
    <input type="hidden" name="name" value="<%= LocalUrl %>" id="hidLocalUrl" />

    <input type="hidden" name="name" value="<%= ProcessRenwuID %>" id="hidProcessRenwuID" />

    <input type="hidden" name="name" value="<%= SpecialUnitListStr %>" id="hidSpecialUnitListStr"/>

    <script>
        StartTest.init();
    </script>
</asp:Content>
