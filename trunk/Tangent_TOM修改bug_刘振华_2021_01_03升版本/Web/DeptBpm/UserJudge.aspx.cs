﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.DeptBpm
{
    public partial class UserJudge : PageBase
    {
        //uid=230&ukaoheid=1&renwuno=4&MsgNo=49669
        /// <summary>
        /// 考核部门ID
        /// </summary>
        protected int KaoHeUnitSysNo
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["uid"] ?? "0", out unitid);
                return unitid;
            }
        }

        protected TG.Model.tg_unit KaoHeUnit
        {
            get
            {
                return new TG.BLL.tg_unit().GetModel(this.KaoHeUnitSysNo);
            }
        }
        /// <summary>
        /// 部门考核ID
        /// </summary>
        protected int KaoHeUnitID
        {
            get
            {
                int ukaoheid = 0;
                int.TryParse(Request["ukaoheid"] ?? "0", out ukaoheid);
                return ukaoheid;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        protected int RenwuNo
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["renwuno"] ?? "0", out renwuid);
                return renwuid;
            }
        }
        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }
        /// <summary>
        /// 任务实体
        /// </summary>
        protected TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                TG.Model.cm_KaoHeRenWu model = new TG.BLL.cm_KaoHeRenWu().GetModel(this.RenwuNo);
                return model;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected int MsgSysNo
        {
            get
            {
                int msgsysid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgsysid);
                return msgsysid;
            }
        }
        /// <summary>
        /// 当前用户的管理角色
        /// </summary>
        public TG.Model.tg_memberRole CurUserRole
        {
            get
            {
                string strWhere = string.Format(" mem_id={0} ", UserSysNo);
                var role = new TG.BLL.tg_memberRole().GetModelList(strWhere);
                return role[0];
            }
        }
        /// <summary>
        /// 需要打分的用户
        /// </summary>
        public List<TG.Model.cm_KaoHeRenwuMem> KaoHeMems
        {
            get
            {
                string strWhere = string.Format(" KaoHeUnitID={0} AND memUnitID={1} AND Statu=0 AND MemID NOT IN ({2})", KaoHeUnitID, KaoHeUnitSysNo, GuDongIdStr);
                var mems = new TG.BLL.cm_KaoHeRenwuMem().GetModelList(strWhere);

                //本部门人员
                string strWhere2 = string.Format(" mem_Unit_ID={0}", KaoHeUnitSysNo);
                var list = new TG.BLL.tg_member().GetModelList(strWhere2);
                //连接排序
                var query = from km in mems
                            join m in list on km.memID equals m.mem_ID
                            orderby m.mem_Order ascending
                            select km;
                
                return query.ToList<TG.Model.cm_KaoHeRenwuMem>();
            }
        }
        /// <summary>
        /// 用户评论数
        /// </summary>
        public List<TG.Model.cm_UserJudgeReport> UserJudges
        {
            get
            {
                string strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1} AND MemID NOT IN ({2})", KaoHeUnitID, UserSysNo, GuDongIdStr);
                var pls = new TG.BLL.cm_UserJudgeReport().GetModelList(strWhere).OrderBy(c => c.ID).ToList();

                //本部门人员
                string strWhere2 = string.Format(" mem_Unit_ID={0}", KaoHeUnitSysNo);
                var list = new TG.BLL.tg_member().GetModelList(strWhere2);
                //连接排序
                var query = from km in pls
                            join m in list on km.MemID equals m.mem_ID
                            orderby m.mem_Order ascending
                            select km;

                return query.ToList<TG.Model.cm_UserJudgeReport>();
            }
        }
        /// <summary>
        /// 股东ID  2017年1月17日
        /// </summary> 
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        /// <summary>
        /// 是否已经存在
        /// </summary>
        public bool IsExsitJudge
        {
            get
            {
                string strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1} AND SaveStatu=0", KaoHeUnitID, UserSysNo);
                var pls = new TG.BLL.cm_UserJudgeReport().GetModelList(strWhere);
                if (pls.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 是否已经提交
        /// </summary>
        public bool IsSubmit
        {
            get
            {
                string strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1} AND SaveStatu=1", KaoHeUnitID, UserSysNo);
                var pls = new TG.BLL.cm_UserJudgeReport().GetModelList(strWhere);
                if (pls.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}