﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="DeptBpmList.aspx.cs" Inherits="TG.Web.DeptBpm.DeptBpmList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/js/wdate/WdatePicker.js"></script>
    <script src="/js/DeptBpm/DeptBpmList.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考核任务 <small>考核任务列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>考核任务</a><i class="fa fa-angle-right"> </i><a>考核任务列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-6">
            <!--考核列表-->
            <% if (CurrentUserLogin != UserShortName)
               { %>
            <div class="portlet box blue" id="divRenWuList">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>考核列表
                    </div>
                    <div class="actions">
                        <a href="#" class="btn btn-sm red" id="btnIsNew">新建任务</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <thead>
                                <tr>
                                    <th style="width: 70px; text-align: center;">任务编号</th>
                                    <th style="width: 70px; text-align: center;">建立时间</th>
                                    <th style="width: 130px; text-align: center;">起始-截止时间</th>
                                    <th style="width: 70px; text-align: center;">项目考核</th>
                                    <th style="width: 70px; text-align: center;">部门考核</th>
                                    <th style="width: 80px; text-align: center;">操作</th>
                                    <% if (UserShortName == "admin")
                                       { 
                                       %>
                                    <th style="width: 60px; text-align: center;">归档</th>
                                       <%}%>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%= HtmlContainer %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <%} %>
            <!-- 考核统计-->

            <div class="portlet box blue" id="divKaoHeSum">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>考核统计
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <thead>
                                <tr>
                                    <th style="width: 100px;" rowspan="2">考核任务</th>
                                    <th colspan="2">技术部门(非股东)项目奖金+部门内奖金(元)</th>
                                    <th rowspan="2">技术部门(非股东)平均月工资合计(元)</th>
                                    <th rowspan="2">奖金基数=(项目奖金+部门内奖金)/平均月工资</th>
                                    <th rowspan="2"></th>
                                    <th rowspan="2"></th>
                                </tr>
                                <tr>
                                    <th style="width: 200px;">实际发放/系统计算值</th>
                                    <th style="width: 100px;">初始录入</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%= HtmlContainer2 %> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <!--考核列表-->
            <% if (CurrentUserLogin != UserShortName)
               { %>
            <div class="portlet box blue" id="divRenwuAllSet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>任务编辑
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tbRenwu">
                            <tr>
                                <td style="width: 80px;">任务编号：</td>
                                <td>
                                    <input type="text" name="name" value=" " id="txtRenWuNo" />例:201601(上半年)，201602(下半年)
                                </td>
                            </tr>
                            <tr>
                                <td>起始时间：</td>
                                <td>
                                    <input type="text" name="name" value=" " id="txtStartDate" onclick="WdatePicker({ readOnly: true })" class="Wdate" />
                                </td>

                            </tr>
                            <tr>
                                <td>截止时间：</td>
                                <td>
                                    <input type="text" name="name" value=" " id="txtEndDate" onclick="WdatePicker({ readOnly: true })" class="Wdate" />
                                </td>
                            </tr>
                            <tr>
                                <td>备注信息：</td>
                                <td>
                                    <input type="text" name="name" value=" " id="txtSub" />
                                </td>
                            </tr>
                            <tr>
                                <td>项目考核：</td>
                                <td>
                                    <select id="projStatus">
                                        <option value="进行中">进行中</option>
                                        <option value="暂停中">暂停中</option>
                                        <option value="已完成">已完成</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>部门考核：</td>
                                <td>
                                    <select id="deptStatus">
                                        <option value="进行中">进行中</option>
                                        <option value="暂停中">暂停中</option>
                                        <option value="已完成">已完成</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><a href="#" class="btn btn-sm red" id="btnSave">保存</a></td>
                               
                            </tr>
                        </table>

                        <table id="tbJiangJin" style="display: none;">
                            <tr>
                                <td style="width: 80px;">总奖金数：</td>
                                <td style="width: 170px;">￥<input type="text" name="name" value="" style="width: 130px; height: 28px; font-size: 13pt; font-weight: bold;" id="txtAllCount" />元
                                </td>
                                <td>
                                    <a href="#" id="btnSaveJJ" class="btn btn-sm red">保存</a>
                                    <a href="#" id="btnSaveJJedit" class="btn btn-sm green" style="display: none;">修改</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>
    <!--加载js-->
    <input type="hidden" name="name" value="0" id="hidIsNew" />
    <input type="hidden" name="name" value="<%=UserSysNo %>" id="hidUserSysNo">
    <input type="hidden" name="name" value="<%= DateTime.Now.ToShortDateString() %>" id="hidCurDate" />
    <input type="hidden" name="name" value="0" id="hidRecordID" />
    <script>
        DeptBpm.Init();
    </script>
</asp:Content>
