﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="StartUnitAllot.aspx.cs" Inherits="TG.Web.DeptBpm.StartUnitAllot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/DeptBpm/StartUnitAllot.js"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>部门奖金分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>部门奖金分配</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>部门奖金计算
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px; display: none;">项目部门:
                                </td>
                                <td style="width: 120px; display: none;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged"
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">考核任务:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drpRenwu" runat="server" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="drpRenwu_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 150px;">
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" runat="server" Text="刷新" OnClick="btn_Search_Click" />
                                    <asp:Button CssClass="btn btn-sm red" Enabled="true" ID="btn_Export" runat="server" Text="导出数据" OnClick="btn_Export_Click" />
                                </td>
                                <td></td>
                            </tr>
                        </table>


                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th>初始录入总奖金数：</th>
                                    <th><span class="badge badge-danger" id="spAllcount"><%=Math.Round(Convert.ToDecimal(RenwuAllCount),2) %></span></th>
                                    <th colspan="10">
                                        <% if (UserShortName == "admin")
                                           {

                                               if (!this.IsBak)
                                               {%>
                                        <a href="#" class="btn btn-sm yellow" id="btnBak">发起归档</a>
                                        <%}
                                               else
                                               { %>
                                        <a href="#" class="btn btn-sm default disabled">已归档</a>
                                        <%       }
                                           } %>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 140px;">部门名称</th>
                                    <th style="width: 90px;">平均月工资</th>
                                    <th style="width: 80px;">部门绩效</th>
                                    <th style="width: 100px;">部门奖金占比</th>
                                    <th style="width: 90px;">部门总奖金</th>
                                    <th style="width: 90px;">月工资倍数</th>
                                    <th style="width: 80px;">项目奖金</th> 
                                    <th style="width: 100px;">项目奖金占比</th>
                                    <th style="width: 90px;">部门内奖金</th>
                                    <th style="width: 90px;">

                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>下半年
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year %>上半年
                                        <%} %>
                                    </th>
                                    <th style="width: 90px;">

                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>上半年
                                    <%}
                                           else
                                           { %>

                                        <%= RenwuModel.StartDate.Year-1 %>下半年
                                    <%} %>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    decimal? hj = 0;
                                    decimal? hj1 = 0;
                                    decimal? hj2 = 0;
                                    decimal? hj3 = 0;
                                    decimal? hj4 = 0;
                                    decimal? hj5 = 0;
                                    decimal? hj6 = 0;
                                    if (!IsSave)
                                    {

                                        if (ProjAllotUnitList.Rows.Count > 0)
                                        {
                                            decimal? jiangbi = 0;
                                            decimal? renbi = 0;
                                            decimal? gongzbi = 0;

                                            hj = 0;
                                            hj1 = 0;
                                            hj2 = 0;
                                            hj3 = 0;
                                            //判断是否归档
                                            bool isprevbak = IsPrevYearBak;
                                            bool isprevtwobak = IsPrevYearTwoBak;
                                            
                                            foreach (System.Data.DataRow dr in ProjAllotUnitList.Rows)
                                            { %>
                                <tr>
                                    <td><span unitid="<%= dr["unit_ID"].ToString() %>"><%= dr["unit_Name"].ToString() %></span></td>
                                    <td style="text-align: right;"><% hj += Math.Round(Convert.ToDecimal(dr["AvgBumemGz"].ToString()), 2); %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["AvgBumemGz"].ToString()),2) %></span></td>
                                    <td>
                                        <input val="<%= Convert.ToDecimal(dr["AvgBumemGz"].ToString())%>" valall="<%= Convert.ToDecimal(dr["AvgBumemGzAll"].ToString())%>" type="text" name="name" value="" style="width: 60px;" />
                                    </td>
                                    <td style="text-align: right;">
                                        <span>0</span>%
                                    </td>
                                    <td style="text-align: right;">
                                        <span>0</span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span>0</span>
                                    </td>
                                    <td style="text-align: right;"><% hj1 += Math.Floor(Convert.ToDecimal(dr["AllotCount"].ToString())); %>
                                        <span><%= Math.Floor(Convert.ToDecimal(dr["AllotCount"].ToString())) %></span>
                                    </td>                            
                                    <td style="text-align: right;"><span>0</span>%</td>
                                    <td style="text-align: right;"><span>0</span></td>

                                    <td style="text-align: right;">

                                        <% 
                                                if ((RenwuModel.StartDate.Year - 1) < 2016 && (RenwuModel.StartDate.Month < 8))
                                                {
                                                    hj3 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>
                                        <%}
                                                else
                                                {
                                                    if (isprevbak)
                                                    {
                                                        hj3 += Convert.ToDecimal(dr["xbn"].ToString());

                                                        //加上建筑二部的
                                                        if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                                        {
                                                            hj3 += 401600;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        hj3 += Convert.ToDecimal(dr["xbn_unbak"].ToString());
                                                    }
                                        %>

                                        <!--start-->
                                        <% if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn"].ToString())) + 401600%></span>
                                        <% }
                                           else
                                           {
                                               if (isprevbak)
                                               {
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn"].ToString()))%></span>
                                        <%}
                                               else
                                               {%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn_unbak"].ToString()))%></span>
                                        <%}
                                           } %>

                                        <!--- end -->


                                        <% }%>
                                        
                                    </td>
                                    <td style="text-align: right;">

                                        <% 
                                                if ((RenwuModel.StartDate.Year - 1) < 2016)
                                                {
                                                    if (RenwuModel.StartDate.Month >= 8)
                                                    {
                                                        hj2 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>
                                        <%}
                                                    else
                                                    {
                                                        hj2 += Convert.ToDecimal(dr["sbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn2"].ToString())) %></span>
                                        <%
                                                    }
                                        %>
                                        <%}
                                                else
                                                {
                                                    if (isprevtwobak)
                                                    {
                                                        hj2 += Convert.ToDecimal(dr["sbn"].ToString());

                                                        //加上部门合并的数值
                                                        if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                                        {
                                                            hj2 += 324200;
                                                        }//2017 xiabannian
                                                        else if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month >= 8) && dr["unit_ID"].ToString() == "231")
                                                        {
                                                            hj2 += 401600;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        hj2 += Convert.ToDecimal(dr["sbn_unbak"].ToString());
                                                    }
                                        %>

                                        <!-- start-->
                                        <% if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn"].ToString()))+324200+15 %></span>
                                        <%}
                                           else if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month >= 8) && dr["unit_ID"].ToString() == "231")
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn"].ToString())) + 401600%></span>
                                        <% }
                                           else
                                           {
                                               if (isprevtwobak)
                                               {%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn"].ToString()))%></span>
                                        <%}
                                               else
                                               {%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn_unbak"].ToString()))%></span>

                                        <%  }
                                           } %>
                                        <!---end--->


                                        <% }%>

                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                        }
                                    }
                                    else
                                    {
                                        hj = 0;
                                        hj1 = 0;
                                        hj2 = 0;
                                        hj3 = 0;
                                        hj4 = 0;
                                        hj5 = 0;
                                        hj6 = 0;

                                        //判断是否归档
                                        bool isprevbak = IsPrevYearBak;
                                        bool isprevtwobak = IsPrevYearTwoBak;
                                        
                                        foreach (System.Data.DataRow dr in UnitAllotList.Rows)
                                        {
                                %>
                                <tr>
                                    <td><span unitid="<%= dr["unit_ID"].ToString() %>"><%= dr["unit_Name"].ToString() %></span></td>
                                    <td style="text-align: right;"><% hj += Math.Round(Convert.ToDecimal(dr["AvgBumemGz"].ToString()), 2); %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["AvgBumemGz"].ToString()),2) %></span></td>
                                    <td style="text-align: right;">
                                        <%  if (dr["Stat"].ToString() == "0")
                                            {%>
                                        <input val="<%= Convert.ToDecimal(dr["AvgBumemGz"].ToString())%>" valall="<%= Convert.ToDecimal(dr["AvgBumemGzAll"].ToString())%>" type="text" name="name" value="<%= dr["Unitjx"].ToString() %>" style="width: 60px;" />
                                        <%}
                                            else
                                            { %>
                                        <span><%= dr["Unitjx"].ToString() %></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hj1 += Convert.ToDecimal(dr["Unitjiangbi"].ToString()); %>
                                        <span><%= Convert.ToDecimal(dr["Unitjiangbi"].ToString()) %></span>%
                                    </td>
                                    <td style="text-align: right;">
                                        <% hj2 += Convert.ToDecimal(dr["Unitjiangall"].ToString()); %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["Unitjiangall"].ToString())) %></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= Convert.ToDecimal(dr["MonthBei"].ToString()) %></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <%  if (dr["unit_Name"].ToString() == "特务一部" | dr["unit_Name"].ToString() == "特务二部" | dr["unit_Name"].ToString() == "特务三部" | dr["unit_Name"].ToString() == "室内部")
                                            {%>
                                             <span>0.00</span>
                                       
                                            <%}
                                            else
                                            { %>
                                             <% hj3 += Convert.ToDecimal(Math.Floor(Convert.ToDecimal(dr["AllotCount"].ToString()))); %>
                                        <span><%= Convert.ToDecimal(Math.Floor(Convert.ToDecimal(dr["AllotCount"].ToString()))) %></span>
                                        <%} %>
                                        
                                          </td>



                                    <td style="text-align: right;"><span><%= Convert.ToDecimal(dr["ProjCountbi"].ToString()) %></span>%</td>
                                    <td style="text-align: right;"><% hj4 += Convert.ToDecimal(dr["Unitjiang"].ToString()); %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["Unitjiang"].ToString())) %></span></td>

                                    <td style="text-align: right;">

                                        <%if (!this.IsBak)
                                          { %>

                                        <!-- start -->
                                        <% 
                                              if ((RenwuModel.StartDate.Year - 1) < 2016 && (RenwuModel.StartDate.Month < 8))
                                              {
                                                  hj6 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>

                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>

                                        <%}
                                              else
                                              {
                                                  //判断是否归档
                                                  if (isprevbak)
                                                  {
                                                      hj6 += Convert.ToDecimal(dr["xbn"].ToString());
                                                      //加上建筑二部的
                                                      if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                                      {
                                                          hj6 += 401600;
                                                      }
                                                  }
                                                  else
                                                  {
                                                      hj6 += Convert.ToDecimal(dr["xbn_unbak"].ToString());
                                                  }
                                        %>

                                        <!---start--->
                                        <% if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn"].ToString())) + 401600%></span>
                                        <% }
                                           else
                                           {
                                               if (isprevbak)
                                               {
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn"].ToString()))%></span>
                                        <%}
                                               else
                                               {%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn_unbak"].ToString()))%></span>
                                        <%}
                                           } %>

                                        <!--- end -->


                                        <% }%>

                                        <!-- end -->
                                        <%}
                                          else
                                          {
                                              hj6 += Convert.ToDecimal(dr["BeforeYear"].ToString());
                                              
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["BeforeYear"].ToString())) %></span>

                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">


                                        <% if (!this.IsBak)
                                           { %>

                                        <% 
                                               if ((RenwuModel.StartDate.Year - 1) < 2016)
                                               {
                                                   if (RenwuModel.StartDate.Month >= 8)
                                                   {
                                                       hj5 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>
                                        <%}
                                                   else
                                                   {
                                                       hj5 += Convert.ToDecimal(dr["sbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn2"].ToString())) %></span>
                                        <%
                                                   }
                                        %>
                                        <%}
                                               else
                                               {
                                                   if (isprevtwobak)
                                                   {
                                                       hj5 += Convert.ToDecimal(dr["sbn"].ToString());

                                                       //加上部门合并的数值
                                                       if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                                       {
                                                           hj5 += 324200;
                                                       }
                                                       else if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month >= 8) && dr["unit_ID"].ToString() == "231")
                                                       {
                                                           hj5 += 401600;
                                                       }
                                                   }
                                                   else
                                                   {
                                                       hj5 += Convert.ToDecimal(dr["sbn_unbak"].ToString());
                                                   }
                                        %>

                                        <!-- start-->
                                        <% if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month < 8) && dr["unit_ID"].ToString() == "231")
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn"].ToString()))+324200+15 %></span>
                                        <%}
                                           else if (RenwuModel.StartDate.Year == 2017 && (RenwuModel.StartDate.Month >= 8) && dr["unit_ID"].ToString() == "231")
                                           {%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn"].ToString())) + 401600%></span>
                                        <%}
                                           else
                                           {
                                               if (isprevtwobak)
                                               {%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn"].ToString()))%></span>
                                        <%}
                                               else
                                               {%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn_unbak"].ToString()))%></span>
                                        <% }
                                           } %>
                                        <!---end--->

                                        <% }%>

                                        <%}
                                           else
                                           {

                                               hj5 += Convert.ToDecimal(dr["AfterYear"].ToString());

                                        %>

                                        <span><%= Math.Round(Convert.ToDecimal(dr["AfterYear"].ToString())) %></span>

                                        <%} %>

                                    </td>
                                    <td></td>
                                </tr>
                                <% }
                                    }%>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <% if (!IsSave)
                                       { %>
                                    <td>合计</td>
                                    <td style="text-align: right;"><%= hj %></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= hj1 %></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= hj3 %></td>
                                    <td style="text-align: right;"><%= hj2 %></td>

                                    <td></td>
                                    <%}
                                       else
                                       { %>
                                    <td>合计</td>
                                    <td style="text-align: right;"><%= hj %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= hj1 %>%</td>
                                    <td style="text-align: right;"><%= Math.Floor(Convert.ToDecimal(hj2)) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= hj3 %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Floor(Convert.ToDecimal(hj4)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hj6)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hj5)) %></td>

                                    <td></td>
                                    <%} %>
                                </tr>
                                <tr>
                                    <td colspan="11" align="center">
                                        <% if (!IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit">确认提交</a><span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>

                                        <%}
                                           else
                                           { %>

                                        <a href="#" class="btn btn-sm red disabled" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red disabled" id="btnSubmit">确认提交</a>
                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="name" value="<%=RenwuNo %>" id="hidRenwuNo" />
    <input type="hidden" name="name" value="<%= IsSave %>" id="hidSaved" />
    <input type="hidden" name="name" value="<%= IsSubmit %>" id="hidSubmit" />
    <script>
        unitAllot.init();
    </script>
</asp:Content>
