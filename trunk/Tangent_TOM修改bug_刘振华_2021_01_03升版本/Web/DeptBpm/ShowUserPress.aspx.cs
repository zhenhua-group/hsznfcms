﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.DeptBpm
{
    public partial class ShowUserPress : PageBase
    {
        /// <summary>
        /// 考核部门ID
        /// </summary>
        protected int KaoHeUnitSysNo
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["uid"] ?? "0", out unitid);
                return unitid;
            }
        }

        protected TG.Model.tg_unit KaoHeUnit
        {
            get
            {
                return new TG.BLL.tg_unit().GetModel(this.KaoHeUnitSysNo);
            }
        }
        /// <summary>
        /// 部门考核ID
        /// </summary>
        protected int KaoHeUnitID
        {
            get
            {
                int ukaoheid = 0;
                int.TryParse(Request["id"] ?? "0", out ukaoheid);
                return ukaoheid;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        protected int RenwuNo
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["renwuno"] ?? "0", out renwuid);
                return renwuid;
            }
        }


        public bool IsSendMsgToMng
        {
            get
            {
                string strWhere = string.Format(" ID={0} AND IsSendMng=1", this.KaoHeUnitID);

                var list = new TG.BLL.cm_KaoHeRenwuUnit().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 需要打分的用户
        /// </summary>
        public DataTable KaoHeMems
        {
            get
            {
                //2017年8月2日 按人员表排序
                string strSql = string.Format(@"select *,
	                        (case (select COUNT(*) from dbo.cm_UserJudgeReport Where KaoHeUnitID=A.KaoHeUnitID AND SaveStatu=1 AND InsertUserId=A.memID) when 0 then '未提交' else '已提交' end) IsSubmit
                            ,isnull((select roleName from cm_RoleMag where ID=A.MagRole),'') As MagName
                            ,isnull((select roleName from cm_RoleMag where ID=A.NormalRole),'') As TecName
                            ,isnull((select unit_name from tg_unit where unit_Id=A.IsOutUnitID),'本部门') AS OutUnitName
                        ,(Select COUNT(*)
                        From dbo.cm_UserJudgeReport
                        Where ((Judge1 IS NULL) 
	                        OR (Judge2 IS NULL) 
	                        OR (Judge3 IS NULL) 
	                        OR (Judge4 IS NULL) 
	                        OR (Judge5 IS NULL) 
	                        OR (Judge6 IS NULL) 
	                        OR (Judge7 IS NULL) 
	                        OR (Judge8 IS NULL) 
	                        OR (Judge9 IS NULL) 
	                        OR (Judge10 IS NULL) 
	                        OR (Judge11 IS NULL) 
	                        OR (Judge12 IS NULL)) AND KaoHeUnitID=A.KaoHeUnitID AND InsertUserId=A.memID) AS IsComplete
                            from cm_KaoHeRenwuMem A Left JOIN tg_member M on A.memID = M.mem_ID
                            where KaoHeUnitID={0} AND memUnitID={1} AND Statu=0
                            Order By M.mem_Order ASC ", this.KaoHeUnitID, this.KaoHeUnitSysNo);
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
                return dt;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}