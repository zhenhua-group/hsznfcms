﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Text;

namespace TG.Web.DeptBpm
{
    public partial class UnitAllAllotRecordCompare : PageBase
    {
        /// <summary>
        /// 总奖金分配
        /// </summary>
        public string RecordUrl
        {
            get
            {
                return "/DeptBpm/UnitAllAllotRecord.aspx?unitid=" + this.drp_unit3.SelectedValue + "&renwuno=" + RenwuNo + "&action=" + ActionDo;
            }
        }
        /// <summary>
        /// 数据对比
        /// </summary>
        public string RecordCompareUrl
        {
            get
            {
                return "/DeptBpm/UnitAllAllotRecordCompare.aspx?unitid=" + CurUnitID + "&renwuno=" + RenwuNo + "&action=" + ActionDo;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public int RenwuNo
        {
            get
            {
                int renwuno = 0;
                int.TryParse(Request["renwuno"] ?? "0", out renwuno);
                return renwuno;
            }
        }
        /// <summary>
        /// 选中的ID
        /// 
        /// </summary>
        public int CurUnitID
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["unitid"] ?? "0", out unitid);
                if (unitid == 0)
                {
                    unitid = Convert.ToInt32(this.drp_unit3.SelectedValue);
                }
                return unitid;
            }
        }
        /// <summary>
        /// Action
        /// </summary>
        public int ActionDo
        {
            get
            {
                int id = 0;
                int.TryParse(Request["action"] ?? "0", out id);
                return id;
            }
        }
        /// <summary>
        /// 考核任务
        /// </summary>
        public TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(this.RenwuNo);
                return model;
            }
        }

        public bool IsBak
        {
            get
            {
                int allcount = 0;
                //当前职务状态有备份状态
                string strWhere = string.Format(" RenwuID={0} AND IsFired='{1}'", this.RenwuNo, this.drpZw.SelectedItem.Text);
                allcount = new TG.BLL.cm_KaoHeAllMemsCompareHis().GetRecordCount(strWhere);

                //查看部门下备份情况
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                    {
                        strWhere = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '建筑一部%' OR UnitName like '建筑二部%') ", this.RenwuNo, this.drpZw.SelectedItem.Text);
                    }
                    else
                    {
                        strWhere = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '{2}%') ", this.RenwuNo, this.drpZw.SelectedItem.Text, this.drp_unit3.SelectedItem.Text.Trim());
                    }
                }

                int count = new TG.BLL.cm_KaoHeAllMemsCompareHis().GetRecordCount(strWhere);

                if (allcount > 0)
                {
                    return true;
                }
                else if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<TG.Model.cm_KaoHeAllMemsCompareHis> AllHisList
        {
            get
            {
                string strWhere = string.Format(" RenwuID={0}", this.RenwuNo);

                if (this.drp_unit3.SelectedValue != "-1")
                {
                    if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                    {
                        strWhere = string.Format(" RenwuID={0} AND (UnitName like'建筑一部%' OR UnitName like '建筑二部%')", this.RenwuNo, this.drp_unit3.SelectedItem.Text.Trim());
                    }
                    else
                    {
                        strWhere = string.Format(" RenwuID={0} AND UnitName='{1}'", this.RenwuNo, this.drp_unit3.SelectedItem.Text.Trim());
                    }
                }

                //离职 在职
                strWhere += string.Format(" AND IsFired='{0}'", this.drpZw.SelectedItem.Text);

                return new TG.BLL.cm_KaoHeAllMemsCompareHis().GetModelList(strWhere).OrderBy(c => c.ID).ToList(); ;
            }
        }

        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var list = bll_unit.GetModelList(" unit_ParentID<>0 AND unit_ID<>230 ");
            TG.BLL.cm_DropUnitList bll_drop = new BLL.cm_DropUnitList();
            var listShow = bll_drop.GetModelList(" flag=0 ");
            var listExt = new TG.BLL.tg_unitExt().GetModelList("");
            //查询不显示部门
            var query = from u in list
                        join p in listShow on u.unit_ID equals p.unit_ID
                        select u;

            var showList = query.ToList();
            //部门排序
            var result = from u in showList
                         join p in listExt on u.unit_ID equals p.unit_ID
                         orderby p.unit_Order ascending
                         select u;

            this.drp_unit3.DataSource = result.ToList();
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }
        /// <summary>
        ///// 上半年
        ///// </summary>
        //protected string SBNWhere
        //{
        //    get
        //    {
        //        //上半年考核
        //        if (RenwuModel.StartDate.Month < 7)
        //        {
        //            int year = RenwuModel.StartDate.Year - 1;

        //            return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
        //        }
        //        else
        //        {
        //            int year = RenwuModel.StartDate.Year - 1;
        //            return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
        //        }
        //    }
        //}
        ///// <summary>
        ///// 下半年
        ///// </summary>
        //protected string XBNWhere
        //{
        //    get
        //    {
        //        //上半年考核
        //        if (RenwuModel.StartDate.Month < 7)
        //        {
        //            int year = RenwuModel.StartDate.Year - 1;
        //            return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
        //        }
        //        else
        //        {
        //            int year = RenwuModel.StartDate.Year;
        //            return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
        //        }
        //    }
        //}


        /// <summary>
        /// 上半年
        /// </summary>
        protected string SBNWhere
        {
            get
            {
                //上半年考核
                if (RenwuModel.StartDate.Month < 7)
                {
                    int year = RenwuModel.StartDate.Year - 1;

                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
                }
                else
                {
                    int year = RenwuModel.StartDate.Year - 1;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
                }
            }
        }
        /// <summary>
        /// 下半年
        /// </summary>
        protected string XBNWhere
        {
            get
            {
                //上半年考核
                if (RenwuModel.StartDate.Month < 7)
                {
                    int year = RenwuModel.StartDate.Year - 1;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
                }
                else
                {
                    int year = RenwuModel.StartDate.Year;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
                }
            }
        }
        /// <summary>
        /// 股东ID
        /// </summary>
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        /// <summary>
        /// 本部门所有在职人员
        /// </summary>
        protected DataTable UnitMemsList
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }

                //分配部门
                string strWhereUnit = "";
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                    {
                        strWhereUnit = string.Format(" AND (m.mem_Unit_ID IN (231,232)) ", this.RenwuNo, this.drp_unit3.SelectedItem.Text.Trim());
                    }
                    else
                    {
                        strWhereUnit = string.Format(" AND m.mem_Unit_ID={0}", this.drp_unit3.SelectedValue);
                    }
                }

                string strSql = "";
                //分配部门
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    strSql = string.Format(@" select m.mem_ID
	                                            ,mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((select  hj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS hj
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2})/6,0) AS pjgz
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2}),0) AS yfjj 
                                                ,isnull((Select Top 1 B.OrderID
	                                            From (
		                                            Select MemID
		                                            ,pjgz
		                                            ,RANK() over(order by pjgz2 DESC) AS OrderID
		                                            From(
                                                        select aa.MemID,aa.pjgz
															  ,case AA.pjgz when 0 then 0
															  else kd.hj/AA.pjgz end as pjgz2
														from (
			                                                Select MemID,isnull((Sum(yfgz)-SUM(yfjj))/6,0) AS pjgz 
                                                            From cm_KaoHeMemsWages a left join tg_member mm on a.MemID=mm.mem_ID 
                                                            Where (a.yfgz is not null) AND mm.mem_isFired=0 AND mm.mem_Unit_ID=m.mem_Unit_ID {2}
                                                            Group by a.MemID
                                                        ) AA left join dbo.cm_KaoHeAllMemsAllotDetails KD on kd.MemID=aa.MemID where Kd.RenwuID={3}
		                                            ) A
	                                            ) B
                                                Where B.MemID=m.mem_ID),0) AS OrderID
                                                ,isnull((select top 1 B.OrderID
	                                                From (
		                                                select MemID
				                                                ,AllotCount
				                                                ,RANK() over(order by AllotCount DESC) AS OrderID
		                                                From (
			                                                Select r.MemID AS MemID
			                                                ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
			                                                From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
			                                                Where B.memUnitID=m.mem_Unit_ID 
				                                                AND (B.KaoHeUnitID IN (Select ID 
										                                                 From dbo.cm_KaoHeRenwuUnit
										                                                 Where UnitID=m.mem_Unit_ID AND RenWuNo={3}))
				                                                AND (R.KaoHeUnitID IN (Select ID 
										                                                 From dbo.cm_KaoHeRenwuUnit
										                                                 Where UnitID=m.mem_Unit_ID AND RenWuNo={3})) 
				                                                AND BB.Statu=0 
				                                                AND (BB.KaoHeUnitID IN (Select ID 
										                                                 From dbo.cm_KaoHeRenwuUnit
										                                                 Where UnitID=m.mem_Unit_ID AND RenWuNo={3}))
                                                        group by r.memID
                                                    ) A
                                                ) B 
                                                Where B.MemID=m.mem_ID),0) AS OrderID2
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {5})),0) AS sbn
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {6})),0) AS xbn
                                                ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS sbn2
                                                ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS xbn2
                                                From tg_member m 
                                                where 1=1 AND m.mem_isFired={1} 
                                                AND m.mem_Unit_ID={0} 
                                                AND (m.mem_ID IN (Select memID From cm_KaoHeRenwuMem Where Statu=0 AND KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{3}'))) 
                                                AND m.mem_ID NOT IN ({7})
                                                Order By m.mem_Order", this.drp_unit3.SelectedValue, this.drpZw.SelectedValue, strWhere, this.RenwuNo, RenwuModel.StartDate, SBNWhere, XBNWhere, GuDongIdStr);
                    //如果是离职
                    if (this.drpZw.SelectedIndex > 0)
                    {
                        strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((select  hj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={2} AND MemID=m.mem_ID),0) AS hj
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {1})/6,0) AS pjgz
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {1}),0) AS yfjj 
                                                ,isnull((Select Top 1 B.OrderID
	                                            From (
		                                            Select MemID
		                                            ,pjgz
		                                            ,RANK() over(order by pjgz2 DESC) AS OrderID
		                                            From(
                                                        select aa.MemID,aa.pjgz
															  ,case AA.pjgz when 0 then 0
															  else kd.hj/AA.pjgz end as pjgz2
														from (
			                                                Select MemID,isnull((Sum(yfgz)-SUM(yfjj))/6,0) AS pjgz 
                                                            From cm_KaoHeMemsWages a left join tg_member mm on a.MemID=mm.mem_ID 
                                                            Where (a.yfgz is not null) AND mm.mem_isFired=1 AND mm.mem_Unit_ID=m.mem_Unit_ID {1}
                                                            Group by a.MemID
                                                        ) AA left join dbo.cm_KaoHeAllMemsAllotDetails KD on kd.MemID=aa.MemID where Kd.RenwuID={2}
		                                            ) A
	                                            ) B
                                                Where B.MemID=m.mem_ID),0) AS OrderID
                                                ,isnull((select top 1 B.OrderID
	                                            From (
		                                            select MemID
				                                            ,AllotCount
				                                            ,RANK() over(order by AllotCount DESC) AS OrderID
		                                            From (
			                                            Select r.MemID AS MemID
			                                            ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
			                                            From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
			                                            Where B.memUnitID=m.mem_Unit_ID 
				                                            AND (B.KaoHeUnitID IN (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2}))
				                                            AND (R.KaoHeUnitID IN (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2})) 
				                                            AND BB.Statu=0 
				                                            AND (BB.KaoHeUnitID IN (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2}))
                                                    group by r.memID
                                                ) A
                                            ) B 
                                            Where B.MemID=m.mem_ID),0) AS OrderID2
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {6})),0) AS sbn
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {7})),0) AS xbn
                                                ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS sbn2
                                                ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS xbn2
                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.tg_memberExt ex on m.mem_ID=ex.mem_ID
                                                Where r.RoleIdMag<>5 {0} AND m.mem_isFired={3} AND (ex.mem_OutTime BETWEEN '{4}' AND '{5}') 
                                                Order By m.mem_Order ", strWhereUnit, strWhere, this.RenwuNo, this.drpZw.SelectedValue, RenwuModel.StartDate, RenwuModel.EndDate, SBNWhere, XBNWhere);
                    }

                }
                else
                {
                    strSql = string.Format(@" select m.mem_ID
	                                            ,mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((select  hj from dbo.cm_KaoHeAllMemsAllotDetails where MemID=m.mem_ID AND RenwuID={2}),0) AS hj
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {1})/6,0) AS pjgz 
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {1}),0) AS yfjj
                                                ,isnull((Select Top 1 B.OrderID
	                                            From (
		                                            Select MemID
		                                            ,pjgz
		                                            ,RANK() over(order by pjgz2 DESC) AS OrderID
		                                            From(
                                                        select aa.MemID,aa.pjgz
															  ,case AA.pjgz when 0 then 0
															  else kd.hj/AA.pjgz end as pjgz2
														from (
			                                                Select MemID,isnull((Sum(yfgz)-SUM(yfjj))/6,0) AS pjgz 
                                                            From cm_KaoHeMemsWages a left join tg_member mm on a.MemID=mm.mem_ID 
                                                            Where mm.mem_Unit_ID=m.mem_Unit_ID AND mm.mem_isFired=0 AND (a.yfgz is not null) {1}
                                                            Group by a.MemID
                                                        ) AA left join dbo.cm_KaoHeAllMemsAllotDetails KD on kd.MemID=aa.MemID where Kd.RenwuID={2}
		                                            ) A
	                                            ) B
                                                Where B.MemID=m.mem_ID),0) AS OrderID
                                                ,isnull((select top 1 B.OrderID
	                                            From (
		                                            select MemID
				                                            ,AllotCount
				                                            ,RANK() over(order by AllotCount DESC) AS OrderID
		                                            From (
			                                            Select r.MemID AS MemID
			                                            ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
			                                            From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID --left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
			                                            Where B.memUnitID=m.mem_Unit_ID AND B.Statu=0 
				                                            AND (B.KaoHeUnitID = (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2}))
				                                            AND (R.KaoHeUnitID = (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2}))
                                                        group by r.memID
                                                ) A
                                            ) B 
                                            Where B.MemID=m.mem_ID),0) AS OrderID2
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails KMA Where MemID=m.mem_ID AND Exists (Select ID From dbo.cm_KaoHeRenWu Where ID=KMA.RenwuID AND {4})),0) AS sbn
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails KMA Where MemID=m.mem_ID AND Exists (Select ID From dbo.cm_KaoHeRenWu Where ID=KMA.RenwuID AND {5})),0) AS xbn
                                                ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{3}')-1),0) AS sbn2
                                                ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{3}')-1),0) AS xbn2
                                                From tg_member m 
                                                Where 1=1
                                                AND m.mem_isFired={0} 
                                                AND m.mem_Unit_ID<>230 
                                                AND (Exists (Select memID From cm_KaoHeRenwuMem KM Where memID=m.mem_ID AND Statu=0 AND Exists (Select ID From dbo.cm_KaoHeRenwuUnit Where ID=KM.KaoHeUnitID AND RenWuNo='{2}'))) 
                                                AND NOT Exists (Select mem_ID  From tg_member Where (mem_ID=1440 OR mem_ID=1441 OR mem_ID=1442
                                                                                                OR mem_ID=1443 OR mem_ID=1445 OR mem_ID=1446
					                                                                            OR mem_ID=1474 OR mem_ID=1493 OR mem_ID=1498
					                                                                            OR mem_ID=1519 OR mem_ID=1538 OR mem_ID=1554
					                                                                            OR mem_ID=1577) AND mem_ID=m.mem_ID)
                                                Order By m.mem_Order", this.drpZw.SelectedValue, strWhere, this.RenwuNo, RenwuModel.StartDate, SBNWhere, XBNWhere, GuDongIdStr);

                    //如果是离职
                    if (this.drpZw.SelectedIndex > 0)
                    {
                        strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((select  hj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={2} AND MemID=m.mem_ID),0) AS hj
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {1})/6,0) AS pjgz
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {1}),0) AS yfjj 
                                                ,isnull((Select Top 1 B.OrderID
	                                            From (
		                                            Select MemID
		                                            ,pjgz
		                                            ,RANK() over(order by pjgz2 DESC) AS OrderID
		                                            From(
                                                        select aa.MemID,aa.pjgz
															  ,case AA.pjgz when 0 then 0
															  else kd.hj/AA.pjgz end as pjgz2
														from (
			                                                Select MemID,isnull((Sum(yfgz)-SUM(yfjj))/6,0) AS pjgz 
                                                            From cm_KaoHeMemsWages a left join tg_member mm on a.MemID=mm.mem_ID 
                                                            Where (a.yfgz is not null) AND mm.mem_isFired=1 AND mm.mem_Unit_ID=m.mem_Unit_ID {1}
                                                            Group by a.MemID
                                                        ) AA left join dbo.cm_KaoHeAllMemsAllotDetails KD on kd.MemID=aa.MemID where Kd.RenwuID={2}
		                                            ) A
	                                            ) B
                                                Where B.MemID=m.mem_ID),0) AS OrderID
                                                ,isnull((select top 1 B.OrderID
	                                            From (
		                                            select MemID
				                                            ,AllotCount
				                                            ,RANK() over(order by AllotCount DESC) AS OrderID
		                                            From (
			                                            Select r.MemID AS MemID
			                                            ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
			                                            From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
			                                            Where B.memUnitID=m.mem_Unit_ID 
				                                            AND (B.KaoHeUnitID IN (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2}))
				                                            AND (R.KaoHeUnitID IN (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2})) 
				                                            AND BB.Statu=0 
				                                            AND (BB.KaoHeUnitID IN (Select ID 
										                                             From dbo.cm_KaoHeRenwuUnit
										                                             Where UnitID=m.mem_Unit_ID AND RenWuNo={2}))
                                                    group by r.memID
                                                ) A
                                            ) B 
                                            Where B.MemID=m.mem_ID),0) AS OrderID2
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {6})),0) AS sbn
                                                ,isnull((select SUM(hj)+SUM(yfjj) From dbo.cm_KaoHeAllMemsAllotDetails Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {7})),0) AS xbn
                                                ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS sbn2
                                                ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryZJJ Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS xbn2
                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.tg_memberExt ex on m.mem_ID=ex.mem_ID
                                                Where r.RoleIdMag<>5 {0} AND m.mem_isFired={3} AND (ex.mem_OutTime BETWEEN '{4}' AND '{5}') 
                                                Order By m.mem_Order ", strWhereUnit, strWhere, this.RenwuNo, this.drpZw.SelectedValue, RenwuModel.StartDate, RenwuModel.EndDate, SBNWhere, XBNWhere);
                    }
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();


                if (this.drp_unit3.Items.FindByValue(this.CurUnitID.ToString()) != null)
                {
                    this.drp_unit3.Items.FindByValue(this.CurUnitID.ToString()).Selected = true;
                }
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }

        protected void btn_Export_Click(object sender, EventArgs e)
        {
            if (!IsBak)
            {
                ExportIn();
            }
            else
            {
                ExportInHis();
            }
        }

        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void drpZw_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected string SBNTitleStr
        {
            get
            {
                string title = "";
                if (RenwuModel.StartDate.Month < 7)
                {
                    title = (RenwuModel.StartDate.Year - 1) + "下半年";
                }
                else
                {
                    title = RenwuModel.StartDate.Year + "上半年";
                }

                return title;
            }
        }

        protected string XBNTitleStr
        {
            get
            {
                string title = "";
                if (RenwuModel.StartDate.Month < 7)
                {
                    title = (RenwuModel.StartDate.Year - 1) + "上半年";
                }
                else
                {
                    title = (RenwuModel.StartDate.Year - 1) + "下半年";
                }

                return title;
            }
        }
        public void ExportIn()
        {
            DataTable dt = this.UnitMemsList;

            string modelPath = " ~/TemplateXls/shujuduibi.xls";

            if (this.drpZw.SelectedValue == "0")
            {
                modelPath = " ~/TemplateXls/shujuduibilizhi.xls";
            }

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;

            var rowOne = ws.GetRow(0);
            rowOne.GetCell(8).SetCellValue(SBNTitleStr);
            rowOne.GetCell(10).SetCellValue(XBNTitleStr);


            decimal hj = 0;
            decimal pj = 0;
            decimal sbn = 0;
            decimal xbn = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["unitName"].ToString());

                //cell = dataRow.CreateCell(1);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["unitName"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                if (dt.Rows[i]["mem_isFired"].ToString() == "0")
                {
                    cell.SetCellValue("在职");
                }
                else
                {
                    cell.SetCellValue("离职");
                }

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["hj"].ToString());
                hj = Convert.ToDecimal(dt.Rows[i]["hj"].ToString());


                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pjgz"].ToString());
                pj = Convert.ToDecimal(dt.Rows[i]["pjgz"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                string val = (pj != 0 ? Math.Round(hj / pj, 2) : 0).ToString();
                cell.SetCellValue(val);

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["OrderID"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;

                cell.SetCellValue(dt.Rows[i]["OrderID2"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["sbn"].ToString());
                sbn = Convert.ToDecimal(dt.Rows[i]["sbn"].ToString());

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;

                cell.SetCellValue((hj - sbn).ToString());

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["xbn"].ToString());
                xbn = Convert.ToDecimal(dt.Rows[i]["xbn"].ToString());

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue((hj - xbn).ToString());


            }
            //导出文件名
            string strFileName = string.Format("{0}员工年度数据对比导出.xls", this.drpZw.SelectedItem.Text.Trim());
            if (this.drp_unit3.SelectedIndex > 0)
            {
                strFileName = string.Format("{0}{1}员工年度数据对比导出.xls", this.drp_unit3.SelectedItem.Text.Trim(), this.drpZw.SelectedItem.Text.Trim());
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(strFileName);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        public void ExportInHis()
        {
            var hisList = this.AllHisList;

            if (hisList.Count > 0)
            {
                string modelPath = " ~/TemplateXls/shujuduibi.xls";

                if (this.drpZw.SelectedValue == "0")
                {
                    modelPath = " ~/TemplateXls/shujuduibilizhi.xls";
                }

                HSSFWorkbook wb = null;

                //如果没有模板路径，则创建一个空的workbook和一个空的sheet
                if (string.IsNullOrEmpty(modelPath))
                {
                    wb = new HSSFWorkbook();
                    wb.CreateSheet();
                    wb.GetSheetAt(0).CreateRow(0);
                }
                else
                {
                    using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        wb = new HSSFWorkbook(fileStream);
                        fileStream.Close();
                    }
                }


                //内容样式
                ICellStyle style2 = wb.CreateCellStyle();
                style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style2.VerticalAlignment = VerticalAlignment.CENTER;
                style2.WrapText = true;
                style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font2 = wb.CreateFont();
                font2.FontHeightInPoints = 10;//字号
                font2.FontName = "宋体";//字体
                style2.SetFont(font2);

                //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
                var ws = wb.GetSheet("Sheet1");
                if (ws == null)
                    ws = wb.GetSheetAt(0);

                int row = 2;

                var rowOne = ws.GetRow(0);
                rowOne.GetCell(8).SetCellValue(SBNTitleStr);
                rowOne.GetCell(10).SetCellValue(XBNTitleStr);


                decimal hj = 0;
                decimal pj = 0;
                decimal sbn = 0;
                decimal xbn = 0;


                for (int i = 0; i < hisList.Count; i++)
                {

                    var model = hisList[i];

                    var dataRow = ws.GetRow(i + row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(i + row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.UnitName);

                    //cell = dataRow.CreateCell(1);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dt.Rows[i]["unitName"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.UserName);

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;

                    cell.SetCellValue(model.IsFired);


                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.JJHJ.ToString());
                    hj = Convert.ToDecimal(model.JJHJ);


                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.PJYGZ.ToString());
                    pj = Convert.ToDecimal(model.PJYGZ);

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;

                    cell.SetCellValue(model.PJYGZBS.ToString());

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.PJYGZPM.ToString());

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;

                    cell.SetCellValue(model.BMNHPPM.ToString());

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.SBN.ToString());
                    sbn = Convert.ToDecimal(model.SBN);

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;

                    cell.SetCellValue(model.SBNZF.ToString());

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.XBN.ToString());
                    xbn = Convert.ToDecimal(model.XBN.ToString());

                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.XBNZF.ToString());


                }

                //导出文件名
                string strFileName = string.Format("{0}员工年度数据对比导出.xls", this.drpZw.SelectedItem.Text.Trim());
                if (this.drp_unit3.SelectedIndex > 0)
                {
                    strFileName = string.Format("{0}{1}员工年度数据对比导出.xls", this.drp_unit3.SelectedItem.Text.Trim(), this.drpZw.SelectedItem.Text.Trim());
                }

                using (MemoryStream memoryStream = new MemoryStream())
                {

                    wb.Write(memoryStream);

                    string name = System.Web.HttpContext.Current.Server.UrlEncode(strFileName);
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                    Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.ContentEncoding = Encoding.UTF8;
                    wb = null;
                    Response.End();
                }

            }
        }
    }
}