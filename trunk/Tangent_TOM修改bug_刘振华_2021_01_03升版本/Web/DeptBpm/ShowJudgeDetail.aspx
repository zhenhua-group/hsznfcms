﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ShowJudgeDetail.aspx.cs" Inherits="TG.Web.DeptBpm.ShowJudgeDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="../js/assets/plugins/data-tables/DT_bootstrap.css" />

    <script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/DT_bootstrap.js"></script>


    <script src="../js/DeptBpm/ShowJudge.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>考核评分查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>查看考核评分</a></li>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>部门名称：<%= KaoHeUnit.unit_Name %>
                    </div>
                    <div class="actions">
                        <a href="#" id="btnOrderTable" class="btn btn-sm red">排序</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-hover" id="tbData">
                        <thead>
                            <tr>
                                <th colspan="16" align="center"><span><%= KaoHeUnit.unit_Name %>&nbsp;
                                    <%if (RenwuModel.RenWuNo.ToString().ToCharArray()[RenwuModel.RenWuNo.ToString().Length - 1] == '1')
                                      { %>
                                    <b>(<%= RenwuModel.RenWuNo.ToString()%>) </b>
                                    <%}
                                      else
                                      {%>
                                    <b>(<%= RenwuModel.RenWuNo.ToString()%>) </b>
                                    <%} %>
                                    &nbsp;互评与自评打分表 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打分人： <%= memModel.mem_Name %></span>
                                </th>

                            </tr>
                            <tr class="warning">
                                <th style="width: 75px;"><a href="#" title="恢复默认排序" id="btnDefault">被评价人</a></th>
                                <th style="width: 75px;">劳动强度</th>
                                <th style="width: 75px;">工作效率</th>
                                <th style="width: 85px;">工作主动性</th>
                                <th style="width: 75px;">工作质量</th>
                                <th style="width: 75px;">工作难度</th>
                                <th style="width: 75px;">劳动纪律</th>
                                <th style="width: 100px;">内部沟通能力</th>
                                <th style="width: 110px;">合作与互助精神</th>
                                <th style="width: 100px;">外部沟通能力</th>
                                <th style="width: 110px;">组织与配合能力</th>
                                <th style="width: 75px;">学习意愿</th>
                                <th style="width: 110px;">公共事务积极性</th>
                                <th style="width: 50px;">得分</th>
                                <th style="width: 50px;">排名</th>
                                <th></th>
                            </tr>
                            <%--<tr class="warning">
                                <th style="width: 75px;">权重</th>
                                <th style="width: 75px;">10</th>
                                <th style="width: 75px;">12</th>
                                <th style="width: 85px;">10</th>
                                <th style="width: 75px;">12</th>
                                <th style="width: 75px;">8</th>
                                <th style="width: 75px;">6</th>
                                <th style="width: 100px;">5</th>
                                <th style="width: 110px;">5</th>
                                <th style="width: 100px;">4</th>
                                <th style="width: 110px;">4</th>
                                <th style="width: 75px;">2</th>
                                <th style="width: 110px;">1</th>
                                <th style="width: 80px;"></th>
                                <th></th>
                                <th></th>
                            </tr>--%>
                        </thead>
                        <tbody>
                            <%
                                decimal? avg1 = 0;
                                decimal? avg2 = 0;
                                decimal? avg3 = 0;
                                decimal? avg4 = 0;
                                decimal? avg5 = 0;
                                decimal? avg6 = 0;
                                decimal? avg7 = 0;
                                decimal? avg8 = 0;
                                decimal? avg9 = 0;
                                decimal? avg10 = 0;
                                decimal? avg11 = 0;
                                decimal? avg12 = 0;
                                decimal? JudgeAllCount = 0;
                                int userCount = UserJudges.Count;
                                int index = 1;

                                UserJudges.ForEach(c =>
                                        {

                                            avg1 += c.Judge1 ?? 0;
                                            avg2 += c.Judge2 ?? 0;
                                            avg3 += c.Judge3 ?? 0;
                                            avg4 += c.Judge4 ?? 0;
                                            avg5 += c.Judge5 ?? 0;
                                            avg6 += c.Judge6 ?? 0;
                                            avg7 += c.Judge7 ?? 0;
                                            avg8 += c.Judge8 ?? 0;
                                            avg9 += c.Judge9 ?? 0;
                                            avg10 += c.Judge10 ?? 0;
                                            avg11 += c.Judge11 ?? 0;
                                            avg12 += c.Judge12 ?? 0;
                                            JudgeAllCount += c.JudgeCount;
                            %>
                            <tr>
                                <td><span memid="<%=c.MemID%>" memname="<%=c.MemName%>" class="badge badge-info"><%= c.MemName%></span></td>
                                <td>
                                    <span><%= c.Judge1%></span></td>
                                <td>
                                    <span><%= c.Judge2%></span></td>
                                <td>
                                    <span><%= c.Judge3%></span></td>
                                <td>
                                    <span><%= c.Judge4%></span></td>
                                <td>
                                    <span><%= c.Judge5%></span></td>
                                <td>
                                    <span><%= c.Judge6%></span></td>
                                <td>
                                    <span><%= c.Judge7%></span></td>
                                <td>
                                    <span><%= c.Judge8%></span></td>
                                <td>
                                    <span><%= c.Judge9%></span></td>
                                <td>
                                    <span><%= c.Judge10%></span></td>
                                <td>
                                    <span><%= c.Judge11%></span></td>
                                <td>
                                    <span><%= c.Judge12%></span></td>
                                <td>
                                    <span><%= c.JudgeCount %></span></td>
                                <td>
                                    <%--<%= index++ %>--%>
                                    <%= c.IndexID %>
                                </td>
                                <td></td>
                            </tr>
                            <% });%>
                        </tbody>
                        <tfoot>
                            <tr class="warning">
                                <td>平均值</td>
                                <td><%= Convert.ToDecimal(avg1 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg2 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg3 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg4 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg5 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg6 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg7 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg8 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg9 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg10 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg11 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg12 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(JudgeAllCount/userCount).ToString("f2") %></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="16" align="center">
                                    <% if (IsHaveNull)
                                       {%>
                                    <a href="#" id="btnSync" class="btn btn-xs red">同步考核数据</a>
                                    <%} %>
                                    <a href="/DeptBpm/ShowUserPress.aspx?uid=<%= KaoHeUnitSysNo%>&renwuno=<%=RenwuNo %>&id=<%= KaoHeUnitID%>" class="btn btn-xs default">返回</a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <% if (UserJudgesHis.Count > 0)
                       { %>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th colspan="16" align="center"><span><%= KaoHeUnit.unit_Name %>&nbsp;
                                    <%if (RenwuModel.RenWuNo.ToString().ToCharArray()[RenwuModel.RenWuNo.ToString().Length - 1] == '1')
                                      { %>
                                    <b>上半年度 </b>
                                    <%}
                                      else
                                      {%>
                                    <b>下半年度 </b>
                                    <%} %>
                                    &nbsp;互评与自评打分表 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;打分人： <%= memModel.mem_Name %></span><span class="badge badge-danger">(同步历史数据)</span>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 75px;">被评价人</th>
                                <th style="width: 75px;">劳动强度</th>
                                <th style="width: 75px;">工作效率</th>
                                <th style="width: 85px;">工作主动性</th>
                                <th style="width: 75px;">工作质量</th>
                                <th style="width: 75px;">工作难度</th>
                                <th style="width: 75px;">劳动纪律</th>
                                <th style="width: 100px;">内部沟通能力</th>
                                <th style="width: 110px;">合作与互助精神</th>
                                <th style="width: 100px;">外部沟通能力</th>
                                <th style="width: 110px;">组织与配合能力</th>
                                <th style="width: 75px;">学习意愿</th>
                                <th style="width: 110px;">公共事务积极性</th>
                                <th style="width: 50px;">得分</th>
                                <th style="width: 50px;">排名</th>
                                <th></th>
                            </tr>
                            <tr style="display: none;">
                                <th style="width: 75px;">权重</th>
                                <th style="width: 75px;">10</th>
                                <th style="width: 75px;">12</th>
                                <th style="width: 85px;">10</th>
                                <th style="width: 75px;">12</th>
                                <th style="width: 75px;">8</th>
                                <th style="width: 75px;">6</th>
                                <th style="width: 100px;">5</th>
                                <th style="width: 110px;">5</th>
                                <th style="width: 100px;">4</th>
                                <th style="width: 110px;">4</th>
                                <th style="width: 75px;">2</th>
                                <th style="width: 110px;">1</th>
                                <th style="width: 80px;"></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                      decimal? avg1His = 0;
                                      decimal? avg2His = 0;
                                      decimal? avg3His = 0;
                                      decimal? avg4His = 0;
                                      decimal? avg5His = 0;
                                      decimal? avg6His = 0;
                                      decimal? avg7His = 0;
                                      decimal? avg8His = 0;
                                      decimal? avg9His = 0;
                                      decimal? avg10His = 0;
                                      decimal? avg11His = 0;
                                      decimal? avg12His = 0;
                                      decimal? JudgeAllCountHis = 0;
                                      int userCountHis = UserJudgesHis.Count;
                                      int indexHis = 1;

                                      UserJudgesHis.ForEach(c =>
                                              {

                                                  avg1His += c.Judge1 ?? 0;
                                                  avg2His += c.Judge2 ?? 0;
                                                  avg3His += c.Judge3 ?? 0;
                                                  avg4His += c.Judge4 ?? 0;
                                                  avg5His += c.Judge5 ?? 0;
                                                  avg6His += c.Judge6 ?? 0;
                                                  avg7His += c.Judge7 ?? 0;
                                                  avg8His += c.Judge8 ?? 0;
                                                  avg9His += c.Judge9 ?? 0;
                                                  avg10His += c.Judge10 ?? 0;
                                                  avg11His += c.Judge11 ?? 0;
                                                  avg12His += c.Judge12 ?? 0;
                                                  JudgeAllCountHis += c.JudgeCount;
                            %>
                            <tr>
                                <td><span memid="<%=c.MemID%>" memname="<%=c.MemName%>" class="badge badge-info"><%= c.MemName%></span></td>
                                <td>
                                    <span><%= c.Judge1%></span></td>
                                <td>
                                    <span><%= c.Judge2%></span></td>
                                <td>
                                    <span><%= c.Judge3%></span></td>
                                <td>
                                    <span><%= c.Judge4%></span></td>
                                <td>
                                    <span><%= c.Judge5%></span></td>
                                <td>
                                    <span><%= c.Judge6%></span></td>
                                <td>
                                    <span><%= c.Judge7%></span></td>
                                <td>
                                    <span><%= c.Judge8%></span></td>
                                <td>
                                    <span><%= c.Judge9%></span></td>
                                <td>
                                    <span><%= c.Judge10%></span></td>
                                <td>
                                    <span><%= c.Judge11%></span></td>
                                <td>
                                    <span><%= c.Judge12%></span></td>
                                <td>
                                    <span><%= c.JudgeCount %></span></td>
                                <td>
                                    <span><b><%= indexHis++ %></span></b></td>
                                <td></td>
                            </tr>
                            <% });%>
                        </tbody>
                        <tfoot>
                            <tr class="warning">
                                <td>平均值</td>
                                <td><%= Convert.ToDecimal(avg1His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg2His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg3His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg4His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg5His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg6His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg7His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg8His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg9His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg10His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg11His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg12His / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(JudgeAllCount/userCount).ToString("f2") %></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                    <%} %>
                </div>
            </div>
        </div>
    </div>
    <script>
        showJudge.init();
    </script>

    <input type="hidden" name="name" value="<%= KaoHeUnitID%>" id="hidKaoHeUnitID" />
    <input type="hidden" name="name" value="<%= MemId %>" id="hidInserUserID" />
    <input type="hidden" name="name" value="<%= KaoHeUnitSysNo %>" id="hidUnitID" />

</asp:Content>
