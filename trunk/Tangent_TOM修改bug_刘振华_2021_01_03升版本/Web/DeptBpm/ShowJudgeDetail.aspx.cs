﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;

namespace TG.Web.DeptBpm
{
    public partial class ShowJudgeDetail : PageBase
    {
        /// <summary>
        /// 考核部门ID
        /// </summary>
        protected int KaoHeUnitSysNo
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["uid"] ?? "0", out unitid);
                return unitid;
            }
        }

        protected TG.Model.tg_unit KaoHeUnit
        {
            get
            {
                return new TG.BLL.tg_unit().GetModel(this.KaoHeUnitSysNo);
            }
        }

        /// <summary>
        /// 部门考核ID
        /// </summary>
        protected int KaoHeUnitID
        {
            get
            {
                int ukaoheid = 0;
                int.TryParse(Request["ukaoheid"] ?? "0", out ukaoheid);
                return ukaoheid;
            }
        }

        public int RenwuNo
        {
            get
            {
                int renwuno = 0;
                int.TryParse(Request["renwuno"] ?? "0", out renwuno);
                return renwuno;
            }
        }
        /// <summary>
        /// 打分用户ID
        /// </summary>
        protected int MemId
        {
            get
            {
                int memid = 0;
                int.TryParse(Request["memid"] ?? "0", out memid);
                return memid;
            }
        }

        /// <summary>
        /// 任务实体
        /// </summary>
        protected TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                TG.Model.cm_KaoHeRenwuUnit unitModel = new TG.BLL.cm_KaoHeRenwuUnit().GetModel(KaoHeUnitID);
                TG.Model.cm_KaoHeRenWu model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(unitModel.RenWuNo));
                return model;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected TG.Model.tg_member memModel
        {
            get
            {
                return new TG.BLL.tg_member().GetModel(MemId);
            }
        }

        /// <summary>
        /// 股东ID  2017年1月17日
        /// </summary> 
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        /// <summary>
        /// 用户评论数
        /// </summary>
        public List<TG.Model.cm_UserJudgeReportEntityOrder> UserJudges
        {
            get
            {
                //string strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1}", KaoHeUnitID, MemId);
                //var pls = new TG.BLL.cm_UserJudgeReport().GetModelList(strWhere);
                //pls = pls.OrderByDescending(c => c.JudgeCount).ToList();

                string strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1} AND MemID NOT IN ({2})", KaoHeUnitID, MemId, GuDongIdStr);
                var pls = new TG.BLL.cm_UserJudgeReport().GetModelList(strWhere);
                pls = pls.OrderByDescending(c => c.JudgeCount).ToList();

                strWhere = string.Format(" KaoHeUnitID={0}", KaoHeUnitID);
                var khlist = new TG.BLL.cm_KaoHeRenwuMem().GetModelList(strWhere);

                //排序
                var plsOrder = pls.OrderByDescending(c => c.JudgeCount).ToList();

                int i = 1;
                var orderIndex = (from pr in plsOrder
                                  select new
                                  {
                                      MemID = pr.MemID,
                                      OrderIndex = i++
                                  }).ToList();

                var result = from p in pls
                             join m in khlist on p.MemID equals m.memID into temp
                             from rlt in temp.DefaultIfEmpty()
                             join or in orderIndex on p.MemID equals or.MemID
                             select new cm_UserJudgeReportEntityOrder()
                             {
                                 ID = p.ID,
                                 MemID = p.MemID,
                                 MemName = p.MemName,
                                 Judge1 = p.Judge1,
                                 Judge2 = p.Judge2,
                                 Judge3 = p.Judge3,
                                 Judge4 = p.Judge4,
                                 Judge5 = p.Judge5,
                                 Judge6 = p.Judge6,
                                 Judge7 = p.Judge7,
                                 Judge8 = p.Judge8,
                                 Judge9 = p.Judge9,
                                 Judge10 = p.Judge10,
                                 Judge11 = p.Judge11,
                                 Judge12 = p.Judge12,
                                 SaveStatu = p.SaveStatu,
                                 KaoHeUnitID = p.KaoHeUnitID,
                                 InsertUserId = p.InsertUserId,
                                 InsertDate = p.InsertDate,
                                 JudgeCount = p.JudgeCount,
                                 OrderID = rlt.ID,
                                 IndexID = or.OrderIndex
                             };

                //pls = pls.OrderByDescending(c => c.JudgeCount).ToList();
                //return pls;

                return result.OrderBy(c => c.OrderID).ToList();
            }
        }
        public List<TG.Model.cm_UserJudgeReportHis> UserJudgesHis
        {
            get
            {
                string strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1}", KaoHeUnitID, MemId);
                var pls = new TG.BLL.cm_UserJudgeReportHis().GetModelList(strWhere);
                pls = pls.OrderByDescending(c => c.JudgeCount).ToList();
                return pls;
            }
        }
        /// <summary>
        /// 是否有没打分的情况
        /// </summary>
        protected bool IsHaveNull
        {
            get
            {
                StringBuilder sb2 = new StringBuilder();
                sb2.Append(" Select *");
                sb2.Append(" From dbo.cm_UserJudgeReport");
                sb2.AppendFormat(@" Where ((Judge1 IS NULL) 
	                            OR (Judge2 IS NULL) 
	                            OR (Judge3 IS NULL) 
	                            OR (Judge4 IS NULL) 
	                            OR (Judge5 IS NULL) 
	                            OR (Judge6 IS NULL) 
	                            OR (Judge7 IS NULL) 
	                            OR (Judge8 IS NULL) 
	                            OR (Judge9 IS NULL) 
	                            OR (Judge10 IS NULL) 
	                            OR (Judge11 IS NULL) 
	                            OR (Judge12 IS NULL) ) AND KaoHeUnitID={0} AND InsertUserId={1}", this.KaoHeUnitID, this.MemId);
                DataTable dt2 = TG.DBUtility.DbHelperSQL.Query(sb2.ToString()).Tables[0];

                if (dt2.Rows.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}