﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.DeptBpm
{
    public partial class SetUnitUsers : PageBase
    {
        /// <summary>
        /// 分配任务ID
        /// </summary>
        public string RenWuNo
        {
            get
            {
                return Request["renwuno"] ?? "";
            }
        }
        /// <summary>
        /// 考核部门ID
        /// </summary>
        public int UnitID
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["uid"] ?? "0", out unitid);
                return unitid;
            }
        }
        /// <summary>
        /// 考核部门id
        /// </summary>
        public int KaoHeUnitID
        {
            get
            {
                int kaoheid = 0;
                int.TryParse(Request["id"] ?? "0", out kaoheid);
                return kaoheid;
            }
        }
        public TG.Model.tg_unit Unit
        {
            get
            {
                return new TG.BLL.tg_unit().GetModel(UnitID);
            }
        }
        /// <summary>
        /// 当前部门所有人
        /// </summary>
        public DataTable MemsTable
        {
            get
            {
                //查询语句
                StringBuilder sb = new StringBuilder();
                if (IsSavedMems)
                {
                    sb.Append(" select A.*");
                    sb.Append(" ,B.MagRole AS RoleIdMag,B.NormalRole AS RoleIdTec");
                    sb.Append(" ,isnull((select roleName from cm_RoleMag where ID=B.MagRole),'') As MagName");
                    sb.Append(" ,isnull((select roleName from cm_RoleMag where ID=B.NormalRole),'') As TecName");
                    sb.Append(" ,B.Weights,B.Statu,(select top 1 Weights from tg_memberRole where mem_Id=A.mem_ID) As RoleWeights");
                    sb.Append(" ,(select  top 1 RoleIdMag from tg_memberRole where mem_Id=A.mem_ID) As RoleMagRole");
                    sb.Append(" ,(select  top 1 RoleIdTec from tg_memberRole where mem_Id=A.mem_ID) As RoleNormalRole");
                    sb.Append(" ,isnull((select unit_name from tg_unit where unit_Id=B.IsOutUnitID),'本部门') AS OutUnitName");
                    sb.Append(" ,B.IsOutUnitID");
                    sb.Append(" from tg_member A left join dbo.cm_KaoHeRenwuMem B on A.mem_ID=B.memID");
                    sb.AppendFormat(" Where A.mem_isFired=0 AND B.memUnitID={0} AND B.KaoHeUnitID={1} Order by A.mem_Order asc,A.mem_ID ", UnitID, KaoHeUnitID);
                }
                else
                {
                    sb.Append(" select A.*");
                    sb.Append(" ,B.RoleIdTec,B.RoleIdMag");
                    sb.Append(" ,isnull((select roleName from cm_RoleMag where ID=B.RoleIdMag),'') As MagName");
                    sb.Append(" ,isnull((select roleName from cm_RoleMag where ID=B.RoleIdTec),'') As TecName");
                    sb.Append(" ,B.Weights,0 AS Statu");
                    sb.Append(" ,'本部门' AS OutUnitName");
                    sb.Append(" ,0 AS IsOutUnitID");
                    sb.Append(" from tg_member A left join dbo.tg_memberRole B on A.mem_ID=B.mem_ID");
                    sb.AppendFormat(" Where B.mem_Unit_ID={0} AND A.mem_isFired=0  Order by A.mem_Order asc,A.mem_ID ", UnitID);
                }
                //得到结果
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                return dt;
            }
        }
        /// <summary>
        /// 是否已经保存
        /// </summary>
        public bool IsSavedMems
        {
            get
            {
                bool isSave = false;
                string strWhere = string.Format(" KaoHeUnitID={0} AND memUnitID={1}", KaoHeUnitID, UnitID);
                var list = new TG.BLL.cm_KaoHeRenwuMem().GetModelList(strWhere);
                if (list.Count > 0)
                {
                    isSave = true;
                }
                return isSave;
            }
        }

        public string Url
        {
            get
            {
                string url = "SetUnitUsers.aspx" + HttpContext.Current.Request.Url.Query;
                return url;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }



    }
}