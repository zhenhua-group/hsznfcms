﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace TG.Web.DeptBpm
{
    public partial class ShowAllJudgeCountDetails : PageBase
    {
        /// <summary>
        /// 考核部门ID
        /// </summary>
        protected int KaoHeUnitSysNo
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["uid"] ?? "0", out unitid);
                return unitid;
            }
        }

        protected TG.Model.tg_unit KaoHeUnit
        {
            get
            {
                return new TG.BLL.tg_unit().GetModel(this.KaoHeUnitSysNo);
            }
        }

        /// <summary>
        /// 部门考核ID
        /// </summary>
        protected int KaoHeUnitID
        {
            get
            {
                int ukaoheid = 0;
                int.TryParse(Request["id"] ?? "0", out ukaoheid);
                return ukaoheid;
            }
        }

        public int RenwuNo
        {
            get
            {
                int renwuno = 0;
                int.TryParse(Request["renwuno"] ?? "0", out renwuno);
                return renwuno;
            }
        }
        protected int MemID
        {
            get
            {
                int memid = 0;
                int.TryParse(Request["memid"] ?? "0", out memid);
                return memid;
            }
        }
        /// <summary>
        /// 任务实体
        /// </summary>
        protected TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                TG.Model.cm_KaoHeRenwuUnit unitModel = new TG.BLL.cm_KaoHeRenwuUnit().GetModel(KaoHeUnitID);
                TG.Model.cm_KaoHeRenWu model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(unitModel.RenWuNo));
                return model;
            }
        }
        /// <summary>
        /// 考核报告
        /// </summary>
        protected List<cm_UserJudgeReportEntity> JudgeReport
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                #region 汇总注释 2016年5月11日
                //                sb.Append(@"select  
                //                         R.MemID
                //	                    ,R.MemName
                //                        ,MAX(B.Weights) AS Weights
                //                        ,MAX(SaveStatu) AS SaveStatu
                //	                    ,Max(InsertUserId) AS InsertUserId
                //	                    ,convert(decimal(18,2),avg(Judge1)) AS Judge1
                //	                    ,convert(decimal(18,2),avg(Judge2)) AS Judge2
                //	                    ,convert(decimal(18,2),avg(Judge3))  AS  Judge3
                //	                    ,convert(decimal(18,2),avg(Judge4)) AS Judge4
                //	                    ,convert(decimal(18,2),avg(Judge5)) AS Judge5
                //	                    ,convert(decimal(18,2),avg(Judge6)) AS Judge6
                //	                    ,convert(decimal(18,2),avg(Judge7)) AS Judge7
                //	                    ,convert(decimal(18,2),avg(Judge8)) AS Judge8
                //	                    ,convert(decimal(18,2),avg(Judge9)) AS Judge9
                //	                    ,convert(decimal(18,2),avg(Judge10)) AS Judge10
                //	                    ,convert(decimal(18,2),avg(Judge11)) AS Judge11
                //	                    ,convert(decimal(18,2),avg(Judge12)) AS Judge12
                //	                    ,convert(decimal(18,2),avg(JudgeCount)) AS JudgeCount");
                //                sb.Append(" From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.MemID=B.memID");
                //                sb.AppendFormat(" Where B.KaoHeUnitID={0} AND R.KaoHeUnitID={0} AND R.SaveStatu=1", this.KaoHeUnitID);
                //                sb.Append(" Group by R.MemID,R.MemName");
                #endregion

                sb.Append(@"Select B.memID
	                      ,B.memName
	                      ,B.Weights
	                      ,Judge1
	                      ,Judge2
	                      ,Judge3
	                      ,Judge4
	                      ,Judge5
	                      ,Judge6
	                      ,Judge7
	                      ,Judge8
	                      ,Judge9
	                      ,Judge10
	                      ,Judge11
	                      ,Judge12
	                      ,JudgeCount");
                sb.Append(" From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID");
                sb.AppendFormat(" where R.KaoHeUnitID={0} AND SaveStatu=1 AND B.KaoHeUnitID={0} AND R.MemID={1} Order By B.ID", this.KaoHeUnitID, this.MemID);

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];

                string jsonStr = JsonConvert.SerializeObject(dt);

                var list = JsonConvert.DeserializeObject<List<cm_UserJudgeReportEntity>>(jsonStr);

                var plsOrder = list.OrderByDescending(c => c.JudgeCount).ToList();

                //2017年1月13日 加
                int i = 0;
                int index = 1;
                decimal? tempval = 0;

                var orderIndex = (from pr in plsOrder
                                  select new
                                  {
                                      MemID = pr.MemID,
                                      OrderIndex = CountOrder(pr.JudgeCount, ref i, ref tempval, ref index)
                                  }).ToList();
                //list = list.OrderByDescending(c => c.JudgeCount).ToList();
                //2017年1月13日 加
                var result = from p in list
                             join r in orderIndex on p.MemID equals r.MemID
                             select new cm_UserJudgeReportEntity()
                             {
                                 ID = p.ID,
                                 MemID = p.MemID,
                                 MemName = p.MemName,
                                 Judge1 = p.Judge1,
                                 Judge2 = p.Judge2,
                                 Judge3 = p.Judge3,
                                 Judge4 = p.Judge4,
                                 Judge5 = p.Judge5,
                                 Judge6 = p.Judge6,
                                 Judge7 = p.Judge7,
                                 Judge8 = p.Judge8,
                                 Judge9 = p.Judge9,
                                 Judge10 = p.Judge10,
                                 Judge11 = p.Judge11,
                                 Judge12 = p.Judge12,
                                 SaveStatu = p.SaveStatu,
                                 KaoHeUnitID = p.KaoHeUnitID,
                                 InsertUserId = p.InsertUserId,
                                 InsertDate = p.InsertDate,
                                 JudgeCount = p.JudgeCount,
                                 IndexID = r.OrderIndex,
                                 Weights = p.Weights
                             };

                return result.ToList();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected int CountOrder(decimal? judgeCount, ref int i, ref decimal? tempval, ref int index)
        {
            if (i == 0)
            {
                tempval = judgeCount;
                i++;
            }
            else
            {
                if (tempval != judgeCount)
                {
                    tempval = judgeCount;
                    index++;
                }
            }

            return index;
        }
    }


    [Serializable]
    public class cm_UserJudgeReportEntity
    {
        public cm_UserJudgeReportEntity()
        { }
        #region Model
        private int _id;
        private int? _memid;
        private string _memname;
        private decimal? _judge1;
        private decimal? _judge2;
        private decimal? _judge3;
        private decimal? _judge4;
        private decimal? _judge5;
        private decimal? _judge6;
        private decimal? _judge7;
        private decimal? _judge8;
        private decimal? _judge9;
        private decimal? _judge10;
        private decimal? _judge11;
        private decimal? _judge12;
        private int? _savestatu = 0;
        private int _kaoheunitid;
        private int _insertuserid;
        private DateTime _insertdate = DateTime.Now;
        private decimal? _judgecount = 0M;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? MemID
        {
            set { _memid = value; }
            get { return _memid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MemName
        {
            set { _memname = value; }
            get { return _memname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge1
        {
            set { _judge1 = value; }
            get { return _judge1; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge2
        {
            set { _judge2 = value; }
            get { return _judge2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge3
        {
            set { _judge3 = value; }
            get { return _judge3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge4
        {
            set { _judge4 = value; }
            get { return _judge4; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge5
        {
            set { _judge5 = value; }
            get { return _judge5; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge6
        {
            set { _judge6 = value; }
            get { return _judge6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge7
        {
            set { _judge7 = value; }
            get { return _judge7; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge8
        {
            set { _judge8 = value; }
            get { return _judge8; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge9
        {
            set { _judge9 = value; }
            get { return _judge9; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge10
        {
            set { _judge10 = value; }
            get { return _judge10; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge11
        {
            set { _judge11 = value; }
            get { return _judge11; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge12
        {
            set { _judge12 = value; }
            get { return _judge12; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SaveStatu
        {
            set { _savestatu = value; }
            get { return _savestatu; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int KaoHeUnitID
        {
            set { _kaoheunitid = value; }
            get { return _kaoheunitid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int InsertUserId
        {
            set { _insertuserid = value; }
            get { return _insertuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? JudgeCount
        {
            set { _judgecount = value; }
            get { return _judgecount; }
        }

        public decimal? Weights
        { get; set; }

        public int? IndexID
        { get; set; }
        #endregion Model
    }
}