﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitAllAllotRecordCompare.aspx.cs" Inherits="TG.Web.DeptBpm.UnitAllAllotRecordCompare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="../js/assets/plugins/data-tables/DT_bootstrap.css" />

    <script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/DT_bootstrap.js"></script>

    <script>
        $(function () {

            $("#btnDefault").click(function () {

                $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
            });

            
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>年度总奖金数据对比</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>年度总奖金数据对比</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>年度总奖金数据对比
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">项目部门:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged"
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">职位状态:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drpZw" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpZw_SelectedIndexChanged">
                                        <asp:ListItem Value="0">在职</asp:ListItem>
                                        <asp:ListItem Value="1">离职</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 130px;">
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Export" runat="server" Text="导出数据" OnClick="btn_Export_Click" />
                                </td>
                                <td>
                                    <% if (this.UserShortName == "admin")
                                       {

                                           if (!this.IsBak)
                                           {%>

                                    <a href="#" class="btn btn-sm yellow" id="btnBak">发起归档</a>
                                    <%}
                                           else {%> 
                                    <a href="#" class="btn btn-sm default disabled" >已归档</a>
                                           
                                           <%}
                                       } %>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th colspan="4">
                                        <a href="<%= RecordUrl %>" class="btn btn-sm blue">奖金分配</a>
                                        <a href="<%= RecordCompareUrl %>" class="btn btn-sm blue active">数据对比</a>
                                    </th>
                                    <th colspan="9"></th>
                                </tr>
                                <tr class="warning">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                </tr>
                                <tr>
                                    <th style="width: 90px;">部门名称</th>
                                    <th style="width: 70px;"><a href="#" id="btnDefault" title="点击恢复默认">姓名</a></th>
                                    <th style="width: 70px;">职位状态</th>
                                    <th style="width: 70px;">奖金合计(含预发)</th>
                                    <th style="width: 90px;">平均月工资</th>
                                    <th style="width: 90px;">平均月工资倍数</th>
                                    <th style="width: 90px;">平均月工资倍数排名</th>
                                    <th style="width: 95px;">部门内自评互评分数排名</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>年下奖金
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year %>年上奖金
                                        <%} %>
                                    </th>
                                    <th style="width: 90px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>下增福
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year %>上增福
                                        <%} %>
                                    </th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>年上奖金
                                         <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>年下奖金
                                        <%} %>
                                    </th>
                                    <th style="width: 90px;">

                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>上增福
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %>下增福
                                        <%} %>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    
                                    decimal? hjcol = 0;
                                    decimal? hjcol2 = 0;
                                    decimal? hjcol3 = 0;
                                    decimal? hjcol4 = 0;
                                    decimal? hjcol5 = 0;
                                    decimal? hjcol6 = 0;

                                    if (!IsBak) { 
                                    
                                    if (UnitMemsList.Rows.Count > 0)
                                    {
                                        decimal hj = 0;
                                        decimal pj = 0;
                                        decimal sbn = 0;
                                        decimal xbn = 0;
                                        foreach (System.Data.DataRow dr in UnitMemsList.Rows)
                                        {
                                            hj = 0;
                                %>

                                <tr>
                                    <td><span unitid="<%= dr["unitID"].ToString()%>"><%= dr["unitName"].ToString()%></span></td>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td>
                                        <% if (dr["mem_isFired"].ToString() == "0")
                                           { %>
                                        <span isfired="0">在职</span>
                                        <%}
                                           else
                                           { %>
                                        <span isfired="1">离职</span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% 
                                           hj = Convert.ToDecimal(dr["hj"].ToString())+Convert.ToDecimal(dr["yfjj"].ToString());
                                           //如果人员离职，合计小于0为0
                                           if (dr["mem_isFired"].ToString() == "1")
                                           {
                                               if (hj < 0)
                                               {
                                                   hj = 0;
                                               }
                                           } %>

                                        <%
                                            hjcol += Convert.ToDecimal(hj);
                                        %>

                                        <span><%= Convert.ToDecimal(hj).ToString("f0") %></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <% pj = Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2); %>

                                        <%
                                            hjcol2 += Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()));
                                        %>

                                        <span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString())) %></span>
                                    </td>
                                    <td style="text-align: right;"><span><%= pj!=0?Math.Round(hj/pj,2):0%></span></td>
                                    <td style="text-align: right;"><span><%= dr["OrderID"].ToString() %></span></td>
                                    <td style="text-align: right;"><span><%= dr["OrderID2"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <%
                                            if ((RenwuModel.StartDate.Year - 1) < 2016 && (RenwuModel.StartDate.Month < 8))
                                            {
                                                xbn = Convert.ToDecimal(dr["xbn2"].ToString());

                                                hjcol3 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Convert.ToDecimal(dr["xbn2"].ToString()).ToString("f0")%></span>
                                        <%}
                                            else
                                            {
                                                xbn = Convert.ToDecimal(dr["xbn"].ToString());

                                                hjcol3 += Convert.ToDecimal(dr["xbn"].ToString());
                                                
                                        %>
                                        <span><%= Convert.ToDecimal(dr["xbn"].ToString()).ToString("f0")%></span>
                                        <%}%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                            hjcol4 += (hj - xbn);
                                        %>
                                        <span><%= (hj-xbn).ToString("f0") %></span></td>
                                    <td style="text-align: right;">
                                        <% 
                                            if ((RenwuModel.StartDate.Year - 1) < 2016)
                                            {
                                                if (RenwuModel.StartDate.Month >= 8)
                                                {
                                                    sbn = Convert.ToDecimal(dr["xbn2"].ToString());

                                                    hjcol5 += Convert.ToDecimal(dr["xbn2"].ToString());
                                                    
                                        %>
                                        <span><%= Convert.ToDecimal(dr["xbn2"].ToString()).ToString("f0")%></span>
                                        <%
                                                }
                                                else
                                                {
                                                    sbn = Convert.ToDecimal(dr["sbn2"].ToString());
                                                    hjcol5 += Convert.ToDecimal(dr["sbn2"].ToString());
                                                    
                                        %>
                                        <span><%= Convert.ToDecimal(dr["sbn2"].ToString()).ToString("f0")%></span>
                                        <%} %>
                                        <%}
                                            else
                                            {
                                                sbn = Convert.ToDecimal(dr["sbn"].ToString());

                                                hjcol5 += Convert.ToDecimal(dr["sbn"].ToString());
                                                
                                        %>
                                        <span><%= Convert.ToDecimal(dr["sbn"].ToString()).ToString("f0")%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                            hjcol6 += (hj - sbn);
                                        %>

                                        <span><%= (hj-sbn).ToString("f0")%></span></td>

                                    <td></td>

                                </tr>
                                <%}

                                    }
                                    }
                                    else {

                                        AllHisList.ForEach(c => {%>
                                    
                                       <tr>
                                    <td><span><%= c.UnitName%></span></td>
                                    <td><span><%= c.UserName%></span></td>
                                    <td><span><%= c.IsFired%></span></td>
                                    <td style="text-align: right;">
                                        <%
                                            hjcol += Convert.ToDecimal(c.JJHJ);
                                        %>

                                        <span><%= Convert.ToDecimal(c.JJHJ).ToString("f0") %></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                            hjcol2 += Math.Round(Convert.ToDecimal(c.PJYGZ));
                                        %>

                                        <span><%= Math.Round(Convert.ToDecimal(c.PJYGZ)) %></span>
                                    </td>
                                    <td style="text-align: right;"><span><%= c.PJYGZBS%></span></td>
                                    <td style="text-align: right;"><span><%= Convert.ToDecimal(c.PJYGZPM).ToString("f0") %></span></td>
                                    <td style="text-align: right;"><span><%= Convert.ToDecimal(c.BMNHPPM).ToString("f0")%></span></td>
                                    <td style="text-align: right;">
                                        <%
                                            
                                                hjcol3 += Convert.ToDecimal(c.SBN);
                                        %>
                                        
                                      
                                        <span><%= Convert.ToDecimal(c.SBN).ToString("f0")%></span>
                                        
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                            hjcol4 += c.SBNZF;
                                        %>
                                        <span><%= Convert.ToDecimal(c.SBNZF).ToString("f0") %></span></td>
                                    <td style="text-align: right;">
                                        <% 
                                                    hjcol5 += Convert.ToDecimal(c.XBN);
                                                    
                                        %>
                                        
                                        <span><%= Convert.ToDecimal(c.XBN).ToString("f0")%></span>
                                      
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                            hjcol6 += c.XBNZF;
                                        %>

                                        <span><%= Convert.ToDecimal(c.XBNZF).ToString("f0")%></span></td>

                                    <td></td>

                                </tr>
                                    <% });}
                                %>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计：</td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol2).ToString("f0") %></td>
                                    <td style="text-align: right;"></td>
                                    <td style="text-align: right;"></td>
                                    <td style="text-align: right;"></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol3).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol4).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol5).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol6).ToString("f0") %></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--<input type="hidden" name="name" value="<%= IsSubmit %>" id="hidIsSubmit" />--%>
    <input type="hidden" name="name" value="<%= RenwuNo %>" id="hidRenwuno" />
    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo">


    <script>

        
        //复制合计到头部合计
        var setHeadHeji = function () {

            var arrFooter = $("#tbDataOne tfoot tr").find("td");
            var arrHeader = $("#tbDataOne thead tr").eq(1).find("th");
            if (arrFooter.length > 0) {
                arrFooter.each(function (i) {

                    arrHeader.eq(i).text(arrFooter.eq(i).text());
                });
            }
        }

        setHeadHeji();


        var _containerItem = $("#tbDataOne tbody tr");

        $("#btnBak").click(function () {

            if (!confirm("确定要发起年度总奖金对比归档吗？")) {
                return false;
            }
            if (_containerItem.length == 0) {
                alert("没有要归档的信息！");
                return false;
            }

            var renwuid = $("#hidRenwuno").val();

            //获取归档统计记录
            var arryData = new Array();
            _containerItem.each(function (index, domEle) {

                var rowdata = $(this);

                arryData[index] = {
                    ID: 0,
                    RenwuID: renwuid,
                    UnitName: $.trim(rowdata.children().eq(0).text()),
                    UserName: $.trim(rowdata.children().eq(1).text()),
                    IsFired: $.trim(rowdata.children().eq(2).text()),
                    JJHJ: $.trim(rowdata.children().eq(3).text()),
                    PJYGZ: $.trim(rowdata.children().eq(4).text()),
                    PJYGZBS: $.trim(rowdata.children().eq(5).text()),
                    PJYGZPM: $.trim(rowdata.children().eq(6).text()),
                    BMNHPPM: $.trim(rowdata.children().eq(7).text()),
                    SBN: $.trim(rowdata.children().eq(8).text()),
                    SBNZF: $.trim(rowdata.children().eq(9).text()),
                    XBN: $.trim(rowdata.children().eq(10).text()),
                    XBNZF: $.trim(rowdata.children().eq(11).text())
                }

            });

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "allcpbak";

            var jsonData = JSON.stringify(arryData);

            var data = { "action": action, "isNew": 0, "data": jsonData, "unitname": $("#ctl00_ContentPlaceHolder1_drp_unit3 option:selected").text() };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == 1) {
                        alert("年度奖金对比数据归档成功!");

                        $("#btnBak").attr("class","btn btn-sm default disabled").text("已归档");
                    }
                    else if (result == 2) {
                        alert('已归档，无需重复归档！');
                    }
                    else if (result == 3) {
                        alert("考核任务未归档，请先归档考核任务！");
                    }

                }
                else {
                    alert("错误！");
                }
            });
        });

        //给table排序
        var initOrderTable = function () {

            $("#tbDataOne").dataTable({
               <%if (drp_unit3.SelectedValue != "-1")
                 {%>
                "bPaginate": false,
                <%}%>

                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [0] },
                    { "bSortable": false, "aTargets": [1] },
                    { "bSortable": false, "aTargets": [2] },
                    { "bSortable": true, "aTargets": [3] },
                    { "bSortable": true, "aTargets": [4] },
                    { "bSortable": true, "aTargets": [5] },
                    { "bSortable": true, "aTargets": [6] },
                    { "bSortable": true, "aTargets": [7] },
                    { "bSortable": false, "aTargets": [8] },
                    { "bSortable": false, "aTargets": [9] },
                    { "bSortable": false, "aTargets": [10] },
                    { "bSortable": false, "aTargets": [11] },
                    { "bSortable": false, "aTargets": [12] }
                ],
                "iDisplayLength": 15
            });


            jQuery('#tbDataOne_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
            jQuery('#tbDataOne_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
            //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
        }


        initOrderTable();
    </script>
</asp:Content>
