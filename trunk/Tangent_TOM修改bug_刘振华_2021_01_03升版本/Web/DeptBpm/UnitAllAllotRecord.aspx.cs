﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Text;


namespace TG.Web.DeptBpm
{
    public partial class UnitAllAllotRecord : PageBase
    {
        public bool t=true;
        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }
        /// <summary>
        /// 总奖金分配
        /// </summary>
        public string RecordUrl
        {
            get
            {
                return "/DeptBpm/UnitAllAllotRecord.aspx?unitid=" + CurUnitID + "&renwuno=" + RenwuNo + "&action=" + ActionDo;
            }
        }
        /// <summary>
        /// 数据对比
        /// </summary>
        public string RecordCompareUrl
        {
            get
            {
                return "/DeptBpm/UnitAllAllotRecordCompare.aspx?unitid=" + this.drp_unit3.SelectedValue + "&renwuno=" + RenwuNo + "&action=" + ActionDo;
            }
        }

        /// <summary>
        /// 选中的ID
        /// 
        /// </summary>
        public int CurUnitID
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["unitid"] ?? "0", out unitid);
                if (unitid == 0)
                {
                    unitid = Convert.ToInt32(this.drp_unit3.SelectedValue);
                }
                return unitid;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public int RenwuNo
        {
            get
            {
                int renwuno = 0;
                int.TryParse(Request["renwuno"] ?? "0", out renwuno);
                return renwuno;
            }
        }

        /// <summary>
        /// Action
        /// </summary>
        public int ActionDo
        {
            get
            {
                int id = 0;
                int.TryParse(Request["action"] ?? "0", out id);
                return id;
            }
        }
        /// <summary>
        /// 考核任务
        /// </summary>
        public TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(this.RenwuNo);
                return model;
            }
        }

        public string IsCheck
        {
            get
            {
                return Request["check"] ?? "0";
            }
        }

        public bool IsBak
        {
            get
            {
                //if (t)
                //{
                    string strWhere = string.Format(" RenwuID={0} AND IsFired='{1}'", this.RenwuNo, this.drpZw.SelectedItem.Text);

                    if (this.drp_unit3.SelectedValue != "-1")
                    {
                        if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                        {
                            strWhere = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '建筑一部%' OR UnitName like '建筑二部%') ", this.RenwuNo, this.drpZw.SelectedItem.Text);
                        }
                        else
                        {
                            strWhere = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '{2}%') ", this.RenwuNo, this.drpZw.SelectedItem.Text, this.drp_unit3.SelectedItem.Text.Trim());
                        }
                    }


                    int count = new TG.BLL.cm_KaoHeAllMemsHis().GetRecordCount(strWhere);

                    if (count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                //}
                //else
                //{
                //    string strWhere = string.Format(" RenwuID={0} AND IsFired='{1}'", this.RenwuNo, this.drpZw.SelectedItem.Text);

                //    if (this.drp_unit3.SelectedValue != "-1")
                //    {
                //        if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                //        {
                //            strWhere = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '建筑一部%' OR UnitName like '建筑二部%') ", this.RenwuNo, this.drpZw.SelectedItem.Text);
                //        }
                //        else
                //        {
                //            strWhere = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '{2}%') ", this.RenwuNo, this.drpZw.SelectedItem.Text, this.drp_unit3.SelectedItem.Text.Trim());
                //        }
                //    }


                //    int count = new TG.BLL.cm_KaoHeAllMemsHis().GetRecordCount(strWhere);

                //    if (count > 0)
                //    {
                //        return true;
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}
              
            }
        }

        public List<TG.Model.cm_KaoHeAllMemsHis> AllHisList
        {
            get
            {
                string strWhere = string.Format(" RenwuID={0}", this.RenwuNo);

                if (this.drp_unit3.SelectedValue != "-1")
                {
                    if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                    {
                        strWhere = string.Format(" RenwuID={0} AND (UnitName like'建筑一部%' OR UnitName like '建筑二部%')", this.RenwuNo, this.drp_unit3.SelectedItem.Text.Trim());
                    }
                    else
                    {
                        strWhere = string.Format(" RenwuID={0} AND UnitName='{1}'", this.RenwuNo, this.drp_unit3.SelectedItem.Text.Trim());
                    }
                }
                //离职 在职
                strWhere += string.Format(" AND IsFired='{0}'", this.drpZw.SelectedItem.Text);

                return new TG.BLL.cm_KaoHeAllMemsHis().GetModelList(strWhere).OrderBy(c => c.ID).ToList();
            }
        }
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var list = bll_unit.GetModelList(" unit_ParentID<>0 AND unit_ID<>230 ");
            TG.BLL.cm_DropUnitList bll_drop = new BLL.cm_DropUnitList();
            var listShow = bll_drop.GetModelList(" flag=0 ");
            var listExt = new TG.BLL.tg_unitExt().GetModelList("");
            //查询不显示部门
            var query = from u in list
                        join p in listShow on u.unit_ID equals p.unit_ID
                        select u;

            var showList = query.ToList();
            //部门排序
            var result = from u in showList
                         join p in listExt on u.unit_ID equals p.unit_ID
                         orderby p.unit_Order ascending
                         select u;

            this.drp_unit3.DataSource = result.ToList();
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }

        /// <summary>
        /// 其他部门
        /// </summary>
        public List<int> SpecialUnitList
        {
            get
            {
                //return new List<int>() { 
                //    230,
                //    233,
                //    235,
                //    236,
                //    234,
                //    235,
                //    240,
                //    241,
                //    242,
                //    243,
                //    244
                //};

                //所有不是施工图部门的部门ID
                // qpl 2016年11月23日
                List<int> list = new List<int>();
                // 0 为施工部门  
                string strWhere = string.Format(" unit_Type<>{0}", 0);
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(strWhere);

                unitlist.ForEach(c =>
                {
                    list.Add(c.unit_ID);
                });

                return list;
            }
        }
        /// <summary>
        /// 本部门所有在职人员
        /// </summary>
        protected DataTable UnitMemsList
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }
                //分配部门
                string strWhereUnit = "";
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                    {
                        strWhereUnit = string.Format(" AND (m.mem_Unit_ID IN (231,232)) AND (KM.memUnitID IN (231,232)) ", this.RenwuNo, this.drp_unit3.SelectedItem.Text.Trim());
                    }
                    else
                    {
                        strWhereUnit = string.Format(" AND m.mem_Unit_ID={0} AND KM.memUnitID={0}", this.drp_unit3.SelectedValue);
                    }
                }

                string strWhereUnit2 = "";

                if (this.drp_unit3.SelectedValue != "-1")
                {
                    if (this.drp_unit3.SelectedItem.Text.Trim() == "建筑部" && RenwuModel.StartDate.Year < 2017)
                    {
                        strWhereUnit2 = string.Format(" AND (m.mem_Unit_ID IN (231,232)) ", this.drp_unit3.SelectedValue);
                    }
                    else
                    {
                        strWhereUnit2 = string.Format(" AND m.mem_Unit_ID={0} ", this.drp_unit3.SelectedValue);
                    }
                }

                string strSql = "";
                if (this.drpZw.SelectedValue == "0")
                {
                    //    strSql = string.Format(@" select m.mem_ID
                    //                             ,mem_Name
                    //                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                    //                                ,m.mem_Unit_ID AS unitID
                    //                                ,m.mem_isFired
                    //                                ,isnull((Select SUM(xmjjmng) From dbo.cm_KaoHeMemsMngAllot Where MemID=m.mem_ID AND RenwuID={3}),0) AS MngCount
                    //                                ,isnull((select bmjl from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmjl 
                    //                                ,isnull((select gstz from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS gstz 
                    //                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2}),0) AS yfjj 
                    //                                ,isnull((select SUM(AllotCountBefore) from dbo.cm_KaoHeMemsAllotChange where RenwuID={3} AND MemID=m.mem_ID),0) AS AllotCountBefore
                    //                                ,isnull((select SUM(bmnjs) from dbo.cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjs
                    //                                ,isnull((select SUM(xmjjjs)+SUM(xmjjmng) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjj
                    //                                ,isnull((select SUM(yxyg) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS yxyg
                    //                                From dbo.cm_KaoHeRenwuMem KM inner join tg_member m on KM.memID=m.mem_ID left join tg_memberRole r on m.mem_ID=r.mem_Id
                    //                                where (m.mem_ID NOT IN ({4})) {0} AND m.mem_isFired={1} AND m.mem_Unit_ID<>230 AND KM.Statu=0 AND KM.IsOutUnitID=0 AND (KM.KaoHeUnitID IN ( Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{3}')) 
                    //                                Order By m.mem_Unit_ID, m.mem_Order ", strWhereUnit, this.drpZw.SelectedValue, strWhere, this.RenwuNo, GuDongIdStr);

                    strSql = string.Format(@" select m.mem_ID
	                                            ,mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((Select SUM(xmjjmng) From dbo.cm_KaoHeMemsMngAllot Where MemID=m.mem_ID AND RenwuID={3}),0) AS MngCount
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2}),0) AS yfjj 
                                                ,isnull((select SUM(AllotCountBefore) from dbo.cm_KaoHeMemsAllotChange where RenwuID={3} AND MemID=m.mem_ID),0) AS AllotCountBefore
                                                ,isnull((select bmjl from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmjl 
                                                ,isnull((select gstz from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS gstz 
                                                ,isnull((select xmjjjs from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjjjs 
                                                ,isnull((select SUM(bmnjs) from dbo.cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjjjs 
                                                ,isnull((select bmnjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjj
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj
                                                ,isnull((select SUM(yxyg) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS yxyg
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj 
                                                ,isnull((select tcgx from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS tcgx 
                                                ,isnull((select bimjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bimjj 
                                                ,isnull((select ghbt from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS ghbt 
                                                ,isnull((select hj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS hj 
                                                ,isnull((select xmjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjj 
                                                ,isnull((select zcfy from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS zcfy
                                                From dbo.cm_KaoHeRenwuMem KM inner join tg_member m on KM.memID=m.mem_ID left join tg_memberRole r on m.mem_ID=r.mem_Id
                                                where 1=1 {0} AND m.mem_isFired={1} AND m.mem_Unit_ID<>230 AND KM.Statu=0 AND KM.IsOutUnitID=0 
                                                AND (KM.KaoHeUnitID = ( Select ID From dbo.cm_KaoHeRenwuUnit Where UnitID=m.mem_Unit_ID AND RenWuNo='{3}'))
                                                 AND NOT EXISTS ( Select mem_ID 
					                                             From tg_member  
					                                             Where (mem_ID=1440 OR mem_ID=1441
					                                             OR mem_ID=1442 OR mem_ID=1443 OR mem_ID=1445 OR mem_ID=1446
					                                             OR mem_ID=1474 OR mem_ID=1493 OR mem_ID=1498 OR mem_ID=1519
					                                             OR mem_ID=1538 OR mem_ID=1554 OR mem_ID=1577) AND mem_ID=m.mem_ID)
                                                Order By m.mem_Unit_ID,m.mem_Order ", strWhereUnit, this.drpZw.SelectedValue, strWhere, this.RenwuNo, strWhereUnit2, GuDongIdStr);

                }
                else
                {
                    //strSql = string.Format(@" select m.mem_ID
                    //                         ,m.mem_Name
                    //                            ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                    //                            ,m.mem_Unit_ID AS unitID
                    //                            ,m.mem_isFired
                    //                            ,isnull((Select SUM(bmjltz) From dbo.cm_KaoHeMemsOutMngAllot Where MemID=m.mem_ID AND RenwuID={3}),0) AS MngCount
                    //                            ,isnull((select bmjl from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmjl   
                    //                            ,isnull((select gstz from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS gstz                                               
                    //                            ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2}),0) AS yfjj 
                    //                            ,isnull((select SUM(AllotCountBefore) from dbo.cm_KaoHeMemsAllotChange where RenwuID={3} AND MemID=m.mem_ID),0) AS AllotCountBefore
                    //                            ,isnull((select SUM(bmnjs) from dbo.cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjs
                    //                            ,isnull((select SUM(xmjjjs)+SUM(xmjjmng) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjj
                    //                            ,isnull((select SUM(yxyg) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS yxyg
                    //                            From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.tg_memberExt ex on m.mem_ID=ex.mem_ID
                    //                            where (m.mem_ID NOT IN ({6})) {0} AND m.mem_isFired={1} AND (ex.mem_OutTime BETWEEN '{5}' AND '{4}') 
                    //                            Order By m.mem_Unit_ID,m.mem_Order ", strWhereUnit2, this.drpZw.SelectedValue, strWhere, this.RenwuNo, RenwuModel.EndDate, RenwuModel.StartDate, GuDongIdStr);

                    strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((Select SUM(xmjjmng) From dbo.cm_KaoHeMemsMngAllot Where MemID=m.mem_ID AND RenwuID={3}),0) AS MngCount
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2}),0) AS yfjj 
                                                ,isnull((select SUM(AllotCountBefore) from dbo.cm_KaoHeMemsAllotChange where RenwuID={3} AND MemID=m.mem_ID),0) AS AllotCountBefore
                                                ,isnull((select bmjl from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmjl 
                                                ,isnull((select gstz from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS gstz 
                                                ,isnull((select xmjjjs from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjjjs 
                                               ,isnull((select SUM(bmnjs) from dbo.cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjjjs 
                                                ,isnull((select bmnjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjj
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj
                                                ,isnull((select SUM(yxyg) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS yxyg
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj 
                                                ,isnull((select tcgx from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS tcgx 
                                                ,isnull((select bimjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bimjj 
                                                ,isnull((select ghbt from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS ghbt 
                                                ,isnull((select hj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS hj 
                                                ,isnull((select xmjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjj 
                                                ,isnull((select zcfy from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS zcfy 
                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.tg_memberExt ex on m.mem_ID=ex.mem_ID
                                                where 1=1 {0} AND m.mem_isFired={1} AND (ex.mem_OutTime BETWEEN '{5}' AND '{4}') AND m.mem_ID NOT IN ({6})
                                                Order By m.mem_Unit_ID,m.mem_Order ", strWhereUnit, this.drpZw.SelectedValue, strWhere, this.RenwuNo, RenwuModel.EndDate, RenwuModel.StartDate, GuDongIdStr);

                }


                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }
        /// <summary>
        /// 股东ID
        /// </summary>
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        //<summary>
        //本部门所有人员
        //</summary>
        protected DataTable UnitMemsSaveList
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }
                //分配部门
                string strWhereUnit = "";
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    strWhereUnit = string.Format(" AND m.mem_Unit_ID={0}", this.drp_unit3.SelectedValue);
                }
                //设计部门
                string strWhereUnit2 = " AND (MemID IN (Select mem_ID From tg_member Where m.mem_Unit_ID IN (231,232,237,238,239))) ";

                string strSql = "";
                if (this.drpZw.SelectedValue == "0")
                {
                    strSql = string.Format(@" select m.mem_ID
	                                            ,mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2}),0) AS yfjj 
                                                ,isnull((select SUM(AllotCountBefore) from dbo.cm_KaoHeMemsAllotChange where RenwuID={3} AND MemID=m.mem_ID),0) AS AllotCountBefore
                                                ,isnull((select bmjl from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmjl 
                                                ,isnull((select gstz from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS gstz 
                                                ,isnull((select xmjjjs from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjjjs 
                                                ,isnull((select SUM(bmnjs) from dbo.cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjjjs 
                                                ,isnull((select bmnjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjj
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj
                                                ,isnull((select SUM(yxyg) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS yxyg
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj 
                                                ,isnull((select tcgx from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS tcgx 
                                                ,isnull((select bimjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bimjj 
                                                ,isnull((select ghbt from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS ghbt 
                                                ,isnull((select hj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS hj 
                                                ,isnull((select xmjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjj 
                                                ,isnull((select zcfy from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS zcfy
                                                From dbo.cm_KaoHeRenwuMem KM inner join tg_member m on KM.memID=m.mem_ID left join tg_memberRole r on m.mem_ID=r.mem_Id
                                                where 1=1 {0} AND m.mem_isFired={1} AND m.mem_Unit_ID<>230 AND KM.Statu=0 AND KM.IsOutUnitID=0 
                                                AND (KM.KaoHeUnitID = ( Select ID From dbo.cm_KaoHeRenwuUnit Where UnitID=m.mem_Unit_ID AND RenWuNo='{3}'))
                                                 AND NOT EXISTS ( Select mem_ID 
					                                             From tg_member  
					                                             Where (mem_ID=1440 OR mem_ID=1441
					                                             OR mem_ID=1442 OR mem_ID=1443 OR mem_ID=1445 OR mem_ID=1446
					                                             OR mem_ID=1474 OR mem_ID=1493 OR mem_ID=1498 OR mem_ID=1519
					                                             OR mem_ID=1538 OR mem_ID=1554 OR mem_ID=1577) AND mem_ID=m.mem_ID)
                                                Order By m.mem_Unit_ID,m.mem_Order ", strWhereUnit, this.drpZw.SelectedValue, strWhere, this.RenwuNo, strWhereUnit2, GuDongIdStr);
                }
                else
                {
                    strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,(Select unit_Name From tg_unit Where unit_ID=m.mem_Unit_ID) AS unitName
                                                ,m.mem_Unit_ID AS unitID
                                                ,m.mem_isFired
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {2}),0) AS yfjj 
                                                ,isnull((select SUM(AllotCountBefore) from dbo.cm_KaoHeMemsAllotChange where RenwuID={3} AND MemID=m.mem_ID),0) AS AllotCountBefore
                                                ,isnull((select bmjl from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmjl 
                                                ,isnull((select gstz from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS gstz 
                                                ,isnull((select xmjjjs from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjjjs 
                                                ,isnull((select SUM(bmnjs) from dbo.cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjjjs 
                                                ,isnull((select bmnjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bmnjj
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj
                                                ,isnull((select SUM(yxyg) from cm_KaoHeMemsMngAllot where RenwuID={3} AND MemID=m.mem_ID),0) AS yxyg
                                                ,isnull((select xmglj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmglj 
                                                ,isnull((select tcgx from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS tcgx 
                                                ,isnull((select bimjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS bimjj 
                                                ,isnull((select ghbt from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS ghbt 
                                                ,isnull((select hj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS hj 
                                                ,isnull((select xmjj from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS xmjj 
                                                ,isnull((select zcfy from dbo.cm_KaoHeAllMemsAllotDetails where RenwuID={3} AND MemID=m.mem_ID),0) AS zcfy 
                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.tg_memberExt ex on m.mem_ID=ex.mem_ID
                                                where 1=1 {0} AND m.mem_isFired={1} AND (ex.mem_OutTime BETWEEN '{5}' AND '{4}') AND m.mem_ID NOT IN ({6})
                                                Order By m.mem_Unit_ID,m.mem_Order ", strWhereUnit, this.drpZw.SelectedValue, strWhere, this.RenwuNo, RenwuModel.EndDate, RenwuModel.StartDate, GuDongIdStr);

                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsSave
        {

            get
            {
                if (t)//判断是否为true 添加填不上，同时不对
                {
                    bool result = false;
                    string strWhere = string.Format(" RenwuID={0} AND IsFired={1}", this.RenwuNo, this.drpZw.SelectedValue);
                    if (this.drp_unit3.SelectedValue != "-1")
                    {
                        strWhere = string.Format(" RenwuID={0} AND IsFired={1} AND UnitID={2}", this.RenwuNo, this.drpZw.SelectedValue, this.drp_unit3.SelectedValue);
                    }

                    var list = new TG.BLL.cm_KaoHeAllMemsAllotDetails().GetModelList(strWhere);

                    if (list.Count > 0)
                    {
                        result = true;
                    }

                    return result;
                }
                else
                {
                    bool result = true;
                    string strWhere = string.Format(" RenwuID={0} AND IsFired={1}", this.RenwuNo, this.drpZw.SelectedValue);
                    if (this.drp_unit3.SelectedValue != "-1")
                    {
                        strWhere = string.Format(" RenwuID={0} AND IsFired={1} AND UnitID={2}", this.RenwuNo, this.drpZw.SelectedValue, this.drp_unit3.SelectedValue);
                    }

                    var list = new TG.BLL.cm_KaoHeAllMemsAllotDetails().GetModelList(strWhere);

                    if (list.Count > 0)
                    {
                        result = false;
                    }

                    return result;
                }
               
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsSubmit
        {

           get
            {

                if (t)
                {
                    bool result = false;
                    string strWhere = string.Format(" RenwuID={0} AND IsFired={1} AND Stat=1", this.RenwuNo, this.drpZw.SelectedValue);
                    if (this.drp_unit3.SelectedValue != "-1")
                    {
                        strWhere = string.Format(" RenwuID={0} AND IsFired={1} AND UnitID={2} AND Stat=1", this.RenwuNo, this.drpZw.SelectedValue, this.drp_unit3.SelectedValue);
                    }

                    var list = new TG.BLL.cm_KaoHeAllMemsAllotDetails().GetModelList(strWhere);

                    if (list.Count > 0)
                    {
                        result = true;
                    }

                    return result;

                }
                else
                {
                    bool result = false;
                    string strWhere = string.Format(" RenwuID={0} AND IsFired={1} AND Stat=1", this.RenwuNo, this.drpZw.SelectedValue);
                    if (this.drp_unit3.SelectedValue != "-1")
                    {
                        strWhere = string.Format(" RenwuID={0} AND IsFired={1} AND UnitID={2} AND Stat=1", this.RenwuNo, this.drpZw.SelectedValue, this.drp_unit3.SelectedValue);
                    }

                    var list = new TG.BLL.cm_KaoHeAllMemsAllotDetails().GetModelList(strWhere);

                    if (list.Count > 0)
                    {
                        result = false;
                    }

                    return result;
                }

             }
            
        }
      
 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();

                if (this.drp_unit3.Items.FindByValue(this.CurUnitID.ToString()) != null)
                {
                    this.drp_unit3.Items.FindByValue(this.CurUnitID.ToString()).Selected = true;
                }
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }

        protected void btn_Export_Click(object sender, EventArgs e)
        {
            if (!IsBak)
            {
                ExportIn();
            }
            else
            {
                ExportInHis();
            }

        }

        protected void btn_Export_Clickcx(object sender, EventArgs e)
        {

            t = false;
          
        }
     

        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void drpZw_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        public void ExportIn()
        {
            DataTable dt = this.UnitMemsSaveList;

            string modelPath = " ~/TemplateXls/nianduzongjianjin.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["unitName"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                if (dt.Rows[i]["mem_isFired"].ToString() == "0")
                {
                    cell.SetCellValue("在职");
                }
                else
                {
                    cell.SetCellValue("离职");
                }

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmjl"].ToString()));


                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["gstz"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yfjj"].ToString()));
                //项目计算值
                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjjs"].ToString()));
                //部门内计算值
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmnjjjs"].ToString()));
                //项目奖金
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjj"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmnjj"].ToString()));

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmglj"].ToString()));

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yxyg"].ToString()));

                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["tcgx"].ToString()));

                cell = dataRow.CreateCell(13);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bimjj"].ToString()));

                cell = dataRow.CreateCell(14);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["zcfy"].ToString()));

                cell = dataRow.CreateCell(15);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["ghbt"].ToString()));

                cell = dataRow.CreateCell(16);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hj"].ToString()));
            }
            //导出文件名
            string strFileName = string.Format("{0}员工年度总奖金导出.xls", this.drpZw.SelectedItem.Text.Trim());
            if (this.drp_unit3.SelectedIndex > 0)
            {
                strFileName = string.Format("{0}{1}员工年度总奖金导出.xls", this.drp_unit3.SelectedItem.Text.Trim(), this.drpZw.SelectedItem.Text.Trim());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(strFileName);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }


        public void ExportInHis()
        {
            var hisList = this.AllHisList;

            if (hisList.Count > 0)
            {
                string modelPath = " ~/TemplateXls/nianduzongjianjin.xls";

                HSSFWorkbook wb = null;

                //如果没有模板路径，则创建一个空的workbook和一个空的sheet
                if (string.IsNullOrEmpty(modelPath))
                {
                    wb = new HSSFWorkbook();
                    wb.CreateSheet();
                    wb.GetSheetAt(0).CreateRow(0);
                }
                else
                {
                    using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        wb = new HSSFWorkbook(fileStream);
                        fileStream.Close();
                    }
                }


                //内容样式
                ICellStyle style2 = wb.CreateCellStyle();
                style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style2.VerticalAlignment = VerticalAlignment.CENTER;
                style2.WrapText = true;
                style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font2 = wb.CreateFont();
                font2.FontHeightInPoints = 10;//字号
                font2.FontName = "宋体";//字体
                style2.SetFont(font2);

                //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
                var ws = wb.GetSheet("Sheet1");
                if (ws == null)
                    ws = wb.GetSheetAt(0);

                int row = 2;
                for (int i = 0; i < hisList.Count; i++)
                {
                    var model = hisList[i];

                    var dataRow = ws.GetRow(i + row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(i + row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)

                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.UnitName);

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(model.UserName);

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;

                    cell.SetCellValue(model.IsFired);

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.BMJLTZ));


                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.GSTZ));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.YFJJ));
                    //项目计算值
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.XMJJJS));
                    //部门内计算值
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.BMJJJS));
                    //项目奖金
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.XMJJ));

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.BMNJJ));

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.XMGLJ));

                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.YXYG));

                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.TCGX));

                    cell = dataRow.CreateCell(13);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.BIM));

                    cell = dataRow.CreateCell(14);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.ZCFY));


                    cell = dataRow.CreateCell(15);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.GHBZ));

                    cell = dataRow.CreateCell(16);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(model.HJ));
                }

                //导出文件名
                string strFileName = string.Format("{0}员工年度总奖金导出.xls", this.drpZw.SelectedItem.Text.Trim());
                if (this.drp_unit3.SelectedIndex > 0)
                {
                    strFileName = string.Format("{0}{1}员工年度总奖金导出.xls", this.drp_unit3.SelectedItem.Text.Trim(), this.drpZw.SelectedItem.Text.Trim());
                }

                using (MemoryStream memoryStream = new MemoryStream())
                {

                    wb.Write(memoryStream);

                    string name = System.Web.HttpContext.Current.Server.UrlEncode(strFileName);
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                    Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.ContentEncoding = Encoding.UTF8;
                    wb = null;
                    Response.End();
                }
            }
        }
        /// <summary>
        /// 查询所有施工图部门ID   qpl 2016年11月23日
        /// </summary>
        public string UnitArchString
        {
            get
            {
                // qpl 2016年11月23日
                string str = "";
                // 0 为施工部门  
                string strWhere = string.Format(" unit_Type={0}", 0);
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(strWhere);

                unitlist.ForEach(c =>
                {
                    str += c.unit_ID + ",";
                });

                return str;
            }
        }
        /// <summary>
        /// 查询本次未参与考核人员
        /// </summary>
        public List<TG.Model.cm_KaoHeRenwuMem> UnKaoheMems
        {
            get
            {
                //已设置考核人员
                string strWhere = string.Format(" Statu=1 AND KaoHeUnitID IN (Select ID From cm_KaoHeRenwuUnit Where RenWuNo='{0}')", this.RenwuNo);
                var unKaoheMems = new BLL.cm_KaoHeRenwuMem().GetModelList(strWhere);

                return unKaoheMems;
            }
        }
    }
}