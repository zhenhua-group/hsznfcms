﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.DeptBpm
{
    public partial class StartUnitAllot : PageBase //System.Web.UI.Page
    {
        /// <summary>
        /// 考核任务
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public string RenwuNo
        {
            get
            {
                return this.drpRenwu.SelectedValue;
            }
        }
        /// <summary>
        /// 考核任务
        /// </summary>
        public TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));
                return model;
            }
        }
        /// <summary>
        /// 任务总奖金数
        /// </summary>
        public decimal? RenwuAllCount
        {
            get
            {
                decimal? result = 0;
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));

                if (model != null)
                {
                    result = model.JianAllCount;
                }
                return result;
            }
        }
        /// <summary>
        /// 是否已经保存
        /// </summary>
        public bool IsSave
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0} ", this.RenwuNo);
                var list = new TG.BLL.cm_KaoHeUnitAllotReport().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        /// <summary>
        /// 是否已经保存
        /// </summary>
        public bool IsSubmit
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0} AND Stat=1 ", this.RenwuNo);
                var list = new TG.BLL.cm_KaoHeUnitAllotReport().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }

        public bool IsBak
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0}",this.RenwuNo);

                int count = new TG.BLL.cm_KaoHeUnitAllotHis().GetRecordCount(strWhere);

                if(count>0)
                {
                    result = true;
                }

                return result;
            }
        }
        /// <summary>
        /// 上次是否归档
        /// </summary>
        public bool IsPrevYearBak
        {
            get
            {
                var renwuList = new TG.BLL.cm_KaoHeRenWu().GetModelList(XBNWhere);
                if (renwuList.Count > 0)
                {
                    int renwuID = renwuList[0].ID;

                    string strWhere = string.Format(" RenwuID={0}", renwuID);

                    int count = new TG.BLL.cm_KaoHeUnitAllotHis().GetRecordCount(strWhere);

                    if (count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 下次是否归档
        /// </summary>
        public bool IsPrevYearTwoBak
        {
            get
            {
                var renwuList = new TG.BLL.cm_KaoHeRenWu().GetModelList(SBNWhere);
                if (renwuList.Count > 0)
                {
                    int renwuID = renwuList[0].ID;

                    string strWhere = string.Format(" RenwuID={0}", renwuID);

                    int count = new TG.BLL.cm_KaoHeUnitAllotHis().GetRecordCount(strWhere);

                    if (count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 股东ID
        /// </summary>
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        /// <summary>
        /// 比例
        /// </summary>
        public DataTable ProjAllotUnitList
        {

            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }

                string renwuid = this.drpRenwu.SelectedValue;
                //添加 西安分公司  282 2017年4月18日
                string strSql = string.Format(@"Select unit.unit_ID,unit.unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCountBefore) AS AllotCount
	                                            From dbo.cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({3})) AND RenwuID={0}
                                            ),0) AS AllotCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND a.mem_Unit_ID=unit.unit_ID AND mem_isFired=0) AS MemCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND mem_isFired=0) AS AllMemCount                                            
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where b.mem_Unit_ID=unit.unit_ID {1}),0) AS BumenGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where 1=1 AND b.mem_isFired=0 AND (b.mem_ID NOT IN({3})) {1}),0) AS AllBumemGz      
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join cm_KaoHeRenwuMem KM on KM.memID=a.MemID left join tg_member b on a.MemID= b.mem_ID where KM.Statu=0 AND (KM.KaoHeUnitID IN ( Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}')) AND b.mem_Unit_ID=unit.unit_ID AND KM.memUnitID=unit.unit_ID AND (KM.memID Not IN (Select mem_ID From tg_memberRole Where RoleIdMag=5)) {1}),0)/6 AS AvgBumemGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where 1=1 AND b.mem_isFired=0 AND (b.mem_ID NOT IN({3})) {1}),0)/6 AS AvgBumemGzAll 
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {4}))),0) AS sbn_unbak
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {5}))),0) AS xbn_unbak
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {4}),0) AS sbn
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {5}),0) AS xbn
                                            ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{2}')-1),0) AS sbn2
                                            ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{2}')-1),0) AS xbn2
                                            From tg_unit unit left join tg_unitExt ext on unit.unit_ID=ext.unit_ID
                                            Where unit_ParentID<>0 
                                                  AND unit.unit_ID IN (231,233,234,236,237,238,239,281,282)
                                                  AND EXISTS (select ID from cm_DropUnitList where flag=0 AND unit.unit_ID= unit.unit_ID)
                                            Order By ext.unit_Order", renwuid, strWhere, RenwuModel.StartDate, GuDongIdStr, SBNWhere, XBNWhere);
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    strSql = string.Format(@"Select unit_ID,unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCountBefore) AS AllotCount
	                                            From dbo.cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({4})) AND RenwuID={0}
                                            ),0) AS AllotCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND a.mem_Unit_ID=unit.unit_ID AND mem_isFired=0) AS MemCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND mem_isFired=0) AS AllMemCount
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where b.mem_Unit_ID=unit.unit_ID {2}),0) AS BumenGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where 1=1 AND b.mem_isFired=0 AND (b.mem_ID NOT IN({4})) {2}),0) AS AllBumemGz                                            
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join cm_KaoHeRenwuMem KM on KM.memID=a.MemID left join tg_member b on a.MemID= b.mem_ID where KM.Statu=0 AND (KM.KaoHeUnitID IN ( Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}')) AND b.mem_Unit_ID=unit.unit_ID AND KM.memUnitID=unit.unit_ID AND (KM.memID Not IN (Select mem_ID From tg_memberRole Where RoleIdMag=5)) {2}),0)/6 AS AvgBumemGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where 1=1 AND b.mem_isFired=0 AND (b.mem_ID NOT IN({4})) {2}),0)/6 AS AvgBumemGzAll 
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {5}))),0) AS sbn_unbak
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {6}))),0) AS xbn_unbak
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {5}),0) AS sbn
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {6}),0) AS xbn
                                            ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{2}')-1),0) AS sbn2
                                            ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{2}')-1),0) AS xbn2
                                            From tg_unit unit
                                            Where unit_ID={1} AND unit_ParentID<>0 AND EXISTS (select ID from cm_DropUnitList where flag=0 AND unit_ID= unit.unit_ID)", renwuid, this.drp_unit3.SelectedValue, strWhere, RenwuModel.StartDate, GuDongIdStr, SBNWhere, XBNWhere);
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }
        /// <summary>
        /// 部门分配列表
        /// </summary>
        public DataTable UnitAllotList
        {
            get;
            set;
        }

        private void GetResultDataTable()
        {
            string strWhere = "";
            if (RenwuModel != null)
            {
                strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                {
                    strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                }
            }
            //2017年5月23日 去掉232
            string renwuid = this.drpRenwu.SelectedValue;
            string strSql = string.Format(@"Select unit.unit_ID,unit.unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCountBefore) AS AllotCount
	                                            From dbo.cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({3})) AND RenwuID={0}
                                            ),0) AS AllotCount 
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join cm_KaoHeRenwuMem KM on KM.memID=a.MemID left join tg_member b on a.MemID= b.mem_ID where KM.Statu=0 AND (KM.KaoHeUnitID IN ( Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}')) AND b.mem_Unit_ID=unit.unit_ID AND KM.memUnitID=unit.unit_ID AND (a.memID NOT IN ({3})) {1}),0)/6 AS AvgBumemGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where 1=1 AND b.mem_isFired=0 AND (b.mem_ID NOT IN({3})) {1}),0)/6 AS AvgBumemGzAll 
                                            ,(select AvgGongzi from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS AvgGongzi
                                            ,(select Unitjx from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjx
                                            ,(select Unitjiangbi from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjiangbi
                                            ,(select SUM(Unitjiangall) from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjiangall
                                            ,(select MonthBei from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS MonthBei
                                            ,(select ProjCount from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS ProjCount
                                            ,(select ProjCountbi from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS ProjCountbi
                                            ,(select Unitjiang from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjiang
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {4}))),0) AS sbn_unbak
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {5}))),0) AS xbn_unbak   
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {4}),0) AS sbn
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {5}),0) AS xbn                                        
                                            ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{2}')-1),0) AS sbn2
                                            ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{2}')-1),0) AS xbn2
                                            ,(select Stat from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Stat
                                            From tg_unit unit left join tg_unitExt ext on unit.unit_ID=ext.unit_ID
                                            Where unit_ParentID<>0 AND unit.unit_ID IN (231,233,234,236,237,238,239,281,282)
                                            Order by ext.unit_Order ", renwuid, strWhere, RenwuModel.StartDate, GuDongIdStr, SBNWhere, XBNWhere);
            if (this.drp_unit3.SelectedValue != "-1")
            {
                strSql = string.Format(@"Select unit_ID,unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCountBefore) AS AllotCount
	                                            From dbo.cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({6})) AND RenwuID={0}
                                            ),0) AS AllotCount                                   
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join cm_KaoHeRenwuMem KM on KM.memID=a.MemID left join tg_member b on a.MemID= b.mem_ID where KM.Statu=0 AND (KM.KaoHeUnitID IN ( Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}')) AND b.mem_Unit_ID=unit.unit_ID AND KM.memUnitID=unit.unit_ID AND (a.memID NOT IN ({6})) {2}),0)/6 AS AvgBumemGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID where 1=1 {2}),0)/6 AS AvgBumemGzAll 
                                            ,(select AvgGongzi from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS AvgGongzi
                                            ,(select Unitjx from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjx
                                            ,(select Unitjiangbi from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjiangbi
                                            ,(select SUM(Unitjiangall) from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjiangall
                                            ,(select MonthBei from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS MonthBei
                                            ,(select ProjCount from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS ProjCount
                                            ,(select ProjCountbi from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS ProjCountbi
                                            ,(select Unitjiang from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Unitjiang
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {4}))),0) AS sbn_unbak
                                            ,isnull((select SUM(Unitjiangall) From dbo.cm_KaoHeUnitAllotReport Where UnitID= unit.unit_ID AND (RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {5}))),0) AS xbn_unbak
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {4}),0) AS sbn
                                            ,isnull((select TOP 1 BMZJJ From cm_KaoHeUnitAllotHis A left join cm_KaoHeRenWu B on a.RenwuID=B.ID Where A.UnitID= unit.unit_ID AND {5}),0) AS xbn                                            
                                            ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{3}')-1),0) AS sbn2
                                            ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryBMJJ Where UnitID= unit.unit_ID AND AllotYear=Year('{3}')-1),0) AS xbn2
                                            ,(select Stat from dbo.cm_KaoHeUnitAllotReport where RenwuID={0} AND UnitID=unit.unit_ID) AS Stat
                                            From tg_unit unit
                                            Where unit_ParentID<>0 AND unit_ID={1}", renwuid, this.drp_unit3.SelectedValue, strWhere, RenwuModel.StartDate, SBNWhere, XBNWhere,GuDongIdStr);
            }

            //如果是已归档数据则加载已归档数据
            if (this.IsSubmit && this.IsBak)
            {
                strSql = string.Format(@"SELECT 
                                [RenwuID]
                              ,[RenwuName]
                              ,0 AS unit_ID
                              ,[UnitName] AS unit_Name
                              ,[PJYGZ] AS AvgBumemGz
                              ,[BMJX] AS Unitjx
                              ,[BMJJZB] AS Unitjiangbi
                              ,[BMZJJ] AS Unitjiangall
                              ,[YGZBS] AS MonthBei
                              ,[XMJJ] AS AllotCount
                              ,[XMJJZB] AS ProjCountbi
                              ,[BMNJJ] AS Unitjiang
                              ,[BeforeYear] AS BeforeYear
                              ,[AfterYear] AS AfterYear
                              ,[AllCount]
                              ,1 AS [Stat]
                               FROM [cm_KaoHeUnitAllotHis] Where RenwuID={0}", renwuid);
            }


            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

            UnitAllotList = dt;
        }
        /// <summary>
        /// 上半年
        /// </summary>
        protected string SBNWhere
        {
            get
            {
                //上半年考核
                if (RenwuModel.StartDate.Month < 7)
                {
                    int year = RenwuModel.StartDate.Year - 1;

                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
                }
                else
                {
                    int year = RenwuModel.StartDate.Year - 1;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
                }
            }
        }
        /// <summary>
        /// 下半年
        /// </summary>
        protected string XBNWhere
        {
            get
            {
                //上半年考核
                if (RenwuModel.StartDate.Month < 7)
                {
                    int year = RenwuModel.StartDate.Year - 1;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
                }
                else
                {
                    int year = RenwuModel.StartDate.Year;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindRenwu();
                GetResultDataTable();
            }
        }
        /// <summary>
        /// 绑定考核任务
        /// </summary>
        protected void BindRenwu()
        {
            if (RenwuList.Count > 0)
            {
                RenwuList.OrderByDescending(c => c.StartDate).ToList().ForEach(c =>
                {
                    this.drpRenwu.Items.Add(new ListItem(c.RenWuNo, c.ID.ToString()));
                });
            }
        }

        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 AND unit_ID IN (231,232,233,234,236,237,238,239)");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            if (IsSave)
            {
                this.btn_Export.Enabled = true;
            }
            else
            {
                this.btn_Export.Enabled = true;
            }

            GetResultDataTable();
        }
        protected void btn_Export_Click(object sender, EventArgs e)
        {
            GetResultDataTable();
            ExportShigong();
        }

        /// <summary>
        /// 施工
        /// </summary>
        protected void ExportShigong()
        {
            DataTable dt = this.UnitAllotList;

            string modelPath = " ~/TemplateXls/bumenjiangjin.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(this.drpRenwu.SelectedItem.Text);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["unit_Name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDecimal(dt.Rows[i]["AvgBumemGz"].ToString()).ToString("f2"));


                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Unitjx"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Math.Round(Convert.ToDecimal(dt.Rows[i]["Unitjiangall"].ToString())).ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue("0");
 
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("部门间奖金调整导出.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        protected void drpRenwu_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetResultDataTable();
        }

        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}