﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.DeptBpm
{
    public partial class StartTesting : PageBase
    {
        /// <summary>
        /// 考核任务ID
        /// </summary>
        public int RenwuId
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["id"] ?? "0", out renwuid);
                return renwuid;
            }
        }
        public string RenwuNo
        { get; set; }
        /// <summary>
        /// 任务列表
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        /// <summary>
        /// 部门列表
        /// </summary>
        public List<TG.Model.tg_unit> UnitList
        {
            get
            {
                //判断此次考核是否已经备档
                if (!CurRenwuIsBak)
                {
                    var list = new TG.BLL.tg_unit().GetModelList(" unit_ParentID<>0 ").OrderBy(c => c.unit_ID).ToList();
                    list = list.Where(c => c.unit_ID != 230).ToList();

                    var listShow = new TG.BLL.cm_DropUnitList().GetModelList(" flag=0");
                    var listExt = new TG.BLL.tg_unitExt().GetModelList("");

                    var query = from c in list
                                join s in listShow on c.unit_ID equals s.unit_ID
                                select c;

                    var unitOnelist = query.ToList();

                    var queryOne = (from c in unitOnelist
                                    join e in listExt on c.unit_ID equals e.unit_ID
                                    where e.unit_Type != 3
                                    orderby e.unit_Order ascending
                                    select c).Concat(from c in unitOnelist
                                                     join e in listExt on c.unit_ID equals e.unit_ID
                                                     where e.unit_Type == 3
                                                     orderby e.unit_Order ascending
                                                     select c);

                    return queryOne.ToList();
                }
                else
                {
                    var list = new TG.BLL.tg_unit().GetModelList(" unit_ParentID<>0 ").OrderBy(c => c.unit_ID).ToList();
                    list = list.Where(c => c.unit_ID != 230).ToList();

                    string strWhere = string.Format(" RenWuNo='{0}'", ProcessRenwuID);
                    var listHis = new TG.BLL.cm_KaoHeRenwuUnit().GetModelList(strWhere);

                    var query = from c in list
                                join s in listHis on c.unit_ID equals s.UnitID
                                select new TG.Model.tg_unit()
                                {
                                    unit_ID = c.unit_ID,
                                    unit_Name = s.UnitName,
                                    unit_ParentID = c.unit_ParentID,
                                    unit_Intro = c.unit_Intro,
                                    unit_IsEndUnit = c.unit_IsEndUnit
                                };

                    return query.ToList();
                }
            }
        }

        public List<int> SpecialUnitList
        {
            get
            {
                //List<int> list = new List<int>() { 
                //    230,
                //    242,
                //    243,
                //    244,
                //};

                //特殊部门行政
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(" unit_Type=3 ");

                var list = from u in unitlist
                           where u.unit_Type == 3
                           select u.unit_ID;
                return list.ToList<int>();
            }
        }

        public string SpecialUnitListStr
        {
            get
            {
                string str = "";

                SpecialUnitList.ForEach(u =>
                {
                    str += u.ToString() + ",";
                });

                return str;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDropDownList();
                SelectCurItem();
                GetRenwuModel(this.RenwuId);
            }
        }
        /// <summary>
        /// 本地Url
        /// </summary>
        public string LocalUrl
        {
            get
            {
                return "/DeptBpm/StartTesting.aspx?" + HttpContext.Current.Request.QueryString;
            }
        }
        /// <summary>
        /// 查询任务实体
        /// </summary>
        /// <param name="Id"></param>
        protected void GetRenwuModel(int Id)
        {
            if (Id > 0)
            {
                TG.BLL.cm_KaoHeRenWu bll = new BLL.cm_KaoHeRenWu();
                var model = bll.GetModel(Id);

                this.RenwuNo = model.RenWuNo;
            }
        }

        public int ProcessRenwuID
        {
            get
            {
                int renwuid = 0;
                string strWhere = " DeptStatus='进行中' ";
                var bll = new BLL.cm_KaoHeRenWu();
                var list = bll.GetModelList(strWhere);

                if (list.Count > 0)
                {
                    list = list.OrderByDescending(c => c.ID).ToList();
                    renwuid = list[0].ID;
                }

                //如果当前选中考核列表 2017年6月1日
                if (this.selRenwu.SelectedIndex > 0)
                {
                    renwuid = int.Parse(this.selRenwu.SelectedValue);
                }

                return renwuid;
            }
        }

        protected void selRenwu_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 绑定任务列表
        /// </summary>
        protected void BindDropDownList()
        {
            RenwuList.ForEach(c =>
            {
                selRenwu.Items.Add(new ListItem(c.RenWuNo, c.ID.ToString()));
            });
        }
        /// <summary>
        /// 选中当前考核任务
        /// </summary>
        protected void SelectCurItem()
        {
            if (this.selRenwu.Items.FindByValue(this.RenwuId.ToString()) != null)
            {
                this.selRenwu.Items.FindByValue(this.RenwuId.ToString()).Selected = true;
            }
        }
        /// <summary>
        /// 当前任务是否已经归档
        /// </summary>
        protected bool CurRenwuIsBak
        {
            get
            {
                var hisList = new TG.BLL.cm_KaoheRenwuHistory().GetModelList("");

                if (hisList.Exists(c => c.RenWuID == ProcessRenwuID))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}