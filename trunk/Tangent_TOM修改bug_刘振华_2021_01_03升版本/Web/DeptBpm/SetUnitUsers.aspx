﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SetUnitUsers.aspx.cs" Inherits="TG.Web.DeptBpm.SetUnitUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--CSS-->
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS-->
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>


    <script src="../js/DeptBpm/SetUnitUser.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>部门考核人列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>设置部门考核人</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>部门名称：<%= Unit.unit_Name.Trim() %>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">

                        <table class="table table-bordered table-hover">
                            <tr>
                                <td width="150">
                                    <b>本部门参与审核人员:</b>
                                </td>
                                <td width="120">
                                    <b><% if (IsSavedMems)
                                          { %>
                                        <div id="divTips"><span class="badge badge-success">已保存考核人员</span></div>
                                        <% }
                                          else
                                          { %>
                                        <div id="divTips"><span class="badge badge-danger">未保存考核人员</span></div>
                                        <%} %>
                                    </b>
                                </td>
                                <td width="100">
                                    <a href="#" class="btn btn-xs red" id="btnSetMems">保存参与考核人员&nbsp;<i class="fa fa-save"></i></a>
                                </td>
                                <td width="100">
                                    <a href="#PMName" class="btn btn-xs green" data-toggle="modal" id="btnSetOutmems">添加外所考核人员&nbsp;<i class="fa fa-plus"></i></a>

                                </td>
                                <td>
                                    <a href="/DeptBpm/StartTesting.aspx?id=<%=RenWuNo %>" class="btn btn-sm default">返回</a>
                                </td>
                                <td></td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                    <th style="width: 30px;">
                                        <input type="checkbox" name="name" value="0" id="chkAll" checked="checked" /></th>
                                    <th style="width: 50px;">序号</th>
                                    <th style="width: 90px;">姓名</th>
                                    <th style="width: 90px;">部门</th>
                                    <th style="width: 80px;">是否考核</th>
                                    <th style="width: 90px;">管理职位</th>
                                    <th style="width: 90px;">技术职位</th>
                                    <th style="width: 90px;">打分权重</th>
                                    <th style="width: 90px;">实际部门</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 1;
                                   if (MemsTable.Rows.Count > 0)
                                   {
                                       foreach (System.Data.DataRow dr in MemsTable.Rows)
                                       { %>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="name" value="0" class="chkitem" uname="<%= dr["mem_Name"].ToString().Trim() %>" uid="<%=dr["mem_ID"].ToString() %>" checked="checked" /></td>
                                    <td><%= index++ %></td>
                                    <td><%= dr["mem_Name"].ToString().Trim() %></td>
                                    <td><%= Unit.unit_Name.Trim() %></td>
                                    <td>
                                        <select>
                                            <% if (dr["Statu"].ToString() == "0")
                                               { %>
                                            <option value="0" selected="selected">是</option>
                                            <option value="1">否</option>
                                            <%}
                                               else
                                               { %>
                                            <option value="0">是</option>
                                            <option value="1" selected="selected">否</option>
                                            <%} %>
                                        </select></td>
                                    <td>
                                        <%if (IsSavedMems)
                                          { %>
                                        <input type="hidden" name="name" value="<%= dr["RoleMagRole"] %>" /><span id="spRoleMag"><%= dr["MagName"] %></span>
                                        <%}
                                          else
                                          { %>
                                        <input type="hidden" name="name" value="<%= dr["RoleIdMag"] %>" /><span id="spRoleMag"><%= dr["MagName"] %></span>
                                        <%} %>
                                    </td>
                                    <td>
                                        <%if (IsSavedMems)
                                          { %>
                                        <input type="hidden" name="name" value="<%= dr["RoleNormalRole"] %>" /><span id="spRoleTec"><%= dr["TecName"] %></span>
                                        <%}
                                          else
                                          { %>
                                        <input type="hidden" name="name" value="<%= dr["RoleIdTec"] %>" /><span id="spRoleTec"><%= dr["TecName"] %></span>
                                        <%} %>
                                    </td>
                                    <td><%if (IsSavedMems)
                                          { %>
                                        <input type="hidden" name="name" value="<%= dr["RoleWeights"] %>" /><span id="spWeights"><%= dr["Weights"] %></span>
                                        <%}
                                          else
                                          { %>
                                        <input type="hidden" name="name" value="<%= dr["Weights"] %>" /><span id="spWeights"><%= dr["Weights"] %></span>
                                        <%} %>
                                    </td>
                                    <td>
                                        <input type="hidden" name="name" value="<%= dr["IsOutUnitID"] %>" /><span id="spOutUnit"><%= dr["OutUnitName"] %></span>
                                    </td>
                                </tr>

                                <%}
                                   }%>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="name" value="<%= KaoHeUnitID %>" id="hidKaoHeUnitId" />
    <input type="hidden" name="name" value="<%= UnitID %>" id="hidUnitid" />
    <input type="hidden" name="name" value="<%= Unit.unit_Name.Trim() %>" id="hidUnitName" />
    <input type="hidden" name="name" value="<%= Url %>" id="hidLocalUrl" />

    <script>
        SetUsers.init();
    </script>

    <!--项目经理弹出层-->
    <div id="PMName" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择人员</h4>
        </div>
        <div class="modal-body">
            <div id="gcFzr_Dialog">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>生产部门:
                                    </td>
                                    <td>
                                        <select id="select_gcFzr_Unit" class="from-control">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="gcFzr_MemTable" class="table table-bordered" style="text-align: center">
                                <tr class="trBackColor">
                                    <td style="width: 60px;">序号
                                    </td>
                                    <td style="width: 180px;">人员名称
                                    </td>
                                    <td align="center" style="width: 60px;">操作
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="gcFzr_ForPageDiv" class="divNavigation pageDivPosition">
                            总<label id="gcfzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;第<label id="gcfzr_nowPageIndex">0</label>/<label id="gcfzr_allPageCount">0</label>页&nbsp;&nbsp;<span id="gcfzr_firstPage" style="cursor: pointer;">首页</span>&nbsp; <span id="gcfzr_prevPage" style="cursor: pointer;"><<</span>&nbsp;&nbsp;<span id="gcfzr_nextPage" style="cursor: pointer;">>></span>&nbsp; <span id="gcfzr_lastPage" style="cursor: pointer;">末页</span>&nbsp;&nbsp;跳至<input type="text" id="gcfzr_pageIndex" style="width: 20px; border-style: none none solid none;" />页&nbsp;&nbsp; <span id="gcfzr_gotoPageIndex" style="cursor: pointer;">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
