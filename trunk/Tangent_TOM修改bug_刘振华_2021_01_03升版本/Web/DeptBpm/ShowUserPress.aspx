﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ShowUserPress.aspx.cs" Inherits="TG.Web.DeptBpm.ShowUserPress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/DeptBpm/ShowUserPress.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>打分进度</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>打分进度</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>部门名称：<%= KaoHeUnit.unit_Name %>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-hover" id="tbData">
                        <thead>
                            <tr>
                                <th style="width: 50px;">序号</th>
                                <th style="width: 90px;">姓名</th>
                                <th style="width: 90px;">部门</th>
                                <th style="width: 80px;">是否考核</th>
                                <th style="width: 90px;">管理职位</th>
                                <th style="width: 90px;">技术职位</th>
                                <th style="width: 90px;">打分权重</th>
                                <th style="width: 90px;">实际部门</th>
                                <th style="width: 90px;">打分进度</th>
                                <th style="width: 90px;">是否完整</th>
                                <th style="width: 90px;">打分</th>
                                <th>成绩</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% int index = 1;
                               int iscomplete = 0;
                               if (KaoHeMems.Rows.Count > 0)
                               {
                                   foreach (System.Data.DataRow dr in KaoHeMems.Rows)
                                   { %>
                            <tr>
                                <td><%= index++ %></td>
                                <td><span class="badge badge-info"><%= dr["memName"].ToString().Trim() %></span></td>
                                <td><%= KaoHeUnit.unit_Name.Trim() %></td>
                                <td>是
                                </td>
                                <td>
                                    <span id="spRoleMag"><%= dr["MagName"] %></span>
                                </td>
                                <td>
                                    <span id="spRoleTec"><%= dr["TecName"] %></span>
                                </td>
                                <td style="text-align:right;">
                                    <span id="spWeights"><%= dr["Weights"] %></span>
                                </td>
                                <td>
                                    <span id="spOutUnit"><%= dr["OutUnitName"] %></span>
                                </td>
                                <td>
                                    <%if (dr["IsSubmit"].ToString() == "已提交")
                                      { %>
                                    <span class="badge badge-success"><%= dr["IsSubmit"] %></span>
                                    <%}
                                      else
                                      { %>
                                    <span class="badge badge-danger"><%= dr["IsSubmit"] %></span>

                                    <%} %>
                                </td>
                                <td>
                                    <% if (dr["IsComplete"].ToString() == "0")
                                       {
                                           if (dr["IsSubmit"].ToString() == "已提交")
                                           {
                                               iscomplete++;%>
                                    <span>完整</span>
                                    <%}
                                           else
                                           {%>
                                    <span>未提交</span>
                                    <%     }
                                       }
                                       else
                                       {
                                    %>
                                    <span>不完整</span>
                                    <%} %>
                                </td>
                                <td>
                                    <%if (dr["IsSubmit"].ToString() == "已提交")
                                      { %>
                                    <a href="ShowJudgeDetail.aspx?uid=<%= dr["memUnitID"].ToString() %>&ukaoheid=<%= dr["KaoHeUnitID"].ToString() %>&memid=<%=dr["memID"].ToString() %>&renwuno=<%=RenwuNo %>" class="btn default btn-xs red-stripe">查看评分</a>
                                    <%}
                                      else
                                      { %>
                                    <a href="#" class="btn default btn-xs red-stripe disabled">查看评分</a>

                                    <%} %>
                                </td>
                                <td>
                                    <a href="/DeptBpm/ShowAllJudgeCountDetails.aspx?uid=<%=KaoHeUnitSysNo %>&renwuno=<%=RenwuNo %>&id=<%=KaoHeUnitID %>&memid=<%=dr["memID"].ToString() %>" action="checked" class="btn btn-sm red" target="_blank">查看成绩</a>
                                </td>

                            </tr>

                            <%}
                               }%>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="12" align="center">
                                    <%--<% if (iscomplete == KaoHeMems.Rows.Count)
                                       { %>
                                    <a href="/DeptBpm/ShowAllJudgeCountDetails.aspx?uid=<%=KaoHeUnitSysNo %>&renwono=<%=RenwuNo %>&id=<%=KaoHeUnitID %>" class="btn btn-sm red">考核打分汇总</a>
                                    <% }else
                                       {%>
                                    <a href="javascript:alert('考核打分未全部完成！');" class="btn btn-sm red">考核打分汇总</a>

                                    <%} %>--%>
                                    <% if (IsSendMsgToMng)
                                       { %>
                                    <a href="#" class="btn btn-sm red disabled" id="btnSendToMng">发起部门内奖金经理调整</a>

                                    <%}
                                       else
                                       { %>
                                    <a href="#" class="btn btn-sm red" id="btnSendToMng">发起部门内奖金经理调整</a>
                                    <%} %>
                                    <a href="/DeptBpm/StartTesting.aspx?id=<%= RenwuNo %>" class="btn btn-sm default">返回</a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="name" value="<%= iscomplete == KaoHeMems.Rows.Count?1:0 %>" id="hidIsComplete" />

    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="<%= RenwuNo %>" id="hidRenwuno" />
    <input type="hidden" name="name" value="<%= KaoHeUnitSysNo %>" id="hidUnitID" />
    <input type="hidden" name="name" value="<%= KaoHeUnitID %>" id="hidKaoheUnitID" />

    <script>
        userPress.init();
    </script>
</asp:Content>
