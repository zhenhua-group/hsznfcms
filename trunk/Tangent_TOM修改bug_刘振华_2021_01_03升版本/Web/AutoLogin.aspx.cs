﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web
{
    public partial class AutoLogin : System.Web.UI.Page
    {
        protected string UserLogin
        {
            get
            {
                string userlogin = Request.QueryString["memlogin"];
                if (!string.IsNullOrEmpty(userlogin))
                {
                    return userlogin;
                }
                else
                {
                    return "";
                }
            }

        }
        protected string UserPassWord
        {
            get
            {
                string userpassword = Request.QueryString["mempwd"];
                if (!string.IsNullOrEmpty(userpassword))
                {
                    return userpassword;
                }
                else
                {
                    return "";
                }
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string strWhere = string.Format(" mem_Login='{0}'", UserLogin);

            //查询用户实体
            List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(strWhere);
            if (list.Count > 0)
            {
                TG.Model.tg_member usermodel = list[0];
                //比较登录
                if (UserPassWord == usermodel.mem_Password)
                {
                    //判断用户是否存在
                    if (Request.Cookies["userinfo"] == null)
                    {
                        //创建Cookie
                        HttpCookie userinfo = new HttpCookie("userinfo");
                        //设置时间一个月
                        userinfo.Expires = DateTime.Now.AddDays(30);

                        userinfo.Values.Add("memid", usermodel.mem_ID.ToString());
                        userinfo.Values.Add("memlogin", usermodel.mem_Login);
                        userinfo.Values.Add("principal", usermodel.mem_Principalship_ID.ToString());
                        //添加
                        Response.AppendCookie(userinfo);
                    }
                    else
                    {
                        //修改Cookie
                        HttpCookie userinfo = Response.Cookies["userinfo"];

                        if (userinfo != null)
                        {
                            userinfo.Values["memid"] = usermodel.mem_ID.ToString();
                            userinfo.Values["memlogin"] = usermodel.mem_Login;
                            userinfo.Values["principal"] = usermodel.mem_Principalship_ID.ToString();

                            Response.SetCookie(userinfo);
                        }
                    }

                    Response.Redirect("/mainpage/WelcomePage.aspx");
                }
                else
                {
                    Response.Redirect("login.aspx");
                }
            }
            else
            {
                Response.Redirect("login.aspx");

            }

        }
    }
}