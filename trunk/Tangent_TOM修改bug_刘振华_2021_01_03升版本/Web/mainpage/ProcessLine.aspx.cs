﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;

namespace TG.Web.mainpage
{
    public partial class ProcessLine : PageBase
    {
        TG.BLL.cm_Coperation bllCop = new TG.BLL.cm_Coperation();
        TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
        TG.BLL.cm_ContactPersionInfo bllper = new TG.BLL.cm_ContactPersionInfo();
        public List<ProjectPlanRole> ProjectPlanRoleList { get; set; }
        public ProjectDesignPlanRole ProjectDesignPlanRoleList { get; set; }
        public string Custtime { get; set; }
        public string Custdate { get; set; }
        public string Coptime { get; set; }
        public string Copdate { get; set; }
        public string Projtime { get; set; }
        public string Projdate { get; set; }
        public string Packtime { get; set; }
        public string Packdate { get; set; }
        public string Pjdtime { get; set; }
        public string Pjddate { get; set; }
        public string Acounttime { get; set; }
        public string Acountdate { get; set; }
        public string Valuetime { get; set; }
        public string Valuedate { get; set; }
        public string AcountID { get; set; }
        public string AcountTable { get; set; }
        public string CeHuaTable { get; set; }
        public string JinDuTable { get; set; }
        public string TuZhiTable { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //初始化时间2015年8月12日16:17:33
                Custtime = DateTime.Now.Date.ToString("yyyy/M/dd");
                Custdate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                Coptime = DateTime.Now.Date.ToString("yyyy/M/dd");
                Copdate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                Projtime = DateTime.Now.Date.ToString("yyyy/M/dd");
                Projdate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                Acounttime = DateTime.Now.Date.ToString("yyyy/M/dd");
                Acountdate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                Valuetime = DateTime.Now.Date.ToString("yyyy/M/dd");
                Valuedate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                Packtime = DateTime.Now.Date.ToString("yyyy/M/dd");
                Packdate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                Pjdtime = DateTime.Now.Date.ToString("yyyy/M/dd");
                Pjddate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                BindData();
                BindAcount(AcountID);
            }
        }
        /// <summary>
        /// 绑定收款信息
        /// </summary>
        /// <param name="AcountID"></param>
        private void BindAcount(string AcountID)
        {
            if (!string.IsNullOrEmpty(AcountID))
            {

                if (AcountID.Contains(','))
                {
                    //可切割

                    string[] acountArr = AcountID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < acountArr.Length; i++)
                    {
                        StringBuilder buildtitle = new StringBuilder();
                        string strsql = @"SELECT   p.ID,p.projID,p.Acount,p.FromUser,t.mem_Name,p.InAcountTime,p.Status,P.InAcountCode,p.ProcessMark
from cm_ProjectCharge p left join tg_member t on p.InAcountUser=t.mem_ID where p.projID=" + acountArr[i];
                        TG.Model.cm_Coperation CopModel = new TG.BLL.cm_Coperation().GetModel(int.Parse(acountArr[i]));
                        buildtitle.AppendFormat("<table class=\"table table-bordered\"><tr align=\"center\"><td>合同名称:</td><td colspan=\"6\">{0}</td></tr>", CopModel.cpr_Name.Trim());
                        DataTable dt = DBUtility.DbHelperSQL.Query(strsql).Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            buildtitle.Append("<tr><td>序号</td><td>入账额(万元)</td><td>汇款人</td><td>入账人</td><td>收款时间</td><td>状态</td><td>备注</td></tr>");
                            decimal allcount = 0.00M;
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                string chargeStatu = "";
                                string status = dt.Rows[j]["Status"].ToString().Trim();
                                if (status == "A")
                                {
                                    chargeStatu = "财务待确认";
                                }
                                else if (status == "C")
                                {
                                    chargeStatu = "所长待确认";
                                }
                                else if (status == "B")
                                {
                                    chargeStatu = "财务不通过";
                                }
                                else if (status == "E")
                                {
                                    chargeStatu = "完成入账";
                                }
                                else if (status == "D")
                                {
                                    chargeStatu = "所长不通过";
                                }
                                buildtitle.AppendFormat("<tr><td>" + (j + 1) + "</td><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>", dt.Rows[j]["Acount"], dt.Rows[j]["FromUser"], dt.Rows[j]["mem_Name"], Convert.ToDateTime(dt.Rows[j]["InAcountTime"]).ToString("yyyy/MM/dd"), chargeStatu, dt.Rows[j]["ProcessMark"]);
                                allcount += Convert.ToDecimal(dt.Rows[j]["Acount"]);
                            }
                            DateTime datetimeCop = Convert.ToDateTime(dt.Rows[0]["InAcountTime"]);
                            Acounttime = datetimeCop.Date.ToString("yyyy/M/dd");
                            Acountdate = datetimeCop.ToLocalTime().ToString("hh:mm");
                            buildtitle.AppendFormat("<tr><td>合计：</td><td colspan=\"6\">" + allcount + "</td>");
                        }
                        else
                        {
                            buildtitle.Append("<tr><td colspan=\"7\">没有收费信息</td></tr>");
                            Acounttime = DateTime.Now.Date.ToString("yyyy/M/dd");
                            Acountdate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                        }
                        buildtitle.Append("</table>");
                        AcountTable += buildtitle.ToString();
                    }
                }
                else
                {
                    //不可切割
                    StringBuilder buildtitle = new StringBuilder();
                    string strsql = @"SELECT   p.ID,p.projID,p.Acount,p.FromUser,t.mem_Name,p.InAcountTime,p.Status,P.InAcountCode,p.ProcessMark
from cm_ProjectCharge p left join tg_member t on p.InAcountUser=t.mem_ID where p.projID=" + AcountID;
                    TG.Model.cm_Coperation CopModel = new TG.BLL.cm_Coperation().GetModel(int.Parse(AcountID));
                    buildtitle.AppendFormat("<table class=\"table table-bordered\" style=\"font-size:12px;\"><tr align=\"center\" ><td>合同名称:</td><td colspan=\"6\">{0}</td></tr>", CopModel.cpr_Name.Trim());
                    DataTable dt = DBUtility.DbHelperSQL.Query(strsql).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        buildtitle.Append("<tr><td>序号</td><td>入账额(万元)</td><td>汇款人</td><td>入账人</td><td>收款时间</td><td>状态</td><td>备注</td></tr>");
                        decimal allcount = 0.00M;
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            string chargeStatu = "";
                            string status = dt.Rows[j]["Status"].ToString().Trim();
                            if (status == "A")
                            {
                                chargeStatu = "财务待确认";
                            }
                            else if (status == "C")
                            {
                                chargeStatu = "所长待确认";
                            }
                            else if (status == "B")
                            {
                                chargeStatu = "财务不通过";
                            }
                            else if (status == "E")
                            {
                                chargeStatu = "完成入账";
                            }
                            else if (status == "D")
                            {
                                chargeStatu = "所长不通过";
                            }
                            buildtitle.AppendFormat("<tr><td>" + (j + 1) + "</td><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>", dt.Rows[j]["Acount"], dt.Rows[j]["FromUser"], dt.Rows[j]["mem_Name"], Convert.ToDateTime(dt.Rows[j]["InAcountTime"]).ToString("yyyy/MM/dd"), chargeStatu, dt.Rows[j]["ProcessMark"]);
                            allcount += Convert.ToDecimal(dt.Rows[j]["Acount"]);
                        }

                        DateTime datetimeCop = Convert.ToDateTime(dt.Rows[0]["InAcountTime"]);
                        Acounttime = datetimeCop.Date.ToString("yyyy/M/dd");
                        Acountdate = datetimeCop.ToLocalTime().ToString("hh:mm");
                        buildtitle.AppendFormat("<tr><td>合计：</td><td colspan=\"6\">" + allcount + "</td>");
                    }
                    else
                    {
                        buildtitle.Append("<tr><td colspan=\"7\">没有收费信息</td></tr>");
                        Acounttime = DateTime.Now.Date.ToString("yyyy/M/dd");
                        Acountdate = DateTime.Now.ToLocalTime().ToString("hh:mm");
                    }
                    buildtitle.Append("</table>");
                    AcountTable += buildtitle.ToString();
                }
            }
            else
            {
                AcountTable = "<table class=\"table table-bordered\"><tr align=\"center\"><td colspan=\"7\">没有收费信息</td></tr></table>";
            }
        }

        private void BindData()
        {
            string customerID = Request.Params["Cst_Id"];
            string coperationID = Request.Params["cprid"];
            string projectID = Request.Params["pro_id"];

            if (!string.IsNullOrEmpty(customerID))
            {
                //客户为基准
                BindCustomer(customerID);
            }
            if (!string.IsNullOrEmpty(coperationID))
            {
                //以合同为基准
                BindCoperation(coperationID);
            }
            if (!string.IsNullOrEmpty(projectID))
            {
                //以项目为基准
                BindProject(projectID);
            }
        }
        /// <summary>
        /// 绑定项目信息列表(从项目条件)
        /// </summary>
        /// <param name="projectID">项目ID</param>
        private void BindProject(string projectID)
        {
            DataSet projectSet = new TG.BLL.cm_Project().GetList(" pro_ID =" + projectID);
            GridView1.DataSource = projectSet;
            GridView1.DataBind();
            TG.Model.cm_Project ProModel = new TG.BLL.cm_Project().GetModel(int.Parse(projectID));
            Projtime = Convert.ToDateTime(ProModel.InsertDate).Date.ToString("yyyy/M/dd");
            Projdate = Convert.ToDateTime(ProModel.InsertDate).ToLocalTime().ToString("hh:mm");
            if (ProModel != null)
            {
                if (ProModel.CoperationSysNo != null && ProModel.CoperationSysNo != 0)
                {
                    DataSet coperationSet = new TG.BLL.cm_Coperation().GetList("cpr_Id =" + ProModel.CoperationSysNo);
                    this.gv_rContract.DataSource = coperationSet;
                    this.gv_rContract.DataBind();
                    TG.Model.cm_Coperation CopModel = new TG.BLL.cm_Coperation().GetModel(ProModel.CoperationSysNo);
                    if (CopModel != null)
                    {
                        Coptime = Convert.ToDateTime(CopModel.RegTime).Date.ToString("yyyy/M/dd");
                        Copdate = Convert.ToDateTime(CopModel.RegTime).ToLocalTime().ToString("hh:mm");
                        BindCustomer(Convert.ToInt32(CopModel.cst_Id));
                        AcountID = CopModel.cpr_Id.ToString();
                    }
                }
                else
                {
                    string strwhere = "name='" + ProModel.ChgJia.Trim() + "' and Phone='" + ProModel.Phone.Trim() + "'";
                    TG.Model.cm_ContactPersionInfo permodel = bllper.GetModel(strwhere);
                    TG.Model.cm_CustomerInfo model = bll.GetModel(int.Parse(permodel.Cst_Id.ToString()));
                    if (model != null)
                    {
                        //客户基本信息
                        this.txtCst_No.Text = model.Cst_No == null ? "" : model.Cst_No.Trim();
                        this.txtCst_Brief.Text = model.Cst_Brief == null ? "" : model.Cst_Brief.Trim();
                        this.txtCst_Name.Text = model.Cst_Name == null ? "" : model.Cst_Name;
                        this.txtCpy_Address.Text = model.Cpy_Address == null ? "" : model.Cpy_Address.Trim();
                        this.txtCode.Text = model.Code ?? "";
                        this.txtLinkman.Text = model.Linkman == null ? "" : model.Linkman;
                        this.txtCpy_Phone.Text = model.Cpy_Phone == null ? "" : model.Cpy_Phone;
                        this.txtCpy_Fax.Text = model.Cpy_Fax == null ? "" : model.Cpy_Fax;
                        Custtime = Convert.ToDateTime(model.InsertDate).Date.ToString("yyyy/M/dd");
                        Custdate = Convert.ToDateTime(model.InsertDate).ToLocalTime().ToString("hh:mm");

                    }
                }
            }
            //绑定策划信息，不确定有没有
            BindPlanInfo(projectID);
            BindPlanStateInfo(projectID);
            BindPack(projectID);
        }
        /// <summary>
        /// 绑定策划信息
        /// </summary>
        /// <param name="projectID">项目ID</param>
        private void BindPlanInfo(string projectID)
        {
            TG.Model.cm_Project ProModel = new TG.BLL.cm_Project().GetModel(int.Parse(projectID));
            //策划人员
            ProjectPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanRoleAndUsers(int.Parse(projectID));
            //设计人员
            ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(int.Parse(projectID));
            if (ProjectDesignPlanRoleList.Users.Count != 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<table class=\"table table-bordered\" style=\"width: 98%;\" align=\"center\">");
                builder.Append("<tr align=\"center\"><td colspan=\"2\">" + ProModel.pro_name.Trim() + "</td></tr>");
                builder.Append("<tr><td width=\"20%\">角色</td><td width=\"80%\">人员和专业</td></tr>");
                builder.Append("<tr><td>项目总负责:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 1; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>助理:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 6; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>专业负责人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 2; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>校对人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 3; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>审核人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 4; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>审定人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 5; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>设计人:</td><td>");
                ProjectDesignPlanRoleList.Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("</table>");
                CeHuaTable += builder.ToString();
            }
            else
            {
                CeHuaTable += "<table class=\"table table-bordered\" style=\"width: 98%;\" align=\"center\"><tr align=\"center\"><td colspan=\"2\">" + ProModel.pro_name.Trim() + "</td></tr><tr><td colspan=\"2\">没有策划信息...</td></tr></table>";
            }
        }
        //项目进度
        private void BindPlanStateInfo(string projectID)
        {
            TG.Model.cm_Project ProModel = new TG.BLL.cm_Project().GetModel(int.Parse(projectID));
            List<ProjectPlanSubItem> subList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanSubitemList(int.Parse(projectID));
            if (subList.Count != 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<table class=\"table table-bordered\" style=\"width: 98%;\" align=\"center\">");
                builder.Append("<tr  align=\"center\"><td colspan=\"4\">" + ProModel.pro_name.Trim() + "</td></tr>");
                builder.Append("<tr><td>阶段</td><td>开始时间</td><td>结束时间</td><td>图纸张数:</td></tr>");
                Pjdtime = Convert.ToDateTime(subList[0].StartDateString).Date.ToString("yyyy/M/dd");
                Pjddate = Convert.ToDateTime(subList[0].StartDateString).ToLocalTime().ToString("hh:mm");
                foreach (ProjectPlanSubItem item in subList)
                {
                    builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", item.DesignLevel, item.StartDateString, item.EndDateString, item.SumPaper);

                }

                builder.Append("</table>");
                JinDuTable += builder.ToString();
            }
            else
            {
                JinDuTable += "<table class=\"table table-bordered\" style=\"width: 98%;\" align=\"center\"><tr align=\"center\"><td colspan=\"4\">" + ProModel.pro_name.Trim() + "</td></tr><tr><td colspan=\"4\">没有项目进度信息...</td></tr></table>";
            }
        }
        //图纸信息
        private void BindPack(string projectID)
        {
            TG.Model.cm_Project ProModel = new TG.BLL.cm_Project().GetModel(int.Parse(projectID));
            DataSet ds = TG.DBUtility.DbHelperSQL.RunProcedure("P_cm_Pageback", new SqlParameter[] { new SqlParameter("@query", ProModel.ReferenceSysNo.ToString()), new SqlParameter("@queryName", ProModel.pro_name.ToString().Trim()) }, "table");
            StringBuilder builder = new StringBuilder();
            builder.Append("<table class=\"table table-bordered\" style=\"width: 98%;\" align=\"center\">");
            builder.Append("<tr align=\"center\"><td colspan=\"6\">" + ProModel.pro_name.Trim() + "</td></tr>");

            // 循环读取结果集
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    builder.Append("<tr><td>文件数量</td><td>待提交</td><td>待打印</td><td>已打印</td><td>已删除</td><td>已归档</td></tr>");
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        builder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>", ds.Tables[0].Rows[i]["wjsl"], ds.Tables[0].Rows[i]["DTJ"], ds.Tables[0].Rows[i]["DDY"], ds.Tables[0].Rows[i]["YDY"], ds.Tables[0].Rows[i]["YSC"], ds.Tables[0].Rows[i]["YGD"]);
                    }
                }
                else
                {
                    builder.Append("<tr><td colspan=\"6\">没有图纸信息</td></tr>");
                }
                builder.Append("</table>");
                TuZhiTable += builder.ToString();
            }
            else
            {
                TuZhiTable = "<table class=\"table table-bordered\"><tr ><td colspan=\"6\">没有图纸信息...</td></tr></table>";
            }

        }
        /// <summary>
        /// 绑定合同信息列表
        /// </summary>
        /// <param name="coperationID"></param>
        private void BindCoperation(string coperationID)
        {
            DataSet coperationSet = new TG.BLL.cm_Coperation().GetList("cpr_Id=" + coperationID);

            this.gv_rContract.DataSource = coperationSet;
            this.gv_rContract.DataBind();

            TG.Model.cm_Coperation CopModel = new TG.BLL.cm_Coperation().GetModel(int.Parse(coperationID));
            AcountID = CopModel.cpr_Id.ToString();
            if (CopModel != null)
            {
                Coptime = Convert.ToDateTime(CopModel.RegTime).Date.ToString("yyyy/M/dd");
                Copdate = Convert.ToDateTime(CopModel.RegTime).ToLocalTime().ToString("hh:mm");
                BindCustomer(Convert.ToInt32(CopModel.cst_Id));
                DataSet projectSet = new TG.BLL.cm_Project().GetList(" CoperationSysNo =" + CopModel.cpr_Id);
                GridView1.DataSource = projectSet;
                GridView1.DataBind();
                if (projectSet.Tables.Count > 0)
                {
                    if (projectSet.Tables[0].Rows.Count > 0)
                    {
                        DateTime datetimeproj = Convert.ToDateTime(projectSet.Tables[0].Rows[0]["pro_startTime"]);
                        Projtime = datetimeproj.Date.ToString("yyyy/M/dd");
                        Projdate = datetimeproj.ToLocalTime().ToString("hh:mm");
                        for (int i = 0; i < projectSet.Tables[0].Rows.Count; i++)
                        {
                            BindPlanInfo(projectSet.Tables[0].Rows[i]["pro_ID"].ToString());
                            BindPlanStateInfo(projectSet.Tables[0].Rows[i]["pro_ID"].ToString());
                            BindPack(projectSet.Tables[0].Rows[i]["pro_ID"].ToString());
                        }
                    }
                }
            }



        }
        /// <summary>
        /// 绑定合同列表（从客户条件）
        /// </summary>
        /// <param name="customerID">客户id</param>
        private void BindCoperation(int customerID)
        {
            DataSet coperationSet = new TG.BLL.cm_Coperation().GetList(" cst_Id=" + customerID);
            this.gv_rContract.DataSource = coperationSet;
            this.gv_rContract.DataBind();
            string CopIDList = "";
            if (coperationSet.Tables.Count > 0)
            {
                if (coperationSet.Tables[0].Rows.Count > 0)
                {
                    DateTime datetimeproj = Convert.ToDateTime(coperationSet.Tables[0].Rows[0]["InsertDate"]);
                    Coptime = datetimeproj.Date.ToString("yyyy/M/dd");
                    Copdate = datetimeproj.ToLocalTime().ToString("hh:mm");
                    for (int i = 0; i < coperationSet.Tables[0].Rows.Count; i++)
                    {
                        CopIDList += Convert.ToInt32(coperationSet.Tables[0].Rows[i]["cpr_Id"]) + ",";
                    }
                }
            }
            if (CopIDList.Length > 0)
            {
                CopIDList = CopIDList.Substring(0, CopIDList.Length - 1);
                DataSet projectSet = new TG.BLL.cm_Project().GetList(" CoperationSysNo in(" + CopIDList + ")");
                GridView1.DataSource = projectSet;
                GridView1.DataBind();
                if (projectSet.Tables.Count > 0)
                {
                    if (projectSet.Tables[0].Rows.Count > 0)
                    {
                        DateTime datetimeproj = Convert.ToDateTime(projectSet.Tables[0].Rows[0]["pro_startTime"]);
                        Projtime = datetimeproj.Date.ToString("yyyy/M/dd");
                        Projdate = datetimeproj.ToLocalTime().ToString("hh:mm");
                        for (int i = 0; i < projectSet.Tables[0].Rows.Count; i++)
                        {
                            BindPlanInfo(projectSet.Tables[0].Rows[i]["pro_ID"].ToString());
                            BindPlanStateInfo(projectSet.Tables[0].Rows[i]["pro_ID"].ToString());
                            BindPack(projectSet.Tables[0].Rows[i]["pro_ID"].ToString());
                        }
                    }
                }
                AcountID = CopIDList;
            }
        }
        /// <summary>
        /// 绑定客户信息
        /// </summary>
        /// <param name="customerID">客户id</param>
        private void BindCustomer(string customerID)
        {
            TG.Model.cm_CustomerInfo model = bll.GetModel(int.Parse(customerID));
            if (model != null)
            {
                //客户基本信息
                this.txtCst_No.Text = model.Cst_No == null ? "" : model.Cst_No.Trim();
                this.txtCst_Brief.Text = model.Cst_Brief == null ? "" : model.Cst_Brief.Trim();
                this.txtCst_Name.Text = model.Cst_Name == null ? "" : model.Cst_Name;
                this.txtCpy_Address.Text = model.Cpy_Address == null ? "" : model.Cpy_Address.Trim();
                this.txtCode.Text = model.Code ?? "";
                this.txtLinkman.Text = model.Linkman == null ? "" : model.Linkman;
                this.txtCpy_Phone.Text = model.Cpy_Phone == null ? "" : model.Cpy_Phone;
                this.txtCpy_Fax.Text = model.Cpy_Fax == null ? "" : model.Cpy_Fax;
                Custtime = Convert.ToDateTime(model.InsertDate).Date.ToString("yyyy/M/dd");
                Custdate = Convert.ToDateTime(model.InsertDate).ToLocalTime().ToString("hh:mm");
                BindCoperation(model.Cst_Id);
            }

        }
        /// <summary>
        /// 绑定客户信息(从合同条件)
        /// </summary>
        /// <param name="customerID"></param>
        private void BindCustomer(int customerID)
        {
            TG.Model.cm_CustomerInfo model = bll.GetModel(customerID);
            if (model != null)
            {
                //客户基本信息
                this.txtCst_No.Text = model.Cst_No == null ? "" : model.Cst_No.Trim();
                this.txtCst_Brief.Text = model.Cst_Brief == null ? "" : model.Cst_Brief.Trim();
                this.txtCst_Name.Text = model.Cst_Name == null ? "" : model.Cst_Name;
                this.txtCpy_Address.Text = model.Cpy_Address == null ? "" : model.Cpy_Address.Trim();
                this.txtCode.Text = model.Code ?? "";
                this.txtLinkman.Text = model.Linkman == null ? "" : model.Linkman;
                this.txtCpy_Phone.Text = model.Cpy_Phone == null ? "" : model.Cpy_Phone;
                this.txtCpy_Fax.Text = model.Cpy_Fax == null ? "" : model.Cpy_Fax;
                Custtime = Convert.ToDateTime(model.InsertDate).Date.ToString("yyyy/M/dd");
                Custdate = Convert.ToDateTime(model.InsertDate).ToLocalTime().ToString("hh:mm");
            }

        }
        //定义总值
        decimal sumarea = 0m;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal area = Convert.ToDecimal(e.Row.Cells[4].Text);
                sumarea += area;
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[4].Text = sumarea.ToString("f4");
            }
        }
        //定义总值
        decimal sumacount = 0m;
        protected void gv_rContract_DataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal acount = Convert.ToDecimal(e.Row.Cells[3].Text);
                sumacount += acount;
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[3].Text = sumacount.ToString("f4");
            }
        }
    }
}