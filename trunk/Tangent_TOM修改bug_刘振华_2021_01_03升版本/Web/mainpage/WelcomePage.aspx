﻿<%@ Page Title="TOM生产经营管理系统" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="WelcomePage.aspx.cs" Inherits="TG.Web.mainpage.WelcomePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />

    <script src="/js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>

    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/welcome.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首 页 <small>首页信息</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="row" style="display:none;">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-briefcase"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <%=YearCustomerSum %>
                    </div>
                    <div class="desc">
                        <span id="sp_customer" runat="Server">客户总数</span>
                    </div>
                </div>
                <a class="more" href="../Customer/cst_CustomerInfoListByMaster.aspx">更多<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-book"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <%= YearCoperationSum %>
                    </div>
                    <div class="desc">
                        <span id="sp_coperation" runat="Server">合同总数</span>
                    </div>
                </div>
                <a class="more" href="../Coperation/cpr_CorperationListBymaster.aspx">更多 <i class="m-icon-swapright m-icon-white"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-table"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <%=YearProjectSum %>
                    </div>
                    <div class="desc">
                        <span id="sp_project" runat="Server">项目总数</span>
                    </div>
                </div>
                <a class="more" href="../ProjectManage/ProjectListBymaster.aspx">更多<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span style="font-size: 22pt;">
                            <%=YearChargeSum %></span>
                    </div>
                    <div class="desc">
                        <span id="sp_charge" runat="Server">部门收款额</span>
                    </div>
                </div>
                <a class="more" href="../Coperation/ProjectChargeBymaster.aspx?ParamType=homepage">更多<i class="m-icon-swapright m-icon-white"></i> </a>
            </div>
        </div>
    </div>
    <!--结束概括-->
    <div class="clearfix">
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table tabbable-full-width">
                    <tr>
                        <td style="text-align: center;">
                            <table class="table" style="width:1500px;margin:0 auto;">
                                <tr>
                                    <td colspan="9" style="text-align:left;color:#444;font-size:16pt;font-weight:600;">
                                        <span>Hello,<asp:Label Text="" runat="server" ID="mem_Name" /></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 66%;">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;">
                                                    <span><a href="/Calendar/Apply.aspx">请假</a></span>
                                                </td>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;">
                                                    <span><a href="/Calendar/Apply.aspx?applytype=travel">出差</a></span>
                                                </td>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;">
                                                    <a href="/Calendar/Apply.aspx?applytype=gomeet"><span>外勤</span><br />
                                                    <span>加班</span></a>
                                                </td>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;">
                                                    <a href="/Calendar/Apply.aspx"><span>自驾</span></a>
                                                </td>
                                                <td rowspan="2" style="width: 270px; height: 160px;text-align:left;color:#fff;">
                                                    <span>员工岗位公告</span><br />
                                                    <span>常务副总任命书</span><br />
                                                    <span>内部推荐通知</span><br />

                                                    <div style="margin-top:130px;margin-left:20px;color:#999;text-align:right;height:36px;width:270px;font-weight:600;">
                                                        <span>NOTIC</span><br />
                                                        <span>通知公告</span>
                                                    </div>
                                                </td>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;"">
                                                    <span>建成</span><br />
                                                    <span>项目</span>
                                                </td>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;"">
                                                    <span>标准化</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;">
                                                    <span>我要</span><br />
                                                    <span>订餐</span>
                                                </td>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;">
                                                    <span>取消</span><br />
                                                    <span>订餐</span>
                                                </td>
                                                <td style="width: 100px; height: 120px;">...</td>
                                                <td style="width: 100px; height: 120px;vertical-align:bottom;font-size:13px;font-weight:600;color:#999;text-align:right;">
                                                    <span>PROCEDURE</span><br />
                                                    <span>工作流程</span>
                                                </td>
                                                <td style="width: 100px; height: 120px;">
                                                    <div style="margin-top:80px;margin-left:20px;color:#999;text-align:right;height:36px;width:95px;font-weight:600;">
                                                        <span>DATA CENTER</span><br />
                                                        <span>资料库</span>
                                                    </div>
                                                </td>
                                                <td style="width: 100px; height: 120px;vertical-align:middle;font-size:16px;font-weight:600;color:#777;"">
                                                    <span>建筑</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td rowspan="2" style="width: 33%;text-align:left;">
                                        <img src="../js/assets/img/bg/bg1.jpg" width="350" height="510" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 66%;">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td style="width: 200px; height: 240px;">
                                                    <div style="margin-top:0px;margin-left:95px;color:#999;text-align:right;height:36px;width:90px;font-weight:600;">
                                                        <span>MY PROJECTS</span><br />
                                                        <span>参与项目</span>
                                                    </div>

                                                    <div style="margin-top:20px;color:#999;text-align:center;height:150px;width:200px;font-weight:900;font-size:60pt;">
                                                        <span id="item_all">0</span>
                                                    </div>
                                                </td>
                                                <td style="width: 200px; height: 240px;">
                                                    <div style="margin-top:0px;margin-left:0px;color:#999;text-align:right;height:36px;width:120px;font-weight:600;">
                                                        <span>PERSONAL NOTICE</span><br />
                                                        <span>待办事宜</span>
                                                    </div>

                                                    <div style="margin-top:20px;color:#ff0000;text-align:center;height:150px;width:200px;font-weight:900;font-size:60pt;">
                                                        <span><a href="../Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A&action=done" target="_self" id="A1"
                                    style="color: red; font-weight: bold;"><%= UserDoneMsgCount %></a></span>
                                                    </div>
                                                </td>
                                                <td style="width: 300px; height: 240px;">
                                                    <img src="../js/assets/img/bg/bg2.jpg" width="300" height="240"/>
                                                </td>
                                                <td style="width: 300px; height: 240px;text-align:left;">
                                                    <ul style="text-align:left;color:white;">
                                                        <li>Welcom To 协同办公</li>
                                                        <li>三维的那些事儿</li>
                                                        <li>2016日本院</li>
                                                        <li>关于“管线综合实践与应用”的培训</li>
                                                        <li>如何把压力变动力</li>
                                                        <li>...</li>
                                                    </ul>
                                                </td>
                                                
                                            </tr>
                                            </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <div class="row" style="display:none;">
        <div class="col-md-6">
            <!-- 第一列-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>我的信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td width="120" rowspan="4">
                                    <asp:Image onerror="javascript:this.src='../js/assets/img/avatar3.jpg'" runat="server"
                                        ID="lbl_img" Width="120" />
                                </td>
                                <td width="90" style="width: 100px;">登录名：
                                </td>
                                <td width="117">
                                    <asp:Label Text="" runat="server" ID="mem_Login" />
                                </td>
                                <td width="100">姓名：
                                </td>
                                <td width="127">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td width="90">专业：
                                </td>
                                <td>
                                    <asp:Label Text="" runat="server" ID="mem_Speciality" />
                                </td>
                                <td width="90">部门：
                                </td>
                                <td>
                                    <asp:Label Text="" runat="server" ID="mem_Unit" />
                                </td>
                            </tr>
                            <tr>
                                <td width="90">TCD角色：
                                </td>
                                <td>
                                    <asp:Label Text="" runat="server" ID="lbl_js" />
                                </td>
                                <td width="100">职称：
                                </td>
                                <td>
                                    <asp:Label Text="" runat="server" ID="lbl_zc" />
                                </td>
                            </tr>
                            <tr>
                                <td width="90">时间：
                                </td>
                                <td colspan="3">
                                    <asp:Label Text="" runat="server" ID="lbl_logintime" />
                                </td>
                            </tr>
                            <tr>
                                <td width="120" align="center"><a href="/SystemSet/SetPersinalBymaster.aspx">编辑个人信息</a>
                                </td>
                                <td width="90">消息：
                                </td>
                                <td>()条
                                </td>
                                <td width="100">待办项：
                                </td>
                                <td>(<a href="../Coperation/cpr_SysMsgListViewBymaster.aspx?action=done&flag=A" target="_self"
                                    id="sys_msg" style="color: red; font-weight: bold;"><%=UserDoneMsgCount%></a>)条
                                </td>
                            </tr>
                            <tr>
                                <td width="120" align="center">登录IP：
                                </td>
                                <td width="90" colspan="4">
                                    <asp:Label Text="" runat="server" ID="lbl_ip" />
                                </td>
                                <%-- <td width="100">产值待办项：
                                </td>
                                <td>(<a href="/Coperation/cpr_SysValueMsgListViewBymaster.aspx?action=done&flag=A" target="_self" id="person_msg" style="color: red; font-weight: bold;"><%=UserValueDoneMsgCount %></a>)条
                                </td>--%>
                            </tr>
                            <tr>
                                <td>TOM角色：</td>
                                <td colspan="4">
                                    <button class="btn popovers btn-sm blue" data-trigger="hover" data-container="body" data-placement="right" data-content="<%=UserRoleList %>" data-original-title="TOM角色">查看角色</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--个人日志-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-comments"></i>个人日志
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px; overflow: hidden; width: auto;" data-always-visible="1"
                        data-rail-visible="0">
                        <ul class="feeds">
                            <%=WorkLogList %>
                        </ul>
                    </div>
                </div>
                <div class="task-footer">
                    <span class="pull-right"><a href="../SystemSet/WorkLogListByMaster.aspx">查看更多<i class="m-icon-swapright m-icon-gray"></i></a>
                        &nbsp; </span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- 右侧列开始-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-coffee"></i>基本概况
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a><a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive" style="height: 222px;">
                        <table class="table table-bordered ">
                            <tr>
                                <td style="width: 150px;">项目总数：
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;">部门总数：
                                </td>
                                <td>
                                    <span class="label label-sm label-danger" id="item_bumen">0</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;">设计师总数：
                                </td>
                                <td>
                                    <span class="label label-sm label-danger" id="item_per">0</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;">图纸文件数：
                                </td>
                                <td>
                                    <span class="label label-sm label-danger" id="item_zhi">0</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--院内公告-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-comments"></i>系统公告
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px; overflow: hidden; width: auto;" data-always-visible="1"
                        data-rail-visible="0">
                        <ul class="feeds">
                            <%= PubMsgList %>
                        </ul>
                    </div>
                </div>
                <div class="task-footer">
                    <span class="pull-right"><a href="../SystemSet/PreviewWebBannerListBymaster.aspx">查看更多
                        <i class="m-icon-swapright m-icon-gray"></i></a>&nbsp; </span>
                </div>
            </div>

        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
    <!--公告信息-->
    <div id="ajax-modal" class="modal fade" data-width="800" aria-hidden="true"
        style="display: none; width: 800px; margin-left: -379px; margin-top: -300px;" tabindex="-1">
    </div>
    <!--隐藏域-->
    <input type="hidden" id="HiddenUserSysno" runat="server" value="" />
</asp:Content>
