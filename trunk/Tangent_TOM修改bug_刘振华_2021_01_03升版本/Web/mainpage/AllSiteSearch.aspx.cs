﻿using Geekees.Common.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
 
namespace TG.Web.mainpage
{
    public partial class AllSiteSearch : PageBase
    {
        public string asTreeviewdrpunitObjID
        {
            get
            {
                return this.proj_drp_unit.GetClientTreeObjectId();
            }
        }


        TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定客户--部门
                Bindcustunit();
                //绑定客户--年份
                Bindcustyear();
                //绑定合同--年份
                BindCopYear();
                //绑定项目年份
                BindProjYear();
                //绑定合同--部门
                SelectCurrentYear();
                //绑定树形部门项目通用
                LoadUnitTree();
                //默认选中单位
                SelectedCurUnit();
                BindProPreviewPower();
                BindcustPreviewPower();
                BindcopPreviewPower();

            }
        }

        #region 绑定项目初始权限
        public void BindProPreviewPower()
        {
            StringBuilder sb = new StringBuilder("");

            //检查权限
            GetproPreviewPowerSql(ref sb);

            this.hid_whereproj.Value = sb.ToString();
        }
        private void GetproPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        #endregion
        #region 绑定合同初始权限信息
        public void BindcopPreviewPower()
        {
            StringBuilder strWhere = new StringBuilder("");
            //合同名称

            //判断个人或全部
            GetcopPreviewPowerSql(ref strWhere);

            this.hid_wherecop.Value = strWhere.ToString();


        }
        private void GetcopPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        #endregion

        #region 绑定客户初始权限信息
        public void BindcustPreviewPower()
        {
            StringBuilder strWhere = new StringBuilder("");
            //权限
            GetCustPreviewPowerSql(ref strWhere);
            this.hid_wherecust.Value = strWhere.ToString();

        }
        /// <summary>
        /// 查看权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetCustPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND InsertUserID =" + UserSysNo);
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }
        #endregion
        //绑定年份
        protected void BindProjYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.proj_drp_year.Items.Add(list[i]);
                }
            }
        }
        //初始年份
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.proj_drp_year.Items.FindByText(curyear) != null)
            {
                this.proj_drp_year.Items.FindByText(curyear).Selected = true;
            }
            if (this.cop_drp_year.Items.FindByText(curyear) != null)
            {
                this.cop_drp_year.Items.FindByText(curyear).Selected = true;
            }
            if (this.cust_drp_year.Items.FindByText(curyear) != null)
            {
                this.cust_drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        private void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.cop_drp_unit.CheckNodes(curUnit, true);
            this.proj_drp_unit.CheckNodes(curUnit, true);
        }
        //绑定树结构的部门
        private void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.cop_drp_unit.RootNode;
                ASTreeViewNode rootp = this.proj_drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                rootp.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }
        //得到部门数据
        private DataTable GetUnit()
        {
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bll_unit.GetList(strWhere).Tables[0];
            return dt;
        }
        //设置树结构的样式。
        private void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.cop_drp_unit.Theme = macOS;
            this.proj_drp_unit.Theme = macOS;
        }
        /// <summary>
        /// 是否检查权限
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //绑定普通部门
        private void Bindcustunit()
        {
            string strWhere = "";
            //如果是部门和个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID=" + UserUnitNo + " ";
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.cust_drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.cust_drp_unit.DataTextField = "unit_Name";
            this.cust_drp_unit.DataValueField = "unit_ID";
            this.cust_drp_unit.DataBind();
        }

        private void Bindcustyear()
        {
            List<string> list = new TG.BLL.cm_CustomerInfo().GetCustomerYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.cust_drp_year.Items.Add(list[i]);
                }
            }
        }
        //绑定年份
        protected void BindCopYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.cop_drp_year.Items.Add(list[i]);
                }
            }
        }
    }
}