﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowContent.aspx.cs" Inherits="TG.Web.mainpage.ShowContent" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="renderer" content="webkit">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="MobileOptimized" content="320" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <style type="text/css">
    .modal-body {
     padding:5px 20px;
    }
    </style>
    <script type="text/javascript">
        $(function () {

            //返回
            $("#btn_back").click(function () {
                window.history.back();
            });

            if ($("#hid_type").val() == "0") {
                $(".other").attr("style","display:block");
                
            }
            var sumpeople=<%=allpeople %>;
          
            if (sumpeople<1) {
                $(".divclass1").attr("style", "display:none");
            }
            $("#divopen").click(function () {

                $("#roleUserDiv").attr("style", "display:block");
                $(".divclass1").attr("style", "display:none");
                $(".divclass2").attr("style", "display:block");
            });
            $("#divclose").click(function () {
                $("#roleUserDiv").attr("style", "display:none");
                $(".divclass2").attr("style", "display:none");
                $(".divclass1").attr("style", "display:block");
            });
          
        });
    </script>
    <title></title>
</head>
<body>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">查看公告</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="newsbt">
                            <asp:Label ID="lbl_title" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="newsly">
                            作者：<asp:Label ID="lbl_user" runat="server"></asp:Label>&nbsp;&nbsp;发布时间：
                                <asp:Label ID="lbl_time" runat="server"></asp:Label>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="msgcontent">
                            <asp:Literal ID="lbl_content" runat="server"></asp:Literal>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="other" style="display: none">
                            <div class="font13">
                                接收人员(<strong><%=allpeople%></strong>) <span class="help-block"></span>
                            </div>
                            <!--选择人员显示主Div-->
                            <div class="divclass1">
                                &nbsp;&nbsp;&nbsp; &nbsp;<a href="javascript:void(0)" id="divopen">展开↓</a>
                            </div>
                            <div style="width: 100%;" id="roleUserDiv">
                                <asp:Repeater ID="RepeaterUsers" runat="server">
                                    <ItemTemplate>
                                        <span style="margin-right: 10px;" id="userSpan" usersysno="<%#Eval("ToUser") %>">
                                            <%#Eval("UserName") %><img style="margin-left: 5px; display: none; cursor: pointer;"
                                                id="deleteUserlinkButton" src="/Images/pro_icon_03.gif" /></span>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <div class="divclass2">
                                    <a href="javascript:void(0)" id="divclose">收起↑</a>
                                </div>

                            </div>
                            <div id="attachTableResult">
                                <div class="font13">
                                    附件下载(<strong><%=allfile%></strong>) <span class="help-block"></span>
                                </div>
                                <asp:Repeater ID="RepeaterAttach" runat="server">
                                    <ItemTemplate>
                                        <span style="margin-right: 10px;">
                                            <img style='width: 16px; height: 16px;' src="<%#Eval("IconPath") %>" /><a href="/SystemSet/DownLoadFile.aspx?fileName=<%#Server.UrlEncode(Eval("FileName").ToString()) %>&filePath=<%#Server.UrlEncode(Eval("RelativePath").ToString()) %>"
                                                target="_blank"><%#Eval("FileName") %></a> </span>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </div>
                        </div>
                    </div>
                </div>

                <!--接收人员-->
                <div class="row" id="webBannerTR" style="display: none;">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>接收人员</legend>
                            <!--选择人员显示主Div-->
                        </fieldset>

                    </div>
                </div>
                <!--添加附件，公告管理特有-->
                <div class="row" id="addAttach" style="display: none;">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>附件下载</legend>
                        </fieldset>

                    </div>
                </div>



               
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">关闭</button>
    </div>

    <input type="hidden" value='<%= MsgType %>' id="hid_type" />

</body>
</html>

