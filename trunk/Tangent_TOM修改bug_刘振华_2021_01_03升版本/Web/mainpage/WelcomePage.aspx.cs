﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.mainpage
{
    public partial class WelcomePage : PageBase
    {
        //公共bll
        TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
        //人员 
        TG.BLL.tg_member bllTGmember = new TG.BLL.tg_member();
        //单位生产部门
        TG.BLL.tg_unit bllTGunit = new TG.BLL.tg_unit();
        //专业
        TG.BLL.tg_speciality bllSPeciality = new TG.BLL.tg_speciality();
        /// <summary>
        /// 检查权限
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        //登陆名
        public int memId
        {
            get
            {
                int memeid = 0;
                int.TryParse(UserInfo["memid"].ToString() ?? "0", out  memeid);
                return memeid;
            }
        }
        //登陆用户名
        public string memLogin
        {
            get
            {
                return UserInfo["memlogin"].ToString();
            }
        }
        //职位
        public string princiPal
        {
            get
            {
                return UserInfo["principal"].ToString();
            }
        }
        //待办事项
        public string UserDoneMsgCount
        {
            get;
            set;
        }
        //系统消息
        public string UserSysMsg
        {
            get;
            set;
        }
        //产值代办项
        public string UserValueDoneMsgCount
        {
            get;
            set;
        }
        /// <summary>
        /// 用户角色列表
        /// </summary>
        public string UserRoleList
        {
            get
            {
                //权限
                List<TG.Model.cm_Role> roles = new TG.BLL.cm_Role().GetRoleList(UserSysNo);
                string rolesname = "";
                if (roles != null)
                {
                    foreach (TG.Model.cm_Role role in roles)
                    {
                        rolesname += "[" + role.RoleName + "]";
                    }
                }

                if (rolesname == "")
                {
                    rolesname = "未定义角色";
                }
                return rolesname;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //绑定人员信息
            BindMemInformation();
        }
        //绑定人员信息
        protected void BindMemInformation()
        {
            TG.Model.tg_member MemModel = bllTGmember.GetModel(memId);
            if (MemModel != null)
            {
                //头像--暂无
                TG.Model.cm_EleSign es = new TG.BLL.cm_EleSign().GetModel2(memId);
                if (es != null)
                {
                    this.lbl_img.ImageUrl = es.elesign;
                }

                this.mem_Login.Text = MemModel.mem_Login;
                this.mem_Name.Text = MemModel.mem_Name;
                //部门
                TG.Model.tg_unit UnitModel = bllTGunit.GetModel(MemModel.mem_Unit_ID);
                if (UnitModel != null)
                {
                    this.mem_Unit.Text = UnitModel.unit_Name;
                }
                //专业
                TG.Model.tg_speciality SpeciModel = bllSPeciality.GetModel(MemModel.mem_Speciality_ID);
                if (SpeciModel != null)
                {
                    this.mem_Speciality.Text = SpeciModel.spe_Name;
                }
            }
            this.lbl_logintime.Text = DateTime.Now.ToLongDateString();
            string CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (CustomerIP == null)
                CustomerIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            this.lbl_ip.Text = CustomerIP;
            //代办事项
            UserDoneMsgCount = new TG.BLL.cm_SysMsg().GetSysMsgDoneCount(UserSysNo).ToString();
            //产值待办  注销 2017年4月10日
            //UserValueDoneMsgCount = new TG.BLL.cm_SysMsg().GetSysValueMsgDoneCount(UserSysNo).ToString();
            //日志
            //string strSql = " Select Count(*) From cm_WorkLog Where Status=0 AND InUser=" + UserSysNo;
            //UserWorkLog = bll_db.GetList(strSql).Tables[0].Rows[0][0].ToString();
            //未读消息
            //UserSysMsg = new TG.BLL.cm_SysMsg().GetSysMsgCount(UserSysNo).ToString();

            //角色
            TG.Model.tg_principalship tpp = new TG.BLL.tg_principalship().GetModel(MemModel.mem_Principalship_ID);
            if (tpp != null)
            {
                this.lbl_js.Text = tpp.pri_Name;
            }
            //、职称  暂无
            //TG.BLL.cm_MemArchLevelRelation bll_relation = new TG.BLL.cm_MemArchLevelRelation();
            //TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();

            //List<TG.Model.cm_MemArchLevelRelation> models_relation = bll_relation.GetModelList(" mem_ID=" + model.mem_ID);
            //if (models_relation.Count > 0)
            //{
            //    //职称
            //    TG.Model.cm_MemArchLevel model_level = bll_level.GetModel(models_relation[0].ArchLevel_ID);
            //    if (model_level != null)
            //    {
            //        this.lbl_zc.Text = model_level.Name.Trim();
            //    }

            //}
            //else
            //{
            //    this.lbl_zc.Text = "无";
            //}
            //隐藏域赋值
            this.HiddenUserSysno.Value = UserSysNo.ToString();
        }
        /// <summary>
        /// 公告列表
        /// </summary>
        protected string PubMsgList
        {
            get
            {
                string strSql = "select  wb.* from cm_WebBanner wb where wb.Status=0  order by wb.SysNo desc";
                DataTable ds_wb = bll_db.GetList(strSql).Tables[0];

                string itemlist = "";
                foreach (DataRow dr in ds_wb.Rows)
                {

                    itemlist += "<li>";
                    itemlist += "<a href=\"javascript:void(0)\" msgid=" + dr["SysNo"].ToString() + " msgtitle=" + dr["Title"].ToString() + " data-toggle=\"modal\">";
                    itemlist += "<div class=\"col1\">";
                    itemlist += "<div class=\"cont\">";
                    itemlist += "<div class=\"cont-col1\">";
                    itemlist += "<div class=\"desc\"> " + dr["Title"].ToString() + "</div>";
                    itemlist += "</div> </div></div><div class=\"col2\">";
                    itemlist += "<div class=\"date\">";
                    itemlist += "<span style=\"color:#2a6496;\">" + Convert.ToDateTime(dr["InDate"].ToString()).ToString("yyyy/MM/dd") + "</span>";
                    itemlist += "</div></div>";
                    itemlist += "</a></li>";
                }

                if (itemlist == "")
                {
                    itemlist = "<li><a>没有公告信息！</a></li>";
                }

                return itemlist;
            }
        }
        /// <summary>
        /// 工作列表
        /// </summary>
        protected string WorkLogList
        {
            get
            {
                string date = DateTime.Now.ToString("yyyy-MM-dd");
                string strSql = " Select Top 10 * From cm_WorkLog Where  (InDate BETWEEN '" + date + " 00:00:00' AND '" + date + " 23:59:59') AND Status=0 AND InUser=" + UserSysNo + " ORDER BY SysNo DESC ";
                DataTable ds_wl = bll_db.GetList(strSql).Tables[0];

                string itemlist = "";
                foreach (DataRow dr in ds_wl.Rows)
                {
                    itemlist += "<li>";
                    itemlist += "<a href=\"../SystemSet/ShowMsgViewBymaster.aspx?type=1&sysno=" + dr["SysNo"].ToString() + "\">";
                    itemlist += "<div class=\"col1\">";
                    itemlist += "<div class=\"cont\">";
                    itemlist += "<div class=\"cont-col1\">";
                    itemlist += "<div class=\"desc\"> " + dr["Title"].ToString() + "</div>";
                    itemlist += "</div> </div></div><div class=\"col2\">";
                    itemlist += "<div class=\"date\">";
                    itemlist += "<span class=\"label label-sm label-danger\">new</span>";
                    itemlist += "</div></div>";
                    itemlist += "</a></li>";
                }

                if (itemlist == "")
                {
                    itemlist = "<li><a>没有日志信息！</a></li>";
                }

                return itemlist;
            }
        }

        /// <summary>
        /// 查看权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND InsertUserID =" + UserSysNo);
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 部门客户总数
        /// </summary>
        protected string YearCustomerSum
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                //权限
                GetPreviewPowerSql(ref sb);
                //新增
                sb.AppendFormat(" AND year(InsertDate)={0} ", DateTime.Now.Year);

                if (base.RolePowerParameterEntity.PreviewPattern == 0)//个人
                {
                    this.sp_customer.InnerText = "个人当年客户总数";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)//部门
                {
                    this.sp_customer.InnerText = "部门当年客户总数";
                }
                else
                {
                    this.sp_customer.InnerText = "全院当年客户总数";
                }

                string cst_Count = new TG.BLL.cm_CustomerInfo().GetListPageProcCount(sb.ToString()).ToString();
                return cst_Count;
            }
        }
        /// <summary>
        /// 部门合同总数
        /// </summary>
        protected string YearCoperationSum
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                //新增
                sb.AppendFormat(" AND year(cpr_SignDate)={0} ", DateTime.Now.Year);

                if (base.RolePowerParameterEntity.PreviewPattern == 0)//个人
                {
                    this.sp_coperation.InnerText = "个人当年合同总数";
                    sb.AppendFormat(" AND PMUserID={0} ", UserSysNo);
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)//部门
                {
                    this.sp_coperation.InnerText = "部门当年合同总数";
                    sb.AppendFormat(" AND cpr_Unit=(Select unit_Name From tg_unit Where unit_ID={0}) ", UserUnitNo);
                }
                else
                {
                    this.sp_coperation.InnerText = "全院当年合同总数";
                }

                string cpr_Count = new TG.BLL.cm_Coperation().GetListPageProcCount(sb.ToString()).ToString();
                return cpr_Count;
            }
        }
        /// <summary>
        /// 部门项目数
        /// </summary>
        protected string YearProjectSum
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                //新增
                sb.AppendFormat(" AND year(pro_startTime)={0} ", DateTime.Now.Year);

                if (base.RolePowerParameterEntity.PreviewPattern == 0)//个人
                {
                    this.sp_project.InnerText = "个人当年项目总数";
                    sb.AppendFormat(" AND PMUserID={0} ", UserSysNo);
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)//部门
                {
                    this.sp_project.InnerText = "部门当年项目总数";
                    sb.AppendFormat(" AND Unit=(Select unit_Name From tg_unit Where unit_ID={0}) ", UserUnitNo);
                }
                else
                {
                    this.sp_project.InnerText = "全院当年项目总数";
                }

                string proj_Count = new TG.BLL.cm_Project().GetListPageProcCount(sb.ToString()).ToString();
                return proj_Count;
            }
        }
        /// <summary>
        /// 本年度收款额
        /// </summary>
        protected string YearChargeSum
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                //查询项目经理 本年度收款额
                sb.Append(" Select isnull(SUM(Acount),0.00) AS ChargeSum ");
                sb.Append(" From cm_ProjectCharge ");
                sb.AppendFormat(" Where YEAR(InAcountTime)={0} ", DateTime.Now.Year);

                if (base.RolePowerParameterEntity.PreviewPattern == 0)//个人
                {
                    this.sp_charge.InnerText = "个人项目当年收费额";
                    sb.AppendFormat(" AND projID IN (Select cpr_ID From cm_Coperation Where PMUserID={0}) ", UserSysNo);
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)//部门
                {
                    this.sp_charge.InnerText = "部门项目当年收费额";
                    sb.AppendFormat(" AND projID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=(Select unit_Name From tg_unit Where unit_ID={0})) ", UserUnitNo);
                }
                else
                {
                    this.sp_charge.InnerText = "全院本年度收费额";
                }

                string chg_Count = Convert.ToDecimal(TG.DBUtility.DbHelperSQL.GetSingle(sb.ToString())).ToString("f4");
                return chg_Count;
            }
        }
    }
}