﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;

namespace TG.Web.Interview
{
    public partial class InterviewManage : PageBase
    {

        TG.BLL.InterView _training = new TG.BLL.InterView();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();

                BindYear();


                BindGrid_Inerview();

            }
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.InterView().GetTrainingYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }




        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }


        protected void BindGrid_Inerview()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.Department.SelectedValue != "-1")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.Department.SelectedValue);
                sb.AppendFormat("and (Interview_Unit='{0}') ", keyname);
            }
            if (this.drp_year.Text != "-1")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.drp_year.Text);
                sb.AppendFormat("and (YEAR(Interview_Date)={0}) ", keyname);
            }
            if (this.Interview_Name.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.Interview_Name.Text.Trim());
                sb.AppendFormat("and (Interview_Name Like '%{0}%') ", keyname);
            }
            if (this.drp_state.SelectedIndex > 0)
            {
                sb.AppendFormat("and (Interview_results='{0}') ", this.drp_state.SelectedValue);
            }

            this.hid_where.Value = sb.ToString();
            //角色数量
            this.AspNetPager1.RecordCount = int.Parse(_training.GetRecordCount(sb.ToString()).ToString());
            //this.AspNetPager1.
            //绑定
            this.grid_Interview.DataSource = _training.GetListByPage(sb.ToString(), "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize * this.AspNetPager1.CurrentPageIndex);
            this.grid_Interview.DataBind();
            //GroupCol(this.grid_mem);

        }
        protected string GetStatusPicture()
        {
            string strNum = "123";
            return strNum.Trim();
        }

        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            //if (base.RolePowerParameterEntity.PreviewPattern == 0)
            //{
            //    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            //}
            //else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            //{
            //    strWhere = " unit_ID =" + UserUnitNo;
            //}
            //else
            //{
            strWhere = " 1=1 ";
            //  }
            //不显示的单位
            //strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            strWhere += " AND unit_ParentID<>0 ";

            this.drp_Interviewunit.DataSource = bll_unit.GetList(strWhere);
            this.drp_Interviewunit.DataTextField = "unit_Name";
            this.drp_Interviewunit.DataValueField = "unit_Name";
            this.drp_Interviewunit.DataBind();

            this.Department.DataSource = bll_unit.GetList(strWhere);
            this.Department.DataTextField = "unit_Name";
            this.Department.DataValueField = "unit_Name";
            this.Department.DataBind();


            this.drp_Interviewunitedit.DataSource = bll_unit.GetList(strWhere);
            this.drp_Interviewunitedit.DataTextField = "unit_Name";
            this.drp_Interviewunitedit.DataValueField = "unit_Name";
            this.drp_Interviewunitedit.DataBind();


        }






        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindGrid_Inerview();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindGrid_Inerview();
        }

        //生产部门改变重新绑定数据
        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            this.AspNetPager1.CurrentPageIndex = 0;
            //绑定数据
            BindGrid_Inerview();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BindGrid_Inerview();
        }
    }
}