﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AjaxPro;

namespace TG.Web.Calendar
{
    public partial class Apply : PageBase
    {       
        //考勤记录传参
        public string datastring ="";
      
        protected int applyID
        {
            get
            {
                int id = 0;
                int.TryParse(Request["applyID"], out id);
                return id;
            }
        }
        public string towork = "09:00";
        public string offwork = "17:30";
        public string tx_towork = "09:00";
        public string tx_offwork = "17:30";
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(Apply));

            datastring = Request["date"] ?? DateTime.Now.ToString("yyyy-MM-dd");

            //首页传参
            string home_applytype = Request.QueryString["applytype"] ?? "";
            if (!string.IsNullOrEmpty(home_applytype))
            {
                this.hid_applytype.Value = home_applytype;
                if (this.RadioType.Items.FindByValue(home_applytype) != null)
                {
                    this.RadioType.Items.FindByValue(home_applytype).Selected = true;
                   
                }

            }
            if (!IsPostBack)
            {
                int months = 0, years = 0,days=0;
                if (!string.IsNullOrEmpty(datastring))
                {
                    months = Convert.ToDateTime(datastring).Month;
                    years = Convert.ToDateTime(datastring).Year;
                    days=Convert.ToDateTime(datastring).Day;
                }
                else
                {
                    months = DateTime.Now.Month;
                    years = DateTime.Now.Year;
                    days=DateTime.Now.Day;
                }   

                //考勤月上下班
                if (days>15)
                {
                    if (months==12)
                    {
                        months = 1;
                        years = years + 1;
                    }
                    else
                    {
                        months = months + 1;
                    }   
                }

                List<TG.Model.cm_PersonAttendSet> list_pas = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_month=" + months + " and attend_year=" + years + " and mem_ID=" + UserSysNo);
                if (list_pas != null && list_pas.Count > 0)
                {
                    //出差显示
                    towork = list_pas[0].ToWork;
                    offwork = list_pas[0].OffWork;
                    //用于计算时间
                    tx_towork = list_pas[0].ToWork;
                    tx_offwork = list_pas[0].OffWork;
                }


                //有申请记录
                if (applyID > 0)
                {
                    TG.Model.cm_ApplyInfo apf = new TG.BLL.cm_ApplyInfo().GetModel(applyID);
                    if (apf!=null)
                    {
                        this.hid_applyID.Value = applyID.ToString();
                        this.hid_applytype.Value = apf.applytype;
                        hid_iscar.Value = apf.iscar == null ? "" : apf.iscar;

                        string applytype = apf.applytype;
                        if (this.RadioType.Items.FindByValue(applytype) != null)
                        {
                            this.RadioType.Items.FindByValue(applytype).Selected = true;
                            this.RadioType.Enabled = false;
                        }
                        if (applytype == "leave")
                        {
                            if (this.applyType.Items.FindByValue(apf.reason) != null)
                            {
                                this.applyType.Items.FindByValue(apf.reason).Selected = true;
                            }
                            starttime.Value = apf.starttime.ToString("yyyy-MM-dd HH:mm");
                            endtime.Value = apf.endtime.ToString("yyyy-MM-dd HH:mm");
                            totaltime.Value = apf.totaltime==null?"0":apf.totaltime.ToString();
                            applyContent.Value = apf.remark == null ? "" : apf.remark;
                        }
                        else if (applytype == "travel")
                        {
                            proname.Value = apf.reason == null ? "" : apf.reason;
                            address.Value = apf.address == null ? "" : apf.address;
                            totaltime2.Value = apf.totaltime == null ? "0" : apf.totaltime.ToString();
                            applyContent2.Value = apf.remark == null ? "" : apf.remark;
                            kilometre.Value = apf.kilometre == null ? "0" : apf.kilometre.ToString();
                            starttime7.Value = apf.starttime.ToString("yyyy-MM-dd HH:mm");
                            endtime7.Value = apf.endtime.ToString("yyyy-MM-dd HH:mm");
                           // datastring = (apf.starttime == null ? "" : apf.starttime.ToString("yyyy-MM-dd"));
                           // towork = apf.starttime.ToString("HH:mm");
                           // offwork= apf.endtime.ToString("HH:mm");
                        }
                        else if (applytype == "gomeet")
                        {
                            reason.Value = apf.reason == null ? "" : apf.reason;
                            address2.Value = apf.address == null ? "" : apf.address;
                            starttime2.Value = apf.starttime.ToString("yyyy-MM-dd HH:mm");
                            endtime2.Value = apf.endtime.ToString("yyyy-MM-dd HH:mm");
                           // start_sj.Value = apf.starttime.ToString("HH:mm");
                           // end_sj.Value = apf.endtime.ToString("HH:mm");
                            totaltime3.Value = apf.totaltime == null ? "0" : apf.totaltime.ToString();
                            kilometre2.Value = apf.kilometre == null ? "0" : apf.kilometre.ToString();
                        }
                        else if (applytype == "forget")
                        {
                            starttime3.Value = apf.starttime.ToString("yyyy-MM-dd HH:mm");
                            endtime3.Value = apf.endtime.ToString("yyyy-MM-dd HH:mm");
                            //start_sj2.Value = apf.starttime.ToString("HH:mm");
                            //end_sj2.Value = apf.endtime.ToString("HH:mm");
                            totaltime7.Value = apf.totaltime == null ? "0" : apf.totaltime.ToString();
                            applyContent3.Value = apf.remark == null ? "" : apf.remark;
                        }
                        else if (applytype == "company")
                        {
                            if (this.applyType4.Items.FindByValue(apf.address) != null)
                            {
                                this.applyType4.Items.FindByValue(apf.address).Selected = true;
                            }
                            starttime4.Value = apf.starttime.ToString("yyyy-MM-dd HH:mm");
                            endtime4.Value = apf.endtime.ToString("yyyy-MM-dd HH:mm");
                            totaltime4.Value = apf.totaltime == null ? "0" : apf.totaltime.ToString();
                            applyContent4.Value = apf.remark == null ? "" : apf.remark;
                            kilometre4.Value = apf.kilometre == null ? "0" : apf.kilometre.ToString();
                        }
                        else if (applytype == "depart")
                        {
                            if (this.applyType5.Items.FindByValue(apf.reason) != null)
                            {
                                this.applyType5.Items.FindByValue(apf.reason).Selected = true;
                            }
                            starttime5.Value = apf.starttime.ToString("yyyy-MM-dd HH:mm");
                            endtime5.Value = apf.endtime.ToString("yyyy-MM-dd HH:mm");
                            totaltime5.Value = apf.totaltime == null ? "0" : apf.totaltime.ToString();
                            applyContent5.Value = apf.remark == null ? "" : apf.remark;
                        }
                        else if (applytype == "addwork")
                        {
                            if (this.applyType6.Items.FindByValue(apf.reason) != null)
                            {
                                this.applyType6.Items.FindByValue(apf.reason).Selected = true;
                            }
                            starttime6.Value = apf.starttime.ToString("yyyy-MM-dd");
                            start_sj6.Value = apf.starttime.ToString("HH:mm");
                            end_sj6.Value = apf.endtime.ToString("HH:mm");
                            totaltime6.Value = apf.totaltime == null ? "0" : apf.totaltime.ToString();
                            applyContent6.Value = apf.remark == null ? "" : apf.remark;
                        }
                        
                    }
                    else
                    {
                        Response.Write("<script language='javascript'>alert('此记录已删除!');window.history.back();</script>");
                        Response.End();
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(datastring))
                    {
                        datastring = DateTime.Now.ToString("yyyy-MM-dd");
                    }

                    starttime.Value = datastring + " 09:00";
                    endtime.Value = datastring + " 17:30";

                    starttime7.Value = datastring +" "+ towork;
                    endtime7.Value = datastring + " " + offwork;
                    starttime2.Value = datastring + " " + towork;
                    endtime2.Value = datastring + " " + offwork;
                   // start_sj.Value = towork;
                   // end_sj.Value = offwork;
                    starttime3.Value = datastring + " " + towork;
                    endtime3.Value = datastring + " " + offwork;
                  //  start_sj2.Value = "09:00";
                  //  end_sj2.Value ="17:30";

                    starttime4.Value = datastring + " 09:00";
                    endtime4.Value = datastring + " 17:30";
                    starttime5.Value = datastring + " 09:00";
                    endtime5.Value = datastring + " 17:30";
                    starttime6.Value = datastring;
                    start_sj6.Value = "09:00";
                    end_sj6.Value = "17:30";                                  


                }

               
                
            }           
          
        }
        //循环日期判断是否有假期
        [AjaxMethod]
        public int IsHoliday(string start,string end)
        {
            int str = 0;
            //排除前后当天日期，取中间时间的日期
            DateTime offdate = Convert.ToDateTime(end.Substring(0,10));
            DateTime ondate = Convert.ToDateTime(Convert.ToDateTime(start).AddDays(1).ToString("yyyy-MM-dd"));
            for (; ondate.CompareTo(offdate) < 0; ondate = ondate.AddDays(1))
            {
                List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" convert(varchar(10),holiday,120)='" +ondate.ToString("yyyy-MM-dd") + "' order by id");
                //存在
                if (list != null && list.Count > 0)
                {
                    //节假日，需减1天
                    if (list[0].daytype == 1)
                    {
                        str++;
         
                    }
                }
                else
                {
                    //周六日0~6
                    string temp = Convert.ToDateTime(ondate).DayOfWeek.ToString();
                    if (temp == "Sunday" || temp == "Saturday")
                    {
                        str++;
                    }
                }
            }          

            return str;
        }

        //判断是否是假期
        [AjaxMethod]
        public new bool isJQ(DateTime date)
        {
            bool flag = false;
            List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" convert(varchar(10),holiday,120)='" + date.ToString("yyyy-MM-dd") + "' order by id");
            //存在
            if (list != null && list.Count > 0)
            {
                //节假日，需减1天
                if (list[0].daytype == 1)
                {
                    flag = true;
                }
            }
            else
            {
                //周六日0~6
                string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
                if (temp == "Sunday" || temp == "Saturday")
                {
                    flag = true;
                }
            }
            return flag;
        }
       
    }
}