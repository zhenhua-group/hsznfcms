﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Configuration;


namespace TG.Web.Calendar
{
    public partial class ApplyStatis : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
              
                //绑定权限
                BindPreviewPower();
                //在职
                StringBuilder sb = new StringBuilder(" and mem_isFired=0 ");
                //检查权限
                GetPreviewPowerSql(ref sb);

                this.hid_where.Value = sb.ToString();
            }

        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();

            string sqlwhere = "";

            if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sqlwhere = " and unit_ID IN (Select top 1 mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_ID=" + UserSysNo + " order by mr.mem_unit_ID desc)";
            }


            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 and unit_ID not in (" + NotShowUnitList + ")" + sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定          
            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
      
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            
            //个人，只有管理员看到全部
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (mem_ID =" + UserSysNo + ") ");
            }
            //部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {

                sb.Append(" AND mem_ID in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=(select top 1 mem_Unit_ID from tg_memberRole where mem_ID=" + UserSysNo + " order by mem_Unit_ID desc))");
            }
            
        }
    }

}