﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="YearSummary.aspx.cs" Inherits="TG.Web.Calendar.YearSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
       <script src="../js/echarts/build/dist/echarts.js"></script>
    <script type="text/javascript" lang="javascript">
        $(function () {
          
            var drp_year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
            var drp_month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
            var next_year = drp_year;
            //下半年
            if (drp_month == "1") {
                next_year = (parseInt(drp_year) + 1);
                $("#title").html(drp_year + ".08  -  " + next_year + ".01月");
                var $td = "";
                for (var i = 8; i <= 12; i++) {
                    $td = $td + "<td style=\"width:100px;background-color:#f5f5f5; text-align:center;font-weight:bold;\">" + i + "月</td>";
                }
                $td = $td + "<td style=\"width:100px;background-color:#f5f5f5; text-align:center;font-weight:bold;\">1月</td>";

                $("#tbl>tbody>tr:eq(0)>td:eq(2)").after($td);
            }
            else {
                $("#title").html(drp_year + ".02  -  " + next_year + ".07月");
                var $td = "";
                for (var i = 2; i <= 7; i++) {
                    $td = $td + "<td style=\"width:100px;background-color:#f5f5f5; text-align:center;font-weight:bold;\">" + i + "月</td>";
                }
                $("#tbl>tbody>tr:eq(0)>td:eq(2)").after($td);
            }
            //排名
            var hidsort = $("#ctl00_ContentPlaceHolder1_hid_sort").val();
            if (hidsort != "") {
                //按部门分组
                var hidsortlist = hidsort.split("|");
                var index = 1;
                //最后有一个空值
                for (var j = 0; j < (hidsortlist.length - 1) ; j++) {
                   //每个部门排名
                    var sortlist = hidsortlist[j].split(",");   
                    //去重复
                    var new_arr = uniquemeth(sortlist);  

                    for (var i = 0; i < (sortlist.length) ; i++) {
                        //从小到大排序
                        var newlist=new_arr.sort(function(a,b){return b - a; });
                        //得到索引加1，从0开始。
                        var count=($.inArray(sortlist[i],newlist))+1;

                        $("#tbl>tbody>tr:eq(" + (index) + ")>td:last").text(count);
                        index++;
                    }
                }
            }
          
            //选择单个部门显示
            if ($("#ctl00_ContentPlaceHolder1_drp_unit").val()!="-1") {
                $("#main").css("display","");
                //查询后的图
                var clmdata = <%= xAxis %>;
                var seriesdata = [<%= SeriesData %>];
                var legenddata = <%= LegendData %>;
                var yAxisData = [<%= yAxis %>];            

                loadChartsData(clmdata, seriesdata,legenddata,yAxisData);
            }
            else
            {
                $("#main").css("display","none");
            }

            //显示加载中
            $("#ctl00_ContentPlaceHolder1_btn_search").click(function () {
                $('body').modalmanager('loading');
            });
            //默认隐藏
            $("#ctl00_ContentPlaceHolder1_btn_export").hide();
            $("#ctl00_ContentPlaceHolder1_btn_allexport").hide();
            //权限控制
            if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "1") {
                //全部
                $("#ctl00_ContentPlaceHolder1_btn_export").show();
                $("#ctl00_ContentPlaceHolder1_btn_allexport").show();
            }
            else if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "2") {
                //部门
                $("#ctl00_ContentPlaceHolder1_btn_export").show();
            }
            //部门导出
            $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {
                //  $("body").modalmanager('loading');
                //导出放在一个隐藏的iframe中，这样不影响当前页面上脚本的运行，才可以隐藏遮罩层
                // querySession();
            });
            //全部导出
            $("#ctl00_ContentPlaceHolder1_btn_allexport").click(function () {
                $("body").modalmanager('loading');
                //导出放在一个隐藏的iframe中，这样不影响当前页面上脚本的运行，才可以隐藏遮罩层
                setTimeout("querySession()", 10000);
            });
        });
        function querySession() {
            var retValue = $.ajax({ url: '/HttpHandler/Calendar/CalendarHandler.ashx?action=GetDownloadState&Random=' + Math.floor(Math.random() * (1000000 + 1)), type: 'GET', async: false, cache: false }).responseText;
            //返回0代表Excel还没有准备完成，否则返回1，准备完成删除sesson,隐藏遮罩
            if (retValue * 1 == 0) {
                //一秒执行一次
                setTimeout("querySession()", 5000);
            }
            else {
                //隐藏遮罩层
                $("body").modalmanager('removeLoading');
            }
        }
        //去除重复
        function uniquemeth(arr) {
            var new_arr=[];
            for(var i=0;i<arr.length;i++) {
                var items=arr[i];
                //判断元素是否存在于new_arr中，如果不存在则插入到new_arr的最后
                if($.inArray(items,new_arr)==-1) {
                    new_arr.push(items);
                }
            }
            return new_arr;
        }
       
        var loadChartsData = function (clmdata, seriesdata, legenddata, yAxisData) {

            //配置路径
            require.config({
                paths: {
                    echarts: '../js/echarts/build/dist'
                }
            });
            //加载
            require(
                ['echarts',
                    'echarts/chart/line',
                'echarts/chart/bar'],
                function (ec) {
                    var mycharts = ec.init(document.getElementById('main'));

                    var option = {
                        title: {
                            text: "",
                            //  subtext: "单位：（小时）"
                        },
                        tooltip: {
                            show: true
                        },
                        legend: {
                            data: legenddata,
                            x: 'center'
                        },
                        toolbox: {
                            show: true,
                           // orient: 'vertical',
                           // x: 'right',
                          //  y: 'bottom',
                            feature: {
                               // mark: { show: true },
                                //dataView : {show: true, readOnly: false},
                             //   magicType: { show: true, type: ['line', 'bar'] },
                             //   restore: { show: true },
                                saveAsImage: { show: true }
                            }
                        },
                        calculable: true,
                        xAxis: [
                            {
                                'type': 'category',
                                'axisLabel': { 'interval': 0 },
                                'data': clmdata
                            }
                        ],
                        yAxis: yAxisData,
                        series: seriesdata
                    };

                    mycharts.setOption(option);
                }
            );
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>考勤年汇总</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">考勤汇总分析</a><i class="fa fa-angle-right"> </i><a href="#">考勤年汇总</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询条件
                    </div>
                       <div class="actions">
                        <asp:Button CssClass="btn btn-sm red " ID="btn_export" runat="Server" Text="部门导出" OnClick="btn_export_Click"></asp:Button>&nbsp;<asp:Button CssClass="btn btn-sm red " ID="btn_allexport" runat="server" OnClick="btn_allexport_Click" Text="全部导出"></asp:Button>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                            <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                  <%--  <asp:ListItem Value="-1">---全部---</asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
                            <td style="width: 50px;">月份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="0">上半年</asp:ListItem>
                                    <asp:ListItem Value="1">下半年</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td>
                                <asp:Button CssClass="btn blue" Text="查询" ID="btn_search" OnClick="btn_search_Click" runat="server" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body form">
                    <table border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto;">
                        <tr>
                            <td id="title" style="text-align: center; font-size: 14px; font-weight: bold; line-height: 35px;"></td>
                        </tr>
                    </table>
                    <table id="tbl" class="table table-bordered table-responsive table-hover" style="width: 100%; margin: 0 auto; margin-bottom: 0px;">
                        <tr style="background-color: #f5f5f5; text-align: center; font-weight: bold;">
                            <td style="width: 100px;">部门名称</td>
                            <td style="width: 60px;">序号</td>
                            <td style="width: 100px;">姓名</td>
                            <td style="width: 120px;">合计(小时)</td>
                            <td style="width: 150px;">平均月加班(小时)</td>
                            <td style="width: 150px;">平均周加班(小时)</td>
                            <td style="width: 60px;">排名</td>
                        </tr>
                        <asp:Literal ID="lithtml" runat="server"></asp:Literal>
                    </table>
                     <br />
                     <div id="main" style="height: 350px;width: 99%; margin: 0 auto;">                      
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField runat="server" ID="hid_sort" Value="" />

</asp:Content>
