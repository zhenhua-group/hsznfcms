﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;

namespace TG.Web.Calendar
{
    public partial class TravelCarStatis : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                BindData();
            }
        }
        //绑定数据
        public void BindData()
        {
            DataTable dt = GetData();
            //生成table
            CreateTable(dt);
            CreateTable2(dt);
        }
        //获取数据
        public DataTable GetData()
        {
            StringBuilder sb = new StringBuilder("");
            //检查权限
            GetPreviewPowerSql(ref sb);
            //部门
            if (this.drp_unit.SelectedIndex > 0)
            {
                sb.Append(" AND adduser in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=" + this.drp_unit.SelectedValue + ")");
            }

            //区域
            if (this.drp_qy.SelectedIndex > 0)
            {
                if (this.drp_qy.SelectedValue == "西安")
                {
                    sb.Append(" AND adduser in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=282)");
                }
                else
                {
                    sb.Append(" AND adduser in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID<>282)");
                }
            }
            string drpyear = this.drp_year.SelectedValue;
            string drpmonth = this.drp_month.SelectedValue;
            drpmonth = drpmonth.PadLeft(2, '0');
            int days = DateTime.DaysInMonth(Convert.ToInt32(drpyear), Convert.ToInt32(drpmonth));

            DateTime starttime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-01 00:00:00");
            DateTime endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-" + days + " 23:59:59");
            //sb.Append(" and starttime between '" + starttime + "' and '" + endtime + "' and (applytype='travel' or applytype='gomeet') order by adduser asc,reason asc,starttime asc");
            sb.Append(" and year(starttime)=" + drpyear + " and month(starttime)=" + drpmonth + " and (applytype='travel' or applytype='gomeet' or applytype='company') order by (select unit_order from tg_unitExt where unit_ID=(select mem_Unit_ID from tg_member where mem_ID=adduser)) asc,(select mem_Order from tg_member where mem_ID=adduser) asc,reason asc,starttime asc");

            //打卡数据
            // DataTable dt = TG.DBUtility.DbHelperSQL.Query("select a.*,m.mem_Name as AddUserName,m.mem_Order, from cm_ApplyInfo a inner join tg_member m on a.adduser=m.mem_ID where 1=1 ").Tables[0];
            DataTable dt = new TG.BLL.cm_ApplyInfo().GetApplyList(sb.ToString()).Tables[0];
            return dt;
        }
        //私车公用
        public void CreateTable(DataTable datatable)
        {
            DataTable dt = new DataView(datatable) { RowFilter = "iscar='s' and kilometre>=35" }.ToTable();
            StringBuilder sb = new StringBuilder();
            if (dt != null && dt.Rows.Count > 0)
            {
                int row = 1;
                string proname = "";
                int userid = 0;
                int cs = 1;
                foreach (DataRow item in dt.Rows)
                {
                    decimal gl = Convert.ToDecimal(item["kilometre"]);
                    decimal dj = 0, xj = 0;
                    //费用
                    if (gl >= 35 && gl < 120)
                    {
                        dj = decimal.Parse("1.5");
                    }
                    else if (gl >= 120)
                    {
                        dj = decimal.Parse("1.8");
                    }
                    //小计
                    xj = gl * dj;

                    sb.Append("<tr>");
                    if (userid != Convert.ToInt32(item["adduser"]))
                    {
                        cs = 1;
                        int cls = dt.Select("adduser=" + item["adduser"]).Count();
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (cls) + "'>" + row + "</td>");
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (cls) + "'>" + item["AddUserName"] + "</td>");
                        row++;
                    }
                    // sb.Append("<td align=\"center\">" + cs + "</td>");
                    sb.Append("<td align=\"center\">" + item["kilometre"] + "</td>");
                    sb.Append("<td align=\"center\">" + Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd") + "</td>");
                    sb.Append("<td align=\"center\">￥ " + dj.ToString("f2") + "</td>");
                    sb.Append("<td align=\"center\">￥ " + xj.ToString("f2") + "</td>");
                    //合计
                    if (userid == Convert.ToInt32(item["adduser"]))
                    {
                        if (proname != item["reason"].ToString())
                        {
                            //var ase = dt.AsEnumerable().Where(s => s.Field<string>("reason") == item["reason"].ToString() && s.Field<int>("adduser") == userid);
                            DataRow[] drlist = dt.Select("reason='" + item["reason"].ToString() + "' and adduser=" + userid + "");
                            decimal sum = 0;
                            for (int i = 0; i < drlist.Count(); i++)
                            {

                                decimal dr_gl = Convert.ToDecimal(drlist[i]["kilometre"]);
                                //每条记录公里不同，费用也不同
                                if (dr_gl >= 35 && dr_gl < 120)
                                {
                                    sum = sum + (dr_gl * decimal.Parse("1.5"));
                                }
                                else if (dr_gl >= 120)
                                {
                                    sum = sum + (dr_gl * decimal.Parse("1.8"));

                                }
                            }

                            //同一人同一项目合计
                            proname = item["reason"].ToString();
                            sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (drlist.Count()) + "'>￥ " + sum.ToString("f2") + "</td>");
                            sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (drlist.Count()) + "'>" + item["reason"] + "</td>");

                        }

                    }
                    else
                    {
                        //下一个人的合并信息
                        decimal total = 0, sum = 0;
                        int cls = 0;
                        DataRow[] drlist = dt.Select("adduser=" + item["adduser"]);
                        for (int i = 0; i < drlist.Count(); i++)
                        {
                            decimal dr_dj = 0;
                            decimal dr_gl = Convert.ToDecimal(drlist[i]["kilometre"]);
                            //每条记录公里不同，费用也不同
                            if (dr_gl >= 35 && dr_gl < 120)
                            {
                                dr_dj = decimal.Parse("1.5");
                            }
                            else if (dr_gl >= 120)
                            {
                                dr_dj = decimal.Parse("1.8");
                            }
                            //相同项目
                            if (drlist[i]["reason"].ToString() == item["reason"].ToString())
                            {
                                sum = sum + (dr_gl * dr_dj);
                                cls++;
                            }

                            total = total + (dr_gl * dr_dj);
                        }
                        //同一人同一项目合计
                        proname = item["reason"].ToString();
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (cls) + "'>￥ " + sum.ToString("f2") + "</td>");
                        //同一人全部项目合计
                        userid = Convert.ToInt32(item["adduser"]);
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (drlist.Count()) + "'>￥ " + total.ToString("f2") + "</td>");
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (cls) + "'>" + item["reason"] + "</td>");
                    }


                    sb.Append("<td align=\"center\"></td>");
                    sb.Append("</tr>");
                    //条数
                    cs++;
                }
            }
            lithtml.Text = sb.ToString();
        }
        //差旅补贴
        public void CreateTable2(DataTable datatable)
        {
            StringBuilder sb = new StringBuilder();

            DataTable dt = new DataView(datatable) { RowFilter = "applytype='travel'" }.ToTable();

            if (dt != null && dt.Rows.Count > 0)
            {

                int row = 1;
                string proname = "";
                int userid = 0;
              
                int relationid = 0;
                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    DataRow item = dt.Rows[k];
                    //获取当前人每天只有一条数据集合
                    List<DataRow> drlist = dt.AsEnumerable().Where(t => t.Field<int>("adduser") == Convert.ToInt32(item["adduser"])).OrderBy(t => t.Field<DateTime>("starttime")).GroupBy(t => t.Field<DateTime>("starttime").ToString("yyyy-MM-dd")).Select(r => r.Last()).ToList();

                    // if (userid == Convert.ToInt32(item["adduser"]))
                    //   {
                    //年月日相同，时间不相同,需排除不用显示
                    int gs = drlist.Where(d => d.Field<DateTime>("starttime").ToString("yyyy-MM-dd") == Convert.ToDateTime(item["starttime"]).ToString("yyyy-MM-dd") && d.Field<DateTime>("starttime") != Convert.ToDateTime(item["starttime"])).Count();
                    if (gs > 0)
                    {
                        continue;
                    }

                    //  }


                    decimal fy = 30;
                    string sql = "select rm.RoleName as MagName,rt.RoleName as TecName  from tg_memberRole m left join cm_RoleMag rm on m.RoleIdMag=rm.ID left join cm_RoleMag rt on m.RoleIdTec=rt.ID where mem_Id=" + item["adduser"];
                    DataTable role_dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
                    if (role_dt != null && role_dt.Rows.Count > 0)
                    {
                        if (role_dt.Rows[0][0].ToString().Contains("经理"))
                        {
                            fy = decimal.Parse("50");
                        }
                        else if (role_dt.Rows[0][1].ToString().Contains("设计总监"))
                        {
                            fy = decimal.Parse("40");
                        }
                    }

                    //合计
                    if (userid == Convert.ToInt32(item["adduser"]))
                    {
                        var prouser = dt.AsEnumerable().Where(s => s.Field<string>("reason") == item["reason"].ToString() && s.Field<int>("adduser") == userid).GroupBy(t => t.Field<DateTime>("starttime").ToString("yyyy-MM-dd")).Select(r => r.Last());
                        int lxrqcount = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false).Count();
                        //获取当前人连续日期的RelationID不同的个数
                        int relacount = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false).GroupBy(s => s.Field<int>("RelationID")).Count();
                        //不同项目,不能有连续日期
                        if (proname != item["reason"].ToString())
                        {
                          
                            if (prouser.Count() > 0)
                            {  
                                sb.Append("<tr>");
                                int ase = prouser.Count();
                                //同一人不同项目合计
                                proname = item["reason"].ToString();
                                decimal userfy = (ase * fy);

                                //连续日期
                                int rqcount = 1;
                                string datestr = Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd");
                                if (item["RelationID"] != DBNull.Value)
                                {
                                    if (item["RelationID"] != null&&relationid != Convert.ToInt32(item["RelationID"]))
                                    {
                                        List<DataRow> rq_drlist = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false && s.Field<int>("RelationID") == Convert.ToInt32(item["RelationID"])).OrderBy(t => t.Field<int>("ID")).ToList<DataRow>();
                                        if (rq_drlist != null && rq_drlist.Count() > 0)
                                        {
                                            rqcount = rq_drlist.Count();
                                            datestr = Convert.ToDateTime(rq_drlist[0]["starttime"]).ToString("yyyy.MM.dd") + " - " + Convert.ToDateTime(rq_drlist[(rq_drlist.Count() - 1)]["starttime"]).ToString("yyyy.MM.dd");
                                           // fy = fy * rqcount;
                                        }                                       
                                        sb.Append("<td align=\"center\">" + rqcount + "</td>");
                                        sb.Append("<td align=\"center\">" + datestr + "</td>");
                                        //费用
                                        sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                                        //小计
                                        sb.Append("<td align=\"center\">￥ " + (fy * rqcount).ToString("f2") + "</td>");
                                       
                                        //赋值
                                        relationid = Convert.ToInt32(item["RelationID"]);

                                       
                                    }
                                }
                                else
                                {                                  
                                    sb.Append("<td align=\"center\">" + rqcount + "</td>");
                                    sb.Append("<td align=\"center\">" + datestr + "</td>");
                                    sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                                    sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                                }
                                //排除连续日期条数

                                ase = ase - (lxrqcount - relacount);
                               

                                sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (ase) + "'>￥ " + userfy.ToString("f2") + "</td>");
                                sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (ase) + "'>" + proname + "</td>");
                                
                                sb.Append("<td align=\"center\"></td>");
                                sb.Append("</tr>");
                           
                            }

                           
                        }
                        else
                        {
                            //同一个人，同项目，日期分 连续和不连续
                            if (prouser.Count() > 0)
                            {
                                //连续日期
                                int rqcount = 1;
                                string datestr = Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd");
                                if (item["RelationID"] != DBNull.Value)
                                {
                                    if (item["RelationID"] != null && relationid != Convert.ToInt32(item["RelationID"]))
                                    {

                                        List<DataRow> rq_drlist = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false && s.Field<int>("RelationID") == Convert.ToInt32(item["RelationID"])).OrderBy(t => t.Field<int>("ID")).ToList<DataRow>();
                                        if (rq_drlist != null && rq_drlist.Count() > 0)
                                        {
                                            rqcount = rq_drlist.Count();
                                            datestr = Convert.ToDateTime(rq_drlist[0]["starttime"]).ToString("yyyy.MM.dd") + " - " + Convert.ToDateTime(rq_drlist[(rq_drlist.Count() - 1)]["starttime"]).ToString("yyyy.MM.dd");
                                           // fy = fy * rqcount;
                                        }
                                        sb.Append("<tr>");
                                        sb.Append("<td align=\"center\">" + rqcount + "</td>");
                                        sb.Append("<td align=\"center\">" + datestr + "</td>");
                                        //费用
                                        sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                                       //小计
                                        sb.Append("<td align=\"center\">￥ " + (fy * rqcount).ToString("f2") + "</td>");
                                        //赋值
                                        relationid = Convert.ToInt32(item["RelationID"]);

                                        sb.Append("<td align=\"center\"></td>");
                                        sb.Append("</tr>");
                                    }
                                }
                                else
                                {
                                    sb.Append("<tr>");
                                    sb.Append("<td align=\"center\">" + rqcount + "</td>");
                                    sb.Append("<td align=\"center\">" + datestr + "</td>");
                                    sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                                    sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                                    sb.Append("<td align=\"center\"></td>");
                                    sb.Append("</tr>");
                                }

                                
                            }
                        }

                    }
                    else
                    {
                        //不同人
                        sb.Append("<tr>");
                        //获取当前人连续日期的条数
                        int lxrqcount = drlist.Where(s => Convert.IsDBNull(s["RelationID"]) == false).Count();
                        //获取当前人连续日期的RelationID不同的个数
                        int relacount = drlist.Where(s => Convert.IsDBNull(s["RelationID"]) == false).GroupBy(s => s.Field<int>("RelationID")).Count();
                        //获取当前人总条数
                        int sumcls = drlist.Count();
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (sumcls - (lxrqcount - relacount)) + "'>" + row + "</td>");
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (sumcls - (lxrqcount - relacount)) + "'>" + item["AddUserName"] + "</td>");
                        row++;

                        //连续日期
                        int rqcount = 1;
                        string datestr = Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd");
                        if (item["RelationID"] != DBNull.Value)
                        {
                            if (item["RelationID"] != null && relationid != Convert.ToInt32(item["RelationID"]))
                            {
                                List<DataRow> rq_drlist = drlist.Where(s => Convert.IsDBNull(s["RelationID"])==false && s.Field<string>("reason") == item["reason"].ToString() && s.Field<int>("RelationID") == Convert.ToInt32(item["RelationID"])).OrderBy(t => t.Field<int>("ID")).ToList<DataRow>();
                               
                                if (rq_drlist != null && rq_drlist.Count() > 0)
                                {
                                    rqcount = rq_drlist.Count();
                                    datestr = Convert.ToDateTime(rq_drlist[0]["starttime"]).ToString("yyyy.MM.dd") + " - " + Convert.ToDateTime(rq_drlist[(rq_drlist.Count() - 1)]["starttime"]).ToString("yyyy.MM.dd");
                                    
                                }

                                sb.Append("<td align=\"center\">" + rqcount + "</td>");

                                sb.Append("<td align=\"center\">" + datestr + "</td>");
                                //费用
                                sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                                //小计
                                sb.Append("<td align=\"center\">￥ " + (fy * rqcount).ToString("f2") + "</td>");
                                //赋值
                                relationid = Convert.ToInt32(item["RelationID"]);
                            }
                        }
                        else
                        {
                            sb.Append("<td align=\"center\">" + rqcount + "</td>");
                            sb.Append("<td align=\"center\">" + datestr + "</td>");
                            //费用
                            sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                            //小计
                            sb.Append("<td align=\"center\">￥ " + fy.ToString("f2") + "</td>");
                            //赋值
                        }


                        decimal total = 0, sum = 0;
                        int cls = 0;
                        //DataRow[] drlist = dt.Select("adduser=" + item["adduser"]);
                        for (int i = 0; i < drlist.Count(); i++)
                        {
                            if (drlist[i]["reason"].ToString() == item["reason"].ToString())
                            {
                                sum = sum + fy;
                                cls++;
                            }

                            total = total + fy;
                        }

                        //排除连续日期条数
                        if (rqcount > 1)
                        {
                            cls = cls - (rqcount - 1);
                        }

                        //同一人同一项目合计
                        proname = item["reason"].ToString();
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (cls) + "'>￥ " + sum.ToString("f2") + "</td>");
                        //同一人全部项目合计
                        userid = Convert.ToInt32(item["adduser"]);
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (sumcls - (lxrqcount - relacount)) + "'>￥ " + total.ToString("f2") + "</td>");
                        sb.Append("<td align=\"center\" style='vertical-align:middle;' rowspan='" + (cls) + "'>" + item["reason"] + "</td>");
                        sb.Append("<td align=\"center\"></td>");
                        sb.Append("</tr>");
                    }
                  
                   
                }
            }
            lithtml2.Text = sb.ToString();
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {
            BindData();
        }

        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();

            var bll_unitExt = new TG.BLL.tg_unitExt();
            string sqlwhere = "";

            if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sqlwhere = " and unit_ID IN (Select top 1 mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_ID=" + UserSysNo + " order by mr.mem_unit_ID desc)";
            }

            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 and unit_ID not in (" + NotShowUnitList + ")" + sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定          
            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }

            for (int i = 1; i <= 12; i++)
            {
                this.drp_month.Items.Add(i.ToString());
            }

        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人，只有管理员看到全部
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (adduser =" + UserSysNo + ") ");
            }
            //部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND adduser in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=(select top 1 mem_Unit_ID from tg_memberRole where mem_ID=" + UserSysNo + " order by mem_Unit_ID desc))");
            }
        }

        protected void btn_export_Click(object sender, EventArgs e)
        {
            ExportDataToExcel("私车公用补贴");
        }

        protected void btn_export2_Click(object sender, EventArgs e)
        {
            ExportDataToExcel("差旅补贴");
        }
        private void ExportDataToExcel(string title)
        {

            HSSFWorkbook wb = null;
            string modelPath = "~/TemplateXls/TravelCarStatis.xls";
            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            string pathname = this.drp_year.SelectedValue + "年" + this.drp_month.SelectedValue.PadLeft(2, '0') + "月" + title;
           if (this.drp_unit.SelectedIndex>0)
           {
               pathname = this.drp_unit.SelectedItem.Text + pathname;
           }
            IRow dataRowTitle = ws.GetRow(0);
            dataRowTitle.GetCell(0).SetCellValue(pathname);

            int index = 2;
            DataTable dt = new DataTable();
            DataTable datatable = GetData();
            if (title == "私车公用补贴")
            {
                int row = 1;
                string proname = "";
                int userid = 0;
                int cs = 1;
                dt = new DataView(datatable) { RowFilter = "iscar='s' and kilometre>=35" }.ToTable();
                //创建所有行
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow item = dt.Rows[i];

                    var dataRow = ws.GetRow(i + index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(i + index);//生成行
                }
                //插入值
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow item = dt.Rows[i];

                    var dataRow = ws.GetRow(i + index);//读行
                    //  if (dataRow == null)
                    //      dataRow = ws.CreateRow(i + index);//生成行


                    decimal gl = Convert.ToDecimal(item["kilometre"]);
                    decimal dj = 0, xj = 0;
                    //费用
                    if (gl >= 35 && gl < 120)
                    {
                        dj = decimal.Parse("1.5");
                    }
                    else if (gl >= 120)
                    {
                        dj = decimal.Parse("1.8");
                    }
                    //小计
                    xj = gl * dj;


                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;

                    var cell2 = dataRow.CreateCell(1);
                    cell2.CellStyle = style2;

                    if (userid != Convert.ToInt32(item["adduser"]))
                    {
                        cs = 1;
                        int cls = dt.Select("adduser=" + item["adduser"]).Count();

                        ws.AddMergedRegion(new CellRangeAddress((i + index), ((cls - 1) + index + i), 0, 0));
                        cell.SetCellValue((row));

                        ws.AddMergedRegion(new CellRangeAddress((i + index), ((cls - 1) + index + i), 1, 1));
                        cell2.SetCellValue(item["AddUserName"].ToString());

                        row++;

                    }

                    //cell = dataRow.CreateCell(2);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(cs.ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(item["kilometre"].ToString());

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd"));


                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;

                    cell.SetCellValue("￥ " + dj.ToString("f2"));


                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue("￥ " + xj.ToString("f2"));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;

                    ICell cell7 = dataRow.CreateCell(7);
                    cell7.CellStyle = style2;

                    ICell cell8 = dataRow.CreateCell(8);
                    cell8.CellStyle = style2;



                    //合计
                    if (userid == Convert.ToInt32(item["adduser"]))
                    {
                        if (proname != item["reason"].ToString())
                        {
                            //  var ase = dt.AsEnumerable().Where(s => s.Field<string>("reason") == item["reason"].ToString() && s.Field<int>("adduser") == userid);
                            //  decimal sum = ase.Sum(s => s.Field<decimal>("kilometre") * dj);


                            DataRow[] drlist = dt.Select("reason='" + item["reason"].ToString() + "' and adduser=" + userid + "");
                            decimal sum = 0;
                            for (int j = 0; j < drlist.Count(); j++)
                            {

                                decimal dr_gl = Convert.ToDecimal(drlist[j]["kilometre"]);
                                //每条记录公里不同，费用也不同
                                if (dr_gl >= 35 && dr_gl < 120)
                                {
                                    sum = sum + (dr_gl * decimal.Parse("1.5"));
                                }
                                else if (dr_gl >= 120)
                                {
                                    sum = sum + (dr_gl * decimal.Parse("1.8"));

                                }
                            }


                            //同一人同一项目合计
                            proname = item["reason"].ToString();
                            ws.AddMergedRegion(new CellRangeAddress((i + index), (((drlist.Count()) - 1) + index + i), 6, 6));
                            cell.SetCellValue("￥ " + sum.ToString("f2"));

                            ws.AddMergedRegion(new CellRangeAddress((i + index), (((drlist.Count()) - 1) + index + i), 8, 8));
                            cell8.SetCellValue(item["reason"].ToString());

                        }


                    }
                    else
                    {
                        decimal total = 0, sum = 0;
                        int cls = 0;
                        DataRow[] drlist = dt.Select("adduser=" + item["adduser"]);
                        for (int j = 0; j < drlist.Count(); j++)
                        {
                            decimal dr_dj = 0;
                            decimal dr_gl = Convert.ToDecimal(drlist[j]["kilometre"]);
                            //每条记录公里不同，费用也不同
                            if (dr_gl >= 35 && dr_gl < 120)
                            {
                                dr_dj = decimal.Parse("1.5");
                            }
                            else if (dr_gl >= 120)
                            {
                                dr_dj = decimal.Parse("1.8");
                            }
                            //相同项目
                            if (drlist[j]["reason"].ToString() == item["reason"].ToString())
                            {
                                sum = sum + (dr_gl * dr_dj);
                                cls++;
                            }

                            total = total + (dr_gl * dr_dj);
                        }
                        //同一人同一项目合计
                        proname = item["reason"].ToString();
                        ws.AddMergedRegion(new CellRangeAddress((i + index), ((cls - 1) + index + i), 6, 6));
                        cell.SetCellValue("￥ " + sum.ToString("f2"));
                        //同一人全部项目合计
                        userid = Convert.ToInt32(item["adduser"]);
                        ws.AddMergedRegion(new CellRangeAddress((i + index), ((drlist.Count() - 1) + index + i), 7,7));
                        cell7.SetCellValue("￥ " + total.ToString("f2") + "");

                        ws.AddMergedRegion(new CellRangeAddress((i + index), ((cls - 1) + index + i), 8, 8));
                        cell8.SetCellValue(item["reason"].ToString());
                    }


                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue("");

                    cs++;
                }

            }
            else
            {
                dt = new DataView(datatable) { RowFilter = "applytype='travel'" }.ToTable();
                ws.GetRow(1).GetCell(2).SetCellValue("天数");

                //创建所有行
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow item = dt.Rows[i];

                    var dataRow = ws.GetRow(i + index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(i + index);//生成行
                }

                int row = 1;
                string proname = "";
                int userid = 0;
                int passcount = 0;//过滤连续日期条数
                int relationid = 0;
                //插入值
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow item = dt.Rows[i];

                    //获取当前人每天只有一条数据集合
                    List<DataRow> drlist = dt.AsEnumerable().Where(t => t.Field<int>("adduser") == Convert.ToInt32(item["adduser"])).OrderBy(t => t.Field<DateTime>("starttime")).GroupBy(t => t.Field<DateTime>("starttime").ToString("yyyy-MM-dd")).Select(r => r.Last()).ToList();

                    // if (userid == Convert.ToInt32(item["adduser"]))
                    //   {
                    //年月日相同，时间不相同,需排除不用显示
                    int gs = drlist.Where(d => d.Field<DateTime>("starttime").ToString("yyyy-MM-dd") == Convert.ToDateTime(item["starttime"]).ToString("yyyy-MM-dd") && d.Field<DateTime>("starttime") != Convert.ToDateTime(item["starttime"])).Count();
                    if (gs > 0)
                    {
                        index--;
                        continue;
                    }


                   
                    //  if (dataRow == null)
                    //      dataRow = ws.CreateRow(i + index);//生成行               


                    decimal fy = 30;
                    string sql = "select rm.RoleName as MagName,rt.RoleName as TecName  from tg_memberRole m left join cm_RoleMag rm on m.RoleIdMag=rm.ID left join cm_RoleMag rt on m.RoleIdTec=rt.ID where mem_Id=" + item["adduser"];
                    DataTable role_dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
                    if (role_dt != null && role_dt.Rows.Count > 0)
                    {
                        if (role_dt.Rows[0][0].ToString().Contains("经理"))
                        {
                            fy = decimal.Parse("50");
                        }
                        else if (role_dt.Rows[0][1].ToString().Contains("设计总监"))
                        {
                            fy = decimal.Parse("40");
                        }
                    }


                    //cell = dataRow.CreateCell(2);
                   // cell.CellStyle = style2;
                   // cell.SetCellValue(cs.ToString());

                    //合计
                    if (userid == Convert.ToInt32(item["adduser"]))
                    {
                       
                            var prouser = dt.AsEnumerable().Where(s => s.Field<string>("reason") == item["reason"].ToString() && s.Field<int>("adduser") == userid).GroupBy(t => t.Field<DateTime>("starttime").ToString("yyyy-MM-dd")).Select(r => r.Last());
                            //获取当前人连续日期的条数
                            int lxrqcount = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false).Count();
                            //获取当前人连续日期的RelationID不同的个数
                            int relacount = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false).GroupBy(s => s.Field<int>("RelationID")).Count();
                            //不同项目,不能有连续日期
                            if (proname != item["reason"].ToString())
                            {
                              
                                if (prouser.Count() > 0)
                                {  
                                var dataRow = ws.GetRow(i + index);//读行

                                var cell = dataRow.CreateCell(0);
                                cell.CellStyle = style2;

                                var cell2 = dataRow.CreateCell(1);
                                cell2.CellStyle = style2;

                                ICell cell6 = dataRow.CreateCell(6);
                                cell6.CellStyle = style2;

                                ICell cell7 = dataRow.CreateCell(7);
                                cell7.CellStyle = style2;

                                ICell cell8 = dataRow.CreateCell(8);
                                cell8.CellStyle = style2;
                                    int ase = prouser.Count();
                                    //同一人不同项目合计
                                    proname = item["reason"].ToString();
                                    decimal userfy = (ase * fy);

                                    //连续日期
                                    int rqcount = 1;
                                    string datestr = Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd");
                                    if (item["RelationID"] != DBNull.Value)
                                    {
                                        if (item["RelationID"] != null && relationid != Convert.ToInt32(item["RelationID"]))
                                        {
                                            List<DataRow> rq_drlist = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false && s.Field<int>("RelationID") == Convert.ToInt32(item["RelationID"])).OrderBy(t => t.Field<int>("ID")).ToList<DataRow>();
                                            if (rq_drlist != null && rq_drlist.Count() > 0)
                                            {
                                                rqcount = rq_drlist.Count();
                                                datestr = Convert.ToDateTime(rq_drlist[0]["starttime"]).ToString("yyyy.MM.dd") + " - " + Convert.ToDateTime(rq_drlist[(rq_drlist.Count() - 1)]["starttime"]).ToString("yyyy.MM.dd");
                                                // fy = fy * rqcount;
                                            }


                                            cell = dataRow.CreateCell(2);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue(rqcount);

                                            cell = dataRow.CreateCell(3);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue(datestr);

                                            //费用
                                            cell = dataRow.CreateCell(4);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue("￥ " + fy.ToString("f2"));

                                            //小计
                                            cell = dataRow.CreateCell(5);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue("￥ " + (fy * rqcount).ToString("f2"));

                                            //赋值
                                            relationid = Convert.ToInt32(item["RelationID"]);
                                          

                                        }
                                    }
                                    else
                                    {
                                        cell = dataRow.CreateCell(2);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue(rqcount);

                                        cell = dataRow.CreateCell(3);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue(datestr);

                                        //费用
                                        cell = dataRow.CreateCell(4);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue("￥ " + fy.ToString("f2"));

                                        //小计
                                        cell = dataRow.CreateCell(5);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue("￥ " + fy.ToString("f2"));
                                    
                                    }
                                    //排除连续日期条数
                                   
                                     ase = ase - (lxrqcount - relacount);
                                   
                                    //同一个人不同项目合计
                                    ws.AddMergedRegion(new CellRangeAddress((i + index), ((ase - 1) + index + i), 6, 6));
                                    cell6.SetCellValue("￥ " + (userfy).ToString("f2"));
                                    //项目
                                    ws.AddMergedRegion(new CellRangeAddress((i + index), ((ase - 1) + index + i), 8, 8));
                                    cell8.SetCellValue(proname);

                                    var cell9 = dataRow.CreateCell(9);
                                    cell9.CellStyle = style2;
                                    cell9.SetCellValue("");
                                    if (rqcount>1)
                                    {
                                        //减去连续日期
                                        index = index - (rqcount - 1);
                                    }
                                    
                                   
                                }


                            }
                            else
                            {
                               
                                //同一个人，同项目，日期分 连续和不连续
                                if (prouser.Count() > 0)
                                {
                                    //连续日期
                                    int rqcount = 1;
                                    string datestr = Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd");
                                    if (item["RelationID"] != DBNull.Value)
                                    {
                                        if (item["RelationID"] != null && relationid != Convert.ToInt32(item["RelationID"]))
                                        {

                                            List<DataRow> rq_drlist = prouser.Where(s => Convert.IsDBNull(s["RelationID"]) == false && s.Field<int>("RelationID") == Convert.ToInt32(item["RelationID"])).OrderBy(t => t.Field<int>("ID")).ToList<DataRow>();
                                            if (rq_drlist != null && rq_drlist.Count() > 0)
                                            {
                                                rqcount = rq_drlist.Count();
                                                datestr = Convert.ToDateTime(rq_drlist[0]["starttime"]).ToString("yyyy.MM.dd") + " - " + Convert.ToDateTime(rq_drlist[(rq_drlist.Count() - 1)]["starttime"]).ToString("yyyy.MM.dd");
                                                // fy = fy * rqcount;
                                            }
                                            var dataRow = ws.GetRow(i + index);//读行

                                            var cell = dataRow.CreateCell(0);
                                            cell.CellStyle = style2;

                                            var cell2 = dataRow.CreateCell(1);
                                            cell2.CellStyle = style2;

                                            cell = dataRow.CreateCell(2);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue(rqcount);

                                            cell = dataRow.CreateCell(3);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue(datestr);

                                            //费用
                                            cell = dataRow.CreateCell(4);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue("￥ " + fy.ToString("f2"));

                                            //小计
                                            cell = dataRow.CreateCell(5);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue("￥ " + (fy * rqcount).ToString("f2"));

                                            ICell cell6 = dataRow.CreateCell(6);
                                            cell6.CellStyle = style2;

                                            ICell cell7 = dataRow.CreateCell(7);
                                            cell7.CellStyle = style2;

                                            ICell cell8 = dataRow.CreateCell(8);
                                            cell8.CellStyle = style2;

                                            cell = dataRow.CreateCell(9);
                                            cell.CellStyle = style2;
                                            cell.SetCellValue("");

                                            if (rqcount > 1)
                                            {
                                                //减去连续日期
                                                index = index - (rqcount - 1);
                                            }
                                            //赋值
                                            relationid = Convert.ToInt32(item["RelationID"]);
                                        }
                                    }
                                    else
                                    {

                                        var dataRow = ws.GetRow(i + index);//读行
                                        var cell = dataRow.CreateCell(0);
                                        cell.CellStyle = style2;

                                        var cell2 = dataRow.CreateCell(1);
                                        cell2.CellStyle = style2;

                                        cell = dataRow.CreateCell(2);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue(rqcount);

                                        cell = dataRow.CreateCell(3);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue(datestr);


                                        cell = dataRow.CreateCell(4);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue("￥ " + fy.ToString("f2"));


                                        cell = dataRow.CreateCell(5);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue("￥ " + fy.ToString("f2"));

                                        ICell cell6 = dataRow.CreateCell(6);
                                        cell6.CellStyle = style2;

                                        ICell cell7 = dataRow.CreateCell(7);
                                        cell7.CellStyle = style2;

                                        ICell cell8 = dataRow.CreateCell(8);
                                        cell8.CellStyle = style2;

                                        cell = dataRow.CreateCell(9);
                                        cell.CellStyle = style2;
                                        cell.SetCellValue("");
                                    }


                                }
                            }

                       


                    }
                    else
                    {
                        
                        //获取当前人连续日期的条数
                        int lxrqcount = drlist.Where(s => Convert.IsDBNull(s["RelationID"]) == false).Count();
                        //获取当前人连续日期的RelationID不同的个数
                        int relacount = drlist.Where(s => Convert.IsDBNull(s["RelationID"]) == false).GroupBy(s => s.Field<int>("RelationID")).Count();
                        //获取当前人总条数
                        int sumcls = drlist.Count();

                        var dataRow = ws.GetRow(i + index);//读行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;

                        var cell2 = dataRow.CreateCell(1);
                        cell2.CellStyle = style2;

                        ws.AddMergedRegion(new CellRangeAddress((i + index), (((sumcls - (lxrqcount - relacount)) - 1) + index + i), 0, 0));
                        cell.SetCellValue((row));                      

                        ws.AddMergedRegion(new CellRangeAddress((i + index ), (((sumcls - (lxrqcount - relacount)) - 1) + index + i), 1, 1));
                        cell2.SetCellValue(item["AddUserName"].ToString());

                        row++;

                        //连续日期
                        int rqcount = 1;
                        string datestr = Convert.ToDateTime(item["starttime"]).ToString("yyyy.MM.dd");
                        if (item["RelationID"] != DBNull.Value)
                        {
                            if (item["RelationID"] != null && relationid != Convert.ToInt32(item["RelationID"]))
                            {
                                List<DataRow> rq_drlist = drlist.Where(s => Convert.IsDBNull(s["RelationID"]) == false && s.Field<string>("reason") == item["reason"].ToString() && s.Field<int>("RelationID") == Convert.ToInt32(item["RelationID"])).OrderBy(t => t.Field<int>("ID")).ToList<DataRow>();

                                if (rq_drlist != null && rq_drlist.Count() > 0)
                                {
                                    rqcount = rq_drlist.Count();
                                    datestr = Convert.ToDateTime(rq_drlist[0]["starttime"]).ToString("yyyy.MM.dd") + " - " + Convert.ToDateTime(rq_drlist[(rq_drlist.Count() - 1)]["starttime"]).ToString("yyyy.MM.dd");

                                }

                                cell = dataRow.CreateCell(2);
                                cell.CellStyle = style2;
                                cell.SetCellValue(rqcount);

                                cell = dataRow.CreateCell(3);
                                cell.CellStyle = style2;
                                cell.SetCellValue(datestr);


                                cell = dataRow.CreateCell(4);
                                cell.CellStyle = style2;
                                cell.SetCellValue("￥ " + fy.ToString("f2"));


                                cell = dataRow.CreateCell(5);
                                cell.CellStyle = style2;
                                cell.SetCellValue("￥ " + (fy * rqcount).ToString("f2"));

                                //赋值
                                relationid = Convert.ToInt32(item["RelationID"]);
                            }
                        }
                        else
                        {
                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(rqcount);

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(datestr);

                            //费用
                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue("￥ " + fy.ToString("f2"));

                            //小计
                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue("￥ " + (fy).ToString("f2"));
                           
                        }

                        decimal total = 0, sum = 0;
                        int cls = 0;
                        // DataRow[] drlist = dt.Select("adduser=" + item["adduser"]);
                        for (int j = 0; j < drlist.Count(); j++)
                        {
                            if (drlist[j]["reason"].ToString() == item["reason"].ToString())
                            {
                                sum = sum + fy;
                                cls++;
                            }

                            total = total + fy;
                        }

                        //排除连续日期条数
                        if (rqcount > 1)
                        {
                            cls = cls - (rqcount - 1);
                        }

                        ICell cell6 = dataRow.CreateCell(6);
                        cell6.CellStyle = style2;

                        ICell cell7 = dataRow.CreateCell(7);
                        cell7.CellStyle = style2;

                        ICell cell8 = dataRow.CreateCell(8);
                        cell8.CellStyle = style2;

                        //同一人同一项目合计
                        proname = item["reason"].ToString();
                        ws.AddMergedRegion(new CellRangeAddress((i + index ), ((cls - 1) + index + i), 6, 6));
                        cell6.SetCellValue("￥ " + sum.ToString("f2"));
                        //同一人全部项目合计
                        userid = Convert.ToInt32(item["adduser"]);
                        ws.AddMergedRegion(new CellRangeAddress((i + index), (((sumcls - (lxrqcount - relacount)) - 1) + index + i), 7, 7));
                        cell7.SetCellValue("￥ " + total.ToString("f2") + "");

                        ws.AddMergedRegion(new CellRangeAddress((i + index), ((cls - 1) + index + i), 8, 8));
                        cell8.SetCellValue(proname);

                          cell = dataRow.CreateCell(9);
                          cell.CellStyle = style2;
                          cell.SetCellValue("");

                          //排除连续日期条数
                          if (rqcount > 1)
                          {
                              //减去连续日期
                              index = index -(rqcount - 1);
                          }
                    }

                 
                }
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

    }
}