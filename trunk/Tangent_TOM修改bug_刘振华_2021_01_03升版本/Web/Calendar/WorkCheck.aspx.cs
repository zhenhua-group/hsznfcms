﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;
using System.Globalization;
using System.Text;
using System.IO;


namespace TG.Web.Calendar
{
    public partial class WorkCheck : PageBase
    {
        private Thread O;
        private CultureInfo O10OO0lO10;
        DataTable datatable = new DataTable();
        DataTable datatable2 = new DataTable();
        /// <summary>
        /// 设置当前数据库链接地址
        /// </summary>
        protected void SetCurrentConnectionString()
        {

            string yearmonth = this.selectYear.Items[this.selectYear.SelectedIndex].Text + this.selectMonth.Items[this.selectMonth.SelectedIndex].Text;
            datatable = GetCurrentMonth(yearmonth, "1");

        }
        private void SetCurrentConnectionString2()
        {
            string YearIndex = this.selectYear.Value;
            string MonthIndex = this.selectMonth.Value;
            if (MonthIndex == "12")
            {
                YearIndex = (Convert.ToInt32(YearIndex) + 1) + "";
                MonthIndex = "1";
            }
            else
            {
                MonthIndex = (Convert.ToInt32(MonthIndex) + 1) + "";
            }
            string yearmonth = YearIndex.PadLeft(2, '0') + MonthIndex.PadLeft(2, '0');
            datatable2 = GetCurrentMonth(yearmonth, "2");

        }
        /// <summary>
        /// 是否存在考勤数据
        /// </summary>
        protected bool IsExsitKQFile
        {
            get
            {
                string filepath = Server.MapPath("~/Attach_User/kaoqin/KQ" + this.selectYear.Items[this.selectYear.SelectedIndex].Text + this.selectMonth.Items[this.selectMonth.SelectedIndex].Text + ".xls");

                if (File.Exists(filepath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 下拉框用户
        /// </summary>
        public int UserID
        {
            get;
            set;
        }

        /// <summary>
        /// 下拉框用户姓名
        /// </summary>
        public string UserName
        {
            get;
            set;
        }
        //登录人密码
        public string currentpassword
        {
            get
            {
                string password = "";
                if (UserSysNo > 0)
                {
                    password = new TG.BLL.tg_member().GetModel(UserSysNo).mem_Password;
                }
                return password;
            }
        }
        protected string datastring
        {
            get { return Request["date"] ?? DateTime.Now.ToShortDateString(); }
        }
       
        TG.BLL.tg_staffRelation bllsta = new TG.BLL.tg_staffRelation();
        List<TG.Model.cm_PersonAttendSet> pas_list = new List<TG.Model.cm_PersonAttendSet>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.Page.IsPostBack)
            {
                this.hid_userid.Value = UserSysNo.ToString();
                int oldyear = 2016;
                //初始化年
                int curryear = DateTime.Now.Year;
                for (int i = oldyear; i <= curryear; i++)
                {
                    this.selectYear.Items.Add(i.ToString());
                }
                //绑定权限
                BindPreviewPower();
                //绑定部门
                BindUnit();
              
                if (!string.IsNullOrEmpty(datastring))
                {
                    string year = Convert.ToDateTime(datastring).Year.ToString();
                    string month = Convert.ToDateTime(datastring).Month.ToString();
                    string day = Convert.ToDateTime(datastring).Day.ToString();
                    //初始化日期值
                    this.HidCurrtDate.Value = year + "-" + month;
                    this.selectYear.Items.FindByValue(year).Selected = true;
                    this.selectMonth.Items.FindByValue(month).Selected = true;
                    this.DataCalendar1.VisibleDate = new DateTime(int.Parse(year), int.Parse(month), 16);
                    //  DateTime startDate = new DateTime(this.DataCalendar1.VisibleDate.Year, this.DataCalendar1.VisibleDate.Month, 1).AddDays(-7.0);
                    //  DateTime endDate = new DateTime(this.DataCalendar1.VisibleDate.Date.AddMonths(1).Year, this.DataCalendar1.VisibleDate.Date.AddMonths(1).Month, 1).AddDays(7.0);
                }
                else
                {
                    //初始化日期值
                    this.HidCurrtDate.Value = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString();
                    this.selectYear.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
                    this.selectMonth.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
                    this.DataCalendar1.VisibleDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    //  DateTime startDate = new DateTime(this.DataCalendar1.VisibleDate.Year, this.DataCalendar1.VisibleDate.Month, 1).AddDays(-7.0);
                    // DateTime endDate = new DateTime(this.DataCalendar1.VisibleDate.Date.AddMonths(1).Year, this.DataCalendar1.VisibleDate.Date.AddMonths(1).Month, 1).AddDays(7.0);
                }
                //初始化人员               
                BindMember();


                UserID = UserSysNo;
                UserName = new TG.BLL.tg_member().GetModel(UserSysNo).mem_Name;
             
            }

            if (this.Page.IsPostBack)
            {
                //当前年月份
                this.DataCalendar1.VisibleDate = new DateTime(Convert.ToInt32(this.selectYear.Value), Convert.ToInt32(this.selectMonth.Value), 1);
                //当前年月份往上个月推7天
                DateTime time20 = new DateTime(this.DataCalendar1.VisibleDate.Year, this.DataCalendar1.VisibleDate.Month, 1).AddDays(-7.0);
                //当月1号
                DateTime StateDate = new DateTime(this.DataCalendar1.VisibleDate.Year, this.DataCalendar1.VisibleDate.Month, 1);
                //下个月份加上7天
                DateTime time28 = new DateTime(this.DataCalendar1.VisibleDate.Date.AddMonths(1).Year, this.DataCalendar1.VisibleDate.Date.AddMonths(1).Month, 1).AddDays(7.0);
                //下月1号
                DateTime EndDate = new DateTime(this.DataCalendar1.VisibleDate.Date.AddMonths(1).Year, this.DataCalendar1.VisibleDate.Date.AddMonths(1).Month, 1);

                //绑定人员
                BindMember();
               

                UserID = Convert.ToInt32(userlist.Value);
                UserName = userlist.Items[userlist.SelectedIndex].Text;

            }

            //日期格式初始化
            this.Ol010Ol01l1O1lllO11();

            //加载1号到15号考勤
            SetCurrentConnectionString();

            //加载16号到31号考勤
            SetCurrentConnectionString2();


            this.DataCalendar1.DayRender += new DayRenderEventHandler(DataCalendar1_DayRender);

            //   BindTaster();


            //后台设置上班时间
            pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" mem_ID=" + UserSysNo);            


        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        private void BindTaster()
        {
            string strwhere = " firstLeader=" + UserSysNo;
            int count = bllsta.GetRecordCount(strwhere);
            if (count > 0)
            {
                this.hiddenIstaster.Value = "1";
                this.hiddentaster.Value = UserSysNo.ToString();
            }
            else
            {
                this.hiddenIstaster.Value = "0";
            }
        }
        //暂无用(原文件代码)
        #region

        //点击日期跳转页
        protected void DataCalendar1_SelectionChanged(object sender, EventArgs e)
        {
            DateTime selectedDate = this.DataCalendar1.SelectedDate;
            base.Response.Redirect("Apply.aspx?date=" + selectedDate.Year.ToString() + "-" + selectedDate.Month.ToString() + "-" + selectedDate.Day.ToString());
        }
        private void Ol010Ol01l1O1lllO11()
        {
            this.DataCalendar1.PreRender += new EventHandler(this.Olll1l1O0lO00O0101lll0O);
        }


        private void Olll1l1O0lO00O0101lll0O(object sender, EventArgs e)
        {
            this.O = Thread.CurrentThread;
            this.O10OO0lO10 = this.O.CurrentCulture;
            CultureInfo info = (CultureInfo)this.O10OO0lO10.Clone();
            info.DateTimeFormat.DayNames = new string[] { "日", "一", "二", "三", "四", "五", "六" };
            info.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Sunday;
            this.O.CurrentCulture = info;
        }
        #endregion
        //日期单元格绑定
        protected void DataCalendar1_DayRender(object sender, DayRenderEventArgs e)
        {
            //日历呈现时当前日期时间
            string daytime = e.Day.Date.ToString("yyyy-MM-dd");
            //是否其他月日期
            bool isothermonth = e.Day.IsOtherMonth;
            //是否可以选中日期
            bool isselectable = e.Day.IsSelectable;
            //是否已经选中
            bool isselected = e.Day.IsSelected;
            //是否为今天
            bool istoday = e.Day.IsToday;
            //是否为周么
            bool isweekend = e.Day.IsWeekend;

            UserCheckTime userchetime = new UserCheckTime();
            //其他月份不查询
            string showContent = "";
            //是当月日期
            if (!isothermonth)
            {

                string applystring = "";
                //如果当前是自己只能才可以申请
                if (UserSysNo == UserID)
                {
                    //申请字符串
                    applystring = "<a href='Apply.aspx?date=" + daytime + "' title='" + daytime + "'>申请</a>";
                }

                //日期单元格字符串拼接
                showContent += "<a href=\"Apply.aspx?date=" + daytime + "\" style='color:black;'>" + e.Day.DayNumberText + "</a><br/>";
                showContent += string.Format("<table width=\"100%\" class=\"kqdj\">");

                //申请记录
                DataTable dt = BindApply(daytime);

                //考勤数据存在
                if (datatable.Rows.Count > 0 || datatable2.Rows.Count > 0)
                {
                    //考勤记录
                    userchetime = SelectCardTimeByDateTime(daytime, dt);
                    if (!string.IsNullOrEmpty(userchetime.InCheckedTime) || !string.IsNullOrEmpty(userchetime.OutCheckedTime))
                    {

                        showContent += string.Format("<tr><td style='font-size:9pt;'>上班:{0}</td><td rowSpan=\"2\" align='right' style='font-size:9pt;'>{1}<td></tr>", userchetime.InCheckedTime, applystring);
                        showContent += string.Format("<tr><td style='font-size:9pt;'>下班:{0}</td><tr>", userchetime.OutCheckedTime);
                    }
                    else
                    {
                        showContent += string.Format("<tr><td style=\"font-size:9pt;color:red;text-align:left;\">未打卡</td><Td align='right'>{0}</td><tr>", applystring);
                    }
                }
                else
                {
                    showContent += string.Format("<tr><td colspan='2' align='right'>{0}</td><tr>", applystring);
                }
                //申请记录循环
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string sSort = "";
                        switch (dr["applytype"].ToString())
                        {
                            case "travel":
                                sSort = "出差";
                                break;
                            case "gomeet":
                                sSort = "外勤";
                                break;
                            case "forget":
                                sSort = "未打卡";
                                break;
                            case "company":
                                sSort = "公司活动";
                                break;
                            case "depart":
                                sSort = "部门活动";
                                break;
                            case "addwork":
                                sSort = "加班离岗";
                                break;
                            default:
                                sSort = "请假";
                                break;
                        }
                        string hrefurl = "";
                        string content = "";

                        switch (dr["AuditStatus"].ToString())
                        {
                            case "":
                            case null:
                                string applyname = new TG.BLL.tg_member().GetModel(Convert.ToInt32(dr["adduser"])).mem_Name;
                                string applytime = dr["starttime"] + " - " + dr["endtime"];
                                //如果当前是自己只能预览
                                //if (UserSysNo == UserID)
                                //{
                                //    hrefurl = "href='Apply.aspx?applyID=" + dr["id"] + "&date=" + daytime + "'";
                                //}
                                //else
                                //{
                                //  hrefurl = "applyID=\"" + dr["id"] + "\" daytime=\"" + e.Day.Date.ToString("yyyy年MM月dd日") + "\" applyname=\"" + applyname + "\" applytime=\"" + applytime + "\" applysort=\"" + sSort + "\" reason=\"" + dr["reason"] + "\" addtime=\"" + dr["applyTime"] + "\" data-toggle=\"modal\" href=\"#AuditMsg\"";
                                //  }
                                content = "(尚未审批)";

                                break;
                            case "A":
                                content = "(审批中)";
                                break;
                            case "C":
                            case "E":
                                content = "(未通过审批)";

                                break;
                            case "B":
                                string SpecialPerson = System.Configuration.ConfigurationManager.AppSettings["SpecialPerson"];
                                SpecialPerson = SpecialPerson.Replace("|", ",") + ",";
                                if (((Convert.ToDouble(dr["totaltime"]) / 7.5) >= 5) && !SpecialPerson.Contains(dr["memName"].ToString() + ","))
                                {
                                    content = "(审批中)";
                                }
                                break;
                            case "N":
                                content = "(已退回)";
                                break;


                        }
                        string iscar = "否";
                        if (dr["iscar"] != null && dr["iscar"].ToString() == "s")
                        {
                            iscar = "是";
                        }
                        string color = content == "" ? "" : "style='color:red'";
                        hrefurl = " applyID=\"" + dr["id"] + "\" applytype=\"" + dr["applytype"] + "\" reason=\"" + dr["reason"] + "\" address=\"" + dr["address"] + "\" applysort=\"" + sSort + "\" applycontent=\"" + content + "\" starttime=\"" + dr["starttime"] + "\" endtime=\"" + dr["endtime"] + "\" totaltime=\"" + dr["totaltime"] + "\" iscar=\"" + iscar + "\" kilometre=\"" + dr["kilometre"] + "\" remark=\"" + dr["remark"] + "\" memname=\"" + dr["memName"] + "\"  data-toggle=\"modal\" href=\"#AuditMsg\" ";
                        showContent += "<tr><td colspan='2'><a " + hrefurl + color + " >" + sSort;
                        showContent += content + "</a></td></tr>";
                        ////未审批或未通过
                        //if (dr["AuditStatus"].ToString() == "0")
                        //{

                        //    //审批内容不为空,即未通过
                        //    if (dr["remark"] != null && !string.IsNullOrEmpty(dr["remark"].ToString()))
                        //    {
                        //        hrefurl = "href='Apply.aspx?applyID=" + dr["id"] + "&date=" + daytime + "'";
                        //        content = "(未通过审批)";
                        //    }
                        //    else
                        //    {


                        //        content = "(尚未审批)";

                        //        //有审批按钮
                        //        //content = "(尚未审批)</a><a applyID=\"" + dr["id"] + "\" daytime=\"" + e.Day.Date.ToString("yyyy年MM月dd日") + "\" applyname=\"" + applyname + "\" applytime=\"" + applytime + "\" applysort=\"" + sSort + "\" reason=\"" + dr["reason"] + "\" addtime=\"" + dr["applyTime"] + "\" data-toggle=\"modal\" href=\"#AuditMsg\" style='font-size:8px;padding:0px 1px;' class=\"btn green btn-xs\">审批</a></td></tr>";
                        //    }


                        //    //判断是京外出差需显示外出时间
                        //    if (dr["sort"].ToString() == "2")
                        //    {
                        //        showContent += "(" + dr["outTime"] + ")";
                        //    }
                        //    showContent += content + "</a></td></tr>";
                        //}
                        //else
                        //{
                        //    showContent += "<tr><td colspan='2'><a href='Apply.aspx?applyID=" + dr["id"] + "&date=" + daytime + "'>" + sSort;
                        //    //判断是京外出差需显示外出时间
                        //    if (dr["sort"].ToString() == "2")
                        //    {
                        //        showContent += "(" + dr["outTime"] + ")";
                        //    }
                        //    showContent += "</a></td></tr>";
                        //}

                    }
                }
                showContent += "</table>";
            }


            e.Cell.Text = showContent;
        }
        /// <summary>
        /// 考勤记录
        /// </summary>
        /// <param name="datetime"></param>
        private UserCheckTime SelectCardTimeByDateTime(string daytime, DataTable dt_apply)
        {
            UserCheckTime userchecked = new UserCheckTime();
            int days = Convert.ToDateTime(daytime).Day;
            //小于1-15号读取datatable，大于15号读取datatable2
            if (days < 16)
            {
                if (datatable != null && datatable.Rows.Count > 0)
                {
                    userchecked = GetUserCheckTime(daytime, datatable, dt_apply);
                }
            }
            else
            {
                if (datatable2 != null && datatable2.Rows.Count > 0)
                {
                    userchecked = GetUserCheckTime(daytime, datatable2, dt_apply);
                }
            }


            return userchecked;
        }
        /// <summary>
        /// 得到上下班打卡公用方法
        /// </summary>
        /// <param name="datatable"></param>
        /// <returns></returns>
        private UserCheckTime GetUserCheckTime(string daytime, DataTable datatable, DataTable dt_apply)
        {
          
               int months = Convert.ToDateTime(daytime).Month;
               int years = Convert.ToDateTime(daytime).Year;
               int days = Convert.ToDateTime(daytime).Day;

               //考勤月上下班
               if (days > 15)
               {
                   if (months == 12)
                   {
                       months = 1;
                       years = years + 1;
                   }
                   else
                   {
                       months = months + 1;
                   }
               }

            //考勤上下班
            string towork = "09:00", offwork = "17:30";
            if (pas_list != null && pas_list.Count > 0)
            {
                var pas_list2 = pas_list.Where(p => p.attend_year == years && p.attend_month == months);
                if (pas_list2 != null && pas_list2.Count()>0)
                {
                    towork = pas_list2.First().ToWork;
                    offwork = pas_list2.First().OffWork;
                }
                
            }
           

            UserCheckTime userchecked = new UserCheckTime();
            DataTable dt = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (daytime + " 06:00:00") + "' and CHECKTIME<='" + ((Convert.ToDateTime(daytime).AddDays(1).ToString("yyyy-MM-dd")) + " 06:00:00") + "' and UserName='" + UserName + "'" }.ToTable();
            int count = dt.Rows.Count;
            if (count > 0)
            {
                DateTime dt_in = Convert.ToDateTime(dt.Rows[0]["CHECKTIME"].ToString());

                DateTime dt_out = Convert.ToDateTime(dt.Rows[count - 1]["CHECKTIME"].ToString());
                //中午下班判断
                DateTime dt_mid = Convert.ToDateTime(daytime + " 11:50:00");
                //中午上班
                DateTime dt_sbtime = Convert.ToDateTime(daytime + " 13:00:59");
                //上午打卡时间限制
                DateTime dt_morning = Convert.ToDateTime(daytime + " " + towork + ":59");
                //下午打卡时间限制
                DateTime dt_afternoon = Convert.ToDateTime(daytime + " " + offwork + ":00");
                //未打卡
                string emptyStyle = "<span style=\"color:red;\">未打卡</span>";

                //出差和外勤申请记录
                string str = "";
                if (dt_apply != null && dt_apply.Rows.Count > 0)
                {
                    //出差或外勤今天的0点到6,允许迟到半天
                    DataTable travel_yestoday = new DataView(dt_apply) { RowFilter = "applytype in ('travel','gomeet') and endtime<='" + (daytime + " 06:00:00") + "'" }.ToTable();
                    if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                    {
                        str = "bt";
                    }
                }
                //昨天下班打卡时间
                string yestoday = "";
                DateTime yes_time = Convert.ToDateTime(daytime);
                yes_time = yes_time.AddDays(-1);
                DataTable dt_yestoday = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (yes_time.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (daytime + " 06:00:00") + "' and UserName='" + UserName + "'" }.ToTable();
                if (dt_yestoday != null && dt_yestoday.Rows.Count > 0)
                {
                    yestoday = Convert.ToDateTime(dt_yestoday.Rows[(dt_yestoday.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm");
                }
                //已查询允许迟到半天
                if (str == "")
                {
                    //出差或外勤昨天22点到23:50,允许迟到半小时
                    string strwhere = string.Format(" and (applytype='travel' or applytype='gomeet') and '{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) and adduser={1} and starttime>='{2}' and endtime<'{3}'", yes_time.ToString("yyyy-MM-dd"), UserID, (yes_time.ToString("yyyy-MM-dd") + " 22:00:00"), (yes_time.ToString("yyyy-MM-dd") + " 23:50:00"));
                    DataTable travel_yestoday = new TG.BLL.cm_ApplyInfo().GetApplyList(strwhere).Tables[0];
                    if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                    {
                        str = "bxs";
                    }
                }

                //昨天加班到22：00，所以允许今天迟到半小时后,统计从9:30
                if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 22:00:00") && Convert.ToDateTime(yestoday) < Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bxs")
                {
                    dt_morning = dt_morning.AddMinutes(30);
                }
                //昨天加班到23:50，所以允许迟到半天,统计从13：0:0
                else if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bt")
                {
                    dt_morning = dt_sbtime;
                }

                if (count == 1)
                {
                    //上班打卡时间是下午
                    if (dt_morning > dt_mid)
                    {
                        userchecked.InCheckedTime = dt_in <= dt_morning ? dt_in.ToShortTimeString() : "<span style=\"color:red;\">" + dt_in.ToShortTimeString() + "</span>";
                        userchecked.OutCheckedTime = emptyStyle;
                        //迟到分情况
                        if (dt_in > dt_morning)
                        {
                            userchecked.InCheckedTime = ChiDao(dt_in, dt_morning, userchecked.InCheckedTime);
                        }

                    }
                    else
                    {
                        //上午
                        userchecked.InCheckedTime = dt_in <= dt_mid ? (dt_in <= dt_morning ? dt_in.ToShortTimeString() : "<span style=\"color:red;\">" + dt_in.ToShortTimeString() + "</span>") : emptyStyle;
                        //下午
                        userchecked.OutCheckedTime = dt_out >= dt_mid ? (dt_out >= dt_afternoon ? dt_out.ToShortTimeString() : "<span style=\"color:red;\">" + dt_out.ToShortTimeString() + "</span>") : emptyStyle;
                       
                        //迟到分情况
                        if (dt_in <= dt_mid&&dt_in > dt_morning)
                        {
                            userchecked.InCheckedTime = ChiDao(dt_in, dt_morning, userchecked.InCheckedTime);
                        }
                    }

                }
                else
                {
                    userchecked.InCheckedTime = dt_in <= dt_morning ? dt_in.ToShortTimeString() : "<span style=\"color:red;\">" + dt_in.ToShortTimeString() + "</span>";
                    userchecked.OutCheckedTime = dt_out >= dt_afternoon ? dt_out.ToShortTimeString() : "<span style=\"color:red;\">" + dt_out.ToShortTimeString() + "</span>";
                    //迟到分情况
                    if (dt_in > dt_morning)
                    {
                        userchecked.InCheckedTime = ChiDao(dt_in, dt_morning, userchecked.InCheckedTime);
                    }
                }

            }
            return userchecked;
        }
        /// <summary>
        /// 迟到分情况
        /// </summary>
        /// <param name="dt_in"></param>
        /// <param name="dt_morning"></param>
        /// <param name="oldstr"></param>
        /// <returns></returns>
        public string ChiDao(DateTime dt_in, DateTime dt_morning, string oldstr)
        {
            string str = oldstr;
            TimeSpan ts = dt_in - dt_morning;
            //大于等于4分钟才算迟到
            if (ts.TotalMinutes > 3)
            {
                if (ts.TotalMinutes >= 4 && ts.TotalMinutes <= 14)//迟到
                {
                    str = "<span style=\"color:blue;\">" + dt_in.ToShortTimeString() + "</span>";

                }
            }
            else
            {
                //小于3分钟不变色
                str = dt_in.ToShortTimeString();
            }

            return str;
        }
        /// <summary>
        /// 绑定申请记录
        /// </summary>
        /// <param name="daytime"></param>
        /// <returns></returns>
        private DataTable BindApply(string daytime)
        {
            
            DataSet dataSet = new DataSet();
            //第二天凌晨
            DateTime tomr_daytime = Convert.ToDateTime(daytime).AddDays(1);

            string sql = @"select *,(select top 1 SysNo from cm_applyinfoaudit where applyID=ID order by SysNo desc) as AuditSysNo
                                   ,(select top 1 Status from cm_applyinfoaudit where applyID=ID order by SysNo desc) as AuditStatus
                                   ,(select mem_Name from tg_member where mem_ID=adduser) as memName
                            from cm_applyinfo where adduser=" + UserID + @" and (
                                (applytype not in ('travel','gomeet') and '" + daytime + @"' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120)) 
                             or (applytype in ('travel','gomeet') and '" + daytime + " 06:00:00'<=starttime and endtime<='" + tomr_daytime.ToString("yyyy-MM-dd")+ " 06:00:00')) order by addtime desc";
            dataSet = TG.DBUtility.DbHelperSQL.Query(sql);
            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                //当天是节假日和周六日，排除 请假，公司活动，部门活动
                if (isJQ(Convert.ToDateTime(daytime)))
                {
                    DataTable dt_apply = new DataView(dataSet.Tables[0]) { RowFilter = "applytype not in ('leave','company','depart')" }.ToTable();
                    if (dt_apply != null && dt_apply.Rows.Count > 0)
                    {
                        return dt_apply;
                    }
                }
                else
                {
                    return dataSet.Tables[0];
                }
                
            }
            return null;
        }

        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();
            string sqlwhere = "";

            if ((base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2))
            {
                sqlwhere = " and unit_ID IN (Select top 1 mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_ID=" + UserSysNo + " order by mr.mem_unit_ID desc)";
            }
            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 and unit_ID not in (" + NotShowUnitList + ")" + sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定          
            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定人员
        protected void BindMember()
        {
            //初始化人员 
            StringBuilder sb = new StringBuilder(" (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + this.selectYear.Value + "-" + (this.selectMonth.Value.PadLeft(2, '0')) + "-16'))");
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (mem_ID =" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND mem_ID in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=" + UserUnitNo + ")");
            }
            //选择部门
            if (this.drp_unit.SelectedIndex > 0)
            {
                sb.Append(" AND mem_ID in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=" + this.drp_unit.SelectedValue + ")");
            }
            userlist.DataSource = new TG.BLL.tg_member().GetList(sb.ToString());
            userlist.DataTextField = "mem_Name";
            userlist.DataValueField = "mem_ID";
            userlist.DataBind();
            string olduserid = this.hid_userid.Value;

            if (this.userlist.Items.FindByValue(olduserid) != null)
            {
                this.userlist.Items.FindByValue(olduserid).Selected = true;
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
               
            }
        }
       
    }

    /// <summary>
    /// 用户打卡对象
    /// </summary>
    class UserCheckTime
    {
        public string InCheckedTime { get; set; }

        public string OutCheckedTime { get; set; }
    }
}