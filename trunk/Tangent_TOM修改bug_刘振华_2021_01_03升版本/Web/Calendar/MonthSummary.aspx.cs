﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Configuration;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace TG.Web.Calendar
{
    public partial class MonthSummary : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                BindData();
            }

        }
        private DataTable BindWhere(string strtype)
        {
            StringBuilder sb = new StringBuilder(" 1=1 ");
            //导出全部部门不需要判断
            if (strtype != "all")
            {
                //部门
                // if (this.drp_unit.SelectedIndex > 0)
                // {
                sb.Append(" AND t.Unit_ID=" + this.drp_unit.SelectedValue + "");
                //  }
            }
            //检查权限
            GetPreviewPowerSql(ref sb);
            sb.Append(" and t.unit_ParentID<>0 and t.unit_ID<>230  and t.unit_ID not in (" + NotShowUnitList + ") order by te.unit_Order asc,t.unit_ID asc ");

            string sql = "select t.* from tg_unit t join tg_unitExt te on  te.unit_ID=t.unit_ID where " + sb.ToString();

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            return dt;
        }
        //根据条件查询
        private void BindData()
        {

            DataTable dt = BindWhere("");
            //生成table
            CreateTable(dt);
        }
        public void CreateTable(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();

            string drpyear = this.drp_year.SelectedValue;
            string drpmonth = this.drp_month.SelectedValue;
            var nextyear = Convert.ToInt32(drpyear);
            var nextmonth = (Convert.ToInt32(drpmonth) - 1);
            if (drpmonth == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) - 1);
                nextmonth = 12;
            }

            DateTime starttime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
            DateTime endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-15");
            //不算考勤
            string aa = ConfigurationManager.AppSettings["NoWork"];
            string nowork = "'" + (aa.Replace(",", "','")) + "'";

            if (dt != null && dt.Rows.Count > 0)
            {
                //获取锁定数据
                List<TG.Model.cm_MonthSummaryHis> his_list = new TG.BLL.cm_MonthSummaryHis().GetModelList(" dataDate='" + drpyear + "-" + drpmonth.PadLeft(2, '0') + "'");
                //得到所有节假日
                List<TG.Model.cm_HolidayConfig> holi_list = new TG.BLL.cm_HolidayConfig().GetModelList("");
                //后台设置上班时间
                List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList("");
                //申请加班记录
                DataTable dt_time = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype in ('travel','gomeet','addwork','forget') ").Tables[0];
                //加班统计手动修改数据
                List<TG.Model.cm_ApplyStatisData> overtime_list = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='MonthSummary' order by id desc");

                sb2.Append("<tr>");
                int row = 1;
                foreach (DataRow item in dt.Rows)
                {
                    int rs = 0, pjgs = 0, cyjsr = 0;
                    //平均值，中位值，最小值，最大值，标准差,标准差系数,部门经理系数
                    decimal avgtime = 0, middletime = 0, mintime = 0, maxtime = 0, standardtime = 0, standardxs = 0, managerxs = 0;
                    decimal weekovertime = 0, sumbztime = 0, managertime = 0;//标准差总值
                    //把钟志宏添加到结构部 
                    string where = "";
                    if (item["unit_ID"].ToString() == "237")
                    {
                        where = " m.mem_ID=1445 or ";
                    }
                    //循环人员
                    DataTable dt_list = TG.DBUtility.DbHelperSQL.Query("select m.*,mr.RoleIdMag from tg_member m left join tg_memberRole mr on m.mem_ID=mr.mem_Id where " + where + " (mr.mem_Unit_ID=" + item["unit_ID"] + " and mem_Name not in (" + nowork + ") and (mem_isFired=0 or m.mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + starttime.ToString("yyyy-MM-dd") + "'))) order by mem_Order asc,m.mem_ID asc").Tables[0];
                    // List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(where + " (mem_id in (select mem_Id  from tg_memberRole where mem_Unit_ID=" + item["unit_ID"] + ") and mem_Name not in (" + nowork + ") and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + starttime.ToString("yyyy-MM-dd") + "'))) order by mem_Order asc,mem_ID asc");

                    List<TG.Model.tg_member> list = new TG.BLL.tg_member().DataTableToList(dt_list);
                    if (list != null && list.Count > 0)
                    {
                        rs = list.Count;
                        decimal[] sz = new decimal[rs];

                        decimal sumovertime = 0;
                        // decimal weekovertime = 0;
                        //排除无打卡记录人
                        int wdkjl = 0;
                        for (int i = 0; i < list.Count; i++)
                        {
                            sb.Append("<tr >");
                            if (i == 0)
                            {
                                sb.Append("<td align=\"center\" rowspan='" + (list.Count + 1) + "' >" + item["unit_Name"] + "</td>");
                            }                          

                            decimal overtime = 0;
                            string beizhu = "";
                            string iswdkjl = "";
                            decimal zjbovertime = 0m;
                            string colorStr_overtime = "", colorStr_weektime = "";
                           
                            TG.Model.cm_MonthSummaryHis his_mem = null;
                            if (his_list != null && his_list.Count > 0)
                            {
                                his_mem = his_list.Where(h => h.mem_id == list[i].mem_ID).OrderByDescending(h => h.ID).FirstOrDefault();
                            }
                            //人员数据
                            if (his_mem != null)
                            {
                                overtime = Convert.ToDecimal(his_mem.overtime);
                                zjbovertime = Convert.ToDecimal(his_mem.weekovertime);
                                beizhu = his_mem.content;
                                iswdkjl = his_mem.isiswdkjl;
                                if (his_mem.isiswdkjl=="s")
                                {
                                    wdkjl = wdkjl + 1;
                                }
                                
                            }
                            else
                            {
                                //记录上次未打卡记录人数
                                int oldwdkjl = wdkjl;

                                overtime = GetOverTime(holi_list, pas_list, dt_time, this.drp_year.SelectedValue, this.drp_month.SelectedValue, Convert.ToInt32(list[i].mem_ID), list[i].mem_Name, starttime, endtime, ref wdkjl);

                                 zjbovertime = overtime / decimal.Parse("4.28");

                                //判断此人是否未打卡记录
                                if (wdkjl > oldwdkjl)
                                {
                                    iswdkjl = "s";
                                }

                                //手动修改加班
                                if (overtime_list != null && overtime_list.Count > 0)
                                {
                                    //加班
                                    var data_model = overtime_list.Where(d => d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        overtime = data_model.dataValue;
                                        zjbovertime = overtime / decimal.Parse("4.28");
                                        colorStr_overtime = " style='background-color:yellow;'";
                                    }
                                    //周加班
                                    data_model = overtime_list.Where(d => d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "weektime").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        zjbovertime = data_model.dataValue;
                                        colorStr_weektime = " style='background-color:yellow;'";
                                    }

                                    //提醒
                                    data_model = overtime_list.Where(d => d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "beizhu").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        beizhu = data_model.dataContent;
                                    }
                                }
                            }
                            //周加班大于20字体变红
                            string sizecolor = "";
                            if (zjbovertime > 20)
                            {
                                sizecolor = " style='color:red' ";

                                if (colorStr_overtime != "")
                                {
                                    colorStr_overtime = " style='background-color:yellow;color:red;'";
                                }
                                else
                                {
                                    colorStr_overtime = sizecolor;
                                }

                                if (colorStr_weektime != "")
                                {
                                    colorStr_weektime = " style='background-color:yellow;color:red;'";
                                }
                                else
                                {
                                    colorStr_weektime = sizecolor;
                                }
                            }
                           
                            sb.Append("<td align=\"center\" " + sizecolor + ">" + row + "</td>");
                            sb.Append("<td align=\"center\" " + sizecolor + " iswdkjl='" + iswdkjl + "' mem_id='" + list[i].mem_ID + "' mem_unitid='" + list[i].mem_Unit_ID + "'>" + list[i].mem_Name + "</td>");

                            sb.Append("<td align=\"center\" " + colorStr_overtime + " rel=" + list[i].mem_ID + " datatype='overtime' value='" + overtime.ToString("f1") + "'>" + overtime.ToString("f1") + "</td>");
                            sb.Append("<td align=\"center\" " + colorStr_weektime + " rel=" + list[i].mem_ID + " datatype='weektime' value='" + zjbovertime.ToString("f1") + "'>" + (zjbovertime).ToString("f1") + "</td>");
                            sb.Append("<td align=\"center\" ><input type='text' memid='" + list[i].mem_ID + "'  datatype='beizhu' value='" + beizhu + "' class='form-control input-sm' style='width:100px;'/></td>");
                            sb.Append("</tr>");
                            sumovertime += overtime;
                            weekovertime += zjbovertime;
                            row++;

                            //部门经理
                            if (dt_list.Rows[i]["RoleIdMag"].ToString() == "5" || dt_list.Rows[i]["RoleIdMag"].ToString() == "44")
                            {
                                managertime = managertime + zjbovertime;
                            }
                            sz[i] = zjbovertime;

                        }
                        //参与计算人数，排除无打卡记录人
                        cyjsr = list.Count - wdkjl;
                        #region 月分析
                        if (cyjsr > 0)
                        {
                            //平均值
                            avgtime = (cyjsr == 0 ? 0 : (weekovertime / cyjsr));
                        }

                        //按参与计算人数
                        //向上取整数
                        double dd = Math.Ceiling((double)cyjsr / (double)2);
                        int bb = Convert.ToInt32(dd);
                        pjgs = cyjsr % 2;
                        //从大到小排序
                        sz = sort(sz);
                        decimal gspjz = Math.Round(avgtime, 1);
                        for (int i = 0; i < cyjsr; i++)
                        {
                            //取得中间值
                            //奇数 中间位置的数据
                            if (pjgs > 0 && (i + 1) == bb)
                            {
                                middletime = sz[i];
                            }
                            else //偶数
                            {
                                //中间位置的两个数值的算术平均数
                                if ((i + 1) == bb)
                                {
                                    //得到中间数的第一个
                                    middletime = sz[i];
                                    //得到中间数的第二个
                                    middletime = (middletime + sz[(i + 1)]) / 2;
                                }
                            }

                            //不包含周加班为0的人员
                            // if (sz[i] > 0)
                            //  {
                            sumbztime = sumbztime + (sz[i] - gspjz) * (sz[i] - gspjz);
                            //   }
                        }
                        //标准差
                        if (cyjsr > 1)
                        {
                            sumbztime = sumbztime / (cyjsr - 1);
                            standardtime = Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(sumbztime)));
                        }
                        //部门经理
                        if (Math.Round(avgtime, 1) > 0)
                        {
                            //经理系数
                            managerxs = Math.Round(managertime, 1) / Math.Round(avgtime, 1) * 100;
                            //标准差系数
                            standardxs = (Math.Round(standardtime, 1) / Math.Round(avgtime, 1) * 100);
                        }
                        //最大值
                        maxtime = sz[0];
                        //最小值
                        if (cyjsr > 0)
                        {
                            mintime = sz[(cyjsr - 1)];
                        }
                        #endregion
                        sb.Append("<tr style='font-weight:bold;' hs='" + (list.Count + 1) + "'>");
                        sb.Append("<td align=\"center\" >合计</td>");
                        sb.Append("<td align=\"center\" ></td>");
                        sb.Append("<td align=\"center\" >" + sumovertime.ToString("f1") + "</td>");
                        sb.Append("<td align=\"center\" >" + (cyjsr == 0 ? "0" : (weekovertime / cyjsr).ToString("f1")) + "</td>");
                        sb.Append("<td align=\"center\" ></td>");
                        sb.Append("</tr>");

                    }

                    #region  月分析
                    sb2.Append("<Td>");
                    sb2.Append("<table class=\"table table-bordered table-responsive table-hover\" style=\"width:250px; margin: 0 auto; margin-bottom: 0px;\">");
                    sb2.Append("<tr><td colspan=\"2\" style=\"background-color:#ffffcc;text-align: center;\">" + item["unit_Name"] + "</td></tr>");
                    sb2.Append("<tr style=\"background-color:#ffffcc; text-align:center;\"> <td style=\"width:150px;\">项目</td><td style=\"width:100px;\">统计量</td></tr>");
                    sb2.Append("<Tr><Td>参与计算人数</Td><Td style=\"text-align:right;\">" + cyjsr + "</Td></Tr>");
                    sb2.Append("<Tr><Td><b>平均数</b></Td><Td style=\"text-align:right;\"><b>" + Math.Round(avgtime, 1) + "</b></Td></Tr>");
                    sb2.Append("<Tr><Td>中位数</Td><Td style=\"text-align:right;\">" + middletime.ToString("f1") + "</Td></Tr>");
                    sb2.Append("<Tr><Td>最大值</Td><Td style=\"text-align:right;\">" + maxtime.ToString("f1") + "</Td></Tr>");
                    sb2.Append("<Tr><Td>最小值</Td><Td style=\"text-align:right;\">" + mintime.ToString("f1") + "</Td></Tr>");
                    sb2.Append("<Tr><Td>极差</Td><Td style=\"text-align:right;\">" + (maxtime - mintime).ToString("f1") + "</Td></Tr>");
                    sb2.Append("<Tr><Td>标准差</Td><Td style=\"text-align:right;\">" + standardtime.ToString("f1") + "</Td></Tr>");
                    sb2.Append("<Tr><Td><b>标准差系数</b></Td><Td style=\"text-align:right;\"><b>" + standardxs.ToString("f1") + "%</b></Td></Tr>");
                    sb2.Append("<Tr><Td><b>部门经理系数</b></Td><Td style=\"text-align:right;\"><b>" + managerxs.ToString("f1") + "%</b></Td></Tr>");
                    sb2.Append("</table>");
                    sb2.Append("</Td>");
                    #endregion
                }
                sb2.Append("</tr>");
            }
            else
            {
                sb.Append("<tr><td colspan='19' style='color:red;'>没有数据！</td></tr>");
            }


            lithtml.Text = sb.ToString();
            lithtml2.Text = sb2.ToString();
        }


        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {
            BindData();
        }
        protected void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt = BindWhere("");
            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/MonthSummary.xls", this.drp_unit.SelectedItem.Text.Trim());
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }

        protected void btn_allexport_Click(object sender, EventArgs e)
        {
            //排除离职人员
            DataTable dt = BindWhere("all");

            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/MonthSummary.xls", "全院");
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }
        private void ExportDataToExcel(DataTable dt, string modelPath, string pathname)
        {


            string drpyear = this.drp_year.SelectedValue;
            string drpmonth = this.drp_month.SelectedValue;
            var nextyear = Convert.ToInt32(drpyear);
            var nextmonth = (Convert.ToInt32(drpmonth) - 1);
            if (drpmonth == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) - 1);
                nextmonth = 12;
            }

            pathname = "【" + pathname.Trim() + "】月汇总" + nextyear + "." + nextmonth + ".16-" + drpyear + "." + drpmonth + ".15";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //   style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //   style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 10;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体


            //背景颜色样式
            ICellStyle style3 = wb.CreateCellStyle(); ;
            style3.VerticalAlignment = VerticalAlignment.CENTER;
            style3.WrapText = true;
            font2.Color = HSSFColor.RED.index;
            //   style3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            // style3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style3.FillPattern = FillPatternType.SOLID_FOREGROUND;
            // style3.FillForegroundColor = HSSFColor.PINK.index;
            style3.SetFont(font2);

            //背景颜色样式
            ICellStyle style4 = wb.CreateCellStyle();
            style4.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style4.FillForegroundColor = HSSFColor.YELLOW.index;
            style4.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style4.VerticalAlignment = VerticalAlignment.CENTER;
            style4.WrapText = true;
            style4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;


            //背景颜色样式
            ICellStyle style5 = wb.CreateCellStyle();
            style5.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style5.FillForegroundColor = HSSFColor.YELLOW.index;
            style5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.SetFont(font2);



            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            IRow dataRowTitle = ws.GetRow(0);
            dataRowTitle.Height = 30 * 20;
            //ws.SetColumnWidth(2, 25 * 256);         
            ICell acell21 = dataRowTitle.GetCell(0);
            acell21.SetCellValue(pathname);
            // CellRangeAddress cellRangeAddress2 = new CellRangeAddress(1, 1, 0, 1);
            // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            //ws.AddMergedRegion(cellRangeAddress2);

            DateTime starttime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
            DateTime endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-15");
            //不算考勤
            string aa = ConfigurationManager.AppSettings["NoWork"];
            string nowork = "'" + (aa.Replace(",", "','")) + "'";

            //获取锁定数据
            List<TG.Model.cm_MonthSummaryHis> his_list = new TG.BLL.cm_MonthSummaryHis().GetModelList(" dataDate='" + drpyear + "-" + drpmonth.PadLeft(2, '0') + "'");
            //得到所有节假日
            List<TG.Model.cm_HolidayConfig> holi_list = new TG.BLL.cm_HolidayConfig().GetModelList("");
            //后台设置上班时间
            List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList("");
            //申请加班记录
            DataTable dt_time = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype in ('travel','gomeet','addwork','forget') ").Tables[0];
            //加班统计手动修改数据
            List<TG.Model.cm_ApplyStatisData> overtime_list = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='MonthSummary' order by id desc");


            int row = 1;
            int hjs = 0;
            foreach (DataRow item in dt.Rows)
            {
                int rs = 0, pjgs = 0, cyjsr = 0;
                //平均值，中位值，最小值，最大值，标准差,标准差系数,部门经理系数
                decimal avgtime = 0, middletime = 0, mintime = 0, maxtime = 0, standardtime = 0, standardxs = 0, managerxs = 0;
                decimal weekovertime = 0, sumbztime = 0, managertime = 0;//标准差总值
                //把钟志宏添加到结构部 
                string where = "";
                if (item["unit_ID"].ToString() == "237")
                {
                    where = " m.mem_ID=1445 or ";
                }
                //循环人员
                DataTable dt_list = TG.DBUtility.DbHelperSQL.Query("select m.*,mr.RoleIdMag from tg_member m left join tg_memberRole mr on m.mem_ID=mr.mem_Id where " + where + " (mr.mem_Unit_ID=" + item["unit_ID"] + " and mem_Name not in (" + nowork + ") and (mem_isFired=0 or m.mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + starttime.ToString("yyyy-MM-dd") + "'))) order by mem_Order asc,m.mem_ID asc").Tables[0];
                // List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(where + " (mem_id in (select mem_Id  from tg_memberRole where mem_Unit_ID=" + item["unit_ID"] + ") and mem_Name not in (" + nowork + ") and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + starttime.ToString("yyyy-MM-dd") + "'))) order by mem_Order asc,mem_ID asc");

                List<TG.Model.tg_member> list = new TG.BLL.tg_member().DataTableToList(dt_list);
                if (list != null && list.Count > 0)
                {
                    rs = list.Count;
                    decimal[] sz = new decimal[rs];

                    decimal sumovertime = 0;
                    // decimal weekovertime = 0;
                    //排除无打卡记录人
                    int wdkjl = 0;
                    for (int i = 0; i < list.Count; i++)
                    {
                        var dataRow = ws.GetRow((row + 1 + hjs));//第一行
                        if (dataRow == null)
                            dataRow = ws.CreateRow((row + 1 + hjs));//生成行


                        if (i == 0)
                        {
                            ICell cell0 = dataRow.GetCell(0);
                            if (cell0 == null)
                                cell0 = dataRow.CreateCell(0);

                            cell0.SetCellValue(item["unit_Name"].ToString().Trim());
                            CellRangeAddress cellRangeAddress2 = new CellRangeAddress((row + 1 + hjs), (row + 1 + hjs + list.Count), 0, 0);
                            // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
                            ws.AddMergedRegion(cellRangeAddress2);


                        }
                        decimal overtime = 0;
                        string beizhu = "";                       
                        string iswdkjl = "";
                        decimal zjbovertime = 0m;
                        string colorStr_overtime = "", colorStr_weektime = "";

                        TG.Model.cm_MonthSummaryHis his_mem = null;
                        if (his_list != null && his_list.Count > 0)
                        {
                            his_mem = his_list.Where(h => h.mem_id == list[i].mem_ID).OrderByDescending(h => h.ID).FirstOrDefault();
                        }
                        //人员数据
                        if (his_mem != null)
                        {
                            overtime = Convert.ToDecimal(his_mem.overtime);
                            zjbovertime = Convert.ToDecimal(his_mem.weekovertime);
                            beizhu = his_mem.content;
                            iswdkjl = his_mem.isiswdkjl;
                            if (his_mem.isiswdkjl == "s")
                            {
                                wdkjl = wdkjl + 1;
                            }
                        }
                        else
                        {

                            //记录上次未打卡记录人数
                            int oldwdkjl = wdkjl;

                        overtime = GetOverTime(holi_list, pas_list, dt_time, this.drp_year.SelectedValue, this.drp_month.SelectedValue, Convert.ToInt32(list[i].mem_ID), list[i].mem_Name, starttime, endtime, ref wdkjl);

                         zjbovertime = overtime / decimal.Parse("4.28");
                         //判断此人是否未打卡记录
                         if (wdkjl > oldwdkjl)
                         {
                             iswdkjl = "s";
                         }
                        //手动修改加班
                        if (overtime_list != null && overtime_list.Count > 0)
                        {
                            //加班
                            var data_model = overtime_list.Where(d => d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                overtime = data_model.dataValue;
                                zjbovertime = overtime / decimal.Parse("4.28");
                                colorStr_overtime = " style='background-color:yellow;'";
                            }
                            //周加班
                            data_model = overtime_list.Where(d => d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "weektime").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                zjbovertime = data_model.dataValue;
                                colorStr_weektime = " style='background-color:yellow;'";
                            }

                            //提醒
                            data_model = overtime_list.Where(d => d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "beizhu").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                beizhu = data_model.dataContent;
                            }
                        }
                        }
                        //周加班大于20字体变红
                        string sizecolor = "";
                        if (zjbovertime > 20)
                        {
                            sizecolor = " style='color:red' ";

                            if (colorStr_overtime != "")
                            {
                                colorStr_overtime = " style='background-color:yellow;color:red;'";
                            }
                            else
                            {
                                colorStr_overtime = sizecolor;
                            }

                            if (colorStr_weektime != "")
                            {
                                colorStr_weektime = " style='background-color:yellow;color:red;'";
                            }
                            else
                            {
                                colorStr_weektime = sizecolor;
                            }
                        }
                        //第一行
                        ICell cell = dataRow.GetCell(1);
                        if (cell == null)
                            cell = dataRow.CreateCell(1);
                        cell.SetCellValue(row);
                        if (sizecolor.Contains("red"))
                        {
                            cell.CellStyle = style3;
                        }
                        //第二行
                        cell = dataRow.GetCell(2);
                        if (cell == null)
                            cell = dataRow.CreateCell(2);
                        cell.SetCellValue(list[i].mem_Name);
                        if (sizecolor.Contains("red"))
                        {
                            cell.CellStyle = style3;
                        }
                        //第三行
                        cell = dataRow.GetCell(3);
                        if (cell == null)
                            cell = dataRow.CreateCell(3);
                        cell.SetCellValue(overtime.ToString("f1"));
                        if (colorStr_overtime.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                            if (sizecolor.Contains("red"))
                            {
                                //黄色加字体红色
                                cell.CellStyle = style5;
                            }
                        }
                        else
                        {
                            if (sizecolor.Contains("red"))
                            {
                                cell.CellStyle = style3;
                            }

                        }
                        //第四行
                        cell = dataRow.GetCell(4);
                        if (cell == null)
                            cell = dataRow.CreateCell(4);
                        cell.SetCellValue(zjbovertime.ToString("f1"));
                        if (colorStr_weektime.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                            if (sizecolor.Contains("red"))
                            {
                                //黄色加字体红色
                                cell.CellStyle = style5;
                            }
                        }
                        else
                        {
                            if (sizecolor.Contains("red"))
                            {
                                cell.CellStyle = style3;
                            }

                        }
                        //第五行
                        cell = dataRow.GetCell(5);
                        if (cell == null)
                            cell = dataRow.CreateCell(5);
                        cell.SetCellValue(beizhu);

                        sumovertime += overtime;
                        weekovertime += zjbovertime;

                        row++;

                        //部门经理
                        if (dt_list.Rows[i]["RoleIdMag"].ToString() == "5" || dt_list.Rows[i]["RoleIdMag"].ToString() == "44")
                        {
                            managertime = managertime + zjbovertime;
                        }
                        sz[i] = zjbovertime;

                    }
                    //参与计算人数，排除无打卡记录人
                    cyjsr = list.Count - wdkjl;
                    #region 月分析
                    if (cyjsr > 0)
                    {
                        //平均值
                        avgtime = (cyjsr == 0 ? 0 : (weekovertime / cyjsr));
                    }
                    //按参与计算人数
                    //向上取整数
                    double dd = Math.Ceiling((double)cyjsr / (double)2);
                    int bb = Convert.ToInt32(dd);
                    pjgs = cyjsr % 2;
                    //从大到小排序
                    sz = sort(sz);
                    decimal gspjz = Math.Round(avgtime, 1);
                    for (int i = 0; i < cyjsr; i++)
                    {
                        //取得中间值
                        //奇数 中间位置的数据
                        if (pjgs > 0 && (i + 1) == bb)
                        {
                            middletime = sz[i];
                        }
                        else //偶数
                        {
                            //中间位置的两个数值的算术平均数
                            if ((i + 1) == bb)
                            {
                                //得到中间数的第一个
                                middletime = sz[i];
                                //得到中间数的第二个
                                middletime = (middletime + sz[(i + 1)]) / 2;
                            }
                        }

                        //不包含周加班为0的人员
                        // if (sz[i] > 0)
                        //  {
                        sumbztime = sumbztime + (sz[i] - gspjz) * (sz[i] - gspjz);
                        //   }
                    }
                    //标准差
                    if (cyjsr > 1)
                    {
                        sumbztime = sumbztime / (cyjsr - 1);
                        standardtime = Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(sumbztime)));
                    }
                    //部门经理
                    if (Math.Round(avgtime, 1) > 0)
                    {
                        //经理系数
                        managerxs = Math.Round(managertime, 1) / Math.Round(avgtime, 1) * 100;
                        //标准差系数
                        standardxs = (Math.Round(standardtime, 1) / Math.Round(avgtime, 1) * 100);
                    }
                    //最大值
                    maxtime = sz[0];
                    //最小值
                    if (cyjsr > 0)
                    {
                        mintime = sz[(cyjsr - 1)];
                    }
                    #endregion
                    //合计
                    var enddataRow = ws.GetRow((row + 1 + hjs));//第一行
                    if (enddataRow == null)
                        enddataRow = ws.CreateRow((row + 1 + hjs));//生成行

                    ICell endcell = enddataRow.GetCell(1);
                    if (endcell == null)
                        endcell = enddataRow.CreateCell(1);

                    endcell.SetCellValue("合计");

                    endcell = enddataRow.GetCell(2);
                    if (endcell == null)
                        endcell = enddataRow.CreateCell(2);
                    endcell.SetCellValue("");

                    endcell = enddataRow.GetCell(3);
                    if (endcell == null)
                        endcell = enddataRow.CreateCell(3);
                    endcell.SetCellValue(sumovertime.ToString("f1"));

                    endcell = enddataRow.GetCell(4);
                    if (endcell == null)
                        endcell = enddataRow.CreateCell(4);
                    endcell.SetCellValue((cyjsr == 0 ? "0" : (weekovertime / cyjsr).ToString("f1")));

                    endcell = enddataRow.GetCell(5);
                    if (endcell == null)
                        endcell = enddataRow.CreateCell(5);
                    endcell.SetCellValue("");

                    hjs = hjs + 1;
                }

                #region  月分析
                //第一行部门名称
                var zjdataRow = ws.GetRow(((hjs - 1) * 12) + 1);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 1);//生成行               
                ICell zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue(item["unit_Name"].ToString());
                CellRangeAddress cellRangeAddress7 = new CellRangeAddress((((hjs - 1) * 12) + 1), (((hjs - 1) * 12) + 1), 7, 8);
                ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress7, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
                ws.AddMergedRegion(cellRangeAddress7);
                zjcell.CellStyle = style4;

                //第二行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 2);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 2);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("项目");
                zjcell.CellStyle = style4;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue("统计量");
                zjcell.CellStyle = style4;

                //第三行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 3);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 3);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("参与计算人数");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(cyjsr);
                zjcell.CellStyle = style2;
                //第四行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 4);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 4);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("平均数");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(Math.Round(avgtime, 1).ToString());
                zjcell.CellStyle = style2;
                //第五行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 5);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 5);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("中位数");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(middletime.ToString("f1"));
                zjcell.CellStyle = style2;
                //第六行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 6);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 6);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("最大值");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(maxtime.ToString("f1"));
                zjcell.CellStyle = style2;
                //第七行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 7);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 7);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("最小值");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(mintime.ToString("f1"));
                zjcell.CellStyle = style2;
                //第八行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 8);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 8);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("极差");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue((maxtime - mintime).ToString("f1"));
                zjcell.CellStyle = style2;
                //第九行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 9);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 9);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("标准差");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(standardtime.ToString("f1"));
                zjcell.CellStyle = style2;
                //第十行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 10);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 10);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("标准差系数");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(standardxs.ToString("f1") + "%");
                zjcell.CellStyle = style2;
                //第十一行
                zjdataRow = ws.GetRow(((hjs - 1) * 12) + 11);//第一行
                if (zjdataRow == null)
                    zjdataRow = ws.CreateRow(((hjs - 1) * 12) + 11);//生成行
                zjcell = zjdataRow.GetCell(7);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(7);
                zjcell.SetCellValue("部门经理系数");
                zjcell.CellStyle = style2;

                zjcell = zjdataRow.GetCell(8);
                if (zjcell == null)
                    zjcell = zjdataRow.CreateCell(8);
                zjcell.SetCellValue(managerxs.ToString("f1") + "%");
                zjcell.CellStyle = style2;

                #endregion
            }



            //写入Session
            Session["SamplePictures"] = true;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();

            string sqlwhere = "";
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                sqlwhere = " unit_ParentID<>0  and unit_ID not in (" + NotShowUnitList + ") ";
            }
            else
            {
                sqlwhere = " unit_ID=" + UserUnitNo;
            }
            sqlwhere = sqlwhere + " and unit_ID<>230 order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID) asc,unit_ID asc ";
            var unitList = bll_unit.GetModelList(sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");
            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;

            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
                this.isapplymanager.Value = base.IsApplyManager.ToString();
            }
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }

            for (int i = 1; i <= 12; i++)
            {
                this.drp_month.Items.Add(i.ToString());
            }

        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
            //选择部门第一个
            //this.drp_unit.Items[0].Selected = true;
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人，只有管理员看到全部
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND t.unit_ID =" + UserUnitNo + " ");
            }
            //部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND t.unit_ID =" + UserUnitNo + " ");
            }
        }


    }

}