﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Configuration;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace TG.Web.Calendar
{
    public partial class UnitOvertimeMoney : PageBase
    {
        //配置人
        private string[] SpecialPersonList
        {
            get
            {
                string SpecialPerson = ConfigurationManager.AppSettings["SpecialPerson"];
                return SpecialPerson.Split('|');
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                BindData();
            }

        }
        //根据条件查询
        private DataTable BindWhere()
        {
            string drpyear = this.drp_year.SelectedValue;
            string drpmonth = this.drp_month.SelectedValue;
            var nextyear = Convert.ToInt32(drpyear);
            var nextmonth = (Convert.ToInt32(drpmonth) - 1);
            if (drpmonth == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) - 1);
                nextmonth = 12;
            }
            DateTime outtime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
            StringBuilder sb = new StringBuilder(" 1=1 ");

            //部门
            if (this.drp_unit.SelectedIndex > 0)
            {
                sb.Append(" AND mr.mem_Unit_ID=" + this.drp_unit.SelectedValue + "");
            }
            //排除离职人员
            sb.Append(" and (mem_isFired=0 or m.mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + outtime.ToString("yyyy-MM-dd") + "'))");
            TG.BLL.tg_member bllMem = new TG.BLL.tg_member();
            //检查权限
            GetPreviewPowerSql(ref sb);

            //   int count = int.Parse(bllMem.GetRecordCount(sb.ToString()).ToString());
            //所有记录数
            // this.AspNetPager1.RecordCount = count;
            //排除张兵，张翠珍、张维	

            //不算考勤
            string aa = ConfigurationManager.AppSettings["NoWork"];
            string nowork = "'" + (aa.Replace(",", "','")) + "'";
            sb.Append(" and m.mem_Name not in (" + nowork + ") ");

            //排除部门经理和经理办公室
            sb.Append(" and mr.RoleIdMag<>5 and mr.RoleIdMag<>44 and mr.mem_Unit_ID<>230 and mr.mem_id not in (select mem_id from cm_UnitManagerOverTime) ");
            //取得查询数据
            DataSet ds = new DataSet();
            string sql = "select m.*,mr.mem_Unit_ID as Unit_ID,(select unit_name from tg_unit where unit_id=mr.mem_Unit_ID) as unitname,(select unit_Order from tg_unitExt where unit_ID=mr.mem_Unit_ID) as unitorder,isnull((select mem_GongZi from tg_memberExtInfo where mem_ID=m.mem_ID),0) as gongzi from tg_member m left join tg_memberRole mr on m.mem_ID=mr.mem_Id where " + sb.ToString() + " order by unitorder asc,mem_Order asc,m.mem_ID asc";
            ds = TG.DBUtility.DbHelperSQL.Query(sql);
            //明细
            DataTable dt = ds.Tables[0];

            return dt;
        }
        //绑定数据
        private void BindData()
        {
            DataTable dt = BindWhere();
            if (dt != null && dt.Rows.Count > 0)
            {
                CreateTable(dt);
            }
        }
        List<TG.Model.cm_HolidayConfig> list = new List<TG.Model.cm_HolidayConfig>();
        //考勤
        string towork = "09:00", offwork = "17:30";
        public void CreateTable(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();

            string drpyear = this.drp_year.SelectedValue;
            string drpmonth = this.drp_month.SelectedValue;
            var nextyear = Convert.ToInt32(drpyear);
            var nextmonth = (Convert.ToInt32(drpmonth) - 1);
            if (drpmonth == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) - 1);
                nextmonth = 12;
            }

            DateTime all_starttime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
            DateTime all_endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-15");

            //合计
            decimal sum_sum = 0;//请假总和  月工资
            //decimal sum_late = 0;//迟到 
            decimal sum_over = 0;//加班  加班
            //  decimal sum_year = 0; //年假                  
            decimal sum_marry = 0;//婚假  计算额
            //  decimal sum_mater = 0;//产/陪产假
            //  decimal sum_die = 0;//丧假
            //  decimal sum_leave = 0;//事假
            //  decimal sum_sick = 0; //病假
            decimal sum_dpe = 0;//调配额

            //获取锁定数据
            List<TG.Model.cm_ApplyStatisDetailHis> his_list = new TG.BLL.cm_ApplyStatisDetailHis().GetModelList(" (ISDATE(dataDate)=1 and dataDate between '" + all_starttime.ToString("yyyy-MM-dd") + "' and '" + all_endtime.ToString("yyyy-MM-dd") + "')");

            //打卡数据
            DataTable datatable = GetCurrentMonth((drpyear + drpmonth.PadLeft(2, '0')), "1");

            //后台设置上班时间
            List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_year=" + drpyear + " and attend_month=" + drpmonth);
            //得到所有节假日日期。
            list = new TG.BLL.cm_HolidayConfig().GetModelList("");

            //筛选当前年，小于当前月之前的，部门活动申请数据          
            string unitwhere = string.Format(" and convert(varchar(10),starttime,120)>='{0}' and convert(varchar(10),endtime,120)<'{1}' and applytype='depart' ", ((Convert.ToInt32(drpyear) - 1) + "-12-16"), (nextyear + "-" + nextmonth.ToString().PadLeft(2, '0') + "-16"));
            DataTable unit_dt = new TG.BLL.cm_ApplyInfo().GetApplyList(unitwhere).Tables[0];

            //部门人系数
            List<TG.Model.cm_ApplyUnitOvertime> list_auo = new TG.BLL.cm_ApplyUnitOvertime().GetModelList(" OverYear=" + drpyear + " and OverMonth=" + drpmonth + " ");

            //手动修改数据
            List<TG.Model.cm_ApplyStatisData> list_data = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='StatisDetail' order by id desc");

            if (dt != null && dt.Rows.Count > 0)
            {

                int row = 1;
                int old_unitid = 0;
                int xtindex = 1;
                int cls = 0;
                foreach (DataRow item in dt.Rows)
                {
                    sb.Append("<tr >");
                    if (old_unitid != Convert.ToInt32(item["Unit_ID"]))
                    {
                        sum_sum = 0;
                        //  sum_late = 0;//迟到 
                        sum_over = 0;//加班
                        //  sum_year = 0; //年假                  
                        sum_marry = 0;//婚假
                        //   sum_mater = 0;//产/陪产假
                        //   sum_die = 0;//丧假
                        //   sum_leave = 0;//事假
                        //   sum_sick = 0; //病假
                        sum_dpe = 0;
                        xtindex = 1;
                        old_unitid = Convert.ToInt32(item["Unit_ID"]);
                        cls = dt.Select("Unit_ID=" + old_unitid).Count();
                        sb.Append("<td align=\"center\" rowspan='" + (cls + 1) + "'>" + item["unitname"] + "</td>");
                    }


                    //获取某人上下班时间
                    if (pas_list != null && pas_list.Count > 0)
                    {
                        List<TG.Model.cm_PersonAttendSet> pas_list_mem = pas_list.Where(p => p.mem_ID == Convert.ToInt32(item["mem_ID"])).ToList();
                        if (pas_list_mem != null && pas_list_mem.Count > 0)
                        {
                            towork = pas_list_mem[0].ToWork;
                            offwork = pas_list_mem[0].OffWork;
                        }
                        else
                        {
                            towork = "09:00";
                            offwork = "17:30";
                        }
                    }
                    //筛选当前年当前人，小于当前月之前的，部门活动申请数据总小时
                    decimal sumunittime = 0;
                    if (unit_dt != null && unit_dt.Rows.Count > 0)
                    {
                        // sumunittime = unit_dt.AsEnumerable().Where(s => s.Field<int>("adduser") == Convert.ToInt32(item["mem_ID"])).Sum(s => s.Field<decimal>("totaltime"));
                        DataTable unit_dt2 = new DataView(unit_dt) { RowFilter = "adduser=" + item["mem_ID"] + "" }.ToTable();
                        if (unit_dt2 != null && unit_dt2.Rows.Count > 0)
                        {
                            //得到小于一天的申请
                            sumunittime = unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > 0 && s.Field<decimal>("totaltime") <= Convert.ToDecimal(7.5)).Sum(s => s.Field<decimal>("totaltime"));
                            //大于一天申请，按7.5算
                            sumunittime = sumunittime + ((unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > Convert.ToDecimal(7.5)).Count()) * Convert.ToDecimal(7.5));
                        }
                    }

                    string where = string.Format(" and adduser={0}", item["mem_ID"]);
                    DataTable dt_all = new TG.BLL.cm_ApplyInfo().GetApplyList(where).Tables[0];

                    //请假
                    decimal time_sum = 0;//请假总和
                    decimal time_late = 0;//迟到 
                    decimal time_over = 0;//加班
                    decimal time_year = 0; //年假                  
                    decimal time_marry = 0;//婚假
                    decimal time_mater = 0;//产/陪产假
                    decimal time_die = 0;//丧假
                    decimal time_leave = 0;//事假
                    decimal time_sick = 0; //病假

                    DateTime starttime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
                    DateTime endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-15");


                    //请假统计
                    for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddDays(1))
                    {
                        decimal day_late = 0;//迟到 
                        decimal day_over = 0;//加班
                        decimal day_year = 0; //年假                  
                        decimal day_marry = 0;//婚假
                        decimal day_mater = 0;//产/陪产假
                        decimal day_die = 0;//丧假
                        decimal day_leave = 0;//事假
                        decimal day_sick = 0; //病假
                        //小于当前时间
                        if (starttime <= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                        {
                            TG.Model.cm_ApplyStatisDetailHis his_mem = null;
                            if (his_list != null && his_list.Count > 0)
                            {
                                his_mem = his_list.Where(h => h.mem_id == Convert.ToInt32(item["mem_ID"]) && h.dataDate == starttime.ToString("yyyy-MM-dd")).OrderByDescending(h => h.ID).FirstOrDefault();
                            }
                            if (his_mem != null)
                            {
                                day_late = Convert.ToDecimal(his_mem.time_late);
                                day_over = Convert.ToDecimal(his_mem.time_over);
                                day_marry = Convert.ToDecimal(his_mem.time_marry);
                                day_mater = Convert.ToDecimal(his_mem.time_mater);
                                day_die = Convert.ToDecimal(his_mem.time_die);
                                day_leave = Convert.ToDecimal(his_mem.time_leave);
                                day_sick = Convert.ToDecimal(his_mem.time_sick);

                            }
                            else
                            {

                                //获取某人某年某月某日
                                List<TG.Model.cm_ApplyStatisData> list_data2_Detail = new List<TG.Model.cm_ApplyStatisData>();
                                if (list_data != null && list_data.Count > 0)
                                {
                                    list_data2_Detail = list_data.Where(asd => asd.mem_id == Convert.ToInt32(item["mem_ID"]) && asd.dataYear == starttime.Year && asd.dataMonth == starttime.Month && asd.dataDay == starttime.Day).ToList();
                                }

                                //上班时间
                                //   DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00");
                                //中午下班时间
                                DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50:00");
                                //中午上班时间
                                DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00:59");
                                //下午下班时间
                                //    DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");

                                //打卡统计
                                DateTime tomr_endtime = starttime;
                                DataTable dt_late = new DataTable();
                                if (datatable != null && datatable.Rows.Count > 0)
                                {

                                    dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + item["mem_Name"] + "'" }.ToTable();
                                    tomr_endtime = starttime;
                                }

                                //考勤申请记录
                                // string where = string.Format(" and adduser={0} and '{1}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) ", item["mem_ID"], starttime.ToString("yyyy-MM-dd"));
                                //  DataTable dt_all = new TG.BLL.cm_ApplyInfo().GetApplyList(where).Tables[0];
                                //截取年月日格式
                                int len = starttime.ToString().IndexOf(" ") + 1;
                                string temp = starttime.ToString().Substring(0, len);
                                DataTable dt_leave = new DataTable();
                                if (dt_all != null && dt_all.Rows.Count > 0)
                                {
                                    dt_leave = new DataView(dt_all) { RowFilter = "(((('" + starttime + "'>=starttime and '" + starttime + "'<=endtime) or '" + temp + "'=substring(Convert(starttime,'System.String'),1," + len + ") or substring(Convert(endtime,'System.String'),1," + len + ")='" + temp + "')) ) or ('" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00')" }.ToTable();
                                }



                                if (dt_leave != null && dt_leave.Rows.Count > 0)
                                {
                                    //加班离岗
                                    DataTable dt_addwork = new DataView(dt_leave) { RowFilter = "applytype='addwork' " }.ToTable();
                                    if (dt_addwork != null && dt_addwork.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dt_addwork.Rows)
                                        {
                                            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                            day_over = day_over - totaltime_over;
                                        }
                                    }
                                }

                                if (dt_late != null && dt_late.Rows.Count > 0)
                                {
                                    DataTable travel_yestoday = new DataTable();
                                    DataTable dt_apply = new DataTable();
                                    DataTable dt_flag = new DataTable();
                                    if (dt_leave != null && dt_leave.Rows.Count > 0)
                                    {
                                        //出差或外勤今天的0点到6,允许迟到半天
                                        travel_yestoday = new DataView(dt_leave) { RowFilter = "applytype in ('travel','gomeet','forget') and endtime<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();

                                        //申请记录统计
                                        dt_apply = new DataView(dt_leave) { RowFilter = "applytype not in ('addwork')" }.ToTable();
                                        if (dt_apply != null && dt_apply.Rows.Count > 0)
                                        {
                                            dt_flag = new DataView(dt_apply) { RowFilter = "endtime>'" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();

                                            #region 事假
                                            DataTable dt_leave1 = new DataView(dt_apply) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                            day_leave = day_leave + GetLeave(dt_leave1, starttime);
                                            //每年部门活动带薪时间1天（7.5小时），超过7.5小时按事假计算
                                            decimal unittime = 0;
                                            DataTable dt_temp = new DataView(dt_apply) { RowFilter = "applytype='depart' " }.ToTable();
                                            if (dt_temp != null && dt_temp.Rows.Count > 0)
                                            {
                                                //当天的部门活动小时
                                                unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                            }

                                            day_leave = day_leave + unittime;

                                            #endregion
                                            #region 病假
                                            DataTable dt_sick = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='病假'" }.ToTable();
                                            day_sick = day_sick + GetLeave(dt_sick, starttime);
                                            #endregion
                                            #region 加班
                                            tomr_endtime = starttime;
                                            DataTable dt_over = new DataView(dt_apply) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00'" }.ToTable();
                                            if (dt_over != null && dt_over.Rows.Count > 0)
                                            {
                                                foreach (DataRow dr in dt_over.Rows)
                                                {
                                                    decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                                    day_over = day_over + totaltime_over;
                                                }
                                            }


                                            #endregion
                                            #region 婚假
                                            DataTable dt_marry = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='婚假'" }.ToTable();
                                            day_marry = day_marry + GetLeave(dt_marry, starttime);
                                            #endregion
                                            #region 产/陪产假
                                            DataTable dt_mater = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='产/陪产假'" }.ToTable();
                                            day_mater = day_mater + GetLeave(dt_mater, starttime);
                                            #endregion
                                            #region 丧假
                                            DataTable dt_die = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='丧假'" }.ToTable();
                                            day_die = day_die + GetLeave(dt_die, starttime);
                                            #endregion
                                        }
                                    }
                                    if (!isListJQ(list, starttime))//排除节假日和周六日
                                    {
                                        #region 迟到
                                        DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                        DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                        DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                        DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                                        //出差和外勤申请记录
                                        string str = "";
                                        //判断申请记录是否存在,打卡记录只有一条或无数据或上午出差，下午出差时用到
                                        bool flag = false;
                                        if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                        {
                                            str = "bt";
                                        }
                                        if (dt_flag != null && dt_flag.Rows.Count > 0)
                                        {
                                            flag = true;
                                        }
                                        //迟到情况
                                        if ((shiji > houtai && shiji < houtaioff) || shiji > houtaioff)
                                        {
                                            //昨天下班打卡时间
                                            string yestoday = "";
                                            DateTime yes_time = starttime;
                                            yes_time = yes_time.AddDays(-1);
                                            DataTable dt_yestoday = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (yes_time.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + item["mem_Name"] + "'" }.ToTable();
                                            if (dt_yestoday != null && dt_yestoday.Rows.Count > 0)
                                            {
                                                yestoday = Convert.ToDateTime(dt_yestoday.Rows[(dt_yestoday.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm");
                                            }
                                            //已查询允许迟到半天
                                            if (str == "")
                                            {
                                                //截取年月日格式
                                                int yes_len = yes_time.ToString().IndexOf(" ") + 1;
                                                string yes_temp = yes_time.ToString().Substring(0, yes_len);
                                                travel_yestoday = new DataTable();
                                                if (dt_all != null && dt_all.Rows.Count > 0)
                                                {
                                                    travel_yestoday = new DataView(dt_all) { RowFilter = "applytype in ('travel','gomeet','forget') and starttime>='" + yes_time.ToString("yyyy-MM-dd") + " 06:00:00' and endtime<='" + yes_time.ToString("yyyy-MM-dd") + " 23:59:59'" }.ToTable();
                                                }
                                                //需要执行数据库
                                                //出差或外勤昨天22点到23:50,允许迟到半小时
                                                //string strwhere = string.Format(" and (applytype='travel' or applytype='gomeet') and '{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) and adduser={1} and starttime>='{2}' and endtime<'{3}'", yes_time.ToString("yyyy-MM-dd"), memid, (yes_time.ToString("yyyy-MM-dd") + " 22:00:00"), (yes_time.ToString("yyyy-MM-dd") + " 23:50:00"));
                                                //DataTable travel_yestoday = new TG.BLL.cm_ApplyInfo().GetApplyList(strwhere).Tables[0];
                                                if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                                {
                                                    //查询结束时间是否大于23点50分钟
                                                    DataTable temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 23:50:00'") }.ToTable();
                                                    if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                    {
                                                        str = "bt";
                                                    }
                                                    else
                                                    {
                                                        //查询结束时间是否大于22点00分钟
                                                        temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 21:50:00'") }.ToTable();
                                                        if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                        {
                                                            str = "bxs";
                                                        }
                                                    }
                                                }
                                            }

                                            //昨天加班到22：00，所以允许今天迟到半小时后,统计从9:30
                                            if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 21:50:00") && Convert.ToDateTime(yestoday) < Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bxs")
                                            {
                                                houtai = houtai.AddMinutes(30);
                                            }
                                            //昨天加班到23:50，所以允许迟到半天,统计从13：0:0
                                            else if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bt")
                                            {
                                                houtai = zwsbtime;
                                            }

                                            TimeSpan ts = shiji - houtai;
                                            if (ts.TotalMinutes > 3)
                                            {
                                                if (ts.TotalMinutes >= 4 && ts.TotalMinutes <= 14)//迟到
                                                {

                                                    day_late++;


                                                }
                                                else if (ts.TotalMinutes > 14 && ts.TotalMinutes < 30 && !flag) //迟到大于10分钟按0.5事假统计
                                                {

                                                    day_leave = day_leave + decimal.Parse("0.5");


                                                }
                                                else
                                                {

                                                    //没有申请记录
                                                    if (!flag)
                                                    {
                                                        if (shiji > houtaioff)
                                                        {
                                                            //下班时间-上班时间减去一小时
                                                            TimeSpan ts2 = houtaioff - houtai;
                                                            day_leave = day_leave + GetHours(ts2);
                                                            //上班时间不是下午1点。
                                                            if (houtai < zwsbtime)
                                                            {
                                                                day_leave = day_leave - 1;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //上班未上班, 正常上班时间9点、9点半
                                                            if (shiji >= zwxbtime && zwxbtime > houtai)
                                                            {
                                                                TimeSpan ts2 = zwxbtime - houtai;
                                                                day_leave = day_leave + GetHours(ts2);
                                                            }
                                                            else
                                                            {
                                                                //上午上班，打卡迟到大于30分钟
                                                                day_leave = day_leave + GetHours(ts);

                                                            }

                                                            //实际打卡时间大于下午上班时间, 正常上班时间9点、9点半
                                                            if (shiji > zwsbtime && zwxbtime > houtai)
                                                            {
                                                                TimeSpan ts3 = shiji - zwsbtime;
                                                                day_leave = day_leave + GetHours(ts3);
                                                            }
                                                        }
                                                    }

                                                }


                                                //超过5次后，按照0.5小时事假扣除
                                                if (day_late > 0 && (time_late + 1) > 5)
                                                {
                                                    day_leave = day_leave + decimal.Parse("0.5");
                                                }
                                            }
                                        }
                                        //早退                              
                                        if (dt_late.Rows.Count > 0 && shijioff < houtaioff && !flag)
                                        {
                                            DateTime zaotui = shijioff;
                                            //只有一条早上打卡记录,按上班时间计算
                                            if (shijioff < houtai)
                                            {
                                                zaotui = houtai;
                                            }
                                            //下班打卡时间是上午,中午下班时间-早退时间
                                            if (shijioff < zwxbtime)
                                            {
                                                if (dt_late.Rows.Count == 1)
                                                {
                                                    zaotui = houtai;
                                                    TimeSpan ts_sw = zwxbtime - zaotui;
                                                    day_leave = GetHours(ts_sw);
                                                    //超过5次后，按照0.5小时事假扣除
                                                    if (day_late > 0 && (time_late + 1) > 5)
                                                    {
                                                        day_leave = day_leave + decimal.Parse("0.5");
                                                    }
                                                }
                                                else
                                                {
                                                    TimeSpan ts_sw = zwxbtime - zaotui;
                                                    day_leave = day_leave + GetHours(ts_sw);
                                                }

                                            }

                                            //下班打卡时间是下午1点之前,就默认是下午1点
                                            if (shijioff < zwsbtime)
                                            {
                                                zaotui = zwsbtime;
                                            }

                                            TimeSpan ts2 = houtaioff - zaotui;
                                            decimal ztqj = GetHours(ts2);

                                            day_leave = day_leave + ztqj;

                                        }

                                        //下班时间+30分钟，算加班
                                        DateTime jiaban = houtaioff.AddMinutes(30);

                                        if (shiji > jiaban && dt_late.Rows.Count > 1)
                                        {
                                            int mine = shiji.Minute;
                                            if (mine > 0 && mine < 15)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                            }
                                            else if (mine > 30 && mine < 45)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                            }
                                            else
                                            {
                                                jiaban = shiji;
                                            }

                                        }

                                        //加班统计                       
                                        if (shijioff > jiaban)
                                        {
                                            decimal totaltime_forget = 0;
                                            //判断是否有未打卡申请记录
                                            tomr_endtime = starttime;
                                            if (dt_leave != null && dt_leave.Rows.Count > 0)
                                            {
                                                DataTable dt_over = new DataView(dt_leave) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                                if (dt_over != null && dt_over.Rows.Count > 0)
                                                {
                                                    totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                                }
                                            }
                                            //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                            if (totaltime_forget == 0)
                                            {
                                                day_over = day_over + GetOver(jiaban, shijioff);
                                            }

                                        }


                                        #endregion
                                    }
                                    else
                                    {
                                        //周六日，节假日加班
                                        if (dt_late != null && dt_late.Rows.Count > 0)
                                        {
                                            DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                            DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                            DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                            DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                                            //实际打卡时间
                                            int mine = shiji.Minute;
                                            DateTime jiaban = shiji;
                                            if (mine > 0 && mine < 15)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                            }
                                            else if (mine > 30 && mine < 45)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                            }

                                            decimal totaltime_forget = 0;
                                            //判断是否有未打卡申请记录
                                            tomr_endtime = starttime;
                                            if (dt_leave != null && dt_leave.Rows.Count > 0)
                                            {
                                                DataTable dt_over = new DataView(dt_leave) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                                if (dt_over != null && dt_over.Rows.Count > 0)
                                                {
                                                    totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                                }
                                            }
                                            //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                            if (totaltime_forget == 0)
                                            {
                                                day_over = day_over + GetOver(shiji, shijioff);
                                            }

                                            ////全天
                                            //if (shiji <= houtai && shijioff >= houtaioff)
                                            //{
                                            //    day_over = day_over + decimal.Parse("7.5");
                                            //}
                                            //else
                                            //{
                                            //    DateTime start = shiji;
                                            //    DateTime end = shijioff;
                                            //    //加班统计   
                                            //    if (shiji < houtai)
                                            //    {
                                            //        start = houtai;
                                            //    }
                                            //    else if (shiji >= zwxbtime && shiji <= zwsbtime)
                                            //    {
                                            //        shiji = zwsbtime;
                                            //    }
                                            //    if (shijioff > houtaioff)
                                            //    {
                                            //        end = houtaioff;
                                            //    }
                                            //    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
                                            //    {
                                            //        end = zwxbtime;
                                            //    }

                                            //    //都是上午打卡    
                                            //    if (end <= zwxbtime)
                                            //    {
                                            //        end = shijioff;
                                            //        day_over = day_over + GetOver(start, end);
                                            //    }
                                            //    else //下班打卡是下午
                                            //    {
                                            //        //上班打卡也是下午
                                            //        if (start >= zwsbtime)
                                            //        {
                                            //            day_over = day_over + GetOver(start, end);
                                            //        }
                                            //        else//上班打卡是上午
                                            //        {
                                            //            //上午加班时间
                                            //            day_over = day_over + GetOver(start, zwxbtime);
                                            //            //下午加班时间
                                            //            day_over = day_over + GetOver(zwsbtime, end);
                                            //        }
                                            //    }
                                            //}
                                            ////晚上加班
                                            //DateTime jiaban = houtaioff.AddMinutes(30);

                                            //if (shiji > jiaban)
                                            //{
                                            //    int mine = shiji.Minute;
                                            //    if (mine > 0 && mine < 15)
                                            //    {
                                            //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                            //    }
                                            //    else if (mine > 30 && mine < 45)
                                            //    {
                                            //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                            //    }
                                            //    else
                                            //    {
                                            //        jiaban = shiji;
                                            //    }

                                            //}

                                            ////加班统计                       
                                            //if (shijioff > jiaban)
                                            //{
                                            //    day_over = day_over + GetOver(jiaban, shijioff);
                                            //}

                                        }

                                    }
                                }
                                else
                                {
                                    DataTable dt_time = dt_leave;
                                    if (dt_time != null && dt_time.Rows.Count > 0)
                                    {
                                        if (!isListJQ(list, starttime))//排除节假日和周六日
                                        {
                                            #region  年假
                                            //DataTable dt_year = new DataView(dt_time) { RowFilter = "applytype='leave' and (reason='年假')" }.ToTable();
                                            //day_year = day_year + GetLeave(dt_year, starttime);
                                            #endregion
                                            #region 事假
                                            DataTable dt_leave1 = new DataView(dt_time) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                            day_leave = day_leave + GetLeave(dt_leave1, starttime);
                                            //每年部门活动带薪时间1天（7.5小时），超过7.5小时按事假计算
                                            decimal unittime = 0;
                                            DataTable dt_temp = new DataView(dt_time) { RowFilter = "applytype='depart' " }.ToTable();
                                            if (dt_temp != null && dt_temp.Rows.Count > 0)
                                            {
                                                //当天的部门活动小时
                                                unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                            }

                                            day_leave = day_leave + unittime;

                                            #endregion
                                            #region 病假
                                            DataTable dt_sick = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='病假'" }.ToTable();
                                            day_sick = day_sick + GetLeave(dt_sick, starttime);
                                            #endregion

                                            #region 婚假
                                            DataTable dt_marry = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='婚假'" }.ToTable();
                                            day_marry = day_marry + GetLeave(dt_marry, starttime);
                                            #endregion
                                            #region 产/陪产假
                                            DataTable dt_mater = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='产/陪产假'" }.ToTable();
                                            day_mater = day_mater + GetLeave(dt_mater, starttime);
                                            #endregion
                                            #region 丧假
                                            DataTable dt_die = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='丧假'" }.ToTable();
                                            day_die = day_die + GetLeave(dt_die, starttime);
                                            #endregion
                                        }

                                        #region 加班
                                        tomr_endtime = starttime;
                                        DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00'" }.ToTable();
                                        if (dt_over != null && dt_over.Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dt_over.Rows)
                                            {
                                                decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                                day_over = day_over + totaltime_over;
                                            }
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        if (!isListJQ(list, starttime))//排除节假日和周六日
                                        {
                                            //一天未打卡，无申请
                                            day_leave = day_leave + decimal.Parse("7.5");
                                        }
                                    }

                                }



                                //判断是否手动改过
                                if (list_data2_Detail != null && list_data2_Detail.Count > 0)
                                {
                                    var data_model = list_data2_Detail.Where(d => d.dataType == "late").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_late = data_model.dataValue;
                                        //超过5次后，按照0.5小时事假扣除
                                        if (day_late > 0 && (time_late + 1) > 5)
                                        {
                                            day_leave = day_leave + decimal.Parse("0.5");
                                        }
                                    }
                                     data_model = list_data2_Detail.Where(d => d.dataType == "leave").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_leave = data_model.dataValue;

                                    }

                                    data_model = list_data2_Detail.Where(d => d.dataType == "sick").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_sick = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_over = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "marry").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_marry = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "mater").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_mater = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "die").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_die = data_model.dataValue;

                                    }
                                    

                                }



                            }
                        }
                        time_late = time_late + day_late;
                        time_over = time_over + day_over;
                        //  time_year = time_year + day_year;
                        time_marry = time_marry + day_marry;
                        time_mater = time_mater + day_mater;
                        time_die = time_die + day_die;
                        time_leave = time_leave + day_leave;
                        time_sick = time_sick + day_sick;
                    }
                    //请假
                    time_sum = time_year + time_marry + time_mater + time_die + time_leave + time_sick;
                    //加班为0，不需要显示人员
                    //  if (time_over > 0)
                    //   {
                    decimal gongzi = Convert.ToDecimal(item["gongzi"]);
                    decimal xs = 1m, jqz = 0m;
                    string dpe = "",tzzt="";
                    if (list_auo != null)
                    {
                        TG.Model.cm_ApplyUnitOvertime model_auo = list_auo.Where(auo => auo.mem_ID == Convert.ToInt32(item["mem_ID"])).FirstOrDefault();
                        if (model_auo != null)
                        {
                            tzzt = "√";
                            xs = model_auo.Quotiety;
                            dpe = model_auo.QuotaMoney == null ? "" : model_auo.QuotaMoney.ToString();

                        }
                    }

                    //加权值
                    jqz = gongzi * time_over * xs / 10000;
                    //加权值总和
                    sum_marry = sum_marry + jqz;
                    //工资总和
                    sum_sum = sum_sum + gongzi;

                    sb.Append("<td align=\"center\" unitname='" + item["unitname"] + "' rel='bc'>" + row + "</td>");
                    sb.Append("<td align=\"center\" memid='" + item["mem_ID"] + "'>" + item["mem_Name"] + "</td>");
                    sb.Append("<td align=\"center\" style='display:none;'>" + gongzi.ToString("N0") + "</td>");
                    sb.Append("<td  align=\"center\" >" + time_sum.ToString("f1") + "</td>");
                    sb.Append("<td align=\"center\" >" + time_late.ToString("f0") + "</td>");
                    sb.Append("<td align=\"center\">" + time_over.ToString("f1") + "</td>");
                    sb.Append("<td align=\"center\" ><input type='text' class='form-control input-sm' name='xs' id='" + item["mem_ID"] + "' value='" + xs.ToString("f2") + "'/></td>");
                    sb.Append("<td align=\"center\"  >" + jqz.ToString("f2") + "</td>");
                    sb.Append("<td align=\"center\" ><input type='text' class='form-control input-sm' name='tpe'  value='" + dpe + "'/></td>");
                    sb.Append("<td align=\"center\"  >" + tzzt + "</td>");
                    sb.Append("</tr>");

                    //加班总和
                    sum_over = sum_over + time_over;
                    //调配额总和
                    sum_dpe = sum_dpe + (dpe == "" ? 0 : Convert.ToDecimal(dpe));
                    row = row + 1;
                    //  }
                    //else
                    //{
                    //    sb.Append("</tr>");
                    //}

                    //相同增加合计
                    if (xtindex == cls)
                    {
                        sb.Append("<tr style='font-weight:bold;'>");
                        sb.Append("<td align=\"center\" unitname='" + item["unitname"] + "'></td>");
                        sb.Append("<td align=\"center\" >总额</td>");
                        sb.Append("<td align=\"center\" style='display:none;'>" + sum_sum.ToString("N0") + "</td>");
                        sb.Append("<td align=\"center\" ></td>");
                        sb.Append("<td align=\"center\" ></td>");
                        sb.Append("<td align=\"center\">" + sum_over.ToString("f1") + "</td>");
                        sb.Append("<td align=\"center\" ></td>");
                        sb.Append("<td align=\"center\" >" + sum_marry.ToString("f2") + "</td>");
                        sb.Append("<td align=\"center\" >" + (sum_dpe == 0 ? "" : sum_dpe.ToString("N0")) + "</td>");
                        sb.Append("<td align=\"center\" ></td>");
                        sb.Append("</tr>");

                    }
                    xtindex++;

                }

            }
            else
            {
                sb.Append("<tr><td colspan='10' style='color:red;'>没有数据！</td></tr>");
            }


            lithtml.Text = sb.ToString();
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {
            // this.AspNetPager1.CurrentPageIndex = 1;
            BindData();
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        //protected void AspNetPager1_PageChanged(object src, EventArgs e)
        //{
        //    BindData();
        //}
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();
            string sqlwhere = "";

            if (base.RolePowerParameterEntity.PreviewPattern != 1)
            {
                //张翠珍
                //if (base.UserShortName == "zhangcuizhen")
                //{
                //    string spname = SpecialPersonList[0];
                //    spname = "'" + (spname.Replace(",", "','")) + "'";
                //    sqlwhere = " and unit_ID in (Select mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where m.mem_Name in (" + spname + "))";
                //}
                //else if (base.UserShortName == "zhangwei1")
                //{
                //    string spname = SpecialPersonList[1] + "," + SpecialPersonList[2];
                //    spname = "'" + (spname.Replace(",", "','")) + "'";
                //    sqlwhere = " and unit_ID in (Select mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where m.mem_Name in (" + spname + "))";
                //}
                //else
                if (base.UserShortName == "zhangbing")
                {
                    sqlwhere = " and unit_ID=284";
                }
                else
                {
                    sqlwhere = " and unit_ID IN (Select mem_unit_ID From tg_memberRole Where mem_unit_ID<>230 and mem_ID=" + UserSysNo + ")";
                }

            }

            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 and unit_ID<>230 and unit_ID not in (" + NotShowUnitList + ")" + sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定          
            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
                int count = UserRolesList.Where(u => u.RoleName == "管理员").Count();
                if (count > 0)
                {
                    this.ismanager.Value = "1";
                }

                object o = TG.DBUtility.DbHelperSQL.GetSingle("select top 1 RoleIdMag from tg_memberRole where mem_unit_ID<>230 and mem_ID=" + base.UserSysNo + " order by mem_unit_ID desc");
                if (o != null)
                {
                    this.previewPower.Value = o.ToString();
                }
            }
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }

            for (int i = 1; i <= 12; i++)
            {
                this.drp_month.Items.Add(i.ToString());
            }

        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
            //部门ID
            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
            else
            {
                //选择部门
                if (this.drp_unit.Items.Count > 1)
                {
                    this.drp_unit.Items[1].Selected = true;
                }
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {

            //只有admin和孟颖可以看到全部
            if (base.RolePowerParameterEntity.PreviewPattern != 1)
            {
                //张翠珍
                //if (base.UserShortName == "zhangcuizhen")
                //{
                //    string spname = SpecialPersonList[0];
                //    spname = "'" + (spname.Replace(",", "','")) + "'";
                //    sb.Append(" AND mr.mem_Unit_ID in (Select mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_unit_ID<>230 and m.mem_Name in (" + spname + "))");
                //}
                //else if (base.UserShortName == "zhangwei1")
                //{
                //    string spname = SpecialPersonList[1] + "," + SpecialPersonList[2];
                //    spname = "'" + (spname.Replace(",", "','")) + "'";
                //    sb.Append(" AND mr.mem_Unit_ID in (Select mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_unit_ID<>230 and m.mem_Name in (" + spname + "))");
                //}
                //else 
                if (base.UserShortName == "zhangbing")
                {
                    sb.Append(" AND mr.mem_Unit_ID=284 ");

                }
                else
                {
                    sb.Append(" AND mr.mem_Unit_ID IN (Select mem_unit_ID From tg_memberRole Where mem_unit_ID<>230 and mem_ID=" + UserSysNo + ")");
                }
                //个人
                //if (base.RolePowerParameterEntity.PreviewPattern == 0)
                //{
                //    sb.Append(" AND m.mem_ID=" + UserSysNo);
                //}
            }

        }

        //导出
        protected void btn_export_Click(object sender, EventArgs e)
        {

            DataTable dt = BindWhere();
            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/UnitOvertimeMoney.xls");
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }
        private void ExportDataToExcel(DataTable dt, string modelPath)
        {
            string drpyear = this.drp_year.SelectedValue;
            var drpmonth = this.drp_month.SelectedValue;
            int nextyear = Convert.ToInt32(drpyear);
            var nextmonth = Convert.ToInt32(drpmonth) - 1;
            if (drpmonth == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) - 1);
                nextmonth = 12;
            }
            string pathname = "部门加班补助" + nextyear + "." + nextmonth + ".16-" + drpyear + "." + drpmonth + ".15";
            if (this.drp_unit.SelectedIndex > 0)
            {
                pathname = this.drp_unit.SelectedItem.Text.Trim() + pathname;
            }
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.RIGHT;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //背景颜色样式
            ICellStyle style3 = wb.CreateCellStyle(); ;
            style3.VerticalAlignment = VerticalAlignment.CENTER;
            style3.WrapText = true;
            style3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style3.FillForegroundColor = HSSFColor.PINK.index;
            style3.SetFont(font1);

            //背景颜色样式
            ICellStyle style4 = wb.CreateCellStyle();
            style4.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style4.FillForegroundColor = HSSFColor.YELLOW.index;
            style4.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style4.VerticalAlignment = VerticalAlignment.CENTER;
            style4.WrapText = true;
            style4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;


            //背景颜色样式
            ICellStyle style5 = wb.CreateCellStyle();
            style5.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style5.FillForegroundColor = HSSFColor.GREEN.index;
            style5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;


            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            IRow dataRowTitle = ws.GetRow(0);
            dataRowTitle.Height = 30 * 20;
            //ws.SetColumnWidth(2, 25 * 256);         
            ICell acell21 = dataRowTitle.GetCell(0);
            acell21.SetCellValue(pathname);
            // CellRangeAddress cellRangeAddress2 = new CellRangeAddress(1, 1, 0, 1);
            // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            //ws.AddMergedRegion(cellRangeAddress2);




            DateTime all_starttime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
            DateTime all_endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-15");


            //合计
            decimal sum_sum = 0;//请假总和  月工资
            //decimal sum_late = 0;//迟到 
            decimal sum_over = 0;//加班  加班
            //  decimal sum_year = 0; //年假                  
            decimal sum_marry = 0;//婚假  计算额
            //  decimal sum_mater = 0;//产/陪产假
            //  decimal sum_die = 0;//丧假
            //  decimal sum_leave = 0;//事假
            //  decimal sum_sick = 0; //病假
            decimal sum_dpe = 0;//调配额

            //获取锁定数据
            List<TG.Model.cm_ApplyStatisDetailHis> his_list = new TG.BLL.cm_ApplyStatisDetailHis().GetModelList(" (ISDATE(dataDate)=1 and dataDate between '" + all_starttime.ToString("yyyy-MM-dd") + "' and '" + all_endtime.ToString("yyyy-MM-dd") + "')");

            //打卡数据
            DataTable datatable = GetCurrentMonth((drpyear + drpmonth.PadLeft(2, '0')), "1");

            //后台设置上班时间
            List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_year=" + drpyear + " and attend_month=" + drpmonth);
            //得到所有节假日日期。
            list = new TG.BLL.cm_HolidayConfig().GetModelList("");

            //筛选当前年，小于当前月之前的，部门活动申请数据          
            string unitwhere = string.Format(" and convert(varchar(10),starttime,120)>='{0}' and convert(varchar(10),endtime,120)<'{1}' and applytype='depart' ", ((Convert.ToInt32(drpyear) - 1) + "-12-16"), (nextyear + "-" + nextmonth.ToString().PadLeft(2, '0') + "-16"));
            DataTable unit_dt = new TG.BLL.cm_ApplyInfo().GetApplyList(unitwhere).Tables[0];

            //部门人系数
            List<TG.Model.cm_ApplyUnitOvertime> list_auo = new TG.BLL.cm_ApplyUnitOvertime().GetModelList(" OverYear=" + drpyear + " and OverMonth=" + drpmonth + " ");

            //手动修改数据
            List<TG.Model.cm_ApplyStatisData> list_data = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='StatisDetail' order by id desc");

            if (dt != null && dt.Rows.Count > 0)
            {

                int row = 1;
                int old_unitid = 0;
                int xtindex = 1;
                int cls = 0;
                int hjs = 0;//总额个数
                foreach (DataRow item in dt.Rows)
                {
                    var dataRow = ws.GetRow((row + 1 + hjs));//第一行
                    if (dataRow == null)
                        dataRow = ws.CreateRow((row + 1 + hjs));//生成行

                    if (old_unitid != Convert.ToInt32(item["Unit_ID"]))
                    {
                        sum_sum = 0;
                        //  sum_late = 0;//迟到 
                        sum_over = 0;//加班
                        //  sum_year = 0; //年假                  
                        sum_marry = 0;//婚假
                        //   sum_mater = 0;//产/陪产假
                        //   sum_die = 0;//丧假
                        //   sum_leave = 0;//事假
                        //   sum_sick = 0; //病假
                        sum_dpe = 0;
                        xtindex = 1;
                        old_unitid = Convert.ToInt32(item["Unit_ID"]);
                        cls = dt.Select("Unit_ID=" + old_unitid).Count();

                        ICell cell0 = dataRow.GetCell(0);
                        if (cell0 == null)
                            cell0 = dataRow.CreateCell(0);

                        cell0.SetCellValue(item["unitname"].ToString().Trim());

                        CellRangeAddress cellRangeAddress2 = new CellRangeAddress((row + 1 + hjs), (row + 1 + hjs + cls), 0, 0);
                        ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
                        ws.AddMergedRegion(cellRangeAddress2);

                    }


                    //获取某人上下班时间
                    if (pas_list != null && pas_list.Count > 0)
                    {
                        List<TG.Model.cm_PersonAttendSet> pas_list_mem = pas_list.Where(p => p.mem_ID == Convert.ToInt32(item["mem_ID"])).ToList();
                        if (pas_list_mem != null && pas_list_mem.Count > 0)
                        {
                            towork = pas_list_mem[0].ToWork;
                            offwork = pas_list_mem[0].OffWork;
                        }
                        else
                        {
                            towork = "09:00";
                            offwork = "17:30";
                        }
                    }
                    //筛选当前年当前人，小于当前月之前的，部门活动申请数据总小时
                    decimal sumunittime = 0;
                    if (unit_dt != null && unit_dt.Rows.Count > 0)
                    {
                        // sumunittime = unit_dt.AsEnumerable().Where(s => s.Field<int>("adduser") == Convert.ToInt32(item["mem_ID"])).Sum(s => s.Field<decimal>("totaltime"));
                        DataTable unit_dt2 = new DataView(unit_dt) { RowFilter = "adduser=" + item["mem_ID"] + "" }.ToTable();
                        if (unit_dt2 != null && unit_dt2.Rows.Count > 0)
                        {
                            //得到小于一天的申请
                            sumunittime = unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > 0 && s.Field<decimal>("totaltime") <= Convert.ToDecimal(7.5)).Sum(s => s.Field<decimal>("totaltime"));
                            //大于一天申请，按7.5算
                            sumunittime = sumunittime + ((unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > Convert.ToDecimal(7.5)).Count()) * Convert.ToDecimal(7.5));
                        }
                    }

                    string where = string.Format(" and adduser={0}", item["mem_ID"]);
                    DataTable dt_all = new TG.BLL.cm_ApplyInfo().GetApplyList(where).Tables[0];

                    //请假
                    decimal time_sum = 0;//请假总和
                    decimal time_late = 0;//迟到 
                    decimal time_over = 0;//加班
                    decimal time_year = 0; //年假                  
                    decimal time_marry = 0;//婚假
                    decimal time_mater = 0;//产/陪产假
                    decimal time_die = 0;//丧假
                    decimal time_leave = 0;//事假
                    decimal time_sick = 0; //病假

                    DateTime starttime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
                    DateTime endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-15");


                    //请假统计
                    for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddDays(1))
                    {
                        decimal day_late = 0;//迟到 
                        decimal day_over = 0;//加班
                        decimal day_year = 0; //年假                  
                        decimal day_marry = 0;//婚假
                        decimal day_mater = 0;//产/陪产假
                        decimal day_die = 0;//丧假
                        decimal day_leave = 0;//事假
                        decimal day_sick = 0; //病假
                        //小于当前时间
                        if (starttime <= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                        {
                            TG.Model.cm_ApplyStatisDetailHis his_mem = null;
                            if (his_list != null && his_list.Count > 0)
                            {
                                his_mem = his_list.Where(h => h.mem_id == Convert.ToInt32(item["mem_ID"]) && h.dataDate == starttime.ToString("yyyy-MM-dd")).OrderByDescending(h => h.ID).FirstOrDefault();
                            }
                            if (his_mem != null)
                            {
                                day_late = Convert.ToDecimal(his_mem.time_late);
                                day_over = Convert.ToDecimal(his_mem.time_over);
                                day_marry = Convert.ToDecimal(his_mem.time_marry);
                                day_mater = Convert.ToDecimal(his_mem.time_mater);
                                day_die = Convert.ToDecimal(his_mem.time_die);
                                day_leave = Convert.ToDecimal(his_mem.time_leave);
                                day_sick = Convert.ToDecimal(his_mem.time_sick);

                            }
                            else
                            {

                                //获取某人某年某月某日
                                List<TG.Model.cm_ApplyStatisData> list_data2_Detail = new List<TG.Model.cm_ApplyStatisData>();
                                if (list_data != null && list_data.Count > 0)
                                {
                                    list_data2_Detail = list_data.Where(asd => asd.mem_id == Convert.ToInt32(item["mem_ID"]) && asd.dataYear == starttime.Year && asd.dataMonth == starttime.Month && asd.dataDay == starttime.Day).ToList();
                                }

                                //上班时间
                                //   DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00");
                                //中午下班时间
                                DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50:00");
                                //中午上班时间
                                DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00:59");
                                //下午下班时间
                                //    DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");

                                //打卡统计
                                DateTime tomr_endtime = starttime;
                                DataTable dt_late = new DataTable();
                                if (datatable != null && datatable.Rows.Count > 0)
                                {

                                    dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + item["mem_Name"] + "'" }.ToTable();
                                    tomr_endtime = starttime;
                                }

                                //考勤申请记录
                                // string where = string.Format(" and adduser={0} and '{1}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) ", item["mem_ID"], starttime.ToString("yyyy-MM-dd"));
                                //  DataTable dt_all = new TG.BLL.cm_ApplyInfo().GetApplyList(where).Tables[0];
                                //截取年月日格式
                                int len = starttime.ToString().IndexOf(" ") + 1;
                                string temp = starttime.ToString().Substring(0, len);
                                DataTable dt_leave = new DataTable();
                                if (dt_all != null && dt_all.Rows.Count > 0)
                                {
                                    dt_leave = new DataView(dt_all) { RowFilter = "(((('" + starttime + "'>=starttime and '" + starttime + "'<=endtime) or '" + temp + "'=substring(Convert(starttime,'System.String'),1," + len + ") or substring(Convert(endtime,'System.String'),1," + len + ")='" + temp + "')) ) or ('" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00')" }.ToTable();
                                }



                                if (dt_leave != null && dt_leave.Rows.Count > 0)
                                {
                                    //加班离岗
                                    DataTable dt_addwork = new DataView(dt_leave) { RowFilter = "applytype='addwork' " }.ToTable();
                                    if (dt_addwork != null && dt_addwork.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dt_addwork.Rows)
                                        {
                                            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                            day_over = day_over - totaltime_over;
                                        }
                                    }
                                }

                                if (dt_late != null && dt_late.Rows.Count > 0)
                                {
                                    DataTable travel_yestoday = new DataTable();
                                    DataTable dt_apply = new DataTable();
                                    DataTable dt_flag = new DataTable();
                                    if (dt_leave != null && dt_leave.Rows.Count > 0)
                                    {
                                        //出差或外勤今天的0点到6,允许迟到半天
                                        travel_yestoday = new DataView(dt_leave) { RowFilter = "applytype in ('travel','gomeet','forget') and endtime<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();

                                        //申请记录统计
                                        dt_apply = new DataView(dt_leave) { RowFilter = "applytype not in ('addwork')" }.ToTable();
                                        if (dt_apply != null && dt_apply.Rows.Count > 0)
                                        {
                                            dt_flag = new DataView(dt_apply) { RowFilter = "endtime>'" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();
                                            #region 事假
                                            DataTable dt_leave1 = new DataView(dt_apply) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                            day_leave = day_leave + GetLeave(dt_leave1, starttime);
                                            //每年部门活动带薪时间1天（7.5小时），超过7.5小时按事假计算
                                            decimal unittime = 0;
                                            DataTable dt_temp = new DataView(dt_apply) { RowFilter = "applytype='depart' " }.ToTable();
                                            if (dt_temp != null && dt_temp.Rows.Count > 0)
                                            {
                                                //当天的部门活动小时
                                                unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                            }

                                            day_leave = day_leave + unittime;

                                            #endregion
                                            #region 病假
                                            DataTable dt_sick = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='病假'" }.ToTable();
                                            day_sick = day_sick + GetLeave(dt_sick, starttime);
                                            #endregion
                                            #region 加班
                                            tomr_endtime = starttime;
                                            DataTable dt_over = new DataView(dt_apply) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00'" }.ToTable();
                                            if (dt_over != null && dt_over.Rows.Count > 0)
                                            {
                                                foreach (DataRow dr in dt_over.Rows)
                                                {
                                                    decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                                    day_over = day_over + totaltime_over;
                                                }
                                            }


                                            #endregion
                                            #region 婚假
                                            DataTable dt_marry = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='婚假'" }.ToTable();
                                            day_marry = day_marry + GetLeave(dt_marry, starttime);
                                            #endregion
                                            #region 产/陪产假
                                            DataTable dt_mater = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='产/陪产假'" }.ToTable();
                                            day_mater = day_mater + GetLeave(dt_mater, starttime);
                                            #endregion
                                            #region 丧假
                                            DataTable dt_die = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='丧假'" }.ToTable();
                                            day_die = day_die + GetLeave(dt_die, starttime);
                                            #endregion
                                        }
                                    }
                                    if (!isListJQ(list, starttime))//排除节假日和周六日
                                    {
                                        #region 迟到
                                        DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                        DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                        DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                        DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                                        //出差和外勤申请记录
                                        string str = "";
                                        //判断申请记录是否存在,打卡记录只有一条或无数据或上午出差，下午出差时用到
                                        bool flag = false;
                                        if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                        {
                                            str = "bt";
                                        }
                                        if (dt_flag != null && dt_flag.Rows.Count > 0)
                                        {
                                            flag = true;
                                        }
                                        //迟到情况
                                        if ((shiji > houtai && shiji < houtaioff) || shiji > houtaioff)
                                        {
                                            //昨天下班打卡时间
                                            string yestoday = "";
                                            DateTime yes_time = starttime;
                                            yes_time = yes_time.AddDays(-1);
                                            DataTable dt_yestoday = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (yes_time.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + item["mem_Name"] + "'" }.ToTable();
                                            if (dt_yestoday != null && dt_yestoday.Rows.Count > 0)
                                            {
                                                yestoday = Convert.ToDateTime(dt_yestoday.Rows[(dt_yestoday.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm");
                                            }
                                            //已查询允许迟到半天
                                            if (str == "")
                                            {
                                                //截取年月日格式
                                                int yes_len = yes_time.ToString().IndexOf(" ") + 1;
                                                string yes_temp = yes_time.ToString().Substring(0, yes_len);
                                                travel_yestoday = new DataTable();
                                                if (dt_all != null && dt_all.Rows.Count > 0)
                                                {
                                                    travel_yestoday = new DataView(dt_all) { RowFilter = "applytype in ('travel','gomeet','forget') and starttime>='" + yes_time.ToString("yyyy-MM-dd") + " 06:00:00' and endtime<='" + yes_time.ToString("yyyy-MM-dd") + " 23:59:59'" }.ToTable();
                                                }
                                                //需要执行数据库
                                                //出差或外勤昨天22点到23:50,允许迟到半小时
                                                //string strwhere = string.Format(" and (applytype='travel' or applytype='gomeet') and '{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) and adduser={1} and starttime>='{2}' and endtime<'{3}'", yes_time.ToString("yyyy-MM-dd"), memid, (yes_time.ToString("yyyy-MM-dd") + " 22:00:00"), (yes_time.ToString("yyyy-MM-dd") + " 23:50:00"));
                                                //DataTable travel_yestoday = new TG.BLL.cm_ApplyInfo().GetApplyList(strwhere).Tables[0];
                                                if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                                {
                                                    //查询结束时间是否大于23点50分钟
                                                    DataTable temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 23:50:00'") }.ToTable();
                                                    if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                    {
                                                        str = "bt";
                                                    }
                                                    else
                                                    {
                                                        //查询结束时间是否大于22点00分钟
                                                        temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 21:50:00'") }.ToTable();
                                                        if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                        {
                                                            str = "bxs";
                                                        }
                                                    }
                                                }
                                            }

                                            //昨天加班到22：00，所以允许今天迟到半小时后,统计从9:30
                                            if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 21:50:00") && Convert.ToDateTime(yestoday) < Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bxs")
                                            {
                                                houtai = houtai.AddMinutes(30);
                                            }
                                            //昨天加班到23:50，所以允许迟到半天,统计从13：0:0
                                            else if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bt")
                                            {
                                                houtai = zwsbtime;
                                            }

                                            TimeSpan ts = shiji - houtai;
                                            if (ts.TotalMinutes > 3)
                                            {
                                                if (ts.TotalMinutes >= 4 && ts.TotalMinutes <= 14)//迟到
                                                {

                                                    day_late++;


                                                }
                                                else if (ts.TotalMinutes > 14 && ts.TotalMinutes < 30 && !flag) //迟到大于10分钟按0.5事假统计
                                                {

                                                    day_leave = day_leave + decimal.Parse("0.5");


                                                }
                                                else
                                                {

                                                    //没有申请记录
                                                    if (!flag)
                                                    {
                                                        if (shiji > houtaioff)
                                                        {
                                                            //下班时间-上班时间减去一小时
                                                            TimeSpan ts2 = houtaioff - houtai;
                                                            day_leave = day_leave + GetHours(ts2);
                                                            //上班时间不是下午1点。
                                                            if (houtai < zwsbtime)
                                                            {
                                                                day_leave = day_leave - 1;
                                                            }
                                                        }
                                                        else
                                                        {

                                                            //上班未上班, 正常上班时间9点、9点半
                                                            if (shiji >= zwxbtime && zwxbtime > houtai)
                                                            {
                                                                TimeSpan ts2 = zwxbtime - houtai;
                                                                day_leave = day_leave + GetHours(ts2);
                                                            }
                                                            else
                                                            {
                                                                //上午上班，打卡迟到大于30分钟
                                                                day_leave = day_leave + GetHours(ts);

                                                            }

                                                            //实际打卡时间大于下午上班时间, 正常上班时间9点、9点半
                                                            if (shiji > zwsbtime && zwxbtime > houtai)
                                                            {
                                                                TimeSpan ts3 = shiji - zwsbtime;
                                                                day_leave = day_leave + GetHours(ts3);
                                                            }
                                                        }
                                                    }

                                                }


                                                //超过5次后，按照0.5小时事假扣除
                                                if (day_late > 0 && (time_late + 1) > 5)
                                                {
                                                    day_leave = day_leave + decimal.Parse("0.5");
                                                }
                                            }
                                        }
                                        //早退                              
                                        if (dt_late.Rows.Count > 0 && shijioff < houtaioff && !flag)
                                        {
                                            DateTime zaotui = shijioff;
                                            //只有一条早上打卡记录,按上班时间计算
                                            if (shijioff < houtai)
                                            {
                                                zaotui = houtai;
                                            }
                                            //下班打卡时间是上午,中午下班时间-早退时间
                                            if (shijioff < zwxbtime)
                                            {
                                                if (dt_late.Rows.Count == 1)
                                                {
                                                    zaotui = houtai;
                                                    TimeSpan ts_sw = zwxbtime - zaotui;
                                                    day_leave = GetHours(ts_sw);
                                                    //超过5次后，按照0.5小时事假扣除
                                                    if (day_late > 0 && (time_late + 1) > 5)
                                                    {
                                                        day_leave = day_leave + decimal.Parse("0.5");
                                                    }
                                                }
                                                else
                                                {
                                                    TimeSpan ts_sw = zwxbtime - zaotui;
                                                    day_leave = day_leave + GetHours(ts_sw);
                                                }

                                            }

                                            //下班打卡时间是下午1点之前,就默认是下午1点
                                            if (shijioff < zwsbtime)
                                            {
                                                zaotui = zwsbtime;
                                            }

                                            TimeSpan ts2 = houtaioff - zaotui;
                                            decimal ztqj = GetHours(ts2);

                                            day_leave = day_leave + ztqj;

                                        }

                                        //下班时间+30分钟，算加班
                                        DateTime jiaban = houtaioff.AddMinutes(30);

                                        if (shiji > jiaban && dt_late.Rows.Count > 1)
                                        {
                                            int mine = shiji.Minute;
                                            if (mine > 0 && mine < 15)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                            }
                                            else if (mine > 30 && mine < 45)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                            }
                                            else
                                            {
                                                jiaban = shiji;
                                            }

                                        }

                                        //加班统计                       
                                        if (shijioff > jiaban)
                                        {
                                            decimal totaltime_forget = 0;
                                            //判断是否有未打卡申请记录
                                            tomr_endtime = starttime;
                                            if (dt_leave != null && dt_leave.Rows.Count > 0)
                                            {
                                                DataTable dt_over = new DataView(dt_leave) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                                if (dt_over != null && dt_over.Rows.Count > 0)
                                                {
                                                    totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                                }
                                            }
                                            //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                            if (totaltime_forget == 0)
                                            {
                                                day_over = day_over + GetOver(jiaban, shijioff);
                                            }
                                            // day_over = day_over + GetOver(jiaban, shijioff);
                                        }


                                        #endregion
                                    }
                                    else
                                    {
                                        //周六日，节假日加班
                                        if (dt_late != null && dt_late.Rows.Count > 0)
                                        {
                                            DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                            DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                            DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                            DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                                            //实际打卡时间
                                            int mine = shiji.Minute;
                                            DateTime jiaban = shiji;
                                            if (mine > 0 && mine < 15)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                            }
                                            else if (mine > 30 && mine < 45)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                            }

                                            decimal totaltime_forget = 0;
                                            //判断是否有未打卡申请记录
                                            tomr_endtime = starttime;
                                            if (dt_leave != null && dt_leave.Rows.Count > 0)
                                            {
                                                DataTable dt_over = new DataView(dt_leave) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                                if (dt_over != null && dt_over.Rows.Count > 0)
                                                {
                                                    totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                                }
                                            }
                                            //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                            if (totaltime_forget == 0)
                                            {
                                                day_over = day_over + GetOver(shiji, shijioff);
                                            }
                                            // day_over = day_over + GetOver(shiji, shijioff);
                                            ////全天
                                            //if (shiji <= houtai && shijioff >= houtaioff)
                                            //{
                                            //    day_over = day_over + decimal.Parse("7.5");
                                            //}
                                            //else
                                            //{
                                            //    DateTime start = shiji;
                                            //    DateTime end = shijioff;
                                            //    //加班统计   
                                            //    if (shiji < houtai)
                                            //    {
                                            //        start = houtai;
                                            //    }
                                            //    else if (shiji >= zwxbtime && shiji <= zwsbtime)
                                            //    {
                                            //        shiji = zwsbtime;
                                            //    }
                                            //    if (shijioff > houtaioff)
                                            //    {
                                            //        end = houtaioff;
                                            //    }
                                            //    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
                                            //    {
                                            //        end = zwxbtime;
                                            //    }

                                            //    //都是上午打卡    
                                            //    if (end <= zwxbtime)
                                            //    {
                                            //        end = shijioff;
                                            //        day_over = day_over + GetOver(start, end);
                                            //    }
                                            //    else //下班打卡是下午
                                            //    {
                                            //        //上班打卡也是下午
                                            //        if (start >= zwsbtime)
                                            //        {
                                            //            day_over = day_over + GetOver(start, end);
                                            //        }
                                            //        else//上班打卡是上午
                                            //        {
                                            //            //上午加班时间
                                            //            day_over = day_over + GetOver(start, zwxbtime);
                                            //            //下午加班时间
                                            //            day_over = day_over + GetOver(zwsbtime, end);
                                            //        }
                                            //    }
                                            //}
                                            ////晚上加班
                                            //DateTime jiaban = houtaioff.AddMinutes(30);

                                            //if (shiji > jiaban)
                                            //{
                                            //    int mine = shiji.Minute;
                                            //    if (mine > 0 && mine < 15)
                                            //    {
                                            //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                            //    }
                                            //    else if (mine > 30 && mine < 45)
                                            //    {
                                            //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                            //    }
                                            //    else
                                            //    {
                                            //        jiaban = shiji;
                                            //    }

                                            //}

                                            ////加班统计                       
                                            //if (shijioff > jiaban)
                                            //{
                                            //    day_over = day_over + GetOver(jiaban, shijioff);
                                            //}

                                        }

                                    }
                                }
                                else
                                {
                                    DataTable dt_time = dt_leave;
                                    if (dt_time != null && dt_time.Rows.Count > 0)
                                    {
                                        if (!isListJQ(list, starttime))//排除节假日和周六日
                                        {
                                            #region  年假
                                            //DataTable dt_year = new DataView(dt_time) { RowFilter = "applytype='leave' and (reason='年假')" }.ToTable();
                                            //day_year = day_year + GetLeave(dt_year, starttime);
                                            #endregion
                                            #region 事假
                                            DataTable dt_leave1 = new DataView(dt_time) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                            day_leave = day_leave + GetLeave(dt_leave1, starttime);
                                            //每年部门活动带薪时间1天（7.5小时），超过7.5小时按事假计算
                                            decimal unittime = 0;
                                            DataTable dt_temp = new DataView(dt_time) { RowFilter = "applytype='depart' " }.ToTable();
                                            if (dt_temp != null && dt_temp.Rows.Count > 0)
                                            {
                                                //当天的部门活动小时
                                                unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                            }

                                            day_leave = day_leave + unittime;

                                            #endregion
                                            #region 病假
                                            DataTable dt_sick = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='病假'" }.ToTable();
                                            day_sick = day_sick + GetLeave(dt_sick, starttime);
                                            #endregion

                                            #region 婚假
                                            DataTable dt_marry = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='婚假'" }.ToTable();
                                            day_marry = day_marry + GetLeave(dt_marry, starttime);
                                            #endregion
                                            #region 产/陪产假
                                            DataTable dt_mater = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='产/陪产假'" }.ToTable();
                                            day_mater = day_mater + GetLeave(dt_mater, starttime);
                                            #endregion
                                            #region 丧假
                                            DataTable dt_die = new DataView(dt_time) { RowFilter = "applytype='leave' and reason='丧假'" }.ToTable();
                                            day_die = day_die + GetLeave(dt_die, starttime);
                                            #endregion
                                        }

                                        #region 加班
                                        tomr_endtime = starttime;
                                        DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00'" }.ToTable();
                                        if (dt_over != null && dt_over.Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dt_over.Rows)
                                            {
                                                decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                                day_over = day_over + totaltime_over;
                                            }
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        if (!isListJQ(list, starttime))//排除节假日和周六日
                                        {
                                            //一天未打卡，无申请
                                            day_leave = day_leave + decimal.Parse("7.5");
                                        }
                                    }

                                }



                                //判断是否手动改过
                                if (list_data2_Detail != null && list_data2_Detail.Count > 0)
                                {
                                    var data_model = list_data2_Detail.Where(d => d.dataType == "late").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_late = data_model.dataValue;
                                        //超过5次后，按照0.5小时事假扣除
                                        if (day_late > 0 && (time_late + 1) > 5)
                                        {
                                            day_leave = day_leave + decimal.Parse("0.5");
                                        }
                                    }

                                     data_model = list_data2_Detail.Where(d => d.dataType == "leave").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_leave = data_model.dataValue;

                                    }

                                    data_model = list_data2_Detail.Where(d => d.dataType == "sick").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_sick = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_over = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "marry").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_marry = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "mater").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_mater = data_model.dataValue;

                                    }
                                    data_model = list_data2_Detail.Where(d => d.dataType == "die").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        day_die = data_model.dataValue;

                                    }                                   

                                }



                            }
                        }
                        time_late = time_late + day_late;
                        time_over = time_over + day_over;
                        //  time_year = time_year + day_year;
                        time_marry = time_marry + day_marry;
                        time_mater = time_mater + day_mater;
                        time_die = time_die + day_die;
                        time_leave = time_leave + day_leave;
                        time_sick = time_sick + day_sick;
                    }
                    //请假
                    time_sum = time_year + time_marry + time_mater + time_die + time_leave + time_sick;
                    //加班为0，不需要显示人员
                    //  if (time_over > 0)
                    //   {
                    decimal gongzi = Convert.ToDecimal(item["gongzi"]);
                    decimal xs = 1m, jqz = 0m;
                    string dpe = "",tzzt="";
                    if (list_auo != null)
                    {
                        TG.Model.cm_ApplyUnitOvertime model_auo = list_auo.Where(auo => auo.mem_ID == Convert.ToInt32(item["mem_ID"])).FirstOrDefault();
                        if (model_auo != null)
                        {
                            tzzt = "√";
                            xs = model_auo.Quotiety;
                            dpe = model_auo.QuotaMoney == null ? "" : model_auo.QuotaMoney.ToString();

                        }
                    }

                    //加权值
                    jqz = gongzi * time_over * xs / 10000;
                    //加权值总和
                    sum_marry = sum_marry + jqz;
                    //工资总和
                    sum_sum = sum_sum + gongzi;

                    //第二列
                    ICell cell = dataRow.GetCell((1));
                    if (cell == null)
                        cell = dataRow.CreateCell((1));
                    cell.CellStyle = style2;
                    cell.SetCellValue(row);

                    //第三列
                    cell = dataRow.GetCell(2);
                    if (cell == null)
                        cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(item["mem_Name"].ToString());
                    //第四列
                    cell = dataRow.GetCell(3);
                    if (cell == null)
                        cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(gongzi.ToString("N0"));
                    //第五列
                    cell = dataRow.GetCell(4);
                    if (cell == null)
                        cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(time_sum.ToString("f1"));
                    //第六列
                    cell = dataRow.GetCell(5);
                    if (cell == null)
                        cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(time_late.ToString("f0"));
                    //第七列
                    cell = dataRow.GetCell(6);
                    if (cell == null)
                        cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(time_over.ToString("f1"));
                    //第八列
                    cell = dataRow.GetCell(7);
                    if (cell == null)
                        cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(xs.ToString("f2"));
                    //第九列
                    cell = dataRow.GetCell(8);
                    if (cell == null)
                        cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(jqz.ToString("f2"));
                    //第十列
                    cell = dataRow.GetCell(9);
                    if (cell == null)
                        cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dpe);
                    //第十一列
                    cell = dataRow.GetCell(10);
                    if (cell == null)
                        cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(tzzt);

                    //加班总和
                    sum_over = sum_over + time_over;
                    //调配额总和
                    sum_dpe = sum_dpe + (dpe == "" ? 0 : Convert.ToDecimal(dpe));
                    row = row + 1;
                    //   }

                    //相同增加合计
                    if (xtindex == cls)
                    {
                        decimal sum_jse = 0;
                        //加权值大于0
                        if (sum_marry > 0)
                        {
                            //【单位分配】=sum【加班时间】*26/sum【加权值】
                            decimal dwfp = Math.Round((Math.Round(sum_over, 1) * 26 / Math.Round(sum_marry, 2)), 2);
                            int startindex = (row + 1 + hjs) - cls;
                            //部门每个人计算额
                            for (int k = 0; k < cls; k++)
                            {
                                var unitRow = ws.GetRow(startindex + k);
                                var unitCell = unitRow.GetCell(6);
                                string unitover = unitCell.StringCellValue;
                                unitCell = unitRow.GetCell(8);
                                string unitjqz = unitCell.StringCellValue;
                                decimal jse = 0;
                                //【加班时间】*26
                                if (cls == 1)
                                {
                                    jse = Convert.ToDecimal(unitover) * 26;

                                }
                                else
                                {
                                    //【计算额】=【加权值】*【单位分配】
                                    jse = Convert.ToDecimal(unitjqz) * dwfp;

                                }
                                sum_jse = sum_jse + Math.Round(jse);
                                unitCell.SetCellValue(jse.ToString("N0"));
                            }
                        }

                        dataRow = ws.GetRow((row + 1 + hjs));//第一行
                        if (dataRow == null)
                            dataRow = ws.CreateRow((row + 1 + hjs));//生成行
                        //第二列
                        cell = dataRow.GetCell(1);
                        if (cell == null)
                            cell = dataRow.CreateCell(1);
                        cell.CellStyle = style1;
                        cell.SetCellValue("");

                        //第三列
                        cell = dataRow.GetCell(2);
                        if (cell == null)
                            cell = dataRow.CreateCell(2);
                        cell.CellStyle = style1;
                        cell.SetCellValue("总额");
                        //第四列
                        cell = dataRow.GetCell(3);
                        if (cell == null)
                            cell = dataRow.CreateCell(3);
                        cell.CellStyle = style1;
                        cell.SetCellValue(sum_sum.ToString("N0"));
                        //第五列
                        cell = dataRow.GetCell(4);
                        if (cell == null)
                            cell = dataRow.CreateCell(4);
                        cell.CellStyle = style1;
                        cell.SetCellValue("");
                        //第六列
                        cell = dataRow.GetCell(5);
                        if (cell == null)
                            cell = dataRow.CreateCell(5);
                        cell.CellStyle = style1;
                        cell.SetCellValue("");
                        //第七列
                        cell = dataRow.GetCell(6);
                        if (cell == null)
                            cell = dataRow.CreateCell(6);
                        cell.CellStyle = style1;
                        cell.SetCellValue(sum_over.ToString("f1"));
                        //第八列
                        cell = dataRow.GetCell(7);
                        if (cell == null)
                            cell = dataRow.CreateCell(7);
                        cell.CellStyle = style1;
                        cell.SetCellValue("");
                        //第九列
                        cell = dataRow.GetCell(8);
                        if (cell == null)
                            cell = dataRow.CreateCell(8);
                        cell.CellStyle = style1;
                        cell.SetCellValue(sum_jse.ToString("N0"));

                        cell = dataRow.GetCell(9);
                        if (cell == null)
                            cell = dataRow.CreateCell(9);
                        cell.CellStyle = style1;
                        cell.SetCellValue((sum_dpe == 0 ? "" : sum_dpe.ToString("N0")));

                        //第十一列
                        cell = dataRow.GetCell(10);
                        if (cell == null)
                            cell = dataRow.CreateCell(10);
                        cell.CellStyle = style1;
                        cell.SetCellValue("");


                        hjs = hjs + 1;
                    }
                    xtindex++;

                }

            }

            //写入Session
            Session["SamplePictures"] = true;
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }

}