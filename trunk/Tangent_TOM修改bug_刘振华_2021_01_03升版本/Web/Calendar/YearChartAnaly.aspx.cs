﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Configuration;

namespace TG.Web.Calendar
{
    public partial class YearChartAnaly : PageBase
    {
        //部门名称
        public string unitData { get; set; }
        //组内平均值
        public string avgData { get; set; }
        //标准差系数
        public string bzcxsData { get; set; }
        //经理周加班系数
        public string managerData { get; set; }
        #region 组内平均表基本设置
        /// <summary>
        /// Y坐标数据
        /// </summary>
        public string xAxis { get; set; }
        /// <summary>
        /// y坐标数据
        /// </summary>
        public string yAxis { get; set; }
        /// <summary>
        /// 图例
        /// </summary>
        public string LegendData { get; set; }
        /// <summary>
        /// 统计数据
        /// </summary>
        public string SeriesData { get; set; }

        /// <summary>
        /// Y坐标数据
        /// </summary>
        public string xAxis2 { get; set; }
        /// <summary>
        /// y坐标数据
        /// </summary>
        public string yAxis2 { get; set; }
        /// <summary>
        /// 图例
        /// </summary>
        public string LegendData2 { get; set; }
        /// <summary>
        /// 统计数据
        /// </summary>
        public string SeriesData2 { get; set; }
        #endregion
        #region 图表统计数据 2015年6月10日
        /// <summary>
        /// 获取图例数据
        /// </summary>
        protected void GetLegendValue()
        {
            //范围图
            StringBuilder legendData = new StringBuilder();
            //数据图例
            legendData.Append("[");


            legendData.AppendFormat("\"{0}\",", "组内平均值");
          //  legendData.AppendFormat("\"{0}\",", "经理周加班数");
            //legendData.AppendFormat("\"{0}\"", "综合平均值");  
            //x坐标
            legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
            legendData.Append("]");
            LegendData = legendData.ToString();

            LegendData2 = "[\"组间标准差系数\"]";

        }

        /// <summary>
        ///  获取X坐标数据
        /// </summary>
        private void GetxAxisValue()
        {
            //横向坐标
            StringBuilder sbxAxis = new StringBuilder();

            sbxAxis.Append("[");

            if (!string.IsNullOrEmpty(unitData))
            {

                string[] list = unitData.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < (list.Count()); i++)
                {
                    sbxAxis.Append("\"" + list[i] + "\",");
                }
                //x坐标
                sbxAxis.Remove(sbxAxis.ToString().LastIndexOf(','), 1);
            }

            
            sbxAxis.Append("]");

            xAxis = sbxAxis.ToString();

            xAxis2 = sbxAxis.ToString();
        }

        /// <summary>
        /// 设置Y坐标数据
        /// </summary>
        private void SetyAxisValue()
        {
            StringBuilder sbyAxis = new StringBuilder();

            sbyAxis.Append(@"{
                            type : 'value',
                            name : '平均每人每周加班时间',
                            axisLabel : {
                                formatter: '{value} 小时'}
                        }");

            yAxis = sbyAxis.ToString();

            yAxis2 = "{type : 'value',name : '标准差系数',axisLabel : { formatter: '{value}%'}}";
        }

        /// <summary>
        /// 获取实际数据
        /// </summary>
        private void GetSeriesData()
        {
            StringBuilder sbSeries = new StringBuilder();

            //加载数据 
            //组内平均值
            string strData = GetCountDataByYear(avgData);
            sbSeries.AppendFormat(@"{{name:'组内平均值',
                                        type:'bar',
                                        data:{0},                                      
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", strData);
          

            sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
            //返回数据
            SeriesData = sbSeries.ToString();


            //标准差系数
            strData = GetCountDataByYear(bzcxsData);
            SeriesData2 = string.Format(@"{{name:'组间标准差系数',
                                        type:'bar',
                                        data:{0},                                      
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", strData);

        }
        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="slttype"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        private string GetCountDataByYear(string type)
        {
            //横向坐标
            //统计值
            StringBuilder sbyAxis = new StringBuilder();
            sbyAxis.Append("[");
            if (!string.IsNullOrEmpty(type))
            {

                string[] list = type.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < (list.Count()); i++)
                {
                    sbyAxis.Append(list[i] + ",");
                }
                //y坐标
                sbyAxis.Remove(sbyAxis.ToString().LastIndexOf(','), 1);
            }

           
            sbyAxis.Append("]");

            return sbyAxis.ToString();
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                BindData();

                //统计图表
                GetLegendValue();
                GetxAxisValue();
                SetyAxisValue();
                GetSeriesData();
            }

        }
        //得到显示的部门
        public string UnitIDList
        {
            get { return ConfigurationManager.AppSettings["UnitIDList"]; }
        }
        //根据条件查询
        private void BindData()
        {

            StringBuilder sb = new StringBuilder(" 1=1 ");

            //部门
            //if (this.drp_unit.SelectedIndex > 0)
            //{
            //    sb.Append(" AND Unit_ID=" + this.drp_unit.SelectedValue + "");
            //}

            TG.BLL.tg_member bllMem = new TG.BLL.tg_member();
            //检查权限
            GetPreviewPowerSql(ref sb);

            sb.Append(" and unit_ID in (" + UnitIDList + ") order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID) asc,unit_ID asc");

            DataTable dt = new TG.BLL.tg_unit().GetList(sb.ToString()).Tables[0];

            //生成table
            CreateTable(dt);
        }

        public void CreateTable(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            string drpyear = this.drp_year.SelectedValue;
            int nextyear = Convert.ToInt32(drpyear);
            int drpmonth = 2, nextmonth = 7;
            if (this.drp_month.SelectedValue == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) + 1);
                drpmonth = 8;
                nextmonth = 1;
            }

            DateTime starttime = Convert.ToDateTime(drpyear + "-" + (drpmonth - 1) + "-16");
            DateTime endtime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-15");

            string aa = ConfigurationManager.AppSettings["NoWork"];
            string nowork = "'" + (aa.Replace(",", "','")) + "'";

            decimal pjs = 0, sumpjs = 0, bzc = 0, sumbzc = 0;
            //排除最后两个部门
            string unitlist = UnitIDList.Substring(0, UnitIDList.LastIndexOf(","));
            unitlist = unitlist.Substring(0, unitlist.LastIndexOf(","));
            int count = unitlist.Split(',').Length;

            StringBuilder sb1 = new StringBuilder("<tr><td></td>");
            StringBuilder sb2 = new StringBuilder("<tr><td align=\"center\">组内平均值</td>");
            StringBuilder sb3 = new StringBuilder("<tr><td align=\"center\">组间标准差系数</td>");
           
            StringBuilder sb5 = new StringBuilder("<tr><td align=\"center\"></td>");
            StringBuilder sb6 = new StringBuilder("<tr><td align=\"center\">综合平均值</td>");
            StringBuilder sb7 = new StringBuilder("<tr><td align=\"center\">平均标准差系数</td>");
            if (dt != null && dt.Rows.Count > 0)
            {

                //得到所有节假日
                List<TG.Model.cm_HolidayConfig> holi_list = new TG.BLL.cm_HolidayConfig().GetModelList("");
                //后台设置上班时间
                List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList("");
                //申请加班记录
                DataTable dt_time = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype in ('travel','gomeet','addwork','forget') ").Tables[0];

                foreach (DataRow item in dt.Rows)
                {
                   
                    int rs = 0, pjgs = 0, cyjsr = 0;
                    //平均值，中位值，最小值，最大值，标准差,标准差系数,部门经理系数
                    decimal avgtime = 0, middletime = 0, mintime = 0, maxtime = 0, standardtime = 0, standardxs = 0, managerxs = 0;
                    decimal weekovertime = 0, sumbztime = 0, managertime = 0;//标准差总值

                    //把钟志宏添加到结构部 
                    string where = "";
                    if (item["unit_ID"].ToString() == "237")
                    {
                        where = " m.mem_ID=1445 or ";
                    }
                    //循环人员
                    DataTable list = TG.DBUtility.DbHelperSQL.Query("select m.*,mr.RoleIdMag from tg_member m left join tg_memberRole mr on m.mem_ID=mr.mem_Id where " + where + " (mr.mem_Unit_ID=" + item["unit_ID"] + " and mem_Name not in (" + nowork + ") and (mem_isFired=0 or m.mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + starttime.ToString("yyyy-MM-dd") + "'))) order by mem_Order asc,m.mem_ID asc").Tables[0];
                    if (list != null && list.Rows.Count > 0)
                    {
                        rs = list.Rows.Count;                     
                        decimal[] sz = new decimal[rs];
                        //排除无打卡记录人
                        int wdkjl = 0;
                        for (int i = 0; i < list.Rows.Count; i++)
                        {

                            //6个月的加班总和
                            decimal overtime = 0;
                            overtime = GetOverTimeYear(holi_list, pas_list, dt_time, Convert.ToInt32(list.Rows[i]["mem_ID"]), list.Rows[i]["mem_Name"].ToString(), starttime, endtime, ref wdkjl);
                            //周加班
                            overtime = Math.Round(overtime / (decimal.Parse("4.28") * 6), 1);
                            weekovertime += overtime;                           

                            //部门经理
                            if (list.Rows[i]["RoleIdMag"].ToString() == "5" || list.Rows[i]["RoleIdMag"].ToString() == "44")
                            {
                                managertime = managertime + overtime;
                            }
                            sz[i] = overtime;
                        }
                        //参与计算人数，排除无打卡记录人
                        cyjsr = rs - wdkjl;
                        if (cyjsr > 0)
                        {
                            //平均值
                            avgtime = (cyjsr == 0 ? 0 : (weekovertime / cyjsr));
                        }

                        //按参与计算人数
                        //向上取整数
                        double dd = Math.Ceiling((double)cyjsr / (double)2);
                        int bb = Convert.ToInt32(dd);
                        pjgs = cyjsr % 2;


                        //排序
                        sz = sort(sz);
                        //排除0个数
                       // int gs = sz.Count();
                        decimal gspjz = Math.Round(avgtime, 1);
                        // if (gs > 0)
                        //    {
                        //       gspjz = Math.Round((weekovertime / gs), 1);
                        //    }
                        for (int i = 0; i < cyjsr; i++)
                        {
                            //取得中间值
                            //奇数 中间位置的数据
                            if (pjgs > 0 && (i + 1) == bb)
                            {
                                middletime = sz[i];
                            }
                            else //偶数
                            {
                                //中间位置的两个数值的算术平均数
                                if ((i + 1) == bb)
                                {
                                    middletime = sz[i];
                                    middletime = (middletime + sz[(i+1)]) / 2;
                                }
                               
                            }

                            //不包含周加班为0的人员
                            //  if (sz[i] > 0)
                            //  {
                            sumbztime = sumbztime + (sz[i] - gspjz) * (sz[i] - gspjz);
                            //   }
                        }
                        //标准差
                        if (cyjsr > 1)
                        {
                            sumbztime = sumbztime / (cyjsr - 1);
                            //平方根
                            standardtime = Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(sumbztime)));
                        }


                        //部门经理
                        if (Math.Round(avgtime, 1) > 0)
                        {
                            //经理系数
                            managerxs = Math.Round(managertime, 1) / Math.Round(avgtime, 1) * 100;
                            //标准差系数
                            standardxs = (Math.Round(standardtime, 1) / Math.Round(avgtime, 1) * 100);
                        }
                        //if (Math.Round(gspjz,1)>0)
                        //{
                        //    //标准差系数
                        //    standardxs = (Math.Round(standardtime, 1) / Math.Round(gspjz, 1) * 100);
                        //}

                    }
                    if (cyjsr >= 2)
                    {
                        sb1.Append("<td align=\"center\">" + item["unit_Name"] + "</td>");
                        sb2.Append("<Td align=\"center\">" + Math.Round(avgtime, 1) + "</td>");
                        sb3.Append("<Td align=\"center\">" + Math.Round(standardxs, 1) + "%</td>");

                        //第二个表
                        if (unitlist.Contains(item["unit_ID"].ToString()))
                        {
                            sb5.Append("<td align=\"center\">" + item["unit_Name"] + "</td>");
                            sumpjs = sumpjs + Math.Round(avgtime, 1);
                            sumbzc = sumbzc + Math.Round(standardxs, 1);

                        }
                        //图表数据
                        unitData = unitData + item["unit_Name"].ToString().Trim() + ",";
                        avgData = avgData + Math.Round(avgtime, 1) + ",";
                        bzcxsData = bzcxsData + Math.Round(standardxs, 1) + ",";
                        managerData = managerData + Math.Round(managertime, 1) + ",";
                    }
                    else
                    {
                        count--;
                    }
                   
                }
                if (count>0)
                {
                    //加权限
                    if (base.RolePowerParameterEntity.PreviewPattern != 1)
                    {
                        count = dt.Rows.Count;
                    }
                    //平均数
                    pjs = Math.Round((sumpjs / count), 2);
                    //标准差
                    bzc = Math.Round((sumbzc / count), 2);
                    //循环显示数据
                    for (int j = 0; j < count; j++)
                    {
                        sb6.Append("<Td align=\"center\">" + pjs + "</td>");
                        sb7.Append("<Td align=\"center\">" + bzc + "%</td>");
                    }
                }
               
            }
            sb1.Append("</tr>");
            sb2.Append("</tr>");
            sb3.Append("</tr>");
           
            sb5.Append("</tr>");
            sb6.Append("</tr>");
            sb7.Append("</tr>");

            lithtml.Text = sb1.ToString() + sb2.ToString() + sb3.ToString();
            lithtml2.Text = sb5.ToString() + sb6.ToString() + sb7.ToString();
        }
        #region 已存在
        //获取加班时间
        //public decimal GetOverTime(int mem_id, string mem_name, DateTime starttime, DateTime endtime, ref int wdkjl)
        //{
        //    decimal time_over = 0;
        //    //申请出差加班
        //    string where = string.Format(" and applytype='travel' and convert(varchar(10),starttime,120) between '{0}' and '{1}' and adduser={2}", starttime.ToString("yyyy-MM-dd"), endtime.ToString("yyyy-MM-dd"), mem_id);
        //    DataTable dt_time = new TG.BLL.cm_ApplyInfo().GetApplyList(where).Tables[0];
        //    if (dt_time != null && dt_time.Rows.Count > 0)
        //    {
        //        foreach (DataRow dr in dt_time.Rows)
        //        {
        //            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
        //            time_over = time_over + totaltime_over;
        //        }
        //    }
        //    //判断这半年中此人是否有打卡记录
        //    bool flag = false;
        //    //循环最后一个日期 相同
        //    starttime = starttime.AddDays(-1);
        //    for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddMonths(1))
        //    {
        //        //结束日期 到这个月15号结束
        //        DateTime enddate = starttime;
        //        //开始日期 上月16号开始
        //        DateTime startdate = starttime.AddMonths(-1).AddDays(1);

        //        //考勤
        //        string towork = "", offwork = "";
        //        //后台设置上班时间
        //        List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" mem_ID=" + mem_id + " and attend_year=" + enddate.Year + " and attend_month=" + enddate.Month);
        //        if (pas_list != null && pas_list.Count > 0)
        //        {
        //            towork = pas_list[0].ToWork;
        //            offwork = pas_list[0].OffWork;
        //        }
        //        else
        //        {
        //            towork = "09:00";
        //            offwork = "17:30";
        //        }
        //        //考勤加班
        //        DataTable datatable = GetCurrentMonth((enddate.Year + enddate.Month.ToString().PadLeft(2, '0')), "1");

        //        //打卡统计
        //        //
        //        DataTable dt_late = new DataTable();
        //        if (datatable != null && datatable.Rows.Count > 0)
        //        {
        //            DateTime tomr_endtime = enddate;
        //            string tempTime = "";
        //            dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (startdate.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 05:59:59") + "' and UserName='" + mem_name + "'" }.ToTable();
        //            if (dt_late != null && dt_late.Rows.Count > 0)
        //            {
        //                flag = true;
        //                for (int i = 0; i < dt_late.Rows.Count; i++)
        //                {
        //                    DateTime shijioff = Convert.ToDateTime(dt_late.Rows[i]["CHECKTIME"].ToString());
        //                    DateTime houtai = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
        //                    DateTime houtaioff = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
        //                    //判断是否假期加班
        //                    if (isJQ(shijioff))
        //                    {
        //                        //判断是否已经执行过
        //                        if (tempTime != shijioff.ToString("yyyy-MM-dd"))
        //                        {

        //                            tempTime = shijioff.ToString("yyyy-MM-dd");
        //                            DateTime tomr_shijioff = shijioff;
        //                            DataTable dt_currt = new DataView(dt_late) { RowFilter = "CHECKTIME>='" + (shijioff.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 05:59:59") + "'" }.ToTable();
        //                            if (dt_currt != null && dt_currt.Rows.Count > 0)
        //                            {
        //                                //上班时间
        //                                DateTime ontime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 09:00");
        //                                //中午下班时间
        //                                DateTime zwxbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 11:50");
        //                                //中午上班时间
        //                                DateTime zwsbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 13:00");
        //                                //下午下班时间
        //                                DateTime offtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 17:30");

        //                                DateTime shiji = Convert.ToDateTime(dt_currt.Rows[0]["CHECKTIME"]);//实际上班打卡时间
        //                                shijioff = Convert.ToDateTime(dt_currt.Rows[(dt_currt.Rows.Count - 1)]["CHECKTIME"]);//实际下班打卡时间
        //                                //全天
        //                                if (shiji <= houtai && shijioff >= houtaioff)
        //                                {
        //                                    time_over = time_over + decimal.Parse("7.5");
        //                                }
        //                                else
        //                                {
        //                                    DateTime start = shiji;
        //                                    DateTime end = shijioff;
        //                                    //加班统计   
        //                                    if (shiji < houtai)
        //                                    {
        //                                        start = houtai;
        //                                    }
        //                                    else if (shiji >= zwxbtime && shiji <= zwsbtime)
        //                                    {
        //                                        shiji = zwsbtime;
        //                                    }
        //                                    if (shijioff > houtaioff)
        //                                    {
        //                                        end = houtaioff;
        //                                    }
        //                                    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
        //                                    {
        //                                        end = zwxbtime;
        //                                    }

        //                                    //都是上午打卡    
        //                                    if (end <= zwxbtime)
        //                                    {
        //                                        end = shijioff;
        //                                        time_over = time_over + GetOver(start, end);
        //                                    }
        //                                    else //下班打卡是下午
        //                                    {
        //                                        //上班打卡也是下午
        //                                        if (start >= zwsbtime)
        //                                        {
        //                                            time_over = time_over + GetOver(start, end);
        //                                        }
        //                                        else//上班打卡是上午
        //                                        {
        //                                            //上午加班时间
        //                                            time_over = time_over + GetOver(start, zwxbtime);
        //                                            //下午加班时间
        //                                            time_over = time_over + GetOver(zwsbtime, end);
        //                                        }
        //                                    }
        //                                }
        //                                //晚上加班
        //                                DateTime jiaban = houtaioff.AddMinutes(30);
        //                                //加班统计                       
        //                                if (shijioff > jiaban)
        //                                {
        //                                    time_over = time_over + GetOver(jiaban, shijioff);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        //下班时间+30分钟，算加班
        //                        DateTime jiaban = houtaioff.AddMinutes(30);
        //                        //加班统计                       
        //                        if (shijioff > jiaban)
        //                        {
        //                            time_over = time_over + GetOver(jiaban, shijioff);
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    //无打卡记录需加1
        //    if (!flag)
        //    {
        //        wdkjl = wdkjl + 1;
        //    }
        //    return time_over;
        //}

        ////返回加班时间
        //public decimal GetOver(DateTime start, DateTime end)
        //{
        //    decimal over = 0;
        //    if (end > start)
        //    {
        //        TimeSpan ts_jb = end - start;
        //        //强制转换，没有四舍五入
        //        int hours = (int)(ts_jb.TotalHours);

        //        double minutes = ts_jb.TotalMinutes - (hours * 60);
        //        //大于15分钟，即加班
        //        if (minutes > 15 && minutes <= 45)
        //        {
        //            over = decimal.Parse("0.5");
        //        }
        //        else if (minutes > 45 && minutes < 60)
        //        {
        //            over = decimal.Parse("1");
        //        }

        //        over = over + hours;
        //    }
        //    return over;
        //}
        ////判断是否是假期
        //public bool isJQ(DateTime date)
        //{
        //    bool flag = false;
        //    List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" convert(varchar(10),holiday,120)='" + date.ToString("yyyy-MM-dd") + "' order by id");
        //    //存在
        //    if (list != null && list.Count > 0)
        //    {
        //        //节假日，需减1天
        //        if (list[0].daytype == 1)
        //        {
        //            flag = true;
        //        }
        //    }
        //    else
        //    {
        //        //周六日0~6
        //        string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
        //        if (temp == "Sunday" || temp == "Saturday")
        //        {
        //            flag = true;
        //        }
        //    }
        //    return flag;
        //}
        ////从小到大排序
        //public decimal[] sort(decimal[] a)
        //{
        //    //排序  
        //    decimal t = 0;
        //    for (int i = 0; i < a.Length; i++)
        //    {
        //        for (int j = i + 1; j < a.Length; j++)
        //        {
        //            if (a[i] > a[j])
        //            {
        //                t = a[j];
        //                a[j] = a[i];
        //                a[i] = t;
        //            }
        //        }
        //    }
        //    return a;

        //}
#endregion
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {

            BindData();
            //统计图表
            GetLegendValue();
            GetxAxisValue();
            SetyAxisValue();
            GetSeriesData();
        }

        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();

            string sqlwhere = "";
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                sqlwhere = " unit_ParentID<>0 and unit_ID not in (" + NotShowUnitList + ") ";
            }
            else
            {
                sqlwhere = " unit_ID=" + UserUnitNo;
            }
            sqlwhere = sqlwhere + " and unit_ID<>230 order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID) asc,unit_ID asc";
           // this.drp_unit.DataSource = bll_unit.GetList(sqlwhere);
          //  this.drp_unit.DataTextField = "unit_Name";
          //  this.drp_unit.DataValueField = "unit_ID";
          //  this.drp_unit.DataBind();
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }


        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人，只有管理员看到全部
            if (base.RolePowerParameterEntity.PreviewPattern != 1)
            {
                sb.Append(" AND unit_ID =" + UserUnitNo + " ");
            }
            //部门
            //else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            //{
            //    sb.Append(" AND mem_ID in (Select mem_ID From tg_member Where mem_unit_ID=" + UserUnitNo + ")");
            //}
        }


    }

}