﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="MonthSummary.aspx.cs" Inherits="TG.Web.Calendar.MonthSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" lang="javascript">
        $(function () {

            var drp_year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
            var drp_month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
            var next_year = parseInt(drp_year);
            var next_month = (parseInt(drp_month) - 1);
            if (drp_month == "1") {
                next_year = (parseInt(drp_year) - 1);
                next_month = 12;
            }
            $("#title").html(next_year + "." + next_month + ".16 - " + drp_year + "." + drp_month + ".15");

            //增加背景色ffffcc
            $("#tbl>tbody>tr[hs]").each(function (i, k) {
                var hs = $(k).attr("hs");
                var hjindex = $(k).index();
                var sum = $(k).find("td:eq(3)").text();
                if (parseFloat(sum) > 15) {
                    var startindex = hjindex - parseInt(hs);
                    $("#tbl>tbody>tr:gt(" + startindex + "):lt(" + (hjindex) + ")").css("background-color", "#ffffcc");
                }
            });

            //只有考勤管理员才可以修改数据
            if ($("#ctl00_ContentPlaceHolder1_isapplymanager").val() == "True") {
                $("#tbl_btn").css("display", "");
                $("#btn_lock").show();
                //添加文本框,只触发一次，或者移除绑定事件
                $("#tbl>tbody>tr>td[rel]:not(td[style*='green'])").bind("dblclick", function () {
                    //var $td0 = $(this).parent().parent().find("tr:eq(0)>td:eq(" + $(this).index() + ")");
                    //  var col_year = $td0.attr("year"); //年
                    //   var col_month = $td0.attr("month"); //月
                    //   var col_day = $td0.text(); //日
                    var zhi = $(this).attr("value"); //
                    $(this).html("<span class='input-group'><input type='text' memid='" + $(this).attr("rel") + "'  datatype='" + $(this).attr("datatype") + "' value='" + zhi + "' class='form-control input-sm' style='width:50px;'/><a href='javascript:void(0)' old='" + zhi + "' style='color:red;font-weight:bold;font-size:16px;' class='qx'>×</a></span>");
                    //移除绑定事件
                    // $(this).unbind("dblclick");
                });
                $(".qx").live("click", function () {
                    var zhi = $(this).attr("old");
                    $(this).parents("td").html(zhi);
                });
            } else {
                $(":text[datatype='beizhu']", "#tbl").attr("readonly", true);
            }


            //保存
            $("#btn_save").click(function () {
                var count = 0;
                var arr = new Array();
                //修改数据
                $(":text:not(input[datatype='beizhu'])", "#tbl").each(function (i, n) {
                    //验证数字
                    if (!reg_math.test($(n).val())) {
                        alert("请输入数字");
                        count++;
                        return false;
                    }
                    else {
                        var obj = {
                            mem_id: $(n).attr("memid"),
                            dataYear: $("#ctl00_ContentPlaceHolder1_drp_year").val(),
                            dataMonth: $("#ctl00_ContentPlaceHolder1_drp_month").val(),
                            dataValue: $(n).val(),
                            dataType: $(n).attr("datatype"),
                            dataSource: "MonthSummary",
                            dataContent: ""
                        };
                        arr.push(obj);
                        //隐藏数据文本框
                        var zhi = $(n).val();
                        $(n).parents("td").html(zhi).css("background-color", "yellow");
                    }

                });

                //备注
                $(":text[datatype='beizhu']", "#tbl").each(function (i, n) {
                    var obj = {
                        mem_id: $(n).attr("memid"),
                        dataYear: $("#ctl00_ContentPlaceHolder1_drp_year").val(),
                        dataMonth: $("#ctl00_ContentPlaceHolder1_drp_month").val(),
                        dataValue: "0",
                        dataType: $(n).attr("datatype"),
                        dataSource: "MonthSummary",
                        dataContent: $(n).val()
                    };
                    arr.push(obj);
                });

                if (count == 0 && arr.length > 0) {
                    $.post("/HttpHandler/Calendar/CalendarHandler.ashx", { "action": "updatedata", "data": Global.toJSON(arr) }, function (result) {
                        if (result == "1") {
                            alert("保存成功！");
                            $("#ctl00_ContentPlaceHolder1_btn_search").click();
                        }
                        else {
                            alert("保存失败！");
                        }
                    });

                }

            });

            //数字验证正则    
            var reg_math = /^[+-]?\d+(\.\d+)?$/;
            $(":text:not(input[datatype='beizhu'])", "#tbl").live("change", function () {
                var _inputValue = $(this).val();
                if (!reg_math.test(_inputValue)) {
                    alert("请输入数字");
                    $(this).val("0");
                }
            });

            //显示加载中
            $("#ctl00_ContentPlaceHolder1_btn_search").click(function () {
                $('body').modalmanager('loading');
            });
            //默认隐藏
            $("#ctl00_ContentPlaceHolder1_btn_export").hide();
            $("#ctl00_ContentPlaceHolder1_btn_allexport").hide();
            //权限控制
            if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "1") {
                //全部
                $("#ctl00_ContentPlaceHolder1_btn_export").show();
                $("#ctl00_ContentPlaceHolder1_btn_allexport").show();
            }
            else if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "2") {
                //部门
                $("#ctl00_ContentPlaceHolder1_btn_export").show();
            }
            //部门导出
            $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {
                //  $("body").modalmanager('loading');
                //导出放在一个隐藏的iframe中，这样不影响当前页面上脚本的运行，才可以隐藏遮罩层
                // querySession();
            });
            //全部导出
            $("#ctl00_ContentPlaceHolder1_btn_allexport").click(function () {
                $("body").modalmanager('loading');
                //导出放在一个隐藏的iframe中，这样不影响当前页面上脚本的运行，才可以隐藏遮罩层
                setTimeout("querySession()", 10000);
            });

            //锁定
            $("#btn_lock").click(function () {
                $(":text:not(input[datatype='beizhu'])", "#tbl").each(function (i, n) {

                    //验证数字
                    if (!reg_math.test($(n).val())) {
                        alert("请输入数字");
                        return false;
                    }
                    else {
                        //隐藏数据文本框
                        var zhi = $(n).val();
                        $(n).parents("td").html(zhi);
                    }
                });

                var date = drp_year + "-" + (parseInt(drp_month) < 10 ? "0" + drp_month : drp_month);
                var arr = new Array();
                //循环日期
                $("#tbl tr:gt(0)").each(function (i, n) {

                    var mem_id = $(n).find("td:eq(-4)").attr("mem_id");
                    var memname = $(n).find("td:eq(-4)").text();
                    var mem_unitid = $(n).find("td:eq(-4)").attr("mem_unitid");
                    var iswdkjl = $(n).find("td:eq(-4)").attr("iswdkjl");

                    var obj = {
                        mem_id: mem_id,
                        mem_name: memname,
                        mem_unit_ID: mem_unitid,
                        dataDate: date,
                        dataYear: drp_year,
                        dataMonth: drp_month,
                        overtime: $.trim($(n).find("td:eq(-3)").text()),
                        weekovertime: $.trim($(n).find("td:eq(-2)").text()),
                        content: $.trim($(n).find("td:eq(-1) input").val()),
                        isiswdkjl: $.trim(iswdkjl)
                    };
                    arr.push(obj);

                });


                $.post("/HttpHandler/Calendar/CalendarHandler.ashx", { "action": "lock", "source": "MonthSummary", "data": Global.toJSON(arr) }, function (result) {
                    if (result == "1") {
                        alert("锁定成功！");
                    }
                    else {
                        alert("锁定失败！");
                    }
                });
            });

        });
        function querySession() {
            var retValue = $.ajax({ url: '/HttpHandler/Calendar/CalendarHandler.ashx?action=GetDownloadState&Random=' + Math.floor(Math.random() * (1000000 + 1)), type: 'GET', async: false, cache: false }).responseText;
            //返回0代表Excel还没有准备完成，否则返回1，准备完成删除sesson,隐藏遮罩
            if (retValue * 1 == 0) {
                //一秒执行一次
                setTimeout("querySession()", 5000);
            }
            else {
                //隐藏遮罩层
                $("body").modalmanager('removeLoading');
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>考勤月汇总</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">考勤汇总分析</a><i class="fa fa-angle-right"> </i><a href="#">考勤月汇总</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询条件
                    </div>
                     <div class="actions">
                        <asp:Button CssClass="btn btn-sm red " ID="btn_export" runat="Server" Text="部门导出" OnClick="btn_export_Click"></asp:Button>&nbsp;<asp:Button CssClass="btn btn-sm red " ID="btn_allexport" runat="server" OnClick="btn_allexport_Click" Text="全部导出"></asp:Button>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                            <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <%-- <asp:ListItem Value="-1">---全部---</asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
                            <td style="width: 50px;">月份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
                            <td>
                                <asp:Button CssClass="btn blue" Text="查询" ID="btn_search" OnClick="btn_search_Click" runat="server" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                     <div class="actions">
                        <input class="btn btn-sm red " style="display:none;" id="btn_lock" type="button" value="锁定" />
                    </div>
                </div>
                <div class="portlet-body form">
                    <table border="0" cellspacing="0" cellpadding="0" style="width: 90%; margin: 0 auto;">
                        <tr>
                            <td id="title" style="text-align: center; font-size: 14px; width:600px; font-weight: bold; line-height: 35px;"></td>
                       <Td>&nbsp;</Td>
                             </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" width="90%" >
                        <tr>
                            <td >
                                <table id="tbl" class="table table-bordered table-responsive table-hover" style="width: 600px; margin: 0 auto; margin-bottom: 0px;">
                                    <tr style="background-color: #f5f5f5; text-align: center; font-weight: bold;">
                                        <td style="width: 100px;">部门名称</td>
                                        <td style="width: 50px;">序号</td>
                                        <td style="width: 80px;">姓名</td>
                                        <td style="width: 100px;">加班(小时)</td>
                                        <td style="width: 100px;">周加班(小时)</td>
                                        <td style="width: 120px;">提醒</td>
                                    </tr>
                                    <asp:Literal ID="lithtml" runat="server"></asp:Literal>
                                </table>
                            </td>
                            <td style="vertical-align:top;">
                                <table style="width: 100%; margin: 0 auto;">
                                    <asp:Literal ID="lithtml2" runat="server"></asp:Literal>
                                </table>
                            </td>
                        </tr>
                    </table>


                    <table id="tbl_btn" border="0" cellspacing="0" cellpadding="0" style="width: 90%; margin: 0 auto; display: none;">
                        <tr>
                            <td style="text-align: center; line-height: 35px; width:600px;">
                                <input id="btn_save" type="button" class="btn blue" value="保存" /></td>
                            <TD></TD>
                        </tr>
                    </table>
                </div>
            </div>


        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="isapplymanager" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />


</asp:Content>
