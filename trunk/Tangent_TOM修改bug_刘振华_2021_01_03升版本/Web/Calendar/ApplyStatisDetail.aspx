﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ApplyStatisDetail.aspx.cs" Inherits="TG.Web.Calendar.ApplyStatisDetail" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
      <link href="../js/fixtablehead/css/component.css" rel="stylesheet" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script src="../js/Calendar/ApplyStatisDetail.js"></script>
     <script src="../js/fixtablehead/jquery.ba-throttle-debounce.min.js"></script>
    <style type="text/css">
        .sticky-thead th {
            background-color: #4b8df8;
        }

        /*AspNetPager*/
        .pages {
            color: black;
            font-size: 10pt;
            margin-top: 3px;
        }

            .pages a, .pages .cpb {
                text-decoration: none;
                float: left;
                padding: 0 5px;
                border: 1px solid #ddd;
                background: #ffff;
                margin: 0 2px;
                font-size: 10pt;
                color: #000;
               
            }

                .pages a:hover {
                    background-color: #E61636;
                    color: #fff;
                    border: 1px solid #E61636;
                    text-decoration: none;
                }

            .pages .cpb {
                font-weight: bold;
                color: #fff;
                background: #E61636;
                border: 1px solid #E61636;
            }

            .pages .indexbox {
                height: 20px;
            }

            .pages .submitbtn {
                height: 20px;
                width: 30px;
                border: solid 1px black;
            }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>考勤统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">考勤统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>统计条件
                    </div>
                    <div class="actions">
                        <asp:Button CssClass="btn btn-sm red " ID="btn_export" runat="Server" Text="部门导出" OnClick="btn_export_Click"></asp:Button>&nbsp;<asp:Button CssClass="btn btn-sm red " ID="btn_allexport" runat="server" OnClick="btn_allexport_Click" Text="全部导出"></asp:Button>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                             <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                              <td style="width: 50px;">人员:</td>
                             <td style="width: 100px;">                                
                                <select id="drp_user" runat="server" class="form-control" style="width:100px;">
                                    <option value="-1">---全部---</option>
                                </select>
                            </td>
                            <td style="width: 50px;" align="right">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
                            <td style="width: 50px;" align="right">月份:</td>
                            <td style="width: 80px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="60px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
  
                            <td>
                                <asp:Button ID="btn_seach" CssClass="btn  blue" Text="查询" runat="server" OnClick="btn_seach_Click" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>统计结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <input class="btn btn-sm red " style="display:none;" id="btn_lock" type="button" value="锁定" />
                    </div>
                </div>
                <div class="portlet-body form">
                  <table border="0" cellspacing="0" cellpadding="0" style="width:99%;margin:0 auto;">
                  	<tr>
                  		<td id="title" style="text-align:center; font-size:14px; font-weight:bold; line-height:35px;"></td>
                  	</tr>
                  </table>
                   <table id="tbl" style=" width:99%;margin:0 auto;margin-bottom:0px;" class="table table-bordered table-responsive table-hover table-full-width">
                   	<asp:Literal ID="lit" runat="server"></asp:Literal>
                      
                   </table>
                    <table id="tbl_btn" border="0" cellspacing="0" cellpadding="0" style="width:99%;margin:0 auto;display:none;">
                  	<tr>
                  		<td style="text-align:center;line-height:35px;"><input id="btn_save" type="button" class="btn blue" value="保存"  /></td>
                  	</tr> 
                  </table>
                </div>
              
            </div>
           <%-- <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                CustomInfoSectionWidth="35%" OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox"
                PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left"
                ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到"
                PageIndexBoxStyle="width:25px;" PageSize="10" SubmitButtonClass="submitbtn">
            </webdiyer:AspNetPager>--%>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="HiddenUserName" Value="" />
    <asp:HiddenField runat="server" ID="isapplymanager" Value="" />
    <asp:HiddenField ID="hid_userid" runat="server" Value="-1" />
</asp:Content>
