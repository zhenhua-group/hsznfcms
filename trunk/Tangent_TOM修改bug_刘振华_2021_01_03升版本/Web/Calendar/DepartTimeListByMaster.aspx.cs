﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;

namespace TG.Web.Calendar
{
    public partial class DepartTimeListByMaster : PageBase
    {
        //审批人ID
        public int AuditUserID
        {
            get
            {
                int userid = 0;
                int.TryParse(Request["AuditUserID"] ?? "0", out userid);
                return userid;
            }
        }
        //申请ID
        public int applyID
        {
            get
            {
                int appid = 0;
                int.TryParse(Request["applyID"] ?? "0", out appid);
                return appid;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                StringBuilder sb = new StringBuilder("");
                //检查权限
                GetPreviewPowerSql(ref sb);

                this.hid_where.Value = sb.ToString();

                if (IsApplyManager)
                {
                    this.btn_done.Visible = true;
                }
                else
                {
                    this.btn_done.Visible = false;
                }
                //特殊审批人
                string spname = ConfigurationManager.AppSettings["SpecialPersonAudit"];
                SpecialPersonAudit.Value = spname;
                spname = "'" + (spname.Replace(",", "','")) + "'";
                List<TG.Model.tg_member> model_memlist = new TG.BLL.tg_member().GetModelList(" mem_Name in (" + spname + ") order by mem_ID asc");
                if (model_memlist != null && model_memlist.Count > 0)
                {
                    string idlist = String.Join(",", model_memlist.Select(a => a.mem_ID + "").ToArray<string>());
                    SpecialPersonAuditID.Value = idlist;
                }
                //特殊人
                SpecialPerson.Value = ConfigurationManager.AppSettings["SpecialPerson"];

            }

        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();

            string sqlwhere = "";

            if ((base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2) )
            {
                sqlwhere = " and unit_ID IN (Select top 1 mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_ID=" + UserSysNo + " order by mr.mem_unit_ID desc)";
            }

            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 and unit_ID not in (" + NotShowUnitList + ")" + sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定          
            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }

            for (int i = 1; i <= 12; i++)
            {
                this.drp_month.Items.Add(i.ToString());
            }

        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }

            //月份
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
                this.isapplymanager.Value = base.IsApplyManager.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    sb.Append(" AND (adduser =" + UserSysNo + ") ");
                }//部门
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    sb.Append(" AND adduser in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=(select top 1 mem_Unit_ID from tg_memberRole where mem_ID=" + UserSysNo + " order by mem_Unit_ID desc))");
                }
           
        }
    }

}