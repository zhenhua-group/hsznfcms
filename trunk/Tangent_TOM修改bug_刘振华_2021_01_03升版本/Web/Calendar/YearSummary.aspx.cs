﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Configuration;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace TG.Web.Calendar
{
    public partial class YearSummary : PageBase
    {
        //人员名称
        public string memData { get; set; }
        //加班值
        public string overtimeData { get; set; }

        #region 组内平均表基本设置
        /// <summary>
        /// Y坐标数据
        /// </summary>
        public string xAxis { get; set; }
        /// <summary>
        /// y坐标数据
        /// </summary>
        public string yAxis { get; set; }
        /// <summary>
        /// 图例
        /// </summary>
        public string LegendData { get; set; }
        /// <summary>
        /// 统计数据
        /// </summary>
        public string SeriesData { get; set; }


        #endregion
        #region 图表统计数据 2017年5月10日
        /// <summary>
        /// 获取图例数据
        /// </summary>
        protected void GetLegendValue()
        {
            //范围图
            StringBuilder legendData = new StringBuilder();
            //数据图例
            legendData.Append("[");

            if (!string.IsNullOrEmpty(memData))
            {

                string[] list = memData.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < (list.Count()); i++)
                {

                    legendData.AppendFormat("\"{0}\",", list[i]);
                }
                //x坐标
                legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
            }

            legendData.Append("]");
            LegendData = legendData.ToString();



        }

        /// <summary>
        ///  获取X坐标数据
        /// </summary>
        private void GetxAxisValue()
        {
            //横向坐标
            StringBuilder sbxAxis = new StringBuilder();

            sbxAxis.Append("[");

            int drpmonth = 2, nextmonth = 7;
            string temp = "";
            if (this.drp_month.SelectedValue == "1")
            {
                drpmonth = 8;
                nextmonth = 12;
                temp = "\"1月\",";
            }
            for (int i = drpmonth; i <= nextmonth; i++)
            {
                sbxAxis.Append("\"" + i + "月\",");
            }
            sbxAxis.Append(temp);
            //x坐标
            sbxAxis.Remove(sbxAxis.ToString().LastIndexOf(','), 1);
            sbxAxis.Append("]");

            xAxis = sbxAxis.ToString();


        }

        /// <summary>
        /// 设置Y坐标数据
        /// </summary>
        private void SetyAxisValue()
        {
            StringBuilder sbyAxis = new StringBuilder();

            sbyAxis.Append(@"{
                            type : 'value',
                            name : '月加班小时',
                            axisLabel : {
                                formatter: '{value} 小时'}
                        }");

            yAxis = sbyAxis.ToString();


        }

        /// <summary>
        /// 获取实际数据
        /// </summary>
        private void GetSeriesData()
        {
            StringBuilder sbSeries = new StringBuilder();

            //加载数据 
            if (!string.IsNullOrEmpty(overtimeData))
            {
                string[] memlist = memData.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string[] list = overtimeData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < list.Count(); i++)
                {
                    string strData = GetCountDataByYear(list[i]);
                    sbSeries.AppendFormat(@"{{name:'{0}',
                                        type:'line',
                                        data:{1},                                      
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", memlist[i], strData);
                }

                sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
            }
            //返回数据
            SeriesData = sbSeries.ToString();

        }
        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="slttype"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        private string GetCountDataByYear(string type)
        {
            //横向坐标
            //统计值
            StringBuilder sbyAxis = new StringBuilder();
            sbyAxis.Append("[");
            if (!string.IsNullOrEmpty(type))
            {

                string[] list = type.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < (list.Count()); i++)
                {
                    sbyAxis.Append(list[i] + ",");
                }
                //y坐标
                sbyAxis.Remove(sbyAxis.ToString().LastIndexOf(','), 1);
            }


            sbyAxis.Append("]");

            return sbyAxis.ToString();
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                BindData();
                //统计图表
                GetLegendValue();
                GetxAxisValue();
                SetyAxisValue();
                GetSeriesData();
            }

        }
        private DataTable BindWhere(string strtype)
        {
            StringBuilder sb = new StringBuilder(" 1=1 ");
            //导出全部部门不需要判断
            if (strtype != "all")
            {
                //部门
                //  if (this.drp_unit.SelectedIndex > 0)
                //  {
                sb.Append(" AND t.Unit_ID=" + this.drp_unit.SelectedValue + " ");
                //  }
            }
            //检查权限
            GetPreviewPowerSql(ref sb);

            sb.Append(" and t.unit_ParentID<>0 and t.unit_ID<>230  and t.unit_ID not in (" + NotShowUnitList + ") order by te.unit_Order asc,t.unit_ID asc ");

            string sql = "select t.* from tg_unit t join tg_unitExt te on  te.unit_ID=t.unit_ID where " + sb.ToString();
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            return dt;
        }
        //根据条件查询
        private void BindData()
        {

            DataTable dt = BindWhere("");
            //生成table
            CreateTable(dt);
        }
        //考勤
        string towork = "09:00", offwork = "17:30";
        List<TG.Model.cm_PersonAttendSet> pas_list = new List<TG.Model.cm_PersonAttendSet>();
        DataTable dt_time = new DataTable();
        //考勤统计手动修改数据
        List<TG.Model.cm_ApplyStatisData> asd_list = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource in ('StatisDetail','MonthSummary') order by id desc");
        //获取锁定数据
        List<TG.Model.cm_MonthSummaryHis> his_list = new TG.BLL.cm_MonthSummaryHis().GetModelList("");

        public void CreateTable(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            string drpyear = this.drp_year.SelectedValue;
            int nextyear = Convert.ToInt32(drpyear);
            int drpmonth = 2, nextmonth = 7;
            if (this.drp_month.SelectedValue == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) + 1);
                drpmonth = 8;
                nextmonth = 12;
            }

            string aa = ConfigurationManager.AppSettings["NoWork"];
            string nowork = "'" + (aa.Replace(",", "','")) + "'";
            if (dt != null && dt.Rows.Count > 0)
            {

                //后台设置上班时间
                pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList("");
                //申请加班记录
                dt_time = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype in ('travel','gomeet','addwork','forget') ").Tables[0];

                int row = 1;
                string strsort = "";
                foreach (DataRow item in dt.Rows)
                {
                    //把钟志宏添加到结构部 
                    string where = "";
                    if (item["unit_ID"].ToString() == "237")
                    {
                        where = " mem_ID=1445 or ";
                    }
                    //循环人员
                    List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(where + " (mem_id in (select mem_Id  from tg_memberRole where mem_Unit_ID=" + item["unit_ID"] + ") and mem_Name not in (" + nowork + ") and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + (drpyear + "-" + "0" + (drpmonth - 1) + "-16") + "')))  order by mem_Order asc,mem_ID asc");
                    if (list != null && list.Count > 0)
                    {

                        for (int i = 0; i < list.Count; i++)
                        {
                            decimal sumovertime = 0;
                            decimal weekovertime = 0;
                            string monthdata = "";

                            sb.Append("<tr >");
                            if (i == 0)
                            {
                                sb.Append("<td align=\"center\" rowspan='" + (list.Count) + "' >" + item["unit_Name"] + "</td>");
                            }
                            sb.Append("<td align=\"center\" >" + row + "</td>");
                            sb.Append("<td align=\"center\" >" + list[i].mem_Name + "</td>");

                            for (int m = drpmonth; m <= nextmonth; m++)
                            {

                                DateTime starttime = Convert.ToDateTime(drpyear + "-" + (m - 1).ToString().PadLeft(2, '0') + "-16");
                                DateTime endtime = Convert.ToDateTime(drpyear + "-" + m.ToString().PadLeft(2, '0') + "-15");

                                decimal overtime = 0;

                                TG.Model.cm_MonthSummaryHis his_mem = null;
                                if (his_list != null && his_list.Count > 0)
                                {
                                    his_mem = his_list.Where(h => h.mem_id == list[i].mem_ID && h.dataDate == endtime.ToString("yyyy-MM")).OrderByDescending(h => h.ID).FirstOrDefault();
                                }
                                //人员数据
                                if (his_mem != null)
                                {
                                    overtime = Convert.ToDecimal(his_mem.overtime);
                                }
                                else
                                {
                                    //  if (list[i].mem_Name != "朱昀" && list[i].mem_Name != "肖显明")
                                    //  {
                                    overtime = GetOverTime(Convert.ToInt32(list[i].mem_ID), list[i].mem_Name, starttime, endtime);
                                    //   }


                                    //手动修改加班
                                    if (asd_list != null && asd_list.Count > 0)
                                    {
                                        //加班
                                        var data_model = asd_list.Where(d => d.dataSource == "MonthSummary" && d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                        if (data_model != null)
                                        {
                                            overtime = data_model.dataValue;

                                        }
                                    }
                                }
                                sb.Append("<td align=\"center\" >" + overtime.ToString("f1") + "</td>");
                                sumovertime = sumovertime + overtime;
                                //图表数据
                                monthdata = monthdata + Math.Round(overtime, 1) + ",";

                            }
                            if (this.drp_month.SelectedValue == "1")
                            {
                                //下半年包含下一年1月份
                                DateTime starttime = Convert.ToDateTime(drpyear + "-12-16");
                                DateTime endtime = Convert.ToDateTime(nextyear + "-01-15");

                                decimal overtime = 0;
                                  TG.Model.cm_MonthSummaryHis his_mem = null;
                                if (his_list != null && his_list.Count > 0)
                                {
                                    his_mem = his_list.Where(h => h.mem_id == list[i].mem_ID && h.dataDate == endtime.ToString("yyyy-MM")).OrderByDescending(h => h.ID).FirstOrDefault();
                                }
                                //人员数据
                                if (his_mem != null)
                                {
                                    overtime = Convert.ToDecimal(his_mem.overtime);
                                }
                                else
                                {
                                    //  if (list[i].mem_Name != "朱昀" && list[i].mem_Name != "肖显明")
                                    //  {
                                    overtime = GetOverTime(Convert.ToInt32(list[i].mem_ID), list[i].mem_Name, starttime, endtime);
                                    //  }
                                    //手动修改加班
                                    if (asd_list != null && asd_list.Count > 0)
                                    {
                                        //加班
                                        var data_model = asd_list.Where(d => d.dataSource == "MonthSummary" && d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                        if (data_model != null)
                                        {
                                            overtime = data_model.dataValue;

                                        }
                                    }
                                }
                                sb.Append("<td align=\"center\" >" + overtime.ToString("f1") + "</td>");
                                sumovertime = sumovertime + overtime;
                                //图表数据
                                monthdata = monthdata + Math.Round(overtime, 1) + ",";
                            }
                            //周加班
                            weekovertime = Math.Round(sumovertime / (decimal.Parse("4.28") * 6), 1);
                            sb.Append("<td align=\"center\" >" + sumovertime.ToString("f1") + "</td>");
                            sb.Append("<td align=\"center\" >" + (sumovertime / 6).ToString("f1") + "</td>");
                            sb.Append("<td align=\"center\" >" + weekovertime + "</td>");
                            sb.Append("<td align=\"center\" ></td>");
                            sb.Append("</tr>");
                            //排名
                            strsort = strsort + weekovertime + ",";
                            row++;

                            //图表数据
                            memData = memData + list[i].mem_Name.Trim() + ",";
                            //删除最后一个逗号
                            monthdata = monthdata.Remove(monthdata.LastIndexOf(','), 1);
                            overtimeData = overtimeData + monthdata + "|";
                        }


                    }
                    if (strsort != "")
                    {
                        strsort = strsort.Substring(0, (strsort.Length - 1)) + "|";
                    }

                }
                this.hid_sort.Value = strsort;
            }
            else
            {
                sb.Append("<tr><td colspan='19' style='color:red;'>没有数据！</td></tr>");
            }


            lithtml.Text = sb.ToString();
        }
        //获取加班时间
        public decimal GetOverTime(int mem_id, string mem_name, DateTime starttime, DateTime endtime)
        {
            decimal sumtime_over = 0;            //申请出差加班

            //if (dt_time != null && dt_time.Rows.Count > 0)
            //{
            //    DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype='travel' and starttime>='" + starttime + "' and starttime<='" + (endtime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
            //    if (dt_over != null && dt_over.Rows.Count>0)
            //    {
            //        foreach (DataRow dr in dt_over.Rows)
            //        {
            //            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
            //            time_over = time_over + totaltime_over;
            //        }
            //    }

            //}

            ////加班离岗    
            //if (dt_time != null && dt_time.Rows.Count > 0)
            //{
            //    DataTable dt_addwork = new DataView(dt_time) { RowFilter = "applytype='addwork' and starttime>='" + starttime + "' and starttime<='" + (endtime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
            //    if (dt_addwork != null && dt_addwork.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt_addwork.Rows)
            //        {
            //            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
            //            time_over = time_over - totaltime_over;
            //        }
            //    }
            //}

            if (pas_list != null && pas_list.Count > 0)
            {
                List<TG.Model.cm_PersonAttendSet> list = pas_list.Where(p => p.mem_ID == mem_id && p.attend_year == endtime.Year && p.attend_month == endtime.Month).ToList();
                if (list != null && list.Count > 0)
                {
                    towork = list[0].ToWork;
                    offwork = list[0].OffWork;
                }

            }


            //考勤加班
            DataTable datatable = GetCurrentMonth((endtime.Year + endtime.Month.ToString().PadLeft(2, '0')), "1");

            //打卡统计
            //
            DataTable dt_late = new DataTable();
            if (datatable != null && datatable.Rows.Count > 0)
            {
                DateTime tomr_endtime = endtime;
                dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + mem_name + "'" }.ToTable();
                for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddDays(1))
                {
                    decimal time_over = 0;
                    DateTime shijioff = starttime;
                    DateTime houtai = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                    DateTime houtaioff = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                    DateTime tomr_shijioff = shijioff;

                    DataTable dt_currt = new DataView(dt_late) { RowFilter = "CHECKTIME>='" + (shijioff.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();
                    if (dt_currt != null && dt_currt.Rows.Count > 0)
                    {
                        //上班时间
                        // DateTime ontime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 09:00");
                        //中午下班时间
                        DateTime zwxbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 11:50");
                        //中午上班时间
                        DateTime zwsbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 13:00");
                        //下午下班时间
                        //  DateTime offtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 17:30");

                        DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_currt.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                        shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_currt.Rows[(dt_currt.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间

                        //判断是否有未打卡申请记录        
                        decimal totaltime_forget = 0;
                        if (dt_time != null && dt_time.Rows.Count > 0)
                        {
                            tomr_shijioff = starttime;
                            DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype='forget' and starttime>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and endtime<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and adduser=" + mem_id + "" }.ToTable();
                            if (dt_over != null && dt_over.Rows.Count > 0)
                            {
                                totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                            }
                        }
                        
                        //判断是否假期加班
                        if (isJQ(starttime))
                        {
                            int mine = shiji.Minute;
                            DateTime jiaban = shiji;
                            if (mine > 0 && mine < 15)
                            {
                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                            }
                            else if (mine > 30 && mine < 45)
                            {
                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                            }

                           
                            //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                            if (totaltime_forget == 0)
                            {
                                time_over = time_over + GetOver(jiaban, shijioff);
                            }

                            //节假日全天
                          //  time_over = time_over + GetOver(jiaban, shijioff);
                            //全天
                            //if (shiji <= houtai && shijioff >= houtaioff)
                            //{
                            //    time_over = time_over + decimal.Parse("7.5");
                            //}
                            //else
                            //{
                            //    DateTime start = shiji;
                            //    DateTime end = shijioff;
                            //    //加班统计   
                            //    if (shiji < houtai)
                            //    {
                            //        start = houtai;
                            //    }
                            //    else if (shiji >= zwxbtime && shiji <= zwsbtime)
                            //    {
                            //        shiji = zwsbtime;
                            //    }
                            //    if (shijioff > houtaioff)
                            //    {
                            //        end = houtaioff;
                            //    }
                            //    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
                            //    {
                            //        end = zwxbtime;
                            //    }

                            //    //都是上午打卡    
                            //    if (end <= zwxbtime)
                            //    {
                            //        end = shijioff;
                            //        time_over = time_over + GetOver(start, end);
                            //    }
                            //    else //下班打卡是下午
                            //    {
                            //        //上班打卡也是下午
                            //        if (start >= zwsbtime)
                            //        {
                            //            time_over = time_over + GetOver(start, end);
                            //        }
                            //        else//上班打卡是上午
                            //        {
                            //            //上午加班时间
                            //            time_over = time_over + GetOver(start, zwxbtime);
                            //            //下午加班时间
                            //            time_over = time_over + GetOver(zwsbtime, end);
                            //        }
                            //    }
                            //}

                        }
                        else
                        {
                            //工作日只有晚上加班，
                            //晚上加班
                            DateTime jiaban = houtaioff.AddMinutes(30);

                            if (shiji > jiaban && dt_currt.Rows.Count > 1)
                            {
                                int mine = shiji.Minute;
                                if (mine > 0 && mine < 15)
                                {
                                    jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                }
                                else if (mine > 30 && mine < 45)
                                {
                                    jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                }
                                else
                                {
                                    jiaban = shiji;
                                }

                            }

                            //加班统计                       
                            if (shijioff > jiaban)
                            {
                                //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                if (totaltime_forget == 0)
                                { 
                                    time_over = time_over + GetOver(jiaban, shijioff);
                                }
                               
                            }
                        }


                    }

                    //申请加班记录
                    if (dt_time != null && dt_time.Rows.Count > 0)
                    {
                        //加班
                        tomr_shijioff = starttime;
                        DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype in ('travel','gomeet','forget') and starttime>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and endtime<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and adduser=" + mem_id + "" }.ToTable();
                        if (dt_over != null && dt_over.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt_over.Rows)
                            {
                                decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                time_over = time_over + totaltime_over;
                            }
                        }
                        //离岗
                        DataTable dt_addwork = new DataView(dt_time) { RowFilter = "applytype='addwork' and starttime>='" + starttime + "' and starttime<='" + (starttime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
                        if (dt_addwork != null && dt_addwork.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt_addwork.Rows)
                            {
                                decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                time_over = time_over - totaltime_over;
                            }
                        }
                    }

                    //考勤统计手动修改记录
                    if (asd_list != null && asd_list.Count > 0)
                    {
                        var data_model = asd_list.Where(d => d.dataSource == "StatisDetail" && d.mem_id == mem_id && d.dataYear == starttime.Year && d.dataMonth == starttime.Month && d.dataDay == starttime.Day && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                        if (data_model != null)
                        {
                            time_over = data_model.dataValue;
                        }
                    }
                    sumtime_over = sumtime_over + time_over;

                }
            }

            return sumtime_over;
        }
        #region
        ////返回加班时间
        //public decimal GetOver(DateTime start, DateTime end)
        //{
        //    decimal over = 0;
        //    if (end > start)
        //    {
        //        TimeSpan ts_jb = end - start;
        //        //强制转换，没有四舍五入
        //        int hours = (int)(ts_jb.TotalHours);

        //        double minutes = ts_jb.TotalMinutes - (hours * 60);
        //        //大于15分钟，即加班
        //        if (minutes > 15 && minutes <= 45)
        //        {
        //            over = decimal.Parse("0.5");
        //        }
        //        else if (minutes > 45 && minutes < 60)
        //        {
        //            over = decimal.Parse("1");
        //        }

        //        over = over + hours;
        //    }
        //    return over;
        //}
        ////判断是否是假期
        //public bool isJQ(DateTime date)
        //{
        //    bool flag = false;
        //    List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" convert(varchar(10),holiday,120)='" + date.ToString("yyyy-MM-dd") + "' order by id");
        //    //存在
        //    if (list != null && list.Count > 0)
        //    {
        //        //节假日，需减1天
        //        if (list[0].daytype == 1)
        //        {
        //            flag = true;
        //        }
        //    }
        //    else
        //    {
        //        //周六日0~6
        //        string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
        //        if (temp == "Sunday" || temp == "Saturday")
        //        {
        //            flag = true;
        //        }
        //    }
        //    return flag;
        //}
        #endregion
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {

            BindData();
            //统计图表
            GetLegendValue();
            GetxAxisValue();
            SetyAxisValue();
            GetSeriesData();


        }

        protected void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt = BindWhere("");

            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/YearSummary.xls", this.drp_unit.SelectedItem.Text.Trim());
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }

        protected void btn_allexport_Click(object sender, EventArgs e)
        {
            //排除离职人员
            DataTable dt = BindWhere("all");

            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/YearSummary.xls", "全院");
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }
        private void ExportDataToExcel(DataTable dt, string modelPath, string pathname)
        {

            string drpyear = this.drp_year.SelectedValue;
            int nextyear = Convert.ToInt32(drpyear);
            int drpmonth = 2, nextmonth = 7;
            //标题
            string title = drpyear + ".02-" + nextyear + ".07月";
            if (this.drp_month.SelectedValue == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) + 1);
                drpmonth = 8;
                nextmonth = 12;

                title = drpyear + ".08-" + nextyear + ".01月";
            }

            pathname = "【" + pathname.Trim() + "】年汇总" + title;

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //   style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //   style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 10;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            //  style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //   style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //   style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            IRow dataRowTitle = ws.GetRow(0);
            dataRowTitle.Height = 30 * 20;
            //ws.SetColumnWidth(2, 25 * 256);         
            ICell acell21 = dataRowTitle.GetCell(0);
            acell21.SetCellValue(pathname);
            // CellRangeAddress cellRangeAddress2 = new CellRangeAddress(1, 1, 0, 1);
            // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            //ws.AddMergedRegion(cellRangeAddress2);
            //月份
            int cols = 3;
            for (int i = drpmonth; i <= nextmonth; i++)
            {
                acell21 = ws.GetRow(1).GetCell(cols);
                acell21.SetCellValue(i + "月");
                cols = cols + 1;
            }
            if (this.drp_month.SelectedValue == "1")
            {
                acell21 = ws.GetRow(1).GetCell(cols);
                acell21.SetCellValue("1月");
            }

            string aa = ConfigurationManager.AppSettings["NoWork"];
            string nowork = "'" + (aa.Replace(",", "','")) + "'";


            //后台设置上班时间
            pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList("");
            //申请加班记录
            dt_time = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype in ('travel','addwork','forget') ").Tables[0];

            int row = 1;
            string strsort = "";
            foreach (DataRow item in dt.Rows)
            {
                //把钟志宏添加到结构部 
                string where = "";
                if (item["unit_ID"].ToString() == "237")
                {
                    where = " mem_ID=1445 or ";
                }
                //循环人员
                List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(where + " (mem_id in (select mem_Id  from tg_memberRole where mem_Unit_ID=" + item["unit_ID"] + ") and mem_Name not in (" + nowork + ") and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + (drpyear + "-" + "0" + (drpmonth - 1) + "-16") + "')))  order by mem_Order asc,mem_ID asc");
                if (list != null && list.Count > 0)
                {

                    for (int i = 0; i < list.Count; i++)
                    {
                        decimal sumovertime = 0;
                        decimal weekovertime = 0;
                        string monthdata = "";

                        var dataRow = ws.GetRow((row + 1));//第一行
                        if (dataRow == null)
                            dataRow = ws.CreateRow((row + 1));//生成行


                        if (i == 0)
                        {

                            ICell cell0 = dataRow.GetCell(0);
                            if (cell0 == null)
                                cell0 = dataRow.CreateCell(0);

                            cell0.SetCellValue(item["unit_Name"].ToString().Trim());
                            CellRangeAddress cellRangeAddress2 = new CellRangeAddress((row + 1), (row + list.Count), 0, 0);
                            // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
                            ws.AddMergedRegion(cellRangeAddress2);

                        }
                        //第一行
                        ICell cell = dataRow.GetCell(1);
                        if (cell == null)
                            cell = dataRow.CreateCell(1);
                        cell.SetCellValue(row);

                        //第二行
                        cell = dataRow.GetCell(2);
                        if (cell == null)
                            cell = dataRow.CreateCell(2);
                        cell.SetCellValue(list[i].mem_Name);

                        int yue = 3;
                        for (int m = drpmonth; m <= nextmonth; m++)
                        {

                            DateTime starttime = Convert.ToDateTime(drpyear + "-" + (m - 1).ToString().PadLeft(2, '0') + "-16");
                            DateTime endtime = Convert.ToDateTime(drpyear + "-" + m.ToString().PadLeft(2, '0') + "-15");

                            decimal overtime = 0;

                                TG.Model.cm_MonthSummaryHis his_mem = null;
                                if (his_list != null && his_list.Count > 0)
                                {
                                    his_mem = his_list.Where(h => h.mem_id == list[i].mem_ID && h.dataDate == endtime.ToString("yyyy-MM")).OrderByDescending(h => h.ID).FirstOrDefault();
                                }
                                //人员数据
                                if (his_mem != null)
                                {
                                    overtime = Convert.ToDecimal(his_mem.overtime);
                                }
                                else
                                {

                                    //  if (list[i].mem_Name != "朱昀" && list[i].mem_Name != "肖显明")
                                    //  {
                                    overtime = GetOverTime(Convert.ToInt32(list[i].mem_ID), list[i].mem_Name, starttime, endtime);
                                    //   }


                                    //手动修改加班
                                    if (asd_list != null && asd_list.Count > 0)
                                    {
                                        //加班
                                        var data_model = asd_list.Where(d => d.dataSource == "MonthSummary" && d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                        if (data_model != null)
                                        {
                                            overtime = data_model.dataValue;

                                        }
                                    }
                                }

                            //第三到八行
                            cell = dataRow.GetCell(yue);
                            if (cell == null)
                                cell = dataRow.CreateCell(yue);
                            cell.SetCellValue(overtime.ToString("f1"));

                            sumovertime = sumovertime + overtime;
                            //图表数据
                            monthdata = monthdata + Math.Round(overtime, 1) + ",";

                            yue = yue + 1;
                        }
                        if (this.drp_month.SelectedValue == "1")
                        {
                            //下半年包含下一年1月份
                            DateTime starttime = Convert.ToDateTime(drpyear + "-12-16");
                            DateTime endtime = Convert.ToDateTime(nextyear + "-01-15");

                            decimal overtime = 0;

                            TG.Model.cm_MonthSummaryHis his_mem = null;
                                if (his_list != null && his_list.Count > 0)
                                {
                                    his_mem = his_list.Where(h => h.mem_id == list[i].mem_ID && h.dataDate == endtime.ToString("yyyy-MM")).OrderByDescending(h => h.ID).FirstOrDefault();
                                }
                                //人员数据
                                if (his_mem != null)
                                {
                                    overtime = Convert.ToDecimal(his_mem.overtime);
                                }
                                else
                                {
                                    //  if (list[i].mem_Name != "朱昀" && list[i].mem_Name != "肖显明")
                                    //  {
                                    overtime = GetOverTime(Convert.ToInt32(list[i].mem_ID), list[i].mem_Name, starttime, endtime);
                                    //  }
                                    //手动修改加班
                                    if (asd_list != null && asd_list.Count > 0)
                                    {
                                        //加班
                                        var data_model = asd_list.Where(d => d.dataSource == "MonthSummary" && d.mem_id == list[i].mem_ID && d.dataYear == endtime.Year && d.dataMonth == endtime.Month && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                        if (data_model != null)
                                        {
                                            overtime = data_model.dataValue;

                                        }
                                    }
                                }

                            sumovertime = sumovertime + overtime;
                            //图表数据
                            monthdata = monthdata + Math.Round(overtime, 1) + ",";

                            cell = dataRow.GetCell(yue);
                            if (cell == null)
                                cell = dataRow.CreateCell(yue);
                            cell.SetCellValue(overtime.ToString("f1"));

                            sumovertime = sumovertime + overtime;
                            //图表数据
                            monthdata = monthdata + Math.Round(overtime, 1) + ",";
                        }
                        //周加班
                        weekovertime = Math.Round(sumovertime / (decimal.Parse("4.28") * 6), 1);
                        //第九行
                        cell = dataRow.GetCell(9);
                        if (cell == null)
                            cell = dataRow.CreateCell(9);
                        cell.SetCellValue(sumovertime.ToString("f1"));
                        //第十行
                        cell = dataRow.GetCell(10);
                        if (cell == null)
                            cell = dataRow.CreateCell(10);
                        cell.SetCellValue((sumovertime / 6).ToString("f1"));
                        //第十一行
                        cell = dataRow.GetCell(11);
                        if (cell == null)
                            cell = dataRow.CreateCell(11);
                        cell.SetCellValue(weekovertime.ToString());

                        //第十二行排序
                        cell = dataRow.GetCell(12);
                        if (cell == null)
                            cell = dataRow.CreateCell(12);
                        cell.SetCellValue("");

                        //排名
                        strsort = strsort + weekovertime + ",";
                        row++;
                    }


                }
                if (strsort != "")
                {
                    strsort = strsort.Substring(0, (strsort.Length - 1)) + "|";
                }

            }
            //排序
            //按部门分组          
            string[] hidsortlist = strsort.Split('|');
            int index = 2;
            //最后有一个空值
            for (var j = 0; j < (hidsortlist.Length - 1); j++)
            {
                //每个部门排名
                string[] sortlist = hidsortlist[j].Split(',');
                string[] arrlist = sortlist;
                arrlist = arrlist.Distinct().OrderByDescending(s => s).ToArray();//去重复，排序
                //  Array.Sort(sortlist);//升序
                // Array.Reverse(sortlist);//反转
                for (var i = 0; i < (sortlist.Length); i++)
                {
                    //索引
                    int count = Array.IndexOf(arrlist, sortlist[i]);

                    //第十二行排序
                    ICell cell = ws.GetRow(index).GetCell(12);
                    cell.SetCellValue(count + 1);
                    index++;
                }
            }



            //写入Session
            Session["SamplePictures"] = true;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }

        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();

            string sqlwhere = "";
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                sqlwhere = " unit_ParentID<>0  and unit_ID not in (" + NotShowUnitList + ") ";
            }
            else
            {
                sqlwhere = " unit_ID=" + UserUnitNo;
            }
            sqlwhere = sqlwhere + " and unit_ID<>230 order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID) asc,unit_ID asc ";
            var unitList = bll_unit.GetModelList(sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");
            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;

            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }


        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人，只有管理员看到全部
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND t.unit_ID =" + UserUnitNo + " ");
            }
            //部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND t.unit_ID =" + UserUnitNo + " ");
            }
        }


    }

}