﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;



namespace TG.Web.Calendar
{
    public partial class YearAllStatis : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                BindProject();
            }

        }
        //绑定人员
        private void BindUser()
        {

            if (this.drp_unit.SelectedIndex > 0)
            {
                string olduserid = this.hid_userid.Value;

                string where = " mem_Unit_ID=" + this.drp_unit.SelectedValue + " ";
                //个人权限
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    where = " mem_Unit_ID=" + this.drp_unit.SelectedValue + " and mem_id=" + UserSysNo + " ";
                }
                where = where + " and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where year(mem_OutTime)>=" + this.drp_year.SelectedValue + ")) order by mem_Order asc,mem_ID asc";

                List<TG.Model.tg_member> dt = new TG.BLL.tg_member().GetModelList(where);
                this.drp_user.DataSource = dt;
                this.drp_user.DataTextField = "mem_Name";
                this.drp_user.DataValueField = "mem_ID";
                this.drp_user.DataBind();

                ListItem li = new ListItem();
                li.Text = "---全部---";
                li.Value = "-1";
                drp_user.Items.Insert(0, li);

                if (olduserid != "-1")
                {
                    if (this.drp_user.Items.FindByValue(olduserid) != null)
                    {
                        this.drp_user.Items.FindByValue(olduserid).Selected = true;
                    }
                }
            }
            else
            {
                drp_user.Items.Clear();
                ListItem li = new ListItem();
                li.Text = "---全部---";
                li.Value = "-1";
                drp_user.Items.Add(li);
            }

        }
        private string BindWhere(string strtype)
        {
            StringBuilder sb = new StringBuilder(" 1=1 ");
            //导出全部部门不需要判断
            if (strtype != "all")
            {
                //部门
                if (this.drp_unit.SelectedIndex > 0)
                {
                    sb.Append(" AND mem_Unit_ID=" + this.drp_unit.SelectedValue + " and mem_Unit_ID not in (" + NotShowUnitList + ") ");
                }
                if (this.drp_user.Value != "-1")
                {
                    sb.Append(" AND mem_ID=" + this.drp_user.Value + " ");
                }
            }
            //排除离职人员
            sb.Append(" and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where year(mem_OutTime)>=" + this.drp_year.SelectedValue + "))");

            //检查权限
            GetPreviewPowerSql(ref sb);
            return sb.ToString();
        }
        //根据条件查询
        private void BindProject()
        {
            BindUser();
            StringBuilder sb = new StringBuilder(BindWhere(""));


            //  TG.BLL.tg_member bllMem = new TG.BLL.tg_member();

            //  int count = int.Parse(bllMem.GetRecordCount(sb.ToString()).ToString());
            //所有记录数
            //  this.AspNetPager1.RecordCount = count;


            //取得查询数据
            DataSet ds = new DataSet();
            string sql = "select *,(select unit_name from tg_unit where unit_id=mem_Unit_ID) as unitname,(select unit_Order from tg_unitExt where unit_ID=mem_Unit_ID) as unitorder from tg_member where " + sb.ToString() + " order by unitorder asc,mem_Order asc,mem_ID asc";
            // ds = TG.DBUtility.DbHelperSQL.Query(sql, (AspNetPager1.StartRecordIndex - 1), AspNetPager1.PageSize);
            ds = TG.DBUtility.DbHelperSQL.Query(sql);
            //明细
            DataTable dt = ds.Tables[0];

            //生成table
            CreateTable(dt);
        }
        List<TG.Model.cm_HolidayConfig> list = new List<TG.Model.cm_HolidayConfig>();
        //考勤
        string towork = "09:00", offwork = "17:30";
        //各类单项统计手动修改数据
        List<TG.Model.cm_ApplyStatisData> list_data_Detail = new List<TG.Model.cm_ApplyStatisData>();
        List<TG.Model.cm_ApplyStatisData> list_data = new List<TG.Model.cm_ApplyStatisData>();
        List<TG.Model.cm_ApplyStatisData> list_data_all = new List<TG.Model.cm_ApplyStatisData>();
        DataTable dt_apply = new DataTable();
        public void CreateTable(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();

            string drpyear = this.drp_year.SelectedValue;
            //string drpmonth = "1";
            //var nextyear = Convert.ToInt32(drpyear);
            //var nextmonth = (Convert.ToInt32(drpmonth) - 1);
            //if (drpmonth == "1")
            //{
            //    nextyear = (Convert.ToInt32(drpyear) - 1);
            //    nextmonth = 12;
            //}

            //2月份
            int days = DateTime.DaysInMonth(Convert.ToInt32(drpyear), 2);
            hid_days.Value = days.ToString();

            if (dt != null && dt.Rows.Count > 0)
            {
                //获取锁定数据
                List<TG.Model.cm_YearAllStatisHis> his_list = new TG.BLL.cm_YearAllStatisHis().GetModelList(" dataDate='" + drpyear + "'");

                //得到所有节假日日期。
                list = new TG.BLL.cm_HolidayConfig().GetModelList("");
                //后台设置上班时间
                List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_year=" + drp_year.SelectedValue);
                //手动修改数据年假统计             
                list_data_all = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='YearAllStatis' order by id desc");
                //手动修改数据年假及带薪假
                list_data = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='YearPaidStatis' order by id desc");
                //各类单项统计手动修改数据
                list_data_Detail = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='StatisDetail' order by id desc");
                //请假数据
                dt_apply = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype not in ('addwork')").Tables[0];
                int row = 1;
                int old_unitid = 0;

                foreach (DataRow item in dt.Rows)
                {
                    int memid = Convert.ToInt32(item["mem_ID"]);
                    string memname = item["mem_Name"].ToString();

                    sb.Append("<tr >");
                    if (old_unitid != Convert.ToInt32(item["mem_Unit_ID"]))
                    {

                        old_unitid = Convert.ToInt32(item["mem_Unit_ID"]);
                        int cls = dt.Select("mem_Unit_ID=" + old_unitid).Count();
                        sb.Append("<td align=\"center\" rowspan='" + (cls) + "' >" + item["unitname"] + "</td>");
                    }

                    //2017年剩余年假
                    decimal totaltime = 0m;

                    //显示2016剩余年假
                    decimal showoldtotaltime = 0m;
                    //修改数据
                    string colorStr_privyear = "";
                    //判断是否手动改过2017剩(小时)
                    string colorStr_currtyear = "";
                    //今年年假
                    int currYearHoliday = 0;
                    string tempStr = "";

                    TG.Model.cm_YearAllStatisHis his_mem = null;
                    if (his_list != null && his_list.Count > 0)
                    {
                        his_mem = his_list.Where(h => h.mem_id == memid).OrderByDescending(h => h.ID).FirstOrDefault();
                    }
                    //人员数据
                    if (his_mem != null)
                    {
                        int i = 0;
                        System.Reflection.PropertyInfo[] properties = his_mem.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
                        foreach (System.Reflection.PropertyInfo prop in properties)
                        {
                            //属性名
                            string name = prop.Name;
                            //属性值
                            // object value = prop.GetValue(his_mem, null);
                            //属性的类型判断、是值类型、属性名中包含“_”
                            if (prop.PropertyType.IsValueType && prop.PropertyType == typeof(decimal?) && name.IndexOf("_") > -1)
                            {
                                string value = prop.GetValue(his_mem, null).ToString();
                                string tStr = name.Substring(5);//截取最后日期
                                tStr = tStr.Insert((tStr.Length - 2), "-");
                                tempStr += "<td align=\"center\"  rel=" + memid + " datatype='" + tStr + "' month='" + (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : (i + 1))) + "' value='" + value + "'>" + value + "</td>";
                                i++;
                            }

                        }
                        currYearHoliday = Convert.ToInt32(his_mem.yearDay);
                        showoldtotaltime = Convert.ToDecimal(his_mem.lastyearHour);
                        totaltime = Convert.ToDecimal(his_mem.nextyearHour);

                    }
                    else
                    {
                        #region 上月剩余年假

                        //去年年假
                        //int oldYearHoliday = 0;

                        //List<TG.Model.cm_PersonAttendSet> paslist = new TG.BLL.cm_PersonAttendSet().GetModelList(" mem_ID=" + item["mem_ID"] + " and attend_month=0 and OffWork='" + (Convert.ToInt32(drpyear) - 1) + "'");
                        //if (paslist != null && paslist.Count > 0)
                        //{
                        //    oldYearHoliday = Convert.ToInt32(paslist[0].ToWork);
                        //}


                        List<TG.Model.tg_memberExtInfo> memextinfo = new TG.BLL.tg_memberExtInfo().GetModelList(" mem_ID=" + item["mem_ID"]);
                        if (memextinfo != null && memextinfo.Count > 0)
                        {
                            currYearHoliday = Convert.ToInt32(memextinfo[0].mem_HolidayYear);
                        }

                        int oldyear = Convert.ToInt32(drpyear) - 1;

                        #endregion



                        string[] timelist = { "12-16~12-31", "1-1~1-15", "1-16~2-15", "2-16~2-" + days + "", "3-1~3-15", "3-16~4-15", "4-16~5-15", "5-16~6-15", "6-16~7-15", "7-16~8-15", "8-16~9-15", "9-16~10-15", "10-16~11-15", "11-16~12-15" };
                        //2016年剩余年假
                        //  decimal oldtotaltime = Convert.ToDecimal(oldYearHoliday * 7.5);
                        decimal oldtotaltime = GetOldYearTime(item["mem_ID"].ToString(), item["mem_Name"].ToString());
                        //2017年剩余年假
                        totaltime = Convert.ToDecimal(currYearHoliday * 7.5);

                        //显示2016剩余年假
                        showoldtotaltime = oldtotaltime;
                        //修改数据
                        colorStr_privyear = "";

                        //判断是否手动改过2016剩余年假
                        if (list_data_all != null && list_data_all.Count > 0)
                        {
                            //事假
                            var data_model = list_data_all.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == 0 && d.dataType == "privyear").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                oldtotaltime = data_model.dataValue;
                                showoldtotaltime = data_model.dataValue;
                                colorStr_privyear = " style='background-color:yellow;'";
                            }
                        }


                        int i = 0;
                        //打卡次数
                        int totallate = 0;
                        foreach (string timeStr in timelist)
                        {
                            //获取某人上下班时间
                            if (pas_list != null && pas_list.Count > 0)
                            {
                                List<TG.Model.cm_PersonAttendSet> pas_list_mem = pas_list.Where(p => p.mem_ID == Convert.ToInt32(item["mem_ID"]) && p.attend_month == (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : i))).ToList();
                                if (pas_list_mem != null && pas_list_mem.Count > 0)
                                {
                                    towork = pas_list_mem[0].ToWork;
                                    offwork = pas_list_mem[0].OffWork;
                                }
                                else
                                {
                                    towork = "09:00";
                                    offwork = "17:30";
                                }
                            }

                            //修改数据
                            string colorStr_month = "";
                            //使用年假
                            decimal value = 0;
                            string[] strlist = timeStr.Split('~');

                            if (i == 4)
                            {
                                int sumlate = 0;
                                //3月份
                                decimal timeslot = GetTreeDataTableTime(Convert.ToInt32(item["mem_ID"]), (i - 1), item["mem_Name"].ToString(), oldyear + "-3-1", oldyear + "-" + strlist[1], ref sumlate);

                                //迟到次数超过5扣半小时事假
                                if (totallate >= 5)
                                {
                                    //后半月又有迟到
                                    if (sumlate > 0)
                                    {
                                        timeslot = timeslot + (sumlate * decimal.Parse("0.5"));
                                    }
                                }
                                else
                                {
                                    if (sumlate > 0 && sumlate < 5)
                                    {
                                        int zh = (totallate + sumlate);
                                        //合计迟到大于5
                                        if (zh > 5)
                                        {
                                            timeslot = timeslot + ((zh - 5) * decimal.Parse("0.5"));
                                        }
                                    }
                                    else if (sumlate >= 5)
                                    {
                                        timeslot = timeslot + (totallate * decimal.Parse("0.5"));
                                    }
                                }

                                //判断是否手动改过，年假统计
                                if (list_data_all != null && list_data_all.Count > 0)
                                {
                                    var data_model = list_data_all.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == (i - 1) && d.dataType == strlist[1]).OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        timeslot = data_model.dataValue;
                                        colorStr_month = " style='background-color:yellow;'";
                                    }

                                }
                                //年假及带薪假统计
                                if (list_data != null && list_data.Count > 0)
                                {
                                    //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月去年年假剩余】
                                    var data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 1) && asd.dataType == "privMonth") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 2) && asd.dataType == "lastMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        oldtotaltime = data_model.dataValue;

                                    }
                                    //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月今年年假剩余】
                                    data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 1) && asd.dataType == "privMonthCurr") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 2) && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        totaltime = data_model.dataValue;

                                    }
                                }

                                if (totaltime >= timeslot)
                                {
                                    value = value + timeslot;
                                    totaltime = totaltime - timeslot;
                                }
                                else if (totaltime > 0 && totaltime < timeslot)
                                {
                                    value = value + totaltime;
                                    totaltime = 0;
                                }

                            }
                            else
                            {
                                int sumlate = 0;
                                decimal timeslot = GetTreeDataTableTime(Convert.ToInt32(item["mem_ID"]), (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : i)), item["mem_Name"].ToString(), oldyear + "-" + strlist[0], oldyear + "-" + strlist[1], ref sumlate);

                                //判断是否手动改过 年假统计
                                if (list_data_all != null && list_data_all.Count > 0)
                                {
                                    var data_model = list_data_all.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : i)) && d.dataType == strlist[1]).OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        timeslot = data_model.dataValue;
                                        colorStr_month = " style='background-color:yellow;'";
                                    }

                                }

                                //年假及带薪假统计
                                if (i > 1)
                                {
                                    if (list_data != null && list_data.Count > 0)
                                    {
                                        //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月去年年假剩余】
                                        var data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i > 3 ? (i - 1) : i) && asd.dataType == "privMonth") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == ((i > 3 ? (i - 1) : i) - 1) && asd.dataType == "lastMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                        if (data_model != null)
                                        {
                                            oldtotaltime = data_model.dataValue;

                                        }
                                        //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月今年年假剩余】
                                        data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i > 3 ? (i - 1) : i) && asd.dataType == "privMonthCurr") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == ((i > 3 ? (i - 1) : i) - 1) && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                        if (data_model != null)
                                        {
                                            totaltime = data_model.dataValue;

                                        }
                                    }
                                }
                                //2016年上一年 12-16~12-31
                                if (i == 0)
                                {
                                    //迟到次数
                                    totallate = sumlate;

                                    //使用去年年假                                       
                                    if (oldtotaltime >= timeslot)
                                    {
                                        //去年剩余年假
                                        oldtotaltime = oldtotaltime - timeslot;
                                        //使用年假
                                        value = timeslot;
                                    }
                                    else
                                    {
                                        value = oldtotaltime;
                                        oldtotaltime = 0;
                                    }



                                }//1.1-2.28
                                else if (i > 0 && i < 4)
                                {
                                    //没有修改数据，加上迟到事假
                                    if (i == 1 && colorStr_month == "")
                                    {
                                        //迟到次数超过5扣半小时事假
                                        if (totallate >= 5)
                                        {
                                            //后半月又有迟到
                                            if (sumlate > 0)
                                            {
                                                timeslot = timeslot + (sumlate * decimal.Parse("0.5"));
                                            }
                                        }
                                        else
                                        {
                                            if (sumlate > 0 && sumlate < 5)
                                            {
                                                int zh = (totallate + sumlate);
                                                //合计迟到大于5
                                                if (zh > 5)
                                                {
                                                    timeslot = timeslot + ((zh - 5) * decimal.Parse("0.5"));
                                                }
                                            }
                                            else if (sumlate >= 5)
                                            {
                                                timeslot = timeslot + (totallate * decimal.Parse("0.5"));
                                            }
                                        }

                                    }

                                    totallate = sumlate;

                                    //2016年年假有剩余
                                    if (oldtotaltime >= timeslot)
                                    {
                                        oldtotaltime = oldtotaltime - timeslot;
                                        value = timeslot;
                                    }
                                    else
                                    {
                                        decimal sy = timeslot;
                                        //2016年年假
                                        if (oldtotaltime > 0 && oldtotaltime < timeslot)
                                        {
                                            value = oldtotaltime;
                                            sy = timeslot - oldtotaltime;
                                            oldtotaltime = 0;

                                        }
                                        //用2017年年假
                                        if (Math.Abs(sy) > 0 && totaltime >= sy)
                                        {
                                            totaltime = totaltime - sy;
                                            value = timeslot;
                                        }
                                        else
                                        {
                                            value = value + totaltime;
                                            totaltime = 0;

                                        }


                                    }



                                }//3.16-12.15
                                else if (i > 4)
                                {

                                    if (totaltime >= timeslot)
                                    {
                                        value = timeslot;
                                        totaltime = totaltime - timeslot;
                                    }
                                    else if (totaltime > 0 && totaltime < timeslot)
                                    {
                                        value = totaltime;
                                        totaltime = 0;
                                    }


                                }

                            }

                            tempStr += "<td align=\"center\" " + colorStr_month + " rel=" + memid + " datatype='" + strlist[1] + "' month='" + (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : i)) + "' value='" + value.ToString("f1") + "'>" + value.ToString("f1") + "</td>";
                            oldyear = Convert.ToInt32(drpyear);
                            i++;
                        }
                        //
                        //判断是否手动改过2017剩(小时)
                        colorStr_currtyear = "";
                        if (list_data_all != null && list_data_all.Count > 0)
                        {
                            //事假
                            var data_model = list_data_all.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == 0 && d.dataType == "currtyear").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                totaltime = data_model.dataValue;
                                colorStr_currtyear = " style='background-color:yellow;'";
                            }
                        }
                    }
                    sb.Append("<td align=\"center\" >" + row + "</td>");
                    sb.Append("<td align=\"center\"  mem_id=" + memid + " mem_unitid=" + item["mem_Unit_ID"] + ">" + item["mem_Name"] + "</td>");
                    sb.Append("<td  align=\"center\" >" + currYearHoliday + "</td>");
                    sb.Append("<td align=\"center\" >" + (currYearHoliday * 7.5) + "</td>");
                    sb.Append("<td align=\"center\" " + colorStr_privyear + " rel=" + memid + " datatype='privyear' month='0' value='" + showoldtotaltime + "'>" + showoldtotaltime + "</td>");
                    sb.Append(tempStr);
                    sb.Append("<td align=\"center\" " + colorStr_currtyear + " rel=" + memid + " datatype='currtyear' month='0' value='" + totaltime.ToString("f1") + "'>" + totaltime.ToString("f1") + "</td>");
                    sb.Append("</tr>");
                    row = row + 1;

                }

            }
            else
            {
                sb.Append("<tr><td colspan='19' style='color:red;'>没有数据！</td></tr>");
            }

            lithtml.Text = sb.ToString();
        }
        //获取考勤统计详细锁定数据
        List<TG.Model.cm_ApplyStatisDetailHis> Detailhis_list = new TG.BLL.cm_ApplyStatisDetailHis().GetModelList("");
        //获取请假时间
        public decimal GetTreeDataTableTime(int memID, int month, string memname, string oldstarttime, string oldendtime, ref int totallate)
        {
            int year = Convert.ToDateTime(oldendtime).Year;
            if (Convert.ToDateTime(oldendtime).Month == 12 && month == 1)
            {
                //上一年12月16-31
                year = year + 1;
            }
            //打卡数据
            DataTable datatable = GetCurrentMonth((year + month.ToString().PadLeft(2, '0')), "1");

            ////考勤申请记录          
            DataTable dt_all = new DataView(dt_apply) { RowFilter = "adduser=" + memID + "" }.ToTable();

            DateTime starttime = Convert.ToDateTime(oldstarttime);
            DateTime endtime = Convert.ToDateTime(oldendtime);
            decimal sumqjtotal = 0;
            int sum_late = 0;

            //筛选当前年，小于当前月之前的，部门活动申请数据 
            decimal sumunittime = 0;
            if (dt_all != null && dt_all.Rows.Count > 0)
            {
                List<TG.Model.cm_ApplyInfo> list_apply = new TG.BLL.cm_ApplyInfo().DataTableToList(dt_all);

                List<TG.Model.cm_ApplyInfo> unit_dt2 = list_apply.Where(a => Convert.ToDateTime(a.starttime.ToString("yyyy-MM-dd")) >= Convert.ToDateTime((Convert.ToInt32(year) - 1) + "-12-16") && Convert.ToDateTime(a.endtime.ToString("yyyy-MM-dd")) < starttime && a.applytype == "depart").ToList();
                if (unit_dt2 != null && unit_dt2.Count > 0)
                {
                    //得到小于一天的申请
                    sumunittime = unit_dt2.Where(s => s.totaltime > 0 && s.totaltime <= Convert.ToDecimal(7.5)).Sum(s => s.totaltime).GetValueOrDefault(0);
                    //大于一天申请，按7.5算
                    sumunittime = sumunittime + ((unit_dt2.Where(s => s.totaltime > Convert.ToDecimal(7.5)).Count()) * Convert.ToDecimal(7.5));
                }
                // sumunittime = list_apply.Where(a => Convert.ToDateTime(a.starttime.ToString("yyyy-MM-dd")) >= Convert.ToDateTime((starttime.Year) + "-12-16") && Convert.ToDateTime(a.endtime.ToString("yyyy-MM-dd")) < starttime && a.applytype == "depart").Sum(a => a.totaltime).GetValueOrDefault(0);

            }

            for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddDays(1))
            {
                decimal qjtotal = 0;
                //小于当前时间
                if (starttime <= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    TG.Model.cm_ApplyStatisDetailHis Detailhis_mem = null;
                    if (Detailhis_list != null && Detailhis_list.Count > 0)
                    {
                        Detailhis_mem = Detailhis_list.Where(h => h.mem_id == memID && h.dataDate == starttime.ToString("yyyy-MM-dd")).OrderByDescending(h => h.ID).FirstOrDefault();
                    }
                    if (Detailhis_mem != null)
                    {
                        qjtotal = Convert.ToDecimal(Detailhis_mem.time_leave);
                    }
                    else
                    {
                        //上班时间
                        //  DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00");
                        //中午下班时间
                        DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50:00");
                        //中午上班时间
                        DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00:59");
                        //下午下班时间
                        //  DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");
                        int time_late = 0;
                        ////排除节假日和周六日
                        if (!isListJQ(list, starttime))
                        {


                            //当天打卡记录
                            DateTime tomr_endtime = starttime;
                            DataTable dt_late = new DataTable();
                            if (datatable != null && datatable.Rows.Count > 0)
                            {

                                dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + ((tomr_endtime.AddDays(1)).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + memname + "'" }.ToTable();
                                tomr_endtime = starttime;
                            }

                            //考勤申请记录                   
                            //string where = string.Format(" and adduser={0} and '{1}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) ", memID, starttime.ToString("yyyy-MM-dd"));
                            //截取年月日格式
                            int len = starttime.ToString().IndexOf(" ") + 1;
                            string temp = starttime.ToString().Substring(0, len);
                            DataTable dt_leave = new DataTable();
                            if (dt_all != null && dt_all.Rows.Count > 0)
                            {
                                dt_leave = new DataView(dt_all) { RowFilter = "(((('" + starttime + "'>=starttime and '" + starttime + "'<=endtime) or '" + temp + "'=substring(Convert(starttime,'System.String'),1," + len + ") or substring(Convert(endtime,'System.String'),1," + len + ")='" + temp + "')) ) or ('" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00')" }.ToTable();
                            }

                            


                            //先判断系统打卡时间，如果无打开时间，则判断当天是否有申请  
                            if (dt_late != null && dt_late.Rows.Count > 0)
                            {
                                #region
                                DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                                //出差和外勤申请记录
                                string str = "";
                                //判断申请记录是否存在,打卡记录只有一条或无数据或上午出差，下午出差时用到
                                bool flag = false;
                                if (dt_leave != null && dt_leave.Rows.Count > 0)
                                {
                                    //出差或外勤今天的0点到6,允许迟到半天
                                    DataTable travel_yestoday = new DataView(dt_leave) { RowFilter = "applytype in ('travel','gomeet','forget') and endtime<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();
                                    if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                    {
                                        str = "bt";
                                    }
                                    //申请记录统计
                                    DataTable dt_apply1 = new DataView(dt_leave) { RowFilter = "applytype not in ('addwork')" }.ToTable();
                                    if (dt_apply1 != null && dt_apply1.Rows.Count > 0)
                                    {
                                        DataTable dt_flag = new DataView(dt_apply) { RowFilter = "endtime>'" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();
                                        if (dt_flag != null && dt_flag.Rows.Count > 0)
                                        {
                                            flag = true;
                                        }

                                        #region 请假
                                        DataTable dt_leave1 = new DataView(dt_apply1) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                        qjtotal = qjtotal + GetLeave(dt_leave1, starttime);
                                        //每年部门活动带薪时间3天（7.5*3小时），超过22.5小时按事假计算,每次申请最多使用1天
                                        decimal unittime = 0;
                                        DataTable dt_temp = new DataView(dt_apply1) { RowFilter = "applytype='depart' " }.ToTable();
                                        if (dt_temp != null && dt_temp.Rows.Count > 0)
                                        {
                                            //当天的部门活动小时
                                            unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                        }

                                        qjtotal = qjtotal + unittime;
                                        #endregion



                                    }
                                }
                                //迟到情况
                                if ((shiji > houtai && shiji < houtaioff) || shiji > houtaioff)
                                {
                                    //昨天下班打卡时间
                                    string yestoday = "";
                                    DateTime yes_time = starttime;
                                    yes_time = yes_time.AddDays(-1);
                                    DataTable dt_yestoday = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (yes_time.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + memname + "'" }.ToTable();
                                    if (dt_yestoday != null && dt_yestoday.Rows.Count > 0)
                                    {
                                        yestoday = Convert.ToDateTime(dt_yestoday.Rows[(dt_yestoday.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm");
                                    }
                                    //已查询允许迟到半天
                                    if (str == "")
                                    {
                                        //截取年月日格式
                                        int yes_len = yes_time.ToString().IndexOf(" ") + 1;
                                        string yes_temp = yes_time.ToString().Substring(0, yes_len);
                                        DataTable travel_yestoday = new DataTable();
                                        if (dt_all != null && dt_all.Rows.Count > 0)
                                        {
                                            travel_yestoday = new DataView(dt_all) { RowFilter = "applytype in ('travel','gomeet','forget') and starttime>='" + yes_time.ToString("yyyy-MM-dd") + " 06:00:00' and endtime<='" + yes_time.ToString("yyyy-MM-dd") + " 23:59:59'" }.ToTable();
                                        }
                                        //需要执行数据库
                                        //出差或外勤昨天22点到23:50,允许迟到半小时
                                        //string strwhere = string.Format(" and (applytype='travel' or applytype='gomeet') and '{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) and adduser={1} and starttime>='{2}' and endtime<'{3}'", yes_time.ToString("yyyy-MM-dd"), memid, (yes_time.ToString("yyyy-MM-dd") + " 22:00:00"), (yes_time.ToString("yyyy-MM-dd") + " 23:50:00"));
                                        //DataTable travel_yestoday = new TG.BLL.cm_ApplyInfo().GetApplyList(strwhere).Tables[0];
                                        if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                        {
                                            //查询结束时间是否大于23点50分钟
                                            DataTable temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 23:50:00'") }.ToTable();
                                            if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                            {
                                                str = "bt";
                                            }
                                            else
                                            {
                                                //查询结束时间是否大于22点00分钟
                                                temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 21:50:00'") }.ToTable();
                                                if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                {
                                                    str = "bxs";
                                                }
                                            }
                                        }
                                    }
                                    //昨天加班到22：00，所以允许今天迟到半小时后,统计从9:30
                                    if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 21:50:00") && Convert.ToDateTime(yestoday) < Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bxs")
                                    {
                                        houtai = houtai.AddMinutes(30);
                                    }
                                    //昨天加班到23:50，所以允许迟到半天,统计从13：0:0
                                    else if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bt")
                                    {
                                        houtai = zwsbtime;
                                    }
                                    TimeSpan ts = shiji - houtai;
                                    if (ts.TotalMinutes > 3)
                                    {
                                        if (ts.TotalMinutes >= 4 && ts.TotalMinutes <= 14)//迟到
                                        {

                                            time_late++;


                                        }
                                        else if (ts.TotalMinutes > 14 && ts.TotalMinutes < 30 && !flag) //迟到大于10分钟按0.5事假统计
                                        {

                                            qjtotal = qjtotal + decimal.Parse("0.5");


                                        }
                                        else
                                        {
                                            //没有申请记录
                                            if (!flag)
                                            {
                                                if (shiji > houtaioff)
                                                {
                                                    //下班时间-上班时间减去一小时
                                                    TimeSpan ts2 = houtaioff - houtai;
                                                    qjtotal = qjtotal + GetHours(ts2);
                                                    //上班时间不是下午1点。
                                                    if (houtai < zwsbtime)
                                                    {
                                                        qjtotal = qjtotal - 1;
                                                    }
                                                }
                                                else
                                                {
                                                    //上班未上班, 正常上班时间9点、9点半
                                                    if (shiji >= zwxbtime && zwxbtime > houtai)
                                                    {
                                                        TimeSpan ts2 = zwxbtime - houtai;
                                                        qjtotal = qjtotal + GetHours(ts2);
                                                    }
                                                    else
                                                    {
                                                        //上午上班，打卡迟到大于30分钟
                                                        qjtotal = qjtotal + GetHours(ts);

                                                    }

                                                    //实际打卡时间大于下午上班时间, 正常上班时间9点、9点半
                                                    if (shiji > zwsbtime && zwxbtime > houtai)
                                                    {
                                                        TimeSpan ts3 = shiji - zwsbtime;
                                                        qjtotal = qjtotal + GetHours(ts3);
                                                    }
                                                }
                                            }

                                        }


                                        //超过5次后，按照0.5小时事假扣除
                                        if (time_late > 0 && (sum_late + 1) > 5)
                                        {
                                            qjtotal = qjtotal + decimal.Parse("0.5");
                                        }
                                    }
                                }

                                //早退
                                if (dt_late.Rows.Count > 0 && shijioff < houtaioff && !flag)
                                {
                                    DateTime zaotui = shijioff;
                                    //只有一条早上打卡记录,按上班时间计算
                                    if (shijioff < houtai)
                                    {
                                        zaotui = houtai;
                                    }
                                    //下班打卡时间是上午,中午下班时间-早退时间
                                    if (shijioff < zwxbtime)
                                    {
                                        if (dt_late.Rows.Count == 1)
                                        {
                                            zaotui = houtai;
                                            TimeSpan ts_sw = zwxbtime - zaotui;
                                            qjtotal = GetHours(ts_sw);
                                            //超过5次后，按照0.5小时事假扣除
                                            if (time_late > 0 && (sum_late + 1) > 5)
                                            {
                                                qjtotal = qjtotal + decimal.Parse("0.5");
                                            }
                                        }
                                        else
                                        {
                                            TimeSpan ts_sw = zwxbtime - zaotui;
                                            qjtotal = qjtotal + GetHours(ts_sw);
                                        }

                                    }

                                    //下班打卡时间是下午1点之前,就默认是下午1点
                                    if (shijioff < zwsbtime)
                                    {
                                        zaotui = zwsbtime;
                                    }

                                    TimeSpan ts2 = houtaioff - zaotui;
                                    decimal ztqj = GetHours(ts2);

                                    qjtotal = qjtotal + ztqj;
                                }

                                #endregion
                            }
                            else
                            {
                                if (!isListJQ(list, starttime))
                                {
                                    //请假申请
                                    if (dt_leave != null && dt_leave.Rows.Count > 0)
                                    {
                                        DataTable dt = new DataView(dt_leave) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable(); ;
                                        if (dt != null && dt.Rows.Count > 0)
                                        {
                                            qjtotal = qjtotal + GetLeave(dt, starttime);
                                        }

                                        //每年部门活动带薪时间1天（7.5小时），超过7.5小时按事假计算
                                        decimal unittime = 0;
                                        DataTable dt_temp = new DataView(dt_leave) { RowFilter = "applytype='depart' " }.ToTable();
                                        if (dt_temp != null && dt_temp.Rows.Count > 0)
                                        {
                                            //当天的部门活动小时
                                            unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                        }
                                        qjtotal = qjtotal + unittime;

                                    }
                                    else
                                    {
                                        //一天未打卡，无申请
                                        qjtotal = qjtotal + decimal.Parse("7.5");
                                    }
                                }

                            }
                           
                        }

                        //修改数据
                        //获取某人某年某月某日              
                        if (list_data_Detail != null && list_data_Detail.Count > 0)
                        {
                            var data_model = list_data_Detail.Where(asd => asd.mem_id == memID && asd.dataYear == starttime.Year && asd.dataMonth == starttime.Month && asd.dataDay == starttime.Day && asd.dataType == "late").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_late = Convert.ToInt32(data_model.dataValue);
                                //超过5次后，按照0.5小时事假扣除
                                if (time_late > 0 && (sum_late + 1) > 5)
                                {
                                    qjtotal = qjtotal + decimal.Parse("0.5");
                                }
                            }
                            data_model = list_data_Detail.Where(asd => asd.mem_id == memID && asd.dataYear == starttime.Year && asd.dataMonth == starttime.Month && asd.dataDay == starttime.Day && asd.dataType == "leave").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                qjtotal = data_model.dataValue;
                            }
                           
                        }
                        sum_late = sum_late + time_late;
                    }
                }
                sumqjtotal = sumqjtotal + qjtotal;
            }
            //迟到次数
            totallate = sum_late;
            return sumqjtotal;
        }
        //获取上一年剩余时间
        public decimal GetOldYearTime(string memid, string memname)
        {
            //上月剩余去年年假,
            decimal privMonthDay = 0;
            //上月剩余今年年假,
            decimal privMonthCurrDay = 0;
            //今年年假
            int currYearHoliday = 0;
            //初始年假
            int oldYearHoliday = 0;
            List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" mem_ID=" + memid + " and attend_month=0 and OffWork='2016'");

            //2016年假
            if (pas_list != null && pas_list.Count > 0)
            {
                oldYearHoliday = Convert.ToInt32(pas_list[0].ToWork);
            }

            //每年年假
            List<TG.Model.tg_memberExtInfo> memextinfo = new TG.BLL.tg_memberExtInfo().GetModelList(" mem_ID=" + memid);
            if (memextinfo != null && memextinfo.Count > 0)
            {
                currYearHoliday = Convert.ToInt32(memextinfo[0].mem_HolidayYear);
            }

            for (int y = 2017; y < DateTime.Today.Year; y++)
            {
                //2016年剩余年假
                decimal oldtotaltime = Convert.ToDecimal(oldYearHoliday * 7.5);
                if (y > 2017)
                {
                    //循环剩余年假
                    oldtotaltime = privMonthCurrDay;
                }

                //2017年剩余年假
                decimal totaltime = Convert.ToDecimal(currYearHoliday * 7.5);
                int oldyear = Convert.ToInt32(y) - 1;
                int drpyear = Convert.ToInt32(y);
                DateTime oldstarttime = Convert.ToDateTime(y + "-01-16");
                DateTime oldendtime = Convert.ToDateTime(y + "-12-15");

                //判断是否手动改过2016剩余年假
                if (list_data_all != null && list_data_all.Count > 0)
                {
                    //事假
                    var data_model = list_data_all.Where(d => d.mem_id == Convert.ToInt32(memid) && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == 0 && d.dataType == "privyear").OrderByDescending(d => d.id).FirstOrDefault();
                    if (data_model != null)
                    {
                        oldtotaltime = data_model.dataValue;

                    }
                }

                //默认是上一年剩余
                // privMonthDay = oldtotaltime + totaltime;
                //之前年假使用情况 
                //循环最后一个日期 相同
                oldstarttime = oldstarttime.AddDays(-1);
                int i = 1;
                for (; oldstarttime.CompareTo(oldendtime) <= 0; oldstarttime = oldstarttime.AddMonths(1))
                {

                    //结束日期 到这个月15号结束
                    DateTime enddate = oldstarttime;
                    //开始日期 上月16号开始
                    DateTime startdate = oldstarttime;
                    startdate = startdate.AddMonths(-1).AddDays(1);

                    //2016-12-16~2017-01-15统计
                    if (i == 1)
                    {
                        //if (list_data != null && list_data.Count > 0)
                        //{
                        //    //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月去年年假剩余】
                        //    var data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == enddate.Year && asd.dataMonth == enddate.Month && asd.dataType == "privMonth") || (asd.dataYear == startdate.Year && asd.dataMonth == startdate.Month && asd.dataType == "lastMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                        //    if (data_model != null)
                        //    {
                        //        oldtotaltime = data_model.dataValue;

                        //    }
                        //    //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月今年年假剩余】
                        //    data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == enddate.Year && asd.dataMonth == enddate.Month && asd.dataType == "privMonthCurr") || (asd.dataYear == startdate.Year && asd.dataMonth == startdate.Month && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                        //    if (data_model != null)
                        //    {
                        //        totaltime = data_model.dataValue;

                        //    }
                        //}

                        int totallate = 0;
                        //2016-12月份16-31                          
                        decimal timeslot = GetTreeDataTableTime(Convert.ToInt32(memid), i, memname, (oldyear + "-12-16"), (oldyear + "-12-31"), ref totallate);

                        //判断是否手动改过
                        if (list_data_all != null && list_data_all.Count > 0)
                        {
                            var data_model = list_data_all.Where(d => d.mem_id == Convert.ToInt32(memid) && d.dataYear == oldyear && d.dataMonth == i && d.dataType == "12-31").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                timeslot = data_model.dataValue;

                            }
                        }

                        //使用去年年假                                       
                        if (oldtotaltime >= timeslot)
                        {
                            //去年剩余年假
                            oldtotaltime = oldtotaltime - timeslot;

                        }
                        else
                        {
                            oldtotaltime = 0;
                        }
                        int sumlate = 0;
                        //1月份1-15
                        timeslot = GetTreeDataTableTime(Convert.ToInt32(memid), i, memname, (drpyear + "-01-01"), enddate.ToString("yyyy-MM-dd"), ref sumlate);
                        //迟到次数超过5扣半小时事假
                        if (totallate >= 5)
                        {
                            //后半月又有迟到
                            if (sumlate > 0)
                            {
                                timeslot = timeslot + (sumlate * decimal.Parse("0.5"));
                            }
                        }
                        else
                        {
                            if (sumlate > 0 && sumlate < 5)
                            {
                                int zh = (totallate + sumlate);
                                //合计迟到大于5
                                if (zh > 5)
                                {
                                    timeslot = timeslot + ((zh - 5) * decimal.Parse("0.5"));
                                }
                            }
                            else if (sumlate >= 5)
                            {
                                timeslot = timeslot + (totallate * decimal.Parse("0.5"));
                            }
                        }

                        //判断是否手动改过
                        if (list_data_all != null && list_data_all.Count > 0)
                        {
                            var data_model = list_data_all.Where(d => d.mem_id == Convert.ToInt32(memid) && d.dataYear == drpyear && d.dataMonth == i && d.dataType == enddate.ToString("MM-dd")).OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                timeslot = data_model.dataValue;

                            }
                        }


                        //2016年年假有剩余
                        if (oldtotaltime >= timeslot)
                        {
                            oldtotaltime = oldtotaltime - timeslot;

                        }
                        else
                        {
                            decimal sy = timeslot;
                            //2016年年假
                            if (oldtotaltime > 0 && oldtotaltime < timeslot)
                            {

                                sy = timeslot - oldtotaltime;
                                oldtotaltime = 0;

                            }
                            //用2017年年假
                            if (Math.Abs(sy) > 0 && totaltime >= sy)
                            {
                                totaltime = totaltime - sy;

                            }
                            else
                            {

                                totaltime = 0;

                            }


                        }
                        //上月剩余年假
                        privMonthDay = oldtotaltime;
                        privMonthCurrDay = totaltime;
                    }
                    //2-16~3-15统计
                    else if (i == 3)
                    {
                        if (list_data != null && list_data.Count > 0)
                        {
                            //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月年假剩余】
                            var data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == enddate.Year && asd.dataMonth == enddate.Month && asd.dataType == "privMonth") || (asd.dataYear == startdate.Year && asd.dataMonth == startdate.Month && asd.dataType == "lastMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                oldtotaltime = data_model.dataValue;

                            }
                            //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月今年年假剩余】
                            data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == enddate.Year && asd.dataMonth == enddate.Month && asd.dataType == "privMonthCurr") || (asd.dataYear == startdate.Year && asd.dataMonth == startdate.Month && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                totaltime = data_model.dataValue;

                            }
                        }
                        int totallate = 0;
                        //2月份16-28
                        int days = DateTime.DaysInMonth(Convert.ToInt32(drpyear), 2);
                        decimal timeslot = GetTreeDataTableTime(Convert.ToInt32(memid), i, memname, (drpyear + "-02-16"), (drpyear + "-02-" + days), ref totallate);

                        //判断是否手动改过
                        if (list_data_all != null && list_data_all.Count > 0)
                        {
                            var data_model = list_data_all.Where(d => d.mem_id == Convert.ToInt32(memid) && d.dataYear == drpyear && d.dataMonth == i && d.dataType == ("-02-" + days)).OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                timeslot = data_model.dataValue;

                            }
                        }

                        //2016年年假有剩余
                        if (oldtotaltime >= timeslot)
                        {
                            oldtotaltime = oldtotaltime - timeslot;

                        }
                        else
                        {
                            decimal sy = timeslot;
                            //2016年年假
                            if (oldtotaltime > 0 && oldtotaltime < timeslot)
                            {

                                sy = timeslot - oldtotaltime;
                                oldtotaltime = 0;

                            }
                            //用2017年年假
                            if (Math.Abs(sy) > 0 && totaltime >= sy)
                            {
                                totaltime = totaltime - sy;

                            }
                            else
                            {
                                totaltime = 0;
                            }
                        }

                        int sumlate = 0;
                        //3月份1-15
                        timeslot = GetTreeDataTableTime(Convert.ToInt32(memid), i, memname, (drpyear + "-03-01"), enddate.ToString("yyyy-MM-dd"), ref sumlate);
                        //迟到次数超过5扣半小时事假
                        if (totallate >= 5)
                        {
                            //后半月又有迟到
                            if (sumlate > 0)
                            {
                                timeslot = timeslot + (sumlate * decimal.Parse("0.5"));
                            }
                        }
                        else
                        {
                            if (sumlate > 0 && sumlate < 5)
                            {
                                int zh = (totallate + sumlate);
                                //合计迟到大于5
                                if (zh > 5)
                                {
                                    timeslot = timeslot + ((zh - 5) * decimal.Parse("0.5"));
                                }
                            }
                            else if (sumlate >= 5)
                            {
                                timeslot = timeslot + (totallate * decimal.Parse("0.5"));
                            }
                        }

                        //判断是否手动改过
                        if (list_data_all != null && list_data_all.Count > 0)
                        {
                            var data_model = list_data_all.Where(d => d.mem_id == Convert.ToInt32(memid) && d.dataYear == drpyear && d.dataMonth == i && d.dataType == enddate.ToString("MM-dd")).OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                timeslot = data_model.dataValue;

                            }
                        }

                        if (totaltime >= timeslot)
                        {

                            totaltime = totaltime - timeslot;
                        }
                        else if (totaltime > 0 && totaltime < timeslot)
                        {

                            totaltime = 0;
                        }
                        //上月剩余年假
                        privMonthDay = oldtotaltime;
                        privMonthCurrDay = totaltime;
                    }
                    else
                    {
                        int totallate = 0;
                        decimal timeslot = GetTreeDataTableTime(Convert.ToInt32(memid), i, memname, startdate.ToString("yyyy-MM-dd"), enddate.ToString("yyyy-MM-dd"), ref totallate);

                        //判断是否手动改过
                        if (list_data_all != null && list_data_all.Count > 0)
                        {
                            var data_model = list_data_all.Where(d => d.mem_id == Convert.ToInt32(memid) && d.dataYear == enddate.Year && d.dataMonth == i && d.dataType == enddate.ToString("MM-dd")).OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                timeslot = data_model.dataValue;

                            }
                        }

                        if (i == 2)
                        {
                            if (list_data != null && list_data.Count > 0)
                            {
                                //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月年假剩余】
                                var data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == enddate.Year && asd.dataMonth == enddate.Month && asd.dataType == "privMonth") || (asd.dataYear == startdate.Year && asd.dataMonth == startdate.Month && asd.dataType == "lastMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                if (data_model != null)
                                {
                                    oldtotaltime = data_model.dataValue;

                                }
                                //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月今年年假剩余】
                                data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == enddate.Year && asd.dataMonth == enddate.Month && asd.dataType == "privMonthCurr") || (asd.dataYear == startdate.Year && asd.dataMonth == startdate.Month && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                if (data_model != null)
                                {
                                    totaltime = data_model.dataValue;

                                }
                            }

                            //2016年年假有剩余
                            if (oldtotaltime >= timeslot)
                            {
                                oldtotaltime = oldtotaltime - timeslot;
                            }
                            else
                            {
                                decimal sy = timeslot;
                                //2016年年假
                                if (oldtotaltime > 0 && oldtotaltime < timeslot)
                                {

                                    sy = timeslot - oldtotaltime;
                                    oldtotaltime = 0;

                                }
                                //用2017年年假
                                if (Math.Abs(sy) > 0 && totaltime >= sy)
                                {
                                    totaltime = totaltime - sy;

                                }
                                else
                                {

                                    totaltime = 0;

                                }
                            }
                            //上月剩余年假
                            privMonthDay = oldtotaltime;
                            privMonthCurrDay = totaltime;
                        }
                        else
                        {
                            if (list_data != null && list_data.Count > 0)
                            {
                                //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月年假剩余】
                                var data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == enddate.Year && asd.dataMonth == enddate.Month && asd.dataType == "privMonthCurr") || (asd.dataYear == startdate.Year && asd.dataMonth == startdate.Month && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                if (data_model != null)
                                {
                                    totaltime = data_model.dataValue;

                                }
                            }
                            if (totaltime >= timeslot)
                            {

                                totaltime = totaltime - timeslot;
                            }
                            else if (totaltime > 0 && totaltime < timeslot)
                            {

                                totaltime = 0;
                            }
                            //上月剩余年假
                            privMonthCurrDay = totaltime;
                            privMonthDay = 0;
                        }

                    }

                    i++;
                }
                //【本月今年年假剩余】
                if (list_data != null && list_data.Count > 0)
                {
                    var data_model = list_data.Where(asd => asd.mem_id == Convert.ToInt32(memid) && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == 12 && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                    if (data_model != null)
                    {
                        privMonthCurrDay = data_model.dataValue;

                    }
                }

                //判断是否手动改过2017剩(小时)              
                if (list_data_all != null && list_data_all.Count > 0)
                {
                    //事假
                    var data_model = list_data_all.Where(d => d.mem_id == Convert.ToInt32(memid) && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == 0 && d.dataType == "currtyear").OrderByDescending(d => d.id).FirstOrDefault();
                    if (data_model != null)
                    {
                        privMonthCurrDay = data_model.dataValue;
                    }
                }
            }
            //上月去年剩余
            privMonthDay = privMonthCurrDay;

            return privMonthDay;
        }
        #region 已注销
        //public decimal GetLeave(DataTable dt_leave, DateTime starttime)
        //{
        //    decimal time_leave = 0;
        //    if (dt_leave != null && dt_leave.Rows.Count > 0)
        //    {
        //        //上班时间
        //        DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00");
        //        //中午下班时间
        //        DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50");
        //        //中午上班时间
        //        DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00");
        //        //下午下班时间
        //        DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");
        //        foreach (DataRow dr in dt_leave.Rows)
        //        {
        //            DateTime starttime_leave = Convert.ToDateTime(dr["starttime"]);
        //            DateTime endtime_leave = Convert.ToDateTime(dr["endtime"]);
        //            decimal totaltime_leave = Convert.ToDecimal(dr["totaltime"]);
        //            //请假一天以内
        //            if (totaltime_leave <= decimal.Parse("7.5"))
        //            {
        //                time_leave = time_leave + totaltime_leave;
        //            }
        //            else //请假日期不在一天内
        //            {

        //                //分三种情况，开始时间相同，结束时间相同，中间时间也就是7.5小时
        //                if (starttime_leave.ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd"))
        //                {
        //                    if (starttime_leave < ontime)
        //                    {
        //                        starttime_leave = ontime;
        //                    }
        //                    if (starttime_leave < offtime)
        //                    {
        //                        TimeSpan tspan = offtime - starttime_leave;
        //                        double hours = tspan.TotalHours;
        //                        //开始时间不超过12点，需减去1小时中午休息时间
        //                        if (starttime_leave < zwxbtime)
        //                        {
        //                            hours = hours - 1;
        //                        }
        //                        time_leave = time_leave + Convert.ToDecimal(hours);
        //                    }

        //                }
        //                else if (endtime_leave.ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd"))
        //                {
        //                    //结束时间超过17:30点，按17:30点计算
        //                    if (endtime_leave > offtime)
        //                    {
        //                        endtime_leave = offtime;
        //                    }
        //                    if (endtime_leave > ontime)
        //                    {
        //                        TimeSpan tspan = endtime_leave - ontime;
        //                        double hours = tspan.TotalHours;
        //                        //结束时间超过13点，需减去1小时中午休息时间
        //                        if (endtime_leave > zwsbtime)
        //                        {
        //                            hours = hours - 1;
        //                        }
        //                        time_leave = time_leave + Convert.ToDecimal(hours);
        //                    }
        //                }
        //                else
        //                {
        //                    time_leave = time_leave + decimal.Parse("7.5");
        //                }
        //            }
        //        }
        //    }
        //    return time_leave;
        //}

        //计算请假时间
        //public decimal GetTotalTime(DateTime starttime, DateTime endtime)
        //{
        //    decimal total = 0;
        //    //上班时间
        //    DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00");
        //    //中午下班时间
        //    DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50");
        //    //中午上班时间
        //    DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00");
        //    //下午下班时间
        //    DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");
        //    //同一天
        //    if (starttime.ToString("yyyy-MM-dd") == endtime.ToString("yyyy-MM-dd"))
        //    {
        //        if (starttime < ontime)
        //        {
        //            starttime = ontime;
        //        }
        //        if (starttime >= zwxbtime && starttime <= zwsbtime)
        //        {
        //            starttime = zwsbtime;
        //        }
        //        if (endtime >= zwxbtime && endtime <= zwsbtime)
        //        {
        //            endtime = zwxbtime;
        //        }
        //        if (endtime > offtime)
        //        {
        //            endtime = offtime;
        //        }
        //        if (endtime > ontime && starttime < offtime)
        //        {

        //            if (endtime > zwsbtime && starttime < zwxbtime)
        //            {
        //                //请一天假，
        //                TimeSpan minus = endtime - starttime;
        //                //减去中午休息一小时
        //                total = total + Convert.ToDecimal(minus.TotalHours) - 1;
        //            }
        //            else
        //            {
        //                //半天假，
        //                TimeSpan minus = endtime - starttime;
        //                total = total + Convert.ToDecimal(minus.TotalHours);

        //            }

        //        }
        //    }
        //    else //不同一天
        //    {

        //        //请假天数，排除前后当天日期
        //        var days = (Convert.ToDateTime(endtime.ToShortDateString())).Subtract(Convert.ToDateTime(starttime.ToShortDateString())).Days - 1;

        //        //排除周六日和节假日
        //        int holiday = IsHoliday(starttime.ToString("yyyy-MM-dd HH:mm:ss"), endtime.ToString("yyyy-MM-dd HH:mm:ss"));

        //        if (holiday > 0)
        //        {
        //            days = days - holiday;
        //        }
        //        /////////////////////////开始时间
        //        //排除假期
        //        if (!isJQ(starttime))
        //        {

        //            //开始时间不超过9点，按9点计算
        //            if (starttime < ontime)
        //            {
        //                starttime = ontime;
        //            }
        //            if (starttime >= zwxbtime && starttime < zwsbtime)
        //            {
        //                starttime = zwsbtime;
        //            }

        //            if (starttime < offtime)
        //            {
        //                TimeSpan minus = offtime - starttime;
        //                total += Convert.ToDecimal(minus.TotalHours);
        //                //开始时间不超过12点，需减去1小时中午休息时间
        //                if (starttime < zwxbtime)
        //                {
        //                    total = total - 1;
        //                }
        //            }
        //        }
        //        /////////////////结束时间
        //        //排除假期
        //        if (!isJQ(endtime))
        //        {
        //            //上班时间
        //            ontime = Convert.ToDateTime(endtime.ToString("yyyy-MM-dd") + " 09:00");
        //            //中午下班时间
        //            zwxbtime = Convert.ToDateTime(endtime.ToString("yyyy-MM-dd") + " 11:50");
        //            //中午上班时间
        //            zwsbtime = Convert.ToDateTime(endtime.ToString("yyyy-MM-dd") + " 13:00");
        //            //下午下班时间
        //            offtime = Convert.ToDateTime(endtime.ToString("yyyy-MM-dd") + " 17:30");
        //            //结束时间超过17:30点，按17:30点计算
        //            if (endtime > offtime)
        //            {
        //                endtime = offtime;
        //            }
        //            if (endtime >= zwxbtime && endtime < zwsbtime)
        //            {
        //                endtime = zwxbtime;
        //            }
        //            if (endtime > ontime)
        //            {
        //                TimeSpan minus2 = endtime - ontime;
        //                total += Convert.ToDecimal(minus2.TotalHours);
        //                //结束时间超过13点，需减去1小时中午休息时间
        //                if (endtime > zwsbtime)
        //                {
        //                    total = total - 1;
        //                }
        //            }
        //        }
        //        ////加上中间请假天数*7.5
        //        total = total + Convert.ToDecimal(days * 7.5);
        //    }
        //    return total;
        //}

        //返回请假时间

        ////循环日期判断是否有假期
        //public int IsHoliday(string start, string end)
        //{
        //    int str = 0;
        //    //排除前后当天日期，取中间时间的日期
        //    DateTime offdate = Convert.ToDateTime(end.Substring(0, 10));
        //    DateTime ondate = Convert.ToDateTime(Convert.ToDateTime(start).AddDays(1).ToString("yyyy-MM-dd"));
        //    for (; ondate.CompareTo(offdate) < 0; ondate = ondate.AddDays(1))
        //    {
        //        bool flag = isJQ(ondate);
        //        if (flag)
        //        {
        //            str++;
        //        }
        //    }

        //    return str;
        //}
        #endregion
        #region PageBase已存在

        ////返回加班时间
        //public decimal GetOver(DateTime start, DateTime end)
        //{
        //    decimal over = 0;
        //    if (end > start)
        //    {
        //        TimeSpan ts_jb = end - start;
        //        //强制转换，没有四舍五入
        //        int hours = (int)(ts_jb.TotalHours);

        //        double minutes = ts_jb.TotalMinutes - (hours * 60);
        //        //大于15分钟，即加班
        //        if (minutes > 15 && minutes <= 45)
        //        {
        //            over = decimal.Parse("0.5");
        //        }
        //        else if (minutes > 45 && minutes < 60)
        //        {
        //            over = decimal.Parse("1");
        //        }

        //        over = over + hours;
        //    }
        //    return over;
        //}       
        ////判断是否是假期
        //public bool isJQ(DateTime date)
        //{
        //    bool flag = false;
        //    List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" convert(varchar(10),holiday,120)='" + date.ToString("yyyy-MM-dd") + "' order by id");
        //    //存在
        //    if (list != null && list.Count > 0)
        //    {
        //        //节假日，需减1天
        //        if (list[0].daytype == 1)
        //        {
        //            flag = true;
        //        }
        //    }
        //    else
        //    {
        //        //周六日0~6
        //        string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
        //        if (temp == "Sunday" || temp == "Saturday")
        //        {
        //            flag = true;
        //        }
        //    }
        //    return flag;
        //}
        #endregion
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {

            BindProject();
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        //protected void AspNetPager1_PageChanged(object src, EventArgs e)
        //{
        //    BindProject();
        //}
        protected void btn_export_Click(object sender, EventArgs e)
        {
            string sqlwhere = BindWhere("");
            string sql = "select *,(select unit_name from tg_unit where unit_id=mem_Unit_ID) as unitname,(select unit_Order from tg_unitExt where unit_ID=mem_Unit_ID) as unitorder from tg_member where " + sqlwhere + " order by unitorder asc,mem_Order asc,mem_ID asc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/YearAllStatis.xls", this.drp_unit.SelectedItem.Text.Trim());
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }

        protected void btn_allexport_Click(object sender, EventArgs e)
        {
            //排除离职人员
            string sqlwhere = BindWhere("all");
            string sql = "select *,(select unit_name from tg_unit where unit_id=mem_Unit_ID) as unitname,(select unit_Order from tg_unitExt where unit_ID=mem_Unit_ID) as unitorder from tg_member where " + sqlwhere + " order by unitorder asc,mem_Order asc,mem_ID asc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/YearAllStatis.xls", "全院");
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }
        private void ExportDataToExcel(DataTable dt, string modelPath, string pathname)
        {
            string drpyear = this.drp_year.SelectedValue;

            pathname = drpyear + "年" + pathname.Trim() + "年假统计";



            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //   style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //   style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            // style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //背景颜色样式
            ICellStyle style3 = wb.CreateCellStyle(); ;
            style3.VerticalAlignment = VerticalAlignment.CENTER;
            style3.WrapText = true;
            //   style3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            // style3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //  style3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style3.FillForegroundColor = HSSFColor.PINK.index;
            style3.SetFont(font1);

            //背景颜色样式
            ICellStyle style4 = wb.CreateCellStyle();
            style4.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style4.FillForegroundColor = HSSFColor.YELLOW.index;
            style4.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style4.VerticalAlignment = VerticalAlignment.CENTER;
            style4.WrapText = true;
            style4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;


            //背景颜色样式
            ICellStyle style5 = wb.CreateCellStyle();
            style5.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style5.FillForegroundColor = HSSFColor.GREEN.index;
            // style5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            // style5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            // style5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            // style5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;


            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            IRow dataRowTitle = ws.GetRow(0);
            dataRowTitle.Height = 30 * 20;
            //ws.SetColumnWidth(2, 25 * 256);         
            ICell acell21 = dataRowTitle.GetCell(0);
            acell21.SetCellValue(pathname);
            // CellRangeAddress cellRangeAddress2 = new CellRangeAddress(1, 1, 0, 1);
            // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            //ws.AddMergedRegion(cellRangeAddress2);

            //2月份
            int days = DateTime.DaysInMonth(Convert.ToInt32(drpyear), 2);
            //标题年数字变化
            acell21 = ws.GetRow(1).GetCell(3);
            acell21.SetCellValue(drpyear);

            acell21 = ws.GetRow(1).GetCell(4);
            acell21.SetCellValue(drpyear + "新");

            acell21 = ws.GetRow(1).GetCell(5);
            acell21.SetCellValue((Convert.ToInt32(drpyear) - 1) + "剩");

            acell21 = ws.GetRow(1).GetCell(20);
            acell21.SetCellValue(drpyear + "剩");

            acell21 = ws.GetRow(2).GetCell(9);
            acell21.SetCellValue("2." + days);

            //获取锁定数据
            List<TG.Model.cm_YearAllStatisHis> his_list = new TG.BLL.cm_YearAllStatisHis().GetModelList(" dataDate='" + drpyear + "'");

            //得到所有节假日日期。
            list = new TG.BLL.cm_HolidayConfig().GetModelList("");
            //后台设置上班时间
            List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_year=" + drpyear);
            //手动修改数据年假统计             
            list_data_all = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='YearAllStatis' order by id desc");
            //手动修改数据年假及带薪假
            list_data = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='YearPaidStatis' order by id desc");
            //各类单项统计手动修改数据
            list_data_Detail = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='StatisDetail' order by id desc");
            //请假数据
            dt_apply = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype not in ('addwork')").Tables[0];
            int row = 1;
            int old_unitid = 0;

            foreach (DataRow item in dt.Rows)
            {
                int memid = Convert.ToInt32(item["mem_ID"]);
                string memname = item["mem_Name"].ToString();

                var dataRow = ws.GetRow((row + 2));//第一行
                if (dataRow == null)
                    dataRow = ws.CreateRow((row + 2));//生成行

                if (old_unitid != Convert.ToInt32(item["mem_Unit_ID"]))
                {

                    old_unitid = Convert.ToInt32(item["mem_Unit_ID"]);
                    int cls = dt.Select("mem_Unit_ID=" + old_unitid).Count();

                    ICell cell0 = dataRow.GetCell(0);
                    if (cell0 == null)
                        cell0 = dataRow.CreateCell(0);

                    cell0.SetCellValue(item["unitname"].ToString().Trim());

                    CellRangeAddress cellRangeAddress2 = new CellRangeAddress((row + 2), (row + 1 + cls), 0, 0);
                    // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
                    ws.AddMergedRegion(cellRangeAddress2);
                }

                //2017年剩余年假
                decimal totaltime = 0m;

                //显示2016剩余年假
                decimal showoldtotaltime = 0m;
                //修改数据
                string colorStr_privyear = "";
                //判断是否手动改过2017剩(小时)
                string colorStr_currtyear = "";
                //今年年假
                int currYearHoliday = 0;

                TG.Model.cm_YearAllStatisHis his_mem = null;
                if (his_list != null && his_list.Count > 0)
                {
                    his_mem = his_list.Where(h => h.mem_id == memid).OrderByDescending(h => h.ID).FirstOrDefault();
                }
                //人员数据
                if (his_mem != null)
                {
                    int i = 0;
                    System.Reflection.PropertyInfo[] properties = his_mem.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
                    foreach (System.Reflection.PropertyInfo prop in properties)
                    {
                        //属性名
                        string name = prop.Name;
                        //属性值
                        // object value = prop.GetValue(his_mem, null);
                        //属性的类型判断、是值类型、属性名中包含“_”
                        if (prop.PropertyType.IsValueType && prop.PropertyType == typeof(decimal?) && name.IndexOf("_") > -1)
                        {
                            string value = prop.GetValue(his_mem, null).ToString();

                            //第7到20列
                            ICell cell6 = dataRow.GetCell(6 + i);
                            if (cell6 == null)
                                cell6 = dataRow.CreateCell(6 + i);

                            cell6.SetCellValue(value);
                            i++;
                        }

                    }
                    currYearHoliday = Convert.ToInt32(his_mem.yearDay);
                    showoldtotaltime = Convert.ToDecimal(his_mem.lastyearHour);
                    totaltime = Convert.ToDecimal(his_mem.nextyearHour);

                }
                else
                {

                    #region 上月剩余年假


                    //今年年假
                    currYearHoliday = 0;
                    ////去年年假
                    //int oldYearHoliday = 0;

                    //List<TG.Model.cm_PersonAttendSet> paslist = new TG.BLL.cm_PersonAttendSet().GetModelList(" mem_ID=" + item["mem_ID"] + " and attend_month=0 and OffWork='" + (Convert.ToInt32(drpyear) - 1) + "'");
                    //if (paslist != null && paslist.Count > 0)
                    //{
                    //    oldYearHoliday = Convert.ToInt32(paslist[0].ToWork);
                    //}


                    List<TG.Model.tg_memberExtInfo> memextinfo = new TG.BLL.tg_memberExtInfo().GetModelList(" mem_ID=" + item["mem_ID"]);
                    if (memextinfo != null && memextinfo.Count > 0)
                    {
                        currYearHoliday = Convert.ToInt32(memextinfo[0].mem_HolidayYear);
                    }

                    int oldyear = Convert.ToInt32(drpyear) - 1;

                    #endregion



                    string[] timelist = { "12-16~12-31", "1-1~1-15", "1-16~2-15", "2-16~2-" + days + "", "3-1~3-15", "3-16~4-15", "4-16~5-15", "5-16~6-15", "6-16~7-15", "7-16~8-15", "8-16~9-15", "9-16~10-15", "10-16~11-15", "11-16~12-15" };
                    //2016年剩余年假
                    // decimal oldtotaltime = Convert.ToDecimal(oldYearHoliday * 7.5);
                    decimal oldtotaltime = GetOldYearTime(item["mem_ID"].ToString(), item["mem_Name"].ToString());
                    //2017年剩余年假
                    totaltime = Convert.ToDecimal(currYearHoliday * 7.5);

                    //显示2016剩余年假
                    showoldtotaltime = oldtotaltime;
                    //修改数据
                    colorStr_privyear = "";

                    //判断是否手动改过2016剩余年假
                    if (list_data_all != null && list_data_all.Count > 0)
                    {
                        //事假
                        var data_model = list_data_all.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == 0 && d.dataType == "privyear").OrderByDescending(d => d.id).FirstOrDefault();
                        if (data_model != null)
                        {
                            oldtotaltime = data_model.dataValue;
                            showoldtotaltime = data_model.dataValue;
                            colorStr_privyear = " style='background-color:yellow;'";
                        }
                    }


                    int i = 0;
                    //打卡次数
                    int totallate = 0;
                    foreach (string timeStr in timelist)
                    {
                        //获取某人上下班时间
                        if (pas_list != null && pas_list.Count > 0)
                        {
                            List<TG.Model.cm_PersonAttendSet> pas_list_mem = pas_list.Where(p => p.mem_ID == Convert.ToInt32(item["mem_ID"]) && p.attend_month == (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : i))).ToList();
                            if (pas_list_mem != null && pas_list_mem.Count > 0)
                            {
                                towork = pas_list_mem[0].ToWork;
                                offwork = pas_list_mem[0].OffWork;
                            }
                            else
                            {
                                towork = "09:00";
                                offwork = "17:30";
                            }
                        }

                        //修改数据
                        string colorStr_month = "";
                        decimal value = 0;
                        string[] strlist = timeStr.Split('~');

                        if (i == 4)
                        {
                            int sumlate = 0;
                            //3月份
                            decimal timeslot = GetTreeDataTableTime(Convert.ToInt32(item["mem_ID"]), (i - 1), item["mem_Name"].ToString(), oldyear + "-3-1", oldyear + "-" + strlist[1], ref sumlate);
                            //迟到次数超过5扣半小时事假
                            if (totallate >= 5)
                            {
                                //后半月又有迟到
                                if (sumlate > 0)
                                {
                                    timeslot = timeslot + (sumlate * decimal.Parse("0.5"));
                                }
                            }
                            else
                            {
                                if (sumlate > 0 && sumlate < 5)
                                {
                                    int zh = (totallate + sumlate);
                                    //合计迟到大于5
                                    if (zh > 5)
                                    {
                                        timeslot = timeslot + ((zh - 5) * decimal.Parse("0.5"));
                                    }
                                }
                                else if (sumlate >= 5)
                                {
                                    timeslot = timeslot + (totallate * decimal.Parse("0.5"));
                                }
                            }
                            //判断是否手动改过
                            if (list_data_all != null && list_data_all.Count > 0)
                            {
                                var data_model = list_data_all.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == (i - 1) && d.dataType == strlist[1]).OrderByDescending(d => d.id).FirstOrDefault();
                                if (data_model != null)
                                {
                                    timeslot = data_model.dataValue;
                                    colorStr_month = " style='background-color:yellow;'";
                                }

                            }
                            //年假及带薪假统计
                            if (list_data != null && list_data.Count > 0)
                            {
                                //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月去年年假剩余】
                                var data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 1) && asd.dataType == "privMonth") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 2) && asd.dataType == "lastMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                if (data_model != null)
                                {
                                    oldtotaltime = data_model.dataValue;

                                }
                                //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月今年年假剩余】
                                data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 1) && asd.dataType == "privMonthCurr") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i - 2) && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                if (data_model != null)
                                {
                                    totaltime = data_model.dataValue;

                                }
                            }

                            if (totaltime >= timeslot)
                            {
                                value = value + timeslot;
                                totaltime = totaltime - timeslot;
                            }
                            else if (totaltime > 0 && totaltime < timeslot)
                            {
                                value = value + totaltime;
                                totaltime = 0;
                            }

                        }
                        else
                        {
                            int sumlate = 0;
                            decimal timeslot = GetTreeDataTableTime(Convert.ToInt32(item["mem_ID"]), (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : i)), item["mem_Name"].ToString(), oldyear + "-" + strlist[0], oldyear + "-" + strlist[1], ref sumlate);

                            //判断是否手动改过
                            if (list_data_all != null && list_data_all.Count > 0)
                            {
                                var data_model = list_data.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == (i == 0 ? (i + 1) : (i > 3 ? (i - 1) : i)) && d.dataType == strlist[1]).OrderByDescending(d => d.id).FirstOrDefault();
                                if (data_model != null)
                                {
                                    timeslot = data_model.dataValue;
                                    colorStr_month = " style='background-color:yellow;'";
                                }

                            }
                            //年假及带薪假统计
                            if (i > 1)
                            {
                                if (list_data != null && list_data.Count > 0)
                                {
                                    //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月去年年假剩余】
                                    var data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i > 3 ? (i - 1) : i) && asd.dataType == "privMonth") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == ((i > 3 ? (i - 1) : i) - 1) && asd.dataType == "lastMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        oldtotaltime = data_model.dataValue;

                                    }
                                    //获取本月的上月年假剩余记录和上个月的本月年假剩余记录，都代表一个数据【上月今年年假剩余】
                                    data_model = list_data.Where(asd => asd.mem_id == memid && ((asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == (i > 3 ? (i - 1) : i) && asd.dataType == "privMonthCurr") || (asd.dataYear == Convert.ToInt32(drpyear) && asd.dataMonth == ((i > 3 ? (i - 1) : i) - 1) && asd.dataType == "currentMonth"))).OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        totaltime = data_model.dataValue;

                                    }
                                }
                            }
                            //2016年上一年 12-16~12-31
                            if (i == 0)
                            {
                                //迟到次数
                                totallate = sumlate;
                                //使用去年年假                                       
                                if (oldtotaltime >= timeslot)
                                {
                                    //去年剩余年假
                                    oldtotaltime = oldtotaltime - timeslot;
                                    //使用年假
                                    value = timeslot;
                                }
                                else
                                {
                                    value = oldtotaltime;
                                    oldtotaltime = 0;
                                }



                            }//1.1-2.28
                            else if (i > 0 && i < 4)
                            {
                                //没有修改数据，加上迟到事假
                                if (i == 1 && colorStr_month == "")
                                {
                                    //迟到次数超过5扣半小时事假
                                    if (totallate >= 5)
                                    {
                                        //后半月又有迟到
                                        if (sumlate > 0)
                                        {
                                            timeslot = timeslot + (sumlate * decimal.Parse("0.5"));
                                        }
                                    }
                                    else
                                    {
                                        if (sumlate > 0 && sumlate < 5)
                                        {
                                            int zh = (totallate + sumlate);
                                            //合计迟到大于5
                                            if (zh > 5)
                                            {
                                                timeslot = timeslot + ((zh - 5) * decimal.Parse("0.5"));
                                            }
                                        }
                                        else if (sumlate >= 5)
                                        {
                                            timeslot = timeslot + (totallate * decimal.Parse("0.5"));
                                        }
                                    }

                                }

                                totallate = sumlate;

                                //2016年年假有剩余
                                if (oldtotaltime >= timeslot)
                                {
                                    oldtotaltime = oldtotaltime - timeslot;
                                    value = timeslot;
                                }
                                else
                                {
                                    decimal sy = timeslot;
                                    //2016年年假
                                    if (oldtotaltime > 0 && oldtotaltime < timeslot)
                                    {
                                        value = oldtotaltime;
                                        sy = timeslot - oldtotaltime;
                                        oldtotaltime = 0;

                                    }
                                    //用2017年年假
                                    if (Math.Abs(sy) > 0 && totaltime >= sy)
                                    {
                                        totaltime = totaltime - sy;
                                        value = timeslot;
                                    }
                                    else
                                    {
                                        value = value + totaltime;
                                        totaltime = 0;

                                    }


                                }



                            }//3.16-12.15
                            else if (i > 4)
                            {

                                if (totaltime >= timeslot)
                                {
                                    value = timeslot;
                                    totaltime = totaltime - timeslot;
                                }
                                else if (totaltime > 0 && totaltime < timeslot)
                                {
                                    value = totaltime;
                                    totaltime = 0;
                                }


                            }

                        }

                        //第7到20列
                        ICell cell6 = dataRow.GetCell(6 + i);
                        if (cell6 == null)
                            cell6 = dataRow.CreateCell(6 + i);

                        if (colorStr_month.Contains("yellow"))
                        {
                            //黄色
                            cell6.CellStyle = style4;
                        }
                        cell6.SetCellValue(value.ToString("f1"));

                        oldyear = Convert.ToInt32(drpyear);
                        i++;
                    }
                    //
                    //判断是否手动改过2017剩(小时)
                    colorStr_currtyear = "";
                    if (list_data != null && list_data.Count > 0)
                    {
                        //事假
                        var data_model = list_data.Where(d => d.mem_id == memid && d.dataYear == Convert.ToInt32(drpyear) && d.dataMonth == 0 && d.dataType == "currtyear").OrderByDescending(d => d.id).FirstOrDefault();
                        if (data_model != null)
                        {
                            totaltime = data_model.dataValue;
                            colorStr_currtyear = " style='background-color:yellow;'";
                        }
                    }
                }
                //第二列
                ICell cell = dataRow.GetCell(1);
                if (cell == null)
                    cell = dataRow.CreateCell(1);
                cell.SetCellValue(row);

                //第三列
                cell = dataRow.GetCell(2);
                if (cell == null)
                    cell = dataRow.CreateCell(2);
                cell.SetCellValue(item["mem_Name"].ToString());
                //第四列
                cell = dataRow.GetCell(3);
                if (cell == null)
                    cell = dataRow.CreateCell(3);
                cell.SetCellValue(currYearHoliday);

                //第五列
                cell = dataRow.GetCell(4);
                if (cell == null)
                    cell = dataRow.CreateCell(4);
                cell.SetCellValue((currYearHoliday * 7.5));

                //第六列
                cell = dataRow.GetCell(5);
                if (cell == null)
                    cell = dataRow.CreateCell(5);

                if (colorStr_privyear.Contains("yellow"))
                {
                    //黄色
                    cell.CellStyle = style4;
                }
                cell.SetCellValue(showoldtotaltime.ToString("f1"));

                //第21列
                cell = dataRow.GetCell(20);
                if (cell == null)
                    cell = dataRow.CreateCell(20);

                if (colorStr_currtyear.Contains("yellow"))
                {
                    //黄色
                    cell.CellStyle = style4;
                }
                cell.SetCellValue(totaltime.ToString("f1"));

                row = row + 1;

            }

            //写入Session
            Session["SamplePictures"] = true;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();

            var bll_unitExt = new TG.BLL.tg_unitExt();
            string sqlwhere = "";

            if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sqlwhere = " and unit_ID IN (Select top 1 mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_ID=" + UserSysNo + " order by mr.mem_unit_ID desc)";
            }

            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 and unit_ID not in (" + NotShowUnitList + ")" + sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定          
            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
                this.isapplymanager.Value = base.IsApplyManager.ToString();
            }
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }

            //for (int i = 1; i <= 12; i++)
            //{
            //    this.drp_month.Items.Add(i.ToString());
            //}

        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            //string curmonth = DateTime.Now.Month.ToString();
            //if (this.drp_month.Items.FindByText(curmonth) != null)
            //{
            //    this.drp_month.Items.FindByText(curmonth).Selected = true;
            //}

            //选中部门
            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人，只有管理员看到全部
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (mem_ID =" + UserSysNo + ") ");
            }
            //部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND mem_ID in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=" + UserUnitNo + ")");
            }
        }


    }

}