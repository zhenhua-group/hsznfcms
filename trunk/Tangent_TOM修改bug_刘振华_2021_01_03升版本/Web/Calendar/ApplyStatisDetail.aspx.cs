﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Configuration;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace TG.Web.Calendar
{
    public partial class ApplyStatisDetail : PageBase
    {
        public string mem_ID
        {
            get
            {
                return Request.QueryString["mem_ID"] ?? "0";
            }
        }
        TG.Model.tg_member mem_model = new TG.Model.tg_member();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                mem_model = new TG.BLL.tg_member().GetModel(Convert.ToInt32(mem_ID));
                if (mem_model != null)
                {
                    HiddenUserName.Value = mem_model.mem_Name;
                }

                BindPreviewPower();
                BindYear();
                SelectCurrentYear();
                //
                BindUnit();
                //绑定数据
                BindMem("inity");
            }



        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();

            string sqlwhere = "";

            if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sqlwhere = " and unit_ID IN (Select top 1 mr.mem_unit_ID From tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id Where mr.mem_ID=" + UserSysNo + " order by mr.mem_unit_ID desc)";
            }


            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 and unit_ID not in (" + NotShowUnitList + ")" + sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定          
            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定人员
        private void BindUser()
        {

            if (this.drp_unit.SelectedIndex > 0)
            {
                string drp_year = this.drp_year.SelectedValue;
                string drp_month = this.drp_month.SelectedValue;
                var next_year = Convert.ToInt32(drp_year);
                var next_month = (Convert.ToInt32(drp_month) - 1);
                if (drp_month == "1")
                {
                    next_year = (Convert.ToInt32(drp_year) - 1);
                    next_month = 12;
                }

                string olduserid = this.hid_userid.Value;

                string where = " mem_Unit_ID=" + this.drp_unit.SelectedValue + " ";
                //个人权限
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    where = " mem_Unit_ID=" + this.drp_unit.SelectedValue + " and mem_id=" + UserSysNo + " ";
                }
                where = where + " and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + next_year + "-" + next_month.ToString().PadLeft(2, '0') + "-16')) order by mem_Order asc,mem_ID asc";

                List<TG.Model.tg_member> dt = new TG.BLL.tg_member().GetModelList(where);
                this.drp_user.DataSource = dt;
                this.drp_user.DataTextField = "mem_Name";
                this.drp_user.DataValueField = "mem_ID";
                this.drp_user.DataBind();

                ListItem li = new ListItem();
                li.Text = "---全部---";
                li.Value = "-1";
                drp_user.Items.Insert(0, li);

                if (olduserid != "-1")
                {
                    if (this.drp_user.Items.FindByValue(olduserid) != null)
                    {
                        this.drp_user.Items.FindByValue(olduserid).Selected = true;
                    }
                }
            }
            else
            {
                drp_user.Items.Clear();
                ListItem li = new ListItem();
                li.Text = "---全部---";
                li.Value = "-1";
                drp_user.Items.Add(li);
            }

        }
        //查询条件
        public string BindWhere()
        {
            StringBuilder sb = new StringBuilder();

            string drp_year = this.drp_year.SelectedValue;
            string drp_month = this.drp_month.SelectedValue;
            var next_year = Convert.ToInt32(drp_year);
            var next_month = (Convert.ToInt32(drp_month) - 1);
            if (drp_month == "1")
            {
                next_year = (Convert.ToInt32(drp_year) - 1);
                next_month = 12;
            }

            //排除离职人员
            sb.Append(" (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + next_year + "-" + next_month.ToString().PadLeft(2, '0') + "-16'))");



            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" and mem_ID=" + mem_ID + " ");
            }
            //权限部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                this.HiddenUserName.Value = this.drp_unit.Items[1].Text;
                sb.Append(" and mem_ID in (select mr.mem_ID from tg_member m join tg_memberRole mr on m.mem_id=mr.mem_Id where mr.mem_Unit_ID=(select top 1 mem_Unit_ID from tg_memberRole where mem_ID=" + UserSysNo + " order by mem_Unit_ID desc))");
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                //选择部门
                if (this.drp_unit.SelectedIndex > 0)
                {
                    sb.Append(" and mem_Unit_ID=" + this.drp_unit.SelectedValue + "");
                    this.HiddenUserName.Value = this.drp_unit.SelectedItem.Text;
                }
                else
                {
                    this.HiddenUserName.Value = "全院";
                }
            }
            //查询个人
            if (this.drp_user.Value != "-1")
            {
                sb.Append(" AND mem_ID=" + this.drp_user.Value + " ");
                this.HiddenUserName.Value = this.drp_user.Items[this.drp_user.SelectedIndex].Text;
            }

            return sb.ToString();
        }
        //循环人员
        public void BindMem(string diffstr)
        {
            StringBuilder sb = new StringBuilder();
            if (diffstr == "inity")
            {
                if (mem_model != null)
                {
                    if (this.drp_unit.Items.FindByValue(mem_model.mem_Unit_ID.ToString()) != null)
                    {
                        this.drp_unit.Items.FindByValue(mem_model.mem_Unit_ID.ToString()).Selected = true;
                    }
                }

                this.hid_userid.Value = mem_ID.ToString();
                //绑定人员i
                BindUser();
                sb.Append(" mem_ID=" + mem_ID + " ");
            }
            else
            {
                //绑定人员
                BindUser();
                //查询条件
                sb.Append(BindWhere());

            }


            //int count = int.Parse(new TG.BLL.tg_member().GetRecordCount(sb.ToString()).ToString());
            ////所有记录数
            //this.AspNetPager1.RecordCount = count;

            string sql = "select *,(select unit_Order from tg_unitExt where unit_ID=mem_Unit_ID) as unitorder from tg_member where " + sb.ToString() + " order by unitorder asc,mem_Order asc,mem_ID asc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];


            if (dt != null && dt.Rows.Count > 0)
            {
                BindData(dt);
            }
        }
        //绑定数据
        public void BindData(DataTable list_mem)
        {

            string drp_year = this.drp_year.SelectedValue;
            string drp_month = this.drp_month.SelectedValue;
            var next_year = Convert.ToInt32(drp_year);
            var next_month = (Convert.ToInt32(drp_month) - 1);
            if (drp_month == "1")
            {
                next_year = (Convert.ToInt32(drp_year) - 1);
                next_month = 12;
            }


            DateTime all_starttime = Convert.ToDateTime(next_year + "-" + next_month + "-16");
            DateTime all_endtime = Convert.ToDateTime(drp_year + "-" + drp_month + "-15");

            //获取锁定数据
            List<TG.Model.cm_ApplyStatisDetailHis> his_list = new TG.BLL.cm_ApplyStatisDetailHis().GetModelList(" (ISDATE(dataDate)=1 and dataDate between '" + all_starttime.ToString("yyyy-MM-dd") + "' and '" + all_endtime.ToString("yyyy-MM-dd") + "') or  dataDate='" + (all_endtime.ToString("yyyy-MM")) + "合计'");

            //打卡数据
            DataTable datatable = GetCurrentMonth((drp_year + drp_month.PadLeft(2, '0')), "1");
            string towork = "", offwork = "";
            //后台设置上班时间
            List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_year=" + drp_year + " and attend_month=" + drp_month);
            //筛选当前年当前人，小于当前月之前的，部门活动申请数据总小时
            string unitwhere = string.Format(" and convert(varchar(10),starttime,120)>='{0}' and convert(varchar(10),endtime,120)<'{1}' and applytype='depart' ", ((Convert.ToInt32(drp_year) - 1) + "-12-16"), (all_starttime.ToString("yyyy-MM-dd")));
            DataTable unit_dt = new TG.BLL.cm_ApplyInfo().GetApplyList(unitwhere).Tables[0];
            //节假日
            List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" 1=1 order by id");
            //手动修改数据
            List<TG.Model.cm_ApplyStatisData> list_data = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='StatisDetail' order by id desc");
            //table 标题只显示一次
            bool headflag = true;
            //循环人员
            foreach (DataRow mem_dr in list_mem.Rows)
            {
                int memid = Convert.ToInt32(mem_dr["mem_ID"]);
                string memname = mem_dr["mem_Name"].ToString();
                List<TG.Model.cm_ApplyStatisDetailHis> his_listmem = new List<TG.Model.cm_ApplyStatisDetailHis>();
                if (his_list != null && his_list.Count > 0)
                {
                    his_listmem = his_list.Where(h => h.mem_id == memid).ToList();
                }

                string headStr = "<tr><th style='width:90px;'>&nbsp;</th>";
                string htmlStr = "<tr style='background-color:#f5f5f5'><td mem_id='" + memid + "' mem_unitid='" + mem_dr["mem_Unit_ID"] + "'><b>" + memname + "</b></td>";
                string leaveStr = "<tr><td align=\"right\">请假</td>";
                string sickStr = "<tr><td align=\"right\">病假</td>";
                string overtimeStr = "<tr><td align=\"right\">加班</td>";
                string marryStr = "<tr><td align=\"right\">婚假</td>";
                string materStr = "<tr><td align=\"right\">产/陪产假</td>";
                string dieStr = "<tr><td align=\"right\">丧假</td>";
                string lateStr = "<tr><td align=\"right\">迟到(次数)</td>";


                //人员数据
                if (his_listmem != null && his_listmem.Count > 0)
                {
                    if (headflag)
                    {
                        DateTime starttime2 = all_starttime;
                        DateTime endtime2 = all_endtime;

                        for (; starttime2.CompareTo(endtime2) <= 0; starttime2 = starttime2.AddDays(1))
                        {
                            headStr = headStr + "<th style='width:40px; text-align:center; font-weight:bold;' year=" + starttime2.Year + " month=" + starttime2.Month + ">" + starttime2.Day + "</th>";
                        }
                        headStr = headStr + "<th style='width:60px; text-align:center;'>&nbsp;</th></tr>";

                        lit.Text = "<thead>" + headStr + "</thead><tbody>";
                        headflag = false;
                    }

                    //排除最后合计
                    his_listmem.Where(h => h.dataDate.Contains("合计") == false).ToList().ForEach(h =>
                    {

                        string bgcolor = "";
                        if (h.isHoliday == "s")
                        {
                            bgcolor = " style=' background-color:green;'";
                        }

                        htmlStr = htmlStr + "<td></td>";
                        leaveStr = leaveStr + "<td " + bgcolor + " rel=" + memid + " datatype='leave' value='" + h.time_leave + "'>" + h.time_leave + "</td>";
                        sickStr = sickStr + "<td " + bgcolor + " rel=" + memid + " datatype='sick' value='" + h.time_sick + "'>" + h.time_sick + "</td>";
                        overtimeStr = overtimeStr + "<td " + bgcolor + " rel=" + memid + " datatype='overtime' value='" + h.time_over + "'>" + h.time_over + "</td>";
                        marryStr = marryStr + "<td " + bgcolor + " rel=" + memid + " datatype='marry' value='" + h.time_marry + "'>" + h.time_marry + "</td>";
                        materStr = materStr + "<td " + bgcolor + " rel=" + memid + " datatype='mater' value='" + h.time_mater + "'>" + h.time_mater + "</td>";
                        dieStr = dieStr + "<td " + bgcolor + " rel=" + memid + " datatype='die' value='" + h.time_die + "'>" + h.time_die + "</td>";
                        lateStr = lateStr + "<td " + bgcolor + " rel=" + memid + " datatype='late' value='" + h.time_late + "'>" + h.time_late + "</td>";

                    });
                    TG.Model.cm_ApplyStatisDetailHis his_model = his_listmem[his_listmem.Count - 1];
                    htmlStr = htmlStr + "<td><b>合计</b></td></tr>";
                    leaveStr = leaveStr + "<td><b>" + his_model.time_leave + "</b></td></tr>";
                    sickStr = sickStr + "<td><b>" + his_model.time_sick + "</b></td></tr>";
                    overtimeStr = overtimeStr + "<td><b>" + his_model.time_over + "</b></td></tr>";
                    marryStr = marryStr + "<td><b>" + his_model.time_marry + "</b></td></tr>";
                    materStr = materStr + "<td><b>" + his_model.time_mater + "</b></td></tr>";
                    dieStr = dieStr + "<td><b>" + his_model.time_die + "</b></td></tr>";
                    lateStr = lateStr + "<td><b>" + his_model.time_late + "</b></td></tr>";

                    lit.Text = lit.Text + htmlStr + leaveStr + sickStr + overtimeStr + marryStr + materStr + dieStr + lateStr;

                }
                else
                {

                    //后台设置上班时间              
                    if (pas_list != null && pas_list.Count > 0)
                    {
                        List<TG.Model.cm_PersonAttendSet> pas_list2 = pas_list.Where(p => p.mem_ID == memid).ToList();
                        if (pas_list2 != null && pas_list2.Count > 0)
                        {
                            towork = pas_list2[0].ToWork;
                            offwork = pas_list2[0].OffWork;
                        }
                        else
                        {
                            towork = "09:00";
                            offwork = "17:30";
                        }
                    }
                    else
                    {
                        towork = "09:00";
                        offwork = "17:30";
                    }

                    //筛选当前年当前人，小于当前月之前的，部门活动申请数据使用带薪部门假总小时
                    decimal sumunittime = 0;

                    if (unit_dt != null && unit_dt.Rows.Count > 0)
                    {
                        DataTable unit_dt2 = new DataView(unit_dt) { RowFilter = "adduser=" + memid + "" }.ToTable();
                        if (unit_dt2 != null && unit_dt2.Rows.Count > 0)
                        {
                            //得到小于一天的申请
                            sumunittime = unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > 0 && s.Field<decimal>("totaltime") <= Convert.ToDecimal(7.5)).Sum(s => s.Field<decimal>("totaltime"));
                            //大于一天申请，按7.5算
                            sumunittime = sumunittime + ((unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > Convert.ToDecimal(7.5)).Count()) * Convert.ToDecimal(7.5));
                        }
                    }

                    //合计
                    decimal sum_leave = 0;
                    decimal sum_sick = 0;
                    decimal sum_over = 0;
                    decimal sum_marry = 0;
                    decimal sum_mater = 0;
                    decimal sum_die = 0;
                    decimal sum_late = 0;

                    //重新赋值
                    DateTime starttime = Convert.ToDateTime(next_year + "-" + next_month + "-16");
                    DateTime endtime = Convert.ToDateTime(drp_year + "-" + drp_month + "-15");

                    for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddDays(1))
                    {
                        //获取某人某年某月某日
                        List<TG.Model.cm_ApplyStatisData> list_data2 = new List<TG.Model.cm_ApplyStatisData>();
                        if (list_data != null && list_data.Count > 0)
                        {
                            list_data2 = list_data.Where(asd => asd.mem_id == memid && asd.dataYear == starttime.Year && asd.dataMonth == starttime.Month && asd.dataDay == starttime.Day).ToList();
                        }

                        string colorStr = "";
                        if (list != null && list.Count > 0)
                        {

                            List<TG.Model.cm_HolidayConfig> list2 = list.Where(h => Convert.ToDateTime(h.holiday).ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd")).OrderBy(h => h.id).ToList();
                            //存在
                            if (list2 != null && list2.Count > 0)
                            {
                                //节假日，需减1天
                                if (list2[0].daytype == 1)
                                {
                                    colorStr = " style=' background-color:green;' ";
                                }
                            }
                            else
                            {
                                //周六日0~6
                                string temp = starttime.DayOfWeek.ToString();
                                if (temp == "Sunday" || temp == "Saturday")
                                {
                                    colorStr = " style=' background-color:green;' ";
                                }
                            }
                        }
                        else
                        {
                            //周六日0~6
                            string temp = starttime.DayOfWeek.ToString();
                            if (temp == "Sunday" || temp == "Saturday")
                            {
                                colorStr = " style=' background-color:green;' ";
                            }
                        }

                        //上班时间
                        //  DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00:59");
                        //中午下班时间
                        DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50:00");
                        //中午上班时间
                        DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00:59");
                        //下午下班时间
                        //   DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");
                        //考勤打卡数据   
                        DateTime tomr_endtime = starttime;

                        DataTable dt_late = new DataTable();
                        if (datatable != null && datatable.Rows.Count > 0)
                        {
                            dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + ((tomr_endtime.AddDays(1)).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + memname + "'" }.ToTable();

                        }
                        //申请记录
                        tomr_endtime = starttime;
                        string where = string.Format(" and (('{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120)) or ('{0} 06:00:00'<=starttime and endtime<='{1} 06:00:00')) and adduser={2}", starttime.ToString("yyyy-MM-dd"), (tomr_endtime.AddDays(1)).ToString("yyyy-MM-dd"), memid);
                        DataTable dt = new TG.BLL.cm_ApplyInfo().GetApplyList(where).Tables[0];

                        decimal time_leave = 0;
                        decimal time_sick = 0;
                        decimal time_over = 0;
                        decimal time_marry = 0;
                        decimal time_mater = 0;
                        decimal time_die = 0;
                        decimal time_late = 0;
                        //小于当前时间
                        if (starttime <= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                        {

                            //先判断系统打卡时间，如果无打开时间，则判断当天是否有申请                  
                            if (dt_late != null && dt_late.Rows.Count > 0)
                            {
                                DataTable travel_yestoday = new DataTable();
                                DataTable dt_apply = new DataTable();
                                DataTable dt_flag = new DataTable();
                                //申请记录
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    //出差或外勤今天的0点到6,允许迟到半天
                                    travel_yestoday = new DataView(dt) { RowFilter = "applytype in ('travel','gomeet','forget') and endtime<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();

                                    //申请记录 只读取当天6点到第二天6点数据                                  
                                    dt_apply = new DataView(dt) { RowFilter = "applytype not in ('addwork') " }.ToTable();
                                    if (dt_apply != null && dt_apply.Rows.Count > 0)
                                    {
                                        dt_flag = new DataView(dt_apply) { RowFilter = "endtime>'" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();

                                        #region 请假
                                        DataTable dt_leave = new DataView(dt_apply) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                        time_leave = time_leave + GetLeave(dt_leave, starttime);
                                        //每年部门活动带薪时间3天（7.5*3小时），超过22.5小时按事假计算,每次申请最多使用1天
                                        decimal unittime = 0;
                                        DataTable dt_temp = new DataView(dt_apply) { RowFilter = "applytype='depart' " }.ToTable();
                                        if (dt_temp != null && dt_temp.Rows.Count > 0)
                                        {
                                            //当天的部门活动小时
                                            unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                        }

                                        time_leave = time_leave + unittime;
                                        #endregion
                                        #region 病假
                                        DataTable dt_sick = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='病假' " }.ToTable();
                                        time_sick = time_sick + GetLeave(dt_sick, starttime);
                                        #endregion
                                        #region 加班
                                        //申请记录 只读取当天6点到第二天6点数据
                                        tomr_endtime = starttime;
                                        DataTable dt_over = new DataView(dt_apply) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                        if (dt_over != null && dt_over.Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dt_over.Rows)
                                            {
                                                decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                                time_over = time_over + totaltime_over;
                                            }
                                        }

                                        #endregion
                                        #region 婚假
                                        DataTable dt_marry = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='婚假' " }.ToTable();
                                        time_marry = time_marry + GetLeave(dt_marry, starttime);
                                        #endregion
                                        #region 产/陪产假
                                        DataTable dt_mater = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='产/陪产假' " }.ToTable();
                                        time_mater = time_mater + GetLeave(dt_mater, starttime);
                                        #endregion
                                        #region 丧假
                                        DataTable dt_die = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='丧假' " }.ToTable();
                                        time_die = time_die + GetLeave(dt_die, starttime);
                                        #endregion

                                    }
                                }

                                if (colorStr == "")//排除节假日和周六日
                                {
                                    #region 迟到
                                    DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                    DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                    DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                    DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间


                                    //出差和外勤申请记录
                                    string str = "";
                                    //判断申请记录是否存在,打卡记录只有一条或无数据或上午出差，下午出差时用到
                                    bool flag = false;
                                    if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                    {
                                        str = "bt";
                                    }
                                    //排除当天早上6点之前记录，属于昨天的数据
                                    if (dt_flag != null && dt_flag.Rows.Count > 0)
                                    {
                                        flag = true;
                                    }
                                    //迟到情况,小于下班时间，或 第一次打卡时间超过下班时间
                                    if ((shiji > houtai && shiji < houtaioff) || shiji > houtaioff)
                                    {

                                        //昨天下班打卡时间
                                        string yestoday = "";
                                        DateTime yes_time = starttime;
                                        //昨天
                                        yes_time = yes_time.AddDays(-1);
                                        DataTable dt_yestoday = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (yes_time.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + memname + "'" }.ToTable();
                                        if (dt_yestoday != null && dt_yestoday.Rows.Count > 0)
                                        {
                                            yestoday = Convert.ToDateTime(dt_yestoday.Rows[(dt_yestoday.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm");
                                        }
                                        //已查询允许迟到半天
                                        if (str == "")
                                        {
                                            //出差或外勤昨天22点到23:50,允许迟到半小时
                                            string strwhere = string.Format(" and applytype in ('travel','gomeet','forget') and '{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) and adduser={1} and starttime>='{2}' and endtime<='{3}'", yes_time.ToString("yyyy-MM-dd"), memid, (yes_time.ToString("yyyy-MM-dd") + " 06:00:00"), (yes_time.ToString("yyyy-MM-dd") + " 23:59:59"));
                                            travel_yestoday = new TG.BLL.cm_ApplyInfo().GetApplyList(strwhere).Tables[0];
                                            if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                            {
                                                //查询结束时间是否大于23点50分钟
                                                DataTable temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 23:50:00'") }.ToTable();
                                                if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                {
                                                    str = "bt";
                                                }
                                                else
                                                {
                                                    //查询结束时间是否大于22点00分钟
                                                    temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 21:50:00'") }.ToTable();
                                                    if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                    {
                                                        str = "bxs";
                                                    }
                                                }


                                            }
                                        }

                                        //昨天加班到22：00，所以允许今天迟到半小时后,统计从9:30
                                        if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 21:50:00") && Convert.ToDateTime(yestoday) < Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bxs")
                                        {
                                            houtai = houtai.AddMinutes(30);
                                        }
                                        //昨天加班到23:50，所以允许迟到半天,统计从13：0:0
                                        else if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bt")
                                        {
                                            houtai = zwsbtime;
                                        }

                                        TimeSpan ts = shiji - houtai;
                                        //大于等于4分钟才算迟到
                                        if (ts.TotalMinutes > 3)
                                        {

                                            if (ts.TotalMinutes >= 4 && ts.TotalMinutes <= 14)//迟到
                                            {
                                                time_late++;
                                            }
                                            else if (ts.TotalMinutes > 14 && ts.TotalMinutes < 30 && !flag) //迟到大于10分钟按0.5事假统计
                                            {
                                                time_leave = time_leave + decimal.Parse("0.5");
                                            }
                                            else
                                            {
                                                //没有申请记录
                                                if (!flag)
                                                {
                                                    //第一次打卡时间超过下班时间
                                                    if (shiji > houtaioff)
                                                    {
                                                        //下班时间-上班时间减去一小时
                                                        TimeSpan ts2 = houtaioff - houtai;
                                                        time_leave = time_leave + GetHours(ts2);
                                                        //上班时间不是下午1点。
                                                        if (houtai < zwsbtime)
                                                        {
                                                            time_leave = time_leave - 1;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //上班未上班, 正常上班时间9点、9点半
                                                        if (shiji >= zwxbtime && zwxbtime > houtai)
                                                        {
                                                            TimeSpan ts2 = zwxbtime - houtai;
                                                            time_leave = time_leave + GetHours(ts2);
                                                        }
                                                        else
                                                        {
                                                            //上午上班，打卡迟到大于30分钟
                                                            time_leave = time_leave + GetHours(ts);

                                                        }

                                                        //实际打卡时间大于下午上班时间, 正常上班时间9点、9点半
                                                        if (shiji > zwsbtime && zwxbtime > houtai)
                                                        {
                                                            TimeSpan ts3 = shiji - zwsbtime;
                                                            time_leave = time_leave + GetHours(ts3);
                                                        }
                                                    }
                                                }
                                            }

                                            //超过5次后，按照0.5小时事假扣除
                                            if (time_late > 0 && (sum_late + 1) > 5)
                                            {
                                                time_leave = time_leave + decimal.Parse("0.5");
                                            }
                                        }
                                    }

                                    //早退
                                    if (dt_late.Rows.Count > 0 && shijioff < houtaioff && !flag)
                                    {
                                        DateTime zaotui = shijioff;
                                        //只有一条早上打卡记录,按上班时间计算
                                        if (shijioff < houtai)
                                        {
                                            zaotui = houtai;
                                        }
                                        //下班打卡时间是上午,中午下班时间-早退时间
                                        if (shijioff < zwxbtime)
                                        {
                                            if (dt_late.Rows.Count == 1)
                                            {
                                                zaotui = houtai;
                                                TimeSpan ts_sw = zwxbtime - zaotui;
                                                time_leave = GetHours(ts_sw);

                                                //超过5次后，按照0.5小时事假扣除
                                                if (time_late > 0 && (sum_late + 1) > 5)
                                                {
                                                    time_leave = time_leave + decimal.Parse("0.5");
                                                }
                                            }
                                            else
                                            {
                                                TimeSpan ts_sw = zwxbtime - zaotui;
                                                time_leave = time_leave + GetHours(ts_sw);
                                            }
                                        }


                                        //下班打卡时间是下午1点之前,就默认是下午1点
                                        if (shijioff < zwsbtime)
                                        {
                                            zaotui = zwsbtime;
                                        }

                                        TimeSpan ts2 = houtaioff - zaotui;
                                        decimal ztqj = GetHours(ts2);

                                        time_leave = time_leave + ztqj;
                                    }

                                    //下班时间+30分钟，算加班
                                    DateTime jiaban = houtaioff.AddMinutes(30);
                                    //晚上加班 开始打卡时间大于加班时间已超过晚上6点，无需加半个小时
                                    if (shiji > jiaban && dt_late.Rows.Count > 1)
                                    {

                                        int mine = shiji.Minute;
                                        if (mine > 0 && mine < 15)
                                        {
                                            jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                        }
                                        else if (mine > 30 && mine < 45)
                                        {
                                            jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                        }
                                        else
                                        {
                                            jiaban = shiji;
                                        }

                                    }
                                    //加班统计                       
                                    if (shijioff > jiaban)
                                    {
                                        decimal totaltime_forget = 0;
                                        //判断是否有未打卡申请记录
                                        tomr_endtime = starttime;
                                        if (dt != null && dt.Rows.Count > 0)
                                        {
                                            DataTable dt_over = new DataView(dt) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                            if (dt_over != null && dt_over.Rows.Count > 0)
                                            {
                                                totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                            }
                                        }
                                        //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                        if (totaltime_forget == 0)
                                        {
                                            time_over = time_over + GetOver(jiaban, shijioff);
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    //周六日，节假日加班

                                    DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                    DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                    DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                    DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                                    //实际打卡时间

                                    int mine = shiji.Minute;
                                    DateTime jiaban = shiji;
                                    if (mine > 0 && mine < 15)
                                    {
                                        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                    }
                                    else if (mine > 30 && mine < 45)
                                    {
                                        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                    }


                                    decimal totaltime_forget = 0;
                                    //判断是否有未打卡申请记录
                                    tomr_endtime = starttime;
                                    if (dt != null && dt.Rows.Count > 0)
                                    {
                                        DataTable dt_over = new DataView(dt) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                        if (dt_over != null && dt_over.Rows.Count > 0)
                                        {
                                            totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                        }
                                    }
                                    //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                    if (totaltime_forget == 0)
                                    {
                                        time_over = time_over + GetOver(jiaban, shijioff);
                                    }

                                    #region 按考勤计算加班

                                    ////全天
                                    //if (shiji <= houtai && shijioff >= houtaioff)
                                    //{
                                    //    time_over = time_over + decimal.Parse("7.5");
                                    //}
                                    //else
                                    //{
                                    //   DateTime start = shiji;
                                    //  DateTime end = shijioff;

                                    //    //加班统计   
                                    //    if (shiji < houtai)
                                    //    {
                                    //        start = houtai;
                                    //    }
                                    //    else if (shiji >= zwxbtime && shiji <= zwsbtime)
                                    //    {
                                    //        shiji = zwsbtime;
                                    //    }
                                    //    if (shijioff > houtaioff)
                                    //    {
                                    //        end = houtaioff;
                                    //    }
                                    //    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
                                    //    {
                                    //        end = zwxbtime;
                                    //    }

                                    //    //都是上午打卡    
                                    //    if (end <= zwxbtime)
                                    //    {
                                    //        end = shijioff;
                                    //        time_over = time_over + GetOver(start, end);
                                    //    }
                                    //    else //下班打卡是下午
                                    //    {
                                    //        //上班打卡也是下午
                                    //        if (start >= zwsbtime)
                                    //        {
                                    //            time_over = time_over + GetOver(start, end);
                                    //        }
                                    //        else//上班打卡是上午
                                    //        {
                                    //            //上午加班时间
                                    //            time_over = time_over + GetOver(start, zwxbtime);
                                    //            //下午加班时间
                                    //            time_over = time_over + GetOver(zwsbtime, end);
                                    //        }
                                    //    }
                                    //}
                                    ////晚上加班
                                    ////第一次打卡时间已超过晚上6点，无需加半个小时
                                    //DateTime jiaban = houtaioff.AddMinutes(30);
                                    //if (shiji > jiaban)
                                    //{
                                    //    int mine = shiji.Minute;
                                    //    if (mine > 0 && mine < 15)
                                    //    {
                                    //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                    //    }
                                    //    else if (mine > 30 && mine < 45)
                                    //    {
                                    //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                    //    }
                                    //    else
                                    //    {
                                    //        jiaban = shiji;
                                    //    }

                                    //}

                                    ////加班统计                       
                                    //if (shijioff > jiaban)
                                    //{
                                    //    time_over = time_over + GetOver(jiaban, shijioff);
                                    //}
                                    #endregion

                                }
                            }
                            else
                            {

                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    //排除节假日和周六日
                                    if (colorStr == "")
                                    {
                                        #region 请假
                                        DataTable dt_leave = new DataView(dt) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                        time_leave = time_leave + GetLeave(dt_leave, starttime);
                                        //每年部门活动带薪时间3天（7.5*3小时），超过22.5小时按事假计算,每次申请最多使用1天
                                        decimal unittime = 0;
                                        DataTable dt_temp = new DataView(dt) { RowFilter = "applytype='depart' " }.ToTable();
                                        if (dt_temp != null && dt_temp.Rows.Count > 0)
                                        {
                                            //当天的部门活动小时
                                            unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                        }

                                        time_leave = time_leave + unittime;
                                        #endregion
                                        #region 病假
                                        DataTable dt_sick = new DataView(dt) { RowFilter = "applytype='leave' and reason='病假' " }.ToTable();
                                        time_sick = time_sick + GetLeave(dt_sick, starttime);
                                        #endregion
                                        #region 婚假
                                        DataTable dt_marry = new DataView(dt) { RowFilter = "applytype='leave' and reason='婚假' " }.ToTable();
                                        time_marry = time_marry + GetLeave(dt_marry, starttime);
                                        #endregion
                                        #region 产/陪产假
                                        DataTable dt_mater = new DataView(dt) { RowFilter = "applytype='leave' and reason='产/陪产假' " }.ToTable();
                                        time_mater = time_mater + GetLeave(dt_mater, starttime);
                                        #endregion
                                        #region 丧假
                                        DataTable dt_die = new DataView(dt) { RowFilter = "applytype='leave' and reason='丧假' " }.ToTable();
                                        time_die = time_die + GetLeave(dt_die, starttime);
                                        #endregion
                                    }
                                    #region 加班
                                    //只读取当天6点到第二天6点数据
                                    tomr_endtime = starttime;
                                    DataTable dt_over = new DataView(dt) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                    if (dt_over != null && dt_over.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dt_over.Rows)
                                        {
                                            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                            time_over = time_over + totaltime_over;
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    if (colorStr == "")
                                    {
                                        //一天未打卡，无申请
                                        time_leave = time_leave + decimal.Parse("7.5");
                                    }
                                }

                            }

                            if (dt != null && dt.Rows.Count > 0)
                            {
                                //加班离岗
                                DataTable dt_addwork = new DataView(dt) { RowFilter = "applytype='addwork' " }.ToTable();
                                if (dt_addwork != null && dt_addwork.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt_addwork.Rows)
                                    {
                                        decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                        time_over = time_over - totaltime_over;
                                    }
                                }
                            }
                        }
                        string colorStr_leave = colorStr;
                        string colorStr_sick = colorStr;
                        string colorStr_overtime = colorStr;
                        string colorStr_marry = colorStr;
                        string colorStr_mater = colorStr;
                        string colorStr_die = colorStr;
                        string colorStr_late = colorStr;
                        //判断是否手动改过
                        if (list_data2 != null && list_data2.Count > 0)
                        {
                            var data_model = list_data2.Where(d => d.dataType == "late").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_late = data_model.dataValue;
                                //超过5次后，按照0.5小时事假扣除
                                if (time_late > 0 && (sum_late + 1) > 5)
                                {
                                    time_leave = time_leave + decimal.Parse("0.5");
                                }
                                colorStr_late = " style='background-color:yellow;'";
                            }
                             data_model = list_data2.Where(d => d.dataType == "leave").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_leave = data_model.dataValue;
                                colorStr_leave = " style='background-color:yellow;'";
                            }

                            data_model = list_data2.Where(d => d.dataType == "sick").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_sick = data_model.dataValue;
                                colorStr_sick = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_over = data_model.dataValue;
                                colorStr_overtime = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "marry").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_marry = data_model.dataValue;
                                colorStr_marry = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "mater").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_mater = data_model.dataValue;
                                colorStr_mater = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "die").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_die = data_model.dataValue;
                                colorStr_die = " style='background-color:yellow;'";
                            }
                            

                        }

                        headStr = headStr + "<th style='width:40px; text-align:center; font-weight:bold;' year=" + starttime.Year + " month=" + starttime.Month + ">" + starttime.Day + "</th>";
                        htmlStr = htmlStr + "<td></td>";
                        leaveStr = leaveStr + "<td " + colorStr_leave + " rel=" + memid + " datatype='leave' value='" + (Math.Round(time_leave, 1) == 0 ? "" : time_leave.ToString("f1")) + "'>" + (Math.Round(time_leave, 1) == 0 ? "" : time_leave.ToString("f1")) + "</td>";
                        sickStr = sickStr + "<td " + colorStr_sick + " rel=" + memid + " datatype='sick' value='" + (Math.Round(time_sick, 1) == 0 ? "" : time_sick.ToString("f1")) + "'>" + (Math.Round(time_sick, 1) == 0 ? "" : time_sick.ToString("f1")) + "</td>";
                        overtimeStr = overtimeStr + "<td " + colorStr_overtime + " rel=" + memid + " datatype='overtime' value='" + (Math.Round(time_over, 1) == 0 ? "" : time_over.ToString("f1")) + "'>" + (Math.Round(time_over, 1) == 0 ? "" : time_over.ToString("f1")) + "</td>";
                        marryStr = marryStr + "<td " + colorStr_marry + " rel=" + memid + " datatype='marry' value='" + (Math.Round(time_marry, 1) == 0 ? "" : time_marry.ToString("f1")) + "'>" + (Math.Round(time_marry, 1) == 0 ? "" : time_marry.ToString("f1")) + "</td>";
                        materStr = materStr + "<td " + colorStr_mater + " rel=" + memid + " datatype='mater' value='" + (Math.Round(time_mater, 1) == 0 ? "" : time_mater.ToString("f1")) + "'>" + (Math.Round(time_mater, 1) == 0 ? "" : time_mater.ToString("f1")) + "</td>";
                        dieStr = dieStr + "<td " + colorStr_die + " rel=" + memid + " datatype='die' value='" + (Math.Round(time_die, 1) == 0 ? "" : time_die.ToString("f1")) + "'>" + (Math.Round(time_die, 1) == 0 ? "" : time_die.ToString("f1")) + "</td>";
                        lateStr = lateStr + "<td " + colorStr_late + " rel=" + memid + " datatype='late' value='" + (time_late == 0 ? "" : time_late.ToString("f0")) + "'>" + (time_late == 0 ? "" : time_late.ToString("f0")) + "</td>";

                        //合计
                        sum_leave = sum_leave + time_leave;
                        sum_sick = sum_sick + time_sick;
                        sum_over = sum_over + time_over;
                        sum_marry = sum_marry + time_marry;
                        sum_mater = sum_mater + time_mater;
                        sum_die = sum_die + time_die;
                        sum_late = sum_late + time_late;
                    }

                    headStr = headStr + "<th style='width:60px; text-align:center;'>&nbsp;</th></tr>";
                    htmlStr = htmlStr + "<td><b>合计</b></td></tr>";
                    leaveStr = leaveStr + "<td><b>" + sum_leave.ToString("f1") + "</b></td></tr>";
                    sickStr = sickStr + "<td><b>" + sum_sick.ToString("f1") + "</b></td></tr>";
                    overtimeStr = overtimeStr + "<td><b>" + sum_over.ToString("f1") + "</b></td></tr>";
                    marryStr = marryStr + "<td><b>" + sum_marry.ToString("f1") + "</b></td></tr>";
                    materStr = materStr + "<td><b>" + sum_mater.ToString("f1") + "</b></td></tr>";
                    dieStr = dieStr + "<td><b>" + sum_die.ToString("f1") + "</b></td></tr>";
                    lateStr = lateStr + "<td><b>" + sum_late.ToString("f0") + "</b></td></tr>";
                    if (headflag)
                    {
                        lit.Text = "<thead>" + headStr + "</thead><tbody>";
                    }
                    lit.Text = lit.Text + htmlStr + leaveStr + sickStr + overtimeStr + marryStr + materStr + dieStr + lateStr;
                    headflag = false;
                }
            }
            lit.Text = lit.Text + "</tbody>";

        }

        ////返回加班时间
        //public decimal GetOver(DateTime start, DateTime end)
        //{
        //    decimal over = 0;
        //    if (end > start)
        //    {
        //        TimeSpan ts_jb = end - start;
        //        //强制转换，没有四舍五入
        //        int hours = (int)(ts_jb.TotalHours);

        //        double minutes = ts_jb.TotalMinutes - (hours * 60);
        //        //大于15分钟，即加班
        //        if (minutes > 15 && minutes <= 45)
        //        {
        //            over = decimal.Parse("0.5");
        //        }
        //        else if (minutes > 45 && minutes < 60)
        //        {
        //            over = decimal.Parse("1");
        //        }

        //        over = over + hours;
        //    }
        //    return over;
        //}
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }

            for (int i = 1; i <= 12; i++)
            {
                this.drp_month.Items.Add(i.ToString());
            }

        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
                this.isapplymanager.Value = base.IsApplyManager.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        protected void btn_seach_Click(object sender, EventArgs e)
        {
            //绑定数据
            BindMem("");
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        //protected void AspNetPager1_PageChanged(object src, EventArgs e)
        //{
        //    BindMem("");
        //}

        protected void btn_export_Click(object sender, EventArgs e)
        {
            string sqlwhere = BindWhere();
            string sql = "select *,(select unit_Order from tg_unitExt where unit_ID=mem_Unit_ID) as unitorder from tg_member where " + sqlwhere + " order by unitorder asc,mem_Order asc,mem_ID asc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/ApplyStatisDetail.xls", this.HiddenUserName.Value);
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }

        protected void btn_allexport_Click(object sender, EventArgs e)
        {
            string drp_year = this.drp_year.SelectedValue;
            string drp_month = this.drp_month.SelectedValue;
            var next_year = Convert.ToInt32(drp_year);
            var next_month = (Convert.ToInt32(drp_month) - 1);
            if (drp_month == "1")
            {
                next_year = (Convert.ToInt32(drp_year) - 1);
                next_month = 12;
            }

            //排除离职人员
            string sqlwhere = " (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + next_year + "-" + next_month.ToString().PadLeft(2, '0') + "-16'))";
            string sql = "select *,(select unit_Order from tg_unitExt where unit_ID=mem_Unit_ID) as unitorder from tg_member where " + sqlwhere + " order by unitorder asc,mem_Order asc,mem_ID asc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                ExportDataToExcel(dt, "~/TemplateXls/ApplyStatisDetail.xls", "全院");
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无数据！');history.back();</script>");

            }
        }
        private void ExportDataToExcel(DataTable list_mem, string modelPath, string pathname)
        {
            string drpyear = this.drp_year.SelectedValue;
            var drpmonth = this.drp_month.SelectedValue;
            int nextyear = Convert.ToInt32(drpyear);
            var nextmonth = Convert.ToInt32(drpmonth) - 1;
            if (drpmonth == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) - 1);
                nextmonth = 12;
            }
            pathname = "【" + pathname.Trim() + "】考勤统计" + nextyear + "." + nextmonth + ".16-" + drpyear + "." + drpmonth + ".15";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style1 = wb.CreateCellStyle();
            // style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //背景颜色样式
            ICellStyle style3 = wb.CreateCellStyle(); ;
            style3.VerticalAlignment = VerticalAlignment.CENTER;
            style3.WrapText = true;
            style3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style3.FillForegroundColor = HSSFColor.PINK.index;
            style3.SetFont(font1);

            //背景颜色样式
            ICellStyle style4 = wb.CreateCellStyle();
            style4.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style4.FillForegroundColor = HSSFColor.YELLOW.index;
            style4.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style4.VerticalAlignment = VerticalAlignment.CENTER;
            style4.WrapText = true;
            style4.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style4.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;


            //背景颜色样式
            ICellStyle style5 = wb.CreateCellStyle();
            style5.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style5.FillForegroundColor = HSSFColor.GREEN.index;
            style5.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style5.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;


            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            IRow dataRowTitle = ws.GetRow(0);
            dataRowTitle.Height = 30 * 20;
            //ws.SetColumnWidth(2, 25 * 256);         
            ICell acell21 = dataRowTitle.GetCell(0);
            acell21.SetCellValue(pathname);
            // CellRangeAddress cellRangeAddress2 = new CellRangeAddress(1, 1, 0, 1);
            // ((HSSFSheet)ws).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            //ws.AddMergedRegion(cellRangeAddress2);



            int index = 1;
            DateTime all_starttime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-16");
            DateTime all_endtime = Convert.ToDateTime(drpyear + "-" + drpmonth + "-15");
            //获取锁定数据
            List<TG.Model.cm_ApplyStatisDetailHis> his_list = new TG.BLL.cm_ApplyStatisDetailHis().GetModelList(" (ISDATE(dataDate)=1 and dataDate between '" + all_starttime.ToString("yyyy-MM-dd") + "' and '" + all_endtime.ToString("yyyy-MM-dd") + "') or  dataDate='" + (all_endtime.ToString("yyyy-MM")) + "合计'");
            //打卡数据
            DataTable datatable = GetCurrentMonth((drpyear + drpmonth.PadLeft(2, '0')), "1");
            string towork = "", offwork = "";
            //后台设置上班时间
            List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_year=" + drpyear + " and attend_month=" + drpmonth);
            //筛选当前年当前人，小于当前月之前的，部门活动申请数据总小时
            string unitwhere = string.Format(" and convert(varchar(10),starttime,120)>='{0}' and convert(varchar(10),endtime,120)<'{1}' and applytype='depart' ", ((Convert.ToInt32(drpyear) - 1) + "-12-16"), (all_starttime.ToString("yyyy-MM-dd")));
            DataTable unit_dt = new TG.BLL.cm_ApplyInfo().GetApplyList(unitwhere).Tables[0];
            //节假日
            List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" 1=1 order by id");
            //手动修改数据
            List<TG.Model.cm_ApplyStatisData> list_data = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource='StatisDetail' order by id desc");
            //table 标题只显示一次
            bool headflag = true;
            //循环人员
            foreach (DataRow mem_dr in list_mem.Rows)
            {

                int memid = Convert.ToInt32(mem_dr["mem_ID"]);
                string memname = mem_dr["mem_Name"].ToString();

                List<TG.Model.cm_ApplyStatisDetailHis> his_listmem = new List<TG.Model.cm_ApplyStatisDetailHis>();
                if (his_list != null && his_list.Count > 0)
                {
                    his_listmem = his_list.Where(h => h.mem_id == memid).ToList();
                }

                //行
                var dataRow = ws.GetRow(index);//第一行
                if (dataRow == null)
                    dataRow = ws.CreateRow(index);//生成行

                var dataRow2 = ws.GetRow((index + 1));//第二行
                if (dataRow2 == null)
                    dataRow2 = ws.CreateRow((index + 1));//生成行

                var dataRow3 = ws.GetRow((index + 2));//第三行
                if (dataRow3 == null)
                    dataRow3 = ws.CreateRow((index + 2));

                var dataRow4 = ws.GetRow((index + 3));//第四行
                if (dataRow4 == null)
                    dataRow4 = ws.CreateRow((index + 3));//生成行

                var dataRow5 = ws.GetRow((index + 4));//第五行
                if (dataRow5 == null)
                    dataRow5 = ws.CreateRow((index + 4));//生成行

                var dataRow6 = ws.GetRow((index + 5));//第六行
                if (dataRow6 == null)
                    dataRow6 = ws.CreateRow((index + 5));//生成行

                var dataRow7 = ws.GetRow((index + 6));//第七行
                if (dataRow7 == null)
                    dataRow7 = ws.CreateRow((index + 6));//生成行

                var dataRow8 = ws.GetRow((index + 7));//第八行
                if (dataRow8 == null)
                    dataRow8 = ws.CreateRow((index + 7));//生成行

                var dataRow9 = ws.GetRow((index + 8));//第九行
                if (dataRow9 == null)
                    dataRow9 = ws.CreateRow((index + 8));//生成行

                //第一行第一列
                ICell cell = null;
                if (headflag)
                {
                    cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style1;
                    cell.SetCellValue("");
                }

                //第二行第一列
                cell = dataRow2.GetCell(0);
                if (cell == null)
                    cell = dataRow2.CreateCell(0);

                cell.CellStyle = style3;
                cell.SetCellValue(memname);
                //第三行第一列              
                cell = dataRow3.CreateCell(0);
                cell.CellStyle = style2;
                cell.CellStyle.Alignment = HorizontalAlignment.RIGHT;
                cell.SetCellValue("请假");
                //第四行第一列              
                cell = dataRow4.CreateCell(0);
                cell.CellStyle = style2;
                cell.CellStyle.Alignment = HorizontalAlignment.RIGHT;
                cell.SetCellValue("病假");
                //第五行第一列
                cell = dataRow5.CreateCell(0);
                cell.CellStyle = style2;
                cell.CellStyle.Alignment = HorizontalAlignment.RIGHT;
                cell.SetCellValue("加班");
                //第六行第一列
                cell = dataRow6.CreateCell(0);
                cell.CellStyle = style2;
                cell.CellStyle.Alignment = HorizontalAlignment.RIGHT;
                cell.SetCellValue("婚假");
                //第七行第一列
                cell = dataRow7.CreateCell(0);
                cell.CellStyle = style2;
                cell.CellStyle.Alignment = HorizontalAlignment.RIGHT;
                cell.SetCellValue("产/陪产假");
                //第八行第一列
                cell = dataRow8.CreateCell(0);
                cell.CellStyle = style2;
                cell.CellStyle.Alignment = HorizontalAlignment.RIGHT;
                cell.SetCellValue("丧假");
                //第九行第一列
                cell = dataRow9.CreateCell(0);
                cell.CellStyle = style2;
                cell.CellStyle.Alignment = HorizontalAlignment.RIGHT;
                cell.SetCellValue("迟到(次数)");


                //人员锁定数据
                if (his_listmem != null && his_listmem.Count > 0)
                {
                    if (headflag)
                    {
                        DateTime starttime2 = all_starttime;
                        DateTime endtime2 = all_endtime;
                        int k = 1;
                        for (; starttime2.CompareTo(endtime2) <= 0; starttime2 = starttime2.AddDays(1))
                        {
                            //第一行第中间列
                            cell = dataRow.GetCell(k);
                            if (cell == null)
                                cell = dataRow.CreateCell(k);
                            cell.CellStyle = style1;
                            cell.SetCellValue(starttime2.Day);
                            k++;
                        }

                        //第一行第最后列
                        cell = dataRow.GetCell((k));
                        if (cell == null)
                            cell = dataRow.CreateCell((k));
                        cell.CellStyle = style1;
                        cell.SetCellValue("");

                        headflag = false;
                    }
                    int i = 1;
                    //排除最后合计
                    his_listmem.Where(h => h.dataDate.Contains("合计") == false).ToList().ForEach(h =>
                    {

                        //第二行第中间列
                        cell = dataRow2.GetCell(i);
                        if (cell == null)
                            cell = dataRow2.CreateCell(i);
                        cell.CellStyle = style3;
                        cell.SetCellValue("");
                        //第三行第中间列
                        cell = dataRow3.CreateCell(i);
                        cell.CellStyle = style2;

                        if (h.isHoliday == "s")
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue(h.time_leave);
                        //第四行第中间列
                        cell = dataRow4.CreateCell(i);
                        cell.CellStyle = style2;
                        if (h.isHoliday == "s")
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue(h.time_sick);
                        //第五行第中间列
                        cell = dataRow5.GetCell(i);
                        cell = dataRow5.CreateCell(i);
                        cell.CellStyle = style2;
                        if (h.isHoliday == "s")
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue(h.time_over);
                        //第六行第中间列
                        cell = dataRow6.CreateCell(i);
                        cell.CellStyle = style2;
                        if (h.isHoliday == "s")
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue(h.time_marry);
                        //第七行第中间列
                        cell = dataRow7.CreateCell(i);
                        cell.CellStyle = style2;
                        if (h.isHoliday == "s")
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue(h.time_mater);
                        //第八行第中间列
                        cell = dataRow8.CreateCell(i);
                        cell.CellStyle = style2;
                        if (h.isHoliday == "s")
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue(h.time_die);
                        //第九行第中间列
                        cell = dataRow9.CreateCell(i);
                        cell.CellStyle = style2;
                        if (h.isHoliday == "s")
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue(h.time_late);
                        i++;

                    });
                    TG.Model.cm_ApplyStatisDetailHis his_model = his_listmem[his_listmem.Count - 1];

                    //第二行第一列
                    cell = dataRow2.GetCell((i));
                    if (cell == null)
                        cell = dataRow2.CreateCell((i));
                    cell.CellStyle = style3;
                    // cell.CellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
                    // cell.CellStyle.FillForegroundColor = HSSFColor.PINK.index;
                    cell.SetCellValue("合计");
                    //第三行第一列
                    cell = dataRow3.CreateCell((i));
                    cell.CellStyle = style1;
                    cell.CellStyle.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(his_model.time_leave);
                    //第四行第一列
                    cell = dataRow4.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(his_model.time_sick);
                    //第五行第一列
                    cell = dataRow5.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(his_model.time_over);
                    //第六行第一列
                    cell = dataRow6.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(his_model.time_marry);
                    //第七行第一列
                    cell = dataRow7.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(his_model.time_mater);
                    //第八行第一列
                    cell = dataRow8.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(his_model.time_die);
                    //第九行第一列
                    cell = dataRow9.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(his_model.time_late);

                }
                else
                {

                    //后台设置上班时间              
                    if (pas_list != null && pas_list.Count > 0)
                    {
                        List<TG.Model.cm_PersonAttendSet> pas_list2 = pas_list.Where(p => p.mem_ID == memid).ToList();
                        if (pas_list2 != null && pas_list2.Count > 0)
                        {
                            towork = pas_list2[0].ToWork;
                            offwork = pas_list2[0].OffWork;
                        }
                        else
                        {
                            towork = "09:00";
                            offwork = "17:30";
                        }
                    }
                    else
                    {
                        towork = "09:00";
                        offwork = "17:30";
                    }

                    //筛选当前年当前人，小于当前月之前的，部门活动申请数据总小时
                    decimal sumunittime = 0;

                    if (unit_dt != null && unit_dt.Rows.Count > 0)
                    {
                        DataTable unit_dt2 = new DataView(unit_dt) { RowFilter = "adduser=" + memid + "" }.ToTable();
                        if (unit_dt2 != null && unit_dt2.Rows.Count > 0)
                        {
                            //得到小于一天的申请
                            sumunittime = unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > 0 && s.Field<decimal>("totaltime") <= Convert.ToDecimal(7.5)).Sum(s => s.Field<decimal>("totaltime"));
                            //大于一天申请，按7.5算
                            sumunittime = sumunittime + ((unit_dt2.AsEnumerable().Where(s => s.Field<decimal>("totaltime") > Convert.ToDecimal(7.5)).Count()) * Convert.ToDecimal(7.5));
                        }
                    }


                    //合计
                    decimal sum_leave = 0;
                    decimal sum_sick = 0;
                    decimal sum_over = 0;
                    decimal sum_marry = 0;
                    decimal sum_mater = 0;
                    decimal sum_die = 0;
                    decimal sum_late = 0;

                    //重新赋值
                    DateTime starttime = all_starttime;
                    DateTime endtime = all_endtime;

                    int i = 1;
                    for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddDays(1))
                    {
                        //获取某人某年某月某日
                        List<TG.Model.cm_ApplyStatisData> list_data2 = new List<TG.Model.cm_ApplyStatisData>();
                        if (list_data != null && list_data.Count > 0)
                        {
                            list_data2 = list_data.Where(asd => asd.mem_id == memid && asd.dataYear == starttime.Year && asd.dataMonth == starttime.Month && asd.dataDay == starttime.Day).ToList();
                        }

                        string colorStr = "";
                        if (list != null && list.Count > 0)
                        {

                            List<TG.Model.cm_HolidayConfig> list2 = list.Where(h => Convert.ToDateTime(h.holiday).ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd")).OrderBy(h => h.id).ToList();
                            //存在
                            if (list2 != null && list2.Count > 0)
                            {
                                //节假日，需减1天
                                if (list2[0].daytype == 1)
                                {
                                    colorStr = " style=' background-color:green;' ";
                                }
                            }
                            else
                            {
                                //周六日0~6
                                string temp = starttime.DayOfWeek.ToString();
                                if (temp == "Sunday" || temp == "Saturday")
                                {
                                    colorStr = " style=' background-color:green;' ";
                                }
                            }
                        }
                        else
                        {
                            //周六日0~6
                            string temp = starttime.DayOfWeek.ToString();
                            if (temp == "Sunday" || temp == "Saturday")
                            {
                                colorStr = " style=' background-color:green;' ";
                            }
                        }

                        //上班时间
                        //  DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00:59");
                        //中午下班时间
                        DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50");
                        //中午上班时间
                        DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00:59");
                        //下午下班时间
                        //   DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");
                        //考勤打卡数据     
                        DateTime tomr_endtime = starttime;
                        DataTable dt_late = new DataTable();
                        if (datatable != null && datatable.Rows.Count > 0)
                        {
                            dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + ((tomr_endtime.AddDays(1)).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + memname + "'" }.ToTable();
                        }
                        //申请记录
                        tomr_endtime = starttime;
                        string where = string.Format(" and (('{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120)) or ('{0} 06:00:00'<=starttime and endtime<='{1} 06:00:00')) and adduser={2}", starttime.ToString("yyyy-MM-dd"), (tomr_endtime.AddDays(1)).ToString("yyyy-MM-dd"), memid);
                        DataTable dt = new TG.BLL.cm_ApplyInfo().GetApplyList(where).Tables[0];


                        decimal time_leave = 0;
                        decimal time_sick = 0;
                        decimal time_over = 0;
                        decimal time_marry = 0;
                        decimal time_mater = 0;
                        decimal time_die = 0;
                        decimal time_late = 0;

                        //小于当前时间
                        if (starttime <= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                        {
                            //先判断系统打卡时间，如果无打开时间，则判断当天是否有申请                  
                            if (dt_late != null && dt_late.Rows.Count > 0)
                            {
                                DataTable travel_yestoday = new DataTable();
                                DataTable dt_apply = new DataTable();
                                DataTable dt_flag =new DataTable();
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    //出差或外勤今天的0点到6,允许迟到半天
                                    travel_yestoday = new DataView(dt) { RowFilter = "applytype in ('travel','gomeet','forget') and endtime<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();

                                    //申请记录统计                                 
                                    dt_apply = new DataView(dt) { RowFilter = "applytype not in ('addwork') " }.ToTable();
                                    if (dt_apply != null && dt_apply.Rows.Count > 0)
                                    {
                                        dt_flag = new DataView(dt_apply) { RowFilter = "endtime>'" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();

                                        #region 请假
                                        DataTable dt_leave = new DataView(dt_apply) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                        time_leave = time_leave + GetLeave(dt_leave, starttime);
                                        //每年部门活动带薪时间3天（7.5*3小时），超过22.5小时按事假计算,每次申请最多使用1天
                                        decimal unittime = 0;
                                        DataTable dt_temp = new DataView(dt_apply) { RowFilter = "applytype='depart' " }.ToTable();
                                        if (dt_temp != null && dt_temp.Rows.Count > 0)
                                        {
                                            //当天的部门活动小时
                                            unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                        }

                                        time_leave = time_leave + unittime;
                                        #endregion
                                        #region 病假
                                        DataTable dt_sick = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='病假' " }.ToTable();
                                        time_sick = time_sick + GetLeave(dt_sick, starttime);
                                        #endregion
                                        #region 加班
                                        tomr_endtime = starttime;
                                        //只读取当天6点到第二天6点数据
                                        DataTable dt_over = new DataView(dt_apply) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                        if (dt_over != null && dt_over.Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dt_over.Rows)
                                            {
                                                decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                                time_over = time_over + totaltime_over;
                                            }
                                        }

                                        #endregion
                                        #region 婚假
                                        DataTable dt_marry = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='婚假' " }.ToTable();
                                        time_marry = time_marry + GetLeave(dt_marry, starttime);
                                        #endregion
                                        #region 产/陪产假
                                        DataTable dt_mater = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='产/陪产假' " }.ToTable();
                                        time_mater = time_mater + GetLeave(dt_mater, starttime);
                                        #endregion
                                        #region 丧假
                                        DataTable dt_die = new DataView(dt_apply) { RowFilter = "applytype='leave' and reason='丧假' " }.ToTable();
                                        time_die = time_die + GetLeave(dt_die, starttime);
                                        #endregion
                                        
                                    }
                                }
                                if (colorStr == "")//排除节假日和周六日
                                {
                                    #region 迟到
                                    DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                    DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                    DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                    DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间

                                    //出差和外勤申请记录
                                    string str = "";
                                    //判断申请记录是否存在,打卡记录只有一条或无数据或上午出差，下午出差时用到
                                    bool flag = false;
                                    if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                    {
                                        str = "bt";
                                    }
                                    if (dt_flag != null && dt_flag.Rows.Count > 0)
                                    {                                       
                                        flag = true;
                                    }
                                    //迟到情况,小于下班时间 或 第一次打卡时间大于下班时间
                                    if ((shiji > houtai && shiji < houtaioff)||shiji>houtaioff)
                                    {
                                        //昨天下班打卡时间
                                        string yestoday = "";
                                        DateTime yes_time = starttime;
                                        yes_time = yes_time.AddDays(-1);
                                        DataTable dt_yestoday = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (yes_time.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + memname + "'" }.ToTable();
                                        if (dt_yestoday != null && dt_yestoday.Rows.Count > 0)
                                        {
                                            yestoday = Convert.ToDateTime(dt_yestoday.Rows[(dt_yestoday.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm");
                                        }
                                        //已查询允许迟到半天
                                        if (str == "")
                                        {
                                            //出差或外勤昨天22点到23:50,允许迟到半小时
                                            string strwhere = string.Format(" and applytype in ('travel','gomeet','forget') and '{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) and adduser={1} and starttime>='{2}' and endtime<='{3}'", yes_time.ToString("yyyy-MM-dd"), memid, (yes_time.ToString("yyyy-MM-dd") + " 06:00:00"), (yes_time.ToString("yyyy-MM-dd") + " 23:59:59"));
                                            // string strwhere = string.Format(" and applytype in ('travel','gomeet','forget') and '{0}' between convert(varchar(10),starttime,120) and convert(varchar(10),endtime,120) and adduser={1} and starttime>='{2}' and endtime<='{3}'", yes_time.ToString("yyyy-MM-dd"), memid, (yes_time.ToString("yyyy-MM-dd") + " 21:50:00"), (yes_time.ToString("yyyy-MM-dd") + " 23:59:59"));
                                            travel_yestoday = new TG.BLL.cm_ApplyInfo().GetApplyList(strwhere).Tables[0];
                                            if (travel_yestoday != null && travel_yestoday.Rows.Count > 0)
                                            {
                                                //查询结束时间是否大于23点50分钟
                                                DataTable temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 23:50:00'") }.ToTable();
                                                if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                {
                                                    str = "bt";
                                                }
                                                else
                                                {
                                                    //查询结束时间是否大于22点00分钟
                                                    temp_yestoday = new DataView(travel_yestoday) { RowFilter = "endtime>='" + (yes_time.ToString("yyyy-MM-dd") + " 21:50:00'") }.ToTable();
                                                    if (temp_yestoday != null && temp_yestoday.Rows.Count > 0)
                                                    {
                                                        str = "bxs";
                                                    }
                                                }
                                            }
                                        }

                                        //昨天加班到22：00，所以允许今天迟到半小时后,统计从9:30
                                        if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 21:50:00") && Convert.ToDateTime(yestoday) < Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bxs")
                                        {
                                            houtai = houtai.AddMinutes(30);
                                        }
                                        //昨天加班到23:50，所以允许迟到半天,统计从13：0:0
                                        else if ((!string.IsNullOrEmpty(yestoday) && Convert.ToDateTime(yestoday) >= Convert.ToDateTime(yes_time.ToString("yyyy-MM-dd") + " 23:50:00")) || str == "bt")
                                        {
                                            houtai = zwsbtime;
                                        }

                                        TimeSpan ts = shiji - houtai;
                                        //大于等于4分钟才算迟到
                                        if (ts.TotalMinutes > 3)
                                        {

                                            if (ts.TotalMinutes >= 4 && ts.TotalMinutes <= 14)//迟到
                                            {

                                                time_late++;


                                            }
                                            else if (ts.TotalMinutes > 14 && ts.TotalMinutes < 30 && !flag) //迟到大于10分钟按0.5事假统计
                                            {
                                                time_leave = time_leave + decimal.Parse("0.5");
                                            }
                                            else
                                            {
                                                //没有申请记录
                                                if (!flag)
                                                {
                                                    if (shiji > houtaioff)
                                                    {
                                                        //下班时间-上班时间减去一小时
                                                        TimeSpan ts2 = houtaioff - houtai;
                                                        time_leave = time_leave + GetHours(ts2);
                                                        //上班时间不是下午1点。
                                                        if (houtai < zwsbtime)
                                                        {
                                                            time_leave = time_leave - 1;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //上班未上班, 正常上班时间9点、9点半
                                                        if (shiji >= zwxbtime && zwxbtime > houtai)
                                                        {
                                                            TimeSpan ts2 = zwxbtime - houtai;
                                                            time_leave = time_leave + GetHours(ts2);
                                                        }
                                                        else
                                                        {
                                                            //上午上班，打卡迟到大于30分钟
                                                            time_leave = time_leave + GetHours(ts);

                                                        }

                                                        //实际打卡时间大于下午上班时间, 正常上班时间9点、9点半
                                                        if (shiji > zwsbtime && zwxbtime > houtai)
                                                        {
                                                            TimeSpan ts3 = shiji - zwsbtime;
                                                            time_leave = time_leave + GetHours(ts3);
                                                        }
                                                    }
                                                }

                                            }


                                            //超过5次后，按照0.5小时事假扣除
                                            if (time_late > 0 && (sum_late + 1) > 5)
                                            {
                                                time_leave = time_leave + decimal.Parse("0.5");
                                            }
                                        }
                                    }

                                    //早退
                                    if (dt_late.Rows.Count > 0 && shijioff < houtaioff && !flag)
                                    {
                                        DateTime zaotui = shijioff;
                                        //只有一条早上打卡记录,按上班时间计算
                                        if (shijioff < houtai)
                                        {
                                            zaotui = houtai;
                                        }
                                        //下班打卡时间是上午,中午下班时间-早退时间
                                        if (shijioff < zwxbtime)
                                        {
                                            if (dt_late.Rows.Count == 1)
                                            {
                                                zaotui = houtai;
                                                TimeSpan ts_sw = zwxbtime - zaotui;
                                                time_leave = GetHours(ts_sw);
                                                //超过5次后，按照0.5小时事假扣除
                                                if (time_late > 0 && (sum_late + 1) > 5)
                                                {
                                                    time_leave = time_leave + decimal.Parse("0.5");
                                                }
                                            }
                                            else
                                            {
                                                TimeSpan ts_sw = zwxbtime - zaotui;
                                                time_leave = time_leave + GetHours(ts_sw);
                                            }
                                        }

                                        //下班打卡时间是下午1点之前,就默认是下午1点
                                        if (shijioff < zwsbtime)
                                        {
                                            zaotui = zwsbtime;
                                        }

                                        TimeSpan ts2 = houtaioff - zaotui;
                                        decimal ztqj = GetHours(ts2);

                                        time_leave = time_leave + ztqj;
                                    }

                                    //下班时间+30分钟，算加班
                                    DateTime jiaban = houtaioff.AddMinutes(30);
                                    if (shiji > jiaban && dt_late.Rows.Count > 1)
                                    {
                                        int mine = shiji.Minute;
                                        if (mine > 0 && mine < 15)
                                        {
                                            jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                        }
                                        else if (mine > 30 && mine < 45)
                                        {
                                            jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                        }
                                        else
                                        {
                                            jiaban = shiji;
                                        }

                                    }
                                    //加班统计                       
                                    if (shijioff > jiaban)
                                    {
                                        decimal totaltime_forget = 0;
                                        //判断是否有未打卡申请记录
                                        tomr_endtime = starttime;
                                        if (dt != null && dt.Rows.Count > 0)
                                        {
                                            DataTable dt_over = new DataView(dt) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                            if (dt_over != null && dt_over.Rows.Count > 0)
                                            {
                                                totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                            }
                                        }
                                        //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                        if (totaltime_forget == 0)
                                        {
                                            time_over = time_over + GetOver(jiaban, shijioff);
                                        }
                                        //time_over = time_over + GetOver(jiaban, shijioff);
                                    }



                                    #endregion
                                }
                                else
                                {
                                    //周六日，节假日加班

                                    DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                    DateTime shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_late.Rows[(dt_late.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间
                                    DateTime houtai = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                    DateTime houtaioff = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间

                                    //实际打卡时间
                                    int mine = shiji.Minute;
                                    DateTime jiaban = shiji;
                                    if (mine > 0 && mine < 15)
                                    {
                                        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                    }
                                    else if (mine > 30 && mine < 45)
                                    {
                                        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                    }

                                    decimal totaltime_forget = 0;
                                    //判断是否有未打卡申请记录
                                    tomr_endtime = starttime;
                                    if (dt != null && dt.Rows.Count > 0)
                                    {
                                        DataTable dt_over = new DataView(dt) { RowFilter = "applytype='forget' and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00' " }.ToTable();
                                        if (dt_over != null && dt_over.Rows.Count > 0)
                                        {
                                            totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                        }
                                    }
                                    //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                    if (totaltime_forget == 0)
                                    {
                                        time_over = time_over + GetOver(jiaban, shijioff);
                                    }

                                    // time_over = time_over + GetOver(jiaban, shijioff);
                                    #region

                                    //全天
                                    //if (shiji <= houtai && shijioff >= houtaioff)
                                    //{
                                    //    time_over = time_over + decimal.Parse("7.5");
                                    //}
                                    //else
                                    //{
                                    //    DateTime start = shiji;
                                    //    DateTime end = shijioff;
                                    //    //加班统计   
                                    //    if (shiji < houtai)
                                    //    {
                                    //        start = houtai;
                                    //    }
                                    //    else if (shiji >= zwxbtime && shiji <= zwsbtime)
                                    //    {
                                    //        shiji = zwsbtime;
                                    //    }
                                    //    if (shijioff > houtaioff)
                                    //    {
                                    //        end = houtaioff;
                                    //    }
                                    //    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
                                    //    {
                                    //        end = zwxbtime;
                                    //    }

                                    //    //都是上午打卡    
                                    //    if (end <= zwxbtime)
                                    //    {
                                    //        end = shijioff;
                                    //        time_over = time_over + GetOver(start, end);
                                    //    }
                                    //    else //下班打卡是下午
                                    //    {
                                    //        //上班打卡也是下午
                                    //        if (start >= zwsbtime)
                                    //        {
                                    //            time_over = time_over + GetOver(start, end);
                                    //        }
                                    //        else//上班打卡是上午
                                    //        {
                                    //            //上午加班时间
                                    //            time_over = time_over + GetOver(start, zwxbtime);
                                    //            //下午加班时间
                                    //            time_over = time_over + GetOver(zwsbtime, end);
                                    //        }
                                    //    }
                                    //}
                                    ////晚上加班
                                    ////第一次打卡时间已超过晚上6点，无需加半个小时
                                    //DateTime jiaban = houtaioff.AddMinutes(30);
                                    //if (shiji > jiaban)
                                    //{
                                    //    int mine = shiji.Minute;
                                    //    if (mine > 0 && mine < 15)
                                    //    {
                                    //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                    //    }
                                    //    else if (mine > 30 && mine < 45)
                                    //    {
                                    //        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                    //    }
                                    //    else
                                    //    {
                                    //        jiaban = shiji;
                                    //    }

                                    //}

                                    ////加班统计                       
                                    //if (shijioff > jiaban)
                                    //{
                                    //    time_over = time_over + GetOver(jiaban, shijioff);
                                    //}

                                    #endregion
                                }
                            }
                            else
                            {

                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    if (colorStr == "")
                                    {
                                        #region 请假
                                        DataTable dt_leave = new DataView(dt) { RowFilter = "applytype='leave' and (reason='事假' or reason='年假')" }.ToTable();
                                        time_leave = time_leave + GetLeave(dt_leave, starttime);
                                        //每年部门活动带薪时间1天（7.5小时），超过7.5小时按事假计算
                                        decimal unittime = 0;
                                        DataTable dt_temp = new DataView(dt) { RowFilter = "applytype='depart' " }.ToTable();
                                        if (dt_temp != null && dt_temp.Rows.Count > 0)
                                        {
                                            //当天的部门活动小时
                                            unittime = GetLeaveDepart(dt_temp, starttime, ref sumunittime);
                                        }

                                        time_leave = time_leave + unittime;
                                        #endregion
                                        #region 病假
                                        DataTable dt_sick = new DataView(dt) { RowFilter = "applytype='leave' and reason='病假' " }.ToTable();
                                        time_sick = time_sick + GetLeave(dt_sick, starttime);
                                        #endregion

                                        #region 婚假
                                        DataTable dt_marry = new DataView(dt) { RowFilter = "applytype='leave' and reason='婚假' " }.ToTable();
                                        time_marry = time_marry + GetLeave(dt_marry, starttime);
                                        #endregion
                                        #region 产/陪产假
                                        DataTable dt_mater = new DataView(dt) { RowFilter = "applytype='leave' and reason='产/陪产假' " }.ToTable();
                                        time_mater = time_mater + GetLeave(dt_mater, starttime);
                                        #endregion
                                        #region 丧假
                                        DataTable dt_die = new DataView(dt) { RowFilter = "applytype='leave' and reason='丧假' " }.ToTable();
                                        time_die = time_die + GetLeave(dt_die, starttime);
                                        #endregion
                                    }
                                    #region 加班
                                    tomr_endtime = starttime;
                                    //只读取当天6点到第二天6点数据
                                    DataTable dt_over = new DataView(dt) { RowFilter = "applytype in ('travel','gomeet','forget') and '" + starttime.ToString("yyyy-MM-dd") + " 06:00:00'<=starttime and endtime<='" + tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00'" }.ToTable();
                                    if (dt_over != null && dt_over.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dt_over.Rows)
                                        {
                                            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                            time_over = time_over + totaltime_over;
                                        }
                                    }
                                    #endregion

                                }
                                else
                                {
                                    if (colorStr == "")
                                    {
                                        //一天未打卡，无申请
                                        time_leave = time_leave + decimal.Parse("7.5");
                                    }

                                }

                            }
                            //加班离岗
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                DataTable dt_addwork = new DataView(dt) { RowFilter = "applytype='addwork' " }.ToTable();
                                if (dt_addwork != null && dt_addwork.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt_addwork.Rows)
                                    {
                                        decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                        time_over = time_over - totaltime_over;
                                    }
                                }

                            }
                        }
                        string colorStr_leave = colorStr;
                        string colorStr_sick = colorStr;
                        string colorStr_overtime = colorStr;
                        string colorStr_marry = colorStr;
                        string colorStr_mater = colorStr;
                        string colorStr_die = colorStr;
                        string colorStr_late = colorStr;
                        //判断是否手动改过
                        if (list_data2 != null && list_data2.Count > 0)
                        {
                            var data_model = list_data2.Where(d => d.dataType == "late").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_late = data_model.dataValue;
                                //超过5次后，按照0.5小时事假扣除
                                if (time_late > 0 && (sum_late + 1) > 5)
                                {
                                    time_leave = time_leave + decimal.Parse("0.5");
                                }
                                colorStr_late = " style='background-color:yellow;'";
                            }
                             data_model = list_data2.Where(d => d.dataType == "leave").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_leave = data_model.dataValue;
                                colorStr_leave = " style='background-color:yellow;'";
                            }

                            data_model = list_data2.Where(d => d.dataType == "sick").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_sick = data_model.dataValue;
                                colorStr_sick = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_over = data_model.dataValue;
                                colorStr_overtime = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "marry").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_marry = data_model.dataValue;
                                colorStr_marry = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "mater").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_mater = data_model.dataValue;
                                colorStr_mater = " style='background-color:yellow;'";
                            }
                            data_model = list_data2.Where(d => d.dataType == "die").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_die = data_model.dataValue;
                                colorStr_die = " style='background-color:yellow;'";
                            }
                            

                        }

                        //日期执行一次
                        if (headflag)
                        {
                            //第一行第中间列
                            cell = dataRow.GetCell(i);
                            if (cell == null)
                                cell = dataRow.CreateCell(i);
                            cell.CellStyle = style1;
                            cell.SetCellValue(starttime.Day);
                        }
                        //第二行第中间列
                        cell = dataRow2.GetCell(i);
                        if (cell == null)
                            cell = dataRow2.CreateCell(i);
                        cell.CellStyle = style3;
                        cell.SetCellValue("");
                        //第三行第中间列
                        cell = dataRow3.CreateCell(i);
                        cell.CellStyle = style2;

                        if (colorStr_leave.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                        }
                        else if (colorStr_leave.Contains("green"))
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue((Math.Round(time_leave, 1) == 0 ? "" : time_leave.ToString("f1")));
                        //第四行第中间列
                        cell = dataRow4.CreateCell(i);
                        cell.CellStyle = style2;
                        if (colorStr_sick.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                        }
                        else if (colorStr_sick.Contains("green"))
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue((Math.Round(time_sick, 1) == 0 ? "" : time_sick.ToString("f1")));
                        //第五行第中间列
                        cell = dataRow5.GetCell(i);
                        cell = dataRow5.CreateCell(i);
                        cell.CellStyle = style2;
                        if (colorStr_overtime.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                        }
                        else if (colorStr_overtime.Contains("green"))
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue((Math.Round(time_over, 1) == 0 ? "" : time_over.ToString("f1")));
                        //第六行第中间列
                        cell = dataRow6.CreateCell(i);
                        cell.CellStyle = style2;
                        if (colorStr_marry.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                        }
                        else if (colorStr_marry.Contains("green"))
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue((Math.Round(time_marry, 1) == 0 ? "" : time_marry.ToString("f1")));
                        //第七行第中间列
                        cell = dataRow7.CreateCell(i);
                        cell.CellStyle = style2;
                        if (colorStr_mater.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                        }
                        else if (colorStr_mater.Contains("green"))
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue((Math.Round(time_mater, 1) == 0 ? "" : time_mater.ToString("f1")));
                        //第八行第中间列
                        cell = dataRow8.CreateCell(i);
                        cell.CellStyle = style2;
                        if (colorStr_die.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                        }
                        else if (colorStr_die.Contains("green"))
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue((Math.Round(time_die, 1) == 0 ? "" : time_die.ToString("f1")));
                        //第九行第中间列
                        cell = dataRow9.CreateCell(i);
                        cell.CellStyle = style2;
                        if (colorStr_late.Contains("yellow"))
                        {
                            //黄色
                            cell.CellStyle = style4;
                        }
                        else if (colorStr_late.Contains("green"))
                        {
                            //绿色                           
                            cell.CellStyle = style5;
                        }
                        cell.SetCellValue((time_late == 0 ? "" : time_late.ToString("f0")));
                        //合计
                        sum_leave = sum_leave + time_leave;
                        sum_sick = sum_sick + time_sick;
                        sum_over = sum_over + time_over;
                        sum_marry = sum_marry + time_marry;
                        sum_mater = sum_mater + time_mater;
                        sum_die = sum_die + time_die;
                        sum_late = sum_late + time_late;
                        i++;
                    }

                    if (headflag)
                    {
                        cell = dataRow.GetCell((i));
                        if (cell == null)
                            cell = dataRow.CreateCell((i));
                        cell.CellStyle = style1;
                        cell.SetCellValue("");
                    }

                    //第二行第一列
                    cell = dataRow2.GetCell((i));
                    if (cell == null)
                        cell = dataRow2.CreateCell((i));
                    cell.CellStyle = style3;
                    // cell.CellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
                    // cell.CellStyle.FillForegroundColor = HSSFColor.PINK.index;
                    cell.SetCellValue("合计");
                    //第三行第一列
                    cell = dataRow3.CreateCell((i));
                    cell.CellStyle = style1;
                    cell.CellStyle.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(sum_leave.ToString("f1"));
                    //第四行第一列
                    cell = dataRow4.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(sum_sick.ToString("f1"));
                    //第五行第一列
                    cell = dataRow5.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(sum_over.ToString("f1"));
                    //第六行第一列
                    cell = dataRow6.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(sum_marry.ToString("f1"));
                    //第七行第一列
                    cell = dataRow7.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(sum_mater.ToString("f1"));
                    //第八行第一列
                    cell = dataRow8.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(sum_die.ToString("f1"));
                    //第九行第一列
                    cell = dataRow9.CreateCell((i));
                    cell.CellStyle = style1;
                    style2.Alignment = HorizontalAlignment.CENTER;
                    cell.SetCellValue(sum_late.ToString("f0"));

                    headflag = false;
                }
                index = index + 8;
            }

            //写入Session
            Session["SamplePictures"] = true;
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }


    }

}