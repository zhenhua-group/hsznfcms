﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Configuration;

namespace TG.Web.Calendar
{
    public partial class YearAnalysis : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();

                BindData();
            }

        }
        //根据条件查询
        private void BindData()
        {

            StringBuilder sb = new StringBuilder(" 1=1 ");

            //部门
            if (this.drp_unit.SelectedIndex > 0)
            {
                sb.Append(" AND t.Unit_ID=" + this.drp_unit.SelectedValue + "");
            }

            TG.BLL.tg_member bllMem = new TG.BLL.tg_member();
            //检查权限
            GetPreviewPowerSql(ref sb);

            sb.Append(" and t.unit_ParentID<>0 and t.unit_ID<>230  and t.unit_ID not in (" + NotShowUnitList + ") order by te.unit_Order asc,t.unit_ID asc ");

            string sql = "select t.* from tg_unit t join tg_unitExt te on  te.unit_ID=t.unit_ID where " + sb.ToString();
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];

            //生成table
            CreateTable(dt);
        }

        public void CreateTable(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            string drpyear = this.drp_year.SelectedValue;
            int nextyear = Convert.ToInt32(drpyear);
            int drpmonth = 2, nextmonth = 7;
            if (this.drp_month.SelectedValue == "1")
            {
                nextyear = (Convert.ToInt32(drpyear) + 1);
                drpmonth = 8;
                nextmonth = 1;
            }

            DateTime starttime = Convert.ToDateTime(drpyear + "-" + (drpmonth - 1) + "-16");
            DateTime endtime = Convert.ToDateTime(nextyear + "-" + nextmonth + "-15");

            string aa = ConfigurationManager.AppSettings["NoWork"];
            string nowork = "'" + (aa.Replace(",", "','")) + "'";
            if (dt != null && dt.Rows.Count > 0)
            {
                //得到所有节假日
                List<TG.Model.cm_HolidayConfig> holi_list = new TG.BLL.cm_HolidayConfig().GetModelList("");
                //后台设置上班时间
                List<TG.Model.cm_PersonAttendSet> pas_list = new TG.BLL.cm_PersonAttendSet().GetModelList("");
                //申请加班记录
                DataTable dt_time = new TG.BLL.cm_ApplyInfo().GetApplyList(" and applytype in ('travel','gomeet','addwork','forget') ").Tables[0];
                int row = 1;
                foreach (DataRow item in dt.Rows)
                {
                    int rs = 0, pjgs = 0, cyjsr = 0;
                    //平均值，中位值，最小值，最大值，标准差,标准差系数,部门经理系数
                    decimal avgtime = 0, middletime = 0, mintime = 0, maxtime = 0, standardtime = 0, standardxs = 0, managerxs = 0;
                    decimal weekovertime = 0, sumbztime = 0, managertime = 0;//标准差总值

                    //把钟志宏添加到结构部 
                    string where = "";
                    if (item["unit_ID"].ToString() == "237")
                    {
                        where = " m.mem_ID=1445 or ";
                    }
                    //循环人员
                    DataTable list = TG.DBUtility.DbHelperSQL.Query("select m.*,mr.RoleIdMag from tg_member m left join tg_memberRole mr on m.mem_ID=mr.mem_Id where " + where + " (mr.mem_Unit_ID=" + item["unit_ID"] + " and mem_Name not in (" + nowork + ") and (mem_isFired=0 or m.mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + starttime.ToString("yyyy-MM-dd") + "'))) order by mem_Order asc,m.mem_ID asc").Tables[0];
                    if (list != null && list.Rows.Count > 0)
                    {
                        rs = list.Rows.Count;
                        decimal[] sz = new decimal[rs];
                        //排除无打卡记录人
                        int wdkjl = 0;
                        for (int i = 0; i < list.Rows.Count; i++)
                        {

                            //6个月的加班总和
                            decimal overtime = 0;
                            overtime = GetOverTimeYear(holi_list, pas_list, dt_time, Convert.ToInt32(list.Rows[i]["mem_ID"]), list.Rows[i]["mem_Name"].ToString(), starttime, endtime, ref wdkjl);
                            //周加班
                            overtime = Math.Round(overtime / (decimal.Parse("4.28") * 6), 1);


                            weekovertime += overtime;

                            //部门经理
                            if (list.Rows[i]["RoleIdMag"].ToString() == "5" || list.Rows[i]["RoleIdMag"].ToString() == "44")
                            {
                                managertime = managertime + overtime;
                            }
                            sz[i] = overtime;
                        }
                        //参与计算人数，排除无打卡记录人
                        cyjsr = rs - wdkjl;
                        if (cyjsr > 0)
                        {

                            //平均值
                            avgtime = (cyjsr == 0 ? 0 : (weekovertime / cyjsr));
                        }
                        //按参与计算人数
                        //向上取整数
                        double dd = Math.Ceiling((double)cyjsr / (double)2);
                        int bb = Convert.ToInt32(dd);
                        pjgs = cyjsr % 2;

                        //从大到小排序
                        sz = sort(sz);

                        //排除0个数
                        //  int gs = sz.Count();
                        decimal gspjz = Math.Round(avgtime, 1); ;
                        // if (gs > 0)
                        //    {
                        //       gspjz = Math.Round((weekovertime / gs), 1);
                        //    }
                        for (int i = 0; i < cyjsr; i++)
                        {
                            //取得中间值
                            //奇数 中间位置的数据
                            if (pjgs > 0 && (i + 1) == bb)
                            {
                                middletime = sz[i];
                            }
                            else //偶数
                            {
                                //中间位置的两个数值的算术平均数
                                if ((i + 1) == bb)
                                {
                                    middletime = sz[i];
                                    middletime = (middletime + sz[(i + 1)]) / 2;
                                }

                            }

                            //不包含周加班为0的人员
                            //  if (sz[i] > 0)
                            //  {
                            sumbztime = sumbztime + (sz[i] - gspjz) * (sz[i] - gspjz);
                            //   }
                        }
                        //标准差
                        //标准差
                        if (cyjsr > 1)
                        {
                            sumbztime = sumbztime / (cyjsr - 1);
                            standardtime = Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(sumbztime)));
                        }


                        //部门经理
                        if (Math.Round(avgtime, 1) > 0)
                        {
                            //经理系数
                            managerxs = Math.Round(managertime, 1) / Math.Round(avgtime, 1) * 100;
                            //标准差系数
                            standardxs = (Math.Round(standardtime, 1) / Math.Round(avgtime, 1) * 100);
                        }
                        //if (Math.Round(gspjz,1)>0)
                        //{
                        //    //标准差系数
                        //    standardxs = (Math.Round(standardtime, 1) / Math.Round(gspjz, 1) * 100);
                        //}

                        //最大值
                        maxtime = sz[0];
                        //最小值
                        if (cyjsr > 0)
                        {
                            mintime = sz[(cyjsr - 1)];
                        }

                    }

                    sb.Append("<Td>");
                    sb.Append("<table class=\"table table-bordered table-responsive table-hover\" style=\"width:300px; margin: 0 auto; margin-bottom: 0px;\">");
                    sb.Append("<tr><td colspan=\"2\" style=\"background-color:#ffffcc;text-align: center;\">" + item["unit_Name"] + "</td></tr>");
                    sb.Append("<tr style=\"background-color:#ffffcc; text-align:center;\"> <td style=\"width:150px;\">项目</td><td style=\"width:100px;\">统计量</td></tr>");
                    sb.Append("<Tr><Td>参与计算人数</Td><Td style=\"text-align:right;\">" + cyjsr + "</Td></Tr>");
                    sb.Append("<Tr><Td><b>平均数</b></Td><Td style=\"text-align:right;\"><b>" + Math.Round(avgtime, 1) + "</b></Td></Tr>");
                    sb.Append("<Tr><Td>中位数</Td><Td style=\"text-align:right;\">" + middletime.ToString("f1") + "</Td></Tr>");
                    sb.Append("<Tr><Td>最大值</Td><Td style=\"text-align:right;\">" + maxtime.ToString("f1") + "</Td></Tr>");
                    sb.Append("<Tr><Td>最小值</Td><Td style=\"text-align:right;\">" + mintime.ToString("f1") + "</Td></Tr>");
                    sb.Append("<Tr><Td>极差</Td><Td style=\"text-align:right;\">" + (maxtime - mintime).ToString("f1") + "</Td></Tr>");
                    sb.Append("<Tr><Td>标准差</Td><Td style=\"text-align:right;\">" + standardtime.ToString("f1") + "</Td></Tr>");
                    sb.Append("<Tr><Td><b>标准差系数</b></Td><Td style=\"text-align:right;\"><b>" + standardxs.ToString("f1") + "%</b></Td></Tr>");
                    sb.Append("<Tr><Td><b>部门经理系数</b></Td><Td style=\"text-align:right;\"><b>" + managerxs.ToString("f1") + "%</b></Td></Tr>");
                    sb.Append("</table>");
                    sb.Append("</Td>");
                    if (row % 2 == 0)
                    {
                        sb.Append("</tr><tr>");
                    }
                    row++;
                }

            }
            else
            {
                sb.Append("<tr><td colspan='19' style='color:red;'>没有数据！</td></tr>");
            }


            lithtml.Text = sb.ToString();
        }

        #region PageBase 已存在
        ////返回加班时间
        //public decimal GetOver(DateTime start, DateTime end)
        //{
        //    decimal over = 0;
        //    if (end > start)
        //    {
        //        TimeSpan ts_jb = end - start;
        //        //强制转换，没有四舍五入
        //        int hours = (int)(ts_jb.TotalHours);

        //        double minutes = ts_jb.TotalMinutes - (hours * 60);
        //        //大于15分钟，即加班
        //        if (minutes > 15 && minutes <= 45)
        //        {
        //            over = decimal.Parse("0.5");
        //        }
        //        else if (minutes > 45 && minutes < 60)
        //        {
        //            over = decimal.Parse("1");
        //        }

        //        over = over + hours;
        //    }
        //    return over;
        //}
        ////判断是否是假期
        //public bool isJQ(DateTime date)
        //{
        //    bool flag = false;
        //    List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" convert(varchar(10),holiday,120)='" + date.ToString("yyyy-MM-dd") + "' order by id");
        //    //存在
        //    if (list != null && list.Count > 0)
        //    {
        //        //节假日，需减1天
        //        if (list[0].daytype == 1)
        //        {
        //            flag = true;
        //        }
        //    }
        //    else
        //    {
        //        //周六日0~6
        //        string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
        //        if (temp == "Sunday" || temp == "Saturday")
        //        {
        //            flag = true;
        //        }
        //    }
        //    return flag;
        //}
        ////从小到大排序
        //public decimal[] sort(decimal[] a)
        //{
        //    //排序  
        //    decimal t = 0;
        //    for (int i = 0; i < a.Length; i++)
        //    {
        //        for (int j = i + 1; j < a.Length; j++)
        //        {
        //            if (a[i] > a[j])
        //            {
        //                t = a[j];
        //                a[j] = a[i];
        //                a[i] = t;
        //            }
        //        }
        //    }
        //    return a;

        //}
        #endregion
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {

            BindData();
        }

        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();

            string sqlwhere = "";
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                sqlwhere = " unit_ParentID<>0  and unit_ID not in (" + NotShowUnitList + ") ";
            }
            else
            {
                sqlwhere = " unit_ID=" + UserUnitNo;
            }
            sqlwhere = sqlwhere + " and unit_ID<>230 order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID) asc,unit_ID asc ";
            var unitList = bll_unit.GetModelList(sqlwhere);
            var unitExtList = bll_unitExt.GetModelList("");
            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;

            this.drp_unit.DataSource = query.ToList<TG.Model.tg_unit>();
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //绑定年份和月
        protected void BindYear()
        {
            int oldyear = 2017;
            //初始化年
            int curryear = DateTime.Now.Year;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drp_year.Items.Add(i.ToString());
            }


        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string curmonth = DateTime.Now.Month.ToString();
            if (this.drp_month.Items.FindByText(curmonth) != null)
            {
                this.drp_month.Items.FindByText(curmonth).Selected = true;
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人，只有管理员看到全部
            if (base.RolePowerParameterEntity.PreviewPattern != 1)
            {
                sb.Append(" AND t.unit_ID =" + UserUnitNo + " ");
            }
            //部门
            //else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            //{
            //    sb.Append(" AND mem_ID in (Select mem_ID From tg_member Where mem_unit_ID=" + UserUnitNo + ")");
            //}
        }


    }

}