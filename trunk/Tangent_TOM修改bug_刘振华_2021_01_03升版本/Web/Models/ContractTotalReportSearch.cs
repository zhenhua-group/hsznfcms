﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.Models
{
    public class ContractTotalReportSearch
    {
        public Dictionary<string,string> depts { get; set; }
        public int year { get; set; }
        
        public int quarter { get; set; }

        public int month { get; set; }

    }

    
}