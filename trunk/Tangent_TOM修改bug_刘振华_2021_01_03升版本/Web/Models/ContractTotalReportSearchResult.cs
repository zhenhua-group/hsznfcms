﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TG.Model;

namespace TG.Web.Models
{
    public class ContractTotalReportSearchResult
    {
        public List<CoperationAndProjectCountExcel> ContractTotalData { get; set; }

        public string StrTip { get; set; }

        public List<string> DeptNames { get; set; }

        public List<decimal> CustCount { get; set; }
        public List<decimal> CprCount { get; set; }
        public List<decimal> ProjCount { get; set; }
        public List<decimal> PlanCount { get; set; }
        public List<decimal> NoPlanCount { get; set; }

    }
}