﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TG.Model;

namespace TG.Web
{
    public class CommonAudit
    {
        public static List<int> GetUserSysNoList(string departmentName)
        {
            //查询该合同的承接部门负责人SysNo
            string departmentChargeManSql = string.Format("select isnull(u.mem_ID,0) as UserSysNo from tg_unit de join tg_member u on u.mem_Unit_ID = de.unit_ID where de.unit_Name = N'{0}' ", departmentName == null ? " " : departmentName.Trim());

            SqlDataReader reader = TG.DBUtility.DbHelperSQL.ExecuteReader(departmentChargeManSql);

            List<int> resultList = new List<int>();

            while (reader.Read())
            {
                resultList.Add(Convert.ToInt32(reader["UserSysNo"]));
            }

            reader.Close();

            return resultList;
        }

        public static List<int> GetUserSysNoFromRole(int roleSysNo)
        {
            string sql = "SELECT isnull([Users],N'') FROM [dbo].[cm_Role] where SysNo =" + roleSysNo;

            object objResult = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            string userString = objResult == null ? string.Empty : objResult.ToString();

            List<int> resultList = new List<int>();

            if (!string.IsNullOrEmpty(userString))
            {
                string[] userSysNoArray = userString.Split(',');

                foreach (string userSysNo in userSysNoArray)
                {
                    if (userSysNo != ",")
                    {
                        resultList.Add(string.IsNullOrEmpty(userSysNo) == true ? 0 : int.Parse(userSysNo));
                    }
                }
            }
            return resultList;
        }

        public static string GetRoleName(int roleSysNo, SysMessageViewEntity sysMessageViewEntity)
        {
            string sql = string.Format("select RoleName,Users from cm_Role where SysNo=" + roleSysNo);

            SqlDataReader reader = TG.DBUtility.DbHelperSQL.ExecuteReader(sql);

            string roleName = string.Empty;
            string userName = string.Empty;
            while (reader.Read())
            {
                roleName = reader["RoleName"].ToString();
                userName = reader["Users"].ToString();
            }

            string[] userSysNoArray = userName.Split(',');

            TG.BLL.tg_member bp = new BLL.tg_member();

            ArrayList arrayList = new ArrayList();

            if (userSysNoArray != null && userSysNoArray.Length > 0)
            {
                for (int i = 0; i < userSysNoArray.Length; i++)
                {
                    if (userSysNoArray[i] != "" && userSysNoArray[i] != ",")
                    {
                        TG.Model.tg_member user = bp.GetModel(int.Parse(userSysNoArray[i]));
                        if (user != null)
                            arrayList.Add(new { UserSysNo = user.mem_ID, UserName = user.mem_Name });
                    }
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = roleName, UserList = arrayList, MessageTemplate = sysMessageViewEntity });
        }
        //获取用户列表
        public static string GetRoleName(int roleSysNo)
        {
            string sql = string.Format("select RoleName,Users from cm_Role where SysNo=" + roleSysNo);

            SqlDataReader reader = TG.DBUtility.DbHelperSQL.ExecuteReader(sql);

            string roleName = string.Empty;
            string userName = string.Empty;
            while (reader.Read())
            {
                roleName = reader["RoleName"].ToString();
                userName = reader["Users"].ToString();
            }

            string[] userSysNoArray = userName.Split(',');

            TG.BLL.tg_member bp = new BLL.tg_member();

            ArrayList arrayList = new ArrayList();

            if (userSysNoArray != null && userSysNoArray.Length > 0)
            {
                for (int i = 0; i < userSysNoArray.Length; i++)
                {
                    if (userSysNoArray[i] != "" && userSysNoArray[i] != ",")
                    {
                        TG.Model.tg_member user = bp.GetModel(int.Parse(userSysNoArray[i]));
                        if (user != null)
                            arrayList.Add(new { UserSysNo = user.mem_ID, UserName = user.mem_Name });
                    }
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = roleName, UserList = arrayList });
        }
        //获取消息json对象
        public static string GetMessagEntity(SysMessageViewEntity sysMessageViewEntity)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(sysMessageViewEntity);
        }
    }
}