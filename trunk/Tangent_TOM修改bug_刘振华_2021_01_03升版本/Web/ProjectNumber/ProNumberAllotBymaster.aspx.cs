﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using System.Data;
using TG.Common;

namespace TG.Web.ProjectNumber
{
    public partial class ProNumberAllotBymaster : PageBase
    {
        //项目信息
        public int ProjectID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["pro_id"], out proid);
                return proid;
            }
        }
        //工程号ID
        public int ProNumberID
        {
            get
            {
                int pronumid = 0;
                int.TryParse(Request["pronum_id"], out pronumid);
                return pronumid;
            }
        }
        //FromUserID
        public int FromUserID
        {
            get
            {
                int userid = 0;
                int.TryParse(Request["FromUserID"], out userid);
                return userid;
            }
        }
        //ToRole
        public string ToRole
        {
            get
            {
                return Request["ToRole"] ?? "";
            }
        }
        //消息ID
        public int MessageID
        {
            get
            {
                int msgSysNo = 0;
                int.TryParse(Request["MsgNo"], out msgSysNo);
                return msgSysNo;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hid_projid.Value = ProjectID.ToString();
            if (!IsPostBack)
            {
                GetProInfo();
            }
            else
            {
                SaveProNum();
            }
        }

        private void SaveProNum()
        {
            //分配工号
            TG.BLL.cm_projectNumber bll_num = new TG.BLL.cm_projectNumber();
            TG.Model.cm_projectNumber model_num = new TG.Model.cm_projectNumber();
            model_num.ProNumber = proNumber.Value.Trim();
            model_num.proNum_id = ProNumberID;
            //插入tg_project数据
            TG.Model.cm_Project pr_modol = new TG.BLL.cm_Project().GetModel(ProjectID);
            //更新cm_Project 项目工号
            string updateCmProjectSql = string.Format("Update cm_Project Set pro_number=N'{0}' Where pro_ID={1}", model_num.ProNumber, pr_modol.bs_project_Id);
            int AffectRow = TG.DBUtility.DbHelperSQL.ExecuteSql(updateCmProjectSql);
            //更新tg_project表,项目工号
            string updateTGProjectSql = string.Format("Update tg_project set pro_Order =N'{0}' where pro_ID ={1}", model_num.ProNumber, pr_modol.ReferenceSysNo);
            int count = TG.DBUtility.DbHelperSQL.ExecuteSql(updateTGProjectSql);

            if (count > 0)
            {
                if (!IsExsitNumber())
                {
                    //发送消息
                    SendRequest(pr_modol.pro_name.Trim());
                    //插入cm_projectNumber数据
                    bll_num.Update(model_num);
                    //待办事项状态更新
                    int msgcount = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(MessageID);

                    //update by 20130608
                    //发送消息给项目总负责进行项目策划
                    //项目总负责人ID
                    int pmUserID = pr_modol.PMUserID;
                    SysMessageViewEntity sysMessageDataEntity2 = new SysMessageViewEntity
                    {
                        InUser = UserSysNo,
                        FromUser = pmUserID,
                        MsgType = 8,//发送消息给项目总负责进行策划
                        ReferenceSysNo = pr_modol.bs_project_Id.ToString(),
                        ToRole = "0",
                        Status = "A",
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pr_modol.pro_name, "项目立项通过，工号已分配，项目总负责可以进行项目策划！"),
                        QueryCondition = pr_modol.pro_name,
                        ExtendField = "",
                        IsDone = "A"
                    };
                    new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageDataEntity2);
                    //更新待办状态
                    UpdateWorkDoneStatus();

                    MessageBox.ShowAndRedirect(this, "工号分配成功!", "ProNumberBymaster.aspx");
                }
                else
                {
                    //插入cm_projectNumber数据
                    bll_num.Update(model_num);
                    //更新待办状态
                    UpdateWorkDoneStatus();

                    MessageBox.ShowAndRedirect(this, "工号更新成功!", "ProNumberBymaster.aspx");
                }
            }
            else
            {
                MessageBox.ShowAndRedirect(this, "工号更新失败!", "ProNumberBymaster.aspx");
            }
        }
        //向申请人发送消息
        private void SendRequest(string ProjectName)
        {
            string pro_id = ProjectID.ToString();
            string pronum_id = ProNumberID.ToString();
            string fromUser_id = FromUserID.ToString();
            //工号审批阶段用户发消息
            TG.Model.cm_ProjectDistributionJobNumberConfigEntity distributionJobNumberConfig = new TG.BLL.cm_ProjectDistributionJobNumberConfigBP().GetDistributionJobNumberConfig(1);
            SysMessageViewEntity sysMessage = new SysMessageViewEntity
            {
                MsgType = 3,
                ReferenceSysNo = "pro_id=" + pro_id + "&pronum_id=" + pronum_id + "&FromUser=" + fromUser_id + "" + "&ToRole=1",
                InUser = UserSysNo,
                FromUser = int.Parse(fromUser_id),
                ToRole = "0",
                MessageContent = string.Format("关于 \"{0}\"的工号已分配，可以进行项目策划！", ProjectName),
                IsDone = "B"
            };
            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessage);
        }
        //判断项目是否已经分配工号
        protected bool IsExsitNumber()
        {
            bool flag = false;
            TG.BLL.cm_projectNumber bll_num = new TG.BLL.cm_projectNumber();
            string strwhere = " pro_id=" + ProjectID;
            DataSet ds_cout = bll_num.GetList(strwhere);
            if (ds_cout.Tables.Count > 0)
            {
                if (ds_cout.Tables[0].Rows.Count > 0)
                {
                    if (ds_cout.Tables[0].Rows[0]["proNumber"].ToString() != "")
                    {
                        flag = true;
                    }
                }
            }
            return flag;
        }
        //更新待办状态
        protected void UpdateWorkDoneStatus()
        {
            //修改办公状态
            int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(MessageID);
        }
        public void GetProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProjectID);
            //项目名称
            this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
            //关联合同
            this.txt_reletive.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
            //项目工号
            this.proNumber.Value = pro_model.Pro_number ?? "";
            //管理级别
            string level = pro_model.pro_level.ToString();
            if (level.Trim() == "0")
            {
                this.lbl_level.Text = "院管";
            }
            else
            {
                this.lbl_level.Text = "所管";
            }

            //审核级别
            string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
            if (auditlevel.Trim() == "1,0")
            {
                this.lbl_auditlevel.Text = "院审";
            }
            else if (auditlevel.Trim() == "0,1")
            {
                this.lbl_auditlevel.Text = "所审";
            }
            else if (auditlevel.Trim() == "1,1")
            {
                this.lbl_auditlevel.Text = "院审,所审";
            }


            //建筑级别
            string BuildType = pro_model.BuildType.Trim();
            this.drp_buildtype.Text = BuildType;
            //建设单位
            this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
            //承接部门
            this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            //建设地点
            this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
            //建设规模
            this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
            //项目结构形式
            string StrStruct = pro_model.pro_StruType == null ? "" : pro_model.pro_StruType.Trim();
            this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
            //建筑分类
            string BuildStructType = pro_model.pro_kinds == null ? "" : pro_model.pro_kinds.Trim();
            this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
            //显示项目阶段
            string str_jd = pro_model.pro_status;
            if (str_jd.IndexOf(',') > -1)
            {
                this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
            }
            //项目来源
            string pro_src = "";
            if (pro_model.Pro_src != null)
            {
                pro_src = new TG.BLL.cm_Dictionary().GetModel(Convert.ToInt32(pro_model.Pro_src)).dic_Name;
            }
            this.ddsource.Text = pro_src;
            //合同额
            this.txtproAcount.Text = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
            //行业性质
            string industry = pro_model.Industry == null ? "" : pro_model.Industry.Trim();
            this.ddProfessionType.Text = industry;
            //甲方
            this.txt_Aperson.Text = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
            this.txt_phone.Text = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
            //项目总负责
            this.txt_PMName.Text = pro_model.PMName;
            this.txt_PMPhone.Text = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
            //开始结束日期
            this.txt_startdate.Text = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
            this.txt_finishdate.Text = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");
            //经济所
            string isotherprt = "";
            if (pro_model.ISTrunEconomy == "1")
            {
                isotherprt += "经济所,";
            }
            //暖通
            if (pro_model.ISHvac == "1")
            {
                isotherprt += "暖通热力所,";
            }
            //土建所
            if (pro_model.ISArch == "1")
            {
                isotherprt += "土建所,";
            }
            if (isotherprt.IndexOf(',') > -1)
            {
                this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
            }
            else
            {
                this.lbl_isotherprt.Text = "无";
            }
            //项目特征概况
            string projSub = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
            this.txt_sub.InnerHtml = TG.Common.StringPlus.HtmlEncode(projSub);
            //项目备注
            string projInfo = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
            this.txt_remark.InnerHtml = TG.Common.StringPlus.HtmlEncode(projInfo);
        }
    }
}