﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace TG.Web.ProjectNumber
{
    public partial class SelectProjectNum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定配置
                BingConfig();
            }
        }

        //绑定项目工号配置文件
        protected void BingConfig()
        {
            TG.BLL.cm_ProjectNumConfig bll = new TG.BLL.cm_ProjectNumConfig();
            DataSet ds_type = bll.GetList("");

            this.drp_protype.DataSource = ds_type;
            this.drp_protype.DataTextField = "ProType";
            this.drp_protype.DataValueField = "ID";
            this.drp_protype.DataBind();
        }
        //选择编号类型
        protected void drp_protype_SelectedIndexChanged(object sender, EventArgs e)
        {
            //清除全部
            this.drp_protypenum.Items.Clear();
            TG.BLL.cm_ProjectNumConfig bll = new TG.BLL.cm_ProjectNumConfig();
            TG.Model.cm_ProjectNumConfig model = bll.GetModel(int.Parse(this.drp_protype.SelectedItem.Value));
            int istart = 0;
            int iend = 0;
            if (model != null)
            {
                //前缀
                string prefix = model.PreFix.Trim();
                string mark = model.Mark.Trim();
                string strstart = model.StartNum.Trim();
                string strend = model.EndNum.Trim();
                if (strstart != "")
                {
                    //获取开始日期
                    int.TryParse(strstart, out istart);
                }
                if (strend != "")
                {
                    int.TryParse(strend, out iend);
                }
                //添加工号列表
                this.drp_protypenum.Items.Add(new ListItem("---选择工号---", "-1"));
                for (int i = istart; i <= iend; i++)
                {
                    string strkey = prefix + mark + Prefixf(i.ToString());
                    ListItem item = new ListItem(strkey, strkey);
                    this.drp_protypenum.Items.Add(item);
                }
            }
            //去除
            RemoveItems();
        }
        //去除使用的工号
        protected void RemoveItems()
        {
            TG.BLL.cm_projectNumber bll = new TG.BLL.cm_projectNumber();
            List<string> projNumbers = bll.GetExistsCoperationNo();
            //去掉已经使用的工号
            foreach (string pronum in projNumbers)
            {
                this.drp_protypenum.Items.Remove(pronum);
            }
        }
        /// <summary>
        /// 计算配置数
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        protected string Prefixf(string str)
        {
            string rlt = "";
            if (str.Length == 1)
            {
                rlt = "00" + str;
            }
            else if (str.Length == 2)
            {
                rlt = "0" + str;
            }
            else if (str.Length == 3)
            {
                rlt = str;
            }
            return rlt;
        }
    }
}
