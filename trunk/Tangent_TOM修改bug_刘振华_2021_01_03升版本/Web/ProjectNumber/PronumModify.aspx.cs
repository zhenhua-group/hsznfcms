﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;

namespace TG.Web.ProjectNumber
{
    public partial class PronumModify : PageBase
    {//cm_project项目pro_id
        string pro_id = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["pro_id"] == null)
            {
                Response.Redirect("ProNumber.aspx");
            }
            else
            {
                pro_id = Request.QueryString["pro_id"];
            }
            if (!IsPostBack)
            {
                GetProjectInfo();
                GetproNum();

            }
        }

        //获取立项信息
        public string projd = "";
        public string protype = "";
        public string projb = "";
        public void GetProjectInfo()
        {
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_modol = pro.GetModel(int.Parse(pro_id));
            txtproname.Text = pro_modol.pro_name == null ? "" : pro_modol.pro_name.Trim();
            projd = pro_modol.pro_status;
            protype = pro_modol.pro_kinds;
            //txtprotype.Value = pro_modol.pro_kinds == null ? "" : pro_modol.pro_kinds.Trim();
            projb = pro_modol.pro_level.ToString();


        }
        public void GetproNum()
        {
            //获取项目工号
            TG.BLL.cm_projectNumber pronum = new TG.BLL.cm_projectNumber();
            TG.Model.cm_projectNumber pronum_modol = pronum.GetModel(int.Parse(Request.QueryString["pronum_id"]));
            this.proNumber.Text = pronum_modol.ProNumber == null ? "" : pronum_modol.ProNumber.Trim();
        }
        /// <summary>
        /// 保存工号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnok_Click1(object sender, EventArgs e)
        {

        }
        //向申请人发送消息
        private void SendRequest(string ProjectName)
        {
            TG.Model.cm_ProjectDistributionJobNumberConfigEntity distributionJobNumberConfig = new TG.BLL.cm_ProjectDistributionJobNumberConfigBP().GetDistributionJobNumberConfig(1);

            SysMessageViewEntity sysMessage = new SysMessageViewEntity
            {
                MsgType = 3,
                ReferenceSysNo = pro_id,
                InUser = UserSysNo,
                FromUser = UserSysNo,
                ToRole = distributionJobNumberConfig.RoleSysNo.ToString(),
                MessageContent = string.Format("关于 \"{0}\"的工号申请消息！", ProjectName)
            };
            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessage);
        }
        protected void btnok_Click(object sender, ImageClickEventArgs e)
        {
            TG.BLL.cm_projectNumber pronum = new TG.BLL.cm_projectNumber();
            TG.Model.cm_projectNumber pro_modol = new TG.Model.cm_projectNumber();
            pro_modol.ProNumber = proNumber.Text.Trim();
            string pronumId = Request.QueryString["pronum_id"];
            pro_modol.proNum_id = int.Parse(pronumId);
            bool result = pronum.Exists(proNumber.Text.Trim());
            if (!result)
            {
                //插入tg_project数据
                TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
                TG.Model.cm_Project pr_modol = pro.GetModel(int.Parse(pro_id));

                pr_modol.JobNum = proNumber.Text.Trim();

                //发送消息
                SendRequest(pr_modol.pro_name.Trim());

                //插入cm_projectNumber数据
                pronum.Update(pro_modol);
                Response.Redirect("ProNumber.aspx");
            }
            else
            {
                GetProjectInfo();
                TG.Common.MessageBox.Show(this, "该工号已经存在，请重新选择！");
            }
        }

    }

}
