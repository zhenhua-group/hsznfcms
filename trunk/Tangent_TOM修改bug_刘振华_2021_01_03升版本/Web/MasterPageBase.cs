﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web
{
    public class MasterPageBase : System.Web.UI.MasterPage
    {
        private static TG.BLL.tg_member _bll_user;

        //静态初始化
        static MasterPageBase()
        {
            _bll_user = new TG.BLL.tg_member();
        }

        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        /// <summary>
        /// 用户ID
        /// </summary>
        protected int UserSysNo
        {
            get
            {
                object objUserSysNo = this.UserInfo["memid"];
                if (objUserSysNo != null)
                {
                    return Convert.ToInt32(objUserSysNo);
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// 返回用户登录名
        /// </summary>
        protected string UserSysLoginName
        {
            get
            {
                object objUserLogin = this.UserInfo["memlogin"];

                if (objUserLogin != null)
                {
                    return objUserLogin.ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 返回用户真实姓名
        /// </summary>
        protected string UserSysName
        {
            get
            {
                object objUserSysNo = this.UserInfo["memid"];
                if (objUserSysNo == null)
                {
                    objUserSysNo = 0;
                }
                TG.Model.tg_member user = _bll_user.GetModel(Convert.ToInt32(objUserSysNo));
                if (user != null)
                {
                    return user.mem_Name;
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 性别
        /// </summary>
        protected int UserSex
        {
            get
            {
                object objUserSysNo = this.UserInfo["memid"];
                if (objUserSysNo == null)
                {
                    objUserSysNo = 0;
                }
                TG.Model.tg_member user = _bll_user.GetModel(Convert.ToInt32(objUserSysNo));
                if (user != null)
                {
                    if (user.mem_Sex.Trim() == "男")
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// 用户密码
        /// </summary>
        protected string UserPwd
        {
            get
            {
                object objUserSysNo = this.UserInfo["memid"];
                if (objUserSysNo == null)
                {
                    objUserSysNo = 0;
                }
                TG.Model.tg_member user = _bll_user.GetModel(Convert.ToInt32(objUserSysNo));
                if (user != null)
                {
                    return user.mem_Password.Trim();
                }
                else
                {
                    return "";
                }
            }
        }
    }
}