﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//新加
using Geekees.Common.Controls;
using System.Data;

namespace TG.Web.leftmenu
{
    public partial class left_proj : System.Web.UI.Page
    {
        static string str_action = "bumen";
        static int loadflag = 0;

        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            str_action = Request.QueryString["action"] ?? str_action;
            if (!IsPostBack && Request.QueryString["t1"] != "ajaxLoad")
            {
                string memid = Convert.ToString(UserInfo["memid"] ?? "0");
                //个人信息
                TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                TG.Model.tg_member model_mem = bll_mem.GetModel(int.Parse(memid));
                if (model_mem != null)
                {
                    string mem_login = model_mem.mem_Login;
                    string mem_unitid = model_mem.mem_Unit_ID.ToString();
                    string perFlag = model_mem.mem_Principalship_ID.ToString();

                    if (perFlag == "1" && loadflag == 0)
                    {
                        str_action = "bumen";
                        loadflag = 1;
                    }
                    if (perFlag == "5" && loadflag == 0)
                    {
                        str_action = "bumen";
                        loadflag = 1;
                    }
                    //初始化加载
                    if (str_action == "bumen")
                    {
                        GenerationTree(perFlag, memid, mem_unitid);
                    }
                    else if (str_action == "shijian")
                    {
                        GenerationTreeByTime(perFlag, memid, mem_unitid);
                    }
                    else if (str_action == "geren")
                    {
                        GenerationTreeByPer(perFlag, mem_login, mem_unitid);
                    }
                    //设置根节点图标
                    this.ASTreeView1.RootNode.NodeIcon = "~/images/root_sub.bmp";
                }
            }
        }
        //树菜单的AJAX请求
        protected override void Render(HtmlTextWriter writer)
        {
            if (Request.QueryString["t1"] == "ajaxLoad")
            {
                string virsulKey = Request.QueryString["virtualParentKey"];
                string para = string.Empty;
                if (virsulKey == null)
                {
                    para = " IS NULL";
                }
                else
                {
                    para = virsulKey;
                }
                //声明项目数据Table
                DataTable dt_proj = null;
                string memid = Convert.ToString(UserInfo["memid"] ?? "0");
                //个人信息
                TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                TG.Model.tg_member model_mem = bll_mem.GetModel(int.Parse(memid));
                if (model_mem != null)
                {
                    string mem_login = model_mem.mem_Login;
                    string mem_unitid = model_mem.mem_Unit_ID.ToString();
                    string perFlag = model_mem.mem_Principalship_ID.ToString();
                    //当是所长的时候
                    string t_mem_id = "0";
                    if (str_action == "bumen")
                    {
                        dt_proj = GetProjDataByUnit(perFlag, memid, para).Tables[0];
                    }
                    else if (str_action == "shijian")
                    {
                        dt_proj = GetProjDataByTime(perFlag, memid, mem_unitid, para).Tables[0];
                    }
                    else if (str_action == "geren")
                    {
                        dt_proj = GetProjDataByPer(perFlag, para, mem_unitid).Tables[0];
                        t_mem_id = para;
                    }
                    //声明根节点
                    ASTreeViewNode root = this.ASTreeView1.RootNode;
                    foreach (DataRow dr in dt_proj.Rows)
                    {
                        string unitName = dr["pro_DesignUnit"].ToString();
                        string proName = dr["pro_Name"].ToString();
                        string proID = dr["pro_ID"].ToString();
                        string proStatus = dr["pro_Status"].ToString();

                        ASTreeViewLinkNode node = new ASTreeViewLinkNode(proName, proID);
                        node.VirtualParentKey = unitName;
                        node.NavigateUrl = "#";
                        node.AdditionalAttributes.Add(new KeyValuePair<string, string>("onclick", "showRight(" + dr["pro_ID"].ToString() + "," + para + ");"));
                        root.AppendChild(node);
                    }
                    //显示html
                    HtmlGenericControl ulRoot = new HtmlGenericControl("ul");
                    ASTreeView1.TreeViewHelper.ConvertTree(ulRoot, root, false);
                    foreach (Control c in ulRoot.Controls)
                        c.RenderControl(writer);
                }
            }
            else
            {
                base.Render(writer);
            }
        }
        //根据部门创建树
        public void GenerationTree(string perflag, string memid, string unit)
        {
            DataSet ds_projUnit = GetDataByUnit(perflag, memid, unit);
            if (ds_projUnit.Tables.Count > 0)
            {
                DataTable dt_projUnit = ds_projUnit.Tables[0];
                ASTreeViewNode root = this.ASTreeView1.RootNode;
                foreach (DataRow dr in dt_projUnit.Rows)
                {
                    string unitName = dr["pro_DesignUnit"].ToString();
                    string unitID = dr["unit_ID"].ToString();
                    int childNodesCount = 20;

                    ASTreeViewLinkNode node = new ASTreeViewLinkNode(unitName, unitID);
                    node.VirtualParentKey = unitID;
                    node.VirtualNodesCount = 2;
                    node.IsVirtualNode = childNodesCount > 0;
                    node.NavigateUrl = "#";
                    node.AdditionalAttributes.Add(new KeyValuePair<string, string>("onclick", "return false;"));
                    root.AppendChild(node);
                }
            }
        }
        //部门查询
        public DataSet GetDataByUnit(string perflag, string memid, string unit)
        {
            //sql查询语句
            string str_sql = @"SELECT DISTINCT pro_DesignUnit,unit_ID 
                        FROM tg_project,tg_unit WHERE pro_DesignUnit = unit_Name AND unit_Name IN (Select tg_project.pro_DesignUnit 
                        From tg_project inner join tg_relation on tg_project.pro_ID=tg_relation.pro_ID 
                        Where tg_relation.mem_ID=" + memid + ")  ORDER BY unit_ID";
            //院长或者管理员
            if (perflag == "1" || perflag == "5")
            {
                str_sql = @"SELECT DISTINCT pro_DesignUnit,unit_ID 
                        FROM tg_project,tg_unit 
                        WHERE pro_DesignUnit = unit_Name  ORDER BY unit_ID";
            }
            else if (perflag == "2")
            {
                str_sql = @"SELECT DISTINCT pro_DesignUnit,unit_ID FROM tg_project,tg_unit WHERE pro_DesignUnit = unit_Name AND unit_ID=" + unit;
            }
            //返回结果集
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            return bll.GetList(str_sql);
        }
        //根据部门获取项目信息
        public DataSet GetProjDataByUnit(string perflag, string memid, string unit)
        {
            string str_sql = @"SELECT pro_Name,tg_project.pro_ID,pro_DesignUnit,pro_Status 
                        From tg_project inner join tg_relation on tg_project.pro_ID=tg_relation.pro_ID 
                        Where tg_relation.mem_ID=" + memid + " AND pro_DesignUnit IN (Select unit_Name From tg_unit Where unit_ID=" + unit + ")";
            //院长或网络管理员
            if (perflag == "1" || perflag == "5")
            {
                str_sql = @"SELECT pro_Name,pro_ID,pro_DesignUnit,pro_Status 
                        FROM tg_project Where pro_DesignUnit IN (Select unit_Name From tg_unit Where unit_ID=" + unit + ")";
            }
            else if (perflag == "2")
            {
                str_sql = @"SELECT pro_Name,pro_ID,pro_DesignUnit,pro_Status 
                    FROM tg_project WHERE pro_DesignUnit IN (Select unit_Name From tg_unit Where unit_ID=" + unit + ")";
            }
            //返回结果集
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            return bll.GetList(str_sql);
        }
        //根据时间创建树
        public void GenerationTreeByTime(string perflag, string memid, string unit)
        {
            DataSet ds_projTime = GetDataByTime(perflag, memid, unit);
            if (ds_projTime.Tables.Count > 0)
            {
                DataTable dt_projTime = ds_projTime.Tables[0];

                ASTreeViewNode root = this.ASTreeView1.RootNode;

                foreach (DataRow dr in dt_projTime.Rows)
                {
                    string unitName = DateTime.Now.Year.ToString();
                    if (dr["pro_StartTime"].ToString().Length >= 4)
                    {
                        unitName = dr["pro_StartTime"].ToString().Substring(0, 4);
                    }
                    int childNodesCount = 20;
                    ASTreeViewLinkNode node = new ASTreeViewLinkNode(unitName, unitName);
                    node.VirtualParentKey = unitName;
                    node.VirtualNodesCount = 2;
                    node.IsVirtualNode = childNodesCount > 0;
                    node.NavigateUrl = "#";
                    node.AdditionalAttributes.Add(new KeyValuePair<string, string>("onclick", "return false;"));
                    root.AppendChild(node);
                }
            }
        }
        //时间查询
        public DataSet GetDataByTime(string perflag, string memid, string unit)
        {
            //查询语句
            string str_sql = @"Select distinct YEAR(tg_project.pro_StartTime) as pro_StartTime 
                        From tg_project inner join tg_relation on tg_project.pro_ID=tg_relation.pro_ID 
                        Where tg_relation.mem_ID=" + memid + " order by YEAR(tg_project.pro_StartTime)";
            //院长或网络管理员
            if (perflag == "1" || perflag == "5")
            {
                str_sql = "select distinct YEAR(tg_project.pro_StartTime) as pro_StartTime from tg_project";
            }
            else if (perflag == "2")//所长
            {
                str_sql = @"select distinct YEAR(tg_project.pro_StartTime) as pro_StartTime 
                     From tg_project 
                     Where pro_DesignUnit=(Select unit_Name 
                     From tg_unit Where Unit_ID=" + unit + ") order by YEAR(tg_project.pro_StartTime)";
            }
            //返回结果集
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            return bll.GetList(str_sql);
        }
        //根据时间获取项目信息
        public DataSet GetProjDataByTime(string perflag, string memid, string unit, string strtime)
        {
            string str_sql = @"Select DISTINCT tg_project.pro_ID,tg_project.pro_Name,tg_project.pro_Status,tg_project.pro_DesignUnit 
                        From Tg_Project inner join tg_relation on tg_project.pro_ID=tg_relation.pro_ID Where tg_relation.mem_ID=" + memid + " AND pro_StartTime > '" + strtime + "-01-01' and pro_StartTime <= '" + strtime + "-12-31'";
            //院长或网络管理员
            if (perflag == "1" || perflag == "5")
            {
                str_sql = @"Select DISTINCT pro_ID,pro_Name,tg_project.pro_Status,tg_project.pro_DesignUnit 
                        From Tg_Project Where pro_StartTime > '" + strtime + "-01-01' and pro_StartTime <= '" + strtime + "-12-31'";
            }
            else if (perflag == "2")//所长
            {
                str_sql = @"Select DISTINCT tg_project.pro_ID,tg_project.pro_Name,tg_project.pro_Status,tg_project.pro_DesignUnit 
                        From Tg_Project
                        Where pro_DesignUnit=(Select unit_Name 
                        From tg_unit Where unit_ID=" + unit + ") AND pro_StartTime > '" + strtime + "-01-01' and pro_StartTime <= '" + strtime + "-12-31'";
            }
            //返回结果集
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            return bll.GetList(str_sql);
        }
        //根据人员创建树
        public void GenerationTreeByPer(string perflag, string memlogin, string unit)
        {
            DataSet ds_mem = GetDataByPer(perflag, memlogin, unit);
            if (ds_mem.Tables.Count > 0)
            {
                DataTable dt_mem = ds_mem.Tables[0];
                ASTreeViewNode root = this.ASTreeView1.RootNode;
                foreach (DataRow dr in dt_mem.Rows)
                {
                    string unitName = dr["mem_Name"].ToString();
                    string unitID = dr["mem_ID"].ToString();
                    int childNodesCount = 20;

                    ASTreeViewLinkNode node = new ASTreeViewLinkNode(unitName, unitID);
                    node.VirtualParentKey = unitID;
                    node.VirtualNodesCount = 2;
                    node.IsVirtualNode = childNodesCount > 0;
                    node.NavigateUrl = "#";
                    node.AdditionalAttributes.Add(new KeyValuePair<string, string>("onclick", "return false;"));
                    root.AppendChild(node);
                }
            }
        }
        //人员查询
        public DataSet GetDataByPer(string perflag, string memlogin, string unit)
        {
            //sql 查询语句
            string str_sql = @"SELECT mem_Name,mem_ID FROM tg_member WHERE mem_login='" + memlogin + "'";
            //院长或网络管理员
            if (perflag == "1" || perflag == "5")
            {
                str_sql = "SELECT mem_Name,mem_ID FROM tg_member";
            }
            else if (perflag == "2")
            {
                str_sql = "SELECT mem_Name,mem_ID FROM tg_member Where mem_Unit_ID=" + unit;
            }
            //返回结果集
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            return bll.GetList(str_sql);
        }
        //根据人员获取项目信息
        public DataSet GetProjDataByPer(string perflag, string memid, string unit)
        {
            string str_sql = @"select tg_project.pro_ID,tg_project.pro_Name,tg_project.pro_Status,tg_project.pro_DesignUnit from Tg_Project inner join tg_relation on tg_project.pro_ID=tg_relation.pro_ID Where tg_relation.mem_ID=" + memid + "";
            //院长或网络管理员
            if (perflag == "1" || perflag == "5")
            {
                str_sql = @"select tg_project.pro_ID,tg_project.pro_Name,tg_project.pro_Status,tg_project.pro_DesignUnit from Tg_Project inner join tg_relation on tg_project.pro_ID=tg_relation.pro_ID Where tg_relation.mem_ID= " + memid;
            }
            else if (perflag == "2")
            {
                str_sql = @"select tg_project.pro_ID,tg_project.pro_Name,tg_project.pro_Status,tg_project.pro_DesignUnit from Tg_Project inner join tg_relation on tg_project.pro_ID=tg_relation.pro_ID Where tg_relation.mem_ID= " + memid;
            }
            //返回结果集
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            return bll.GetList(str_sql);
        }
    }
}
