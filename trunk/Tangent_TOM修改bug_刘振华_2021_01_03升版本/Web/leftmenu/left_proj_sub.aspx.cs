﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Web.UI.HtmlControls;
using System.Data;

namespace TG.Web.leftmenu
{
    public partial class left_proj_sub : PageBase
    {
        static string str_proid = "0";
        static string str_spec_id = "-1";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Request.QueryString["t1"] != "ajaxLoad")
            {
                //项目id
                str_proid = Request.QueryString["proid"] ?? "0";
                //得到专业的id
                string str_memid = UserSysNo.ToString();
                TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                TG.Model.tg_member model = bll_mem.GetModel(int.Parse(str_memid));
                if (model != null)
                {
                    string str_pricl = TcdUserRole;
                    if (str_pricl == "2" || str_pricl == "1" || str_pricl == "5")
                    {
                        str_spec_id = getMemSpeid(str_memid);
                    }
                    //加载树
                    InitTreeView(str_proid);
                }
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        protected override void Render(HtmlTextWriter writer)
        {
            if (Request.QueryString["t1"] == "ajaxLoad")
            {
                string virsulKey = Request.QueryString["virtualParentKey"];
                string para = string.Empty;
                if (virsulKey == null)
                {
                    para = " IS NULL";
                }
                else
                {
                    para = virsulKey;
                }
                //遍历添加节点
                ASTreeViewLinkNode root = new ASTreeViewLinkNode("", "");
                GetRecord(para, ref root);

                HtmlGenericControl ulRoot = new HtmlGenericControl("ul");
                ASTreeView1.TreeViewHelper.ConvertTree(ulRoot, root, false);
                foreach (Control c in ulRoot.Controls)
                    c.RenderControl(writer);
            }
            else
            {
                base.Render(writer);
            }
        }
        /// <summary>
        /// 获取用户的专业
        /// </summary>
        /// <param name="mem_id"></param>
        protected string getMemSpeid(string mem_id)
        {
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            TG.Model.tg_member model_mem = bll_mem.GetModel(int.Parse(mem_id));
            if (model_mem != null)
            {
                return model_mem.mem_Speciality_ID.ToString();
            }
            else
            {
                return "-1";
            }
        }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="pro_id">项目id</param>
        protected void InitTreeView(string pro_id)
        {
            //根节点Sql语句
            TG.BLL.tg_project bll_proj = new TG.BLL.tg_project();
            TG.Model.tg_project model = bll_proj.GetModel(int.Parse(pro_id));
            //声明根节点
            ASTreeViewNode root = this.ASTreeView1.RootNode;
            if (model != null)
            {
                this.ASTreeView1.RootNodeText = model.pro_Name;
                this.ASTreeView1.RootNodeValue = model.pro_ID.ToString();
                this.ASTreeView1.RootNode.NodeIcon = "~/images/root_sub.bmp";
            }
            //声明一级节点
            ASTreeViewLinkNode node = null;
            //创建一级节点树
            TG.BLL.tg_subproject bll_subproj = new TG.BLL.tg_subproject();
            string str_where = " pro_parentID=" + pro_id + " AND pro_parentSubID='0'";
            DataTable dt_subproj = bll_subproj.GetList(str_where).Tables[0];
            //创建子节点
            if (dt_subproj.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_subproj.Rows)
                {
                    //取得全局变量
                    string pro_id_2 = dr["pro_ID"].ToString();
                    string proName = dr["pro_Name"].ToString();
                    string proParentSubID = dr["pro_parentSubID"].ToString();
                    int childNodes = dt_subproj.Rows.Count;
                    //判断节点下是否有子节点

                    //添加一级树
                    str_where = " pro_parentSubID=" + pro_id_2 + "";
                    DataTable dt_subproj_child = bll_subproj.GetList(str_where).Tables[0];

                    if (dt_subproj_child.Rows.Count == 0)
                    {
                        TG.BLL.tg_endSubItem bll_end = new TG.BLL.tg_endSubItem();
                        string str_w_end = " subproID=" + pro_id_2 + "";
                        DataTable dt_end = bll_end.GetList(str_w_end).Tables[0];
                        if (dt_end.Rows.Count == 0)
                        {
                            node = CreateTreeNode(proName, pro_id_2, childNodes, false, "#", "onclick", "return false;", pro_id_2);
                            node.NodeIcon = "~/images/root_item.bmp";
                        }
                        else
                        {
                            node = CreateTreeNode(proName, pro_id_2, childNodes, true, "#", "onclick", "return false;", pro_id_2);
                            node.NodeIcon = "~/images/root_item.bmp";
                        }
                    }
                    else
                    {
                        node = CreateTreeNode(proName, pro_id_2, childNodes, true, "#", "onclick", "return false;", pro_id_2);
                        node.NodeIcon = "~/images/root_item.bmp";
                    }
                    //添加一级子节点
                    if (node != null)
                        root.AppendChild(node);
                }
            }
        }

        /// <summary>
        /// 获取树信息
        /// </summary>
        /// <param name="pro_id">项目id</param>
        protected void GetRecord(string pro_id, ref ASTreeViewLinkNode root)
        {
            //声明子节点
            ASTreeViewLinkNode node = null;
            //查询树表
            TG.BLL.tg_subproject bll_subproj = new TG.BLL.tg_subproject();
            string str_where = " pro_parentSubID=" + pro_id + "";
            DataTable dt_subproj = bll_subproj.GetList(str_where).Tables[0];
            if (dt_subproj.Rows.Count == 0)
            {
                TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                string str_w_end = "SELECT DISTINCT purposeID,folder FROM tg_endSubItem,tg_purpose WHERE subproID=" + pro_id + " AND purposeID=purpose_ID";
                DataTable dt_end = bll_db.GetList(str_w_end).Tables[0];
                if (dt_end.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_end.Rows)
                    {
                        string purID = dr["purposeID"].ToString();
                        TG.BLL.tg_purpose bll_pur = new TG.BLL.tg_purpose();
                        string str_w_pur = " purpose_ID=" + purID + "";
                        DataTable dt_pur = bll_pur.GetList(str_w_pur).Tables[0];

                        string purposeID = dt_pur.Rows[0]["purpose_ID"].ToString().Trim();
                        //点击事件
                        string theFunction = "showContent(" + pro_id + "," + "0," + purID + ");";
                        if (purposeID == "3")
                        {
                            //加载往来公函
                            theFunction = "showContent(" + pro_id + "," + "0," + purID + ");";
                            string purposeName = dt_pur.Rows[0]["purpose_Name"].ToString().Trim();
                            //创建公函节点
                            node = CreateTreeNode(purposeName, purposeID, 1, false, "#", "onclick", theFunction, purposeID);
                            node.NodeIcon = "~/images/root_item2.bmp";
                            GetGongHan(pro_id, purposeName, ref node);
                            //添加公函节点到树
                            if (node != null)
                                root.AppendChild(node);
                        }
                        else
                        {
                            string purposefolder = dt_pur.Rows[0]["folder"].ToString().Trim();
                            string purposeName = dt_pur.Rows[0]["purpose_Name"].ToString().Trim();

                            if (purposefolder == "1")
                            {
                                node = CreateTreeNode(purposeName, purposeID, 0, false, "#", "onclick", theFunction, purposeID);
                                if (purposeName == "设计")
                                {
                                    node.NodeIcon = "~/images/sheji.bmp";
                                }
                                else if (purposeName == "提资")
                                {
                                    node.NodeIcon = "~/images/tizi.bmp";
                                }
                                else if (purposeName == "备档")
                                {
                                    node.NodeIcon = "~/images/beidang.bmp";
                                }
                                //加载各个专业节点
                                GetSpecial(pro_id, purposeID, ref node);
                            }
                            else
                            {
                                int itemCount = GetGHIfEnd("0", pro_id, purposeID);
                                if (itemCount == 0)
                                {
                                    //加载三级单项菜单
                                    node = CreateTreeNode(purposeName, purposeID, 0, false, "#", "onclick", theFunction, purposeID);
                                    node.NodeIcon = "~/images/root_item2.bmp";
                                }
                                else
                                {
                                    //加载设计，提资，备档
                                    node = CreateTreeNode(purposeName, purposeID, 1, false, "#", "onclick", theFunction, purposeID);
                                    GetGhSubItem("0", purposeID, pro_id, ref node);
                                }
                            }
                            //添加公函节点到树
                            if (node != null)
                                root.AppendChild(node);
                        }
                    }
                }
            }
            else if (dt_subproj.Rows.Count == 1)
            {
                string proID = dt_subproj.Rows[0]["pro_ID"].ToString();
                string proName = dt_subproj.Rows[0]["pro_Name"].ToString();
                TG.BLL.tg_endSubItem bll_end = new TG.BLL.tg_endSubItem();
                string str_w_end = " subproID=" + proID;
                DataTable dt_end = bll_end.GetList(str_w_end).Tables[0];
                //点击事件
                string theFunction = "showContent(" + pro_id + "," + "0," + proID + ");";
                if (dt_end.Rows.Count == 0)
                {
                    node = CreateTreeNode(proName, proID, 0, false, "#", "onclick", theFunction, proID);
                }
                else
                {
                    node = CreateTreeNode(proName, proID, 1, false, "#", "onclick", theFunction, proID);
                    GetRecord(proID, ref node);
                }

                if (node != null)
                    root.AppendChild(node);
            }
            else
            {
                foreach (DataRow dr in dt_subproj.Rows)
                {
                    string proID = dr["pro_ID"].ToString();
                    string proName = dr["pro_Name"].ToString();
                    //检索表单
                    TG.BLL.tg_endSubItem bll_end = new TG.BLL.tg_endSubItem();
                    string str_w_end = " subproID=" + proID;
                    DataTable dt_end = bll_end.GetList(str_w_end).Tables[0];
                    //点击事件
                    string theFunction = "showContent(" + pro_id + "," + "0," + proID + ");";
                    if (dt_end.Rows.Count == 0)
                    {
                        node = CreateTreeNode(proName, proID, 0, false, "#", "onclick", theFunction, proID);
                    }
                    else
                    {
                        node = CreateTreeNode(proName, proID, 1, false, "#", "onclick", theFunction, proID);
                        GetRecord(proID, ref node);
                    }
                    if (node != null)
                        root.AppendChild(node);
                }
            }
        }

        /// <summary>
        /// 添加公函节点
        /// </summary>
        /// <param name="purid"></param>
        /// <param name="purname"></param>
        /// <param name="root">父节点对象</param>
        protected void GetGongHan(string subproid, string purname, ref ASTreeViewLinkNode root)
        {
            //声明节点
            ASTreeViewLinkNode node = null;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            string str_sql = "SELECT name,id,parentid,subproid,purposeid,purpose_Name FROM tg_subpurpose,tg_purpose WHERE subproid=" + subproid + " AND  purposeid=3 AND purposeid = purpose_ID AND parentid=0";
            DataTable dt_all = bll_db.GetList(str_sql).Tables[0];
            //添加公函节点
            if (dt_all.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_all.Rows)
                {
                    string id = dr["id"].ToString();
                    string name = dr["name"].ToString();
                    string purposeid = dr["purposeid"].ToString();
                    string parentid = dr["parentid"].ToString();
                    int itemCount = GetGHIfEnd(id, subproid, "3");
                    //链接字符
                    string theFunction = "showContent(" + subproid + "," + id + "," + purposeid + "," + parentid + "," + "1" + ")";
                    if (itemCount == 0)
                    {
                        node = CreateTreeNode(name, id, 0, false, "#", "onclick", theFunction, id);
                    }
                    else
                    {
                        node = CreateTreeNode(name, id, 1, false, "#", "onclick", theFunction, id);
                        GetGhSubItem(id, purposeid, subproid, ref node);
                    }
                    if (node != null)
                        root.AppendChild(node);
                }
            }
        }
        /// <summary>
        /// 创建公函子节点
        /// </summary>
        /// <param name="parentid"></param>
        /// <param name="subproid"></param>
        /// <param name="purposeid"></param>
        /// <param name="root">父节点对象</param>
        protected void GetGhSubItem(string parentid, string purposeid, string subproid, ref ASTreeViewLinkNode root)
        {
            //声明节点
            ASTreeViewLinkNode node = null;
            TG.BLL.tg_subpurpose bll_subpur = new TG.BLL.tg_subpurpose();
            string str_w_pur = " parentid=" + parentid + " AND purposeid=" + purposeid + " AND subproid=" + subproid + "";
            DataTable dt_pur = bll_subpur.GetList(str_w_pur).Tables[0];
            if (dt_pur.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_pur.Rows)
                {
                    string id = dr["id"].ToString();
                    string name = dr["name"].ToString();
                    int itemCount = GetGHIfEnd(id, subproid, purposeid);

                    //点击链接
                    string theFunction = "showContent(" + subproid + "," + id + "," + purposeid + "," + parentid + "," + "1" + ")";
                    if (itemCount == 0)
                    {
                        string targetUrl = "#";
                        node = CreateTreeNode(name, id, 0, false, targetUrl, "onclick", theFunction, id);
                    }
                    else
                    {
                        string targetUrl = "#";
                        node = CreateTreeNode(name, id, 1, false, targetUrl, "onclick", theFunction, id);
                        GetGhSubItem(id, subproid, purposeid, ref node);
                    }
                    if (node != null)
                        root.AppendChild(node);
                }
            }
        }
        /// <summary>
        /// 获取专业节点
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="purposeid"></param>
        /// <param name="root">父节点对象</param>
        protected void GetSpecial(string proid, string purposeid, ref ASTreeViewLinkNode root)
        {
            //个人信息
            string str_memid = UserSysNo.ToString();
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            TG.Model.tg_member model_mem = bll_mem.GetModel(int.Parse(str_memid));
            //获取角色
            string str_princ = model_mem.mem_Principalship_ID.ToString();
            string str_spec = model_mem.mem_Speciality_ID.ToString();
            string str_unitid = model_mem.mem_Unit_ID.ToString();
            //声明一个节点
            ASTreeViewLinkNode node = null;

            TG.BLL.tg_config bll_cfg = new TG.BLL.tg_config();
            string str_w_cfg = " conf_name='OtherSpeCanSee'";
            DataTable dt_cfg = bll_cfg.GetList(str_w_cfg).Tables[0];
            if (dt_cfg.Rows.Count > 0)
            {
                //当数据库配置可以查看全专业和当权限为网络管理员和院长的时候可以查看全专业
                if (dt_cfg.Rows[0]["conf_value"].ToString() == "1" || str_princ == "5" || str_princ == "1")
                {
                    TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
                    DataTable dt_spec = bll_spec.GetList("").Tables[0];

                    if (dt_spec.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_spec.Rows)
                        {
                            string speid = dr["spe_ID"].ToString().Trim();
                            string speName = dr["spe_Name"].ToString().Trim();

                            TG.BLL.tg_subClass bll_subclass = new TG.BLL.tg_subClass();
                            string str_w_subcls = " class_ID=" + speid + " AND subproID=" + proid + " AND purposeid=" + purposeid + "";
                            DataTable dt_subcls = bll_subclass.GetList(str_w_subcls).Tables[0];

                            string theFunction = "showContent(" + proid + "," + speid + "," + purposeid + ");";
                            if (dt_subcls.Rows.Count > 0)
                            {
                                //根
                                node = CreateTreeNode(speName, speid, 1, false, "#", "onclick", theFunction, proid);
                                node.NodeIcon = "~/images/specail.bmp";
                                foreach (DataRow dr2 in dt_subcls.Rows)
                                {
                                    string classid = dr2["class_ID"].ToString();
                                    string subproid = dr2["subproID"].ToString();
                                    //获取子项
                                    GetItem(classid, subproid, purposeid, ref node);
                                }
                            }
                            else
                            {
                                node = CreateTreeNode(speName, speid, 0, false, "#", "onclick", theFunction, proid);
                                node.NodeIcon = "~/images/specail.bmp";
                            }

                            if (str_spec_id == speid)
                            {
                                node.NodeIcon = "~/js/astreeview/astreeview/images/item_2.gif";
                            }

                            if (node != null)
                                root.AppendChild(node);
                        }
                    }
                }
                else if (str_princ == "2")//如果是所长权限
                {
                    if (IsFlag(proid, str_unitid))
                    {
                        TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
                        DataTable dt_spec = bll_spec.GetList("").Tables[0];

                        if (dt_spec.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt_spec.Rows)
                            {
                                string speid = dr["spe_ID"].ToString();
                                string speName = dr["spe_Name"].ToString().Trim();
                                string theFunction = "showContent(" + proid + "," + speid + "," + purposeid + ");";
                                node = CreateTreeNode(speName, speid, 1, false, "#", "onclick", theFunction, proid);
                                node.NodeIcon = "~/images/specail.bmp";

                                //显示所长专业
                                if (str_spec_id == speid)
                                {
                                    node.NodeIcon = "~/js/astreeview/astreeview/images/item_2.gif";
                                }

                                try
                                {
                                    root.AppendChild(node);
                                }
                                catch
                                {
                                    node = CreateTreeNode("无专业", "-1", 1, false, "#", "onclick", theFunction, proid);
                                    root.AppendChild(node);
                                    continue;
                                }
                            }
                        }
                    }
                    else//涉及的专业
                    {
                        TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
                        string str_where = "";
                        if (str_princ == "2")
                        {
                            str_where = " spe_ID=" + str_spec_id;
                        }
                        else
                        {
                            str_where = " spe_ID=" + str_spec;
                        }

                        DataTable dt_spec = bll_spec.GetList(str_where).Tables[0];

                        foreach (DataRow dr in dt_spec.Rows)
                        {
                            string spe_id = dr["spe_ID"].ToString();
                            TG.BLL.tg_subClass bll_subclass = new TG.BLL.tg_subClass();
                            string str_w_subcls = " class_ID=" + spe_id + " AND subproID=" + proid + " AND purposeid=" + purposeid + "";
                            DataTable dt_subcls = bll_subclass.GetList(str_w_subcls).Tables[0];

                            string speid = dr["spe_ID"].ToString();
                            string speName = dr["spe_Name"].ToString();

                            if (dt_subcls.Rows.Count > 0)
                            {
                                string theFunction = "showContent(" + proid + "," + speid + "," + purposeid + ");";
                                node = CreateTreeNode(speName, speid, 1, false, "#", "onclick", theFunction, speid);
                                node.NodeIcon = "~/images/img_root/specail.bmp";

                                foreach (DataRow dr2 in dt_subcls.Rows)
                                {
                                    GetItem(dr2["class_ID"].ToString(), dr2["subproID"].ToString(), purposeid, ref node);
                                }
                            }
                            else
                            {
                                string theFunction = "showContent(" + proid + "," + speid + "," + purposeid + ");";
                                node = CreateTreeNode(speName, speid, 0, false, "#", "onclick", theFunction, speid);
                                node.NodeIcon = "~/images/specail.bmp";
                            }
                            if (node != null)
                                root.AppendChild(node);
                        }
                    }
                }
                else//普通用户
                {
                    TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
                    string str_where = "";
                    if (TcdUserRole == "2")
                    {
                        str_where = " spe_ID=" + str_spec_id;
                    }
                    else
                    {
                        str_where = " spe_ID=" + str_spec;
                    }
                    DataTable dt_spec = bll_spec.GetList(str_where).Tables[0];
                    foreach (DataRow dr in dt_spec.Rows)
                    {
                        string spe_id = dr["spe_ID"].ToString();
                        TG.BLL.tg_subClass bll_subclass = new TG.BLL.tg_subClass();
                        string str_w_subcls = " class_ID=" + spe_id + " AND subproID=" + proid + " AND purposeid=" + purposeid + "";
                        DataTable dt_subcls = bll_subclass.GetList(str_w_subcls).Tables[0];
                        string speid = dr["spe_ID"].ToString();
                        string speName = dr["spe_Name"].ToString();

                        if (dt_spec.Rows.Count > 0)
                        {
                            string theFunction = "showContent(" + proid + "," + speid + "," + purposeid + ");";
                            node = CreateTreeNode(speName, speid, 1, false, "#", "onclick", theFunction, speid);
                            node.NodeIcon = "~/images/specail.bmp";
                            foreach (DataRow dr2 in dt_subcls.Rows)
                            {
                                GetItem(dr2["class_ID"].ToString(), dr2["subproID"].ToString(), purposeid, ref node);
                            }
                        }
                        else
                        {
                            string theFunction = "showContent(" + proid + "," + speid + "," + purposeid + ");";
                            node = CreateTreeNode(speName, speid, 0, false, "#", "onclick", theFunction, speid);
                            node.NodeIcon = "~/images/specail.bmp";
                        }

                        if (node != null)
                            root.AppendChild(node);
                    }
                }
            }
        }
        //是否是本所项目
        protected bool IsFlag(string proid, string unitid)
        {
            string unit_ID = unitid;
            string str_sql = "select unit_ID from tg_unit where Unit_Name like '%'+(select pro_DesignUnit from tg_project where pro_ID=(select pro_parentID from tg_subproject where pro_ID=" + proid + "))+'%'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataTable dt_unit = bll_db.GetList(str_sql).Tables[0];
            if (dt_unit.Rows.Count > 0)
            {
                if (dt_unit.Rows[0][0].ToString() == unit_ID)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取节点信息
        /// </summary>
        /// <param name="classid"></param>
        /// <param name="proid"></param>
        /// <param name="purposeid"></param>
        /// <param name="root">父节点对象</param>
        protected void GetItem(string classid, string proid, string purposeid, ref ASTreeViewLinkNode root)
        {
            //声明一个节点
            ASTreeViewLinkNode node = null;
            TG.BLL.tg_subClass bll_subclass = new TG.BLL.tg_subClass();
            string str_w_subcls = " class_ID=" + classid + " AND subproID=" + proid + " AND purposeid=" + purposeid + "";
            DataTable dt_subcls = bll_subclass.GetList(str_w_subcls).Tables[0];

            foreach (DataRow dr in dt_subcls.Rows)
            {
                string subproid = dr["subproID"].ToString();
                string id = dr["id"].ToString();
                string pursid = dr["purposeid"].ToString();
                string name = dr["name"].ToString();
                string parentid = dr["parentID"].ToString();

                int itemCount = GetIfEnd(dr["id"].ToString(), classid, proid);
                string theFunction = "return false";
                if (parentid == "0")
                {
                    if (itemCount == 0)
                    {
                        theFunction = "showContent(" + proid + "," + id + "," + classid + "," + pursid + "," + "1);";
                        node = CreateTreeNode(name, id, 0, false, "#", "onclick", theFunction, pursid);
                    }
                    else
                    {
                        node = CreateTreeNode(name, id, 1, false, "#", "onclick", "return false;", pursid);
                        GetSubItem(id, classid, proid, purposeid, ref node);
                    }
                }
                //添加专业节点
                if (node != null)
                    root.AppendChild(node);
            }
        }
        /// <summary>
        /// 获取子节点信息
        /// </summary>
        /// <param name="parentid"></param>
        /// <param name="classid"></param>
        /// <param name="subproid"></param>
        /// <param name="purposeid"></param>
        /// <param name="root">父节点对象</param>
        protected void GetSubItem(string parentid, string classid, string subproid, string purposeid, ref ASTreeViewLinkNode root)
        {
            // 声明一个节点
            ASTreeViewLinkNode node = null;
            TG.BLL.tg_subClass bll_subclass = new TG.BLL.tg_subClass();
            string str_w_subcls = " parentID=" + parentid + " AND class_ID=" + classid + " AND subproID=" + subproid + "AND purposeid=" + purposeid + "";
            DataTable dt_subcls = bll_subclass.GetList(str_w_subcls).Tables[0];

            if (dt_subcls.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_subcls.Rows)
                {
                    string subid = dr["id"].ToString();
                    string subname = dr["name"].ToString();
                    string subpurid = dr["purposeid"].ToString();

                    int itemCount = GetIfEnd(subid, classid, subproid);
                    if (itemCount == 0)
                    {
                        string theFunction = "showContent(" + subproid + "," + subid + "," + classid + "," + subpurid + "," + "1);";
                        node = CreateTreeNode(subname, subid, 0, false, "#", "onclick", theFunction, subproid);

                        if (node != null)
                            root.AppendChild(node);
                    }
                    else
                    {
                        node = CreateTreeNode(subname, subid, 1, false, "#", "onclick", "return false;", subproid);
                        GetSubItem(subid, classid, subproid, subpurid, ref node);
                    }
                }
            }
        }
        /// <summary>
        /// 判断节点是否含有子节点
        /// </summary>
        /// <param name="parentid"></param>
        /// <param name="classid"></param>
        /// <param name="subproid"></param>
        /// <returns>返回数量</returns>
        protected int GetIfEnd(string parentid, string classid, string subproid)
        {
            TG.BLL.tg_subClass bll_subclass = new TG.BLL.tg_subClass();
            string str_w_subcls = " parentID=" + parentid + " AND class_ID=" + classid + " AND subproID=" + subproid + "";
            //返回记录数
            return bll_subclass.GetRecordCount(str_w_subcls);
        }
        /// <summary>
        /// 返回公函子节点
        /// </summary>
        /// <param name="parentid"></param>
        /// <param name="subproid"></param>
        /// <param name="purposeid"></param>
        /// <returns>返回子节点数</returns>
        protected int GetGHIfEnd(string parentid, string subproid, string purposeid)
        {
            TG.BLL.tg_subpurpose bll_subpur = new TG.BLL.tg_subpurpose();
            string str_where = " parentid=" + parentid + " AND purposeid=" + purposeid + " AND subproid=" + subproid + "";
            //返回记录数
            return bll_subpur.GetRecordCount(str_where);
        }
        /// <summary>
        /// 创建节点方法
        /// </summary>
        /// <param name="treename"></param>
        /// <param name="treeid"></param>
        /// <param name="virtualcount"></param>
        /// <param name="isvirtual"></param>
        /// <param name="navigateurl"></param>
        /// <param name="key"></param>
        /// <param name="keyvalue"></param>
        /// <returns></returns>
        protected ASTreeViewLinkNode CreateTreeNode(string treename, string treeid, int virtualcount, bool isvirtual, string navigateurl, string key, string keyvalue, string proid)
        {
            ASTreeViewLinkNode node = new ASTreeViewLinkNode(treename, treeid);
            node.VirtualNodesCount = virtualcount;
            node.OpenState = ASTreeViewNodeOpenState.Close;
            node.VirtualParentKey = proid;
            node.IsVirtualNode = isvirtual;
            node.NavigateUrl = navigateurl;
            //如果没有子节点
            if (virtualcount == 0)
                node.EnableOpenClose = true;
            node.AdditionalAttributes.Add(new KeyValuePair<string, string>(key, keyvalue));
            //返回节点对象
            return node;
        }
    }
}
