﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace TG.Web.HomePage
{

    public partial class hm_showMemsBymaster : System.Web.UI.Page
    {
        TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
        TG.BLL.tg_principalship bll_pric = new TG.BLL.tg_principalship();
        protected void Page_Load(object sender, EventArgs e)
        {
            string unitid = Request.QueryString["unitid"] ?? "-1";
            if (!IsPostBack)
            {
                ViewState["Unit_ID"] = unitid;
                BindUnit();
                BindBasic();
            }
        }
        //绑定基本信息
        protected void BindBasic()
        {
            this.lbl_unitName.Text = new TG.BLL.tg_unit().GetModel(int.Parse(ViewState["Unit_ID"].ToString())).unit_Name;
            string strWhere = " mem_Unit_ID=" + ViewState["Unit_ID"].ToString();
            TG.BLL.tg_member bll_proj = new TG.BLL.tg_member();
            this.lbl_AllCount.Text = bll_proj.GetRecordCount(strWhere).ToString();


            string sql = "";
            sql = "select *,(select pri_Name from tg_principalship where pri_ID=isnull(mem_Principalship_ID,0)) as pri_Name from tg_member  where mem_Unit_ID=" + ViewState["Unit_ID"] + "";
            DataTable memlist = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            string szlist = "", fszlist = "", gcslist = "";
            int boy = 0, gril = 0;
            if (memlist != null && memlist.Rows.Count > 0)
            {
                for (int i = 0; i < memlist.Rows.Count; i++)
                {
                    if (memlist.Rows[i]["pri_Name"].ToString().Trim() == "所长")
                    {
                        szlist = szlist + memlist.Rows[i]["mem_Name"].ToString() + ",";
                    }
                    if (memlist.Rows[i]["pri_Name"].ToString().Trim() == "副所长")
                    {
                        fszlist = fszlist + memlist.Rows[i]["mem_Name"].ToString() + ",";
                    }
                    if (memlist.Rows[i]["pri_Name"].ToString().Trim() == "主任工程师")
                    {
                        gcslist = gcslist + memlist.Rows[i]["mem_Name"].ToString() + ",";
                    }
                    if (memlist.Rows[i]["mem_Sex"].ToString().Trim() == "男")
                    {
                        boy = boy + 1;
                    }
                    if (memlist.Rows[i]["mem_Sex"].ToString().Trim() == "女")
                    {
                        gril = gril + 1;
                    }
                }
            }
            //所长
            this.lbl_sz.Text = szlist != "" ? szlist.Substring(0, szlist.Length - 1) : "0";
            //副所长
            this.lbl_fsz.Text = fszlist != "" ? fszlist.Substring(0, fszlist.Length - 1) : "0";
            //主任工程师
            this.lbl_gcs.Text = gcslist != "" ? gcslist.Substring(0, gcslist.Length - 1) : "0";

            this.lbl_boy.Text = boy + "";
            this.lbl_gril.Text = gril + "";

        }
        //加载项目信息
        protected void BindUnit()
        {
            string strWhere = " and mem_Unit_ID=" + ViewState["Unit_ID"].ToString();
            this.hid_where.Value = strWhere;


            //记录数
            // this.AspNetPager1.RecordCount = bll_proj.GetRecordCount(strWhere);

            //绑定分页
            // DataSet ds_mem = bll_proj.GetListByPage(strWhere, " mem_Speciality_ID asc", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            //  this.gridView.DataSource = ds_mem;
            //  this.gridView.DataBind();
        }
    }
}