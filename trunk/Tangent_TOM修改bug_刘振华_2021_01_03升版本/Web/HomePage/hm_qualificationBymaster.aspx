﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="hm_qualificationBymaster.aspx.cs" Inherits="TG.Web.HomePage.hm_qualificationBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <link href="../css/m_comm.css" rel="stylesheet" type="text/css" /> 
    <%--<link href="../css/HomePage.css" rel="stylesheet" type="text/css" />--%>
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/hm_qualification.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.tablesort.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="../js/HomePage/hm_projCount.js"></script>
    <script type="text/javascript" src="../js/assets/scripts/portfolio.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("a").click(function () {
                $(".selectimg").remove();
                var hd = $(this).attr("action");
                $.get("../HttpHandler/GetShowImg.ashx", { "quaid": hd }, function (resultStr) {
                    var imgsrc = resultStr;
                    $("#ImgSrc").append(imgsrc);
                });
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        首页 <small>院资质</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a href="#">院资质</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>资质查询</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body " style="display: block;">
                            <table class="table-responsive">
                                <tr>
                                    <td>资质名称:</td>
                                    <td><input type="text" class="form-control input-sm" runat="Server" id="txt_keyname" /></td>
                                    <td>级别: </td>
                                    <td><asp:DropDownList ID="txt_jb" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">--选择级别--</asp:ListItem>
                                                <asp:ListItem Value="甲级">甲级</asp:ListItem>
                                                <asp:ListItem Value="乙级">乙级</asp:ListItem>
                                                <asp:ListItem Value="丙级">丙级</asp:ListItem>
                                                <asp:ListItem Value="丁级">丁级</asp:ListItem>
                                            </asp:DropDownList></td>
                                    <td>年审时间:</td>
                                    <td><input type="text" name="txt_year" id="txt_year" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" size="20" style="width: 150px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                                    <td><input type="submit" class="btn blue" value="查询" id="btn_search" /></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>资质信息列表</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                                    <div class="tabbable-custom boxless">
                                        <ul class="nav nav-tabs">
                                            <li class=""><a href="#tab_2" data-toggle="tab">3 列</a></li>
                                            <li class="active"><a href="#tab_3" data-toggle="tab">2 列</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane" id="tab_2">
                                                <!-- BEGIN FILTER -->
                                                <div class="filter-v1 margin-top-10">
                                                    <div class="row mix-grid thumbnails">
                                                        <asp:Repeater ID="replist" runat="server">
                                                            <ItemTemplate>
                                                                <div class="col-md-4 col-sm-6 mix category_1 mix_all" style="display: block; opacity: 1;">
                                                                    <div class="mix-inner">
                                                                        <table class="cls_tab">
                                                                            <tr style="background-color: #ffffff;">
                                                                                <td width="40%" rowspan="5" align='center' style="background-color: #f0f0f0;">
                                                                                    <a href="hm_quainfoBymaster.aspx?quaid=<%#Eval("qua_Id") %>">
                                                                                        <img src="<%#ShowImg(Eval("qua_Id").ToString()) %>" onerror="javascript:this.src='../Images/zw.jpg'"
                                                                                            border="0" style="width: 150px; height: 120px; margin: 5px 5px;" /></a>
                                                                                </td>
                                                                                <td width="15%" align='left'>
                                                                                    资质名称：
                                                                                </td>
                                                                                <td width="45%" align='left'>
                                                                                    <%#Eval("qua_Name") %>
                                                                                </td>
                                                                                <tr>
                                                                                    <td align='left'>
                                                                                        资质时间：
                                                                                    </td>
                                                                                    <td align='left'>
                                                                                        <%#Convert.ToDateTime(Eval("qua_ZzTime")).ToString("yyyy.MM.dd")%>
                                                                                        -
                                                                                        <%#Convert.ToDateTime(Eval("qua_SpTime")).ToString("yyyy.MM.dd")%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="background-color: #ffffff;">
                                                                                    <td align='left'>
                                                                                        资质级别：
                                                                                    </td>
                                                                                    <td align='left'>
                                                                                        <%#Eval("qua_jb")%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align='left'>
                                                                                        年审时间：
                                                                                    </td>
                                                                                    <td align='left'>
                                                                                        <%#string.IsNullOrEmpty(Eval("qua_yeartime").ToString())==true?"":Convert.ToDateTime(Eval("qua_yeartime")).ToString("yyyy年MM月dd日")%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="background-color: #ffffff;">
                                                                                    <td align='left'>
                                                                                        资质内容：
                                                                                    </td>
                                                                                    <td align='left' title='<%#Eval("qua_Content")%>'>
                                                                                        <%#Eval("qua_Content").ToString().Length > 18 ? Eval("qua_Content").ToString().Substring(0, 15)+"..." : Eval("qua_Content")%>
                                                                                    </td>
                                                                                </tr>
                                                                        </table>
                                                                        <br />
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- END FILTER -->
                                            </div>
                                            <div class="tab-pane active" id="tab_3">
                                                <!-- BEGIN FILTER -->
                                                <div class="filter-v1 margin-top-10">
                                                    <div class="row mix-grid thumbnails">
                                                        <asp:Repeater ID="Repeater1" runat="server">
                                                            <ItemTemplate>
                                                                <div class="col-md-6 col-sm-6 mix category_1 mix_all" style="display: block; opacity: 1;">
                                                                    <div class="mix-inner">
                                                                        <table class="cls_tab">
                                                                            <tr style="background-color: #ffffff;">
                                                                                <td width="40%" rowspan="5" align='center' style="background-color: #f0f0f0;">
                                                                                    <a href="hm_quainfoBymaster.aspx?quaid=<%#Eval("qua_Id") %>">
                                                                                        <img src="<%#ShowImg(Eval("qua_Id").ToString()) %>" onerror="javascript:this.src='../Images/zw.jpg'"
                                                                                            border="0" style="width: 150px; height: 120px; margin: 5px 5px;" /></a>
                                                                                </td>
                                                                                <td width="15%" align='left'>
                                                                                    资质名称：
                                                                                </td>
                                                                                <td width="45%" align='left'>
                                                                                    <%#Eval("qua_Name") %>
                                                                                </td>
                                                                                <tr>
                                                                                    <td align='left'>
                                                                                        资质时间：
                                                                                    </td>
                                                                                    <td align='left'>
                                                                                        <%#Convert.ToDateTime(Eval("qua_ZzTime")).ToString("yyyy.MM.dd")%>
                                                                                        -
                                                                                        <%#Convert.ToDateTime(Eval("qua_SpTime")).ToString("yyyy.MM.dd")%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="background-color: #ffffff;">
                                                                                    <td align='left'>
                                                                                        资质级别：
                                                                                    </td>
                                                                                    <td align='left'>
                                                                                        <%#Eval("qua_jb")%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align='left'>
                                                                                        年审时间：
                                                                                    </td>
                                                                                    <td align='left'>
                                                                                        <%#string.IsNullOrEmpty(Eval("qua_yeartime").ToString())==true?"":Convert.ToDateTime(Eval("qua_yeartime")).ToString("yyyy年MM月dd日")%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="background-color: #ffffff;">
                                                                                    <td align='left'>
                                                                                        资质内容：
                                                                                    </td>
                                                                                    <td align='left' title='<%#Eval("qua_Content")%>'>
                                                                                        <%#Eval("qua_Content").ToString().Length > 18 ? Eval("qua_Content").ToString().Substring(0, 15)+"..." : Eval("qua_Content")%>
                                                                                    </td>
                                                                                </tr>
                                                                        </table>
                                                                        <br />
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- END FILTER -->
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
