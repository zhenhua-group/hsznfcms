﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace TG.Web.HomePage
{
    public partial class hm_InfoByMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCollegePage();
            }
        }
        protected void GetCollegePage()
        {
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            string strSql = " Select Top 1 * From cm_CollegeInfo";
            DataTable dt_content = bll.GetList(strSql).Tables[0];
            if (dt_content.Rows.Count > 0)
            {
                this.Literal1.Text = dt_content.Rows[0][1].ToString();
            }
            else
            {
                this.Literal1.Text = "请在系统设置中设置院系基本介绍！";
            }
        }
    }
}