﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="hm_projPackCountBymaster.aspx.cs" Inherits="TG.Web.HomePage.hm_projPackCountBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/HomePage/hm_projPackCount_jq.js"></script>
    <%--OnClick="btn_export_Click"--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页 <small>院图纸文件统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>院图纸文件统计</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>全院文件总数： <span class="badge badge-red"
                            id="lbl_AllCount" runat="Server">3</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <asp:Button ID="btn_Report" Text="导出Excel" runat="server" CssClass="btn red btn-sm" OnClick="btn_Report_Click" />
                        </div>
                        <div class="btn-group">
                            <a class="btn default btn-xs" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">查询条件<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="hm_projPackCountBymasterAboutMonth.aspx"><i class="i"></i>按月份</a></li>
                                <li><a href="hm_projPackCountBymasterAboutQuarter.aspx"><i class="i"></i>按季度</a></li>
                                <li><a href="hm_projPackCountBymasterAboutYear.aspx"><i class="i"></i>按年份</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_where1" runat="server" Value="" />
    <asp:HiddenField ID="hid_where2" runat="server" Value="" />
    <asp:HiddenField ID="hid_where3" runat="server" Value="" />
</asp:Content>
