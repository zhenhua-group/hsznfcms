﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.HomePage
{
    public partial class hm_projPackCountBymasterAboutQuarter : PageBase
    {
        //检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        TG.BLL.tg_package blll_proj = new TG.BLL.tg_package();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                //全院项目树
                GetAllProjectCount();
                //绑定权限
                BindPreviewPower();
                GetUnitProjectCount();

            }
        }
        protected void SelectCurrentYear()
        {
            int curmonth = DateTime.Now.Month;
            string quarter = "";
            if (curmonth < 4)
            {
                quarter = "第一季度";
            }
            if (curmonth > 3 && curmonth < 7)
            {
                quarter = "第二季度";
            }
            if (curmonth > 6 && curmonth < 10)
            {
                quarter = "第三季度";
            }
            if (curmonth > 9)
            {
                quarter = "第四季度";
            }
            if (this.drp_quarter.Items.FindByText(quarter) != null)
            {
                this.drp_quarter.Items.FindByText(quarter).Selected = true;
            }
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new List<string>();
            list.Add("第一季度"); list.Add("第二季度"); list.Add("第三季度"); list.Add("第四季度");
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_quarter.Items.Add(list[i]);
                }
            }
            List<string> listyear = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (listyear != null)
            {
                for (int i = 0; i < listyear.Count; i++)
                {
                    this.drp_year.Items.Add(listyear[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        /// <summary>
        /// 获取全院项目总数
        /// </summary>
        protected void GetAllProjectCount()
        {
            string strWhere = "";
            this.lbl_AllCount.InnerText = blll_proj.GetRecordCount(strWhere).ToString();
        }
        /// <summary>
        /// 获取所有单位项目数
        /// </summary>
        protected void GetUnitProjectCount()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            strWhere += " AND (unit_ParentID not in(0,231,254) OR  unit_ID=236)";
            this.hid_where.Value = " and " + strWhere;
            //DataSet ds_unit = bll_unit.GetList(strWhere);
            //if (ds_unit.Tables.Count > 0)
            //{
            // this.gridView1.DataSource = ds_unit;
            // this.gridView1.DataBind();
            // }
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        protected void btn_Report_Click(object sender, EventArgs e)
        {
            string strWhere = "";
            StringBuilder strsql = new StringBuilder();
            StringBuilder strsql2 = new StringBuilder();
            string unit = this.drp_unit.SelectedValue;
            string year = this.drp_year.SelectedItem.Value;
            string quarter = this.drp_quarter.SelectedValue;

            //年份
            if (year != "-1")
            {
                if (quarter != "-1")
                {

                    string begintime = "";
                    string donetime = "";
                    if (quarter == "第一季度")
                    {
                        begintime = year + "-1-1";
                        donetime = year + "-3-31";
                    }
                    if (quarter == "第二季度")
                    {
                        begintime = year + "-4-1";
                        donetime = year + "-6-30";
                    }
                    if (quarter == "第三季度")
                    {
                        begintime = year + "-7-1";
                        donetime = year + "-9-30";
                    }
                    if (quarter == "第四季度")
                    {
                        begintime = year + "-10-1";
                        donetime = year + "-12-31";
                    }
                    strsql.AppendFormat(" AND senddate  between '{0}' and '{1}' ", begintime, donetime);
                    strsql2.AppendFormat(" AND ver_date between '{0}' and '{1}' ", begintime, donetime);
                }

            }
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            strWhere += " AND (unit_ParentID not in(0,231,254) OR  unit_ID=236)";
            if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
            {
                strWhere += "AND unit_ID=" + unit;
            }
            strWhere = " and " + strWhere;
            SqlParameter[] parameters ={
                                         new SqlParameter("@PageSize","1000"),
                                           new SqlParameter("@PageIndex","1"),
                                             new SqlParameter("@Where",strWhere),
                                             new SqlParameter("@Where1",strsql.ToString()),
                                             new SqlParameter("@Where2",strsql2.ToString()),
                                               new SqlParameter("@OrderBy","unit_ID"),
                                                 new SqlParameter("@Sort","asc"),
                                                 new SqlParameter("@Total","")
                                     };
            SqlDataReader dr = TG.DBUtility.DbHelperSQL.RunProcedure("P_cm_Unit_jq", parameters);
            ExportDataToExcel(dr, "~/TemplateXls/hm_projPackCount.xls", "院文件统计列表");
        }
        private void ExportDataToExcel(SqlDataReader reader, string modelPath, string pathname)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            int index = 1;

            //ws.SetColumnWidth(2, 25 * 256);
            while (reader.Read())
            {
                IRow row = ws.CreateRow(index);
                ICell cell00 = row.CreateCell(0);
                cell00.SetCellValue(index);
                ICell cell0 = row.CreateCell(1);
                cell0.SetCellValue(reader["unit_Name"].ToString());

                ICell cell1 = row.CreateCell(2);
                cell1.SetCellValue(reader["wjsl"].ToString());

                ICell cell2 = row.CreateCell(3);
                cell2.SetCellValue(reader["DTJ"].ToString());

                ICell cell3 = row.CreateCell(4);
                cell3.SetCellValue(reader["DDY"].ToString());

                ICell cell4 = row.CreateCell(5);
                cell4.SetCellValue(reader["YDY"].ToString());
                ICell cell5 = row.CreateCell(6);
                cell5.SetCellValue(reader["YSC"].ToString());
                ICell cell6 = row.CreateCell(7);
                cell6.SetCellValue(reader["YGD"].ToString());

                index++;
            }
            reader.Close();
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}