﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using System.Text;

namespace TG.Web.HomePage
{
    public partial class hm_qualificationBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_AttachInfo attbll = new TG.BLL.cm_AttachInfo();
        protected string ObjHtml = string.Empty;
        TG.BLL.cm_Qualification quabll = new cm_Qualification();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowQua();
            }
            else
            {
                SubmitQua();
            }
        }

        private void SubmitQua()
        {
            ShowQua();
        }
        //显示每条资质
        protected void ShowQua()
        {
            StringBuilder builder1 = new StringBuilder();

            if (this.txt_keyname.Value != "")
            {
                builder1.Append(" And qua_Name LIKE '%" + this.txt_keyname.Value.Trim() + "%'");
            }
            if (this.txt_jb.SelectedValue != "")
            {
                builder1.Append(" And qua_jb='" + this.txt_jb.SelectedValue.Trim() + "'");
            }
            if (this.txt_year.Value != "")
            {
                builder1.Append(" And DATEDIFF(day,qua_yeartime,'" + Convert.ToDateTime(this.txt_year.Value).ToString("yyyy-MM-dd") + "')=0 ");
            }
            builder1.Append(" order by qua_yeartime desc,qua_id desc");
            this.replist.DataSource = quabll.GetQuaSearchList(builder1.ToString()); ;
            this.replist.DataBind();
            this.Repeater1.DataSource = quabll.GetQuaSearchList(builder1.ToString()); ;
            this.Repeater1.DataBind();


            //StringBuilder builder = new StringBuilder();
            //DataSet ds = quabll.GetQuaSearchList("");
            //foreach (DataRow row in ds.Tables[0].Rows)
            //{
            //    builder.Append("<div class='cls_data'><table class='cls_tab'><tr><td style='width: 20%;' align='center'>资质名称</td><td style='width: 80%;' align='left'>" + row["qua_Name"].ToString() + "</td><tr><td style='width: 20%;' align='center'>资质内容</td><td style='width: 80%;' align='left'><span class='spanClass'>" + row["qua_Content"].ToString() + "</span></td></tr><tr><td style='width: 20%;' align='center'>资质终止时间</td><td style='width: 80%;' align='left'>" + Convert.ToDateTime(row["qua_ZzTime"]).ToString("yyyy-MM-dd") + "</td></tr><tr><td style='width: 20%;' align='center'>审批提醒时间</td><td style='width: 80%;' align='left'>" + Convert.ToDateTime(row["qua_SpTime"]).ToString("yyyy-MM-dd") + "</td></tr><tr><td style='width: 20%;' align='center'>资质图片</td><td style='width: 80%;' align='left'><div class='ImgSrc'>" + ShowImg(row["qua_Id"].ToString()) + "</div></td></tr></table></div>");
            //}
            //ObjHtml = builder.ToString();
        }
        //显示所对应的的img图片
        protected string ShowImg(string quaid)
        {
            string Getsrc = string.Empty;
            string attinfoid = quabll.GetattInfoid(quaid);
            List<TG.Model.cm_AttachInfo> list = attbll.GetModelList("Temp_No='" + attinfoid + "'");
            StringBuilder builder = new StringBuilder();
            if (list.Count > 0)
            {
                //../Attach_User/filedata/qualfile/204248/header-bg(31).jpg
                //foreach (TG.Model.cm_AttachInfo item in list)
                //{
                //    builder.Append("<img src='../Attach_User/filedata/qualfile/" + item.FileUrl + "' class='selectimg' >");
                //}
                Getsrc = "../Attach_User/filedata/qualfile/" + list[0].FileUrl.ToString();
            }
            else
            {
                Getsrc = "../Images/zw.jpg";
            }
            return Getsrc;
        }
    }
}