﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.HomePage
{
    public partial class hm_showProjectAboutStatusBymaster : System.Web.UI.Page
    {
        TG.BLL.tg_project bll_proj = new TG.BLL.tg_project();
        protected void Page_Load(object sender, EventArgs e)
        {
            string unitid = Request.QueryString["unitid"] ?? "-1";
            if (!IsPostBack)
            {
                ViewState["Unit_ID"] = unitid;
                //  BindUnit();
                BindProject();
                this.unitId.Value = unitid;
            }
        }
        protected void BindProject()
        {
            string option = Request.Params["option"] ?? "";
            string status = Request.Params["status"] ?? "";
            TG.BLL.tg_project bll_proj = new TG.BLL.tg_project();
            this.lbl_unitName.InnerText = new TG.BLL.tg_unit().GetModel(int.Parse(ViewState["Unit_ID"].ToString())).unit_Name;
            string strWhere = " AND AT.pro_DesignUnitID=" + ViewState["Unit_ID"].ToString();
            string statusLbl = "";
            if (status != "")
            {
                strWhere += "AND cm.pro_Status='" + status + "'";

                string[] arrayList = status.Split(new char[] { ',' }, StringSplitOptions.None);
                for (int i = 0; i < arrayList.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrayList[i].Trim()))
                    {
                        statusLbl += arrayList[i] + "+";
                    }
                }
                if (statusLbl == "")
                {
                    statusLbl = "项目阶段：无";
                }
                else
                {
                    statusLbl = statusLbl.Substring(0, statusLbl.Length - 1);
                    statusLbl = "项目阶段:" + statusLbl;
                }
                this.lbl_unitStatus.InnerText = statusLbl;
            }
            switch (option)
            {
                case "A":
                    strWhere += "AND (AT.pro_Status='进行中' or AT.pro_Status='')";
                    this.lbl_unitType.InnerText = "  (进行中)";
                    break;
                case "B":
                    strWhere += "AND AT.pro_Status='完工'";
                    this.lbl_unitType.InnerText = "  (完工)";
                    break;
                case "C":
                    strWhere += "AND AT.pro_Status='暂停'";
                    this.lbl_unitType.InnerText = "  (暂停)";
                    break;
                case "D":
                    strWhere += "AND AT.pro_Status=''";
                    this.lbl_unitType.InnerText = "  (未进行)";
                    break;
                default:
                    break;
            }
            this.lbl_AllCount.InnerText = bll_proj.GetRecordCountNew(strWhere).ToString();
            this.hid_where.Value = strWhere;
            //所有记录数
            // this.AspNetPager1.RecordCount = int.Parse(bll_proj.GetRecordCount(strWhere.ToString()).ToString());
            //分页
            // this.gridView.DataSource = bll_proj.GetListByPage(strWhere.ToString(), "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            //this.gridView.DataBind();
        }
        protected void btn_Report_Click(object sender, EventArgs e)
        {
            string option = Request.Params["option"] ?? "";
            string status = Request.Params["status"] ?? "";
            string pathname = "";
            TG.BLL.tg_project bll_proj = new TG.BLL.tg_project();
            pathname += new TG.BLL.tg_unit().GetModel(int.Parse(ViewState["Unit_ID"].ToString())).unit_Name.Trim();
            string strWhere = " AND AT.pro_DesignUnitID=" + ViewState["Unit_ID"].ToString();
            string statusLbl = "";
            if (status != "")
            {
                strWhere += "AND cm.pro_Status='" + status + "'";

                string[] arrayList = status.Split(new char[] { ',' }, StringSplitOptions.None);
                for (int i = 0; i < arrayList.Length; i++)
                {
                    if (!string.IsNullOrEmpty(arrayList[i].Trim()))
                    {
                        statusLbl += arrayList[i] + "+";
                    }
                }
                if (statusLbl == "")
                {
                    statusLbl = "项目阶段：无";
                }
                else
                {
                    statusLbl = statusLbl.Substring(0, statusLbl.Length - 1);
                    statusLbl = "项目阶段:" + statusLbl;
                }
            }
            pathname += statusLbl;
            switch (option)
            {
                case "A":
                    strWhere += "AND (AT.pro_Status='进行中' or AT.pro_Status='')";
                    this.lbl_unitType.InnerText = "  (进行中)";
                    pathname += "  (进行中) ";
                    break;
                case "B":
                    strWhere += "AND AT.pro_Status='完工'";
                    this.lbl_unitType.InnerText = "  (完工)";
                    pathname += "  (完工) ";
                    break;
                case "C":
                    strWhere += "AND AT.pro_Status='暂停'";
                    this.lbl_unitType.InnerText = "  (暂停)";
                    pathname += "  (暂停) ";
                    break;
                case "D":
                    strWhere += "AND AT.pro_Status=''";
                    this.lbl_unitType.InnerText = "  (未进行)";
                    break;
                default:
                    break;
            }
            SqlParameter[] parameters ={
                                         new SqlParameter("@PageSize","1000"),
                                           new SqlParameter("@PageIndex","1"),
                                             new SqlParameter("@Where", strWhere),
                                               new SqlParameter("@OrderBy","AT.pro_ID"),
                                                 new SqlParameter("@Sort","asc"),
                                                 new SqlParameter("@Total","")
                                     };
            SqlDataReader dr = TG.DBUtility.DbHelperSQL.RunProcedure("P_cm_showprojectAboutStatus_jq", parameters);
            ExportDataToExcel(dr, "~/TemplateXls/hm_showProjectAboutStatusBymaster.xls", pathname + "项目统计列表");
        }
        private void ExportDataToExcel(SqlDataReader reader, string modelPath, string pathname)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            int index = 1;

            //ws.SetColumnWidth(2, 25 * 256);
            while (reader.Read())
            {
                IRow row = ws.CreateRow(index);
                ICell cell0 = row.CreateCell(0);
                cell0.SetCellValue(index);

                ICell cell1 = row.CreateCell(1);
                cell1.SetCellValue(reader["pro_Order"].ToString());

                ICell cell2 = row.CreateCell(2);
                cell2.SetCellValue(reader["pro_Name"].ToString());

                ICell cell3 = row.CreateCell(3);
                cell3.SetCellValue(reader["pro_BuildUnit"].ToString());

                ICell cell4 = row.CreateCell(4);
                cell4.SetCellValue(reader["kssj"].ToString());

                ICell cell5 = row.CreateCell(5);
                cell5.SetCellValue(reader["jssj"].ToString());
                ICell cell6 = row.CreateCell(6);
                cell6.SetCellValue(reader["pro_Status"].ToString());
                ICell cell7 = row.CreateCell(7);
                cell7.SetCellValue(reader["FASJ"].ToString());
                ICell cell8 = row.CreateCell(8);
                cell8.SetCellValue(reader["CBSJ"].ToString());
                ICell cell9 = row.CreateCell(9);
                cell9.SetCellValue(reader["SGTSJ"].ToString());

                index++;
            }
            reader.Close();
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}