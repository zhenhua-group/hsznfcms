﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.HomePage
{
    public partial class hm_ConstMemsBymaster : System.Web.UI.Page
    {
        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        public string unitname
        {
            get
            {
                return Request["unitname"];

            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //人员信息
                BindMem();

            }
        }
        //绑定角色信息
        protected void BindMem()
        {
            StringBuilder sb = new StringBuilder("");
            int unitid = 0;
            if (unitname.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(unitname.Trim());
                DataTable dt = new TG.BLL.tg_unit().GetList(" unit_Name='" + keyname + "'").Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    unitid = Convert.ToInt32(dt.Rows[0]["unit_ID"]);
                }
            }
            //部门
            this.lbl_unit.Text = unitname.Trim();
            string sql = "";
            sql = "select *,(select pri_Name from tg_principalship where pri_ID=isnull(mem_Principalship_ID,0)) as pri_Name from tg_member  where mem_Unit_ID=" + unitid + "";
            DataTable memlist = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            string szlist = "", fszlist = "", gcslist = "";
            int boy = 0, gril = 0;
            if (memlist != null && memlist.Rows.Count > 0)
            {
                for (int i = 0; i < memlist.Rows.Count; i++)
                {
                    if (memlist.Rows[i]["pri_Name"].ToString().Trim() == "所长")
                    {
                        szlist = szlist + memlist.Rows[i]["mem_Name"].ToString() + ",";
                    }
                    if (memlist.Rows[i]["pri_Name"].ToString().Trim() == "副所长")
                    {
                        fszlist = fszlist + memlist.Rows[i]["mem_Name"].ToString() + ",";
                    }
                    if (memlist.Rows[i]["pri_Name"].ToString().Trim() == "主任工程师")
                    {
                        gcslist = gcslist + memlist.Rows[i]["mem_Name"].ToString() + ",";
                    }
                    if (memlist.Rows[i]["mem_Sex"].ToString().Trim() == "男")
                    {
                        boy = boy + 1;
                    }
                    if (memlist.Rows[i]["mem_Sex"].ToString().Trim() == "女")
                    {
                        gril = gril + 1;
                    }
                }
            }
            //所长
            this.lbl_sz.Text = szlist != "" ? szlist.Substring(0, szlist.Length - 1) : "";
            //副所长
            this.lbl_fsz.Text = fszlist != "" ? fszlist.Substring(0, fszlist.Length - 1) : "";
            //主任工程师
            this.lbl_gcs.Text = gcslist != "" ? gcslist.Substring(0, gcslist.Length - 1) : "";

            sb.AppendFormat(" mem_Unit_ID={0}", unitid);
            //角色数量
            //  this.AspNetPager1.RecordCount = int.Parse(bll_mem.GetRecordCount(sb.ToString()).ToString());
            //总数量
            this.lbl_all.Text = bll_mem.GetRecordCount(sb.ToString()).ToString();
            this.lbl_boy.Text = boy + "";
            this.lbl_gril.Text = gril + "";

            this.hid_where.Value = " and " + sb.ToString();
            //绑定

            // this.grid_mem.DataSource = bll_mem.GetListByPage(sb.ToString(), " mem_Speciality_ID asc", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            // this.grid_mem.DataBind();
        }


        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //单位
                //TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
                //string str_unitid = e.Row.Cells[5].Text;
                //if (str_unitid != "")
                //{
                //    TG.Model.tg_unit model_unit = bll_unit.GetModel(int.Parse(str_unitid));
                //    if (model_unit != null)
                //    {
                //        e.Row.Cells[5].Text = model_unit.unit_Name.ToString().Trim();
                //    }
                //}

                //专业
                TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
                string str_specid = e.Row.Cells[3].Text;
                if (str_specid != "")
                {
                    TG.Model.tg_speciality model_spec = bll_spec.GetModel(int.Parse(str_specid));
                    if (model_spec != null)
                    {
                        e.Row.Cells[3].Text = model_spec.spe_Name.ToString().Trim();
                    }
                }
                ////年龄
                //string birthday = e.Row.Cells[4].Text.Trim();
                //if (birthday!="")
                //{
                //    e.Row.Cells[4].Text = (DateTime.Now.Year - Convert.ToDateTime(birthday).Year) + "";
                //}
                //角色
                TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();
                string str_priid = e.Row.Cells[4].Text;
                if (str_priid != "")
                {
                    TG.Model.tg_principalship model_pri = bll_pri.GetModel(int.Parse(str_priid));
                    if (model_pri != null)
                    {
                        e.Row.Cells[4].Text = model_pri.pri_Name.ToString().Trim();
                    }
                }
                //职称               
                TG.BLL.cm_MemArchLevelRelation bll_relation = new TG.BLL.cm_MemArchLevelRelation();
                TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
                string memid = e.Row.Cells[5].Text;
                if (memid != "")
                {

                    List<TG.Model.cm_MemArchLevelRelation> models_relation = bll_relation.GetModelList(" mem_ID=" + memid);
                    if (models_relation.Count > 0)
                    {
                        //职称
                        TG.Model.cm_MemArchLevel model_level = bll_level.GetModel(models_relation[0].ArchLevel_ID);
                        if (model_level != null)
                        {
                            e.Row.Cells[5].Text = model_level.Name.Trim();
                        }

                    }
                    else
                    {
                        e.Row.Cells[5].Text = "无";
                    }

                }
                //项目数量
                string mem_id = e.Row.Cells[7].Text;
                string sqlwhere = " PMUserID=" + mem_id;
                List<TG.Model.cm_Project> prolist = new TG.BLL.cm_Project().GetModelList(sqlwhere);
                if (prolist != null)
                {
                    e.Row.Cells[7].Text = prolist.Count + "";
                }
            }
        }
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindMem();
        }
    }
}