﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.HomePage
{
    public partial class hm_quainfoBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_Qualification quabll = new TG.BLL.cm_Qualification();
        TG.BLL.cm_QuaAttach cm_bll = new TG.BLL.cm_QuaAttach();
        protected void Page_Load(object sender, EventArgs e)
        {
            string quaid = Request.QueryString["quaid"];
            string attinfoid = Request.QueryString["attinfoid"];
            if (!IsPostBack)
            {
                //绑定资质
                BindQuali(quaid);
            }

        }
        /// <summary>
        /// 绑定资质
        /// </summary>
        /// <param name="quaid"></param>
        private void BindQuali(string quaid)
        {
            TG.Model.cm_Qualification model = quabll.GetSingleModel(quaid);
            this.lbl_title.Text = model.qua_Name;
            this.lbl_content.Text = model.qua_Content;
            this.lbl_Zztime.Text = model.qua_ZzTime == null ? "" : Convert.ToDateTime(model.qua_ZzTime).ToString("yyyy-MM-dd");
            this.lbl_Sptime.Text = model.qua_SpTime == null ? "" : Convert.ToDateTime(model.qua_SpTime).ToString("yyyy-MM-dd");

            this.lbl_jb.Text = model.qua_jb;
            this.lbl_yeartime.Text = model.qua_yeartime == null ? "" : Convert.ToDateTime(model.qua_yeartime).ToString("yyyy-MM-dd");

        }
        //得到Id
        public string GetProjectID()
        {
            string quaid = Request.QueryString["quaid"];
            return quabll.GetattInfoid(quaid);

        }
    }
}