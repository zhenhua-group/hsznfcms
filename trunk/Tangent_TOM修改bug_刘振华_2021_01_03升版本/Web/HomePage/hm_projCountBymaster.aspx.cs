﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.HomePage
{
    public partial class hm_projCountBymaster : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetProjCount();
                //全院项目树

                //部门项目
                GetUnitProjectCount();
            }
        }

        private void GetProjCount()
        {
            TG.BLL.tg_project blll_proj = new TG.BLL.tg_project();
            this.projCount.InnerText = blll_proj.GetRecordCount("").ToString();
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 获取所有单位项目数
        /// </summary>
        protected void GetUnitProjectCount()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            ////如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " and unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " and unit_ID= " + UserUnitNo;
            }

            //不显示的单位

            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            strWhere += " AND (unit_ParentID not in(0,231,254) OR  unit_ID=236)";
            this.hid_where.Value = strWhere;

            //// 分页数
            //this.AspNetPager1.RecordCount = bll_unit.GetRecordCount(strWhere);

            ////排序
            //string orderString =  OrderColumn + " " + Order;

            ////分页数据
            //DataSet ds_unit = bll_unit.GetListByPage(strWhere, orderString, this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            //if (ds_unit.Tables.Count > 0)
            //{
            //    this.gridView.DataSource = ds_unit;
            //    this.gridView.DataBind();
            //}
        }

        protected void btn_Report_Click(object sender, EventArgs e)
        {
            string strWhere = "";
            ////如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " and unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " and unit_ID= " + UserUnitNo;
            }

            //不显示的单位

            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            strWhere += " AND (unit_ParentID not in(0,231,254) OR  unit_ID=236)";
            SqlParameter[] parameters ={
                                         new SqlParameter("@PageSize","1000"),
                                           new SqlParameter("@PageIndex","1"),
                                             new SqlParameter("@Where",strWhere),
                                               new SqlParameter("@OrderBy","unit_ID"),
                                                 new SqlParameter("@Sort","asc"),
                                                 new SqlParameter("@Total","")
                                     };
            SqlDataReader dr = TG.DBUtility.DbHelperSQL.RunProcedure("P_cm_projCount_jq", parameters);
            ExportDataToExcel(dr, "~/TemplateXls/hm_projCountBymaster.xls", "院项目统计列表");
        }
        private void ExportDataToExcel(SqlDataReader reader, string modelPath, string pathname)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            int index = 1;

            //ws.SetColumnWidth(2, 25 * 256);
            while (reader.Read())
            {
                IRow row = ws.CreateRow(index);
                ICell cell00 = row.CreateCell(0);
                cell00.SetCellValue(index);
                ICell cell0 = row.CreateCell(1);
                cell0.SetCellValue(reader["unit_Name"].ToString());

                ICell cell1 = row.CreateCell(2);
                cell1.SetCellValue(reader["memcount"].ToString());

                ICell cell2 = row.CreateCell(3);
                cell2.SetCellValue(reader["sz"].ToString());

                ICell cell3 = row.CreateCell(4);
                cell3.SetCellValue(reader["projectcount"].ToString());

                ICell cell4 = row.CreateCell(5);
                cell4.SetCellValue(reader["ProstateA"].ToString());
                ICell cell5 = row.CreateCell(6);
                cell5.SetCellValue(reader["ProstateB"].ToString());
                ICell cell6 = row.CreateCell(7);
                cell6.SetCellValue(reader["ProstateC"].ToString());

                index++;
            }
            reader.Close();
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}