﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.HomePage
{
    public partial class hm_showProjectStatusBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string unitid = Request.QueryString["unitid"] ?? "-1";
            if (!IsPostBack)
            {
                ViewState["Unit_ID"] = unitid;
                //  BindUnit();
                BindProject();
                this.unitId.Value = unitid;
            }
        }
        protected void BindProject()
        {
            string option = Request.Params["option"] ?? "";
            TG.BLL.tg_project bll_proj = new TG.BLL.tg_project();
            this.lbl_unitName.InnerText = new TG.BLL.tg_unit().GetModel(int.Parse(ViewState["Unit_ID"].ToString())).unit_Name;
            string strWhere1 = " AND pro_DesignUnitID=" + ViewState["Unit_ID"].ToString();
            string strWhereother = "AND  mm.pro_DesignUnitID=" + ViewState["Unit_ID"].ToString();
            string strWhere = " AND cmp.Unit='" + this.lbl_unitName.InnerText + "'";
            switch (option)
            {
                case "A":
                    strWhere += "AND (pro_Status='进行中' or pro_Status='')";
                    this.lbl_unitType.InnerText = "  (进行中)";
                    break;
                case "B":
                    strWhere += "AND pro_Status='完工'";
                    this.lbl_unitType.InnerText = "  (完工)";
                    break;
                case "C":
                    strWhere += "AND pro_Status='暂停'";
                    this.lbl_unitType.InnerText = "  (暂停)";
                    break;
                case "D":
                    strWhere += "AND pro_Status=''";
                    this.lbl_unitType.InnerText = "  (未进行)";
                    break;
                default:
                    break;
            }
            this.lbl_AllCount.InnerText = bll_proj.GetRecordCount(strWhere1).ToString();
            this.hid_where.Value = strWhere;
            this.hid_where1.Value = strWhereother;
            //所有记录数
            // this.AspNetPager1.RecordCount = int.Parse(bll_proj.GetRecordCount(strWhere.ToString()).ToString());
            //分页
            // this.gridView.DataSource = bll_proj.GetListByPage(strWhere.ToString(), "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            //this.gridView.DataBind();
        }
        protected void btn_Report_Click(object sender, EventArgs e)
        {
            string option = Request.Params["option"] ?? "";
            string pathname = "";
            string strWhere1 = " AND pro_DesignUnitID=" + ViewState["Unit_ID"].ToString();
            string strWhereother = "AND  mm.pro_DesignUnitID=" + ViewState["Unit_ID"].ToString();
            pathname += new TG.BLL.tg_unit().GetModel(int.Parse(ViewState["Unit_ID"].ToString())).unit_Name;
            string strWhere = " AND cmp.Unit='" + this.lbl_unitName.InnerText + "'";
            switch (option)
            {
                case "A":
                    strWhere += "AND (pro_Status='进行中' or pro_Status='')";
                    this.lbl_unitType.InnerText = "  (进行中)";
                    break;
                case "B":
                    strWhere += "AND pro_Status='完工'";
                    this.lbl_unitType.InnerText = "  (完工)";
                    break;
                case "C":
                    strWhere += "AND pro_Status='暂停'";
                    this.lbl_unitType.InnerText = "  (暂停)";
                    break;
                case "D":
                    strWhere += "AND pro_Status=''";
                    this.lbl_unitType.InnerText = "  (未进行)";
                    break;
                default:
                    break;
            }
            this.hid_where.Value = strWhere;
            this.hid_where1.Value = strWhereother;
            SqlParameter[] parameters ={
                                         new SqlParameter("@PageSize","1000"),
                                           new SqlParameter("@PageIndex","1"),
                                             new SqlParameter("@Where", strWhere),
                                              new SqlParameter("@Where1", strWhereother),
                                               new SqlParameter("@OrderBy","Status"),
                                                 new SqlParameter("@Sort","asc"),
                                                 new SqlParameter("@Total","")
                                     };
            SqlDataReader dr = TG.DBUtility.DbHelperSQL.RunProcedure("P_cm_showProjectStatus_jq", parameters);
            ExportDataToExcel(dr, "~/TemplateXls/hm_showProjectStatusBymaster.xls", pathname.Trim() + "项目阶段统计列表");
        }
        private void ExportDataToExcel(SqlDataReader reader, string modelPath, string pathname)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            int index = 1;

            //ws.SetColumnWidth(2, 25 * 256);
            while (reader.Read())
            {
                IRow row = ws.CreateRow(index);
                ICell cell00 = row.CreateCell(0);
                cell00.SetCellValue(index);
                ICell cell0 = row.CreateCell(1);
                cell0.SetCellValue(reader["Status"].ToString().Trim());

                ICell cell1 = row.CreateCell(2);
                cell1.SetCellValue(reader["Jdcount"].ToString());

                ICell cell2 = row.CreateCell(3);
                cell2.SetCellValue(reader["JXcount"].ToString());

                ICell cell3 = row.CreateCell(4);
                cell3.SetCellValue(reader["ZTcount"].ToString());

                ICell cell4 = row.CreateCell(5);
                cell4.SetCellValue(reader["WGcount"].ToString());
                index++;
            }
            reader.Close();
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}