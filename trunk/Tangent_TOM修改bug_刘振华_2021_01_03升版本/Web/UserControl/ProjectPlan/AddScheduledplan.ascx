﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddScheduledplan.ascx.cs"
    Inherits="TG.Web.UserControl.ProjectPlan.AddScheduledplan" %>
<div id="AddScheduledplanMain">
    <table style="width: 500px; margin-bottom: 20px" class="table table-bordered table-hover">
        <tr>
            <td style="width: 100px">
                设计阶段
            </td>
            <td colspan="3">
                <select style="width: 120px;" id="designLevel">
                    <option value="方案设计">方案设计</option>
                    <option value="初步设计">初步设计</option>
                    <option value="主专业提资">主专业提资</option>
                    <option value="二次反提资">二次反提资</option>
                    <option value="施工图设计">施工图设计</option>
                    <option value="校对">校对</option>
                    <option value="审核">审核</option>
                    <option value="审定">审定</option>
                    <option value="备档">备档</option>
                    <option value="打印">打印</option>
                    <option value="晒图">晒图</option>
                    <option value="盖章">盖章</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                开始时间
            </td>
            <td>
                <input id="txtStartTime" class="Wdate" name="txt_date1" onclick="WdatePicker({ readOnly: true })"
                    size="20" style="width: 120px;" type="text" />
            </td>
            <td>
                结束时间
            </td>
            <td>
                <input id="txtFinishtime" class="Wdate" name="txt_date2" onclick="WdatePicker({ readOnly: true })"
                    size="20" style="width: 120px;" type="text" />
            </td>
        </tr>
    </table>
    <fieldset style="width: 500px">
        <legend style="font-size: 10pt;">图纸张数（折合1#图纸）</legend>
        <table class="table table-bordered table-hover" style="width: 500px;">
            <tr>
                <td style="width: 17%;" align="center">
                    建筑
                </td>
                <td style="width: 17%" align="center">
                    结构
                </td>
                <td style="width: 17%" align="center">
                    电气
                </td>
                <td style="width: 17%" align="center">
                    暖通
                </td>
                <td style="width: 17%" align="center">
                    给排水
                </td>
                <td style="width: 15%" align="center">
                    市政
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input id="txtjz" type="text" name="number" style="width: 50%;" />
                </td>
                <td align="center">
                    <input id="txtjg" type="text" name="number" style="width: 50%;" />
                </td>
                <td align="center">
                    <input id="txtdq" type="text" name="number" style="width: 50%;" />
                </td>
                <td align="center">
                    <input id="txtnt" type="text" name="number" style="width: 50%;" />
                </td>
                <td align="center">
                    <input id="txtgps" type="text" name="number" style="width: 50%;" />
                </td>
                <td align="center">
                    <input id="txtsz" type="text" name="number" style="width: 50%;" />
                </td>
            </tr>
        </table>
    </fieldset>
</div>
