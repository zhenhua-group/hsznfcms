﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model.UserControl;
using Newtonsoft.Json;

namespace TG.Web.UserControl
{
    public partial class ChooseUser : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ChooseUser));
            BindDepartment();
        }

        /// <summary>
        /// 绑定部门列表
        /// </summary>
        private void BindDepartment()
        {
            this.RepeaterDepartment.DataSource = new TG.BLL.tg_unit().GetList("");
            this.RepeaterDepartment.DataBind();
        }

        [AjaxMethod]
        public string GetUsers(string queryString)
        {
            ChooseUserQueryEntity queryEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ChooseUserQueryEntity>(queryString);

            int totalCount = 0;

            List<ChooseUserViewEntity> resultList = new TG.BLL.ChooseUserBP().GetUserList(queryEntity, out totalCount);

            object[] resultArray = new object[2];

            resultArray[0] = totalCount;
            resultArray[1] = resultList;

            return Newtonsoft.Json.JsonConvert.SerializeObject(resultArray);
        }
    }
}