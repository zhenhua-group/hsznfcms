﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using AjaxPro;
using Newtonsoft.Json;
using TG.Model;
using TG.BLL;

namespace TG.Web.UserControl
{
    public partial class AuditConfigBase : System.Web.UI.UserControl
    {
        /// <summary>
        /// 审核共通类
        /// </summary>
        public List<TG.Model.AuditConfigBase> AuditConfigBaseList { get; set; }

        /// <summary>
        /// 角色集合
        /// </summary>
        public List<TG.Model.cm_Role> RoleList { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 数据库表名
        /// </summary>
        public string TableName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AuditConfigBase));

            if (string.IsNullOrEmpty(TableName))
            {
                throw new Exception("TableName表名不能为空！");
            }
            else if (string.IsNullOrEmpty(Title))
            {
                throw new Exception("Title标题不能为空！");
            }
            InitControl();
        }


        private void InitControl()
        {
            AuditConfigBaseList = new TG.BLL.AuditConfigCommonBP().GetAudtConfigBaseEntityList(TableName);

            RoleList = new TG.BLL.cm_Role().GetRoleList();
        }

        /// <summary>
        /// 修改角色所对应的用户
        /// </summary>
        /// <param name="userSysNoString"></param>
        /// <param name="roleSysNoString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string UpdateRoleUsers(string userSysNoString, string roleSysNoString)
        {
            TG.BLL.cm_Role roleBLL = new TG.BLL.cm_Role();

            int roleSysNo = int.Parse(roleSysNoString);

            TG.Model.cm_Role finalEntity = roleBLL.GetRole(roleSysNo);

            finalEntity.Users = userSysNoString;

            int count = roleBLL.UpdateRole(finalEntity);

            return count + "";
        }

        /// <summary>
        /// 修改工作流对应的角色
        /// </summary>
        [AjaxMethod]
        public string UpdateCoperationAuditConfigRoleRelationship(string jsonData, string tableName)
        {
            AuditConfigCommonBP bp = new AuditConfigCommonBP();

            //得到前台要修改的参数
            List<AuditConfigEntity> auditConfigEntityList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AuditConfigEntity>>(jsonData);
            int count = 0;
            foreach (var auditParameter in auditConfigEntityList)
            {
                count = bp.UpdateProjectAuditConfigToRelationship(auditParameter, tableName);
            }
            return count + "";
        }

        /// <summary>
        /// 根据角色查询所有的用户集合
        /// </summary>
        /// <param name="roleSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetRoleUsersList(string roleSysNo)
        {
            List<TG.Model.tg_member> resultList = new TG.BLL.cm_Role().GetRoleUsersList(int.Parse(roleSysNo));

            return Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
        }
    }
}