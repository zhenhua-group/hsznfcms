﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectAuditListView.ascx.cs"
    Inherits="TG.Web.UserControl.ProjectAudit.ProjectAuditListView" %>

<script src="../js/Global.js" type="text/javascript"></script>

<script src="../js/Jquery-extend.js" type="text/javascript"></script>

<script src="../js/ProjectMamage/ProjectAuditListView.js" type="text/javascript"></script>

<div id="tabs0">
    <ul style="padding-bottom: 10px;" id="tabsLi">
        <li style="float: left; list-style-type: none;">
            <div class="cls_1">
                <a href="#tabs0-1" index="0" style="font-size: 12px; font-weight: normal;" class="tabsChangedIn">
                    待评审</a></div>
        </li>
        <li style="float: left; list-style-type: none; padding-left: 5px;">
            <div class="cls_1">
                <a href="#tabs0-2" index="1" style="color: Blue; font-size: 12px; font-weight: normal;">
                    已评审</a></div>
        </li>
    </ul>
    <div id="tabs0-1">
        <div class="cls_data" style="overflow-y: scroll; height: 350px;">
            <table class="cls_content_head" id="tableResult_01" style="text-align: center; width: 97%;">
                <tr>
                    <td style="width: 45%" align="center">
                        项目名称
                    </td>
                    <td style="width: 15%;" align="center">
                        登记日期
                    </td>
                    <td style="width: 10%" align="center">
                        登记人
                    </td>
                    <td style="width: 20%;" align="center">
                        审核状态
                    </td>
                    <td style="width: 10%;" align="center">
                        操作
                    </td>
                </tr>
            </table>
        </div>
        <div class="cls_data_bottom">
            <div id="pager">
            </div>
        </div>
    </div>
    <div id="tabs0-2" style="display: none;">
        <div class="cls_data" style="overflow-y: scroll; height: 350px;">
            <table class="cls_content_head" style="text-align: center; width: 97%;" id="tableResult_02">
                <tr>
                    <td style="width: 45%" align="center">
                        项目名称
                    </td>
                    <td style="width: 15%;" align="center">
                        登记日期
                    </td>
                    <td style="width: 10%" align="center">
                        登记人
                    </td>
                    <td style="width: 20%;" align="center">
                        审核状态
                    </td>
                    <td style="width: 10%;" align="center">
                        操作
                    </td>
                </tr>
            </table>
        </div>
        <div class="cls_data_bottom">
            <div id="pager">
            </div>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="UserSysNoHidden" value="<%=UserSysNo %>" />
</div>
