﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using TG.Model;
using System.Collections.Generic;
using Geekees.Common.Controls;
using System.Text;

namespace TG.Web.UserControl
{
    public partial class UserOfTheDepartmentTree : System.Web.UI.UserControl
    {
        private const string folderIcon = "/js/astreeview/astreeview/images/user_group2.png";

        private const string fileIcon = "/js/astreeview/astreeview/images/user_male.png";

        private const string rootIcon = "/js/astreeview/astreeview/images/user_part2.png";
        /// <summary>
        /// 是否单选
        /// </summary>
        public bool IsRadio { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //样式
            SetDropDownTreeThem();
            //初始化树
            InitTree();
        }

        private void InitTree()
        {
            List<TreeDepartmentEntity> treeDepartmentList = new TG.BLL.cm_ProjectPlanBP().GetTreeDepartmentList();

            ASTreeViewNode rootNode = new ASTreeViewNode("弘石产值分配系统");
            rootNode.NodeIcon = rootIcon;
            rootNode.EnableCheckbox = false;
            if (IsRadio)
            {
                rootNode.EnableCheckbox = false;
            }
            this.astvMyTree.RootNode.AppendChild(rootNode);

            treeDepartmentList.ForEach((department) =>
            {
                ASTreeViewNode departmentNode = new ASTreeViewNode(department.DepartmentName, department.DepartmentSysNo.ToString(), folderIcon);
                departmentNode.OpenState = ASTreeViewNodeOpenState.Close;
                if (IsRadio)
                {
                    departmentNode.EnableCheckbox = false;
                }
                rootNode.AppendChild(departmentNode);

                department.Users.ForEach((user) =>
                {
                    string userfileicon = fileIcon;
                    if (user.UserSex == "女")
                    {
                        userfileicon = "/js/astreeview/astreeview/images/user_female.png";
                    }
                    string specialtyName = "[" + user.SpecialtyName + "]";
                    ASTreeViewNode userNode = new ASTreeViewNode(user.UserName + specialtyName, user.UserSysNo.ToString(), userfileicon);

                    List<KeyValuePair<string, string>> customerAttributesList = new List<KeyValuePair<string, string>>
                                        {
                                                new KeyValuePair<string, string>("lastLeveFlag","true"),
                                                new KeyValuePair<string, string>("userSysNo",user.UserSysNo.ToString()),
                                                new KeyValuePair<string, string>("userName", user.UserName),
                                                new KeyValuePair<string, string>("specialtyno", user.SpecialtyNo.ToString()),
                                                new KeyValuePair<string, string>("specialtyName", user.SpecialtyName),
                                                new KeyValuePair<string, string>("departmentName", department.DepartmentName),
                                                new KeyValuePair<string, string>("userSex", user.UserSex)
                                        };
                    userNode.AdditionalAttributes.AddRange(customerAttributesList);
                    departmentNode.AppendChild(userNode);
                });
            });
        }

        /// <summary>
        /// 初始化单选模式
        /// </summary>
        /// <param name="treeDepartmentList"></param>
        private void InitRadioDialog(List<TreeDepartmentEntity> treeDepartmentList)
        {
            ASTreeViewTextNode rootNode = new ASTreeViewTextNode("天正生产经营管理系统");

            this.astvMyTree.RootNode.AppendChild(rootNode);

            treeDepartmentList.ForEach((department) =>
            {
                var departmentNode = new ASTreeViewTextNode(department.DepartmentName, department.DepartmentSysNo.ToString());
                departmentNode.OpenState = ASTreeViewNodeOpenState.Close;
                rootNode.AppendChild(departmentNode);

                department.Users.ForEach((user) =>
                {
                    ASTreeViewTextNode userNode = new ASTreeViewTextNode(user.UserName, user.UserSysNo.ToString());

                    List<KeyValuePair<string, string>> customerAttributesList = new List<KeyValuePair<string, string>>
                                        {
                                                new KeyValuePair<string, string>("lastLeveFlag","true"),
                                                new KeyValuePair<string, string>("userSysNo",user.UserSysNo.ToString()),
                                                new KeyValuePair<string, string>("userName", user.UserName),
                                                new KeyValuePair<string, string>("specialtyName", user.SpecialtyName),
                                                new KeyValuePair<string, string>("departmentName", department.DepartmentName)
                                        };
                    userNode.AdditionalAttributes.AddRange(customerAttributesList);
                    departmentNode.AppendChild(userNode);
                });
            });
        }

        /// <summary>
        /// 初始化多选模式
        /// </summary>
        /// <param name="treeDepartmentList"></param>
        private void InitCheckBoxDialog(List<TreeDepartmentEntity> treeDepartmentList)
        {
            ASTreeViewNode rootNode = new ASTreeViewNode("天正生产经营管理系统");

            this.astvMyTree.RootNode.AppendChild(rootNode);

            treeDepartmentList.ForEach((department) =>
            {
                var departmentNode = new ASTreeViewNode(department.DepartmentName, department.DepartmentSysNo.ToString(), folderIcon);
                departmentNode.OpenState = ASTreeViewNodeOpenState.Close;
                rootNode.AppendChild(departmentNode);

                department.Users.ForEach((user) =>
                {
                    ASTreeViewNode userNode = new ASTreeViewNode(user.UserName, user.UserSysNo.ToString(), fileIcon);


                    List<KeyValuePair<string, string>> customerAttributesList = new List<KeyValuePair<string, string>>
                                        {
                                                new KeyValuePair<string, string>("lastLeveFlag","true"),
                                                new KeyValuePair<string, string>("userSysNo",user.UserSysNo.ToString()),
                                                new KeyValuePair<string, string>("userName", user.UserName),
                                                new KeyValuePair<string, string>("specialtyName", user.SpecialtyName),
                                                new KeyValuePair<string, string>("departmentName", department.DepartmentName)
                                        };
                    userNode.AdditionalAttributes.AddRange(customerAttributesList);
                    departmentNode.AppendChild(userNode);
                });
            });
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.astvMyTree.Theme = macOS;
            this.astvMyTree.Theme = macOS;
        }
    }
}