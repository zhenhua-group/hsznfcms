﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Web.Script.Serialization;
using TG.Model;
using System.Data;
using TG.BLL;

namespace TG.Web.UserControl
{
    public partial class ChooseCustomer : System.Web.UI.UserControl
    {
        //分页
        public int PageSize { get; set; }
        //用户ID
        public int UserSysNo { get; set; }
        //权限
        public int PreviewPower { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ChooseCustomer));
            if (!IsPostBack)
            {
                BindCustomerType();
            }
        }
        //加载客户类型
        protected void BindCustomerType()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定客户类别
            string str_where = "dic_type='cst_type'";
            DataSet ds_cst = dic.GetList(str_where);
            customerLevel.DataValueField = "ID";
            customerLevel.DataTextField = "dic_name";
            customerLevel.DataSource = ds_cst;
            customerLevel.DataBind();
        }
        [AjaxMethod]
        public string SearchCustomerRecord(string queryObjectString)
        {
            ChooseCustomerQueryEntity queryEntity = JavaScriptDeserializer.DeserializeFromJson<ChooseCustomerQueryEntity>(queryObjectString);

            ResultList<CustomerViewEntity> resultList = new TG.BLL.ChooseCustomerBP().GetCustomerRecordList(queryEntity);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);

            return json;
        }
    }
}