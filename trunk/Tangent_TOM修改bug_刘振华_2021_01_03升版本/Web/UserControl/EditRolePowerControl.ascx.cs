﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;
using AjaxPro;
using Newtonsoft.Json;

namespace TG.Web.UserControl
{
    public partial class EditRolePowerControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(EditRolePowerControl));

            if (!IsPostBack)
            {

            }
        }
        //绑定功能查看权限列表
        [AjaxMethod]
        public string BindEditRolePowerControl(int roleSysNo)
        {
            List<RolePowerListViewEntity> resultList = new RolePowerBP().GetRolePowerListViewEntity(roleSysNo);
            return Newtonsoft.Json.JsonConvert.SerializeObject(resultList);
        }
        //保存功能权限列表
        [AjaxMethod]
        public string SaveRolePowerEntityList(string rolePowerEntityListString)
        {
            List<RolePowerViewEntity> rolePowerEntityList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RolePowerViewEntity>>(rolePowerEntityListString);

            return new RolePowerBP().SaveRolePowerViewEntity(rolePowerEntityList) + "";
        }
    }
}