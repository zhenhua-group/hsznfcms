﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserOfTheDepartmentTree.ascx.cs"
        Inherits="TG.Web.UserControl.UserOfTheDepartmentTree" %>
<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="ct" %>
<link href="<%=ResolveUrl("~/js/astreeview/astreeview/astreeview.css")%>" type="text/css"
        rel="stylesheet" />
<link href="<%=ResolveUrl("~/js/astreeview/contextmenu/contextmenu.css")%>" type="text/css"
        rel="stylesheet" />
<div id="ChooseUserOfTheDepartmentMain">
        <ct:ASTreeView ID="astvMyTree" runat="server" BasePath="~/js/astreeview/astreeview/"
                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false"
                EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="true"
                EnableCustomizedNodeIcon="true" EnableContextMenu="true" EnableDebugMode="false"
                EnableContextMenuAdd="false" OnNodeDragAndDropCompletingScript="dndCompletingHandler( elem, newParent )"
                OnNodeDragAndDropCompletedScript="dndCompletedHandler( elem, newParent )"
                OnNodeDragAndDropStartScript="dndStartHandler( elem )" EnableMultiLineEdit="false"
                EnableEscapeInput="false" EnableTheme="true" />
        <input type="hidden" id="IsRadio" value="<%=IsRadio %>" />
</div>

<script src="<%=ResolveUrl("~/js/astreeview/astreeview/astreeview_packed.js")%>"
        type="text/javascript"></script>

<script src="<%=ResolveUrl("~/js/astreeview/contextmenu/contextmenu_packed.js")%>"
        type="text/javascript"></script>

