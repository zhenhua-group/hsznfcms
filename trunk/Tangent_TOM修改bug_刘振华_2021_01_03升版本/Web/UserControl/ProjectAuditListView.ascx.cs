﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using AjaxPro;
using TG.Model;
using System.Collections.Generic;
using TG.BLL;
using Newtonsoft.Json;
using System.Linq;


namespace TG.Web.UserControl.ProjectAudit
{
    public partial class ProjectAuditListView : ControlBase
    {
        public List<List<ProjectAuditListViewDataEntity>> SourceList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjectAuditListView));
        }

        [AjaxMethod]
        public string GetProjectAuditList(string queryString)
        {
            SysMsgQueryEntity queryEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<SysMsgQueryEntity>(queryString);

            ResultEntityList<ProjectAuditListViewDataEntity> projectAuditListViewDataEntityList = new ResultEntityList<ProjectAuditListViewDataEntity>(15) { };

            int dataCount = 0;

            projectAuditListViewDataEntityList.Body = new TG.BLL.cm_SysMsg().GetProjectAuditListViewEntity(UserSysNo, 15, queryEntity.PageCurrent, queryEntity.ProjectAuditStatus, out dataCount);
            projectAuditListViewDataEntityList.TotalCount = dataCount;

            return Newtonsoft.Json.JsonConvert.SerializeObject(projectAuditListViewDataEntityList);
        }
    }
}