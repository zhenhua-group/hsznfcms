﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseProjectValueUser.ascx.cs"
    Inherits="TG.Web.UserControl.ChooseProjectValueUser" %>
<link href="/css/UserControl/ChooseUser.css" rel="stylesheet" />
<style type="text/css">
    .style1 {
        width: 25%;
        height: 20px;
    }

    .style2 {
        width: 40%;
        height: 20px;
    }

    .style3 {
        width: 10%;
        height: 20px;
    }
</style>
<div id="chooseUserMainDivContainer">

    <table style="width: 100%;" class="mainTable">
        <tr>
            <td>
                <!--QueryConditionTable-->
                <table style="width: 100%;" class="show_project" id="tbSelect">
                    <tr>
                        <td>姓名或登录名：
                        </td>
                        <td>
                            <input type="text" id="chooseUserOfNameTextBox" class="textBox" />
                        </td>
                        <td>选择部门：
                        </td>
                        <td>
                            <select id="chooseUserDepartmentDropDownList">
                                <option value="0">----请选择----</option>
                                <asp:Repeater ID="RepeaterDepartment" runat="server">
                                    <ItemTemplate>
                                        <option value="<%#Eval("unit_ID") %>">
                                            <%#Eval("unit_Name") %></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>专业：
                        </td>
                        <td>
                            <select id="chooseSpeName" style="width: 145px;">
                                <asp:Repeater ID="RepeaterSpe" runat="server">
                                    <ItemTemplate>
                                        <option value="<%#Eval("spe_ID") %>">
                                            <%#Eval("spe_Name")%></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>
                        </td>
                        <td>角色：
                        </td>
                        <td>
                            <input id="chkDesign" type="checkbox" checked="checked" />设计
                                <input id="chkHead" type="checkbox" />专业负责
                                <input id="chkAudit" type="checkbox" />审核
                                <input id="chkProof" type="checkbox" />校对
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center;">
                            <input type="button" id="chooseUserSearchButton" class="btn blue btn-sm" value="查询" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <!--ResultTable-->
                <table style="width: 100%;" id="chooseUserResultTable" class="resultTable">
                    <tr>
                        <td align="center" class="style3">
                            <input type="checkbox" id="selectAllCheckBox" />
                        </td>
                        <td align="center" class="style1">姓名
                        </td>
                        <td align="center" class="style2">部门
                        </td>
                        <td align="center" class="style1">专业
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>

</div>
