﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model.UserControl;
using Newtonsoft.Json;

namespace TG.Web.UserControl
{
    public partial class AddExternalMember : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AddExternalMember));
            if (!IsPostBack)
            {
                BindSpec();
                BindDepartment();
            }
        }

        [AjaxMethod]
        public string GetUsers(string queryString)
        {
            ChooseExternalUserEntity queryEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ChooseExternalUserEntity>(queryString);

            int totalCount = 0;

            List<ChooseExternalUserViewEntity> resultList = new TG.BLL.ChooseUserBP().GetUserList(queryEntity, out totalCount);

            object[] resultArray = new object[2];

            resultArray[0] = totalCount;
            resultArray[1] = resultList;

            return Newtonsoft.Json.JsonConvert.SerializeObject(resultArray);
        }

        [AjaxMethod]
        public int AddExternalUser(string jsonObj)
        {
            AddExternalUser planProject = Newtonsoft.Json.JsonConvert.DeserializeObject<AddExternalUser>(jsonObj);
            return new TG.BLL.ChooseUserBP().AddExternalUserInfo(planProject);
        }
        
        //绑定专业
        protected void BindSpec()
        {
            this.RepeaterSpe.DataSource = new TG.BLL.tg_speciality().GetList("");
            this.RepeaterSpe.DataBind();
            this.rpSpe.DataSource = new TG.BLL.tg_speciality().GetList("");
            this.rpSpe.DataBind();
        }

        /// 绑定部门列表
        /// </summary>
        private void BindDepartment()
        {
            this.RepeaterDepartment.DataSource = new TG.BLL.tg_unit().GetList("");
            this.RepeaterDepartment.DataBind();
            this.rpUnit.DataSource = new TG.BLL.tg_unit().GetList("");
            this.rpUnit.DataBind();
        }
    }
}