﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="AddResearchProjectBymaster.aspx.cs" Inherits="TG.Web.ThechnicalQuality.AddResearchProjectBymaster" %>

<%@ Register Src="../UserControl/ChooseUser.ascx" TagName="ChooseUser" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_proj.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>

    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script src="../js/ThechnicalQuality/ResearchProject/AddResearchProjectBymaster.js"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>
    <style type="text/css">
        input[readonly] {
            background-image: #FFC;
        }
    </style>
    <script type="text/javascript">
        var hid_researchid = '<%=GetResearchProjectID() %>';
        //用户ID
        var userid = '<%=UserSysNo %>';
        if (userid == "0") {
            window.parent.parent.document.location.href = "../index.html";
        }
        if (cprid != "0") {
            var swfu;
            window.onload = function () {
                swfu = new SWFUpload({

                    upload_url: "../ProcessUpload/upload_research.aspx?type=research&id=" + hid_researchid + "&userid=" + userid,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                    file_types_description: "上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccessShowResult,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_style: '{background-color:#d8d8d8 }',
                    button_width: 61,
                    button_height: 22, //<span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder"><i class="fa fa-paper-clip"></i>选择文件</span> </span>
                    button_text: '<span class="fileupload-new">选择文件</span>',
                    button_text_style: '.fileupload-new {background-color:#d8d8d8 ;} ',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,
                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">技术质量 <small>科研项目添加</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">技术质量</a><i class="fa fa-angle-right"> </i><a href="#">科研项目管理</a><i
        class="fa fa-angle-right"> </i><a href="#">科研项目录入</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>科研项目录入
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">科研项目信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td colspan="4">
                                            <span style="color: red;">提示:黄色背景文本框为必填项！<i class="fa fa-warning"></i></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px;">项目编号:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" id="txt_proNumber" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />

                                            </div>
                                        </td>
                                        <td style="width: 120px;">实际开始日期:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" name="txt_actualStartDate" id="Text2" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px;">项目名称:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" id="txt_proName" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                                <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                            </div>
                                        </td>
                                        <td style="width: 120px;">实际完成日期:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" name="txt_ActualEndDate" id="Text3" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目负责人:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" id="txt_PMName" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" readonly />
                                                <asp:HiddenField ID="hid_pmname" runat="server" Value="0" />
                                                <asp:HiddenField ID="hid_pmuserid" runat="server" Value="0" />
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#ZXSZ" id="sch_PM" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                        <td>批准日期 :
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" name="txt_approvalDate" id="Text4" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目负责部门:
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <input type="text" id="txt_unit" runat="server" class="form-control input-sm"
                                                        style="background-color: #FFC;" readonly />
                                                    <asp:HiddenField ID="hid_unit" runat="server" Value="" />
                                                </div>
                                                <div class="col-md-3">
                                                    <a id="sch_unit" href="#CJBM" class="btn blue btn-sm"
                                                        data-toggle="modal"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>项目状态:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="drp_proStatus" runat="server" Width="120px" AppendDataBoundItems="true" CssClass="form-control"
                                                    Style="background-color: #FFC;">
                                                    <asp:ListItem Value="-1">---选择类别---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>预计开始时间:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" name="txt_expectStartDate" id="txt_startdate" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="background-color: #FFC; width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                            </div>
                                        </td>
                                        <td>预计完成时间:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" name="txt_expectEndDate" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="background-color: #FFC; width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目类型:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="drp_proType" runat="server" Width="120px" AppendDataBoundItems="true" CssClass="form-control"
                                                    Style="background-color: #FFC;">
                                                    <asp:ListItem Value="-1">---选择类别---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>协作单位(内部):
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <input type="text" id="txt_internalUnit" runat="server" class="form-control input-sm"
                                                        style="background-color: #FFC;" readonly />
                                                    <asp:HiddenField ID="hidinternalUnitID" runat="server" Value="" />
                                                </div>
                                                <div class="col-md-3">
                                                    <a id="sch_unitMultiple" href="#CJBMMultiple" class="btn blue btn-sm"
                                                        data-toggle="modal"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>项目分类:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="drp_classify" runat="server" Width="120px" AppendDataBoundItems="true" CssClass="form-control"
                                                    Style="background-color: #FFC;">
                                                    <asp:ListItem Value="-1">---选择类别---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>委托单位:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" id="txt_client" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>协作单位(外部):
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_outsideUnit" rows="3" runat="Server"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>课题现状和重要性简介:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_briefing" rows="3" runat="Server"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>课题目标:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_target" rows="3" runat="Server"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>课题内容:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_content" rows="3" runat="Server" placeholder="Enter a message ..."
                                                    maxlength="500" style="background-color: #FFC;"> </textarea>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>项目备注:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_remark" rows="3" runat="Server"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <h4 class="form-section">
                        <div class="row">
                            <div class="col-md-2">
                                立项申请整体数据
                            </div>
                            <div class="col-md-8">
                                <div id="divFileProgressContainer" style="float: right;">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder">
                                    <i class="fa fa-paper-clip"></i>选择文件</span> </span>
                            </div>
                        </div>
                    </h4>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed flip-contentr" id="datas_att" style="width: 98%;" align="center">
                                        <thead class="flip-content">
                                            <tr id="att_row">
                                                <td style="width: 40px;" align="center" id="att_id">序号
                                                </td>
                                                <td style="width: 300px;" align="center" id="att_filename">文件名称
                                                </td>
                                                <td style="width: 80px" align="center" id="att_filesize">文件大小
                                                </td>
                                                <td style="width: 80px;" align="center" id="att_filetype">文件类型
                                                </td>
                                                <td style="width: 120px;" align="center" id="att_uptime">上传时间
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper">&nbsp;
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper2">&nbsp;
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <h4 class="form-section">
                        <div class="row">
                            <div class="col-md-10">
                                经费及费用情况
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </h4>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">

                                <table class="table table-bordered table-striped table-condensed flip-contentr" style="width: 98%;" align="center">

                                    <tr>
                                        <td style="width: 120px;">申请项目经费:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="txt_appFunds" runat="server" class="form-control"
                                                        style="background-color: #FFC; width: 80%;" />（元）
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 120px;">批准项目经费:
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="txt_auditFunds" runat="server" class="form-control"
                                                    style="width: 80%;" />（元）
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>已拨付自筹经费:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="txt_selfFunds" runat="server" class="form-control"
                                                        style="width: 80%;" />（元）
                                                </div>
                                            </div>
                                        </td>
                                        <td>已拨付外部提供经费:
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="txt_outsideFunds" runat="server" class="form-control"
                                                    style="width: 80%;" />（元）
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>已拨付总经费:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="txt_totalFunds" runat="server" class="form-control"
                                                        style="width: 80%;" />（元）
                                                </div>
                                            </div>
                                        </td>
                                        <td>未到位总经费:
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="txt_notFunds" runat="server" class="form-control"
                                                    style="width: 80%;" />（元）
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>已支出费用:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="txt_payFunds" runat="server" class="form-control"
                                                        style="width: 80%;" />（元）
                                                </div>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>经费说明:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_fundsRemark" rows="3" runat="Server" placeholder="Enter a message ..."
                                                    maxlength="500"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>支出经费说明:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_notFundsRemark" rows="3" runat="Server" placeholder="Enter a message ..."
                                                    maxlength="500"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="submit" class="btn green" id="btn_Save">
                                保存</button>
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <input id="hidproId" type="hidden" runat="server" />
    <asp:HiddenField ID="hid_researchid" runat="server" />
    <!-- 弹出框 -->

    <!--承接部门-->
    <div id="CJBM" class="modal
    fade yellow"
        tabindex="-1" data-width="400" aria-hidden="true" style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">项目负责部门</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_cjbmDiv">
                <div class="row">
                    <div class="col-md-12">
                        <table id="cpr_cjbmTable" class="table table-bordered table-striped table-condensed flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td style="width: 60px;">序号
                                </td>
                                <td style="width: 340px;">单位名称
                                </td>
                                <td style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="cjbmByPageDiv" class="divNavigation  pageDivPosition">
                            总<label id="cjbm_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="cjbm_nowPageIndex">0</label>/<label id="cjbm_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="cjbm_firstPage">首页</span>&nbsp; <span id="cjbm_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="cjbm_nextPage">&gt;&gt;</span>&nbsp; <span id="cjbm_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="cjbm_pageIndex" style="height: 3; width: 20px; border-style: hidden; border-bottom-style: solid;" />页&nbsp;&nbsp; <span id="cjbm_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--承接部门（内部可以多选)-->
    <div id="CJBMMultiple" class="modal
    fade yellow"
        tabindex="-1" data-width="400" aria-hidden="true" style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">协作单位(内部)</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_cjbmDivMultiple">
                <div class="row">
                    <div class="col-md-12">
                        <table id="cpr_cjbmTableMultiple" class="table table-bordered table-striped table-condensed flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td style="width: 60px;">序号
                                </td>
                                <td style="width: 340px;">单位名称
                                </td>
                                <td style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="cjbmByPageDivMultiple" class="divNavigation  pageDivPosition">
                            总<label id="cjbm_allDataCountMultiple" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="cjbm_nowPageIndexMultiple">0</label>/<label id="cjbm_allPageCountMultiple">0</label>页&nbsp;&nbsp;
                            <span id="cjbm_firstPageMultiple">首页</span>&nbsp; <span id="cjbm_prevPageMultiple">&lt;&lt;</span>&nbsp;
                            <span id="cjbm_nextPageMultiple">&gt;&gt;</span>&nbsp; <span id="cjbm_lastPageMultiple">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="cjbm_pageIndexMultiple" style="height: 3; width: 20px; border-style: hidden; border-bottom-style: solid;" />页&nbsp;&nbsp; <span id="Span5">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--甲方负责人-->
    
    <!--执行设总-->
    <div id="ZXSZ" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择项目负责人</h4>
        </div>
        <div class="modal-body">
            <div id="chooseUserMain">
                <uc1:ChooseUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_user_close" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>

</asp:Content>
