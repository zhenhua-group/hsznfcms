﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ThechnicalQuality
{
    public partial class AddResearchProjectBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        //返回一个文件上传的随机ID
        public string GetResearchProjectID()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }

        //添加附件
        protected void AddCoperationAttach(string cprid)
        {
            string strSql = " UPDATE [cm_AttachInfo] SET [Cpr_Id] =" + cprid + " WHERE [Temp_No] = '" + this.hid_researchid.Value + "' AND UploadUser='" + UserSysNo + "' AND OwnType='research'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);

        }
    }
}