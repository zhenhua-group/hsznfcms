﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace TG.Web
{
    public class ControlBase : System.Web.UI.UserControl
    {
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        /// <summary>
        /// 用户登录系统自增号
        /// </summary>
        protected int UserSysNo
        {
            get
            {
                object objUserSysNo = this.UserInfo["memid"];
                return objUserSysNo == null ? 0 : Convert.ToInt32(objUserSysNo);
            }
        }

        /// <summary>
        /// 登录用户简称
        /// </summary>
        protected string UserShortName
        {
            get
            {
                object objUserShortName = this.UserInfo["memlogin"];
                return objUserShortName == null ? "" : objUserShortName.ToString();
            }
        }
    }
}
