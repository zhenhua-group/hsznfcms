﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using System.Text;
using AjaxPro;

namespace TG.Web.ProjectPlan
{
    public partial class ProPlanAddBymaster : PageBase
    {
        public int FlagBackType
        {
            get
            {
                int iflag = 0;
                int.TryParse(Request["flag"] ?? "0", out iflag);
                return iflag;
            }
        }
        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo
        {
            get
            {
                int projectSysNo = 0;
                int.TryParse(Request["projectSysNo"], out projectSysNo);
                return projectSysNo;
                //return 2;
            }
        }
        public string HiddenisAudit { get; set; }
        /// <summary>
        /// 关联的合同名称
        /// </summary>
        public string CoperationName
        {
            get
            {
                return Request["CoperationName"];
            }
        }

        public string IsEdit
        {
            get
            {
                return Request["Action"];
            }
        }

        public int ProjectPlanAuditSysNo
        {
            get
            {
                int projectPlanAuditSysNo = 0;
                int.TryParse(Request["projectPlanAuditSysNo"], out projectPlanAuditSysNo);
                return projectPlanAuditSysNo;
            }
        }

        public string JobNumber
        {
            get
            {
                return Request["JobNumber"];
            }
        }

        public TG.Model.cm_Project Project { get; set; }

        public List<ProjectPlanRole> ProjectPlanRoleList { get; set; }

        public ProjectDesignPlanRole ProjectDesignPlanRoleList { get; set; }

        public string AuditHTML { get; set; }

        public string AuditStatus { get; set; }

        /// <summary>
        /// 是否存在审核记录
        /// </summary>
        public string HasAudit { get; set; }
        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProPlanAddBymaster));
            if (ProjectSysNo > 0)
            {
                GetProInfo(ProjectSysNo.ToString());
                switch (IsEdit)
                {
                    case "3":
                        GetUserList();
                        GetProjectPlanSubItemList();
                        break;
                }
            }

            if (!IsPostBack)
            {
                //获取有关策划的消息
                GetMsgID();
                //返回地址
                GetPageBackUrl();
                HiddenisAudit = GetHiddenISAudit();
                //绑定已经写过的资料卡
                BindZLK();
            }
        }

        private void BindZLK()
        {
            string sqlBind = @"SELECT item_val FROM tg_probill WHERE bill_title='2' AND pro_id=" + Project.ReferenceSysNo;
            object ProObjBii = TG.DBUtility.DbHelperSQL.GetSingle(sqlBind);
            if (ProObjBii != null)
            {
                string arrayprobii = ProObjBii.ToString().Split(new char[] { '=' }, 3)[2];
                string[] arrayprobiichild = arrayprobii.Split('\r');

                if (0 < arrayprobiichild.Length)
                {
                    this.BM_3149_0_0.Value = arrayprobiichild[0].Split('\n')[0];
                    this.BM_3149_0_1.Value = arrayprobiichild[0].Split('\n')[1];
                    this.BM_3149_0_2.Value = arrayprobiichild[0].Split('\n')[2];
                    this.BM_3149_0_3.Value = arrayprobiichild[0].Split('\n')[3];
                    this.BM_3149_0_4.Value = arrayprobiichild[0].Split('\n')[4];
                    this.BM_3149_0_5.Value = arrayprobiichild[0].Split('\n')[5];
                    this.BM_3149_0_6.Value = arrayprobiichild[0].Split('\n')[6];
                    this.BM_3149_0_7.Value = arrayprobiichild[0].Split('\n')[7];
                    this.BM_3149_0_8.Value = arrayprobiichild[0].Split('\n')[8];
                    this.BM_3149_0_9.Value = arrayprobiichild[0].Split('\n')[9];
                    this.BM_3149_0_10.Value = arrayprobiichild[0].Split('\n')[10];
                    this.BM_3149_0_11.Value = arrayprobiichild[0].Split('\n')[11];
                }
                if (1 < arrayprobiichild.Length)
                {
                    this.BM_3149_1_0.Value = arrayprobiichild[1].Split('\n')[1];
                    this.BM_3149_1_1.Value = arrayprobiichild[1].Split('\n')[2];
                    this.BM_3149_1_2.Value = arrayprobiichild[1].Split('\n')[3];
                    this.BM_3149_1_3.Value = arrayprobiichild[1].Split('\n')[4];
                    this.BM_3149_1_4.Value = arrayprobiichild[1].Split('\n')[5];
                    this.BM_3149_1_5.Value = arrayprobiichild[1].Split('\n')[6];
                    this.BM_3149_1_6.Value = arrayprobiichild[1].Split('\n')[7];
                    this.BM_3149_1_7.Value = arrayprobiichild[1].Split('\n')[8];
                    this.BM_3149_1_8.Value = arrayprobiichild[1].Split('\n')[9];
                    this.BM_3149_1_9.Value = arrayprobiichild[1].Split('\n')[10];
                    this.BM_3149_1_10.Value = arrayprobiichild[1].Split('\n')[11];
                    this.BM_3149_1_11.Value = arrayprobiichild[1].Split('\n')[12];
                }
                if (2 < arrayprobiichild.Length)
                {
                    this.BM_3149_2_0.Value = arrayprobiichild[2].Split('\n')[1];
                    this.BM_3149_2_1.Value = arrayprobiichild[2].Split('\n')[2];
                    this.BM_3149_2_2.Value = arrayprobiichild[2].Split('\n')[3];
                    this.BM_3149_2_3.Value = arrayprobiichild[2].Split('\n')[4];
                    this.BM_3149_2_4.Value = arrayprobiichild[2].Split('\n')[5];
                    this.BM_3149_2_5.Value = arrayprobiichild[2].Split('\n')[6];
                    this.BM_3149_2_6.Value = arrayprobiichild[2].Split('\n')[7];
                    this.BM_3149_2_7.Value = arrayprobiichild[2].Split('\n')[8];
                    this.BM_3149_2_8.Value = arrayprobiichild[2].Split('\n')[9];
                    this.BM_3149_2_9.Value = arrayprobiichild[2].Split('\n')[10];
                    this.BM_3149_2_10.Value = arrayprobiichild[2].Split('\n')[11];
                    this.BM_3149_2_11.Value = arrayprobiichild[2].Split('\n')[12];
                }
                if (3 < arrayprobiichild.Length)
                {
                    this.BM_3149_3_0.Value = arrayprobiichild[3].Split('\n')[1];
                    this.BM_3149_3_1.Value = arrayprobiichild[3].Split('\n')[2];
                    this.BM_3149_3_2.Value = arrayprobiichild[3].Split('\n')[3];
                    this.BM_3149_3_3.Value = arrayprobiichild[3].Split('\n')[4];
                    this.BM_3149_3_4.Value = arrayprobiichild[3].Split('\n')[5];
                    this.BM_3149_3_5.Value = arrayprobiichild[3].Split('\n')[6];
                    this.BM_3149_3_6.Value = arrayprobiichild[3].Split('\n')[7];
                    this.BM_3149_3_7.Value = arrayprobiichild[3].Split('\n')[8];
                    this.BM_3149_3_8.Value = arrayprobiichild[3].Split('\n')[9];
                    this.BM_3149_3_9.Value = arrayprobiichild[3].Split('\n')[10];
                    this.BM_3149_3_10.Value = arrayprobiichild[3].Split('\n')[11];
                    this.BM_3149_3_11.Value = arrayprobiichild[3].Split('\n')[12];
                }
                if (4 < arrayprobiichild.Length)
                {
                    this.BM_3149_4_0.Value = arrayprobiichild[4].Split('\n')[1];
                    this.BM_3149_4_1.Value = arrayprobiichild[4].Split('\n')[2];
                    this.BM_3149_4_2.Value = arrayprobiichild[4].Split('\n')[3];
                    this.BM_3149_4_3.Value = arrayprobiichild[4].Split('\n')[4];
                    this.BM_3149_4_4.Value = arrayprobiichild[4].Split('\n')[5];
                    this.BM_3149_4_5.Value = arrayprobiichild[4].Split('\n')[6];
                    this.BM_3149_4_6.Value = arrayprobiichild[4].Split('\n')[7];
                    this.BM_3149_4_7.Value = arrayprobiichild[4].Split('\n')[8];
                    this.BM_3149_4_8.Value = arrayprobiichild[4].Split('\n')[9];
                    this.BM_3149_4_9.Value = arrayprobiichild[4].Split('\n')[10];
                    this.BM_3149_4_10.Value = arrayprobiichild[4].Split('\n')[11];
                    this.BM_3149_4_11.Value = arrayprobiichild[4].Split('\n')[12];
                }
                if (5 < arrayprobiichild.Length)
                {
                    this.BM_3149_5_0.Value = arrayprobiichild[5].Split('\n')[1];
                    this.BM_3149_5_1.Value = arrayprobiichild[5].Split('\n')[2];
                    this.BM_3149_5_2.Value = arrayprobiichild[5].Split('\n')[3];
                    this.BM_3149_5_3.Value = arrayprobiichild[5].Split('\n')[4];
                    this.BM_3149_5_4.Value = arrayprobiichild[5].Split('\n')[5];
                    this.BM_3149_5_5.Value = arrayprobiichild[5].Split('\n')[6];
                    this.BM_3149_5_6.Value = arrayprobiichild[5].Split('\n')[7];
                    this.BM_3149_5_7.Value = arrayprobiichild[5].Split('\n')[8];
                    this.BM_3149_5_8.Value = arrayprobiichild[5].Split('\n')[9];
                    this.BM_3149_5_9.Value = arrayprobiichild[5].Split('\n')[10];
                    this.BM_3149_5_10.Value = arrayprobiichild[5].Split('\n')[11];
                    this.BM_3149_5_11.Value = arrayprobiichild[5].Split('\n')[12];
                }
                if (6 < arrayprobiichild.Length)
                {
                    this.BM_3149_6_0.Value = arrayprobiichild[6].Split('\n')[1];
                    this.BM_3149_6_1.Value = arrayprobiichild[6].Split('\n')[2];
                    this.BM_3149_6_2.Value = arrayprobiichild[6].Split('\n')[3];
                    this.BM_3149_6_3.Value = arrayprobiichild[6].Split('\n')[4];
                    this.BM_3149_6_4.Value = arrayprobiichild[6].Split('\n')[5];
                    this.BM_3149_6_5.Value = arrayprobiichild[6].Split('\n')[6];
                    this.BM_3149_6_6.Value = arrayprobiichild[6].Split('\n')[7];
                    this.BM_3149_6_7.Value = arrayprobiichild[6].Split('\n')[8];
                    this.BM_3149_6_8.Value = arrayprobiichild[6].Split('\n')[9];
                    this.BM_3149_6_9.Value = arrayprobiichild[6].Split('\n')[10];
                    this.BM_3149_6_10.Value = arrayprobiichild[6].Split('\n')[11];
                    this.BM_3149_6_11.Value = arrayprobiichild[6].Split('\n')[12];
                }
                if (7 < arrayprobiichild.Length)
                {
                    this.BM_3149_7_0.Value = arrayprobiichild[7].Split('\n')[1];
                    this.BM_3149_7_1.Value = arrayprobiichild[7].Split('\n')[2];
                    this.BM_3149_7_2.Value = arrayprobiichild[7].Split('\n')[3];
                    this.BM_3149_7_3.Value = arrayprobiichild[7].Split('\n')[4];
                    this.BM_3149_7_4.Value = arrayprobiichild[7].Split('\n')[5];
                    this.BM_3149_7_5.Value = arrayprobiichild[7].Split('\n')[6];
                    this.BM_3149_7_6.Value = arrayprobiichild[7].Split('\n')[7];
                    this.BM_3149_7_7.Value = arrayprobiichild[7].Split('\n')[8];
                    this.BM_3149_7_8.Value = arrayprobiichild[7].Split('\n')[9];
                    this.BM_3149_7_9.Value = arrayprobiichild[7].Split('\n')[10];
                    this.BM_3149_7_10.Value = arrayprobiichild[7].Split('\n')[11];
                    this.BM_3149_7_11.Value = arrayprobiichild[7].Split('\n')[12];
                }
                if (8 < arrayprobiichild.Length)
                {
                    this.BM_3149_8_0.Value = arrayprobiichild[8].Split('\n')[1];
                    this.BM_3149_8_1.Value = arrayprobiichild[8].Split('\n')[2];
                    this.BM_3149_8_2.Value = arrayprobiichild[8].Split('\n')[3];
                    this.BM_3149_8_3.Value = arrayprobiichild[8].Split('\n')[4];
                    this.BM_3149_8_4.Value = arrayprobiichild[8].Split('\n')[5];
                    this.BM_3149_8_5.Value = arrayprobiichild[8].Split('\n')[6];
                    this.BM_3149_8_6.Value = arrayprobiichild[8].Split('\n')[7];
                    this.BM_3149_8_7.Value = arrayprobiichild[8].Split('\n')[8];
                    this.BM_3149_8_8.Value = arrayprobiichild[8].Split('\n')[9];
                    this.BM_3149_8_9.Value = arrayprobiichild[8].Split('\n')[10];
                    this.BM_3149_8_10.Value = arrayprobiichild[8].Split('\n')[11];
                    this.BM_3149_8_11.Value = arrayprobiichild[8].Split('\n')[12];
                }
                if (9 < arrayprobiichild.Length)
                {
                    this.BM_3149_9_0.Value = arrayprobiichild[9].Split('\n')[1];
                    this.BM_3149_9_1.Value = arrayprobiichild[9].Split('\n')[2];
                    this.BM_3149_9_2.Value = arrayprobiichild[9].Split('\n')[3];
                    this.BM_3149_9_3.Value = arrayprobiichild[9].Split('\n')[4];
                    this.BM_3149_9_4.Value = arrayprobiichild[9].Split('\n')[5];
                    this.BM_3149_9_5.Value = arrayprobiichild[9].Split('\n')[6];
                    this.BM_3149_9_6.Value = arrayprobiichild[9].Split('\n')[7];
                    this.BM_3149_9_7.Value = arrayprobiichild[9].Split('\n')[8];
                    this.BM_3149_9_8.Value = arrayprobiichild[9].Split('\n')[9];
                    this.BM_3149_9_9.Value = arrayprobiichild[9].Split('\n')[10];
                    this.BM_3149_9_10.Value = arrayprobiichild[9].Split('\n')[11];
                    this.BM_3149_9_11.Value = arrayprobiichild[9].Split('\n')[12];
                }
                if (10 < arrayprobiichild.Length)
                {
                    this.BM_3149_10_0.Value = arrayprobiichild[10].Split('\n')[1];
                    this.BM_3149_10_1.Value = arrayprobiichild[10].Split('\n')[2];
                    this.BM_3149_10_2.Value = arrayprobiichild[10].Split('\n')[3];
                    this.BM_3149_10_3.Value = arrayprobiichild[10].Split('\n')[4];
                    this.BM_3149_10_4.Value = arrayprobiichild[10].Split('\n')[5];
                    this.BM_3149_10_5.Value = arrayprobiichild[10].Split('\n')[6];
                    this.BM_3149_10_6.Value = arrayprobiichild[10].Split('\n')[7];
                    this.BM_3149_10_7.Value = arrayprobiichild[10].Split('\n')[8];
                    this.BM_3149_10_8.Value = arrayprobiichild[10].Split('\n')[9];
                    this.BM_3149_10_9.Value = arrayprobiichild[10].Split('\n')[10];
                    this.BM_3149_10_10.Value = arrayprobiichild[10].Split('\n')[11];
                    this.BM_3149_10_11.Value = arrayprobiichild[10].Split('\n')[12];
                }
                if (11 < arrayprobiichild.Length)
                {
                    this.BM_3149_11_0.Value = arrayprobiichild[11].Split('\n')[1];
                    this.BM_3149_11_1.Value = arrayprobiichild[11].Split('\n')[2];
                    this.BM_3149_11_2.Value = arrayprobiichild[11].Split('\n')[3];
                    this.BM_3149_11_3.Value = arrayprobiichild[11].Split('\n')[4];
                    this.BM_3149_11_4.Value = arrayprobiichild[11].Split('\n')[5];
                    this.BM_3149_11_5.Value = arrayprobiichild[11].Split('\n')[6];
                    this.BM_3149_11_6.Value = arrayprobiichild[11].Split('\n')[7];
                    this.BM_3149_11_7.Value = arrayprobiichild[11].Split('\n')[8];
                    this.BM_3149_11_8.Value = arrayprobiichild[11].Split('\n')[9];
                    this.BM_3149_11_9.Value = arrayprobiichild[11].Split('\n')[10];
                    this.BM_3149_11_10.Value = arrayprobiichild[11].Split('\n')[11];
                    this.BM_3149_11_11.Value = arrayprobiichild[11].Split('\n')[12];
                }

            }
            //throw new NotImplementedException();
        }

        private string GetHiddenISAudit()
        {
            string sql = @"
SELECT * FROM cm_ProjectPlanAudit WHERE ProjectSysNo=" + ProjectSysNo;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            if (o == null)
            {
                return "0";
            }
            else
            {
                return "1";
            }

            //throw new NotImplementedException();
        }
        //返回的url
        protected void GetPageBackUrl()
        {
            if (FlagBackType == 0)
            {
                this.hid_backurl.Value = "ProPlanListBymaster.aspx";
            }
            else
            {
                TG.Model.cm_Project proj_model = new TG.BLL.cm_Project().GetModel(ProjectSysNo);
                //查询消息  
                string strSql = "Select SysNo From cm_SysMsg Where MsgType='7' AND ReferenceSysNo='" + ProjectSysNo + "'";
                string msgid = Convert.ToString(TG.DBUtility.DbHelperSQL.GetSingle(strSql));
                string backurl = "ProPlanListBymaster.aspx?projectSysNo" + ProjectSysNo + "&PMUserID=" + proj_model.PMUserID + "&MsgNo=" + msgid;
                this.hid_backurl.Value = backurl;
            }
        }
        public void GetProInfo(string strProId)
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            Project = pro_model;
            //项目名称
            this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
            //关联合同
            this.txt_reletive.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
            //管理级别
            string level = pro_model.pro_level.ToString();
            if (level.Trim() == "0")
            {
                this.lbl_level.Text = "院管";
            }
            else
            {
                this.lbl_level.Text = "所管";
            }
            //审核级别
            string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
            if (auditlevel.Trim() == "1,0")
            {
                this.lbl_auditlevel.Text = "院审";
            }
            else if (auditlevel.Trim() == "0,1")
            {
                this.lbl_auditlevel.Text = "所审";
            }
            else if (auditlevel.Trim() == "1,1")
            {
                this.lbl_auditlevel.Text = "院审,所审";
            }
            //建筑级别
            string BuildType = pro_model.BuildType.Trim();
            this.drp_buildtype.Text = BuildType;
            //建设单位
            this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
            //承接部门
            this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            //建设地点
            this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
            //建设规模
            this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
            //项目结构形式
            string StrStruct = pro_model.pro_StruType;
            if (!string.IsNullOrEmpty(StrStruct))
            {
                this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
            }
            //建筑分类
            string BuildStructType = pro_model.pro_kinds;
            if (!string.IsNullOrEmpty(BuildStructType))
            {
                this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
            }
            //显示项目阶段
            string str_jd = pro_model.pro_status;
            if (str_jd.IndexOf(',') > -1)
            {
                this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
            }
            //经济所
            string isotherprt = "";
            if (pro_model.ISTrunEconomy == "1")
            {
                isotherprt += "经济所,";
            }
            //暖通
            if (pro_model.ISHvac == "1")
            {
                isotherprt += "暖通热力所,";
            }
            //土建所
            if (pro_model.ISArch == "1")
            {
                isotherprt += "土建所,";
            }
            if (isotherprt.IndexOf(',') > -1)
            {
                this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
            }
            else
            {
                this.lbl_isotherprt.Text = "无";
            }
            //项目来源
            string pro_src = pro_model.Pro_src.ToString();

        }

        /// <summary>
        /// 保存项目策划信息
        /// </summary>
        /// <param name="jsonObj"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string SavePlanRecord(string jsonObj)
        {
            PlanProjectViewEntity planProject = Newtonsoft.Json.JsonConvert.DeserializeObject<PlanProjectViewEntity>(jsonObj);
            string sqlGet = "SELECT SysNo from cm_ProjectPlanEditRecord WHERE ProjectSysNo= " + planProject.ProjectSysNo;
            object istrue = TG.DBUtility.DbHelperSQL.GetSingle(sqlGet);
            if (istrue != null)
            {
                string sqlupdate = @"UPDATE dbo.cm_ProjectPlanAudit
                                    SET Status = 'F'
                                    WHERE ProjectSysNo = " + planProject.ProjectSysNo;
                TG.DBUtility.DbHelperSQL.GetSingle(sqlupdate);
            }
            return new TG.BLL.cm_ProjectPlanBP().InsertPlanUser(planProject).ToString();
        }

        [AjaxMethod]
        public string DeleteProjectPlanSubItem(string sysNo)
        {
            return new TG.BLL.cm_ProjectPlanBP().DeleteProjectPlanSubItem(int.Parse(sysNo)) + "";
        }

        [AjaxMethod]
        public string CheckAudit(int projectSysNo)
        {
            return new TG.BLL.cm_ProjectPlanBP().IsExistsProjectPlanAudit(projectSysNo) == true ? "1" : "0";
        }
        /// <summary>
        /// 判断是否有图纸信息
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string isTruePack(int projectSysNo, int UserSysNo)
        {
            string result = "0";
            string packID = new TG.BLL.cm_ProjectPlanBP().IsTruePack(projectSysNo, UserSysNo);
            if (!string.IsNullOrEmpty(packID))
            {
                result = "1";
            }
            return result;
        }
        public void GetUserList()
        {
            ProjectPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanRoleAndUsers(ProjectSysNo);
            //设计人员
            ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(ProjectSysNo);
            RepeaterAllUsers.DataSource = new TG.BLL.cm_ProjectPlanBP().GetPlanUsers(ProjectSysNo);
            RepeaterAllUsers.DataBind();
        }

        public void GetProjectPlanSubItemList()
        {
            RepeaterProjectPlanSubItem.DataSource = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanSubitemList(ProjectSysNo);
            RepeaterProjectPlanSubItem.DataBind();
        }

        //获取当前项目策划的消息ID
        public void GetMsgID()
        {
            string strSql = "Select SysNo From cm_SysMsg Where MsgType='8' AND ReferenceSysNo='" + ProjectSysNo + "'";
            string msgid = Convert.ToString(TG.DBUtility.DbHelperSQL.GetSingle(strSql));
            this.hid_msgid.Value = msgid;
        }
        /// <summary>
        /// 提交审批
        /// </summary>
        [AjaxMethod]
        public string SubmitApply(string queryString)
        {
            ProjectPlanAuditParamterEntity par = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanAuditParamterEntity>(queryString);

            int sysNo = 0;

            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
            //是否存在策划审批
            bool isExists = projectPlanBP.IsExistsProjectPlanAudit(par.ProjectSysNo);
            //项目实体
            TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(par.ProjectSysNo);

            string result = "0";
            //如果项目未策划
            if (!isExists)
            {
                sysNo = projectPlanBP.InsertProjectPlanAudit(new cm_ProjectPlanAuditEntity
                                {
                                    ProjectSysNo = par.ProjectSysNo,
                                    InUser = UserSysNo,
                                    Status = "A"
                                });

                TG.Model.cm_ProjectPlanAuditConfigEntity projectPlanAuditConfigEntity = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditConfigEntity(1);
                //实例化审批消息
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    //ReferenceSysNo = sysNo.ToString(),
                    ReferenceSysNo = string.Format("ProjectPlanAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                    FromUser = UserSysNo,
                    InUser = UserSysNo,
                    MsgType = 4,
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目策划审核"),
                    QueryCondition = project.pro_name,
                    IsDone = "A"
                };

                //声明
                string roleSysNo = string.Empty;
                string roleName = string.Empty;
                //第一审批阶段人员iD
                roleSysNo = projectPlanAuditConfigEntity.RoleSysNo.ToString();
                sysMessageViewEntity.ToRole = roleSysNo;
                //获取审批人列表人名
                roleName = CommonAudit.GetRoleName(int.Parse(roleSysNo), sysMessageViewEntity);
                if (!string.IsNullOrEmpty(roleName))
                {
                    result = roleName;
                }
            }
            else
            {
                result = "0";
            }
            return result;
        }


        /// <summary>
        /// 提交审批
        /// </summary>
        [AjaxMethod]
        public string SaveZLK(string propid, string queryString)
        {
            string sql = @"SELECT bill_title FROM tg_probill WHERE  pro_id=" + propid + "AND bill_title='2' ";
            object result = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            string resultJs = "";
            if (result == null)
            {
                //add
                string sqlInser = @"INSERT INTO dbo.tg_probill(bill_title,item_tilte,item_val,state,pro_id,bill_Num)VALUES ('2','','" + queryString + "','1'," + Convert.ToInt32(propid) + ",1)";
                int resNum = TG.DBUtility.DbHelperSQL.ExecuteSql(sqlInser);
                if (resNum > 0)
                {
                    resultJs = resNum.ToString();
                }
            }
            else
            {
                //update
                string updateSql = @"UPDATE dbo.tg_probill SET item_val='" + queryString + "' where bill_title='2' and pro_id=" + propid;
                int resNum = TG.DBUtility.DbHelperSQL.ExecuteSql(updateSql);
                if (resNum > 0)
                {
                    resultJs = resNum.ToString();
                }
            }
            return resultJs;
        }
        protected void GetVal_item(out StringBuilder builder)
        {
            builder = new StringBuilder();
            builder.Append("3002=" + Project.pro_name + "\n\n");
            builder.Append("3149=" + this.BM_3149_0_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_1.Value);
            builder.Append("\n");

            builder.Append(this.BM_3149_0_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_0_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");
            //2
            builder.Append(this.BM_3149_1_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_1_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");

            builder.Append(this.BM_3149_2_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_2_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");

            builder.Append(this.BM_3149_3_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_3_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");


            builder.Append(this.BM_3149_4_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_4_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");

            builder.Append(this.BM_3149_5_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_5_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");

            builder.Append(this.BM_3149_6_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_6_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");

            builder.Append(this.BM_3149_7_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_7_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");
            builder.Append(this.BM_3149_8_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_8_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");
            builder.Append(this.BM_3149_9_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_9_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");
            builder.Append(this.BM_3149_10_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_10_11.Value);
            builder.Append("\n");
            builder.Append("\r\n");
            builder.Append(this.BM_3149_11_0.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_1.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_2.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_3.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_4.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_5.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_6.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_7.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_8.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_9.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_10.Value);
            builder.Append("\n");
            builder.Append(this.BM_3149_11_11.Value);
            builder.Append("\n");
        }
    }
}