﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using AjaxPro;

namespace TG.Web.ProjectPlan
{
    public partial class ProPlanAuditEditBymaster : PageBase
    {
        //审批消息ID
        public int MessageID
        {
            get
            {
                int msgSysNo = 0;
                int.TryParse(Request["MsgNo"], out msgSysNo);
                return msgSysNo;
            }
        }
        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        /// <summary>
        /// 项目审核SysNo
        /// </summary>
        public int ProjectPlanAuditSysNo
        {
            get
            {
                int projectPlanAuditSysNo = 0;
                int.TryParse(Request["projectPlanAuditSysNo"], out projectPlanAuditSysNo);
                return projectPlanAuditSysNo;
            }
        }
        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// 消息状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }
        }
        //项目ID
        public int _projid;
        public int ProjectSysNo
        {
            get
            {
                int projSysNo = 0;
                int.TryParse(Request["ProjSysNo"], out projSysNo);
                if (projSysNo != 0)
                {
                    this._projid = projSysNo;
                }
                return this._projid;
            }
            set
            {
                this._projid = value;
            }
        }
        public string AuditHTML { get; set; }

        public string Action { get; set; }

        //类型
        public string MsgType
        {
            get
            {
                return Request["msyType"];
            }
        }
        public string AuditStatus { get; set; }

        public List<ProjectPlanRole> ProjectPlanRoleList { get; set; }

        public ProjectPlanViewParameterEntity ProjectPlanViewParameterEntity { get; set; }
        public string JobNumber { get { return ProjectPlanViewParameterEntity.JobNumber; } }
        public ProjectDesignPlanRole ProjectDesignPlanRoleList { get; set; }
        public TG.Model.cm_Project Project { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProPlanAuditEditBymaster));
            //修改说明：使报错页面转到错误页
            //参数说明：无
            //修改原因：防止报错
            //修改人：sgq
            //修改时间：2013-12-12
            string sql = @"SELECT ProjectSysNo FROM cm_ProjectPlanEditRecord WHERE SysNo=" + ProjectPlanAuditSysNo;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            if (o != null)
            {
                if (!IsPostBack)
                {
                    if (ProjectPlanAuditSysNo != 0)
                    {
                        //审批信息
                        GetProjectPlanAudit();
                    }
                    //项目信息
                    GetProInfo(ProjectSysNo.ToString());
                    //项目工号
                    GetProjectNumberEntity();
                    //策划人员信息
                    GetProjPlanUserList();
                    //策划计划信息
                    GetProjectPlanSubItemList();

                }
                GetOption();
            }
            else
            {
                Response.Redirect("../Error.aspx?msgtype=" + MsgType);
            }
        }
        protected void GetOption()
        {
            string sql = @"SELECT optionsedit FROM cm_ProjectPlanEditRecord WHERE ProjectSysNo=" + ProjectSysNo;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            this.optionsmes.Text = o.ToString();
        }
        //获取项目工号实体
        protected void GetProjectNumberEntity()
        {
            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
            ProjectPlanViewParameterEntity = projectPlanBP.GetProjectPlanViewParameterEntity(ProjectSysNo);
        }
        //获取策划用户列表
        protected void GetProjPlanUserList()
        {
            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
            ProjectPlanRoleList = projectPlanBP.GetProjectPlanRoleAndUsers(ProjectSysNo);
            //设计人员
            ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(ProjectSysNo);
            RepeaterAllUsers.DataSource = new TG.BLL.cm_ProjectPlanBP().GetPlanUsers(ProjectSysNo);
            RepeaterAllUsers.DataBind();
        }
        //获取项目信息
        public void GetProInfo(string strProId)
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            Project = pro_model;
            //项目名称
            this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
            //关联合同
            this.txt_reletive.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
            //管理级别
            string level = pro_model.pro_level.ToString();
            if (level.Trim() == "0")
            {
                this.lbl_level.Text = "院管";
            }
            else
            {
                this.lbl_level.Text = "所管";
            }
            //审核级别
            string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
            if (auditlevel.Trim() == "1,0")
            {
                this.lbl_auditlevel.Text = "院审";
            }
            else if (auditlevel.Trim() == "0,1")
            {
                this.lbl_auditlevel.Text = "所审";
            }
            else if (auditlevel.Trim() == "1,1")
            {
                this.lbl_auditlevel.Text = "院审,所审";
            }
            //建筑级别
            string BuildType = pro_model.BuildType.Trim();
            this.drp_buildtype.Text = BuildType;
            //建设单位
            this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
            //承接部门
            this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            //建设地点
            this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
            //建设规模
            this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
            //项目结构形式
            string StrStruct = pro_model.pro_StruType;
            if (!string.IsNullOrEmpty(StrStruct))
            {
                this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
            }
            //建筑分类
            string BuildStructType = pro_model.pro_kinds;
            if (!string.IsNullOrEmpty(BuildStructType))
            {
                this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
            }
            //显示项目阶段
            string str_jd = pro_model.pro_status;
            if (str_jd.IndexOf(',') > -1)
            {
                this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
            }
            //经济所
            string isotherprt = "";
            if (pro_model.ISTrunEconomy == "1")
            {
                isotherprt += "经济所,";
            }
            //暖通
            if (pro_model.ISHvac == "1")
            {
                isotherprt += "暖通热力所,";
            }
            //土建所
            if (pro_model.ISArch == "1")
            {
                isotherprt += "土建所,";
            }
            if (isotherprt.IndexOf(',') > -1)
            {
                this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
            }
            else
            {
                this.lbl_isotherprt.Text = "无";
            }
            //项目来源
            string pro_src = pro_model.Pro_src.ToString();
        }
        //获取审批轨迹
        private void GetProjectPlanAudit()
        {
            TG.Model.cm_ProjectPlanEditRecordEntity projectPlanAudit = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanEditRecordEntity(new cm_ProjectPlanEditRecordEntityQueryEntity { ProjectPlanEditRecordSysNo = ProjectPlanAuditSysNo });

            //获取审批时的项目ID
            ProjectSysNo = projectPlanAudit.ProjectSysNo;

            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();

            GetProjectPlanAuditEntity(projectPlanAudit);
        }

        /// <summary>
        /// 查询审核实体的方法
        /// </summary>
        public void GetProjectPlanAuditEntity(cm_ProjectPlanEditRecordEntity projectPlan)
        {
            AuditStatus = projectPlan.Status;

            //查询项目审核策划的流程信息
            List<string> processDescription = new TG.BLL.cm_ProjectPlanBP().GetAuditProcessDescription();

            string[] auditUserArray = projectPlan.AuditUserArray;
            //审批时间
            string[] auditUserDate = projectPlan.AuditDateArray;

            string html = "";
            html = "<table class=\"show_project\" style=\"width: 100%; font-size: 12px\">";
            if (projectPlan != null && projectPlan.SuggestionArray != null)
            {
                TG.BLL.tg_member uBp = new BLL.tg_member();
                int i = 0;

                if ((AuditStatus != MessageStatus) && (MessageStatus == "A" || MessageStatus == "C"))
                {
                    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[0]));
                    string usreName = user == null ? "" : user.mem_Name;
                    string auditDate = auditUserDate[0];
                    html += JoinAuditHTML(projectPlan.SuggestionArray[0], usreName, processDescription[0], auditDate);
                }
                else if ((AuditStatus != MessageStatus) && (MessageStatus == "B" || MessageStatus == "E"))
                {
                    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[1]));
                    string usreName = user == null ? "" : user.mem_Name;
                    string auditDate = auditUserDate[1].ToString();
                    html += JoinAuditHTML(projectPlan.SuggestionArray[1], usreName, processDescription[1], auditDate);
                }
                else if ((AuditStatus != MessageStatus) && (MessageStatus == "D" || MessageStatus == "G"))
                {
                    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[2]));
                    string usreName = user == null ? "" : user.mem_Name;
                    string auditDate = auditUserDate[2].ToString();
                    html += JoinAuditHTML(projectPlan.SuggestionArray[2], usreName, processDescription[2], auditDate);
                }
                else
                {
                    foreach (string suggestion in projectPlan.SuggestionArray)
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[i]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[i].ToString();
                        html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate);
                        i++;
                    }
                }
                //foreach (string suggestion in projectPlan.SuggestionArray)
                //{
                //    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[i]));
                //    string usreName = user == null ? "" : user.mem_Name;
                //    //时间
                //    string auditDate = auditUserDate[i].ToString();

                //    html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate);
                //    i++;
                //}
            }
            if (AuditStatus == MessageStatus)
            {
                if (AuditStatus != "C" && AuditStatus != "E" && AuditStatus != "G" && AuditStatus != "I" && AuditStatus != "H" && AuditStatus != "F")
                {
                    if (CheckPower())
                    {
                        html += JoinAuditHTML("", "", "", "");
                    }
                    else
                    {
                        html += "<tr><td align=\"center\" colspan=\"2\" style=\"width:100%;\">您没有权限审核该项！</td></tr>";
                            //<tr><td align=\"center\" colspan=\"2\" style=\"width:100%;\"><input type=\"button\" id=\"Button1\" name=\"controlBtn\" class=\"btn\" value=\"返回\" onclick=\"javascript:history.back();\" /></td></tr
                    }
                }
            }
            html += "  </table>";
            AuditHTML = html;
        }

        public bool CheckPower()
        {
            int roleSysNo = new TG.BLL.cm_ProjectPlanBP().GetProcessRoleSysNo(AuditStatus);

            bool hasPower = new TG.BLL.cm_Role().CheckPower(roleSysNo, UserSysNo);

            return hasPower;
        }

        private string JoinAuditHTML(string suggestion, string userName, string roleName, string auditDate)
        {
            string disableFlag = "";
            string idFlag = "";
            if (!string.IsNullOrEmpty(suggestion))
            {
                disableFlag = "disabled=disabled";
            }
            else
            {
                idFlag = "suggestionTextArea";
            }

            string html = "";
            html += "<tr>";
            html += "<td style=\"width: 20%\" align=\"center\">";
            string sugesstionString = "意见";
            if (!string.IsNullOrEmpty(roleName) && !string.IsNullOrEmpty(userName))
            {
                sugesstionString = string.Format("{0} : {1}的意见", roleName, userName);
            }
            html += sugesstionString;
            html += "</td>";
            html += "<td style=\"width: 70%\">";
            html += "&nbsp;";
            html += " <textarea style=\"height:60px;width:98%;border:solid 1px gray;\" id=\"" + idFlag + "\" " + disableFlag + ">" + suggestion + "</textarea>";
            html += "</td>";
            html += "<td style=\"width:15%;\">";
            html += auditDate;
            html += "</td>";
            html += "</tr>";
            //审批
            if (string.IsNullOrEmpty(suggestion))
            {
                html += "<tr><td align=\"center\" colspan=\"2\">";
                html += "<input type=\"button\" id=\"AgreeButton\" data-toggle=\"modal\" value=\"通过\" class=\"btn green\"/>&nbsp;";
                html += "<input type=\"button\" id=\"DisAgreeButton\" value=\"不通过\" class=\"btn red\"/>&nbsp;";
                //html += "<input type=\"button\" id=\"Button1\" name=\"controlBtn\" class=\"btn\" value=\"返回\" onclick=\"javascript:history.back();\" />";
                html += " </td></tr>";
            }
         
            html += "</tr>";
            return html;
        }
        //绑定策划子项
        public void GetProjectPlanSubItemList()
        {
            RepeaterProjectPlanSubItem.DataSource = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanSubitemList(ProjectSysNo);
            RepeaterProjectPlanSubItem.DataBind();
        }

        /// <summary>
        /// 审核通过
        /// </summary>
        /// <param name="projectPlanAudit"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string Agree(string projectPlanAudit, string flag)
        {
            string result = "";
            ProjectPlanAuditViewParameterEntity projectPlanAuditViewParameterEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanAuditViewParameterEntity>(projectPlanAudit);

            int asciiCode = (int)Convert.ToChar(projectPlanAuditViewParameterEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode++;
            }
            else
            {
                asciiCode = asciiCode + 2;
            }
            //实现
            if (flag == "0")
            {
                result = GetNextProcessRoleUser(projectPlanAuditViewParameterEntity.Status);
            }
            else
            {
                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();

                cm_ProjectPlanAuditEntity projectPlanAuditEntity = projectPlanBP.GetProjectPlanAuditEntity(new cm_ProjectPlanAuditQueryEntity { ProjectPlanAuditSysNo = projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo });

                projectPlanAuditEntity.Status = ((char)asciiCode).ToString();
                projectPlanAuditEntity.Suggestion = projectPlanAuditEntity.Suggestion + projectPlanAuditViewParameterEntity.Suggestion;
                projectPlanAuditEntity.AuditUser = projectPlanAuditEntity.AuditUser + UserSysNo;
                projectPlanAuditEntity.AuditDate = projectPlanAuditEntity.AuditDate + DateTime.Now;

                int count = projectPlanBP.UpdateProjectPlanAudit(projectPlanAuditEntity);

                if (count > 0)
                {
                    SysMessageViewEntity sysMessageEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo.ToString(),
                        ReferenceSysNo = string.Format("ProjectPlanAuditSysNo={0}&MessageStatus={1}", projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo.ToString(), projectPlanAuditEntity.Status),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 13,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", projectPlanAuditViewParameterEntity.CoperationName, "项目策划审核"),
                        QueryCondition = projectPlanAuditViewParameterEntity.CoperationName,
                        Status = "A",
                        IsDone = "A"
                    };
                    //发送消息
                    int position = 1;
                    switch (projectPlanAuditEntity.Status)
                    {
                        case "B":
                            position = 2;
                            sysMessageEntity.ToRole = projectPlanBP.GetProjectPlanAuditConfigEntity(2).RoleSysNo + "";
                            break;
                        case "D":
                            position = 3;
                            sysMessageEntity.ToRole = projectPlanBP.GetProjectPlanAuditConfigEntity(3).RoleSysNo + "";
                            break;
                        case "F":
                        case "H":
                            sysMessageEntity.ToRole = "0";
                            sysMessageEntity.FromUser = projectPlanAuditEntity.InUser;
                            sysMessageEntity.MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", projectPlanAuditViewParameterEntity.CoperationName, "项目策划申请修改审核审核通过");
                            break;
                    }

                    TG.Model.cm_ProjectPlanAuditConfigEntity configEntity = projectPlanBP.GetProjectPlanAuditConfigEntity(position);
                    //最后一个流程审批给发起用户发送消息
                    if (projectPlanAuditEntity.Status == "F")
                    {
                        //最后流程发送给发起用户
                        sysMessageEntity.IsDone = "B";
                        int msgcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageEntity);
                        if (msgcount > 0)
                        {
                            result = "1";
                        }
                    }
                    else
                    {
                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            result = sysMsgString;
                        }
                        else
                        {
                            result = "0";
                        }
                    }

                }
                else
                {
                    return "0";
                }
            }

            return result;
        }

        /// <summary>
        /// 审核不通过
        /// </summary>
        /// <param name="projectPlanAudit"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DisAgree(string projectPlanAudit)
        {
            ProjectPlanAuditViewParameterEntity projectPlanAuditViewParameterEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanAuditViewParameterEntity>(projectPlanAudit);
            int asciiCode = (int)Convert.ToChar(projectPlanAuditViewParameterEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode = asciiCode + 2;
            }
            else
            {
                asciiCode = asciiCode + 3;
            }

            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();

            cm_ProjectPlanAuditEntity projectPlanAuditEntity = projectPlanBP.GetProjectPlanAuditEntity(new cm_ProjectPlanAuditQueryEntity { ProjectPlanAuditSysNo = projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo });

            projectPlanAuditEntity.Status = ((char)asciiCode).ToString();
            projectPlanAuditEntity.Suggestion = projectPlanAuditEntity.Suggestion + projectPlanAuditViewParameterEntity.Suggestion;
            projectPlanAuditEntity.AuditUser = projectPlanAuditEntity.AuditUser + UserSysNo;
            projectPlanAuditEntity.AuditDate = projectPlanAuditEntity.AuditDate + DateTime.Now;

            int count = projectPlanBP.UpdateProjectPlanAudit(projectPlanAuditEntity);

            if (count > 0)
            {
                SysMessageViewEntity msg = new SysMessageViewEntity
                {
                    //ReferenceSysNo = projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo.ToString(),
                    ReferenceSysNo = string.Format("ProjectPlanAuditSysNo={0}&MessageStatus={1}", projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo.ToString(), projectPlanAuditEntity.Status),
                    FromUser = projectPlanAuditEntity.InUser,
                    InUser = UserSysNo,
                    MsgType = 13,
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", projectPlanAuditViewParameterEntity.CoperationName, "项目策划申请修改审核未通过"),
                    QueryCondition = projectPlanAuditViewParameterEntity.CoperationName,
                    ToRole = "0",
                    Status = "A",
                    IsDone = "B"
                };
                new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
            }
            return count + "";
        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser(string status)
        {
            string result = "";
            int roleSysNo = 0;
            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
            //获取项目配置审批实体
            switch (status)
            {
                case "A":
                    roleSysNo = projectPlanBP.GetProjectPlanAuditConfigEntity(2).RoleSysNo;
                    break;
                case "B":
                    roleSysNo = projectPlanBP.GetProjectPlanAuditConfigEntity(3).RoleSysNo;
                    break;
            }

            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(roleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }

        /// <summary>
        /// 审核通过
        /// </summary>
        /// <param name="projectPlanAudit"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string AgreeEdit(string projectPlanAudit, string flag)
        {
            string result = "";
            ProjectPlanEditRecordViewParameterEntity projectPlanAuditViewParameterEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanEditRecordViewParameterEntity>(projectPlanAudit);

            int asciiCode = (int)Convert.ToChar(projectPlanAuditViewParameterEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode++;
            }
            else
            {
                asciiCode = asciiCode + 2;
            }
            //实现
            if (flag == "0")
            {
                result = GetNextProcessRoleUser(projectPlanAuditViewParameterEntity.Status);
            }
            else
            {
                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();

                cm_ProjectPlanEditRecordEntity projectPlanAuditEntity = projectPlanBP.GetProjectPlanEditRecordEntity(new cm_ProjectPlanEditRecordEntityQueryEntity { ProjectPlanEditRecordSysNo = projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo });

                projectPlanAuditEntity.Status = ((char)asciiCode).ToString();
                projectPlanAuditEntity.Suggestion = projectPlanAuditEntity.Suggestion + projectPlanAuditViewParameterEntity.Suggestion;
                projectPlanAuditEntity.AuditUser = projectPlanAuditEntity.AuditUser + UserSysNo;
                projectPlanAuditEntity.AuditDate = projectPlanAuditEntity.AuditDate + DateTime.Now;

                int count = projectPlanBP.UpdateProjectPlanEditRecord(projectPlanAuditEntity);

                if (count > 0)
                {
                    SysMessageViewEntity sysMessageEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo.ToString(),
                        ReferenceSysNo = string.Format("ProjectPlanAuditSysNo={0}&MessageStatus={1}", projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo.ToString(), projectPlanAuditEntity.Status),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 13,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", projectPlanAuditViewParameterEntity.CoperationName, "项目策划申请修改审核"),
                        QueryCondition = projectPlanAuditViewParameterEntity.CoperationName,
                        Status = "A",
                        IsDone = "A"
                    };
                    //发送消息
                    int position = 1;
                    switch (projectPlanAuditEntity.Status)
                    {
                        case "B":
                            position = 2;
                            sysMessageEntity.ToRole = projectPlanBP.GetProjectPlanAuditConfigEntity(2).RoleSysNo + "";
                            break;
                        case "D":
                            position = 3;
                            sysMessageEntity.ToRole = projectPlanBP.GetProjectPlanAuditConfigEntity(3).RoleSysNo + "";
                            break;
                        case "F":
                        case "H":
                            sysMessageEntity.ToRole = "0";
                            sysMessageEntity.FromUser = projectPlanAuditEntity.InUser;
                            sysMessageEntity.MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", projectPlanAuditViewParameterEntity.CoperationName, "项目策划申请修改审核审核通过");
                            break;
                    }

                    TG.Model.cm_ProjectPlanAuditConfigEntity configEntity = projectPlanBP.GetProjectPlanAuditConfigEntity(position);
                    //最后一个流程审批给发起用户发送消息
                    if (projectPlanAuditEntity.Status == "F")
                    {
                        //最后流程发送给发起用户
                        sysMessageEntity.IsDone = "B";
                        int msgcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageEntity);
                        if (msgcount > 0)
                        {
                            string sqlupdate = @"UPDATE dbo.cm_ProjectPlanAudit
                            SET Status = 'R'
                            WHERE ProjectSysNo = " + projectPlanAuditEntity.ProjectSysNo;
                            TG.DBUtility.DbHelperSQL.GetSingle(sqlupdate);
                            result = "1";
                        }
                    }
                    else
                    {
                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {

                            result = sysMsgString;
                        }
                        else
                        {

                            result = "0";
                        }
                    }

                }
                else
                {
                    return "0";
                }
            }

            return result;
        }

        /// <summary>
        /// 审核不通过
        /// </summary>
        /// <param name="projectPlanAudit"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DisAgreeEdit(string projectPlanAudit)
        {
            ProjectPlanEditRecordViewParameterEntity projectPlanAuditViewParameterEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanEditRecordViewParameterEntity>(projectPlanAudit);
            int asciiCode = (int)Convert.ToChar(projectPlanAuditViewParameterEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode = asciiCode + 2;
            }
            else
            {
                asciiCode = asciiCode + 3;
            }

            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();

            cm_ProjectPlanEditRecordEntity projectPlanAuditEntity = projectPlanBP.GetProjectPlanEditRecordEntity(new cm_ProjectPlanEditRecordEntityQueryEntity { ProjectPlanEditRecordSysNo = projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo });

            projectPlanAuditEntity.Status = ((char)asciiCode).ToString();
            projectPlanAuditEntity.Suggestion = projectPlanAuditEntity.Suggestion + projectPlanAuditViewParameterEntity.Suggestion;
            projectPlanAuditEntity.AuditUser = projectPlanAuditEntity.AuditUser + UserSysNo;
            projectPlanAuditEntity.AuditDate = projectPlanAuditEntity.AuditDate + DateTime.Now;

            int count = projectPlanBP.UpdateProjectPlanEditRecord(projectPlanAuditEntity);

            if (count > 0)
            {
                SysMessageViewEntity msg = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("ProjectPlanAuditSysNo={0}&MessageStatus={1}", projectPlanAuditViewParameterEntity.ProjectPlanAuditSysNo.ToString(), projectPlanAuditEntity.Status),
                    FromUser = projectPlanAuditEntity.InUser,
                    InUser = UserSysNo,
                    MsgType = 13,
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", projectPlanAuditViewParameterEntity.CoperationName, "项目策划申请修改审核未通过"),
                    QueryCondition = projectPlanAuditViewParameterEntity.CoperationName,
                    ToRole = "0",
                    Status = "A",
                    IsDone = "B"
                };
                new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
            }
            return count + "";
        }
        [AjaxMethod]
        public string SubmitApplyEdit(string queryString, string flag)
        {
            string result = "";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectPlanAuditParamterEntity par = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanAuditParamterEntity>(queryString);

                int sysNo = 0;

                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
                //是否存在策划审批
                bool isExists = projectPlanBP.IsExistsProjectPlanEditRecord(par.ProjectSysNo);
                //项目实体
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(par.ProjectSysNo);

                //如果项目未策划
                if (!isExists)
                {
                    sysNo = projectPlanBP.InsertProjectPlanEdit(new cm_ProjectPlanEditRecordEntity
                    {
                        ProjectSysNo = par.ProjectSysNo,
                        optionsedit = "sss",
                        InUser = UserSysNo,
                        Status = "A"
                    });

                    TG.Model.cm_ProjectPlanAuditConfigEntity projectPlanAuditConfigEntity = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditConfigEntity(1);
                    //实例化审批消息
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = sysNo.ToString(),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 13,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目策划审核"),
                        QueryCondition = project.pro_name,
                        IsDone = "A",
                        Status = "A"
                    };

                    //第一审批阶段人员iD
                    string roleSysNo = projectPlanAuditConfigEntity.RoleSysNo.ToString();
                    sysMessageViewEntity.ToRole = roleSysNo;
                    //获取审批人列表人名
                    string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        result = sysMsgString;
                    }
                }
                else
                {
                    result = "1";
                }
            }
            return result;

        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            TG.Model.cm_ProjectPlanAuditConfigEntity projectPlanAuditConfigEntity = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditConfigEntity(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectPlanAuditConfigEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }
    }
}