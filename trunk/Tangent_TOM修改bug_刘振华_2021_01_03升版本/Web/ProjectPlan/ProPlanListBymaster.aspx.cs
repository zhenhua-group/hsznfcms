﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model;
using System.Text;
using System.Data;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;

namespace TG.Web.ProjectPlan
{
    public partial class ProPlanListBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProPlanListBymaster));
            if (!IsPostBack)
            {
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //部门
                BindUnit();
                //未策划
                GetProjectPlanListOne();
                //已策划
                GetProjectPlanListTwo();

                //绑定权限
                BindPreviewPower();
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //检查是否需要权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //当前用户ID
        public string GetUserSysNo()
        {
            return UserSysNo.ToString();
        }
        //消息传值项目ID
        public int ProjectSysNo
        {
            get
            {
                int projsysno = 0;
                int.TryParse(Request["ProjectSysNo"] ?? "0", out projsysno);
                return projsysno;
            }
        }
        //项目总负责ID
        public int PMUserID
        {
            get
            {
                int userid = 0;
                int.TryParse(Request["PMUserID"] ?? "0", out userid);
                return userid;
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (P.InsertUserID =" + UserSysNo + " OR P.PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND P.Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //项目总负责权限判断
        private void GetPreviewPowerByProjUserSql(ref StringBuilder sb)
        {
            sb.Append(" AND P.PMUserID=" + PMUserID);
        }
        //未发起项目策划
        private void GetProjectPlanListOne()
        {
            TG.BLL.cm_ProjectPlanBP bll = new TG.BLL.cm_ProjectPlanBP();

            StringBuilder sb = new StringBuilder("");
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            //}
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            //如果是通过审批消息策划的情况
            if (PMUserID > 0)
            {
                this.hid_flag.Value = "1";
                GetPreviewPowerByProjUserSql(ref sb);
            }
            else
            {
                GetPreviewPowerSql(ref sb);
            }
            this.hid_where.Value = sb.ToString();
            //所有记录数
            //  this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(sb.ToString()).ToString());
            //分页
            //this.gvproPlan.DataSource = bll.GetProjectPlanListNo(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            // this.gvproPlan.DataBind();
        }
        //项目策划通过审批
        private void GetProjectPlanListTwo()
        {
            TG.BLL.cm_ProjectPlanBP bll = new TG.BLL.cm_ProjectPlanBP();

            StringBuilder sb = new StringBuilder("");
            //如果是通过审批消息策划的情况
            if (PMUserID > 0)
            {
                GetPreviewPowerByProjUserSql(ref sb);
            }
            else
            {
                GetPreviewPowerSql(ref sb);
            }
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            //}
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            this.hid_where2.Value = sb.ToString();
            //所有记录数
            // this.AspNetPager2.RecordCount = int.Parse(bll.GetListPageProcYesCount(sb.ToString()).ToString());

            // this.gvAuditstatus.DataSource = bll.GetProjectPlanListYes(sb.ToString(), this.AspNetPager2.StartRecordIndex, this.AspNetPager2.EndRecordIndex);
            //this.gvAuditstatus.DataBind();
        }

        [AjaxMethod]
        public string SubmitApply(string queryString, string flag)
        {
            string result = "";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectPlanAuditParamterEntity par = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanAuditParamterEntity>(queryString);

                int sysNo = 0;

                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
                //是否存在策划审批
                bool isExists = projectPlanBP.IsExistsProjectPlanAudit(par.ProjectSysNo);
                //项目实体
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(par.ProjectSysNo);

                //如果项目未策划
                if (!isExists)
                {
                    sysNo = projectPlanBP.InsertProjectPlanAudit(new cm_ProjectPlanAuditEntity
                    {
                        ProjectSysNo = par.ProjectSysNo,
                        InUser = UserSysNo,
                        Status = "A"
                    });

                    TG.Model.cm_ProjectPlanAuditConfigEntity projectPlanAuditConfigEntity = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditConfigEntity(1);
                    //实例化审批消息
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = sysNo.ToString(),
                        ReferenceSysNo = string.Format("ProjectPlanAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 4,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目策划评审"),
                        QueryCondition = project.pro_name,
                        IsDone = "A",
                        Status = "A"
                    };

                    //第一审批阶段人员iD
                    string roleSysNo = projectPlanAuditConfigEntity.RoleSysNo.ToString();
                    sysMessageViewEntity.ToRole = roleSysNo;
                    //获取审批人列表人名
                    string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        result = sysMsgString;
                    }
                }
                else
                {
                    result = "1";
                }
            }
            return result;

        }
        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            TG.Model.cm_ProjectPlanAuditConfigEntity projectPlanAuditConfigEntity = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditConfigEntity(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectPlanAuditConfigEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        /// <summary>
        /// 导出未策划信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Export_NoPlan_Click(object sender, EventArgs e)
        {
            DataTable dt = GetProjectNoPlanList();

            string modelPath = " ~/TemplateXls/ProjectPlan.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            ws.GetRow(0).GetCell(0).SetCellValue("未策划项目信息");
            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ProjectName"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["IsBackup"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["SubmitDate"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ProjectJobNumber"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["PMName"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;

                cell.SetCellValue("未提交评审");
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("未策划项目列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 导出已策划信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Export_YesPlan_Click(object sender, EventArgs e)
        {
            DataTable dt = GetProjectYesPlanList();

            string modelPath = " ~/TemplateXls/ProjectPlan.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            ws.GetRow(0).GetCell(0).SetCellValue("已策划项目信息");
            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ProjectName"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["IsBackup"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["SubmitDate"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ProjectJobNumber"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["PMName"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;

                cell.SetCellValue(setStatus(dt.Rows[i]["ProjectPlanAuditSysNo"].ToString(), dt.Rows[i]["ProjectPlanAuditStatus"].ToString()));
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("已策划项目列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        //未发起项目策划
        private DataTable GetProjectNoPlanList()
        {
            TG.BLL.cm_Project bll = new TG.BLL.cm_Project();

            DataTable dt = bll.GetProjectNoPlanExportInfo(getWhere()).Tables[0];

            return dt;
        }


        //已发起项目策划
        private DataTable GetProjectYesPlanList()
        {
            TG.BLL.cm_Project bll = new TG.BLL.cm_Project();

            DataTable dt = bll.GetProjectYesPlanExportInfo(getWhere()).Tables[0];

            return dt;
        }

        /// <summary>
        /// 条件
        /// </summary>
        /// <returns></returns>
        private string getWhere()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            }
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            }

            //如果是通过审批消息策划的情况
            if (PMUserID > 0)
            {
                this.hid_flag.Value = "1";
                GetPreviewPowerByProjUserSql(ref sb);
            }
            else
            {
                GetPreviewPowerSql(ref sb);
            }
            return sb.ToString();
        }

        /// <summary>
        /// 得到状态
        /// </summary>
        /// <param name="auditId"></param>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        private string setStatus(string auditId, string auditStatus)
        {
            string status = "";
            if (string.IsNullOrEmpty(auditStatus) && auditId == "0")
            {
                status = "未发起审批";
            }
            else if (auditStatus == "A" && auditId != "0")
            {
                status = "设总发起审批";
            }
            else if (auditStatus == "B" && auditId != "0")
            {
                status = "承接部门负责人审批通过";
            }
            else if (auditStatus == "C" && auditId != "0")
            {
                status = "承接部门负责人审批不通过";
            }
            else if (auditStatus == "D" && auditId != "0")
            {
                status = "生产经营部审批通过";
            }
            else if (auditStatus == "E" && auditId != "0")
            {
                status = "生产经营部审批不通过";
            }
            else if (auditStatus == "F" && auditId != "0")
            {
                status = "全部通过";
            }
            else if (auditStatus == "G" && auditId != "0")
            {
                status = "技术质量部审批不通过";
            }

            return status;
        }
    }

    public class ProjectPlanAuditParamterEntity
    {
        public int ProjectSysNo { get; set; }

        public string CoperationName { get; set; }
    }
    public class ProjectPlanRecordParamterEntity
    {
        public int ProjectSysNo { get; set; }

        public string CoperationName { get; set; }

        public string copoption { get; set; }
    }

}