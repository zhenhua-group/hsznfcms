﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.BLL;
using TG.Model;

namespace TG.Web.ProjectPlan
{
    public partial class ProPlanMiddlePage : PageBase
    {
        protected string projectSysNo
        {
            get
            {
                return Request["projectSysNo"].ToString();
            }
        }
        public string pronumber
        {
            get;
            set;
        }
        public string cprname
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {   //注册页面
            Utility.RegisterTypeForAjax(typeof(ProPlanMiddlePage));
            if (!IsPostBack)
            {
                this.hidproId.Value = projectSysNo;
                this.hiduserid.Value = UserSysNo + "";
            }
            if (!string.IsNullOrEmpty(projectSysNo))
            {
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(projectSysNo));
                if (pro != null)
                {
                    pronumber = pro.Pro_number.Trim();
                    TG.Model.cm_Coperation cpr = new TG.BLL.cm_Coperation().GetModel(pro.CoperationSysNo);
                    if (cpr != null)
                    {
                        cprname = cpr.cpr_Name.Trim();
                    }
                }
            }

        }

        protected void btn_ProList_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProPlanListBymaster.aspx");
        }
        protected void btn_ShowPro_Click(object sender, EventArgs e)
        {          
           
            Response.Redirect("ProPlanAddBymaster.aspx?projectSysNo=" + projectSysNo + "&Action=3&JobNumber=" + pronumber + "&CoperationName=" + cprname + "&flag=1");
        }
        [AjaxMethod]
        public string SubmitApply(string queryString, string flag)
        {
            string result = "";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectPlanAuditParamterEntity par = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectPlanAuditParamterEntity>(queryString);

                int sysNo = 0;

                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
                //是否存在策划审批
                bool isExists = projectPlanBP.IsExistsProjectPlanAudit(par.ProjectSysNo);
                //项目实体
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(par.ProjectSysNo);

                //如果项目未策划
                if (!isExists)
                {
                    sysNo = projectPlanBP.InsertProjectPlanAudit(new cm_ProjectPlanAuditEntity
                    {
                        ProjectSysNo = par.ProjectSysNo,
                        InUser = UserSysNo,
                        Status = "A"
                    });

                    TG.Model.cm_ProjectPlanAuditConfigEntity projectPlanAuditConfigEntity = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditConfigEntity(1);
                    //实例化审批消息
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = sysNo.ToString(),
                        ReferenceSysNo = string.Format("ProjectPlanAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 4,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目策划评审"),
                        QueryCondition = project.pro_name,
                        IsDone = "A",
                        Status = "A"
                    };

                    //第一审批阶段人员iD
                    string roleSysNo = projectPlanAuditConfigEntity.RoleSysNo.ToString();
                    sysMessageViewEntity.ToRole = roleSysNo;
                    //获取审批人列表人名
                    string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        result = sysMsgString;
                    }
                }
                else
                {
                    result = "1";
                }
            }
            return result;

        }
        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            TG.Model.cm_ProjectPlanAuditConfigEntity projectPlanAuditConfigEntity = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditConfigEntity(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectPlanAuditConfigEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }

    }
}