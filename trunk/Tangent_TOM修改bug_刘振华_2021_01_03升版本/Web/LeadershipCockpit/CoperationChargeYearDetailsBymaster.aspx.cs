﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using AjaxPro;
using Geekees.Common.Controls;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationChargeYearDetailsBymaster : PageBase
    {
        //收款开始时间
        protected string ChargeStartTime { get; set; }
        //收款结束时间
        protected string ChargeEndTime { get; set; }
        protected string filetitle;
        protected void Page_Load(object sender, EventArgs e)
        {
            //注册页面
            Utility.RegisterTypeForAjax(typeof(CoperationChargeYearDetailsBymaster));
            if (!IsPostBack)
            {
                //绑定生产部门
                BindUnit();
                //绑定树形部门
                LoadUnitTree();
                //初始部门
                SelectedCurUnit();

                //初始时间
                InitDate();
                //合同年份
                BindYear();
                //默认当年
                SelectCurrentYear();
                //绑定部门产值
                BindUnitAllot();

            }
        }
        //是否检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //判断是否是全部查看
        protected string IsCheckAllPower
        {
            get
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drpunit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drpunit.Theme = macOS;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        protected void BindUnit()
        {
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

        }
        //绑定目标值
        //绑定目标值
        protected void BindUnitAllot()
        {
            CreateTimeQuery();

            DataSet ds = GetUnitAllot();

            //绑定数据
            this.grid_allot.DataSource = ds;
            this.grid_allot.DataBind();

            this.grid_allot.UseAccessibleHeader = true;
            this.grid_allot.HeaderRow.TableSection = TableRowSection.TableHeader;

            CreateTipQuery();
        }

        /// <summary>
        /// 筛选条件绑定信息
        /// </summary>
        /// <returns></returns>
        protected DataSet GetUnitAllot()
        {
            string strWhere = "";
            //合同年份
            string str_year = "";
            if (this.drp_year1.SelectedIndex != 0)
            {
                str_year = this.drp_year1.SelectedValue;
              
            }

            //生产部门
            string unitlist = GetCheckedNodes();
            string unitnamelist = GetCheckedNodeName();
            strWhere = string.Format(" AND unit_ID IN ({0}) ", unitlist);
                 

            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            // GetLabUnit();
            string query = "";
            string startyear = this.txt_year1.Value;
            string endyear = this.txt_year2.Value;
            string cbxtimetype = this.hid_time.Value;
            string jidu = this.drpJidu.SelectedValue;
            string month = this.drpMonth.SelectedValue;

            //合同时间段 
            if (cbxtimetype == "1")
            {
                string curtime = DateTime.Now.Year.ToString();
                if (startyear == "" && endyear != "")
                {
                    query = string.Format(" AND cpr_SignDate<='{0}'", endyear.Trim());
                    str_year = Convert.ToDateTime(endyear).Year.ToString();
                }
                else if (startyear != "" && endyear == "")
                {
                    query = string.Format(" AND cpr_SignDate>='{0}'", startyear.Trim());
                    str_year = Convert.ToDateTime(startyear).Year.ToString();
                }
                else if (startyear != "" && endyear != "")
                {
                    query = string.Format(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", startyear.Trim(), endyear.Trim());
                    str_year = Convert.ToDateTime(startyear).Year.ToString();
                }
                else
                {
                    str_year = "";
                }
                
            }
            else
            {
                //年份
                if (!string.IsNullOrEmpty(str_year) && str_year != "-1")
                {
                    //年
                    string stryear = str_year;
                    //季度
                    string strjidu = jidu;
                    //月
                    string stryue = month;
                    //
                    string start = "";
                    string end = "";
                    //年
                    if (strjidu == "0" && stryue == "0")
                    {
                        start = stryear + "-01-01 00:00:00";
                        end = stryear + "-12-31 23:59:59 ";
                    }
                    else if (strjidu != "0" && stryue == "0") //年季度
                    {
                        start = stryear;
                        end = stryear;
                        switch (strjidu)
                        {
                            case "1":
                                start += "-01-01 00:00:00";
                                end += "-03-31 23:59:59";
                                break;
                            case "2":
                                start += "-04-01 00:00:00";
                                end += "-06-30 23:59:59";
                                break;
                            case "3":
                                start += "-07-01 00:00:00";
                                end += "-09-30 23:59:59";
                                break;
                            case "4":
                                start += "-10-01 00:00:00";
                                end += "-12-31 23:59:59";
                                break;
                        }
                    }
                    else if (strjidu == "0" && stryue != "0")//年月份
                    {
                        //当月有几天
                        int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                        start = stryear + "-" + stryue + "-01 00:00:00";
                        end = stryear + "-" + stryue + "-" + days + " 23:59:59";
                    }
                    query = string.Format(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", start.Trim(), end.Trim());

                }
            }

            //合计
            CoperationCountAll(ChargeStartTime, ChargeEndTime,str_year,query);

            string unitCopAllotSql = "";
            string unitAllotSql = "";
            if (str_year != "")
            {
                unitCopAllotSql = "select isnull(UnitAllot,0) from cm_UnitCopAllot where (UnitID=unit_ID) and AllotYear='" + str_year.Trim() + "'";
                unitAllotSql = "select isnull(UnitAllot,0) from cm_UnitAllot where (UnitID=unit_ID) and AllotYear='" + str_year.Trim() + "'";
            }
            else
            {
                unitCopAllotSql = "select isnull(sum(UnitAllot),0) from cm_UnitCopAllot where (UnitID=unit_ID)";
                unitAllotSql = "select isnull(sum(UnitAllot),0) from cm_UnitAllot where (UnitID=unit_ID)";
            }


            //Sql查询统计结果
            string strSql = string.Format(@"select unit_ID,unit_Name,
                                        --已签合同数量
                                        (select COUNT(*) from cm_Coperation where (cpr_Unit=unit_Name) {0}) as CprCount,
                                        --已付款合同数量
                                        (select COUNT(*) from cm_Coperation C where (cpr_Unit=unit_Name) and (Select COUNT(*) from cm_ProjectCharge where cprID=C.cpr_ID and  Status<>'B')>0 {0}) as PayCprCount,
                                        --未付款合同数量
                                        (select COUNT(*) from cm_Coperation C where (cpr_Unit=unit_Name) and (Select COUNT(*) from cm_ProjectCharge where cprID=C.cpr_ID and  Status<>'B')=0 {0}) as NoPayCprCount,
                                        --合同面积
                                        (select isnull(SUM(convert(decimal,BuildArea)),0) from cm_Coperation where (cpr_Unit=unit_Name) {0}) as CprArea,
                                        --已签合同总额
                                        (select isnull(SUM(cpr_Acount),0) from cm_Coperation where (cpr_Unit=unit_Name) {0}) as CprAcount,
                                        --合同额目标值
                                        ({3}) as CprAcountTarget,
                                        --已收费额
                                        (select isnull(SUM(Acount),0) from cm_ProjectCharge where (cprID in (select cpr_ID from cm_Coperation where cpr_Unit=unit_Name )) and (Status<>'B') and (InAcountTime between '{1}' and '{2}')) as UnitChargeDone,
                                        --产值目标值 
                                        ({4}) as CprChargeDoneTarget
                                        from tg_unit", query, ChargeStartTime, ChargeEndTime, unitCopAllotSql, unitAllotSql);
            strSql += " Where 1=1 " + strWhere + " order by unit_ID ";
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            DataSet ds = bll.GetList(strSql);
            return ds;
        }
        /// <summary>
        ///  获取选择部门id
        /// </summary>
        private string GetCheckedNodes()
        {
            //默认部门ID
            StringBuilder sbUnitList = new StringBuilder("0");

            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);
            //不选中 
            if (nodes.Count == 0)
            {
                DataTable dt = GetUnit();
                foreach (DataRow row in dt.Rows)
                {
                    string unitid = row["unit_ID"].ToString().Trim();
                    sbUnitList.AppendFormat(",{0}", unitid);
                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitid = nd.NodeValue;
                    sbUnitList.AppendFormat(",{0}", unitid);
                }
            }

            return sbUnitList.ToString();
        }

        /// <summary>
        /// 获取选中部门
        /// </summary>
        /// <returns></returns>
        private string GetCheckedNodeName()
        {
            //默认部门ID
            StringBuilder sbUnitList = new StringBuilder();

            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);
            //不选中 
            if (nodes.Count == 0)
            {
                sbUnitList.Append("全院部门");
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    sbUnitList.AppendFormat("[{0}]", unitname);
                }
            }

            return sbUnitList.ToString();
        }

        //绑定目标值
        protected void BindUnitAllotDone()
        {
            CreateTimeQuery();

            DataSet ds = GetUnitAllot();

            //绑定数据

            this.grid_allot.DataSource = ds;
            this.grid_allot.DataBind();
            this.grid_allot.UseAccessibleHeader = true;
            if (this.grid_allot.HeaderRow != null)
            {
                this.grid_allot.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            CreateTipQueryDone();

        }

        /// <summary>
        /// 筛选条件绑定信息
        /// </summary>
        /// <returns></returns>


        //合计
        //已签合同数
        int cprcountall = 0;
        //已付款合同数
        int cprpaycountall = 0;
        //未付款合同数
        int cprnopaycountall = 0;
        //建筑面积
        decimal cprbuildarea = 0m;
        //合同总额
        decimal cpracountall = 0m;
        //收款
        decimal cprchargeall = 0m;
        // 欠款
        decimal cprnochargeall = 0m;
        //产值
        decimal cprchargedoneall = 0m;
        //欠款
        decimal cprnopayacount = 0m;
        decimal copAllallot = 0m;
        decimal copAllallotC = 0m;
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //合同额
                decimal cprallotyear = 0m;
                decimal cprallot = 0m;
                decimal cprpersent = 0m;
                //收费
                decimal chgallotyear = 0m;
                decimal chgallot = 0m;
                decimal chgpersent = 0m;
                //产值
                //decimal chgdoneallotyear = 0m;
                //decimal chgdoneallot = 0m;
                //decimal chgdonepersent = 0m;
                //合同目标值
                if (e.Row.Cells[5].Text != "&nbsp;")
                {
                    cprallotyear = Convert.ToDecimal(e.Row.Cells[5].Text);
                }
                //合同总额 
                if (e.Row.Cells[6].Text != "&nbsp;")
                {
                    cprallot = Convert.ToDecimal(e.Row.Cells[6].Text);
                }
                //比例
                if (cprallotyear > 0)
                {
                    cprpersent = (cprallot / cprallotyear) * 100;
                }
                e.Row.Cells[7].Text = cprpersent.ToString("f2") + "%";
                //shouf
                if (e.Row.Cells[8].Text != "&nbsp;")
                {
                    chgallotyear = Convert.ToDecimal(e.Row.Cells[8].Text);
                }
                if (e.Row.Cells[9].Text != "&nbsp;")
                {
                    chgallot = Convert.ToDecimal(e.Row.Cells[9].Text);
                }
                if (chgallotyear > 0)
                {
                    chgpersent = (chgallot / chgallotyear) * 100;
                }
                e.Row.Cells[10].Text = chgpersent.ToString("f2") + "%";
                //chanzhi
                //if (e.Row.Cells[8].Text != "&nbsp;")
                //{
                //    chgdoneallotyear = Convert.ToDecimal(e.Row.Cells[8].Text);
                //}
                //if (e.Row.Cells[9].Text != "&nbsp;")
                //{
                //    chgdoneallot = Convert.ToDecimal(e.Row.Cells[9].Text);
                //}
                //if (chgdoneallotyear > 0)
                //{
                //    chgdonepersent = (chgdoneallot / chgdoneallotyear) * 100;
                //}
                //e.Row.Cells[10].Text = chgdonepersent.ToString("f2") + "%";
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "全院合计：";
                e.Row.Cells[1].Text = cprcountall.ToString();
                e.Row.Cells[2].Text = cprpaycountall.ToString();
                e.Row.Cells[3].Text = cprnopaycountall.ToString();
                e.Row.Cells[4].Text = cprbuildarea.ToString("f4");
                e.Row.Cells[6].Text = cpracountall.ToString("f4");
                e.Row.Cells[5].Text = copAllallotC.ToString("f4");
                //e.Row.Cells[9].Text = cprnochargeall.ToString("f4");
                e.Row.Cells[8].Text = copAllallot.ToString("f4");
                e.Row.Cells[9].Text = cprchargedoneall.ToString("f4");
                if (copAllallot != 0m)
                {
                    e.Row.Cells[10].Text = ((cprchargedoneall / copAllallot) * 100).ToString("f4") + "%";
                }
                if (copAllallotC != 0m)
                {
                    e.Row.Cells[7].Text = ((cpracountall / copAllallotC) * 100).ToString("f4") + "%";
                }


            }


        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_start.Value = str_year + "-01-01";
            this.txt_end.Value = str_year + "-12-31";
            this.txtdateStart.Value = str_year + "-01-01";
            this.txtdateEnd.Value = str_year + "-12-31";
            this.txt_year1.Value = str_year + "-01-01";
            this.txt_year2.Value = str_year + "-12-31";
        }
        //合计
        protected void CoperationCountAll(string start, string end, string year,string query)
        {
            string sqlwhere = query;
           

            string strSql = string.Format(@"Select COUNT(*) as cprCount,
                                                            (select COUNT(*) from cm_Coperation C where cpr_id in (select distinct cprid from cm_ProjectCharge where Status<>'B' and cprID<>0) {0}) as PayCprCount,
                                                            (select COUNT(*) from cm_Coperation C where cpr_id not in (select distinct cprid from cm_ProjectCharge where Status<>'B' and cprID<>0) {0}) as NoPayCprCount,
                                                            isnull(SUM(convert(decimal,BuildArea)),0) as AreaCount,
                                                            isnull(SUM(cpr_Acount),0) as AcountCount,                                                           
                                                            isnull((select isnull(SUM(Acount),0) from cm_ProjectCharge where (cprID in (select cpr_ID from cm_Coperation where 1=1 )) and (Status<>'B') and (InAcountTime between '{1}' and '{2}')),0) as UnitChargeDone
                                                            from cm_Coperation C where 1=1 {0}", sqlwhere, start, end);

            DataSet ds = TG.DBUtility.DbHelperSQL.Query(strSql);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    cprcountall = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                    cprpaycountall = Convert.ToInt32(ds.Tables[0].Rows[0][1].ToString());
                    cprnopaycountall = Convert.ToInt32(ds.Tables[0].Rows[0][2].ToString());
                    cprbuildarea = Convert.ToDecimal(ds.Tables[0].Rows[0][3].ToString());
                    cpracountall = Convert.ToDecimal(ds.Tables[0].Rows[0][4].ToString());
                    //cprnochargeall = Convert.ToDecimal(ds.Tables[0].Rows[0][5].ToString());
                    cprchargedoneall = Convert.ToDecimal(ds.Tables[0].Rows[0][5].ToString());
                    cprnopayacount = cpracountall - cprchargedoneall;
                }
            }
            //合同目标
            //if (year == "")
            //{
            //    year = DateTime.Now.Year.ToString();
            //}
            string strSqlAllot = "";
            string strSqlAllotC = "";
            if (year == "")
            {
                strSqlAllot = @"SELECT sum(UnitAllot) FROM cm_UnitCopAllot ";
                strSqlAllotC = @"SELECT sum(UnitAllot) FROM cm_UnitAllot ";
            }
            else
            {

                if (this.drp_year.SelectedIndex != 0 && this.drp_year2.SelectedIndex != 0)
                {
                    string year1 = this.drp_year.SelectedItem.Value;
                    string year2 = this.drp_year2.SelectedItem.Value;
                    strSqlAllot = "select isnull(sum(UnitAllot),0) from cm_UnitCopAllot where   AllotYear in ('" + year1 + "','" + year2 + "')";
                    strSqlAllotC = "select isnull(sum(UnitAllot),0) from cm_UnitAllot where AllotYear in ('" + year1 + "','" + year2 + "')";
                }
                else
                {
                    strSqlAllot = @"SELECT sum(UnitAllot) FROM cm_UnitCopAllot WHERE AllotYear='" + year + "'";
                    strSqlAllotC = @"SELECT sum(UnitAllot) FROM cm_UnitAllot WHERE AllotYear='" + year + "'";
                }
            }

            object o = TG.DBUtility.DbHelperSQL.GetSingle(strSqlAllot);
            if (o != null)
            {
                copAllallotC = Convert.ToDecimal(o.ToString());
            }
            //收费目标

            object m = TG.DBUtility.DbHelperSQL.GetSingle(strSqlAllotC);
            if (m != null)
            {
                copAllallot = Convert.ToDecimal(m.ToString());
            }

        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                    this.drp_year1.Items.Add(list[i]);
                    this.drp_year2.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            //if (this.drp_year.Items.FindByText(curyear) != null)
            //{
            //    this.drp_year.Items.FindByText(curyear).Selected = true;
            //}
            if (this.drp_year1.Items.FindByText(curyear) != null)
            {
                this.drp_year1.Items.FindByText(curyear).Selected = true;
            }
            //if (this.drp_year2.Items.FindByText(curyear) != null)
            //{
            //    this.drp_year2.Items.FindByText(curyear).Selected = true;
            //}
        }
        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drpunit.CheckNodes(curUnit, true);
        }

        //创建时间查询条件
        protected void CreateTimeQuery()
        {
            string start = this.txtdateStart.Value.Trim();
            string end = this.txtdateEnd.Value.Trim();
            string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            if (start == "" && end != "")
            {
                start = curtime;
            }
            else if (start != "" && end == "")
            {
                end = curtime;
            }
            else if (start == "" && end == "")
            {
                start = curtime;
                end = curtime;
            }

            ChargeStartTime = start;
            ChargeEndTime = end;
        }
        protected void CreateTimeQueryDone()
        {
            string start = this.txt_start.Value.Trim();
            string end = this.txt_end.Value.Trim();
            string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            if (start == "" && end != "")
            {
                start = curtime;
            }
            else if (start != "" && end == "")
            {
                end = curtime;
            }
            else if (start == "" && end == "")
            {
                start = curtime;
                end = curtime;
            }

            ChargeStartTime = start;
            ChargeEndTime = end;
        }
        //统计提示话术
        protected void CreateTipQuery()
        {
            string strTip = "";

            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);
            bool IsCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (IsCheckAll)
            {
                //全部权限
                if (IsCheckAllPower == "1")
                {
                    strTip += "全院部门";
                }
                else
                {
                    //部门权限
                    string unitname = (new TG.BLL.tg_unit().GetModel(UserUnitNo).unit_Name);
                    strTip += unitname;
                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    strTip += string.Format("[{0}]", unitname);
                }
            }
            //选中了合同时间段
            if (this.hid_time.Value == "1")
            {
                string startyear = this.txt_year1.Value;
                string endyear = this.txt_year2.Value;
                if (startyear == "" && endyear != "")
                {
                    strTip += Convert.ToDateTime(endyear).Year.ToString()+"年";                  
                }
                else if (startyear != "" && endyear == "")
                {
                    strTip += Convert.ToDateTime(startyear).Year.ToString() + "年";
                }
                else if (startyear != "" && endyear != "")
                {
                    strTip += Convert.ToDateTime(startyear).Year.ToString() + "年";
                }
                else
                {
                    strTip += "全部年份";
                }
            }
            else
            {
                //年份
                if (this.drp_year1.SelectedIndex == 0)
                {
                    strTip += "全部年份";
                }
                else
                {
                    strTip += this.drp_year1.SelectedItem.Text.Trim() + "年";
                    if (this.drpJidu.SelectedIndex>0&&this.drpMonth.SelectedIndex==0)
                    {
                        strTip += this.drpJidu.SelectedItem.Text;
                    }
                    else if (this.drpJidu.SelectedIndex== 0 && this.drpMonth.SelectedIndex> 0)
                    {
                        strTip += this.drpMonth.SelectedValue+"月";
                    }
                    
                }
            }
           
            //收款
            strTip += "签订合同数，合同面积，签订合同额，合同目标值，产值目标值与截止" + ChargeStartTime + "到" + ChargeEndTime + "合同财务收款额统计 ";

            this.lbl_tip.Text = strTip;
            this.lbl_tiptitle.Text = "(全院合计包含隐藏的部门)";
        }
        protected void CreateTipQueryDone()
        {
            string strTip = "";
            string unitName = this.HiddenlabUnit.Value;
            if (IsCheckAllPower == "1")
            {
                if (unitName.Length > 0)
                {
                    if (unitName.Contains("全院部门"))
                    {
                        strTip += "全院";
                    }
                    else
                    {
                        strTip += unitName.Trim();
                    }
                }
                else
                {
                    strTip += "全院";
                }
            }
            else
            {
                strTip += new TG.BLL.tg_unit().GetModel(UserUnitNo).unit_Name;

            }
            if (this.drp_year.SelectedIndex != 0 && this.drp_year2.SelectedIndex != 0)
            {

                strTip += this.drp_year.SelectedItem.Text.Trim() + "年-" + this.drp_year2.SelectedItem.Text.Trim() + "年";
            }
            else if (this.drp_year.SelectedIndex == 0 && this.drp_year2.SelectedIndex != 0)
            {
                strTip += this.drp_year2.SelectedItem.Text.Trim() + "年";
            }
            else if (this.drp_year.SelectedIndex != 0 && this.drp_year2.SelectedIndex == 0)
            {
                strTip += this.drp_year.SelectedItem.Text.Trim() + "年";
            }
            else
            {
                strTip += "全部年份";
            }



            //收款
            strTip += "签订合同数，合同面积，签订合同额，合同目标值，产值目标值与截止" + ChargeStartTime + "到" + ChargeEndTime + "合同财务收款额统计 ";

            this.lbl_tip.Text = strTip;
            this.lbl_tiptitle.Text = "(全院合计包含隐藏的部门)";
        }
        //生产部门改变重新绑定数据
        protected void drp_unitLone_SelectedIndexChanged(object sender, EventArgs e)
        {
            //查询按钮值更改
            this.SelectType.Value = "0";
            //this.txtdateStart.Value = this.txtdateStart.Value;
            //this.txtdateEnd.Value = this.txtdateEnd.Value;
            BindUnitAllot();
        }

        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btn_export_Click(object sender, EventArgs e)
        {
            CreateTimeQueryDone();
            CreateTipQueryDone();

            DataTable dt = GetUnitAllotDone().Tables[0];

            string modelPath = " ~/TemplateXls/CoperationChargeYearDetails.xls";

            ExprotExcel(dt, modelPath);
        }

        private void ExprotExcel(DataTable dt, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //标题样式
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 9;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheetAt(0);
            if (ws == null)
                ws = wb.GetSheetAt(0);
            //读取标题
            IRow dataRowTitle = ws.GetRow(0);
            ICell celltitle = dataRowTitle.GetCell(0);
            dataRowTitle.GetCell(0).SetCellValue(this.lbl_tip.Text.Trim());
            //dataRowTitle.GetCell(0).CellStyle = style1;
            ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 10));
            //在这里进行Excel分类的切割
            //设计
            DataTable dtsj = dt.Clone();
            //多营
            DataTable dtdj = dt.Clone();
            //增加监理和勘察分开
            DataTable dtkc = dt.Clone();
            DataTable dtjl = dt.Clone();
            //管理部门
            DataTable dtel = dt.Clone();
            //238是设计所的
            //254是多经部门
            //其他的就是管理部门可以
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (unitparenrID(dt.Rows[i]["unit_ID"].ToString()) == "238")
                {
                    if (dt.Rows[i]["unit_ID"].ToString() == "250")//勘察
                    {
                        dtkc.ImportRow(dt.Rows[i]);
                    }
                    else if (dt.Rows[i]["unit_ID"].ToString() == "253")//监理
                    {
                        dtjl.ImportRow(dt.Rows[i]);
                    }
                    else//其他
                    {
                        dtsj.ImportRow(dt.Rows[i]);
                    }



                }
                else if (unitparenrID(dt.Rows[i]["unit_ID"].ToString()) == "254")
                {
                    dtdj.ImportRow(dt.Rows[i]);
                }
                else
                {
                    if (unitparenrID(dt.Rows[i]["unit_ID"].ToString()) != "0" || dt.Rows[i]["unit_ID"].ToString() == "230")
                    {
                        dtel.ImportRow(dt.Rows[i]);
                    }

                }
            }
            int row = 3;
            //管理部门的显示表头
            if (dtel.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.SetCellValue("管理部门部分");
                row = row + 1;
                ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 10));
                cell.CellStyle = style2;
            }
            //各个管理部门的数据
            for (int i = 0; i < dtel.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dtel.Rows[i]["unit_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["CprCount"].ToString()));

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["PayCprCount"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["NoPayCprCount"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["CprArea"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["CprAcountTargets"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["CprAcount"].ToString()));
                //合同额
                decimal cprallotyear = 0m;
                decimal cprallot = 0m;
                decimal cprpersent = 0m;
                //收费
                decimal chgallotyear = 0m;
                decimal chgallot = 0m;
                decimal chgpersent = 0m;

                //合同目标值
                if (!string.IsNullOrEmpty(dtel.Rows[i]["CprAcountTarget"].ToString()))
                {
                    cprallotyear = Convert.ToDecimal(dtel.Rows[i]["CprAcountTarget"].ToString());
                }
                //合同总额 
                if (!string.IsNullOrEmpty(dtel.Rows[i]["CprAcount"].ToString()))
                {
                    cprallot = Convert.ToDecimal(dtel.Rows[i]["CprAcount"].ToString());
                }
                //比例
                if (cprallotyear > 0)
                {
                    cprpersent = (cprallot / cprallotyear) * 100;
                }



                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(cprpersent.ToString("f2") + "%");

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["CprChargeDoneTargets"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["UnitChargeDone"].ToString()));

                //shouf
                if (!string.IsNullOrEmpty(dtel.Rows[i]["CprChargeDoneTarget"].ToString()))
                {
                    chgallotyear = Convert.ToDecimal(dtel.Rows[i]["CprChargeDoneTarget"].ToString());
                }
                if (!string.IsNullOrEmpty(dtel.Rows[i]["UnitChargeDone"].ToString()))
                {
                    chgallot = Convert.ToDecimal(dtel.Rows[i]["UnitChargeDone"].ToString());
                }
                if (chgallotyear > 0)
                {
                    chgpersent = (chgallot / chgallotyear) * 100;
                }

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(chgpersent.ToString("f2") + "%");
                row = row + 1;
            }
            //管理合计
            if (dtel.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("管理合计");
                row = row + 1;
                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");

                //cell.SetCellValue()
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellFormula("IF(F" + row + "=0,\"\",(G" + row + "/F" + row + ")*100)");
                //"IF(B" + index + "=0,\" \",(C" + index + "-B" + index + ")/B" + index + "*100)"
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(J3:J" + (row - 1) + ")");

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellFormula("IF(I" + row + "=0,\"\",(J" + row + "/I" + row + ")*100)");

            }
            //设计所表头           
            if (dtsj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.SetCellValue("设计、监理、勘察部分");
                row = row + 1;
                ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 10));
                cell.CellStyle = style2;
            }
            //各设计列表
            for (int i = 0; i < dtsj.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dtsj.Rows[i]["unit_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["CprCount"].ToString()));

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["PayCprCount"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["NoPayCprCount"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["CprArea"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["CprAcountTargets"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["CprAcount"].ToString()));
                //合同额
                decimal cprallotyear = 0m;
                decimal cprallot = 0m;
                decimal cprpersent = 0m;
                //收费
                decimal chgallotyear = 0m;
                decimal chgallot = 0m;
                decimal chgpersent = 0m;

                //合同目标值
                if (!string.IsNullOrEmpty(dtsj.Rows[i]["CprAcountTarget"].ToString()))
                {
                    cprallotyear = Convert.ToDecimal(dtsj.Rows[i]["CprAcountTarget"].ToString());
                }
                //合同总额 
                if (!string.IsNullOrEmpty(dtsj.Rows[i]["CprAcount"].ToString()))
                {
                    cprallot = Convert.ToDecimal(dtsj.Rows[i]["CprAcount"].ToString());
                }
                //比例
                if (cprallotyear > 0)
                {
                    cprpersent = (cprallot / cprallotyear) * 100;
                }



                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(cprpersent.ToString("f2") + "%");

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["CprChargeDoneTargets"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["UnitChargeDone"].ToString()));

                //shouf
                if (!string.IsNullOrEmpty(dtsj.Rows[i]["CprChargeDoneTarget"].ToString()))
                {
                    chgallotyear = Convert.ToDecimal(dtsj.Rows[i]["CprChargeDoneTarget"].ToString());
                }
                if (!string.IsNullOrEmpty(dtsj.Rows[i]["UnitChargeDone"].ToString()))
                {
                    chgallot = Convert.ToDecimal(dtsj.Rows[i]["UnitChargeDone"].ToString());
                }
                if (chgallotyear > 0)
                {
                    chgpersent = (chgallot / chgallotyear) * 100;
                }

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(chgpersent.ToString("f2") + "%");
                row = row + 1;
            }
            //设计所合计
            if (dtsj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("设计所合计");
                row = row + 1;
                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");
                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");
                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");
                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");


                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellFormula("IF(F" + row + "=0,\"\",(G" + row + "/F" + row + ")*100)");

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(J" + (row - dtsj.Rows.Count) + ":J" + (row - 1) + ")");
                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellFormula("IF(I" + row + "=0,\"\",(J" + row + "/I" + row + ")*100)");

                //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
            }
            //勘察&设计所合计一下
            if (dtkc.Rows.Count > 0)
            {
                for (int i = 0; i < dtkc.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtkc.Rows[i]["unit_Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["CprCount"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["PayCprCount"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["NoPayCprCount"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["CprArea"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["CprAcountTargets"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["CprAcount"].ToString()));
                    //合同额
                    decimal cprallotyear = 0m;
                    decimal cprallot = 0m;
                    decimal cprpersent = 0m;
                    //收费
                    decimal chgallotyear = 0m;
                    decimal chgallot = 0m;
                    decimal chgpersent = 0m;

                    //合同目标值
                    if (!string.IsNullOrEmpty(dtkc.Rows[i]["CprAcountTarget"].ToString()))
                    {
                        cprallotyear = Convert.ToDecimal(dtkc.Rows[i]["CprAcountTarget"].ToString());
                    }
                    //合同总额 
                    if (!string.IsNullOrEmpty(dtkc.Rows[i]["CprAcount"].ToString()))
                    {
                        cprallot = Convert.ToDecimal(dtkc.Rows[i]["CprAcount"].ToString());
                    }
                    //比例
                    if (cprallotyear > 0)
                    {
                        cprpersent = (cprallot / cprallotyear) * 100;
                    }
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(cprpersent.ToString("f2") + "%");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["CprChargeDoneTargets"].ToString()));

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["UnitChargeDone"].ToString()));

                    //shouf
                    if (!string.IsNullOrEmpty(dtkc.Rows[i]["CprChargeDoneTarget"].ToString()))
                    {
                        chgallotyear = Convert.ToDecimal(dtkc.Rows[i]["CprChargeDoneTarget"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dtkc.Rows[i]["UnitChargeDone"].ToString()))
                    {
                        chgallot = Convert.ToDecimal(dtkc.Rows[i]["UnitChargeDone"].ToString());
                    }
                    if (chgallotyear > 0)
                    {
                        chgpersent = (chgallot / chgallotyear) * 100;
                    }

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(chgpersent.ToString("f2") + "%");
                    row = row + 1;
                }
                var dataRowk = ws.GetRow(row);//读行
                if (dataRowk == null)
                    dataRowk = ws.CreateRow(row);//生成行
                var cellk = dataRowk.CreateCell(0);
                if (cellk == null)
                    cellk = dataRowk.CreateCell(0);
                cellk.CellStyle = style2;
                cellk.SetCellValue("勘察、设计合计");
                row = row + 1;
                cellk = dataRowk.CreateCell(1);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                cellk = dataRowk.CreateCell(2);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(3);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(4);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(5);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(6);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(7);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("IF(F" + row + "=0,\"\",(G" + row + "/F" + row + ")*100)");

                cellk = dataRowk.CreateCell(8);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(9);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(10);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("IF(I" + row + "=0,\"\",(J" + row + "/I" + row + ")*100)");
                //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
            }
            //最后是监理
            if (dtjl.Rows.Count > 0)
            {
                for (int i = 0; i < dtjl.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtjl.Rows[i]["unit_Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["CprCount"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["PayCprCount"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["NoPayCprCount"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["CprArea"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["CprAcountTargets"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["CprAcount"].ToString()));
                    //合同额
                    decimal cprallotyear = 0m;
                    decimal cprallot = 0m;
                    decimal cprpersent = 0m;
                    //收费
                    decimal chgallotyear = 0m;
                    decimal chgallot = 0m;
                    decimal chgpersent = 0m;

                    //合同目标值
                    if (!string.IsNullOrEmpty(dtjl.Rows[i]["CprAcountTarget"].ToString()))
                    {
                        cprallotyear = Convert.ToDecimal(dtjl.Rows[i]["CprAcountTarget"].ToString());
                    }
                    //合同总额 
                    if (!string.IsNullOrEmpty(dtjl.Rows[i]["CprAcount"].ToString()))
                    {
                        cprallot = Convert.ToDecimal(dtjl.Rows[i]["CprAcount"].ToString());
                    }
                    //比例
                    if (cprallotyear > 0)
                    {
                        cprpersent = (cprallot / cprallotyear) * 100;
                    }



                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(cprpersent.ToString("f2") + "%");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["CprChargeDoneTargets"].ToString()));

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["UnitChargeDone"].ToString()));

                    //shouf
                    if (!string.IsNullOrEmpty(dtjl.Rows[i]["CprChargeDoneTarget"].ToString()))
                    {
                        chgallotyear = Convert.ToDecimal(dtjl.Rows[i]["CprChargeDoneTarget"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dtjl.Rows[i]["UnitChargeDone"].ToString()))
                    {
                        chgallot = Convert.ToDecimal(dtjl.Rows[i]["UnitChargeDone"].ToString());
                    }
                    if (chgallotyear > 0)
                    {
                        chgpersent = (chgallot / chgallotyear) * 100;
                    }

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(chgpersent.ToString("f2") + "%");
                    row = row + 1;
                }
                //主营合计

                var dataRowk = ws.GetRow(row);//读行
                if (dataRowk == null)
                    dataRowk = ws.CreateRow(row);//生成行
                var cellk = dataRowk.CreateCell(0);
                if (cellk == null)
                    cellk = dataRowk.CreateCell(0);
                cellk.CellStyle = style2;
                cellk.SetCellValue("主营合计");
                row = row + 1;
                cellk = dataRowk.CreateCell(1);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                cellk = dataRowk.CreateCell(2);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(3);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");
                cellk = dataRowk.CreateCell(4);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");
                cellk = dataRowk.CreateCell(5);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");
                cellk = dataRowk.CreateCell(6);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(7);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("IF(F" + row + "=0,\"\",(G" + row + "/F" + row + ")*100)");

                cellk = dataRowk.CreateCell(8);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(9);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(10);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("IF(I" + row + "=0,\"\",(J" + row + "/I" + row + ")*100)");

            }
            //多径部门表头
            if (dtdj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.SetCellValue("多种经营部分");
                row = row + 1;
                ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 10));
                cell.CellStyle = style2;
            }
            //多径部门详细列表
            for (int i = 0; i < dtdj.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dtdj.Rows[i]["unit_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["CprCount"].ToString()));

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["PayCprCount"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["NoPayCprCount"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["CprArea"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["CprAcountTargets"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["CprAcount"].ToString()));
                //合同额
                decimal cprallotyear = 0m;
                decimal cprallot = 0m;
                decimal cprpersent = 0m;
                //收费
                decimal chgallotyear = 0m;
                decimal chgallot = 0m;
                decimal chgpersent = 0m;

                //合同目标值
                if (!string.IsNullOrEmpty(dtdj.Rows[i]["CprAcountTarget"].ToString()))
                {
                    cprallotyear = Convert.ToDecimal(dtdj.Rows[i]["CprAcountTarget"].ToString());
                }
                //合同总额 
                if (!string.IsNullOrEmpty(dtdj.Rows[i]["CprAcount"].ToString()))
                {
                    cprallot = Convert.ToDecimal(dtdj.Rows[i]["CprAcount"].ToString());
                }
                //比例
                if (cprallotyear > 0)
                {
                    cprpersent = (cprallot / cprallotyear) * 100;
                }



                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(cprpersent.ToString("f2") + "%");

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["CprChargeDoneTargets"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["UnitChargeDone"].ToString()));

                //shouf
                if (!string.IsNullOrEmpty(dtdj.Rows[i]["CprChargeDoneTarget"].ToString()))
                {
                    chgallotyear = Convert.ToDecimal(dtdj.Rows[i]["CprChargeDoneTarget"].ToString());
                }
                if (!string.IsNullOrEmpty(dtdj.Rows[i]["UnitChargeDone"].ToString()))
                {
                    chgallot = Convert.ToDecimal(dtdj.Rows[i]["UnitChargeDone"].ToString());
                }
                if (chgallotyear > 0)
                {
                    chgpersent = (chgallot / chgallotyear) * 100;
                }

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(chgpersent.ToString("f2") + "%");
                row = row + 1;
            }
            //多径合计
            if (dtdj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("多经营所合计");
                row = row + 1;
                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");
                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");
                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellFormula("IF(F" + row + "=0,\"\",(G" + row + "/F" + row + ")*100)");

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(J" + (row - dtdj.Rows.Count) + ":J" + (row - 1) + ")");

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellFormula("IF(I" + row + "=0,\"\",(J" + row + "/I" + row + ")*100)");
            }
            //总院合计
            IRow bottomrow = ws.CreateRow(row);
            ICell bottomcell = bottomrow.CreateCell(0);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue("全院合计：");

            bottomcell = bottomrow.CreateCell(1);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(cprcountall.ToString("f4"));

            bottomcell = bottomrow.CreateCell(2);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(cprpaycountall.ToString("f4"));

            bottomcell = bottomrow.CreateCell(3);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(cprnopaycountall.ToString("f4"));

            bottomcell = bottomrow.CreateCell(4);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(cprbuildarea.ToString("f4"));

            bottomcell = bottomrow.CreateCell(5);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(copAllallotC.ToString("f4"));

            bottomcell = bottomrow.CreateCell(6);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(cpracountall.ToString("f4"));

            bottomcell = bottomrow.CreateCell(7);
            bottomcell.CellStyle = style1;
            if (copAllallotC == 0)
            {
                bottomcell.SetCellValue("0%");
            }
            else
            {
                bottomcell.SetCellValue(((cpracountall / copAllallotC) * 100).ToString("f4") + "%");
            }


            bottomcell = bottomrow.CreateCell(8);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(copAllallot.ToString("f4"));

            bottomcell = bottomrow.CreateCell(9);
            bottomcell.CellStyle = style1;
            bottomcell.SetCellValue(cprchargedoneall.ToString("f4"));

            bottomcell = bottomrow.CreateCell(10);
            bottomcell.CellStyle = style1;
            if (copAllallot == 0)
            {
                bottomcell.SetCellValue("0%");
            }
            else
            {
                bottomcell.SetCellValue(((cprchargedoneall / copAllallot) * 100).ToString("f4") + "%");
            }


            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(lbl_tip.Text + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public string unitparenrID(string unitid)
        {
            return bll_unit.GetModel(int.Parse(unitid)).unit_ParentID.ToString();
        }


        protected void btn_Search_Click(object sender, EventArgs e)
        {
            //查询按钮值更改
            this.SelectType.Value = "1";
            CreateTimeQueryDone();
            // GetLabUnit();

            DataSet ds = GetUnitAllotDone();

            //绑定数据

            this.grid_allot.DataSource = ds;
            this.grid_allot.DataBind();
            this.grid_allot.UseAccessibleHeader = true;
            if (this.grid_allot.HeaderRow != null)
            {
                this.grid_allot.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            CreateTipQueryDone();
        }
        //暂无用 自定义部门查询与普通查询部门公用一个。
        private void GetLabUnit()
        {
            string unithtml = "";
            List<TG.Model.tg_unit> list = new List<Model.tg_unit>();
            TG.BLL.tg_unit bllUnit = new BLL.tg_unit();
            string unitids = this.HiddenlabUnitID.Value;
            if (unitids.Length > 0)
            {
                string[] unitarray = unitids.Split(',');
                for (int i = 0; i < unitarray.Length - 1; i++)
                {
                    if (unitarray[i] != "-1")
                    {
                        TG.Model.tg_unit model = new Model.tg_unit();
                        model = bllUnit.GetModel(int.Parse(unitarray[i]));
                        list.Add(model);
                    }
                    else
                    {
                        unithtml = "<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"-1\" unitname=\"-----全院部门-----\">全院部门<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>";
                    }

                }
            }
            if (list.Count > 0)
            {
                foreach (TG.Model.tg_unit item in list)
                {
                    unithtml += "<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + item.unit_ID + "\" unitname=\"" + item.unit_Name + "\">" + item.unit_Name + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>";
                }
            }
            this.labUnit.InnerHtml = unithtml;
        }
        protected DataSet GetUnitAllotDone()
        {
            //获取部门id
            this.HiddenlabUnitID.Value = GetCheckedNodes();
            this.HiddenlabUnit.Value = GetCheckedNodeName();
            StringBuilder strsql = new StringBuilder();

            // 已签订合同数
            string doneSignnum1 = this.doneSignnum1.Value.Trim();
            string doneSignnum2 = this.doneSignnum2.Value.Trim();
            if (!string.IsNullOrEmpty(doneSignnum1) && string.IsNullOrEmpty(doneSignnum2))
            {
                strsql.Append(" AND CprCount >= " + doneSignnum1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(doneSignnum1) && !string.IsNullOrEmpty(doneSignnum2))
            {
                strsql.Append(" AND CprCount <= " + doneSignnum2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(doneSignnum1) && !string.IsNullOrEmpty(doneSignnum2))
            {
                strsql.Append(" AND (CprCount >=" + doneSignnum1.Trim() + " AND CprCount <= " + doneSignnum2.Trim() + ") ");
            }
            //已收款合同数
            string doneChargenum1 = this.doneChargenum1.Value.Trim();
            string doneChargenum2 = this.doneChargenum2.Value.Trim();
            if (!string.IsNullOrEmpty(doneChargenum1) && string.IsNullOrEmpty(doneChargenum2))
            {
                strsql.Append(" AND PayCprCount >= " + doneChargenum1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(doneChargenum1) && !string.IsNullOrEmpty(doneChargenum2))
            {
                strsql.Append(" AND PayCprCount <= " + doneChargenum2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(doneChargenum1) && !string.IsNullOrEmpty(doneChargenum2))
            {
                strsql.Append(" AND (PayCprCount >=" + doneChargenum1.Trim() + " AND PayCprCount <= " + doneChargenum2.Trim() + ") ");
            }
            //未收款合同数
            string nodoneChargenum1 = this.nodoneChargenum1.Value.Trim();
            string nodoneChargenum2 = this.nodoneChargenum2.Value.Trim();
            if (!string.IsNullOrEmpty(nodoneChargenum1) && string.IsNullOrEmpty(nodoneChargenum2))
            {
                strsql.Append(" AND NoPayCprCount >= " + nodoneChargenum1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(nodoneChargenum1) && !string.IsNullOrEmpty(nodoneChargenum2))
            {
                strsql.Append(" AND NoPayCprCount <= " + nodoneChargenum2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(nodoneChargenum1) && !string.IsNullOrEmpty(nodoneChargenum2))
            {
                strsql.Append(" AND (NoPayCprCount >=" + nodoneChargenum1.Trim() + " AND NoPayCprCount <= " + nodoneChargenum2.Trim() + ") ");
            }
            //合同面积
            string copArea1 = this.copArea1.Value.Trim();
            string copArea2 = this.copArea2.Value.Trim();
            if (!string.IsNullOrEmpty(copArea1) && string.IsNullOrEmpty(copArea2))
            {
                strsql.Append(" AND CprArea >= " + copArea1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(copArea1) && !string.IsNullOrEmpty(copArea2))
            {
                strsql.Append(" AND CprArea <= " + copArea2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(copArea1) && !string.IsNullOrEmpty(copArea2))
            {
                strsql.Append(" AND (CprArea >=" + copArea1.Trim() + " AND CprArea <= " + copArea2.Trim() + ") ");
            }
            //合同目标值
            string copTarget1 = this.copTarget1.Value.Trim();
            string copTarget2 = this.copTarget2.Value.Trim();
            if (!string.IsNullOrEmpty(copTarget1) && string.IsNullOrEmpty(copTarget2))
            {
                strsql.Append(" AND CprAcountTarget >= " + copTarget1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(copTarget1) && !string.IsNullOrEmpty(copTarget2))
            {
                strsql.Append(" AND CprAcountTarget <= " + copTarget2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(copTarget1) && !string.IsNullOrEmpty(copTarget2))
            {
                strsql.Append(" AND (CprAcountTarget >=" + copTarget1.Trim() + " AND CprAcountTarget <= " + copTarget2.Trim() + ") ");
            }
            //合同总额
            string copAcount1 = this.copAcount1.Value.Trim();
            string copAcount2 = this.copAcount2.Value.Trim();
            if (!string.IsNullOrEmpty(copAcount1) && string.IsNullOrEmpty(copAcount2))
            {
                strsql.Append(" AND CprAcount >= " + copAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(copAcount1) && !string.IsNullOrEmpty(copAcount2))
            {
                strsql.Append(" AND CprAcount <= " + copAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(copAcount1) && !string.IsNullOrEmpty(copAcount2))
            {
                strsql.Append(" AND (CprAcount >=" + copAcount1.Trim() + " AND CprAcount <= " + copAcount2.Trim() + ") ");
            }
            //产值目标值
            string chargeTarget1 = this.chargeTarget1.Value.Trim();
            string chargeTarget2 = this.chargeTarget2.Value.Trim();
            if (!string.IsNullOrEmpty(chargeTarget1) && string.IsNullOrEmpty(chargeTarget2))
            {
                strsql.Append(" AND CprChargeDoneTarget >= " + chargeTarget1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(chargeTarget1) && !string.IsNullOrEmpty(chargeTarget2))
            {
                strsql.Append(" AND CprChargeDoneTarget <= " + chargeTarget2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(chargeTarget1) && !string.IsNullOrEmpty(chargeTarget2))
            {
                strsql.Append(" AND (CprChargeDoneTarget >=" + chargeTarget1.Trim() + " AND CprChargeDoneTarget <= " + chargeTarget2.Trim() + ") ");
            }
            //已收费额
            string doneChargeacount1 = this.doneChargeacount1.Value.Trim();
            string doneChargeacount2 = this.doneChargeacount2.Value.Trim();
            if (!string.IsNullOrEmpty(doneChargeacount1) && string.IsNullOrEmpty(doneChargeacount2))
            {
                strsql.Append(" AND UnitChargeDone >= " + doneChargeacount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(doneChargeacount1) && !string.IsNullOrEmpty(doneChargeacount2))
            {
                strsql.Append(" AND UnitChargeDone <= " + doneChargeacount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(doneChargeacount1) && !string.IsNullOrEmpty(doneChargeacount2))
            {
                strsql.Append(" AND (UnitChargeDone >=" + doneChargeacount1.Trim() + " AND  UnitChargeDone<= " + doneChargeacount2.Trim() + ") ");
            }

            //合同年份
            string str_year = "";
            if (this.drp_year.SelectedIndex != 0 && this.drp_year2.SelectedIndex != 0)
            {
                string year = this.drp_year.SelectedItem.Value;
                string year2 = this.drp_year2.SelectedItem.Value;
                if (int.Parse(year) > int.Parse(year2))
                {
                    str_year = year2;
                }
                else
                {
                    str_year = year;
                }

            }
            else if (this.drp_year.SelectedIndex == 0 && this.drp_year2.SelectedIndex != 0)
            {
                string year = this.drp_year.SelectedItem.Value;
                string year2 = this.drp_year2.SelectedItem.Value;
                str_year = year2;
            }
            else if (this.drp_year.SelectedIndex != 0 && this.drp_year2.SelectedIndex == 0)
            {
                string year = this.drp_year.SelectedItem.Value;
                string year2 = this.drp_year2.SelectedItem.Value;
                str_year = year;
            }

            string strWhere = "";
            string query = "";
            //部门权限
            if (IsCheckAllPower == "0")
            {
                strWhere = " AND unit_ID in( " + UserUnitNo + ")";
            }

            //正常查询
            if (this.SelectType.Value == "0")
            {
                CreateTipQuery();
                string unitlist = GetCheckedNodes();
                strWhere = string.Format(" AND unit_ID IN ({0}) ", unitlist);

                if (this.drp_year1.SelectedIndex!=0)
                {
                    str_year = this.drp_year1.SelectedValue;
                }
                string startyear = this.txt_year1.Value;
                string endyear = this.txt_year2.Value;
                string cbxtimetype = this.hid_time.Value;
                string jidu = this.drpJidu.SelectedValue;
                string month = this.drpMonth.SelectedValue;

                //合同时间段 
                if (cbxtimetype == "1")
                {
                    string curtime = DateTime.Now.Year.ToString();
                    if (startyear == "" && endyear != "")
                    {
                        query = string.Format(" AND cpr_SignDate<='{0}'", endyear.Trim());
                        str_year = Convert.ToDateTime(endyear).Year.ToString();
                    }
                    else if (startyear != "" && endyear == "")
                    {
                        query = string.Format(" AND cpr_SignDate>='{0}'", startyear.Trim());
                        str_year = Convert.ToDateTime(startyear).Year.ToString();
                    }
                    else if (startyear != "" && endyear != "")
                    {
                        query = string.Format(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", startyear.Trim(), endyear.Trim());
                        str_year = Convert.ToDateTime(startyear).Year.ToString();
                    }
                    else
                    {
                        str_year = "";
                    }

                }
                else
                {
                   
                    //年份
                    if (!string.IsNullOrEmpty(str_year) && str_year != "-1")
                    {
                        //年
                        string stryear = str_year;
                        //季度
                        string strjidu = jidu;
                        //月
                        string stryue = month;
                        //
                        string start = "";
                        string end = "";
                        //年
                        if (strjidu == "0" && stryue == "0")
                        {
                            start = stryear + "-01-01 00:00:00";
                            end = stryear + "-12-31 23:59:59 ";
                        }
                        else if (strjidu != "0" && stryue == "0") //年季度
                        {
                            start = stryear;
                            end = stryear;
                            switch (strjidu)
                            {
                                case "1":
                                    start += "-01-01 00:00:00";
                                    end += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    start += "-04-01 00:00:00";
                                    end += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    start += "-07-01 00:00:00";
                                    end += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    start += "-10-01 00:00:00";
                                    end += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (strjidu == "0" && stryue != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                            start = stryear + "-" + stryue + "-01 00:00:00";
                            end = stryear + "-" + stryue + "-" + days + " 23:59:59";
                        }
                        query = string.Format(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", start.Trim(), end.Trim());

                    }
                }
               

            }//自定义查询
            else
            {
                CreateTipQueryDone();
                //生产部门drp_unitLone
                string unit = this.HiddenlabUnitID.Value;
                if (unit.Length > 0)
                {
                    // unit = unit.Substring(0, unit.Length - 1);

                    if (unit.Trim() != "-1" && !string.IsNullOrEmpty(unit.Trim()))
                    {
                        strWhere = " AND unit_ID in( " + unit + ")";
                    }
                }

                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

                if (strsql.Length > 0)
                {
                    strWhere += strsql.ToString();
                }


                //年份
                if (str_year != "")
                {
                    if (this.drp_year.SelectedIndex != 0 && this.drp_year2.SelectedIndex != 0)
                    {
                        string year = this.drp_year.SelectedItem.Value;
                        string year2 = this.drp_year2.SelectedItem.Value;
                        query = string.Format(" and cpr_SignDate between '{0}-01-01' and '{1}-12-31'", year, year2);
                    }
                    else
                    {
                        query = string.Format(" and cpr_SignDate between '{0}-01-01' and '{1}-12-31'", str_year, str_year);
                    }
                }

            }
            //合计
            CoperationCountAll(ChargeStartTime, ChargeEndTime, str_year, query);
            string unitCopAllotSql = "";
            string unitAllotSql = "";
            if (str_year != "")
            {
                if (this.drp_year.SelectedIndex != 0 && this.drp_year2.SelectedIndex != 0)
                {
                    string year = this.drp_year.SelectedItem.Value;
                    string year2 = this.drp_year2.SelectedItem.Value;
                    unitCopAllotSql = "select isnull(sum(UnitAllot),0) from cm_UnitCopAllot where (UnitID=unit_ID) and   AllotYear in ('" + year + "','" + year2 + "')";
                    unitAllotSql = "select isnull(sum(UnitAllot),0) from cm_UnitAllot where (UnitID=unit_ID) and AllotYear in ('" + year + "','" + year2 + "')";
                }
                else
                {
                    unitCopAllotSql = "select isnull(UnitAllot,0) from cm_UnitCopAllot where (UnitID=unit_ID) and  AllotYear in ('" + str_year + "','" + str_year + "')";
                    unitAllotSql = "select isnull(UnitAllot,0) from cm_UnitAllot where (UnitID=unit_ID)  and  AllotYear in ('" + str_year + "','" + str_year + "')";
                }
            }
            else
            {
                unitCopAllotSql = "select isnull(sum(UnitAllot),0) from cm_UnitCopAllot where (UnitID=unit_ID)";
                unitAllotSql = "select isnull(sum(UnitAllot),0) from cm_UnitAllot where (UnitID=unit_ID)";
            }


            //Sql查询统计结果
            string strSql = string.Format(@"select *,isnull(c.CprAcountTarget,0)as CprAcountTargets,isnull(c.CprChargeDoneTarget,0)as CprChargeDoneTargets from (select unit_ID,unit_Name,
                                        --已签合同数量
                                        (select COUNT(*) from cm_Coperation where (cpr_Unit=unit_Name) {0}) as CprCount,
                                        --已付款合同数量
                                        (select COUNT(*) from cm_Coperation C where (cpr_Unit=unit_Name) and (Select COUNT(*) from cm_ProjectCharge where cprID=C.cpr_ID and  Status<>'B')>0 {0}) as PayCprCount,
                                        --未付款合同数量
                                        (select COUNT(*) from cm_Coperation C where (cpr_Unit=unit_Name) and (Select COUNT(*) from cm_ProjectCharge where cprID=C.cpr_ID and  Status<>'B')=0 {0}) as NoPayCprCount,
                                        --合同面积
                                        (select isnull(SUM(convert(decimal,BuildArea)),0) from cm_Coperation where (cpr_Unit=unit_Name) {0}) as CprArea,
                                        --已签合同总额
                                        (select isnull(SUM(cpr_Acount),0) from cm_Coperation where (cpr_Unit=unit_Name) {0}) as CprAcount,
                                        --合同额目标值
                                        ({3}) as CprAcountTarget,
                                        --已收费额
                                        (select isnull(SUM(Acount),0) from cm_ProjectCharge where (cprID in (select cpr_ID from cm_Coperation where cpr_Unit=unit_Name )) and (Status<>'B') and (InAcountTime between '{1}' and '{2}')) as UnitChargeDone,
                                        --产值目标值
                                        ({4}) as CprChargeDoneTarget
                                        from tg_unit) c", query, ChargeStartTime, ChargeEndTime, unitCopAllotSql, unitAllotSql);
            strSql += " Where 1=1 " + strWhere + " order by unit_ID ";
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            DataSet ds = bll.GetList(strSql);
            return ds;
        }
        [AjaxPro.AjaxMethod]
        public void Recearch()
        {
            BindUnitAllot();
        }
    }
}