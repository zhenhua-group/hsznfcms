﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using TG.BLL;
using AjaxPro;
using TG.Model;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace TG.Web.LeadershipCockpit
{
    public partial class MapOfCity_gs : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(MapOfCity_gs));
        }
        /// <summary>
        /// 得到每个省份的项目数
        /// </summary>
        [AjaxMethod]
        public string GetRecordOfProvince(int ssf)
        {
            MapOfCity_gsBP mapBP = new MapOfCity_gsBP();

            List<MapOfChinaEntity> provinces = mapBP.GetProjectCountByProvinceName();

            return Newtonsoft.Json.JsonConvert.SerializeObject(provinces);
        }

        /// <summary>
        /// 根据省份名称，得到项目列表
        /// </summary>
        /// <param name="provinceName"></param>
        /// <param name="pageCurrent"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetProjectListByProvinceName(string provinceName, int pageCurrent)
        {
            List<ProjectForMap> projectList = new MapOfCity_gsBP().GetProjectListByProvinceName(provinceName, 15, pageCurrent - 1);

            return Newtonsoft.Json.JsonConvert.SerializeObject(projectList);
        }
        [AjaxMethod]
        public string GetProjectCountByProvinceName(string provinceName, int pageCurrent)
        {
            List<ProjectForMap> projectList = new MapOfCity_gsBP().GetProjectListByProvinceName(provinceName);

            return projectList.Count.ToString();
        }
        /// <summary>
        /// 根据项目SysNo得到项目信息
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetProjectInfoByProjectSysNo(int projectSysNo)
        {
            ProjectDetailForMap project = new MapOfCity_gsBP().GetProjectInfoByProjectSysNo(projectSysNo);

            return Newtonsoft.Json.JsonConvert.SerializeObject(project);
        }
        [AjaxMethod]
        public string Getstring()
        {
            return "serfdw";
        }
    }
}