﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using Geekees.Common.Controls;

namespace TG.Web.LeadershipCockpit
{
    public partial class ProjectCountBymaster : PageBase
    {
        public string unitname = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定单位
                BindUnit();
                //绑定树单位
                LoadUnitTree();
                SelectedCurUnit();

                //绑定年份
                BindYear();
                //初始年
                InitDropDownByYear();
                //统计
                CountProjectAll();

                //初始化标题              
                CreateTipQuery();
                this.titlecount.Value = ChartTip;
            }
            else
            {
                //绑定树单位
                LoadUnitTree();
            }
        }
        //检查是否需要权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //判断是否是全部查看
        protected string IsCheckAllPower
        {
            get
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>

        TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        protected void BindUnit()
        {
            string strWhere = "";
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            DataSet ds = bll_unit.GetList(strWhere);



            this.drp_unit1.DataSource = ds;
            this.drp_unit1.DataTextField = "unit_Name";
            this.drp_unit1.DataValueField = "unit_ID";
            this.drp_unit1.DataBind();

            this.drp_unit2.DataSource = ds;
            this.drp_unit2.DataTextField = "unit_Name";
            this.drp_unit2.DataValueField = "unit_ID";
            this.drp_unit2.DataBind();

            this.drp_unit3.DataSource = ds;
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();

            this.drp_unit4.DataSource = ds;
            this.drp_unit4.DataTextField = "unit_Name";
            this.drp_unit4.DataValueField = "unit_ID";
            this.drp_unit4.DataBind();
        }

        #region 绑定树部门 2015年6月19日
        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drpunit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }

            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drpunit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    //只有在权限不是全部的时候，用到这个变量。
                    unitname = dr["unit_Name"].ToString();
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drpunit.Theme = macOS;
        }
        #endregion
        /// <summary>
        /// 绑定年份
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                    this.drp_year1.Items.Add(list[i]);
                    this.drp_year2.Items.Add(list[i]);
                    this.drp_year2_1.Items.Add(list[i]);
                    this.drp_year2_2.Items.Add(list[i]);
                    this.drp_year3_1.Items.Add(list[i]);
                    this.drp_year3_2.Items.Add(list[i]);
                    this.drp_year4_1.Items.Add(list[i]);
                    this.drp_year4_2.Items.Add(list[i]);
                }
            }
        }
        ///<summary>
        ///报表名称
        ///</summary>
        protected string ChartTip { get; set; }
        // 统计类型
        protected string CountType
        {
            get
            {
                return this.drp_type.SelectedItem.Text;
            }
        }
        //统计
        protected void CountProjectAll()
        {
            if (this.drp_type.SelectedIndex == 0)
            {
                //显示
                ShowCountTypeControl("0");
                //全部
                CountProjectByAll();
            }
            else if (this.drp_type.SelectedIndex == 1)
            {
                //显示
                ShowCountTypeControl("1");
                //等级统计
                CountProjectByLevel();
            }
            else if (this.drp_type.SelectedIndex == 2)
            {
                //显示
                ShowCountTypeControl("2");
                //等级统计
                CountProjectByIndustry();
                //单位
                this.div_util3.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);
            }
            else if (this.drp_type.SelectedIndex == 3)
            {
                //显示
                ShowCountTypeControl("3");
                //等级统计
                CountProjectByIndustryScale();
                //单位
                this.div_util3.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);
            }
            else if (this.drp_type.SelectedIndex == 4)
            {
                //显示
                ShowCountTypeControl("4");
                //等级统计
                CountProjectBySrc();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void CountProjectByAll()
        {
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }

            // 项目综合统计
            // string type = "1";


            //this.GridView1.DataSource = new TG.BLL.cm_Project().CountProjectByStatus(year, unitid, type);
            //this.GridView1.DataBind();

            //this.GridView1.UseAccessibleHeader = true;
            //this.GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        /// <summary>
        /// 
        /// </summary>
        protected void CountProjectByLevel()
        {
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }


            // this.GridView2.DataSource = new TG.BLL.cm_Project().CountProjectByLevel(year, unitid);
            // this.GridView2.DataBind();
            // this.GridView2.UseAccessibleHeader = true;
            // this.GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        /// <summary>
        /// 
        /// </summary>
        protected void CountProjectByIndustry()
        {
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }

            //项目个数
            // string type = "0";
            // this.GridView3.DataSource = new TG.BLL.cm_Project().CountProjectByIndustry(year, unitid, type);
            // this.GridView3.DataBind();
            //  this.GridView3.UseAccessibleHeader = true;
            // this.GridView3.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        /// <summary>
        /// 
        /// </summary>
        protected void CountProjectByIndustryScale()
        {
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }

            //项目个数
            // string type = "1";
            // this.GridView4.DataSource = new TG.BLL.cm_Project().CountProjectByIndustry(year, unitid, type);
            // this.GridView4.DataBind();

            // this.GridView4.UseAccessibleHeader = true;
            // this.GridView4.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        /// <summary>
        /// 
        /// </summary>
        protected void CountProjectBySrc()
        {
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }

            // this.GridView5.DataSource = new TG.BLL.cm_Project().CountProjectBySrc(year, unitid);
            // this.GridView5.DataBind();

            // this.GridView5.UseAccessibleHeader = true;
            // this.GridView5.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        //统计提示话术
        protected void CreateTipQuery()
        {
            string strTip = "";
            //部门权限
            if (IsCheckAllPower == "0")
            {
                strTip = unitname.Trim();
            }
            else
            {

                if (!string.IsNullOrEmpty(this.HiddenlabUnit.Value) && !this.HiddenlabUnit.Value.Contains("全院部门") && this.HiddenlabUnit.Value != "0")
                {
                    strTip += this.HiddenlabUnit.Value.Trim();
                }
                else
                {
                    strTip += "全院";
                }

            }

            string drpyear = "";
            string drpyear1 = "";
            string drpyear2 = "";
            string yearjd = this.drpJidu.SelectedItem.Value;
            string yearyf = this.drpMonth.SelectedItem.Value;
            if (this.drp_type.SelectedValue == "0")
            {
                drpyear1 = this.drp_year1.SelectedValue;
                drpyear2 = this.drp_year2.SelectedValue;
            }
            else if (this.drp_type.SelectedValue == "1")
            {
                drpyear1 = this.drp_year2_1.SelectedValue;
                drpyear2 = this.drp_year2_2.SelectedValue;
            }
            else if (this.drp_type.SelectedValue == "2" || this.drp_type.SelectedValue == "3")
            {
                drpyear1 = this.drp_year3_1.SelectedValue;
                drpyear2 = this.drp_year3_2.SelectedValue;
            }
            else if (this.drp_type.SelectedValue == "4")
            {
                drpyear1 = this.drp_year4_1.SelectedValue;
                drpyear2 = this.drp_year4_2.SelectedValue;
            }

            if (drpyear1 == "-1" && drpyear2 != "-1")
            {
                drpyear = "小于" + drpyear2;
            }
            else if (drpyear2 == "-1" && drpyear1 != "-1")
            {
                drpyear = "大于" + drpyear1;
            }
            else if (drpyear2 != "-1" && drpyear1 != "-1")
            {
                drpyear = drpyear1 + "年至" + drpyear2;
            }
            if (yearjd != "0")
            {
                drpyear = this.drp_year.SelectedItem.Text.Trim() + this.drpJidu.SelectedItem.Text.Trim();
            }
            if (yearyf != "0")
            {
                drpyear = this.drp_year.SelectedItem.Text.Trim() + this.drpMonth.SelectedItem.Text.Trim();
            }

            //年份
            if (drpyear == "")
            {
                strTip += "全部年份";
            }
            else
            {
                strTip += drpyear;
            }

            ChartTip = strTip;
        }
        //初始年份
        protected void InitDropDownByYear()
        {
            if (this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }

        }
        //选中当前年份
        protected string SelectCurrentYear()
        {
            string year = DateTime.Now.Year.ToString();
            //选择当前年份
            this.drp_year.ClearSelection();
            if (this.drp_year.Items.FindByValue(year) != null)
            {
                this.drp_year.Items.FindByValue(year).Selected = true;
                //统计提示
                //   CreateTipQuery();
            }
            return year;
        }
        //控制对应统计类型
        protected void ShowCountTypeControl(string type)
        {
            // this.div_Container1.Visible = type == "0" ? true : false;
            // this.div_Container2.Visible = type == "1" ? true : false;
            //  this.div_Container3.Visible = type == "2" ? true : false;
            //  this.div_Container3.Visible = type == "3" ? true : false;
            //  this.div_Container4.Visible = type == "4" ? true : false;
        }

        //返回统计单位
        protected string GetUtilByDrpText(string text)
        {
            if (text.IndexOf("数量") > -1)
            {
                return "单位:个";
            }
            else if (text.IndexOf("规模") > -1)
            {
                return "单位:㎡";
            }
            else if (text.IndexOf("额") > -1)
            {
                return "单位:万元";
            }
            else
            {
                return "";
            }
        }

        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            //绑定数据
            CountProjectAll();
        }
        /// <summary>
        /// 获取所有选中部门名称
        /// </summary>
        /// <returns></returns>
        public string GetCheckedNodesId()
        {
            StringBuilder sbUnitlist = new StringBuilder("");
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);

            foreach (ASTreeViewNode node in nodes)
            {
                if (node.NodeValue == "0")
                    break;

                sbUnitlist.AppendFormat("{0},", node.NodeValue);

            }
            //判断部门不为空
            if (sbUnitlist.ToString() != "")
            {
                sbUnitlist.Remove(sbUnitlist.ToString().LastIndexOf(','), 1);

            }

            return sbUnitlist.ToString();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string modelPath = " ~/TemplateXls/ProjectCount_Status.xls";
            //统计
            CreateTipQuery();

            //默认是自定义部门
            //  string unitnamelist = this.HiddenlabUnit.Value;

            string unitidlist = GetCheckedNodesId();
            string year = this.drp_year.SelectedValue;

            //正常查询部门
            //if (this.SelectType.Value=="0")
            //{
            //    unitidlist = GetCheckedNodesId();
            //}

            //如果权限是本部门
            if (IsCheckAllPower == "0")
            {
                unitidlist = UserUnitNo.ToString();
            }

            StringBuilder sb = new StringBuilder();
            //部门id
            if (!string.IsNullOrEmpty(unitidlist) && unitidlist != "0" && unitidlist != "-1")
            {
                sb.AppendFormat(" and ID in ({0})", unitidlist);
            }

            //类型判断
            if (this.drp_type.SelectedIndex == 0)
            {
                //显示内容
                // ShowCountTypeControl("0");
                //统计              
                //string type = "1";
                //dt = new TG.BLL.cm_Project().CountProjectByStatus(year, unitid, type).Tables[0];

                string drpyear1 = "";
                string drpyear2 = "";

                string syeartime = this.drp_year1.SelectedValue;
                string eyeartime = this.drp_year2.SelectedValue;
                //年份
                //年份
                if ((!string.IsNullOrEmpty(syeartime) && syeartime != "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                }
                else if ((!string.IsNullOrEmpty(eyeartime) && eyeartime != "-1") && (string.IsNullOrEmpty(syeartime) || syeartime == "-1"))
                {
                    drpyear2 = drpyear2 + "-12-31 23:59:59";
                }
                else if ((string.IsNullOrEmpty(syeartime) || syeartime == "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = "";
                    drpyear2 = "";
                }
                else
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                    drpyear2 = eyeartime + "-12-31 23:59:59";
                }
                string projectcount1 = this.project_count1.Value;
                string projectcount2 = this.project_count2.Value;
                string projectarea1 = this.project_area1.Value;
                string projectarea2 = this.project_area2.Value;
                string projectauditcount1 = this.project_auditcount1.Value;
                string projectauditcount2 = this.project_auditcount2.Value;
                string projectingcount1 = this.project_ingcount1.Value;
                string projectingcount2 = this.project_ingcount2.Value;
                string projectstopcount1 = this.project_stopcount1.Value;
                string projectstopcount2 = this.project_stopcount2.Value;
                string projectfinishcount1 = this.project_finishcount1.Value;
                string projectfinishcount2 = this.project_finishcount2.Value;
                //正常查询
                if (this.SelectType.Value == "0")
                {
                    //选中check框
                    string hidcktime = this.hid_time.Value;
                    if (hidcktime == "1")
                    {
                        drpyear1 = this.txt_year1.Value;
                        drpyear2 = this.txt_year2.Value;
                    }
                    else
                    {
                        string stryear = this.drp_year.SelectedValue;
                        //季度
                        string strjidu = this.drpJidu.SelectedValue;
                        //月
                        string stryue = this.drpMonth.SelectedValue;
                        //年
                        if (strjidu == "0" && stryue == "0")
                        {
                            drpyear1 = stryear + "-01-01 00:00:00";
                            drpyear2 = stryear + "-12-31 23:59:59 ";
                        }
                        else if (strjidu != "0" && stryue == "0") //年季度
                        {
                            drpyear1 = stryear;
                            drpyear2 = stryear;
                            switch (strjidu)
                            {
                                case "1":
                                    drpyear1 += "-01-01 00:00:00";
                                    drpyear2 += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    drpyear1 += "-04-01 00:00:00";
                                    drpyear2 += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    drpyear1 += "-07-01 00:00:00";
                                    drpyear2 += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    drpyear1 += "-10-01 00:00:00";
                                    drpyear2 += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (strjidu == "0" && stryue != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                            drpyear1 = stryear + "-" + stryue + "-01 00:00:00";
                            drpyear2 = stryear + "-" + stryue + "-" + days + " 23:59:59";
                        }
                        else
                        {
                            drpyear1 = "";
                            drpyear2 = "";
                        }
                        //if (this.drp_year.SelectedIndex > 0)
                        //{
                        //    drpyear1 = this.drp_year.SelectedValue;
                        //    drpyear2 = this.drp_year.SelectedValue;
                        //}
                        //else
                        //{
                        //    drpyear1 = "null";
                        //    drpyear2 = "null";
                        //}
                    }
                }//自定义
                else
                {
                    //项目总数
                    if (!string.IsNullOrEmpty(projectcount1) && string.IsNullOrEmpty(projectcount2))
                    {
                        sb.AppendFormat(" and A>={0}", projectcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectcount2) && string.IsNullOrEmpty(projectcount1))
                    {
                        sb.AppendFormat(" and A<={0}", projectcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectcount2) && !string.IsNullOrEmpty(projectcount1))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", projectcount1, projectcount2);
                    }
                    //项目规模
                    if (!string.IsNullOrEmpty(projectarea1) && string.IsNullOrEmpty(projectarea2))
                    {
                        sb.AppendFormat(" and B>={0}", projectarea1);
                    }
                    else if (!string.IsNullOrEmpty(projectarea2) && string.IsNullOrEmpty(projectarea1))
                    {
                        sb.AppendFormat(" and B<={0}", projectarea2);
                    }
                    else if (!string.IsNullOrEmpty(projectarea1) && !string.IsNullOrEmpty(projectarea2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", projectarea1, projectarea2);
                    }
                    //立项通过数
                    if (!string.IsNullOrEmpty(projectauditcount1) && string.IsNullOrEmpty(projectauditcount2))
                    {
                        sb.AppendFormat(" and C>={0}", projectauditcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectauditcount2) && string.IsNullOrEmpty(projectauditcount1))
                    {
                        sb.AppendFormat(" and C<={0}", projectauditcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectauditcount2) && !string.IsNullOrEmpty(projectauditcount1))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", projectauditcount1, projectauditcount2);
                    }
                    //进行中项目
                    if (!string.IsNullOrEmpty(projectingcount1) && string.IsNullOrEmpty(projectingcount2))
                    {
                        sb.AppendFormat(" and D>={0}", projectingcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectingcount2) && string.IsNullOrEmpty(projectingcount1))
                    {
                        sb.AppendFormat(" and D<={0}", projectingcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectingcount2) && !string.IsNullOrEmpty(projectingcount1))
                    {
                        sb.AppendFormat(" and D between {0} and {1}", projectingcount1, projectingcount2);
                    }
                    //暂停项目
                    if (!string.IsNullOrEmpty(projectstopcount1) && string.IsNullOrEmpty(projectstopcount2))
                    {
                        sb.AppendFormat(" and E>={0}", projectstopcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectstopcount2) && string.IsNullOrEmpty(projectstopcount1))
                    {
                        sb.AppendFormat(" and E<={0}", projectstopcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectstopcount2) && !string.IsNullOrEmpty(projectstopcount1))
                    {
                        sb.AppendFormat(" and E between {0} and {1}", projectstopcount1, projectstopcount2);
                    }
                    //完成项目
                    if (!string.IsNullOrEmpty(projectfinishcount1) && string.IsNullOrEmpty(projectfinishcount2))
                    {
                        sb.AppendFormat(" and F>={0}", projectfinishcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectfinishcount2) && string.IsNullOrEmpty(projectfinishcount1))
                    {
                        sb.AppendFormat(" and F<={0}", projectfinishcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectfinishcount2) && !string.IsNullOrEmpty(projectfinishcount1))
                    {
                        sb.AppendFormat(" and F between {0} and {1}", projectfinishcount1, projectfinishcount2);
                    }
                }
                string timewhere = "";
                if (!string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between '" + drpyear1 + "' and '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else if (!string.IsNullOrEmpty(drpyear1) && string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime > '" + drpyear1 + "' )";
                }
                else if (string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between < '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else
                {
                    timewhere = "(1=1)";
                }
                dt = new TG.BLL.cm_Project().CountProjectByCustom(timewhere, this.drp_type.SelectedValue, sb.ToString()).Tables[0];
                modelPath = " ~/TemplateXls/ProjectCount_Status.xls";
            }
            else if (this.drp_type.SelectedIndex == 1)
            {
                //显示内容
                // ShowCountTypeControl("1");
                //统计
                // dt = new TG.BLL.cm_Project().CountProjectByLevel(year, unitid).Tables[0];


                string drpyear1 = "";
                string drpyear2 = "";

                string syeartime = this.drp_year2_1.SelectedValue;
                string eyeartime = this.drp_year2_2.SelectedValue;
                //年份
                //年份
                if ((!string.IsNullOrEmpty(syeartime) && syeartime != "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                }
                else if ((!string.IsNullOrEmpty(eyeartime) && eyeartime != "-1") && (string.IsNullOrEmpty(syeartime) || syeartime == "-1"))
                {
                    drpyear2 = drpyear2 + "-12-31 23:59:59";
                }
                else if ((string.IsNullOrEmpty(syeartime) || syeartime == "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = "";
                    drpyear2 = "";
                }
                else
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                    drpyear2 = eyeartime + "-12-31 23:59:59";
                }

                string levelcount_1 = this.txt_levelcount_1.Value;
                string levelcount_2 = this.txt_levelcount_2.Value;
                string levelcpr_1 = this.txt_levelcpr_1.Value;
                string levelcpr_2 = this.txt_levelcpr_2.Value;
                string levelcount1_1 = this.txt_levelcount1_1.Value;
                string levelcount1_2 = this.txt_levelcount1_2.Value;
                string levelcpr1_1 = this.txt_levelcpr1_1.Value;
                string levelcpr1_2 = this.txt_levelcpr1_2.Value;
                string levelcount2_1 = this.txt_levelcount2_1.Value;
                string levelcount2_2 = this.txt_levelcount2_2.Value;
                string levelcpr2_1 = this.txt_levelcpr2_1.Value;
                string levelcpr2_2 = this.txt_levelcpr2_2.Value;
                string levelcount3_1 = this.txt_levelcount3_1.Value;
                string levelcount3_2 = this.txt_levelcount3_2.Value;
                string levelcpr3_1 = this.txt_levelcpr3_1.Value;
                string levelcpr3_2 = this.txt_levelcpr3_2.Value;
                //正常查询
                if (this.SelectType.Value == "0")
                {
                    //选中check框
                    string hidcktime = this.hid_time.Value;
                    if (hidcktime == "1")
                    {
                        drpyear1 = this.txt_year1.Value;
                        drpyear2 = this.txt_year2.Value;
                    }
                    else
                    {
                        string stryear = this.drp_year.SelectedValue;
                        //季度
                        string strjidu = this.drpJidu.SelectedValue;
                        //月
                        string stryue = this.drpMonth.SelectedValue;
                        //年
                        if (strjidu == "0" && stryue == "0")
                        {
                            drpyear1 = stryear + "-01-01 00:00:00";
                            drpyear2 = stryear + "-12-31 23:59:59 ";
                        }
                        else if (strjidu != "0" && stryue == "0") //年季度
                        {
                            drpyear1 = stryear;
                            drpyear2 = stryear;
                            switch (strjidu)
                            {
                                case "1":
                                    drpyear1 += "-01-01 00:00:00";
                                    drpyear2 += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    drpyear1 += "-04-01 00:00:00";
                                    drpyear2 += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    drpyear1 += "-07-01 00:00:00";
                                    drpyear2 += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    drpyear1 += "-10-01 00:00:00";
                                    drpyear2 += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (strjidu == "0" && stryue != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                            drpyear1 = stryear + "-" + stryue + "-01 00:00:00";
                            drpyear2 = stryear + "-" + stryue + "-" + days + " 23:59:59";
                        }
                        else
                        {
                            drpyear1 = "";
                            drpyear2 = "";
                        }

                    }
                }//自定义
                else
                {
                    //特级数量
                    if (!string.IsNullOrEmpty(levelcount_1) && string.IsNullOrEmpty(levelcount_2))
                    {
                        sb.AppendFormat(" and A>={0}", levelcount_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount_2) && string.IsNullOrEmpty(levelcount_1))
                    {
                        sb.AppendFormat(" and A<={0}", levelcount_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr_1) && !string.IsNullOrEmpty(levelcount_2))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", levelcpr_1, levelcount_2);
                    }
                    //特级合同额
                    if (!string.IsNullOrEmpty(levelcpr_1) && string.IsNullOrEmpty(levelcpr_2))
                    {
                        sb.AppendFormat(" and A1>={0}", levelcpr_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr_2) && string.IsNullOrEmpty(levelcpr_1))
                    {
                        sb.AppendFormat(" and A1<={0}", levelcpr_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr_1) && !string.IsNullOrEmpty(levelcpr_2))
                    {
                        sb.AppendFormat(" and A1 between {0} and {1}", levelcpr_1, levelcpr_2);
                    }
                    //一级数量
                    if (!string.IsNullOrEmpty(levelcount1_1) && string.IsNullOrEmpty(levelcount1_2))
                    {
                        sb.AppendFormat(" and B>={0}", levelcount1_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount1_2) && string.IsNullOrEmpty(levelcount1_1))
                    {
                        sb.AppendFormat(" and B<={0}", levelcount1_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcount1_1) && !string.IsNullOrEmpty(levelcount1_2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", levelcount1_1, levelcount1_2);
                    }
                    //一级合同额
                    if (!string.IsNullOrEmpty(levelcpr1_1) && string.IsNullOrEmpty(levelcpr1_2))
                    {
                        sb.AppendFormat(" and B1>={0}", levelcpr1_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr1_2) && string.IsNullOrEmpty(levelcpr1_1))
                    {
                        sb.AppendFormat(" and B1<={0}", levelcpr1_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr1_1) && !string.IsNullOrEmpty(levelcpr1_2))
                    {
                        sb.AppendFormat(" and B1 between {0} and {1}", levelcpr1_1, levelcpr1_2);
                    }
                    //二级数量
                    if (!string.IsNullOrEmpty(levelcount2_1) && string.IsNullOrEmpty(levelcount2_2))
                    {
                        sb.AppendFormat(" and C>={0}", levelcount2_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount2_2) && string.IsNullOrEmpty(levelcount2_1))
                    {
                        sb.AppendFormat(" and C<={0}", levelcount2_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcount2_1) && !string.IsNullOrEmpty(levelcount2_2))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", levelcount2_1, levelcount2_2);
                    }
                    //二级合同额
                    if (!string.IsNullOrEmpty(levelcpr2_1) && string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and C1>={0}", levelcpr2_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_2) && string.IsNullOrEmpty(levelcpr2_1))
                    {
                        sb.AppendFormat(" and C1<={0}", levelcpr2_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_1) && !string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and C1 between {0} and {1}", levelcpr2_1, levelcpr2_2);
                    }
                    //三级数量
                    if (!string.IsNullOrEmpty(levelcount3_1) && string.IsNullOrEmpty(levelcount3_2))
                    {
                        sb.AppendFormat(" and D>={0}", levelcount3_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount3_2) && string.IsNullOrEmpty(levelcount3_1))
                    {
                        sb.AppendFormat(" and D<={0}", levelcount3_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcount3_1) && !string.IsNullOrEmpty(levelcount3_2))
                    {
                        sb.AppendFormat(" and D between {0} and {1}", levelcount3_1, levelcount3_2);
                    }
                    //三级合同额
                    if (!string.IsNullOrEmpty(levelcpr2_1) && string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and D1>={0}", levelcpr2_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_2) && string.IsNullOrEmpty(levelcpr2_1))
                    {
                        sb.AppendFormat(" and D1<={0}", levelcpr2_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_1) && !string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and D1 between {0} and {1}", levelcpr2_1, levelcpr2_2);
                    }
                }
                string timewhere = "";
                if (!string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between '" + drpyear1 + "' and '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else if (!string.IsNullOrEmpty(drpyear1) && string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime > '" + drpyear1 + "' )";
                }
                else if (string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between < '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else
                {
                    timewhere = "(1=1)";
                }
                dt = new TG.BLL.cm_Project().CountProjectByCustom(timewhere, this.drp_type.SelectedValue, sb.ToString()).Tables[0];
                modelPath = " ~/TemplateXls/ProjectCount_Level.xls";
            }
            else if (this.drp_type.SelectedIndex == 2 || this.drp_type.SelectedIndex == 3)
            {
                //显示内容
                //  ShowCountTypeControl("2");
                //统计
                //项目个数
                //  string type = "0";
                //  dt = new TG.BLL.cm_Project().CountProjectByIndustry(year, unitid, type).Tables[0];

                string drpyear1 = "";
                string drpyear2 = "";

                string syeartime = this.drp_year3_1.SelectedValue;
                string eyeartime = this.drp_year3_2.SelectedValue;
                //年份
                //年份
                if ((!string.IsNullOrEmpty(syeartime) && syeartime != "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                }
                else if ((!string.IsNullOrEmpty(eyeartime) && eyeartime != "-1") && (string.IsNullOrEmpty(syeartime) || syeartime == "-1"))
                {
                    drpyear2 = drpyear2 + "-12-31 23:59:59";
                }
                else if ((string.IsNullOrEmpty(syeartime) || syeartime == "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = "";
                    drpyear2 = "";
                }
                else
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                    drpyear2 = eyeartime + "-12-31 23:59:59";
                }

                string txt_gj1 = this.txt_gj1.Value;
                string txt_gj2 = this.txt_gj2.Value;
                string txt_fdc1 = this.txt_fdc1.Value;
                string txt_fdc2 = this.txt_fdc2.Value;
                string txt_sz1 = this.txt_sz1.Value;
                string txt_sz2 = this.txt_sz2.Value;
                string txt_yy1 = this.txt_yy1.Value;
                string txt_yy2 = this.txt_yy2.Value;
                string txt_dl1 = this.txt_dl1.Value;
                string txt_dl2 = this.txt_dl2.Value;
                string txt_tx1 = this.txt_tx1.Value;
                string txt_tx2 = this.txt_tx2.Value;
                string txt_yh1 = this.txt_yh1.Value;
                string txt_yh2 = this.txt_yh2.Value;
                string txt_xx1 = this.txt_xx1.Value;
                string txt_xx2 = this.txt_xx2.Value;
                string txt_sw1 = this.txt_sw1.Value;
                string txt_sw2 = this.txt_sw2.Value;
                //正常查询
                if (this.SelectType.Value == "0")
                {
                    //选中check框
                    string hidcktime = this.hid_time.Value;
                    if (hidcktime == "1")
                    {
                        drpyear1 = this.txt_year1.Value;
                        drpyear2 = this.txt_year2.Value;
                    }
                    else
                    {
                        string stryear = this.drp_year.SelectedValue;
                        //季度
                        string strjidu = this.drpJidu.SelectedValue;
                        //月
                        string stryue = this.drpMonth.SelectedValue;
                        //年
                        if (strjidu == "0" && stryue == "0")
                        {
                            drpyear1 = stryear + "-01-01 00:00:00";
                            drpyear2 = stryear + "-12-31 23:59:59 ";
                        }
                        else if (strjidu != "0" && stryue == "0") //年季度
                        {
                            drpyear1 = stryear;
                            drpyear2 = stryear;
                            switch (strjidu)
                            {
                                case "1":
                                    drpyear1 += "-01-01 00:00:00";
                                    drpyear2 += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    drpyear1 += "-04-01 00:00:00";
                                    drpyear2 += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    drpyear1 += "-07-01 00:00:00";
                                    drpyear2 += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    drpyear1 += "-10-01 00:00:00";
                                    drpyear2 += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (strjidu == "0" && stryue != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                            drpyear1 = stryear + "-" + stryue + "-01 00:00:00";
                            drpyear2 = stryear + "-" + stryue + "-" + days + " 23:59:59";
                        }
                        else
                        {
                            drpyear1 = "";
                            drpyear2 = "";
                        }
                        //if (this.drp_year.SelectedIndex > 0)
                        //{
                        //    drpyear1 = this.drp_year.SelectedValue;
                        //    drpyear2 = this.drp_year.SelectedValue;
                        //}
                        //else
                        //{
                        //    drpyear1 = "null";
                        //    drpyear2 = "null";
                        //}
                    }
                }//自定义
                else
                {
                    //公建
                    if (!string.IsNullOrEmpty(txt_gj1) && string.IsNullOrEmpty(txt_gj2))
                    {
                        sb.AppendFormat(" and A>={0}", txt_gj1);
                    }
                    else if (!string.IsNullOrEmpty(txt_gj2) && string.IsNullOrEmpty(txt_gj1))
                    {
                        sb.AppendFormat(" and A<={0}", txt_gj2);
                    }
                    else if (!string.IsNullOrEmpty(txt_gj1) && !string.IsNullOrEmpty(txt_gj2))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", txt_gj1, txt_gj2);
                    }
                    //房地产
                    if (!string.IsNullOrEmpty(txt_fdc1) && string.IsNullOrEmpty(txt_fdc2))
                    {
                        sb.AppendFormat(" and B>={0}", txt_fdc1);
                    }
                    else if (!string.IsNullOrEmpty(txt_fdc2) && string.IsNullOrEmpty(txt_fdc1))
                    {
                        sb.AppendFormat(" and B<={0}", txt_fdc2);
                    }
                    else if (!string.IsNullOrEmpty(txt_fdc1) && !string.IsNullOrEmpty(txt_fdc2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", txt_fdc1, txt_fdc2);
                    }
                    //市政
                    if (!string.IsNullOrEmpty(txt_sz1) && string.IsNullOrEmpty(txt_sz2))
                    {
                        sb.AppendFormat(" and C>={0}", txt_sz1);
                    }
                    else if (!string.IsNullOrEmpty(txt_sz2) && string.IsNullOrEmpty(txt_sz1))
                    {
                        sb.AppendFormat(" and C<={0}", txt_sz2);
                    }
                    else if (!string.IsNullOrEmpty(txt_sz1) && !string.IsNullOrEmpty(txt_sz2))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", txt_sz1, txt_sz2);
                    }
                    //医院
                    if (!string.IsNullOrEmpty(txt_yy1) && string.IsNullOrEmpty(txt_yy2))
                    {
                        sb.AppendFormat(" and D>={0}", txt_yy1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yy2) && string.IsNullOrEmpty(txt_yy1))
                    {
                        sb.AppendFormat(" and D<={0}", txt_yy2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yy1) && !string.IsNullOrEmpty(txt_yy2))
                    {
                        sb.AppendFormat(" and D between {0} and {1}", txt_yy1, txt_yy2);
                    }
                    //电力
                    if (!string.IsNullOrEmpty(txt_dl1) && string.IsNullOrEmpty(txt_dl2))
                    {
                        sb.AppendFormat(" and E>={0}", txt_dl1);
                    }
                    else if (!string.IsNullOrEmpty(txt_dl2) && string.IsNullOrEmpty(txt_dl1))
                    {
                        sb.AppendFormat(" and E<={0}", txt_dl2);
                    }
                    else if (!string.IsNullOrEmpty(txt_dl1) && !string.IsNullOrEmpty(txt_dl2))
                    {
                        sb.AppendFormat(" and E between {0} and {1}", txt_dl1, txt_dl2);
                    }
                    //通信
                    if (!string.IsNullOrEmpty(txt_tx1) && string.IsNullOrEmpty(txt_tx2))
                    {
                        sb.AppendFormat(" and F>={0}", txt_tx1);
                    }
                    else if (!string.IsNullOrEmpty(txt_tx2) && string.IsNullOrEmpty(txt_tx1))
                    {
                        sb.AppendFormat(" and F<={0}", txt_tx2);
                    }
                    else if (!string.IsNullOrEmpty(txt_tx1) && !string.IsNullOrEmpty(txt_tx2))
                    {
                        sb.AppendFormat(" and F between {0} and {1}", txt_tx1, txt_tx2);
                    }
                    //银行
                    if (!string.IsNullOrEmpty(txt_yh1) && string.IsNullOrEmpty(txt_yh2))
                    {
                        sb.AppendFormat(" and G>={0}", txt_tx1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yh2) && string.IsNullOrEmpty(txt_yh1))
                    {
                        sb.AppendFormat(" and G<={0}", txt_yh2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yh1) && !string.IsNullOrEmpty(txt_yh2))
                    {
                        sb.AppendFormat(" and G between {0} and {1}", txt_yh1, txt_yh2);
                    }
                    //学校
                    if (!string.IsNullOrEmpty(txt_xx1) && string.IsNullOrEmpty(txt_xx2))
                    {
                        sb.AppendFormat(" and H>={0}", txt_xx1);
                    }
                    else if (!string.IsNullOrEmpty(txt_xx2) && string.IsNullOrEmpty(txt_xx1))
                    {
                        sb.AppendFormat(" and H<={0}", txt_xx2);
                    }
                    else if (!string.IsNullOrEmpty(txt_xx1) && !string.IsNullOrEmpty(txt_xx2))
                    {
                        sb.AppendFormat(" and H between {0} and {1}", txt_xx1, txt_xx2);
                    }
                    //涉外 
                    if (!string.IsNullOrEmpty(txt_sw1) && string.IsNullOrEmpty(txt_sw2))
                    {
                        sb.AppendFormat(" and I>={0}", txt_sw1);
                    }
                    else if (!string.IsNullOrEmpty(txt_sw2) && string.IsNullOrEmpty(txt_sw1))
                    {
                        sb.AppendFormat(" and I<={0}", txt_sw2);
                    }
                    else if (!string.IsNullOrEmpty(txt_sw1) && !string.IsNullOrEmpty(txt_sw2))
                    {
                        sb.AppendFormat(" and I between {0} and {1}", txt_sw1, txt_sw2);
                    }
                }
                string timewhere = "";
                if (!string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between '" + drpyear1 + "' and '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else if (!string.IsNullOrEmpty(drpyear1) && string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime > '" + drpyear1 + "' )";
                }
                else if (string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between < '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else
                {
                    timewhere = "(1=1)";
                }
                dt = new TG.BLL.cm_Project().CountProjectByCustom(timewhere, this.drp_type.SelectedValue, sb.ToString()).Tables[0];
                this.div_util3.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);
                modelPath = " ~/TemplateXls/ProjectCount_Industry.xls";
            }
            //else if (this.drp_type.SelectedIndex== 3)
            //{
            //    //显示内容
            //    ShowCountTypeControl("3");
            //    //统计
            //    //项目个数
            //    string type = "1";
            //    dt = new TG.BLL.cm_Project().CountProjectByIndustry(year, unitid, type).Tables[0];
            //    //单位
            //    this.div_util3.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);

            //    modelPath = " ~/TemplateXls/ProjectCount_Industry.xls";
            //}
            else if (this.drp_type.SelectedIndex == 4)
            {
                //显示内容
                //  ShowCountTypeControl("4");
                //统计
                //统计类型
                // int type = int.Parse(this.drp_type.SelectedValue) - 6;

                string drpyear1 = "";
                string drpyear2 = "";

                string syeartime = this.drp_year4_1.SelectedValue;
                string eyeartime = this.drp_year4_2.SelectedValue;
                //年份
                //年份
                if ((!string.IsNullOrEmpty(syeartime) && syeartime != "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                }
                else if ((!string.IsNullOrEmpty(eyeartime) && eyeartime != "-1") && (string.IsNullOrEmpty(syeartime) || syeartime == "-1"))
                {
                    drpyear2 = drpyear2 + "-12-31 23:59:59";
                }
                else if ((string.IsNullOrEmpty(syeartime) || syeartime == "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = "";
                    drpyear2 = "";
                }
                else
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                    drpyear2 = eyeartime + "-12-31 23:59:59";
                }
                string txt_gkzbcount1 = this.txt_gkzbcount1.Value;
                string txt_gkzbcount2 = this.txt_gkzbcount2.Value;
                string txt_gkzbscale1 = this.txt_gkzbscale1.Value;
                string txt_gkzbscale2 = this.txt_gkzbscale2.Value;
                string txt_yqzbcount1 = this.txt_yqzbcount1.Value;
                string txt_yqzbcount2 = this.txt_yqzbcount2.Value;
                string txt_yqzbscale1 = this.txt_yqzbscale1.Value;
                string txt_yqzbscale2 = this.txt_yqzbscale2.Value;
                string txt_zxwtcount1 = this.txt_zxwtcount1.Value;
                string txt_zxwtcount2 = this.txt_zxwtcount2.Value;
                string txt_zxwtscale1 = this.txt_zxwtscale1.Value;
                string txt_zxwtscale2 = this.txt_zxwtscale2.Value;
                //正常查询
                if (this.SelectType.Value == "0")
                {
                    //选中check框
                    string hidcktime = this.hid_time.Value;
                    if (hidcktime == "1")
                    {
                        drpyear1 = this.txt_year1.Value;
                        drpyear2 = this.txt_year2.Value;
                    }
                    else
                    {
                        string stryear = this.drp_year.SelectedValue;
                        //季度
                        string strjidu = this.drpJidu.SelectedValue;
                        //月
                        string stryue = this.drpMonth.SelectedValue;
                        //年
                        if (strjidu == "0" && stryue == "0")
                        {
                            drpyear1 = stryear + "-01-01 00:00:00";
                            drpyear2 = stryear + "-12-31 23:59:59 ";
                        }
                        else if (strjidu != "0" && stryue == "0") //年季度
                        {
                            drpyear1 = stryear;
                            drpyear2 = stryear;
                            switch (strjidu)
                            {
                                case "1":
                                    drpyear1 += "-01-01 00:00:00";
                                    drpyear2 += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    drpyear1 += "-04-01 00:00:00";
                                    drpyear2 += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    drpyear1 += "-07-01 00:00:00";
                                    drpyear2 += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    drpyear1 += "-10-01 00:00:00";
                                    drpyear2 += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (strjidu == "0" && stryue != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                            drpyear1 = stryear + "-" + stryue + "-01 00:00:00";
                            drpyear2 = stryear + "-" + stryue + "-" + days + " 23:59:59";
                        }
                        else
                        {
                            drpyear1 = "";
                            drpyear2 = "";
                        }
                        //if (this.drp_year.SelectedIndex > 0)
                        //{
                        //    drpyear1 = this.drp_year.SelectedValue;
                        //    drpyear2 = this.drp_year.SelectedValue;
                        //}
                        //else
                        //{
                        //    drpyear1 = "null";
                        //    drpyear2 = "null";
                        //}
                    }
                }//自定义
                else
                {
                    //项目总数
                    if (!string.IsNullOrEmpty(txt_gkzbcount1) && string.IsNullOrEmpty(txt_gkzbcount2))
                    {
                        sb.AppendFormat(" and A>={0}", txt_gkzbcount1);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbcount2) && string.IsNullOrEmpty(txt_gkzbcount1))
                    {
                        sb.AppendFormat(" and A<={0}", txt_gkzbcount2);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbcount1) && !string.IsNullOrEmpty(txt_gkzbcount2))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", txt_gkzbcount1, txt_gkzbcount2);
                    }
                    //项目规模
                    if (!string.IsNullOrEmpty(txt_gkzbscale1) && string.IsNullOrEmpty(txt_gkzbscale2))
                    {
                        sb.AppendFormat(" and A1>={0}", txt_gkzbscale1);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbscale2) && string.IsNullOrEmpty(txt_gkzbscale1))
                    {
                        sb.AppendFormat(" and A1<={0}", txt_gkzbscale2);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbscale1) && !string.IsNullOrEmpty(txt_gkzbscale2))
                    {
                        sb.AppendFormat(" and A1 between {0} and {1}", txt_gkzbscale1, txt_gkzbscale2);
                    }
                    //立项通过数
                    if (!string.IsNullOrEmpty(txt_yqzbcount1) && string.IsNullOrEmpty(txt_yqzbcount2))
                    {
                        sb.AppendFormat(" and B>={0}", txt_yqzbcount1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbcount2) && string.IsNullOrEmpty(txt_yqzbcount1))
                    {
                        sb.AppendFormat(" and B<={0}", txt_yqzbcount2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbcount1) && !string.IsNullOrEmpty(txt_yqzbcount2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", txt_yqzbcount1, txt_yqzbcount2);
                    }
                    //进行中项目
                    if (!string.IsNullOrEmpty(txt_yqzbscale1) && string.IsNullOrEmpty(txt_yqzbscale2))
                    {
                        sb.AppendFormat(" and B1>={0}", txt_yqzbscale1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbscale2) && string.IsNullOrEmpty(txt_yqzbscale1))
                    {
                        sb.AppendFormat(" and B1<={0}", txt_yqzbscale2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbscale1) && !string.IsNullOrEmpty(txt_yqzbscale2))
                    {
                        sb.AppendFormat(" and B1 between {0} and {1}", txt_yqzbscale1, txt_yqzbscale2);
                    }
                    //暂停项目
                    if (!string.IsNullOrEmpty(txt_zxwtcount1) && string.IsNullOrEmpty(txt_zxwtcount2))
                    {
                        sb.AppendFormat(" and C>={0}", txt_zxwtcount1);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtcount2) && string.IsNullOrEmpty(txt_zxwtcount1))
                    {
                        sb.AppendFormat(" and C<={0}", txt_zxwtcount2);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtcount1) && !string.IsNullOrEmpty(txt_zxwtcount2))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", txt_zxwtcount1, txt_zxwtcount2);
                    }
                    //完成项目
                    if (!string.IsNullOrEmpty(txt_zxwtscale1) && string.IsNullOrEmpty(txt_zxwtscale2))
                    {
                        sb.AppendFormat(" and C1>={0}", txt_zxwtscale1);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtscale2) && string.IsNullOrEmpty(txt_zxwtscale1))
                    {
                        sb.AppendFormat(" and C1<={0}", txt_zxwtscale2);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtscale1) && !string.IsNullOrEmpty(txt_zxwtscale2))
                    {
                        sb.AppendFormat(" and C1 between {0} and {1}", txt_zxwtscale1, txt_zxwtscale2);
                    }
                }
                string timewhere = "";
                if (!string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between '" + drpyear1 + "' and '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else if (!string.IsNullOrEmpty(drpyear1) && string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime > '" + drpyear1 + "' )";
                }
                else if (string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between < '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else
                {
                    timewhere = "(1=1)";
                }
                dt = new TG.BLL.cm_Project().CountProjectByCustom(timewhere, this.drp_type.SelectedValue, sb.ToString()).Tables[0];
                modelPath = " ~/TemplateXls/ProjectCount_Src.xls";
            }

            DataRow[] drsDel = dt.Select(string.Format("ID in ({0})", base.NotShowUnitList));
            //直接在集合中删除不显示的部门
            foreach (DataRow drDel in drsDel)
            {
                dt.Rows.Remove(drDel);
            }

            //把生产经营部放到最后
            drsDel = dt.Select(string.Format("Name='{0}'", "生产经营部"));
            DataRow dr = dt.NewRow();
            foreach (DataRow drDel in drsDel)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[i] = drDel[i];
                }
                dt.Rows.Remove(drDel);
                //类型判断
                dt.Rows.InsertAt(dr, (dt.Rows.Count - 1));

            }

            if (dt != null && dt.Rows.Count > 0)
            {

                HSSFWorkbook wb = null;

                //如果没有模板路径，则创建一个空的workbook和一个空的sheet
                if (string.IsNullOrEmpty(modelPath))
                {
                    wb = new HSSFWorkbook();
                    wb.CreateSheet();
                    wb.GetSheetAt(0).CreateRow(0);
                }
                else
                {
                    using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        wb = new HSSFWorkbook(fileStream);
                        fileStream.Close();
                    }
                }

                //标题样式
                ICellStyle style1 = wb.CreateCellStyle();
                style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style1.VerticalAlignment = VerticalAlignment.CENTER;
                style1.WrapText = true;
                //style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                //style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                //style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                //style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font1 = wb.CreateFont();
                font1.FontHeightInPoints = 12;//字号
                font1.FontName = "宋体";//字体
                font1.Boldweight = (short)700;
                style1.SetFont(font1);

                //内容样式
                ICellStyle style2 = wb.CreateCellStyle();
                style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style2.VerticalAlignment = VerticalAlignment.CENTER;
                style2.WrapText = true;
                style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font2 = wb.CreateFont();
                font2.FontHeightInPoints = 10;//字号
                font2.FontName = "宋体";//字体
                style2.SetFont(font2);

                //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
                var ws = wb.GetSheetAt(0);
                if (ws == null)
                    ws = wb.GetSheetAt(0);

                string exporttitle = this.titlecount.Value.Replace(",", "，") + "项目综合统计按" + CountType + "统计";
                //读取标题
                IRow dataRowTitle = ws.GetRow(0);
                ICell celltitle = dataRowTitle.GetCell(0);
                celltitle.SetCellValue(exporttitle);
                //dataRowTitle.GetCell(0).CellStyle = style1;
                //在这里进行Excel分类的切割
                //设计
                DataTable dtsj = dt.Clone();
                //多营
                DataTable dtdj = dt.Clone();
                //增加监理和勘察分开
                DataTable dtkc = dt.Clone();
                DataTable dtjl = dt.Clone();
                //管理部门
                DataTable dtel = dt.Clone();
                //全部部门
                DataTable dtall = dt.Clone();
                //238是设计所的
                //254是多经部门
                //其他的就是管理部门可以
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ID"].ToString() != "500" && dt.Rows[i]["ID"].ToString() != "300")
                    {
                        if (unitparenrID(dt.Rows[i]["ID"].ToString()) == "238")
                        {
                            if (dt.Rows[i]["ID"].ToString() == "250")//勘察
                            {
                                dtkc.ImportRow(dt.Rows[i]);
                            }
                            else if (dt.Rows[i]["ID"].ToString() == "253")//监理
                            {
                                dtjl.ImportRow(dt.Rows[i]);
                            }
                            else//其他
                            {
                                dtsj.ImportRow(dt.Rows[i]);
                            }
                        }
                        else if (unitparenrID(dt.Rows[i]["ID"].ToString()) == "254")
                        {
                            dtdj.ImportRow(dt.Rows[i]);
                        }
                        else
                        {
                            if (unitparenrID(dt.Rows[i]["ID"].ToString()) != "0" || dt.Rows[i]["ID"].ToString() == "230")
                            {
                                dtel.ImportRow(dt.Rows[i]);
                            }

                        }
                    }
                    else
                    {
                        dtall.ImportRow(dt.Rows[i]);
                    }
                }
                //类型判断
                if (this.drp_type.SelectedIndex == 0)
                {
                    //标题合并
                    ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 6));

                    int row = 3;
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理部门部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 6));
                    }
                    //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                    for (int i = 0; i < dtel.Rows.Count; i++)
                    {

                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["F"].ToString()));
                        row = row + 1;
                    }
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                        //cell.SetCellValue()
                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                    }
                    //===设计
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计、监理、勘察部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 6));
                    }

                    for (int i = 0; i < dtsj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["F"].ToString()));

                        row = row + 1;
                    }
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //勘察&设计所合计一下
                    if (dtkc.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtkc.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["E"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["F"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("勘察、设计合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //最后是监理
                    if (dtjl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtjl.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["E"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["F"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("主营合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");


                    }
                    //多经部门
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多种经营部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 6));
                    }

                    for (int i = 0; i < dtdj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["F"].ToString()));
                        //cell = dataRow.CreateCell(8);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                        //cell = dataRow.CreateCell(9);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                        row = row + 1;
                    }
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多经营所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                    }
                    for (int i = 0; i < dtall.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["F"].ToString()));

                        row = row + 1;
                    }



                }
                else if (this.drp_type.SelectedIndex == 1)
                {
                    //标题合并
                    ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 8));

                    int row = 4;
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理部门部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 8));
                    }
                    //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                    for (int i = 0; i < dtel.Rows.Count; i++)
                    {

                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C1"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D1"].ToString()));
                        row = row + 1;
                    }
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                        //cell.SetCellValue()
                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                    }
                    //===设计
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计、监理、勘察部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 8));
                    }

                    for (int i = 0; i < dtsj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C1"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D1"].ToString()));

                        row = row + 1;
                    }
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");
                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //勘察&设计所合计一下
                    if (dtkc.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtkc.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A1"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B1"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C1"].ToString()));
                            cell = dataRow.CreateCell(7);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));
                            cell = dataRow.CreateCell(8);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D1"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("勘察、设计合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(7);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(8);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");
                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //最后是监理
                    if (dtjl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtjl.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A1"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B1"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C1"].ToString()));
                            cell = dataRow.CreateCell(7);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D"].ToString()));
                            cell = dataRow.CreateCell(8);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D1"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("主营合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(7);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(8);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                    }
                    //多经部门
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多种经营部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 8));
                    }

                    for (int i = 0; i < dtdj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C1"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D1"].ToString()));
                        //cell = dataRow.CreateCell(8);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                        //cell = dataRow.CreateCell(9);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                        row = row + 1;
                    }
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多经营所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");

                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");
                    }
                    for (int i = 0; i < dtall.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C1"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D1"].ToString()));

                        row = row + 1;
                    }

                }
                else if (this.drp_type.SelectedIndex == 2)
                {
                    //标题合并
                    ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 9));

                    //单位
                    font1.FontHeightInPoints = 8;//字号                  
                    font1.Boldweight = (short)0;
                    style1.SetFont(font1);

                    IRow TwoRow = ws.GetRow(1);
                    ICell TwoCell = TwoRow.GetCell(0);
                    ws.AddMergedRegion(new CellRangeAddress(1, 1, 0, 8));

                    TwoCell = TwoRow.GetCell(9);
                    TwoCell.CellStyle = style1;
                    TwoCell.SetCellValue(GetUtilByDrpText(this.drp_type.SelectedItem.Text));

                    int row = 3;

                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理部门部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                    }
                    //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                    for (int i = 0; i < dtel.Rows.Count; i++)
                    {

                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["I"].ToString()));
                        row = row + 1;
                    }
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                        //cell.SetCellValue()
                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(J3:J" + (row - 1) + ")");
                    }
                    //===设计
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计、监理、勘察部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                    }

                    for (int i = 0; i < dtsj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["I"].ToString()));

                        row = row + 1;
                    }
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");

                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(J" + (row - dtsj.Rows.Count) + ":J" + (row - 1) + ")");
                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //勘察&设计所合计一下
                    if (dtkc.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtkc.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["E"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["F"].ToString()));
                            cell = dataRow.CreateCell(7);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["G"].ToString()));
                            cell = dataRow.CreateCell(8);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["H"].ToString()));
                            cell = dataRow.CreateCell(9);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["I"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("勘察、设计合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(7);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(8);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(9);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");
                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //最后是监理
                    if (dtjl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtjl.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["E"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["F"].ToString()));
                            cell = dataRow.CreateCell(7);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["G"].ToString()));
                            cell = dataRow.CreateCell(8);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["H"].ToString()));
                            cell = dataRow.CreateCell(9);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["I"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("主营合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(7);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(8);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(9);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");

                    }
                    //多经部门
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多种经营部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                    }

                    for (int i = 0; i < dtdj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["I"].ToString()));
                        //cell = dataRow.CreateCell(8);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                        //cell = dataRow.CreateCell(9);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                        row = row + 1;
                    }
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多经营所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");

                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");

                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(J" + (row - dtdj.Rows.Count) + ":J" + (row - 1) + ")");
                    }
                    for (int i = 0; i < dtall.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["I"].ToString()));
                        row = row + 1;
                    }

                }
                else if (this.drp_type.SelectedIndex == 3)
                {
                    //标题合并
                    ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 9));

                    //单位
                    font1.FontHeightInPoints = 8;//字号                  
                    font1.Boldweight = (short)0;
                    style1.SetFont(font1);

                    IRow TwoRow = ws.GetRow(1);
                    ICell TwoCell = TwoRow.GetCell(0);
                    ws.AddMergedRegion(new CellRangeAddress(1, 1, 0, 8));

                    TwoCell = TwoRow.GetCell(9);
                    TwoCell.CellStyle = style1;
                    TwoCell.SetCellValue(GetUtilByDrpText(this.drp_type.SelectedItem.Text));

                    int row = 3;

                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理部门部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                    }
                    //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                    for (int i = 0; i < dtel.Rows.Count; i++)
                    {

                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["I"].ToString()));
                        row = row + 1;
                    }
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                        //cell.SetCellValue()
                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(J3:J" + (row - 1) + ")");
                    }
                    //===设计
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计、监理、勘察部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                    }

                    for (int i = 0; i < dtsj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["I"].ToString()));

                        row = row + 1;
                    }
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");

                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(J" + (row - dtsj.Rows.Count) + ":J" + (row - 1) + ")");
                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //勘察&设计所合计一下
                    if (dtkc.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtkc.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["E"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["F"].ToString()));
                            cell = dataRow.CreateCell(7);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["G"].ToString()));
                            cell = dataRow.CreateCell(8);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["H"].ToString()));
                            cell = dataRow.CreateCell(9);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["I"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("勘察、设计合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(7);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(8);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(9);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");
                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //最后是监理
                    if (dtjl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtjl.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["E"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["F"].ToString()));
                            cell = dataRow.CreateCell(7);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["G"].ToString()));
                            cell = dataRow.CreateCell(8);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["H"].ToString()));
                            cell = dataRow.CreateCell(9);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["I"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("主营合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(7);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(8);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(9);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");

                    }
                    //多经部门
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多种经营部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                    }

                    for (int i = 0; i < dtdj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["I"].ToString()));
                        //cell = dataRow.CreateCell(8);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                        //cell = dataRow.CreateCell(9);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                        row = row + 1;
                    }
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多经营所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");

                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");

                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(J" + (row - dtdj.Rows.Count) + ":J" + (row - 1) + ")");
                    }
                    for (int i = 0; i < dtall.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["I"].ToString()));
                        row = row + 1;
                    }

                }
                else if (this.drp_type.SelectedIndex == 4)
                {
                    //标题合并
                    ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 6));

                    int row = 4;
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理部门部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 6));
                    }
                    //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                    for (int i = 0; i < dtel.Rows.Count; i++)
                    {

                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C1"].ToString()));
                        row = row + 1;
                    }
                    if (dtel.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("管理合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                        //cell.SetCellValue()
                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                    }
                    //===设计
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计、监理、勘察部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 6));
                    }

                    for (int i = 0; i < dtsj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C1"].ToString()));

                        row = row + 1;
                    }
                    if (dtsj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("设计所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //勘察&设计所合计一下
                    if (dtkc.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtkc.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A1"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B1"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C1"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("勘察、设计合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                        //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                    }
                    //最后是监理
                    if (dtjl.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtjl.Rows.Count; i++)
                        {
                            var dataRow = ws.GetRow(row);//读行
                            if (dataRow == null)
                                dataRow = ws.CreateRow(row);//生成行

                            var cell = dataRow.GetCell(0);
                            if (cell == null)
                                cell = dataRow.CreateCell(0);

                            cell.CellStyle = style2;
                            cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                            cell = dataRow.CreateCell(1);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                            cell = dataRow.CreateCell(2);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A1"].ToString()));

                            cell = dataRow.CreateCell(3);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                            cell = dataRow.CreateCell(4);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B1"].ToString()));

                            cell = dataRow.CreateCell(5);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                            cell = dataRow.CreateCell(6);
                            cell.CellStyle = style2;
                            cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C1"].ToString()));

                            row = row + 1;
                        }
                        var dataRowk = ws.GetRow(row);//读行
                        if (dataRowk == null)
                            dataRowk = ws.CreateRow(row);//生成行
                        var cellk = dataRowk.CreateCell(0);
                        if (cellk == null)
                            cellk = dataRowk.CreateCell(0);
                        cellk.CellStyle = style2;
                        cellk.SetCellValue("主营合计");
                        row = row + 1;
                        cellk = dataRowk.CreateCell(1);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                        cellk = dataRowk.CreateCell(2);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(3);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(4);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(5);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                        cellk = dataRowk.CreateCell(6);
                        cellk.CellStyle = style2;
                        cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");


                    }
                    //多经部门
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多种经营部分");
                        row = row + 1;
                        ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 6));
                    }

                    for (int i = 0; i < dtdj.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C1"].ToString()));
                        //cell = dataRow.CreateCell(8);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                        //cell = dataRow.CreateCell(9);
                        //cell.CellStyle = style2;
                        //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                        row = row + 1;
                    }
                    if (dtdj.Rows.Count > 0)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行
                        var cell = dataRow.CreateCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);
                        cell.CellStyle = style2;
                        cell.SetCellValue("多经营所合计");
                        row = row + 1;
                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                    }
                    for (int i = 0; i < dtall.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C1"].ToString()));

                        row = row + 1;
                    }



                }
                using (MemoryStream memoryStream = new MemoryStream())
                {

                    wb.Write(memoryStream);

                    string name = System.Web.HttpContext.Current.Server.UrlEncode(exporttitle + ".xls");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                    Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.ContentEncoding = Encoding.UTF8;
                    wb = null;
                    Response.End();
                }
            }
            else
            {
                Response.Write("<script type=javascript>alert('无数据');</script>");
            }
        }

        public string unitparenrID(string unitid)
        {
            return bll_unit.GetModel(int.Parse(unitid)).unit_ParentID.ToString();
        }
    }
}