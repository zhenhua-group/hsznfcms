﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="CoperationChargeYearRpt.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationChargeYearRpt" %>
<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>部门目标产值完成报表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>部门目标产值完成报表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>统计条件
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table-responsive">
                                    <tr>
                                        <td>生产部门：</td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="drp_unit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全体部门-------" />
                                        </td>
                                        <td class="year">年份：
                                        </td>
                                        <td class="year">
                                            <%--<cc1:ASDropDownTreeView ID="" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="90px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="---全部---" />--%>
                                            <asp:DropDownList ID="drpYear" runat="server">
                                                <asp:ListItem Value="0">--全部--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="year">季度：
                                        </td>
                                        <td class="year">
                                            <asp:DropDownList ID="drpJidu" runat="server">
                                                <asp:ListItem Value="0">--全部--</asp:ListItem>
                                                <asp:ListItem Value="1" Text="一季度"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="二季度"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="三季度"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="四季度"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="year">月份：
                                        </td>
                                        <td class="year">
                                            <asp:DropDownList ID="drpMonth" runat="server">
                                                <asp:ListItem Value="0" Text="--全部--"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="一月"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="二月"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="三月"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="四月"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="五月"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="六月"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="七月"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="八月"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="九月"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="十月"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="十一月"></asp:ListItem>
                                                <asp:ListItem Value="12" Text="十二月"></asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                        <td class="noyear" style="display: none;">收款时间：
                                        </td>
                                        <td class="noyear" style="display: none;">
                                            <input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 100px; height: 25px; vertical-align: middle; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td class="noyear" style="display: none;">至<input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" style="width: 100px; vertical-align: middle; height: 25px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>统计类型
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpType" runat="server">
                                                <asp:ListItem Value="1">产值</asp:ListItem>
                                                <asp:ListItem Value="2">合同额</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkTime" runat="server" />时间段
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" Text="查询" CssClass="btn blue btn-sm" OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>图形报表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <a href="CoperationChargeYearDetailsBymaster.aspx?flag=a" class="btn red btn-sm">返回</a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div id="main" style="height: 500px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="../js/echarts/build/dist/echarts.js"></script>

    <script type="text/javascript">



        $(function () {

            


            //季度选择
            $("#ctl00_ContentPlaceHolder1_drpJidu").change(function() {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled",true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                }
            });
            //月份选择
            $("#ctl00_ContentPlaceHolder1_drpMonth").change(function() {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled",true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
            });
            //时间段
            $("#ctl00_ContentPlaceHolder1_chkTime").click(function() {
                var obj = $(this);
     
                if (obj.attr("checked")) {
                    $(".year").hide();
                    $(".noyear").show();
                } else {
                    $(".year").show();
                    $(".noyear").hide();
                }
            });

            //判断时间段是否为选中状态
            if ($("#ctl00_ContentPlaceHolder1_chkTime").attr("checked")) {
                $(".year").hide();
                $(".noyear").show();
            }
           

            //点击查询后的图
            loadChartsData();
        });

        var loadChartsData = function () {

            //配置路径
            require.config({
                paths: {
                    echarts: '../js/echarts/build/dist'
                }
            });
            //加载
            require(
                ['echarts',
                    'echarts/chart/bar'],
                function (ec) {
                    var mycharts = ec.init(document.getElementById('main'));

                    var option = {
                        title: {
                            text:"部门产值目标完成统计",
                            subtext:"单位：（万元）"
                        },
                        tooltip: {
                            trigger: 'axis',
                            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                            },
                            formatter: function (params){
                                return params[0].name + '<br/>'
                                       + params[0].seriesName + ' : ' + params[0].value + '<br/>'
                                       + params[1].seriesName + ' : ' + (params[1].value + params[0].value);
                            }

                        },
                        legend: {
                            selectedMode:false,
                            data: <%= LegendData %>
                        },
                        toolbox: {
                            show : true,
                            feature : {
                                mark : {show: true},
                                magicType : {show: true, type: ['line', 'bar']},
                                restore : {show: true},
                                saveAsImage : {show: true}
                            }
                        },
                        calculable : true,
                        xAxis: [
                            {
                                'type': 'category',
                                'axisLabel':{'interval':0},
                                'data': <%= xAxis %>
                            }
                        ],
                        yAxis: [<%= yAxis %>],
                        series: [<%= SeriesData %>]
                    };

                    mycharts.setOption(option);
                }
            );
        }


    </script>
</asp:Content>
