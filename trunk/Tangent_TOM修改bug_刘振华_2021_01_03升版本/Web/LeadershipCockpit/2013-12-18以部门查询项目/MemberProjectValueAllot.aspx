﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberProjectValueAllot.aspx.cs"
    Inherits="TG.Web.LeadershipCockpit.MemberProjectValueAllot" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cls_chart
        {
            font-size: 9pt;
            width: 100%;
        }
        
        .cls_nav
        {
            width: 70px;
        }
        
        .cls_nav a
        {
            text-decoration: none;
        }
        
        .show_projectNumber
        {
            width: 860px;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }
        
        .show_projectNumber td
        {
            border: solid 1px #CCC;
            font-size: 9pt;
            font-family: "微软雅黑";
            height: 20px;
        }
        
        .cls_show_cst_jiben_2
        {
            border-collapse: collapse;
            border: solid 0px black;
            font-size: 9pt;
        }
        
        #labTime
        {
            float: right;
        }
        #labDanW
        {
            float: right;
        }
        #mytab
        {
           
            border-collapse: collapse;
            font-size: 9pt;
            font-family: 微软雅黑;
            margin-left: 10px;
            margin-right: 20px;
            margin-bottom: 20px;
            margin: auto;
        }
        #mytab td
        {
            padding: 0px 4px;
            height: 22px;
            text-align: center;
            background: white;
            border: solid 1px black;
            font-family: 微软雅黑;
            font-size: 9pt;
            text-align: center;
            word-wrap: break-word;
            word-break: break-all;
        }
        #monthtab
        {
            border-collapse: collapse;
            width: 99%;
            font-size: 9pt;
            font-family: 微软雅黑;
            margin-left: 10px;
            margin-right: 20px;
        }
        #monthtab td
        {
            border: solid 1px black;
        }
        .cls_GridView_Style2 .cls_Column_Short
        {
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/FunctionChart/FusionCharts.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/LeadershipCockpit/MemberProjectValueAllot.js" type="text/javascript"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container" style="width:100%;">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[部门产值分配统计大表]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div" >
                    <tr>
                        <td>
                            生产部门：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" Width="120px">
                                <asp:ListItem Value="-1">-----生产部门-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            年份：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_year" runat="server">
                                <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <a href="###" id="btn_ok" style="border: none;">
                                <img src="../Images/buttons/btn_count.gif" style="height: 21px; width: 64px; border: none;" /></a>
                        </td>
                        <td>
                            <a href="###" id="btn_report" style="border: none;">
                                <img src="../Images/buttons/output.gif" style="height: 21px; width: 64px; border: none;" /></a>
                        </td>
                        <td>
                            <input type="hidden" runat="server" value="" id="pp" />
                            <input type="hidden" runat="server" value="" id="memid" />
                            <input type="hidden" runat="server" value="" id="unitid" />
                            <input type="hidden" runat="server" value="" id="mytabhtml"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" class="cls_show_cst_jiben">
        <tr>
            <td style="border: none;">
                <div class="cls_Container_Report">
                    <div class="cls_Container_Tip" style="margin-top:5px; font-weight:bold;">
                        <label id="title">
                        </label>
                        <label id="unit">
                        </label>
                        部门产值分配统计大表
                    </div>
                    <div style="width: 100%;">
                        <div id="yeardiv" runat="server">
                            <table id="mytab" runat="server">
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
