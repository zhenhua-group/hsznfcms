﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Text;



namespace TG.Web.HttpHandler
{
    /// <summary>
    /// ProjectValueAllot 的摘要说明
    /// </summary>
    public class ProjectValueAllot : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            TG.BLL.tg_member copcon = new BLL.tg_member();

            TG.BLL.StandBookBp standbll = new BLL.StandBookBp();

            context.Response.ContentType = "text/plain";
            string action = context.Request.QueryString["action"];
            if (action == "1")
            {
                string year = context.Request["year"].ToString().Trim();
                int unitid = Convert.ToInt32(context.Request["unitid"]);
                int UserSysNo = Convert.ToInt32(context.Request["memid"]);
                int UserUnitNo = Convert.ToInt32(context.Request["UserUnitNo"]);
                int PreviewPattern = int.Parse(context.Request["pp"]);
                StringBuilder strWhere = new StringBuilder("");

                string unitName = "", sqlwhere = " 1=1 ",sqldatewhere="";
                TG.Model.tg_unit tgunit = new TG.Model.tg_unit();
                if (unitid > 0)
                {
                    sqlwhere = sqlwhere+" and mem_Unit_ID=" + unitid + "";
                    tgunit = new TG.BLL.tg_unit().GetModel(unitid);
                }
                if (tgunit != null)
                {
                    unitName = tgunit.unit_Name.Trim();
                }
                //按照部门
                if (!string.IsNullOrEmpty(unitName))
                {
                    strWhere.AppendFormat(" AND a.mem_ID in (select mem_ID from tg_member where mem_Unit_ID={0} union all select mem_ID from cm_externalMember where mem_Unit_ID={0})", unitid);
                }
                //按照年份
                if (int.Parse(year) > 0)
                {
                    sqldatewhere = sqldatewhere+" AND year(AllotDate)="+year+"";
                }
                //个人
                if (PreviewPattern == 0)
                {
                    sqlwhere = sqlwhere + " and mem_ID=" + UserSysNo + "  and  mem_type=0 ";
                    strWhere.Append(" AND a.mem_ID =" + UserSysNo + " and a.IsExternal=0");
                }
                //部门
                else if (PreviewPattern == 2)
                {
                    strWhere.Append(" AND a.mem_ID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + "  union all Select mem_ID From cm_externalMember Where mem_Unit_ID=" + UserUnitNo + ")");
                }

                List<TG.Model.ProjectValueAllot> listSC = standbll.GetMemberProjectValueAllotProc(strWhere.ToString(), sqldatewhere);

                string td1 = "<tr><td colspan='2' style='font-weight:bold;width:150px;'>项目</td>";
                string td2 = "<tr><td colspan='2' style='font-weight:bold;'>实收产值(万元)</td>";
                string td3 = "<tr><td colspan='2' style='font-weight:bold;'>转经济所(万元)</td>";
                string td3_1 = "<tr><td colspan='2' style='font-weight:bold;'>转暖通所(万元)</td>";
                string td3_2 = "<tr><td colspan='2' style='font-weight:bold;'>转土建所(万元)</td>";
                string td4 = "<tr><td colspan='2' style='font-weight:bold;'>本所产值(万元)</td>";
                string td5 = "<tr><td colspan='2' style='font-weight:bold;'>所留(万元)</td>";
                string td6 = "<tr style='font-weight:bold;'><td>序号</td><td>姓名</td>";
                string tdend = "<tr style='font-weight:bold;'><td colspan='2'>总计：</td>";
                decimal SumCharge = 0, SumWeiFenValue = 0, SumToOtherValue = 0, SumActualValue = 0, SumHavcValue = 0, SumTranBulidingValue = 0;
                foreach (TG.Model.ProjectValueAllot pv in listSC)
                {
                    td1 = td1 + "<td style='width:150px;'>" + pv.CprName + "</td>";
                    td2 = td2 + "<td>" + pv.Charge.ToString("f2") + "</td>";
                    td3 = td3 + "<td>" + pv.EconomyValue.ToString("f2") + "</td>";
                    td3_1 = td3_1 + "<td>" + pv.HavcValue.ToString("f2") + "</td>";
                    td3_2 = td3_2 + "<td>" + pv.TranBulidingValue.ToString("f2") + "</td>";
                    td4 = td4 + "<td>" + pv.UnitValue.ToString("f2") + "</td>";
                    td5 = td5 + "<td>" + pv.TheDeptValue.ToString("f2") + "</td>";                   
                    td6 = td6 + "<td>(元)</td>";
                    SumCharge = SumCharge + pv.Charge;
                    SumWeiFenValue = SumWeiFenValue + pv.EconomyValue;
                    SumHavcValue = SumHavcValue + pv.HavcValue;
                    SumTranBulidingValue = SumTranBulidingValue + pv.TranBulidingValue;
                    SumToOtherValue = SumToOtherValue + pv.UnitValue;
                    SumActualValue = SumActualValue + pv.TheDeptValue;
                }
                td1 = td1 + "<td style='width:150px;'>二次分配产值</td><td style='width:150px;'>方案补贴</td><td style='font-weight:bold;width:150px;'>总计(万元)</td></tr>";
                td2 = td2 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumCharge.ToString("f2") + "</td></tr>";
                td3 = td3 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumWeiFenValue.ToString("f2") + "</td></tr>";
                td3_1 = td3_1 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumHavcValue.ToString("f2") + "</td></tr>";
                td3_2 = td3_2 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumTranBulidingValue.ToString("f2") + "</td></tr>";
                td4 = td4 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumToOtherValue.ToString("f2") + "</td></tr>";
                td5 = td5 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumActualValue.ToString("f2") + "</td></tr>";
                td6 = td6 + "<td>(元)</td><td>(元)</td><td>(元)</td></tr>";

                string td = "";
                //总纵向统计
                decimal sumall = 0;
                //总二次分配产值
                decimal sumercz = 0;
                //总方案补贴
                decimal sumsbcz = 0;

                string sql = @"select * from (select mem_ID,mem_Name,mem_Unit_ID,0 as mem_type from tg_member 
                    union all 
                    select mem_ID,mem_Name,mem_Unit_ID,1 as mem_type from cm_externalMember ) a where " + sqlwhere;
                DataTable listme = DBUtility.DbHelperSQL.Query(sql).Tables[0];
               // List<TG.Model.tg_member> listme = copcon.GetModelList(sqlwhere);
                if (listme!=null&&listme.Rows.Count>0)
                {
              
                    for (int i = 0; i < listme.Rows.Count; i++)
                    {
                        string uname = listme.Rows[i]["mem_Name"].ToString();
                        if (listme.Rows[i]["mem_type"].ToString() == "1")
                        {
                            uname = uname + "(外聘)";
                        }
                        string innertr = "<tr><td>" + (i + 1) + "</td><td>" + uname + "</td>";
                        decimal summoney = 0;
                        foreach (TG.Model.ProjectValueAllot pv in listSC)
                        {
                            decimal sumvalue = standbll.GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(), year);
                            innertr = innertr + "<td>" + sumvalue.ToString("f2") + "</td>";
                            summoney = summoney + sumvalue;
                        }
                        //查询人员的方案补贴
                        decimal sbcz = standbll.GetMemberProjectComValue(" and ValueYear='" + year + "' and MemberId=" + listme.Rows[i]["mem_ID"] + " and UnitId=" + unitid + "");
                        //查询人员二次分配产值
                        decimal ercz = standbll.GetMemberProjectValueAllotSql("2", listme.Rows[i]["mem_ID"].ToString(), "", year);
                        //方案补贴之和
                        sumsbcz = sumsbcz + sbcz;
                        //二次分配产值
                        sumercz = sumercz + ercz;
                        //纵向统计
                        summoney = summoney + sbcz+ercz;
                        //添加到tr中。
                        innertr = innertr + "<td>" + ercz.ToString("f2") + "</td><td>" + sbcz.ToString("f2") + "</td><td style='font-weight:bold;'>" + summoney.ToString("f2") + "</td></tr>";
                        td = td + innertr;
                        //纵向统计之和
                        sumall = sumall + summoney;
                    }

                    //横向统计
                    foreach (TG.Model.ProjectValueAllot pv in listSC)
                    {
                        decimal summoney = 0;
                        for (int i = 0; i < listme.Rows.Count; i++)
                        {
                            decimal sumvalue = standbll.GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(), year);

                            summoney = summoney + sumvalue;
                        }
                        tdend = tdend + "<td>" + summoney.ToString("f2") + "</td>";
                    }

                    //所补产值总计和纵向统计的显示
                    tdend = tdend + "<td>" + sumercz.ToString("f2") + "</td><td>" + sumsbcz.ToString("f2") + "</td><td>" + sumall.ToString("f2") + "</td></tr>";
                 }
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = td1 + td2 + td3 + td3_1 + td3_2 + td4 + td5 + td6 + td + tdend;
                context.Response.Write(json);
            }
           

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}