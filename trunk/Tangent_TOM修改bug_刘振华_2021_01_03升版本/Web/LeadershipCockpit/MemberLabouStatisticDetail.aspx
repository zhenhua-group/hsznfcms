﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="MemberLabouStatisticDetail.aspx.cs" Inherits="TG.Web.LeadershipCockpit.MemberLabouStatisticDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/LeadershipCockpit/MemberLabouStatisticDetail_jq.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#show1_4").parent().attr("class", "active");
            $("#arrow1").attr("class", "arrow open");
            $("#show1_4").parent().parent().css("display", "block");

        });
        //清空数据  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页 <small>人员劳动力统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">院项目统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>
                                <asp:Label ID="lbl_Name" runat="server" ForeColor="Red"></asp:Label>
                                <asp:Label ID="lbl_type" runat="server"></asp:Label>
                            </div>
                            <div class="actions">
                                <asp:Button ID="btn_Output" runat="server" Text="导出" CssClass="btn red btn-sm" OnClick="btn_Output_Click" />
                                <input type="button" value="返回" onclick="javascript: window.history.back();" class="btn default btn-sm">
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <div class="form-body">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <table id="jqGrid">
                                            </table>
                                            <div id="gridpager">
                                            </div>
                                            <div id="nodata" class="norecords">
                                                没有符合条件数据！
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_mem_id" runat="server" Value="" />
    </div>
</asp:Content>
