﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Data;
using TG.Web.HttpHandler;
using Geekees.Common.Controls;
using System.Xml;
using TG.DBUtility;

namespace TG.Web.LeadershipCockpit
{
    public partial class UpdateCoperationAcount : PageBase
    {
        //返回标示
        public string BackFlag
        {
            get
            {
                return Request["flag"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {           
            //合同ID
            string str_cprid = Request.QueryString["cprid"] ?? "";
            string str_flag = Request.QueryString["flag"] ?? "";
            if (!IsPostBack)
            {
                this.hid_cprid.Value = str_cprid;
                this.hid_flag.Value = str_flag;
                //显示合同信息
                ShowCoperation(str_cprid);

            }
        }


        protected void ShowCoperation(string cprid)
        {
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            TG.Model.cm_Coperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            if (model_cpr != null)
            {
                //赋值
                this.txtcpr_No.Text = model_cpr.cpr_No ?? "";
                this.ddcpr_Type.Text = model_cpr.cpr_Type.Trim();
                this.txt_cprType.Text = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
                this.txt_cprName.Text = model_cpr.cpr_Name.Trim();
                this.txt_cprBuildUnit.Text = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();

                this.txt_acount.Value = model_cpr.cpr_Acount.ToString();

                this.txtcpr_Account0.Text = model_cpr.cpr_ShijiAcount.ToString();
                // this.txtInvestAccount.Text = model_cpr.cpr_Touzi.ToString();
                // this.txtInvestAccount0.Text = model_cpr.cpr_ShijiTouzi.ToString();
                string result = "";
                if (!string.IsNullOrEmpty(model_cpr.cpr_Process))
                {
                    if (model_cpr.cpr_Process.Trim() != "")
                    {
                        string[] array = model_cpr.cpr_Process.Split(new char[] { ',' }, StringSplitOptions.None);
                        TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                        for (int i = 0; i < array.Length; i++)
                        {
                            result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                        }
                        result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
                    }
                }
                //  this.ddcpr_Stage.Text = result;
                //合同统计年份
                this.txtSingnDate.Text = Convert.ToDateTime(model_cpr.cpr_SignDate).ToShortDateString();
                this.txtCompleteDate.Text = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToShortDateString();
                //  this.txtcpr_Remark.Text = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
                this.txt_buildArea.Text = Convert.ToString(model_cpr.BuildArea ?? "0").Trim();
                this.txt_proFuze.Text = model_cpr.ChgPeople.Trim();
                // this.txt_fzphone.Text = model_cpr.ChgPhone.Trim();
                this.txtFParty.Text = model_cpr.ChgJia.Trim();
                // this.txt_jiafphone.Text = model_cpr.ChgJiaPhone.Trim();
                if (!string.IsNullOrEmpty(model_cpr.BuildPosition))
                {
                    if (model_cpr.BuildPosition == "-1")
                    {
                        this.ddProjectPosition.Text = "";
                    }
                    else
                    {
                        this.ddProjectPosition.Text = model_cpr.BuildPosition.Trim();
                    }
                }
                if (model_cpr.Industry.Trim() == "-1")
                {
                    this.ddProfessionType.Text = "";
                }
                else
                {
                    this.ddProfessionType.Text = model_cpr.Industry.Trim();
                }
                if (model_cpr.BuildSrc.Trim() == "-1")
                {
                    this.ddSourceWay.Text = "";
                }
                else
                {
                    this.ddSourceWay.Text = model_cpr.BuildSrc.Trim();
                }
                this.txt_cjbm.Text = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit;
                // this.txt_tbcreate.Text = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
                if (model_cpr.StructType != null)
                {
                    //   this.lbl_StructType.Text = TG.Common.StringPlus.ResolveStructString(model_cpr.StructType.ToString());
                }
                //建筑分类
                if (model_cpr.BuildStructType != null)
                {
                    //  this.lbl_BuildStructType.Text = TG.Common.StringPlus.ResolveStructString(model_cpr.BuildStructType.ToString());
                }
                ////设计等级
                if (model_cpr.BuildType != null)
                {
                    this.txt_buildType.Text = model_cpr.BuildType.Trim().Replace('+', ',');
                }

                this.txt_buildType.Text = model_cpr.BuildType == null ? "" : model_cpr.BuildType.Trim();
                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.lbl_upfloor.Text = floors[0].ToString();
                this.lbl_downfloor.Text = floors[1].ToString();
                //多栋楼
                //  this.txt_multibuild.Text = model_cpr.MultiBuild;
                //签订日期
                this.txtSingnDate2.Text = model_cpr.cpr_SignDate2 == null ? "" : Convert.ToDateTime(model_cpr.cpr_SignDate2).ToShortDateString();
            }
            //this.txt_area.Text = model_cpr.cpr_Area;
        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            string acount = "0";
            string str_cprid = Request.QueryString["cprid"] ?? "0";
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            TG.Model.cm_Coperation model_cpr = bll_cpr.GetModel(Convert.ToInt32(str_cprid));
            if (model_cpr != null)
            {
                acount = model_cpr.cpr_Acount.ToString();
                string cpr_subid = str_cprid;
                string pageurl = "cpr_ShowCoprationBymaster.aspx";
                string sql = "update cm_Coperation set cpr_Acount=" + this.txt_acount.Value + " where cpr_id=" + str_cprid;
                string sql2 = "select cpr_id from cm_Coperation where cpr_id=" + str_cprid;//查询其他合同的id

                string tablename = DiffCoperation(model_cpr.cpr_Type2, ref pageurl);
                if (tablename != "")
                {
                    sql = sql + ";update " + tablename + " set cpr_Acount=" + this.txt_acount.Value + " where cpr_FID=" + str_cprid;
                    sql2 = "select top 1 cpr_id from " + tablename + " where cpr_FID=" + str_cprid;
                }
                int num = DbHelperSQL.ExecuteSql(sql);
                if (num > 0)
                {
                    object o = DbHelperSQL.GetSingle(sql2);
                    if (o != null && Convert.ToInt32(o) > 0)
                    {
                        cpr_subid = o.ToString();
                    }
                    else
                    {
                        pageurl = "cpr_ShowCoprationBymaster.aspx";
                        cpr_subid = str_cprid;
                    }

                    //发送消息给所长
                    string unitname = model_cpr.cpr_Unit.Trim();
                    List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(" mem_Principalship_ID=2 and mem_Unit_ID=(select unit_ID from tg_unit where unit_Name='" + unitname + "')");
                    if (list.Count > 0)
                    {
                        foreach (TG.Model.tg_member mem in list)
                        {
                            //实例消息实体
                            TG.Model.SysMessageViewEntity sysMessageViewEntity = new TG.Model.SysMessageViewEntity
                            {
                                //ReferenceSysNo = tempEntity.SysNo.ToString(),
                                ReferenceSysNo = string.Format("{0}?cprid={1}", pageurl, cpr_subid),
                                FromUser = mem.mem_ID,
                                InUser = UserSysNo,
                                MsgType = 100,
                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", model_cpr.cpr_Name, ("合同额由" + acount + "万元，修改成" + this.txt_acount.Value + "万元的")),
                                QueryCondition = model_cpr.cpr_Name,
                                Status = "A",
                                IsDone = "B",
                                ToRole = "0"
                            };
                            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        }
                        //弹出提示
                        TG.Common.MessageBox.ShowAndRedirect(this, "合同额更新成功，已发送消息合同所在部门的所长！", "../LeadershipCockpit/UpdateCoperationList.aspx");


                    }
                    else
                    {
                        Response.Write("<script language='javascript'>alert('合同额更新成功，合同所在部门无所长消息无法发送！');window.location.href='/LeadershipCockpit/UpdateCoperationList.aspx';</script>");

                    }

                }
                else
                {
                    Response.Write("<script language='javascript'>alert('合同更新失败！');window.location.href='/LeadershipCockpit/UpdateCoperationList.aspx';</script>");
                }
            }
        }
        /// <summary>
        /// 获取合同表名
        /// </summary>
        /// <param name="cpr_type2"></param>
        /// <param name="pageurl"></param>
        /// <returns></returns>
        public string DiffCoperation(string cpr_type2, ref string pageurl)
        {
            string url = "";
            switch (cpr_type2)
            {
                case "岩土工程勘察合同":
                    url = "cm_ReconnCoperation";
                    pageurl = "cpr_ShowReconnCoprationBymaster.aspx";
                    break;
                case "岩土工程设计合同":
                    url = "cm_DesignCoperation";
                    pageurl = "cpr_ShowDesignCoprationBymaster.aspx";
                    break;
                case "基坑及边坡监测合同":
                    url = "cm_PitCoperation";
                    pageurl = "cpr_ShowPitCoperationBymaster.aspx";
                    break;
                case "测量及沉降观测合同":
                    url = "cm_MeasureCoperation";
                    pageurl = "cpr_ShowTestCoperationBymaster.aspx";
                    break;
                case "岩土工程检测合同":
                    url = "cm_TestCoperation";
                    pageurl = "cpr_ShowMeasureCoperationBymaster.aspx";
                    break;
                case "岩土工程施工合同":
                    url = "cm_ConstruCoperartion";
                    pageurl = "cpr_ShowConstruCoperartionBymaster.aspx";
                    break;
                case "规划工程合同":
                    url = "cm_PlanningCoperation";
                    pageurl = "cpr_ShowPlanningCoperationBymaster.aspx";
                    break;
                case "景观规划合同":
                    url = "cm_SceneryCoperation";
                    pageurl = "cpr_ShowSceneryCoperationBymaster.aspx";
                    break;
                case "市政工程合同":
                    url = "cm_EngineerCoperation";
                    pageurl = "cpr_ShowEngineerCoperationBymaster.aspx";
                    break;
                case "工程监理":
                case "市政工程监理合同":
                case "景观绿化工程监理合同":
                case "天然气输配工程监理合同":
                case "管网、热力站工程监理合同":
                case "土方工程监理合同":
                    url = "cm_SuperCoperation";
                    pageurl = "cpr_ShowSuperCoperationBymaster.aspx";
                    break;
            }
            return url;
        }     
       
            
    }
}