﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace TG.Web.LeadershipCockpit
{
    public partial class ProjectValueAllotBymaster : PageBase
    {
        public int pro_Id
        {
            get
            {
                return string.IsNullOrEmpty(Request["pro_id"]) == true ? 0 : Convert.ToInt32(Request["pro_id"]);
            }
        }

        public string pp
        {
            get
            {
                return string.IsNullOrEmpty(Request["pp"]) == true ? "0" : Request["pp"];
            }
        }
        public string year
        {
            get
            {
                return string.IsNullOrEmpty(Request["year"]) == true ? DateTime.Now.Year.ToString() : Request["year"];
            }
        }
        public int unitid
        {
            get;
            set;
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_Id);
                if (pro != null)
                {
                    this.pro_name.Text = pro.pro_name;
                    DataTable dtunit = new TG.BLL.tg_unit().GetList(" unit_Name='" + pro.Unit.Trim() + "'").Tables[0];
                    if (dtunit != null && dtunit.Rows.Count > 0)
                    {
                        unitid = Convert.ToInt32(dtunit.Rows[0]["unit_ID"]);
                    }
                    else
                    {
                        unitid = 0;
                    }

                }

                string tr = "";
                decimal allsum = 0m;
                string sqlwhere = "";
                int allcount = 0;
                //个人权限
                //   if (pp == "0")
                //  {
                //     sqlwhere=" AND a.mem_ID=" + base.UserSysNo + "";
                // }
                // else if(pp == "2")
                //  {
                //      sqlwhere = " AND a.mem_ID in (Select mem_ID From tg_member Where mem_Unit_ID=" + base.UserUnitNo + " union all select mem_ID from cm_externalMember where mem_Unit_ID=" + base.UserUnitNo + ")";
                //   }


                DataTable dt = new TG.BLL.StandBookBp().GetMemberProjectValueAllot(pro_Id, year, sqlwhere);
                //部门人员
                DataTable dt1 = new DataView(dt) { RowFilter = "mem_Unit_ID=" + unitid + " and IsExternal='0'" }.ToTable();
                allcount = dt1.Rows.Count;
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        allsum += Convert.ToDecimal(dt1.Rows[i]["totalCount"]);
                        string uname = dt1.Rows[i]["mem_Name"].ToString();
                        tr = tr + "<tr>";
                        if (i == 0)
                        {
                            tr = tr + "<td class='cscolumn' rowspan='" + allcount + "'>本部门(" + pro.Unit.Trim() + ")</td>";
                        }
                        tr = tr + "<td>" + (i + 1) + "</td><td>" + uname + "</td><td>" + Convert.ToDecimal(dt1.Rows[i]["totalCount"]).ToString("f2") + "</td></tr>";
                    }
                }
                //外所人员
                DataTable dt2 = new DataView(dt) { RowFilter = "mem_Unit_ID<>" + unitid + " and IsExternal='0'" }.ToTable();
                allcount = dt2.Rows.Count;
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        allsum += Convert.ToDecimal(dt2.Rows[i]["totalCount"]);
                        string uname = dt2.Rows[i]["mem_Name"].ToString();
                        tr = tr + "<tr>";
                        if (i == 0)
                        {
                            tr = tr + "<td class='cscolumn' rowspan='" + allcount + "'>外所部门</td>";
                        }
                        tr = tr + "<td>" + (i + 1) + "</td><td>" + uname + "</td><td>" + Convert.ToDecimal(dt2.Rows[i]["totalCount"]).ToString("f2") + "</td></tr>";
                    }
                }

                //外聘人员
                DataTable dt3 = new DataView(dt) { RowFilter = "IsExternal='1'" }.ToTable();
                allcount = dt3.Rows.Count;
                if (dt3 != null && dt3.Rows.Count > 0)
                {
                    for (int i = 0; i < dt3.Rows.Count; i++)
                    {
                        allsum += Convert.ToDecimal(dt3.Rows[i]["totalCount"]);
                        string uname = dt3.Rows[i]["mem_Name"].ToString();
                        tr = tr + "<tr>";
                        if (i == 0)
                        {
                            tr = tr + "<td class='cscolumn' rowspan='" + allcount + "'>外聘人员</td>";
                        }
                        tr = tr + "<td>" + (i + 1) + "</td><td>" + uname + "</td><td>" + Convert.ToDecimal(dt3.Rows[i]["totalCount"]).ToString("f2") + "</td></tr>";
                    }
                }


                string foot = "<tr><Td class='cscolumn'>总计：</td><td>&nbsp;</td><td>" + dt.Rows.Count + "(人)</td><td>" + allsum.ToString("f2") + "</td></tr>";
                this.mytabhtml.Value = tr + foot;
            }
        }
    }
}