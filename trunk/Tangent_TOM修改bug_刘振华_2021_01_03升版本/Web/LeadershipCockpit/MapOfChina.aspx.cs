﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using TG.BLL;
using AjaxPro;
using TG.Model;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace TG.Web.LeadershipCockpit
{
    public partial class MapOfChina : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(TG.Web.LeadershipCockpit.MapOfChina));
        }

        /// <summary>
        /// 得到每个省份的项目数
        /// </summary>
        [AjaxMethod]
        public string GetRecordOfProvince(int ssf)
        {
            MapOfChinaBP mapBP = new MapOfChinaBP();

            List<MapOfChinaEntity> provinces = mapBP.GetProjectCountByProvinceName();

            return Newtonsoft.Json.JsonConvert.SerializeObject(provinces);
        }

        /// <summary>
        /// 根据省份名称，得到项目列表
        /// </summary>
        /// <param name="provinceName"></param>
        /// <param name="pageCurrent"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetProjectListByProvinceName(string provinceName, int pageCurrent)
        {
            List<ProjectForMap> projectList = new MapOfChinaBP().GetProjectListByProvinceName(provinceName, 15, pageCurrent - 1);

            return Newtonsoft.Json.JsonConvert.SerializeObject(projectList);
        }

        /// <summary>
        /// 根据项目SysNo得到项目信息
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetProjectInfoByProjectSysNo(int projectSysNo)
        {
            ProjectDetailForMap project = new MapOfChinaBP().GetProjectInfoByProjectSysNo(projectSysNo);

            return Newtonsoft.Json.JsonConvert.SerializeObject(project);
        }
    }
}
