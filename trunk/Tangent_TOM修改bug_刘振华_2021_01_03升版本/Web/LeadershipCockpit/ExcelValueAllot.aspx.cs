﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.UserModel;
using System.Data;

namespace TG.Web.LeadershipCockpit
{
    public partial class ExcelValueAllot : System.Web.UI.Page
    {
        public string year = "", unitName = "";
        public int unitid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            year = Request["year"].ToString().Trim();
            unitid = Convert.ToInt32(Request["unitid"]);
            int UserSysNo = Convert.ToInt32(Request["memid"]);
            int UserUnitNo = Convert.ToInt32(Request["UserUnitNo"]);
            int PreviewPattern = int.Parse(Request["pp"]);
            StringBuilder strWhere = new StringBuilder("");
            string sqlwhere = "", sbsqlwhere = " and ValueYear='" + year + "'", sqldatewhere = "", strwhere1 = "", strwhere2 = "";
            TG.Model.tg_unit tgunit = new TG.Model.tg_unit();
            if (unitid > 0)
            {

                sbsqlwhere = sbsqlwhere + " and UnitId=" + unitid + "";
                tgunit = new TG.BLL.tg_unit().GetModel(unitid);
            }
            if (tgunit != null)
            {
                unitName = tgunit.unit_Name.Trim();
            }

            //按照部门
            if (!string.IsNullOrEmpty(unitName))
            {
                strWhere.AppendFormat(" AND (p.Unit= '{0}')", unitName);
            }
            //按照年份
            if (int.Parse(year) > 0)
            {
                strwhere1 = " and year(InAcountTime)=" + year;
                strwhere2 = " and ActualAllountTime='" + year + "'";
                sqlwhere = " and pva.ActualAllountTime='" + year + "'";
            }
            #region 权限

            //个人
            //   if (PreviewPattern == 0)
            //  {
            //     sqlwhere = sqlwhere + " and pvb.mem_ID=" + UserSysNo + " and  pvb.IsExternal=0";
            //     strWhere.Append(" AND p.InsertUserID =" + UserSysNo + "");
            // }
            //部门
            // else if (PreviewPattern == 2)
            //  {
            //      sqlwhere = sqlwhere + " and pvb.mem_ID in (select mem_ID from tg_member where mem_Unit_ID=" + UserUnitNo + " union all select mem_ID from cm_ExternalMember where mem_Unit_ID=" + UserUnitNo + ")";
            //      strWhere.Append(" AND p.InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            //   }
            #endregion
            string filename = "";

            if (year != "-1")
            {
                filename = filename + year + "年";
            }
            else
            {
                filename = filename + "全年";
            }
            filename += unitName + "部门产值分配统计大表";
            MemoryStream ms = ExportDataToExcel(filename, strWhere.ToString(), sqlwhere, sbsqlwhere, sqldatewhere, strwhere1, strwhere2) as MemoryStream;
            filename = Server.UrlEncode(filename);
            Response.AddHeader("Content-Disposition", ("attachment;filename=" + filename + ".xls"));
            Response.Charset = "UTF-8";
            Response.ContentType = "application/ms-excel";
            Response.BinaryWrite(ms.ToArray());

            ms.Close();
            ms.Dispose();
        }
        public MemoryStream ExportDataToExcel(string title, string strWhere, string sqlwhere, string sbsqlwhere, string sqldatewhere, string strwhere1, string strwhere2)
        {

            //创建workbook
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            ISheet sheet = workbook.CreateSheet("sheet1");

            //表头样式
            ICellStyle style1 = workbook.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;


            style1.WrapText = true;
            //表头单元格边框
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //表头字体设置
            IFont font1 = workbook.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.Boldweight = (short)2000;//加粗
            font1.FontName = "宋体";//字体
            //font.Color = HSSFColor.ROSE.index;//颜色
            style1.SetFont(font1);


            //样式2
            ICellStyle style2 = workbook.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            //表头单元格边框
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //字体设置2
            IFont font2 = workbook.CreateFont();
            font2.FontHeightInPoints = 12;//字号
            //font2.Boldweight = 10;//加粗
            font2.FontName = "宋体";//字体
            //font2.Color = HSSFColor.ROSE.index;//颜色
            style2.SetFont(font2);

            //样式3
            ICellStyle style3 = workbook.CreateCellStyle();
            style3.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style3.VerticalAlignment = VerticalAlignment.CENTER;
            style3.WrapText = true;
            //表头单元格边框
            style3.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style3.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //表头字体设置
            IFont font3 = workbook.CreateFont();
            font3.FontHeightInPoints = 14;//字号
            font3.Boldweight = (short)2000;//加粗
            font3.FontName = "宋体";//字体
            //font.Color = HSSFColor.ROSE.index;//颜色
            style3.SetFont(font3);



            //第一行
            IRow row = sheet.CreateRow(0);
            row.Height = 30 * 20;

            //第二行
            IRow row2 = sheet.CreateRow(1);
            ICell acell21 = row2.CreateCell(0);
            acell21.SetCellValue("项目");
            CellRangeAddress cellRangeAddress2 = new CellRangeAddress(1, 1, 0, 1);
            sheet.AddMergedRegion(cellRangeAddress2);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell21.CellStyle = style1;



            //第三行
            IRow row3 = sheet.CreateRow(2);
            ICell acell31 = row3.CreateCell(0);
            acell31.SetCellValue("实收产值(万元)");
            CellRangeAddress cellRangeAddress3 = new CellRangeAddress(2, 2, 0, 1);
            sheet.AddMergedRegion(cellRangeAddress3);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress3, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell31.CellStyle = style1;

            //第四行
            IRow row4 = sheet.CreateRow(3);
            ICell acell41 = row4.CreateCell(0);
            acell41.SetCellValue("转经济所(万元)");
            CellRangeAddress cellRangeAddress4 = new CellRangeAddress(3, 3, 0, 1);
            sheet.AddMergedRegion(cellRangeAddress4);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress4, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell41.CellStyle = style1;

            //第五行
            IRow row5 = sheet.CreateRow(4);
            ICell acell51 = row5.CreateCell(0);
            acell51.SetCellValue("转暖通所(万元)");
            CellRangeAddress cellRangeAddress5 = new CellRangeAddress(4, 4, 0, 1);
            sheet.AddMergedRegion(cellRangeAddress5);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress5, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell51.CellStyle = style1;

            //第六行
            IRow row5_1 = sheet.CreateRow(5);
            ICell acell51_1 = row5_1.CreateCell(0);
            acell51_1.SetCellValue("转土建所(万元)");
            CellRangeAddress cellRangeAddress5_1 = new CellRangeAddress(5, 5, 0, 1);
            sheet.AddMergedRegion(cellRangeAddress5_1);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress5_1, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell51_1.CellStyle = style1;

            //第七行
            IRow row6 = sheet.CreateRow(6);
            ICell acell61 = row6.CreateCell(0);
            acell61.SetCellValue("本所产值(万元)");
            CellRangeAddress cellRangeAddress6 = new CellRangeAddress(6, 6, 0, 1);
            sheet.AddMergedRegion(cellRangeAddress6);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress6, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell61.CellStyle = style1;

            //第八行
            IRow row7 = sheet.CreateRow(7);
            ICell acell71 = row7.CreateCell(0);
            acell71.SetCellValue("所留(万元)");
            CellRangeAddress cellRangeAddress7 = new CellRangeAddress(7, 7, 0, 1);
            sheet.AddMergedRegion(cellRangeAddress7);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress7, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell71.CellStyle = style1;

            //第九行
            IRow row8 = sheet.CreateRow(8);
            ICell acell81 = row8.CreateCell(0);
            acell81.SetCellValue("序号");
            acell81.CellStyle = style1;
            ICell acell82 = row8.CreateCell(1);
            acell82.SetCellValue("姓名");
            acell82.CellStyle = style1;


            List<TG.Model.ProjectValueAllot> listSC = new TG.BLL.StandBookBp().GetMemberProjectValueAllotProc(strWhere.ToString(), strwhere1, strwhere2);

            decimal SumCharge = 0, SumWeiFenValue = 0, SumToOtherValue = 0, SumActualValue = 0, SumHavcValue = 0, SumTranBulidingValue = 0;
            int obj = 2;
            foreach (TG.Model.ProjectValueAllot pv in listSC)
            {
                //项目名称
                ICell acell2 = row2.CreateCell(obj);
                acell2.SetCellValue(pv.CprName);
                acell2.CellStyle = style2;
                //实收产值
                ICell acell3 = row3.CreateCell(obj);
                acell3.SetCellValue(pv.Charge.ToString("f2"));
                acell3.CellStyle = style2;
                //转经济所
                ICell acell4 = row4.CreateCell(obj);
                acell4.SetCellValue(pv.EconomyValue.ToString("f2"));
                acell4.CellStyle = style2;
                //转暖通所
                ICell acell5 = row5.CreateCell(obj);
                acell5.SetCellValue(pv.HavcValue.ToString("f2"));
                acell5.CellStyle = style2;
                //转土建所
                ICell acell5_1 = row5_1.CreateCell(obj);
                acell5_1.SetCellValue(pv.TranBulidingValue.ToString("f2"));
                acell5_1.CellStyle = style2;
                //本所产值
                ICell acell6 = row6.CreateCell(obj);
                acell6.SetCellValue(pv.UnitValue.ToString("f2"));
                acell6.CellStyle = style2;
                //所留
                ICell acell7 = row7.CreateCell(obj);
                acell7.SetCellValue(pv.TheDeptValue.ToString("f2"));
                acell7.CellStyle = style2;

                //人员菜单一行
                ICell acell8 = row8.CreateCell(obj);
                acell8.SetCellValue("(元)");
                acell8.CellStyle = style1;

                SumCharge = SumCharge + pv.Charge;
                SumWeiFenValue = SumWeiFenValue + pv.EconomyValue;
                SumHavcValue = SumHavcValue + pv.HavcValue;
                SumTranBulidingValue = SumTranBulidingValue + pv.TranBulidingValue;
                SumToOtherValue = SumToOtherValue + pv.UnitValue;
                SumActualValue = SumActualValue + pv.TheDeptValue;

                obj += 1;

            }
            //一次产值分配
            ICell acell2_3_0 = row2.CreateCell((obj));
            acell2_3_0.SetCellValue("一次分配产值");
            acell2_3_0.CellStyle = style2;

            ICell acell3_3_0 = row3.CreateCell((obj));
            acell3_3_0.SetCellValue("");
            acell3_3_0.CellStyle = style2;

            ICell acell4_3_0 = row4.CreateCell((obj));
            acell4_3_0.SetCellValue("");
            acell4_3_0.CellStyle = style2;

            ICell acell5_3_0 = row5.CreateCell((obj));
            acell5_3_0.SetCellValue("");
            acell5_3_0.CellStyle = style2;

            ICell acell5_3_2 = row5_1.CreateCell((obj));
            acell5_3_2.SetCellValue("");
            acell5_3_2.CellStyle = style2;

            ICell acell6_3_0 = row6.CreateCell((obj));
            acell6_3_0.SetCellValue("");
            acell6_3_0.CellStyle = style2;

            ICell acell7_3_0 = row7.CreateCell((obj));
            acell7_3_0.SetCellValue("");
            acell7_3_0.CellStyle = style2;

            ICell acell8_3_0 = row8.CreateCell((obj));
            acell8_3_0.SetCellValue("(元)");
            acell8_3_0.CellStyle = style1;

            //二次产值分配
            ICell acell2_3 = row2.CreateCell((obj + 1));
            acell2_3.SetCellValue("二次分配产值");
            acell2_3.CellStyle = style2;

            ICell acell3_3 = row3.CreateCell((obj + 1));
            acell3_3.SetCellValue("");
            acell3_3.CellStyle = style2;

            ICell acell4_3 = row4.CreateCell((obj + 1));
            acell4_3.SetCellValue("");
            acell4_3.CellStyle = style2;

            ICell acell5_3 = row5.CreateCell((obj + 1));
            acell5_3.SetCellValue("");
            acell5_3.CellStyle = style2;

            ICell acell5_3_1 = row5_1.CreateCell((obj + 1));
            acell5_3_1.SetCellValue("");
            acell5_3_1.CellStyle = style2;

            ICell acell6_3 = row6.CreateCell((obj + 1));
            acell6_3.SetCellValue("");
            acell6_3.CellStyle = style2;

            ICell acell7_3 = row7.CreateCell((obj + 1));
            acell7_3.SetCellValue("");
            acell7_3.CellStyle = style2;

            ICell acell8_3 = row8.CreateCell((obj + 1));
            acell8_3.SetCellValue("(元)");
            acell8_3.CellStyle = style1;

            //方案补贴
            ICell acell23 = row2.CreateCell((obj + 2));
            acell23.SetCellValue("方案补贴");
            acell23.CellStyle = style2;

            ICell acell33 = row3.CreateCell((obj + 2));
            acell33.SetCellValue("");
            acell33.CellStyle = style2;

            ICell acell43 = row4.CreateCell((obj + 2));
            acell43.SetCellValue("");
            acell43.CellStyle = style2;

            ICell acell53 = row5.CreateCell((obj + 2));
            acell53.SetCellValue("");
            acell53.CellStyle = style2;

            ICell acell53_1 = row5_1.CreateCell((obj + 2));
            acell53_1.SetCellValue("");
            acell53_1.CellStyle = style2;

            ICell acell63 = row6.CreateCell((obj + 2));
            acell63.SetCellValue("");
            acell63.CellStyle = style2;

            ICell acell73 = row7.CreateCell((obj + 2));
            acell73.SetCellValue("");
            acell73.CellStyle = style2;

            ICell acell83 = row8.CreateCell((obj + 2));
            acell83.SetCellValue("(元)");
            acell83.CellStyle = style1;

            //总计
            ICell acell24 = row2.CreateCell((obj + 3));
            acell24.SetCellValue("总计(万元)");
            acell24.CellStyle = style1;

            ICell acell34 = row3.CreateCell((obj + 3));
            acell34.SetCellValue(SumCharge.ToString("f2"));
            acell34.CellStyle = style1;

            ICell acell44 = row4.CreateCell((obj + 3));
            acell44.SetCellValue(SumWeiFenValue.ToString("f2"));
            acell44.CellStyle = style1;

            ICell acell54 = row5.CreateCell((obj + 3));
            acell54.SetCellValue(SumHavcValue.ToString("f2"));
            acell54.CellStyle = style1;

            ICell acell54_1 = row5_1.CreateCell((obj + 3));
            acell54_1.SetCellValue(SumTranBulidingValue.ToString("f2"));
            acell54_1.CellStyle = style1;

            ICell acell64 = row6.CreateCell((obj + 3));
            acell64.SetCellValue(SumToOtherValue.ToString("f2"));
            acell64.CellStyle = style1;

            ICell acell74 = row7.CreateCell((obj + 3));
            acell74.SetCellValue(SumActualValue.ToString("f2"));
            acell74.CellStyle = style1;

            ICell acell84 = row8.CreateCell((obj + 3));
            acell84.SetCellValue("(元)");
            acell84.CellStyle = style1;

            //人员名称
            ICell acell24_0 = row2.CreateCell((obj + 4));
            acell24_0.SetCellValue("人员名称");
            acell24_0.CellStyle = style2;

            ICell acell34_0 = row3.CreateCell((obj + 4));
            acell34_0.SetCellValue("");
            acell34_0.CellStyle = style2;

            ICell acell44_0 = row4.CreateCell((obj + 4));
            acell44_0.SetCellValue("");
            acell44_0.CellStyle = style2;

            ICell acell54_0 = row5.CreateCell((obj + 4));
            acell54_0.SetCellValue("");
            acell54_0.CellStyle = style2;

            ICell acell54_2 = row5_1.CreateCell((obj + 4));
            acell54_2.SetCellValue("");
            acell54_2.CellStyle = style2;

            ICell acell64_0 = row6.CreateCell((obj + 4));
            acell64_0.SetCellValue("");
            acell64_0.CellStyle = style2;

            ICell acell74_0 = row7.CreateCell((obj + 4));
            acell74_0.SetCellValue("");
            acell74_0.CellStyle = style2;

            ICell acell84_0 = row8.CreateCell((obj + 4));
            acell84_0.SetCellValue("");
            acell84_0.CellStyle = style2;


            //总纵向统计
            decimal sumall = 0;
            //总二次分配产值
            decimal sumercz = 0;
            //总方案补贴
            decimal sumsbcz = 0;
            //总一次分配产值
            decimal sumonecz = 0;
            //横向统计
            int obj3 = 0;

            #region 暂无用

            //描述:统计所有项目参与的所有人员 zxq  20131224
            //            string sql = string.Format(@"select * from (
            //                                            select pvb.mem_ID as mem_ID,
            //                                            (case when IsExternal='0' then b.mem_Name else c.mem_Name end) as mem_Name,
            //				  		                    (case when IsExternal='0' then b.mem_Unit_ID else c.mem_Unit_ID end) as mem_Unit_ID,
            //                                            (case when IsExternal='1' then '2' when IsExternal='0' and b.mem_Unit_ID<>{0} then '1' else '0' end ) as ordtype,
            //                                            IsExternal as mem_type
            //                                            From cm_project p join cm_ProjectValueAllot pva on pva.pro_ID=p.pro_ID join  cm_ProjectValueByMemberDetails pvb on pva.ID=pvb.AllotID left join tg_member b on pvb.mem_ID=b.mem_ID left join cm_externalMember c on pvb.mem_ID=c.mem_ID 
            //                                            Where pvb.[Status]='S' {1} {2} group by pvb.IsExternal,b.mem_Unit_ID,b.mem_Name,c.mem_Unit_ID,c.mem_Name,pvb.mem_ID
            //                                        ) aa 
            //                                    order by aa.ordtype asc,aa.mem_ID asc", unitid, strWhere.ToString(), sqlwhere);

            //            DataTable listme = DBUtility.DbHelperSQL.Query(sql).Tables[0];

            //  List<TG.Model.tg_member> listme = new TG.BLL.tg_member().GetModelList(sqlwhere);
            #endregion
            //描述:统计所有项目参与的所有人员 zxq  20131225
            DataTable listme = new TG.BLL.StandBookBp().GetMemberProjectValueAllotDetailProc(unitName, unitid, year);

            if (listme != null && listme.Rows.Count > 0)
            {

                for (int i = 0; i < listme.Rows.Count; i++)
                {
                    IRow rows = sheet.CreateRow((9 + i));
                    ICell acells1 = rows.CreateCell(0);
                    acells1.SetCellValue((i + 1) + "");
                    acells1.CellStyle = style2;

                    ICell acells2 = rows.CreateCell(1);

                    string uname = listme.Rows[i]["mem_Name"].ToString();
                    if (listme.Rows[i]["ordtype"].ToString() == "2")
                    {
                        uname = uname + "(外聘)";
                    }
                    else if (listme.Rows[i]["ordtype"].ToString() == "1")
                    {
                        uname = uname + "(外所)";
                    }
                    string innertr = "<tr><td>" + (i + 1) + "</td><td>" + uname + "</td>";
                    acells2.SetCellValue(uname);
                    acells2.CellStyle = style2;

                    decimal summoney = 0; //一次分配产值项目横向总和
                    decimal summoneyend = 0;//最后一列横向之和
                    int obj2 = 2;
                    foreach (TG.Model.ProjectValueAllot pv in listSC)
                    {
                        //描述:第一次分配产值 zxq  20131225
                        //decimal sumvalue = new TG.BLL.StandBookBp().GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(),year);
                        decimal sumvalue = 0;
                        if (listme.Columns.Contains("pro_" + pv.CprId.ToString() + ""))
                        {
                            sumvalue = Convert.ToDecimal(listme.Rows[i]["pro_" + pv.CprId.ToString() + ""]);
                        }
                        else
                        {
                            sumvalue = 0;
                        }
                        ICell acells3 = rows.CreateCell(obj2);
                        acells3.SetCellValue(sumvalue.ToString("f2"));
                        acells3.CellStyle = style2;

                        summoney = summoney + sumvalue;

                        obj2 += 1;
                    }

                    //查询人员的方案补贴
                    //decimal sbcz = new TG.BLL.StandBookBp().GetMemberProjectComValue(sbsqlwhere + " and MemberId=" + listme.Rows[i]["mem_ID"] + "");
                    //描述:查询人员的方案补贴  zxq  20131225
                    decimal sbcz = Convert.ToDecimal(listme.Rows[i]["ComValue"]);

                    //查询人员二次分配产值
                    //decimal ercz = new TG.BLL.StandBookBp().GetMemberProjectValueAllotSql("2", listme.Rows[i]["mem_ID"].ToString(), "", year);
                    //描述:查询人员二次分配产值  zxq  20131225
                    decimal ercz = Convert.ToDecimal(listme.Rows[i]["totalcount2"]);

                    //最后一行一次分配产值之和
                    sumonecz = sumonecz + summoney;
                    //最后一行方案补贴之和
                    sumsbcz = sumsbcz + sbcz;
                    //最后一行二次分配产值之和
                    sumercz = sumercz + ercz;
                    //最后一列横向总计
                    summoneyend = summoney + sbcz + ercz;

                    //一次分配列
                    ICell acells4_4_0 = rows.CreateCell((obj2));
                    acells4_4_0.SetCellValue(summoney.ToString("f2"));
                    acells4_4_0.CellStyle = style2;

                    //二次分配列
                    ICell acells4_4 = rows.CreateCell((obj2 + 1));
                    acells4_4.SetCellValue(ercz.ToString("f2"));
                    acells4_4.CellStyle = style2;


                    //方案补贴列
                    ICell acells4 = rows.CreateCell((obj2 + 2));
                    acells4.SetCellValue(sbcz.ToString("f2"));
                    acells4.CellStyle = style2;

                    //纵向统计列
                    ICell acells5 = rows.CreateCell((obj2 + 3));
                    acells5.SetCellValue(summoneyend.ToString("f2"));
                    acells5.CellStyle = style1;

                    //人员名称
                    ICell acells6 = rows.CreateCell((obj2 + 4));
                    acells6.SetCellValue(uname);
                    acells6.CellStyle = style2;

                    //纵向统计列之和
                    sumall = sumall + summoneyend;

                }

                //横向统计行
                int rowcount = (9 + listme.Rows.Count);
                IRow rowend = sheet.CreateRow(rowcount);
                ICell acellend1 = rowend.CreateCell(0);
                acellend1.SetCellValue("总计：");
                CellRangeAddress cellRangeAddressend = new CellRangeAddress(rowcount, rowcount, 0, 1);
                sheet.AddMergedRegion(cellRangeAddressend);
                ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddressend, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
                acellend1.CellStyle = style1;

                //横向统计
                obj3 = 2;
                foreach (TG.Model.ProjectValueAllot pv in listSC)
                {
                    decimal summoney = 0;
                    for (int i = 0; i < listme.Rows.Count; i++)
                    {
                        // decimal sumvalue = new TG.BLL.StandBookBp().GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(), year);
                        //描述:横向统计项目之和  zxq  20131225
                        decimal sumvalue = 0;
                        if (listme.Columns.Contains("pro_" + pv.CprId.ToString() + ""))
                        {
                            sumvalue = Convert.ToDecimal(listme.Rows[i]["pro_" + pv.CprId.ToString() + ""]);
                        }
                        summoney = summoney + sumvalue;
                    }
                    ICell acellend2 = rowend.CreateCell(obj3);
                    acellend2.SetCellValue(summoney.ToString("f2"));
                    acellend2.CellStyle = style1;
                    obj3 += 1;
                }
                //一次产值总计
                ICell acellend3_3_0 = rowend.CreateCell((obj3));
                acellend3_3_0.SetCellValue(sumonecz.ToString("f2"));
                acellend3_3_0.CellStyle = style1;
                //二次产值总计
                ICell acellend3_3 = rowend.CreateCell((obj3 + 1));
                acellend3_3.SetCellValue(sumercz.ToString("f2"));
                acellend3_3.CellStyle = style1;

                //所补产值总计
                ICell acellend3 = rowend.CreateCell((obj3 + 2));
                acellend3.SetCellValue(sumsbcz.ToString("f2"));
                acellend3.CellStyle = style1;
                //纵向统计总和列
                ICell acellend4 = rowend.CreateCell((obj3 + 3));
                acellend4.SetCellValue((sumall).ToString("f2"));
                acellend4.CellStyle = style1;
                //人员名称
                ICell acellend6 = rowend.CreateCell((obj3 + 4));
                acellend6.SetCellValue("");
                acellend6.CellStyle = style1;
            }
            //第一行单元格内容
            int cellcount = (obj3 + 5);
            ICell acell0 = row.CreateCell(0);
            acell0.SetCellValue(title);
            CellRangeAddress cellRangeAddress0 = new CellRangeAddress(0, 0, 0, (cellcount - 1));
            sheet.AddMergedRegion(cellRangeAddress0);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress0, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            acell0.CellStyle = style3;

            for (int i = 0; i < cellcount; i++)
            {
                sheet.SetColumnWidth(i, 15 * 256);
                // sheet.CreateRow(0).Height = 200 * 20; 第一行高度

            }


            //返回值
            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;

            workbook = null;
            sheet = null;
            return ms;
        }
    }
}