﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using Geekees.Common.Controls;

namespace TG.Web.LeadershipCockpit
{
    public partial class ProjectAllotBymaster : PageBase
    {
        TG.BLL.cm_Project project = new TG.BLL.cm_Project();
        public string unitname = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindBuildType();
                //绑定单位
                BindUnit();
                //绑定树单位
                LoadUnitTree();
                SelectedCurUnit();
                //绑定年份
                BindYear();
                //选中默认年
                SelectCurrentYear();
                //绑定项目信息
                BindProject();
                //绑定权限
                BindPreviewPower();
            }
            else
            {
                //绑定树单位
                LoadUnitTree();
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.ddrank.DataSource = bll_dic.GetList(str_where);
            this.ddrank.DataTextField = "dic_Name";
            this.ddrank.DataValueField = "ID";
            this.ddrank.DataBind();
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                    this.drp_year1.Items.Add(list[i]);
                    this.drp_year2.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示部门
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
            //  this.drp_unitLone.DataSource = bll_unit.GetList(strWhere);
            //  this.drp_unitLone.DataTextField = "unit_Name";
            //   this.drp_unitLone.DataValueField = "unit_ID";
            //   this.drp_unitLone.DataBind();
        }
        #region 绑定树部门 2015年6月19日
        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drpunit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }

            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drpunit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    //只有在权限不是全部的时候，用到这个变量。
                    unitname = dr["unit_Name"].ToString();
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drpunit.Theme = macOS;
        }
        #endregion
        //绑定项目列表
        public void BindProject()
        {
            StringBuilder sb = new StringBuilder();
            ////名字不为空
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    sb.Append(" AND pro_Name LIKE '%" + this.txt_keyname.Value.Trim() + "%'   ");
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "'  ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();

            //取得分页总数
            // AspNetPager1.RecordCount = int.Parse(project.GetListPageProcCount(sb.ToString()).ToString());

            //排序
            //  string orderString = " Order by pro_ID desc";
            //  sb.Append(orderString);

            //取得分页信息
            // gv_project.DataSource = project.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            //  gv_project.DataBind();
        }

        //protected void gv_project_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        //获取分配信息
        //        Literal ltl = e.Row.Cells[7].FindControl("Literal1") as Literal;
        //        HiddenField hid = e.Row.Cells[7].FindControl("hid_cprid") as HiddenField;
        //        ltl.Text = GetProjectHtml(ltl.Text, hid.Value);
        //        if (ltl.Text == "")
        //        {
        //            ltl.Text = "无分配记录";
        //        }
        //    }
        //}
        //查询项目分配信息
        TG.BLL.ProjectAllotBP bll_allot = new TG.BLL.ProjectAllotBP();
        protected string GetProjectHtml(string proid, string cprid)
        {
            StringBuilder sb_html = new StringBuilder();

            DataSet ds_allot = bll_allot.GetAllotInfo(proid);

            StringBuilder sb = new StringBuilder();
            ////名字不为空
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    sb.Append(" AND pro_Name LIKE '%" + this.txt_keyname.Value.Trim() + "%'   ");
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "'  ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();

            //取得分页总数
            // AspNetPager1.RecordCount = int.Parse(project.GetListPageProcCount(sb.ToString()).ToString());

            //排序
            //  string orderString = " Order by pro_ID desc";
            //  sb.Append(orderString);

            //取得分页信息
            // gv_project.DataSource = project.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            //  gv_project.DataBind();


            if (ds_allot.Tables.Count > 0)
            {
                //分配次数
                for (int j = 0; j < ds_allot.Tables[0].Rows.Count; j++)
                {
                    //百分比
                    string curRate = ds_allot.Tables[0].Rows[j]["CurRate"] + "%";
                    string curCostId = ds_allot.Tables[0].Rows[j]["ID"].ToString();
                    string url = "../ProjectValueandAllot/ShowProjAllotDetailsTable.aspx?proid=" + proid + "&cprid=" + cprid + "&costid=" + curCostId + "";
                    sb_html.Append("<a href=\"" + url + "\" target=\"_self\" title=\"" + curRate + "\" >分配" + (j + 1) + "</a>|");
                }
            }
            return sb_html.ToString();
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            //if (this.drp_year.Items.FindByText(curyear) != null)
            //{
            //    this.drp_year.Items.FindByText(curyear).Selected = true;
            //}
            if (this.drp_year1.Items.FindByText(curyear) != null)
            {
                this.drp_year1.Items.FindByText(curyear).Selected = true;
            }
            //if (this.drp_year2.Items.FindByText(curyear) != null)
            //{
            //    this.drp_year2.Items.FindByText(curyear).Selected = true;
            //}
        }
        /// <summary>
        /// 获取所有选中部门名称
        /// </summary>
        /// <returns></returns>
        public string GetCheckedNodesName()
        {
            StringBuilder sbUnitlist = new StringBuilder("");
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);

            foreach (ASTreeViewNode node in nodes)
            {
                if (node.NodeValue == "0")
                    continue;

                sbUnitlist.AppendFormat("'{0}',", node.NodeText.Trim());

            }
            //判断部门不为空
            if (sbUnitlist.ToString() != "")
            {
                sbUnitlist.Remove(sbUnitlist.ToString().LastIndexOf(','), 1);

            }

            return sbUnitlist.ToString();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btn_export_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            //检查权限
            GetPreviewPowerSql(ref sb);
            string filetitle = "";
            //正常查询
            string unit1 = GetCheckedNodesName();
            string year1 = this.drp_year1.SelectedValue;

            //自定义
            string unit = this.HiddenUnit.Value;
            string year = this.drp_year.SelectedValue;
            string year2 = this.drp_year2.SelectedValue;
            string txt_proName = this.txt_proName.Value;
            string txt_buildUnit = this.txt_buildUnit.Value;
            string txt_Aperson = this.txt_Aperson.Value;
            string txtproAcount = this.txtproAcount.Value;
            string txtproAcount2 = this.txtproAcount2.Value;
            string txt_signdate = this.txt_signdate.Value;
            string txt_signdate2 = this.txt_signdate2.Value;
            string txt_finishdate = this.txt_finishdate.Value;
            string txt_finishdate2 = this.txt_finishdate2.Value;
            string ddrank = this.ddrank.SelectedItem.Text;

            //正常查询条件
            if (this.SelectType.Value == "0")
            {
                if (!string.IsNullOrEmpty(unit1) && unit1 != "-1" && !unit1.Contains("全院部门"))
                {
                    //  unit1 = "'" + unit1 + "'";
                    if (unit1.Trim() != "-1" && !string.IsNullOrEmpty(unit1.Trim()))
                    {
                        sb.AppendFormat(" AND (Unit in ({0}))", unit1.Trim());
                    }
                }              
            }//自定义查询条件
            else
            {
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    unit = unit.Substring(0, unit.Length - 1);
                    if (unit.Trim() != "-1" && !string.IsNullOrEmpty(unit.Trim()))
                    {
                        sb.AppendFormat(" AND (Unit in ({0}))", unit.Trim());
                    }
                    if (!string.IsNullOrEmpty(year1) && year1 != "-1")
                    {
                        sb2.AppendFormat(" and ActualAllountTime='{0}'  ", year1.Trim());
                    }
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1" && !string.IsNullOrEmpty(year2) && year2 != "-1")
                {
                    sb.AppendFormat(" AND year(pro_startTime) BETWEEN {0} and {1}  ", year.Trim(), year2.Trim());
                }

                //组合查询
                if (!string.IsNullOrEmpty(txt_proName))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_proName.Trim());
                    sb.Append(" AND pro_name LIKE '%" + keyname + "%' ");
                }
                if (!string.IsNullOrEmpty(txt_buildUnit))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_buildUnit.Trim());
                    sb.Append(" AND pro_buildUnit LIKE '%" + keyname + "%'  ");
                }
                if (!string.IsNullOrEmpty(txt_Aperson))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_Aperson.Trim());
                    sb.Append(" AND ChgJia LIKE '%" + keyname + "%'  ");
                }
                //合同额
                if (!string.IsNullOrEmpty(txtproAcount) && string.IsNullOrEmpty(txtproAcount2))
                {
                    sb.Append(" AND cpr_Acount >= " + txtproAcount.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    sb.Append(" AND cpr_Acount <= " + txtproAcount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    sb.Append(" AND (cpr_Acount>=" + txtproAcount.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
                }
                //时间
                if (!string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                {
                    sb.Append(" AND (pro_startTime BETWEEN '" + txt_signdate.Trim() + "' AND '" + txt_signdate2.Trim() + "') ");
                }
                if (!string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                {
                    sb.Append(" AND (pro_finishTime BETWEEN '" + txt_finishdate.Trim() + "' AND '" + txt_finishdate2.Trim() + "') ");
                }
                if (!string.IsNullOrEmpty(ddrank) && !ddrank.Contains("请选择"))
                {
                    sb.Append(" AND BuildType=N'" + ddrank.Trim() + "' ");
                }
            }            

            DataTable dt = project.GetProjectValueShowExportInfo(sb.ToString(), sb2.ToString()).Tables[0];

            string modelPath = " ~/TemplateXls/ProjectAllot.xls";

            ExportExcel(filetitle, dt, modelPath);


        }

        private void ExportExcel(string filetitle, DataTable dt, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //标题样式
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheetAt(0);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue((i + 1) + "");

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ChgJia"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildType"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_buildUnit"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpr_Acount"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["qdrq"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["wcrq"].ToString());

            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(filetitle + "项目分配查看.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
    }
}