﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CoperationChargeDetailViewBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationChargeDetailViewBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {

            $("a[id=previewDetail]").toggle(function () {
                var $detailTable = $("#previewDetailView")
                if ($detailTable.attr("visibility") == "none") {
                    $detailTable.show("slow");
                } else {
                    $detailTable.hide("slow");
                }
            }, function () {
                var $detailTable = $("#previewDetailView")
                if ($detailTable.attr("visibility") == "none") {
                    $detailTable.hide("slow");
                } else {
                    $detailTable.show("slow");
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        领导驾驶舱 <small>合同收费详情</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>合同收费详情</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>合同收费详情</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table style="width: 100%;" class="table table-bordered table-hover">
                                    <tr>
                                        <th style="width: 10%; text-align: center;">
                                            收款次数
                                        </th>
                                        <th style="width: 20%; text-align: center;">
                                            计划收款(万元)
                                        </th>
                                        <th style="width: 15%; text-align: center;">
                                            计划收款时间
                                        </th>
                                        <th style="width: 20%; text-align: center;">
                                            实际收款(万元)
                                        </th>
                                        <th style="width: 15%; text-align: center;">
                                            实际收款时间
                                        </th>
                                        <th style="width: 10%; text-align: center;">
                                            是否逾期
                                        </th>
                                        <th style="width: 10%; text-align: center;">
                                            操作
                                        </th>
                                    </tr>
                                    <%=TableHTML %>
                                </table>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-10">
                                <%=itemTableHTML%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
