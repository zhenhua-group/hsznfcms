﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CoperationFinancialListBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationFinancialListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../js/LeadershipCockpit/CoperationFinancialList_jq.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#show6_1_1").parent().attr("class", "active");
            $("#show6_1").attr("class", "arrow open");
            $("#show6").parent().attr("class", "open");
            $("#arrow6").attr("class", "arrow open");
            $("#show6_1_1").parent().parent().css("display", "block");
            $("#show6_1_1").parent().parent().parent().parent().css("display", "block");
            //输入关键字名称提示下来框
            var paramEntity = {};
            paramEntity.action = "Corperation";
            paramEntity.previewPower = $("#previewPower").val();
            paramEntity.userSysNum = $("#userSysNum").val();
            paramEntity.userUnitNum = $("#userUnitNum").val();
            paramEntity.unitID = $("#drp_unit").val();
            paramEntity.currYear = $("#drp_year").val();
            var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
            autoComplete.GetAutoAJAX();
        });   //清空数据     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        领导驾驶舱 <small>合同计划收费查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a >领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>合同计划收费查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询合同计划收费</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-1">
                                    生产部门:
                                </label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="drp_unit" CssClass="form-control input-sm" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                    </asp:DropDownList>
                                    
                                </div>
                                <label class="control-label col-md-1">
                                    年份:
                                </label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="drp_year" CssClass="form-control input-sm" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                    </asp:DropDownList>
                                    
                                </div>
                                <label class="control-label col-md-1">
                                    合同名称:</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control input-sm" id="txt_keyname" runat="server" />
                                    
                                </div>
                                <div class="col-md-1">
                                    <input type="button" class="btn blue btn-block" value="查询" id="btn_Search" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>合同计划收费查看列表</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="jqGrid">
                                </table>
                                <div id="gridpager">
                                </div>
                                <div id="nodata" class="norecords">
                                    没有符合条件数据！</div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
