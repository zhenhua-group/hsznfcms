﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.LeadershipCockpit
{
    public partial class MemberProjectValueAllotBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //初始年
                InitDropDownByYear();
                //初始单位
                BindUnit();
                //选择 单位
                this.pp.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.memid.Value = UserSysNo.ToString();
                this.unitid.Value = UserUnitNo.ToString();

                //初始化部门
                if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
                {
                    this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
                }

                //绑定
                InitValueAllot();

            }
        }
        /// <summary>
        /// 初始化绑定数据
        /// </summary>
        protected void InitValueAllot()
        {
            //初始化绑定产值分配表
            string unitName = this.drp_unit.SelectedItem.Text.Trim();
            string year = this.drp_year.SelectedValue.Trim();
            StringBuilder strWhere = new StringBuilder("");

            string sqlwhere = "", strwhere1 = "", strwhere2 = "";
            TG.Model.tg_unit tgunit = new TG.Model.tg_unit();

            //按照部门
            if (!string.IsNullOrEmpty(unitName))
            {
                strWhere.AppendFormat(" AND (p.Unit= '{0}')", unitName);
            }
            //按照年份
            if (int.Parse(year) > 0)
            {

                strwhere1 = " and year(InAcountTime)=" + year;
                strwhere2 = " and ActualAllountTime='" + year + "'";
                sqlwhere = " and pva.ActualAllountTime='" + year + "'";
            }
            #region 权限

            //个人权限
            // if (base.RolePowerParameterEntity.PreviewPattern == 0)
            //  {
            //  sqlwhere = sqlwhere + " and pvb.mem_ID=" + UserSysNo + " and  pvb.IsExternal=0";
            //   strWhere.Append(" AND p.InsertUserID =" + UserSysNo + "");
            //  }
            //部门
            //  else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            //  {
            //      sqlwhere = sqlwhere + " and pvb.mem_ID in (select mem_ID from tg_member where mem_Unit_ID=" + UserUnitNo + " union all select mem_ID from cm_ExternalMember where mem_Unit_ID=" + UserUnitNo + ")";
            //      strWhere.Append(" AND p.InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            //  }

            //GetPreviewPowerSql(ref strWhere);
            #endregion
            List<TG.Model.ProjectValueAllot> listSC = new TG.BLL.StandBookBp().GetMemberProjectValueAllotProc(strWhere.ToString(), strwhere1, strwhere2);

            string td1 = "<tr ><td colspan='2' style='font-weight:bold;width:150px;'>项目</td>";
            string td2 = "<tr><td colspan='2' style='font-weight:bold;'>实收产值(万元)</td>";
            string td3 = "<tr><td colspan='2' style='font-weight:bold;'>转经济所(万元)</td>";
            string td3_1 = "<tr><td colspan='2' style='font-weight:bold;'>转暖通所(万元)</td>";
            string td3_2 = "<tr><td colspan='2' style='font-weight:bold;'>转土建所(万元)</td>";
            string td4 = "<tr><td colspan='2' style='font-weight:bold;'>本所产值(万元)</td>";
            string td5 = "<tr><td colspan='2' style='font-weight:bold;'>所留(万元)</td>";
            string td6 = "<tr style='font-weight:bold;'><td>序号</td><td>姓名</td>";
            string tdend = "<tr style='font-weight:bold;'><td colspan='2'>总计：</td>";
            decimal SumCharge = 0, SumWeiFenValue = 0, SumToOtherValue = 0, SumActualValue = 0, SumHavcValue = 0, SumTranBulidingValue = 0;
            foreach (TG.Model.ProjectValueAllot pv in listSC)
            {
                td1 = td1 + "<td style='width:150px;'>" + pv.CprName + "</td>";
                td2 = td2 + "<td>" + pv.Charge.ToString("f2") + "</td>";
                td3 = td3 + "<td>" + pv.EconomyValue.ToString("f2") + "</td>";
                td3_1 = td3_1 + "<td>" + pv.HavcValue.ToString("f2") + "</td>";
                td3_2 = td3_2 + "<td>" + pv.TranBulidingValue.ToString("f2") + "</td>";
                td4 = td4 + "<td>" + pv.UnitValue.ToString("f2") + "</td>";
                td5 = td5 + "<td>" + pv.TheDeptValue.ToString("f2") + "</td>";
                td6 = td6 + "<td>(元)</td>";
                SumCharge = SumCharge + pv.Charge;
                SumWeiFenValue = SumWeiFenValue + pv.EconomyValue;
                SumHavcValue = SumHavcValue + pv.HavcValue;
                SumTranBulidingValue = SumTranBulidingValue + pv.TranBulidingValue;
                SumToOtherValue = SumToOtherValue + pv.UnitValue;
                SumActualValue = SumActualValue + pv.TheDeptValue;

            }
            td1 = td1 + "<td style='width:150px;' >二次分配产值</td><td style='width:150px;' >方案补贴</td><td style='font-weight:bold;width:150px;'>总计(万元)</td></tr>";
            td2 = td2 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumCharge.ToString("f2") + "</td></tr>";
            td3 = td3 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumWeiFenValue.ToString("f2") + "</td></tr>";
            td3_1 = td3_1 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumHavcValue.ToString("f2") + "</td></tr>";
            td3_2 = td3_2 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumTranBulidingValue.ToString("f2") + "</td></tr>";
            td4 = td4 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumToOtherValue.ToString("f2") + "</td></tr>";
            td5 = td5 + "<td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumActualValue.ToString("f2") + "</td></tr>";
            td6 = td6 + "<td>(元)</td><td>(元)</td><td>(元)</td></tr>";

            string td = "";
            //总纵向统计
            decimal sumall = 0;
            //总二次分配产值
            decimal sumercz = 0;
            //总方案补贴
            decimal sumsbcz = 0;

            //描述:统计所有项目参与的所有人员 zxq  20131225
            DataTable listme = new TG.BLL.StandBookBp().GetMemberProjectValueAllotDetailProc(unitName, Convert.ToInt32(this.drp_unit.SelectedValue.ToString().Trim()), year);
            if (listme != null && listme.Rows.Count > 0)
            {

                for (int i = 0; i < listme.Rows.Count; i++)
                {
                    string uname = listme.Rows[i]["mem_Name"].ToString();
                    if (listme.Rows[i]["ordtype"].ToString() == "2")
                    {
                        uname = uname + "(外聘)";
                    }
                    else if (listme.Rows[i]["ordtype"].ToString() == "1")
                    {
                        uname = uname + "(外所)";
                    }
                    string innertr = "<tr><td>" + (i + 1) + "</td><td>" + uname + "</td>";
                    decimal summoney = 0;
                    foreach (TG.Model.ProjectValueAllot pv in listSC)
                    {
                        //描述:第一次分配产值 zxq  20131225
                        //  decimal sumvalue = new TG.BLL.StandBookBp().GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(), year);

                        decimal sumvalue = Convert.ToDecimal(listme.Rows[i]["pro_" + pv.CprId.ToString() + ""]);
                        innertr = innertr + "<td>" + sumvalue.ToString("f2") + "</td>";
                        summoney = summoney + sumvalue;
                    }

                    //查询人员的方案补贴                   
                    // decimal sbcz = new TG.BLL.StandBookBp().GetMemberProjectComValue(" and ValueYear='" + this.drp_year.SelectedValue.Trim() + "' and MemberId=" + listme.Rows[i]["mem_ID"] + " and UnitId=" + this.drp_unit.SelectedValue.Trim() + "");

                    //描述:查询人员的方案补贴  zxq  20131225
                    decimal sbcz = Convert.ToDecimal(listme.Rows[i]["ComValue"]);

                    //查询人员二次分配产值
                    //decimal ercz = new TG.BLL.StandBookBp().GetMemberProjectValueAllotSql("2", listme.Rows[i]["mem_ID"].ToString(), "", year);

                    //描述:查询人员二次分配产值  zxq  20131225
                    decimal ercz = Convert.ToDecimal(listme.Rows[i]["totalcount2"]);

                    //方案补贴之和
                    sumsbcz = sumsbcz + sbcz;
                    //二次分配产值
                    sumercz = sumercz + ercz;
                    //纵向统计
                    summoney = summoney + sbcz + ercz;
                    innertr = innertr + "<td>" + ercz.ToString("f2") + "</td><td>" + sbcz.ToString("f2") + "</td><td style='font-weight:bold;'>" + summoney + "</td></tr>";
                    td = td + innertr;
                    //纵向统计之和
                    sumall = sumall + summoney;
                }

                //横向统计
                foreach (TG.Model.ProjectValueAllot pv in listSC)
                {
                    decimal summoney = 0;
                    for (int i = 0; i < listme.Rows.Count; i++)
                    {

                        //decimal sumvalue = new TG.BLL.StandBookBp().GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(), year);
                        //描述:横向统计项目之和  zxq  20131225
                        decimal sumvalue = Convert.ToDecimal(listme.Rows[i]["pro_" + pv.CprId.ToString() + ""]);
                        summoney = summoney + sumvalue;
                    }
                    tdend = tdend + "<td>" + summoney.ToString("f2") + "</td>";
                }
            }

            //所补产值总计和纵向统计的显示
            tdend = tdend + "<td>" + sumercz.ToString("f2") + "</td><td>" + sumsbcz.ToString("f2") + "</td><td>" + sumall.ToString("f2") + "</td></tr>";

            string json = td1 + td2 + td3 + td3_1 + td3_2 + td4 + td5 + td6 + td + tdend;
            //  int colm = listSC.Count + 4;
            //  int widthsum = (colm * 160);
            //  this.mytabhtml.Style.Add("width", widthsum.ToString());
            this.mytabhtml.Value = json;

        }
        /// <summary>
        ///初始年份
        /// </summary>
        protected void InitDropDownByYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
            //如果当年不存在合同，选上年的
            if (this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            else
            {
                this.drp_year.Items.FindByText((DateTime.Now.Year - 1).ToString()).Selected = true;
            }
            //this.drp_year.DataSource = new TG.BLL.cm_Coperation().GetCoperationYear();
            //this.drp_year.DataBind();
            // this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append("  AND a.mem_ID =" + UserSysNo + "");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND a.mem_ID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }
    }
}