﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;

namespace TG.Web.LeadershipCockpit
{
    public partial class ProjectCountRpt : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectedCurYear();
                LoadUnitTree();
                SelectedCurUnit();
                //统计数据
                GetAllData();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        #region 基本设置 2015年6月9日
        /// <summary>
        /// Y坐标数据
        /// </summary>
        public string xAxis { get; set; }
        /// <summary>
        /// y坐标数据
        /// </summary>
        public string yAxis { get; set; }
        /// <summary>
        /// 图例
        /// </summary>
        public string LegendData { get; set; }
        /// <summary>
        /// 统计数据
        /// </summary>
        public string SeriesData { get; set; }

        /// <summary>
        /// 绑定多选年
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();

            if (list.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drpYear.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (string str in list)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(str, str);
                    firstnode.AppendChild(linknode);
                }
            }
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void SelectedCurYear()
        {
            string[] curYear = { DateTime.Now.Year.ToString() };
            this.drpYear.CheckNodes(curYear);
            //如果当前年不存在
            if (this.drpYear.GetCheckedNodes(false).Count == 0)
            {
                string[] beforeYear = { (DateTime.Now.Year - 1).ToString() };
                this.drpYear.CheckNodes(beforeYear);
            }
            //设置时间段时间
            this.txt_start.Value = DateTime.Now.Year.ToString() + "-01-01";
            this.txt_end.Value = DateTime.Now.Year.ToString() + "-12-31";
        }

        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity != null)
            {

                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
            this.drpYear.Theme = macOS;
        }
        #endregion
        /// <summary>
        /// 获取统计类型
        /// </summary>
        /// <returns></returns>
        protected string GetCountType()
        {
            string result = this.drpQuxian.SelectedItem.Text;
            return result;
        }
        /// <summary>
        /// 获取图例数据
        /// </summary>
        protected void GetLegendValue()
        {
            //范围图
            StringBuilder legendData = new StringBuilder();
            //数据图例
            legendData.Append("[");

            //统计类型
            string type = GetCountType();
            //时间段
            if (!chkTime.Checked)
            {

                List<ASTreeViewNode> yearnodes = this.drpYear.GetCheckedNodes(false);


                //如果年份一个都没有选中则默认当年
                if (yearnodes.Count == 0)
                {
                    string curyear = DateTime.Now.Year.ToString() + "年";
                    legendData.AppendFormat("\"{0}\"", "" + type + "(" + curyear + "年)");
                }
                else
                {

                    //是否全选
                    bool isCheckAllYear = yearnodes.Any(n => n.NodeValue == "0");

                    if (isCheckAllYear)
                    {
                        foreach (ASTreeViewNode node in yearnodes)
                        {
                            if (node.NodeValue == "0")
                                continue;
                            //合同类型
                            if (this.drpQuxian.SelectedIndex == 0 || this.drpQuxian.SelectedIndex == 1)
                            {
                                legendData.AppendFormat("\"{0}\",", "" + type + "(" + node.NodeValue + "年)");
                            }
                            else if (this.drpQuxian.SelectedIndex == 2)
                            {
                                DataTable dt =
                                    new TG.BLL.cm_Dictionary().GetList(" dic_Type='cpr_buildtype' ").Tables[0];

                                foreach (DataRow dr in dt.Rows)
                                {
                                    legendData.AppendFormat("\"{0}\",",
                                        "" + dr["dic_Name"].ToString().Trim() + "(" + node.NodeValue + "年)");
                                }
                            }
                            else if (this.drpQuxian.SelectedIndex == 3)
                            {
                                DataTable dt = new TG.BLL.cm_Dictionary().GetList(" dic_Type='cpr_src' ").Tables[0];

                                foreach (DataRow dr in dt.Rows)
                                {
                                    legendData.AppendFormat("\"{0}\",",
                                        "" + dr["dic_Name"].ToString().Trim() + "(" + node.NodeValue + "年)");
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (ASTreeViewNode node in yearnodes)
                        {
                            //合同类型
                            if (this.drpQuxian.SelectedIndex == 0 || this.drpQuxian.SelectedIndex == 1)
                            {
                                legendData.AppendFormat("\"{0}\",", "" + type + "(" + node.NodeValue + "年)");
                            }
                            else if (this.drpQuxian.SelectedIndex == 2)
                            {
                                DataTable dt =
                                    new TG.BLL.cm_Dictionary().GetList(" dic_Type='cpr_buildtype' ").Tables[0];

                                foreach (DataRow dr in dt.Rows)
                                {
                                    legendData.AppendFormat("\"{0}\",",
                                        "" + dr["dic_Name"].ToString().Trim() + "(" + node.NodeValue + "年)");
                                }
                            }
                            else if (this.drpQuxian.SelectedIndex == 3)
                            {
                                DataTable dt = new TG.BLL.cm_Dictionary().GetList(" dic_Type='cpr_src' ").Tables[0];

                                foreach (DataRow dr in dt.Rows)
                                {
                                    legendData.AppendFormat("\"{0}\",",
                                        "" + dr["dic_Name"].ToString().Trim() + "(" + node.NodeValue + "年)");
                                }
                            }
                        }
                    }

                    //x坐标
                    legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
                    legendData.Append("]");

                    LegendData = legendData.ToString();
                }
            }
            else
            {

                string starttime = this.txt_start.Value;
                string endtime = this.txt_end.Value;
                //开始时间
                if (string.IsNullOrEmpty(starttime))
                    starttime = DateTime.Now.Year + "-01-01";
                //结束时间
                if (string.IsNullOrEmpty(endtime))
                    endtime = DateTime.Now.Year + "-12-31";
                //合同类型
                if (this.drpQuxian.SelectedIndex == 0 || this.drpQuxian.SelectedIndex == 1)
                {
                    legendData.AppendFormat("\"{0}\",", "" + type + "(" + starttime + "至" + endtime + ")");
                }
                else if (this.drpQuxian.SelectedIndex == 2)
                {
                    DataTable dt =
                        new TG.BLL.cm_Dictionary().GetList(" dic_Type='cpr_buildtype' ").Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        legendData.AppendFormat("\"{0}\",",
                            "" + dr["dic_Name"].ToString().Trim() + "(" + starttime + "至" + endtime + ")");
                    }
                }
                else if (this.drpQuxian.SelectedIndex == 3)
                {
                    DataTable dt = new TG.BLL.cm_Dictionary().GetList(" dic_Type='cpr_src' ").Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        legendData.AppendFormat("\"{0}\",",
                            "" + dr["dic_Name"].ToString().Trim() + "(" + starttime + "至" + endtime + ")");
                    }
                }

                //x坐标
                legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
                legendData.Append("]");

                LegendData = legendData.ToString();
            }
        }

        /// <summary>
        ///  获取X坐标数据
        /// </summary>
        private void GetxAxisValue()
        {
            //横向坐标
            StringBuilder sbxAxis = new StringBuilder();

            sbxAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                int index = 0;
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    //X坐标数据
                    if (index % 2 == 0)
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                    else
                    {
                        sbxAxis.Append("\"\\n" + unitname + "\",");
                    }

                    index++;
                }

                index = 0;
            }
            else
            {
                int index = 0;
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    if (nodes.Count >= 10)
                    {

                        if (index % 2 == 0)
                        {
                            sbxAxis.Append("\"" + unitname + "\",");
                        }
                        else
                        {
                            sbxAxis.Append("\"\\n" + unitname + "\",");
                        }
                        index++;
                    }
                    else
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                }

                index = 0;
            }
            //x坐标
            sbxAxis.Remove(sbxAxis.ToString().LastIndexOf(','), 1);
            sbxAxis.Append("]");

            xAxis = sbxAxis.ToString();
        }

        /// <summary>
        /// 设置Y坐标数据
        /// </summary>
        private void SetyAxisValue()
        {
            StringBuilder sbyAxis = new StringBuilder();
            if (this.drpQuxian.SelectedValue == "0")
            {
                sbyAxis.Append(@"{
                            type : 'value',
                            name : '数量',
                            axisLabel : {
                                formatter: '{value} 个'}
                        }");
            }
            else if (this.drpQuxian.SelectedValue == "1")
            {
                sbyAxis.Append(@"{
                            type : 'value',
                            name : '规模',
                            axisLabel : {
                                formatter: '{value} 公顷'}
                        }");
            }
            else if (this.drpQuxian.SelectedValue == "3")
            {
                sbyAxis.Append(@"{
                            type : 'value',
                            name : '数量',
                            axisLabel : {
                                formatter: '{value} 个'}
                        }");
            }
            else if (this.drpQuxian.SelectedValue == "3" || this.drpQuxian.SelectedValue == "7")
            {
                sbyAxis.Append(@"{
                            type : 'value',
                            name : '规模',
                            axisLabel : {
                                formatter: '{value} 公顷'}
                        }");
            }
            else if (this.drpQuxian.SelectedValue == "4" || this.drpQuxian.SelectedValue == "8")
            {
                sbyAxis.Append(@"{
                            type : 'value',
                            name : '收款额',
                            axisLabel : {
                                formatter: '{value} 万'}
                        }");
            }
            yAxis = sbyAxis.ToString();
        }

        /// <summary>
        /// 获取实际数据
        /// </summary>
        private void GetSeriesData()
        {
            StringBuilder sbSeries = new StringBuilder();
            //字典对象
            TG.BLL.cm_Dictionary bllDic = new BLL.cm_Dictionary();
            //统计类型
            string type = GetCountType();
            //时间段
            if (!chkTime.Checked)
            {
                //如果没有选中年按当年计算
                if (this.drpYear.GetCheckedNodes(false).Count == 0)
                {
                    SelectedCurYear();
                }

                //年
                foreach (ASTreeViewNode node in this.drpYear.GetCheckedNodes(false))
                {
                    string checkYear = node.NodeValue;
                    //全选
                    if (checkYear == "0")
                        continue;

                    string strData = "";
                    //查询数量和规模
                    if (this.drpQuxian.SelectedIndex == 0 || this.drpQuxian.SelectedIndex == 1)
                    {
                        strData = GetCountDataByYear(checkYear, "0", "");
                        sbSeries.AppendFormat(@"{{name:'{2}({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData, type);
                    }
                    else if (this.drpQuxian.SelectedIndex == 2)
                    {
                        DataTable dt = bllDic.GetList(" dic_Type='cpr_buildtype' ").Tables[0];

                        foreach (DataRow dr in dt.Rows)
                        {
                            //合同分类
                            string cprtype = dr["dic_Name"].ToString();
                            strData = GetCountDataByYear(checkYear, "1", cprtype);
                            sbSeries.AppendFormat(@"{{name:'{2}({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData, cprtype);
                        }
                    }
                    else if (this.drpQuxian.SelectedIndex == 3)
                    {
                        DataTable dt = bllDic.GetList(" dic_Type='cpr_src' ").Tables[0];

                        foreach (DataRow dr in dt.Rows)
                        {
                            //合同性质
                            string cprtypeId = dr["ID"].ToString();
                            string cprtype = dr["dic_Name"].ToString();
                            strData = GetCountDataByYear(checkYear, "2", cprtypeId);
                            sbSeries.AppendFormat(@"{{name:'{2}({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData, cprtype);
                        }
                    }
                }

                //折线数据 
                //sbSeries.Append(GetSeriesDataExt());

                sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
                //返回数据
                SeriesData = sbSeries.ToString();
            }
            else
            {
                //当前年
                string curYear = DateTime.Now.Year.ToString();
                //时间段
                string starttime = this.txt_start.Value;
                string endtime = this.txt_end.Value;
                //开始时间
                if (string.IsNullOrEmpty(starttime))
                    starttime = DateTime.Now.Year + "-01-01";
                //结束时间
                if (string.IsNullOrEmpty(endtime))
                    endtime = DateTime.Now.Year + "-12-31";

                string strData = "";
                //查询数量和规模
                if (this.drpQuxian.SelectedIndex == 0 || this.drpQuxian.SelectedIndex == 1)
                {
                    strData = GetCountDataByYear(curYear, "0", "");
                    sbSeries.AppendFormat(@"{{name:'{2}({0}至{3})',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", starttime, strData, type, endtime);
                }
                else if (this.drpQuxian.SelectedIndex == 2)
                {
                    DataTable dt = bllDic.GetList(" dic_Type='cpr_buildtype' ").Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        //合同分类
                        string cprtype = dr["dic_Name"].ToString();
                        strData = GetCountDataByYear(curYear, "1", cprtype);
                        sbSeries.AppendFormat(@"{{name:'{2}({0}至{3})',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", starttime, strData, cprtype, endtime);
                    }
                }
                else if (this.drpQuxian.SelectedIndex == 3)
                {
                    DataTable dt = bllDic.GetList(" dic_Type='cpr_src' ").Tables[0];

                    foreach (DataRow dr in dt.Rows)
                    {
                        //合同性质
                        string cprtypeId = dr["ID"].ToString();
                        string cprtype = dr["dic_Name"].ToString();
                        strData = GetCountDataByYear(curYear, "2", cprtypeId);
                        sbSeries.AppendFormat(@"{{name:'{2}({0}至{3})',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", starttime, strData, cprtype, endtime);
                    }
                }

                sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
                //返回数据
                SeriesData = sbSeries.ToString();
            }
        }
        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="slttype"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        private string GetCountDataByYear(string year, string slttype, string typename)
        {
            //横向坐标
            //统计值
            StringBuilder sbyAxis = new StringBuilder();
            sbyAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    //默认
                    if (slttype == "0")
                    {
                        sbyAxis.Append(GetProjCountByUnit(unitname, year) + ",");
                    }
                    else if (slttype == "1")//项目等级
                    {
                        sbyAxis.Append(GetProjCountByLevelUnit(unitname, year, typename) + ",");
                    }
                    else if (slttype == "2")//项目来源
                    {
                        sbyAxis.Append(GetProjCountBySrcUnit(unitname, year, typename) + ",");
                    }

                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    //默认
                    if (slttype == "0")//项目数量
                    {
                        sbyAxis.Append(GetProjCountByUnit(unitname, year) + ",");
                    }
                    else if (slttype == "1")//项目等级
                    {
                        sbyAxis.Append(GetProjCountByLevelUnit(unitname, year, typename) + ",");
                    }
                    else if (slttype == "2")//项目来源
                    {
                        sbyAxis.Append(GetProjCountBySrcUnit(unitname, year, typename) + ",");
                    }
                }
            }
            //y坐标
            sbyAxis.Remove(sbyAxis.ToString().LastIndexOf(','), 1);
            sbyAxis.Append("]");

            return sbyAxis.ToString();
        }

        /// <summary>
        /// 查询时间
        /// </summary>
        /// <param name="unitid"></param>
        /// <returns></returns>
        protected string GetProjCountByUnit(string unitname, string year)
        {
            string strSql = string.Format(@" Select COUNT(*) 
                                            From cm_Project
                                            Where Unit='{0}'", unitname);
            //统计项目规模
            if (this.drpQuxian.SelectedValue == "1")
            {
                strSql = string.Format(@" Select isnull(SUM(ProjectScale)*0.0001,0)
                                          From cm_Project
                                          Where Unit='{0}'", unitname);
            }
            //按时间段查询
            if (this.chkTime.Checked)
            {
                string stattime = this.txt_start.Value;
                string endtime = this.txt_end.Value;

                if (stattime.Trim() == "")
                    stattime = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                if (endtime.Trim() == "")
                    endtime = DateTime.Now.ToString("yyyy-MM-dd" + " 23:59:59");

                strSql += string.Format(" AND (pro_startTime BETWEEN '{0}' AND '{1}') ", stattime, endtime);
            }
            else
            {
                //年
                string stryear = year;
                //季度
                string strjidu = this.drpJidu.SelectedValue;
                //月
                string stryue = this.drpMonth.SelectedValue;

                //全部收款
                if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
                {
                    strSql += string.Format(" AND year(pro_startTime)={0}", stryear);
                }
                else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
                {
                    string start = stryear;
                    string end = stryear;
                    switch (strjidu)
                    {
                        case "1":
                            start += "-01-01 00:00:00";
                            end += "-3-31 23:59:59";
                            break;
                        case "2":
                            start += "-04-01 00:00:00";
                            end += "-6-30 23:59:59";
                            break;
                        case "3":
                            start += "-7-01 00:00:00";
                            end += "-9-30 23:59:59";
                            break;
                        case "4":
                            start += "-10-01 00:00:00";
                            end += "-12-31 23:59:59";
                            break;
                    }
                    strSql += string.Format("AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
                }
                else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
                {
                    //当月有几天
                    int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                    string start = stryear + "-" + stryue + "-01 00:00:00";
                    string end = stryear + "-" + stryue + "-" + days + " 00:00:00";
                    strSql += string.Format("AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
                }
                else
                {
                    //默认当年收款
                    string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                    string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    strSql += string.Format("AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, curtime);
                }
            }

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        /// <summary>
        /// 按项目级别查询
        /// </summary>
        /// <param name="unitname"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        protected string GetProjCountByLevelUnit(string unitname, string year, string typename)
        {
            //合同类型
            string strSql = string.Format(@"Select COUNT(*)
                            From cm_Project B
                            Where Unit='{0}' AND BuildType='{1}' ", unitname, typename);
            //时间字段
            string timeColumn = " B.pro_startTime ";
            //按时间段查询
            if (this.chkTime.Checked)
            {
                string stattime = this.txt_start.Value;
                string endtime = this.txt_end.Value;

                if (stattime.Trim() == "")
                    stattime = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                if (endtime.Trim() == "")
                    endtime = DateTime.Now.ToString("yyyy-MM-dd" + " 23:59:59");

                strSql += string.Format(" AND ({2} BETWEEN '{0}' AND '{1}') ", stattime, endtime, timeColumn);
            }
            else
            {
                //年
                string stryear = year;
                //季度
                string strjidu = this.drpJidu.SelectedValue;
                //月
                string stryue = this.drpMonth.SelectedValue;

                //全部收款
                if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
                {
                    strSql += string.Format(" AND year({1})={0}", stryear, timeColumn);
                }
                else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
                {
                    string start = stryear;
                    string end = stryear;
                    switch (strjidu)
                    {
                        case "1":
                            start += "-01-01 00:00:00";
                            end += "-3-31 23:59:59";
                            break;
                        case "2":
                            start += "-04-01 00:00:00";
                            end += "-6-30 23:59:59";
                            break;
                        case "3":
                            start += "-7-01 00:00:00";
                            end += "-9-30 23:59:59";
                            break;
                        case "4":
                            start += "-10-01 00:00:00";
                            end += "-12-31 23:59:59";
                            break;
                    }
                    strSql += string.Format("AND ({2} BETWEEN '{0}' AND '{1}')", start, end, timeColumn);
                }
                else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
                {
                    //当月有几天
                    int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                    string start = stryear + "-" + stryue + "-01 00:00:00";
                    string end = stryear + "-" + stryue + "-" + days + " 00:00:00";
                    strSql += string.Format("AND ({2} BETWEEN '{0}' AND '{1}')", start, end, timeColumn);
                }
                else
                {
                    //默认当年收款
                    string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                    string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    strSql += string.Format("AND ({2} BETWEEN '{0}' AND '{1}')", start, curtime, timeColumn);
                }
            }

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        /// <summary>
        /// 按项目来源查询
        /// </summary>
        /// <param name="unitname"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        protected string GetProjCountBySrcUnit(string unitname, string year, string typename)
        {
            string strSql = strSql = string.Format(@"Select COUNT(*)
                                                    From cm_Project B
                                                    Where Unit='{0}' AND Pro_src={1}", unitname, typename);

            //时间字段
            string timeColumn = " B.pro_startTime ";

            //按时间段查询
            if (this.chkTime.Checked)
            {
                string stattime = this.txt_start.Value;
                string endtime = this.txt_end.Value;

                if (stattime.Trim() == "")
                    stattime = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                if (endtime.Trim() == "")
                    endtime = DateTime.Now.ToString("yyyy-MM-dd" + " 23:59:59");

                strSql += string.Format(" AND ({2} BETWEEN '{0}' AND '{1}') ", stattime, endtime, timeColumn);
            }
            else
            {
                //年
                string stryear = year;
                //季度
                string strjidu = this.drpJidu.SelectedValue;
                //月
                string stryue = this.drpMonth.SelectedValue;

                //全部收款
                if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
                {
                    strSql += string.Format(" AND year({1})={0}", stryear, timeColumn);
                }
                else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
                {
                    string start = stryear;
                    string end = stryear;
                    switch (strjidu)
                    {
                        case "1":
                            start += "-01-01 00:00:00";
                            end += "-3-31 23:59:59";
                            break;
                        case "2":
                            start += "-04-01 00:00:00";
                            end += "-6-30 23:59:59";
                            break;
                        case "3":
                            start += "-7-01 00:00:00";
                            end += "-9-30 23:59:59";
                            break;
                        case "4":
                            start += "-10-01 00:00:00";
                            end += "-12-31 23:59:59";
                            break;
                    }
                    strSql += string.Format("AND ({2} BETWEEN '{0}' AND '{1}')", start, end, timeColumn);
                }
                else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
                {
                    //当月有几天
                    int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                    string start = stryear + "-" + stryue + "-01 00:00:00";
                    string end = stryear + "-" + stryue + "-" + days + " 00:00:00";
                    strSql += string.Format("AND ({2} BETWEEN '{0}' AND '{1}')", start, end, timeColumn);
                }
                else
                {
                    //默认当年收款
                    string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                    string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    strSql += string.Format("AND ({2} BETWEEN '{0}' AND '{1}')", start, curtime, timeColumn);
                }
            }

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        protected void GetAllData()
        {
            //获取Legend数据
            GetLegendValue();
            //获取X轴数据
            GetxAxisValue();
            //获取条数据
            GetSeriesData();
            //设置坐标
            SetyAxisValue();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetAllData();
        }
    }
}