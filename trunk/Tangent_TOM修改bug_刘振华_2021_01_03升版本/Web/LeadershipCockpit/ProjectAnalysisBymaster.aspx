﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectAnalysisBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.ProjectAnalysisBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectAnalysis.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/LeadershipCockpit/ProjectAnalysis.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/LeadershipCockpit/ProjectAnalysis_jq.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>项目概况</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>项目概况 </a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询项目概况
                    </div>
                   
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>项目名称:</td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_proname" runat="server" /></td>
                            <td>
                                <input type="button" class="btn blue " id="btn_Search" value="查询" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>项目概况列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                     <div class="actions">
                        <asp:Button runat="Server" CssClass="btn red btn-sm" Text="导出Excel" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">

                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <!--阶段信息  -->
    <fieldset id="fd_set" style="display: none;">
        <legend></legend>
        <div class="cls_data">
            <table class="table table-bordered table-hover" id="jd_data">
                <tr id="jd_row">
                    <td align="center" style="width: 30%;" id="purname">阶段名称
                    </td>
                    <td align="center" style="width: 15%;" id="purstart">计划开始时间
                    </td>
                    <td align="center" style="width: 15%;" id="purreadystart">实际开始时间
                    </td>
                    <td align="center" style="width: 15%;" id="purend">计划结束时间
                    </td>
                    <td align="center" style="width: 15%;" id="purreadyend">实际结束时间
                    </td>
                    <td align="center" style="width: 10%;" id="pur_oper">专业
                    </td>
                </tr>
            </table>
        </div>
        <div class="cls_data_bottom" id="jd_nodata">
            没有阶段数据！
        </div>
    </fieldset>
    <!--专业信息-->
    <fieldset id="fd_set2" style="display: none;">
        <legend></legend>
        <div class="cls_data">
            <table class="table table-bordered table-hover" id="spe_data">
                <tr id="spe_row">
                    <td align="center" style="width: 25%;" id="spename">专业名称
                    </td>
                    <td align="center" style="width: 25%;" id="spestart">开始时间
                    </td>
                    <td align="center" style="width: 25%;" id="speend">结束时间
                    </td>
                    <td align="center" style="width: 15%;" id="spe_oper">人员
                    </td>
                </tr>
            </table>
        </div>
        <div class="cls_data_bottom" id="spe_nodata">
            没有专业数据！
        </div>
    </fieldset>
    <!-- 人员信息 -->
    <fieldset id="fd_set3" style="display: none;">
        <legend></legend>
        <div class="cls_data">
            <table class="table table-bordered table-hover" id="user_data">
                <tr id="user_row">
                    <td align="center" style="width: 30%;" id="username">人员名称
                    </td>
                    <td align="center" style="width: 35%;" id="userstart">开始时间
                    </td>
                    <td align="center" style="width: 35%;" id="userend">结束时间
                    </td>
                </tr>
            </table>
        </div>
        <div class="cls_data_bottom" id="user_nodata">
            没有人员数据！
        </div>
    </fieldset>
</asp:Content>
