﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationProjectListBymaster : PageBase
    {
        //public int PageSize { get { return 1; } }
        public string action = "", keyws = "", sql = "", sql2 = "", recount = "0";
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            action = Request.QueryString["fltype"];
            keyws = Request.QueryString["keyws"];

            if (string.IsNullOrEmpty(action) && string.IsNullOrEmpty(keyws))
            {
                action = fltype.Text.Trim();
                keyws = keywords.Text.Trim();
            }
            else
            {

                if (!string.IsNullOrEmpty(keywords.Text.Trim()))
                {
                    action = fltype.Text.Trim();
                    keyws = keywords.Text.Trim();
                }
                else
                {
                    fltype.Text = action;
                    keywords.Text = keyws;
                }
            }

            keyws = CheckString(keyws);

            if (action == "0" || action == "-1")
            {
                sb1.Append(" and (pro_name like '%" + keyws + "%' or pro_Intro like '%" + keyws + "%' or Pro_Number like '%" + keyws + "%')");
                GetPreviewPowerSql(ref sb1);
            }
            if (action == "1" || action == "-1")
            {
                sb2.Append(" and (cpr_Name like '%" + keyws + "%' or TableMaker like '%" + keyws + "%')");
                GetPreviewPowerSql(ref sb2);
            }
            if (!IsPostBack)
            {
                if (keyws.Length > 0)
                {

                    GetList();

                }
                //绑定权限
                BindPreviewPower();
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //特殊符号判断并清空
        public string CheckString(string keystring)
        {
            string SQL_injdata = "'|and|exec|insert|select|delete|update|count|*|chr|mid|master|truncate|char|declare|;|drop|depth";
            string[] str = SQL_injdata.Split('|');
            foreach (string keys in str)
            {
                if (keystring.Contains(keys))
                {
                    //   Response.Write("<script language='Javascript'> alert('请不要非法字符！');</script>");

                    keystring = keystring.Replace(keys, "");

                }
            }
            return keystring;

        }
        //是否需要权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_search_Click(object sender, EventArgs e)
        {

            //查询项目
            GetList();
        }
        public void GetList()
        {

            if (action == "0")
            {

                sql = "select  pro_id,pro_name,pro_Intro,0 as lbtype,pro_startTime as dtime,Pro_Number from cm_Project where 1=1 " + sb1.ToString() + " order by pro_startTime desc";

                sql2 = "select count(*) from cm_Project where 1=1 " + sb1.ToString() + "";
            }
            else if (action == "1")
            {
                sql = "select cpr_id as pro_id,cpr_Name as pro_name,TableMaker as pro_Intro,1 as lbtype,LastUpdate as dtime,0 as Pro_Number from cm_Coperation where 1=1 " + sb2.ToString() + " order by LastUpdate desc";

                sql2 = "select count(*) from cm_Coperation where 1=1 " + sb2.ToString() + "";

            }
            else
            {
                sql = "select * from (select pro_id,pro_name,pro_Intro,0 as lbtype,pro_startTime as dtime,Pro_Number from cm_Project where 1=1 " + sb1.ToString() + " union all select cpr_id as pro_id,cpr_Name as pro_name,TableMaker as pro_Intro,1 as lbtype,LastUpdate as dtime,0 as Pro_Number from cm_Coperation where 1=1 " + sb2.ToString() + ") a order by lbtype asc,dtime desc";

                sql2 = "select count(*) from (select pro_id,pro_name,pro_Intro,0 as lbtype,pro_startTime as dtime,Pro_Number from cm_Project where 1=1 " + sb1.ToString() + " union all select cpr_id as pro_id,cpr_Name as pro_name,TableMaker as pro_Intro,1 as lbtype,LastUpdate as dtime,0 as Pro_Number from cm_Coperation where 1=1 " + sb2.ToString() + ") a ";
            }
            recount = TG.DBUtility.DbHelperSQL.GetSingle(sql2).ToString();
            AspNetPager1.RecordCount = Convert.ToInt32(recount);
            if (!string.IsNullOrEmpty(Request.QueryString["fltype"]) && !string.IsNullOrEmpty(Request.QueryString["keyws"]) && string.IsNullOrEmpty(keywords.Text.Trim()))
            {

                // AspNetPager1.CurrentPageIndex = 2;
            }

            this.gv_project.DataSource = TG.DBUtility.DbHelperSQL.RunPageSql(AspNetPager1.PageSize, AspNetPager1.PageSize * (AspNetPager1.CurrentPageIndex - 1), sql);
            this.gv_project.DataBind();

        }
        protected void ChangePage(object sender, EventArgs e)
        {
            GetList();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btn_export_Click(object sender, EventArgs e)
        {

            if (action == "0")
            {

                sql = "select  pro_id,pro_name,pro_Intro,0 as lbtype,pro_startTime as dtime,Pro_Number from cm_Project where 1=1 " + sb1.ToString() + " order by pro_startTime desc";

            }
            else if (action == "1")
            {
                sql = "select cpr_id as pro_id,cpr_Name as pro_name,TableMaker as pro_Intro,1 as lbtype,LastUpdate as dtime,0 as Pro_Number from cm_Coperation where 1=1 " + sb2.ToString() + " order by LastUpdate desc";
            }
            else
            {
                sql = "select * from (select pro_id,pro_name,pro_Intro,0 as lbtype,pro_startTime as dtime,Pro_Number from cm_Project where 1=1 " + sb1.ToString() + " union all select cpr_id as pro_id,cpr_Name as pro_name,TableMaker as pro_Intro,1 as lbtype,LastUpdate as dtime,0 as Pro_Number from cm_Coperation where 1=1 " + sb2.ToString() + ") a order by lbtype asc,dtime desc";


            }

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];

            string modelPath = " ~/TemplateXls/CoperationProjectList.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //标题样式
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 10;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheetAt(0);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            IRow titleRow = ws.GetRow(0);
            ICell titleCell = titleRow.GetCell(1);
            if (action == "0")
            {
                titleCell.CellStyle = style1;
                titleCell.SetCellValue("项目名称");

                ICell numberCell = titleRow.GetCell(2);
                numberCell.CellStyle = style1;
                numberCell.SetCellValue("工程号");

                ICell bzCell = titleRow.CreateCell(3);
                bzCell.CellStyle = style1;
                bzCell.SetCellValue("备注");
            }



            int row = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue((i + 1) + "");

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());
                //判断项目还是合同
                if (dt.Rows[i]["lbtype"].ToString() == "0")
                {
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["Pro_Number"].ToString());
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["pro_Intro"].ToString());
                }
                else
                {
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["pro_Intro"].ToString());
                }

            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("领导驾驶舱-高级查询.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

    }
}