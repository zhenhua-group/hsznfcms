﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectAllotBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.ProjectAllotBymaster" %>
<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
     <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
     <!--JS--->
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
      <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script> 
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../js/LeadershipCockpit/ProjectAllot_jq.js"></script>
    <script type="text/javascript">
        $(function () {

            //输入关键字名称提示下来框
            var paramEntity = {};
            paramEntity.action = "Corperation";
            paramEntity.previewPower = $("#previewPower").val();
            paramEntity.userSysNum = $("#userSysNum").val();
            paramEntity.userUnitNum = $("#userUnitNum").val();
            paramEntity.unitID = $("#drp_unit").val();
            paramEntity.currYear = $("#drp_year").val();
            var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
            autoComplete.GetAutoAJAX();
        });   //清空数据     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>项目分配查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>项目分配查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目分配查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                           <%-- <td>
                                <asp:DropDownList ID="drp_unitLone" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>--%>

                            <Td>
                                 <cc1:ASDropDownTreeView ID="drpunit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-----全院部门-----" />
                                        </td>

                            <td>分配年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year1" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                </asp:DropDownList></td>
                             <Td>&nbsp;<input type="button" value="查询" id="btn_cx" class="btn blue"/></Td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>自定义条件查询
                    </div>
                    <div class="tools">
                    </div>
                    <div class="actions ">
                        <asp:Button runat="Server" CssClass="btn red btn-sm" Text="导出Excel" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_id">
                                    <tr>
                                        <%--<td align="left">
                                            <input id="cpr_Unit" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Unit">生产部门</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="cpr_Year" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Year">项目年份</span>
                                        </td>
                                        <td align="left">
                                            <input id="ChgJia" name="bb" class="cls_audit" type="checkbox" /><span for="ChgJia">甲方负责人</span>
                                        </td>
                                        <td align="left">
                                            <input id="pro_name" name="bb" class="cls_audit" type="checkbox" /><span for="pro_name">项目名称</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildType" name="bb" class="cls_audit" type="checkbox" /><span for="BuildType">建筑类别</span>
                                        </td>
                                        <td align="left">
                                            <input id="pro_buildUnit" name="bb" class="cls_audit" type="checkbox" /><span for="pro_buildUnit">建设单位</span>
                                        </td>
                                        <td align="left">
                                            <input id="Cpr_Acount" name="bb" class="cls_audit" type="checkbox" /><span for="Cpr_Acount">合同额</span>
                                        </td>
                                        <td align="left">
                                            <input id="qdrq" name="bb" class="cls_audit" type="checkbox" /><span for="qdrq">项目开始日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="wcrq" name="bb" class="cls_audit" type="checkbox" /><span for="wcrq">项目完成日期</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="tbl_id2">
                                        <tr for="cpr_Unit">
                                            <td style="width: 100px;">生产部门:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="160px">
                                                            <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <input type="button" class="btn blue  btn-sm" id="btn_chooes" value="选择" />
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-8" id="labUnit" style="font-size: 14px;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="cpr_Year">
                                            <td style="width: 100px;">项目年份:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp; 至 &nbsp;
                                                             <asp:DropDownList ID="drp_year2" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="140px">
                                                                 <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                             </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="ChgJia">
                                            <td style="width: 100px;">甲方负责人:
                                            </td>
                                            <td>
                                                <input type="text" id="txt_Aperson" class="form-control input-sm " style="width: 390px;" runat="server" />
                                            </td>
                                        </tr>
                                        <tr for="pro_name">
                                            <td style="width: 100px;">项目名称:
                                            </td>
                                            <td>
                                                <input type="text" class="form-control input-sm" id="txt_proName" style="width: 390px;" runat="server" />
                                            </td>
                                        </tr>
                                        <tr for="BuildType">
                                            <td style="width: 100px;">建筑类别:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddrank" runat="server" AppendDataBoundItems="true" CssClass="form-control"
                                                    Width="180">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr for="pro_buildUnit">
                                            <td style="width: 100px;">建设单位:
                                            </td>
                                            <td>
                                                <input type="text" id="txt_buildUnit" class="form-control input-sm" style="width: 390px;" runat="server" />
                                            </td>
                                        </tr>
                                        <tr for="Cpr_Acount">
                                            <td style="width: 100px;">合同额:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txtproAcount" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtproAcount2" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp;万元
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="qdrq">
                                            <td style="width: 100px;">项目开始日期:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_signdate" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                                                runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_signdate2" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                                            runat="Server" style="width: 120px; height: 22px; border: 1px solid
                #e5e5e5" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="wcrq">
                                            <td style="width: 100px;">项目完成日期:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                                                class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_finishdate2" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid
                #e5e5e5" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <input type="button" class="btn blue" id="btn_Search" value="查询" />&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>项目分配列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="HiddenUnit" runat="server" Value="" />
     <asp:HiddenField ID="SelectType" runat="server" Value="0" />
     <input type="hidden" id="hiddenunitName" name="unitname" value="<%=unitname%>"/>
</asp:Content>
