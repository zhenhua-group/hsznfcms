﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationFinancialListBymaster : PageBase
    {
        TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //绑定部门信息
                BindUnit();
                //绑定数据
                BindCoperationNo();
                //绑定权限
                BindPreviewPower();

            }
        }


        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示部门
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            //绑定部门信息
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //正常收款
        private void BindCoperationNo()
        {
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
            StringBuilder strWhere = new StringBuilder("");
            ////合同名称
            //if (this.txt_keyname.Value != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    strWhere.AppendFormat(" AND cpr_Name LIKE '%{0}%'", keyname);
            //}
            ////按照部门
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    strWhere.AppendFormat(" AND (cpr_Unit= '{0}')", this.drp_unit.SelectedItem.Text);
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    strWhere.AppendFormat(" AND year(cpr_SignDate)={0}", this.drp_year.SelectedValue);
            //}
            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();
            //合计
            //CoperationCountAll(strWhere.ToString());
            //所有记录数
            // this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(strWhere.ToString()).ToString());
            //排序
            // string orderString = " Order by cpr_ID DESC ";
            //strWhere.Append(orderString);
            //分页
            //this.gv_CoperationNo.DataSource = bll.GetListByPageProc(strWhere.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            // this.gv_CoperationNo.DataBind();
        }


        //初始时间设置
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

    }
}