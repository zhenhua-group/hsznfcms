﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TG.Web.LeadershipCockpit
{
    public partial class MemberLabouStatisticDetail : System.Web.UI.Page
    {
        /// <summary>
        /// 人员ID
        /// </summary>
        public int Mem_ID
        {
            get
            {
                int mem_id = 0;
                int.TryParse(Request["mem_id"], out mem_id);
                return mem_id;
            }
        }

        /// <summary>
        /// 人员ID
        /// </summary>
        public int type
        {
            get
            {
                int mem_id = 0;
                int.TryParse(Request["type"], out mem_id);
                return mem_id;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData();
                this.hid_where.Value = type.ToString();
                this.hid_mem_id.Value = Mem_ID.ToString();
            }
        }

        private void bindData()
        {
            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member model = bll.GetModel(Mem_ID);
            if (model != null)
            {
                lbl_Name.Text = model.mem_Name;
            }

            if (type == 0)
            {
                lbl_type.Text = "全部参与的项目信息";
            }
            else if (type == 1)
            {
                lbl_type.Text = "参与进行中的项目信息";
            }
            else if (type == 2)
            {
                lbl_type.Text = "参与完工的项目信息";
            }
            else if (type == 3)
            {
                lbl_type.Text = "参与暂停中的项目信息";
            }
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getTable();

            string modelPath = " ~/TemplateXls/Member_Labou_Statistic_Detail.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            ws.GetRow(0).GetCell(0).SetCellValue(lbl_Name.Text + "_" + lbl_type.Text);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_Name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_DesignUnit"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_Status"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_StartTime"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_FinishTime"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ProjectCurrentStatus"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["memRoles"].ToString());

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(lbl_Name.Text + "_人员劳动力统计.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到人员劳动力信息
        /// </summary>
        /// <returns></returns>
        private DataTable getTable()
        {
            string strTempWhere = "";
            if (type == 0)
            {
                strTempWhere = " AND mem_id=" + Mem_ID + "";
            }
            if (type == 1)
            {
                strTempWhere = "  AND mem_id=" + Mem_ID + "  AND pro_Status='进行中'";
            }
            else if (type == 2)
            {
                strTempWhere = "  AND mem_id=" + Mem_ID + "  AND pro_Status='完成'";
            }
            else if (type == 3)
            {
                strTempWhere = "  AND mem_id=" + Mem_ID + "  AND pro_Status='暂停'";
            }
            TG.BLL.MemberLabouStatistic bll = new TG.BLL.MemberLabouStatistic();
            DataTable dt = bll.getMemberLabouStatisticDetail(strTempWhere, Mem_ID).Tables[0];
            return dt;
        }
    }
}