﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TG.DBUtility;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationChargeDetailViewBymaster : System.Web.UI.Page
    {
        public int CoperationSysNo
        {
            get
            {
                int coperationSysNo = 0;
                int.TryParse(Request["coperationSysNo"], out coperationSysNo);
                return coperationSysNo;
            }
        }
        public string TableHTML { get; set; }
        public string itemTableHTML { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CreateHTML();
            }
        }
        private void CreateHTML()
        {
            string sql = "select * from [cm_CoperationCharge]  where cpr_ID=" + CoperationSysNo;

            DataSet set = DbHelperSQL.Query(sql);

            if (set.Tables.Count > 0)
            {
                DataTable table = set.Tables[0];

                if (table.Rows.Count <= 0)
                {

                    TableHTML += "<tr>";
                    TableHTML += "<td style=\"width: 100%; text-align: center;\" colspan=\"7\">暂无记录</td>";
                    TableHTML += "</tr>";

                }
                else
                {
                    for (int i = 0; i < table.Rows.Count; i++)
                    {

                        int itemSysNo = Convert.ToInt32(table.Rows[i][0]);
                        DataSet setItem = DbHelperSQL.Query("select * from [cm_RealCprChg] where chg_id=" + itemSysNo);
                        DataTable tableItem = null;
                        if (setItem.Tables.Count > 0)
                        {
                            tableItem = setItem.Tables[0];
                        }

                        itemTableHTML += "<table style=\"width: 100%; display:none\" class=\"table table-bordered table-hover\" id=\"previewDetailView\" visibility=\"none\">";
                        itemTableHTML += "<tr>";
                        itemTableHTML += "<td style=\"width:20%;text-align:center;\">收款金额(万元)</td>";
                        itemTableHTML += "<td style=\"width:20%;text-align:center;\">收款时间</td>";
                        itemTableHTML += "<td style=\"width:15%;text-align:center;\">收款人</td>";
                        itemTableHTML += "<td style=\"width:15%;text-align:center;\">状态</td>";
                        itemTableHTML += "<td style=\"width:30%;text-align:center;\">备注</td>";
                        itemTableHTML += "</tr>";
                        string realPayDate = "暂无";
                        string isOver = "未逾期";
                        if (tableItem.Rows.Count <= 0)
                        {
                            itemTableHTML += "<tr><td style=\"width:100%;text-align:center;\" colspan=\"5\">暂无记录</td></tr>";
                        }
                        else
                        {
                            for (int j = 0; j < tableItem.Rows.Count; j++)
                            {
                                itemTableHTML += "<tr>";
                                itemTableHTML += "<td style=\"width: 20%; text-align: center;\">" + tableItem.Rows[j]["payCount"].ToString() + "</td>";
                                itemTableHTML += "<td style=\"width: 20%; text-align: center;\">" + Convert.ToDateTime(tableItem.Rows[j]["payTime"]).ToString("yyyy-MM-dd") + "</td>";
                                itemTableHTML += "<td style=\"width: 15%; text-align: center;\">" + tableItem.Rows[j]["acceptuser"].ToString() + "</td>";
                                itemTableHTML += string.Format("<td style=\"width: 15%; text-align: center;color:{1}\">{0}</td>", tableItem.Rows[j]["isover"].ToString() == "1" ? "逾期" : "按期", tableItem.Rows[j]["isover"].ToString() == "1" ? "red" : "black");
                                itemTableHTML += "<td style=\"width: 30%; text-align: center;\">" + tableItem.Rows[j]["mark"].ToString() + "</td>";
                                itemTableHTML += "</tr>";
                                if (j == 0)
                                    realPayDate = Convert.ToDateTime(tableItem.Rows[j]["payTime"]).ToString("yyyy-MM-dd");
                                if (tableItem.Rows[j]["isover"].ToString() == "1")
                                    isOver = "1";
                            }
                        }
                        itemTableHTML += "</table>";


                        TableHTML += "<tr style=\"height:30px;\">";
                        TableHTML += "<td style=\"width: 10%; text-align: center;\">" + table.Rows[i]["Times"].ToString() + "</td>";
                        TableHTML += "<td style=\"width: 20%; text-align: center;\">" + table.Rows[i]["payCount"].ToString() + "</td>";
                        TableHTML += "<td style=\"width: 15%; text-align: center;\">" + Convert.ToDateTime(table.Rows[i]["paytime"]).ToString("yyyy-MM-dd") + "</td>";
                        TableHTML += "<td style=\"width: 20%; text-align: center;\">" + table.Rows[i]["payShiCount"].ToString() + "</td>";
                        TableHTML += "<td style=\"width: 15%; text-align: center;\">" + realPayDate + "</td>";
                        TableHTML += string.Format("<td style=\"width: 10%; text-align: center;color:{1}\">{0}</td>", isOver == "1" ? "逾期" : "按期", isOver == "1" ? "red" : "black");
                        TableHTML += "<td style=\"width: 10%; text-align: center;\"><a href=\"#\" id=\"previewDetail\">查看详细</a></td>";
                        TableHTML += "</tr>";


                    }
                }
            }
        }
    }
}