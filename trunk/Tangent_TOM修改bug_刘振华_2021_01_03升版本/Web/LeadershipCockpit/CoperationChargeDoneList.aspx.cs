﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;


namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationChargeDoneList : PageBase
    {
        //排序字段
        protected string OrderColumn
        {
            get
            {
                return this.hid_column.Value;
            }
        }
        //排序
        protected string Order
        {
            get
            {
                return this.hid_order.Value;
            }
        }
        //合同额
        protected decimal CprAcount { get; set; }
        //已收
        protected decimal CprAcountCharge { get; set; }

        protected TG.BLL.cm_Coperation CMCoperation
        {
            get
            {
                return new TG.BLL.cm_Coperation();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //单位信息
                BindUnit();
                //默认年份
                InitDate();
                //绑定年份
                BindYear();
                //项目信息
                BindDoneProject();
            }
        }
        //检查是否需要权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //通过项目审批工号分配完项目信息
        protected void BindDoneProject()
        {
            StringBuilder sb = new StringBuilder("");
            //名字不为空
            if (this.txt_keyname.Text.Trim() != "")
            {
                sb.Append(" AND cpr_Name LIKE '%" + this.txt_keyname.Text.Trim() + "%'  ");
            }
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND cpr_Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND year(cpr_SignDate)={0}", this.drp_year.SelectedValue);
            }
            //判断权限
            GetPreviewPowerSql(ref sb);
            //数量
            this.AspNetPager1.RecordCount = int.Parse(CMCoperation.GetListPageProcCount(sb.ToString()).ToString());
            //统计
            CountCharge(sb.ToString());
            //排序
            string orderString = " order by " + OrderColumn + " " + Order;
            sb.Append(orderString);
            //分页
            this.grid_cpr.DataSource = CMCoperation.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            this.grid_cpr.DataBind();
        }

        //行绑定
        //总合同额
        decimal allcprallot = 0m;
        //实际收款数
        decimal allrealallot = 0m;
        //总的收费进度
        decimal allpersent = 0m;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl_cprid = e.Row.Cells[5].FindControl("lbl_shiji") as Label;
                lbl_cprid.Text = GetAccout(lbl_cprid.Text);
                //计算百分比
                decimal d_allcount = Convert.ToDecimal(e.Row.Cells[4].Text);
                //累加合同额
                allcprallot += d_allcount;
                decimal d_paycount = Convert.ToDecimal(lbl_cprid.Text);
                //累加实际合同额
                allrealallot += d_paycount;
                decimal d_persent = 0;
                if (d_allcount > 0 && d_paycount > 0)
                {
                    d_persent = (d_paycount / d_allcount) * 100;
                }
                HiddenField hid_persent = e.Row.Cells[6].FindControl("hid_persent") as HiddenField;
                hid_persent.Value = d_persent.ToString("f2");
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[4].Text = CprAcount.ToString("f2");
                e.Row.Cells[5].Text = CprAcountCharge.ToString("f2");
                HiddenField hid_persent = e.Row.Cells[5].FindControl("hid_persent") as HiddenField;
                if (allrealallot > 0)
                {
                    allpersent = (allrealallot / allcprallot) * 100;
                    hid_persent.Value = decimal.Round(allpersent, 4).ToString();
                }
            }
        }
        //得到收款总额
        protected string GetAccout(string cprid)
        {
            //完成入账 后的实际收款额
            string strSql = " Select SUM(Acount) From cm_ProjectCharge Where Status='E' AND cprID=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            string str_count = bll_db.GetList(strSql).Tables[0].Rows[0][0].ToString();
            if (str_count == "")
            {
                str_count = "0.00";
            }
            return str_count;
        }
        //查询
        protected void btn_ok_Click(object sender, ImageClickEventArgs e)
        {
            BindDoneProject();
        }

        //计算合同额和已收款
        protected void CountCharge(string query)
        {
            string start = this.txt_start.Value.Trim();
            string end = this.txt_end.Value.Trim();
            string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            if (start == "" && end != "")
            {
                start = curtime;
            }
            else if (start != "" && end == "")
            {
                end = curtime;
            }
            else if (start == "" && end == "")
            {
                start = curtime;
                end = curtime;
            }

            DataSet result = new TG.BLL.cm_ProjectCharge().GetCoperationChargeCount(query, start, end);
            DataSet result1 = new TG.BLL.cm_ProjectCharge().GetCoperationAcountCount(query);
            if (result1 != null && result1.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(result1.Tables[0].Rows[0]["CprAcount"].ToString()))
                {
                    CprAcount = Convert.ToDecimal(result1.Tables[0].Rows[0]["CprAcount"].ToString());
                }
                else
                {
                    CprAcount = 0;
                }
            }

            if (result != null && result.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(result.Tables[0].Rows[0]["CprChargeAcount"].ToString()))
                {
                    CprAcountCharge = Convert.ToDecimal(result.Tables[0].Rows[0]["CprChargeAcount"].ToString());
                }
                else
                {
                    CprAcountCharge = 0;
                }
            }

        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_start.Value = str_year + "-01-01";
            this.txt_end.Value = str_year + "-12-31";
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindDoneProject();
        }
    }
}