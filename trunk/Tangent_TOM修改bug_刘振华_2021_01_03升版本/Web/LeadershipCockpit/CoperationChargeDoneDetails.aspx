﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CoperationChargeDoneDetails.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationChargeDoneDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cls_content_head
        {
            width: 100%;
            margin: 0 auto;
            background: url(../images/bg_tdhead.gif) repeat-x;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 9pt;
            margin-top: 2px;
        }
        .cls_content_head td
        {
            height: 20px;
            border: solid 1px #CCC;
        }
        .cls_details
        {
            width: 99%;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 9pt;
            font-family: "微软雅黑";
            margin-top: 2px;
        }
        .cls_details td
        {
            height: 20px;
            border: solid 1px #CCC;
        }
    </style>

    <script src="../js/FunctionChart/FusionCharts.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            CommonControl.SetFormWidth();
            $(".cls_details tr:even").css({ "background-color": "White" });
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
     <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[合同收费详情]
            </td>
        </tr>
    </table>
    <fieldset style="height: 140px; width: 96%;">
        <legend>合同收费基本信息</legend>
        <div style="width: 90%; height: 10px;">
        </div>
        <table class="cls_show_cst_jiben" align="center">
            <tr>
                <td style="width: 100px;">
                    合同编号：</td>
                <td style="width: 300px;">
                    <asp:Label ID="lbl_cprNo" runat="server"></asp:Label>
                </td>
                <td>
                    合同分类：</td>
                <td>
                    <asp:Label ID="lbl_cprType" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100px;">
                    合同名称：
                </td>
                <td style="width: 300px;">
                    <asp:Label ID="coperationName" runat="server"></asp:Label>
                </td>
                <td>
                    建筑类别：</td>
                <td>
                    <asp:Label ID="lbl_cprLevel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100px;">
                    建设单位：</td>
                <td style="width: 300px;">
                    <asp:Label ID="lbl_BuildUnit" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    建设规模：</td>
                <td>
                    <asp:Label ID="lbl_cprArea" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100px;">
                    合同额：
                </td>
                <td style="width: 300px;">
                    <asp:Label ID="coperationRealReceivables" runat="server"></asp:Label>
                </td>
                <td style="width: 100px;">
                    实收总额：</td>
                <td>
                    <asp:Label ID="coperationAccount" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 100px;">
                    合同签订日期：</td>
                <td style="width: 300px;">
                    <asp:Label ID="lbl_startTime" runat="server" Text=""></asp:Label>
                </td>
                <td style="width: 100px;">
                    合同完成日期：</td>
                <td>
                    <asp:Label ID="lbl_endTime" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
  
    <fieldset style="width: 96%">
        <legend>收费信息明细列表</legend>
         <div class="cls_data">
            <table style="width: 630px;" class="cls_content_head">
                <tr>
                    <td style="width: 50px;" align="center">
                        序号
                    </td>
                    <td style="width: 90px;" align="center">
                        入账额(万元)
                    </td>
                    <td style="width: 90px;" align="center">
                        比例
                    </td>
                    <td style="width: 80px;" align="center">
                        汇款人
                    </td>
                    <td style="width: 80px;" align="center">
                        入账人
                    </td>
                    <td style="width: 80px;" align="center">
                        收款时间
                    </td>
                    <td style="width: 70px;" align="center">
                        状态
                    </td>
                    <td style="width: 90px;" align="center">
                        备注
                    </td>
                </tr>
            </table>
             <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                Font-Size="12px" Width="630px" OnRowDataBound="gv_Coperation_RowDataBound" >
                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                <Columns>
                    <asp:TemplateField HeaderText="序号">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex+1%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="50px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Acount" HeaderText="入账额">
                        <ItemStyle Width="90px" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lbl_persent" runat="server" Text="0.00%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="90px" />
                    </asp:TemplateField>
                  
                    <asp:BoundField DataField="FromUser" HeaderText="汇款人">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                      <asp:BoundField DataField="InAcountUser" HeaderText="入账人">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="InAcountTime" HeaderText="时间" DataFormatString="{0:d}">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="状态">
                        <ItemStyle Width="70px" />
                    </asp:BoundField>
                        <asp:BoundField DataField="ProcessMark" HeaderText="备注">
                        <ItemStyle Width="90px" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    无收款记录！
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </fieldset>  

    <fieldset style="width: 96%;">
        <legend>收费信息统计报表</legend>
        <table style="width: 100%;">
            <tr>
                <td align="center">
                    <asp:Literal ID="LiteralReportView" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Literal ID="PanelColor" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </fieldset>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
    	<tr>
    		<td align="center"><a href="CoperationChargeDoneList.aspx"><img src="../Images/buttons/btn_back.gif" style="border:none;" /></a></td>
    	</tr>
    </table>
    </form>
</body>
</html>
