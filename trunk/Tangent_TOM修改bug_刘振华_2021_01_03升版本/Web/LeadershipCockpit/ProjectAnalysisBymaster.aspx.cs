﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;

namespace TG.Web.LeadershipCockpit
{
    public partial class ProjectAnalysisBymaster : PageBase
    {
        TG.BLL.tg_project bll_proj = new TG.BLL.tg_project();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定项目信息
                BindProject();
                //绑定生产部门
                BindUnit();
            }

        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND pro_ID in (select ReferenceSysNo from cm_project where InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND pro_DesignUnit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //不显示部门
            strWhere += " unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定项目信息
        protected void BindProject()
        {
            StringBuilder sb = new StringBuilder();
            //if (this.txt_proname.Text != "")
            //{
            //    strWhere += "AND  pro_Name LIKE '%" + this.txt_proname.Text + "%'  ";
            //}
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    strWhere += " AND pro_DesignUnit ='" + this.drp_unit.SelectedItem.Text.Trim() + "'  ";
            //}

            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();

            //所有记录数
            // this.AspNetPager1.RecordCount = int.Parse(bll_proj.GetRecordCount(strWhere.ToString()).ToString());
            //排序
            //  string orderByString = " pro_ID DESC ";
            //分页
            // this.GridView1.DataSource = bll_proj.GetListByPage(strWhere.ToString(), orderByString, this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            // this.GridView1.DataBind();

        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btn_export_Click(object sender, EventArgs e)
        {
            string filetitle = "";
            StringBuilder sb = new StringBuilder();
            if (this.txt_proname.Value != "")
            {
                sb.Append("AND  pro_Name LIKE '%" + this.txt_proname.Value + "%' ");
            }
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND pro_DesignUnit ='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
                filetitle = this.drp_unit.SelectedItem.Text.Trim();
            }
            //检查权限
            GetPreviewPowerSql(ref sb);

            DataTable dt = bll_proj.GetListExport(sb.ToString()).Tables[0];

            string modelPath = " ~/TemplateXls/ProjectAnalysis.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //标题样式
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheetAt(0);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue((i + 1) + "");

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["qdrq"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["wcrq"].ToString());

            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(filetitle + "项目概况.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

    }
}