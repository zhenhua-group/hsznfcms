﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;

namespace TG.Web.LeadershipCockpit
{
    public partial class MemberDetailed : PageBase
    {
        //排序字段
        protected string OrderColumn
        {
            get
            {
                return this.hid_column.Value;
            }
        }
        //排序
        protected string Order
        {
            get
            {
                return this.hid_order.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //绑定年份
                BindYear();
                //默认选中年份
                SelectCurrentYear();
                //绑定项目列表
                if (this.drp_unit.SelectedValue != "-1")
                {
                    BindProject();
                }

                //绑定权限
                BindPreviewPower();

            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        //private void GetPreviewPowerSql(ref StringBuilder sb)
        //{
        //    //个人
        //    if (base.RolePowerParameterEntity.PreviewPattern == 0)
        //    {
        //        sb.Append(" AND p.InsertUserID =" + UserSysNo + "");
        //    }//部门
        //    else if (base.RolePowerParameterEntity.PreviewPattern == 2)
        //    {
        //        sb.Append(" AND p.InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
        //    }
        //}
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
        }
        //根据条件查询
        public void BindProject()
        {
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb2 = new StringBuilder("");
            StringBuilder sb3 = new StringBuilder("");
            TG.BLL.StandBookBp stbb = new TG.BLL.StandBookBp();

            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND me.mem_Unit_ID=" + this.drp_unit.SelectedValue.Trim() + "");
                sb2.Append(" and cs.UnitId=" + this.drp_unit.SelectedValue.Trim() + "");
            }
            //个人权限
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND me.mem_ID=" + UserSysNo + "");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                sb2.AppendFormat(" AND cs.ValueYear='{0}'", this.drp_year.SelectedValue);
                sb3.AppendFormat(" AND year(b.AllotDate)='{0}'", this.drp_year.SelectedValue);
            }

            //检查权限
            // GetPreviewPowerSql(ref sb);
            //所有记录数
            this.AspNetPager1.RecordCount = int.Parse(stbb.GetMemberValueAllotCount(sb.ToString(), sb2.ToString(), sb3.ToString()).ToString());
            //排序
            string orderString = " Order by " + OrderColumn + " " + Order;
            sb.Append(orderString);

            gv_project.DataSource = stbb.GetMemberValueAllotProc(sb.ToString(), sb2.ToString(), sb3.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex); ;
            gv_project.DataBind();
        }

        //行绑定代码

        //总实收产值
        decimal allzcz = 0m;
        //总所补产值
        decimal allsbcz = 0m;
        //总计
        decimal allzj = 0m;
        protected void gv_project_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal d_cz = Convert.ToDecimal(e.Row.Cells[2].Text);
                allzcz += d_cz;
                decimal d_sbcz = Convert.ToDecimal(e.Row.Cells[3].Text);
                allsbcz += d_sbcz;

                decimal d_zj = d_cz + d_sbcz;
                Label lb1 = e.Row.Cells[4].FindControl("valuesum") as Label;
                lb1.Text = d_zj.ToString();
                allzj += d_zj;
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[2].Text = allzcz.ToString("f2");
                e.Row.Cells[3].Text = allsbcz.ToString("f2");
                Label lb1 = e.Row.Cells[4].FindControl("valuesumcount") as Label;
                lb1.Text = allzj.ToString();

            }
        }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, ImageClickEventArgs e)
        {
            //查询项目信息
            BindProject();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_report_Click(object sender, ImageClickEventArgs e)
        {
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb2 = new StringBuilder("");
            StringBuilder sb3 = new StringBuilder("");
            TG.BLL.StandBookBp stbb = new TG.BLL.StandBookBp();

            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND me.mem_Unit_ID=" + this.drp_unit.SelectedValue.Trim() + "");
                sb2.Append(" and cs.UnitId=" + this.drp_unit.SelectedValue.Trim() + "");
            }
            //个人权限
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND me.mem_ID=" + UserSysNo + "");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                sb2.AppendFormat(" AND cs.ValueYear='{0}'", this.drp_year.SelectedValue);
                sb3.AppendFormat(" AND year(b.AllotDate)='{0}'", this.drp_year.SelectedValue);
            }

            DataTable dt = new TG.BLL.StandBookBp().GetExcelMemberValueAllot(sb.ToString(), sb2.ToString(), sb3.ToString());
            ExportDataToExcel(dt);

        }
        #region
        //导出Excel
        private void ExportDataToExcel(DataTable table)
        {
            string title = "";
            if (this.drp_year.SelectedIndex != 0)
            {
                title = this.drp_year.SelectedValue.Trim() + "年";
            }
            if (this.drp_unit.SelectedIndex != 0)
            {
                title += this.drp_unit.SelectedItem.Text.Trim();
            }


            FileStream fileOne = new FileStream(Server.MapPath("/TemplateXls/MemberValueAllot.xls"), FileMode.Open, FileAccess.ReadWrite);


            HSSFWorkbook excel = new HSSFWorkbook(fileOne);
            HSSFSheet sheet = (HSSFSheet)excel.GetSheetAt(0);
            //单元格格式
            HSSFDataFormat format = (HSSFDataFormat)excel.CreateDataFormat();
            HSSFCellStyle cs = (HSSFCellStyle)excel.CreateCellStyle();

            //文字置中 
            cs.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            cs.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            //边框及颜色 
            cs.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //cs.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index; cs.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index;
            cs.RightBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index; cs.TopBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index;
            // 设置字体   
            IFont font1 = excel.CreateFont();
            font1.FontName = "宋体";
            //字体大小 
            font1.FontHeightInPoints = 9;
            cs.SetFont(font1);

            //顶头设置
            HSSFCellStyle csHeader = (HSSFCellStyle)excel.CreateCellStyle();
            //文字置中 
            csHeader.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            csHeader.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont fontHeader = excel.CreateFont();
            fontHeader.FontName = "宋体";
            //字体大小 
            fontHeader.FontHeightInPoints = 9;
            //字体颜色
            fontHeader.Color = HSSFColor.GREY_50_PERCENT.BLACK.index;
            //字体加粗
            fontHeader.Boldweight = (short)2000;
            csHeader.SetFont(fontHeader);
            //csHeader
            IRow dataRowHeader = sheet.GetRow(0);

            dataRowHeader.GetCell(0).SetCellValue(title + "个人产值明细表");
            //dataRowHeader.GetCell(0).CellStyle = csHeader;            


            for (int i = 0; i < table.Rows.Count; i++)
            {
                IRow dataRow = sheet.CreateRow(i + 2);

                for (int j = 0; j < table.Columns.Count; j++)
                {
                    object cellvalue = "";
                    if (j == 0)
                    {
                        cellvalue = (i + 1).ToString();
                    }
                    else
                    {
                        cellvalue = table.Rows[i][j];
                    }
                    WriteExcelValue(dataRow.CreateCell(j), cellvalue);
                    //cs.WrapText = true;
                    dataRow.GetCell(j).CellStyle = cs;
                }
                //纵向总计
                dataRow.CreateCell(table.Columns.Count).SetCellFormula("SUM(C" + (i + 3) + ":D" + (i + 3) + ")");
                dataRow.GetCell(table.Columns.Count).CellStyle = csHeader;
            }
            //横向总计
            int zj = table.Rows.Count + 2;
            IRow dataRowSum = sheet.CreateRow(zj);
            dataRowSum.Height = 25;
            dataRowSum.HeightInPoints = 25;
            for (int i = 0; i < 5; i++)
            {
                sheet.AddMergedRegion(new CellRangeAddress(zj, zj, 0, 1));

                string strsum = "";
                if (i == 0)
                {
                    // dataRowSum.CreateCell(i).SetCellValue("总计：");
                    WriteExcelValue(dataRowSum.CreateCell(i), "总计：");
                    dataRowSum.GetCell(i).CellStyle = csHeader;
                }
                else
                {
                    if (i == 2)
                    {
                        strsum = "SUM(C3:C" + zj + ")";
                    }

                    if (i == 3)
                    {
                        strsum = "SUM(D3:D" + zj + ")";
                    }

                    if (i == 4)
                    {
                        strsum = "SUM(E3:E" + zj + ")";
                    }

                    dataRowSum.CreateCell(i).SetCellFormula(strsum);
                    dataRowSum.GetCell(i).CellStyle = csHeader;
                }

            }


            using (MemoryStream memoryStream = new MemoryStream())
            {

                excel.Write(memoryStream);
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", HttpUtility.UrlEncode(title + "个人产值分配表", Encoding.UTF8)));
                Response.ContentType = "application/ms-excel";
                Response.BinaryWrite(memoryStream.ToArray());
                excel = null;
                fileOne.Close();
                Response.End();
            }
        }
        //判断数据类型
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };

                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());
                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));
                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);
                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);
                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        #endregion
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindProject();
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string unitid = base.UserUnitNo.ToString();
            if (this.drp_unit.Items.FindByValue(unitid) != null)
            {
                this.drp_unit.Items.FindByValue(unitid).Selected = true;
            }
        }
    }
}
