﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="MapOfCity_gsBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.MapOfCity_gsBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .map img
        {
            width: 630px;
            height: 450px;
            position: absolute;
        }
        .map
        {
            margin-top: 0px;
            margin-left: 100px;
        }
        .mapDiv
        {
            width: 80px;
            height: 60px;
            padding: 5px;
            color: #F0F0F0;
            font-size: 10pt;
            font-family: 新宋体;
            background-color: #f0f0f0;
            position: absolute;
            display: none;
            font-style: normal;
            word-break: break-all;
        }
        .imgsrc
        {
            margin-top: 0px;
            margin-left: 100px;
        }
        
        .absoluteDiv
        {
            position: absolute;
            background-repeat: no-repeat;
        }
        .textDiv
        {
            width: 40px;
            height: 20px;
        }
        body
        {
            font-size: 12px;
        }
        /*jQuery UI fakes 默认字体大小  */.ui-widget
        {
            font-size: 1em;
        }
        .ui-dialog .ui-dialog-buttonpane
        {
            padding-top: .1em;
            padding-bottom: .1em;
        }
        /*表格基本样式*/.show_projectNumber
        {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }
        .show_projectNumber td
        {
            border: solid 1px #CCC;
            font-size: 12px;
            font-family: "微软雅黑";
            height: 20px;
        }
        .cls_content_head
        {
            font-weight:bold;
            width: 100%;
            margin: 0 auto;          
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
            margin-top: 2px;
            table-layout: fixed;
        }
        .cls_content_head td
        {
            height: 25px;
            border: solid 1px #CCC;
            word-break: keep-all; /* 不换行 */
            white-space: nowrap; /* 不换行 */
            overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
            text-overflow: ellipsis; /* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用。*/
        }
        .tb_row
        {
            border-collapse: collapse;
            border: solid 1px #CCC;
            width: 100%;
            table-layout: fixed;
        }
        .tb_row td
        {
            height:25px;
            border: solid 1px #CCC;
            word-break: keep-all; /* 不换行 */
            white-space: nowrap; /* 不换行 */
            overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
            text-overflow: ellipsis; /* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用。*/
        }
    </style>
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/LeadershipCockpit/MapOfCity_gs.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#show6_1_5").parent().attr("class", "active");
            $("#show6_1").attr("class", "arrow open");
            $("#show6").parent().attr("class", "open");
            $("#arrow6").attr("class", "arrow open");
            $("#show6_1_5").parent().parent().css("display", "block");
            $("#show6_1_5").parent().parent().parent().parent().css("display", "block");

        });   //清空数据     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        领导驾驶舱 <small>项目分布统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>项目分布统计</a><i class="fa fa-angle-right">
    </i><a>项目分布统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div id="imgsrc" class="imgsrc">
                <div class="map">
                    <img id="cityPro" option="gs" border="0" usemap="#Map" src="city2/甘肃省.png" alt="甘肃省"
                        style="height: 447px; width: 620px; margin-right: 0px; margin-top: 0px; top: 84px;
                        left: 182px;" />
                    <map name="Map" id="Map">
                        <area id="定西" alt="定西" href="#" coords="444, 298, 492, 332" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="定西" />
                        <area id="临夏自治州" alt="临夏自治州" href="#" coords="355, 289, 440, 314" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="临夏自治州" />
                        <area id="兰州市" alt="兰州市" href="#" coords="410, 259, 450, 288" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="兰州市" />
                        <area id="白银市" alt="白银市" href="#" coords="422, 216, 521, 255" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="白银市" />
                        <area id="庆阳市" alt="庆阳市" href="#" coords="525, 203, 619, 270" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="庆阳市" />
                        <area id="张掖市" alt="张掖市" href="#" coords="247, 129, 324, 218" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="张掖市" />
                        <area id="酒泉市" alt="酒泉市" href="#" coords="208, 73, 247, 227" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="酒泉市" />
                        <area id="武威市" alt="武威市" href="#" coords="365, 145, 420, 228" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="武威市" />
                        <area id="金昌市" alt="金昌市" href="#" coords="327, 107, 363, 244" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="金昌市" />
                        <area id="平凉市" alt="平凉市" href="#" coords="532, 271, 585, 304" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="平凉市" />
                        <area id="甘南藏族自治州" alt="甘南藏族自治州" href="#" coords="380, 345, 476, 384" shape="rect"
                            data-toggle="modal" href="#PopAreaForProjectListDiv" title="甘南藏族" />
                        <area id="陇南地区" alt="陇南地区" href="#" coords="485, 358, 549, 405" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="陇南地区" />
                        <area id="天水地区" alt="天水地区" href="#" coords="502, 313, 551, 348" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" title="天水地区" />
                        <area id="嘉峪关市" alt="嘉峪关市" href="#" coords="51, 89, 207, 174" shape="rect" data-toggle="modal"
                            href="#PopAreaForProjectListDiv" data-toggle="modal" href="#PopAreaForProjectListDiv"
                            title="嘉峪关市" />
                    </map>
                </div>
            </div>
        </div>
    </div>
    <!--项目列表PopArea-->
    <div id="PopAreaForProjectListDiv" class="modal fade yellow" tabindex="-1" data-width="1000"
        aria-hidden="true" style="display: none; width: 1000px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                <span id="provinceName"></span>
            </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <!--内容Div-->
                    <div id="PopContext">
                        <table class="cls_content_head" >
                            <tr>
                                <td align="center" style="width: 80px;">
                                    编号
                                </td>
                                <td align="center" style="width: 200px;">
                                    关联合同
                                </td>
                                <td align="center" style="width: 150px;">
                                    项目负责人
                                </td>
                                <td align="center" style="width: 200px;">
                                    建设单位
                                </td>
                                <td align="center" style="width: 50px;">
                                    级别
                                </td>
                                <td align="center" style="width: 100px;">
                                    开始时间
                                </td>
                                <td align="center" style="width: 100px;">
                                    结束时间
                                </td>
                                <td align="center" style="width: 40px;">
                                    操作
                                </td>
                            </tr>
                        </table>
                        <table class="tb_row" id="PopAreaTableForProjectList">
                        </table>
                    </div>
                    <!--分页Div-->
                    <div style="height: 10%" id="PopPager">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--项目详细信息PopArea-->
    <div id="PopAreaForPorProjectDetail" class="modal fade yellow" tabindex="-1" data-width="800"
        aria-hidden="true" style="display: none; width: 800px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                <span id="title"></span>
            </h4>
        </div>
        <div class="modal-body">
          
                    <table class="table table-hover table-bordered" >
                        <tr>
                            <td style="width: 120px">
                                项目名称：
                            </td>
                            <td style="width: 300px">
                                <span id="ProjectName"></span>
                            </td>
                            <td style="width: 120px">
                                合同关联：
                            </td>
                            <td>
                                <span id="CopertaionName"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                结构样式：
                            </td>
                            <td>
                                <span id="StructStyleStringHtml"></span>
                            </td>
                            <td>
                                项目类型：
                            </td>
                            <td>
                                <span id="ProjectType"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                管理级别：
                            </td>
                            <td>
                                <span id="ManagerLevelString"></span>
                            </td>
                            <td>
                                设计阶段：
                            </td>
                            <td id="content">
                                <span id="Status"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                项目来源：
                            </td>
                            <td>
                                <span id="ProjectFrom"></span>
                            </td>
                            <td>
                                合同额：
                            </td>
                            <td>
                                <span id="CoperationFinancial"></span>万元
                            </td>
                        </tr>
                        <tr>
                            <td>
                                建设单位：
                            </td>
                            <td>
                                <span id="BuildUnit"></span>
                            </td>
                            <td>
                                承接部门：
                            </td>
                            <td>
                                <span id="UnderTakeDepartment"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                建设地点：
                            </td>
                            <td>
                                <span id="Address"></span>
                            </td>
                            <td>
                                建设规模：
                            </td>
                            <td>
                                <span id="BuildScale"></span>平米
                            </td>
                        </tr>
                        <tr>
                            <td class="style3">
                                甲方负责人：
                            </td>
                            <td class="style3">
                                <span id="ChargeManJia"></span>
                            </td>
                            <td class="style3">
                                电话：
                            </td>
                            <td class="style3">
                                <span id="PhoneNumber"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                项目开始日期：
                            </td>
                            <td>
                                <span id="StartTimeString"></span>
                            </td>
                            <td>
                                项目完成日期：
                            </td>
                            <td>
                                <span id="EndTimeString"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                项目备注：
                            </td>
                            <td colspan="3">
                                <span id="Remark"></span>
                            </td>
                        </tr>
                    </table>
              
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
