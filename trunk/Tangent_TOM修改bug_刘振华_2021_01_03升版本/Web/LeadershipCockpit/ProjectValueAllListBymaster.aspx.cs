﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using NPOI.SS.Util;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using NPOI.HSSF.Util;

namespace TG.Web.LeadershipCockpit
{
    public partial class ProjectValueAllListBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //绑定年份
                BindYear();
                //默认选中年份
                SelectCurrentYear();
                //绑定项目列表
                BindProject();
                //绑定权限
                BindPreviewPower();

            }
            else
            {
                Output();
            }
        }

        private void Output()
        {
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb1 = new StringBuilder("");
            StringBuilder sb2 = new StringBuilder("");
            TG.BLL.StandBookBp stbb = new TG.BLL.StandBookBp();
            //名字不为空
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.Append(" AND p.pro_Name LIKE '%" + keyname + "%'  ");
            }
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND p.Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                //sb.AppendFormat(" AND year(p.pro_startTime)={0}", this.drp_year.SelectedValue);
                sb1.AppendFormat(" AND year(InAcountTime)={0}", this.drp_year.SelectedValue);
                sb2.AppendFormat(" AND ActualAllountTime='{0}'", this.drp_year.SelectedValue);
            }
            //检查权限
            GetPreviewPowerSql(ref sb);

            //排序
            //string orderString = " Order by " + OrderColumn + " " + Order;
            //sb.Append(orderString);

            DataTable dt = new TG.BLL.StandBookBp().GetExcelProjectValueAllot(sb.ToString(), sb1.ToString(), sb2.ToString());
            ExportDataToExcel(dt);
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();

            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (p.InsertUserID =" + UserSysNo + " OR p.PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND p.Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //根据条件查询
        public void BindProject()
        {
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb1 = new StringBuilder("");
            StringBuilder sb2 = new StringBuilder("");

            //  TG.BLL.StandBookBp stbb = new TG.BLL.StandBookBp();
            //   //名字不为空
            //  if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND p.pro_Name LIKE '%" + keyname + "%'  ");
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND p.Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    //sb.AppendFormat(" AND year(p.pro_startTime)={0}", this.drp_year.SelectedValue);
            //    sb1.AppendFormat(" AND year(InAcountTime)={0}", this.drp_year.SelectedValue);
            //    sb2.AppendFormat(" AND ActualAllountTime='{0}'", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();

            //得到所有总计
            // GetColumnSum(sb.ToString(), sb1.ToString(), sb2.ToString());

            //所有记录数
            // this.AspNetPager1.RecordCount = int.Parse(stbb.GetListPageProcCount(sb.ToString(), sb1.ToString(), sb2.ToString()).ToString());

            //排序
            //  string orderString = " Order by " + OrderColumn + " " + Order;
            // sb.Append(orderString);

            // gv_project.DataSource = stbb.GetListByPageProc(sb.ToString(),sb1.ToString(),sb2.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            //  gv_project.DataBind();

        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
        }
        //导出Excel
        #region

        private void ExportDataToExcel(DataTable table)
        {
            string title = "";
            if (this.drp_year.SelectedIndex != 0)
            {
                title = this.drp_year.SelectedValue.Trim() + "年";
            }
            if (this.drp_unit.SelectedIndex != 0)
            {
                title += this.drp_unit.SelectedItem.Text.Trim();
            }

            HSSFWorkbook excel = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            ISheet sheet = excel.CreateSheet("sheet1");

            //单元格计算格式
            HSSFDataFormat format = (HSSFDataFormat)excel.CreateDataFormat();

            //文字样式
            HSSFCellStyle cs = (HSSFCellStyle)excel.CreateCellStyle();
            //文字置中 
            cs.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            cs.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            //边框及颜色 
            cs.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //cs.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index; cs.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index;
            cs.RightBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index; cs.TopBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index;

            // 设置字体   
            IFont font1 = excel.CreateFont();
            font1.FontName = "宋体";
            //字体大小 
            font1.FontHeightInPoints = 9;
            cs.SetFont(font1);

            //顶头设置(表头和合计的样式)
            HSSFCellStyle csHeader = (HSSFCellStyle)excel.CreateCellStyle();
            //文字置中 
            csHeader.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            csHeader.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            csHeader.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont fontHeader = excel.CreateFont();
            fontHeader.FontName = "宋体";
            //字体大小 
            fontHeader.FontHeightInPoints = 9;
            //字体颜色
            fontHeader.Color = HSSFColor.GREY_50_PERCENT.BLACK.index;
            //字体加粗
            fontHeader.Boldweight = (short)2000;
            csHeader.SetFont(fontHeader);
            //大标题
            IRow dataRowHeader = sheet.CreateRow(0);
            ICell dataCellHeader = dataRowHeader.CreateCell(0);
            dataCellHeader.SetCellValue(title + "项目产值分配明细表");
            CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 0, 0, 7);
            sheet.AddMergedRegion(cellRangeAddress);
            ((HSSFSheet)sheet).SetEnclosedBorderOfRegion(cellRangeAddress, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            dataCellHeader.CellStyle = csHeader;

            IRow dataRowTitle = sheet.CreateRow(1);
            ICell dataCellTitle = dataRowTitle.CreateCell(0);
            dataCellTitle.SetCellValue("序号");
            dataCellTitle.CellStyle = csHeader;
            ICell dataCellTitle1 = dataRowTitle.CreateCell(1);
            dataCellTitle1.SetCellValue("项目名称");
            dataCellTitle1.CellStyle = csHeader;
            ICell dataCellTitle2 = dataRowTitle.CreateCell(2);
            dataCellTitle2.SetCellValue("实收产值(万元)");
            dataCellTitle2.CellStyle = csHeader;
            ICell dataCellTitle3 = dataRowTitle.CreateCell(3);
            dataCellTitle3.SetCellValue("转经济所(万元)");
            dataCellTitle3.CellStyle = csHeader;
            ICell dataCellTitle4 = dataRowTitle.CreateCell(4);
            dataCellTitle4.SetCellValue("转暖通所(万元)");
            dataCellTitle4.CellStyle = csHeader;
            ICell dataCellTitle4_4 = dataRowTitle.CreateCell(5);
            dataCellTitle4_4.SetCellValue("转土建所(万元)");
            dataCellTitle4_4.CellStyle = csHeader;
            ICell dataCellTitle5 = dataRowTitle.CreateCell(6);
            dataCellTitle5.SetCellValue("本所产值(万元)");
            dataCellTitle5.CellStyle = csHeader;
            ICell dataCellTitle6 = dataRowTitle.CreateCell(7);
            dataCellTitle6.SetCellValue("所留(万元)");
            dataCellTitle6.CellStyle = csHeader;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                IRow dataRow = sheet.CreateRow(i + 2);
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    object cellvalue = "";
                    if (j == 0)
                    {
                        cellvalue = (i + 1).ToString();
                    }
                    else
                    {
                        cellvalue = table.Rows[i][j];
                    }
                    WriteExcelValue(dataRow.CreateCell(j), cellvalue);
                    cs.WrapText = true;
                    dataRow.GetCell(j).CellStyle = cs;
                }
                //纵向总计
                //  dataRow.CreateCell(table.Columns.Count).SetCellFormula("SUM(C" + (i + 3) + ":F" + (i + 3) + ")");
                //   dataRow.GetCell(table.Columns.Count).CellStyle = csHeader;
            }
            //横向总计
            int zj = table.Rows.Count + 2;
            IRow dataRowSum = sheet.CreateRow(zj);
            dataRowSum.Height = 25;
            dataRowSum.HeightInPoints = 25;
            for (int i = 0; i < 8; i++)
            {
                sheet.AddMergedRegion(new CellRangeAddress(zj, zj, 0, 1));
                string strsum = "A";
                if (i == 0)
                {
                    // dataRowSum.CreateCell(i).SetCellValue("总计：");
                    WriteExcelValue(dataRowSum.CreateCell(i), "总计：");
                    dataRowSum.GetCell(i).CellStyle = csHeader;
                }
                else
                {

                    int asciiCode = (int)Convert.ToChar(strsum);
                    asciiCode = asciiCode + i;
                    strsum = ((char)asciiCode).ToString();
                    strsum = "SUM(" + strsum + "3:" + strsum + zj + ")";

                    dataRowSum.CreateCell(i).SetCellFormula(strsum);
                    dataRowSum.GetCell(i).CellStyle = csHeader;
                }

            }

            //for (int i = 0; i < zj; i++)
            //{
            //    sheet.SetColumnWidth(i, 50 * 256);
            //    // sheet.CreateRow(0).Height = 200 * 20; 第一行高度
            //}

            sheet.DefaultColumnWidth = 20;
            sheet.DefaultRowHeight = 30;
            sheet.GetRow(0).Height = 40 * 20;
            sheet.GetRow(1).Height = 30 * 20;
            sheet.SetColumnWidth(1, 30 * 256);

            excel.Write(ms);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", HttpUtility.UrlEncode(title + "项目产值分配明细表", Encoding.UTF8)));
            Response.ContentType = "application/ms-excel";
            Response.BinaryWrite(ms.ToArray());
            excel = null;
            sheet = null;
            ms.Flush();
            ms.Position = 0;
            ms.Close();
            ms.Dispose();
            Response.End();
        }
        //判断数据类型
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };

                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());
                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));
                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);
                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);
                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        #endregion
    }
}