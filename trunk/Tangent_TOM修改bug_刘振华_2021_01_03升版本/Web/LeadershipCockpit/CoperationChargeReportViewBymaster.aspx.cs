﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TG.DBUtility;
using System.Text;
using InfoSoftGlobal;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationChargeReportViewBymaster : System.Web.UI.Page
    {
        #region QueryString
        /// <summary>
        /// 合同系统编号
        /// </summary>
        public int CoperationSysNo
        {
            get
            {
                int coperationSysNo = 0;
                int.TryParse(Request["coperationSysNo"], out coperationSysNo);
                return coperationSysNo;
                //return 1;
            }
        }

        /// <summary>
        /// 合同名称
        /// </summary>
        public string CoperationName
        {
            get
            {
                return Request["coperationName"];
            }
        }

        /// <summary>
        /// 合同额
        /// </summary>
        public decimal CoperationAccount
        {
            get
            {
                decimal coperationAccount = decimal.Zero;
                decimal.TryParse(Request["coperationAccount"], out coperationAccount);
                return coperationAccount;
            }
        }

        /// <summary>
        /// 合同实收额
        /// </summary>
        public decimal CoperationRealReceivables
        {
            get
            {
                decimal coperationRealReceivables = decimal.Zero;
                decimal.TryParse(Request["coperationRealReceivables"], out coperationRealReceivables);
                return coperationRealReceivables;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                coperationRealReceivables.Text = CoperationAccount + "(万元)";
                coperationAccount.Text = CoperationRealReceivables + "(万元)";
                coperationName.Text = CoperationName;
            }
            DrawReportView();
            CreateAccountDetailTable();
        }
        /// <summary>
        /// 绘制报表
        /// </summary>
        private void DrawReportView()
        {
            string sql = string.Format("select sum(Acount) as RealPay from cm_ProjectCharge where cprID={0} AND Status<>'B'", CoperationSysNo);
            object objAlreadyPay = DbHelperSQL.GetSingle(sql);
            decimal alreadyPay = objAlreadyPay == null ? decimal.Zero : Convert.ToDecimal(objAlreadyPay);

            StringBuilder strXML = new StringBuilder();
            strXML.Append("<chart caption='合同收费统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='万元' numberPrefix='' baseFontSize='14'>");
            strXML.AppendFormat("<set label='已收费'  value='{0}' />", alreadyPay);
            strXML.AppendFormat("<set label='未收费'  value='{0}' />", CoperationAccount - CoperationRealReceivables);
            strXML.Append("</chart>");

            LiteralReportView.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML.ToString(), "合同收费统计", "100%", "300", false, true, false);
            Dictionary<string, string> dicColor = new Dictionary<string, string>();
            dicColor.Add("已收费", "#B3DAF8");
            dicColor.Add("未收费", "#F6C11D;");
            PanelColor.Text = CreateColorDiv(dicColor);
        }
        //创建颜色层
        private string CreateColorDiv(Dictionary<string, string> descriptionArray)
        {
            string str = "<table style=\"height: 10px;\" class=\"colorDivTableStyle\">";
            str += "<tr>";
            foreach (KeyValuePair<string, string> description in descriptionArray)
            {
                str += "<td>";
                str += "<div style=\"width: 10px; height: 10px; background-color: " + description.Value + ";\">";
                str += "</div>";
                str += "</td>";
                str += "<td>";
                str += description.Key;
                str += "</td>";
            }
            str += "</tr>";
            str += "</table>";
            return str;
        }

        //创建收款信息表
        private void CreateAccountDetailTable()
        {
            //查询计划收款信息
            string sql = "select * from [cm_ProjectCharge]  where  Status<>'B' AND cprID=" + CoperationSysNo;

            DataSet set = DbHelperSQL.Query(sql);

            if (set.Tables.Count > 0)
            {
                DataTable table = set.Tables[0];
                string html = "";
                for (int i = 0; i < table.Rows.Count; i++)
                {

                    html += "<div class=\"col-md-6\">";
                    html += " <table class=\"table table-bordered table-hover\" >";
                    html += "<tr><th  colspan=\"2\" style=\"text-align:center;\">第" + (i + 1) + "次收费</th></tr>";
                    html += " <tr><td style=\"width:150px;\">收款额：</td><td>" + Convert.ToDecimal(table.Rows[i][3].ToString()).ToString("f4") + "(万元)</td></tr>";
                    html += " <tr><td>汇款人：</td><td>" + table.Rows[i][4] + "</td></tr>";
                    html += " <tr><td>入账人：</td><td>" + GetMemNameByLogin(table.Rows[i][5].ToString()) + "</td></tr>";
                    html += " <tr><td>收款时间：</td><td>" + Convert.ToDateTime(table.Rows[i][6]).ToString("yyyy-MM-dd") + "</td></tr>";
                    html += " <tr><td>入账单据号：</td><td>" + table.Rows[i][8].ToString() + "</td></tr>";
                    html += " <tr><td>财务是否确认入账：</td><td>" + GetCaiWuStatus(table.Rows[i][7].ToString().Trim()) + "</td></tr>";
                    html += " <tr><td>备注：</td><td>" + table.Rows[i][9] + "</td></tr>";
                    html += "</table>";
                    html += "</div>";


                }
                LiteralAccountDetail.Text = html;
            }
        }
        //是否财务入账
        protected string GetCaiWuStatus(string status)
        {
            if (status == "E")
            {
                return "是";
            }
            else
            {
                return "否";
            }
        }
        /// <summary>
        /// 20130514
        /// </summary>
        /// <param name="memlogin"></param>
        /// <returns></returns>
        public string GetMemNameByLogin(string memlogin)
        {
            List<TG.Model.tg_member> models = new TG.BLL.tg_member().GetModelList(" mem_ID='" + memlogin + "'");
            if (models.Count > 0)
            {
                return models[0].mem_Name.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}