﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.DBUtility;
using System.Text;
using InfoSoftGlobal;
using System.Data;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationChargeDoneDetails : System.Web.UI.Page
    {
        /// <summary>
        /// 合同系统编号
        /// </summary>
        public int CoperationSysNo
        {
            get
            {
                int coperationSysNo = 0;
                int.TryParse(Request["cprid"], out coperationSysNo);
                return coperationSysNo;
            }
        }
        /// <summary>
        /// 合同额
        /// </summary>
        public decimal CoperationAccount
        {
            get
            {
                decimal? obj = new TG.BLL.cm_Coperation().GetModel(CoperationSysNo).cpr_Acount;
                return Convert.ToDecimal(obj);
            }
        }
        /// <summary>
        /// 已收款
        /// </summary>
        public decimal CoperationChargeAcount
        {
            get
            {
                string strSql = " Select Sum(Acount) From dbo.cm_ProjectCharge Where Status='E' AND cprID=" + CoperationSysNo;
                object obj = DbHelperSQL.GetSingle(strSql);
                return Convert.ToDecimal(obj);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowInfo();
                //绑定明细
                BindChargeList();
                //绑定统计
                DrawReportView();
            }
        }
        /// <summary>
        /// 显示合同的基本信息
        /// </summary>
        protected void ShowInfo()
        {
            TG.Model.cm_Coperation model = new TG.BLL.cm_Coperation().GetModel(CoperationSysNo);
            //显示基本属性
            this.lbl_BuildUnit.Text = model.BuildUnit.Trim();
            this.lbl_cprArea.Text = model.BuildArea;
            this.lbl_cprLevel.Text = model.BuildType;
            this.lbl_cprNo.Text = model.cpr_No;
            this.lbl_cprType.Text = model.cpr_Type;
            this.lbl_startTime.Text = Convert.ToDateTime(model.cpr_SignDate).ToString("yyyy-MM-dd");
            this.lbl_endTime.Text = Convert.ToDateTime(model.cpr_DoneDate).ToString("yyyy-MM-dd");
            this.coperationName.Text = model.cpr_Name.Trim();
            this.coperationRealReceivables.Text = CoperationChargeAcount.ToString("f2");
            this.coperationAccount.Text = CoperationAccount.ToString();
        }
        /// <summary>
        /// 绘制报表
        /// </summary>
        private void DrawReportView()
        {
            //统计图表
            StringBuilder strXML = new StringBuilder();
            strXML.Append("<chart caption='合同收费统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='万元' numberPrefix='' baseFontSize='14'>");
            strXML.AppendFormat("<set label='已收费'  value='{0}' />", CoperationChargeAcount);
            strXML.AppendFormat("<set label='未收费'  value='{0}' />", CoperationAccount - CoperationChargeAcount);
            strXML.Append("</chart>");

            LiteralReportView.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML.ToString(), "合同收费统计", "100%", "300", false, true, false);
            Dictionary<string, string> dicColor = new Dictionary<string, string>();
            dicColor.Add("已收费", "#B3DAF8");
            dicColor.Add("未收费", "#F6C11D;");
            PanelColor.Text = CreateColorDiv(dicColor);
        }
        //创建颜色层
        private string CreateColorDiv(Dictionary<string, string> descriptionArray)
        {
            string str = "<table style=\"height: 10px;\" class=\"colorDivTableStyle\">";
            str += "<tr>";
            foreach (KeyValuePair<string, string> description in descriptionArray)
            {
                str += "<td>";
                str += "<div style=\"width: 10px; height: 10px; background-color: " + description.Value + ";\">";
                str += "</div>";
                str += "</td>";
                str += "<td>";
                str += description.Key;
                str += "</td>";
            }
            str += "</tr>";
            str += "</table>";
            return str;
        }
        //绑定收费列表
        protected void BindChargeList()
        {
            string strWhere = " cprID=" + CoperationSysNo;
            this.gv_Coperation.DataSource = new TG.BLL.cm_ProjectCharge().GetList(strWhere);
            this.gv_Coperation.DataBind();
        }
        //行绑定
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string status = e.Row.Cells[6].Text.Trim();
                if (status == "A")
                {
                    status = "财务确认";
                }
                else if (status == "C")
                {
                    status = "所长确认";
                }
                else if (status == "E")
                {
                    status = "完成入账";
                }
                e.Row.Cells[6].Text = status;
                //计算比例
                Label lbl_persent = e.Row.Cells[2].FindControl("lbl_persent") as Label;
                decimal paycount = Convert.ToDecimal(e.Row.Cells[1].Text);
                if (paycount > 0)
                {
                    lbl_persent.Text = ((paycount / CoperationAccount) * 100).ToString("f2") + "%";
                }
                //入款人
                e.Row.Cells[4].Text = GetUserNameByID(e.Row.Cells[4].Text);

            }
        }
        // 获取用户名称
        protected string GetUserNameByID(string memid)
        {
            if (memid != "")
            {
                return new TG.BLL.tg_member().GetModel(int.Parse(memid)).mem_Name.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}