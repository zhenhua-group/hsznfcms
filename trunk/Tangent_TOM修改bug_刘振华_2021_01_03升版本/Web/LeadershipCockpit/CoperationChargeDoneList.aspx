﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CoperationChargeDoneList.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationChargeDoneList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectCharge.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.tablesort.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="../js/LeadershipCockpit/CoperationChargeDoneList.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <style type="text/css">
        html, body
        {
            height: 100%;
        }
    </style>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;当前位置：[领导驾驶舱]-[合同收款明细]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div">
                    <tr>
                        <td>
                            生产部门：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" Width="150px">
                                <asp:ListItem Value="-1">----------全院部门---------</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td>
                        合同年份：
                        </td>
                        <td>
                           <asp:DropDownList ID="drp_year" runat="server"  AppendDataBoundItems="true">
                            <asp:ListItem Value="-1">--全部年份--</asp:ListItem></asp:DropDownList>
                        </td>
                        <td>
                            收费时间：
                        </td>
                        <td>
                            <input type="text" name="txt_date1" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" size="20" style="width: 80px; background-color: #FFC;" />
                        </td>
                        <td>
                        截止时间：
                        </td>
                        <td>
                            <input type="text" name="txt_date1" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" size="20" style="width: 80px; background-color: #FFC;" />
                        </td>
                        <td>
                            合同名称：
                        </td>
                        <td>
                            <asp:TextBox ID="txt_keyname" runat="server" CssClass="TextBoxBorder"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/buttons/btn_search.gif"
                                OnClick="btn_ok_Click" Height="22px" Width="64px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table style="width: 100%;" class="cls_content_head" id="sorttable">
            <tr>
                <td style="width: 40px;" align="center" sortc="cpr_Id">
                    序号
                </td>
                <td align="center" sortc="cpr_No" style="width: 80px;">
                    合同编号
                </td>
                <td align="center" sortc="cpr_Name">
                    合同名称
                </td>
                <td align="center" style="width: 80px;" sortc="cpr_Unit">
                    承接部门
                </td>
                <td style="width: 90px;" align="center" sortc="cpr_Acount">
                    合同额(万元)
                </td>
                <td style="width: 80px;" align="center">
                    实收总额(万元)
                </td>
                <td style="width: 150px;" align="center">
                    收款进度
                </td>
                <td style="width: 40px;" align="center">
                    详细
                </td>
            </tr>
        </table>
        <asp:GridView ID="grid_cpr" runat="server" AutoGenerateColumns="False" ShowHeader="False"
            ShowFooter="true" Width="100%" Font-Size="12px" OnRowDataBound="GridView1_RowDataBound">
            <EmptyDataTemplate>
                没有数据！
            </EmptyDataTemplate>
            <FooterStyle Height="20px" />
            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text="<%# (Container.DataItemIndex+1).ToString() %>"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        合计：
                    </FooterTemplate>
                    <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="40px" />
                </asp:TemplateField>
                <asp:BoundField DataField="cpr_No">
                    <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="合同名称">
                    <ItemTemplate>
                        <img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px;">
                        <a href="#" title='<%# Eval("cpr_Name") %>'>
                            <%# Eval("cpr_Name").ToString().Length > 35 ? Eval("cpr_Name").ToString().Substring(0, 35) + "......" : Eval("cpr_Name").ToString()%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:BoundField DataField="cpr_Unit">
                    <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="cpr_Acount">
                    <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="lbl_shiji" runat="server" Text='<%# Eval("cpr_Id") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="80px" />
                    <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="progressbar" title="0.00%">
                        </div>
                        <asp:HiddenField ID="hid_persent" runat="server" />
                    </ItemTemplate>
                    <ItemStyle Width="150px" />
                    <FooterTemplate>
                        <div class="progressbar" title="0.00%">
                        </div>
                        <asp:HiddenField ID="hid_persent" runat="server" />
                    </FooterTemplate>
                    <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href='CoperationChargeDoneDetails.aspx?cprid=<%#Eval("cpr_Id") %>' class="cls_chk"
                            style="height: 26px;">查看</a>
                    </ItemTemplate>
                    <ItemStyle Width="40px" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle Font-Size="12px" />
        </asp:GridView>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条" CustomInfoTextAlign="Left"
            FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
            PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
            ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
            TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="30" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
    </div>
    <!-- 排序指定 -->
    <asp:HiddenField ID="hid_column" runat="server" Value="cpr_ID" />
    <asp:HiddenField ID="hid_order" runat="server" />
    </form>
</body>
</html>
