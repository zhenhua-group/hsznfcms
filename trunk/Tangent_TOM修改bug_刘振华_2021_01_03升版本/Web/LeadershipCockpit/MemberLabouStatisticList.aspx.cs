﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.LeadershipCockpit
{
    public partial class MemberLabouStatisticList : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();

                //绑定权限
                BindPreviewPower();

                //绑定字段
                BindColumns();

            }
        }
        //定义键值对
        public Dictionary<string, string> dic { 
            get
            {
                Dictionary<string, string> Dict = new Dictionary<string, string>();
                Dict.Add("姓名", "mem_Name");
                Dict.Add("性别", "mem_Sex");
                Dict.Add("单位", "unit_Name");
                Dict.Add("专业", "spe_Name");
                Dict.Add("参与项目总数", "TotalCount");
                Dict.Add("进行中项目总数", "JinXingCount");
                Dict.Add("完工项目总数", "WanGongCount");
                Dict.Add("暂停项目总数", "ZantingCount");
                return Dict;
            }
        }
        protected void BindColumns()
        {  
            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' checked value='" + pair.Value + "' />" + pair.Key + "</label>";
            }
            this.hid_cols.Value = "姓名,性别,单位,专业,参与项目总数,进行中项目总数,完工项目总数,暂停项目总数";   
        }      

        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        private void BindPreviewPower()
        {
            StringBuilder sb = new StringBuilder();
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND a.mem_ID=" + UserSysNo + "");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" and b.unit_ID= " + UserUnitNo + "");
            }

            this.hid_where.Value = sb.ToString();
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

            //if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            //{
            //    this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            //}
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
           
            DataTable dt = getTable();

            string modelPath = " ~/TemplateXls/Member_Labou_Statistic.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //标题样式
            ICellStyle style1= wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 13;//字号
            font1.FontName = "宋体";//字体
            style1.SetFont(font1);

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int colscount = 0;
            int row = 2;
            for (int i = -1; i < dt.Rows.Count; i++)
            {
                string colsname = this.hid_cols.Value;
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                //列
                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                if (colsname!="")
                {
                    string[] colsnamelist = colsname.Split(',');
                    if (colsnamelist.Length>0)
                    {
                        colscount = colsnamelist.Length;
                        //循环标题
                        if (i == -1)
                        {
                            //第一列
                            cell.CellStyle = style2;
                            cell.SetCellValue("序号");

                            for (int j = 0; j < colsnamelist.Length; j++)
                            {
                                cell = dataRow.CreateCell((j + 1));
                                cell.CellStyle = style2;
                                cell.SetCellValue(colsnamelist[j]);
                            }
                        }//数据
                        else
                        {
                            //第一列
                            cell.CellStyle = style2;
                            cell.SetCellValue((i+1));

                            for (int j = 0; j < colsnamelist.Length; j++)
                            {
                                cell = dataRow.CreateCell((j + 1));
                                cell.CellStyle = style2;
                                //得到字段名称
                                string colname = dic.FirstOrDefault(q => q.Key == colsnamelist[j].Trim()).Value;
                                cell.SetCellValue(dt.Rows[i][colname].ToString());
                            }
                        }

                        
                    }                    
                }
                //设置第一行标题
                if (colscount!=0)
                {
                    //标题
                    IRow heard = ws.CreateRow(0);
                    heard.Height = 30 * 20;
                    ICell heardcell = heard.CreateCell(0);
                    heardcell.SetCellValue("人员劳动力统计");                   
                    ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, colscount));
                    heardcell.CellStyle = style1;
                }
               

                //cell = dataRow.CreateCell(1);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                //cell = dataRow.CreateCell(2);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["mem_Sex"].ToString());

                //cell = dataRow.CreateCell(3);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["unit_Name"].ToString());

                //cell = dataRow.CreateCell(4);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["spe_Name"].ToString());

                //cell = dataRow.CreateCell(5);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["TotalCount"].ToString());

                //cell = dataRow.CreateCell(6);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["JinXingCount"].ToString());

                //cell = dataRow.CreateCell(7);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["WanGongCount"].ToString());

                //cell = dataRow.CreateCell(8);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["ZantingCount"].ToString());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("人员劳动力统计.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }


        /// <summary>
        /// 得到人员劳动力信息
        /// </summary>
        /// <returns></returns>
        private DataTable getTable()
        {
            StringBuilder sb = new StringBuilder();

            string spename = txt_SpeName.Value;
            string signdate = txt_signdate.Value;
            string finishdate = txt_finishdate.Value;
            string projectCount1 = txt_ProjectCount1.Value;
            string projectCount2 = txt_ProjectCount2.Value;
            string jinXingCount1 = txt_ProjectJinXingCount1.Value;
            string jinXingCount2 = txt_ProjectJinXingCount2.Value;
            string zanTingCount1 = txt_ProjectZanTingCount1.Value;
            string zanTingCount2 = txt_ProjectZanTingCount2.Value;
            string wangongCount1 = txt_ProjectWanGongCount1.Value;
            string wangongCount2 = txt_ProjectWanGongCount2.Value;

            StringBuilder strDate = new StringBuilder("");

            if (!string.IsNullOrEmpty(hid_where.Value))
            {
                sb.Append(hid_where.Value);
            }

            if (!string.IsNullOrEmpty(txt_keyname.Value))
            {
                sb.Append(" AND mem_name like '%" + txt_keyname.Value.Trim() + "%'");
            }

            if (drp_unit.SelectedIndex > 0)
            {
                sb.Append(" AND unit_Name like '%" + drp_unit.SelectedItem.Text.Trim() + "%'");
            }


            //专业 
            if (!string.IsNullOrEmpty(spename))
            {
                sb.Append(" AND spe_Name LIKE '%" + spename + "%' ");
            }

            //项目开始日期

            if (!string.IsNullOrEmpty(signdate) && string.IsNullOrEmpty(finishdate))
            {
                strDate.Append(" AND pro_StartTime >= '" + signdate.Trim() + "' ");
            }
            else if (string.IsNullOrEmpty(signdate) && !string.IsNullOrEmpty(finishdate))
            {
                strDate.Append(" AND pro_FinishTime <= '" + finishdate.Trim() + " '");
            }
            else if (!string.IsNullOrEmpty(signdate) && !string.IsNullOrEmpty(finishdate))
            {
                strDate.Append(" AND ( pro_StartTime>='" + signdate.Trim() + "' AND pro_FinishTime <= '" + finishdate.Trim() + "') ");
            }


            //项目总数
            if (!string.IsNullOrEmpty(projectCount1) && string.IsNullOrEmpty(projectCount2))
            {
                sb.Append(" AND TotalCount >=' " + projectCount1.Trim() + "' ");
            }
            else if (string.IsNullOrEmpty(projectCount1) && !string.IsNullOrEmpty(projectCount2))
            {
                sb.Append(" AND TotalCount <= '" + projectCount2.Trim() + " '");
            }
            else if (!string.IsNullOrEmpty(projectCount1) && !string.IsNullOrEmpty(projectCount2))
            {
                sb.Append(" AND (TotalCount>= '" + projectCount1.Trim() + "' AND TotalCount <= '" + projectCount2.Trim() + "' ) ");
            }

            //进行中总数
            if (!string.IsNullOrEmpty(jinXingCount1) && string.IsNullOrEmpty(jinXingCount2))
            {
                sb.Append(" AND JinXingCount >=' " + jinXingCount1.Trim() + "' ");
            }
            else if (string.IsNullOrEmpty(jinXingCount1) && !string.IsNullOrEmpty(jinXingCount2))
            {
                sb.Append(" AND JinXingCount <= '" + jinXingCount2.Trim() + " '");
            }
            else if (!string.IsNullOrEmpty(jinXingCount1) && !string.IsNullOrEmpty(jinXingCount2))
            {
                sb.Append(" AND ( JinXingCount>= '" + jinXingCount1.Trim() + "' AND JinXingCount <= '" + jinXingCount2.Trim() + "' ) ");
            }

            //暂停中总数
            if (!string.IsNullOrEmpty(zanTingCount1) && string.IsNullOrEmpty(zanTingCount2))
            {
                sb.Append(" AND ZantingCount >= '" + zanTingCount1.Trim() + " '");
            }
            else if (string.IsNullOrEmpty(zanTingCount1) && !string.IsNullOrEmpty(zanTingCount2))
            {
                sb.Append(" AND ZantingCount <= '" + zanTingCount2.Trim() + " '");
            }
            else if (!string.IsNullOrEmpty(zanTingCount1) && !string.IsNullOrEmpty(zanTingCount2))
            {
                sb.Append(" AND (ZantingCount>='" + zanTingCount1.Trim() + "' AND ZantingCount <=' " + zanTingCount2.Trim() + "' ) ");
            }

            //完工总数
            if (!string.IsNullOrEmpty(wangongCount1) && string.IsNullOrEmpty(wangongCount2))
            {
                sb.Append(" AND WanGongCount >= '" + wangongCount1.Trim() + "' ");
            }
            else if (string.IsNullOrEmpty(wangongCount1) && !string.IsNullOrEmpty(wangongCount2))
            {
                sb.Append(" AND WanGongCount <=' " + wangongCount2.Trim() + " '");
            }
            else if (!string.IsNullOrEmpty(wangongCount1) && !string.IsNullOrEmpty(wangongCount2))
            {
                sb.Append(" AND (WanGongCount>='" + wangongCount1.Trim() + "' AND WanGongCount <= '" + wangongCount2.Trim() + "') ");
            }

            TG.BLL.MemberLabouStatistic bll = new TG.BLL.MemberLabouStatistic();
            DataTable dt = bll.getMemberLabouStatisticList(sb.ToString(),strDate.ToString()).Tables[0];
            return dt;
        }
    }
}