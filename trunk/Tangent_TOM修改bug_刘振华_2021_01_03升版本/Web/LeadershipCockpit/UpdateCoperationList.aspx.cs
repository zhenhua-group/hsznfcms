﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Data;
using TG.Web.HttpHandler;
using Geekees.Common.Controls;
using System.Xml;
using TG.DBUtility;
using AjaxPro;


namespace TG.Web.LeadershipCockpit
{
    public partial class UpdateCoperationList : PageBase
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            //注册页面
            Utility.RegisterTypeForAjax(typeof(UpdateCoperationList));
            
            if (!IsPostBack)
            {               
                //绑定树形部门
                LoadUnitTree();
                //默认选中单位
                SelectedCurUnit();
                //绑定单位
                //BindUnit();
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //绑定合同
                BindCoperation();
                //绑定权限
                BindPreviewPower();
                
                //合同类别
                BindCorpType();

               
               

            }
        }
      
        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;

        }
        /// <summary>
        /// 选择年份
        /// </summary>
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            //绑定合同类别
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            DataSet ds = bll_dic.GetList(str_where);
            this.drp_type.DataSource = ds;
            this.drp_type.DataTextField = "dic_Name";
            this.drp_type.DataValueField = "ID";
            this.drp_type.DataBind();

            str_where = " dic_Type='cpr_lx'";
            DataSet ds1 = bll_dic.GetList(str_where);

        }
        /// <summary>
        /// 数据绑定
        /// </summary>
        public void BindCoperation()
        {
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
            StringBuilder strWhere = new StringBuilder("");
            //合同名称

            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();


        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }


        }

        /// <summary>
        /// 删除批量合同
        /// </summary>
        /// <param name="cpridlist"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DelCoperation(string cpridlist,string delcontent)
        {
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            string pageurl = "cpr_ShowCoprationBymaster.aspx";
            string tablename = "cm_Coperation";
            if (!string.IsNullOrEmpty(cpridlist))
            {
                string[] idlist = cpridlist.Split(',');
                if (idlist.Length > 0)
                {
                    foreach (string cprid in idlist)
                    {
                        TG.Model.cm_Coperation model_cpr = bll_cpr.GetModel(Convert.ToInt32(cprid));
                        if (model_cpr != null)
                        {
                            string sql = "delete from cm_Coperation where cpr_id=" + cprid;
                            tablename = DiffCoperation(model_cpr.cpr_Type2, ref pageurl);
                            if (tablename != "")
                            {
                                sql += ";delete from " + tablename + " where cpr_FID=" + cprid;
                            }
                            //删除合同
                            int num = DbHelperSQL.ExecuteSql(sql);
                            if (num > 0)
                            {
                                //发送消息给所长
                                string unitname = model_cpr.cpr_Unit.Trim();
                                List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(" mem_Principalship_ID=2 and mem_Unit_ID=(select unit_ID from tg_unit where unit_Name='" + unitname + "')");
                                if (list.Count > 0)
                                {
                                    foreach (TG.Model.tg_member mem in list)
                                    {
                                        //实例消息实体
                                        TG.Model.SysMessageViewEntity sysMessageViewEntity = new TG.Model.SysMessageViewEntity
                                        {
                                            //ReferenceSysNo = tempEntity.SysNo.ToString(),
                                            ReferenceSysNo = string.Format("{0}?cprfid={1}", pageurl, cprid),
                                            FromUser = mem.mem_ID,
                                            InUser = UserSysNo,
                                            MsgType = 101,
                                            MessageContent = string.Format("关于 \"{0}\" 的{1}的消息！", model_cpr.cpr_Name, ("合同已删除！原因："+delcontent)),
                                            QueryCondition = model_cpr.cpr_Name,
                                            Status = "A",
                                            IsDone = "B",
                                            ToRole = "0"
                                        };
                                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                                    }
                                }
                                else
                                {
                                    return "";
                                }

                            }
                        }

                    }

                }
                else
                {
                    return "0";
                }
            }
            else
            {
                return "0";
            }
            return "1";
        }
        /// <summary>
        /// 获取合同表名
        /// </summary>
        /// <param name="cpr_type2"></param>
        /// <param name="pageurl"></param>
        /// <returns></returns>
        public string DiffCoperation(string cpr_type2, ref string pageurl)
        {
            string url = "";
            switch (cpr_type2)
            {
                case "岩土工程勘察合同":
                    url = "cm_ReconnCoperation";
                    pageurl = "cpr_ShowReconnCoprationBymaster.aspx";
                    break;
                case "岩土工程设计合同":
                    url = "cm_DesignCoperation";
                    pageurl = "cpr_ShowDesignCoprationBymaster.aspx";
                    break;
                case "基坑及边坡监测合同":
                    url = "cm_PitCoperation";
                    pageurl = "cpr_ShowPitCoperationBymaster.aspx";
                    break;
                case "测量及沉降观测合同":
                    url = "cm_MeasureCoperation";
                    pageurl = "cpr_ShowTestCoperationBymaster.aspx";
                    break;
                case "岩土工程检测合同":
                    url = "cm_TestCoperation";
                    pageurl = "cpr_ShowMeasureCoperationBymaster.aspx";
                    break;
                case "岩土工程施工合同":
                    url = "cm_ConstruCoperartion";
                    pageurl = "cpr_ShowConstruCoperartionBymaster.aspx";
                    break;
                case "规划工程合同":
                    url = "cm_PlanningCoperation";
                    pageurl = "cpr_ShowPlanningCoperationBymaster.aspx";
                    break;
                case "景观规划合同":
                    url = "cm_SceneryCoperation";
                    pageurl = "cpr_ShowSceneryCoperationBymaster.aspx";
                    break;
                case "市政工程合同":
                    url = "cm_EngineerCoperation";
                    pageurl = "cpr_ShowEngineerCoperationBymaster.aspx";
                    break;
                case "工程监理":
                case "市政工程监理合同":
                case "景观绿化工程监理合同":
                case "天然气输配工程监理合同":
                case "管网、热力站工程监理合同":
                case "土方工程监理合同":
                    url = "cm_SuperCoperation";
                    pageurl = "cpr_ShowSuperCoperationBymaster.aspx";
                    break;
            }
            return url;
        }     
    }
}