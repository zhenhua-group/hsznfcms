﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberDetailed.aspx.cs"
    Inherits="TG.Web.LeadershipCockpit.MemberDetailed" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.tablesort.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style2
        {
            height: 33px;
            border-top: solid 1px #999;
            font-size: 12px;
            background: url(../images/bg_head.gif) repeat-x;
        }
    </style>
    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script src="../js/LeadershipCockpit/MemberDetailed.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <div style="position: absolute;" id="mm">
        &nbsp;
    </div>
    <table class="cls_container">
        <tr>
            <td class="style2">
                &nbsp;&nbsp;当前位置：[个人收入明细表]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div">
                    <tr>
                        <td>
                            生产部门：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit" runat="server" Width="120px" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            年份：<asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" Width="95px">
                                <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:ImageButton ID="btn_Search" runat="server" ImageUrl="~/Images/buttons/btn_search.gif"
                                Width="64px" Height="22px" OnClick="btn_Search_Click" />
                            <%--  <asp:ImageButton ID="btn_report" runat="server" ImageUrl="~/Images/buttons/output.gif"
                                Width="64px" Height="22px" OnClick="btn_report_Click" />  --%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="sorttable">
            <tr>
                <td style="width: 10%" align="center">
                    序号
                </td>
                <td style="width: 20%;" align="center">
                    姓名
                </td>
                <td style="width: 20%" align="center">
                    项目总产值(元)
                </td>
                <td style="width: 20%" align="center">
                    所补产值(元)
                </td>
                <td style="width: 20%; font-weight: bold;" align="center">
                    总计(元)
                </td>
                <td style="width: 10%" align="center">
                    操作
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_project" runat="server" CellPadding="0" AutoGenerateColumns="False"  CssClass="gridView_comm"
            RowStyle-HorizontalAlign="Center" ShowHeader="False" ShowFooter="true" Font-Size="12px"
            RowStyle-Height="22px" Width="100%" OnRowDataBound="gv_project_RowDataBound">
            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
            <EmptyDataTemplate>
                <b>暂无项目信息！</b>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# (Container.DataItemIndex+1).ToString() %>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        总计：
                    </FooterTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                    <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="mem_Name" HeaderText="姓名">
                    <ItemStyle Width="20%" HorizontalAlign="center" />
                </asp:BoundField>
                <asp:BoundField DataField="totalcount" HeaderText="总产值">
                    <ItemStyle Width="20%" HorizontalAlign="right" />
                    <FooterStyle Font-Bold="True" HorizontalAlign="right" />
                </asp:BoundField>
                <asp:BoundField DataField="sbcount" HeaderText="所补产值">
                    <ItemStyle Width="20%" HorizontalAlign="right" />
                    <FooterStyle Font-Bold="True" HorizontalAlign="right" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="总计">
                    <ItemTemplate>
                        <asp:Label ID="valuesum" Text="" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="right" Width="20%" Font-Bold="True" />
                    <FooterTemplate>
                        <asp:Label ID="valuesumcount" Text="" runat="server"></asp:Label>
                    </FooterTemplate>
                    <FooterStyle Font-Bold="True" HorizontalAlign="right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="操作">
                    <ItemTemplate>
                        <a href='javascript:void(0)' onclick="javascript:GetProjectList('<%#Eval("mem_ID") %>','<%#Eval("mem_Name") %>')">
                            查看</a>
                    </ItemTemplate>
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div style="padding-top: 3px; height: 23px; width: 100%;">
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                PageSize="30" SubmitButtonClass="submitbtn">
            </webdiyer:AspNetPager>
        </div>
    </div>
    <div id="PopAreaDivMain">
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <!-- 排序指定 -->
    <asp:HiddenField ID="hid_column" runat="server" Value="me.mem_ID" />
    <asp:HiddenField ID="hid_order" runat="server" Value="asc" />
    </form>
</body>
</html>
