﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;
using Geekees.Common.Controls;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationCountBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定单位
                BindUnit();
                //绑定树形部门
                LoadUnitTree();
                //初始单位
                InitUnitSelect();
                //绑定年份
                BindYear();
                //初始年
                InitDropDownByYear();
                //初始收款时间
                InitDate();
                //默认统计
                // CountCoperationAll();
            }

            // this.GridView1.UseAccessibleHeader = true;
            // this.GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;

        }
        ///<summary>
        ///检查是否需要权限判断
        ///</summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///<summary>
        ///判断是否是全部查看
        ///</summary>
        public string IsCheckAllPower
        {
            get
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        ///<summary>
        ///收款开始时间
        ///</summary>
        protected string ChargeStartTime { get; set; }
        ///<summary>
        ///收款结束时间
        ///</summary>
        protected string ChargeEndTime { get; set; }
        ///<summary>
        ///报表名称
        ///</summary>
        protected string ChartTip { get; set; }
        // 统计类型
        protected string CountType
        {
            get
            {
                return this.drp_type.SelectedItem.Text;
            }
        }
        //返回部门名称
        protected string UnitName
        {
            get
            {
                TG.Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                string unitname = "";
                if (unit != null)
                {
                    unitname = unit.unit_Name;
                }
                return unitname;
            }
        }

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drpunit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drpunit.Theme = macOS;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>

        TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        protected void BindUnit()
        {
            string strWhere = "";
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            DataSet ds = bll_unit.GetList(strWhere);


            this.drp_unit1.DataSource = ds;
            this.drp_unit1.DataTextField = "unit_Name";
            this.drp_unit1.DataValueField = "unit_ID";
            this.drp_unit1.DataBind();

            this.drp_unit2.DataSource = ds;
            this.drp_unit2.DataTextField = "unit_Name";
            this.drp_unit2.DataValueField = "unit_ID";
            this.drp_unit2.DataBind();

            this.drp_unit3.DataSource = ds;
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();

            this.drp_unit4.DataSource = ds;
            this.drp_unit4.DataTextField = "unit_Name";
            this.drp_unit4.DataValueField = "unit_ID";
            this.drp_unit4.DataBind();

            this.drp_unit5.DataSource = ds;
            this.drp_unit5.DataTextField = "unit_Name";
            this.drp_unit5.DataValueField = "unit_ID";
            this.drp_unit5.DataBind();

        }
        ///<summary>
        ///初始单位选中信息
        ///</summary>
        protected void InitUnitSelect()
        {
            string[] curUnit = { "0" };
            this.drpunit.CheckNodes(curUnit, true);
        }

        /// <summary>
        /// 绑定年份
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                    this.drp_year2_01.Items.Add(list[i]);
                    this.drp_year2_02.Items.Add(list[i]);
                    this.drp_year3_01.Items.Add(list[i]);
                    this.drp_year3_02.Items.Add(list[i]);
                    this.drp_year4_01.Items.Add(list[i]);
                    this.drp_year4_02.Items.Add(list[i]);
                    this.drp_year5_01.Items.Add(list[i]);
                    this.drp_year5_02.Items.Add(list[i]);
                }
            }
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_start.Value = str_year + "-01-01";
            this.txt_end.Value = str_year + "-12-31";
            this.txt_year1.Value = str_year + "-01-01";
            this.txt_year2.Value = str_year + "-12-31";
        }
        //创建时间查询条件
        protected void CreateTimeQuery()
        {
            string start = this.txt_start.Value.Trim();
            string end = this.txt_end.Value.Trim();
            string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            if (start == "")
            {
                start = Convert.ToDateTime(curtime).Year+"-01-01";
            }
            if (end == "")
            {
                end = Convert.ToDateTime(curtime).Year + "-12-31";
            }


            ChargeStartTime = start;
            ChargeEndTime = end;
        }
        //统计提示话术
        protected void CreateTipQuery()
        {
            string strTip = "";
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);

            bool IsCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (IsCheckAll)
            {
                //全部权限
                if (IsCheckAllPower == "1")
                {
                    strTip += "全院部门";
                }
                else
                {
                    //部门权限
                   
                    strTip += UnitName;
                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    strTip += string.Format("[{0}]", unitname);
                }
            }
            //年份
            if (this.drp_year.SelectedIndex == 0)
            {
                strTip += "全部年份";
            }
            else
            {
                strTip += this.drp_year.SelectedItem.Text.Trim() + "年";
            }

            ChartTip = strTip;
        }

        //初始年份
        protected void InitDropDownByYear()
        {
            if (this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year2_01.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year2_01.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year2_02.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year2_02.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year3_01.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year3_01.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year3_02.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year3_02.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year4_01.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year4_01.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year4_02.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year4_02.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year5_01.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year5_01.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
            if (this.drp_year5_02.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year5_02.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
        }
        /// <summary>
        /// 合同目标完成情况
        /// </summary>
        protected void CountCoperationTarget()
        {
            CreateTipQuery();
            //初始时间
            CreateTimeQuery();
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            int? unitid;
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);
            if (nodes.Count == 0)
            {
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }

            //查看
            string strWhere = getCountCoperationTargetWhere();

            // this.GridView1.DataSource = new TG.BLL.cm_Coperation().CountCoperationTargetBySql(year, ChargeStartTime, ChargeEndTime, unitid, strWhere);
            //this.GridView1.DataBind();
        }
        /// <summary>
        /// 合同审批情况统计
        /// </summary>
        protected void CountCoperationAudit()
        {
            CreateTipQuery();
            //初始时间
            CreateTimeQuery();
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //if (this.drp_unit.SelectedIndex == 0)
            //{
            //    //全部
            //    if (IsCheckAllPower == "1")
            //    {
            //        unitid = null;
            //    }
            //    else//本部门
            //    {
            //        unitid = UserUnitNo;
            //    }
            //}

            //this.GridView2.DataSource = new TG.BLL.cm_Coperation().CountCoperationAudit(year, ChargeStartTime, ChargeEndTime, unitid);
            //this.GridView2.DataBind();
            //this.GridView2.UseAccessibleHeader = true;
            //this.GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        /// <summary>
        /// 合同类型
        /// </summary>
        protected void CountCoperationType()
        {
            CreateTipQuery();
            //初始时间
            CreateTimeQuery();
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //if (this.drp_unit.SelectedIndex == 0)
            //{
            //    //全部
            //    if (IsCheckAllPower == "1")
            //    {
            //        unitid = null;
            //    }
            //    else//本部门
            //    {
            //        unitid = UserUnitNo;
            //    }
            //}
            ////统计类型
            //int type = int.Parse(this.drp_type.SelectedValue) - 2;


            //    this.GridView4.DataSource = new TG.BLL.cm_Coperation().CountCoperationType(year, ChargeStartTime, ChargeEndTime, unitid, type);
            //    this.GridView4.DataBind();
            //    this.GridView4.UseAccessibleHeader = true;
            //    this.GridView4.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        /// <summary>
        /// 合同等级
        /// </summary>
        protected void CountCoperationLevel()
        {
            CreateTipQuery();
            //初始时间
            CreateTimeQuery();
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //if (this.drp_unit.SelectedIndex == 0)
            //{
            //    //全部
            //    if (IsCheckAllPower == "1")
            //    {
            //        unitid = null;
            //    }
            //    else//本部门
            //    {
            //        unitid = UserUnitNo;
            //    }
            //}

            //this.GridView3.DataSource = new TG.BLL.cm_Coperation().CountCoperationLevel(year, ChargeStartTime, ChargeEndTime, unitid);
            //this.GridView3.DataBind();
            //this.GridView3.UseAccessibleHeader = true;
            //this.GridView3.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        /// <summary>
        /// 合同性质
        /// </summary>
        protected void CountCoperationIndustry()
        {
            CreateTipQuery();
            //初始时间
            CreateTimeQuery();
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //if (this.drp_unit.SelectedIndex == 0)
            //{
            //    //全部
            //    if (IsCheckAllPower == "1")
            //    {
            //        unitid = null;
            //    }
            //    else//本部门
            //    {
            //        unitid = UserUnitNo;
            //    }
            //}
            ////统计类型
            //int type = int.Parse(this.drp_type.SelectedValue) - 6;

            //    this.GridView5.DataSource = new TG.BLL.cm_Coperation().CountCoperationIndustry(year, ChargeStartTime, ChargeEndTime, unitid, type);
            //    this.GridView5.DataBind();
            //    this.GridView5.UseAccessibleHeader = true;
            //    this.GridView5.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        //执行统计
        protected void CountCoperationAll()
        {
            if (this.drp_type.SelectedIndex == 0)
            {
                //显示内容
                ShowCountTypeControl("1");
                //统计
                CountCoperationTarget();
            }
            else if (this.drp_type.SelectedIndex == 1)
            {
                //显示内容
                ShowCountTypeControl("2");
                //统计
                CountCoperationAudit();
            }
            else if (this.drp_type.SelectedIndex == 2)
            {
                //显示内容
                ShowCountTypeControl("3");
                //统计
                CountCoperationLevel();
            }
            else if (this.drp_type.SelectedIndex > 2 && this.drp_type.SelectedIndex <= 6)
            {
                //显示内容
                ShowCountTypeControl("4");
                //统计
                CountCoperationType();
                //单位
                this.div_util4.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);
            }
            else if (this.drp_type.SelectedIndex > 6 && this.drp_type.SelectedIndex <= 10)
            {
                //显示内容
                ShowCountTypeControl("5");
                //统计
                CountCoperationIndustry();
                //单位
                this.div_util5.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);
            }
        }
        //控制对应统计类型
        protected void ShowCountTypeControl(string divtype)
        {
            this.div_Container1.Visible = divtype == "1" ? true : false;
            this.div_Container2.Visible = divtype == "2" ? true : false;
            this.div_Container3.Visible = divtype == "3" ? true : false;
            this.div_Container4.Visible = divtype == "4" ? true : false;
            this.div_Container5.Visible = divtype == "5" ? true : false;
        }
        //选中当前年份
        protected string SelectCurrentYear()
        {
            string year = DateTime.Now.Year.ToString();
            //选择当前年份
            this.drp_year.ClearSelection();
            if (this.drp_year.Items.FindByValue(year) != null)
            {
                this.drp_year.Items.FindByValue(year).Selected = true;
                //统计提示
                CreateTipQuery();
            }
            return year;
        }
        //返回统计单位
        protected string GetUtilByDrpText(string text)
        {
            if (text.IndexOf("数量") > -1)
            {
                return "单位:个";
            }
            else if (text.IndexOf("规模") > -1)
            {
                return "单位:㎡";
            }
            else if (text.IndexOf("额") > -1)
            {
                return "单位:万元";
            }
            else
            {
                return "";
            }
        }
        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            //绑定数据
            CountCoperationAll();
        }

        /// <summary>
        /// 得到合同目标的查询条件
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere()
        {

            StringBuilder sb = new StringBuilder();

            //合同目标值
            string cprTaretAcount1 = txt_CprTaretAcount1.Value;
            string cprTaretAcount2 = txt_CprTaretAcount2.Value;
            //合同目标值
            if (!string.IsNullOrEmpty(cprTaretAcount1) && string.IsNullOrEmpty(cprTaretAcount2))
            {
                sb.Append(" AND cprTarget >= " + cprTaretAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprTaretAcount1) && !string.IsNullOrEmpty(cprTaretAcount2))
            {
                sb.Append(" AND cprTarget <= " + cprTaretAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprTaretAcount1) && !string.IsNullOrEmpty(cprTaretAcount2))
            {
                sb.Append(" AND (cprTarget>= " + cprTaretAcount1.Trim() + " AND cprTarget <= " + cprTaretAcount2.Trim() + " ) ");
            }

            //完成合同额

            string cprAcount1 = txt_CprAcount1.Value;
            string cprAcount2 = txt_CprAcount2.Value;
            if (!string.IsNullOrEmpty(cprAcount1) && string.IsNullOrEmpty(cprAcount2))
            {
                sb.Append(" AND cprAllCount >= " + cprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprAcount1) && !string.IsNullOrEmpty(cprAcount2))
            {
                sb.Append(" AND cprAllCount <= " + cprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprAcount1) && !string.IsNullOrEmpty(cprAcount2))
            {
                sb.Append(" AND (cprAllCount>=" + cprAcount1.Trim() + " AND cprAllCount <= " + cprAcount2.Trim() + " ) ");
            }

            //合同额完成比例

            string cprAcountPercent1 = txt_CprAcountPercent1.Value;
            string cprAcountPercent2 = txt_CprAcountPercent2.Value;
            if (!string.IsNullOrEmpty(cprAcountPercent1) && string.IsNullOrEmpty(cprAcountPercent2))
            {
                sb.Append(" AND Cprprt >= " + cprAcountPercent1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprAcountPercent1) && !string.IsNullOrEmpty(cprAcountPercent2))
            {
                sb.Append(" AND Cprprt <= " + cprAcountPercent2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprAcountPercent1) && !string.IsNullOrEmpty(cprAcountPercent2))
            {
                sb.Append(" AND (Cprprt>=" + cprAcountPercent1.Trim() + " AND Cprprt <= " + cprAcountPercent2.Trim() + " ) ");
            }

            //目标产值
            string cprTarentCharge1 = txt_CprTarentCharge1.Value;
            string cprTarentCharge2 = txt_CprTarentCharge2.Value;
            if (!string.IsNullOrEmpty(cprTarentCharge1) && string.IsNullOrEmpty(cprTarentCharge2))
            {
                sb.Append(" AND AllotTarget >= " + cprTarentCharge1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprTarentCharge1) && !string.IsNullOrEmpty(cprTarentCharge2))
            {
                sb.Append(" AND AllotTarget <= " + cprTarentCharge2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprTarentCharge1) && !string.IsNullOrEmpty(cprTarentCharge2))
            {
                sb.Append(" AND (AllotTarget>=" + cprTarentCharge1.Trim() + " AND AllotTarget <= " + cprTarentCharge2.Trim() + " ) ");
            }

            //完成产值
            string cprCharge1 = txt_CprCharge1.Value;
            string cprCharge2 = txt_CprCharge2.Value;
            if (!string.IsNullOrEmpty(cprCharge1) && string.IsNullOrEmpty(cprCharge2))
            {
                sb.Append(" AND AllotCount >= " + cprCharge1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprCharge1) && !string.IsNullOrEmpty(cprCharge2))
            {
                sb.Append(" AND AllotCount <= " + cprCharge2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprCharge1) && !string.IsNullOrEmpty(cprCharge2))
            {
                sb.Append(" AND (AllotCount>=" + cprCharge1.Trim() + " AND AllotCount <= " + cprCharge2.Trim() + " ) ");
            }

            //完成产值比例
            string cprChargePercent1 = txt_CprChargePercent1.Value;
            string cprChargePercent2 = txt_CprChargePercent2.Value;
            if (!string.IsNullOrEmpty(cprChargePercent1) && string.IsNullOrEmpty(cprChargePercent2))
            {
                sb.Append(" AND AllotPrt >= " + cprChargePercent1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprChargePercent1) && !string.IsNullOrEmpty(cprChargePercent2))
            {
                sb.Append(" AND AllotPrt <= " + cprChargePercent2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprChargePercent1) && !string.IsNullOrEmpty(cprChargePercent2))
            {
                sb.Append(" AND (AllotPrt>=" + cprChargePercent1.Trim() + " AND AllotPrt <= " + cprChargePercent2.Trim() + " ) ");
            }

            return sb.ToString();
        }
        /// <summary>
        /// 获取选中部门ID
        /// </summary>
        /// <returns></returns>
        protected string GetCheckedNodeValue()
        {
            StringBuilder sbUnitlist = new StringBuilder("");
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);

            foreach (ASTreeViewNode node in nodes)
            {
                if (node.NodeValue == "0")
                    continue;

                sbUnitlist.AppendFormat("{0},", node.NodeValue);
            }
            //判断部门不为空
            if (sbUnitlist.ToString() != "")
                sbUnitlist.Remove(sbUnitlist.ToString().LastIndexOf(','), 1);

            return sbUnitlist.ToString();
        }
        //得到部门
        private string getUnitId()
        {
            string strUnit = "";
            //正常查询
          //  if (this.SelectType.Value == "0")
          //  {
                List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);

                bool IsCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;
                //全部的时候
                if (IsCheckAll)
                {
                    //全部
                    if (IsCheckAllPower == "1")
                    {
                        strUnit = "";
                    }
                    else//本部门
                    {
                        strUnit = string.Format(" AND ID IN ({0})", UserUnitNo);
                    }
                }
                else
                {
                    string unitlist = GetCheckedNodeValue();
                    strUnit = string.Format(" AND ( ID in ({0}) ) ", unitlist);
                }
          //  }//自定义查询
         //   else
         //   {
          //      string unit = hiddenUnit.Value;
          //      //全部的时候
           //     if (unit == "-1,")
           //     {
                    //全部
            //        if (IsCheckAllPower == "1")
           //         {
           //             strUnit = "";
          //          }
           //         else//本部门
           //         {
            //            strUnit = " AND ID =" + UserUnitNo + " ";
              //      }
             //   }
             //   else
             //   {
             //       if (!string.IsNullOrEmpty(unit))
             //       {
             //           string unitTemp = unit.Substring(0, unit.Length - 1);
            //            strUnit = " AND ( ID in (" + unitTemp + ") ) ";
             //       }
              //  }
          //  }


            return strUnit;
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string modelPath = " ~/TemplateXls/CoperationCount_Target.xls";
            //统计
            CreateTipQuery();
            //初始时间
            CreateTimeQuery();
           
            //部门
            string strUnit = getUnitId();

            //合同年份区间值
            string startyear = "";
            string endyear = "";
            string sqlwhere = "";        


            //类型判断
            if (this.drp_type.SelectedIndex == 0)
            {
                //显示内容
                ShowCountTypeControl("1");

                string tempYear = DateTime.Now.Year.ToString();
                //合同年份
                string year = this.drp_year.SelectedItem.Value.Trim();
                //合同年份区间段
                if (this.hid_time.Value == "1")
                {
                    startyear = this.txt_year1.Value;
                    endyear = this.txt_year2.Value;
                    if (!string.IsNullOrEmpty(startyear))
                    {
                        sqlwhere = " and cpr_SignDate>='" + startyear + "'";
                        tempYear = Convert.ToDateTime(startyear).Year.ToString();
                    }
                    if (!string.IsNullOrEmpty(endyear))
                    {
                        sqlwhere = sqlwhere+" and cpr_SignDate<='" + endyear + "'";
                        if (string.IsNullOrEmpty(startyear))
                        {
                            tempYear = Convert.ToDateTime(endyear).Year.ToString();
                        }
                    }
                }
                else
                {
                    //不是全年份
                    if (this.drp_year.SelectedIndex > 0)
                    {
                        tempYear = year;
                        string strjidu = this.drpJidu.SelectedValue;
                        string stryue = this.drpMonth.SelectedValue;

                        //年
                        if (strjidu == "0" && stryue == "0")
                        {
                            startyear = year + "-01-01 00:00:00";
                            endyear = year + "-12-31 23:59:59 ";
                        }
                        else if (strjidu != "0" && stryue == "0") //年季度
                        {
                            startyear = year;
                            endyear = year;
                            switch (strjidu)
                            {
                                case "1":
                                    startyear += "-01-01 00:00:00";
                                    endyear += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    startyear += "-04-01 00:00:00";
                                    endyear += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    startyear += "-07-01 00:00:00";
                                    endyear += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    startyear += "-10-01 00:00:00";
                                    endyear += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (strjidu == "0" && stryue != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(year), int.Parse(stryue));
                            startyear = year + "-" + stryue + "-01 00:00:00";
                            endyear = year + "-" + stryue + "-" + days + " 23:59:59";
                        }
                    }
                    else
                    {
                        tempYear = SelectCurrentYear();
                        startyear = tempYear + "-01-01 00:00:00";
                        endyear = tempYear + "-12-31 23:59:59";
                    }

                    if (!string.IsNullOrEmpty(startyear))
                    {
                        sqlwhere = " and cpr_SignDate>='" + startyear + "'";
                    }
                    if (!string.IsNullOrEmpty(endyear))
                {
                        sqlwhere = sqlwhere + " and cpr_SignDate<='" + endyear + "'";
                    }
                }    
               
                string strWhere = "";                
                //正常查询
                if (this.SelectType.Value == "0")
                {
                           
                }//自定义查询
                else
                {
                    strWhere = getCountCoperationTargetWhere();
                }

                strWhere = strWhere + strUnit;

                //统计
                dt = new TG.BLL.cm_Coperation().CountCoperationTargetBySql(tempYear, sqlwhere, ChargeStartTime, ChargeEndTime, strWhere).Tables[0];

                modelPath = " ~/TemplateXls/CoperationCount_Target.xls";
            }
            else if (this.drp_type.SelectedIndex == 1)
            {
                //显示内容
                ShowCountTypeControl("2");
                //统计               
                string strWhere = "";
                string tempYear = "";
                 //正常查询
                if (this.SelectType.Value == "0")
                {
                    if (this.drp_year.SelectedIndex > 0)
                    {
                        tempYear = SqlWhere();
                    }
                }//自定义查询
                else
                {
                     strWhere = getCountCoperationTargetWhere1();
                     //年份
                     string startTime = drp_year2_01.SelectedValue;
                     string endTime = drp_year2_02.SelectedValue;
                            tempYear = yearWhere(startTime, endTime);
                }


                strWhere = strWhere + strUnit;

              

                dt = new TG.BLL.cm_Coperation().CountCoperationAuditBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere).Tables[0];

                modelPath = " ~/TemplateXls/CoperationCount_Audit.xls";
            }
            else if (this.drp_type.SelectedIndex == 2)
            {
                //显示内容
                ShowCountTypeControl("3");
                //统计
                //查看
                 string strWhere = "";
                 string tempYear = "";
                 //正常查询
                 if (this.SelectType.Value == "0")
                 {
                     if (this.drp_year.SelectedIndex > 0)
                     {
                         tempYear = SqlWhere();
                     }
                 }//自定义查询
                 else
                 {
                     strWhere = getCountCoperationTargetWhere2();
                     //年份
                     string startTime = drp_year3_01.SelectedValue;
                     string endTime = drp_year3_02.SelectedValue;
                       tempYear = yearWhere(startTime, endTime);
                 }


                strWhere = strWhere + strUnit;

                dt = new TG.BLL.cm_Coperation().CountCoperationLevelBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere).Tables[0];

                modelPath = " ~/TemplateXls/CoperationCount_Level.xls";
            }
            else if (this.drp_type.SelectedIndex > 2 && this.drp_type.SelectedIndex <= 6)
            {
                //显示内容
                ShowCountTypeControl("4");
                //统计
                //统计类型
                int type = int.Parse(this.drp_type.SelectedValue) - 2;
                //查看
                 string strWhere = "";
                 string tempYear = "";
                 //正常查询
                 if (this.SelectType.Value == "0")
                 {
                     if (this.drp_year.SelectedIndex > 0)
                     {
                         tempYear = SqlWhere();
                     }
                 }//自定义查询
                 else
                 {
                      strWhere = getCountCoperationTargetWhere3();
                      //年份
                      string startTime = drp_year4_01.SelectedValue;
                      string endTime = drp_year4_02.SelectedValue;
                        tempYear = yearWhere(startTime, endTime);
                 }


                strWhere = strWhere + strUnit;

               

                dt = new TG.BLL.cm_Coperation().CountCoperationTypeBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere, type).Tables[0];

                //单位
                this.div_util4.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);

                modelPath = " ~/TemplateXls/CoperationCount_Type.xls";
            }
            else if (this.drp_type.SelectedIndex > 6 && this.drp_type.SelectedIndex <= 10)
            {
                //显示内容
                ShowCountTypeControl("5");
                //统计
                //统计类型
                int type = int.Parse(this.drp_type.SelectedValue) - 6;
                string strWhere = "";
                string tempYear = "";
                 //正常查询
                if (this.SelectType.Value == "0")
                {
                    if (this.drp_year.SelectedIndex > 0)
                    {
                        tempYear = SqlWhere();
                    }
                }//自定义查询
                else
                {
                      strWhere = getCountCoperationTargetWhere4();
                    //年份
                    string startTime = drp_year5_01.SelectedValue;
                    string endTime = drp_year5_02.SelectedValue;
                      tempYear = yearWhere(startTime, endTime);
                }


                strWhere = strWhere + strUnit;

               

                dt = new TG.BLL.cm_Coperation().CountCoperationIndustryBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere, type).Tables[0];

                //单位
                this.div_util5.InnerText = GetUtilByDrpText(this.drp_type.SelectedItem.Text);

                modelPath = " ~/TemplateXls/CoperationCount_Industry.xls";
            }

            DataRow[] drsDel = dt.Select(string.Format("ID in ({0})", base.NotShowUnitList));
            //直接在集合中删除不显示的部门
            foreach (DataRow drDel in drsDel)
            {
                dt.Rows.Remove(drDel);
            }

            //把生产经营部放到最后
            drsDel = dt.Select(string.Format("Name='{0}'", "生产经营部"));
            DataRow dr = dt.NewRow();
            foreach (DataRow drDel in drsDel)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[i] = drDel[i];
                }
                dt.Rows.Remove(drDel);
                //类型判断
                if (this.drp_type.SelectedIndex == 0)
                {
                    dt.Rows.Add(dr);
                }
                else
                {
                    dt.Rows.InsertAt(dr, (dt.Rows.Count - 1));
                }
            }

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //标题样式
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheetAt(0);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            //读取标题
            IRow dataRowTitle = ws.GetRow(0);
            ICell celltitle = dataRowTitle.GetCell(0);

            celltitle.SetCellValue(hiddenTitle.Value);
            //在这里进行Excel分类的切割
            //设计
            DataTable dtsj = dt.Clone();
            //多营
            DataTable dtdj = dt.Clone();
            //增加监理和勘察分开
            DataTable dtkc = dt.Clone();
            DataTable dtjl = dt.Clone();
            //管理部门
            DataTable dtel = dt.Clone();
            //全部部门
            DataTable dtall = dt.Clone();
            //238是设计所的
            //254是多经部门
            //其他的就是管理部门可以
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ID"].ToString() != "500" && dt.Rows[i]["ID"].ToString() != "300")
                {
                    if (unitparenrID(dt.Rows[i]["ID"].ToString()) == "238")
                    {
                        if (dt.Rows[i]["ID"].ToString() == "250")//勘察
                        {
                            dtkc.ImportRow(dt.Rows[i]);
                        }
                        else if (dt.Rows[i]["ID"].ToString() == "253")//监理
                        {
                            dtjl.ImportRow(dt.Rows[i]);
                        }
                        else//其他
                        {
                            dtsj.ImportRow(dt.Rows[i]);
                        }
                    }
                    else if (unitparenrID(dt.Rows[i]["ID"].ToString()) == "254")
                    {
                        dtdj.ImportRow(dt.Rows[i]);
                    }
                    else
                    {
                        if (unitparenrID(dt.Rows[i]["ID"].ToString()) != "0" || dt.Rows[i]["ID"].ToString() == "230")
                        {
                            dtel.ImportRow(dt.Rows[i]);
                        }

                    }
                }
                else
                {
                    dtall.ImportRow(dt.Rows[i]);
                }
            }
            //类型判断
            if (this.drp_type.SelectedIndex == 0)
            {
                //标题合并
                ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 9));

                int row = 2;
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理部门部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                }
                //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                for (int i = 0; i < dtel.Rows.Count; i++)
                {

                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["cpryear"].ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["cprTarget"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["cprAllCount"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["Cprprt"].ToString() + "%");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtel.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtel.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["AllotTarget"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["AllotCount"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("IF(C" + row + "=0,\"\",(D" + row + "/C" + row + ")*100)");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("IF(H" + row + "=0,\"\",(I" + row + "/H" + row + ")*100)");
                }
                //===设计
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计、监理、勘察部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                }

                for (int i = 0; i < dtsj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["cpryear"].ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["cprTarget"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["cprAllCount"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["Cprprt"].ToString() + "%");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtsj.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtsj.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["AllotTarget"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["AllotCount"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("IF(C" + row + "=0,\"\",(D" + row + "/C" + row + ")*100)");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("IF(H" + row + "=0,\"\",(I" + row + "/H" + row + ")*100)");
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //勘察&设计所合计一下
                if (dtkc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtkc.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["cpryear"].ToString());

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["cprTarget"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["cprAllCount"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["Cprprt"].ToString() + "%");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDateTime(dtkc.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDateTime(dtkc.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["AllotTarget"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["AllotCount"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["AllotPrt"].ToString());
                        row = row + 1;
            }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("勘察、设计合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("IF(C" + row + "=0,\"\",(D" + row + "/C" + row + ")*100)");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(9);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("IF(H" + row + "=0,\"\",(I" + row + "/H" + row + ")*100)");
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //最后是监理
                if (dtjl.Rows.Count > 0)
                {
                    for (int i = 0; i < dtjl.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["cpryear"].ToString());

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["cprTarget"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["cprAllCount"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["Cprprt"].ToString() + "%");

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDateTime(dtjl.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDateTime(dtjl.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["AllotTarget"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["AllotCount"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["AllotPrt"].ToString());
                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("主营合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("IF(C" + row + "=0,\"\",(D" + row + "/C" + row + ")*100)");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(9);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("IF(H" + row + "=0,\"\",(I" + row + "/H" + row + ")*100)");

                }
                //多经部门
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多种经营部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                }

                for (int i = 0; i < dtdj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["cpryear"].ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["cprTarget"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["cprAllCount"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["Cprprt"].ToString() + "%");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtdj.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtdj.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotTarget"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多经营所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("IF(H" + row + "=0,\"\",(D" + row + "/C" + row + ")*100)");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("IF(H" + row + "=0,\"\",(I" + row + "/H" + row + ")*100)");
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //全部部门

                if (dtdj.Rows.Count != 0 && dtsj.Rows.Count != 0 && dtel.Rows.Count != 0 && dtkc.Rows.Count != 0 && dtjl.Rows.Count != 0)
                {

                    var dataRowa = ws.GetRow(row);//读行
                    if (dataRowa == null)
                        dataRowa = ws.CreateRow(row);//生成行
                    var cella = dataRowa.CreateCell(0);
                    if (cella == null)
                        cella = dataRowa.CreateCell(0);
                    cella.CellStyle = style2;
                    cella.SetCellValue("全院总合计");

                    cella = dataRowa.CreateCell(2);
                    cella.CellStyle = style2;

                    cella.SetCellFormula("sum(C" + (row - dtdj.Rows.Count - 2) + ",C" + (row) + ",C" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");
                    cella = dataRowa.CreateCell(3);
                    cella.CellStyle = style2;
                    cella.SetCellFormula("sum(D" + (row - dtdj.Rows.Count - 2) + ",D" + (row) + ",D" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");

                    cella = dataRowa.CreateCell(4);
                    cella.CellStyle = style2;
                    cella.SetCellFormula("IF(C" + row + "=0,\"\",(D" + (row + 1) + "/C" + (row + 1) + ")*100)");

                    cella = dataRowa.CreateCell(7);
                    cella.CellStyle = style2;
                    cella.SetCellFormula("sum(H" + (row - dtdj.Rows.Count - 2) + ",H" + (row) + ",H" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");
                    cella = dataRowa.CreateCell(8);
                    cella.CellStyle = style2;
                    cella.SetCellFormula("sum(I" + (row - dtdj.Rows.Count - 2) + ",I" + (row) + ",I" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");

                    cella = dataRowa.CreateCell(9);
                    cella.CellStyle = style2;
                    cella.SetCellFormula("IF(H" + row + "=0,\"\",(I" + (row + 1) + "/H" + (row + 1) + ")*100)");
                    ws.AddMergedRegion(new CellRangeAddress(row, row, 0, 1));
                }
            }
            else if (this.drp_type.SelectedIndex == 1)
            {
                //标题合并
                ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 7));

                int row = 3;
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理部门部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 7));
                }
                //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                for (int i = 0; i < dtel.Rows.Count; i++)
                {

                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["G"].ToString()));
                    row = row + 1;
                }
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                }
                //===设计
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计、监理、勘察部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 7));
                }

                for (int i = 0; i < dtsj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["G"].ToString()));

                    row = row + 1;
                }
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //勘察&设计所合计一下
                if (dtkc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtkc.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["G"].ToString()));

                        row = row + 1;
                }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("勘察、设计合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
            }
                //最后是监理
                if (dtjl.Rows.Count > 0)
                {
                    for (int i = 0; i < dtjl.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["G"].ToString()));

                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("主营合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                }
                //多经部门
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多种经营部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 7));
                }

                for (int i = 0; i < dtdj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["G"].ToString()));
                    //cell = dataRow.CreateCell(8);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多经营所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");
                }
                for (int i = 0; i < dtall.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["G"].ToString()));

                    row = row + 1;
                }

            }
            else if (this.drp_type.SelectedIndex == 2)
            {
                //标题合并
                ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 12));

                int row = 3;
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理部门部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 12));
                }
                //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                for (int i = 0; i < dtel.Rows.Count; i++)
                {

                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A1"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A2"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B1"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B2"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C1"].ToString()));

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C2"].ToString()));
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D1"].ToString()));

                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D2"].ToString()));
                    row = row + 1;
                }
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(J3:J" + (row - 1) + ")");
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(K3:K" + (row - 1) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(L3:L" + (row - 1) + ")");
                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(M3:M" + (row - 1) + ")");
                }
                //===设计
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计、监理、勘察部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 12));
                }

                for (int i = 0; i < dtsj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A1"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A2"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B1"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B2"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C1"].ToString()));

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C2"].ToString()));
                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D1"].ToString()));

                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D2"].ToString()));
                    row = row + 1;
                }
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(J" + (row - dtsj.Rows.Count) + ":J" + (row - 1) + ")");

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(K" + (row - dtsj.Rows.Count) + ":K" + (row - 1) + ")");

                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(L" + (row - dtsj.Rows.Count) + ":L" + (row - 1) + ")");

                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(M" + (row - dtsj.Rows.Count) + ":M" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //勘察&设计所合计一下
                if (dtkc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtkc.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A2"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B2"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C1"].ToString()));

                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C2"].ToString()));

                        cell = dataRow.CreateCell(10);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(11);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D1"].ToString()));
                        cell = dataRow.CreateCell(12);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D2"].ToString()));

                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("勘察、设计合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(9);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(10);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(K" + (row - 2) + ":K" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(11);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(L" + (row - 2) + ":L" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(12);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(M" + (row - 2) + ":M" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //最后是监理
                if (dtjl.Rows.Count > 0)
                {
                    for (int i = 0; i < dtjl.Rows.Count; i++)
                {
                        var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A1"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A2"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B1"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B2"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C1"].ToString()));

                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C2"].ToString()));

                        cell = dataRow.CreateCell(10);
                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["D"].ToString());

                        cell = dataRow.CreateCell(11);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D1"].ToString()));

                        cell = dataRow.CreateCell(12);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D2"].ToString()));

                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("主营合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(9);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(10);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(K" + (row - 2) + ":K" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(11);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(L" + (row - 2) + ":L" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(12);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(M" + (row - 2) + ":M" + (row - 1) + ")");
                }
                //多经部门
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多种经营部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 12));
                }

                for (int i = 0; i < dtdj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A1"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A2"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B1"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B2"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C1"].ToString()));

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C2"].ToString()));

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D1"].ToString()));
                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D2"].ToString()));
                    //cell = dataRow.CreateCell(8);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多经营所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(J" + (row - dtdj.Rows.Count) + ":J" + (row - 1) + ")");

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(K" + (row - dtdj.Rows.Count) + ":K" + (row - 1) + ")");
                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(L" + (row - dtdj.Rows.Count) + ":L" + (row - 1) + ")");

                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(M" + (row - dtdj.Rows.Count) + ":M" + (row - 1) + ")");
                }
                for (int i = 0; i < dtall.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtall.Rows[i]["A"].ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A1"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A2"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B1"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B2"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C1"].ToString()));

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C2"].ToString()));

                    cell = dataRow.CreateCell(10);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(11);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D1"].ToString()));

                    cell = dataRow.CreateCell(12);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D2"].ToString()));

                    row = row + 1;
                }

            }
            else if (this.drp_type.SelectedIndex > 2 && this.drp_type.SelectedIndex <= 6)
            {
                //标题合并
                ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 8));

                //单位
                font1.FontHeightInPoints = 8;//字号                  
                font1.Boldweight = (short)0;
                style1.SetFont(font1);

                IRow TwoRow = ws.GetRow(1);
                ICell TwoCell = TwoRow.GetCell(0);
                ws.AddMergedRegion(new CellRangeAddress(1, 1, 0, 7));

                TwoCell = TwoRow.GetCell(8);
                TwoCell.CellStyle = style1;
                TwoCell.SetCellValue(GetUtilByDrpText(this.drp_type.SelectedItem.Text));


                int row = 3;
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理部门部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 8));
                }
                //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                for (int i = 0; i < dtel.Rows.Count; i++)
                {

                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["H"].ToString()));
                    row = row + 1;
                }
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                }
                //===设计
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计、监理、勘察部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 8));
                }

                for (int i = 0; i < dtsj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["H"].ToString()));

                    row = row + 1;
                }
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //勘察&设计所合计一下
                if (dtkc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtkc.Rows.Count; i++)
                {
                        var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["H"].ToString()));

                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("勘察、设计合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //最后是监理
                if (dtjl.Rows.Count > 0)
                {
                    for (int i = 0; i < dtjl.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["H"].ToString()));

                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("主营合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                }
                //多经部门
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多种经营部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 8));
                }

                for (int i = 0; i < dtdj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["H"].ToString()));
                    //cell = dataRow.CreateCell(8);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多经营所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");
                }
                for (int i = 0; i < dtall.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["H"].ToString()));

                    row = row + 1;
                }
            }
            else if (this.drp_type.SelectedIndex > 6 && this.drp_type.SelectedIndex <= 10)
            {
                //标题合并
                ws.AddMergedRegion(new CellRangeAddress(0, 0, 0, 9));

                //单位
                font1.FontHeightInPoints = 8;//字号                  
                font1.Boldweight = (short)0;
                style1.SetFont(font1);
                IRow TwoRow = ws.GetRow(1);
                ICell TwoCell = TwoRow.GetCell(0);
                ws.AddMergedRegion(new CellRangeAddress(1, 1, 0, 8));

                TwoCell = TwoRow.GetCell(9);
                TwoCell.CellStyle = style1;
                TwoCell.SetCellValue(GetUtilByDrpText(this.drp_type.SelectedItem.Text));

                int row = 3;
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理部门部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                }
                //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列
                for (int i = 0; i < dtel.Rows.Count; i++)
                {

                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["H"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["I"].ToString()));
                    row = row + 1;
                }
                if (dtel.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("管理合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B3:B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E3:E" + (row - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F3:F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G3:G" + (row - 1) + ")");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(J3:J" + (row - 1) + ")");
                }
                //===设计
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计、监理、勘察部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                }

                for (int i = 0; i < dtsj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["H"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["I"].ToString()));

                    row = row + 1;
                }
                if (dtsj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("设计所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtsj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtsj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtsj.Rows.Count) + ":F" + (row - 1) + ")");

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtsj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(J" + (row - dtsj.Rows.Count) + ":J" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //勘察&设计所合计一下
                if (dtkc.Rows.Count > 0)
                {
                    for (int i = 0; i < dtkc.Rows.Count; i++)
                    {
                        var dataRow = ws.GetRow(row);//读行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                        var cell = dataRow.GetCell(0);
                        if (cell == null)
                            cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["I"].ToString()));

                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("勘察、设计合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(9);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");
                    //ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
                }
                //最后是监理
                if (dtjl.Rows.Count > 0)
                {
                    for (int i = 0; i < dtjl.Rows.Count; i++)
                {
                        var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                            dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                        cell.CellStyle = style2;
                        cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                        cell = dataRow.CreateCell(1);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["A"].ToString()));

                        cell = dataRow.CreateCell(2);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["B"].ToString()));

                        cell = dataRow.CreateCell(3);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["C"].ToString()));

                        cell = dataRow.CreateCell(4);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["D"].ToString()));

                        cell = dataRow.CreateCell(5);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["E"].ToString()));

                        cell = dataRow.CreateCell(6);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["F"].ToString()));
                        cell = dataRow.CreateCell(7);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["G"].ToString()));
                        cell = dataRow.CreateCell(8);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["H"].ToString()));
                        cell = dataRow.CreateCell(9);
                        cell.CellStyle = style2;
                        cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["I"].ToString()));

                        row = row + 1;
                    }
                    var dataRowk = ws.GetRow(row);//读行
                    if (dataRowk == null)
                        dataRowk = ws.CreateRow(row);//生成行
                    var cellk = dataRowk.CreateCell(0);
                    if (cellk == null)
                        cellk = dataRowk.CreateCell(0);
                    cellk.CellStyle = style2;
                    cellk.SetCellValue("主营合计");
                    row = row + 1;
                    cellk = dataRowk.CreateCell(1);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(B" + (row - 2) + ":B" + (row - 1) + ")");
                    cellk = dataRowk.CreateCell(2);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(3);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(4);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(E" + (row - 2) + ":E" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(5);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(F" + (row - 2) + ":F" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(6);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(G" + (row - 2) + ":G" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(7);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(8);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                    cellk = dataRowk.CreateCell(9);
                    cellk.CellStyle = style2;
                    cellk.SetCellFormula("sum(J" + (row - 2) + ":J" + (row - 1) + ")");

                }
                //多经部门
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多种经营部分");
                    row = row + 1;
                    ws.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
                }

                for (int i = 0; i < dtdj.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["H"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["I"].ToString()));
                    //cell = dataRow.CreateCell(8);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style2;
                    //cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                if (dtdj.Rows.Count > 0)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue("多经营所合计");
                    row = row + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(B" + (row - dtdj.Rows.Count) + ":B" + (row - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(E" + (row - dtdj.Rows.Count) + ":E" + (row - 1) + ")");

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(F" + (row - dtdj.Rows.Count) + ":F" + (row - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(G" + (row - dtdj.Rows.Count) + ":G" + (row - 1) + ")");

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");

                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellFormula("sum(J" + (row - dtdj.Rows.Count) + ":J" + (row - 1) + ")");
                }
                for (int i = 0; i < dtall.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtall.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["A"].ToString()));

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["B"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["C"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["D"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["E"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["F"].ToString()));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["G"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["H"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtall.Rows[i]["I"].ToString()));
                    row = row + 1;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("合同综合统计按" + CountType + "统计.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }
        /// <summary>
        /// 基本查询的条件
        /// </summary>
        /// <returns></returns>
        private string SqlWhere()
        {
            //合同年份区间值
            string startyear = "";
            string endyear = "";
            string sqlwhere = "";           
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            //合同年份区间段
            if (this.hid_time.Value == "1")
            {
                startyear = this.txt_year1.Value;
                endyear = this.txt_year2.Value;                
            }
            else
            {
                //不是全年份
                if (this.drp_year.SelectedIndex > 0)
                {                    
                    string strjidu = this.drpJidu.SelectedValue;
                    string stryue = this.drpMonth.SelectedValue;

                    //年
                    if (strjidu == "0" && stryue == "0")
                    {
                        startyear = year + "-01-01 00:00:00";
                        endyear = year + "-12-31 23:59:59 ";
                    }
                    else if (strjidu != "0" && stryue == "0") //年季度
                    {
                        startyear = year;
                        endyear = year;
                        switch (strjidu)
                        {
                            case "1":
                                startyear += "-01-01 00:00:00";
                                endyear += "-03-31 23:59:59";
                                break;
                            case "2":
                                startyear += "-04-01 00:00:00";
                                endyear += "-06-30 23:59:59";
                                break;
                            case "3":
                                startyear += "-07-01 00:00:00";
                                endyear += "-09-30 23:59:59";
                                break;
                            case "4":
                                startyear += "-10-01 00:00:00";
                                endyear += "-12-31 23:59:59";
                                break;
                        }
                    }
                    else if (strjidu == "0" && stryue != "0")//年月份
                    {
                        //当月有几天
                        int days = DateTime.DaysInMonth(int.Parse(year), int.Parse(stryue));
                        startyear = year + "-" + stryue + "-01 00:00:00";
                        endyear = year + "-" + stryue + "-" + days + " 23:59:59";
                    }
                }               

                if (!string.IsNullOrEmpty(startyear))
                {
                    sqlwhere = " and cpr_SignDate>='" + startyear + "'";
                }
                if (!string.IsNullOrEmpty(endyear))
                {
                    sqlwhere = sqlwhere + " and cpr_SignDate<='" + endyear + "'";
                }
            }
           
            return sqlwhere;
        }
        public string unitparenrID(string unitid)
        {
            return bll_unit.GetModel(int.Parse(unitid)).unit_ParentID.ToString();
        }
        //得到年份的 查询条件
        /// <summary>
        /// 高级查询年份
        /// </summary>
        /// <param name="startYear"></param>
        /// <param name="endYear"></param>
        /// <returns></returns>
        private string yearWhere(string startYear, string endYear)
        {
            string strWhere = "";

            if (startYear != "-1" && endYear != "-1")
            {
                strWhere = "AND year(cpr_SignDate) >='" + startYear + "' AND  year(cpr_SignDate)<='" + endYear + "'";
            }
            else if (startYear != "-1" && endYear == "-1")
            {
                strWhere = "AND year(cpr_SignDate)>='" + startYear + "'";
            }
            else if (startYear == "-1" && endYear != "-1")
            {
                strWhere = "AND year(cpr_SignDate)<='" + endYear + "'";
            }

            return strWhere;
        }


        /// <summary>
        /// 得到合同审批
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere1()
        {

            string cprCount1 = txtCprCount1.Value;
            string cprCount2 = txtCprCount2.Value;

            string cprSignAcount1 = txtCprSignAcount1.Value;
            string cprSignAcount2 = txtCprSignAcount2.Value;
            string bulid1 = txtBulid1.Value;
            string bulid2 = txtBulid2.Value;
            string chargeCount1 = txtChargeCount1.Value;
            string chargeCount2 = txtChargeCount2.Value;
            string passCount1 = txtPassCount1.Value;
            string passCount2 = txtPassCount2.Value;
            string passingCount1 = txtPassingCount1.Value;
            string passingCount2 = txtPassingCount2.Value;
            string notPassingCount1 = txtNotPassingCount1.Value;
            string notPassingCount2 = txtNotPassingCount2.Value;

            StringBuilder sb = new StringBuilder();

            //已签合同数
            if (!string.IsNullOrEmpty(cprCount1) && string.IsNullOrEmpty(cprCount2))
            {
                sb.Append(" AND A >= " + cprCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprCount1) && !string.IsNullOrEmpty(cprCount2))
            {
                sb.Append(" AND A <= " + cprCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprCount1) && !string.IsNullOrEmpty(cprCount2))
            {
                sb.Append(" AND (A>= " + cprCount1.Trim() + " AND A <= " + cprCount2.Trim() + " ) ");
            }

            //已签合同额

            if (!string.IsNullOrEmpty(cprSignAcount1) && string.IsNullOrEmpty(cprSignAcount2))
            {
                sb.Append(" AND B >= " + cprSignAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprSignAcount1) && !string.IsNullOrEmpty(cprSignAcount2))
            {
                sb.Append(" AND B <= " + cprSignAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprSignAcount1) && !string.IsNullOrEmpty(cprSignAcount2))
            {
                sb.Append(" AND (B>=" + cprSignAcount1.Trim() + " AND B <= " + cprSignAcount2.Trim() + " ) ");
            }

            //建筑规模

            if (!string.IsNullOrEmpty(bulid1) && string.IsNullOrEmpty(bulid2))
            {
                sb.Append(" AND C >= " + bulid1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(bulid1) && !string.IsNullOrEmpty(bulid2))
            {
                sb.Append(" AND C <= " + bulid2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(bulid1) && !string.IsNullOrEmpty(bulid2))
            {
                sb.Append(" AND (C>=" + bulid1.Trim() + " AND C <= " + bulid2.Trim() + " ) ");
            }

            //收款金额

            if (!string.IsNullOrEmpty(chargeCount1) && string.IsNullOrEmpty(chargeCount2))
            {
                sb.Append(" AND D >= " + chargeCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(chargeCount1) && !string.IsNullOrEmpty(chargeCount2))
            {
                sb.Append(" AND D <= " + chargeCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(chargeCount1) && !string.IsNullOrEmpty(chargeCount2))
            {
                sb.Append(" AND (D>=" + chargeCount1.Trim() + " AND D <= " + chargeCount2.Trim() + " ) ");
            }

            //审批合同数
            if (!string.IsNullOrEmpty(passCount1) && string.IsNullOrEmpty(passCount2))
            {
                sb.Append(" AND E >= " + passCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(passCount1) && !string.IsNullOrEmpty(passCount2))
            {
                sb.Append(" AND E <= " + passCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(passCount1) && !string.IsNullOrEmpty(passCount2))
            {
                sb.Append(" AND (E>=" + passCount1.Trim() + " AND E <= " + passCount2.Trim() + " ) ");
            }

            //审批中
            if (!string.IsNullOrEmpty(passingCount1) && string.IsNullOrEmpty(passingCount2))
            {
                sb.Append(" AND F >= " + passingCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(passingCount1) && !string.IsNullOrEmpty(passingCount2))
            {
                sb.Append(" AND F <= " + passingCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(passingCount1) && !string.IsNullOrEmpty(passingCount2))
            {
                sb.Append(" AND (F>=" + passingCount1.Trim() + " AND F <= " + passingCount2.Trim() + " ) ");
            }

            //未通过
            if (!string.IsNullOrEmpty(notPassingCount1) && string.IsNullOrEmpty(notPassingCount2))
            {
                sb.Append(" AND G >= " + notPassingCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(notPassingCount1) && !string.IsNullOrEmpty(notPassingCount2))
            {
                sb.Append(" AND G <= " + notPassingCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(notPassingCount1) && !string.IsNullOrEmpty(notPassingCount2))
            {
                sb.Append(" AND (G>=" + notPassingCount1.Trim() + " AND G <= " + notPassingCount2.Trim() + " ) ");
            }
            return sb.ToString();
        }

        /// <summary>
        /// 得到合同等级
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere2()
        {
            string tJCount1 = txtTJCount1.Value;
            string tJCount2 = txtTJCount2.Value;
            string tJCprAcount1 = txtTJCprAcount1.Value;
            string tJCprAcount2 = txtTJCprAcount2.Value;
            string tJCprChargeAcount1 = txtTJCprChargeAcount1.Value;
            string tJCprChargeAcount2 = txtTJCprChargeAcount2.Value;

            string yJCount1 = txtYJCount1.Value;
            string yJCount2 = txtYJCount2.Value;
            string yJCprAcount1 = txtYJCprAcount1.Value;
            string yJCprAcount2 = txtYJCprAcount2.Value;
            string yJCprChargeAcount1 = txtYJCprChargeAcount1.Value;
            string yJCprChargeAcount2 = txtYJCprChargeAcount1.Value;

            string eJCount1 = txtEJCount1.Value;
            string eJCount2 = txtEJCount2.Value;
            string eJCprAcount1 = txtEJCprAcount1.Value;
            string eJCprAcount2 = txtEJCprAcount2.Value;
            string eJCprChargeAcount1 = txtEJCprChargeAcount1.Value;
            string eJCprChargeAcount2 = txtEJCprChargeAcount1.Value;

            string sJCount1 = txtSJCount1.Value;
            string sJCount2 = txtSJCount2.Value;
            string sJCprAcount1 = txtSJCprAcount1.Value;
            string sJCprAcount2 = txtSJCprAcount2.Value;
            string sJCprChargeAcount1 = txtSJCprChargeAcount1.Value;
            string sJCprChargeAcount2 = txtSJCprChargeAcount1.Value;

            StringBuilder sb = new StringBuilder();

            //特级
            if (!string.IsNullOrEmpty(tJCount1) && string.IsNullOrEmpty(tJCount2))
            {
                sb.Append(" AND A >= " + tJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(tJCount1) && !string.IsNullOrEmpty(tJCount2))
            {
                sb.Append(" AND A <= " + tJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(tJCount1) && !string.IsNullOrEmpty(tJCount2))
            {
                sb.Append(" AND (A>= " + tJCount1.Trim() + " AND A <= " + tJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(tJCprAcount1) && string.IsNullOrEmpty(tJCprAcount2))
            {
                sb.Append(" AND A1 >= " + tJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(tJCprAcount1) && !string.IsNullOrEmpty(tJCprAcount2))
            {
                sb.Append(" AND A1 <= " + tJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(tJCprAcount1) && !string.IsNullOrEmpty(tJCprAcount2))
            {
                sb.Append(" AND (A1>=" + tJCprAcount1.Trim() + " AND A1 <= " + tJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(tJCprChargeAcount1) && string.IsNullOrEmpty(tJCprChargeAcount2))
            {
                sb.Append(" AND A2 >= " + tJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(tJCprChargeAcount1) && !string.IsNullOrEmpty(tJCprChargeAcount2))
            {
                sb.Append(" AND A2 <= " + tJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(tJCprChargeAcount1) && !string.IsNullOrEmpty(tJCprChargeAcount2))
            {
                sb.Append(" AND (A2>=" + tJCprChargeAcount1.Trim() + " AND A2 <= " + tJCprChargeAcount2.Trim() + " ) ");
            }

            //一级
            if (!string.IsNullOrEmpty(yJCount1) && string.IsNullOrEmpty(yJCount2))
            {
                sb.Append(" AND B >= " + yJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(yJCount1) && !string.IsNullOrEmpty(yJCount2))
            {
                sb.Append(" AND B <= " + yJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(yJCount1) && !string.IsNullOrEmpty(yJCount2))
            {
                sb.Append(" AND (B>= " + yJCount1.Trim() + " AND B <= " + yJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(yJCprAcount1) && string.IsNullOrEmpty(yJCprAcount2))
            {
                sb.Append(" AND B1 >= " + yJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(yJCprAcount1) && !string.IsNullOrEmpty(yJCprAcount2))
            {
                sb.Append(" AND B1 <= " + yJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(yJCprAcount1) && !string.IsNullOrEmpty(yJCprAcount2))
            {
                sb.Append(" AND (B1>=" + yJCprAcount1.Trim() + " AND B1 <= " + yJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(yJCprChargeAcount1) && string.IsNullOrEmpty(yJCprChargeAcount2))
            {
                sb.Append(" AND B2 >= " + yJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(yJCprChargeAcount1) && !string.IsNullOrEmpty(yJCprChargeAcount2))
            {
                sb.Append(" AND B2 <= " + yJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(yJCprChargeAcount1) && !string.IsNullOrEmpty(yJCprChargeAcount2))
            {
                sb.Append(" AND (B2>=" + yJCprChargeAcount1.Trim() + " AND B2 <= " + yJCprChargeAcount2.Trim() + " ) ");
            }

            //二级
            if (!string.IsNullOrEmpty(eJCount1) && string.IsNullOrEmpty(eJCount2))
            {
                sb.Append(" AND C >= " + eJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(eJCount1) && !string.IsNullOrEmpty(eJCount2))
            {
                sb.Append(" AND C <= " + eJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(eJCount1) && !string.IsNullOrEmpty(eJCount2))
            {
                sb.Append(" AND (C>= " + eJCount1.Trim() + " AND C <= " + eJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(eJCprAcount1) && string.IsNullOrEmpty(eJCprAcount2))
            {
                sb.Append(" AND C1 >= " + eJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(eJCprAcount1) && !string.IsNullOrEmpty(eJCprAcount2))
            {
                sb.Append(" AND C1 <= " + eJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(eJCprAcount1) && !string.IsNullOrEmpty(eJCprAcount2))
            {
                sb.Append(" AND (C1>=" + eJCprAcount1.Trim() + " AND C1 <= " + eJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(eJCprChargeAcount1) && string.IsNullOrEmpty(eJCprChargeAcount2))
            {
                sb.Append(" AND C2 >= " + eJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(eJCprChargeAcount1) && !string.IsNullOrEmpty(eJCprChargeAcount2))
            {
                sb.Append(" AND C2 <= " + eJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(eJCprChargeAcount1) && !string.IsNullOrEmpty(eJCprChargeAcount2))
            {
                sb.Append(" AND (C2>=" + eJCprChargeAcount1.Trim() + " AND C2 <= " + eJCprChargeAcount2.Trim() + " ) ");
            }

            //三级
            if (!string.IsNullOrEmpty(sJCount1) && string.IsNullOrEmpty(sJCount2))
            {
                sb.Append(" AND D >= " + sJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(sJCount1) && !string.IsNullOrEmpty(sJCount2))
            {
                sb.Append(" AND D <= " + sJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(sJCount1) && !string.IsNullOrEmpty(sJCount2))
            {
                sb.Append(" AND (D>= " + sJCount1.Trim() + " AND A <= " + sJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(sJCprAcount1) && string.IsNullOrEmpty(sJCprAcount2))
            {
                sb.Append(" AND D1 >= " + sJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(sJCprAcount1) && !string.IsNullOrEmpty(sJCprAcount2))
            {
                sb.Append(" AND D1 <= " + sJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(sJCprAcount1) && !string.IsNullOrEmpty(sJCprAcount2))
            {
                sb.Append(" AND (D1>=" + sJCprAcount1.Trim() + " AND D1 <= " + sJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(sJCprChargeAcount1) && string.IsNullOrEmpty(sJCprChargeAcount2))
            {
                sb.Append(" AND D2 >= " + sJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(sJCprChargeAcount1) && !string.IsNullOrEmpty(sJCprChargeAcount2))
            {
                sb.Append(" AND D2 <= " + sJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(sJCprChargeAcount1) && !string.IsNullOrEmpty(sJCprChargeAcount2))
            {
                sb.Append(" AND ( D2>=" + sJCprChargeAcount1.Trim() + " AND D2 <= " + sJCprChargeAcount2.Trim() + " ) ");
            }
            return sb.ToString();
        }

        /// <summary>
        /// 得到合同类型
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere3()
        {
            string type1_01 = txtType1_01.Value;
            string type1_02 = txtType1_02.Value;

            string type2_01 = txtType2_01.Value;
            string type2_02 = txtType2_02.Value;

            string type3_01 = txtType3_01.Value;
            string type3_02 = txtType3_02.Value;

            string type4_01 = txtType4_01.Value;
            string type4_02 = txtType4_02.Value;

            string type5_01 = txtType5_01.Value;
            string type5_02 = txtType5_02.Value;

            string type6_01 = txtType6_01.Value;
            string type6_02 = txtType6_02.Value;

            string type7_01 = txtType7_01.Value;
            string type7_02 = txtType7_02.Value;

            string type8_01 = txtType8_01.Value;
            string type8_02 = txtType8_02.Value;

            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(type1_01) && string.IsNullOrEmpty(type1_02))
            {
                sb.Append(" AND A >= " + type1_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type1_01) && !string.IsNullOrEmpty(type1_02))
            {
                sb.Append(" AND A <= " + type1_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type1_01) && !string.IsNullOrEmpty(type1_02))
            {
                sb.Append(" AND (A>= " + type1_01.Trim() + " AND A <= " + type1_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type2_01) && string.IsNullOrEmpty(type2_02))
            {
                sb.Append(" AND B >= " + type2_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type2_01) && !string.IsNullOrEmpty(type2_02))
            {
                sb.Append(" AND B <= " + type2_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type2_01) && !string.IsNullOrEmpty(type2_02))
            {
                sb.Append(" AND (B>= " + type2_01.Trim() + " AND B <= " + type2_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type3_01) && string.IsNullOrEmpty(type3_02))
            {
                sb.Append(" AND C>= " + type3_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type3_01) && !string.IsNullOrEmpty(type3_02))
            {
                sb.Append(" AND C <= " + type3_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type3_01) && !string.IsNullOrEmpty(type3_02))
            {
                sb.Append(" AND (C>= " + type3_01.Trim() + " AND C <= " + type3_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type4_01) && string.IsNullOrEmpty(type4_02))
            {
                sb.Append(" AND D >= " + type4_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type4_01) && !string.IsNullOrEmpty(type4_02))
            {
                sb.Append(" AND D <= " + type4_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type4_01) && !string.IsNullOrEmpty(type4_02))
            {
                sb.Append(" AND (D>= " + type4_01.Trim() + " AND D <= " + type4_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type5_01) && string.IsNullOrEmpty(type5_02))
            {
                sb.Append(" AND E>= " + type5_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type5_01) && !string.IsNullOrEmpty(type5_02))
            {
                sb.Append(" AND E <= " + type5_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type5_01) && !string.IsNullOrEmpty(type5_02))
            {
                sb.Append(" AND (E>= " + type5_01.Trim() + " AND E <= " + type5_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type6_01) && string.IsNullOrEmpty(type6_02))
            {
                sb.Append(" AND F >= " + type6_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type6_01) && !string.IsNullOrEmpty(type6_02))
            {
                sb.Append(" AND F <= " + type6_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type6_01) && !string.IsNullOrEmpty(type6_02))
            {
                sb.Append(" AND (F>= " + type6_01.Trim() + " AND F <= " + type6_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type7_01) && string.IsNullOrEmpty(type7_02))
            {
                sb.Append(" AND G >= " + type7_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type7_01) && !string.IsNullOrEmpty(type7_02))
            {
                sb.Append(" AND G <= " + type7_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type7_01) && !string.IsNullOrEmpty(type7_02))
            {
                sb.Append(" AND (G>= " + type7_01.Trim() + " AND G<= " + type7_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type8_01) && string.IsNullOrEmpty(type8_02))
            {
                sb.Append(" AND H >= " + type8_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type8_01) && !string.IsNullOrEmpty(type8_02))
            {
                sb.Append(" AND H <= " + type8_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type8_01) && !string.IsNullOrEmpty(type8_02))
            {
                sb.Append(" AND (H>= " + type8_01.Trim() + " AND H <= " + type8_02.Trim() + " ) ");
            }
            return sb.ToString();
        }


        /// <summary>
        /// 得到合同性质
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere4()
        {
            string property1_01 = txtProperty1_01.Value;
            string property1_02 = txtProperty1_02.Value;

            string property2_01 = txtProperty2_01.Value;
            string property2_02 = txtProperty2_02.Value;

            string property3_01 = txtProperty3_01.Value;
            string property3_02 = txtProperty3_02.Value;

            string property4_01 = txtProperty4_01.Value;
            string property4_02 = txtProperty4_02.Value;

            string property5_01 = txtProperty5_01.Value;
            string property5_02 = txtProperty5_02.Value;

            string property6_01 = txtProperty6_01.Value;
            string property6_02 = txtProperty6_02.Value;

            string property7_01 = txtProperty7_01.Value;
            string property7_02 = txtProperty7_02.Value;

            string property8_01 = txtProperty8_01.Value;
            string property8_02 = txtProperty8_02.Value;

            string property9_01 = txtProperty9_01.Value;
            string property9_02 = txtProperty9_02.Value;

            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(property1_01) && string.IsNullOrEmpty(property1_02))
            {
                sb.Append(" AND A >= " + property1_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property1_01) && !string.IsNullOrEmpty(property1_02))
            {
                sb.Append(" AND A <= " + property1_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property1_01) && !string.IsNullOrEmpty(property1_02))
            {
                sb.Append(" AND (A>= " + property1_01.Trim() + " AND A <= " + property1_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property2_01) && string.IsNullOrEmpty(property2_02))
            {
                sb.Append(" AND B >= " + property2_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property2_01) && !string.IsNullOrEmpty(property2_02))
            {
                sb.Append(" AND B <= " + property2_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property2_01) && !string.IsNullOrEmpty(property2_02))
            {
                sb.Append(" AND (B>= " + property2_01.Trim() + " AND B <= " + property2_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property3_01) && string.IsNullOrEmpty(property3_02))
            {
                sb.Append(" AND C >= " + property3_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property3_01) && !string.IsNullOrEmpty(property3_02))
            {
                sb.Append(" AND C <= " + property3_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property3_01) && !string.IsNullOrEmpty(property3_02))
            {
                sb.Append(" AND (C>= " + property3_01.Trim() + " AND C <= " + property3_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property4_01) && string.IsNullOrEmpty(property4_02))
            {
                sb.Append(" AND D >= " + property4_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property4_01) && !string.IsNullOrEmpty(property4_02))
            {
                sb.Append(" AND D <= " + property4_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property4_01) && !string.IsNullOrEmpty(property4_02))
            {
                sb.Append(" AND (D>= " + property4_01.Trim() + " AND D <= " + property4_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property5_01) && string.IsNullOrEmpty(property5_02))
            {
                sb.Append(" AND E >= " + property5_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property5_01) && !string.IsNullOrEmpty(property5_02))
            {
                sb.Append(" AND E <= " + property5_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property5_01) && !string.IsNullOrEmpty(property5_02))
            {
                sb.Append(" AND (E>= " + property5_01.Trim() + " AND E <= " + property5_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property6_01) && string.IsNullOrEmpty(property6_02))
            {
                sb.Append(" AND F >= " + property6_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property6_01) && !string.IsNullOrEmpty(property6_02))
            {
                sb.Append(" AND F <= " + property6_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property6_01) && !string.IsNullOrEmpty(property6_02))
            {
                sb.Append(" AND (F>= " + property6_01.Trim() + " AND F <= " + property6_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property7_01) && string.IsNullOrEmpty(property7_02))
            {
                sb.Append(" AND G >= " + property7_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property7_01) && !string.IsNullOrEmpty(property7_02))
            {
                sb.Append(" AND G<= " + property7_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property7_01) && !string.IsNullOrEmpty(property7_02))
            {
                sb.Append(" AND (G>= " + property7_01.Trim() + " AND G <= " + property7_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property8_01) && string.IsNullOrEmpty(property8_02))
            {
                sb.Append(" AND H >= " + property8_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property8_01) && !string.IsNullOrEmpty(property8_02))
            {
                sb.Append(" AND H <= " + property8_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property8_01) && !string.IsNullOrEmpty(property8_02))
            {
                sb.Append(" AND (H>= " + property8_01.Trim() + " AND H <= " + property8_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property9_01) && string.IsNullOrEmpty(property9_02))
            {
                sb.Append(" AND I >= " + property9_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property9_01) && !string.IsNullOrEmpty(property9_02))
            {
                sb.Append(" AND I <= " + property9_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property9_01) && !string.IsNullOrEmpty(property9_02))
            {
                sb.Append(" AND (I>= " + property9_01.Trim() + " AND I <= " + property9_02.Trim() + " ) ");
            }
            return sb.ToString();
        }

    }
}