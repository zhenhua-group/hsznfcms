﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationAndProjectCountRpt : PageBase
    {
        public string xAxis { get; set; }

        public string yAxis { get; set; }

        public string LegendData { get; set; }

        public string SeriesData { get; set; }

        /// <summary>
        /// 统计语言
        /// </summary>
        public string CountTip
        {
            get;
            set;
        }
        /// <summary>
        /// 表格数据
        /// </summary>
        public string HtmlTable { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectedCurYear();
                LoadUnitTree();
                SelectedCurUnit();
                //默认加载
                GetAllData();
            }
        }
        /// <summary>
        /// 绑定多选年
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();

            if (list.Count > 0)
            {
                //初始化树控件
                foreach (string str in list)
                {
                    this.drpYear.Items.Add(new ListItem(str, str));
                }
            }
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void SelectedCurYear()
        {
            string curYear = DateTime.Now.Year.ToString();
            this.drpYear.ClearSelection();
            //如果当前年不存在
            if (this.drpYear.Items.FindByText(curYear) != null)
            {
                this.drpYear.Items.FindByValue(curYear).Selected = true;
            }
            else
            {
                if (this.drpYear.Items.FindByValue((DateTime.Now.Year - 1).ToString()) != null)
                {
                    this.drpYear.Items.FindByValue((DateTime.Now.Year - 1).ToString()).Selected = true;
                }
            }
        }

        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 是否检查权限
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //判断是否是全部查看
        protected string IsCheckAllPower
        {
            get
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";
            if (base.RolePowerParameterEntity != null)
            {
                //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
        }

        /// <summary>
        /// 统计数据
        /// </summary>
        protected void GetAllData()
        {
            //组织话术
            GetCountCondition();
            //获取数据
            GetCountDataByUnit();

            //获取Legend数据
            GetLegendValue();
            //获取X轴数据
            GetxAxisValue();
            //获取条数据
            GetSeriesData();
            //设置坐标
            SetyAxisData();
        }

        /// <summary>
        /// 统计提示
        /// </summary>
        protected void GetCountCondition()
        {
            string strTip = "";
            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            if (IsCheckAllPower == "1")
            {
                if (nodes.Count == 0)
                {
                    strTip += "全院部门";
                }
                else
                {
                    foreach (ASTreeViewNode node in nodes)
                    {
                        if (node.NodeValue == "0")
                        {
                            strTip += "全院部门";
                            break;
                        }

                        strTip += "[" + node.NodeText.Trim() + "]";
                    }
                }
            }
            else
            {
                strTip = new TG.BLL.tg_unit().GetModel(UserUnitNo).unit_Name.Trim();
            }

            //季度
            if (this.drpJidu.SelectedValue != "0")
            {
                strTip += "第" + this.drpJidu.SelectedItem.Text;
            }
            //月份
            if (this.drpMonth.SelectedValue != "0")
            {
                strTip += this.drpMonth.SelectedItem.Text;
            }
            strTip += "合同信息与项目信息统计表";
            this.CountTip = strTip;
        }

        /// <summary>
        /// 统计部门数据
        /// </summary>
        protected void GetCountDataByUnit()
        {
            StringBuilder sbTable = new StringBuilder();

            List<ASTreeViewNode> checkedNods = this.drp_unit.GetCheckedNodes(false);

            //全没选
            if (checkedNods.Count == 0)
            {
                //循环部门
                DataTable dt = GetUnit();
                //部门名称

                foreach (DataRow dr in dt.Rows)
                {
                    string unitname = dr["unit_Name"].ToString();
                    sbTable.Append(CreateTableRowByUnit(unitname));
                }
            }
            else
            {
                foreach (ASTreeViewNode node in checkedNods)
                {
                    if (node.NodeValue == "0")
                        continue;
                    //部门名称
                    string unitname = node.NodeText.Trim();
                    sbTable.Append(CreateTableRowByUnit(unitname));
                }
            }


            HtmlTable = sbTable.ToString();

            //保存导出列表对象
            ViewState["ExcelList"] = this.ImportExcelList;
        }

        /// <summary>
        /// 创建统计行
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        private string CreateTableRowByUnit(string unitname)
        {
            StringBuilder sbTable = new StringBuilder();

            DataTable dtData = GetCountData(unitname);
            DataRow drData = dtData.Rows[0];
            sbTable.Append("<tr>");
            sbTable.AppendFormat("<td>{0}</td>", unitname);//生产部门
            sbTable.AppendFormat("<td>{0}</td>", drData["CustCount"].ToString());//客户
            sbTable.AppendFormat("<td>{0}</td>", drData["CprCount"].ToString());//合同
            sbTable.AppendFormat("<td>{0}</td>", drData["ProjCount"].ToString());//项目
            sbTable.AppendFormat("<td>{0}</td>", drData["NoPlanCount"].ToString());//未策划
            sbTable.AppendFormat("<td>{0}</td>", drData["PlanCount"].ToString());//已策划
            sbTable.Append("</tr>");

            //创建对象列表
            TG.Model.CoperationAndProjectCountExcel excel = new TG.Model.CoperationAndProjectCountExcel()
            {
                UnitName = unitname,
                CustCount = drData["CustCount"].ToString(),
                CprCount = drData["CprCount"].ToString(),
                ProjCount = drData["ProjCount"].ToString(),
                NoProjPlanCount = drData["NoPlanCount"].ToString(),
                ProjPlanCount = drData["PlanCount"].ToString()
            };

            this.ImportExcelList.Add(excel);

            return sbTable.ToString();
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        protected DataTable GetCountData(string unitname)
        {
            StringBuilder sbSql = new StringBuilder();
            //客户
            StringBuilder sbSqlCust = new StringBuilder();
            //合同
            StringBuilder sbSqlCop = new StringBuilder();
            //项目
            StringBuilder sbSqlProj = new StringBuilder();
            //年
            string stryear = this.drpYear.SelectedValue;
            //季度
            string strjidu = this.drpJidu.SelectedValue;
            //月
            string stryue = this.drpMonth.SelectedValue;

            //全部收款
            if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
            {
                //客户
                sbSqlCust.AppendFormat(" AND year(InsertDate)={0} ", stryear);
                //合同
                sbSqlCop.AppendFormat(" AND year(cpr_SignDate)={0} ", stryear);
                //项目
                sbSqlProj.AppendFormat(" AND year(pro_startTime)={0} ", stryear);
            }
            else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
            {
                string start = stryear;
                string end = stryear;
                switch (strjidu)
                {
                    case "1":
                        start += "-01-01 00:00:00";
                        end += "-3-31 23:59:59";
                        break;
                    case "2":
                        start += "-04-01 00:00:00";
                        end += "-6-30 23:59:59";
                        break;
                    case "3":
                        start += "-7-01 00:00:00";
                        end += "-9-30 23:59:59";
                        break;
                    case "4":
                        start += "-10-01 00:00:00";
                        end += "-12-31 23:59:59";
                        break;
                }
                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, end);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
            }
            else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
            {
                //当月有几天
                int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                string start = stryear + "-" + stryue + "-01 00:00:00";
                string end = stryear + "-" + stryue + "-" + days + " 00:00:00";

                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, end);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
            }
            else
            {
                //默认当年收款
                string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, curtime);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, curtime);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, curtime);

            }

            //执行查询语句
            sbSql.AppendFormat(@"Select
	                            (
		                            Select COUNT(*)
		                            From cm_CustomerInfo
		                            Where InsertUserID IN (
								                            Select mem_ID
								                            From tg_member
								                            Where mem_Unit_ID=U.unit_ID
							        ) {0}
	                            ) AS CustCount
	                            ,(
		                            Select COUNT(*)
		                            From cm_Coperation 
		                            Where cpr_Unit=	U.unit_Name {1}
	                            ) AS CprCount
	                            ,(
		                            Select COUNT(*)
		                            From cm_Project 
		                            Where Unit=U.unit_Name {2}
	                            ) AS ProjCount
	                            ,(
		                            Select COUNT(*)
		                            From cm_Project P
		                            Where Unit=U.unit_Name AND (
									                            Select COUNT(*)
									                            From cm_ProjectPlanAudit
									                            Where ProjectSysNo= P.pro_ID
		                            )=0 {2}
	                            ) AS NoPlanCount 
	                            ,(
		                            Select COUNT(*)
		                            From cm_Project P
		                            Where Unit=U.unit_Name AND (
									                            Select COUNT(*)
									                            From cm_ProjectPlanAudit
									                            Where ProjectSysNo= P.pro_ID
		                            )>0 {2}
	                            ) AS PlanCount
                            From tg_unit U 
                            Where U.unit_Name ='{3}'", sbSqlCust.ToString(), sbSqlCop.ToString(), sbSqlProj.ToString(), unitname);

            DataTable dt = DBUtility.DbHelperSQL.Query(sbSql.ToString()).Tables[0];
            return dt;
        }
        /// <summary>
        /// 计算收款
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        private string GetCountDataByYear(string type)
        {
            //横向坐标
            //统计值
            StringBuilder sbyAxis = new StringBuilder();
            sbyAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    sbyAxis.Append(GetCountDataByUnit(unitname, type) + ",");
                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    sbyAxis.Append(GetCountDataByUnit(unitname, type) + ",");
                }
            }
            //y坐标
            sbyAxis.Remove(sbyAxis.ToString().LastIndexOf(','), 1);
            sbyAxis.Append("]");

            yAxis = sbyAxis.ToString();

            return yAxis;
        }

        /// <summary>
        /// 获取统计数据
        /// </summary>
        private void GetSeriesData()
        {
            StringBuilder sbSeries = new StringBuilder();


            //如果没有选中年按当年计算
            if (this.drpYear.SelectedIndex == 0)
            {
                SelectedCurYear();
            }
            //年

            string checkYear = this.drpYear.SelectedValue;
            //全选
            if (checkYear == "0")
                checkYear = DateTime.Now.Year.ToString();
            //客户
            string strData = GetCountDataByYear("1");
            sbSeries.AppendFormat(@"{{name:'客户数量({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData);

            //合同
            strData = GetCountDataByYear("2");
            sbSeries.AppendFormat(@"{{name:'合同数量({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData);
            //项目
            strData = GetCountDataByYear("3");
            sbSeries.AppendFormat(@"{{name:'项目数量({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData);
            strData = GetCountDataByYear("4");
            //已策划
            sbSeries.AppendFormat(@"{{name:'策划项目数({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData);
            //未策划
            strData = GetCountDataByYear("5");
            sbSeries.AppendFormat(@"{{name:'未策划项目数({0}年)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", checkYear, strData);

            sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
            //返回数据
            SeriesData = sbSeries.ToString();


        }

        /// <summary>
        ///  获取X坐标数据
        /// </summary>
        private void GetxAxisValue()
        {
            //横向坐标
            StringBuilder sbxAxis = new StringBuilder();

            sbxAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                int index = 0;
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    //X坐标数据
                    if (index % 2 == 0)
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                    else
                    {
                        sbxAxis.Append("\"\\n" + unitname + "\",");
                    }

                    index++;
                }

                index = 0;
            }
            else
            {
                int index = 0;
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    if (nodes.Count >= 10)
                    {

                        if (index % 2 == 0)
                        {
                            sbxAxis.Append("\"" + unitname + "\",");
                        }
                        else
                        {
                            sbxAxis.Append("\"\\n" + unitname + "\",");
                        }
                        index++;
                    }
                    else
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                }

                index = 0;
            }
            //x坐标
            sbxAxis.Remove(sbxAxis.ToString().LastIndexOf(','), 1);
            sbxAxis.Append("]");

            xAxis = sbxAxis.ToString();
        }
        /// <summary>
        /// 获取Legend数据
        /// </summary>
        private void GetLegendValue()
        {
            //范围图
            StringBuilder legendData = new StringBuilder();
            //数据图例
            legendData.Append("[");

            //如果年份一个都没有选中则默认当年
            string curyear = this.drpYear.SelectedValue;
            if (this.drpYear.SelectedValue == "0")
            {
                curyear = DateTime.Now.Year.ToString();
            }
            //统计项
            legendData.AppendFormat("\"{0}\",", "客户数量(" + curyear + "年)");
            legendData.AppendFormat("\"{0}\",", "合同数量(" + curyear + "年)");
            legendData.AppendFormat("\"{0}\",", "项目数量(" + curyear + "年)");
            legendData.AppendFormat("\"{0}\",", "策划项目数(" + curyear + "年)");
            legendData.AppendFormat("\"{0}\",", "未策划项目数(" + curyear + "年)");
            //x坐标
            legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
            legendData.Append("]");

            LegendData = legendData.ToString();
        }
        /// <summary>
        /// 查询时间
        /// </summary>
        /// <param name="unitid"></param>
        /// <returns></returns>
        protected string GetCountDataByUnit(string unitname, string type)
        {
            StringBuilder sbSql = new StringBuilder();
            //客户
            StringBuilder sbSqlCust = new StringBuilder();
            //合同
            StringBuilder sbSqlCop = new StringBuilder();
            //项目
            StringBuilder sbSqlProj = new StringBuilder();

            //年
            string stryear = this.drpYear.SelectedValue;
            //季度
            string strjidu = this.drpJidu.SelectedValue;
            //月
            string stryue = this.drpMonth.SelectedValue;

            //全部收款
            if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
            {
                //客户
                sbSqlCust.AppendFormat(" AND year(InsertDate)={0} ", stryear);
                //合同
                sbSqlCop.AppendFormat(" AND year(cpr_SignDate)={0} ", stryear);
                //项目
                sbSqlProj.AppendFormat(" AND year(pro_startTime)={0} ", stryear);
            }
            else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
            {
                string start = stryear;
                string end = stryear;
                switch (strjidu)
                {
                    case "1":
                        start += "-01-01 00:00:00";
                        end += "-3-31 23:59:59";
                        break;
                    case "2":
                        start += "-04-01 00:00:00";
                        end += "-6-30 23:59:59";
                        break;
                    case "3":
                        start += "-7-01 00:00:00";
                        end += "-9-30 23:59:59";
                        break;
                    case "4":
                        start += "-10-01 00:00:00";
                        end += "-12-31 23:59:59";
                        break;
                }
                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, end);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
            }
            else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
            {
                //当月有几天
                int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                string start = stryear + "-" + stryue + "-01 00:00:00";
                string end = stryear + "-" + stryue + "-" + days + " 00:00:00";

                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, end);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
            }
            else
            {
                //默认当年收款
                string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, curtime);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, curtime);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, curtime);

            }
            //按客户
            if (type == "1")
            {
                sbSql.AppendFormat(@"Select
	                            (
		                            Select COUNT(*)
		                            From cm_CustomerInfo
		                            Where InsertUserID IN (
								                            Select mem_ID
								                            From tg_member
								                            Where mem_Unit_ID=U.unit_ID
							        ) {0}
	                            ) AS CustCount
	                            
                            From tg_unit U 
                            Where U.unit_Name ='{1}'", sbSqlCust.ToString(), unitname);


            }
            else if (type == "2")
            {
                sbSql.AppendFormat(@"Select
	                            (
		                            Select COUNT(*)
		                            From cm_Coperation 
		                            Where cpr_Unit=	U.unit_Name {0}
	                            ) AS CprCount
	                            
                            From tg_unit U 
                            Where U.unit_Name ='{1}'", sbSqlCop.ToString(), unitname);
            }
            else if (type == "3")
            {
                sbSql.AppendFormat(@"Select
	                            (
		                            Select COUNT(*)
		                            From cm_Project 
		                            Where Unit=U.unit_Name {0}
	                            ) AS ProjCount
	                            
                            From tg_unit U 
                            Where U.unit_Name ='{1}'", sbSqlProj.ToString(), unitname);
            }
            else if (type == "4")
            {
                sbSql.AppendFormat(@"Select
	                            (
		                            Select COUNT(*)
		                            From cm_Project P
		                            Where Unit=U.unit_Name AND (
									                            Select COUNT(*)
									                            From cm_ProjectPlanAudit
									                            Where ProjectSysNo= P.pro_ID
		                            )=0 {0}
	                            ) AS NoPlanCount 
	                            
                            From tg_unit U 
                            Where U.unit_Name ='{1}'", sbSqlProj.ToString(), unitname);


            }
            else if (type == "5")
            {
                sbSql.AppendFormat(@"Select
	                            (
		                            Select COUNT(*)
		                            From cm_Project P
		                            Where Unit=U.unit_Name AND (
									                            Select COUNT(*)
									                            From cm_ProjectPlanAudit
									                            Where ProjectSysNo= P.pro_ID
		                            )>0 {0}
	                            ) AS PlanCount
                            From tg_unit U 
                            Where U.unit_Name ='{1}'", sbSqlProj.ToString(), unitname);

            }

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(sbSql.ToString()));

            return result;
        }
        /// <summary>
        /// Y轴显示
        /// </summary>
        private void SetyAxisData()
        {
            StringBuilder sbyAxis = new StringBuilder();

            sbyAxis.Append(@"{
                            type : 'value',
                            name : '个数',
                            axisLabel : {
                                formatter: '{value} 个'}
                        }");

            yAxis = sbyAxis.ToString();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetAllData();
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcelByData();
        }

        /// <summary>
        /// 导出列表列表集合
        /// </summary>
        protected List<TG.Model.CoperationAndProjectCountExcel> ImportExcelList =
            new List<TG.Model.CoperationAndProjectCountExcel>();
        /// <summary>
        /// 导出Excel
        /// </summary>
        protected void ExportExcelByData()
        {
            GetCountCondition();
            //导出列表
            List<TG.Model.CoperationAndProjectCountExcel> excellist = ViewState["ExcelList"] as List<TG.Model.CoperationAndProjectCountExcel>;
            //模板路径
            string pathModel = "~/TemplateXls/CoperationAndProjectCountTlt.xls";

            //导出Excel
            ExportDataToExcel(excellist, pathModel, this.drpYear.SelectedValue);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listSC"></param>
        /// <param name="modelPath"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        private void ExportDataToExcel(List<TG.Model.CoperationAndProjectCountExcel> excellist, string modelPath, string year)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);
            //表格第一行标题
            ws.GetRow(0).GetCell(0).SetCellValue(CountTip);
            //第二行标题
            ws.GetRow(1).GetCell(5).SetCellValue(DateTime.Now.ToString("yyyy年MM月dd日") + "制表");

            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            //第四行
            int index = 3;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            //ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            if (excellist.Count > 0)
            {
                foreach (TG.Model.CoperationAndProjectCountExcel item in excellist)
                {
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    WriteExcelValue(cell0, item.UnitName);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    WriteExcelValue(cell1, item.CustCount);
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    WriteExcelValue(cell2, item.CprCount);
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    WriteExcelValue(cell3, item.ProjCount);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    WriteExcelValue(cell4, item.ProjPlanCount); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    WriteExcelValue(cell5, item.NoProjPlanCount); cell5.CellStyle = style;


                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(this.CountTip.ToString() + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        /// <summary>
        /// 写单元格
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="value"></param>
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}