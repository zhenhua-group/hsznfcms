﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Data;
using Geekees.Common.Controls;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationChargeListBymaster : PageBase
    {
        TG.BLL.cm_CoperationCharge bll_chg = new TG.BLL.cm_CoperationCharge();

        protected string WarningDay
        {
            get
            {
                return ConfigurationManager.AppSettings["WarningDay"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //绑定树形部门
                LoadUnitTree();
                //绑定年份
                BindYear();
                //初始化时间设置
                InitDate();
                //默认选中当前年份
                // SelectCurrentYear();
                //默认选中全部
                SelectedCurUnit();
                //绑定合同
                BindCoperation();
            }
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drpunit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drpunit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drpunit.Theme = macOS;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示部门
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
            this.Chk_unit.DataSource = bll_unit.GetList(strWhere);
            this.Chk_unit.DataTextField = "unit_Name";
            this.Chk_unit.DataValueField = "unit_ID";
            this.Chk_unit.DataBind();

        }
        //选中当前年份
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            if (this.drp_year1.Items.FindByText(curyear) != null)
            {
                this.drp_year1.Items.FindByText(curyear).Selected = true;
            }
            if (this.drp_year2.Items.FindByText(curyear) != null)
            {
                this.drp_year2.Items.FindByText(curyear).Selected = true;
            }
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_start.Value = str_year + "-01-01";
            this.txt_end.Value = str_year + "-12-31";
            this.txt_year1.Value = str_year + "-01-01";
            this.txt_year2.Value = str_year + "-12-31";
        }
        //绑定合同
        protected void BindCoperation()
        {
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            StringBuilder strWhere = new StringBuilder("");

            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);

            this.hid_where.Value = strWhere.ToString();

            //所有记录数
            //this.AspNetPager1.RecordCount = int.Parse(bll_cpr.GetListPageProcCount(strWhere.ToString()).ToString());
            //排序
            // string orderString = " Order by " + OrderColumn + " " + Order;
            // strWhere.Append(orderString);

            // this.GridView1.DataSource = bll_cpr.GetListByPageProc(strWhere.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            // this.GridView1.DataBind();
        }
        /// <summary>
        /// 获取所有选中部门名称
        /// </summary>
        /// <returns></returns>
        public string GetCheckedNodesName()
        {
            StringBuilder sbUnitlist = new StringBuilder("");
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);

            foreach (ASTreeViewNode node in nodes)
            {
                if (node.NodeValue == "0")
                    continue;

                sbUnitlist.AppendFormat("'{0}',", node.NodeText);
            }
            //判断部门不为空
            if (sbUnitlist.ToString() != "")
                sbUnitlist.Remove(sbUnitlist.ToString().LastIndexOf(','), 1);

            return sbUnitlist.ToString();
        }
        public void btn_export_Click(object sender, EventArgs e)
        {
            string filetitle = "";
            //正常查询
            string unit1 = GetCheckedNodesName();
            string year = this.drp_year.SelectedValue;
            string startyear = this.txt_year1.Value;
            string endyear = this.txt_year2.Value;
            string cbxtimetype = this.hid_time.Value;
            string jidu = this.drpJidu.SelectedValue;
            string month = this.drpMonth.SelectedValue;
           

            //自定义查询
            string unit = this.HiddenUnit.Value;
            string year1 = this.drp_year1.SelectedValue;
            string year2 = this.drp_year2.SelectedValue;
            string keyname = this.txt_cprName.Value;
            string txtproAcount1 = this.txtproAcount.Value;
            string txtproAcount2 = this.txtproAcount2.Value;
            string projcharge1 = this.projcharge1.Value;
            string projcharge2 = this.projcharge2.Value;
            string txtprogress = this.txtprogress.Value;
            string txtprogress2 = this.txtprogress2.Value;
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            StringBuilder builder = new StringBuilder();
            StringBuilder strsql = new StringBuilder("");
            StringBuilder strWhere = new StringBuilder("");

            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);
            //基本查询
            if (SelectType.Value == "0")
            {
                //合同时间段 
                if (cbxtimetype == "1")
                {
                    string curtime = DateTime.Now.Year.ToString();
                    if (startyear == "" && endyear != "")
                    {
                        builder.AppendFormat(" AND cpr_SignDate<='{0}'", endyear.Trim());
                    }
                    else if (startyear != "" && endyear == "")
                    {
                        builder.AppendFormat(" AND cpr_SignDate>='{0}'", startyear.Trim());
                    }
                    else if (startyear != "" && endyear != "")
                    {
                        builder.AppendFormat(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", startyear.Trim(), endyear.Trim());
                    }
                }
                else
                {
                    //年份
                    if (!string.IsNullOrEmpty(year) && year != "-1")
                    {
                        //年
                        string stryear = year;
                        //季度
                        string strjidu = jidu;
                        //月
                        string stryue = month;
                        //
                        string start = "";
                        string end = "";
                        //年
                        if (strjidu == "0" && stryue == "0")
                        {
                            start = stryear + "-01-01 00:00:00";
                            end = stryear + "-12-31 23:59:59 ";
                        }
                        else if (strjidu != "0" && stryue == "0") //年季度
                        {
                            start = stryear;
                            end = stryear;
                            switch (strjidu)
                            {
                                case "1":
                                    start += "-01-01 00:00:00";
                                    end += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    start += "-04-01 00:00:00";
                                    end += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    start += "-07-01 00:00:00";
                                    end += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    start += "-10-01 00:00:00";
                                    end += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (strjidu == "0" && stryue != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                            start = stryear + "-" + stryue + "-01 00:00:00";
                            end = stryear + "-" + stryue + "-" + days + " 23:59:59";
                        }
                        builder.AppendFormat(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", start.Trim(), end.Trim());

                    }
                }
                if (!string.IsNullOrEmpty(unit1) && !unit1.Contains("全院部门"))
                {
                    if (unit1.Trim() != "-1" && !string.IsNullOrEmpty(unit1.Trim()))
                    {
                        builder.AppendFormat(" AND (cpr_Unit in ({0}))", unit1.Trim());
                    }
                }
            }
            else
            {
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
                {
                    unit = unit.Substring(0, unit.Length - 1);
                    if (unit.Trim() != "-1" && !string.IsNullOrEmpty(unit.Trim()))
                    {
                        builder.AppendFormat(" AND (cpr_Unit in ({0})) ", unit.Trim());
                    }
                }

                //年份
                if (!string.IsNullOrEmpty(year1) && year1 != "-1" && (string.IsNullOrEmpty(year2) || year2 == "-1"))
                {
                    builder.AppendFormat(" AND year(cpr_SignDate)>={0} ", year1.Trim());
                }
                else if ((string.IsNullOrEmpty(year1) || year1 == "-1") && !string.IsNullOrEmpty(year2) && year2 != "-1")
                {
                    builder.AppendFormat(" AND year(cpr_SignDate)<={0} ", year2.Trim());
                }
                else if (!string.IsNullOrEmpty(year1) && year1 != "-1" && !string.IsNullOrEmpty(year2) && year2 != "-1")
                {
                    builder.AppendFormat(" AND year(cpr_SignDate) BETWEEN {0} and {1} ", year1.Trim(), year2.Trim());
                }            

                //合同额1
                //合同额2
                if (!string.IsNullOrEmpty(txtproAcount1) && string.IsNullOrEmpty(txtproAcount2))
                {
                    builder.Append(" AND cpr_Acount >= " + txtproAcount1.Trim() + " ");

                }
                else if (string.IsNullOrEmpty(txtproAcount1) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    builder.Append(" AND cpr_Acount <= " + txtproAcount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtproAcount1) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    builder.Append(" AND (cpr_Acount>=" + txtproAcount1.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
                }
                //合同收费
                if (!string.IsNullOrEmpty(projcharge1) && string.IsNullOrEmpty(projcharge2))
                {
                    builder.Append(" AND ssze >= " + projcharge1.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(projcharge1) && !string.IsNullOrEmpty(projcharge2))
                {
                    builder.Append(" AND ssze <= " + projcharge2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(projcharge1) && !string.IsNullOrEmpty(projcharge2))
                {
                    builder.Append(" AND (ssze>=" + projcharge1.Trim() + " AND ssze <= " + projcharge2.Trim() + ") ");
                }
                //收费进度
                if (!string.IsNullOrEmpty(txtprogress) && string.IsNullOrEmpty(txtprogress2))
                {
                    builder.Append(" AND sfjd >= " + txtprogress.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtprogress) && !string.IsNullOrEmpty(txtprogress2))
                {
                    builder.Append(" AND sfjd <= " + txtprogress2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtprogress) && !string.IsNullOrEmpty(txtprogress2))
                {
                    builder.Append(" AND (sfjd>=" + txtprogress.Trim() + " AND sfjd <= " + txtprogress2.Trim() + ") ");
                }
            }
            builder.Append(strWhere);

            StringBuilder sbTime = new StringBuilder();

            //关联收费
            StringBuilder sbCharge = new StringBuilder();
            //收费日期
            if (!string.IsNullOrEmpty(txt_start.Value) && !string.IsNullOrEmpty(txt_end.Value))
            {
                sbTime.Append(" AND (InAcountTime BETWEEN '" + txt_start.Value.Trim() + "' AND '" + txt_end.Value + "') ");
                sbCharge.Append(" Inner JOIN (SELECT DISTINCT CPRID FROM cm_ProjectCharge  WHERE 1=1  AND Status<>'B' " + sbTime.ToString() + "  ) CH   ON  C.cpr_Id=CH.cprID	");

            }
            else if (string.IsNullOrEmpty(txt_start.Value.Trim()) && !string.IsNullOrEmpty(txt_end.Value))
            {
                sbTime.Append(" AND InAcountTime <='" + txt_end.Value + "' ");
                sbCharge.Append(" Inner JOIN (SELECT DISTINCT CPRID FROM cm_ProjectCharge  WHERE 1=1  AND Status<>'B' " + sbTime.ToString() + "  ) CH   ON  C.cpr_Id=CH.cprID	");

            }
            else if (!string.IsNullOrEmpty(txt_start.Value.Trim()) && string.IsNullOrEmpty(txt_end.Value))
            {
                sbTime.Append(" AND InAcountTime >='" + txt_start.Value.Trim() + "' ");
                sbCharge.Append(" Inner JOIN (SELECT DISTINCT CPRID FROM cm_ProjectCharge  WHERE 1=1  AND Status<>'B' " + sbTime.ToString() + "  ) CH   ON  C.cpr_Id=CH.cprID	");
            }
            else
            {
                builder.Append(" AND null is null");
            }

            DataTable dt = bll_cpr.GetScrCoperationListExport(builder.ToString(), sbTime.ToString(), sbCharge.ToString()).Tables[0];

            string modelPath = " ~/TemplateXls/CoperationChargeList.xls";

            ExportExcel(filetitle, dt, modelPath);
        }

        private void ExportExcel(string filetitle, DataTable dt, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，GetSchCoperationListExport则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_No"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Name"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Acount"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ssze"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["sfjd"].ToString() + "%");

                //cell = dataRow.CreateCell(6);
                //cell.CellStyle = style2;
                //string str = "未完成收款";
                //if (dt.Rows[i]["sfjd"].ToString().Trim() == "100.00" || dt.Rows[i]["sfjd"].ToString().Trim() == "100")
                //{
                //    str = "完成收款";
                //}
                //cell.SetCellValue(str);

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(filetitle + "合同收款明细.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }


        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                    this.drp_year1.Items.Add(list[i]);
                    this.drp_year2.Items.Add(list[i]);
                }
            }
        }
    }
}