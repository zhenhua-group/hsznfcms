﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Data;
using System.Text;

namespace TG.Web.LeadershipCockpit
{
    public partial class CoperationChargeYearRpt : PageBase
    {
        public string xAxis { get; set; }

        public string yAxis { get; set; }

        public string LegendData { get; set; }

        public string SeriesData { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectedCurYear();
                LoadUnitTree();
                SelectedCurUnit();
                SetTimeDefaultValue();
                //加载数据
                GetAllData();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 绑定多选年
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();

            if (list.Count > 0)
            {
                ////声明根节点
                //ASTreeViewNode root = this.drpYear.RootNode;
                //ASTreeViewNode firstnode = new ASTreeViewNode("全部", "0");
                //root.AppendChild(firstnode);
                ////初始化树控件
                //foreach (string str in list)
                //{
                //    ASTreeViewNode linknode = new ASTreeViewNode(str, str);
                //    firstnode.AppendChild(linknode);
                //}

                foreach (string item in list)
                {
                    ListItem option = new ListItem(item, item);
                    this.drpYear.Items.Add(option);
                }
            }
        }

        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void SelectedCurYear()
        {
            //string[] curYear = { DateTime.Now.Year.ToString() };
            //this.drpYear.CheckNodes(curYear);

            string curYear = DateTime.Now.Year.ToString();
            if (this.drpYear.Items.FindByText(curYear) != null)
            {
                this.drpYear.Items.FindByValue(curYear).Selected = true;
            }
        }

        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drp_unit.CheckNodes(curUnit, true);
        }

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = " ";
            if (base.RolePowerParameterEntity != null)
            {

                //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
            //this.drpYear.Theme = macOS;
        }
        /// <summary>
        /// 设置时间段文本框默认值
        /// </summary>
        protected void SetTimeDefaultValue()
        {
            this.txt_start.Value = DateTime.Now.Year + "-01-01";
            this.txt_start.Disabled = true;
            this.txt_end.Value = DateTime.Now.ToString("yyyy-MM-dd");
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        protected void GetAllData()
        {
            //加载图例
            GetLegendValue();
            //加载X轴
            GetxAxisValue();
            //加载y轴
            GetyAxisValue();
            //加载数据
            GetSeriesData();
        }
        /// <summary>
        /// 返回图例
        /// </summary>
        /// <returns></returns>
        protected void GetLegendValue()
        {
            //范围图
            StringBuilder legendData = new StringBuilder();
            //数据图例
            legendData.Append("[");
            string title = "产值";
            if (drpType.SelectedValue == "2")
            {
                title = "合同额";
            }

            //年
            //foreach (ASTreeViewNode node in this.drpYear.GetCheckedNodes(false))
            //{
            //    string checkYear = node.NodeValue;
            //    //全选
            //    if (checkYear == "0")
            //        continue;

            //    legendData.AppendFormat("\"未完成{0}({1}年)\",", title, node.NodeValue);
            //    legendData.AppendFormat("\"目标{0}({1}年)\",", title, node.NodeValue);
            //}
            //选中的年
            string curYear = this.drpYear.SelectedValue;
            legendData.AppendFormat("\"未完成{0}({1}年)\",", title, curYear);
            legendData.AppendFormat("\"目标{0}({1}年)\",", title, curYear);


            legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
            legendData.Append("]");

            LegendData = legendData.ToString();
        }
        /// <summary>
        /// 返回X坐标
        /// </summary>
        private void GetxAxisValue()
        {
            //横向坐标
            StringBuilder sbxAxis = new StringBuilder();

            sbxAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                int index = 0;
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    //X坐标数据
                    if (index % 2 == 0)
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                    else
                    {
                        sbxAxis.Append("\"\\n" + unitname + "\",");
                    }

                    index++;
                }

                index = 0;
            }
            else
            {
                int index = 0;
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    if (nodes.Count >= 10)
                    {

                        if (index % 2 == 0)
                        {
                            sbxAxis.Append("\"" + unitname + "\",");
                        }
                        else
                        {
                            sbxAxis.Append("\"\\n" + unitname + "\",");
                        }
                        index++;
                    }
                    else
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                }

                index = 0;
            }
            //x坐标
            sbxAxis.Remove(sbxAxis.ToString().LastIndexOf(','), 1);
            sbxAxis.Append("]");

            xAxis = sbxAxis.ToString();
        }
        /// <summary>
        /// 获取y坐标
        /// </summary>
        private void GetyAxisValue()
        {
            StringBuilder sbyAxis = new StringBuilder();

            sbyAxis.Append(@"{
                            type : 'value',
                            boundaryGap: [0, 0.1]
                        }");

            yAxis = sbyAxis.ToString();
        }
        /// <summary>
        /// 获取统计数据
        /// </summary>
        private void GetSeriesData()
        {
            StringBuilder sbSeries = new StringBuilder();
            //如果没有选中年按当年计算
            //if (this.drpYear.GetCheckedNodes(false).Count == 0)
            //{
            //    SelectedCurYear();
            //}
            if (this.drpYear.SelectedValue == "0")
            {
                this.drpYear.ClearSelection();
                if (this.drpYear.Items.FindByValue(DateTime.Now.Year.ToString()) != null)
                {
                    this.drpYear.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
                }
                else
                {
                    this.drpYear.Items.FindByValue((DateTime.Now.Year-1).ToString()).Selected = true;
                }
            }
            string title = "产值";
            if (drpType.SelectedValue == "2")
            {
                title = "合同额";
            }
            //年
            //foreach (ASTreeViewNode node in this.drpYear.GetCheckedNodes(false))
            //{
            string checkYear = this.drpYear.SelectedValue;
            //全选
            //if (checkYear == "0")
            //    continue;
            //获取当前完成收款额
            //resulttype 0表示计算当前完成的产值
            //drpType表示计算的类型
            string strData = GetCountDataByYear(checkYear, drpType.SelectedValue, "0");

            sbSeries.AppendFormat(@"{{name:'目标{2}({0}年)',
                                        type:'bar',
                                        stack:'sum',
                                        barCategoryGap:'20%',
                                        itemStyle:{{
                                                    normal: {{
                                                        color: 'tomato',
                                                        barBorderColor: 'tomato',
                                                        barBorderWidth: 3,
                                                        barBorderRadius:0,
                                                        label : {{
                                                            show: true, position: 'insideTop'
                                                        }}
                                                    }}
                                                }},
                                        data:{1}
                                        
                                    }},
                                ", checkYear, strData, title);

            //1表示计算未完成的产值
            string strData2 = GetCountDataByYear(checkYear, drpType.SelectedValue, "1");
            sbSeries.AppendFormat(@"{{name:'未完成{2}({0}年)',
                                        type:'bar',
                                        stack:'sum',
                                        barCategoryGap:'50%',
                                        itemStyle:{{
                                                    normal: {{
                                                        color: '#fff',
                                                        barBorderColor: 'tomato',
                                                        barBorderWidth: 3,
                                                        barBorderRadius:0,
                                                        label : {{
                                                            show: true,
                                                            position: 'top',
                                                            formatter: function (params) {{
                                                                for (var i = 0, l = option.xAxis[0].data.length; i < l; i++) {{
                                                                    if (option.xAxis[0].data[i] == params.name) {{
                                                                        return option.series[0].data[i] + params.value;
                                                                    }}
                                                                }}
                                                            }},
                                                            textStyle: {{
                                                                        color: 'tomato'
                                                                       }}
                                                        }}
                                                    }}
                                                }},
                                        data:{1}
                                        
                                    }},
                                ", checkYear, strData2, title);
            //}

            sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
            //返回数据
            SeriesData = sbSeries.ToString();
        }
        /// <summary>
        /// 获取最后值
        /// </summary>
        /// <param name="year"></param>
        /// <param name="tabletype">目标完成表，0为产值表，1为合同额表</param>
        /// <param name="resulttype"></param>
        /// <returns></returns>
        private string GetCountDataByYear(string year, string tabletype, string resulttype)
        {
            //横向坐标
            //统计值
            StringBuilder sbyAxis = new StringBuilder();
            sbyAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    string unitid = row["unit_ID"].ToString().Trim();

                    if (tabletype == "1")//表示查询产值目标值
                    {
                        //已完成产值
                        string result = GetCprAcountByUnit(unitname, year);
                        decimal doneCount = Convert.ToDecimal(result == "" ? "0" : result);
                        //目标产值
                        string result2 = GetCprAcountTargetByUnit(unitid, year, tabletype);
                        //如果没有设定目标值默认5000
                        if (string.IsNullOrEmpty(result2))
                        {
                            result2 = "3500.00";
                        }
                        decimal target = Convert.ToDecimal(result2);
                        //未完成
                        decimal yue = target - doneCount;
                        //如果超额则taget等于实际完成
                        if (yue <= 0)
                        {
                            yue = 0;
                        }
                        //计算未完成产值“1”为yue
                        if (resulttype == "1")
                        {
                            sbyAxis.Append(yue.ToString() + ",");
                        }
                        else
                        {
                            sbyAxis.Append(doneCount.ToString() + ",");
                        }
                    }
                    else
                    {
                        //已完成合同额
                        string result = GetCprAcountByUnitExt(unitname, year);
                        decimal donecprCount = Convert.ToDecimal(result == "" ? "0" : result);
                        //目标合同额
                        string result2 = GetCprAcountTargetByUnit(unitid, year, tabletype);
                        //如果没有设定目标值默认5000
                        if (string.IsNullOrEmpty(result2))
                        {
                            result2 = "3500.00";
                        }
                        decimal targetcprCount = Convert.ToDecimal(result2);
                        //差额
                        decimal yue = targetcprCount - donecprCount;
                        //如果超额则taget等于实际完成
                        if (yue <= 0)
                        {
                            yue = 0;
                        }
                        //计算结构“1”为yue
                        if (resulttype == "1")
                        {
                            sbyAxis.Append(yue.ToString() + ",");
                        }
                        else
                        {
                            sbyAxis.Append(donecprCount.ToString() + ",");
                        }
                    }
                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    string unitid = nd.NodeValue.Trim();

                    //0为产值
                    if (tabletype == "1")
                    {
                        //已完成产值
                        string result = GetCprAcountByUnit(unitname, year);
                        decimal doneCount = Convert.ToDecimal(result == "" ? "0" : result);
                        //目标产值
                        string result2 = GetCprAcountTargetByUnit(unitid, year, tabletype);
                        //如果没有设置目标值为5000
                        if (string.IsNullOrEmpty(result2))
                        {
                            result2 = "3500.00";
                        }
                        decimal target = Convert.ToDecimal(result2);
                        //计算差额
                        decimal yue = target - doneCount;
                        //如果超额则taget等于实际完成
                        if (yue <= 0)
                        {
                            yue = 0;
                        }

                        //计算结构“1”为yue
                        if (resulttype == "1")
                        {
                            sbyAxis.Append(yue.ToString() + ",");
                        }
                        else
                        {
                            sbyAxis.Append(doneCount.ToString() + ",");
                        }
                    }
                    else
                    {
                        //已完成合同额
                        string result = GetCprAcountByUnitExt(unitname, year);
                        decimal donecprCount = Convert.ToDecimal(result == "" ? "0" : result);
                        //目标合同额
                        string result2 = GetCprAcountTargetByUnit(unitid, year, tabletype);
                        //如果没有设置合同目标值
                        if (string.IsNullOrEmpty(result2))
                        {
                            result2 = "3500.00";
                        }
                        decimal targetcprCount = Convert.ToDecimal(result2);
                        //差额
                        decimal yue = targetcprCount - donecprCount;
                        //如果超额则taget等于实际完成
                        if (yue <= 0)
                        {
                            yue = 0;
                        }

                        //计算结构“1”为yue
                        if (resulttype == "1")
                        {
                            sbyAxis.Append(yue.ToString() + ",");
                        }
                        else
                        {
                            sbyAxis.Append(donecprCount.ToString() + ",");
                        }
                    }

                }
            }
            //y坐标
            sbyAxis.Remove(sbyAxis.ToString().LastIndexOf(','), 1);
            sbyAxis.Append("]");

            return sbyAxis.ToString();
        }
        /// <summary>
        /// 获取产值完成情况
        /// </summary>
        /// <param name="unitname"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        protected string GetCprAcountByUnit(string unitname, string year)
        {
            string strSql = string.Format(@" Select isnull(sum(A.Acount),0)
                               From cm_ProjectCharge A 
                               Left Join cm_Coperation B on A.cprID=B.cpr_Id 
                               Where B.cpr_Unit='{0}' AND Status<>'B' ", unitname);

            //按时间段查询
            if (this.chkTime.Checked)
            {
                string stattime = this.txt_start.Value;
                string endtime = this.txt_end.Value;

                if (stattime.Trim() == "")
                    stattime = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                if (endtime.Trim() == "")
                    endtime = DateTime.Now.ToString("yyyy-MM-dd" + " 23:59:59");

                strSql += string.Format(" AND (A.InAcountTime BETWEEN '{0}' AND '{1}') ", stattime, endtime);
            }
            else
            {
                //年
                string stryear = year;
                //季度
                string strjidu = this.drpJidu.SelectedValue;
                //月
                string stryue = this.drpMonth.SelectedValue;

                //全部收款
                if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
                {
                    strSql += string.Format(" AND year(A.InAcountTime)={0}", stryear);
                }
                else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
                {
                    string start = stryear;
                    string end = stryear;
                    switch (strjidu)
                    {
                        case "1":
                            start += "-01-01 00:00:00";
                            end += "-3-31 23:59:59";
                            break;
                        case "2":
                            start += "-04-01 00:00:00";
                            end += "-6-30 23:59:59";
                            break;
                        case "3":
                            start += "-7-01 00:00:00";
                            end += "-9-30 23:59:59";
                            break;
                        case "4":
                            start += "-10-01 00:00:00";
                            end += "-12-31 23:59:59";
                            break;
                    }
                    strSql += string.Format("AND (A.InAcountTime BETWEEN '{0}' AND '{1}')", start, end);
                }
                else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
                {
                    //当月有几天
                    int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                    string start = stryear + "-" + stryue + "-01 00:00:00";
                    string end = stryear + "-" + stryue + "-" + days + " 00:00:00";
                    strSql += string.Format("AND (A.InAcountTime BETWEEN '{0}' AND '{1}')", start, end);
                }
                else
                {
                    //默认当年收款
                    string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                    string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    strSql += string.Format("AND (A.InAcountTime BETWEEN '{0}' AND '{1}')", start, curtime);
                }
            }

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        /// <summary>
        /// 获取目标额
        /// </summary>
        /// <param name="unitname"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        protected string GetCprAcountTargetByUnit(string unitid, string year, string tabletype)
        {
            string tableName = "cm_UnitAllot";
            //如果是“1” 则为合同目标
            if (tabletype == "2")
            {
                tableName = "cm_UnitCopAllot";
            }
            string strSql = string.Format(@" Select isnull(UnitAllot,0)
                                             From {2}
                                             Where UnitID={0} AND AllotYear='{1}'", unitid, year, tableName);


            //按时间段查询
            if (this.chkTime.Checked)
            {
                string stattime = this.txt_start.Value;
                string endtime = this.txt_end.Value;

                if (stattime.Trim() == "")
                    stattime = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                if (endtime.Trim() == "")
                    endtime = DateTime.Now.ToString("yyyy-MM-dd" + " 23:59:59");

                strSql = string.Format(@" Select isnull(UnitAllot,0)
                                             From {2}
                                             Where UnitID={0} AND AllotYear=year('{1}')", unitid, stattime, tableName);
            }


            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        /// <summary>
        /// 获取当前签订合同额
        /// </summary>
        /// <param name="unitname"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        protected string GetCprAcountByUnitExt(string unitname, string year)
        {

            string strSql = string.Format(@" Select isnull(SUM(cpr_Acount),0)
                                            From cm_Coperation
                                            Where cpr_Unit='{0}' ", unitname);

            //按时间段查询
            if (this.chkTime.Checked)
            {
                string stattime = this.txt_start.Value;
                string endtime = this.txt_end.Value;

                if (stattime.Trim() == "")
                    stattime = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                if (endtime.Trim() == "")
                    endtime = DateTime.Now.ToString("yyyy-MM-dd" + " 23:59:59");

                strSql += string.Format(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}') ", stattime, endtime);
            }
            else
            {
                //年
                string stryear = year;
                //季度
                string strjidu = this.drpJidu.SelectedValue;
                //月
                string stryue = this.drpMonth.SelectedValue;

                //全部收款
                if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
                {
                    strSql += string.Format(" AND year(cpr_SignDate)={0}", stryear);
                }
                else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
                {
                    string start = stryear;
                    string end = stryear;
                    switch (strjidu)
                    {
                        case "1":
                            start += "-01-01 00:00:00";
                            end += "-3-31 23:59:59";
                            break;
                        case "2":
                            start += "-04-01 00:00:00";
                            end += "-6-30 23:59:59";
                            break;
                        case "3":
                            start += "-7-01 00:00:00";
                            end += "-9-30 23:59:59";
                            break;
                        case "4":
                            start += "-10-01 00:00:00";
                            end += "-12-31 23:59:59";
                            break;
                    }
                    strSql += string.Format("AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                }
                else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
                {
                    //当月有几天
                    int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                    string start = stryear + "-" + stryue + "-01 00:00:00";
                    string end = stryear + "-" + stryue + "-" + days + " 00:00:00";
                    strSql += string.Format("AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                }
                else
                {
                    //默认当年收款
                    string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                    string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    strSql += string.Format("AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, curtime);
                }
            }

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetAllData();
        }
    }
}