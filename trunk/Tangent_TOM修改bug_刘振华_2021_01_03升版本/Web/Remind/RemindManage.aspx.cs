﻿using Aspose.Words.Drawing;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Remind
{
    public partial class RemindManage : PageBase
    {
        public string projectNum = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //绑定数据
                BindData();
                //绑定权限
                BindPreviewPower();

            }
        }
        //绑定年份
        protected void BindYear()
        {
            //List<string> list = new TG.BLL.cm_ProjectNumberInfo().GetProjectNumberInfoYear();
            //if (list != null)
            //{
            //    for (int i = 0; i < list.Count; i++)
            //    {
            //        this.drp_year.Items.Add(list[i]);
            //    }
            //}
        }

        protected void SelectCurrentYear()
        {
            //string curyear = DateTime.Now.Year.ToString();
            //if (this.drp_year.Items.FindByText(curyear) != null)
            //{
            //    this.drp_year.Items.FindByText(curyear).Selected = true;
            //}
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (Inuser =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND UnitID= (Select unit_ID From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            //if (base.RolePowerParameterEntity.PreviewPattern == 0)
            //{
            //    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            //}
            //else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            //{
            //    strWhere = " unit_ID =" + UserUnitNo;
            //}
            //else
            //{
            strWhere = " 1=1 ";
            //  }
            //不显示的单位
            strWhere += " AND unit_ParentID<>0 ";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定数据
        public void BindData()
        {
            TG.BLL.cm_projectNumber bll_num = new TG.BLL.cm_projectNumber();
            StringBuilder sb = new StringBuilder("");
            //检查权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

    }
}