﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string msy = Request.Params["msgtype"];
            if (msy.Trim() == "11")
            {
                this.Msg.Text = "项目审批后申请修改消息已过期";
            }
            else if (msy.Trim() == "12")
            {
                this.Msg.Text = "合同审批后申请修改消息已过期";
            }
            else if (msy.Trim() == "13")
            {
                this.Msg.Text = "项目策划后申请修改消息已过期";
            }
        }
    }
}