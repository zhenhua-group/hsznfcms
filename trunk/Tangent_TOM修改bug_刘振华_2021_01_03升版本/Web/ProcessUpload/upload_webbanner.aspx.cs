﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;

namespace TG.Web.ProcessUpload
{
    public partial class upload_webbanner : System.Web.UI.Page
    {
        public string WebBannerSysNo
        {
            get
            {
                return Request["webBannerSysNo"];
            }
        }

        private string physicalPath = HttpContext.Current.Server.MapPath("/Attach_User/filedata/webbannerfile/{0}");

        protected void Page_Load(object sender, EventArgs e)
        {
            WebBannerAttachBP bp = new WebBannerAttachBP();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                try
                {
                    HttpPostedFile postFile = Request.Files[i];
                    string fileName = postFile.FileName;
                    int fileSize = postFile.ContentLength;
                    string fileExtendName = Path.GetExtension(postFile.FileName);

                    int sameNameFileCount = 0;

                    if (bp.ExistsSameNameByFile(fileName, out sameNameFileCount))
                    {
                        fileName = fileName.Substring(0, fileName.IndexOf('.'));
                        fileName = fileName + "(" + sameNameFileCount + ")" + fileExtendName;
                    }

                    WebBannerAttachViewEntity dataEntity = new WebBannerAttachViewEntity
                    {
                        WebBannerSysNo = WebBannerSysNo,
                        FileName = fileName,
                        FileType = fileExtendName,
                        FileSize = fileSize,
                        IconPath = GetIconType(fileExtendName),
                        RelativePath = string.Format("/Attach_User/filedata/webbannerfile/{0}", fileName)
                    };

                    int sysNo = bp.AddWebBannerAttach(dataEntity);
                    if (sysNo > 0)
                    {
                        postFile.SaveAs(Server.MapPath("/Attach_User/filedata/webbannerfile/" + fileName));
                    }
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
        }

        /// <summary>
        /// 根据后缀名返回文件
        /// </summary>
        /// <param name="extend"></param>
        /// <returns></returns>
        protected string GetIconType(string extend)
        {
            string path = "";
            switch (extend.Trim().ToLower())
            {
                case ".jpg":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".png":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".bmp":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".jpeg":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".accdb":
                    path = "../images/fileicon/accdb.gif";
                    break;
                case ".chm":
                    path = "../images/fileicon/chm.gif";
                    break;
                case ".doc":
                    path = "../images/fileicon/doc.gif";
                    break;
                case ".docx":
                    path = "../images/fileicon/docx.gif";
                    break;
                case ".exe":
                    path = "../images/fileicon/exe.gif";
                    break;
                case "htm":
                    path = "../images/fileicon/htm.gif";
                    break;
                case ".html":
                    path = "../images/fileicon/html.gif";
                    break;
                case ".mdb":
                    path = "../images/fileicon/mdb.gif";
                    break;
                case ".pdf":
                    path = "../images/fileicon/pdf.gif";
                    break;
                case ".ppt":
                    path = "../images/fileicon/ppt.gif";
                    break;
                case ".pptx":
                    path = "../images/fileicon/pptx.gif";
                    break;
                case ".rar":
                    path = "../images/fileicon/rar.gif";
                    break;
                case ".txt":
                    path = "../images/fileicon/txt.gif";
                    break;
                case ".xls":
                    path = "../images/fileicon/xls.gif";
                    break;
                case ".xlsx":
                    path = "../images/fileicon/xlsx.gif";
                    break;
                case ".zip":
                    path = "../images/fileicon/zip.gif";
                    break;
                default:
                    path = "../images/fileicon/unknow.gif";
                    break;
            }

            return path;
        }
    }
}
