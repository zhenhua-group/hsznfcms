﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace TG.Web.ProcessUpload
{
    public partial class upLoad_cpr : System.Web.UI.Page
    {
        //声明实例
        TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
        TG.Model.cm_AttachInfo model_att = new TG.Model.cm_AttachInfo();
        string str_memid;
        protected void Page_Load(object sender, EventArgs e)
        {
            UploadFile();
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        public void UploadFile()
        {
            //无缓存
            Response.CacheControl = "no-cache";
            //关联主表ID
            string str_fileid = Request.QueryString["id"] ?? "";
            //用户ID
            str_memid = Request.QueryString["userid"] ?? "";
            //检查是否登录超时
            if (IsOvertime())
            {
                throw new Exception("登录超时！");
            }
            //开始上传
            if (str_memid != "")
            {
                //获取上传路径
                string up_path = str_fileid + "|";
                //获取文件部分路径
                string dir_path = GetPhysicPath(up_path);
                //获取真实路径
                string s_rpath = CreateDirPathAndRetrun(dir_path);
                string updir = s_rpath;
                if (this.Page.Request.Files.Count > 0)
                {
                    try
                    {
                        for (int j = 0; j < this.Page.Request.Files.Count; j++)
                        {
                            //取得上传文件
                            HttpPostedFile uploadFile = this.Page.Request.Files[j];
                            string str_name = uploadFile.FileName;

                            string str_size = uploadFile.ContentLength.ToString();
                            string str_ext = Path.GetExtension(uploadFile.FileName);
                            //存在同名文件
                            if (IsExsitDirOrFile(str_name))
                            {
                                str_name = str_name.Substring(0, str_name.IndexOf('.'));
                                str_name = str_name + "(" + GetSameFileCount(str_name).ToString() + ")" + str_ext;
                            }
                            //网络下载地址
                            string str_path = dir_path + str_name;
                            if (uploadFile.ContentLength > 0)
                            {
                                //数据库记录
                                bool done = InsertFileData(str_name, str_size, str_ext, str_path, str_fileid);
                                if (done)
                                {
                                    //保存文件
                                    uploadFile.SaveAs(string.Format("{0}\\{1}", updir, str_name));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write("Message" + ex.ToString());
                    }
                }
            }
        }
        /// <summary>
        /// 登录是否超时
        /// </summary>
        /// <returns></returns>
        protected bool IsOvertime()
        {
            //用户
            string memid = Request.QueryString["userid"] ?? "";
            if (memid == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 添加文件数据
        /// </summary>
        /// <param name="name">文件名</param>
        /// <param name="size">文件大小</param>
        /// <param name="fileextend">扩展名</param>
        /// <param name="prtid">登录ID</param>
        /// <returns></returns>
        protected bool InsertFileData(string fname, string fsize, string fextend, string fpath, string cprid)
        {
            //声明返回值
            bool flag = false;

            //赋值
            model_att.Cpr_Id = decimal.Parse(cprid);
            model_att.Cst_Id = -1;
            model_att.Temp_No = cprid;
            model_att.FileName = fname;
            model_att.FileSize = fsize;
            model_att.FileType = fextend;
            model_att.FileTypeImg = GetIconType(fextend);
            model_att.OwnType = "cpr";
            model_att.FileUrl = fpath;
            model_att.UploadTime = DateTime.Now;
            model_att.UploadUser = str_memid;

            int num = bll_att.Add(model_att);
            if (num > 0)
            {
                flag = true;
            }
            //返回结果
            return flag;
        }
        /// <summary>
        /// 是否存在同名文件
        /// </summary>
        /// <param name="name">文件名</param>
        /// <param name="prtid">暂定为登录ID</param>
        /// <returns></returns>
        protected bool IsExsitDirOrFile(string name)
        {
            //返回标示
            bool flag = false;
            string str_where = " FileName='" + name + "' AND OwnType='cpr'";
            TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
            List<TG.Model.cm_AttachInfo> model_atts = bll_att.GetModelList(str_where);
            if (model_atts.Count > 0)
            {
                flag = true;
            }

            //返回值
            return flag;
        }
        /// <summary>
        /// 同名文件个数
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected int GetSameFileCount(string name)
        {
            int i_filecount = 0;
            TG.BLL.cm_AttachInfo bll = new TG.BLL.cm_AttachInfo();
            string strWhere = " FileName like'" + name + "%'  AND OwnType='cpr'";
            List<TG.Model.cm_AttachInfo> models = bll.GetModelList(" ");
            i_filecount = models.Count;
            //返回文件数
            return i_filecount;
        }
        /// <summary>
        /// 根据后缀名返回文件
        /// </summary>
        /// <param name="extend"></param>
        /// <returns></returns>
        protected string GetIconType(string extend)
        {
            string path = "";
            switch (extend.Trim().ToLower())
            {
                case ".jpg":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".png":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".bmp":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".jpeg":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".accdb":
                    path = "../images/fileicon/accdb.gif";
                    break;
                case ".chm":
                    path = "../images/fileicon/chm.gif";
                    break;
                case ".doc":
                    path = "../images/fileicon/doc.gif";
                    break;
                case ".docx":
                    path = "../images/fileicon/docx.gif";
                    break;
                case ".exe":
                    path = "../images/fileicon/exe.gif";
                    break;
                case "htm":
                    path = "../images/fileicon/htm.gif";
                    break;
                case ".html":
                    path = "../images/fileicon/html.gif";
                    break;
                case ".mdb":
                    path = "../images/fileicon/mdb.gif";
                    break;
                case ".pdf":
                    path = "../images/fileicon/pdf.gif";
                    break;
                case ".ppt":
                    path = "../images/fileicon/ppt.gif";
                    break;
                case ".pptx":
                    path = "../images/fileicon/pptx.gif";
                    break;
                case ".rar":
                    path = "../images/fileicon/rar.gif";
                    break;
                case ".txt":
                    path = "../images/fileicon/txt.gif";
                    break;
                case ".xls":
                    path = "../images/fileicon/xls.gif";
                    break;
                case ".xlsx":
                    path = "../images/fileicon/xlsx.gif";
                    break;
                case ".zip":
                    path = "../images/fileicon/zip.gif";
                    break;
                default:
                    path = "../images/fileicon/unknow.gif";
                    break;
            }

            return path;
        }
        /// <summary>
        /// 获取物理存储路径
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public string GetPhysicPath(string paths)
        {
            string path = "";
            if (paths.IndexOf('|') > -1)
            {
                string[] array_path = paths.Split(new char[] { '|' }, StringSplitOptions.None);
                if (array_path.Length > 0)
                {
                    for (int i = array_path.Length - 1; i >= 0; i--)
                    {
                        if (array_path[i].ToString() != "")
                        {
                            path += array_path[i].ToString() + "/";
                        }
                    }
                }
            }
            return path;
        }
        /// <summary>
        /// 创建文件路径
        /// </summary>
        /// <param name="dirpath"></param>
        protected string CreateDirPathAndRetrun(string dirpath)
        {
            string config_dir = "~/Attach_User/filedata/cprfile/" + dirpath;
            string target_dir = Server.MapPath(config_dir);
            if (!ExsitDirectry(target_dir))
            {
                Directory.CreateDirectory(target_dir);
            }
            //返回完整路径
            return target_dir;
        }
        /// <summary>
        /// 判断路径是否存在
        /// </summary>
        /// <param name="path">路径</param>
        /// <returns></returns>
        protected bool ExsitDirectry(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
