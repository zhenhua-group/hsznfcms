﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace TG.Web.ProcessUpload
{
    public partial class upload_excel : System.Web.UI.Page
    {
        //用户id
        string userid = "admin";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UploadFile();
        }
        public void CreateThumb()
        {
            System.Drawing.Image image1 = null;
            System.Drawing.Image image2 = null;
            Bitmap bitmap1 = null;
            Graphics graphics1 = null;
            MemoryStream stream1 = null;
            MemoryStream stream2 = null;
            try
            {
                try
                {
                    int num5;
                    int num6;
                    HttpPostedFile file1 = base.Request.Files["Filedata"];
                    image2 = System.Drawing.Image.FromStream(file1.InputStream);
                    int num1 = image2.Width;
                    int num2 = image2.Height;
                    int num3 = 300;
                    int num4 = 180;
                    float single1 = ((float)num3) / ((float)num4);
                    float single2 = ((float)num1) / ((float)num2);
                    if (single1 > single2)
                    {
                        num6 = num4;
                        num5 = (int)Math.Floor((double)(single2 * num4));
                    }
                    else
                    {
                        num6 = (int)Math.Floor((double)(((float)num3) / single2));
                        num5 = num3;
                    }
                    num5 = (num5 > num3) ? num3 : num5;
                    num6 = (num6 > num4) ? num4 : num6;
                    bitmap1 = new Bitmap(num3, num4);
                    graphics1 = Graphics.FromImage(bitmap1);
                    graphics1.FillRectangle(new SolidBrush(Color.Black), new Rectangle(0, 0, num3, num4));
                    int num7 = (num3 - num5) / 2;
                    int num8 = (num4 - num6) / 2;
                    graphics1.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics1.DrawImage(image2, num7, num8, num5, num6);
                    stream1 = new MemoryStream();
                    bitmap1.Save(stream1, ImageFormat.Jpeg);
                    stream2 = new MemoryStream();
                    image2.Save(stream2, ImageFormat.Jpeg);
                    string text1 = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    Thumbnail thumbnail1 = new Thumbnail(text1, stream2.GetBuffer(), stream1.GetBuffer());
                    List<Thumbnail> list1 = this.Session["file_info"] as List<Thumbnail>;
                    if (list1 == null)
                    {
                        list1 = new List<Thumbnail>();
                        this.Session["file_info"] = list1;
                    }
                    list1.Add(thumbnail1);
                    base.Response.StatusCode = 200;
                    base.Response.Write(text1);
                }
                catch
                {
                    base.Response.StatusCode = 500;
                    base.Response.Write("An error occured");
                    base.Response.End();
                }
            }
            finally
            {
                if (bitmap1 != null)
                {
                    bitmap1.Dispose();
                }
                if (graphics1 != null)
                {
                    graphics1.Dispose();
                }
                if (image2 != null)
                {
                    image2.Dispose();
                }
                if (image1 != null)
                {
                    image1.Dispose();
                }
                if (stream1 != null)
                {
                    stream1.Close();
                }
            }
        }
        public string GetPhysicPath(string paths)
        {
            string text1 = "";
            if (paths.IndexOf('|') > -1)
            {
                string[] textArray1 = paths.Split(new char[] { '|' }, StringSplitOptions.None);
                if (textArray1.Length > 0)
                {
                    for (int num1 = textArray1.Length - 1; num1 >= 0; num1--)
                    {
                        if (textArray1[num1].ToString() != "")
                        {
                            text1 = text1 + textArray1[num1].ToString() + "/";
                        }
                    }
                }
            }
            return text1;
        }
        protected void SaveTumble(string name)
        {
            this.CreateThumb();
            if (this.Session["file_info"] != null)
            {
                List<Thumbnail> list1 = this.Session["file_info"] as List<Thumbnail>;
                List<Thumbnail>.Enumerator enumerator1 = list1.GetEnumerator();
                try
                {
                    while (enumerator1.MoveNext())
                    {
                        Thumbnail thumbnail1 = enumerator1.Current;
                        //string text1 = updir + @"\min\";
                        //if (!this.ExsitDirectry(text1))
                        //{
                        //    Directory.CreateDirectory(text1);
                        //}
                        //FileStream stream1 = new FileStream(text1 + name, FileMode.Create);
                        //BinaryWriter writer1 = new BinaryWriter(stream1);
                        //writer1.Write(thumbnail1.miniData);
                        //writer1.Close();
                        //stream1.Close();
                    }
                }
                finally
                {
                    enumerator1.Dispose();
                }
            }
        }
        protected bool IsOvertime()
        {

            return false;
        }
        public void UploadFile()
        {
            base.Response.CacheControl = "no-cache";
            string text1 = base.Request.QueryString["year"] ?? "";
            string text2 = base.Request.QueryString["month"] ?? "";
            string filenameDemo = "KQ" + text1 + text2;
            if (this.IsOvertime())
            {
                throw new Exception("\u767b\u5f55\u8d85\u65f6\uff01");
            }
            if (text2 != "")
            {
                if (this.Page.Request.Files.Count > 0)
                {
                    try
                    {
                        for (int num1 = 0; num1 < this.Page.Request.Files.Count; num1++)
                        {
                            HttpPostedFile file1 = this.Page.Request.Files[num1];
                            string text7 = file1.FileName;
                            string filenametext = text7.Substring(0, text7.IndexOf('.'));
                            if (filenametext != filenameDemo)
                            {
                                text7 = filenameDemo + ".xls";
                            }
                            text7 = CreateDirPathAndRetrun(text7);
                            string text8 = file1.ContentLength.ToString();
                            string text9 = Path.GetExtension(file1.FileName);
                            if (this.IsExsitDirOrFile(text7))
                            {
                                text7 = text7.Substring(0, text7.IndexOf('.'));
                                string[] textArray1 = new string[5];
                                textArray1[0] = text7;
                                textArray1[1] = "(";
                                int num2 = this.GetSameFileCount(text7);
                                textArray1[2] = num2.ToString();
                                textArray1[3] = ")";
                                textArray1[4] = text9;
                                text7 = string.Concat(textArray1);
                            }
                            string text10 = text7;
                            if ((file1.ContentLength > 0) && this.InsertFileData(text7, text8, text9, text10, text1))
                            {
                                file1.SaveAs(text7);
                                //this.SaveTumble(text7);
                            }
                        }
                    }
                    catch (Exception exception1)
                    {
                        base.Response.Write("Message" + exception1.ToString());
                    }
                }
            }
        }
        protected int GetSameFileCount(string name)
        {
            int num1 = 0;


            return num1;
        }
        protected bool IsExsitDirOrFile(string name)
        {
            bool flag1 = false;
            return flag1;
        }
        protected string CreateDirPathAndRetrun(string dirpath)
        {
            string dir_str = "~/Attach_User/kaoqin/";
            if (!this.ExsitDirectry(Server.MapPath(dir_str)))
            {
                Directory.CreateDirectory(Server.MapPath(dir_str));
            }
            string text2 = dir_str + dirpath;
            string text3 = Server.MapPath(text2);
            return text3;
        }
        protected bool ExsitDirectry(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            return false;
        }
        protected bool InsertFileData(string fname, string fsize, string fextend, string fpath, string projid)
        {
            bool flag1 = false;
            flag1 = true;
            return flag1;
        }

        protected string GetIconType(string extend)
        {
            string path = "";
            switch (extend.Trim().ToLower())
            {
                case ".jpg":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".png":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".bmp":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".gif":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".jpeg":
                    path = "../images/fileicon/jpg.png";
                    break;
                case ".accdb":
                    path = "../images/fileicon/accdb.gif";
                    break;
                case ".chm":
                    path = "../images/fileicon/chm.gif";
                    break;
                case ".doc":
                    path = "../images/fileicon/doc.gif";
                    break;
                case ".docx":
                    path = "../images/fileicon/docx.gif";
                    break;
                case ".exe":
                    path = "../images/fileicon/exe.gif";
                    break;
                case "htm":
                    path = "../images/fileicon/htm.gif";
                    break;
                case ".html":
                    path = "../images/fileicon/html.gif";
                    break;
                case ".mdb":
                    path = "../images/fileicon/mdb.gif";
                    break;
                case ".pdf":
                    path = "../images/fileicon/pdf.gif";
                    break;
                case ".ppt":
                    path = "../images/fileicon/ppt.gif";
                    break;
                case ".pptx":
                    path = "../images/fileicon/pptx.gif";
                    break;
                case ".rar":
                    path = "../images/fileicon/rar.gif";
                    break;
                case ".txt":
                    path = "../images/fileicon/txt.gif";
                    break;
                case ".xls":
                    path = "../images/fileicon/xls.gif";
                    break;
                case ".xlsx":
                    path = "../images/fileicon/xlsx.gif";
                    break;
                case ".zip":
                    path = "../images/fileicon/zip.gif";
                    break;
                default:
                    path = "../images/fileicon/unknow.gif";
                    break;
            }

            return path;
        }
    }
    /// <summary>
    /// Summary description for Thumbnail
    /// </summary>
    public class Thumbnail
    {
        public Thumbnail(string id, byte[] data, byte[] minidata)
        {
            this.ID = id;
            this.Data = data;
            this.miniData = minidata;
        }

        private string id;
        public string ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        private byte[] max_data;
        public byte[] Data
        {
            get
            {
                return this.max_data;
            }
            set
            {
                this.max_data = value;
            }
        }

        private byte[] thumbnail_data;
        public byte[] miniData
        {
            get
            {
                return this.thumbnail_data;
            }
            set
            {
                this.thumbnail_data = value;
            }
        }
    }
}
