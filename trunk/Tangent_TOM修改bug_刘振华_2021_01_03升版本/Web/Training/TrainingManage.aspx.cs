﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;

namespace TG.Web.Training
{
    public partial class TrainManage : PageBase
    {

        TG.BLL.Training _training = new TG.BLL.Training();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                BindMonth();
                //人员信息
                BindMem();
                //部门信息
                BindUnit();
        
            }
        }

        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "1=1";
            ////如果只能查看个人数据
            //if (base.RolePowerParameterEntity.PreviewPattern == 0)
            //{
            //    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            //}
            //else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            //{
            //    strWhere = " unit_ID =" + UserUnitNo;
            //}
            //else
            //{
            //    strWhere = " 1=1 ";
            //}
            //不显示的单位
            strWhere += " AND unit_ParentID<>0";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.Training().GetTrainingYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        //绑定年份
        protected void BindMonth()
        {
            List<string> list = new TG.BLL.Training().GetTrainingMonth();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_month.Items.Add(list[i]);
                }
            }
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="gridView"></param>
       /// <param name="cols">起始列,从0开始</param>
       /// <param name="sRow">开始行</param>
       /// <param name="eRow">结束行</param>
        public static void GroupCol(GridView gridView)//row行,合并某一列的某些行
        {

               for (int j = 0; j < gridView.Rows.Count; j++)
               {
                   TableCell oldtc13 = gridView.Rows[j].Cells[13];
                   decimal t13 = Convert.ToDecimal(gridView.Rows[j].Cells[12].Text);
                   oldtc13.Text = t13.ToString();
                   for (int k = j + 1; k < gridView.Rows.Count; k++)
                   {
                       TableCell oldtc = gridView.Rows[j].Cells[1];
                       TableCell tc = gridView.Rows[k].Cells[1];

                       TableCell oldtc2 = gridView.Rows[j].Cells[2];
                       TableCell tc2 = gridView.Rows[k].Cells[2];

                       TableCell oldtc3 = gridView.Rows[j].Cells[3];
                       TableCell tc3 = gridView.Rows[k].Cells[3];

                       TableCell oldtc4 = gridView.Rows[j].Cells[4];
                       TableCell tc4 = gridView.Rows[k].Cells[4];

                       TableCell oldtc5 = gridView.Rows[j].Cells[5];
                       TableCell tc5 = gridView.Rows[k].Cells[5];

                       TableCell tc12 = gridView.Rows[k].Cells[12];

                       //TableCell oldtc13 = gridView.Rows[j].Cells[13];
                       TableCell tc13 = gridView.Rows[k].Cells[13];

                       TableCell oldtc14 = gridView.Rows[j].Cells[14];
                       TableCell tc14 = gridView.Rows[k].Cells[14];

                       string t = oldtc.Text;
                       string t1 = tc.Text;
                       if (t == t1)
                       {

                           tc.Visible = false;
                           tc2.Visible = false;
                           tc3.Visible = false;
                           tc4.Visible = false;
                           tc5.Visible = false;
                           tc13.Visible = false;
                           tc14.Visible = false;


                           if (oldtc.RowSpan == 0)
                           {
                               oldtc.RowSpan = 1;
                               oldtc2.RowSpan = 1;
                               oldtc3.RowSpan = 1;
                               oldtc4.RowSpan = 1;
                               oldtc5.RowSpan = 1;
                               oldtc13.RowSpan = 1;
                               oldtc14.RowSpan = 1;
                             
                           }
                           oldtc.RowSpan++;
                           oldtc2.RowSpan++;
                           oldtc3.RowSpan++;
                           oldtc4.RowSpan++;
                           oldtc5.RowSpan++;
                           oldtc13.RowSpan++;
                           t13 += Convert.ToDecimal(tc12.Text);
                           oldtc13.Text = t13.ToString();
                           oldtc14.RowSpan++;
                
                       }
                       else
                       {
                           
                           j = k - 1;
                           break;
                       }
                   }
               }

        }



        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }

        //绑定角色信息
        protected void BindMem()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.property.Text != "-1")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.property.Text);
                sb.AppendFormat("and (t.property='{0}') ", keyname);
            }
            if (this.drp_year.Text != "-1")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.drp_year.Text);
                sb.AppendFormat("and (YEAR(t.BeginDate)={0}) ", keyname);
            }
            if (this.drp_month.Text != "-1")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.drp_month.Text);
                sb.AppendFormat("and (MONTH(t.BeginDate)={0}) ", keyname);
            }


            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat("and (t.Title Like '%{0}%' or t.Title Like '%{0}%') ", keyname);
            }

            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND detail.UnitName='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }

            this.hid_where.Value = sb.ToString();
            //角色数量
            this.AspNetPager1.RecordCount = int.Parse(_training.GetRecordCount(sb.ToString()).ToString());
            //this.AspNetPager1.
            //绑定
            this.grid_mem.DataSource = _training.GetListByPage(sb.ToString(), "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize *this.AspNetPager1.CurrentPageIndex);
            this.grid_mem.DataBind();
           GroupCol(this.grid_mem);
          
        }



        //删除角色
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            for (int i = 0; i < this.grid_mem.Rows.Count; i++)
            {
                CheckBox chk = this.grid_mem.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                string str_memid = this.grid_mem.Rows[i].Cells[1].Text;
                if (chk.Checked)
                {
                    bll_mem.Delete(int.Parse(str_memid));
                }
            }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "用户信息删除成功！", "Sys_MemBymaster.aspx");

        }


        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    //单位
            //    TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            //    string str_unitid = e.Row.Cells[6].Text;
            //    if (str_unitid != "")
            //    {
            //        TG.Model.tg_unit model_unit = bll_unit.GetModel(int.Parse(str_unitid));
            //        if (model_unit != null)
            //        {
            //            e.Row.Cells[6].Text = model_unit.unit_Name.ToString().Trim();
            //        }
            //    }
            //    //角色
            //    TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();
            //    string str_priid = e.Row.Cells[8].Text;
            //    if (str_priid != "")
            //    {
            //        TG.Model.tg_principalship model_pri = bll_pri.GetModel(int.Parse(str_priid));
            //        if (model_pri != null)
            //        {
            //            e.Row.Cells[8].Text = model_pri.pri_Name.ToString().Trim();
            //        }
            //    }
            //    //专业
            //    TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
            //    string str_specid = e.Row.Cells[7].Text;
            //    if (str_specid != "")
            //    {
            //        TG.Model.tg_speciality model_spec = bll_spec.GetModel(int.Parse(str_specid));
            //        if (model_spec != null)
            //        {
            //            e.Row.Cells[7].Text = model_spec.spe_Name.ToString().Trim();
            //        }
            //    }
            //    //职位
            //    TG.BLL.cm_MemArchLevelRelation bll_relation = new TG.BLL.cm_MemArchLevelRelation();
            //    TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
            //    Label lbl_arch = e.Row.Cells[11].FindControl("lbl_archlevel") as Label;
            //    if (lbl_arch != null)
            //    {
            //        List<TG.Model.cm_MemArchLevelRelation> models_relation = bll_relation.GetModelList(" mem_ID=" + lbl_arch.Text);
            //        if (models_relation.Count > 0)
            //        {
            //            //职称
            //            TG.Model.cm_MemArchLevel model_level = bll_level.GetModel(models_relation[0].ArchLevel_ID);
            //            if (model_level != null)
            //            {
            //                lbl_arch.Text = model_level.Name.Trim();
            //                HiddenField hid_archlevelid = e.Row.Cells[11].FindControl("hid_archlevel") as HiddenField;
            //                //赋值
            //                hid_archlevelid.Value = model_level.ArchLevel_ID.ToString();
            //            }

            //        }
            //        else
            //        {
            //            lbl_arch.Text = "无";
            //        }
            //    }
            //    //头像
            //    TG.BLL.cm_EleSign bll_ele = new TG.BLL.cm_EleSign();

            //    HiddenField memid_field = e.Row.Cells[1].FindControl("hidden_memid") as HiddenField;
            //    if (!string.IsNullOrEmpty(memid_field.Value))
            //    {
            //        TG.Model.cm_EleSign model_ele = bll_ele.GetModel2(int.Parse(memid_field.Value));
            //        if (model_ele != null)
            //        {
            //            e.Row.Cells[12].Text = "<a href='" + model_ele.elesign + "'><img src='" + model_ele.elesign + "' onerror=\"this.src='/Images/zw.jpg'\" border=0 width=20 height=20/></a>";
            //            // e.Row.Cells[12].Text = model_ele.elesign;
            //        }

            //    }
            //}
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindMem();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindMem();
        }

        //生产部门改变重新绑定数据
        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            this.AspNetPager1.CurrentPageIndex = 0;
            //绑定数据
            BindMem();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BindMem();
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Response.Redirect("../Training/TrainingStatics.aspx");
        }
    }
}