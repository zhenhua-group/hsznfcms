﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfoSoftGlobal;
using TG.Common;

namespace TG.Web.Training
{
    public partial class TrainingStatics : PageBase
    {

        TG.BLL.Training _training = new TG.BLL.Training();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string beginDate = this.txtBeginDate.Value;
                string endDate = this.txtEndDate.Value;
                TG.BLL.Training bll = new TG.BLL.Training();


                List<TG.Model.Training> lis = bll.GetCount(beginDate, endDate);
                StringBuilder strXML = new StringBuilder();
                strXML.Append(
                    "<chart showPercentInToolTip='1' caption='外部培训月份数量分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='人' baseFontSize='12'>");
                for (int i = 0; i < lis.Count; i++)
                {
                    strXML.AppendFormat("<set label='{0}'  value='{1}' />", lis[i].Year + "年" + lis[i].Month + "月",
                        lis[i].Count);

                }
                strXML.Append("</chart>");
                Literal1.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML.ToString(),
                    "外部培训月份数量分布统计", "100%", "300", false, true, false);



                List<TG.Model.Training> lis1 = bll.GetCount1(beginDate, endDate);
                StringBuilder strXML1 = new StringBuilder();
                strXML1.Append(
                    "<chart  showPercentInToolTip='1' caption='外部培训月份费用分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='元' baseFontSize='12'>");
                for (int i = 0; i < lis1.Count; i++)
                {
                    strXML1.AppendFormat("<set label='{0}'  value='{1}' />", lis1[i].Year + "年" + lis1[i].Month + "月",
                        lis1[i].Totals);

                }
                strXML1.Append("</chart>");
                Literal2.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML1.ToString(),
                    "外部培训月份费用分布统计", "100%", "300", false, true);


            }
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            string beginDate = this.txtBeginDate.Value;
            string endDate = this.txtEndDate.Value;
            TG.BLL.Training bll = new TG.BLL.Training();


            List<TG.Model.Training> lis = bll.GetCount(beginDate, endDate);
            StringBuilder strXML = new StringBuilder();
            strXML.Append(
                "<chart showPercentInToolTip='1' caption='外部培训月份数量分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='人' baseFontSize='12'>");
            for (int i = 0; i < lis.Count; i++)
            {
                strXML.AppendFormat("<set label='{0}'  value='{1}' />", lis[i].Year + "年" + lis[i].Month + "月",
                    lis[i].Count);

            }
            strXML.Append("</chart>");
            Literal1.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML.ToString(),
                "外部培训月份数量分布统计", "100%", "300", false, true, false);



            List<TG.Model.Training> lis1 = bll.GetCount1(beginDate, endDate);
            StringBuilder strXML1 = new StringBuilder();
            strXML1.Append(
                "<chart showPercentInToolTip='1' caption='外部培训月份费用分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='元' baseFontSize='12'>");
            for (int i = 0; i < lis1.Count; i++)
            {
                strXML1.AppendFormat("<set label='{0}'  value='{1}' />", lis1[i].Year + "年" + lis1[i].Month + "月",
                    lis1[i].Totals);

            }
            strXML1.Append("</chart>");
            Literal2.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML1.ToString(),
                "外部培训月份费用分布统计", "100%", "300", false, true);

        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            string beginDate = this.txtBeginDate.Value;
            string endDate = this.txtEndDate.Value;
            TG.BLL.Training bll = new TG.BLL.Training();


            List<TG.Model.Training> lis = bll.GetCount2(beginDate, endDate);
            StringBuilder strXML = new StringBuilder();
            strXML.Append(
                "<chart showPercentInToolTip='1' caption='外部培训部门数量分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='人' baseFontSize='12'>");
            for (int i = 0; i < lis.Count; i++)
            {
                strXML.AppendFormat("<set label='{0}'  value='{1}' />", lis[i].UnitName.Trim(), lis[i].Count);

            }
            strXML.Append("</chart>");
            Literal1.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML.ToString(),
                "外部培训部门数量分布统计", "100%", "300", false, true, false);


            List<TG.Model.Training> lis1 = bll.GetCount3(beginDate, endDate);
            StringBuilder strXML1 = new StringBuilder();
            strXML1.Append("<chart showPercentInToolTip='1' caption='外部培训部门费用分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='元' baseFontSize='12'>");
            for (int i = 0; i < lis1.Count; i++)
            {
                strXML1.AppendFormat("<set label='{0}'  value='{1}' />", lis1[i].UnitName.Trim(), lis1[i].Totals);

            }
            strXML1.Append("</chart>");
            Literal2.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML1.ToString(),
                "外部培训部门费用分布统计", "100%", "300", false, true);






        }
        protected void Button3_Click(object sender, EventArgs e)
        {

            string beginDate = this.txtBeginDate.Value;
            string endDate = this.txtEndDate.Value;
            TG.BLL.Training bll = new TG.BLL.Training();

            List<TG.Model.Training> lis = bll.GetCount4(beginDate, endDate);
            StringBuilder strXML = new StringBuilder();
            strXML.Append(
                "<chart showPercentInToolTip='1' caption='外部培训员工数量分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='次' baseFontSize='12'>");
            for (int i = 0; i < lis.Count; i++)
            {
                strXML.AppendFormat("<set label='{0}'  value='{1}' />", lis[i].CanYuRen, lis[i].Count);

            }
            strXML.Append("</chart>");
            Literal1.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML.ToString(),
                "外部培训员工数量分布统计", "100%", "300", false, true, false);


            List<TG.Model.Training> lis1 = bll.GetCount5(beginDate, endDate);
            StringBuilder strXML1 = new StringBuilder();
            strXML1.Append("<chart showPercentInToolTip='1' caption='外部培训员工费用分布统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='元' baseFontSize='12'>");
            for (int i = 0; i < lis1.Count; i++)
            {
                strXML1.AppendFormat("<set label='{0}'  value='{1}' />", lis1[i].CanYuRen, lis1[i].Totals);

            }
            strXML1.Append("</chart>");
            Literal2.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML1.ToString(), "外部培训员工费用分布统计", "100%", "300", false, true, false);



        }
        protected void Button4_Click(object sender, EventArgs e)
        {
            string beginDate = this.txtBeginDate.Value;
            string endDate = this.txtEndDate.Value;
            TG.BLL.Training bll = new TG.BLL.Training();
            List<TG.Model.Training> lis = bll.GetCount6(beginDate, endDate);



            //组合统计xml数据
            StringBuilder xmlData = new StringBuilder();
            StringBuilder categories = new StringBuilder();
            StringBuilder currentYear = new StringBuilder();
            StringBuilder previousYear = new StringBuilder();
            //统计条形图

            xmlData.Append("<chart caption='外部培训部门覆盖率' formatNumberScale='0'  baseFontSize='13' numberSuffix='%'>");
            categories.Append("<categories>");
            currentYear.Append("<dataset seriesName='部门'>");


            if (lis.Count > 0)
            {
                for (int i = 0; i < lis.Count; i++)
                {
                    categories.AppendFormat("<category name='{0}' />", lis[i].UnitName.Trim());
                    if (string.IsNullOrEmpty(lis[i].Count))
                        currentYear.AppendFormat("<set value='{0}' />", 0);
                    else
                    {
                        currentYear.AppendFormat("<set value='{0}' />", Math.Round(Convert.ToDouble(lis[i].Count) / Convert.ToDouble(lis[i].Totals) * 100, 2));
                    }

                }
            }

            categories.Append("</categories>");
            currentYear.Append("</dataset>");
            previousYear.Append("</dataset>");
            xmlData.Append(categories.ToString());
            xmlData.Append(currentYear.ToString());
            xmlData.Append("</chart>");
            Literal1.Text = "";
            Literal2.Text = FusionCharts.RenderChart("../js/FunctionChart/MSColumn2D.swf", "", xmlData.ToString(), "productSales", "100%", "300", false, true);



        }

    }
}