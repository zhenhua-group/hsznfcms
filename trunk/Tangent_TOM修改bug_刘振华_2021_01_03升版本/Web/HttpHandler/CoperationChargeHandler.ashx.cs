﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class CoperationChargeHandler : HandlerCommon, IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request.Params["action"];
            if (action == "add")
            {
                TG.Model.cm_RealCprChg model = new TG.Model.cm_RealCprChg();
                TG.BLL.cm_RealCprChg bll = new TG.BLL.cm_RealCprChg();
                string chgid = context.Request.Params["id"];
                string chgcount = context.Request.Params["chgcount"];
                string time = context.Request.Params["time"];
                string ptime = context.Request.Params["ptime"];
                string user = context.Request.Params["user"];
                string mark = context.Request.Params["mark"];
                string billNo = context.Request["billNo"];
                string remitter = context.Request["remitter"];
                //赋值
                model.chg_id = int.Parse(chgid);
                model.payCount = Convert.ToDecimal(chgcount);
                model.payTime = Convert.ToDateTime(time);
                model.acceptuser = user;
                model.mark = mark;
                model.BillNo = billNo;
                model.Remitter = remitter;
                //判断是否收款逾期
                DateTime d_time = Convert.ToDateTime(time);
                DateTime d_ptime = Convert.ToDateTime(ptime);
                if (CompareDate(d_time, d_ptime) <= 0)
                {
                    model.isover = 1;
                }

                int identitySysNo = bll.Add(model);

                if (identitySysNo > 0)
                {
                    //查询财务RoleSysNo
                    string sql1 = string.Format("SELECT TOP 1 [SysNo] FROM [dbo].[cm_Role] where RoleName like N'%{0}%' order by SysNo DESC", "财务");
                    object objRoleSysNo = TG.DBUtility.DbHelperSQL.GetSingle(sql1);
                    int roleSysNo = objRoleSysNo == null ? 0 : Convert.ToInt32(objRoleSysNo);

                    //查询合同信息
                    DataSet set = TG.DBUtility.DbHelperSQL.Query(string.Format(@"SELECT 
                                                                                                            max(c.cpr_Name) as CoperationName
	                                                                                                       ,max(c.cpr_Unit) as Unit
                                                                                                            FROM [dbo].[cm_RealCprChg] rc
                                                                                                            join cm_CoperationCharge cc on cc.ID = rc.chg_id
                                                                                                            join cm_Coperation c on c.cpr_Id = cc.cpr_ID 
                                                                                                            where rc.ID = {0}
                                                                                                            group by c.cpr_ID", identitySysNo));
                    if (set.Tables.Count > 0)
                    {
                        string coperationName = set.Tables[0].Rows[0]["CoperationName"].ToString();
                        string departmentName = set.Tables[0].Rows[0]["Unit"].ToString();
                        //发送消息给财务
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("coperationName={0}&deparmentName={1}&fromUser={2}&sysNo={3}", coperationName, departmentName, UserSysNo, identitySysNo),
                            FromUser = UserSysNo,
                            InUser = UserSysNo,
                            MsgType = 6,
                            ToRole = roleSysNo.ToString(),
                            MessageContent = string.Format("关于{0}部门的{1}合同入账确认！", departmentName, coperationName),
                            QueryCondition = coperationName
                        };
                        int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    }

                    context.Response.Write("ok");
                }
                else
                {
                    context.Response.Write("no");
                }
            }
            else if (action == "select")
            {
                string chgid = context.Request.Params["id"];
                string strWhere = " chg_Id=" + chgid;
                TG.BLL.cm_RealCprChg bll_db = new TG.BLL.cm_RealCprChg();
                DataTable dt = bll_db.GetList(strWhere).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            else if (action == "del")
            {
                string chgid = context.Request.Params["id"];
                TG.BLL.cm_RealCprChg bll_db = new TG.BLL.cm_RealCprChg();
                if (bll_db.Delete(int.Parse(chgid)))
                {
                    context.Response.Write("ok");
                }
            }
        }
        //比较时间
        protected int CompareDate(DateTime dt1, DateTime dt2)
        {
            TimeSpan t1 = new TimeSpan(dt1.Ticks);
            TimeSpan t2 = new TimeSpan(dt2.Ticks);
            TimeSpan t3 = t2.Subtract(t1);
            //返回天数
            return t3.Days;
        }
        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName);
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(dt.Columns[j].ColumnName);
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(dt.Rows[i][j].ToString());
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
