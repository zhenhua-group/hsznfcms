﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace TG.Web.HttpHandler.Coperation
{
    public abstract class CoperationListHandler : IHttpHandler
    {  //存储过程名
        public abstract string ProcName { get; }
        //合同申请修改存储过程名
        public abstract string ProcName2 { get; }
        //合同申请修改存储过程名
        public abstract string ProcName3 { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                string year = DateTime.Now.Year.ToString();
                string sqlwhere = "";
                //年份
                if (year != "-1")
                {
                    sqlwhere = " AND year(cpr_SignDate)=" + year;
                }              

                sqlwhere = sqlwhere + strWhere;
                parameters.Where = sqlwhere;
                parameters.ProcedureName = ProcName3;
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                string strResponse = "";
                TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
                TG.Model.cm_Coperation model = new TG.Model.cm_Coperation();
                string cstid = forms.Get("EmpId");

                ArrayList listAudit = bll.DeleteList(cstid);
                //存在审批中的合同
                if (listAudit.Count > 0)
                {
                    string message = "";

                    for (int i = 0; i < listAudit.Count; i++)
                    {
                        model = bll.GetModel(int.Parse(listAudit[i].ToString()));
                        if (model != null)
                        {
                            message = message + model.cpr_Name + "合同在审批中，或者审批完毕\r\n ";
                        }
                    }
                    message = message + "不能删除";
                    strResponse = message;
                }
                else
                {
                    strResponse = "删除成功";
                }

                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                string cprtype = context.Request.QueryString["cprtype"] ?? "";//合同分类
                string buildunit = context.Request.QueryString["buildunit"] ?? "";//建设单位
                string account1 = context.Request.QueryString["account1"] ?? "";//合同额区间
                string account2 = context.Request.QueryString["account2"] ?? "";
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    unit = unit.Substring(0, unit.Length - 1);
                    strsql.AppendFormat(" AND (cpr_Unit in ({0})) ", unit.Trim());                  
                }
                //年份
                if (year != "-1"&&!string.IsNullOrEmpty(year))
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }
                //录入时间 
                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                //合同分类
                if (!string.IsNullOrEmpty(cprtype) && !cprtype.Contains("选择"))
                {
                    strsql.AppendFormat(" AND cpr_Type='{0}' ", cprtype);
                }
                //按建设单位查询
                if (buildunit.Trim() != "")
                {
                    buildunit = TG.Common.StringPlus.SqlSplit(buildunit);
                    strsql.AppendFormat(" AND BuildUnit like '%{0}%'", buildunit);
                }
                //合同额
                if (!string.IsNullOrEmpty(account1) && string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount>={0} ",account1);
                }
                else if (string.IsNullOrEmpty(account1) && !string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount<={0} ", account2);
                }
                else if (!string.IsNullOrEmpty(account1) && !string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount between {0} and {1} ", account1, account2);
                }
               

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName3;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
           
            else if (strAction == "Audit")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (cpr_Unit= '{0}')", unit);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }
                strsql.Append("AND cpr_Type not  like'%项目协议%'");

                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
               

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "AuditEdit")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (cpr_Unit= '{0}')", unit);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }

                strsql.Append("AND cpr_Type not  like'%项目协议%'");

                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                
                strsql.Append(strWhere);

                parameters.ProcedureName = ProcName2;
                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "updatecprlist")//合同额修改列表
            {
                string unit = context.Request.QueryString["unit"]??"";
                string year = context.Request.QueryString["year"]??"";
                string keyname = context.Request.QueryString["keyname"]??"";
                string startTime = context.Request.QueryString["startTime"]??"";//录入时间
                string endTime = context.Request.QueryString["endTime"]??"";//录入时间
                string cprtype = context.Request.QueryString["cprtype"] ?? "";//合同分类
                string buildunit = context.Request.QueryString["buildunit"] ?? "";//建设单位
                string account1 = context.Request.QueryString["account1"] ?? "";//合同额区间
                string account2 = context.Request.QueryString["account2"] ?? "";
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    unit = unit.Substring(0, unit.Length - 1);
                    strsql.AppendFormat(" AND (cpr_Unit in ({0})) ", unit.Trim());
                }
                //年份
                if (year != "-1" && !string.IsNullOrEmpty(year))
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }
                //录入时间 
                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                //合同分类
                if (!string.IsNullOrEmpty(cprtype) && !cprtype.Contains("选择"))
                {
                    strsql.AppendFormat(" AND cpr_Type='{0}' ", cprtype);
                }
                //按建设单位查询
                if (buildunit.Trim() != "")
                {
                    buildunit = TG.Common.StringPlus.SqlSplit(buildunit);
                    strsql.AppendFormat(" AND BuildUnit like '%{0}%'", buildunit);
                }
                //合同额
                if (!string.IsNullOrEmpty(account1) && string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount>={0} ", account1);
                }
                else if (string.IsNullOrEmpty(account1) && !string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount<={0} ", account2);
                }
                else if (!string.IsNullOrEmpty(account1) && !string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount between {0} and {1} ", account1, account2);
                }
               

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = "P_cm_UpdateCoperationList_jq";
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        
        public bool IsReusable
        {
            get { return false; }
        }
    }
}