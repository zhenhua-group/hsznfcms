﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;

namespace TG.Web.HttpHandler.Coperation
{
    public abstract class ProjectChargeHandler : IHttpHandler
    {
        //存储过程名
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                StringBuilder strsql = new StringBuilder("");
                string year = context.Request.QueryString["year"];               
                string starttime = context.Request.QueryString["starttime"];
                string endtime = context.Request.QueryString["endtime"];
                //按照年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }

                //收费时间判断
                string start = starttime.Trim();
                string end = endtime.Trim();
                string curtime = DateTime.Now.Year.ToString();

                if (start == "" && end != "")
                {
                    start = curtime+"-01-01";
                }
                else if (start != "" && end == "")
                {
                    end = curtime + "-12-31";
                }
                else if (start == "" && end == "")
                {
                    start = curtime + "-01-01";
                    end = curtime + "-12-31";
                }
                //实际收款时间
                string sqltime = string.Format(" and InAcountTime Between '{0}' AND '{1}'", start, end);
                strsql.Append(strWhere);             
                parameters.Where = strsql.ToString() + "|" + sqltime;
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                string strResponse = "";
                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {
                StringBuilder strsql = new StringBuilder("");
                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string starttime = context.Request.QueryString["starttime"];
                string endtime = context.Request.QueryString["endtime"];
                //名字不为空
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname.Trim());
                    strsql.Append(" AND cpr_Name LIKE '%" + keyname + "%'  ");
                }
                //绑定单位
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.Append(" AND cpr_Unit='" + unit.Trim() + "' ");
                }
                //按照年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }

                //收费时间判断
                string start = starttime.Trim();
                string end = endtime.Trim();
                string curtime = DateTime.Now.Year.ToString();

                if (start == "" && end != "")
                {
                    start = curtime + "-01-01";
                }
                else if (start != "" && end == "")
                {
                    end = curtime + "-12-31";
                }
                else if (start == "" && end == "")
                {
                    start = curtime + "-01-01";
                    end = curtime + "-12-31";
                }
                //实际收款时间
                string sqltime = string.Format(" and InAcountTime Between '{0}' AND '{1}'", start, end);
                strsql.Append(strWhere);
                parameters.Where = strsql.ToString() + "|" + sqltime;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            //高级查询
            else if (strAction == "Search")
            {
                StringBuilder sb = new StringBuilder("");              
                //查询方式
                string andor = context.Request["andor"] ?? "and";
                //合同年份
                string year = context.Request["drp_year"];
                //合同编号
                string txtcpr_No = context.Request["txtcpr_No"];
                //合同分类
                string drp_type = context.Request["drp_type"];
                //合同类型 
                string cprtype = context.Request["cprtype"];
                //合同名称
                string txt_cprname = context.Request["txt_cprname"];
                //建筑规模1
                string txt_Area = context.Request["txt_Area"];
                //建筑规模2
                string txt_Area2 = context.Request["txt_Area2"];
                //建设单位
                string txt_cprBuildUnit = context.Request["txt_cprBuildUnit"];
                //结构形式
                string asTreeviewStruct = context.Request["asTreeviewStruct"];
                //建筑分类
                string asTreeviewStructType = context.Request["asTreeviewStructType"];
                //建筑类别
                string drp_buildtype = context.Request["drp_buildtype"];
                //工程负责人
                string txt_proFuze = context.Request["txt_proFuze"];
                //甲方负责人
                string txtFParty = context.Request["txtFParty"];
                //承接部门
                string drp_unit = context.Request["drp_unit"];
                //合同额1
                string txtcpr_Account = context.Request["txtcpr_Account"];
                //合同额2
                string txtcpr_Account2 = context.Request["txtcpr_Account2"];
                //工程地点
                string txt_Address = context.Request["txt_Address"];
                //行业性质
                string ddType = context.Request["ddType"];
                //合同阶段
                string chk_cprjd = context.Request["chk_cprjd"];
                //工程来源
                string ddSourceWay = context.Request["ddSourceWay"];
                //合同签订日期1
                string txt_signdate_1 = context.Request["txt_signdate_1"];
                //合同签订日期2
                string txt_signdate2_1 = context.Request["txt_signdate2_1"];
                //合同完成日期1
                string txt_finishdate = context.Request["txt_finishdate"];
                //合同完成日期2
                string txt_finishdate2 = context.Request["txt_finishdate2"];
                //合同统计年份1
                string txt_signdate = context.Request["txt_signdate"];
                //合同统计年份2
                string txt_signdate2 = context.Request["txt_signdate2"];
                //合同收费1
                string projcharge1 = context.Request["projcharge1"];
                //合同收费2
                string projcharge2 = context.Request["projcharge2"];
                //按照客户
                string txt_cst = context.Request["txt_cst"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
              
                //合同编号
                if (!string.IsNullOrEmpty(txtcpr_No))
                {
                    sb.Append("  and ( cpr_No like '%" + txtcpr_No.Trim() + "%' ");
                }
                //合同分类
                if (!string.IsNullOrEmpty(drp_type) && !drp_type.Contains("选择"))
                {
                     if (sb.ToString() != "")
                        {
                            sb.Append(" " + andor + " ");
                        }
                        else
                        {
                            sb.Append(" and ( ");
                        }
                   
                    sb.Append(" cpr_Type='" + drp_type.Trim() + "' ");
                }
                //合同分类
                if (!string.IsNullOrEmpty(cprtype) && !cprtype.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append(" cpr_Type2='" + cprtype.Trim() + "' ");
                }
                //合同名称

                if (!string.IsNullOrEmpty(txt_cprname))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_cprname.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Name LIKE '%" + keyname + "%' ");
                }
                //建筑规模1
                //建筑规模2
                if (!string.IsNullOrEmpty(txt_Area) && string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Convert(float,BuildArea) >=" + txt_Area.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txt_Area) && !string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Convert(float,BuildArea) <=" + txt_Area2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txt_Area) && !string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (Convert(float,BuildArea) >=" + txt_Area.Trim() + " AND Convert(float,BuildArea) <=" + txt_Area2.Trim() + ") ");
                }
                //建设单位
                if (!string.IsNullOrEmpty(txt_cprBuildUnit))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildUnit like '%" + txt_cprBuildUnit.Trim() + "%' ");
                }
                //结构形式
                if (!string.IsNullOrEmpty(asTreeviewStruct) && !asTreeviewStruct.Contains("请选择结构形式"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                   // sb.Append(" replace(replace(replace(StructType,'+',','),'*',','),'^',',')  LIKE '%" + asTreeviewStruct + "%' ");
                    sb.Append(" StructType  LIKE '%" + asTreeviewStruct + "%' ");
                }
                //建筑分类
                if (!string.IsNullOrEmpty(asTreeviewStructType) && !asTreeviewStructType.Contains("请选择建筑分类"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                   // sb.Append(" replace(replace(replace(BuildStructType,'+',','),'*',','),'^',',') LIKE '%" + asTreeviewStructType + "%' ");
                    sb.Append(" BuildStructType LIKE '%" + asTreeviewStructType + "%' ");
                }
                //建筑类别
                if (!string.IsNullOrEmpty(drp_buildtype) && !drp_buildtype.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildType =N'" + drp_buildtype.Trim() + "' ");
                }
                //工程负责人
                if (!string.IsNullOrEmpty(txt_proFuze))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_proFuze.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ChgPeople='" + keyname + "' ");
                }
                //甲方负责人
                if (!string.IsNullOrEmpty(txtFParty))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txtFParty.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ChgJia='" + keyname + "' ");
                }
                //年份
                if (year != "-1" && !string.IsNullOrEmpty(year))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.AppendFormat(" year(cpr_SignDate)={0} ", year);
                }
                //承接部门
                if (!string.IsNullOrEmpty(drp_unit) && !drp_unit.Contains("全院部门"))
                {
                    drp_unit = drp_unit.Substring(0, drp_unit.Length - 1);                 

                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_Unit in (" + drp_unit.Trim()+ ")) ");
                }
                //合同额1
                //合同额2
                if (!string.IsNullOrEmpty(txtcpr_Account) && string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Acount >= " + txtcpr_Account.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtcpr_Account) && !string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Acount <= " + txtcpr_Account2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtcpr_Account) && !string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_Acount>=" + txtcpr_Account.Trim() + " AND cpr_Acount <= " + txtcpr_Account2.Trim() + ") ");
                }
                //工程地点
                if (!string.IsNullOrEmpty(txt_Address))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildPosition like '%" + txt_Address.Trim() + "%' ");
                }
                //行业性质
                if (!string.IsNullOrEmpty(ddType) && !ddType.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Industry='" + ddType.Trim() + "' ");
                }
                //合同阶段
                string str_process = chk_cprjd;
                if (!string.IsNullOrEmpty(str_process))
                {
                    str_process = str_process.IndexOf(",") > -1 ? str_process.Remove(str_process.Length - 1) : "";
                    str_process = str_process.Replace("方案", "27").Replace("初设", "28").Replace("施工图", "29").Replace("其他", "30");
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Process like '%" + str_process + "%' ");
                }

                //工程来源
                if (!string.IsNullOrEmpty(ddSourceWay) && !ddSourceWay.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildSrc='" + ddSourceWay.Trim() + "' ");
                }
                //合同签订时间1
                //合同签订时间2
                if (!string.IsNullOrEmpty(txt_signdate_1) && string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate >= '" + txt_signdate_1.Trim() + "' ");
                }
                else if (string.IsNullOrEmpty(txt_signdate_1) && !string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate <= '" + txt_signdate2_1.Trim() + "' ");
                }
                else if (!string.IsNullOrEmpty(txt_signdate_1) && !string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_SignDate BETWEEN '" + txt_signdate_1.Trim() + "' AND '" + txt_signdate2_1.Trim() + "') ");
                }
                ////合同完成日期1
                ////合同完成日期2
                //if (!string.IsNullOrEmpty(txt_finishdate) && string.IsNullOrEmpty(txt_finishdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_DoneDate >= '" + txt_finishdate.Trim() + "' ");
                //}
                //else if (string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_DoneDate <= '" + txt_finishdate2.Trim() + "' ");
                //}
                //else if (!string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" (cpr_DoneDate BETWEEN '" + txt_finishdate.Trim() + "' AND '" + txt_finishdate2.Trim() + "') ");
                //}
                ////合同统计年份1
                ////合同统计年份2
                //if (!string.IsNullOrEmpty(txt_signdate) && string.IsNullOrEmpty(txt_signdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_SignDate >= '" + txt_signdate.Trim() + "' ");
                //}
                //else if (string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" cpr_SignDate <= '" + txt_signdate2.Trim() + "' ");
                //}
                //else if (!string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" (cpr_SignDate BETWEEN '" + txt_signdate.Trim() + "' AND '" + txt_signdate2.Trim() + "') ");
                //}
                //合同收费1
                //合同收费2
                if (!string.IsNullOrEmpty(projcharge1.Trim()) && string.IsNullOrEmpty(projcharge2.Trim()))
                {
                    int Acount = 0;
                    //等于false：acount=0不需要加条件，等于true：acount=projcharge1
                    if (int.TryParse(projcharge1, out Acount))
                    {
                        if (sb.ToString() != "")
                        {
                            sb.Append(" " + andor + " ");
                        }
                        else
                        {
                            sb.Append(" and ( ");
                        }
                        sb.Append(" ssze >= " + projcharge1.Trim() + " ");
                    }
                }
                else if (string.IsNullOrEmpty(projcharge1.Trim()) && !string.IsNullOrEmpty(projcharge2.Trim()))
                {
                    int Acount = 0;
                    //等于false：acount=0不需要加条件，等于true：acount=projcharge2
                    if (int.TryParse(projcharge2, out Acount))
                    {
                        if (sb.ToString() != "")
                        {
                            sb.Append(" " + andor + " ");
                        }
                        else
                        {
                            sb.Append(" and ( ");
                        }
                        sb.Append(" ssze <= " + projcharge2.Trim() + " ");
                    }
                }
                else if (!string.IsNullOrEmpty(projcharge1.Trim()) && !string.IsNullOrEmpty(projcharge2.Trim()))
                {
                    int Acount = 0;
                    if (int.TryParse(projcharge1, out Acount) && int.TryParse(projcharge2, out Acount))
                    {
                        if (sb.ToString() != "")
                        {
                            sb.Append(" " + andor + " ");
                        }
                        else
                        {
                            sb.Append(" and ( ");
                        }
                        sb.Append(" ssze BETWEEN " + projcharge1.Trim() + " AND " + projcharge2.Trim() + " ");
                    }

                }
                //按照客户
                if (!string.IsNullOrEmpty(txt_cst))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_cst.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cst_Id IN (Select Cst_Id From cm_CustomerInfo Where Cst_Name like '%" + keyname + "%') ");
                }
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" (InsertDate between '{0}' and '{1}') ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                if (sb.ToString() != "")
                {
                    sb.Append(")");
                }              

                sb.Append(strWhere);
                parameters.ProcedureName = "P_cm_schCopertion_jq";
                parameters.Where = sb.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());

            }
            //合同收费明细
            else if (strAction == "cprfinalist")
            {

                string unit = context.Request.QueryString["unit"];
                string keyname = context.Request.QueryString["keyname"];
                string year = context.Request.QueryString["year"];//基本查询里年份
                string year1 = context.Request.QueryString["year1"];//高级查询
                string year2 = context.Request.QueryString["year2"];//高级查询
                string startyear = context.Request.QueryString["startyear"];//合同时间段开始时间
                string endyear = context.Request.QueryString["endyear"];//合同时间段结束时间
                string cbxtimetype=context.Request.QueryString["cbxtimetype"]??"";//区分是否选中时间段1为选中0未选
                string jidu = context.Request.QueryString["jidu"];//合同季度
                string month = context.Request.QueryString["month"];//合同月份
                string txtproAcount1 = context.Request.QueryString["txtproAcount1"];
                string txtproAcount2 = context.Request.QueryString["txtproAcount2"];
                string projcharge1 = context.Request.QueryString["projcharge1"];
                string projcharge2 = context.Request.QueryString["projcharge2"];
                string txtprogress = context.Request.QueryString["txtprogress"];
                string txtprogress2 = context.Request.QueryString["txtprogress2"];
                //收费日期
                string chargeStartDate = context.Request.QueryString["chargeStartDate"];
                string chargeEndDate = context.Request.QueryString["chargeEndDate"];
                //查询类型，高级(gj)，否则普通
                string cxtype = context.Request.QueryString["cxtype"] ?? "";

                StringBuilder strsql = new StringBuilder("");//基础
                StringBuilder strUnit = new StringBuilder();//部门
                StringBuilder builder = new StringBuilder();//收费
                StringBuilder strCharge = new StringBuilder();

                //收费日期
                if (!string.IsNullOrEmpty(chargeStartDate) && !string.IsNullOrEmpty(chargeEndDate))
                {
                    builder.Append(" AND (InAcountTime BETWEEN '" + chargeStartDate.Trim() + "' AND '" + chargeEndDate.Trim() + "') ");
                    strCharge.Append(" Inner JOIN (SELECT DISTINCT CPRID FROM cm_ProjectCharge  WHERE 1=1  AND Status<>'B' " + builder.ToString() + "  ) CH   ON  C.cpr_Id=CH.cprID	");

                }
                else if (string.IsNullOrEmpty(chargeStartDate) && !string.IsNullOrEmpty(chargeEndDate))
                {
                    builder.Append(" AND InAcountTime <='" + chargeEndDate.Trim() + "' ");
                    strCharge.Append(" Inner JOIN (SELECT DISTINCT CPRID FROM cm_ProjectCharge  WHERE 1=1  AND Status<>'B' " + builder.ToString() + "  ) CH   ON  C.cpr_Id=CH.cprID	");

                }
                else if (!string.IsNullOrEmpty(chargeStartDate) && string.IsNullOrEmpty(chargeEndDate))
                {
                    builder.Append(" AND InAcountTime >='" + chargeStartDate.Trim() + "' ");
                    strCharge.Append(" Inner JOIN (SELECT DISTINCT CPRID FROM cm_ProjectCharge  WHERE 1=1  AND Status<>'B' " + builder.ToString() + "  ) CH   ON  C.cpr_Id=CH.cprID	");
                }
                else
                {
                    builder.Append(" AND null is null ");
                }

                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%' ", keyname.Trim());
                }

                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
                {
                    unit = unit.Substring(0, unit.Length - 1);
                    strsql.AppendFormat(" AND (cpr_Unit in ({0})) ", unit.Trim());
                    strUnit.AppendFormat(" AND (cpr_Unit in ({0})) ", unit.Trim());
                }
                else
                {
                    strUnit.Append(" AND null is null ");
                }
                if (cxtype == "gj")
                {
                    //年份
                    if (!string.IsNullOrEmpty(year1) && year1 != "-1" && (string.IsNullOrEmpty(year2) || year2 == "-1"))
                    {
                        strsql.AppendFormat(" AND year(cpr_SignDate)>={0} ", year1.Trim());
                    }
                    else if ((string.IsNullOrEmpty(year1) || year1 == "-1") && !string.IsNullOrEmpty(year2) && year2 != "-1")
                    {
                        strsql.AppendFormat(" AND year(cpr_SignDate)<={0} ", year2.Trim());
                    }
                    else if (!string.IsNullOrEmpty(year1) && year1 != "-1" && !string.IsNullOrEmpty(year2) && year2 != "-1")
                    {
                        strsql.AppendFormat(" AND year(cpr_SignDate) BETWEEN {0} and {1} ", year1.Trim(), year2.Trim());
                    }

                }
                else
                {
                    //合同时间段 
                    if (cbxtimetype.Trim() == "1")
                    {
                        string curtime = DateTime.Now.Year.ToString();
                        if (startyear == "" && endyear != "")
                        {
                            strsql.AppendFormat(" AND cpr_SignDate<='{0}'", endyear.Trim());                    
                        }
                        else if (startyear != "" && endyear == "")
                        {
                            strsql.AppendFormat(" AND cpr_SignDate>='{0}'", startyear.Trim());                        
                        }
                        else if (startyear != "" && endyear != "")
                        {
                            strsql.AppendFormat(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", startyear.Trim(), endyear.Trim());
                        }                        
                    }
                    else
                    {
                        //年份
                        if (!string.IsNullOrEmpty(year) && year != "-1")
                        {                            
                            //年
                            string stryear = year;
                            //季度
                            string strjidu = jidu;
                            //月
                            string stryue = month;
                            //
                            string start = "";
                            string end = "";
                            //年
                            if (strjidu == "0" && stryue == "0")
                            {
                                start = stryear + "-01-01 00:00:00";
                                end = stryear + "-12-31 23:59:59 ";
                            }
                            else if (strjidu != "0" && stryue == "0") //年季度
                            {
                                start = stryear;
                                end = stryear;
                                switch (strjidu)
                                {
                                    case "1":
                                        start += "-01-01 00:00:00";
                                        end += "-03-31 23:59:59";
                                        break;
                                    case "2":
                                        start += "-04-01 00:00:00";
                                        end += "-06-30 23:59:59";
                                        break;
                                    case "3":
                                        start += "-07-01 00:00:00";
                                        end += "-09-30 23:59:59";
                                        break;
                                    case "4":
                                        start += "-10-01 00:00:00";
                                        end += "-12-31 23:59:59";
                                        break;
                                }
                            }
                            else if (strjidu == "0" && stryue != "0")//年月份
                            {
                                //当月有几天
                                int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                                start = stryear + "-" + stryue + "-01 00:00:00";
                                end = stryear + "-" + stryue + "-" + days + " 23:59:59";
                            }                           
                            strsql.AppendFormat(" AND cpr_SignDate BETWEEN '{0}' and '{1}' ", start.Trim(), end.Trim());
                           
                        }
                    }
                    

                }
               

                //合同额1
                //合同额2
                if (!string.IsNullOrEmpty(txtproAcount1) && string.IsNullOrEmpty(txtproAcount2))
                {
                    strsql.Append(" AND cpr_Acount >= " + txtproAcount1.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtproAcount1) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    strsql.Append(" AND cpr_Acount <= " + txtproAcount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtproAcount1) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    strsql.Append(" AND (cpr_Acount>=" + txtproAcount1.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
                }
                //合同收费
                if (!string.IsNullOrEmpty(projcharge1) && string.IsNullOrEmpty(projcharge2))
                {
                    strsql.Append(" AND ssze >= " + projcharge1.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(projcharge1) && !string.IsNullOrEmpty(projcharge2))
                {
                    strsql.Append(" AND ssze <= " + projcharge2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(projcharge1) && !string.IsNullOrEmpty(projcharge2))
                {
                    strsql.Append(" AND (ssze>=" + projcharge1.Trim() + " AND ssze <= " + projcharge2.Trim() + ") ");
                }
                //收费进度
                if (!string.IsNullOrEmpty(txtprogress) && string.IsNullOrEmpty(txtprogress2))
                {
                    strsql.Append(" AND sfjd >= " + txtprogress.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtprogress) && !string.IsNullOrEmpty(txtprogress2))
                {
                    strsql.Append(" AND sfjd <= " + txtprogress2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtprogress) && !string.IsNullOrEmpty(txtprogress2))
                {
                    strsql.Append(" AND (sfjd>=" + txtprogress.Trim() + " AND sfjd <= " + txtprogress2.Trim() + ") ");
                }

                strsql.Append(strWhere);
                strsql.Append(" |" + builder.ToString().Trim());//收费
                strsql.Append("/" + strUnit.ToString().Trim());//部门
                strsql.Append("?" + strCharge.ToString().Trim());//合同选择全部

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = "P_cm_ProjectChargeSch_jq";
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}