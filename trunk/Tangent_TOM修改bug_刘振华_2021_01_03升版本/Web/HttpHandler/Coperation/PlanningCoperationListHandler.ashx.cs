﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    /// <summary>
    /// PlanningCoperationListHandler 的摘要说明
    /// </summary>
    public class PlanningCoperationListHandler1 : PlanningCoperationListHandler
    {

        public override string ProcName
        {
            get
            {
                return "P_cm_PlanningCoperation_jq";
            }
        }

        public override string ProcName2
        {
            get
            {
                return "P_cm_CoperationEdit_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_schPlanningCoperation_jq";
            }
        }
    }
}