﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    /// <summary>
    /// PitCoperationListHandler1 的摘要说明
    /// </summary>
    public class PitCoperationListHandler1 : PitCoperationListHandler
    {
        public override string ProcName
        {
            get
            {
                return "P_cm_PitCoperation_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_CoperationEdit_jq";
            }
        }
    }
}