﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    /// <summary>
    /// CoperationListExportHandler 的摘要说明
    /// </summary>
    public class CoperationListExportHandler : IHttpHandler
    {
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }
        public void ProcessRequest(HttpContext context)
        {


            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            string ProcName = "P_cm_Coperation_jq";
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["page"]),
                    PageSize = 20, //int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["sortname"],
                    Sort = context.Request["sortorder"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }
            if (strAction == "export")
            {
                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (cpr_Unit= '{0}')", unit);
                }
                //年份
                if (year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;
                ExportDataToExcel(GetJsonResultByExcel(), "~/TemplateXls/cpr_AcountAndIndustryList.xls", "2014", context);
            }


        }
        public DataTable GetJsonResultByExcel()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };
            return dataResult.Data;
            //dataResult

        }
        private void ExportDataToExcel(DataTable dt, string modelPath, string year, HttpContext context)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(context.Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            ws.GetRow(0).GetCell(0).SetCellValue("年同期对比表");
            ws.GetRow(1).GetCell(0).SetCellValue(DateTime.Now.ToString("单位：万元          yyyy年-MM月-dd日") + "制表");
            ws.GetRow(2).GetCell(2).SetCellValue(int.Parse(year) - 1 + "年按类别划分");
            ws.GetRow(2).GetCell(7).SetCellValue(int.Parse(year) + "年按类别划分");
            //ws.GetRow(3).GetCell(4).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(5).SetCellValue(int.Parse(year) + "年");

            //ws.GetRow(4).GetCell(1).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(2).SetCellValue(DateTime.Now.Month + "月");
            //ws.GetRow(4).GetCell(4).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(5).SetCellValue(DateTime.Now.Month + "月");
            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 4;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            // ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.Write(memoryStream);
                string name = System.Web.HttpContext.Current.Server.UrlEncode("年（1-月)已签订合同额【项目分类】与年同期对比表.xls");
                context.Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                context.Response.ContentType = "application/octet-stream";
                context.Response.BinaryWrite(memoryStream.ToArray());
                context.Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                context.Response.End();
                //System.IO.MemoryStream memStream = new System.IO.MemoryStream(); wk.Write(memStream); memStream.Flush(); memStream.Position = 0; wk = null; byte[] bytes = new byte[memStream.Length]; memStream.Read(bytes, 0, bytes.Length);        // 设置当前流的位置为流的开始         memStream.Seek(0, System.IO.SeekOrigin.Begin);        context.Response.ContentType = "application/octet-stream";        //通知浏览器下载文件而不是打开        string fileName = "TX" + DateTime.Now.Date.ToShortDateString().ToString() + ".xls";//HttpUtility.UrlEncode        context.Response.AddHeader("Content-Disposition", "attachment;  filename=" + fileName);        context.Response.BinaryWrite(bytes);        context.Response.Flush();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}