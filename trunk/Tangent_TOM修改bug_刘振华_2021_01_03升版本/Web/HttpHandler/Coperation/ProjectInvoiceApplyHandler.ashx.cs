﻿using Microsoft.SqlServer.Management.Sdk.Sfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using TG.Model;

namespace TG.Web.HttpHandler.Coperation
{
    /// <summary>
    /// ProjectInvoiceApplyHandler 的摘要说明
    /// </summary>
    public class ProjectInvoiceApplyHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {

        public TG.Model.cm_ProjectInvoiceEntity projectInvoiceEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectInvoiceEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectInvoiceEntity>(data);
                }
                return obj;
            }
        }
        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public string Action
        {
            get
            {
                string action = "0";
                action = Request["action"];
                return action;
            }
        }
        //查询or更新
        public int Flag
        {
            get
            {
                int flag = 0;
                int.TryParse(Request["flag"], out flag);
                return flag;
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                     //暂时没有用
                    case "ApplyInvoice":                       
                        string userList = context.Request["userList"];
                        ApplyProjectInvoice(userList);
                        break;
                    case "add": //添加
                        AddInvoice(context);
                        break;
                    case "edit": //编辑
                        EditInvoice(context);
                        break;
                    case "del": //删除
                        DelInvoice(context);
                        break;
                }
                Response.End();
            }
        }
        //添加收费
        private void AddInvoice(HttpContext context)
        {
            //添加收费
            string projid = context.Request.Params["projid"] ?? "0";
            string cprid = context.Request.Params["cprid"] ?? "0";
            string paycount = context.Request.Params["paycount"] ?? "0";
            string remitter = context.Request.Params["remitter"] ?? "";
            string time = context.Request.Params["time"] ?? "";
            string mark = context.Request.Params["mark"] ?? "";
            string curuser = context.Request.Params["curuser"] ?? "0";
            string drop_type = context.Request.Params["drop_type"] ?? "";
          


            TG.Model.cm_ProjectInvoiceEntity model = new TG.Model.cm_ProjectInvoiceEntity();
            TG.BLL.cm_ProjectInvoice bll = new TG.BLL.cm_ProjectInvoice();
            model.pro_id = projid;
            model.cpr_id = cprid;
            model.InvoiceAmount = paycount;
            model.InvoiceName = remitter;
            model.InvoiceType = drop_type;
            model.InvoiceDate = Convert.ToDateTime(time).ToString("yyyy-MM-dd");
            model.user_id = curuser;          
            model.ApplyMark = mark;        
          
            //消息的结果
            int identity = bll.AddProjectInvoice(model,"");
            if (identity > 0)
            {
                //发送消息到财务
                SendMessageToCW(cprid, projid, identity.ToString());
                context.Response.Write("0");
            }
            else
            {
                context.Response.Write("1");
            }
        }
        //修改收费
        private void EditInvoice(HttpContext context)
        {
            string projid = context.Request.Params["projid"] ?? "0";
            string cprid = context.Request.Params["cprid"] ?? "0";
            string chgid = context.Request.Params["chgid"] ?? "0";
            string paycount = context.Request.Params["paycount"] ?? "0";
            string remitter = context.Request.Params["remitter"] ?? "";
            string time = context.Request.Params["time"] ?? "";
            string mark = context.Request.Params["mark"] ?? "";
            string curuser = context.Request.Params["curuser"] ?? "";
            string drop_type = context.Request.Params["drop_type"] ?? "";

            TG.Model.cm_ProjectInvoice model = new TG.BLL.cm_ProjectInvoice().GetModel(int.Parse(chgid));
            TG.BLL.cm_ProjectInvoice bll = new TG.BLL.cm_ProjectInvoice();
            //赋值
            model.ID = Convert.ToInt32(chgid);
            model.projID = Convert.ToInt32(projid);
            model.cprID = Convert.ToInt32(cprid);
            model.InvoiceDate = Convert.ToDateTime(time);
            model.InvoiceType = drop_type;
            model.ApplyMark = mark;  
            model.InvoiceName = remitter;
            model.InvoiceAmount = Convert.ToDecimal(paycount);
            if (bll.Update(model)>0)
            {
                context.Response.Write("0");
            }
            else
            {
                context.Response.Write("1");
            }
        }

         //删除收费
        private void DelInvoice(HttpContext context)
        {
            string chg_id = context.Request.Params["chgid"] ?? "0";
            if (new TG.BLL.cm_ProjectInvoice().Delete(int.Parse(chg_id)))
            {
                context.Response.Write("0");
            }
            else
            {
                context.Response.Write("1");
            }
        }
        //发送消息到财务
        protected void SendMessageToCW(string cprid, string projid, string identity)
        {
            //查询财务RoleSysNo
            string sql1 = string.Format("SELECT TOP 1 [SysNo] FROM [dbo].[cm_Role] where RoleName like N'%{0}%' order by SysNo DESC", "财务");
            object objRoleSysNo = TG.DBUtility.DbHelperSQL.GetSingle(sql1);
            int roleSysNo = objRoleSysNo == null ? 0 : Convert.ToInt32(objRoleSysNo);

            //查询项目信息
            TG.Model.cm_Coperation model = new TG.BLL.cm_Coperation().GetModel(int.Parse(cprid));
            if (model != null)
            {
                string cprname = model.cpr_Name;
                string cprunit = model.cpr_Unit;

                //发送消息给财务
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("fromUser={0}&sysNo={1}&projID={2}&cprID={3}&auditstatus=A", UserSysNo, identity, projid,cprid),
                    FromUser = UserSysNo,
                    InUser = UserSysNo,
                    MsgType = 102,//合同开票财务确认
                    ToRole = roleSysNo.ToString(),
                    MessageContent = string.Format("关于{0}的{1}合同开票财务部确认！", cprunit, cprname),
                    QueryCondition = cprname,
                    IsDone = "A"
                };
                int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
            }
        }
        /// <summary>
        /// 申请开票
        /// </summary>
        private void ApplyProjectInvoice(string userList)
        {
            if (Flag == 0)
            {
                //返回下阶段用户列表
                GetNextProcessRoleUser();
            }
            else
            {
                TG.BLL.cm_ProjectInvoice bll = new TG.BLL.cm_ProjectInvoice();
                int count = bll.AddProjectInvoice(projectInvoiceEntity, userList);
                if (count > 0)
                {
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
        }
        //返回下一阶段用户列表
        private void GetNextProcessRoleUser()
        {
            string sql = " select top 1 SysNo from cm_Role where RoleName like '%财务%'";
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            int roleID = resultObj == null ? 0 : int.Parse(resultObj.ToString());
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(roleID);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                Response.Write(roleUserString);
            }
            else
            {
                Response.Write("0");
            }

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}