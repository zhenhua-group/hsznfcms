﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;

namespace TG.Web.HttpHandler.Coperation
{
    public abstract class CoperationForReportListHandler : IHttpHandler
    {//存储过程名
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                string strResponse = "";
                TG.BLL.cm_Coperation_back bll = new TG.BLL.cm_Coperation_back();              
                string cstid = forms.Get("EmpId");
                bool flag = bll.DeleteList(cstid);

                if (flag)
                {
                    strResponse="删除成功";
                }
                else
                {
                    strResponse = "删除失败";
                }

                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {
                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (cpr_Unit= '{0}')", unit);
                }
               
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "Search")
            {
                string keyname = context.Request.QueryString["keyname"];
                string unit = context.Request.QueryString["unit"];
                string drp_type = context.Request.QueryString["drp_type"];
                string txt_start = context.Request.QueryString["txt_start"];
                string txt_end = context.Request.QueryString["txt_end"];
                StringBuilder strsql = new StringBuilder("");              

                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && !unit.Contains("请选择"))
                {
                    strsql.AppendFormat(" AND (cpr_Unit= '{0}')", unit);
                }
                if (!string.IsNullOrEmpty(drp_type)&&!drp_type.Contains("请选择"))
                {
                    strsql.AppendFormat(" And cpr_Type='" + drp_type.Trim() + "'");
                }
                if (!string.IsNullOrEmpty(txt_start) &&!string.IsNullOrEmpty(txt_end))
                {
                    strsql.AppendFormat(" And (RegTime BETWEEN '" + txt_start.Trim() + "' AND '" + txt_end.Trim() + "')");
                }
                strsql.Append(strWhere);
                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}