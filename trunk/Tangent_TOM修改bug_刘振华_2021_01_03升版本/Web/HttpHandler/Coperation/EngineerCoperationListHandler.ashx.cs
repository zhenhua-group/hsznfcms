﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    /// <summary>
    /// EngineerCoperationListHandler1 的摘要说明
    /// </summary>
    public class EngineerCoperationListHandler1 : EngineerCoperationListHandler
    {


        public override string ProcName
        {
            get
            {
                return "P_cm_EngineerCoperation_jq";
            }
        }

        public override string ProcName2
        {
            get
            {
                return "P_cm_CoperationEdit_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_schEngineerCoperation_jq";
            }
        }
    }
}