﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    public abstract class cpr_SysValueMsgListViewBymaster : IHttpHandler
    {
        //存储过程名
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }
        //加载所存在的条件参数拼接
        StringBuilder strWhere = new StringBuilder(" and InUser <> 0 ");
        string MessageType;//判断消息类型
        string MessageDone;//判断是否已办
        string MessageIsDoneStatus;//判断已办的状态
        string MessageIsRead;//判断是否已读
        int cpruserSysNum;
        public void ProcessRequest(HttpContext context)
        {
            TG.BLL.QueryPagingParameters parameters;
            context.Response.ContentType = "application/json";
            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            if (strOper != "del")
            {
                MessageType = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["MessageType"]);
                MessageDone = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["MessageDone"]);
                MessageIsDoneStatus = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["MessageIsDoneStatus"]) ?? "";
                MessageIsRead = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["MessageIsRead"]) ?? "";
                cpruserSysNum = int.Parse(System.Web.HttpUtility.UrlDecode(context.Request.QueryString["cpruserSysNum"]));
            }

            if (string.IsNullOrEmpty(MessageIsDoneStatus))
            {
                MessageIsDoneStatus = "A";
            }
            if (string.IsNullOrEmpty(MessageIsRead))
            {
                MessageIsRead = "A";
            }
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (MessageDone == "done")
            {
                strWhere.Append(" AND S.IsDone='" + MessageIsDoneStatus + "'");
            }
            else
            {
                if (strAction == null && MessageIsRead == "A")
                {
                    strWhere.Append(" AND (S.Status='" + MessageIsRead + "' or S.IsDone='A')");

                }
                else
                {
                    strWhere.Append(" AND S.Status='" + MessageIsRead + "'");
                }

            }

            //查询登陆人的消息条件
            string whereSql = " and (";

            //查到个人条件
            whereSql += "(S.FromUser = " + cpruserSysNum + " and S.ToRole = 0 ) ";

            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(cpruserSysNum);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (S.ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") )";
            }

            whereSql += ")";
            strWhere.Append(whereSql);

            //只查询产值 
            strWhere.Append(" and MsgType in (5,10,14,15,16,17,18,19,20,25,26,27,28,29,30,31)");

            //加载数据
            if (strOper == null && strAction == null)
            {

                if (!string.IsNullOrEmpty(MessageType))
                {
                    strWhere.Append(" AND S.MsgType=" + MessageType);
                }
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere.ToString()

                };

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();

                string cpr_id = forms.Get("EmpId");

                string strResponse = "";
                if (cpr_id == "")
                {
                    strResponse = "未选中任何记录！";
                }
                else
                {
                    if (bll_cpr.DeleteList(cpr_id).Count == 0)
                    {
                        strResponse = "删除成功！";
                    }
                    else
                    {
                        strResponse = "删除成功，但所选内容包含审核记录！";
                    }
                }
                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {


                //like名称
                string strCprName = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["name"]);
                if (strCprName == "undefined")
                {
                    strCprName = "";
                }
                //类型
                string type = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["type"]);
                strWhere.Append(" AND  S.QueryCondition like '%" + strCprName.Trim() + "%'");
                if (type != "-1")
                {
                    strWhere.Append(" AND S.MsgType =" + type);
                }

                // string statusSql = " and 1 = 1";
                //if (!string.IsNullOrEmpty(queryEntity.MessageDone))
                //{
                //    if (!string.IsNullOrEmpty(queryEntity.MessageDoneStatus))
                //    {
                //        statusSql += " and s.IsDone = N'" + queryEntity.MessageDoneStatus + "'";
                //    }
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(queryEntity.MessageStatus))
                //    {
                //        statusSql += " and s.Status = N'" + queryEntity.MessageStatus + "'";
                //    }
                //}

                //string msgTypeSql = " and 1 = 1 ";
                //if (!string.IsNullOrEmpty(queryEntity.MessageType))
                //{
                //    msgTypeSql += " and s.MsgType=" + queryEntity.MessageType;
                //}
                //string msgQueryConditionSql = " and 1 = 1 ";
                //if (!string.IsNullOrEmpty(queryEntity.QueryCondition))
                //{
                //    msgQueryConditionSql += " and s.QueryCondition like N'%" + queryEntity.QueryCondition + "%' ";
                //}

                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = String.Empty
                };
                parameters.Where = strWhere.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
        }

        public List<TG.Model.cm_Role> GetRoleByUserSysNo(int userSysNo)
        {
            List<TG.Model.cm_Role> sourceRoleList = new TG.BLL.cm_Role().GetRoleList();

            sourceRoleList = sourceRoleList.Where(delegate(TG.Model.cm_Role role)
            {
                string[] userArray = role.Users.Split(',');
                if (userArray.Contains(userSysNo.ToString()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }).ToList<TG.Model.cm_Role>();

            return sourceRoleList;
        }

        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];

            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
            //return null;
        }

        protected string GetStringRole(int userSysNo)
        {

            return "";
        }
        protected string GetString(string item)
        {
            TG.BLL.tg_unit bllunit = new TG.BLL.tg_unit();
            return bllunit.GetModel(int.Parse(item)).unit_Name;
        }
        public bool IsReusable
        {
            get { throw new NotImplementedException(); }
        }
    }
}