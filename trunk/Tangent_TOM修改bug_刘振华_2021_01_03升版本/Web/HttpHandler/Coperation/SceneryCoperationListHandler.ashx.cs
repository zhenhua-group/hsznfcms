﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    /// <summary>
    /// SceneryCoperationListHandler 的摘要说明
    /// </summary>
    public class SceneryCoperationListHandler1 : SceneryCoperationListHandler
    {
        public override string ProcName
        {
            get
            {
                return "P_cm_SeneryCoperation_jq";
            }
        }

        public override string ProcName2
        {
            get
            {
                return "P_cm_CoperationEdit_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_schSceneryCoperation_jq";
            }
        }
    }
}