﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    public abstract class ConstruCoperationListHandler : IHttpHandler
    {
        //存储过程名
        public abstract string ProcName { get; }
        //合同申请修改存储过程名
        public abstract string ProcName2 { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                string year = DateTime.Now.Year.ToString();
                string sqlwhere = "";
                //年份
                if (year != "-1")
                {
                    sqlwhere = " AND year(cpr_SignDate)=" + year;
                }
                sqlwhere = sqlwhere + strWhere;
                parameters.Where = sqlwhere;
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                string strResponse = "";
                TG.BLL.cm_ConstruCoperartion bll = new TG.BLL.cm_ConstruCoperartion();
                TG.Model.cm_ConstruCoperartion model = new TG.Model.cm_ConstruCoperartion();
                string cstid = forms.Get("EmpId");

                ArrayList listAudit = bll.DeleteDesignList(cstid);
                //存在审批中的合同
                if (listAudit.Count > 0)
                {
                    string message = "";

                    for (int i = 0; i < listAudit.Count; i++)
                    {
                        model = bll.GetModel(int.Parse(listAudit[i].ToString()));
                        if (model != null)
                        {
                            message = message + model.cpr_Name + "合同在审批中，或者审批完毕\r\n ";
                        }
                    }
                    message = message + "不能删除";
                    strResponse = message;
                }
                else
                {
                    strResponse = "删除成功";
                }

                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                string cprtype = context.Request.QueryString["cprtype"] ?? "";//合同分类
                string buildunit = context.Request.QueryString["buildunit"] ?? "";//建设单位
                string account1 = context.Request.QueryString["account1"] ?? "";//合同额区间
                string account2 = context.Request.QueryString["account2"] ?? "";
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    unit = unit.Substring(0, unit.Length - 1);
                    strsql.AppendFormat(" AND (cpr_Unit in ({0})) ", unit.Trim());
                }
                //年份
                if (year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                //合同分类
                if (!string.IsNullOrEmpty(cprtype) && !cprtype.Contains("选择"))
                {
                    strsql.AppendFormat(" AND cpr_Type='{0}' ", cprtype);
                }
                //按建设单位查询
                if (buildunit.Trim() != "")
                {
                    buildunit = TG.Common.StringPlus.SqlSplit(buildunit);
                    strsql.AppendFormat(" AND BuildUnit like '%{0}%'", buildunit);
                }
                //合同额
                if (!string.IsNullOrEmpty(account1) && string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount>={0} ", account1);
                }
                else if (string.IsNullOrEmpty(account1) && !string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount<={0} ", account2);
                }
                else if (!string.IsNullOrEmpty(account1) && !string.IsNullOrEmpty(account2))
                {
                    strsql.AppendFormat(" AND cpr_Acount between {0} and {1} ", account1, account2);
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            //高级查询
            else if (strAction == "Search")
            {
                StringBuilder sb = new StringBuilder("");
                //查询方式
                string andor = context.Request["andor"] ?? "and";
                //合同年份
                string year = context.Request["drp_year"];
                //合同编号
                string txtcpr_No = context.Request["txtcpr_No"];
                //合同分类
                string drp_type = context.Request["drp_type"];
                //合同名称
                string txt_cprname = context.Request["txt_cprname"];
                //建筑规模1
                 string txt_Area = context.Request["txt_Area"];
                //建筑规模2
                 string txt_Area2 = context.Request["txt_Area2"];
                //建设单位
                string txt_cprBuildUnit = context.Request["txt_cprBuildUnit"];

                //建筑类别
                string drp_buildtype = context.Request["drp_buildtype"];
                //工程负责人
                string txt_proFuze = context.Request["txt_proFuze"];
                //甲方负责人
                string txtFParty = context.Request["txtFParty"];
                //承接部门
                string drp_unit = context.Request["drp_unit"];
                //合同额1
                string txtcpr_Account = context.Request["txtcpr_Account"];
                //合同额2
                string txtcpr_Account2 = context.Request["txtcpr_Account2"];
                //工程地点
                string txt_Address = context.Request["txt_Address"];
                //行业性质
                string ddType = context.Request["ddType"];
                //工程来源
                string ddSourceWay = context.Request["ddSourceWay"];
                //合同签订日期1
                string txt_signdate_1 = context.Request["txt_signdate_1"];
                //合同签订日期2
                string txt_signdate2_1 = context.Request["txt_signdate2_1"];
                //合同完成日期1
                string txt_finishdate = context.Request["txt_finishdate"];
                //合同完成日期2
                string txt_finishdate2 = context.Request["txt_finishdate2"];
                //合同统计年份1
                string txt_signdate = context.Request["txt_signdate"];
                //合同统计年份2
                string txt_signdate2 = context.Request["txt_signdate2"];
              
                //按照客户
                string txt_cst = context.Request["txt_cst"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                //合同编号
                if (!string.IsNullOrEmpty(txtcpr_No))
                {
                    sb.Append("  and ( cpr_No like '%" + txtcpr_No.Trim() + "%' ");
                }
                //合同分类
                if (!string.IsNullOrEmpty(drp_type) && !drp_type.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.Append(" cpr_Type='" + drp_type.Trim() + "' ");
                }
                //合同名称

                if (!string.IsNullOrEmpty(txt_cprname))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_cprname.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Name LIKE '%" + keyname + "%' ");
                }
                //建筑规模1
                //建筑规模2
                if (!string.IsNullOrEmpty(txt_Area) && string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Convert(float,BuildArea) >=" + txt_Area.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txt_Area) && !string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Convert(float,BuildArea) <=" + txt_Area2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txt_Area) && !string.IsNullOrEmpty(txt_Area2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (Convert(float,BuildArea) >=" + txt_Area.Trim() + " AND Convert(float,BuildArea) <=" + txt_Area2.Trim() + ") ");
                }
                //建设单位
                if (!string.IsNullOrEmpty(txt_cprBuildUnit))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildUnit like '%" + txt_cprBuildUnit.Trim() + "%' ");
                }

                //建筑类别
                if (!string.IsNullOrEmpty(drp_buildtype) && !drp_buildtype.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildType =N'" + drp_buildtype.Trim() + "' ");
                }
                //工程负责人
                if (!string.IsNullOrEmpty(txt_proFuze))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_proFuze.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ChgPeople='" + keyname + "' ");
                }
                //甲方负责人
                if (!string.IsNullOrEmpty(txtFParty))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txtFParty.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ChgJia='" + keyname + "' ");
                }
                //年份
                if (year != "-1" && !string.IsNullOrEmpty(year))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.AppendFormat(" year(cpr_SignDate)={0} ", year);
                }
                //承接部门
                if (!string.IsNullOrEmpty(drp_unit) && !drp_unit.Contains("全院部门"))
                {
                    drp_unit = drp_unit.Substring(0, drp_unit.Length - 1);

                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_Unit in (" + drp_unit.Trim() + ")) ");
                }
                //合同额1
                //合同额2
                if (!string.IsNullOrEmpty(txtcpr_Account) && string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Acount >= " + txtcpr_Account.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtcpr_Account) && !string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Acount <= " + txtcpr_Account2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtcpr_Account) && !string.IsNullOrEmpty(txtcpr_Account2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_Acount>=" + txtcpr_Account.Trim() + " AND cpr_Acount <= " + txtcpr_Account2.Trim() + ") ");
                }
                //工程地点
                if (!string.IsNullOrEmpty(txt_Address))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildPosition like '%" + txt_Address.Trim() + "%' ");
                }
                //行业性质
                if (!string.IsNullOrEmpty(ddType) && !ddType.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Industry='" + ddType.Trim() + "' ");
                }

                //工程来源
                if (!string.IsNullOrEmpty(ddSourceWay) && !ddSourceWay.Contains("选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildSrc='" + ddSourceWay.Trim() + "' ");
                }
                //合同签订时间1
                //合同签订时间2
                if (!string.IsNullOrEmpty(txt_signdate_1) && string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate2 >= '" + txt_signdate_1.Trim() + "' ");
                }
                else if (string.IsNullOrEmpty(txt_signdate_1) && !string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate2 <= '" + txt_signdate2_1.Trim() + "' ");
                }
                else if (!string.IsNullOrEmpty(txt_signdate_1) && !string.IsNullOrEmpty(txt_signdate2_1))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_SignDate2 BETWEEN '" + txt_signdate_1.Trim() + "' AND '" + txt_signdate2_1.Trim() + "') ");
                }
                //合同统计年份1
                //合同统计年份2
                if (!string.IsNullOrEmpty(txt_signdate) && string.IsNullOrEmpty(txt_signdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate >= '" + txt_signdate.Trim() + "' ");
                }
                else if (string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_SignDate <= '" + txt_signdate2.Trim() + "' ");
                }
                else if (!string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_SignDate BETWEEN '" + txt_signdate.Trim() + "' AND '" + txt_signdate2.Trim() + "') ");
                }
                //合同完成日期1
                //合同完成日期2
                 if (!string.IsNullOrEmpty(txt_finishdate) && string.IsNullOrEmpty(txt_finishdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_DoneDate >= '" + txt_finishdate.Trim() + "' ");
                }
                else if (string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_DoneDate <= '" + txt_finishdate2.Trim() + "' ");
                }
                else if (!string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_DoneDate BETWEEN '" + txt_finishdate.Trim() + "' AND '" + txt_finishdate2.Trim() + "') ");
                }
               
                //按照客户
                if (!string.IsNullOrEmpty(txt_cst))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_cst.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cst_Id IN (Select Cst_Id From cm_CustomerInfo Where Cst_Name like '%" + keyname + "%') ");
                }
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" (InsertDate between '{0}' and '{1}') ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                if (sb.ToString() != "")
                {
                    sb.Append(")");
                }

                sb.Append(strWhere);
                parameters.Where = sb.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());

            }
            else if (strAction == "Audit")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];

                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (cpr_Unit= '{0}')", unit);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }
                // strsql.Append("AND cpr_Type not  like'%项目协议%'");
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "AuditEdit")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];

                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND cpr_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (cpr_Unit= '{0}')", unit);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }

                // strsql.Append("AND cpr_Type not  like'%项目协议%'");
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                strsql.Append(strWhere);

                parameters.ProcedureName = ProcName2;
                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}