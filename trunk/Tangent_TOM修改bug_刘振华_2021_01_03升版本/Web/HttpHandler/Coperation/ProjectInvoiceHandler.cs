﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.Coperation
{
    public abstract class ProjectInvoiceHandler:IHttpHandler
    {
        //存储过程名
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere =context.Request.QueryString["strwhere"];
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                string strResponse = "";
                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {
                StringBuilder strsql = new StringBuilder("");
                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string starttime = context.Request.QueryString["starttime"];
                string endtime = context.Request.QueryString["endtime"];
               
                //名字不为空
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname.Trim());
                    strsql.Append(" AND cpr_Name LIKE '%" + keyname + "%'  ");
                }
                //绑定单位
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.Append(" AND cpr_Unit='" + unit.Trim() + "' ");
                }
                //按照年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(cpr_SignDate)={0} ", year);
                }
               

                //收费时间判断
                string start = starttime.Trim();
                string end = endtime.Trim();
                string curtime = DateTime.Now.ToString("yyyy-MM-dd");

                if (start == "" && end != "")
                {
                    start = curtime;
                }
                else if (start != "" && end == "")
                {
                    end = curtime;
                }
                else if (start == "" && end == "")
                {
                    start = curtime;
                    end = curtime;
                }
                //实际收款时间
                string sqltime = string.Format(" and InvoiceDate Between '{0}' AND '{1}'", start, end);
                strsql.Append(strWhere);
                parameters.Where = strsql.ToString() + "|" + sqltime;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
          

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}