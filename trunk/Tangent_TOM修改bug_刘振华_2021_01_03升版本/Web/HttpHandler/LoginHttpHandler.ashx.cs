﻿
using NetLockBLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// LoginHttpHandler 的摘要说明
    /// </summary>
    public class LoginHttpHandler : IHttpHandler, IRequiresSessionState
    {

        private static TG.BLL.tg_member _bll_user;
        //静态初始化
        static LoginHttpHandler()
        {
            _bll_user = new TG.BLL.tg_member();
        }
        /// <summary>
        /// 请求对象
        /// </summary>
        protected HttpResponse Response
        {
            get
            {
                return HttpContext.Current.Response;
            }
        }
        /// <summary>
        /// 返回对象
        /// </summary>
        protected HttpRequest Request
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }
        /// <summary>
        /// 登录名
        /// </summary>
        protected string UserLogin
        {
            get
            {
                string username = Request.Form.Get("username");
                if (!string.IsNullOrEmpty(username))
                {
                    return System.Web.HttpUtility.UrlDecode(username);
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 用户密码
        /// </summary>
        protected string UserPwd
        {
            get
            {
                string pwd = Request.Form.Get("userpwd");
                if (!string.IsNullOrEmpty(pwd))
                {
                    return pwd;
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 忘记密码
        /// </summary>
        protected string Action
        {
            get
            {
                return Request.Form.Get("action");
            }
        }
        /// <summary>
        /// 加密key
        /// </summary>
        private const int key = 1314;
        /// <summary>
        /// 重置
        /// </summary>
        protected TG.Model.cm_ResetPassword ResetModel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        protected TG.BLL.cm_ResetPassword ResetBll
        {
            get
            {
                return new BLL.cm_ResetPassword();
            }
        }
        /// <summary>
        /// 秘钥
        /// </summary>
        public string IsCheckNetLockPwd
        {
            get
            {
                return ConfigurationManager.AppSettings["IsCheckNetLockPwd"];
            }
        }
        //网络锁
        public string ConfigNetLockServerIP
        {
            get
            {
                return ConfigurationManager.AppSettings["NetLockServerIP"];
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            if (!string.IsNullOrEmpty(Action))
            {
                //重置密码
                if (Action == "repwd")
                {
                    //查询是否已经发送消息
                    string strWhere = string.Format(" mem_Login='{0}'", UserLogin);
                    List<TG.Model.cm_ResetPassword> list = ResetBll.GetModelList(strWhere);
                    if (list.Count == 0)
                    {
                        //添加
                        this.ResetModel = new Model.cm_ResetPassword();
                        this.ResetModel.InsertTime = DateTime.Now;
                        this.ResetModel.IsReset = 0;
                        this.ResetModel.mem_Login = UserLogin;
                        //添加
                        int InsertID = new TG.BLL.cm_ResetPassword().Add(this.ResetModel);
                        if (InsertID > 0)
                        {
                            Response.Write("{\"result\":\"0\",\"msg\":\"重置申请已发送成功！\"}");
                        }
                        else
                        {
                            Response.Write("{\"result\":\"0\",\"msg\":\"发送失败！\"}");
                        }
                    }
                    else
                    {
                        Response.Write("{\"result\":\"0\",\"msg\":\"已发送重置申请，不能重复发送！\"}");
                    }
                }
                else if (Action == "login")
                {
                    HaspLock hasp_lock = new HaspLock();

                    //服务器
                    hasp_lock.ServerIP = this.ConfigNetLockServerIP;
                    //TOM
                    hasp_lock.ProductID = "1400";
                    hasp_lock.FeatureID = 1401;
                    hasp_lock.ConfigCode = this.IsCheckNetLockPwd;

                    try
                    {
                        if (hasp_lock.CheckNotLockSuccess())
                        {
                            if (!string.IsNullOrEmpty(UserLogin))
                            {
                                //超级管理员
                                if (UserLogin == "administrator" && UserPwd == "sjy123456")
                                {
                                    #region 用户登录存储cookie

                                    //判断用户是否存在
                                    if (Request.Cookies["userinfo"] == null)
                                    {
                                        //创建Cookie
                                        HttpCookie userinfo = new HttpCookie("userinfo");
                                        //设置时间一个月
                                        userinfo.Expires = DateTime.Now.AddDays(30);

                                        userinfo.Values.Add("memid", "1");
                                        userinfo.Values.Add("memlogin", "admin");
                                        userinfo.Values.Add("principal", "1");

                                        Response.AppendCookie(userinfo);
                                    }
                                    else
                                    {
                                        //修改Cookie
                                        HttpCookie userinfo = Response.Cookies["userinfo"];

                                        if (userinfo != null)
                                        {
                                            userinfo.Values["memid"] = "1";
                                            userinfo.Values["memlogin"] = "admin";
                                            userinfo.Values["principal"] = "1";

                                            Response.SetCookie(userinfo);
                                        }
                                    }
                                    #endregion

                                    Response.Write("{\"result\":\"0\",\"msg\":\"登录成功！\"}");
                                }
                                else
                                {
                                    string strWhere = string.Format(" mem_Login='{0}'", UserLogin);
                                    //查询用户实体
                                    List<TG.Model.tg_member> list = _bll_user.GetModelList(strWhere);

                                    if (list.Count > 0)
                                    {
                                        TG.Model.tg_member usermodel = list[0];
                                        //密码
                                        string userpwd = GetPwdEncryString(UserPwd);
                                        //比较登录
                                        if (userpwd == usermodel.mem_Password)
                                        {
                                            #region 用户登录存储cookie

                                            //判断用户是否存在
                                            if (Request.Cookies["userinfo"] == null)
                                            {
                                                //创建Cookie
                                                HttpCookie userinfo = new HttpCookie("userinfo");
                                                //设置时间一个月
                                                userinfo.Expires = DateTime.Now.AddDays(30);

                                                userinfo.Values.Add("memid", usermodel.mem_ID.ToString());
                                                userinfo.Values.Add("memlogin", usermodel.mem_Login);
                                                userinfo.Values.Add("principal", usermodel.mem_Principalship_ID.ToString());
                                                //添加
                                                Response.AppendCookie(userinfo);
                                            }
                                            else
                                            {
                                                //修改Cookie
                                                HttpCookie userinfo = Response.Cookies["userinfo"];

                                                if (userinfo != null)
                                                {
                                                    userinfo.Values["memid"] = usermodel.mem_ID.ToString();
                                                    userinfo.Values["memlogin"] = usermodel.mem_Login;
                                                    userinfo.Values["principal"] = usermodel.mem_Principalship_ID.ToString();

                                                    Response.SetCookie(userinfo);
                                                }
                                            }

                                            #endregion

                                            Response.Write("{\"result\":\"0\",\"msg\":\"登录成功！\",\"status\":\"" + usermodel.mem_Status + "\"}");
                                        }
                                    }
                                    else
                                    {
                                        Response.Write("{\"result\":\"1\",\"msg\":\"用户不存在！\",\"status\":\"-1\"}");
                                    }
                                }
                            }

                        }
                        else
                        {
                            Response.Write("{\"result\":\"-2\",\"msg\":\"网络锁登陆失败，请联系管理员！\"}");
                        }
                    }
                    catch
                    {
                        Response.Write("{\"result\":\"-2\",\"msg\":\"网络锁登陆失败，请联系管理员！\"}");
                    }
                }
            }
        }
        /// <summary>
        /// 加密密码
        /// </summary>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public string GetPwdEncryString(string pwd)
        {
            string str_rlt = "";
            string str_array = pwd;
            for (int i = 0; i < str_array.Length; i++)
            {
                char a = Convert.ToChar(str_array.Substring(i, 1));
                str_rlt += (char)((int)a ^ (key >> 8));
            }

            str_array = str_rlt;
            str_rlt = "";
            string str_rlt2 = "";
            int index = 0;
            for (int i = 0; i < str_array.Length; i++)
            {
                str_rlt2 = "";
                index = Convert.ToByte(Convert.ToChar(str_array.Substring(i, 1)));
                str_rlt2 += (char)(65 + index / 26);
                str_rlt2 += (char)(65 + index % 26);
                str_rlt += str_rlt2;
            }

            return str_rlt;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}