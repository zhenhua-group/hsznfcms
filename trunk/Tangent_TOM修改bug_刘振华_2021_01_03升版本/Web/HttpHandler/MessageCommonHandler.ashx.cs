﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TG.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// MessageCommonHandler 的摘要说明
    /// </summary>
    public class MessageCommonHandler : HandlerCommon, IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                SendMessage();
            }
        }


        private void SendMessage()
        {
            MessageTempClass messageTempClass = Newtonsoft.Json.JsonConvert.DeserializeObject<MessageTempClass>(Request["jsonData"]);
            messageTempClass.MessageTemplate.InDate = DateTime.Now;
            messageTempClass.MessageTemplate.ToRole = "0";

            int count = 0;
            TG.BLL.cm_SysMsg bp = new BLL.cm_SysMsg();

            messageTempClass.UserList.ForEach(u =>
            {
                messageTempClass.MessageTemplate.FromUser = u.UserSysNo;
                count = bp.InsertSysMessage(messageTempClass.MessageTemplate);
            });
            if (count > 0)
            {
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class MessageTempClass
        {
            public List<UserTempClass> UserList { get; set; }

            public SysMessageViewEntity MessageTemplate { get; set; }
        }

        public class UserTempClass
        {
            public int UserSysNo { get; set; }

            public string UserName { get; set; }
        }
    }
}