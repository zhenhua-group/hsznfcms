﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TG.Web.HttpHandler
{
	/// <summary>
	/// $codebehindclassname$ 的摘要说明
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	public class DropDownListCountryHandler : HandlerCommon, IHttpHandler
	{
		/// <summary>
		/// 操作类型，2为得到省份，3为得到城市
		/// </summary>
		public string Action
		{
			get
			{
				return Request["Action"];
			}
		}

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";

			switch (Action)
			{
				case "2":
					GetProvinceList();
					break;
				case "3":
					GetCityList();
					break;
			}
			Response.End();
		}

		/// <summary>
		/// 得到省份
		/// </summary>
		private void GetProvinceList()
		{
			string countryID = Request["countryID"];

			if (!string.IsNullOrEmpty(countryID))
			{
				List<TG.Model.Province> provinceList = new TG.BLL.DropDownListCountryBP().GetProvinceList(countryID);

				string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(provinceList);
				Response.Write(jsonResult);
			}
			else
			{
				Response.Write("0");
			}
		}


		private void GetCityList()
		{
			string provinceID = Request["provinceID"];
			if (!string.IsNullOrEmpty(provinceID))
			{
				List<TG.Model.City> cityList = new TG.BLL.DropDownListCountryBP().GetCitys(provinceID);

				string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(cityList);
				Response.Write(jsonResult);
			}
			else
			{
				Response.Write("0");
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}
