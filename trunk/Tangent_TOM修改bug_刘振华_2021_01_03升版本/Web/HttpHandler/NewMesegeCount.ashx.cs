﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

using System.Web.SessionState;


namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class NewMesegeCount : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //2016-1-12 qpl
            HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];

            string action = context.Request["action"];
            int userNo = user["memid"] == null ? 1 : Convert.ToInt32(user["memid"].ToString());

            if (action == "1")
            {
                TG.BLL.cm_SysMsg msg = new TG.BLL.cm_SysMsg();
                int msgCount = msg.GetSysMsgCount(userNo);

                context.Response.Write(msgCount);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
