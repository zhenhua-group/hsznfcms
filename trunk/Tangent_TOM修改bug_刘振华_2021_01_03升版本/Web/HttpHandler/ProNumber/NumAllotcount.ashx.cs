﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace TG.Web.HttpHandler.ProNumber
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class NumAllotcount : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request["action"];
            if (action == "2")
            {
                TG.BLL.cm_projectNumber pro = new TG.BLL.cm_projectNumber();
                string count = pro.ProNumcount().ToString();
                context.Response.Write(count);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
