﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace TG.Web.HttpHandler.ProNumber
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProAuditLocus : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string audit_id = context.Request["audit_No"];

            TG.Model.ProjectAuditDataEntity audit = TG.BLL.tg_ProjectAuditBP.GetProjectAuditEntity(int.Parse(audit_id));

            string result = "";
            string date = audit.AudtiDate == null ? "" : audit.AudtiDate.Trim();
            result = date + "|";
            string sug = audit.Suggestion == null ? "" : audit.Suggestion.Trim();
            result += sug + "|";
            //显示评审人姓名
            string auditUser = audit.AuditUser == null ? "" : audit.AuditUser.Trim().Substring(0, audit.AuditUser.Length - 1);
            string auditUserName = "";
            string users = "";
            string[] user = auditUser.Split(',');
            TG.BLL.tg_member mem = new TG.BLL.tg_member();
            for (int i = 0; i < user.Length; i++)
            {
                TG.Model.tg_member mem_model = mem.GetModel(int.Parse(user[i]));
                auditUserName = mem_model.mem_Name == null ? "" : mem_model.mem_Name.Trim();
                users += auditUserName + ",";
            }
            result += users + "|";
            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
