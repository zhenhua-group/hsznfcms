﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace TG.Web.HttpHandler.ProNumber
{
    /// <summary>
    /// ProPassListHandler1 的摘要说明
    /// </summary>
    public class ProPassListHandler1 : ProPassListHandler
    {

        public override string ProcName
        {
            get
            {
                return "P_cm_Project_Audit_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_ProjectNumber_jq";
            }
        }
    }
}