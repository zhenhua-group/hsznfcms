﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using TG.BLL;
using TG.Model;
using System.Collections.Generic;
using System.Linq;

namespace TG.Web.HttpHandler.ProNumber
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class NumApplycount : HandlerCommon,IHttpHandler
    {


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string rel = "";
            //立项通过的项目数
            string counrt = TG.BLL.tg_ProjectAuditBP.ProjectCount().ToString();
            rel += counrt + ",";
            TG.BLL.cm_projectNumber pro = new TG.BLL.cm_projectNumber();
            string count = pro.ProNumcount().ToString();
            rel += counrt + ",";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, "ProjectList.aspx");
            int previewPattern = (from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0 ? 1 : 0;

            string whereSql = string.Empty;

            if (previewPattern == 0)
            {
                whereSql += " and InsertUserID=" + UserSysNo;
            }

            string proCount = new TG.BLL.cm_Project().GetSetUpProjectCount(whereSql).ToString();
            rel += proCount;
            context.Response.Write(rel);
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
