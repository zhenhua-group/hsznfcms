﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using TG.Model;

namespace TG.Web.HttpHandler.ProNumber
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class NumApplyHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string proid = context.Request.Params["pro_id"];
            string applyuser = context.Request.Params["user"];
            string action = context.Request.Params["action"] ?? "0";
            string flag = context.Request.Params["flag"] ?? "0";
            string result = "0";

            TG.Model.cm_projectNumber model_num = new TG.Model.cm_projectNumber();
            TG.BLL.cm_projectNumber bll_num = new TG.BLL.cm_projectNumber();
            if (action == "0")
            {
                //发起流程
                if (flag == "0")
                {
                    //给配置流程用户发消息
                    TG.Model.cm_ProjectDistributionJobNumberConfigEntity distributionJobNumberConfig = new TG.BLL.cm_ProjectDistributionJobNumberConfigBP().GetDistributionJobNumberConfig(1);
                    //接受用户和用不角色ID
                    string roleUserString = CommonAudit.GetRoleName(distributionJobNumberConfig.RoleSysNo);
                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        result = roleUserString;
                    }
                    else
                    {
                        result = "0";
                    }
                }
                else
                {
                    bool isExsit = bll_num.Exists(int.Parse(proid));
                    if (isExsit)
                    {
                        result = "1";
                    }
                    else
                    {
                        model_num.pro_id = int.Parse(proid);
                        model_num.SubmitDate = DateTime.Now;
                        model_num.SubmintPerson = applyuser;
                        //返回id
                        int proNumSysNo = bll_num.Add(model_num);
                        //审批配置
                        TG.Model.cm_ProjectDistributionJobNumberConfigEntity distributionJobNumberConfig = new TG.BLL.cm_ProjectDistributionJobNumberConfigBP().GetDistributionJobNumberConfig(1);
                        //发消息到分配工号角色
                        TG.BLL.cm_Project project = new TG.BLL.cm_Project();
                        TG.Model.cm_Project pro_modol = project.GetModel(int.Parse(proid));

                        SysMessageViewEntity sysMessage = new SysMessageViewEntity
                        {
                            ReferenceSysNo = "pro_id=" + proid + "&pronum_id=" + proNumSysNo + "&FromUser=" + int.Parse(applyuser),
                            MsgType = 3,
                            InUser = int.Parse(applyuser),
                            FromUser = int.Parse(applyuser),
                            MessageContent = string.Format("关于 \"{0}\"的工号申请消息！", pro_modol.pro_name),
                            QueryCondition = pro_modol.pro_name,
                            IsDone = "A"
                        };

                        //审批角色ID
                        string roleSysNo = distributionJobNumberConfig.RoleSysNo.ToString();
                        sysMessage.ToRole = roleSysNo;
                        //用户消息实体
                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessage);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            result = sysMsgString;
                        }
                    }
                }
            }
            else
            {
                //更新状态
            }

            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
