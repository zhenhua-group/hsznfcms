﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.ProjectMgr
{
    /// <summary>
    /// ExistsInTGProjectHandler 的摘要说明
    /// </summary>
    public class ExistsInTGProjectHandler : HandlerCommon, IHttpHandler
    {
        public string ProjectName
        {
            get
            {
                return Request["projectname"];
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            bool flag = false;
            string action = Request["action"] ?? "";
            string sysno = Request["sysno"] ?? "0";
            //普通合同
            if (action == "cpr")
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_Coperation().Exists(cprname, sysno);
            }
            else if (action == "Engineercpr")//市政合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_EngineerCoperation().Exists(cprname, sysno);
            }
            else if (action == "Planningcpr")//规划工程合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_PlanningCoperation().Exists(cprname, sysno);
            }
            else if (action == "Scenerycpr")//景观规划合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_SceneryCoperation().Exists(cprname, sysno);
            }
            else if (action == "Supercpr")//监理合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_SuperCoperation().Exists(cprname, sysno);
            }
            else if (action == "Reconncpr")//岩石工程勘察合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_ReconnCoperation().Exists(cprname, sysno);
            }
            else if (action == "Designcpr")//岩石工程设计合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_DesignCoperation().Exists(cprname, sysno);
            }
            else if (action == "Pitcpr")//基坑和边坡检测合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_PitCoperation().Exists(cprname, sysno);
            }
            else if (action == "Testcpr")//测量及沉降观测合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_MeasureCoperation().Exists(cprname, sysno);
            }
            else if (action == "Measurecpr")//岩土工程检测合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_TestCoperation().Exists(cprname, sysno);
            }
            else if (action == "Construcpr")//岩土工程施工合同
            {
                string cprname = Request["cprname"];
                flag = new TG.BLL.cm_ConstruCoperartion().Exists(cprname, sysno);
            }
            else
            {
                flag = new TG.BLL.cm_Project().ExistsInTGProject(ProjectName, sysno);
            }
            
            Response.Write(flag == true ? "1" : "0");
            Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}