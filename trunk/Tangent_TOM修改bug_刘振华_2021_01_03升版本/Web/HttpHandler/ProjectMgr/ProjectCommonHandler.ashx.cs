﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using TG.BLL;

namespace TG.Web.HttpHandler.ProjectMgr
{
	/// <summary>
	/// $codebehindclassname$ 的摘要说明
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	public class ProjectCommonHandler : HandlerCommon, IHttpHandler
	{
		/// <summary>
		/// 跳转动作
		/// </summary>
		public string Action
		{
			get
			{
				string action = "";
				action = Request["action"];
				return action;
			}
		}


		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			if (Action == "1")
			{
				GetSetUpProjectCount();
			}
			Response.End();
		}

		/// <summary>
		/// 得到立项信息数
		/// </summary>
		private void GetSetUpProjectCount()
		{
			string count = new TG.BLL.cm_Project().GetSetUpProjectCount("").ToString();
			Response.Write(count);
		}
		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}
