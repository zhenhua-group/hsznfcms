﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aspose.Words;
using System.IO;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// IsoToWord 的摘要说明
    /// </summary>
    public class IsoToWord : IHttpHandler
    {
        /// <summary>
        /// 审核记录
        /// </summary>
        public TG.Model.cm_CoperationAudit coper { get; set; }
        /// <summary>
        /// 客户基本信息
        /// </summary>
        public TG.Model.cm_CustomerInfo CustomerInfo { get; set; }
        /// <summary>
        /// 合同系统自增号
        /// </summary>
        public int CoperationSysNo { get; set; }
        /// <summary>
        /// 合同系统自增号
        /// </summary>
        public int AuditSysno { get; set; }
        /// <summary>
        /// 合同基本信息
        /// </summary>
        public TG.Model.cm_CoperationAuditListView CoperationAuditEntity { get; set; }
        TG.BLL.cm_Coperation copbll = new BLL.cm_Coperation();
        TG.BLL.cm_CoperationAudit auditBll = new BLL.cm_CoperationAudit();
        TG.BLL.cm_SubCoperation subBll = new BLL.cm_SubCoperation();
        TG.BLL.cm_Project cm_proBll = new TG.BLL.cm_Project();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/msword";
            //标示
            string option = context.Request.Params["option"];
            //项目TCD里的tg_project的id
            string proSysNo = context.Request.Params["sysno"];
            //设计阶段
            string status = context.Request.Params["status"];
            //项目对象
            TG.Model.cm_Project ProModel = cm_proBll.GetModelList("ReferenceSysNo=" + proSysNo)[0];
            //项目策划对象id
            string proplan_auditsql = @"SELECT SysNo FROM cm_ProjectPlanAudit WHERE ProjectSysNo=" + ProModel.bs_project_Id;
            object proplan_o = TG.DBUtility.DbHelperSQL.GetSingle(proplan_auditsql);
            int proplan_AuditNo = 0;
            if (proplan_o != null)
            {
                proplan_AuditNo = Convert.ToInt32(proplan_o.ToString());
            }
            //合同id
            CoperationSysNo = Convert.ToInt32(ProModel.cprID);
            //合同对象
            TG.Model.cm_Coperation CprModel = copbll.GetModel(CoperationSysNo);
            //合同审批id
            string cpr_auditsql = @"SELECT SysNo FROM cm_AuditRecord WHERE CoperationSysNo=" + CoperationSysNo;
            object cpr_o = TG.DBUtility.DbHelperSQL.GetSingle(cpr_auditsql);
            int cpr_AuditNo = 0;
            if (cpr_o != null)
            {
                cpr_AuditNo = Convert.ToInt32(cpr_o.ToString());
            }
            //项目审批id
            string pro_auditsql = @"SELECT SysNo FROM cm_ProjectAudit WHERE ProjectSysNo=" + ProModel.bs_project_Id;
            object pro_o = TG.DBUtility.DbHelperSQL.GetSingle(pro_auditsql);
            int pro_AuditNo = 0;
            if (pro_o != null)
            {
                pro_AuditNo = Convert.ToInt32(pro_o.ToString());
            }
            //路径列表
            string cpr_path = context.Server.MapPath(@"~\TemplateWord\QR-7.1-01.doc");
            //判断导出
            //1合同评审
            if (option == "outAudit")
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(cpr_path); //载入模板

                ToWordByAudit(cpr_AuditNo.ToString(), CoperationSysNo.ToString(), cpr_path, out doc);
                doc.Save("《合同评审表》.doc", SaveFormat.Doc, SaveType.OpenInWord, context.Response);
            }
            //2勘察记录
            string pro_path = context.Server.MapPath(@"~\TemplateWord\QR-7.1-04.doc");
            if (option == "outKanCha")
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(pro_path); //载入模板

                ToWordByAudit(ProModel.bs_project_Id.ToString(), CoperationSysNo.ToString(), pro_path, out doc);
                doc.Save("勘察设计任务通知单.doc", SaveFormat.Doc, SaveType.OpenInWord, context.Response);
            }
            //3工程项目进岗人员审批表
            string JGPath = "";
            if (option == "GCJingGang")
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(JGPath); //载入模板

                ToWordByJGAudit(ProModel.bs_project_Id.ToString(), ProModel.pro_level.ToString(), proplan_AuditNo.ToString(), CoperationSysNo.ToString(), status, JGPath, out doc);
                doc.Save("工程项目进岗人员审批表.doc", SaveFormat.Doc, SaveType.OpenInWord, context.Response);
            }
            //4项目进度计划表
            string XMJDPath = context.Server.MapPath(@"~\TemplateWord\QR-7.2-02.doc");
            if (option == "XMJinDu")
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(XMJDPath); //载入模板

                ToWordByJinDU(ProModel.bs_project_Id.ToString(), ProModel.pro_level.ToString(), proplan_AuditNo.ToString(), CoperationSysNo.ToString(), status, XMJDPath, out doc);
                doc.Save("项目进度计划表.doc", SaveFormat.Doc, SaveType.OpenInWord, context.Response);
            }
            if (option == "GKZiLiao")
            {

            }
            if (option == "ShRPSh")
            {

            }
            //合同信息
            GetCoperationInfo();
            //客户信息
            GetCustomerInfo();
        }

        /// <summary>
        /// 导出合同评审表
        /// </summary>

        public void ToWordByAudit(string auditno, string copid, string path, out Aspose.Words.Document doc)
        {
            //throw new NotImplementedException();
            doc = new Aspose.Words.Document(path);
            coper = new TG.BLL.cm_CoperationAudit().GetModelByCoperationSysNo(CoperationSysNo);
            //判断导出那个iso表单，0为合同审核表，1为勘察设计表
            TG.Model.cm_CoperationAudit modelAudit = auditBll.GetModel(int.Parse(auditno));

            List<TG.Model.cm_CoperationAuditListView> listCop = auditBll.GetCoperationListView("AND cpr_Id=" + copid);
            List<TG.Model.cm_SubCoperation> listSubCop = subBll.GetModelList("cpr_Id=" + copid);
            string level = coper.ManageLevel.ToString();
            ///合同审批表
            #region 合同审批

            //审核记录

            //string path = context.Server.MapPath(@"~\TemplateWord\13.QR-7.1-01《合同评审表》.doc");
            //BuildUnit cpr_Id cpr_Name cpr_No cpr_Unit DataTime fourTime fourUnit fristTime fristUnit leaderAdvice leaderTime level productAdvice productTime secondTime secondUnit thirdTime thirdUnit
            if (listCop.Count > 0 && modelAudit != null)
            {
                TG.Model.cm_CoperationAuditListView modelCop = new Model.cm_CoperationAuditListView();
                foreach (TG.Model.cm_CoperationAuditListView item in listCop)
                {
                    if (item.AuditStatus == "J")
                    {
                        modelCop = item;
                    }
                }
                //  2013-06-09 10:15:21,2013-06-09 10:16:34,2013-06-09 10:17:07,2013-06-09 10:17:33,2013-06-09 10:18:22
                //report.InsertValue("BuildUnit", modelCopCop.BuildUnit.Trim());
                if (doc.Range.Bookmarks["BuildUnit"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                    mark.Text = modelCop.BuildUnit == null ? "" : modelCop.BuildUnit.Trim();
                }
                // report.InsertValue("cpr_Id", modelCop.cpr_Id.ToString().Trim());
                if (doc.Range.Bookmarks["cpr_Id"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_Id"];
                    mark.Text = modelCop.cpr_Id.ToString().Trim();
                }
                // report.InsertValue("cpr_Name", modelCop.cpr_Name.Trim());
                if (doc.Range.Bookmarks["cpr_Name"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                    mark.Text = modelCop.cpr_Name == null ? "" : modelCop.cpr_Name.Trim();
                }
                // report.InsertValue("cpr_No", modelCop.cpr_No.Trim());
                if (doc.Range.Bookmarks["cpr_No"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                    mark.Text = modelCop.cpr_No == null ? "" : modelCop.cpr_No.Trim();
                }
                // report.InsertValue("cpr_Unit", modelCop.cpr_Unit.Trim());
                if (doc.Range.Bookmarks["cpr_Unit"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                    mark.Text = modelCop.cpr_Unit == null ? "" : modelCop.cpr_Unit.Trim();
                }
                //report.InsertValue("DataTime", modelAudit.InDate.ToString("yyyy-MM-dd"));
                if (doc.Range.Bookmarks["DataTime"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["DataTime"];
                    mark.Text = modelAudit.InDate.ToString("yyyy-MM-dd");
                }
                // report.InsertValue("fourTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd"));
                if (doc.Range.Bookmarks["fourTime"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["fourTime"];
                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[3]).ToString("yyyy-MM-dd");
                }
                //report.InsertValue("fourUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[4]));
                if (doc.Range.Bookmarks["fourUnit"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["fourUnit"];
                    mark.Text = RoleToCompany(4);
                }
                // report.InsertValue("fristTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                if (doc.Range.Bookmarks["fristTime"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["fristTime"];
                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                }
                // report.InsertValue("fristUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[1]));
                if (doc.Range.Bookmarks["fristUnit"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["fristUnit"];
                    mark.Text = RoleToCompany(1);
                }
                // report.InsertValue("leaderAdvice", modelAudit.GeneralManagerProposal);
                if (doc.Range.Bookmarks["leaderAdvice"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["leaderAdvice"];
                    mark.Text = modelAudit.GeneralManagerProposal == null ? "" : modelAudit.GeneralManagerProposal;
                }
                // report.InsertValue("leaderTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[4]).ToString("yyyy-MM-dd"));
                if (doc.Range.Bookmarks["leaderTime"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["leaderTime"];
                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[4]).ToString("yyyy-MM-dd");
                }
                //report.InsertValue("level", level == "0" ? "一级" : "二级");
                if (doc.Range.Bookmarks["level"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["level"];
                    mark.Text = level == "0" ? "一级" : "二级";
                }
                //report.InsertValue("productAdvice", modelAudit.OperateDepartmentProposal.Replace('|', ' '));
                if (doc.Range.Bookmarks["productAdvice"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["productAdvice"];
                    mark.Text = modelAudit.OperateDepartmentProposal.Replace('|', ' ');
                }
                //report.InsertValue("productTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd"));
                if (doc.Range.Bookmarks["productTime"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["productTime"];
                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[0]).ToString("yyyy-MM-dd");
                }
                //   report.InsertValue("secondTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd"));
                if (doc.Range.Bookmarks["secondTime"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["secondTime"];
                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd");
                }
                // report.InsertValue("secondUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[2]));
                if (doc.Range.Bookmarks["secondUnit"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["secondUnit"];
                    mark.Text = RoleToCompany(2);
                }
                //report.InsertValue("thirdTime", Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd"));
                if (doc.Range.Bookmarks["thirdTime"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["thirdTime"];
                    mark.Text = Convert.ToDateTime(modelAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd");
                }
                // report.InsertValue("thirdUnit", RoleToCompany(modelAudit.AuditUser.Split(',')[3]));
                if (doc.Range.Bookmarks["thirdUnit"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["thirdUnit"];
                    mark.Text = RoleToCompany(3);
                }
                //report.InsertValue("tableMarker", modelCop.TableMaker.Trim());
                if (doc.Range.Bookmarks["tableMarker"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["tableMarker"];
                    mark.Text = modelCop.TableMaker == null ? "" : modelCop.TableMaker.Trim();
                }
                //report.InsertValue("JingBan", modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim());
                if (doc.Range.Bookmarks["JingBan"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["JingBan"];
                    mark.Text = modelCop.UpdateBy == null ? "" : modelCop.UpdateBy.Trim();
                }
            }

            #endregion
        }
        /// <summary>
        /// 导出勘察计划表
        /// </summary>
        public void ToWordByKanCha(string proid, string level, string path2, string copid, out Aspose.Words.Document doc)
        {
            #region 项目审批

            //throw new NotImplementedException();
            doc = new Aspose.Words.Document(path2);
            coper = new TG.BLL.cm_CoperationAudit().GetModelByCoperationSysNo(CoperationSysNo);
            List<TG.Model.cm_CoperationAuditListView> listCop = auditBll.GetCoperationListView("AND cpr_Id=" + copid);
            List<TG.Model.cm_SubCoperation> listSubCop = subBll.GetModelList("cpr_Id=" + copid);
            if (TG.BLL.tg_ProjectAuditBP.GetProjectAuditEntity(int.Parse(copid)) != null)
            {
                TG.Model.cm_CoperationAuditListView modelCop = new Model.cm_CoperationAuditListView();
                foreach (TG.Model.cm_CoperationAuditListView item in listCop)
                {
                    if (item.AuditStatus == "J")
                    {
                        modelCop = item;
                    }
                }
                string subname = "";
                foreach (TG.Model.cm_SubCoperation item in listSubCop)
                {
                    subname += item.Item_Name;
                }

                TG.Model.cm_Project ProjectObj = cm_proBll.GetModel(int.Parse(copid));
                //  2013-06-09 10:15:21,2013-06-09 10:16:34,2013-06-09 10:17:07,2013-06-09 10:17:33,2013-06-09 10:18:22
                //KanChamoney  levle GaiYuSuanmoney cpr_Unit cpr_No cpr_Name cpr_childName buildArea
                //report.InsertValue("KanChamoney", modelCop.cpr_ShijiAcount.ToString().Trim());
                if (doc.Range.Bookmarks["KanChamoney"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["KanChamoney"];
                    mark.Text = "";
                }
                //report.InsertValue("levle", level == "0" ? "一级" : "二级");
                if (doc.Range.Bookmarks["levle"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["levle"];
                    mark.Text = level == "0" ? "院管" : "所管";
                }
                //report.InsertValue("GaiYuSuanmoney", modelCop.cpr_Acount.ToString().Trim());
                if (doc.Range.Bookmarks["GaiYuSuanmoney"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["GaiYuSuanmoney"];
                    mark.Text = "";
                }
                //report.InsertValue("cpr_Unit", modelCop.cpr_Unit.ToString().Trim());
                if (doc.Range.Bookmarks["cpr_Unit"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                    mark.Text = ProjectObj.Unit == null ? "" : ProjectObj.Unit.ToString().Trim();
                }
                if (doc.Range.Bookmarks["shengchanjihua"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["shengchanjihua"];
                    mark.Text = ProjectObj.Unit == null ? "" : ProjectObj.Unit.ToString().Trim();
                }
                //report.InsertValue("cpr_No", modelCop.cpr_No.Trim());
                if (doc.Range.Bookmarks["cpr_No"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                    mark.Text = ProjectObj.ReferenceSysNo == null ? "" : ProjectObj.ReferenceSysNo.ToString().Trim();
                }
                //report.InsertValue("cpr_Name", modelCop.cpr_Name.ToString().Trim());
                if (doc.Range.Bookmarks["cpr_Name"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                    mark.Text = ProjectObj.pro_name == null ? "" : ProjectObj.pro_name.ToString().Trim();
                }
                //report.InsertValue("cpr_childName", subname);
                if (doc.Range.Bookmarks["cpr_childName"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["cpr_childName"];
                    mark.Text = "";
                }
                //report.InsertValue("buildArea", modelCop.BuildArea.ToString().Trim());
                if (doc.Range.Bookmarks["buildArea"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["buildArea"];
                    mark.Text = ProjectObj.ProjectScale == null ? "" : ProjectObj.ProjectScale.ToString().Trim();
                }
            }

            #endregion
        }
        /// <summary>
        /// 导出进岗人员审批表
        /// </summary>

        public void ToWordByJGAudit(string proid, string level, string auditno, string copid, string status, string path3, out Aspose.Words.Document doc)
        {
            #region 工程项目进岗人员审批表

            //先判断有没有所对应的的tcd的存的值。
            //string status = Request.QueryString["status"];

            // string path3 = Server.MapPath(@"~\TemplateWord\2.QR-7.2-01《工程项目进岗人员审批表2》.doc");
            doc = new Aspose.Words.Document(path3); //载入模板
            //书签列表  copid  项目id   level 级别   auditno 审核id
            //得到项目的信息
            TG.Model.cm_Project ProjectModel = cm_proBll.GetModel(int.Parse(copid));
            TG.Model.cm_Coperation CoperModel = copbll.GetModel(ProjectModel.CoperationSysNo);
            //进行判断所对应的的表单id和关键的东西
            int index = -1;
            string[] arrayproject = ProjectModel.pro_status.Split(',');
            for (int i = 0; i < arrayproject.Length; i++)
            {
                if (status == arrayproject[i] || arrayproject[i] == "")
                {
                    index++;
                }
            }
            string biaoshi = "0::" + index;
            string sqlProbii = "SELECT bill_title FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
            object Probii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii);
            //得到工程号
            string sqlByNum = @"
SELECT n.ProNumber FROM cm_Project c LEFT JOIN 
cm_ProjectNumber n ON c.pro_ID=n.Pro_id WHERE c.pro_ID=" + copid;
            object num = TG.DBUtility.DbHelperSQL.GetSingle(sqlByNum);
            TG.BLL.cm_ProjectPlanBP proplan = new TG.BLL.cm_ProjectPlanBP();
            //得到参与人员的信息
            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
            List<TG.Model.ProjectPlanRole> ProjectPlanRoleList = projectPlanBP.GetProjectPlanRoleAndUsers(int.Parse(copid));
            //设计人员
            TG.Model.ProjectDesignPlanRole ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(int.Parse(copid));
            //审核信息
            TG.Model.cm_ProjectPlanAuditEntity projectPlanAudit = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditEntity(new TG.Model.cm_ProjectPlanAuditQueryEntity { ProjectPlanAuditSysNo = int.Parse(auditno) });
            // RepeaterAllUsers.DataSource = new TG.BLL.cm_ProjectPlanBP().GetPlanUsers(ProjectModel.ReferenceSysNo);
            //绑定书签 
            //项目名称
            #region tcd没有进岗信息
            if (Probii == null)
            {
                if (doc.Range.Bookmarks["BM_3002"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3002"];
                    mark.Text = ProjectModel.pro_name == null ? "" : ProjectModel.pro_name.ToString().Trim();
                }
                //工程号
                if (doc.Range.Bookmarks["BM_3003"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3003"];
                    mark.Text = num.ToString().Trim();
                }
                //子项名称
                if (doc.Range.Bookmarks["BM_3004"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3004"];
                    mark.Text = "";
                }
                //管理级别
                if (doc.Range.Bookmarks["BM_3153"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3153"];
                    mark.Text = level == "0" ? "院管" : "所管";
                }
                //功能
                if (doc.Range.Bookmarks["BM_3006"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3006"];
                    mark.Text = "";
                }

                //其他
                if (doc.Range.Bookmarks["BM_3010"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3010"];
                    mark.Text = "";
                }
                //面积
                if (doc.Range.Bookmarks["BM_3007"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3007"];
                    mark.Text = ProjectModel.ProjectScale == null ? "" : ProjectModel.ProjectScale.ToString().Trim();
                }
                //层数
                if (doc.Range.Bookmarks["BM_3008"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3008"];
                    string floor = CoperModel.Floor.Split('|')[0] == null ? "" : "地上：" + CoperModel.Floor.Split('|')[0].Trim() + "";
                    floor += CoperModel.Floor.Split('|')[1] == null ? "" : "地下：" + CoperModel.Floor.Split('|')[1].Trim();
                    mark.Text = floor;
                }
                //结构类型
                if (doc.Range.Bookmarks["BM_3009"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3009"];
                    mark.Text = ProjectModel.pro_StruType == null ? "" : ProjectModel.pro_StruType.ToString().Trim();
                }
                //设计总负责
                if (doc.Range.Bookmarks["BM_3011"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3011"];
                    string usestring = "";
                    ProjectPlanRoleList[0].Users.ForEach((user) =>
                    {
                        usestring += user.UserName + " ";
                    });
                    mark.Text = usestring;
                }
                //设计阶段
                if (doc.Range.Bookmarks["BM_3154"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3154"];

                    mark.Text = status;
                }
                #region 各专业的人
                //建筑专业负责人
                if (doc.Range.Bookmarks["BM_3013"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3013"];
                    string usestring = "";
                    ProjectPlanRoleList[1].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "建筑")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //结构专业负责人
                if (doc.Range.Bookmarks["BM_3014"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3014"];
                    string usestring = "";
                    ProjectPlanRoleList[1].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "结构")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //给排水专业负责人
                if (doc.Range.Bookmarks["BM_3015"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3015"];
                    string usestring = "";
                    ProjectPlanRoleList[1].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "给排水")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //暖通专业负责人
                if (doc.Range.Bookmarks["BM_3016"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3016"];
                    string usestring = "";
                    ProjectPlanRoleList[1].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "暖通")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //电气专业负责人
                if (doc.Range.Bookmarks["BM_3017"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3017"];
                    string usestring = "";
                    ProjectPlanRoleList[1].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "电气")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //概预算专业负责人
                if (doc.Range.Bookmarks["BM_3018"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3018"];
                    string usestring = "";
                    ProjectPlanRoleList[1].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "概预算")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }

                //建筑专业设计人
                if (doc.Range.Bookmarks["BM_3019"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3019"];
                    string usestring = "";
                    ProjectDesignPlanRoleList.Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "建筑")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //结构专业设计人
                if (doc.Range.Bookmarks["BM_3020"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3020"];
                    string usestring = "";
                    ProjectDesignPlanRoleList.Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "结构")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //给排水专业设计人
                if (doc.Range.Bookmarks["BM_3021"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3021"];
                    string usestring = "";
                    ProjectDesignPlanRoleList.Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "给排水")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //暖通专业设计人
                if (doc.Range.Bookmarks["BM_3022"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3022"];
                    string usestring = "";
                    ProjectDesignPlanRoleList.Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "暖通")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //电气专业设计人
                if (doc.Range.Bookmarks["BM_3023"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3023"];
                    string usestring = "";
                    ProjectDesignPlanRoleList.Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "电气")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //概预算专业设计人
                if (doc.Range.Bookmarks["BM_3024"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3024"];
                    string usestring = "";
                    ProjectDesignPlanRoleList.Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "概预算")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }

                //建筑专业校对人
                if (doc.Range.Bookmarks["BM_3025"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3025"];
                    string usestring = "";
                    ProjectPlanRoleList[2].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "建筑")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //结构专业校对人
                if (doc.Range.Bookmarks["BM_3026"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3026"];
                    string usestring = "";
                    ProjectPlanRoleList[2].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "结构")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //给排水专业校对人
                if (doc.Range.Bookmarks["BM_3027"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3027"];
                    string usestring = "";
                    ProjectPlanRoleList[2].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "给排水")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //暖通专业校对人
                if (doc.Range.Bookmarks["BM_3028"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3028"];
                    string usestring = "";
                    ProjectPlanRoleList[2].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "暖通")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //电气专业校对人
                if (doc.Range.Bookmarks["BM_3029"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3029"];
                    string usestring = "";
                    ProjectPlanRoleList[2].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "电气")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                //概预算专业校对人
                if (doc.Range.Bookmarks["BM_3030"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3030"];
                    string usestring = "";
                    ProjectPlanRoleList[2].Users.ForEach((user) =>
                    {
                        if (user.SpecialtyName == "概预算")
                        {
                            usestring += user.UserName + " ";
                        }
                    });
                    mark.Text = usestring;
                }
                #endregion
                #region 院审批一部分
                //院审批意见
                if (doc.Range.Bookmarks["BM_3158"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3158"];
                    string usestring = "";
                    try
                    {
                        usestring = projectPlanAudit.Suggestion.Split('|')[2];
                    }
                    catch (System.Exception ex)
                    {
                        usestring = "";
                    }


                    mark.Text = usestring;
                }
                //原审批的年
                if (doc.Range.Bookmarks["BM_3157"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3157"];
                    string usestring = "";
                    try
                    {
                        usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd").Split('-')[0];
                    }
                    catch (System.Exception ex)
                    {
                        usestring = "";
                    }
                    mark.Text = usestring;
                }
                //原审批的月
                if (doc.Range.Bookmarks["BM_3156"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3156"];
                    string usestring = "";
                    try
                    {
                        usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd").Split('-')[1];
                    }
                    catch (System.Exception ex)
                    {
                        usestring = "";
                    }
                    mark.Text = usestring;
                }
                //原审批的日
                if (doc.Range.Bookmarks["BM_3155"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3155"];
                    string usestring = "";
                    try
                    {
                        usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[2]).ToString("yyyy-MM-dd").Split('-')[2];
                    }
                    catch (System.Exception ex)
                    {
                        usestring = "";
                    }
                    mark.Text = usestring;
                }
                #endregion

                //经济所审批人签字&  时间
                if (doc.Range.Bookmarks["BM_3037"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3037"];
                    string usestring = "";
                    try
                    {
                        usestring = GetRoleName(projectPlanAudit.AuditUser.Split(',')[1]); //projectPlanAudit.Suggestion.Split('|')[2];
                    }
                    catch (System.Exception ex)
                    {
                        usestring = "";
                    }
                    mark.Text = usestring;
                }
                if (doc.Range.Bookmarks["BM_3038"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3038"];
                    string usestring = "";
                    try
                    {
                        usestring = Convert.ToDateTime(projectPlanAudit.AuditDate.Split(',')[1]).ToString("yyyy-MM-dd"); //projectPlanAudit.Suggestion.Split('|')[2];
                    }
                    catch (System.Exception ex)
                    {
                        usestring = "";
                    }
                    mark.Text = usestring;
                }

            }
            #endregion
            #region tcd有进岗信息
            else
            {
                string sqlProbii2 = "SELECT item_val FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
                string ProObjBii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii2).ToString();
                if (ProObjBii != null)
                {
                    string[] arrayprobii = ProObjBii.Split('\n');

                    for (int i = 0; i < arrayprobii.Length; i++)
                    {
                        for (int numb = 3002; numb < 3100; numb++)
                        {
                            if (arrayprobii[i].Split('=')[0] == numb.ToString())
                            {
                                //就给对应的书签赋值
                                if (doc.Range.Bookmarks["BM_" + numb] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_" + numb];
                                    mark.Text = arrayprobii[i].Split('=')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3154")
                            {
                                if (doc.Range.Bookmarks["BM_3154"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3154"];
                                    mark.Text = arrayprobii[i].Split('=')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3032")
                            {
                                if (doc.Range.Bookmarks["BM_3155"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3155"];
                                    mark.Text = arrayprobii[i].Split('=')[1].Split('-')[2];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3032")
                            {
                                if (doc.Range.Bookmarks["BM_3156"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3156"];
                                    mark.Text = arrayprobii[i].Split('=')[1].Split('-')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3032")
                            {
                                if (doc.Range.Bookmarks["BM_3157"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3157"];
                                    mark.Text = arrayprobii[i].Split('=')[1].Split('-')[0];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3031")
                            {
                                if (doc.Range.Bookmarks["BM_3158"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3158"];
                                    mark.Text = arrayprobii[i].Split('=')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3031")
                            {
                                if (doc.Range.Bookmarks["BM_3152"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3152"];
                                    mark.Text = arrayprobii[i].Split('=')[1] == "0" ? "院管" : "所管";
                                }
                            }
                        }

                    }
                }
            }
            #endregion

            #endregion
        }
        /// <summary>
        /// 项目进度计划表
        /// </summary>
        public void ToWordByJinDU(string proid, string level, string auditno, string copid, string status, string path4, out Aspose.Words.Document doc)
        {
            #region
            // string path4 = Server.MapPath(@"~\TemplateWord\3.QR-7.2-02 《项目进度计划表2》.doc");
            doc = new Aspose.Words.Document(path4); //载入模板
            //书签列表  copid  项目id   level 级别   auditno 审核id
            //得到项目的信息
            TG.Model.cm_Project ProjectModel = cm_proBll.GetModel(int.Parse(copid));
            TG.Model.cm_Coperation CoperModel = copbll.GetModel(ProjectModel.CoperationSysNo);
            //得到工程号
            string sqlByNum = @"
SELECT n.ProNumber FROM cm_Project c LEFT JOIN 
cm_ProjectNumber n ON c.pro_ID=n.Pro_id WHERE c.pro_ID=" + copid;
            object num = TG.DBUtility.DbHelperSQL.GetSingle(sqlByNum);
            TG.BLL.cm_ProjectPlanBP proplan = new TG.BLL.cm_ProjectPlanBP();
            //得到参与人员的信息
            TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
            List<TG.Model.ProjectPlanRole> ProjectPlanRoleList = projectPlanBP.GetProjectPlanRoleAndUsers(int.Parse(copid));
            //设计人员
            TG.Model.ProjectDesignPlanRole ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(int.Parse(copid));
            //审核信息
            TG.Model.cm_ProjectPlanAuditEntity projectPlanAudit = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanAuditEntity(new TG.Model.cm_ProjectPlanAuditQueryEntity { ProjectPlanAuditSysNo = int.Parse(auditno) });
            int index = -1;
            string[] arrayproject = ProjectModel.pro_status.Split(',');
            for (int i = 0; i < arrayproject.Length; i++)
            {
                if (status == arrayproject[i] || arrayproject[i] == "")
                {
                    index++;
                }
            }
            string biaoshi = "1::" + index;
            string sqlProbii = "SELECT bill_title FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
            object Probii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii);
            #region tcd没有进度
            if (Probii == null)
            {
                //项目名称
                if (doc.Range.Bookmarks["BM_3002"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3002"];
                    mark.Text = ProjectModel.pro_name == null ? "" : ProjectModel.pro_name.ToString().Trim();
                }
                //设计阶段
                if (doc.Range.Bookmarks["BM_3154"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3154"];

                    mark.Text = ProjectModel.pro_status == null ? "" : ProjectModel.pro_status.ToString().Trim();
                }
                //管理级别
                if (doc.Range.Bookmarks["BM_3153"] != null)
                {
                    Bookmark mark = doc.Range.Bookmarks["BM_3153"];
                    mark.Text = level == "0" ? "院管" : "所管";
                }
            }
            #endregion
            #region tcd有进度
            else
            {
                string sqlProbii2 = "SELECT item_val FROM tg_probill WHERE bill_title='" + biaoshi + "' AND  pro_id=" + ProjectModel.ReferenceSysNo;
                string ProObjBii = TG.DBUtility.DbHelperSQL.GetSingle(sqlProbii2).ToString();
                if (ProObjBii != null)
                {
                    string[] arrayprobii = ProObjBii.Split('\n');

                    for (int i = 0; i < arrayprobii.Length; i++)
                    {
                        for (int numb = 3099; numb < 3150; numb++)
                        {
                            if (arrayprobii[i].Split('=')[0] == numb.ToString())
                            {
                                //就给对应的书签赋值
                                if (doc.Range.Bookmarks["BM_" + numb] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_" + numb];
                                    mark.Text = arrayprobii[i].Split('=')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3002")
                            {
                                if (doc.Range.Bookmarks["BM_3002"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3002"];
                                    mark.Text = arrayprobii[i].Split('=')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3011")
                            {
                                if (doc.Range.Bookmarks["BM_3011"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3011"];
                                    mark.Text = arrayprobii[i].Split('=')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3154")
                            {
                                if (doc.Range.Bookmarks["BM_3154"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3154"];
                                    mark.Text = arrayprobii[i].Split('=')[1];
                                }
                            }
                            if (arrayprobii[i].Split('=')[0] == "3153")
                            {
                                if (doc.Range.Bookmarks["BM_3152"] != null)
                                {
                                    Bookmark mark = doc.Range.Bookmarks["BM_3153"];
                                    mark.Text = arrayprobii[i].Split('=')[1] == "0" ? "院管" : "所管";
                                }
                            }
                        }

                    }
                }

            }
            #endregion

            #endregion
        }
        /// <summary>
        /// 人员资料卡
        /// </summary>
        public void ToWordByMenZL(string proid)
        {

        }
        /// <summary>
        /// 设计输入评审记录
        /// </summary>
        public void ToWordByDesignAudit(string proid)
        {

        }
        public DateTime Gettime(string strtime)
        {
            return Convert.ToDateTime(strtime);
        }

        //根据id得到人员名册
        public string GetRoleName(string roleid)
        {
            string rolesql = @"SELECT mem_Name FROM tg_member WHERE mem_ID=" + roleid;
            return TG.DBUtility.DbHelperSQL.GetSingle(rolesql).ToString();
        }
        //根据用户编号，判断所在部门
        public string RoleToCompany(string str)
        {
            string rolename = "";
            TG.BLL.cm_Role bllrole = new BLL.cm_Role();
            List<TG.Model.cm_Role> listrole = bllrole.GetRoleList();
            foreach (TG.Model.cm_Role item in listrole)
            {
                string[] strArray = item.Users.Split(',');
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (strArray[i] == str)
                    {
                        rolename = item.RoleName;
                        break;
                    }
                }
                if (rolename != "")
                {
                    break;
                }

            }
            return rolename;
        }


        public string RoleToCompany(int position)
        {
            string sql = @"SELECT c.RoleName FROM cm_Role c LEFT JOIN cm_CoperationAuditConfig a ON c.SysNo=a.RoleSysNo WHERE
a.Position=" + position;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return o.ToString();
        }
        protected string GetProfession(string num)
        {
            string result = "";
            switch (num)
            {
                case "-1":
                    result = "";
                    break;
                case "27":
                    result = "方案";
                    break;
                case "28":
                    result = "初设";
                    break;
                case "29":
                    result = "施工图";
                    break;
                case "30":
                    result = "其他";
                    break;
                case "31":
                    result = "公开招标";
                    break;
                case "32":
                    result = "邀请招标";
                    break;
                case "36":
                    result = "计算机行业";
                    break;
                case "33":
                    result = "自行委托";
                    break;
                case "37":
                    result = "教育行业";
                    break;
                case "38":
                    result = "建筑行业";
                    break;
                case "47":
                    result = "科教行业";
                    break;
                case "34":
                    result = "普通客户";
                    break;
                case "35":
                    result = "VIP客户";
                    break;
                case "39":
                    result = "一般";
                    break;
                case "40":
                    result = "密切";
                    break;
                case "41":
                    result = "很密切";
                    break;
                case "43":
                    result = "一级";
                    break;
                case "44":
                    result = "二级";
                    break;
                case "45":
                    result = "三级";
                    break;
                case "46":
                    result = "四级";
                    break;
                default:
                    result = "";
                    break;
            }
            return result;
        }
        /// <summary>
        /// 查询客户信息
        /// </summary>
        private void GetCustomerInfo()
        {
            CustomerInfo = new TG.BLL.cm_CustomerInfo().GetModel(Convert.ToInt32(CoperationAuditEntity.cst_Id));
        }

        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        private void GetCoperationInfo()
        {
            List<TG.Model.cm_CoperationAuditListView> coperationListView = new TG.BLL.cm_CoperationAudit().GetCoperationListView(" and cpr_Id=" + CoperationSysNo);
            if (coperationListView != null && coperationListView.Count > 0)
            {
                CoperationAuditEntity = coperationListView[0];

            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}