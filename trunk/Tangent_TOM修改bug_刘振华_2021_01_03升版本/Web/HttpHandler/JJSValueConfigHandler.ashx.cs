﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// JJSValueConfigHandler 的摘要说明
    /// </summary>
    public class JJSValueConfigHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string option = context.Request.Params["option"];

            TG.BLL.cm_jjsProjectValueAllotConfig JJSbll = new TG.BLL.cm_jjsProjectValueAllotConfig();
            if (option == "get")
            {
                List<TG.Model.cm_jjsProjectValueAllotConfig> JJSModel = JJSbll.GetModelList("");
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(JJSModel);
                context.Response.Write(json);
            }
            else if (option == "save")
            {
                string JD = context.Request.Params["JD"];
                string GPS = context.Request.Params["GPS"];
                string TJ = context.Request.Params["TJ"];
                string JG = context.Request.Params["JG"];
                string CN = context.Request.Params["CN"];
                string DQ = context.Request.Params["DQ"];
                string type = context.Request.Params["type"];
                TG.Model.cm_jjsProjectValueAllotConfig modelJJS = JJSbll.GetModelByType(int.Parse(type));
                if (modelJJS != null)
                {
                    modelJJS.ProofreadPercent = Convert.ToDecimal(JD);
                    modelJJS.BuildingPercent = Convert.ToDecimal(TJ);
                    modelJJS.StructurePercent = Convert.ToDecimal(JG);
                    modelJJS.DrainPercent = Convert.ToDecimal(GPS);
                    modelJJS.HavcPercent = Convert.ToDecimal(CN);
                    modelJJS.ElectricPercent = Convert.ToDecimal(DQ);
                    modelJJS.Totalbuildingpercent = decimal.Parse(TJ) + decimal.Parse(JG);
                    modelJJS.TotalInstallationpercent = decimal.Parse(GPS) + decimal.Parse(CN) + decimal.Parse(DQ);
                    if (JJSbll.Update(modelJJS))
                    {
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            else if (option == "save2")
            {
                string JD2 = context.Request.Params["JD2"];
                string JZH = context.Request.Params["JZH"];
                string AZ = context.Request.Params["AZ"];
                string type = context.Request.Params["type"];
                TG.Model.cm_jjsProjectValueAllotConfig modelJJS = JJSbll.GetModelByType(int.Parse(type));
                if (modelJJS != null)
                {
                    modelJJS.ProofreadPercent = Convert.ToDecimal(JD2);
                    modelJJS.Totalbuildingpercent = Convert.ToDecimal(JZH);
                    modelJJS.TotalInstallationpercent = Convert.ToDecimal(AZ);
                    modelJJS.BuildingPercent = 0;
                    modelJJS.StructurePercent =0;
                    modelJJS.DrainPercent = 0;
                    modelJJS.HavcPercent = 0;
                    modelJJS.ElectricPercent =0;
                    if (JJSbll.Update(modelJJS))
                    {
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
                else
                {
                    context.Response.Write("0");
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}