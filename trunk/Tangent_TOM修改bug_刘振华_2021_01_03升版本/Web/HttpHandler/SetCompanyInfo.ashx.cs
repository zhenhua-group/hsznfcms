﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class SetCompanyInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string flag = context.Request["action"] ?? "";
            if (flag == "del")
            {
                string id = context.Request["id"] ?? "0";
                TG.BLL.cm_Links bll = new TG.BLL.cm_Links();
                if (bll.Delete(int.Parse(id)))
                {
                    context.Response.Write("ok");
                }
            }
            else if (flag == "slt")
            {
                TG.BLL.cm_Links bll = new TG.BLL.cm_Links();
                List<TG.Model.cm_Links> models = bll.GetModelList("");
                string html_menu = "{\"link\":[";

                foreach (TG.Model.cm_Links model in models)
                {
                    html_menu += "{\"" + model.LinkName + "\":\"" + model.LinkAddr + "\"},";
                }
                if (html_menu.LastIndexOf(',') > 0)
                {
                    html_menu = html_menu.Remove(html_menu.LastIndexOf(','), 1);
                }
                html_menu += "]}";
                context.Response.Write(html_menu);
            }
            else if (flag == "loadmap")
            {
                TG.BLL.TblAreaBll bll = new BLL.TblAreaBll();
                string str = bll.GetToAspx();
                str = "/LeadershipCockpit/" + str;
                context.Response.Write(str);
            }
            else if (flag == "info")
            {
                string insertContent = context.Request["content"] ?? "";
                string strSql = " Select Top 1 * From cm_CollegeInfo ";
                TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
                DataTable dt_content = bll.GetList(strSql).Tables[0];
                if (dt_content.Rows.Count > 0)
                {
                    string id = dt_content.Rows[0][0].ToString();
                    strSql = " Update cm_CollegeInfo Set TextContent=N'" + insertContent + "' Where ID=" + id;
                }
                else
                {
                    strSql = " Insert Into cm_CollegeInfo(TextContent) Values(N'" + insertContent + "')";
                }
                if (bll.ExcuteBySql(strSql) > 0)
                {
                    context.Response.Write("ok");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
