﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// SetUnitListShowHandler 的摘要说明
    /// </summary>
    public class SetUnitListShowHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string flag = context.Request["flag"] ?? "";
            string unitid = context.Request["unitid"] ?? "0";
            if (flag == "getdata")
            {
                string strWhere = " unit_ID=" + unitid;
                List<TG.Model.cm_DropUnitList> list = new TG.BLL.cm_DropUnitList().GetModelList(strWhere);
                //返回值
                string rlt = "0";
                if (list.Count > 0)
                {
                    rlt = list[0].flag.ToString();
                }
                context.Response.Write(rlt);
            }
            else if (flag == "update")
            {
                string strWhere = " unit_ID=" + unitid;
                TG.Model.cm_DropUnitList DropModle = new TG.BLL.cm_DropUnitList().GetModelByUnitID(int.Parse(unitid));
                //存在
                if (DropModle != null)
                {

                    //DropModle.flag = DropModle.flag == 0 ? 1 : 0;
                    if (DropModle.flag == 0)
                    {
                        DropModle.flag = 1;
                    }
                    else
                    {
                        DropModle.flag = 0;
                    }
                    new TG.BLL.cm_DropUnitList().Update(DropModle);
                    if (DropModle.flag == 0)
                    {
                        context.Response.Write("0");
                    }
                    else
                    {
                        context.Response.Write("1");
                    }

                }
                else
                {
                    TG.Model.cm_DropUnitList model = new TG.Model.cm_DropUnitList();
                    model.unit_ID = int.Parse(unitid);
                    model.flag = 1;
                    new TG.BLL.cm_DropUnitList().Add(model);
                    context.Response.Write(model.flag.ToString());
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}