﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Text;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;


namespace TG.Web.HttpHandler
{
    public class CommHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //不让浏览器保存缓存
            context.Response.Buffer = true;
            context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";

            string action = context.Request.Params["action"] ?? "no";
            //获取本地用户信息
            HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
            //当前登陆用户 zxq 20140121
            string userSysLogin = Convert.ToString(user["memlogin"] ?? "");
            string userSysNo = Convert.ToString(user["memid"] ?? "1");

            if (action != "no")
            {
                if (action == "cpraddsub")
                {
                    string str_cstno = context.Request.Params["cstno"] ?? "";
                    string str_name = context.Request.Params["subname"] ?? "";
                    string str_area = context.Request.Params["subarea"] ?? "";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    string subid = context.Request.Params["subid"] ?? "0";
                    //收费状态
                    string MoneyStatus = context.Request.Params["MoneyStatus"] ?? "";
                    decimal str_money = 0;
                    decimal.TryParse(context.Request["money"], out str_money);
                    string remark = context.Request["remark"];
                    remark = remark.Replace("'", "''");//替换' 为两个'' 不会截断sql语句

                    TG.BLL.cm_SubCoperation bll_sub = new TG.BLL.cm_SubCoperation();
                    TG.Model.cm_SubCoperation model_sub = new TG.Model.cm_SubCoperation();
                    model_sub.cpr_Id = null;
                    if (str_flag == "edit")
                    {
                        model_sub.cpr_Id = int.Parse(str_cstno);
                    }
                    model_sub.cpr_No = str_cstno;
                    model_sub.Item_Name = str_name;
                    model_sub.Item_Area = str_area;
                    model_sub.Money = str_money;
                    model_sub.Remark = remark;
                    model_sub.LastUpdate = DateTime.Now;
                    model_sub.UpdateBy = Convert.ToString(user["memlogin"] ?? "");
                    model_sub.MoneyStatus = MoneyStatus;
                    try
                    {
                        int count = 0;
                        if (subid == "0")
                        {
                            count = bll_sub.Add(model_sub);
                        }
                        else
                        {
                            model_sub.ID = Convert.ToInt32(subid);
                            count = bll_sub.Update(model_sub) == true ? 2 : 0;
                        }
                        if (count > 0)
                        {
                            context.Response.Write("yes");
                        }

                    }
                    catch (System.Exception ex)
                    { }
                }
                //添加其他合同子项
                if (action == "cpraddsubtype")
                {
                    string str_cstno = context.Request.Params["cstno"] ?? "";
                    string str_name = context.Request.Params["subname"] ?? "";
                    string str_area = context.Request.Params["subarea"] ?? "0";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    string subid = context.Request.Params["subid"] ?? "0";
                    string subtype = context.Request.Params["subtype"] ?? "";
                    decimal str_money = 0;
                    decimal.TryParse(context.Request["money"], out str_money);
                    string remark = context.Request["remark"];
                    remark = remark.Replace("'", "''");//替换' 为两个'' 不会截断sql语句

                    TG.BLL.cm_SubCoperationType bll_sub = new TG.BLL.cm_SubCoperationType();
                    TG.Model.cm_SubCoperationType model_sub = new TG.Model.cm_SubCoperationType();
                    model_sub.cpr_Id = null;
                    if (str_flag == "edit")
                    {
                        model_sub.cpr_Id = int.Parse(str_cstno);
                    }

                    model_sub.cpr_No = str_cstno;
                    model_sub.Item_Name = str_name;
                    model_sub.Item_Area = Convert.ToDecimal(str_area);
                    model_sub.Money = str_money;
                    model_sub.Remark = remark;
                    model_sub.LastUpdate = DateTime.Now;
                    model_sub.UpdateBy = Convert.ToString(user["memlogin"] ?? "");
                    model_sub.SubType = subtype; //合同类型
                    try
                    {
                        int count = 0;
                        if (subid == "0")
                        {
                            count = bll_sub.Add(model_sub);
                        }
                        else
                        {
                            model_sub.ID = Convert.ToInt32(subid);
                            count = bll_sub.Update(model_sub) == true ? 2 : 0;
                        }
                        if (count > 0)
                        {
                            context.Response.Write("yes");
                        }

                    }
                    catch (System.Exception ex)
                    { }
                }
                //岩土工程施工合同子项添加
                else if (action == "cpraddsubConstru")
                {
                    string str_cstno = context.Request.Params["cstno"] ?? "";
                    //施工内容
                    string construction_Name = context.Request.Params["Construction_Name"] ?? "";
                    construction_Name = construction_Name.Replace("'", "''");
                    //工程桩径
                    string project_Diameter = context.Request.Params["Project_Diameter"] ?? "";
                    //数量（根）
                    string quantity = context.Request.Params["Quantity"] ?? "";
                    //桩长
                    string pile_Length = context.Request.Params["Pile_Length"] ?? "";
                    //基坑面积(㎡)
                    string pit_Area = context.Request.Params["Pit_Area"] ?? "";
                    //深度
                    string depth = context.Request.Params["Depth"] ?? "";
                    //降水井数量
                    string well_Amount = context.Request.Params["Well_Amount"] ?? "";
                    //混凝土计划用量
                    string concrete_Amount = context.Request.Params["Concrete_Amount"] ?? "";
                    //钢筋计划用量
                    string reinforced_Amount = context.Request.Params["Reinforced_Amount"] ?? "";
                    //Support_Form
                    string support_Form = context.Request.Params["Support_Form"] ?? "";

                    string str_flag = context.Request.Params["flag"] ?? "";
                    string subid = context.Request.Params["subid"] ?? "0";


                    TG.BLL.cm_SubConstruCoperartion bll_sub = new TG.BLL.cm_SubConstruCoperartion();
                    TG.Model.cm_SubConstruCoperartion model_sub = new TG.Model.cm_SubConstruCoperartion();
                    model_sub.cpr_Id = null;
                    model_sub.cpr_No = str_cstno;
                    if (str_flag == "edit")
                    {
                        model_sub.cpr_Id = int.Parse(str_cstno);
                    }

                    model_sub.cpr_No = str_cstno;
                    model_sub.Construction_Name = construction_Name;
                    if (!string.IsNullOrEmpty(project_Diameter))
                    {
                        model_sub.Project_Diameter = decimal.Parse(project_Diameter);
                    }
                    if (!string.IsNullOrEmpty(quantity))
                    {
                        model_sub.Quantity = int.Parse(quantity);
                    }
                    if (!string.IsNullOrEmpty(pile_Length))
                    {
                        model_sub.Pile_Length = decimal.Parse(pile_Length);
                    }
                    if (!string.IsNullOrEmpty(pit_Area))
                    {
                        model_sub.Pit_Area = decimal.Parse(pit_Area);
                    }
                    if (!string.IsNullOrEmpty(depth))
                    {
                        model_sub.Depth = decimal.Parse(depth);
                    }
                    if (!string.IsNullOrEmpty(well_Amount))
                    {
                        model_sub.Well_Amount = decimal.Parse(well_Amount);
                    }
                    if (!string.IsNullOrEmpty(concrete_Amount))
                    {
                        model_sub.Concrete_Amount = decimal.Parse(concrete_Amount);
                    }
                    if (!string.IsNullOrEmpty(reinforced_Amount))
                    {
                        model_sub.Reinforced_Amount = decimal.Parse(reinforced_Amount);
                    }

                    model_sub.Support_Form = support_Form;

                    model_sub.LastUpdate = DateTime.Now;
                    model_sub.UpdateBy = Convert.ToString(user["memlogin"] ?? "");
                    try
                    {
                        int count = 0;
                        if (subid == "0")
                        {
                            count = bll_sub.Add(model_sub);
                        }
                        else
                        {
                            model_sub.ID = Convert.ToInt32(subid);
                            count = bll_sub.Update(model_sub) == true ? 2 : 0;
                        }
                        if (count > 0)
                        {
                            context.Response.Write("yes");
                        }

                    }
                    catch (System.Exception ex)
                    { }
                }
                //获取分项列表 zxq 20140121
                else if (action == "getsubitemdata")
                {
                    string str_cstno = context.Request.Params["cstno"] ?? "";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    TG.BLL.cm_SubCoperation bll_sub = new TG.BLL.cm_SubCoperation();
                    string str_where = " cpr_No='" + str_cstno + "'";
                    if (str_flag == "edit")
                    {
                        str_where = " cpr_id=" + str_cstno;
                    }
                    DataTable dt = bll_sub.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                    else
                    {
                        context.Response.Write("{\"ds\":[{\"ID\":\"\",\"cpr_Id\":\"\",\"cpr_No\":\"\",\"Item_Name\":\"\",\"Item_Area\":\"\",\"UpdateBy\":\"\",\"LastUpdate\":\"\"}]}");
                    }
                }
                //获取其他合同子项列表 zxq 20141111
                else if (action == "getsubitemdatatype")
                {
                    string str_cstno = context.Request.Params["cstno"] ?? "";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    string subtype = context.Request.Params["subtype"] ?? "";
                    TG.BLL.cm_SubCoperationType bll_sub = new TG.BLL.cm_SubCoperationType();
                    string str_where = " cpr_No='" + str_cstno + "'";
                    if (str_flag == "edit")
                    {
                        str_where = " cpr_id=" + str_cstno;
                    }
                    str_where = str_where + " and SubType='" + subtype + "'";
                    DataTable dt = bll_sub.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                    //else
                    //{
                    //    context.Response.Write("{\"ds\":[{\"ID\":\"\",\"cpr_Id\":\"\",\"cpr_No\":\"\",\"Item_Name\":\"\",\"Item_Area\":\"\",\"UpdateBy\":\"\",\"LastUpdate\":\"\"}]}");
                    //}
                }
                //获取其他合同子项列表-岩土工程施工合同
                else if (action == "getsubitemdataConstru")
                {
                    string str_cstno = context.Request.Params["cstno"] ?? "";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    TG.BLL.cm_SubConstruCoperartion bll_sub = new TG.BLL.cm_SubConstruCoperartion();
                    string str_where = " cpr_No='" + str_cstno + "'";
                    if (str_flag == "edit")
                    {
                        str_where = " cpr_id=" + str_cstno;
                    }
                    DataTable dt = bll_sub.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                    //else
                    //{
                    //    context.Response.Write("{\"ds\":[{\"ID\":\"\",\"cpr_Id\":\"\",\"cpr_No\":\"\",\"Construction_Name\":\"\",\"Project_Diameter\":\"\",\"Quantity\":\"\",\"Pile_Length\":\"\",\"Pit_Area\":\"\",\"Depth\":\"\",\"Support_Form\":\"\",\"Well_Amount\":\"\",\"Concrete_Amount\":\"\",\"Reinforced_Amount\":\"\",\"UpdateBy\":\"\",\"LastUpdate\":\"\"}]}");
                    //}
                }
                //获取合同附件列表 zxq 20140121
                else if (action == "getcprfiles")
                {
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    string str_where = " Cpr_Id=" + str_cprid + " AND UploadUser='" + userSysNo + "'";
                    DataTable dt = bll_att.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //其他类型合同附件
                else if (action == "getcprfilestype")
                {
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    string str_type = context.Request.Params["cpr_type"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    string str_where = " Cpr_Id=" + str_cprid + " AND UploadUser='" + userSysNo + "' AND OwnType='" + str_type + "'";
                    DataTable dt = bll_att.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获取项目附件列表 zxq 20140121
                else if (action == "getprojfiles")
                {
                    string str_projid = context.Request.Params["projid"] ?? "";
                    string str_flag = context.Request.Params["type"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    string str_where = " Proj_Id=" + str_projid + "  AND OwnType='" + str_flag + "'";
                    DataTable dt = bll_att.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //删除子项
                else if (action == "delsubitem")
                {
                    string str_itemid = context.Request.Params["subid"] ?? "";
                    TG.BLL.cm_SubCoperation bll_sub = new TG.BLL.cm_SubCoperation();
                    if (bll_sub.Delete(int.Parse(str_itemid)))
                    {
                        context.Response.Write("yes");
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                //删除其他合同子项
                else if (action == "delsubitemtype")
                {
                    string str_itemid = context.Request.Params["subid"] ?? "";
                    TG.BLL.cm_SubCoperationType bll_sub = new TG.BLL.cm_SubCoperationType();
                    if (bll_sub.Delete(int.Parse(str_itemid)))
                    {
                        context.Response.Write("yes");
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                //删除子项
                else if (action == "delsubitemConstru")
                {
                    string str_itemid = context.Request.Params["subid"] ?? "";
                    TG.BLL.cm_SubConstruCoperartion bll_sub = new TG.BLL.cm_SubConstruCoperartion();
                    if (bll_sub.Delete(int.Parse(str_itemid)))
                    {
                        context.Response.Write("yes");
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                //删除合同计划
                else if (action == "delchargeitem")
                {
                    string chargeid = context.Request.Params["chrgid"] ?? "";
                    TG.BLL.cm_CoperationCharge bll_chg = new TG.BLL.cm_CoperationCharge();
                    if (bll_chg.Delete(int.Parse(chargeid)))
                    {
                        context.Response.Write("yes");
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                //删除其他类型合同收费信息
                else if (action == "delchargeitemtype")
                {
                    string chargeid = context.Request.Params["chrgid"] ?? "";
                    string str_type = context.Request.Params["cpr_type"] ?? "";
                    TG.BLL.cm_CoperationChargeType bll_chg = new TG.BLL.cm_CoperationChargeType();
                    if (bll_chg.Delete(int.Parse(chargeid), str_type))
                    {
                        context.Response.Write("yes");
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                else if (action == "delcprattach")
                {
                    string str_attid = context.Request.Params["attid"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    if (bll_att.Delete(int.Parse(str_attid)))
                    {
                        context.Response.Write("yes");
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                else if (action == "delprojattach")
                {
                    string str_attid = context.Request.Params["attid"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    if (bll_att.Delete(int.Parse(str_attid)))
                    {
                        context.Response.Write("yes");
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                else if (action == "selectsubcpr")
                {
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    TG.BLL.cm_SubCoperation bll_sub = new TG.BLL.cm_SubCoperation();
                    string str_where = " cpr_Id=" + str_cprid;
                    DataTable dt_cpr = bll_sub.GetList(str_where).Tables[0];
                    if (dt_cpr.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt_cpr));
                    }
                }
                else if (action == "selectsubcprConstru")
                {
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    TG.BLL.cm_SubConstruCoperartion bll_sub = new TG.BLL.cm_SubConstruCoperartion();
                    string str_where = " cpr_Id=" + str_cprid;
                    DataTable dt_cpr = bll_sub.GetList(str_where).Tables[0];
                    if (dt_cpr.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt_cpr));
                    }
                }
                else if (action == "selectattach")
                {
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    string str_where = " cpr_ID=" + str_cprid + "AND OwnType='cpr'";
                    DataTable dt = bll_att.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                else if (action == "selecttattachtype")
                {
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    string str_flag = context.Request.Params["flag"] ?? "";
                    string str_type = context.Request.Params["cpr_type"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    string str_where = " cpr_ID=" + str_cprid + "  AND OwnType='" + str_type + "'";
                    DataTable dt = bll_att.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //添加合同计划收费 zxq 20140121
                else if (action == "addcprsk")
                {
                    string flag = context.Request.Params["flag"] ?? "";
                    string str_jine = context.Request.Params["jine"] ?? "0";
                    string str_allcount = context.Request.Params["allcount"] ?? "0";
                    string str_shicount = context.Request.Params["shicount"] ?? "0";
                    string str_date = context.Request.Params["date"] ?? "2010-05-05";
                    string str_skr = context.Request.Params["skr"] ?? "";
                    string str_mark = context.Request.Params["mark"] ?? "";
                    string chargeid = context.Request.Params["chargeid"] ?? "0";
                    str_mark = str_mark.Replace("'", "''");//SQL会把两个'当成一个'字符,不会截断sql语句
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    TG.BLL.cm_CoperationCharge bll_chg = new TG.BLL.cm_CoperationCharge();
                    TG.Model.cm_CoperationCharge model_chg = new TG.Model.cm_CoperationCharge();
                    TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                    model_chg.cpr_ID = decimal.Parse(str_cprid);
                    model_chg.payCount = Convert.ToDecimal(str_jine);
                    model_chg.payShiCount = Convert.ToDecimal(str_shicount);
                    model_chg.acceptuser = str_skr;
                    //判断是否大于合同额
                    //已收款总和
                    decimal d_paycount = 0;
                    string str_sql = " Select Sum(payCount) From cm_CoperationCharge Where cpr_ID=" + str_cprid + " and ID<>" + chargeid + " AND acceptuser='" + userSysLogin + "'";
                    DataSet ds_plan = bll_db.GetList(str_sql);
                    if (ds_plan.Tables.Count > 0)
                    {
                        if (ds_plan.Tables[0].Rows.Count > 0)
                        {
                            d_paycount = Convert.ToDecimal(ds_plan.Tables[0].Rows[0][0].ToString() == "" ? "0" : ds_plan.Tables[0].Rows[0][0].ToString());
                        }
                    }
                    //合同总额
                    decimal d_allcount = Convert.ToDecimal(str_allcount);
                    //合同余额
                    decimal d_yue = d_allcount - d_paycount;
                    //付款金额
                    decimal d_pay = Convert.ToDecimal(str_jine);
                    if (d_pay > d_yue)
                    {
                        context.Response.Write(d_yue.ToString());
                    }
                    else
                    {
                        //收款次数
                        int i_count = 0;
                        string str_where = " Select Count(*) From cm_CoperationCharge Where cpr_ID=" + str_cprid + " and ID<>" + chargeid;
                        DataSet ds = bll_db.GetList(str_where);
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                i_count = int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1;
                            }
                        }

                        model_chg.Times = "第" + i_count + "次收款";


                        //合同金额

                        decimal i_persent = 0;
                        decimal account = 0;

                        decimal paycout = Convert.ToDecimal(str_jine);
                        if (flag == "add")
                        {
                            account = Convert.ToDecimal(str_allcount);
                        }
                        else
                        {
                            string str_where1 = " SELECT [cpr_Acount] FROM [cm_Coperation] Where cpr_id=" + str_cprid;
                            DataSet ds_cout = bll_db.GetList(str_where1);
                            if (ds_cout.Tables.Count > 0)
                            {
                                if (ds_cout.Tables[0].Rows.Count > 0)
                                {
                                    account = Convert.ToDecimal(ds_cout.Tables[0].Rows[0][0].ToString());
                                }
                            }
                        }
                        if (paycout > 0 && account > 0)
                        {
                            i_persent = (paycout / account) * 100;
                        }

                        model_chg.persent = System.Math.Round(i_persent, 2);
                        model_chg.paytime = Convert.ToDateTime(str_date);
                        model_chg.mark = str_mark;

                        int affectRow = 0;
                        if (chargeid == "0")
                        {
                            affectRow = bll_chg.Add(model_chg);
                        }
                        else
                        {
                            string oldtime = new TG.BLL.cm_CoperationCharge().GetModel(Convert.ToInt32(chargeid)).Times;
                            model_chg.Times = oldtime;
                            model_chg.ID = Convert.ToInt32(chargeid);
                            affectRow = bll_chg.Update(model_chg) == true ? 2 : 0;

                        }

                        if (affectRow > 0)
                        {

                            context.Response.Write("yes");

                        }
                    }

                }
                //添加其他合同收费计划
                else if (action == "addcprsktype")
                {
                    string flag = context.Request.Params["flag"] ?? "";
                    string str_jine = context.Request.Params["jine"] ?? "0";
                    string str_allcount = context.Request.Params["allcount"] ?? "0";
                    string str_shicount = context.Request.Params["shicount"] ?? "0";
                    string str_date = context.Request.Params["date"] ?? "2010-05-05";
                    string str_skr = context.Request.Params["skr"] ?? "";
                    string str_mark = context.Request.Params["mark"] ?? "";
                    string chargeid = context.Request.Params["chargeid"] ?? "0";
                    str_mark = str_mark.Replace("'", "''");//SQL会把两个'当成一个'字符,不会截断sql语句
                    string str_cprid = context.Request.Params["cprid"] ?? "";
                    string str_type = context.Request.Params["cpr_type"] ?? "";//合同类型
                    TG.BLL.cm_CoperationChargeType bll_chg = new TG.BLL.cm_CoperationChargeType();
                    TG.Model.cm_CoperationChargeType model_chg = new TG.Model.cm_CoperationChargeType();
                    TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                    model_chg.cpr_ID = decimal.Parse(str_cprid);
                    model_chg.payCount = Convert.ToDecimal(str_jine);
                    model_chg.payShiCount = Convert.ToDecimal(str_shicount);
                    model_chg.acceptuser = str_skr;
                    //判断是否大于合同额
                    //已收款总和
                    decimal d_paycount = 0;
                    string str_sql = " Select Sum(payCount) From cm_CoperationChargeType Where cpr_ID=" + str_cprid + " and ID<>" + chargeid + " AND acceptuser='" + userSysLogin + "' and paytype='" + str_type + "'";
                    DataSet ds_plan = bll_db.GetList(str_sql);
                    if (ds_plan.Tables.Count > 0)
                    {
                        if (ds_plan.Tables[0].Rows.Count > 0)
                        {
                            d_paycount = Convert.ToDecimal(ds_plan.Tables[0].Rows[0][0].ToString() == "" ? "0" : ds_plan.Tables[0].Rows[0][0].ToString());
                        }
                    }
                    //合同总额
                    decimal d_allcount = Convert.ToDecimal(str_allcount);
                    //合同余额
                    decimal d_yue = d_allcount - d_paycount;
                    //付款金额
                    decimal d_pay = Convert.ToDecimal(str_jine);
                    if (d_pay > d_yue)
                    {
                        context.Response.Write(d_yue.ToString());
                    }
                    else
                    {
                        //收款次数
                        int i_count = 0;
                        string str_where = " Select Count(*) From cm_CoperationChargeType Where cpr_ID=" + str_cprid + " and ID<>" + chargeid + " AND paytype='" + str_type + "'";
                        DataSet ds = bll_db.GetList(str_where);
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                i_count = int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1;
                            }
                        }

                        model_chg.Times = "第" + i_count + "次收款";
                        //合同类型
                        model_chg.paytype = str_type;
                        //合同金额

                        decimal i_persent = 0;
                        decimal account = 0;

                        decimal paycout = Convert.ToDecimal(str_jine);
                        if (flag == "add")
                        {
                            account = Convert.ToDecimal(str_allcount);
                        }
                        else
                        {
                            //表名称 多个合同 用不同的表结构
                            string tableName = "cm_ReconnCoperation";
                            //工程监理合同
                            if (str_type == "supercharge")
                            {
                                tableName = "cm_SuperCoperation";
                            }
                            else if (str_type == "designcharge")
                            { //岩土工程
                                tableName = "cm_DesignCoperation";
                            }
                            else if (str_type == "pit")
                            {
                                tableName = "cm_PitCoperation";
                            }
                            else if (str_type == "measurecharge")
                            {
                                tableName = "cm_MeasureCoperation";
                            }
                            else if (str_type == "testcharge")
                            {
                                tableName = "cm_TestCoperation";
                            }
                            else if (str_type == "construcharge")
                            {
                                tableName = "cm_ConstruCoperartion";
                            }
                            else if (str_type == "engineer")
                            {
                                tableName = "cm_EngineerCoperation";
                            }
                            string str_where1 = " SELECT [cpr_Acount] FROM [" + tableName + "] Where cpr_id=" + str_cprid + "";
                            DataSet ds_cout = bll_db.GetList(str_where1);
                            if (ds_cout.Tables.Count > 0)
                            {
                                if (ds_cout.Tables[0].Rows.Count > 0)
                                {
                                    account = Convert.ToDecimal(ds_cout.Tables[0].Rows[0][0].ToString());
                                }
                            }
                        }
                        if (paycout > 0 && account > 0)
                        {
                            i_persent = (paycout / account) * 100;
                        }

                        model_chg.persent = System.Math.Round(i_persent, 2);
                        model_chg.paytime = Convert.ToDateTime(str_date);
                        model_chg.mark = str_mark;

                        int affectRow = 0;
                        if (chargeid == "0")
                        {
                            affectRow = bll_chg.Add(model_chg);
                        }
                        else
                        {
                            string oldtime = new TG.BLL.cm_CoperationChargeType().GetModel(Convert.ToInt32(chargeid), str_type).Times;
                            model_chg.Times = oldtime;
                            model_chg.ID = Convert.ToInt32(chargeid);
                            affectRow = bll_chg.Update(model_chg) == true ? 2 : 0;

                        }

                        if (affectRow > 0)
                        {
                            context.Response.Write("yes");
                        }
                    }

                }
                //获取合同计划收费列表 zxq 20140121
                else if (action == "getplancharge")
                {
                    string str_cprid = context.Request.Params["cstno"] ?? "-2";
                    string strWhere = " cpr_ID=" + str_cprid + " AND acceptuser='" + userSysLogin + "'";
                    DataSet dt_charge = new TG.BLL.cm_CoperationCharge().GetList(strWhere);
                    if (dt_charge.Tables.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt_charge.Tables[0]));
                    }
                }
                //获取合同计划收费列表 20140121  --其他类型合同
                else if (action == "getplanchargetype")
                {
                    string str_cprid = context.Request.Params["cstno"] ?? "-2";
                    string str_type = context.Request.Params["cpr_type"] ?? "";//合同类型
                    string strWhere = " AND cpr_ID=" + str_cprid + " AND acceptuser='" + userSysLogin + "' AND paytype='" + str_type + "'";
                    DataSet dt_charge = new TG.BLL.cm_CoperationChargeType().GetList(strWhere);
                    if (dt_charge.Tables.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt_charge.Tables[0]));
                    }
                }
                else if (action == "getplanchargetypeAll")
                {
                    string str_cprid = context.Request.Params["cstno"] ?? "-2";
                    string str_type = context.Request.Params["cpr_type"] ?? "";//合同类型
                    string strWhere = " AND cpr_ID=" + str_cprid + "  AND paytype='" + str_type + "'";
                    DataSet dt_charge = new TG.BLL.cm_CoperationChargeType().GetList(strWhere);
                    if (dt_charge.Tables.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt_charge.Tables[0]));
                    }
                }
                //获取联系人列表 zxq 20140121
                else if (action == "getcstinfo")
                {
                    string str_cstid = context.Request.Params["cstno"] ?? "0";
                    string strWhere = " Cst_Id=" + str_cstid + " and UpdateBy=" + userSysNo;
                    DataSet dt_cst = new TG.BLL.cm_ContactPersionInfo().GetList(strWhere);
                    if (dt_cst.Tables.Count > 0)
                    {
                        if (dt_cst.Tables[0].Rows.Count > 0)
                        {
                            context.Response.Write(TableToJson(dt_cst.Tables[0]));
                        }
                        else
                        {
                            context.Response.Write("0");
                        }
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
                else if (action == "getcstinfoAll")
                {
                    string str_cstid = context.Request.Params["cstno"] ?? "0";
                    string strWhere = " Cst_Id=" + str_cstid;
                    DataSet dt_cst = new TG.BLL.cm_ContactPersionInfo().GetList(strWhere);
                    if (dt_cst.Tables.Count > 0)
                    {
                        if (dt_cst.Tables[0].Rows.Count > 0)
                        {
                            context.Response.Write(TableToJson(dt_cst.Tables[0]));
                        }
                        else
                        {
                            context.Response.Write("0");
                        }
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
                else if (action == "dellinkinfo")
                {
                    string linkid = context.Request.Params["linkid"] ?? "0";
                    TG.BLL.cm_ContactPersionInfo bll = new TG.BLL.cm_ContactPersionInfo();
                    if (bll.Delete(int.Parse(linkid)))
                    {
                        context.Response.Write("yes");
                    }
                }
                //获取客户满意度列表 zxq 20140121
                else if (action == "getstainfo")
                {
                    string staid = context.Request.Params["stano"] ?? "0";
                    string strWhere = " Cst_Id=" + staid + " and UpdateBy=" + userSysNo;
                    DataSet dt_sta = new TG.BLL.cm_SatisfactionInfo().GetList(strWhere);
                    if (dt_sta.Tables.Count > 0)
                    {
                        if (dt_sta.Tables[0].Rows.Count > 0)
                        {
                            context.Response.Write(TableToJson(dt_sta.Tables[0]));
                        }
                        else
                        {
                            context.Response.Write("0");
                        }
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
                else if (action == "delsta")
                {
                    string staid = context.Request.Params["staid"] ?? "0";
                    TG.BLL.cm_SatisfactionInfo bll = new TG.BLL.cm_SatisfactionInfo();
                    if (bll.Delete(int.Parse(staid)))
                    {
                        context.Response.Write("yes");
                    }
                }
                else if (action == "getbanerattach")
                {
                    string id = context.Request.Params["attachid"] ?? "";
                    TG.BLL.cm_WebBannerAttach bll_att = new TG.BLL.cm_WebBannerAttach();
                    string str_where = " TempNo=" + id;
                    DataTable dt = bll_att.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                else if (action == "delbanerattach")
                {
                    string staid = context.Request.Params["attachid"] ?? "0";
                    TG.BLL.cm_WebBannerAttach bll = new TG.BLL.cm_WebBannerAttach();
                }
                else if (action == "readmsg")
                {
                    string msgid = context.Request.Params["msgID"] ?? "0";
                    //int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(int.Parse(msgid));
                    int count = new TG.BLL.cm_SysMsg().UpdateSysMsgStatus(int.Parse(msgid));
                }
                else if (action == "donemsg")
                {
                    string msgid = context.Request.Params["msgID"] ?? "0";
                    //int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(int.Parse(msgid));
                    int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatusByID(int.Parse(msgid));
                }
                else if (action == "readmsg1")
                {
                    string msgid = context.Request.Params["msgID"] ?? "0";
                    int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatusByID(int.Parse(msgid));
                }
                //增加:处理并返回合同类型  //By FBW
                else if (action == "getCompactType")
                {
                    TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
                    string str_where = " dic_Type='cpr_lx'";
                    DataTable dt = bll_dic.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                else if (action == "getCpr_cjbm")
                {
                    TG.BLL.tg_unit bll_unit = new BLL.tg_unit();
                    string str_where = " (unit_ParentID=0 and unit_IsEndUnit=0) or (unit_ParentID <>0 and unit_IsEndUnit=0) ";
                    string clickID = context.Request.Params["clickID"] ?? "";
                    int pagesize = Convert.ToInt32(context.Request.Params["pagesize"] ?? "10");
                    int index = 1;
                    int gotoindex = 1;
                    int pageCount = 0;
                    if (!String.IsNullOrEmpty(context.Request.Params["index"]))
                    {
                        index = Convert.ToInt32(context.Request.Params["index"] ?? "1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["gotoindex"]))
                    {
                        gotoindex = Convert.ToInt32(context.Request.Params["gotoindex"] ?? "1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["totalPageCount"]))
                    {
                        pageCount = Convert.ToInt32(context.Request.Params["totalPageCount"] ?? "-1");
                    }
                    DataTable dt = null;
                    if (clickID == "firstPage")
                    {
                        dt = bll_unit.GetListByPage(str_where, pagesize, 1).Tables[0];

                    }
                    else
                    {
                        if (clickID == "prevPage")
                        {
                            if (index <= pageCount && index > 1)
                            {
                                dt = bll_unit.GetListByPage(str_where, pagesize, index - 1).Tables[0];
                            }
                        }
                        else if (clickID == "nextPage")
                        {
                            if (index < pageCount && index > 0)
                            {
                                dt = bll_unit.GetListByPage(str_where, pagesize, index + 1).Tables[0];
                            }
                        }
                        else if (clickID == "lastPage")
                        {
                            if (index < pageCount && index > 0)
                            {
                                dt = bll_unit.GetListByPage(str_where, pagesize, pageCount).Tables[0];
                            }
                        }
                        else if (clickID == "gotopageIndex")
                        {
                            if (gotoindex <= pageCount && gotoindex >= 1)
                            {
                                dt = bll_unit.GetListByPage(str_where, pagesize, gotoindex).Tables[0];
                            }
                        }
                        else
                        {
                            dt = bll_unit.GetListByPage(str_where, pagesize, 1).Tables[0];
                        }
                    }
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得所有承接部门个数
                else if (action == "getAllTotalCount")
                {
                    string tName = context.Request.Params["tName"] ?? "";
                    string strWhere = context.Request.Params["strWhere"] ?? "";
                    int pageTotalCount = -1;

                    if (tName == "tg_unit")
                    {
                        TG.BLL.tg_unit bll_unit = new BLL.tg_unit();
                        string str_where = " ((unit_ParentID=0 and unit_IsEndUnit=0) or (unit_ParentID <>0 and unit_IsEndUnit=0)) ";
                        if (!string.IsNullOrEmpty(strWhere))
                        {
                            str_where += " and " + strWhere;
                        }
                        pageTotalCount = bll_unit.GetRecordCount(str_where);
                    }

                    if (tName == "cm_CPInfo")
                    {
                        string previewPower = context.Request.Params["previewPower"] ?? "";
                        string userSysNum = context.Request.Params["userSysNum"] ?? "";
                        string userUnitNum = context.Request.Params["userUnitNum"] ?? "";

                        TG.BLL.cm_ContactPersionInfo bll_CPI = new BLL.cm_ContactPersionInfo();

                        string str_where = " 1=1 ";
                        //个人
                        if (previewPower == "0")
                        {
                            str_where += " AND UpdateBy =" + userSysNum;
                        }
                        else if (previewPower == "2")//部门
                        {
                            str_where += " AND Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + "))";
                        }

                        if (!string.IsNullOrEmpty(strWhere))
                        {
                            str_where += strWhere;
                        }
                        pageTotalCount = bll_CPI.GetRecordCount(str_where);
                    }

                    else if (tName == "cm_Coperation")
                    {
                        TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();

                        string strWhere_cprReletive = "";
                        string[] powerParams = strWhere.Split(',');
                        string cprName = powerParams[0];
                        string previewPower = powerParams[1];
                        string userSysNum = powerParams[2];
                        string userUnitNum = powerParams[3];

                        if (!string.IsNullOrEmpty(cprName))
                        {
                            strWhere_cprReletive += " AND cpr_Name like '%" + cprName + "%'";
                        }
                        //个人
                        if (previewPower == "0")
                        {
                            strWhere_cprReletive += " AND InsertUserID =" + userSysNum + "";
                        }//部门
                        else if (previewPower == "2")
                        {
                            strWhere_cprReletive += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                        }

                        pageTotalCount = bll.GetRecordCount(strWhere_cprReletive.ToString());
                    }

                    context.Response.Write(pageTotalCount);
                }
                //获得合同编号
                else if (action == "getCprNum")
                {
                    string selectOption = context.Request.Params["selectOption"] ?? "";
                    TG.BLL.cm_CoperationNumConfig bll_cprNum = new BLL.cm_CoperationNumConfig();
                    string str_where = "";
                    if (selectOption == "selectOption_cprNum")
                    {
                        string cprID = context.Request.Params["CprTypeID"] ?? "";
                        str_where = " ID='" + cprID + "'";
                    }
                    DataTable dt = bll_cprNum.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得工程负责人的所有部门数
                else if (action == "getGcFzrUnit")
                {
                    TG.BLL.tg_unit bll_unit = new BLL.tg_unit();
                    string str_where = "";
                    DataTable dt = bll_unit.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得某部门下的第一页工程负责人
                else if (action == "getGcFzrMem")
                {
                    string tabName = context.Request.Params["tabName"] ?? "";
                    string unit_ID = context.Request.Params["strWhere"] ?? "";
                    int pagesize = 10;
                    if (tabName == "tg_member" && unit_ID != "")
                    {
                        TG.BLL.tg_member bll_mem = new BLL.tg_member();
                        string str_where = " mem_Unit_ID='" + unit_ID + "' ";
                        DataTable dt = bll_mem.GetListByPage(str_where, pagesize, 1).Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            context.Response.Write(TableToJson(dt));
                        }
                    }
                }
                //获得某部门下的所有工程负责人总个数
                else if (action == "getAllGcFzrMemCount")
                {
                    string unit_ID = context.Request.Params["unit_ID"] ?? "";
                    TG.BLL.tg_member bll_mem = new BLL.tg_member();
                    string str_where = " mem_Unit_ID='" + unit_ID + "' ";
                    DataTable dt = bll_mem.GetList(str_where).Tables[0];
                    context.Response.Write(dt.Rows.Count);
                }
                //分页获取工程负责人数据
                else if (action == "getGcFzrMemByPage")
                {
                    string clickID = context.Request.Params["clickID"] ?? "";
                    string unit_ID = context.Request.Params["unit_ID"] ?? "";
                    int pagesize = Convert.ToInt32(context.Request.Params["pagesize"] ?? "10");
                    int nowPageIndex = 1;
                    int totalPageIndex = 0;
                    int gotoPageIndex = 1;
                    if (!String.IsNullOrEmpty(context.Request.Params["nowPageIndex"]))
                    {
                        nowPageIndex = Convert.ToInt32(context.Request.Params["nowPageIndex"] ?? "1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["totalPageIndex"]))
                    {
                        totalPageIndex = Convert.ToInt32(context.Request.Params["totalPageIndex"] ?? "-1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["gotoPageIndex"]))
                    {
                        gotoPageIndex = Convert.ToInt32(context.Request.Params["gotoPageIndex"] ?? "1");
                    }
                    string str_where = " mem_Unit_ID='" + unit_ID + "' ";
                    TG.BLL.tg_member bll_mem = new BLL.tg_member();
                    DataTable dt = null;

                    #region 分页判断
                    if (clickID == "gcfzr_FirstPage")
                    {
                        dt = bll_mem.GetListByPage(str_where, pagesize, 1).Tables[0];
                    }
                    else
                    {
                        if (clickID == "gcFzr_prevPage")
                        {
                            if (nowPageIndex <= totalPageIndex && nowPageIndex > 1)
                            {
                                dt = bll_mem.GetListByPage(str_where, pagesize, nowPageIndex - 1).Tables[0];
                            }
                        }
                        else if (clickID == "gcFzr_NextPage")
                        {
                            if (nowPageIndex < totalPageIndex && nowPageIndex > 0 && totalPageIndex > 0)
                            {
                                dt = bll_mem.GetListByPage(str_where, pagesize, nowPageIndex + 1).Tables[0];
                            }
                        }
                        else if (clickID == "gcFzr_LastPage")
                        {
                            if (nowPageIndex < totalPageIndex && nowPageIndex > 0 && totalPageIndex > 0)
                            {
                                dt = bll_mem.GetListByPage(str_where, pagesize, totalPageIndex).Tables[0];
                            }
                        }
                        else if (clickID == "gcFzr_gotoPage")
                        {
                            if (gotoPageIndex <= totalPageIndex && gotoPageIndex >= 1 && gotoPageIndex != nowPageIndex)
                            {
                                dt = bll_mem.GetListByPage(str_where, pagesize, gotoPageIndex).Tables[0];
                            }
                        }
                        else
                        {
                            dt = bll_mem.GetListByPage(str_where, pagesize, 1).Tables[0];
                        }

                    }
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                    #endregion
                }
                //获取甲方负责人数
                else if (action == "getJffzrByPage")
                {
                    string clickID = context.Request.Params["clickID"] ?? "";
                    string Cst_Id = context.Request.Params["Cst_Id"] ?? "";
                    string strWhere = context.Request.Params["strWhere"] ?? "";
                    string previewPower = context.Request.Params["previewPower"] ?? "";
                    string userSysNum = context.Request.Params["userSysNum"] ?? "";
                    string userUnitNum = context.Request.Params["userUnitNum"] ?? "";
                    //检查权限
                    if (previewPower == "0")
                    {
                        strWhere += " AND UpdateBy =" + userSysNum;
                    }
                    else if (previewPower == "2")
                    {
                        strWhere += " AND Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + "))";
                    }
                    int pagesize = int.Parse(context.Request.Params["pagesize"] ?? "10");
                    int nowPageIndex = 1;
                    int totalPageIndex = 0;
                    int gotoPageIndex = 1;

                    string str_where = " 1=1 ";

                    if (!String.IsNullOrEmpty(context.Request.Params["nowPageIndex"]))
                    {
                        nowPageIndex = Convert.ToInt32(context.Request.Params["nowPageIndex"] ?? "1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["totalPageIndex"]))
                    {
                        totalPageIndex = Convert.ToInt32(context.Request.Params["totalPageIndex"] ?? "-1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["gotoPageIndex"]))
                    {
                        gotoPageIndex = Convert.ToInt32(context.Request.Params["gotoPageIndex"] ?? "1");
                    }
                    if (Cst_Id != "-1")//区别'项目添加',和'合同添加'中的'甲方负责人'
                    {
                        str_where = " AND Cst_Id=" + Cst_Id + " ";
                    }
                    else if (!string.IsNullOrEmpty(strWhere))
                    {
                        str_where += strWhere;
                    }
                    TG.BLL.cm_ContactPersionInfo bll_CPI = new BLL.cm_ContactPersionInfo();
                    DataTable dt = null;
                    #region 甲方负责人分页判断取值
                    if (clickID == "gcfzr_FirstPage" || clickID == "jffzr_firstPage")
                    {
                        dt = bll_CPI.GetListByPage(str_where, pagesize, 1).Tables[0];
                    }
                    else
                    {
                        if (clickID == "gcFzr_prevPage" || clickID == "jffzr_prevPage")
                        {
                            if (nowPageIndex <= totalPageIndex && nowPageIndex > 1)
                            {
                                dt = bll_CPI.GetListByPage(str_where, pagesize, nowPageIndex - 1).Tables[0];
                            }
                        }
                        else if (clickID == "gcFzr_NextPage" || clickID == "jffzr_nextPage")
                        {
                            if (nowPageIndex < totalPageIndex && nowPageIndex > 0 && totalPageIndex > 0)
                            {
                                dt = bll_CPI.GetListByPage(str_where, pagesize, nowPageIndex + 1).Tables[0];
                            }
                        }
                        else if (clickID == "gcFzr_LastPage" || clickID == "jffzr_lastPage")
                        {
                            if (nowPageIndex < totalPageIndex && nowPageIndex > 0 && totalPageIndex > 0)
                            {
                                dt = bll_CPI.GetListByPage(str_where, pagesize, totalPageIndex).Tables[0];
                            }
                        }
                        else if (clickID == "gcFzr_gotoPage" || clickID == "jffzr_gotoPage")
                        {
                            if (gotoPageIndex <= totalPageIndex && gotoPageIndex >= 1 && gotoPageIndex != nowPageIndex)
                            {
                                dt = bll_CPI.GetListByPage(str_where, pagesize, gotoPageIndex).Tables[0];
                            }
                        }
                        else
                        {
                            dt = bll_CPI.GetListByPage(str_where, pagesize, 1).Tables[0];
                        }

                    }
                    #endregion
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }

                }
                //获得合同关联数据
                else if (action == "getReletiveCpr")
                {
                    StringBuilder strWhere_cprReletive = new StringBuilder();
                    string cprName = context.Request.Params["cprName"] ?? "";
                    string previewPower = context.Request.Params["previewPower"] ?? "";
                    string userSysNum = context.Request.Params["userSysNum"] ?? "";
                    string userUnitNum = context.Request.Params["userUnitNum"] ?? "";
                    string clickID = context.Request.Params["clickID"] ?? "";
                    int pagesize = int.Parse(context.Request.Params["pagesize"] ?? "10");
                    int nowPageIndex = 1;
                    int totalPageIndex = 0;
                    int gotoPageIndex = 1;
                    if (!String.IsNullOrEmpty(context.Request.Params["nowPageIndex"]))
                    {
                        nowPageIndex = int.Parse(context.Request.Params["nowPageIndex"] ?? "1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["totalPageIndex"]))
                    {
                        totalPageIndex = int.Parse(context.Request.Params["totalPageIndex"] ?? "-1");
                    }
                    if (!String.IsNullOrEmpty(context.Request.Params["gotoPageIndex"]))
                    {
                        gotoPageIndex = int.Parse(context.Request.Params["gotoPageIndex"] ?? "1");
                    }
                    if (!String.IsNullOrEmpty(cprName))
                    {
                        strWhere_cprReletive.Append("AND cpr_Name like '%" + cprName + "%'");
                    }
                    //个人
                    if (previewPower == "0")
                    {
                        strWhere_cprReletive.Append(" AND InsertUserID ='" + userSysNum + "'");
                    }//部门
                    else if (previewPower == "2")
                    {
                        strWhere_cprReletive.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")");
                    }
                    string orderByStr = " cpr_Id DESC";
                    TG.BLL.cm_Coperation cpr = new TG.BLL.cm_Coperation();
                    DataTable dt = null;
                    if (clickID == "reletiveCpr_firstPage")
                    {
                        nowPageIndex = 1;
                    }
                    else if (clickID == "reletiveCpr_prevPage")
                    {
                        nowPageIndex--;
                    }
                    else if (clickID == "reletiveCpr_nextPage")
                    {
                        nowPageIndex++;
                    }
                    else if (clickID == "reletiveCpr_lastPage")
                    {
                        nowPageIndex = totalPageIndex;
                    }
                    else if (clickID == "reletiveCpr_gotoPage")
                    {
                        nowPageIndex = gotoPageIndex;
                    }
                    else
                    {
                        nowPageIndex = 1;
                    }
                    dt = cpr.GetListByPage(strWhere_cprReletive.ToString(), pagesize, nowPageIndex, orderByStr).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }

                else if (action == "getJffzrData")
                {
                    string previewPower = context.Request.Params["previewPower"] ?? "";
                    string userSysNum = context.Request.Params["userSysNum"] ?? "";
                    string userUnitNum = context.Request.Params["userUnitNum"] ?? "";
                    string Cst_Id = context.Request.Params["strWhere"] ?? "";

                    string str_where = " Cst_Id='" + Cst_Id + "' ";
                    if (previewPower == "0")
                    {
                        str_where += " and UpdateBy =" + userSysNum;
                    }
                    else if (previewPower == "2")//部门
                    {
                        str_where += " AND Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + "))";
                    }
                    int pagesize = 10;
                    TG.BLL.cm_ContactPersionInfo bll_CPI = new BLL.cm_ContactPersionInfo();
                    DataTable dt = bll_CPI.GetListByPage(str_where, pagesize, 1).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得客户列表
                else if (action == "getCustomerList")
                {
                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string previewPower = context.Request.Params["previewPower"] ?? "";
                    string userSysNum = context.Request.Params["userSysNum"] ?? "";
                    string userUnitNum = context.Request.Params["userUnitNum"] ?? "";

                    string str_where = "";
                    //个人
                    if (previewPower == "0")
                    {
                        str_where = " AND InsertUserID =" + userSysNum;
                    }
                    else if (previewPower == "2")
                    {
                        str_where = " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                    }
                    DataTable dt = bll_cst.GetList(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                else if (action == "getJffzrInfo")
                {
                    string previewPower = context.Request.Params["previewPower"] ?? "";
                    string userSysNum = context.Request.Params["userSysNum"] ?? "";
                    string userUnitNum = context.Request.Params["userUnitNum"] ?? "";

                    string pagesizeStr = context.Request.Params["pagesize"] ?? "";
                    string pageIndexStr = context.Request.Params["pageIndex"] ?? "";
                    int pagesize = 5;
                    int pageIndex = 1;
                    if (!string.IsNullOrEmpty(pagesizeStr))
                    {
                        pagesize = int.Parse(pagesizeStr);
                    }
                    if (!string.IsNullOrEmpty(pageIndexStr))
                    {
                        pageIndex = int.Parse(pageIndexStr);
                    }
                    TG.BLL.cm_ContactPersionInfo bll_CPI = new BLL.cm_ContactPersionInfo();

                    string str_where = " 1=1 ";
                    //个人
                    if (previewPower == "0")
                    {
                        str_where += " AND UpdateBy =" + userSysNum;
                    }
                    else if (previewPower == "2")
                    {
                        str_where += " AND Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + "))";
                    }
                    DataTable dt = bll_CPI.GetListByPage(str_where, pagesize, pageIndex).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得数据总数
                else if (action == "getDataCount")
                {
                    string tName = context.Request.Params["tName"] ?? "";
                    string tId = context.Request.Params["tId"] ?? "";
                    if (tName == "cm_CPI")
                    {
                        string previewPower = context.Request.Params["previewPower"] ?? "";
                        string userSysNum = context.Request.Params["userSysNum"] ?? "";
                        string userUnitNum = context.Request.Params["userUnitNum"] ?? "";

                        TG.BLL.cm_ContactPersionInfo bll_CPI = new BLL.cm_ContactPersionInfo();

                        string str_where = " Cst_Id='" + tId + "'";
                        //个人
                        if (previewPower == "0")
                        {
                            str_where += " and UpdateBy =" + userSysNum;
                        }
                        else if (previewPower == "2")//部门
                        {
                            str_where += " AND Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + "))";
                        }
                        DataTable dt = bll_CPI.GetList(str_where).Tables[0];
                        context.Response.Write(dt.Rows.Count);
                    }
                }
                //查询入账单号
                else if (action == "getFinancial")
                {
                    string name = context.Request.Params["name"] ?? "";
                    decimal account = context.Request.Params["tName"] == "" ? 0 : decimal.Parse(context.Request.Params["account"]);
                    string inTime = context.Request.Params["inTime"] ?? "";
                    string unit = context.Request.Params["unit"] ?? "";
                    TG.BLL.ImportFinancialData bll = new BLL.ImportFinancialData();
                    DataTable dt = bll.GetFinanalNum(name, account, inTime, unit).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //By fbw 20130917 修改
                //获取数据,可根据条件,权限
                else if (action == "getDataToDivDialog")
                {
                    int pageSize = int.Parse(context.Request.Params["pageSize"] ?? "10");
                    int pageIndex = int.Parse(context.Request.Params["pageIndex"] ?? "1");
                    if (pageIndex == 0)//如果要取得是第0页,则取前1000行,即全部数据
                    {
                        pageSize = 1000;
                    }
                    string strWhere = context.Request.Params["strWhere"] ?? "";
                    strWhere = TG.Common.StringPlus.SqlSplit(strWhere);//去除sql关键字
                    string strMark = context.Request.Params["mark"] ?? "";//标识
                    bool userIsPower = Convert.ToBoolean(context.Request.Params["isPower"] ?? "true");//是否需要权限判断
                    string previewPower = "";
                    string userSysNum = "";
                    string userUnitNum = "";
                    string notShowUnitList = "";
                    if (userIsPower)
                    {
                        previewPower = context.Request.Params["previewPower"] ?? "0";
                        userSysNum = context.Request.Params["userSysNum"] ?? "";
                        userUnitNum = context.Request.Params["userUnitNum"] ?? "";
                        notShowUnitList = context.Request.Params["notShowUnitList"] ?? "";

                    }
                    DataTable dt = null;
                    switch (strMark)
                    {
                        #region 承接部门
                        case "proCjbm":
                            TG.BLL.tg_unit bll_unit = new BLL.tg_unit();
                            string str_where = " (unit_ParentID=0 and unit_IsEndUnit=0) or (unit_ParentID <>0 and unit_IsEndUnit=0) ";
                            if (!string.IsNullOrEmpty(strWhere))
                            {
                                str_where += " and " + strWhere;
                            }
                            dt = bll_unit.GetListByPage(str_where, pageSize, pageIndex).Tables[0];
                            break;
                        #endregion
                        #region 合同关联
                        case "reletiveCpr":
                            StringBuilder strWhere_cprReletive = new StringBuilder();
                            bool isSearchUnit = false;
                            if (!String.IsNullOrEmpty(strWhere))
                            {
                                string[] str_cprName_Unit = null;
                                str_cprName_Unit = strWhere.Split('|');
                                if (!String.IsNullOrEmpty(str_cprName_Unit[0]))
                                {
                                    strWhere_cprReletive.Append(" AND cpr_Name like '%" + str_cprName_Unit[0] + "%'");
                                }
                                if (!String.IsNullOrEmpty(str_cprName_Unit[1]))
                                {
                                    strWhere_cprReletive.Append(" AND cpr_Unit='" + str_cprName_Unit[2] + "' ");

                                    if (userUnitNum != str_cprName_Unit[1])
                                    {
                                        isSearchUnit = true;
                                    }
                                    else
                                    {
                                        isSearchUnit = true;
                                        // userUnitNum = str_cprName_Unit[1];
                                    }

                                }

                            }
                            //个人InsertUserID
                            if (previewPower == "0")
                            {
                                strWhere_cprReletive.Append(" AND (InsertUserID =" + userSysNum + " OR PMUserID=" + userSysNum + ")");
                            }
                            //部门
                            else if (previewPower == "2")
                            {
                                strWhere_cprReletive.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + userUnitNum + ")");
                            }

                            string orderByStr = " cpr_Id DESC";
                            TG.BLL.cm_Coperation cpr = new TG.BLL.cm_Coperation();
                            dt = cpr.GetListByPage(strWhere_cprReletive.ToString(), pageSize, pageIndex, orderByStr).Tables[0];
                            break;
                        #endregion
                        #region 甲方负责人
                        case "jffzrCpr":
                            TG.BLL.cm_ContactPersionInfo bll_CPI = new BLL.cm_ContactPersionInfo();
                            StringBuilder str_where_jffzr = new StringBuilder();
                            string userName = TG.Common.StringPlus.SqlSplit(context.Request.Params["Name"] ?? "");
                            string userPhone = TG.Common.StringPlus.SqlSplit(context.Request.Params["Phone"] ?? "");
                            string userDep = TG.Common.StringPlus.SqlSplit(context.Request.Params["Department"] ?? "");
                            //甲方负责人名称
                            if (!String.IsNullOrEmpty(userName))
                            {
                                strWhere += " Name like '%" + userName + "%'";
                            }
                            //电话号码
                            if (!String.IsNullOrEmpty(userPhone))
                            {
                                if (!String.IsNullOrEmpty(strWhere))
                                {
                                    strWhere += " or Phone like '%" + userPhone + "%'";
                                }
                                else
                                {
                                    strWhere += " Phone like '%" + userPhone + "%'";
                                }
                            }
                            //部门
                            if (!String.IsNullOrEmpty(userDep))
                            {
                                if (!String.IsNullOrEmpty(strWhere))
                                {
                                    strWhere += " or Department like '%" + userDep + "%'";
                                }
                                else
                                {
                                    strWhere += " Department like '%" + userDep + "%'";
                                }
                            }
                            if (!String.IsNullOrEmpty(strWhere))
                            {
                                str_where_jffzr.AppendFormat(" ({0}) ", strWhere);
                            }
                            //个人权限
                            if (previewPower == "0")
                            {
                                if (strWhere.Length > 0)
                                {
                                    str_where_jffzr.AppendFormat("  and UpdateBy ='{0}'", userSysNum);
                                }
                                else
                                {
                                    str_where_jffzr.AppendFormat(" UpdateBy='{0}'", userSysNum);
                                }
                            }
                            else if (previewPower == "2")//部门权限
                            {
                                if (str_where_jffzr.Length > 0)
                                {
                                    str_where_jffzr.Append(" AND (Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")))");
                                }
                                else
                                {
                                    str_where_jffzr.Append(" (Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")))");
                                }
                            }
                            dt = bll_CPI.GetListByPage(str_where_jffzr.ToString(), pageSize, pageIndex).Tables[0];
                            break;
                        #endregion
                        #region 合同类型
                        case "cprType":
                            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
                            string str_where_cprType = " dic_Type='cpr_lx'";
                            dt = bll_dic.GetList(str_where_cprType).Tables[0];
                            break;
                        #endregion
                        #region 工程负责人-部门
                        case "gcfzrCprUnit":
                            TG.BLL.tg_unit bll_unit_gcfzr = new TG.BLL.tg_unit();
                            string strWhere_CprUnit = "";
                            if (previewPower == "0")
                            {
                                strWhere_CprUnit += " unit_ID= " + userUnitNum;
                            }
                            else if (previewPower == "2")
                            {
                                strWhere_CprUnit += " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + userSysNum + ")";
                            }
                            if (previewPower == "1")
                            {
                                if (!String.IsNullOrEmpty(notShowUnitList))
                                {
                                    strWhere_CprUnit += " unit_ID NOT IN (" + notShowUnitList + ")";
                                }
                            }
                            //2016-7-7
                            if (!string.IsNullOrEmpty(strWhere_CprUnit))
                            {
                                strWhere_CprUnit += " AND unit_ParentID<>0 ";
                                //2017年4月18日 加载可见部门
                                strWhere_CprUnit += " AND Exists (select ID from cm_DropUnitList DU where flag=0 AND DU.unit_ID=tg_unit.unit_ID)";
                            }
                            else
                            {
                                strWhere_CprUnit += " unit_ParentID<>0 ";
                                strWhere_CprUnit += " AND Exists (select ID from cm_DropUnitList DU where flag=0 AND DU.unit_ID=tg_unit.unit_ID)";
                            }
                            dt = bll_unit_gcfzr.GetList(strWhere_CprUnit).Tables[0];
                            break;
                        #endregion
                        #region 工程负责人-人员
                        case "gcfzrCprMem":
                            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                            string str_where_gcfzr = " t.mem_Unit_ID=" + strWhere;
                            dt = bll_mem.GetListByPage(str_where_gcfzr, pageSize, pageIndex).Tables[0];
                            break;
                        #endregion
                        #region 甲方负责人-公司
                        case "jffzrCop":
                            TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                            StringBuilder strwhere_jffzrCop = new StringBuilder();
                            if (previewPower == "0")
                            {
                                strwhere_jffzrCop.Append(" and InsertUserID =" + userSysNum);
                            }
                            else if (previewPower == "2")
                            {
                                strwhere_jffzrCop.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")");
                            }
                            if (!String.IsNullOrEmpty(strWhere))
                            {
                                strwhere_jffzrCop.AppendFormat(" AND Cst_Name like '%{0}%'", strWhere);
                            }
                            dt = bll_cst.GetList(strwhere_jffzrCop.ToString()).Tables[0];
                            break;
                        #endregion
                        #region 甲方负责人-人员
                        case "jffzrMem":
                            TG.BLL.cm_ContactPersionInfo bll_per = new TG.BLL.cm_ContactPersionInfo();
                            string str_where_jffzrMem = " Cst_Id=" + strWhere;
                            dt = bll_per.GetListByPage(str_where_jffzrMem, pageSize, pageIndex).Tables[0];
                            break;
                        #endregion
                        #region 合同编号
                        case "cprNumUnit":
                            TG.BLL.cm_CoperationNumConfig bll_CprNum = new TG.BLL.cm_CoperationNumConfig();
                            string strWhere_cprNum = "";
                            if (!String.IsNullOrEmpty(strWhere))
                            {
                                strWhere_cprNum += " ID=" + strWhere;
                            }
                            dt = bll_CprNum.GetList(strWhere_cprNum).Tables[0];
                            break;
                        #endregion
                        #region 项目工程编号
                        case "proNum":
                            TG.BLL.cm_ProjectNumConfig bll_cprNum = new TG.BLL.cm_ProjectNumConfig();
                            string strWhere_proNum = "";
                            if (!String.IsNullOrEmpty(strWhere))
                            {
                                strWhere_proNum += " ID=" + strWhere;
                            }
                            dt = bll_cprNum.GetList(strWhere_proNum).Tables[0];
                            break;
                        #endregion
                        default:
                            break;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得数据总数,可根据条件,权限
                else if (action == "getDataAllCount")
                {
                    string strWhere = context.Request.Params["strWhere"] ?? "";
                    strWhere = TG.Common.StringPlus.SqlSplit(strWhere);//去除sql关键字
                    string strMark = context.Request.Params["mark"] ?? "";//标识
                    bool userIsPower = Convert.ToBoolean(context.Request.Params["isPower"] ?? "true");//是否需要权限判断
                    string previewPower = "";
                    string userSysNum = "";
                    string userUnitNum = "";
                    string notShowUnitList = "";
                    if (userIsPower)
                    {
                        previewPower = context.Request.Params["previewPower"] ?? "0";
                        userSysNum = context.Request.Params["userSysNum"] ?? "";
                        userUnitNum = context.Request.Params["userUnitNum"] ?? "";
                        notShowUnitList = context.Request.Params["notShowUnitList"] ?? "";
                    }
                    int pageTotalCount = 0;
                    switch (strMark)
                    {
                        #region 承接部门
                        case "proCjbm":
                            TG.BLL.tg_unit bll_unit = new BLL.tg_unit();
                            string str_where = " ((unit_ParentID=0 and unit_IsEndUnit=0) or (unit_ParentID <>0 and unit_IsEndUnit=0)) ";
                            if (!string.IsNullOrEmpty(strWhere))
                            {
                                str_where += " and " + strWhere;
                            }
                            pageTotalCount = bll_unit.GetRecordCount(str_where);
                            break;
                        #endregion
                        #region 合同关联
                        case "reletiveCpr":
                            StringBuilder strWhere_cprReletive = new StringBuilder();
                            bool isSearchUnit = false;
                            if (!string.IsNullOrEmpty(strWhere))
                            {
                                string[] str_cprName_Unit = null;
                                str_cprName_Unit = strWhere.Split('|');
                                if (!String.IsNullOrEmpty(str_cprName_Unit[0]))
                                {
                                    strWhere_cprReletive.Append(" AND cpr_Name like '%" + str_cprName_Unit[0] + "%'");
                                }
                                if (!String.IsNullOrEmpty(str_cprName_Unit[1]))
                                {
                                    strWhere_cprReletive.Append(" AND cpr_Unit='" + str_cprName_Unit[2] + "' ");
                                    if (userUnitNum != str_cprName_Unit[1])
                                    {
                                        isSearchUnit = true;
                                    }
                                    userUnitNum = str_cprName_Unit[1];
                                }
                            }
                            //个人
                            if (previewPower == "0")
                            {
                                strWhere_cprReletive.Append(" AND InsertUserID ='" + userSysNum + "'");
                            }//部门
                            else if (previewPower == "2")
                            {
                                strWhere_cprReletive.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")");
                            }
                            TG.BLL.cm_Coperation cpr = new TG.BLL.cm_Coperation();
                            pageTotalCount = cpr.GetRecordCount(strWhere_cprReletive.ToString());
                            break;
                        #endregion
                        #region 甲方负责人
                        case "jffzrCpr":
                            TG.BLL.cm_ContactPersionInfo bll_CPI = new BLL.cm_ContactPersionInfo();//无InsertUserID字段
                            StringBuilder str_where_jffzr = new StringBuilder();
                            string userName = TG.Common.StringPlus.SqlSplit(context.Request.Params["Name"] ?? "");
                            string userPhone = TG.Common.StringPlus.SqlSplit(context.Request.Params["Phone"] ?? "");
                            string userDep = TG.Common.StringPlus.SqlSplit(context.Request.Params["Department"] ?? "");
                            if (!String.IsNullOrEmpty(userName))
                            {
                                strWhere += " Name like '%" + userName + "%'";
                            }
                            if (!String.IsNullOrEmpty(userPhone))
                            {
                                if (!String.IsNullOrEmpty(strWhere))
                                {
                                    strWhere += " or Phone like '%" + userPhone + "%'";
                                }
                                else
                                {
                                    strWhere += " Phone like '%" + userPhone + "%'";
                                }
                            }
                            if (!String.IsNullOrEmpty(userDep))
                            {
                                if (!String.IsNullOrEmpty(strWhere))
                                {
                                    strWhere += " or Department like '%" + userDep + "%'";
                                }
                                else
                                {
                                    strWhere += " Department like '%" + userDep + "%'";
                                }
                            }
                            if (!String.IsNullOrEmpty(strWhere))
                            {
                                str_where_jffzr.AppendFormat(" ({0}) ", strWhere);
                            }
                            if (previewPower == "0")
                            {
                                if (strWhere.Length > 0)
                                {
                                    str_where_jffzr.AppendFormat(" and UpdateBy ='{0}'", userSysNum);
                                }
                                else
                                {
                                    str_where_jffzr.AppendFormat(" UpdateBy='{0}'", userSysNum);
                                }
                            }//部门
                            else if (previewPower == "2")
                            {
                                if (str_where_jffzr.Length > 0)
                                {
                                    str_where_jffzr.Append(" AND (Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")))");
                                }
                                else
                                {
                                    str_where_jffzr.Append(" (Cst_Id IN (Select Cst_Id From cm_CustomerInfo Where InsertUserID In (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")))");
                                }
                            }
                            pageTotalCount = bll_CPI.GetRecordCount(str_where_jffzr.ToString());
                            break;
                        #endregion
                        #region 工程负责人人员
                        case "gcfzrCprMem":
                            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                            string str_where_gcfzr = " mem_Unit_ID=" + strWhere;
                            pageTotalCount = bll_mem.GetRecordCount(str_where_gcfzr);
                            break;
                        #endregion
                        #region 甲方负责人人员
                        case "jffzrMem":
                            TG.BLL.cm_ContactPersionInfo bll_per = new TG.BLL.cm_ContactPersionInfo();
                            string str_where_jffzrMem = " Cst_Id=" + strWhere;
                            pageTotalCount = bll_per.GetRecordCount(str_where_jffzrMem);
                            break;
                        #endregion
                        default:
                            break;
                    }
                    if (pageTotalCount > 0)
                    {
                        context.Response.Write(pageTotalCount);
                    }
                }
                //获得合同关联里的生产部门
                else if (action == "getReleCprUnit")
                {
                    string previewPower = context.Request.Params["previewPower"] ?? "0";
                    string userSysNum = context.Request.Params["userSysNum"] ?? "";
                    string userUnitNum = context.Request.Params["userUnitNum"] ?? "";
                    TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
                    string strWhere = "";
                    //权限判断
                    if (previewPower == "0")
                    {
                        strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + userSysNum + ")";
                    }
                    else if (previewPower == "2")
                    {
                        strWhere = " unit_ID= " + userUnitNum;
                    }
                    DataTable dt = bll_unit.GetList(strWhere).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得已使用的合同/项目编号
                else if (action == "removeUsedCprNum")
                {
                    DataTable dt = null;
                    string fixStr = context.Request.Params["fixStr"] ?? "0";
                    string numType = context.Request.Params["type"] ?? "";//1 为项目
                    string sqlStr = "";
                    if (numType == "1")
                    {
                        sqlStr = "select ProNumber from cm_ProjectNumber where ProNumber like'%" + fixStr + "%'";
                    }
                    else
                    {
                        sqlStr = "select cpr_No from cm_Coperation where cpr_No like'%" + fixStr + "%'";
                    }
                    dt = TG.DBUtility.DbHelperSQL.Query(sqlStr).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //开票明细
                else if (action == "getInvoiceDataList")
                {
                    string cpr_id = context.Request.Params["cprId"] ?? "0";
                    string pro_id = context.Request.Params["proid"] ?? "0";
                    TG.BLL.cm_ProjectCharge proChargeBll = new BLL.cm_ProjectCharge();
                    DataTable dt = null;
                    string sqlTxt = @"select a.*,CONVERT(nvarchar(10), InvoiceDate,111) as Invoice_Date,b.mem_Name as InsertUserName,InsertDate from cm_ProjectInvoice
                                    a inner join tg_member b
                                    on a.InsertUserID=b.mem_ID  where a.cprID=" + cpr_id + " and projID=" + pro_id + " order by InsertDate desc";
                    dt = DBUtility.DbHelperSQL.Query(sqlTxt).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                #region 合同收费管理使用
                //获得合同收费详细信息
                else if (action == "getChargeDataList")
                {
                    string proSysNo = context.Request.Params["proSysNo"] ?? "";
                    TG.BLL.cm_ProjectCharge proChargeBll = new BLL.cm_ProjectCharge();
                    DataTable dt = null;
                    string sqlTxt = @"select p.ID,p.projID,p.Acount,p.FromUser,t.mem_Name,p.InAcountTime,p.Status,P.InAcountCode,p.ProcessMark 
                    from cm_ProjectCharge p left join tg_member t on p.InAcountUser=t.mem_ID where p.projID=" + proSysNo;
                    //dt = proChargeBll.GetList(strWhere).Tables[0];
                    dt = DBUtility.DbHelperSQL.Query(sqlTxt).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                //获得合同收费所需值
                else if (action == "getAddEditCharge")
                {
                    string proSysNo = context.Request.Params["proSysNo"] ?? "";
                    string projallcount = context.Request.Params["projallcount"] ?? "";
                    StringBuilder jsonbuild = new StringBuilder();
                    //已确认收款
                    string strSql = " Select Sum(Acount) From cm_ProjectCharge Where status<>'B'  and  projID=" + proSysNo;
                    TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                    string rlt = bll_db.GetList(strSql).Tables[0].Rows[0][0].ToString();
                    if (rlt == "")
                    {
                        rlt = "0.000000";
                    }
                    jsonbuild.Append("[{\"noPayCount\":\"");
                    jsonbuild.Append((Convert.ToDecimal(projallcount) - Convert.ToDecimal(rlt)).ToString());
                    jsonbuild.Append("\",");
                    //承接部门
                    TG.Model.cm_Coperation model = new TG.BLL.cm_Coperation().GetModel(int.Parse(proSysNo));
                    if (model != null)
                    {
                        jsonbuild.Append("\"unit\":\"");
                        jsonbuild.Append(model.cpr_Unit);
                        jsonbuild.Append("\",");
                        //负责人
                        jsonbuild.Append("\"pmusername\":\"");
                        jsonbuild.Append(model.ChgPeople);
                        jsonbuild.Append("\",");
                    }
                    //计算已经收款
                    string strSqlTxt = " Select Sum(Acount) From cm_ProjectCharge Where status<>'B' AND projID=" + proSysNo;
                    rlt = bll_db.GetList(strSqlTxt).Tables[0].Rows[0][0].ToString();
                    if (rlt == "")
                    {
                        rlt = "0.000000";
                    }
                    jsonbuild.Append("\"payComplete\":\"");
                    if (Convert.ToDecimal(rlt) >= Convert.ToDecimal(projallcount))
                    {
                        jsonbuild.Append("1\"");
                    }
                    else
                    {
                        jsonbuild.Append("0\"");
                    }
                    jsonbuild.Append("}]");
                    context.Response.Write(jsonbuild.ToString());
                }
                #endregion

                else if (action == "fzr")
                {
                    string unitid = context.Request["unitid"];
                    string strwhere = " mem_Principalship_ID=2 AND mem_Unit_ID=" + unitid;
                    List<TG.Model.tg_member> list = new TG.BLL.tg_member().GetModelList(strwhere);
                    if (list.Count > 0)
                    {
                        string memName = "";
                        foreach (TG.Model.tg_member mem in list)
                        {
                            memName += mem.mem_Name + ",";
                        }
                        memName = memName.Remove(memName.Length - 1);
                        context.Response.Write(memName);
                    }
                    else
                    {
                        context.Response.Write("无");
                    }
                }
                else if (action == "selectattachBycstId")//查询kehu附件
                {
                    string str_cprid = context.Request.Params["cstid"] ?? "";
                    TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                    string str_where = " Cst_Id=" + str_cprid;
                    DataTable dt = bll_att.GetListAboutUsername(str_where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        context.Response.Write(TableToJson(dt));
                    }
                }
                else if (action == "costtypeRepeat")//判断是否重复
                {
                    string costunit = context.Request.Params["costunit"] ?? "";
                    string costName = context.Request.Params["costName"] ?? "";
                    string id = context.Request.Params["id"] ?? "";
                    string isInput = context.Request.Params["isInput"] ?? "";
                    string type = "";
                    if (costunit == "0")
                    {
                        type = "0";
                    }
                    else if (costunit == "1")
                    {
                        type = "1";
                    }
                    else if (costunit == "2")
                    {
                        type = "2";
                    }
                    else if (costunit == "3")
                    {
                        type = "3";
                    }
                    else
                    {
                        type = "4";
                    }
                    TG.BLL.cm_CostType bll = new TG.BLL.cm_CostType();
                    bool flag = bll.JudgeData(type, costName, id, isInput);
                    if (flag == true)
                    {
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
                else if (action == "divideintoRepeat")//判断院所比例是否重复
                {
                    string unit = context.Request.Params["unit"] ?? "";
                    string percent = context.Request.Params["percent"] ?? "";
                    string id = context.Request.Params["id"] ?? "";
                    string year = context.Request.Params["year"] ?? "";

                    TG.BLL.cm_Divideinfo bll = new TG.BLL.cm_Divideinfo();
                    bool flag = bll.JudgeData(unit, percent, id, year);
                    if (flag == true)
                    {
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }
            }
        }
        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
