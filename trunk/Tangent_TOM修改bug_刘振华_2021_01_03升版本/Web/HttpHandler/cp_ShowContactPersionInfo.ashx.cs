﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.SessionState;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class cp_ShowContactPersionInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string str_rlt = "";
            string str_cstid = context.Request.Params["id"] ?? "0";

            TG.BLL.cm_ContactPersionInfo bll = new TG.BLL.cm_ContactPersionInfo();
            TG.Model.cm_ContactPersionInfo model = bll.GetModel(int.Parse(str_cstid));
            string str_ContactNo = model.ContactNo == null ? "" : model.ContactNo.Trim();
            str_rlt += str_ContactNo + "|";
            string str_Name = model.Name == null ? "" : model.Name.Trim();
            str_rlt += str_Name + "|";
            string str_Duties = model.Duties == null ? "" : model.Duties.Trim();
            str_rlt += str_Duties + "|";
            string str_Department = model.Department == null ? "" : model.Department.Trim();
            str_rlt += str_Department + "|";
            string str_Phone = model.Phone == null ? "" : model.Phone.Trim();
            str_rlt += str_Phone + "|";
            string str_Email = model.Email == null ? "" : model.Email.Trim();
            str_rlt += str_Email + "|";
            string str_bPhone = model.BPhone == null ? "" : model.BPhone.Trim();
            str_rlt += str_bPhone + "|";
            string str_Fphone = model.FPhone == null ? "" : model.FPhone.Trim();
            str_rlt += str_Fphone + "|";
            string str_bFax = model.BFax == null ? "" : model.BFax.Trim();
            str_rlt += str_bFax + "|";
            string str_Ffax = model.FFax == null ? "" : model.FFax.Trim();
            str_rlt += str_Ffax + "|";
            string str_remark = model.Remark == null ? "" : model.Remark.Trim();
            str_rlt += str_remark + "|";
            context.Response.Write(str_rlt);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


    }
}
