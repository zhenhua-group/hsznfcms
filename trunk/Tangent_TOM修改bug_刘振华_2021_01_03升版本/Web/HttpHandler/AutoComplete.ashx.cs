﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using TG.Common;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// AutoComplete 的摘要说明
    /// </summary>
    public class AutoComplete : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            //不让浏览器保存缓存
            context.Response.Buffer = true;
            context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";

            string action = context.Request.Params["action"] ?? "";
            string keywords = context.Request.Params["keywords"] ?? "";
            keywords = TG.Common.StringPlus.SqlSplit(keywords);//去除sql关键字           
            string previewPower = context.Request.Params["previewPower"] ?? "0";
            string userSysNum = context.Request.Params["userSysNum"];
            string userUnitNum = context.Request.Params["userUnitNum"];
            string unitID = context.Request.Params["unitID"];
            unitID = string.IsNullOrEmpty(unitID) ? "0" : unitID;
            string currYear = context.Request.Params["currYear"] ;
            currYear = string.IsNullOrEmpty(currYear) ? "0" : currYear;
            DataTable dt = null;
            string strsql = "",unitname="";
            if (Convert.ToInt32(unitID) > 0)
            {
                unitname = new TG.BLL.tg_unit().GetModel(Convert.ToInt32(unitID)).unit_Name;
            }
            //客户列表
            if (action == "Customer")
            {
                strsql = "select top 50 Cst_Name as OtherName from cm_CustomerInfo where 1=1 ";

                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and Cst_Name like '%" + keywords + "%' ";
                }

                //个人
                if (previewPower == "0")
                {
                    strsql += " AND InsertUserID =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }
                //年份
                if (Convert.ToInt32(currYear)>0)
                {
                    strsql += " and year(InsertDate)="+currYear.Trim()+"";
                }
                //生产部门
                if (Convert.ToInt32(unitID) > 0)
                {
                    strsql += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID="+unitID.Trim()+")";
                }
                strsql += " order by InsertDate desc,Cst_Id desc";
                
            }
            //联系人
            else if (action == "Contact")
            {
                strsql = "select top 50 CP.Name as OtherName from cm_ContactPersionInfo CP inner join cm_CustomerInfo C on C.cst_id=CP.cst_id where 1=1";
                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and CP.Name like '%" + keywords + "%'";
                }

                //个人
                if (previewPower == "0")
                {
                    strsql += " AND C.InsertUserID =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND C.InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }
                strsql += " order by CP.LastUpdate desc,CP.CP_Id desc";
            }


            //文档信息
            else if (action == "Attach")
            {
                strsql = "select top 50 FileName as OtherName from cm_AttachInfo where 1=1 and  cst_Id<>-1 ";
                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and FileName like '%" + keywords + "%'";
                }
                //个人
                if (previewPower == "0")
                {
                    strsql += " AND UploadUser =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND UploadUser IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }
                strsql += " order by UploadTime desc,ID desc";
            }
            //合同列表
            else if (action == "Corperation")
            {
                strsql = "select top 50 cpr_Name as OtherName from cm_Coperation where 1=1 ";
                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and cpr_Name like '%" + keywords + "%' ";
                }

                //个人
                if (previewPower == "0")
                {
                    strsql += " AND InsertUserID =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }
				   //年份
                if (Convert.ToInt32(currYear) > 0)
                {
                    strsql += " and year(cpr_SignDate)=" + currYear.Trim() + "";
                }
                //生产部门
                if (Convert.ToInt32(unitID) > 0)
                {

                    strsql += " AND cpr_Unit= '" + unitname.Trim() + "'";
                }

                strsql += " order by InsertDate desc,cpr_Id desc";
            }
            //合同报备列表
            else if (action == "Corperation_back")
            {
                strsql = "select top 50 cpr_Name as OtherName from cm_Coperation_back where 1=1 ";
                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and cpr_Name like '%" + keywords + "%' ";
                }

                //个人
                if (previewPower == "0")
                {
                    strsql += " AND InsertUserID =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }
                //生产部门
                if (Convert.ToInt32(unitID) > 0)
                {
                    strsql += " AND cpr_Unit='"+unitname.Trim()+"'";
                }
                strsql += " order by InsertDate desc";
            }
            //项目列表
            else if (action == "Project")
            {
                strsql = "select top 50 pro_Name as OtherName from cm_Project where 1=1 ";

                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and pro_Name like '%" + keywords + "%' ";
                }

                //个人
                if (previewPower == "0")
                {
                    strsql += " AND InsertUserID =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }
				 //年份
                if (Convert.ToInt32(currYear) > 0)
                {
                    strsql += " and year(pro_startTime)=" + currYear.Trim() + "";
                }
                //生产部门
                if (Convert.ToInt32(unitID) > 0)
                {
                    strsql += " AND Unit= '" + unitname.Trim() + "'";
                }
                strsql += " order by InsertDate desc,pro_ID desc";
               
            }
            //入账单号
            else if (action == "FinanalNum")
            {
                strsql = @"SELECT top 30 (type+ Year+Month+ CASE WHEN LEN(FinancialNo)=1 THEN '00'+CAST(FinancialNo as CHAR ) WHEN LEN(FinancialNo)=2 THEN '0'+CAST(FinancialNo as CHAR )  ELSE CAST( FinancialNo AS CHAR) END ) AS OtherName  FROM cm_FinancialReport WHERE 1=1 ";


                if (!string.IsNullOrEmpty(keywords))
                {
                    if (PageValidate.IsNumber(keywords))
                    {
                        strsql += " and Account like '%" + keywords + "%' ";
                    }
                    else
                    {
                        strsql += " and name like '%" + keywords + "%' ";
                    }
                }
                strsql += " order by ID desc";
            }
            //领导驾驶舱高级信息查询
            else if (action == "cp_Project")
            {
                strsql = "select top 50 pro_Name as OtherName from cm_Project where 1=1 ";

                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and pro_Name like '%" + keywords + "%' ";
                }

                //个人
                if (previewPower == "0")
                {
                    strsql += " AND InsertUserID =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }

                strsql += " order by InsertDate desc";

            }
            else if (action == "cp_Corperation")
            {
                strsql = "select top 50 cpr_Name as OtherName from cm_Coperation where 1=1 ";
                if (!string.IsNullOrEmpty(keywords))
                {
                    strsql += " and cpr_Name like '%" + keywords + "%' ";
                }

                //个人
                if (previewPower == "0")
                {
                    strsql += " AND InsertUserID =" + userSysNum;
                }
                //部门
                else if (previewPower == "2")
                {
                    strsql += " AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + userUnitNum + ")";
                }

                strsql += " order by InsertDate desc";
            }

            dt = TG.DBUtility.DbHelperSQL.Query(strsql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                context.Response.Write(TableToJson(dt));
            }

        }

        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}