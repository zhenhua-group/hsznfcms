﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using System;
using TG.Common;

namespace TG.Web.HttpHandler
{
    public class LoginHandler : IHttpHandler, IRequiresSessionState
    {
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "text/plain";
            string action = context.Request.Params["action"] ?? "no";
            if (action != "no")
            {
                if (action == "login")
                {
                    string str_login = System.Web.HttpUtility.UrlDecode(context.Request.Params["strlogin"]) ?? "1";
                    string str_pwd = System.Web.HttpUtility.UrlDecode(context.Request.Params["strpwd"]) ?? "1";
                    if (string.IsNullOrEmpty(str_login) || string.IsNullOrEmpty(str_pwd))
                    {
                        context.Response.Write("failed");
                        context.Response.End();
                        return;
                    }
                    if (str_login == "administrators" && str_pwd == "admin012345")
                    {
                        //保存Session
                        context.Session["memid"] = 1;
                        context.Session["memlogin"] = "admin";
                        context.Session["principal"] = "1"; 
                        context.Session["memname"] = "管理员";
                        //输出结果
                        context.Response.Write("success");
                    }
                    else if (GetPwdByLogin(str_login) == StringPlus.GetPwdEncryString(str_pwd))
                    {
                        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                        string str_where = " mem_Login='" + str_login + "'";
                        List<TG.Model.tg_member> models = bll_mem.GetModelList(str_where);
                        if (models.Count > 0)
                        {
                            TG.Model.tg_member model_mem = models[0];
                            //保存Session
                            context.Session["memid"] = model_mem.mem_ID;
                            context.Session["memlogin"] = model_mem.mem_Login;
                            context.Session["principal"] = model_mem.mem_Principalship_ID;
                        }
                        //输出结果
                        context.Response.Write("success");
                    }
                    else
                    {
                        context.Response.Write("failed");
                    }
                }
                else if (action == "chgpwd")
                {
                    string str_memlogin = this.UserInfo["memlogin"]??"0";
                    string str_old = context.Request.Params["oldpwd"] ?? "";
                    string str_new = context.Request.Params["newpwd"] ?? "";
                    if (GetPwdByLogin(str_memlogin) == StringPlus.GetPwdEncryString(str_old))
                    {
                        //加密密码
                        str_new = StringPlus.GetPwdEncryString(str_new);

                        string str_sql = " Update tg_member Set mem_Password='" + str_new + "' Where mem_Login='" + str_memlogin + "'";
                        TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                        int rowAffected = bll_db.ExcuteBySql(str_sql);
                        if (rowAffected > 0)
                        {
                            context.Response.Write("yes");
                        }
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
                else if (action == "exsitlogin")
                {
                    string str_login = context.Request.Params["memlogin"] ?? "";
                    if (str_login != "")
                    {
                        string str_sql = "Select mem_ID From tg_member Where mem_Login='" + str_login + "'";
                        TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                        int rowsCount = bll_db.GetList(str_sql).Tables[0].Rows.Count;
                        if (rowsCount > 0)
                        {
                            context.Response.Write("yes");
                        }
                    }
                    else
                    {
                        context.Response.Write("no");
                    }
                }
            }
        }
        /// <summary>
        /// 返回登录pwd
        /// </summary>
        /// <param name="strlogin"></param>
        /// <returns></returns>
        protected string GetPwdByLogin(string strlogin)
        {
            string str_rlt = "";
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            string str_where = " mem_Login='" + strlogin + "'";
            List<TG.Model.tg_member> models = bll_mem.GetModelList(str_where);
            if (models.Count > 0)
            {
                TG.Model.tg_member model_mem = models[0];
                str_rlt = model_mem.mem_Password;
            }
            return str_rlt;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
