﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.ProjectPlan
{
    /// <summary>
    /// ProPlanListHandler1 的摘要说明
    /// </summary>
    public class ProPlanListHandler1 : ProPlanListHandler
    {

        public override string ProcName
        {
            get
            {
                return "P_cm_ProjectPlanNo_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_ProjectPlanYes_jq";
            }
        }
    }
}