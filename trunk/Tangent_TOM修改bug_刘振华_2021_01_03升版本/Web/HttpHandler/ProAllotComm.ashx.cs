﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProAllotComm : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request.Params["flag"] ?? "";
            //判断提交动作
            if (action == "delallot")
            {
                string id = context.Request.Params["id"] ?? "0";
                //删除所有分配信息
                TG.BLL.ProjectAllotBP bll_allot = new TG.BLL.ProjectAllotBP();
                bll_allot.DeleteAllRecord(int.Parse(id));

                context.Response.Write("ok");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
