﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;

namespace TG.Web.HttpHandler
{
    public abstract class hm_projCountHandler : IHttpHandler
    {
        //院项目统计 存储过程名
        public abstract string ProcName { get; }
        //项目统计里显示所有项目 存储过程名
        public abstract string ProcName2 { get; }
        //项目统计里显示所有人员 存储过程名
        public abstract string ProcName3 { get; }
        //院组织架构统计列表存储过程名
        public abstract string ProcName4 { get; }
        //院文件数统计存储过程名
        public abstract string ProcName5 { get; }
        //院项目统计按状态
        public abstract string ProcName6 { get; }
        //院项目统计按阶段
        public abstract string ProcName7 { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }
        protected TG.BLL.QueryPagingParametersAnOther PagingParametersanother { get; set; }
        protected TG.BLL.QueryPagingParametersOther PagingParametersother { get; set; }
        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            //自定义url参数
            string strAction = context.Request.QueryString["action"];
            string strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
            string strWhere1 = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strWhere1"]);
            string strWhere2 = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strWhere2"]);
            var parameters = new TG.BLL.QueryPagingParameters
            {
                PageIndex = int.Parse(context.Request["PageIndex"]),
                PageSize = int.Parse(context.Request["PageSize"]),
                OrderBy = context.Request["OrderBy"],
                Sort = context.Request["Sort"],
                ProcedureName = ProcName,
                Where = strWhere
            };
            var parametersother = new TG.BLL.QueryPagingParametersOther
            {
                PageIndex = int.Parse(context.Request["PageIndex"]),
                PageSize = int.Parse(context.Request["PageSize"]),
                OrderBy = context.Request["OrderBy"],
                Sort = context.Request["Sort"],
                ProcedureName = ProcName,
                Where = strWhere,
                Where1 = strWhere1
            };
            var parametersanother = new TG.BLL.QueryPagingParametersAnOther
            {
                PageIndex = int.Parse(context.Request["PageIndex"]),
                PageSize = int.Parse(context.Request["PageSize"]),
                OrderBy = context.Request["OrderBy"],
                Sort = context.Request["Sort"],
                ProcedureName = ProcName,
                Where = strWhere,
                Where1 = strWhere1,
                Where2 = strWhere2
            };

            //加载数据
            if (strOper == null && strAction == null)
            {
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
                string unit_id = forms.Get("EmpId");
                string strResponse = "";
                if (bll_unit.DeleteList(unit_id))
                {
                    strResponse = "删除成功！";
                }
                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {

                parameters.Where = strWhere;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "del")
            {

                TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
                string unit_id = context.Request.QueryString["unitid"];
                string strResponse = "";
                if (bll_unit.DeleteList(unit_id))
                {
                    strResponse = "删除成功！";
                }
                context.Response.Write(strResponse);
            }
            else if (strAction == "showproject")
            {
                parameters.Where = strWhere;
                parameters.ProcedureName = ProcName2;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "showcount")
            {
                parameters.Where = strWhere;
                parameters.ProcedureName = ProcName3;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "constmems")
            {
                parameters.Where = strWhere;
                parameters.ProcedureName = ProcName4;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "projPackCount")
            {

                parametersanother.Where = strWhere;
                parametersanother.Where1 = "";
                parametersanother.Where2 = "";
                parametersanother.ProcedureName = ProcName5;
                //查询
                this.PagingParametersanother = parametersanother;

                context.Response.Write(GetJsonResultanother());
            }
            else if (strAction == "projPackCount11")//当月
            {
                StringBuilder strsql = new StringBuilder();
                StringBuilder strsql2 = new StringBuilder();
                string unit = context.Request.Params["unit"];
                string year = context.Request.Params["year"];
                string month = context.Request.Params["month"];
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strWhere += "AND unit_ID=" + unit;
                }
                //年份
                if (year != "-1")
                {//ver_date
                    if (month != "-1")
                    {
                        string begintime = year + "-" + month + "-1";
                        string donetime = year + "-" + month + "-" + TG.Common.TimeParser.GetMonthLastDate(int.Parse(year), int.Parse(month));
                        strsql.AppendFormat(" AND senddate between '{0}' and '{1}' ", begintime, donetime);
                        strsql2.AppendFormat(" AND ver_date between '{0}' and '{1}' ", begintime, donetime);
                    }
                }

                parametersanother.Where = strWhere;
                parametersanother.Where1 = strsql.ToString();
                parametersanother.Where2 = strsql2.ToString();
                parametersanother.ProcedureName = ProcName5;
                //查询
                parametersanother.ProcedureName = ProcName5;
                //查询
                this.PagingParametersanother = parametersanother;

                context.Response.Write(GetJsonResultanother());
            }
            else if (strAction == "projPackCount22")//季度
            {
                StringBuilder strsql = new StringBuilder();
                StringBuilder strsql2 = new StringBuilder();
                string unit = context.Request.Params["unit"];
                string year = context.Request.Params["year"];
                string quarter = context.Request.Params["quarter"];
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strWhere += "AND unit_ID=" + unit;
                }
                //年份
                if (year != "-1")
                {
                    if (quarter != "-1")
                    {

                        string begintime = "";
                        string donetime = "";
                        if (quarter == "第一季度")
                        {
                            begintime = year + "-1-1";
                            donetime = year + "-3-31";
                        }
                        if (quarter == "第二季度")
                        {
                            begintime = year + "-4-1";
                            donetime = year + "-6-30";
                        }
                        if (quarter == "第三季度")
                        {
                            begintime = year + "-7-1";
                            donetime = year + "-9-30";
                        }
                        if (quarter == "第四季度")
                        {
                            begintime = year + "-10-1";
                            donetime = year + "-12-31";
                        }
                        strsql.AppendFormat(" AND senddate  between '{0}' and '{1}' ", begintime, donetime);
                        strsql2.AppendFormat(" AND ver_date between '{0}' and '{1}' ", begintime, donetime);
                    }

                }
                parametersanother.Where = strWhere;
                parametersanother.Where1 = strsql.ToString();
                parametersanother.Where2 = strsql2.ToString();
                parametersanother.ProcedureName = ProcName5;
                //查询
                this.PagingParametersanother = parametersanother;

                context.Response.Write(GetJsonResultanother());
            }
            else if (strAction == "projPackCount33")//年份
            {
                StringBuilder strsql = new StringBuilder();
                StringBuilder strsql2 = new StringBuilder();
                string unit = context.Request.Params["unit"];
                string year = context.Request.Params["year"];
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strWhere += "AND unit_ID=" + unit;
                }
                //年份
                if (year != "-1")
                {
                    strsql.AppendFormat(" AND year(senddate)={0} ", year);
                    strsql2.AppendFormat(" AND year(ver_date)={0} ", year);
                }

                parametersanother.Where = strWhere;
                parametersanother.Where1 = strsql.ToString();
                parametersanother.Where2 = strsql2.ToString();
                parametersanother.ProcedureName = ProcName5;
                //查询
                this.PagingParametersanother = parametersanother;

                context.Response.Write(GetJsonResultanother());
            }
            else if (strAction == "showStatus")
            {
                parametersother.Where = strWhere;
                parametersother.Where1 = strWhere1;
                parametersother.ProcedureName = ProcName6;
                //查询
                this.PagingParametersother = parametersother;

                context.Response.Write(GetJsonResultother());
            }
            else if (strAction == "showprojectAboutStatus")
            {
                parameters.Where = strWhere;
                parameters.ProcedureName = ProcName7;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];
            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        } //返回json
        public virtual string GetJsonResultother()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParametersother.ProcedureName, PagingParametersother.ProcedureParameters).Tables[0];


            int total = PagingParametersother.Total;
            int pageCount = total % PagingParametersother.PageSize == 0 ? total / PagingParametersother.PageSize : total / PagingParametersother.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParametersother.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        public virtual string GetJsonResultanother()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParametersanother.ProcedureName, PagingParametersanother.ProcedureParameters).Tables[0];


            int total = PagingParametersanother.Total;
            int pageCount = total % PagingParametersanother.PageSize == 0 ? total / PagingParametersanother.PageSize : total / PagingParametersanother.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParametersanother.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        public bool IsReusable
        {
            get { return false; }
        }
    }
}