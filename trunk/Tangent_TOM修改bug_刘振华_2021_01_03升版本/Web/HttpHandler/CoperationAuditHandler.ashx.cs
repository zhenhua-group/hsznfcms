﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Newtonsoft.Json;
using TG.BLL;
using TG.Model;
using System.Web.SessionState;
using System.Collections.Generic;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class CoperationAuditHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        public HttpRequest Request { get { return HttpContext.Current.Request; } }
        public HttpResponse Response { get { return HttpContext.Current.Response; } }
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }

        public TG.Model.cm_CoperationAudit CoperationAuditEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_CoperationAudit obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_CoperationAudit>(data);
                }
                return obj;
            }
        }

        /// <summary>
        /// 审核记录SysNo
        /// </summary>
        public int CoperationAuditSysNo
        {
            get
            {
                int coperationAuditSysNo = 0;
                int.TryParse(Request["CoperationAuditSysNo"], out coperationAuditSysNo);
                return coperationAuditSysNo;
            }
        }

        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }
        //查询or更新
        public int Flag
        {
            get
            {
                int flag = 0;
                int.TryParse(Request["flag"], out flag);
                return flag;
            }
        }
        private TG.BLL.cm_CoperationAudit coperationBLL = new TG.BLL.cm_CoperationAudit();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                    case 0:
                        InsertCoperationAuditRecord();
                        break;
                    case 1:
                    case 2:
                        UpdateCoperatioAuditRecord();
                        break;
                    case 3:
                        DoAuditAgain();
                        break;
                }
                Response.End();
            }

        }
        /// <summary>
        /// 发起一条新的合同审批信息
        /// </summary>
        private void InsertCoperationAuditRecord()
        {
            if (Flag == 0)
            {
                //返回下阶段用户列表
                GetNextProcessRoleUser();
            }
            else
            {
                //合同审核实体
                TG.BLL.cm_CoperationAudit coperationAuditBLL = new TG.BLL.cm_CoperationAudit();
                //合同未审核
                if (!coperationAuditBLL.IsExist(CoperationAuditEntity.CoperationSysNo))
                {
                    //查询合同信息
                    TG.Model.cm_Coperation coperation = new TG.BLL.cm_Coperation().GetModel(CoperationAuditEntity.CoperationSysNo);
                    //保存审批阶段轨迹
                    int sysNo = coperationAuditBLL.InsertCoperatioAuditRecord(CoperationAuditEntity);
                    //给对应角色发送审批消息
                    if (sysNo > 0)
                    {
                        //获取审批实体
                        TG.Model.cm_CoperationAudit tempEntity = new TG.BLL.cm_CoperationAudit().GetModel(sysNo);
                        //实例消息实体
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            //ReferenceSysNo = tempEntity.SysNo.ToString(),
                            ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                            FromUser = CoperationAuditEntity.InUser,
                            InUser = CoperationAuditEntity.InUser,
                            MsgType = 1,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审"),
                            QueryCondition = coperation.cpr_Name,
                            Status = "A",
                            IsDone = "A",
                            ToRole = "0"
                        };
                        //声明
                        string sysMsgString = "";
                        //获取审批人列表人名
                        sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            Response.Write(sysMsgString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
            }
        }
        /// <summary>
        /// 修改审核记录
        /// </summary>
        private void UpdateCoperatioAuditRecord()
        {
            if (Flag == 0)
            {
                GetNextProcessRoleUser();
            }
            else
            {
                //项目审批实体
                TG.Model.cm_CoperationAudit oldEntity = coperationBLL.GetModel(CoperationAuditEntity.SysNo);
                //审批赋值
                TG.Model.cm_CoperationAudit EntityParameter = new TG.Model.cm_CoperationAudit
                {
                    SysNo = oldEntity.SysNo,
                    UndertakeProposal = oldEntity.UndertakeProposal,
                    OperateDepartmentProposal = oldEntity.OperateDepartmentProposal,
                    TechnologyDepartmentProposal = oldEntity.TechnologyDepartmentProposal,
                    GeneralManagerProposal = oldEntity.GeneralManagerProposal,
                    BossProposal = oldEntity.BossProposal,
                    ManageLevel = oldEntity.ManageLevel,
                    NeedLegalAdviser = oldEntity.NeedLegalAdviser,
                    LegalAdviserProposal = oldEntity.LegalAdviserProposal
                };

                switch (CoperationAuditEntity.Status)
                {
                    case "A":
                        EntityParameter.UndertakeProposal = CoperationAuditEntity.UndertakeProposal;
                        EntityParameter.Status = Action == 1 ? "B" : "C";
                        break;
                    case "B":
                        EntityParameter.OperateDepartmentProposal = CoperationAuditEntity.OperateDepartmentProposal;
                        EntityParameter.ManageLevel = CoperationAuditEntity.ManageLevel;
                        EntityParameter.NeedLegalAdviser = CoperationAuditEntity.NeedLegalAdviser;
                        EntityParameter.Status = Action == 1 ? "D" : "E";
                        break;
                    case "D":
                        if (EntityParameter.NeedLegalAdviser == 1)
                        {
                            //需要法律顾问的场合
                            EntityParameter.LegalAdviserProposal = CoperationAuditEntity.LegalAdviserProposal;
                            EntityParameter.Status = Action == 1 ? "F" : "G";
                        }
                        else
                        {
                            EntityParameter.TechnologyDepartmentProposal = CoperationAuditEntity.TechnologyDepartmentProposal;
                            EntityParameter.Status = Action == 1 ? "H" : "I";
                        }
                        break;
                    case "F":
                        EntityParameter.TechnologyDepartmentProposal = CoperationAuditEntity.TechnologyDepartmentProposal;
                        EntityParameter.Status = Action == 1 ? "H" : "I";
                        break;
                    case "H":
                        EntityParameter.GeneralManagerProposal = CoperationAuditEntity.GeneralManagerProposal;
                        EntityParameter.Status = Action == 1 ? "J" : "K";
                        break;
                }

                EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) == true ? CoperationAuditEntity.AuditUser : oldEntity.AuditUser + "," + CoperationAuditEntity.AuditUser;
                EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) == true ? DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") : oldEntity.AuditDate + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //更新
                int count = coperationBLL.UpdateCoperatioAuditRecord(EntityParameter);

                //查询合同信息
                TG.Model.cm_Coperation coperation = new TG.BLL.cm_Coperation().GetModel(oldEntity.CoperationSysNo);
                TG.Model.cm_CoperationAudit tempEntity = new TG.BLL.cm_CoperationAudit().GetModel(oldEntity.SysNo);

                if (tempEntity.Status == "H" || tempEntity.Status == "J")
                {
                    //所管项目到技术处流程技术
                    if (EntityParameter.ManageLevel == 1)
                    {
                        //流程结束发消息到流程发起人
                        if (tempEntity.Status == "H")
                        {
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", EntityParameter.SysNo.ToString(), tempEntity.Status),
                                FromUser = oldEntity.InUser,
                                InUser = UserSysNo,
                                MsgType = 1,
                                ToRole = "0",
                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审通过"),
                                QueryCondition = coperation.cpr_Name,
                                Status = "A",
                                IsDone = "B"
                            };
                            int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                            if (resultcount > 0)
                            {
                                Response.Write("1");
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                    }
                    else
                    {
                        //院管项目
                        if (tempEntity.Status == "J")
                        {
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", EntityParameter.SysNo.ToString(), tempEntity.Status),
                                FromUser = oldEntity.InUser,
                                InUser = UserSysNo,
                                MsgType = 1,
                                ToRole = "0",
                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审通过"),
                                QueryCondition = coperation.cpr_Name,
                                IsDone = "B",
                                Status = "A"
                            };
                            int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                            if (resultcount > 0)
                            {
                                Response.Write("1");
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                        else
                        {
                            string roleSysNo = GetStatusProcess(5);
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", EntityParameter.SysNo.ToString(), tempEntity.Status),
                                FromUser = oldEntity.InUser,
                                InUser = UserSysNo,
                                MsgType = 1,
                                ToRole = roleSysNo,
                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审"),
                                QueryCondition = coperation.cpr_Name,
                                IsDone = "A",
                                Status = "A"
                            };
                            //返回消息实体
                            string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                            if (!string.IsNullOrEmpty(sysMsgString))
                            {
                                Response.Write(sysMsgString);
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                    }
                }
                else
                {
                    TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    //是审核通过的场合
                    if (Action == 1)
                    {
                        //发送给选择人的消息，再次赋值ToRole和FromUser，没什么意义，会在添加消息时，重复赋值。
                        //如果是一级项目发送消息给总经理
                        bool needMsg = true;

                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}",oldEntity.SysNo.ToString(), tempEntity.Status),
                            FromUser = UserSysNo,
                            InUser = UserSysNo,
                            MsgType = 1,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审"),
                            QueryCondition = coperation.cpr_Name,
                            IsDone = "A",
                            Status = "A"
                        };
                        //审批角色
                        string roleSysNo = string.Empty;

                        //发送消息给相关人员
                        switch (tempEntity.Status)
                        {
                            case "B":
                                roleSysNo = GetStatusProcess(2);
                                sysMessageViewEntity.ToRole = roleSysNo;
                                break;
                            case "D":
                                if (EntityParameter.NeedLegalAdviser == 1)
                                {
                                    roleSysNo = GetStatusProcess(3);
                                    sysMessageViewEntity.ToRole = roleSysNo;
                                }
                                else
                                {
                                    roleSysNo = GetStatusProcess(4);
                                    sysMessageViewEntity.ToRole = roleSysNo;
                                }
                                break;
                            case "F":
                                roleSysNo = GetStatusProcess(4);
                                sysMessageViewEntity.ToRole = roleSysNo;
                                break;
                            case "H":
                                if (EntityParameter.ManageLevel == 0)//院管项目
                                {
                                    roleSysNo = GetStatusProcess(5);
                                    sysMessageViewEntity.ToRole = roleSysNo;
                                    sysMessageViewEntity.IsDone = "B";
                                }
                                else
                                {
                                    needMsg = false;
                                }
                                break;
                        }
                        //发送消息
                        if (needMsg)
                        {
                            if (count > 0)
                            {
                                string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                                if (!string.IsNullOrEmpty(sysMsgString))
                                {
                                    Response.Write(sysMsgString);
                                }
                                else
                                {
                                    Response.Write("0");
                                }
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                    }
                    else
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                            InUser = UserSysNo,
                            MsgType = 1,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审不通过"),
                            QueryCondition = coperation.cpr_Name,
                            ToRole = "0",
                            FromUser = oldEntity.InUser,
                            IsDone = "B",
                            Status = "A"
                        };
                        int resultCount = sysMsgBLL.InsertSysMessage(sysMessageViewEntity);
                        //审批不同过发送消息给流程发起人
                        if (resultCount > 0)
                        {
                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                }
            }

        }

        /// <summary>
        /// 重新审核修改消息状态
        /// </summary>
        private void DoAuditAgain()
        {
            //修改消息状态为已读
            new TG.BLL.cm_SysMsg().UpDateSysMsgStatusByCoperationSysNo(CoperationAuditSysNo);
        }
        //获取审批阶段对应角色ID
        private string GetStatusProcess(int postion)
        {
            string sql = " Select RoleSysNo from cm_CoperationAuditConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return resultObj == null ? "0" : resultObj.ToString();
        }
        //返回下一阶段用户列表
        private void GetNextProcessRoleUser()
        {
            //发起流程
            if (Action == 0)
            {
                //得到审批用户的实体
                string roleSysNoString = GetStatusProcess(1);
                string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                if (!string.IsNullOrEmpty(roleUserString))
                {
                    Response.Write(roleUserString);
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == 1)//审批流程
            {
                string roleSysNoString = "";
                switch (CoperationAuditEntity.Status)
                {
                    case "A"://承接部门
                        roleSysNoString = GetStatusProcess(2);
                        break;
                    case "B"://生产经营
                        if (CoperationAuditEntity.NeedLegalAdviser == 1)
                        {
                            roleSysNoString = GetStatusProcess(3);
                        }
                        else
                        {
                            roleSysNoString = GetStatusProcess(4);
                        }
                        break;
                    case "D"://法律顾问
                        if (CoperationAuditEntity.NeedLegalAdviser == 0)
                        {
                            roleSysNoString = GetStatusProcess(5);
                        }
                        else
                        {
                            roleSysNoString = GetStatusProcess(4);
                        }
                        break;
                    case "F"://技术部门负责人
                        if (CoperationAuditEntity.ManageLevel == 0)
                        {
                            roleSysNoString = GetStatusProcess(5);
                        }
                        else
                        {
                            roleSysNoString = "1";
                        }
                        break;
                    case "H":
                        roleSysNoString = "1";
                        break;
                }
                //获取用户列表
                if (roleSysNoString == "1")
                {
                    Response.Write("1");
                }
                else
                {
                    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        Response.Write(roleUserString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
