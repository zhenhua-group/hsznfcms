﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Newtonsoft.Json;
using TG.BLL;
using TG.Model;
using System.Web.SessionState;
using System.Collections.Generic;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

    public class CoperationAuditEditHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        //public HttpRequest Request { get { return HttpContext.Current.Request; } }
        //public HttpResponse Response { get { return HttpContext.Current.Response; } }
        //public HdttpSessionState Session { get { return HttpContext.Current.Session; } }

        public TG.Model.cm_CoperationAuditEdit CoperationAuditEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_CoperationAuditEdit obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_CoperationAuditEdit>(data);
                }
                return obj;
            }
        }

        /// <summary>
        /// 审核记录SysNo
        /// </summary>
        public int CoperationAuditSysNo
        {
            get
            {
                int coperationAuditSysNo = 0;
                int.TryParse(Request["CoperationAuditSysNo"], out coperationAuditSysNo);
                return coperationAuditSysNo;
            }
        }

        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }
        //查询or更新
        public int Flag
        {
            get
            {
                int flag = 0;
                int.TryParse(Request["flag"], out flag);
                return flag;
            }
        }
        private TG.BLL.cm_AuditRecordEdit coperationBLL = new TG.BLL.cm_AuditRecordEdit();
        TG.BLL.cm_Coperation bllCop = new BLL.cm_Coperation();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                    case 0:
                        InsertCoperationAuditRecord();
                        break;
                    case 1:
                    case 2:
                        UpdateCoperatioAuditRecord();
                        break;
                    case 3:
                        DoAuditAgain();
                        break;
                }
                Response.End();
            }

        }
        /// <summary>
        /// 发起一条新的合同审批信息
        /// </summary>
        private void InsertCoperationAuditRecord()
        {
            if (Flag == 0)
            {
                //返回下阶段用户列表
                GetNextProcessRoleUser();
            }
            else
            {
                //合同审核实体
                TG.BLL.cm_AuditRecordEdit coperationAuditBLL = new TG.BLL.cm_AuditRecordEdit();
                TG.Model.cm_CoperationAuditEdit ModleOne = coperationAuditBLL.GetModelByCoperationSysNo(CoperationAuditEntity.CoperationSysNo);
                if (ModleOne != null)
                {
                    if (ModleOne.Status == "C" || ModleOne.Status == "E" || ModleOne.Status == "G" || ModleOne.Status == "I" || ModleOne.Status == "K" || ModleOne.Status == "J")
                    {
                        //coperationAuditBLL.Delete(ModleOne.SysNo);
                    }
                }
                //合同未审核
                //if (!coperationAuditBLL.Exists(CoperationAuditEntity.CoperationSysNo))
                //{
                //查询合同信息
                TG.Model.cm_Coperation coperation = new TG.BLL.cm_Coperation().GetModel(CoperationAuditEntity.CoperationSysNo);
                //保存审批阶段轨迹
                int sysNo = coperationAuditBLL.InsertCoperatioAuditRecord(CoperationAuditEntity);
                //给对应角色发送审批消息
                if (sysNo > 0)
                {
                    //获取审批实体
                    TG.Model.cm_CoperationAuditEdit tempEntity = new TG.BLL.cm_AuditRecordEdit().GetModel(sysNo);
                    //实例消息实体
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = tempEntity.SysNo.ToString(),
                        ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 12,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审后修改申请"),
                        QueryCondition = coperation.cpr_Name,
                        Status = "A",
                        IsDone = "A",
                        ToRole = "0"
                    };
                    //声明
                    string sysMsgString = "";
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        Response.Write(sysMsgString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
                //}
                //else
                //{
                //    Response.Write("0");
                //}
            }
        }
        /// <summary>
        /// 修改审核记录
        /// </summary>
        private void UpdateCoperatioAuditRecord()
        {
            if (Flag == 0)
            {
                GetNextProcessRoleUser();
            }
            else
            {
                //项目审批实体
                TG.Model.cm_CoperationAuditEdit oldEntity = coperationBLL.GetModel(CoperationAuditEntity.SysNo);
                //审批赋值
                TG.Model.cm_CoperationAuditEdit EntityParameter = new TG.Model.cm_CoperationAuditEdit
                {
                    SysNo = oldEntity.SysNo,
                    UndertakeProposal = oldEntity.UndertakeProposal,
                    OperateDepartmentProposal = oldEntity.OperateDepartmentProposal,
                    TechnologyDepartmentProposal = oldEntity.TechnologyDepartmentProposal,
                    LegalAdviserProposal = oldEntity.LegalAdviserProposal
                };

                switch (CoperationAuditEntity.Status)
                {
                    case "A":
                        EntityParameter.UndertakeProposal = CoperationAuditEntity.UndertakeProposal;
                        EntityParameter.Status = Action == 1 ? "B" : "C";
                        break;
                    case "B":
                        EntityParameter.OperateDepartmentProposal = CoperationAuditEntity.OperateDepartmentProposal;
                        EntityParameter.Status = Action == 1 ? "D" : "E";
                        break;
                    case "D":
                        //需要法律顾问的场合
                        EntityParameter.LegalAdviserProposal = CoperationAuditEntity.LegalAdviserProposal;
                        EntityParameter.Status = Action == 1 ? "F" : "G";
                        break;

                }

                EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) == true ? CoperationAuditEntity.AuditUser : oldEntity.AuditUser + "," + CoperationAuditEntity.AuditUser;
                EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) == true ? DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") : oldEntity.AuditDate + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //更新
                int count = coperationBLL.UpdateCoperatioAuditRecord(EntityParameter);

                //查询合同信息
                TG.Model.cm_Coperation coperation = new TG.BLL.cm_Coperation().GetModel(oldEntity.CoperationSysNo);
                TG.Model.cm_CoperationAuditEdit tempEntity = new TG.BLL.cm_AuditRecordEdit().GetModel(oldEntity.SysNo);

                if (tempEntity.Status == "D")
                {
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                   {
                       ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", EntityParameter.SysNo.ToString(), tempEntity.Status),
                       FromUser = oldEntity.InUser,
                       InUser = UserSysNo,
                       MsgType = 12,
                       ToRole = "0",
                       MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审后修改申请通过"),
                       QueryCondition = coperation.cpr_Name,
                       Status = "A",
                       IsDone = "B"
                   };
                    int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    //修改合同
                    bllCop.UpdateByEdit(tempEntity.Options, int.Parse(tempEntity.CoperationSysNo.ToString()));
                    if (resultcount > 0)
                    {
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }

                else
                {
                    TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    //是审核通过的场合
                    if (Action == 1)
                    {
                        //如果是一级项目发送消息给总经理
                        bool needMsg = true;

                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                            FromUser = UserSysNo,
                            InUser = UserSysNo,
                            MsgType = 12,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审后修改申请"),
                            QueryCondition = coperation.cpr_Name,
                            IsDone = "A",
                            Status = "A"
                        };
                        //审批角色
                        string roleSysNo = string.Empty;

                        //发送消息给相关人员
                        switch (tempEntity.Status)
                        {
                            case "B":
                                roleSysNo = GetStatusProcess(1);
                                sysMessageViewEntity.ToRole = roleSysNo;
                                break;
                            case "D":
                                roleSysNo = GetStatusProcess(2);
                                sysMessageViewEntity.ToRole = roleSysNo;
                                needMsg = false;
                                break;
                        }
                        //发送消息
                        if (needMsg)
                        {
                            if (count > 0)
                            {
                                string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                                if (!string.IsNullOrEmpty(sysMsgString))
                                {
                                    Response.Write(sysMsgString);
                                }
                                else
                                {
                                    Response.Write("0");
                                }
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                    }
                    else
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("CoperationAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                            InUser = UserSysNo,
                            MsgType = 12,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", coperation.cpr_Name, "合同评审后修改申请不通过"),
                            QueryCondition = coperation.cpr_Name,
                            ToRole = "0",
                            FromUser = oldEntity.InUser,
                            IsDone = "B",
                            Status = "A"
                        };
                        int resultCount = sysMsgBLL.InsertSysMessage(sysMessageViewEntity);
                        //审批不同过发送消息给流程发起人
                        if (resultCount > 0)
                        {
                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                }
            }

        }

        /// <summary>
        /// 重新审核修改消息状态
        /// </summary>
        private void DoAuditAgain()
        {
            //修改消息状态为已读
            new TG.BLL.cm_SysMsg().UpDateSysMsgStatusByCoperationSysNo(CoperationAuditSysNo);
        }
        //获取审批阶段对应角色ID
        private string GetStatusProcess(int postion)
        {
            string sql = " Select RoleSysNo from cm_CoperationAuditEditConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return resultObj == null ? "0" : resultObj.ToString();
        }
        //返回下一阶段用户列表
        private void GetNextProcessRoleUser()
        {
            //发起流程
            if (Action == 0)
            {
                //得到审批用户的实体
                string roleSysNoString = GetStatusProcess(1);
                string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                if (!string.IsNullOrEmpty(roleUserString))
                {
                    Response.Write(roleUserString);
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == 1)//审批流程
            {
                string roleSysNoString = "";
                switch (CoperationAuditEntity.Status)
                {
                    case "A"://所长
                        roleSysNoString = GetStatusProcess(2);
                        break;
                    case "B"://生产经营部
                        roleSysNoString = "1";
                        break;
                    case "D":
                        roleSysNoString = "1";
                        break;
                }
                //获取用户列表
                if (roleSysNoString == "1")
                {
                    Response.Write("1");
                }
                else
                {
                    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        Response.Write(roleUserString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}