﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// InterviewManageHandler 的摘要说明
    /// </summary>
    public class InterviewManageHandler : HandlerCommon, IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var userinfo = HttpContext.Current.Request.Cookies["userinfo"];

            context.Response.ContentType = "text/plain";
            //Action 
            string flag = context.Request.Params["action"] ?? "";
            TG.BLL.InterView bll = new TG.BLL.InterView();
            TG.Model.InterView model = new TG.Model.InterView();
            string userSysNo = userinfo.Values["memid"].ToString();
            if (flag == "add")
            {
                //添加收费
                //  string property = context.Request.Params["property"];

                //部门名称
                string Interview_Unit = context.Request.Params["Interview_unit"];
                //面试人员姓名
                string Interview_Name = context.Request.Params["Inter_Name"];
                //面试时间
                string Interview_Date = Convert.ToString(context.Request.Params["beginDate"]);
                ///面试结果
                string Interview_JG = context.Request.Params["Inter_Jieguo"];
                ///面试备注
                string Interview_Remark = context.Request.Params["Interview_Remark"];
                string tempNo = Request.Params["TempNo"];

                model.Interview_Name = Interview_Name;
                model.Interview_Unit = Interview_Unit;
                model.Interview_Date = Convert.ToDateTime(Interview_Date);
                model.Interview_results = Interview_JG;
                model.Interview_Remark = Interview_Remark;
                try
                {
                    int InterViewID = bll.AddInterview(model);
                    if (InterViewID > 0)
                    {

                        string strSql = " UPDATE [cm_InterviewAttachInfo] SET [Cpr_Id] =" + InterViewID + " WHERE [Temp_No] = '" + tempNo + "' AND UploadUser='" + UserSysNo + "' AND OwnType='interview'";
                        TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                        bll_db.ExcuteBySql(strSql);
                    }
                    if (InterViewID > 0)
                    {
                        context.Response.Write("0");
                    }
                    else
                    {
                        context.Response.Write("1");
                    }
                }
                catch (Exception)
                {


                }

            }

            else if (flag == "del")
            {
                int Interview_ID = Convert.ToInt32(context.Request.Params["ID"]);
                int InterViewID = bll.deleteInterview(Interview_ID);
                if (InterViewID > 0)
                {
                    context.Response.Write("0");
                }
                else
                {
                    context.Response.Write("1");
                }

            }

            if (flag == "edit")
            {
                //添加收费
                int id = Convert.ToInt32(context.Request.Params["ids"]);

                //部门名称
                string Interview_Unit = context.Request.Params["Interview_unit"];
                //面试人员姓名
                string Interview_Name = context.Request.Params["Inter_Name"];
                //面试时间
                string Interview_Date = Convert.ToString(context.Request.Params["beginDate"]);
                ///面试结果
                string Interview_JG = context.Request.Params["Inter_Jieguo"];
                ///面试备注
                string Interview_Remark = context.Request.Params["Interview_Remark"];
                string tempNo = Request.Params["TempNo"];
                string filename = Request.Params["filename"];
                model.Id = id;
                model.Interview_Name = Interview_Name;
                model.Interview_Unit = Interview_Unit;
                model.Interview_Date = Convert.ToDateTime(Interview_Date);
                model.Interview_results = Interview_JG;
                model.Interview_Remark = Interview_Remark;
                try
                {
                    //bool isTemno = bll.ExistsTemp_No(tempNo);
                    bool InterViewID = bll.UpdateInterview(model);
                    bool isTrue = bll.Exists(id, filename);
                    if (!isTrue)
                    {
                        bool isDelete = bll.Delete(id);
                        if (isDelete)
                        {
                            string strSql = " UPDATE [cm_InterviewAttachInfo] SET [Cpr_Id] =" + id + " WHERE [Temp_No] = '" + tempNo + "' AND UploadUser='" + UserSysNo + "' AND OwnType='interview'";
                            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                            bll_db.ExcuteBySql(strSql);
                        }
                    }

                    if (InterViewID == true)
                    {
                        context.Response.Write("0");
                    }
                    else
                    {
                        context.Response.Write("1");
                    }

                }
                catch (Exception)
                {


                }

            }
            else if (flag == "getcprfilestypeinterview")
            {
                string str_cprid = context.Request.Params["cprid"] ?? "";
                string str_flag = context.Request.Params["flag"] ?? "";
                string str_type = context.Request.Params["cpr_type"] ?? "";
                TG.BLL.cm_AttachInfo bll_att = new TG.BLL.cm_AttachInfo();
                string str_where = " Cpr_Id=" + str_cprid + " AND UploadUser='" + userSysNo + "' AND OwnType='" + str_type + "'";
                DataTable dt = bll_att.GetListInterViews(str_where).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
            }
        }
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}