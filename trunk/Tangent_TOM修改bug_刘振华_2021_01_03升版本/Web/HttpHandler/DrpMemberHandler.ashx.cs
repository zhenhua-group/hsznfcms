﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// DrpMemberHandler 的摘要说明
    /// </summary>
    public class DrpMemberHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request["action"];
            //所补产值配置
            if (action == "1")
            {
                string unitID = context.Request["unitID"];
                string PreviewPattern = context.Request["PreviewPattern"];
                string UserSysNo = context.Request["UserSysNo"];
                string sqlwhere = " 1=1 ";
                //如果是个人权限
                if (PreviewPattern == "0")
                {
                    sqlwhere = sqlwhere + " and mem_ID=" + UserSysNo + "";
                }

                if (!string.IsNullOrEmpty(unitID))
                {
                    sqlwhere = sqlwhere + " and mem_Unit_ID=" + unitID + "";
                    List<TG.Model.tg_member> dt = new TG.BLL.tg_member().GetModelList(sqlwhere);
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string json = js.Serialize(dt);
                    context.Response.Write(json);
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            //个人产值明细查询(一)点击查看
            else if (action == "2")
            {
                string memid = context.Request["memid"] ?? "0";
                string year = context.Request["year"];
                #region 暂无用

                //                string sqlwhere = "", sqlwhere2 = "";

                //                if (!string.IsNullOrEmpty(memid))
                //                {
                //                    sqlwhere = " and ps.mem_ID=" + memid + "";
                //                    sqlwhere2 = "  and pro_ID in (select pro_ID from cm_Project where PMUserID="+memid+")";
                //                }
                //                if (!string.IsNullOrEmpty(year) && year != "-1")
                //                {
                //                    sqlwhere =sqlwhere+ " and year(pva.AllotDate)=" + year + "";
                //                    sqlwhere2 = sqlwhere2 + " and year(pva.AllotDate)=" + year + "";
                //                }
                //                string sql = @"select isnull(SUM(totalCount),0) as totalCount,(select pro_name from cm_project where pro_ID=pp.pro_ID) as pro_name,pro_ID from 
                //        (
                //            select isnull(sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as decimal(18,2))),0) as totalCount,(select pro_name from cm_project where pro_ID=pva.pro_ID) as pro_name,pva.pro_ID from cm_ProjectValueByMemberDetails ps 
                //		    left join cm_ProjectValueAllot pva on pva.ID=ps.AllotID 
                //		    where ps.[Status]='S' and ps.IsExternal='0' {0} group by pva.pro_ID,ps.mem_ID
                //	
                //		    union all
                //	
                //		    select isnull(szcount,0),(select pro_name from cm_Project where pro_ID=pj.pro_ID),pj.pro_ID from cm_project pj right join
                //			        (select pro_ID,SUM(DesignManagerCount) as szcount from cm_ProjectValueAllot pva where [Status]='S' {1} group by pro_ID) pp  on pp.pro_ID=pj.pro_ID
                //
                //            union all
                //
                //            select SUM(pva.DesignManagerCount),(select pro_name from cm_Project where pro_ID=pva.Pro_ID),pva.Pro_ID from tg_relation a left join tg_member ps on a.mem_ID=ps.mem_ID left join tg_project c on a.pro_ID=c.pro_ID left join
                //					cm_Project d on c.pro_ID=d.ReferenceSysNo left join cm_TranjjsProjectValueAllot pva on d.pro_ID=pva.Pro_ID  where pva.[Status]='S' and 
                //					ps.mem_Speciality_ID=(select spe_ID from tg_speciality where spe_Name='预算') and
                //					charindex('2',a.mem_RoleIDs)>0  {0} 
                //					group by pva.Pro_ID,a.mem_ID
                //       ) pp group by pro_ID having isnull(SUM(totalCount),0)<>0 order by pro_ID desc";

                //                sql = string.Format(sql, sqlwhere, sqlwhere2);

                //                DataSet set = TG.DBUtility.DbHelperSQL.Query(sql);
                #endregion
                DataSet set = new TG.BLL.StandBookBp().GetMemberProjectDetail(Convert.ToInt32(memid), year);
                string trString = "";
                decimal total = 0;
                if (set.Tables.Count > 0)
                {

                    for (int i = 0; i < set.Tables[0].Rows.Count; i++)
                    {
                        string proname = set.Tables[0].Rows[i]["pro_name"].ToString();
                        if (proname.Length > 75)
                        {
                            proname = proname.Substring(0, 75) + "......";
                        }
                        trString += "<tr>";
                        trString += "<td align=\"center\" style=\"width:10%;\">" + (i + 1) + "</td><td align=\"left\" style=\"width:60%;\" title='" + set.Tables[0].Rows[i]["pro_name"] + "'>" + proname + "</td><td align=\"left\" style=\"width:15%;\">" + set.Tables[0].Rows[i]["Unit"].ToString() + "</td><Td align=\"center\" style=\"width:15%;\">" + Convert.ToDecimal(set.Tables[0].Rows[i]["totalCount"]).ToString("f2") + "</td>";

                        trString += "</tr>";
                        total += decimal.Parse(set.Tables[0].Rows[i]["totalCount"].ToString());
                    }

                }
                trString += "<tr><td align=\"center\">总计：</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"center\">" + total.ToString("f2") + "</td></tr>";
                context.Response.Write(trString);
            }
            //个人产值明细查询(二)点击查看
            else if (action == "3")
            {
                string memid = context.Request["memid"] ?? "0";
                string year = context.Request["year"];
                #region 暂无用
                //string sqlwhere = "", sqlwhere2 = "";

                //if (!string.IsNullOrEmpty(memid))
                //{

                //    sqlwhere = " and ps.mem_ID=" + memid + "";
                //    sqlwhere2 = "  and pro_ID in (select pro_ID from cm_Project where PMUserID=" + memid + ")";
                //}

                //if (!string.IsNullOrEmpty(year)&&year!="-1")
                //{
                //    sqlwhere = sqlwhere + " and year(pva.AllotDate)=" + year + "";
                //    sqlwhere2 = sqlwhere2 + " and year(pva.AllotDate)=" + year + "";
                //}


                //                string sql = @"select isnull(SUM(totalCount),0) as totalCount,(select pro_name from cm_project where pro_ID=pp.pro_ID) as pro_name,pro_ID from 
                //        (
                //            select isnull(sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as decimal(18,2))),0) as totalCount,(select pro_name from cm_project where pro_ID=pva.pro_ID) as pro_name,pva.pro_ID from cm_ProjectValueByMemberDetails ps 
                //		    left join cm_ProjectValueAllot pva on pva.ID=ps.AllotID 
                //		    where ps.[Status]='S' and ps.IsExternal='0' {0} group by pva.pro_ID,ps.mem_ID
                //	
                //		    union all
                //	
                //		    select isnull(szcount,0),(select pro_name from cm_Project where pro_ID=pj.pro_ID),pj.pro_ID from cm_project pj right join
                //			        (select pro_ID,SUM(DesignManagerCount) as szcount from cm_ProjectValueAllot pva where [Status]='S' {1} group by pro_ID) pp  on pp.pro_ID=pj.pro_ID
                //
                //            union all
                //            
                //            select SUM(pva.DesignManagerCount),(select pro_name from cm_Project where pro_ID=pva.Pro_ID),pva.Pro_ID from tg_relation a left join tg_member ps on a.mem_ID=ps.mem_ID left join tg_project c on a.pro_ID=c.pro_ID left join
                //					cm_Project d on c.pro_ID=d.ReferenceSysNo left join cm_TranjjsProjectValueAllot pva on d.pro_ID=pva.Pro_ID  where pva.[Status]='S' and 
                //					ps.mem_Speciality_ID=(select spe_ID from tg_speciality where spe_Name='预算') and
                //					charindex('2',a.mem_RoleIDs)>0  {0} 
                //					group by pva.Pro_ID,a.mem_ID
                //    
                //       ) pp group by pro_ID having isnull(SUM(totalCount),0)<>0 order by pro_ID desc";

                // sql = string.Format(sql, sqlwhere, sqlwhere2);
                //DataSet set = TG.DBUtility.DbHelperSQL.Query(sql);
                #endregion
                DataSet set = new TG.BLL.StandBookBp().GetMemberProjectDetail(Convert.ToInt32(memid), year);
                string trString = "";
                decimal total = 0;
                if (set.Tables.Count > 0)
                {

                    for (int i = 0; i < set.Tables[0].Rows.Count; i++)
                    {
                        string proname = set.Tables[0].Rows[i]["pro_name"].ToString();
                        if (proname.Length > 75)
                        {
                            proname = proname.Substring(0, 75) + "......";
                        }
                        trString += "<tr>";
                        trString += "<td align=\"center\" style=\"width:10%;\">" + (i + 1) + "</td><td align=\"left\" style=\"width:60%;\" title='" + set.Tables[0].Rows[i]["pro_name"] + "'>" + proname + "</td><td align=\"left\" style=\"width:15%;\">" + set.Tables[0].Rows[i]["Unit"].ToString() + "</td><Td align=\"center\" style=\"width:15%;\">" + Convert.ToDecimal(set.Tables[0].Rows[i]["totalCount"]).ToString("f2") + "</td>";
                        trString += "</tr>";
                        total += decimal.Parse(set.Tables[0].Rows[i]["totalCount"].ToString());
                    }

                }
                trString += "<tr><td align=\"center\">总计：</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"center\">" + total.ToString("f2") + "</td></tr>";
                context.Response.Write(trString);
            }

             //个人收入明细表查看
            #region
            else if (action == "4")
            {
                string memid = context.Request["memid"];
                string year = context.Request["year"];
                string sqlwhere = "";
                if (!string.IsNullOrEmpty(memid))
                {
                    sqlwhere = " and ps.mem_ID=" + memid + "";
                }
                if (!string.IsNullOrEmpty(year))
                {
                    sqlwhere = sqlwhere + " and year(pva.AllotDate)=" + year + "";
                }
                string sql = @"select a.* from (select isnull(sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as decimal(18,2))),0) as totalCount,(select pro_name from cm_project where pro_ID=pva.pro_ID) as pro_name,ps.mem_ID,pva.pro_ID from cm_ProjectValueByMemberDetails ps left join cm_ProjectValueAllot pva on pva.ID=ps.AllotID 
	where 1=1 " + sqlwhere + " group by pva.pro_ID,ps.mem_ID) a";
                DataSet set = TG.DBUtility.DbHelperSQL.Query(sql);
                string trString = "";
                decimal total = 0;
                if (set.Tables.Count > 0)
                {

                    for (int i = 0; i < set.Tables[0].Rows.Count; i++)
                    {
                        string proname = set.Tables[0].Rows[i]["pro_name"].ToString();
                        if (proname.Length > 75)
                        {
                            proname = proname.Substring(0, 75) + "......";
                        }
                        trString += "<tr>";
                        trString += "<td align=\"center\" style=\"width:10%;\">" + (i + 1) + "</td><td align=\"left\" style=\"width:70%;\" title='" + set.Tables[0].Rows[i]["pro_name"] + "'>" + proname + "</td><Td align=\"center\" style=\"width:20%;\">" + Convert.ToDecimal(set.Tables[0].Rows[i]["totalCount"]).ToString("f2") + "</td>";
                        trString += "</tr>";
                        total += decimal.Parse(set.Tables[0].Rows[i]["totalCount"].ToString());
                    }

                }
                trString += "<tr><td align=\"center\">总计：</td><td>&nbsp;</td><td align=\"center\">" + total.ToString("f2") + "</td></tr>";
                context.Response.Write(trString);
            }
            #endregion
            //获取部门分配产值
            if (action == "5")
            {
                string unitid = context.Request["unitID"];
                string unitName = context.Request["unitName"];
                string allYear = context.Request["allYear"];
                string wbmcz = "0";
                #region 描述:没有包含转土建或转经济所或转暖通的自留的那部分钱. zxq 20131226
                // string sqlwhere = "";
                //if (unitid != "-1")
                //{
                //    sqlwhere = " and Unit='" + unitName + "'";
                //}
                //if (allYear != "-1")
                //{
                //    sqlwhere += " and year(pro_startTime)=" + allYear + "";
                //}

                // if (sqlwhere!="")
                // {


                //                    object o = TG.DBUtility.DbHelperSQL.GetSingle(@"select isnull(SUM(bmcz)-SUM(yfcz),0)  from(
                //                    select isnull((SUM(sscz)-sum(case when eccz IS NULL then 0 else eccz end)),0) as bmcz,0 as yfcz from                        cm_project p right join                         
                //                         (select pro_ID,isnull(SUM(cast(round(allotcount,0) as decimal(18,2))),0) as eccz from                                          cm_ProjectValueAllot where SecondValue='2' and [Status]<>'D' and ActualAllountTime='" + allYear + "'" + @"  group by pro_ID) c
                //                    on p.pro_ID=c.pro_ID right join 
                //                          (select ProID,
                //                 isnull(sum(cast(round((TheDeptValueCount+AuditCount+DesignCount+HavcCount+isnull(LoanValueCount,0)),0) as decimal (18,2))),0) as sscz  from cm_ProjectSecondValueAllot where [Status]='S' and ActualAllountTime='" + allYear + "'" + @" group by ProID) a
                //                    on a.ProID=p.pro_ID where a.ProID in (select pro_id from cm_project where 1=1 " + sqlwhere + ")" + " union all  select 0 as bmcz,isnull(SUM(ComValue),0) as yfcz  from cm_CompensationSet  where ValueYear='" + allYear + "' and MemberId in (select mem_id from tg_member where mem_Unit_ID=" + unitid + ") ) aa");
                //                    wbmcz = o == null ? "0" : o.ToString();

                #endregion

                if (unitid != "-1" && allYear != "-1" && !string.IsNullOrEmpty(unitName))
                {
                    object o = new TG.BLL.StandBookBp().GetRemainAllotValue(allYear, unitName, Convert.ToInt32(unitid));
                    wbmcz = o == null ? "0" : o.ToString();
                }

                //  }
                // context.Response.Write(wbmcz < 0 ? "0" : wbmcz.ToString()); 
                context.Response.Write(wbmcz);
            }
            else if (action == "wang")
            {
                //效益工资
                string memid = context.Request["memid"] ?? "0";
                string year = context.Request["year"];
                TG.Model.cm_ProjectWageBenefits model = new TG.Model.cm_ProjectWageBenefits();
                TG.BLL.cm_ProjectWageBenefits bll = new TG.BLL.cm_ProjectWageBenefits();
                model = bll.GetModelByYear(int.Parse(memid), year);
                string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                context.Response.Write(jsonResult);
            }
            else if (action == "wangAdd")
            {

                string data = context.Request["data"]; ;
                TG.Model.cm_ProjectWageBenefits obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectWageBenefits>(data);
                }

                int count = new TG.BLL.cm_ProjectWageBenefits().Add(obj);

                context.Response.Write(count);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}