﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TG.Model;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// ProjectChargeHandler 的摘要说明
    /// </summary>
    public class ProjectChargeHandler : HandlerCommon, IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //Action 
            string flag = context.Request.Params["action"] ?? "";
            if (flag == "add")
            {
                //添加收费
                string projid = context.Request.Params["projid"] ?? "0";
                string paycount = context.Request.Params["paycount"] ?? "0";
                string remitter = context.Request.Params["remitter"] ?? "";
                string time = context.Request.Params["time"] ?? "";
                string mark = context.Request.Params["mark"] ?? "";
                string curuser = context.Request.Params["curuser"] ?? "";
                string caiwumark = "";
                string suozhangmark = "";

                TG.Model.cm_ProjectCharge model = new TG.Model.cm_ProjectCharge();
                TG.BLL.cm_ProjectCharge bll = new TG.BLL.cm_ProjectCharge();
                model.projID = int.Parse(projid);
                model.cprID = model.projID;
                model.Acount = Convert.ToDecimal(paycount);
                model.FromUser = remitter;
                model.InAcountUser = curuser;
                model.InAcountTime = Convert.ToDateTime(time);
                model.ProcessMark = mark;
                model.CaiwuMark = caiwumark;
                model.SuoZhangMark = suozhangmark;
                //消息的结果
                int identity = bll.Add(model);
                if (identity > 0)
                {
                    //发送消息到财务
                  //  SendMessageToCW(projid, identity.ToString());
                    context.Response.Write("0");
                }
                else
                {
                    context.Response.Write("1");
                }
            }
            else if (flag == "edit_show")
            {
                string chg_id = context.Request.Params["id"] ?? "0";
                TG.BLL.cm_ProjectCharge bll = new TG.BLL.cm_ProjectCharge();
                TG.Model.cm_ProjectCharge model = bll.GetModel(int.Parse(chg_id));
                if (model != null)
                {
                    context.Response.Write(model.ProcessMark.ToString());
                }
                else
                {
                    context.Response.Write("");
                }
            }
            else if (flag == "edit")
            {
                string projid = context.Request.Params["projid"] ?? "0";
                string chgid = context.Request.Params["chgid"] ?? "0";
                string paycount = context.Request.Params["paycount"] ?? "0";
                string remitter = context.Request.Params["remitter"] ?? "";
                string time = context.Request.Params["time"] ?? "";
                string mark = context.Request.Params["mark"] ?? "";
                string curuser = context.Request.Params["curuser"] ?? "";

                TG.Model.cm_ProjectCharge model = new TG.BLL.cm_ProjectCharge().GetModel(int.Parse(chgid));
                TG.BLL.cm_ProjectCharge bll = new TG.BLL.cm_ProjectCharge();
                //赋值
                model.ID = Convert.ToInt32(chgid);
                model.InAcountCode = model.InAcountCode;
                model.InAcountTime = Convert.ToDateTime(time);
                model.InAcountUser = model.InAcountUser;
                model.ProcessMark = mark;
                model.projID = Convert.ToInt32(projid);
                model.cprID = model.projID;
                model.Status = model.Status;
                model.FromUser = remitter;
                model.Acount = Convert.ToDecimal(paycount);
                if (bll.Update(model))
                {
                    context.Response.Write("0");
                }
                else
                {
                    context.Response.Write("1");
                }
            }
            else if (flag == "del")
            {
                string chg_id = context.Request.Params["chgid"] ?? "0";
                if (new TG.BLL.cm_ProjectCharge().Delete(int.Parse(chg_id)))
                {
                    context.Response.Write("0");
                }
                else
                {
                    context.Response.Write("1");
                }
            }
        }
        //发送消息到财务
        protected void SendMessageToCW(string cprid, string identity)
        {
            //查询财务RoleSysNo
            string sql1 = string.Format("SELECT TOP 1 [SysNo] FROM [dbo].[cm_Role] where RoleName like N'%{0}%' order by SysNo DESC", "财务");
            object objRoleSysNo = TG.DBUtility.DbHelperSQL.GetSingle(sql1);
            int roleSysNo = objRoleSysNo == null ? 0 : Convert.ToInt32(objRoleSysNo);

            //查询项目信息
            TG.Model.cm_Coperation model = new TG.BLL.cm_Coperation().GetModel(int.Parse(cprid));
            if (model != null)
            {
                string cprname = model.cpr_Name;
                string cprunit = model.cpr_Unit;

                //发送消息给财务
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("fromUser={0}&sysNo={1}&projID={2}&auditstatus=A", UserSysNo, identity, cprid),
                    FromUser = UserSysNo,
                    InUser = UserSysNo,
                    MsgType = 7,//项目收款确认
                    ToRole = roleSysNo.ToString(),
                    MessageContent = string.Format("关于{0}的{1}合同收款入账财务部确认！", cprunit, cprname),
                    QueryCondition = cprname,
                    IsDone = "A"
                };
                int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}