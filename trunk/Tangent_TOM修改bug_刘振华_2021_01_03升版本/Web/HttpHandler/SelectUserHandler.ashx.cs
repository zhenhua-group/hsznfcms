﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TG.Web.HttpHandler
{
	/// <summary>
	/// $codebehindclassname$ 的摘要说明
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	public class SelectUserHandler : IHttpHandler
	{

		public HttpRequest Request { get { return HttpContext.Current.Request; } }

		public HttpResponse Response { get { return HttpContext.Current.Response; } }

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			GetUserList();
			Response.End();
		}

		private void GetUserList()
		{
			string userShortName = Request["userShortName"];
			string departmentSysNo = Request["departmentSysNo"];
			string whereSql = "";
			if (!string.IsNullOrEmpty(userShortName))
			{
				whereSql += " AND (mem_Login like N'%" + userShortName + "%' OR mem_Name like N'%" + userShortName + "%')";
			}
			if (!string.IsNullOrEmpty(departmentSysNo) && departmentSysNo != "0")
			{
				whereSql += " AND mem_Unit_ID=" + departmentSysNo;
			}
			List<TG.Model.tg_member> UserList = new TG.BLL.tg_member().GetUsers(whereSql);

			string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(UserList);

			Response.Write(jsonResult);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}
