﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TG.Model;
using Newtonsoft.Json;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// HandlerMainPage 的摘要说明
    /// </summary>
    public class HandlerMainPage : IHttpHandler
    {
        /// <summary>
        /// 请求
        /// </summary>
        public HttpRequest Request
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }
        /// <summary>
        /// 返回
        /// </summary>
        public HttpResponse Response
        {
            get
            {
                return HttpContext.Current.Response;
            }
        }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string UserSysNo
        {
            get
            {
                return Request["userno"] ?? "0";
            }
        }
        /// <summary>
        /// 操作动作
        /// </summary>
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取用户消息与待办
            if (Action == "getmsg")
            {
                string result = "";
                //代办事项
                string UserDoneMsgCount = new TG.BLL.cm_SysMsg().GetSysMsgDoneCount(int.Parse(UserSysNo)).ToString();
                //消息
                string UserSysMsg = new TG.BLL.cm_SysMsg().GetSysMsgCount(int.Parse(UserSysNo)).ToString();

                result = "{\"DoneCount\":\"" + UserDoneMsgCount + "\",\"MsgCount\":\"" + UserSysMsg + "\",\"donemsglist\":";

                //未办
                string strSqlWhere = " IsDone='A' ";
                List<SYSMsgQueryEntityByMain> DoneList = new BLL.cm_SysMsg().GetListForMain(int.Parse(UserSysNo), strSqlWhere);
                TypeAcountClass typeClass = new TypeAcountClass();
                List<SYSMsgQueryEntityByMain> DoneListNew = new List<SYSMsgQueryEntityByMain>();
                //Array[] listArray=new Array[]();

                //foreach (SYSMsgQueryEntityByMain item in DoneList)
                //{
                //  if (item.MsgType==1)
                //  {

                //  }
                    
                        
                    
                //}
                //DoneList.RemoveAt(0);
                result += JsonConvert.SerializeObject(DoneList);

                result += ",\"newmsglist\":";

                //消息
                //条件是未读新消息
                strSqlWhere = " Status='A' ";
                List<SYSMsgQueryEntityByMain> NewList = new BLL.cm_SysMsg().GetListForMainAboutMsg(int.Parse(UserSysNo), strSqlWhere);
                result += JsonConvert.SerializeObject(NewList);

                //结尾
                result += "}";

                context.Response.Write(result);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}