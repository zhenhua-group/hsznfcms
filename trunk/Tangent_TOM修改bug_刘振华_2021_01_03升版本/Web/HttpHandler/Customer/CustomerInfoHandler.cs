﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;

namespace TG.Web.HttpHandler.Customer
{
    public abstract class CustomerInfoHandler : IHttpHandler
    {

        //存储过程名
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                string year = DateTime.Now.Year.ToString();
                string sqlwhere = "";
                //年份
                if (year != "-1")
                {
                    sqlwhere = " AND year(InsertDate)=" + year;
                }
                sqlwhere = sqlwhere + strWhere;
                parameters.Where = sqlwhere;
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                TG.BLL.cm_CustomerInfo bll_unit = new TG.BLL.cm_CustomerInfo();
                string cstid = forms.Get("EmpId");
                string strResponse = "";
                bool flag = false;
                if (cstid.Trim().Length > 0)
                {
                    cstid = cstid.IndexOf(',') > -1 ? cstid : cstid + ",";
                    flag = bll_unit.DeleteList(cstid);
                    if (flag)
                    {
                        strResponse = "客户删除成功！";
                    }
                    else
                    {
                        strResponse = "不能删除的客户，请查看客户是否有关联合同信息！";
                    }
                }
                else
                {
                    strResponse = "客户删除失败！";
                }

                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {
                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间
                string strwhere = context.Request.QueryString["strwhere"];//开始
                //查询方式
                string andor = context.Request.QueryString["andor"] ?? "and";
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" " + andor + " Cst_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (unit != "-1")
                {
                    strsql.AppendFormat(" " + andor + " InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID={0})", unit);
                }
                //年份
                if (year != "-1")
                {
                    strsql.AppendFormat(" " + andor + " year(InsertDate)={0} ", year);
                }

                //录入时间
                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" " + andor + " InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" " + andor + " InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" " + andor + " InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }

                //客户编码
                string custNo = context.Request.QueryString["custNo"];
                if (!string.IsNullOrEmpty(custNo))
                {
                    strsql.Append(" " + andor + " Cst_No like '%" + custNo.Trim() + "%'  ");
                }

                //联系人
                var linkMan = context.Request.QueryString["linkMan"];
                if (!string.IsNullOrEmpty(linkMan))
                {
                    strsql.Append(" " + andor + " Linkman like '%" + linkMan.Trim() + "%'  ");
                }
                //联系电话
                var linkPhone = context.Request.QueryString["linkPhone"];
                if (!string.IsNullOrEmpty(linkPhone))
                {
                    strsql.Append(" " + andor + " Cpy_Phone like '%" + linkPhone.Trim() + "%'  ");
                }

                //公司地址
                var address = context.Request.QueryString["address"];
                if (!string.IsNullOrEmpty(address))
                {
                    strsql.Append(" " + andor + " Cpy_Address like '%" + address.Trim() + "%'  ");
                }

                //邮编
                var code = context.Request.QueryString["code"];
                if (!string.IsNullOrEmpty(code))
                {
                    strsql.Append(" " + andor + " Code like '%" + code.Trim() + "%'  ");
                }
                //传真号
                var cpyFax = context.Request.QueryString["cpyFax"];
                if (!string.IsNullOrEmpty(cpyFax))
                {
                    strsql.Append(" " + andor + " Cpy_Fax like '%" + cpyFax.Trim() + "%'  ");
                }

                //录入人
                var insertName = context.Request.QueryString["insertName"];
                if (!string.IsNullOrEmpty(insertName))
                {
                    strsql.Append(" " + andor + "  InsertUserID in (select mem_id from tg_member where mem_Name like '%" + insertName + "%') ");
                }

                //客户简称
                var cst_Brief = context.Request.QueryString["cst_Brief"];
                if (!string.IsNullOrEmpty(cst_Brief))
                {
                    strsql.Append(" " + andor + " Cst_Brief like '%" + cst_Brief.Trim() + "%'  ");
                }

                //客户英文名称
                var cstEnglishName = context.Request.QueryString["cstEnglishName"];
                if (!string.IsNullOrEmpty(cstEnglishName))
                {
                    strsql.Append(" " + andor + " Cst_EnglishName like '%" + cstEnglishName.Trim() + "%'  ");
                }
                //是否合作
                var isPartner = context.Request.QueryString["isPartner"];
                if (!string.IsNullOrEmpty(isPartner) && isPartner != "-1")
                {
                    strsql.Append(" " + andor + " IsPartner=" + isPartner + "  ");
                }

                //所在国家
                var country = context.Request.QueryString["country"];
                if (!string.IsNullOrEmpty(country))
                {
                    strsql.Append(" " + andor + " Country like '%" + country.Trim() + "%'  ");
                }

                //所在省份
                var province = context.Request.QueryString["province"];
                if (!string.IsNullOrEmpty(province))
                {
                    strsql.Append(" " + andor + " Province like '%" + province.Trim() + "%'  ");
                }
                //所在城市
                var city = context.Request.QueryString["city"];
                if (!string.IsNullOrEmpty(city))
                {
                    strsql.Append(" " + andor + " City like '%" + city.Trim() + "%'  ");
                }

                //客户类型
                var cstType = context.Request.QueryString["cstType"];
                if (!string.IsNullOrEmpty(cstType) && cstType != "-1")
                {
                    strsql.Append(" " + andor + " Type =" + cstType + " ");
                }

                //所属行业
                var profession = context.Request.QueryString["profession"];
                if (!string.IsNullOrEmpty(profession) && profession != "-1")
                {
                    strsql.Append(" " + andor + " Profession =" + profession + " ");
                }

                //分支机构
                var branchPart = context.Request.QueryString["branchPart"];
                if (!string.IsNullOrEmpty(branchPart))
                {
                    strsql.Append(" " + andor + " BranchPart like '%" + branchPart.Trim() + "%'  ");
                }

                //邮箱
                var email = context.Request.QueryString["email"];
                if (!string.IsNullOrEmpty(email))
                {
                    strsql.Append(" " + andor + " Email like '%" + email.Trim() + "%'  ");
                }
                //公司主页
                var principalSheet = context.Request.QueryString["principalSheet"];
                if (!string.IsNullOrEmpty(principalSheet))
                {
                    strsql.Append(" " + andor + " Cpy_PrincipalSheet like '%" + principalSheet.Trim() + "%'  ");
                }

                //关系建立时间
                var date_Start = context.Request.QueryString["date_Start"];
                var date_End = context.Request.QueryString["date_End"];

                if (!string.IsNullOrEmpty(date_Start) && string.IsNullOrEmpty(date_End))
                {
                    strsql.AppendFormat(" " + andor + " CreateRelationTime>='{0}' ", date_Start + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(date_Start) && !string.IsNullOrEmpty(date_End))
                {
                    strsql.AppendFormat(" " + andor + " CreateRelationTime<='{0}' ", date_End + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(date_Start) && !string.IsNullOrEmpty(date_End))
                {
                    strsql.AppendFormat(" " + andor + " CreateRelationTime between '{0}' and '{1}' ", date_Start + "  00:00:00", date_End + " 23:59:59");
                }

                //关系部门
                var relationDepartment = context.Request.QueryString["relationDepartment"];
                if (!string.IsNullOrEmpty(relationDepartment))
                {
                    strsql.Append(" " + andor + " RelationDepartment like '%" + relationDepartment.Trim() + "%'  ");
                }

                //信用级别
                var creditLeve = context.Request.QueryString["creditLeve"];
                if (!string.IsNullOrEmpty(creditLeve) && creditLeve != "-1")
                {
                    strsql.Append(" " + andor + " CreditLeve =" + creditLeve + " ");
                }
                //亲密度
                var closeLeve = context.Request.QueryString["closeLeve"];
                if (!string.IsNullOrEmpty(closeLeve) && closeLeve != "-1")
                {
                    strsql.Append(" " + andor + " CloseLeve =" + closeLeve + " ");
                }

                //开户银行
                var bankName = context.Request.QueryString["bankName"];
                if (!string.IsNullOrEmpty(bankName))
                {
                    strsql.Append(" " + andor + " BankName like '%" + bankName.Trim() + "%'  ");
                }

                //企业代码
                var cpy_Code = context.Request.QueryString["cpy_Code"];
                if (!string.IsNullOrEmpty(cpy_Code))
                {
                    strsql.Append(" " + andor + " Cpy_Code like '%" + cpy_Code.Trim() + "%'  ");
                }
                //开户银行账号
                var bankAccountNo = context.Request.QueryString["bankAccountNo"];
                if (!string.IsNullOrEmpty(bankAccountNo))
                {
                    strsql.Append(" " + andor + " BankAccountNo like '%" + bankAccountNo.Trim() + "%'  ");
                }

                //法定代表
                var lawPerson = context.Request.QueryString["lawPerson"];
                if (!string.IsNullOrEmpty(lawPerson))
                {
                    strsql.Append(" " + andor + " LawPerson like '%" + lawPerson.Trim() + "%'  ");
                }

                //纳税人识别号
                var taxAccountNo = context.Request.QueryString["taxAccountNo"];
                if (!string.IsNullOrEmpty(taxAccountNo))
                {
                    strsql.Append(" " + andor + " TaxAccountNo like '%" + taxAccountNo.Trim() + "%'  ");
                }

                //备注
                var remark = context.Request.QueryString["remark"];
                if (!string.IsNullOrEmpty(remark))
                {
                    strsql.Append(" " + andor + " Remark like '%" + remark.Trim() + "%'  ");
                }

                string whereTemp = strsql.ToString();

                if (andor.Contains("or"))
                {
                    if (!string.IsNullOrEmpty(whereTemp))
                    {
                        whereTemp = " and ( " + whereTemp.Trim().Substring(2) + ")";
                    }
                }

                whereTemp = whereTemp + strwhere;

                parameters.Where = whereTemp;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "Search")
            {
                string sqlwhere = "";
                string andor = context.Request["andor"] ?? "and";//查询方式
                string txtCst_No = context.Request["txtCst_No"];
                string txtCst_Brief = context.Request["txtCst_Brief"];
                string txtCst_Name = context.Request["txtCst_Name"];
                string ddType = context.Request["ddType"];
                string ddProfession = context.Request["ddProfession"];
                string txtCity = context.Request["txtCity"];
                string txtLinkman = context.Request["txtLinkman"];
                string txtProvince = context.Request["txtProvince"];
                string txtCpy_Address = context.Request["txtCpy_Address"];
                string cpyphone = context.Request["Cpy_Phone"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间

                //没有值就默认and
                if (string.IsNullOrEmpty(andor) || andor.Trim() == "undefined")
                {
                    andor = "and";
                }

                if (!string.IsNullOrEmpty(txtCst_No))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txtCst_No.Trim());
                    sqlwhere += " and ( C.Cst_No LIKE '%" + keyname + "%' ";
                }
                if (!string.IsNullOrEmpty(txtCst_Brief))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txtCst_Brief.Trim());
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    sqlwhere += " C.Cst_Brief LIKE '%" + keyname + "%' ";
                }
                if (!string.IsNullOrEmpty(txtCst_Name))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txtCst_Name.Trim());
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    sqlwhere += " C.Cst_Name LIKE '%" + keyname + "%' ";
                }
                if (!string.IsNullOrEmpty(ddType) && ddType.Trim() != "-1")
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    sqlwhere += " CX.Type =" + ddType.Trim() + " ";
                }
                if (!string.IsNullOrEmpty(ddProfession) && ddProfession.Trim() != "-1")
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    sqlwhere += " CX.Profession=" + ddProfession.Trim() + " ";
                }
                if (!string.IsNullOrEmpty(txtCity))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    string keyname = TG.Common.StringPlus.SqlSplit(txtCity.Trim());
                    sqlwhere += " CX.City LIKE '%" + keyname + "%' ";
                }
                if (!string.IsNullOrEmpty(txtLinkman))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    string keyname = TG.Common.StringPlus.SqlSplit(txtLinkman.Trim());
                    sqlwhere += " C.LinkMan LIKE '%" + keyname + "%' ";
                }
                if (!string.IsNullOrEmpty(txtProvince))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    string keyname = TG.Common.StringPlus.SqlSplit(txtProvince.Trim());
                    sqlwhere += " CX.Province LIKE '%" + keyname + "%' ";
                }
                if (!string.IsNullOrEmpty(txtCpy_Address))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    string keyname = TG.Common.StringPlus.SqlSplit(txtCpy_Address.Trim());
                    sqlwhere += " C.Cpy_Address LIKE '%" + keyname + "%' ";
                }
                if (!string.IsNullOrEmpty(cpyphone))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    string keyname = TG.Common.StringPlus.SqlSplit(cpyphone.Trim());
                    sqlwhere += " C.Cpy_Phone LIKE '%" + keyname + "%' ";
                }

                //录入时间
                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    sqlwhere += " InsertDate>='" + startTime + "  00:00:00 '";
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    sqlwhere += " InsertDate<='" + endTime + "  23:59:59 '";
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sqlwhere != "")
                    {
                        sqlwhere += " " + andor + " ";
                    }
                    else
                    {
                        sqlwhere += " and ( ";
                    }
                    sqlwhere += " (InsertDate between '" + startTime + "  00:00:00 ' and '" + endTime + "  23:59:59 ')";
                }

                if (sqlwhere != "")
                {
                    sqlwhere += ")";
                }

                sqlwhere = sqlwhere + strWhere;

                parameters.Where = sqlwhere;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());

            }
            //暂时无用
            #region

            else if (strAction == "del")
            {

                //TG.BLL.cm_CustomerInfo bll_unit = new TG.BLL.cm_CustomerInfo();
                //string unit_id = context.Request.QueryString["unitid"];
                //string strResponse = "";
                //if (bll_unit.DeleteList(unit_id))
                //{
                //    strResponse = "删除成功！";
                //}
                //context.Response.Write(strResponse);
            }
            #endregion
        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}