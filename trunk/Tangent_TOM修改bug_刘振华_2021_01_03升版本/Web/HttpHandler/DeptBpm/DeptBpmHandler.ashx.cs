﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Data;
using TG.Model;


namespace TG.Web.HttpHandler.DeptBpm
{
    /// <summary>
    /// DeptBpmHandler 的摘要说明
    /// </summary>
    public class DeptBpmHandler : IHttpHandler
    {
        public HttpRequest Request
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }
        public HttpResponse Response
        {
            get
            {
                return HttpContext.Current.Response;
            }
        }
        /// <summary>
        /// 动作
        /// </summary>
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        /// <summary>
        /// 是否为新建
        /// </summary>
        public bool IsNewItem
        {
            get
            {
                if (Request["isNew"].ToString() == "0")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 数据实体
        /// </summary>
        public string entityData
        {
            get
            {
                return Request["data"] ?? "";
            }
        }

        public string entityData2
        {
            get
            {
                return Request["data2"] ?? "";
            }
        }
        /// <summary>
        /// 删除ID
        /// </summary>
        public string ID
        {
            get
            {
                return Request["id"] ?? "";
            }
        }
        /// <summary>
        /// 总奖金数
        /// </summary>
        public decimal? JiangCount
        {
            get
            {
                decimal allcount = 0;
                decimal.TryParse(Request["jjcount"] ?? "0", out allcount);
                return allcount;
            }
        }
        /// <summary>
        /// 部门ID
        /// </summary>
        public int UnitID
        {
            get
            {
                int uid = 0;
                int.TryParse(Request["uid"] ?? "0", out uid);
                return uid;
            }
        }
        /// <summary>
        /// 部门考核ID
        /// </summary>
        public int KaoHeUnitID
        {
            get
            {
                int kuniid = 0;
                int.TryParse(Request["ukaoheid"] ?? "0", out kuniid);
                return kuniid;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public string RenwuNo
        {
            get
            {
                return Request["urenwuno"] ?? "0";
            }
        }
        /// <summary>
        /// 发起人ID
        /// </summary>
        public int SendUserID
        {
            get
            {
                int userSysNo = 0;
                int.TryParse(Request["senduserid"] ?? "0", out userSysNo);
                return userSysNo;
            }
        }

        public int FromUserID
        {
            get
            {
                
                int usersysno = 0;
                int.TryParse(Request["fromuserid"] ?? "0", out usersysno);
                return usersysno;
            }
       
        }
        public int Gid
        {
            get
            {
                
                int usersysno = 0;
                int.TryParse(Request["Gid"] ?? "0", out usersysno);
                return usersysno;
            }

        }
        /// <summary>
        /// 评价人ID
        /// </summary>
        public int InsertUserID
        {
            get
            {
                int userSysNo = 0;
                int.TryParse(Request["insertmemid"] ?? "0", out userSysNo);
                return userSysNo;
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            switch (Action)
            {
                case "add"://添加
                    AddItem();
                    break;
                case "edit"://修改
                    EditItem();
                    break;
                case "del"://删除
                    DelItem();
                    break;
                case "bak"://考核任务备份
                    RenWuBak();
                    break;
                case "unitbak":
                    KaoHeUnitBak();
                    break;
                case "archdetailbak":
                    ArchDetailBak();
                    break;
                case "allbak":
                    AllMemsBak();
                    break;
                case "allcpbak":
                    AllMemsCompareBak();
                    break;
                case "setcount"://设置总奖金
                    GetJiangCount();
                    break;
                case "savejj"://设置奖金
                    SaveJiangCount();
                    break;
                case "saveunit"://保存考核部门
                    SaveUnit();
                    break;
                case "getkhunit"://获取考核部门
                    GetKaoheUnit();
                    break;
                case "savemems"://保存考核人员
                    SaveMems();
                    break;
                case "getmemexten"://获取考核人员权重
                    GetMemExter();
                    break;
                case "sendusermsg"://发送考核消息
                    SendMsgToUser();
                    break;
                case "resendusermsg"://重新发送考核消息
                    ReSendMsgToUser();
                    break;
                case "savereport"://保存打分数据
                    SaveReport();
                    break;
                case "snyc"://同步数据
                    SnycData();
                    break;
                case "sendmsgtomng"://发送消息给部门经理进行调整
                    SendMsgToUnitMng();
                    break;
                case "savememsmngallot"://部门经理调整
                    SaveMemsMngAllot();
                    break;
                case "savexishu"://保存系数
                    SaveXiShu();
                    break;
                case "saveoutmemsmngallot"://保存离职人员分配
                    SaveOutMemsMngAllot();
                    break;
                case "resendmsgtomng"://重新发起部门奖金分配
                    ReSendMsgToUnitMng();
                    break;
                case "sendmsgtoallallot"://发起总奖金分配 
                    SendMsgToAllAllot();
                    break;
                case "saveallmemsallotdetails"://保存总奖金分配
                    SaveAllMemsAllotDetails();
                    break;
                case "sendonce"://一键提交
                    SendOnceAllData();
                    break;
            }
        }

        /// <summary>
        /// 股东ID  2017年1月17日
        /// </summary> 
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        /// <summary>
        /// 一键提交所有数据
        /// </summary>
        private void SendOnceAllData()
        {
            //获取本部门已设置的人员
            string strWhere = string.Format(" KaoHeUnitID={0} AND memUnitID={1} AND Statu=0 ", KaoHeUnitID, UnitID);
            var khmems = new TG.BLL.cm_KaoHeRenwuMem().GetModelList(strWhere);

            //获取需要打分的人员
            strWhere = string.Format(" KaoHeUnitID={0} AND memUnitID={1} AND Statu=0 AND MemID NOT IN ({2})", KaoHeUnitID, UnitID, GuDongIdStr);
            var dfmems = new TG.BLL.cm_KaoHeRenwuMem().GetModelList(strWhere);

            //查询所有人角色数据
            strWhere = string.Format(" mem_Unit_ID={0} ", UnitID);
            var memRoles = new TG.BLL.tg_memberRole().GetModelList(strWhere);
            //循环保存每个人的打分数据
            TG.BLL.cm_UserJudgeReport bll = new BLL.cm_UserJudgeReport();
            TG.Model.cm_UserJudgeReport model = new Model.cm_UserJudgeReport();
            //判断是否已经设置考核人员
            if (khmems.Count > 0)
            {
                int cont = 0;
                khmems.ForEach(m =>
                {
                    //查询当前人员是否已经有打分数据
                    int insertUserID = m.memID;
                    strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1} ", KaoHeUnitID, insertUserID);
                    int rowCount = bll.GetRecordCount(strWhere);

                    //已经保存过
                    if (rowCount > 0)
                    {
                        string SqlStr = string.Format(" Update dbo.cm_UserJudgeReport Set SaveStatu=1 Where KaoHeUnitID={0} AND InsertUserId={1}", KaoHeUnitID, insertUserID);
                        //执行更新
                        int affectRow = TG.DBUtility.DbHelperSQL.ExecuteSql(SqlStr);

                        if (affectRow > 0)
                        {
                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("-2");
                        }
                    }
                    else
                    {
                        //给每个人插入打分数据
                        dfmems.Where(f => memRoles.Count(r => r.mem_Id == f.memID && r.RoleIdMag == 5) == 0).ToList().ForEach(n =>
                        {
                            model.InsertDate = DateTime.Now;
                            model.InsertUserId = insertUserID;
                            model.Judge1 = null;
                            model.Judge2 = null;
                            model.Judge3 = null;
                            model.Judge4 = null;
                            model.Judge5 = null;
                            model.Judge6 = null;
                            model.Judge7 = null;
                            model.Judge8 = null;
                            model.Judge9 = null;
                            model.Judge10 = null;
                            model.Judge11 = null;
                            model.Judge12 = null;
                            model.JudgeCount = null;
                            model.KaoHeUnitID = KaoHeUnitID;
                            model.MemID = n.memID;
                            model.MemName = n.memName;
                            model.SaveStatu = 1;
                            //插入打分数据
                            int affectRow = bll.Add(model);
                            if (affectRow > 0)
                            {
                                cont++;
                            }
                        });
                    }

                });
                //添加打分数据
                if (cont > 0)
                {
                    Response.Write("1");
                }
                else
                {
                    Response.Write("-1");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 当前选中的部门
        /// </summary>
        public string SelUnitName
        {
            get
            {
                return Request["unitname"] ?? "";
            }
        }
        /// <summary>
        /// 所有人员备份
        /// </summary>
        private void AllMemsCompareBak()
        {
            var allMemsList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeAllMemsCompareHis>>(entityData);

            if (allMemsList.Count > 0)
            {
                //查询考核任务是否已经归档

                string strWhere = string.Format(" RenwuID={0} ", allMemsList[0].RenwuID);
                int renwuCount = new TG.BLL.cm_KaoheRenwuHistory().GetRecordCount(strWhere);

                //当前备份数据
                string strWhere2 = string.Format(" RenwuID={0} AND IsFired='{1}'", allMemsList[0].RenwuID, allMemsList[0].IsFired);
                int renwuCount2 = new TG.BLL.cm_KaoHeAllMemsCompareHis().GetRecordCount(strWhere2);

                //判断部门是否归档
                if (this.SelUnitName.Trim() != "全部部门")
                {
                    strWhere2 = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '{2}%') ", allMemsList[0].RenwuID, allMemsList[0].IsFired, allMemsList[0].UnitName.Trim());
                    renwuCount2 = new TG.BLL.cm_KaoHeAllMemsCompareHis().GetRecordCount(strWhere2);
                }

                if (renwuCount > 0 && renwuCount2 == 0)
                {
                    var bll = new TG.BLL.cm_KaoHeAllMemsCompareHis();

                    int count = bll.GetRecordCount(strWhere2);
                    if (count == 0)
                    {
                        allMemsList.ForEach(c =>
                        {
                            bll.Add(c);
                        });

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("2");
                    }

                }
                else
                {
                    Response.Write("3");
                }
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 所有人员备份
        /// </summary>
        private void AllMemsBak()
        {
            var allMemsList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeAllMemsHis>>(entityData);

            if (allMemsList.Count > 0)
            {
                //查询考核任务是否已经归档

                string strWhere = string.Format(" RenwuID={0} ", allMemsList[0].RenwuID);
                int renwuCount = new TG.BLL.cm_KaoheRenwuHistory().GetRecordCount(strWhere);

                //当前备份数据
                string strWhere2 = string.Format(" RenwuID={0} AND IsFired='{1}'", allMemsList[0].RenwuID, allMemsList[0].IsFired);
                int renwuCount2 = new TG.BLL.cm_KaoHeAllMemsHis().GetRecordCount(strWhere2);

                //判断部门是否归档
                if (this.SelUnitName.Trim() != "全部部门")
                {
                    strWhere2 = string.Format(" RenwuID={0} AND IsFired='{1}' AND (UnitName like '{2}%') ", allMemsList[0].RenwuID, allMemsList[0].IsFired, allMemsList[0].UnitName.Trim());
                    renwuCount2 = new TG.BLL.cm_KaoHeAllMemsHis().GetRecordCount(strWhere2);
                }

                if (renwuCount > 0 && renwuCount2 == 0)
                {
                    var bll = new TG.BLL.cm_KaoHeAllMemsHis();

                    int count = bll.GetRecordCount(strWhere2);
                    if (count == 0)
                    {
                        allMemsList.ForEach(c =>
                        {
                            bll.Add(c);
                        });

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("2");
                    }

                }
                else
                {
                    Response.Write("3");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 建筑详细归档
        /// </summary>
        private void ArchDetailBak()
        {
            var detailList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeUnitDetailsHis>>(entityData);

            if (detailList.Count > 0)
            {
                //查询考核任务是否已经归档

                string strWhere = string.Format(" RenwuID={0}", detailList[0].RenwuID);
                int renwuCount = new TG.BLL.cm_KaoheRenwuHistory().GetRecordCount(strWhere);

                if (renwuCount > 0)
                {
                    // 查询是否已经归档
                    strWhere = string.Format(" RenwuID={0} AND UnitID={1}", detailList[0].RenwuID, detailList[0].UnitID);
                    var bll = new TG.BLL.cm_KaoHeUnitDetailsHis();
                    int unitCount = bll.GetRecordCount(strWhere);

                    if (unitCount == 0)
                    {
                        detailList.ForEach(c =>
                        {
                            bll.Add(c);
                        });

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("2");
                    }

                    //保存离职人员历史记录
                    var detailsOutList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeUnitDetailsOutHis>>(entityData2);

                    if (detailsOutList.Count > 0)
                    {
                        // 查询是否已经归档
                        strWhere = string.Format(" RenwuID={0} AND UnitID={1}", detailsOutList[0].RenwuID, detailsOutList[0].UnitID);
                        var bll2 = new TG.BLL.cm_KaoHeUnitDetailsOutHis();
                        int unitOutCount = bll2.GetRecordCount(strWhere);

                        if (unitOutCount == 0)
                        {
                            detailsOutList.ForEach(c =>
                            {
                                bll2.Add(c);
                            });
                        }
                    }

                }
                else
                {
                    Response.Write("3");
                }
            }
            else
            {
                Response.Write("4");
            }
        }
        /// <summary>
        /// 备份本次考核下部门信息
        /// </summary>
        private void KaoHeUnitBak()
        {
            var kaoheUnit = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeUnitHis>>(entityData);

            if (kaoheUnit.Count > 0)
            {
                //查询考核任务是否已经归档

                string strWhere = string.Format(" RenwuID={0}", kaoheUnit[0].RenwuID);
                int renwuCount = new TG.BLL.cm_KaoheRenwuHistory().GetRecordCount(strWhere);

                if (renwuCount > 0)
                {
                    // 查询是否已经归档
                    var bll = new TG.BLL.cm_KaoHeUnitHis();
                    int unitCount = bll.GetRecordCount(strWhere);

                    if (unitCount == 0)
                    {
                        kaoheUnit.ForEach(c =>
                        {
                            bll.Add(c);
                        });

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("2");
                    }
                }
                else
                {
                    Response.Write("3");
                }
            }
            else
            {
                Response.Write("4");
            }

        }
        /// <summary>
        /// 归档
        /// </summary>
        private void RenWuBak()
        {
            if (this.RenwuNo != null)
            {

                var KaoHeRenwuCountHisModel = JsonConvert.DeserializeObject<TG.Model.cm_KaoHeRenWuCountHis>(entityData);
                //查询考核任务
                int renwuID = int.Parse(this.ID);
                var kaoheModel = new TG.BLL.cm_KaoHeRenWu().GetModel(renwuID);

                //声明考核历史对象
                var kaoheHisModel = new TG.Model.cm_KaoheRenwuHistory();
                kaoheHisModel.RenWuID = kaoheModel.ID;
                kaoheHisModel.RenWuName = kaoheModel.RenWuNo;

                var bll = new TG.BLL.cm_KaoheRenwuHistory();


                //查询时否已经村子归档
                string strWhere = string.Format(" RenWuID={0}", kaoheModel.ID);
                var count = bll.GetModelList(strWhere).Count;

                if (count == 0)
                {
                    int affectRow = bll.Add(kaoheHisModel);

                    //插入历史数据
                    bool issuccess = InsertRenwuCountHis(KaoHeRenwuCountHisModel);

                    if (affectRow > 0 && issuccess)
                    {
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write(2);
                }

            }
        }
        /// <summary>
        /// 统计考核数据 
        /// </summary>
        private bool InsertRenwuCountHis(TG.Model.cm_KaoHeRenWuCountHis model)
        {
            bool flag = false;
            if (model != null)
            {
                var bllHis = new TG.BLL.cm_KaoHeRenWuCountHis();
                string strWhere = string.Format(" RenwuID={0}", model.RenwuID);

                var list = bllHis.GetModelList(strWhere);

                if (list.Count == 0)
                {
                    int rowCount = bllHis.Add(model);

                    if (rowCount > 0)
                    {
                        flag = true;
                    }
                }
            }

            return flag;
        }
        /// <summary>
        /// 保存总奖金分配
        /// </summary>
        private void SaveAllMemsAllotDetails()
        {
            var allMems = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeAllMemsAllotDetails>>(entityData);

            //判断如果是管理员添加人员 2017年11月24日  设置添加人员部门考核为参加
            if (allMems[0].insertUserID == 1)
            {
                //查询考核任务下的部门考核
                string strWhere = string.Format(" RenWuNo='{0}' AND UnitID={1}", allMems[0].RenwuID, allMems[0].UnitID);
                var bllunit = new BLL.cm_KaoHeRenwuUnit();
                var kaoheUnitList = bllunit.GetModelList(strWhere);
                if (kaoheUnitList.Count > 0)
                {
                    int kaoheunitID = kaoheUnitList[0].ID;
                    //更新人员设置界面
                    var bll = new BLL.cm_KaoHeRenwuMem();
                    strWhere = string.Format(" memID={0} AND KaoHeUnitID={1} AND memUnitID={2}", allMems[0].MemID, kaoheunitID, allMems[0].UnitID);
                    var kaoheMems = bll.GetModelList(strWhere);

                    if (kaoheMems.Count > 0)
                    {
                        kaoheMems[0].Statu = 0;
                        bll.Update(kaoheMems[0]);
                    }
                }
            }

            if (allMems.Count > 0)
            {
                string strWhere = "";
                var bll = new TG.BLL.cm_KaoHeAllMemsAllotDetails();
                allMems.ForEach(c =>
                {
                    strWhere = string.Format(" RenwuID={0} AND MemID={1} AND UnitID={2} ", c.RenwuID, c.MemID, c.UnitID);

                    var list = bll.GetModelList(strWhere);

                    if (list.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        c.ID = list[0].ID;
                        bll.Update(c);
                    }
                });

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 发送消息进行奖金调整
        /// </summary>
        private void SendMsgToAllAllot()
        {
            //插入一条消息
            var bllrecord = new TG.BLL.cm_KaoHeAllMemsAllotRecord();
            var bllrenwu = new TG.BLL.cm_KaoHeRenWu();
            string renwuName = bllrenwu.GetModel(int.Parse(this.RenwuNo)).RenWuNo;
            int allotuserid = this.FromUserID; //张维  
            int fromuserid = this.SendUserID;
            int Gids = this.Gid;//管理员          
            var record = new TG.Model.cm_KaoHeAllMemsAllotRecord();
            record.RenwuID = int.Parse(this.RenwuNo);
            record.RenwuName = renwuName;
            record.AllotUserID =allotuserid;
            record.InserDate = DateTime.Now;
            record.FromUserID = fromuserid;
            record.Gid = Gids;
            record.Stat = 0;

            if (bllrecord.Add(record) > 0)
            {

                string msgType = "年度总奖金";
                //发送消息
                var bllmsg = new TG.BLL.cm_SysMsg();
                var sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("renwuno={0}&action=4", this.RenwuNo),
                    FromUser = this.FromUserID,
                    InUser = this.SendUserID,
                    MsgType = 34,//项目比例设置
                    MessageContent = string.Format("关于\"{0}\"分配任务{1}调整消息！", renwuName.Trim(), msgType),
                    QueryCondition = renwuName + "的" + msgType,
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };
                int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
                var sysMessageViewEntityw = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("renwuno={0}&action=4", this.RenwuNo),
                    FromUser = this.Gid,
                    InUser = this.SendUserID,
                    MsgType = 34,//项目比例设置
                    MessageContent = string.Format("关于\"{0}\"分配任务{1}调整消息！", renwuName.Trim(), msgType),
                    QueryCondition = renwuName + "的" + msgType,
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };

                //发消息
                int countw = bllmsg.InsertSysMessage(sysMessageViewEntityw);
            }

            Response.Write("1");
        }
        /// <summary>
        /// 重新调整消息
        /// </summary>
        private void ReSendMsgToUnitMng()
        {
            //改变submit的状态
            string strWhere = string.Format(" RenwuID={0} AND UnitID={1} AND Stat=1", this.RenwuNo, this.UnitID);
            var bll = new TG.BLL.cm_KaoHeMemsMngAllot();
            var bll2 = new TG.BLL.cm_KaoHeMemsOutMngAllot();

            var list = bll.GetModelList(strWhere);
            var listout = bll2.GetModelList(strWhere);
            //更新在职人员状态
            list.ForEach(c =>
            {
                c.Stat = 0;
                bll.Update(c);
            });
            //更新离职人员状态
            listout.ForEach(c =>
            {

                c.Stat = 0;
                bll2.Update(c);
            });

            //发送消息
            var bllmsg = new TG.BLL.cm_SysMsg();
            //项目
            var unitModel = new TG.BLL.tg_unit().GetModel(this.UnitID);
            string unitname = "";
            if (unitModel != null)
            {
                unitname = unitModel.unit_Name;
            }
            //考核名称
            var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));
            string renwuName = "";
            if (renwuModel != null)
            {
                renwuName = renwuModel.RenWuNo;
            }

            string msgType = "内部奖金计算";
            //实例消息实体
            var sysMessageViewEntity = new SysMessageViewEntity
            {
                ReferenceSysNo = string.Format("uid={0}&renwuno={1}&id={2}&action=3", this.UnitID, this.RenwuNo, this.KaoHeUnitID),
                FromUser = this.FromUserID,
                InUser = this.SendUserID,
                MsgType = 34,//项目比例设置
                MessageContent = string.Format("关于\"{0}\"的{2}考核{1}重新调整消息！", unitname.Trim(), msgType, renwuName),
                QueryCondition = unitname + "的" + msgType,
                Status = "A",
                IsDone = "A",
                ToRole = "0"
            };

            //发消息
            int count = bllmsg.InsertSysMessage(sysMessageViewEntity);

            Response.Write("1");

        }
        /// <summary>
        /// 保存离职人员
        /// </summary>
        private void SaveOutMemsMngAllot()
        {
            var outMems = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeMemsOutMngAllot>>(entityData);

            if (outMems.Count > 0)
            {
                string strWhere = "";
                var bll = new TG.BLL.cm_KaoHeMemsOutMngAllot();
                outMems.ForEach(c =>
                {
                    strWhere = string.Format(" RenwuID={0} AND MemID={1} AND UnitID={2} ", c.RenwuID, c.MemID, c.UnitID);

                    var list = bll.GetModelList(strWhere);

                    if (list.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        c.ID = list[0].ID;
                        bll.Update(c);
                    }
                });

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存比例系数
        /// </summary>
        private void SaveXiShu()
        {
            var xiShu = JsonConvert.DeserializeObject<List<TG.Model.cm_SysProjAllotXiShu>>(entityData);

            if (xiShu.Count > 0)
            {
                var bll = new TG.BLL.cm_SysProjAllotXiShu();
                xiShu.ForEach(c =>
                {
                    //查询是否已经存在
                    var model = new TG.BLL.cm_SysProjAllotXiShu().GetModel(c.ID);

                    if (model != null)
                    {
                        bll.Update(model);
                    }
                });
            }
        }
        /// <summary>
        /// 保存部门经理调制表
        /// </summary>
        private void SaveMemsMngAllot()
        {
            var memsMngList = JsonConvert.DeserializeObject<List<cm_KaoHeMemsMngAllot>>(entityData);

            if (memsMngList.Count > 0)
            {
                string strWhere = "";
                var bll = new TG.BLL.cm_KaoHeMemsMngAllot();
                memsMngList.ForEach(c =>
                {
                    strWhere = string.Format(" RenwuID={0} AND MemID={1} AND UnitID={2} ", c.RenwuID, c.MemID, c.UnitID);

                    var list = bll.GetModelList(strWhere);

                    if (list.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        c.ID = list[0].ID;
                        bll.Update(c);
                    }
                });

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }

        public bool IsSendMng
        {
            get
            {
                string val = Request["issendmng"] ?? "0";
                if (val == "0")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        /// <summary>
        /// 发消息给部门经理
        /// </summary>
        private void SendMsgToUnitMng()
        {
            //根据人员部门找
            //var mems = new TG.BLL.tg_member().GetModelList(" mem_Unit_ID=" + this.UnitID + "");
            var mems = new TG.BLL.tg_member().GetModelList(" mem_isFired=0 ");
            //根据角色部门找
            string where = string.Format(" RoleIdMag=5 AND mem_Unit_ID={0}", this.UnitID);
            //常务副总经理张维，人事行政部门经理
            if (this.UnitID == 242 || this.UnitID == 243 || this.UnitID == 244 || this.UnitID == 281)
            {
                where = string.Format(" RoleIdMag IN (3,5,44) AND mem_Unit_ID={0}", this.UnitID);
            }
            var memroles = new TG.BLL.tg_memberRole().GetModelList(where);

            //查询认识主管
            //var query = from m in mems
            //            join r in memroles on m.mem_ID equals r.mem_Id
            //            where new string[] { "3", "5", "44" }.Contains(r.RoleIdMag.ToString())
            //            select m;

            var query = from m in mems
                        join r in memroles on m.mem_ID equals r.mem_Id
                        select m;

            var userlist = query.ToList();

            if (userlist.Count == 0)
            {
                Response.Write("0");
            }
            else
            {
                //更新提交状态
                var bllKHBll = new TG.BLL.cm_KaoHeRenwuUnit();
                var unitKHModel = bllKHBll.GetModel(this.KaoHeUnitID);
                if (unitKHModel != null)
                {
                    unitKHModel.Statu = 1;
                    bllKHBll.Update(unitKHModel);
                }
                //更新是否发送调整信息
                if (IsSendMng)
                {
                    if (unitKHModel != null)
                    {
                        unitKHModel.IsSendMng = 1;
                        bllKHBll.Update(unitKHModel);
                    }
                }
                //发送消息
                var bllmsg = new TG.BLL.cm_SysMsg();
                //项目
                var unitModel = new TG.BLL.tg_unit().GetModel(this.UnitID);
                string unitname = "";
                if (unitModel != null)
                {
                    unitname = unitModel.unit_Name;
                }
                //考核名称
                var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));
                string renwuName = "";
                if (renwuModel != null)
                {
                    renwuName = renwuModel.RenWuNo;
                }

                userlist.ForEach(m =>
                {
                    //记录内部考核
                    string strWhere = string.Format(" UnitID={0} AND RenwuID={1} AND KaoHeUnitID={2} AND InsertUserID={3} ", this.UnitID, this.RenwuNo, this.KaoHeUnitID, m.mem_ID);
                    var kaoheBll = new TG.BLL.cm_KaoHeMemsMngAllotList();
                    var kaoheList = kaoheBll.GetModelList(strWhere);
                    if (kaoheList.Count == 0)
                    {
                        var kaoheModel = new TG.Model.cm_KaoHeMemsMngAllotList()
                        {
                            UnitID = this.UnitID,
                            UnitName = unitname,
                            RenwuID = int.Parse(this.RenwuNo),
                            RenwuName = renwuName,
                            KaoHeUnitID = this.KaoHeUnitID,
                            InsertUserID = m.mem_ID
                        };

                        kaoheBll.Add(kaoheModel);
                    }
                });



                string msgType = "内部奖金计算";
                userlist.ForEach(m =>
                {//实例消息实体
                    var sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("uid={0}&renwuno={1}&id={2}&action=3", this.UnitID, this.RenwuNo, this.KaoHeUnitID),
                        FromUser = m.mem_ID,
                        InUser = this.SendUserID,
                        MsgType = 34,//项目比例设置
                        MessageContent = string.Format("关于\"{0}\"的{2}考核{1}消息！", unitname.Trim(), msgType, renwuName),
                        QueryCondition = unitname + "的" + msgType,
                        Status = "A",
                        IsDone = "A",
                        ToRole = "0"
                    };

                    //发消息
                    int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
                });

                Response.Write("1");
            }
        }
        /// <summary>
        /// 同步NULL数据
        /// </summary>
        private void SnycData()
        {
            //部门经理是否已经评价
            StringBuilder sb = new StringBuilder();
            sb.Append(" Select * ");
            sb.Append(" From dbo.cm_UserJudgeReport P left join tg_memberRole M on p.InsertUserId=m.mem_Id ");
            sb.AppendFormat(" Where KaoHeUnitID={0} AND m.RoleIdMag=5 AND SaveStatu=1 AND M.mem_Unit_ID={1}", this.KaoHeUnitID, this.UnitID);

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            if (dt.Rows.Count == 0)
            {
                //项目经理无评价记录
                Response.Write("0");
                return;
            }

            StringBuilder sbmng = new StringBuilder();
            sbmng.Append(" Select * ");
            sbmng.Append(" From dbo.cm_UserJudgeReport P left join tg_memberRole M on p.InsertUserId=m.mem_Id ");
            sbmng.AppendFormat(@" Where ((Judge1 IS NULL) 
	                            OR (Judge2 IS NULL) 
	                            OR (Judge3 IS NULL) 
	                            OR (Judge4 IS NULL) 
	                            OR (Judge5 IS NULL) 
	                            OR (Judge6 IS NULL) 
	                            OR (Judge7 IS NULL) 
	                            OR (Judge8 IS NULL) 
	                            OR (Judge9 IS NULL) 
	                            OR (Judge10 IS NULL) 
	                            OR (Judge11 IS NULL) 
	                            OR (Judge12 IS NULL) ) AND KaoHeUnitID={0} AND m.RoleIdMag=5 AND SaveStatu=1 AND M.mem_Unit_ID={1}", this.KaoHeUnitID, this.UnitID);

            DataTable dtmng = TG.DBUtility.DbHelperSQL.Query(sbmng.ToString()).Tables[0];
            if (dtmng.Rows.Count > 0)
            {
                //项目经理无评价记录
                Response.Write("00");
                return;
            }


            //得到需要同步的数据
            StringBuilder sb2 = new StringBuilder();
            sb2.Append(" Select *");
            sb2.Append(" From dbo.cm_UserJudgeReport");
            sb2.AppendFormat(@" Where ((Judge1 IS NULL) 
	                            OR (Judge2 IS NULL) 
	                            OR (Judge3 IS NULL) 
	                            OR (Judge4 IS NULL) 
	                            OR (Judge5 IS NULL) 
	                            OR (Judge6 IS NULL) 
	                            OR (Judge7 IS NULL) 
	                            OR (Judge8 IS NULL) 
	                            OR (Judge9 IS NULL) 
	                            OR (Judge10 IS NULL) 
	                            OR (Judge11 IS NULL) 
	                            OR (Judge12 IS NULL) ) AND KaoHeUnitID={0} AND InsertUserId={1}", this.KaoHeUnitID, this.InsertUserID);
            DataTable dt2 = TG.DBUtility.DbHelperSQL.Query(sb2.ToString()).Tables[0];
            //需要同步
            if (dt.Rows.Count > 0)
            {
                TG.BLL.cm_UserJudgeReport bll = new BLL.cm_UserJudgeReport();
                //记录历史数据
                TG.BLL.cm_UserJudgeReportHis bllHis = new BLL.cm_UserJudgeReportHis();
                //得到原始表单
                string strWhere = string.Format(" KaoHeUnitID={0} AND InsertUserId={1}", this.KaoHeUnitID, this.InsertUserID);
                var HisModelList = bll.GetModelList(strWhere);
                if (HisModelList.Count > 0)
                {
                    var modelHis = new TG.Model.cm_UserJudgeReportHis();
                    HisModelList.ForEach(c =>
                    {
                        //历史数据
                        modelHis.ID = c.ID;
                        modelHis.InsertDate = c.InsertDate;
                        modelHis.InsertUserId = c.InsertUserId;
                        modelHis.Judge1 = c.Judge1;
                        modelHis.Judge2 = c.Judge2;
                        modelHis.Judge3 = c.Judge3;
                        modelHis.Judge4 = c.Judge4;
                        modelHis.Judge5 = c.Judge5;
                        modelHis.Judge6 = c.Judge6;
                        modelHis.Judge7 = c.Judge7;
                        modelHis.Judge8 = c.Judge8;
                        modelHis.Judge9 = c.Judge9;
                        modelHis.Judge10 = c.Judge10;
                        modelHis.Judge11 = c.Judge11;
                        modelHis.Judge12 = c.Judge12;
                        modelHis.JudgeCount = c.JudgeCount;
                        modelHis.KaoHeUnitID = c.KaoHeUnitID;
                        modelHis.SaveStatu = c.SaveStatu;
                        modelHis.MemID = c.MemID;
                        modelHis.MemName = c.MemName;

                        bllHis.Add(modelHis);
                    });
                }
                //同步空的数据
                foreach (DataRow dr in dt2.Rows)
                {
                    string id = dr["ID"].ToString();
                    //得到部门经理打分
                    string memid = dr["MemID"].ToString();
                    DataView dv = dt.DefaultView;
                    dv.RowFilter = string.Format(" MemID={0}", memid);

                    //判断项目经理打分数据中是否存在此人打分
                    if (dv.ToTable().Rows.Count == 0)
                    {
                        continue;
                    }
                    //得到经理打分后具体的人
                    string mngid = dv.ToTable().Rows[0]["ID"].ToString();
                    //获取经理打分具体人
                    var mngModel = bll.GetModel(int.Parse(mngid));
                    //有Null实体
                    var model = bll.GetModel(int.Parse(id));

                    if (mngModel != null && model != null)
                    {
                        //同步
                        model.Judge1 = model.Judge1 == null ? mngModel.Judge1 : model.Judge1;
                        model.Judge2 = model.Judge2 == null ? mngModel.Judge2 : model.Judge2;
                        model.Judge3 = model.Judge3 == null ? mngModel.Judge3 : model.Judge3;
                        model.Judge4 = model.Judge4 == null ? mngModel.Judge4 : model.Judge4;
                        model.Judge5 = model.Judge5 == null ? mngModel.Judge5 : model.Judge5;
                        model.Judge6 = model.Judge6 == null ? mngModel.Judge6 : model.Judge6;
                        model.Judge7 = model.Judge7 == null ? mngModel.Judge7 : model.Judge7;
                        model.Judge8 = model.Judge8 == null ? mngModel.Judge8 : model.Judge8;
                        model.Judge9 = model.Judge9 == null ? mngModel.Judge9 : model.Judge9;
                        model.Judge10 = model.Judge10 == null ? mngModel.Judge10 : model.Judge10;
                        model.Judge11 = model.Judge11 == null ? mngModel.Judge11 : model.Judge11;
                        model.Judge12 = model.Judge12 == null ? mngModel.Judge12 : model.Judge12;

                        //计算总得分
                        decimal? result = 0;
                        result += model.Judge1 * (decimal)10;
                        result += model.Judge2 * (decimal)12;
                        result += model.Judge3 * (decimal)10;
                        result += model.Judge4 * (decimal)12;
                        result += model.Judge5 * (decimal)8;
                        result += model.Judge6 * (decimal)6;
                        result += model.Judge7 * (decimal)5;
                        result += model.Judge8 * (decimal)5;
                        result += model.Judge9 * (decimal)4;
                        result += model.Judge10 * (decimal)4;
                        result += model.Judge11 * (decimal)2;
                        result += model.Judge12 * (decimal)1;

                        decimal? JudgeCount = (20 * result) / 79;

                        model.JudgeCount = JudgeCount;
                        //更新
                        bll.Update(model);

                    }
                    else
                    {
                        Response.Write("000");
                    }
                }

                Response.Write("1");
            }
            else
            {
                Response.Write("000");
                return;
            }

        }
        /// <summary>
        /// 保存评价表
        /// </summary>
        private void SaveReport()
        {
            TG.BLL.cm_UserJudgeReport bll = new BLL.cm_UserJudgeReport();
            var reportModel = JsonConvert.DeserializeObject<List<TG.Model.cm_UserJudgeReport>>(entityData);

            if (reportModel.Count > 0)
            {
                reportModel.ForEach(c =>
                {
                    string strWhere = string.Format(" MemID={2} AND KaoHeUnitID={0} AND InsertUserId={1}", c.KaoHeUnitID, c.InsertUserId, c.MemID);
                    var model = bll.GetModelList(strWhere);
                    if (model.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        //出现一次保存，刷新多次提交的问题，多次插入的问题
                        //保留一条 其余删除
                        if (model.Count == 1)
                        {
                            c.ID = model[0].ID;
                            bll.Update(c);
                        }
                        else if (model.Count > 1)
                        {
                            //2017年1月17日

                            int i = 0;
                            model.OrderBy(a => a.InsertDate).ToList().ForEach(a =>
                            {
                                if (i == 0)
                                {
                                    c.ID = model[0].ID;
                                    bll.Update(c);
                                }
                                else
                                {
                                    bll.Delete(c.ID);
                                }

                                i++;
                            });
                        }
                    }
                });
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 发送消息给考核的用户
        /// </summary>
        private void SendMsgToUser()
        {
            TG.BLL.cm_KaoHeRenwuMem bll = new BLL.cm_KaoHeRenwuMem();
            var list = bll.GetModelList("").Where(c => c.memUnitID == this.UnitID && c.KaoHeUnitID == this.KaoHeUnitID && c.Statu == 0).Select(c => c).ToList();
            //任务
            var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));
            //发送消息
            var bllmsg = new TG.BLL.cm_SysMsg();
            SysMessageViewEntity sysMessageViewEntity;
            //查询部门
            string unitName = "";
            var unitModel = new TG.BLL.tg_unit().GetModel(this.UnitID);
            if (unitModel != null)
            {
                unitName = unitModel.unit_Name.Trim();
            }

            int msgcount = 0;
            list.ForEach(c =>
            {
                //实例消息实体
                sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("uid={0}&ukaoheid={1}&renwuno={2}", c.memUnitID, c.KaoHeUnitID, this.RenwuNo),
                    FromUser = c.memID,
                    InUser = this.SendUserID,
                    MsgType = 33,//分项目
                    MessageContent = string.Format("关于\"{0}\"考核任务的{1}内部打分消息！", renwuModel.RenWuNo, unitName),
                    QueryCondition = c.memUnitName + "的" + c.memName + "考核打分",
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };

                //发消息
                int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
                if (count > 0)
                {
                    msgcount++;
                }
            });

            //更新部门提交状态
            if (msgcount > 0)
            {
                var kaoHeUnitModel = new TG.BLL.cm_KaoHeRenwuUnit().GetModel(this.KaoHeUnitID);
                if (kaoHeUnitModel != null)
                {
                    kaoHeUnitModel.Statu = 1;
                    new TG.BLL.cm_KaoHeRenwuUnit().Update(kaoHeUnitModel);
                }
            }

            if (msgcount > 0)
            {
                Response.Write(msgcount);
            }
            else
            {
                Response.Write("error");
            }
        }

        /// <summary>
        /// 发送消息给考核的用户
        /// </summary>
        private void ReSendMsgToUser()
        {
            TG.BLL.cm_KaoHeRenwuMem bll = new BLL.cm_KaoHeRenwuMem();
            var list = bll.GetModelList("").Where(c => c.memUnitID == this.UnitID && c.KaoHeUnitID == this.KaoHeUnitID && c.Statu == 0).Select(c => c).ToList();
            //任务
            var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));
            //发送消息
            var bllmsg = new TG.BLL.cm_SysMsg();
            SysMessageViewEntity sysMessageViewEntity;
            //查询部门
            string unitName = "";
            var unitModel = new TG.BLL.tg_unit().GetModel(this.UnitID);
            if (unitModel != null)
            {
                unitName = unitModel.unit_Name.Trim();
            }
            //删除已经发送的消息
            string strSql = string.Format(" delete From cm_SysMsg Where ReferenceSysNo='uid={0}&ukaoheid={1}&renwuno={2}' ", this.UnitID, this.KaoHeUnitID, this.RenwuNo);
            int AffectRows = TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);

            int msgcount = 0;
            list.ForEach(c =>
            {
                //实例消息实体
                sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("uid={0}&ukaoheid={1}&renwuno={2}", c.memUnitID, c.KaoHeUnitID, this.RenwuNo),
                    FromUser = c.memID,
                    InUser = this.SendUserID,
                    MsgType = 33,//分项目
                    MessageContent = string.Format("关于\"{0}\"考核任务的{1}内部打分消息！", renwuModel.RenWuNo, unitName),
                    QueryCondition = c.memUnitName + "的" + c.memName + "考核打分",
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };

                //发消息
                int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
                if (count > 0)
                {
                    msgcount++;
                }
            });
            //删除本次考核本部门所有打分数据
            strSql = string.Format(" delete From cm_UserJudgeReport Where KaoHeUnitID={0} ", this.KaoHeUnitID);
            int AffectRows2 = TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);
            //UpdateMoreOrLessUsers(list);

            if (msgcount > 0)
            {
                Response.Write(msgcount);
            }
            else
            {
                Response.Write("error");
            }
        }
        /// <summary>
        /// 删除因添加删除考核人的冗余数据
        /// </summary>
        /// <param name="list"></param>
        private void UpdateMoreOrLessUsers(List<cm_KaoHeRenwuMem> list)
        {
            //查询不是部门管理员
            var listjudge = list.Where(c => c.MagRole != 5).ToList();
            //打分列表
            var bllUserJudge = new TG.BLL.cm_UserJudgeReport();
            var listuserjudge = bllUserJudge.GetModelList(" KaoHeUnitID=" + this.KaoHeUnitID + " ");
            //查询新添加的人
            var listnewuser = (from u in listjudge
                               where !(from d in listuserjudge select d.MemID).Distinct().Contains(u.memID)
                               select u).ToList();
            //给每个打分的人添加一条打分数据，除了自己
            list.ForEach(c =>
            {
                listnewuser.ForEach(n =>
                {
                    if (c.memID != n.memID)
                    {
                        var userjudgerpt = new TG.Model.cm_UserJudgeReport()
                        {
                            MemID = n.memID,
                            MemName = n.memName,
                            Judge1 = 0,
                            Judge2 = 0,
                            Judge3 = 0,
                            Judge4 = 0,
                            Judge5 = 0,
                            Judge6 = 0,
                            Judge7 = 0,
                            Judge8 = 0,
                            Judge9 = 0,
                            Judge10 = 0,
                            Judge11 = 0,
                            Judge12 = 0,
                            SaveStatu = 0,
                            KaoHeUnitID = c.KaoHeUnitID,
                            InsertUserId = c.memID,
                            InsertDate = DateTime.Now,
                            JudgeCount = 0
                        };

                        //新增打分数据
                        bllUserJudge.Add(userjudgerpt);
                    }
                });
            });
            //更新本次考核数据为保存状态
            string strSql = string.Format(" Update cm_UserJudgeReport set SaveStatu=0 Where KaoHeUnitID={0} ", this.KaoHeUnitID);
            int AffectRows2 = TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);

            //查询已删除的考核人
            var listdeluser = (from d in listuserjudge
                               where !(from u in listjudge select u.memID).Contains(Convert.ToInt32(d.MemID))
                               select d).ToList();

            listdeluser.ForEach(u =>
            {
                //删除被评价数据
                bllUserJudge.Delete(u.ID);
                //删除打分数据
                string delSql = string.Format(" InsertUserId={0} AND KaoHeUnitID={1} ", u.MemID, this.KaoHeUnitID);
                int AffectRows3 = TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);
            });

        }
        /// <summary>
        /// 获取权重
        /// </summary>
        private void GetMemExter()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("select RoleIdMag,RoleIdTec,");
            sb.Append("isnull((select RoleName from dbo.cm_RoleMag where ID=RoleIdMag),'') AS RoleMagName,");
            sb.Append("isnull((select RoleName from dbo.cm_RoleMag where ID=RoleIdTec),'') AS RoleTecName,");
            sb.Append("Weights");
            sb.Append(" From dbo.tg_memberRole ");
            sb.AppendFormat(" Where mem_Id={0}", this.ID);

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            if (dt.Rows.Count > 0)
            {
                string json = JsonConvert.SerializeObject(dt);
                Response.Write(json);
            }
            else
            {
                Response.Write("1");
            }
        }
        /// <summary>
        /// 保存考核拥护 
        /// </summary>
        private void SaveMems()
        {
            TG.BLL.cm_KaoHeRenwuMem bll = new BLL.cm_KaoHeRenwuMem();
            List<TG.Model.cm_KaoHeRenwuMem> list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeRenwuMem>>(entityData);

            if (list.Count > 0)
            {
                list.ForEach(m =>
                {
                    //是否已经存在
                    string strWhere = string.Format(" KaoHeUnitID={0} AND memID={1}", m.KaoHeUnitID, m.memID);
                    var model = bll.GetModelList(strWhere);
                    if (model.Count == 0)
                    {
                        //如果为部门经理且参与考核
                        if (m.MagRole == 5 && m.Statu == 0)
                        {
                            //本部门经理
                            if ((m.memUnitID == m.IsOutUnitID) || m.IsOutUnitID == 0)
                            {
                                decimal? result = GetMagWeight(list, m.memID);
                                m.Weights = result < 5 ? 5 : result;
                            }
                        }
                        //新建
                        bll.Add(m);
                    }
                    else
                    {
                        m.ID = model[0].ID;

                        //如果为项目经理且参与考核
                        if (m.MagRole == 5 && m.Statu == 0)
                        {
                            //本部门经理
                            if ((m.memUnitID == m.IsOutUnitID) || m.IsOutUnitID == 0)
                            {
                                decimal? result = GetMagWeight(list, m.memID);
                                m.Weights = result < 5 ? 5 : result;
                            }
                        }
                        //更新
                        bll.Update(m);
                    }
                });

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }

        public decimal? GetMagWeight(List<TG.Model.cm_KaoHeRenwuMem> list, int memId)
        {
            decimal? weights = 0;
            decimal div = 2;
            if (list.Count > 0)
            {
                list.ForEach(m =>
                {
                    //参与考核的人,不是本人，要是本部门所有人员
                    //if (m.Statu == 0 && m.memID != memId && m.IsOutUnitID == 0)
                    if (m.Statu == 0 && m.memID != memId)
                    {
                        weights += m.Weights;
                    }
                });
            }

            return weights / div;
        }
        /// <summary>
        /// 获取存根
        /// </summary>
        private void GetKaoheUnit()
        {
            TG.BLL.cm_KaoHeRenwuUnit bll = new BLL.cm_KaoHeRenwuUnit();

            StringBuilder sb = new StringBuilder();
            sb.Append("Select *");
            sb.Append(",(Select COUNT(*) From dbo.cm_KaoHeRenwuMem Where memUnitID=A.UnitID AND Statu=0 And KaoHeUnitID=A.ID) AS RenYuanCount");//此处的Statu为人员是否考核
            sb.Append(" From cm_KaoHeRenwuUnit A");
            sb.AppendFormat(" Where RenWuNo='{0}'", ID);
            //获取单位考核列表
            DataTable list = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            if (list.Rows.Count > 0)
            {
                string jsonResult = JsonConvert.SerializeObject(list);
                Response.Write(jsonResult);
            }
            else
            {
                Response.Write("1");
            }
        }
        /// <summary>
        /// 设置考核单位
        /// </summary>
        private void SaveUnit()
        {
            //bll
            TG.BLL.cm_KaoHeRenwuUnit bll = new BLL.cm_KaoHeRenwuUnit();
            List<TG.Model.cm_KaoHeRenwuUnit> list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeRenwuUnit>>(entityData);

            if (list.Count > 0)
            {
                list.Where(p => p != null).ToList().ForEach(c =>
                {
                    //是否存在
                    string strWhere = string.Format(" RenWuNo='{0}' AND UnitID={1} ", c.RenWuNo, c.UnitID);
                    var model = bll.GetModelList(strWhere);
                    if (model.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else//更新
                    {
                        c.ID = model[0].ID;
                        c.Statu = model[0].Statu;
                        c.IsIn = model[0].IsIn;
                        bll.Update(c);
                    }
                });

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 添加项
        /// </summary>
        public void AddItem()
        {
            //得到对象
            TG.Model.cm_KaoHeRenWu model = JsonConvert.DeserializeObject<TG.Model.cm_KaoHeRenWu>(entityData);

            if (model != null)
            {
                TG.BLL.cm_KaoHeRenWu bll = new BLL.cm_KaoHeRenWu();
                //是否存在
                string strWhere = string.Format(" RenWuNo='{0}'", model.RenWuNo);
                int exsitCount = bll.GetModelList(strWhere).Count;
                if (exsitCount == 0)
                {
                    int count = bll.Add(model);
                    if (count > 0)
                    {
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("3");
                }
            }
        }
        /// <summary>
        /// 编辑项
        /// </summary>
        public void EditItem()
        {
            //得到对象
            TG.Model.cm_KaoHeRenWu model = JsonConvert.DeserializeObject<TG.Model.cm_KaoHeRenWu>(entityData);

            if (model != null)
            {
                TG.BLL.cm_KaoHeRenWu bll = new BLL.cm_KaoHeRenWu();

                if (bll.Update(model))
                {
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        public void DelItem()
        {
            if (this.ID != null)
            {
                TG.BLL.cm_KaoHeRenWu bll = new BLL.cm_KaoHeRenWu();

                if (bll.Delete(int.Parse(ID)))
                {
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
        }
        /// <summary>
        /// 设定奖金
        /// </summary>
        public void GetJiangCount()
        {
            decimal allcount = 0;

            var model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(ID));

            if (model != null)
            {
                allcount = model.JianAllCount ?? 0;

                Response.Write(allcount);
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        public void SaveJiangCount()
        {
            TG.BLL.cm_KaoHeRenWu bll = new BLL.cm_KaoHeRenWu();
            var model = bll.GetModel(int.Parse(ID));

            if (model != null)
            {
                model.JianAllCount = this.JiangCount;

                if (bll.Update(model))
                {
                    Response.Write(1);
                }
                else
                {
                    Response.Write(1);
                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}