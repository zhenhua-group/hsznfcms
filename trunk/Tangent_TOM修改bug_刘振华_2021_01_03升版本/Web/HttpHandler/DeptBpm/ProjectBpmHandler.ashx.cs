﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using TG.Model;

namespace TG.Web.HttpHandler.DeptBpm
{
    /// <summary>
    /// ProjectBpmHandler 的摘要说明
    /// </summary>
    public class ProjectBpmHandler : IHttpHandler
    {
        public HttpRequest Request
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }
        public HttpResponse Response
        {
            get
            {
                return HttpContext.Current.Response;
            }
        }
        /// <summary>
        /// 动作
        /// </summary>
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }

        /// <summary>
        /// 数据实体
        /// </summary>
        public string entityData
        {
            get
            {
                return Request["data"] ?? "";
            }
        }

        /// <summary>
        /// 部门ID
        /// </summary>
        public int UnitID
        {
            get
            {
                int uid = 0;
                int.TryParse(Request["uid"] ?? "0", out uid);
                return uid;
            }
        }
        /// <summary>
        /// 部门考核ID
        /// </summary>
        public int KaoHeUnitID
        {
            get
            {
                int kuniid = 0;
                int.TryParse(Request["ukaoheid"] ?? "0", out kuniid);
                return kuniid;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public int RenwuID
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["renwuid"] ?? "0", out renwuid);
                return renwuid;
            }
        }
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProjID
        {

            get
            {
                int projid = 0;
                int.TryParse(Request["projid"] ?? "0", out projid);
                return projid;
            }
        }
        /// <summary>
        /// 项目考核ID
        /// </summary>
        public int KaoHeProjID
        {
            get
            {
                int khproid = 0;
                int.TryParse(Request["khproid"] ?? "0", out khproid);
                return khproid;
            }
        }
        /// <summary>
        /// 考核名称ID
        /// </summary>
        public int ProKHNameID
        {
            get
            {
                int pronameid = 0;
                int.TryParse(Request["id"] ?? "0", out pronameid);
                return pronameid;
            }
        }
        public int KHNameID
        {
            get
            {
                int pronameid = 0;
                int.TryParse(Request["khnameid"] ?? "0", out pronameid);
                return pronameid;
            }
        }
        /// <summary>
        /// 考核类型ID
        /// </summary>
        public int ProKHTypeID
        {
            get
            {
                int pronameid = 0;
                int.TryParse(Request["id"] ?? "0", out pronameid);
                return pronameid;
            }
        }
        /// <summary>
        /// 发送消息的人
        /// </summary>
        public int SendUserID
        {
            get
            {
                int senderid = 0;
                int.TryParse(Request["senderid"] ?? "0", out senderid);
                return senderid;
            }
        }

        public int KHtypeID
        {
            get
            {
                int KHtypeid = 0;
                int.TryParse(Request["KHtypeid"] ?? "0", out KHtypeid);
                return KHtypeid;
            }
        }

        public int SpeID
        {
            get
            {
                int speid = 0;
                int.TryParse(Request["speid"] ?? "0", out speid);
                return speid;
            }
        }
        /// <summary>
        /// 绩效
        /// </summary>
        public decimal Jixiao
        {
            get
            {
                decimal jx = 0;
                decimal.TryParse(Request["jixiao"] ?? "0", out jx);
                return jx;
            }
        }
        /// <summary>
        /// 部门ID
        /// </summary>
        public int UnitSysNo
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["unitid"] ?? "0", out unitid);
                return unitid;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Setup
        {
            get
            {
                int id = 0;
                int.TryParse(Request["setup"] ?? "0", out id);
                return id;
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            switch (Action)
            {
                case "saveproj"://设置考核项目
                    SaveProj();
                    break;
                case "getproj"://获取考核项目
                    GetProj();
                    break;
                case "savekhname"://保存考核名称
                    SaveKHName();
                    break;
                case "getkhname"://获取考核名称列表
                    GetKHName();
                    break;
                case "getproname"://获取单个考核名称
                    GetOneKaoheName();
                    break;
                case "delproname"://删除考核名称
                    DeleteProName();
                    break;
                case "gettypelist"://获取考核类型
                    GetTypeList();
                    break;
                case "savetype"://保存考核类型
                    SaveKHType();
                    break;
                case "gettypeone"://编辑获取
                    GetTypeModel();
                    break;
                case "deltype"://删除类型
                    DeleteType();
                    break;
                case "savespecmems":
                    SaveSpecMems();
                    break;
                case "savespecmemsfz":
                    SaveSpecFzMems();
                    break;
                case "getspecmems":
                    GetSpecMems();
                    break;
                case "getspecmemsfz":
                    GetSpecMemsFz();
                    break;
                case "getspecmems2":
                    GetSpecMems2();
                    break;
                case "getspecmems3":
                    GetSpecMemsFzAndMng();
                    break;
                case "savespeprst":
                    SaveSpePersent();
                    break;
                case "savespeprst2":
                    SaveSpePersent2();
                    break;
                case "savespeprst3":
                    SaveSpePersent3();
                    break;
                case "savememsprt":
                    SaveMemsPrt();
                    break;
                case "startpro":
                    StartProKH();
                    break;
                case "startpromsg":
                    SendMsgToSpeMng();
                    break;
                case "gethiscomplete":
                    GetHisComplete();
                    break;
                case "delsetproj":
                    CancelKaoHeProj();
                    break;
              case "yang":
                 yang();
                 break;
            }
        }
        /// <summary>
        /// 取消已设置项目考核
        /// </summary>
        private void CancelKaoHeProj()
        {
            //判断是否已经设置了项目考核
            var bll = new TG.BLL.cm_KaoHeRenwuProj();
            var bllName = new TG.BLL.cm_KaoHeProjName();

            string strWhere = string.Format(" RenWuId={0} AND proID={1}", this.RenwuID, this.ProjID);
            int count = bll.GetRecordCount(strWhere);

            if (count > 0)
            {
                //删除已经设置的考核
                var list = bll.GetModelList(strWhere);
                //项目考核ID
                int kaoheprojid = list[0].ID;
                //查询是否已经添加了考核名称
                strWhere = string.Format(" KaoHeProjId={0} ", kaoheprojid);
                int countName = bllName.GetRecordCount(strWhere);

                if (countName == 0)
                {
                    //是否已经删除成功
                    bool affectRow = bll.Delete(kaoheprojid);

                    if (affectRow)
                    {
                        Response.Write("1");
                    }
                }
                else
                {
                    Response.Write("2");
                }
            }
        }

        private void GetHisComplete()
        {
            ////得到考核任务列表
            //var renwuList = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderBy(c => c.ID).ToList();
            ////当前考核任务
            //int index = renwuList.FindIndex(c => c.ID == this.RenwuID);
            ////查询上一考核任务
            //var prevRenwuModel = renwuList[index];
            //if (index - 1 > -1)
            //{
            //    prevRenwuModel = renwuList[index - 1];
            //}

            ////获取上一考核任务ID
            //int prevRenwuID = prevRenwuModel.ID;
            ////查询项目考核ID
            //string strWhere = string.Format(" RenWuId={0} AND proID={1}", prevRenwuID, this.ProjID);
            //var kaoheProjList = new TG.BLL.cm_KaoHeRenwuProj().GetModelList(strWhere);

            //var namelist = new TG.BLL.cm_KaoHeProjName().GetModelList(" proID = " + this.ProjID + "");

            //var query = from n in namelist
            //            where (from p in kaoheProjList select p.ID).Contains(n.KaoHeProjId)
            //            select n;

            //var result = query.Sum(c => c.DoneScale);

            string strWhere = string.Format(" RenWuId={0} AND proID={1}", this.RenwuID, this.ProjID);
            var kaoheProjList = new TG.BLL.cm_KaoHeRenwuProj().GetModelList(strWhere);

            var namelist = new TG.BLL.cm_KaoHeProjName().GetModelList(" proID = " + this.ProjID + "");

            var query = from n in namelist
                        where (from p in kaoheProjList select p.ID).Contains(n.KaoHeProjId)
                        select n;

            var result = query.Sum(c => c.DoneScale);

            Response.Write(result);
        }
        /// <summary>
        /// 发送消息
        /// </summary>
        private void SendMsgToSpeMng()
        {
            var bll = new TG.BLL.cm_KaoheSpecMems();
            //发送消息给主持人 -2 主持人
            string strWhere = string.Format(" ProId={0} AND ProKHId={1} AND ProKHNameId={2} AND SpeID=-2 ", this.ProjID, this.KaoHeProjID, this.ProKHNameID);
            var listZhuchi = bll.GetModelList(strWhere);
            if (listZhuchi.Count > 0)
            {
                var zhuChiModel = listZhuchi[0];
                if (zhuChiModel != null)
                {
                    //发给主持人发消息
                    SendMsgToUser(zhuChiModel);
                }
            }

            //发消息给负责人
            var bllFz = new TG.BLL.cm_KaoheSpecMemsFz();
            //发消息给专业负责人
            strWhere = string.Format(" ProId={0} AND ProKHId={1} AND ProKHNameId={2} ", this.ProjID, this.KaoHeProjID, this.ProKHNameID);
            var listFuze = bllFz.GetModelList(strWhere);
            if (listFuze.Count > 0)
            {
                listFuze.ForEach(c =>
                {
                    //发消息给负责人
                    SendMsgToUser(c);
                });
            }

            Response.Write("1");
        }
        /// <summary>
        /// 发起项目考核
        /// </summary>
        private void StartProKH()
        {
            var bll = new TG.BLL.cm_KaoHeProjName();

            var model = bll.GetModel(this.ProKHNameID);
            if (model != null)
            {
                //var mems = new TG.BLL.tg_member().GetModelList(" mem_isFired=0 ");
                var mems = new TG.BLL.tg_member().GetModelList(" mem_ID=1441 ");
                var memroles = new TG.BLL.tg_memberRole().GetModelList(" mem_ID=1441 AND RoleIdTec=7 AND RoleIdMag=5");
                //查询张翠珍
                var query = from m in mems
                            join r in memroles on m.mem_ID equals r.mem_Id
                            where r.RoleIdTec == 7 && r.RoleIdMag == 5
                            select m;

                var userlist = query.Distinct().ToList();
                if (userlist.Count == 0)
                {
                    Response.Write("2");
                    return;
                }
                else
                {
                    model.Stat = 1;
                    if (bll.Update(model))
                    {
                        //发送消息
                        var bllmsg = new TG.BLL.cm_SysMsg();
                        //查询任务
                        string renwuName = "";
                        var renwuProjModel = new TG.BLL.cm_KaoHeRenwuProj().GetModel(this.KaoHeProjID);
                        if (renwuProjModel != null)
                        {
                            //查询项目考核设置
                            var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(renwuProjModel.RenWuId);
                            if (renwuModel != null)
                            {
                                renwuName = renwuModel.RenWuNo;
                            }
                        }
                        //项目
                        var proModel = new TG.BLL.cm_Project().GetModel(this.ProjID);
                        string proname = "";
                        if (proModel != null)
                        {
                            proname = proModel.pro_name;
                        }
                        //考核名称
                        string kaoheName = "";
                        var kaoheModel = new TG.BLL.cm_KaoHeProjName().GetModel(this.ProKHNameID);
                        if (kaoheModel != null)
                        {
                            kaoheName = kaoheModel.KName;
                        }

                        string msgType = "项目系数调整";
                        userlist.ForEach(m =>
                        {//实例消息实体
                            var sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("proid={0}&prokhid={1}&khnameid={2}&action=1", this.ProjID, this.KaoHeProjID, this.ProKHNameID),
                                FromUser = m.mem_ID,
                                InUser = this.SendUserID,
                                MsgType = 34,//项目比例设置
                                MessageContent = string.Format("关于\"{0}\"{2}考核{3}的{1}！", proname, msgType, renwuName, kaoheName.Trim()),
                                QueryCondition = proname + "的" + msgType,
                                Status = "A",
                                IsDone = "D",
                                ToRole = "0"
                            };

                            //发消息  
                            //2016年12月27日 注销  不需要再给张翠珍发消息
                            //int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
                        });

                    }
                }

                Response.Write("1");
            }
        }
        /// <summary>
        /// 保存用户分配比例
        /// </summary>
        private void SaveMemsPrt()
        {
            //项目参与人员
            TG.BLL.cm_KaoHeSpeMemsPersent bll = new BLL.cm_KaoHeSpeMemsPersent();
            //项目分配人员
            TG.BLL.cm_KaoHeMemsAllot bllAllot = new BLL.cm_KaoHeMemsAllot();
            //部门经理设置
            TG.BLL.cm_KaoheSpecMems bllUnitMng = new BLL.cm_KaoheSpecMems();

            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeSpeMemsPersent>>(entityData);

            if (list.Count > 0)
            {
                //获取第一个实体
                var modelOne = list[0];
                //查询老的数据
                string strWhereOld = string.Format(" KaoHeNameID={0} AND SpeID={1} ", modelOne.KaoHeNameID, modelOne.SpeID);
                var listold = bll.GetModelList(strWhereOld);
                #region 提交时判断是否有部门经理
                if (modelOne.Stat == 1)
                {
                    TG.BLL.tg_member bllmem = new BLL.tg_member();
                    TG.BLL.tg_memberRole bllrole = new BLL.tg_memberRole();
                    if (this.Setup == 1)
                    {
                        //查询部门,根据本人的部门
                        //var mem = bllmem.GetModel(modelOne.MemID);

                        //如果负责人为外所人会有BUG 2016-8-29 qpl
                        //int unitID = this.UnitSysNo;
                        //if (unitID == 0)
                        //{
                        //    var mem = bllmem.GetModel(modelOne.MemID);
                        //    unitID = mem.mem_Unit_ID;
                        //}


                        //人员列表 2016年7月19日 修改
                        //var memlist = bllmem.GetModelList(" mem_Unit_ID=" + mem.mem_Unit_ID + "");

                        //角色表
                        //var rolelist = bllrole.GetModelList(" mem_Unit_ID=" + unitID + " ");

                        //人员列表
                        var memlist = bllmem.GetModelList(" mem_isFired=0 ");
                        //根据设置的部门经理发消息
                        string strWhere = string.Format(" ProId={0} AND ProKHId={1} AND ProKHNameId={2} AND SpeID={3} ", modelOne.ProID, modelOne.ProKHID, modelOne.KaoHeNameID, modelOne.SpeID);
                        var memMnglist = bllUnitMng.GetModelList(strWhere);

                        var query = from m in memlist
                                    join r in memMnglist on m.mem_ID equals r.MemID
                                    select m;

                        var userlist = query.ToList();
                        if (userlist.Count == 0)
                        {
                            Response.Write("2");
                            return;
                        }
                        else//发消息
                        {
                            userlist.ForEach(c =>
                            {
                                //发消息给部门经理
                                SendMsgToUser(list[0], c);
                            });
                        }
                    }
                    else if (this.Setup == 2)
                    {
                        //如果是建筑专业
                        if (modelOne.SpeID == 1)
                        {
                            //张翠珍为总建筑师
                            var memlist = bllmem.GetModelList(" mem_isFired=0 AND mem_ID=1441 ");
                            //角色表
                            var rolelist = bllrole.GetModelList(" RoleIdTec=7 ");

                            //var query = from m in memlist
                            //            join r in rolelist on m.mem_ID equals r.mem_Id
                            //            where r.RoleIdTec == 7
                            //            select m;

                            var query = from m in memlist
                                        where (from r in rolelist select r.mem_Id).Contains(m.mem_ID)
                                        select m;

                            var userlist = query.ToList();
                            if (userlist.Count == 0)
                            {
                                Response.Write("3");
                                return;
                            }
                            else//发消息
                            {
                                userlist.ForEach(c =>
                                {
                                    //发消息给总建筑师
                                    SendMsgToUser(list[0], c);
                                });
                            }
                        }
                    }
                }
                #endregion

                #region 更新专业对应主持人的绩效
                if (modelOne.SpeID > 1)
                {
                    //查询专业对应的实体
                    string strWhere = string.Format(" ProKHNameId={0} AND SpeID={1} ", modelOne.KaoHeNameID, modelOne.SpeID);

                    var modellist = new TG.BLL.cm_KaoheSpecMems().GetModelList(strWhere);
                    if (modellist.Count > 0)
                    {
                        var model = modellist[0];

                        model.Jixiao = this.Jixiao;
                        //更新
                        new TG.BLL.cm_KaoheSpecMems().Update(model);
                    }
                }
                #endregion

                //插入保存的专业人员信息
                if (listold.Count == 0)
                {
                    list.ForEach(c =>
                    {
                        string strWhere = string.Format(" KaoHeNameID={0} AND SpeID={1} AND MemID={2} ", c.KaoHeNameID, c.SpeID, c.MemID);
                        var modellist = bll.GetModelList(strWhere);

                        if (modellist.Count == 0)
                        {
                            bll.Add(c);
                        }
                        else
                        {
                            var model = modellist[0];
                            c.ID = model.ID;
                            //部门经理编辑
                            if (Setup == 2)
                            {
                                c.InsertUser2 = c.InsertUser1;
                                c.InsertUser1 = model.InsertUser1;
                                c.Stat2 = model.Stat;
                                c.Stat = model.Stat;

                                c.ProID = model.ProID;
                                c.ProKHID = model.ProKHID;
                                c.KaoHeNameID = model.KaoHeNameID;
                                c.MemID = model.MemID;
                                c.MemName = model.MemName;
                                c.SpeID = model.SpeID;
                                c.SpeName = model.SpeName;
                                c.InsertDate2 = DateTime.Now;
                            }
                            else if (Setup == 3)
                            {
                                c.InsertUser3 = c.InsertUser1;
                                c.InsertUser1 = model.InsertUser1;
                                c.Stat3 = model.Stat;
                                c.Stat2 = model.Stat2;
                                c.Stat = model.Stat;

                                c.ProID = model.ProID;
                                c.ProKHID = model.ProKHID;
                                c.KaoHeNameID = model.KaoHeNameID;
                                c.MemID = model.MemID;
                                c.MemName = model.MemName;
                                c.SpeID = model.SpeID;
                                c.SpeName = model.SpeName;
                                c.InsertDate3 = DateTime.Now;
                            }

                            bll.Update(c);
                        }
                    });
                    Response.Write("1");
                }
                else
                {
                    //查询老的数据是否在新更新的数据中
                    foreach (TG.Model.cm_KaoHeSpeMemsPersent mem in listold)
                    {
                        var mm = list.Where(m => m.SpeID == mem.SpeID && m.KaoHeNameID == mem.KaoHeNameID && m.MemID == mem.MemID).ToList();
                        //如果在新的里面找不到
                        if (mm.Count == 0)
                        {
                            //删除
                            new TG.BLL.cm_KaoHeSpeMemsPersent().Delete(mem.ID);
                        }
                    }

                    //添加新加的记录
                    list.ForEach(c =>
                    {
                        string strWhere = string.Format(" KaoHeNameID={0} AND SpeID={1} AND MemID={2} ", c.KaoHeNameID, c.SpeID, c.MemID);
                        var modellist = bll.GetModelList(strWhere);

                        if (modellist.Count == 0)
                        {
                            if (this.Setup == 2)
                            {
                                //如果是部门经理加的人员，默认第一个阶段为1
                                c.InsertUser2 = c.InsertUser1;
                                //第二阶段的状态等于提交数据
                                c.Stat2 = c.Stat;
                                //第一阶段状态始终提交
                                c.Stat = 1;
                            }
                            else if (this.Setup == 3)
                            {
                                //如果是部门经理加的人员，默认第一个阶段为1
                                c.InsertUser2 = c.InsertUser1;
                                c.InsertUser3 = c.InsertUser1;
                                //第二阶段的状态等于提交数据
                                c.Stat3 = c.Stat;
                                //第一阶段状态始终提交
                                c.Stat = 1;
                                c.Stat2 = 1;
                            }
                            //添加自增记录
                            bll.Add(c);
                        }
                        else
                        {
                            //如果此人已经存在  更新
                            if (modellist.Count == 1)
                            {

                                var model = modellist[0];
                                c.ID = model.ID;
                                //部门经理编辑
                                if (Setup == 2)
                                {
                                    c.InsertUser2 = c.InsertUser1;
                                    c.InsertUser1 = model.InsertUser1;
                                    c.Stat2 = c.Stat;
                                    c.Stat = model.Stat;

                                    c.ProID = model.ProID;
                                    c.ProKHID = model.ProKHID;
                                    c.KaoHeNameID = model.KaoHeNameID;
                                    c.MemID = model.MemID;
                                    c.MemName = model.MemName;
                                    c.SpeID = model.SpeID;
                                    c.SpeName = model.SpeName;
                                    c.InsertDate2 = DateTime.Now;
                                }
                                else if (Setup == 3)
                                {
                                    c.InsertUser3 = c.InsertUser1;
                                    c.InsertUser1 = model.InsertUser1;
                                    c.Stat3 = c.Stat;
                                    c.Stat2 = model.Stat2;
                                    c.Stat = model.Stat;

                                    c.ProID = model.ProID;
                                    c.ProKHID = model.ProKHID;
                                    c.KaoHeNameID = model.KaoHeNameID;
                                    c.MemID = model.MemID;
                                    c.MemName = model.MemName;
                                    c.SpeID = model.SpeID;
                                    c.SpeName = model.SpeName;
                                    c.InsertUser2 = model.InsertUser2;
                                    c.InsertDate2 = model.InsertDate2;
                                    c.InsertDate3 = DateTime.Now;
                                }

                                bll.Update(c);
                            }
                            else
                            {
                                //2017年1月17日 
                                //保留最近一个记录 ，删除其他重复记录
                                int i = 0;
                                modellist.OrderBy(a => a.ID).ToList().ForEach(a =>
                                {

                                    if (i == 0)
                                    {
                                        var model = modellist[0];
                                        c.ID = model.ID;
                                        //部门经理编辑
                                        if (Setup == 2)
                                        {
                                            c.InsertUser2 = c.InsertUser1;
                                            c.InsertUser1 = model.InsertUser1;
                                            c.Stat2 = c.Stat;
                                            c.Stat = model.Stat;

                                            c.ProID = model.ProID;
                                            c.ProKHID = model.ProKHID;
                                            c.KaoHeNameID = model.KaoHeNameID;
                                            c.MemID = model.MemID;
                                            c.MemName = model.MemName;
                                            c.SpeID = model.SpeID;
                                            c.SpeName = model.SpeName;
                                            c.InsertDate2 = DateTime.Now;
                                        }
                                        else if (Setup == 3)
                                        {
                                            c.InsertUser3 = c.InsertUser1;
                                            c.InsertUser1 = model.InsertUser1;
                                            c.Stat3 = c.Stat;
                                            c.Stat2 = model.Stat2;
                                            c.Stat = model.Stat;

                                            c.ProID = model.ProID;
                                            c.ProKHID = model.ProKHID;
                                            c.KaoHeNameID = model.KaoHeNameID;
                                            c.MemID = model.MemID;
                                            c.MemName = model.MemName;
                                            c.SpeID = model.SpeID;
                                            c.SpeName = model.SpeName;
                                            c.InsertUser2 = model.InsertUser2;
                                            c.InsertDate2 = model.InsertDate2;
                                            c.InsertDate3 = DateTime.Now;
                                        }

                                        bll.Update(c);
                                    }
                                    else
                                    {
                                        //删除 
                                        bll.Delete(a.ID);
                                    }

                                    i++;
                                });
                            }
                        }
                    });

                    Response.Write("1");
                }
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存总建筑师项目比例
        /// </summary>
        private void SaveSpePersent2()
        {
            TG.BLL.cm_KaoHeSpePersent bll = new BLL.cm_KaoHeSpePersent();

            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeSpePersent>>(entityData);

            if (list.Count > 0)
            {
                int hknameid = 0;
                list.ForEach(c =>
                {
                    string strWhere = string.Format(" ProKHNameID={0} AND SpeID={1} ", c.ProKHNameID, c.SpeID);
                    var modellist = bll.GetModelList(strWhere);

                    if (modellist.Count > 0)
                    {
                        //c.ID = modellist[0].ID;
                        //c.InserUserID1 = modellist[0].InserUserID1;
                        //c.InserDate1 = modellist[0].InserDate1;
                        //c.InserDate2 = DateTime.Now;
                        //c.Stat = modellist[0].Stat;
                        ////更新
                        //bll.Update(c);


                        var model = modellist[0];

                        model.Persent2 = c.Persent2;
                        model.InserUserID2 = c.InserUserID2;
                        model.InserDate2 = DateTime.Now;
                        model.Stat2 = c.Stat2;
                        //更新
                        bll.Update(model);
                    }

                    hknameid = c.ProKHNameID;
                });
                //设置绩效
                string where = string.Format(" ProKHNameID={0} AND SpeID=-2 ", hknameid);
                var memlist = new TG.BLL.cm_KaoheSpecMems().GetModelList(where);

                if (memlist.Count > 0)
                {
                    var model = memlist[0];
                    //更新绩效
                    model.Jixiao = this.Jixiao;

                    new TG.BLL.cm_KaoheSpecMems().Update(model);
                }
                //项目经理评价主持人绩效
                where = string.Format(" ProKHNameID={0} AND SpeID=-1 ", hknameid);
                memlist = new TG.BLL.cm_KaoheSpecMems().GetModelList(where);

                if (memlist.Count > 0)
                {
                    var model = memlist[0];
                    //更新绩效
                    model.Jixiao = this.Jixiao;

                    new TG.BLL.cm_KaoheSpecMems().Update(model);
                }
                //更新“建筑”专业的绩效
                where = string.Format(" ProKHNameID={0} AND SpeID=1 ", hknameid);
                memlist = new TG.BLL.cm_KaoheSpecMems().GetModelList(where);

                if (memlist.Count > 0)
                {
                    var model = memlist[0];
                    //更新绩效
                    model.Jixiao = this.Jixiao;

                    new TG.BLL.cm_KaoheSpecMems().Update(model);
                }

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存主持人分配比例
        /// </summary>
        private void SaveSpePersent()
        {
            TG.BLL.cm_KaoHeSpePersent bll = new BLL.cm_KaoHeSpePersent();
            TG.BLL.cm_KaoheSpecMems bllmems = new BLL.cm_KaoheSpecMems();
            TG.BLL.tg_member bllMem = new BLL.tg_member();
            TG.BLL.tg_memberRole bllRole = new BLL.tg_memberRole();

            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeSpePersent>>(entityData);

            if (list.Count > 0)
            {
                int? stat = 0;
                list.ForEach(c =>
                {
                    string strWhere = string.Format(" ProKHNameID={0} AND SpeID={1} ", c.ProKHNameID, c.SpeID);
                    var modellist = bll.GetModelList(strWhere);

                    if (modellist.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        var model = modellist[0];

                        model.Persent1 = c.Persent1;
                        model.InserUserID1 = c.InserUserID1;
                        model.InserDate1 = DateTime.Now;
                        model.Stat = c.Stat;

                        bll.Update(model);
                    }
                    stat = c.Stat;
                });
                //给总建筑是发送消息
                //改为给 主持人部门的部门经理发消息 2017年4月19日
                if (stat == 1)
                {
                    //TG.BLL.cm_KaoheSpecMems bllmems = new BLL.cm_KaoheSpecMems();

                    //string strWhere = string.Format(" ProKHNameId={0} AND SpeID=-1 ", this.KHNameID);
                    //var mems = bllmems.GetModelList(strWhere);

                    //if (mems.Count > 0)
                    //{
                    //    mems.ForEach(c =>
                    //    {
                    //        //发消息给总建筑师
                    //        SendMsgToUser(c);
                    //    });
                    //}

                    //查询主持人所在部门的部门经理
                    var zhuChiModel = bllMem.GetModel(list[0].InserUserID1);
                    if (zhuChiModel != null)
                    {
                        //查询部门经理
                        string strWhere = string.Format(" RoleIdMag=5 AND mem_Unit_ID={0}", zhuChiModel.mem_Unit_ID);
                        var mems = bllRole.GetModelList(strWhere);

                        //获取人员设置的总建筑师
                        strWhere = string.Format(" ProKHNameId={0} AND SpeID=-1 ", this.KHNameID);
                        var specModel = bllmems.GetModelList(strWhere)[0];
                        if (mems.Count > 0 && specModel != null)
                        {
                            mems.ForEach(c =>
                            {
                                specModel.MemID = c.mem_Id;
                                specModel.SpeID = -3;
                                //发消息给 部门经理
                                SendMsgToUser(specModel);
                            });
                        }
                    }
                }
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存部门经理分配
        /// </summary>
        private void SaveSpePersent3()
        {
            TG.BLL.cm_KaoHeSpePersent bll = new BLL.cm_KaoHeSpePersent();

            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeSpePersent>>(entityData);

            if (list.Count > 0)
            {
                int? stat = 0;
                int hknameid = 0;
                list.ForEach(c =>
                {
                    string strWhere = string.Format(" ProKHNameID={0} AND SpeID={1} ", c.ProKHNameID, c.SpeID);
                    var modellist = bll.GetModelList(strWhere);

                    if (modellist.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        var model = modellist[0];

                        model.Persent3 = c.Persent3;
                        model.InserUserID3 = c.InserUserID3;
                        model.InserDate3 = DateTime.Now;
                        model.Stat3 = c.Stat3;

                        bll.Update(model);
                    }
                    stat = c.Stat3;

                    hknameid = c.ProKHNameID;
                });

                //设置绩效  2017年5月24日
                string where = string.Format(" ProKHNameID={0} AND SpeID=-2 ", hknameid);
                var memlist = new TG.BLL.cm_KaoheSpecMems().GetModelList(where);

                if (memlist.Count > 0)
                {
                    var model = memlist[0];
                    //更新绩效
                    model.Jixiao = this.Jixiao;

                    new TG.BLL.cm_KaoheSpecMems().Update(model);
                }

                //给总建筑是发送消息
                if (stat == 1)
                {
                    TG.BLL.cm_KaoheSpecMems bllmems = new BLL.cm_KaoheSpecMems();

                    string strWhere = string.Format(" ProKHNameId={0} AND SpeID=-1 ", this.KHNameID);
                    var mems = bllmems.GetModelList(strWhere);

                    if (mems.Count > 0)
                    {
                        mems.ForEach(c =>
                        {
                            //发消息给总建筑师
                            SendMsgToUser(c);
                        });
                    }
                }
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 获取专业设置人员
        /// </summary>
        private void GetSpecMems()
        {
            TG.BLL.cm_KaoheSpecMems bll = new BLL.cm_KaoheSpecMems();

            string strWhere = string.Format(" KHTypeId={0} ", this.ProKHTypeID);

            //考核类型
            int typeCount = GetSpecTypeByName(this.ProKHTypeID);
            //未设置类型
            if (typeCount == 3)
            {
                Response.Write("3");
                return;
            }

            var list = bll.GetModelList(strWhere);
            if (list.Count > 0)
            {
                var modellist = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoheSpecMems>>(entityData);
                TG.Model.cm_KaoheSpecMems modelOne = modellist[0];

                #region 判断不同的项目类型是部门经理是否可以添加编辑 qpl 2016年8月12日 后期需改回

                //if (typeCount == 1)
                //{
                //    //实例虚拟部门负责人
                //    var model = new TG.Model.cm_KaoheSpecMems();
                //    model.ProId = modelOne.ProId;
                //    model.ProKHId = modelOne.ProKHId;
                //    model.ProKHNameId = modelOne.ProKHNameId;
                //    model.KHTypeId = modelOne.KHTypeId;
                //    //结构部门经理
                //    model.MemID = -1;
                //    model.MemName = "";
                //    //专业
                //    model.SpeID = 1;
                //    model.SpeName = "建筑";

                //    list.Add(model);
                //}

                //if (typeCount == 1)
                //{
                //    //实例虚拟部门负责人
                //    var model = new TG.Model.cm_KaoheSpecMems();
                //    model.ProId = modelOne.ProId;
                //    model.ProKHId = modelOne.ProKHId;
                //    model.ProKHNameId = modelOne.ProKHNameId;
                //    model.KHTypeId = modelOne.KHTypeId;
                //    //结构部门经理
                //    model.MemID = -1;
                //    model.MemName = "";
                //    //专业
                //    model.SpeID = 2;
                //    model.SpeName = "结构";

                //    list.Add(model);
                //}

                //if (typeCount == 1 || typeCount == 2)
                //{
                //    //实例虚拟部门负责人
                //    var model = new TG.Model.cm_KaoheSpecMems();
                //    model.ProId = modelOne.ProId;
                //    model.ProKHId = modelOne.ProKHId;
                //    model.ProKHNameId = modelOne.ProKHNameId;
                //    model.KHTypeId = modelOne.KHTypeId;
                //    //结构部门经理
                //    model.MemID = -1;
                //    model.MemName = "";
                //    //专业
                //    model.SpeID = 3;
                //    model.SpeName = "暖通";

                //    list.Add(model);
                //}

                #endregion

                string jsonStr = JsonConvert.SerializeObject(list);
                Response.Write(jsonStr);
            }
            else//未保存部门经理
            {
                var modellist = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoheSpecMems>>(entityData);
                var specMems = new List<TG.Model.cm_KaoheSpecMems>();

                if (modellist.Count > 0)
                {
                    TG.Model.cm_KaoheSpecMems modelOne = modellist[0];
                    //项目经理
                    var projModel = new TG.BLL.cm_Project().GetModel(modellist[0].ProId);
                    if (projModel != null)
                    {
                        //实例虚拟部门负责人
                        var model = new TG.Model.cm_KaoheSpecMems();
                        model.ProId = modelOne.ProId;
                        model.ProKHId = modelOne.ProKHId;
                        model.ProKHNameId = modelOne.ProKHNameId;
                        model.KHTypeId = modelOne.KHTypeId;
                        //经理ID
                        model.MemID = projModel.PMUserID;
                        //经理名
                        model.MemName = projModel.PMName;
                        //专业
                        model.SpeID = -1;
                        model.SpeName = "项目经理";

                        specMems.Add(model);
                    }

                    var bllmem = new TG.BLL.tg_member();
                    var bllmemRole = new TG.BLL.tg_memberRole();

                    strWhere = string.Format("");
                    var mems = bllmem.GetModelList(strWhere);
                    strWhere = string.Format(" RoleIdMag=5 AND mem_Unit_ID=231 ");
                    var memRols = bllmemRole.GetModelList(strWhere);

                    var query = from m in mems
                                join r in memRols on m.mem_ID equals r.mem_Id
                                where r.RoleIdMag == 5
                                select m;

                    var userlist = query.ToList();
                    if (userlist.Count > 0)
                    {
                        if (typeCount == 1)
                        {
                            //实例虚拟部门负责人
                            var model = new TG.Model.cm_KaoheSpecMems();
                            model.ProId = modelOne.ProId;
                            model.ProKHId = modelOne.ProKHId;
                            model.ProKHNameId = modelOne.ProKHNameId;
                            model.KHTypeId = modelOne.KHTypeId;
                            //结构部门经理
                            model.MemID = -1;
                            model.MemName = "";
                            //专业
                            model.SpeID = 1;
                            model.SpeName = "建筑";

                            specMems.Add(model);
                        }
                        else
                        {
                            foreach (TG.Model.tg_member mem in userlist)
                            {
                                //实例虚拟部门负责人
                                var model = new TG.Model.cm_KaoheSpecMems();
                                model.ProId = modelOne.ProId;
                                model.ProKHId = modelOne.ProKHId;
                                model.ProKHNameId = modelOne.ProKHNameId;
                                model.KHTypeId = modelOne.KHTypeId;
                                //结构部门经理
                                model.MemID = mem.mem_ID;
                                model.MemName = mem.mem_Name;
                                //专业
                                model.SpeID = 1;
                                model.SpeName = "建筑";

                                specMems.Add(model);
                            }
                        }
                    }
                    //结构
                    strWhere = string.Format(" mem_Unit_ID=237 AND mem_Speciality_ID=2 ");
                    mems = bllmem.GetModelList(strWhere);
                    strWhere = string.Format(" RoleIdMag=5 ");
                    memRols = bllmemRole.GetModelList(strWhere);

                    query = from m in mems
                            join r in memRols on m.mem_ID equals r.mem_Id
                            where r.RoleIdMag == 5
                            select m;

                    userlist = query.ToList();
                    if (userlist.Count > 0)
                    {
                        if (typeCount == 1 || typeCount == 4)
                        {
                            //实例虚拟部门负责人
                            var model = new TG.Model.cm_KaoheSpecMems();
                            model.ProId = modelOne.ProId;
                            model.ProKHId = modelOne.ProKHId;
                            model.ProKHNameId = modelOne.ProKHNameId;
                            model.KHTypeId = modelOne.KHTypeId;
                            //结构部门经理
                            model.MemID = -1;
                            model.MemName = "";
                            //专业
                            model.SpeID = 2;
                            model.SpeName = "结构";

                            specMems.Add(model);
                        }
                        else
                        {
                            //实例虚拟部门负责人
                            var model = new TG.Model.cm_KaoheSpecMems();
                            model.ProId = modelOne.ProId;
                            model.ProKHId = modelOne.ProKHId;
                            model.ProKHNameId = modelOne.ProKHNameId;
                            model.KHTypeId = modelOne.KHTypeId;
                            //结构部门经理
                            model.MemID = userlist[0].mem_ID;
                            model.MemName = userlist[0].mem_Name;
                            //专业
                            model.SpeID = 2;
                            model.SpeName = "结构";

                            specMems.Add(model);
                        }
                    }

                    //暖通
                    strWhere = string.Format(" mem_Unit_ID=238 AND mem_Speciality_ID=3 ");
                    mems = bllmem.GetModelList(strWhere);
                    strWhere = string.Format(" RoleIdMag=5 ");
                    memRols = bllmemRole.GetModelList(strWhere);

                    query = from m in mems
                            join r in memRols on m.mem_ID equals r.mem_Id
                            where r.RoleIdMag == 5
                            select m;

                    userlist = query.ToList();
                    if (userlist.Count > 0)
                    {
                        if (typeCount == 1 || typeCount == 2)
                        {
                            //实例虚拟部门负责人
                            var model = new TG.Model.cm_KaoheSpecMems();
                            model.ProId = modelOne.ProId;
                            model.ProKHId = modelOne.ProKHId;
                            model.ProKHNameId = modelOne.ProKHNameId;
                            model.KHTypeId = modelOne.KHTypeId;
                            //结构部门经理
                            model.MemID = -1;
                            model.MemName = "";
                            //专业
                            model.SpeID = 3;
                            model.SpeName = "暖通";

                            specMems.Add(model);
                        }
                        else
                        {
                            //实例虚拟部门负责人
                            var model = new TG.Model.cm_KaoheSpecMems();
                            model.ProId = modelOne.ProId;
                            model.ProKHId = modelOne.ProKHId;
                            model.ProKHNameId = modelOne.ProKHNameId;
                            model.KHTypeId = modelOne.KHTypeId;
                            //结构部门经理
                            model.MemID = userlist[0].mem_ID;
                            model.MemName = userlist[0].mem_Name;
                            //专业
                            model.SpeID = 3;
                            model.SpeName = "暖通";

                            specMems.Add(model);
                        }
                    }


                    //给排水
                    strWhere = string.Format(" mem_Unit_ID=238 AND mem_Speciality_ID=3 ");
                    mems = bllmem.GetModelList(strWhere);
                    strWhere = string.Format(" RoleIdMag=5 ");
                    memRols = bllmemRole.GetModelList(strWhere);

                    query = from m in mems
                            join r in memRols on m.mem_ID equals r.mem_Id
                            where r.RoleIdMag == 5
                            select m;

                    userlist = query.ToList();
                    if (userlist.Count > 0)
                    {
                        //实例虚拟部门负责人
                        var model = new TG.Model.cm_KaoheSpecMems();
                        model.ProId = modelOne.ProId;
                        model.ProKHId = modelOne.ProKHId;
                        model.ProKHNameId = modelOne.ProKHNameId;
                        model.KHTypeId = modelOne.KHTypeId;
                        //结构部门经理
                        model.MemID = userlist[0].mem_ID;
                        model.MemName = userlist[0].mem_Name;
                        //专业
                        model.SpeID = 4;
                        model.SpeName = "给排水";

                        specMems.Add(model);
                    }

                    //电气
                    strWhere = string.Format(" mem_Unit_ID=239 AND mem_Speciality_ID=5 ");
                    mems = bllmem.GetModelList(strWhere);
                    strWhere = string.Format(" RoleIdMag=5 ");
                    memRols = bllmemRole.GetModelList(strWhere);

                    query = from m in mems
                            join r in memRols on m.mem_ID equals r.mem_Id
                            where r.RoleIdMag == 5
                            select m;

                    userlist = query.ToList();
                    if (userlist.Count > 0)
                    {
                        //实例虚拟部门负责人
                        var model = new TG.Model.cm_KaoheSpecMems();
                        model.ProId = modelOne.ProId;
                        model.ProKHId = modelOne.ProKHId;
                        model.ProKHNameId = modelOne.ProKHNameId;
                        model.KHTypeId = modelOne.KHTypeId;
                        //结构部门经理
                        model.MemID = userlist[0].mem_ID;
                        model.MemName = userlist[0].mem_Name;
                        //专业
                        model.SpeID = 5;
                        model.SpeName = "电气";

                        specMems.Add(model);
                    }

                    string jsonStr = JsonConvert.SerializeObject(specMems);
                    Response.Write(jsonStr);
                }
            }
        }
        /// <summary>
        /// 查询考核名称下类型
        /// </summary>
        /// <param name="nameid"></param>
        /// <returns></returns>
        private int GetSpecTypeByName(int nameid)
        {
            var bllType = new TG.BLL.cm_KaoheProjType();
            //室外工程
            string strWhere = string.Format(" KaoheNameID={0} AND KaoheTypeID=7", nameid);
            var swList = bllType.GetModelList(strWhere);
            //室外其他
            strWhere = string.Format(" KaoheNameID={0} AND KaoheTypeID<>7", nameid);
            var swqtList = bllType.GetModelList(strWhere);
            //景观工程
            strWhere = string.Format(" KaoheNameID={0} AND KaoheTypeID=9", nameid);
            var jgList = bllType.GetModelList(strWhere);
            //景观其他
            strWhere = string.Format(" KaoheNameID={0} AND KaoheTypeID<>9", nameid);
            var jgqtList = bllType.GetModelList(strWhere);
            //精装配合
            strWhere = string.Format(" KaoheNameID={0} AND KaoheTypeID=12", nameid);
            var jzList = bllType.GetModelList(strWhere);
            //精装其他
            strWhere = string.Format(" KaoheNameID={0} AND KaoheTypeID<>12", nameid);
            var jzqtList = bllType.GetModelList(strWhere);


            strWhere = string.Format(" KaoheNameID={0} ", nameid);
            var AllList = bllType.GetModelList(strWhere);

            int swCount = swList.Count;
            int swqtCount = swqtList.Count;

            int jgCount = jgList.Count;
            int jgqtCount = jgqtList.Count;

            int jzCount = jzList.Count;
            int jzqtCount = jzqtList.Count;

            if (swCount > 0 && swqtCount == 0)
            {
                return 1;
            }
            else if (jgCount > 0 && jgqtCount == 0)
            {
                return 2;
            }
            else if (jzCount > 0 && jzqtCount == 0)
            {
                return 4;
            }
            else if (AllList.Count == 0)
            {
                return 3;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 查询部门经理
        /// </summary>
        private void GetSpecMems2()
        {
            TG.BLL.cm_KaoheSpecMems bll = new BLL.cm_KaoheSpecMems();

            string strWhere = string.Format(" ProKHNameId={0} ", this.ProKHTypeID);

            var list = bll.GetModelList(strWhere);
            if (list.Count > 0)
            {
                string jsonStr = JsonConvert.SerializeObject(list);
                Response.Write(jsonStr);
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 查询专业负责人
        /// </summary>
        private void GetSpecMemsFz()
        {
            TG.BLL.cm_KaoheSpecMemsFz bll = new BLL.cm_KaoheSpecMemsFz();

            string strWhere = string.Format(" KHTypeId={0} ", this.ProKHTypeID);

            var list = bll.GetModelList(strWhere);
            if (list.Count > 0)
            {
                string jsonStr = JsonConvert.SerializeObject(list);
                Response.Write(jsonStr);
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 部门经理 and 负责人
        /// </summary>
        private void GetSpecMemsFzAndMng()
        {
            TG.BLL.cm_KaoheSpecMems bll = new BLL.cm_KaoheSpecMems();
            TG.BLL.cm_KaoheSpecMemsFz bllfz = new BLL.cm_KaoheSpecMemsFz();

            //经理和主持人
            string strWhere = string.Format(" ProKHNameId={0} ", this.ProKHTypeID);
            var list = bll.GetModelList(strWhere);
            if (list.Count > 0)
            {
                //负责人
                strWhere = string.Format(" KHTypeId={0} ", this.ProKHTypeID);
                var listfz = bllfz.GetModelList(strWhere);

                list.Where(c => c.SpeID < 0).ToList().ForEach(c =>
                {
                    var memfz = new TG.Model.cm_KaoheSpecMemsFz()
                    {
                        ID = c.ID,
                        ProId = c.ProId,
                        ProKHId = c.ProKHId,
                        ProKHNameId = c.ProKHNameId,
                        KHTypeId = c.KHTypeId,
                        MemID = c.MemID,
                        MemName = c.MemName,
                        SpeName = c.SpeName,
                        SpeID = c.SpeID,
                        Jixiao = c.Jixiao
                    };

                    listfz.Add(memfz);
                });

                string jsonStr = JsonConvert.SerializeObject(listfz);
                Response.Write(jsonStr);
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 增加与更新建筑负责人
        /// </summary>
        /// <param name="c"></param>
        private void AddOrUpdateArchFz(TG.Model.cm_KaoheSpecMems c)
        {
            if (c != null)
            {
                TG.BLL.cm_KaoheSpecMemsFz bllFz = new BLL.cm_KaoheSpecMemsFz();
                //是否已经设置建筑主持人
                string strWhere = string.Format(" KHTypeId={0} AND SpeID=1 ", c.KHTypeId, c.MemID);
                int count = bllFz.GetModelList(strWhere).Count;
                //未设置建筑专业负责人
                if (count == 0)
                {
                    //设置主持人为建筑专业负责人
                    var fzModel = new TG.Model.cm_KaoheSpecMemsFz()
                    {
                        ProId = c.ProId,
                        ProKHId = c.ProKHId,
                        ProKHNameId = c.ProKHNameId,
                        KHTypeId = c.KHTypeId,
                        MemID = c.MemID,
                        MemName = c.MemName,
                        SpeName = "建筑",//建筑专业负责人
                        SpeID = 1,
                        Jixiao = c.Jixiao,
                    };
                    //新建
                    bllFz.Add(fzModel);
                }
                else
                {
                    //如果已经设置了建筑专业的负责人暂时不做任何操作 2016-8-15 qpl

                    //var fzModel = bllFz.GetModelList(strWhere)[0];

                    //if (fzModel != null)
                    //{
                    //    fzModel.ProId = c.ProId;
                    //    fzModel.ProKHId = c.ProKHId;
                    //    fzModel.ProKHNameId = c.ProKHNameId;
                    //    fzModel.KHTypeId = c.KHTypeId;
                    //    fzModel.MemID = c.MemID;
                    //    fzModel.MemName = c.MemName;
                    //    fzModel.SpeName = "建筑";
                    //    fzModel.SpeID = 1;
                    //    fzModel.Jixiao = c.Jixiao;
                    //    //更新
                    //    bllFz.Update(fzModel);
                    //}
                }
            }
        }
        /// <summary>
        /// 保存项目经理
        /// </summary>
        private void SaveSpecMems()
        {
            TG.BLL.cm_KaoheSpecMems bll = new BLL.cm_KaoheSpecMems();
            TG.BLL.cm_KaoheSpecMemsFz bllFz = new BLL.cm_KaoheSpecMemsFz();
            TG.BLL.cm_KaoHeSpecMemsHis bllHis = new BLL.cm_KaoHeSpecMemsHis();

            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoheSpecMems>>(entityData);
          string ActionMems = "";
            //发送消息
            if (list.Count > 0)
            {
                //查询老的数据
                var listold = bll.GetModelList(" KHTypeId=" + this.KHtypeID + " ");
                //未保存部门经理
                if (listold.Count == 0)
                {
                    //保存部门经理
                    list.ForEach(c =>
                                    {
                                        string strWhere = string.Format("  KHTypeId={0} AND MemID={1} AND SpeID={2} ", c.KHTypeId, c.MemID, c.SpeID);
                                        int memscount = bll.GetModelList(strWhere).Count;
                                        if (memscount == 0)
                                        {
                                            //添加
                                            int id = bll.Add(c);
                                            if (id > 0)
                                            {
                                                c.ID = id;
                                                if (c.SpeID > 1)//除项目经理，项目主持人,建筑部门经理
                                                {
                                                    SendMsgToUser(c);
                                                }
                                                //设置主持人为建筑专业负责人
                                                if (c.SpeID == -2)
                                                {
                                                    //如果设置了建筑经理说明建筑专业参与
                                                    bool isArch = list.Exists(f => f.SpeID == 1);
                                                    if (isArch)
                                                    {
                                                        var fzModel = new TG.Model.cm_KaoheSpecMemsFz()
                                                        {
                                                            ProId = c.ProId,
                                                            ProKHId = c.ProKHId,
                                                            ProKHNameId = c.ProKHNameId,
                                                            KHTypeId = c.KHTypeId,
                                                            MemID = c.MemID,
                                                            MemName = c.MemName,
                                                            SpeName = "建筑",//建筑专业负责人
                                                            SpeID = 1,
                                                            Jixiao = c.Jixiao,
                                                        };

                                                        bllFz.Add(fzModel);
                                                    }
                                                }
                                            }
                                        }
                                        //历史记录本次操作人员 2016-8-24
                                        ActionMems += c.MemName + "|";

                                    });


                    //添加操作历史数据 2016-8-24
                    var hisModel = new TG.Model.cm_KaoHeSpecMemsHis()
                    {
                        ActionContent = ActionMems,
                        ActionUserID = this.SendUserID,
                        ProId = list[0].ProId,
                        ProKHId = list[0].ProKHId,
                        ProKHNameId = list[0].ProKHNameId,
                        KHTypeId = list[0].KHTypeId
                    };
                    bllHis.Add(hisModel);

                    Response.Write("1");
                }
                else//已保存项目经理
                {
                    //查询老的数据是否在新更新的数据中
                    foreach (TG.Model.cm_KaoheSpecMems mem in listold)
                    {
                        var mm = list.Where(m => m.SpeID == mem.SpeID && m.KHTypeId == mem.KHTypeId && m.MemID == mem.MemID).ToList();
                        //如果在新的里面找不到
                        if (mm.Count == 0)
                        {
                            //删除
                            new TG.BLL.cm_KaoheSpecMems().Delete(mem.ID);
                        }
                    }

                    //添加新加的人员
                    list.ForEach(c =>
                    {
                        string strWhere = string.Format("  KHTypeId={0} AND MemID={1} AND SpeID={2} ", c.KHTypeId, c.MemID, c.SpeID);
                        int memscount = bll.GetModelList(strWhere).Count;
                        if (memscount == 0)
                        {
                            int id = bll.Add(c);
                            if (id > 0)
                            {
                                c.ID = id;
                                if (c.SpeID > 1)//除项目经理，项目主持人
                                {
                                    SendMsgToUser(c);
                                }
                            }
                        }

                        //如果是主持人新加或变更的时候
                        if (c.SpeID == -2)
                        {
                            AddOrUpdateArchFz(c);
                        }

                        //历史记录本次操作人员 2016-8-24
                        ActionMems += c.MemName + "|";

                    });

                    //添加操作历史数据 2016-8-24
                    var hisModel = new TG.Model.cm_KaoHeSpecMemsHis()
                    {
                        ActionContent = ActionMems,
                        ActionUserID = this.SendUserID,
                        ProId = list[0].ProId,
                        ProKHId = list[0].ProKHId,
                        ProKHNameId = list[0].ProKHNameId,
                        KHTypeId = list[0].KHTypeId
                    };
                    bllHis.Add(hisModel);

                    Response.Write("1");
                }
            }
            else
            {
                Response.Write("0");
            }
        }


        /// <summary>
        /// 撤回
        /// </summary>
        private void yang()
        {
            TG.BLL.cm_KaoheSpecMems bll = new BLL.cm_KaoheSpecMems();
            TG.BLL.cm_KaoheSpecMemsFz bllFz = new BLL.cm_KaoheSpecMemsFz();
            TG.BLL.cm_KaoHeSpecMemsHis bllHis = new BLL.cm_KaoHeSpecMemsHis();

            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoheSpecMems>>(entityData);
 

            string ActionMems = "";
            //发送消息
            if (list.Count > 0)
            {
                //查询老的数据
                var listold = bll.GetModelList(" KHTypeId=" + this.KHtypeID + " ");
                //未保存部门经理
                if (listold.Count == 0)
                {
                    //保存部门经理
                    list.ForEach(c =>
                    {
                        string strWhere = string.Format("  KHTypeId={0} AND MemID={1} AND SpeID={2} ", c.KHTypeId, c.MemID, c.SpeID);
                        int memscount = bll.GetModelList(strWhere).Count;

                       
                        if (c.SpeID == -1)//总建筑师
                        {
                            yang2(c);
                        }
                        else
                        {
                            //发消息给负责人

                            //发消息给专业负责人
                            strWhere = string.Format(" ProId={0} AND ProKHId={1} AND ProKHNameId={2} ",list[0].ProId, list[0].ProKHId,list[0].ProKHNameId);
                            var listFuze = bllFz.GetModelList(strWhere);
                            if (listFuze.Count > 0)
                            {
                                listFuze.ForEach(s =>
                                {
                                    //发消息给负责人
                                    SendMsgToUser(s);
                                });
                            }
 
                        }
                                
                         //历史记录本次操作人员 2016-8-24
                        ActionMems += c.MemName + "|";

                    });
                 Response.Write("1");
                }
             }
            else
            {
                Response.Write("0");
            }
        }


        /// <summary>
        /// 给 
        /// </summary>
        /// <param name="mem"></param>
        private void yang2(TG.Model.cm_KaoheSpecMems c)
        {
            string msgType = "专业间比例设置";
             //查询任务
            string renwuName = "";
            var renwuProjModel = new TG.BLL.cm_KaoHeRenwuProj().GetModel(c.ProKHId);
            if (renwuProjModel != null)
            {
                var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(renwuProjModel.RenWuId);

                if (renwuModel != null)
                {
                    renwuName = renwuModel.RenWuNo;
                }
            }
            //查询考核名称 
            string kaoheName = "";
            var khNameModel = new TG.BLL.cm_KaoHeProjName().GetModel(c.ProKHNameId);//修改无法查询到考核名称的错误 2016-8-15
            if (khNameModel != null)
            {
                kaoheName = khNameModel.KName;
            }
            //查询项目
            var proModel = new TG.BLL.cm_Project().GetModel(c.ProId);
            //发送消息
            var bllmsg = new TG.BLL.cm_SysMsg();
            //如果是工程主持人
            if (c.SpeID == -1)//2017年4月19日 修改一个部门经理审批
            {
                //实例消息实体
                var sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("proid={0}&prokhid={1}&khnameid={2}&khtypeid={3}&id={4}&speid={5}&setup=1", c.ProId, c.ProKHId, c.ProKHNameId, c.KHTypeId, c.ID, c.SpeID),
                    FromUser = c.MemID,
                    InUser = this.SendUserID,
                    MsgType = 34,//项目比例设置
                    //MessageContent = string.Format("关于\"{0}\"{2}考核{3}的{1}！", proModel.pro_name, msgType, renwuName, kaoheName),
                    MessageContent = string.Format("关于\"{0}\"{1}的{2}！", kaoheName, renwuName, msgType),
                    QueryCondition = proModel.pro_name + "的" + msgType,
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };
                //发消息
                int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
            }
          
        }



        /// <summary>
        /// 保存专业负责人
        /// </summary>
        private void SaveSpecFzMems()
        {
            TG.BLL.cm_KaoheSpecMemsFz bll = new BLL.cm_KaoheSpecMemsFz();

            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoheSpecMemsFz>>(entityData);
            //发送消息
            if (list.Count > 0)
            {
                string strWhereOld = string.Format(" KHTypeId={0} AND SpeID={1}", this.KHtypeID, this.SpeID);
                //查询老的数据
                var listold = bll.GetModelList(strWhereOld);

                if (listold.Count == 0)
                {
                    list.ForEach(c =>
                    {
                        string strWhere = string.Format("  KHTypeId={0} AND MemID={1} AND SpeID={2} ", c.KHTypeId, c.MemID, c.SpeID);
                        int memscount = bll.GetModelList(strWhere).Count;
                        if (memscount == 0)
                        {
                            //添加
                            int id = bll.Add(c);
                            if (id > 0)
                            {
                                c.ID = id;
                                if (c.SpeID != -1)//除项目经理
                                {
                                    //取消设置的时候同时给负责人发送消息 2016年7月13日
                                    //SendMsgToUser(c);
                                }
                            }
                        }

                    });
                    Response.Write("1");
                }
                else
                {
                    //查询老的数据是否在新更新的数据中
                    foreach (TG.Model.cm_KaoheSpecMemsFz mem in listold)
                    {
                        var mm = list.Where(m => m.SpeID == mem.SpeID && m.KHTypeId == mem.KHTypeId && m.MemID == mem.MemID).ToList();
                        //如果在新的里面找不到
                        if (mm.Count == 0)
                        {
                            //删除
                            new TG.BLL.cm_KaoheSpecMemsFz().Delete(mem.ID);
                        }
                    }

                    //添加新加的记录

                    list.ForEach(c =>
                    {
                        string strWhere = string.Format("  KHTypeId={0} AND MemID={1} AND SpeID={2} ", c.KHTypeId, c.MemID, c.SpeID);
                        int memscount = bll.GetModelList(strWhere).Count;
                        if (memscount == 0)
                        {
                            int id = bll.Add(c);
                            if (id > 0)
                            {
                                c.ID = id;
                                if (c.SpeID != -1)//除项目经理
                                {
                                    // 取消给设置的同时给专业负责人发送消息 2016年7月13日
                                    //SendMsgToUser(c);
                                }
                            }
                        }

                    });

                    Response.Write("1");
                }
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 给用户发消息
        /// </summary>
        /// <param name="mem"></param>
        private void SendMsgToUser(TG.Model.cm_KaoheSpecMems c)
        {
            string msgType = "专业负责人设置";
            if (c.SpeID == -2 || c.SpeID == -1 || c.SpeID == -3)//2017年4月19日 修改一个部门经理审批
            {
                msgType = "专业间比例设置";
            }
            //查询任务
            string renwuName = "";
            var renwuProjModel = new TG.BLL.cm_KaoHeRenwuProj().GetModel(c.ProKHId);
            if (renwuProjModel != null)
            {
                var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(renwuProjModel.RenWuId);

                if (renwuModel != null)
                {
                    renwuName = renwuModel.RenWuNo;
                }
            }
            //查询考核名称 
            string kaoheName = "";
            var khNameModel = new TG.BLL.cm_KaoHeProjName().GetModel(c.ProKHNameId);//修改无法查询到考核名称的错误 2016-8-15
            if (khNameModel != null)
            {
                kaoheName = khNameModel.KName;
            }
            //查询项目
            var proModel = new TG.BLL.cm_Project().GetModel(c.ProId);
            //发送消息
            var bllmsg = new TG.BLL.cm_SysMsg();
            //如果是工程主持人
            if (c.SpeID == -2 || c.SpeID == -1 || c.SpeID == -3)//2017年4月19日 修改一个部门经理审批
            {
                //实例消息实体
                var sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("proid={0}&prokhid={1}&khnameid={2}&khtypeid={3}&id={4}&speid={5}&setup=1", c.ProId, c.ProKHId, c.ProKHNameId, c.KHTypeId, c.ID, c.SpeID),
                    FromUser = c.MemID,
                    InUser = this.SendUserID,
                    MsgType = 34,//项目比例设置
                    //MessageContent = string.Format("关于\"{0}\"{2}考核{3}的{1}！", proModel.pro_name, msgType, renwuName, kaoheName),
                    MessageContent = string.Format("关于\"{0}\"{1}的{2}！", kaoheName, renwuName, msgType),
                    QueryCondition = proModel.pro_name + "的" + msgType,
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };
                //发消息
                int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
            }
            else
            {
                //实例消息实体
                var sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("proid={0}&prokhid={1}&khnameid={2}&khtypeid={3}&id={4}&speid={5}&setup=1&action=6", c.ProId, c.ProKHId, c.ProKHNameId, c.KHTypeId, c.ID, c.SpeID),
                    FromUser = c.MemID,
                    InUser = this.SendUserID,
                    MsgType = 34,//项目比例设置
                    //MessageContent = string.Format("关于\"{0}\"{2}考核{3}的{1}！", proModel.pro_name, msgType, renwuName, kaoheName),
                    MessageContent = string.Format("关于\"{0}\"{1}的{2}！", kaoheName, renwuName, msgType),
                    QueryCondition = proModel.pro_name + "的" + msgType,
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };
                //发消息
                int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
            }
        }

        /// <summary>
        /// 给负责人发消息
        /// </summary>
        /// <param name="mem"></param>
        private void SendMsgToUser(TG.Model.cm_KaoheSpecMemsFz c)
        {
            string msgType = "专业人员间比例设置";
            if (c.SpeID == -2)
            {
                msgType = "审核专业间比例设置";
            }
            //查询任务
            string renwuName = "";
            var renwuProjModel = new TG.BLL.cm_KaoHeRenwuProj().GetModel(c.ProKHId);
            if (renwuProjModel != null)
            {
                var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(renwuProjModel.RenWuId);

                if (renwuModel != null)
                {
                    renwuName = renwuModel.RenWuNo;
                }
            }
            //考核名称
            string kaoheName = "";
            var kaoheNameModel = new TG.BLL.cm_KaoHeProjName().GetModel(c.ProKHNameId);
            if (kaoheNameModel != null)
            {
                kaoheName = kaoheNameModel.KName;
            }
            //查询项目
            var proModel = new TG.BLL.cm_Project().GetModel(c.ProId);
            //发送消息
            var bllmsg = new TG.BLL.cm_SysMsg();

            //实例消息实体
            var sysMessageViewEntity = new SysMessageViewEntity
            {
                ReferenceSysNo = string.Format("proid={0}&prokhid={1}&khnameid={2}&khtypeid={3}&id={4}&speid={5}&setup=1", c.ProId, c.ProKHId, c.ProKHNameId, c.KHTypeId, c.ID, c.SpeID),
                FromUser = c.MemID,
                InUser = this.SendUserID,
                MsgType = 34,//项目比例设置
                //MessageContent = string.Format("关于\"{0}\"{2}考核{3}的{1}！", proModel.pro_name, msgType, renwuName, kaoheName),
                MessageContent = string.Format("关于\"{0}\"{1}的{2}！", kaoheName, renwuName, msgType),
                QueryCondition = proModel.pro_name + "的" + msgType,
                Status = "A",
                IsDone = "A",
                ToRole = "0"
            };
            //发消息
            int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
        }
        /// <summary>
        /// 给用户发消息
        /// </summary>
        /// <param name="mem"></param>
        private void SendMsgToUser(TG.Model.cm_KaoHeSpeMemsPersent c, TG.Model.tg_member mem)
        {
            string msgType = "项目人员比例设置";
            if (c.SpeID == -2)
            {
                msgType = "项目间比例设置";
            }
            //查询任务
            string renwuName = "";
            var renwuProjModel = new TG.BLL.cm_KaoHeRenwuProj().GetModel(c.ProKHID);
            if (renwuProjModel != null)
            {
                var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(renwuProjModel.RenWuId);

                if (renwuModel != null)
                {
                    renwuName = renwuModel.RenWuNo;
                }
            }
            //考核名称
            string kaoheName = "";
            var kaoheNameModel = new TG.BLL.cm_KaoHeProjName().GetModel(c.KaoHeNameID);
            if (kaoheNameModel != null)
            {
                kaoheName = kaoheNameModel.KName;
            }
            //审批阶段
            int setup = this.Setup + 1;
            //查询项目
            var proModel = new TG.BLL.cm_Project().GetModel(c.ProID);
            //发送消息
            var bllmsg = new TG.BLL.cm_SysMsg();
            //实例消息实体
            var sysMessageViewEntity = new SysMessageViewEntity
            {
                ReferenceSysNo = string.Format("proid={0}&prokhid={1}&khnameid={2}&khtypeid={3}&speid={3}&id={4}&setup={5}&action=5", c.ProID, c.ProKHID, c.KaoHeNameID, c.SpeID, c.ID, setup),
                FromUser = mem.mem_ID,
                InUser = this.SendUserID,
                MsgType = 34,//项目比例设置
                //MessageContent = string.Format("关于\"{0}\"{2}考核{3}的审核{1}！", proModel.pro_name, msgType, renwuName, kaoheName),
                MessageContent = string.Format("关于\"{0}\"{1}的{2}！", kaoheName, renwuName, msgType),
                QueryCondition = proModel.pro_name + "的" + msgType,
                Status = "A",
                IsDone = "A",
                ToRole = "0"
            };

            //发消息
            int count = bllmsg.InsertSysMessage(sysMessageViewEntity);
        }
        /// <summary>
        /// 获取类型
        /// </summary>
        private void GetTypeModel()
        {
            var model = new TG.BLL.cm_KaoheProjType().GetModel(this.ProKHTypeID);
            if (model != null)
            {
                string jsonStr = JsonConvert.SerializeObject(model);
                Response.Write(jsonStr);
            }
            else
            {
                Response.Write("1");
            }
        }
        /// <summary>
        /// 删除分类
        /// </summary>
        private void DeleteType()
        {
            bool isdel = new TG.BLL.cm_KaoheProjType().Delete(this.ProKHTypeID);
            if (isdel)
            {
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        private void SaveKHType()
        {
            TG.BLL.cm_KaoheProjType bll = new TG.BLL.cm_KaoheProjType();
            //实体
            var entity = JsonConvert.DeserializeObject<TG.Model.cm_KaoheProjType>(entityData);
            if (entity != null)
            {
                if (entity.ID == 0)
                {
                    bll.Add(entity);
                }
                else
                {
                    bll.Update(entity);
                }

                Response.Write("1");
            }
        }
        /// <summary>
        /// 查询类型列表
        /// </summary>
        private void GetTypeList()
        {
            string strWhere = string.Format(" KaoheNameID={0}", this.ProKHNameID);
            var list = new TG.BLL.cm_KaoheProjType().GetModelList(strWhere);

            if (list.Count > 0)
            {
                //排序
                list = list.OrderBy(c => c.ID).ToList();

                string jsonStr = JsonConvert.SerializeObject(list);
                Response.Write(jsonStr);
            }
            else
            {
                Response.Write("1");
            }
        }
        /// <summary>
        /// 删除项目名称
        /// </summary>
        private void DeleteProName()
        {
            bool isdel = new TG.BLL.cm_KaoHeProjName().Delete(this.ProKHNameID);
            if (isdel)
            {
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 获取单个考核
        /// </summary>
        private void GetOneKaoheName()
        {
            var model = new TG.BLL.cm_KaoHeProjName().GetModel(this.ProKHNameID);
            if (model != null)
            {
                string jsonStr = JsonConvert.SerializeObject(model);
                Response.Write(jsonStr);
            }
            else
            {
                Response.Write("1");
            }
        }

        /// <summary>
        /// 获取考核名称
        /// </summary>
        private void GetKHName()
        {
            if (this.ProjID != 0)
            {
                //得到考核任务列表
                var renwuList = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderBy(c => c.ID).ToList();
                //当前考核任务
                int index = renwuList.FindIndex(c => c.ID == this.RenwuID);
                //查询上一考核任务
                var prevRenwuModel = renwuList[index];
                if (index - 1 > -1)
                {
                    prevRenwuModel = renwuList[index - 1];
                }

                //获取上一考核任务ID
                int prevRenwuID = prevRenwuModel.ID;

                StringBuilder sb = new StringBuilder();
                sb.Append("select * ");
                sb.Append(" ,isnull((Select SUM(virallotCount) From dbo.cm_KaoheProjType Where KaoheNameID=P.ID),0) AS ViralCount");
                sb.Append(" ,(Select RenWuId From dbo.cm_KaoHeRenwuProj Where ID=P.KaoHeProjId) AS RenwuID");
                sb.Append(" ,(Select RenwuNo From cm_KaoHeRenwu Where ID=(Select top 1 RenWuId From dbo.cm_KaoHeRenwuProj Where ID=P.KaoHeProjId)) AS RenwuName");
                sb.Append(" From cm_KaoHeProjName P");
                sb.AppendFormat(" Where (KaoHeProjId IN (Select ID from cm_KaoHeRenwuProj Where RenWuId={0} AND proID={1})) OR (KaoHeProjId={2} AND proID={1}) ", prevRenwuID, this.ProjID, this.KaoHeProjID);
                sb.AppendFormat(" Order By RenwuID ");

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string jsonStr = JsonConvert.SerializeObject(dt);
                    Response.Write(jsonStr);
                }
                else
                {
                    Response.Write("1");
                }
            }
            else
            {
                Response.Write("1");
            }
        }
        /// <summary>
        /// 考核名称历史ID
        /// </summary>
        private int KaoHeNameIdOld
        {
            get
            {
                int id = 0;
                int.TryParse(Request["kaohenameidold"] ?? "0", out id);
                return id;
            }
        }
        /// <summary>
        /// 保存考核名称
        /// </summary>
        private void SaveKHName()
        {
            TG.BLL.cm_KaoHeProjName bll = new TG.BLL.cm_KaoHeProjName();
            //得到实体
            var entity = JsonConvert.DeserializeObject<TG.Model.cm_KaoHeProjName>(entityData);
            if (entity != null)
            {
                if (entity.ID == 0)
                {
                    //添加
                    int id = bll.Add(entity);
                    //历史数据id不为空
                    if (this.KaoHeNameIdOld > 0)
                    {
                        //查询原始类型
                        var blltype = new TG.BLL.cm_KaoheProjType();
                        string strWhere = string.Format(" KaoheNameID={0} ", this.KaoHeNameIdOld);
                        var typelist = blltype.GetModelList(strWhere);

                        typelist.ForEach(c =>
                        {
                            var model = new TG.Model.cm_KaoheProjType()
                            {
                                KaoheNameID = id,
                                KaoheTypeID = c.KaoheTypeID,
                                KaoheTypeName = c.KaoheTypeName,
                                RealScale = c.RealScale,
                                TypeXishu = c.TypeXishu,
                                InsertUserID = entity.InserUserID,
                                InsertDate = entity.InsertDate,
                                virallotCount = c.virallotCount
                            };
                            //插入考核类型
                            blltype.Add(model);
                        });
                    }
                }
                else
                {
                    //更新
                    bll.Update(entity);
                }

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 获取设置项目信息
        /// </summary>
        private void GetProj()
        {
            TG.BLL.cm_KaoHeRenwuProj bll = new BLL.cm_KaoHeRenwuProj();

            StringBuilder sb = new StringBuilder();
            sb.Append("Select *");
            sb.Append(" From dbo.cm_KaoHeRenwuProj ");
            if (this.UnitID != -1)
            {
                sb.AppendFormat(" Where RenWuID={0} AND UnitID={1}", this.RenwuID, this.UnitID);
            }
            else
            {
                sb.AppendFormat(" Where RenWuID={0} ", this.RenwuID);
            }
            //获取单位考核列表
            DataTable list = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            if (list.Rows.Count > 0)
            {
                string jsonResult = JsonConvert.SerializeObject(list);
                Response.Write(jsonResult);
            }
            else
            {
                Response.Write("1");
            }
        }
        /// <summary>
        /// 保存设置项目
        /// </summary>

        private void SaveProj()
        {
            TG.BLL.cm_KaoHeRenwuProj bll = new TG.BLL.cm_KaoHeRenwuProj();
            //得到实体 
            var list = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeRenwuProj>>(entityData);

            if (list.Count > 0)
            {
                list.ForEach(c =>
                {
                    //查询本次分配本部门的项目是否已经录入
                    string strWhere = string.Format(" RenWuId='{0}' AND UnitID={1} AND proID={2} ", c.RenWuId, c.UnitID, c.proID);
                    var modellist = bll.GetModelList(strWhere);

                    if (modellist.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        c.ID = modellist[0].ID;
                        //保持状态
                        c.Statu = modellist[0].Statu;
                        c.InsertUserId = modellist[0].InsertUserId;
                        c.InsertDate = modellist[0].InsertDate;
                        bll.Update(c);
                    }
                });

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}