﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TG.Model;

namespace TG.Web.HttpHandler.DeptBpm
{
    /// <summary>
    /// ProjDeptHandler 的摘要说明
    /// </summary>
    public class ProjDeptHandler : IHttpHandler
    {
        public HttpRequest Request
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }
        public HttpResponse Response
        {
            get
            {
                return HttpContext.Current.Response;
            }
        }

        /// <summary>
        /// 动作
        /// </summary>
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }

        /// <summary>
        /// 数据实体
        /// </summary>
        public string entityDataProj
        {
            get
            {
                return Request["data"] ?? "";
            }
        }
        public string entityDataSpe
        {
            get
            {
                return Request["data2"] ?? "";
            }
        }
        public string entityDataZcr
        {
            get
            {
                return Request["data3"] ?? "";
            }
        }
        public string entityDataMem
        {
            get
            {
                return Request["data4"] ?? "";
            }
        }

        /// <summary>
        /// 部门ID
        /// </summary>
        public int UnitID
        {
            get
            {
                int uid = 0;
                int.TryParse(Request["uid"] ?? "0", out uid);
                return uid;
            }
        }
        /// <summary>
        /// 部门考核ID
        /// </summary>
        public int KaoHeUnitID
        {
            get
            {
                int kuniid = 0;
                int.TryParse(Request["ukaoheid"] ?? "0", out kuniid);
                return kuniid;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public int RenwuID
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["renwuid"] ?? "0", out renwuid);
                return renwuid;
            }
        }
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProjID
        {

            get
            {
                int projid = 0;
                int.TryParse(Request["projid"] ?? "0", out projid);
                return projid;
            }
        }
        /// <summary>
        /// 项目考核ID
        /// </summary>
        public int KaoHeProjID
        {
            get
            {
                int khproid = 0;
                int.TryParse(Request["khproid"] ?? "0", out khproid);
                return khproid;
            }
        }
        /// <summary>
        /// 考核名称ID
        /// </summary>
        public int ProKHNameID
        {
            get
            {
                int pronameid = 0;
                int.TryParse(Request["id"] ?? "0", out pronameid);
                return pronameid;
            }
        }
        public int KHNameID
        {
            get
            {
                int pronameid = 0;
                int.TryParse(Request["khnameid"] ?? "0", out pronameid);
                return pronameid;
            }
        }
        /// <summary>
        /// 考核类型ID
        /// </summary>
        public int ProKHTypeID
        {
            get
            {
                int pronameid = 0;
                int.TryParse(Request["id"] ?? "0", out pronameid);
                return pronameid;
            }
        }
        /// <summary>
        /// 发送消息的人
        /// </summary>
        public int SendUserID
        {
            get
            {
                int senderid = 0;
                int.TryParse(Request["senderid"] ?? "0", out senderid);
                return senderid;
            }
        }

        public int KHtypeID
        {
            get
            {
                int KHtypeid = 0;
                int.TryParse(Request["KHtypeid"] ?? "0", out KHtypeid);
                return KHtypeid;
            }
        }

        public int MemID
        {
            get
            {
                int memid = 0;
                int.TryParse(Request["memid"] ?? "0", out memid);
                return memid;
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            switch (Action)
            {
                case "startdept":
                    SendMsgToMng();
                    break;
                case "saveprojallot":
                    SaveAllot();
                    break;
                case "savereport":
                    SaveMemsReport();
                    break;
                case "saveunitreport":
                    SaveUnitReport();
                    break;
                case "getroleunit":
                    GetMemsRoleUnit();
                    break;
                case "getmemallotdetails":
                    GetMemsAllotDetails();
                    break;
                case "bak":
                    SaveUnitAllotBakData();
                    break;
                case "projbak":
                    ProjBak();
                    break;
            }
        }
        /// <summary>
        /// 项目分配数据备份
        /// </summary>
        private void ProjBak()
        {
            var projList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeProjHis>>(entityDataProj);
            var weightList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeProjWeightHis>>(entityDataSpe);
            var weightList2 = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeProjWeightHis>>(entityDataZcr);

            //都不为空
            if (projList.Count > 0 && weightList.Count > 0 && weightList2.Count > 0)
            {
                string strWhere = string.Format(" RenwuID={0} ", projList[0].RenwuID);

                var bllrenwu = new TG.BLL.cm_KaoheRenwuHistory();
                var renwuCount = bllrenwu.GetRecordCount(strWhere);

                if (renwuCount > 0)
                {
                    var bll = new TG.BLL.cm_KaoHeProjHis();
                    var bllout = new TG.BLL.cm_KaoHeProjWeightHis();

                    int count = bll.GetRecordCount(strWhere);
                    if (count == 0)
                    {
                        //项目调整
                        projList.ForEach(c =>
                        {
                            bll.Add(c);
                        });

                        //权重1
                        weightList.ForEach(c =>
                        {
                            bllout.Add(c);
                        });

                        //权重2
                        weightList2.ForEach(c =>
                        {
                            bllout.Add(c);
                        });


                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("2");
                    }
                }
                else
                {
                    Response.Write("3");
                }
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存部门奖金分配备份数据
        /// </summary>
        private void SaveUnitAllotBakData()
        {
            //获取实际数据列表
            var allotList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeUnitAllotHis>>(entityDataProj);

            if (allotList.Count > 0)
            {
                //查询当前考核任务是不是已经备档
                string strWhere = string.Format(" RenwuID={0}", this.RenwuID);
                int kaoHeHisCount = new TG.BLL.cm_KaoheRenwuHistory().GetRecordCount(strWhere);
                //考核任务已经备档
                if (kaoHeHisCount > 0)
                {
                    var bllAllot = new TG.BLL.cm_KaoHeUnitAllotHis();
                    //查询是否已经备档
                    int allotCount = bllAllot.GetRecordCount(strWhere);

                    if (allotCount == 0)
                    {
                        allotList.ForEach(c =>
                        {
                            //备档
                            bllAllot.Add(c);
                        });

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("2");
                    }
                }
                else
                {
                    Response.Write("3");
                }
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 获取参与项目分配情况
        /// </summary>
        private void GetMemsAllotDetails()
        {
            if (this.RenwuID > 0)
            {
                string strSql = string.Format(@"select  MAX(C.proName) AS proName,
		                                        MAX(B.KName) AS KName,
		                                        SUM(AllotCount) AS AllotCount,
		                                        C.proID
                                                from cm_KaoHeMemsAllot A left join cm_KaoHeProjName B on a.ProKHNameId=b.ID left join cm_KaoHeRenwuProj C on b.KaoHeProjId=c.ID
                                                Where ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})
                                                AND A.MemID={1}
                                                group by C.proID", this.RenwuID, this.MemID);

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                string jsonTable = JsonConvert.SerializeObject(dt);

                Response.Write(jsonTable);
            }
        }

        private void GetMemsRoleUnit()
        {
            string strWhere = string.Format(" mem_Id={0}", this.ProKHTypeID);

            var bllRole = new TG.BLL.tg_memberRole();

            var modellist = bllRole.GetModelList(strWhere);

            string jsonStr = JsonConvert.SerializeObject(modellist);

            Response.Write(jsonStr);
        }
        /// <summary>
        /// 保存部门数据
        /// </summary>
        private void SaveUnitReport()
        {
            var unitAllotList = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeUnitAllotReport>>(entityDataProj);

            if (unitAllotList.Count > 0)
            {
                var bll = new TG.BLL.cm_KaoHeUnitAllotReport();

                unitAllotList.ForEach(c =>
                {
                    string strWhere = string.Format(" UnitID={0} AND RenwuID={1}", c.UnitID, c.RenwuID);

                    var list = bll.GetModelList(strWhere);
                    if (list.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        c.ID = list[0].ID;
                        bll.Update(c);
                    }
                });

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存分配调整数据
        /// </summary>
        private void SaveMemsReport()
        {
            var projAllotRpt = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeMemsAllotChange>>(entityDataProj);
            //保存调整后的值
            if (projAllotRpt.Count > 0)
            {
                TG.BLL.cm_KaoHeMemsAllotChange bll = new BLL.cm_KaoHeMemsAllotChange();
                projAllotRpt.ForEach(c =>
                {
                    //插叙你是否已经保存过
                    string strWhere = string.Format(" RenwuID={0} AND MemID={1}", c.RenwuID, c.MemID);

                    var list = bll.GetModelList(strWhere);

                    if (list.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        c.ID = list[0].ID;
                        bll.Update(c);
                    }
                });
            }
            //保存部门权重
            var projAllotWeights = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeMemsAllotReport>>(entityDataSpe);
            if (projAllotWeights.Count > 0)
            {
                TG.BLL.cm_KaoHeMemsAllotReport bll = new BLL.cm_KaoHeMemsAllotReport();
                projAllotWeights.ForEach(c =>
                {
                    //插叙你是否已经保存过
                    string strWhere = string.Format(" RenwuID={0} AND UnitID={1}", c.RenwuID, c.UnitID);

                    var list = bll.GetModelList(strWhere);

                    if (list.Count == 0)
                    {
                        bll.Add(c);
                    }
                    else
                    {
                        c.ID = list[0].ID;
                        bll.Update(c);
                    }
                });
            }

            Response.Write("1");
        }
        /// <summary>
        /// 保存数据
        /// </summary>
        private void SaveAllot()
        {
            //项目基础信息
            var projAllot = JsonConvert.DeserializeObject<TG.Model.cm_KaoHeProjAllot>(entityDataProj);

            if (projAllot != null)
            {
                //项目分配id
                int AllotID = 0;
                //查询是否已经存在
                var bllproj = new TG.BLL.cm_KaoHeProjAllot();
                string strWhere = string.Format(" ProKHNameId={0} AND ProId={1}", projAllot.ProKHNameId, projAllot.ProId);
                var list = bllproj.GetModelList(strWhere);
                if (list.Count == 0)
                {
                    AllotID = bllproj.Add(projAllot);
                }
                else
                {
                    projAllot.ID = list[0].ID;
                    AllotID = list[0].ID;
                    //更新
                    bllproj.Update(projAllot);
                }

                //插入专业
                if (AllotID > 0)
                {
                    //专业
                    var bllspe = new TG.BLL.cm_KaoHeSpePersentAllot();
                    var spelist = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeSpePersentAllot>>(entityDataSpe);
                    if (spelist.Count > 0)
                    {
                        spelist.ForEach(c =>
                        {
                            strWhere = string.Format(" ProKHNameId={0} AND AllotID={1} AND SpeID={2} ", c.ProKHNameId, AllotID, c.SpeID);

                            var modellist = bllspe.GetModelList(strWhere);

                            if (modellist.Count == 0)
                            {
                                c.AllotID = AllotID;
                                bllspe.Add(c);
                            }
                            else
                            {
                                c.ID = modellist[0].ID;
                                c.AllotID = modellist[0].AllotID;
                                bllspe.Update(c);
                            }
                        });
                    }
                    else
                    {
                        Response.Write("0");
                        return;
                    }

                    //主持人
                    var bllzcr = new TG.BLL.cm_KaoHeSpecMemsAllot();
                    var zcrlist = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeSpecMemsAllot>>(entityDataZcr);
                    if (zcrlist.Count > 0)
                    {
                        zcrlist.ForEach(c =>
                        {
                            strWhere = string.Format(" ProKHNameId={0} AND AllotID={1} AND SpeID={2} ", c.ProKHNameId, AllotID, c.SpeID);

                            var modellist = bllzcr.GetModelList(strWhere);

                            if (modellist.Count == 0)
                            {
                                c.AllotID = AllotID;
                                bllzcr.Add(c);
                            }
                            else
                            {
                                c.ID = modellist[0].ID;
                                c.AllotID = modellist[0].AllotID;
                                bllzcr.Update(c);
                            }
                        });
                    }
                    else
                    {
                        Response.Write("0");
                        return;
                    }

                    //员工
                    var bllmem = new TG.BLL.cm_KaoHeMemsAllot();
                    var memlist = JsonConvert.DeserializeObject<List<TG.Model.cm_KaoHeMemsAllot>>(entityDataMem);
                    if (memlist.Count > 0)
                    {
                        memlist.ForEach(c =>
                        {
                            strWhere = string.Format(" ProKHNameId={0} AND AllotID={1} AND SpeID={2} AND MemID={3}", c.ProKHNameId, AllotID, c.SpeID, c.MemID);

                            var modellist = bllmem.GetModelList(strWhere);

                            if (modellist.Count == 0)
                            {
                                c.AllotID = AllotID;
                                bllmem.Add(c);
                            }
                            else
                            {
                                c.ID = modellist[0].ID;
                                c.AllotID = modellist[0].AllotID;
                                bllmem.Update(c);
                            }
                        });
                    }
                    else
                    {
                        Response.Write("0");
                        return;
                    }

                }
            }

            Response.Write("1");
        }
        /// <summary>
        /// 发送消息给
        /// </summary>
        private void SendMsgToMng()
        {
            string msgType = "项目奖金分配调整";

            //查询项目
            var proModel = new TG.BLL.cm_Project().GetModel(this.ProjID);
            //查询任务
            var projKHModel = new TG.BLL.cm_KaoHeRenwuProj().GetModel(this.KaoHeProjID);
            string renwuName = "";
            if (projKHModel != null)
            {
                var renwuModel = new TG.BLL.cm_KaoHeRenWu().GetModel(projKHModel.RenWuId);
                renwuName = renwuModel.RenWuNo;
            }
            //分考核名称
            string KhName = "";
            var kaoheNameModel = new TG.BLL.cm_KaoHeProjName().GetModel(this.ProKHNameID);
            if (kaoheNameModel != null)
            {
                KhName = kaoheNameModel.KName;
            }

            //消息发送数量
            int count = 0;
            if (IsCompleteKaoHeName(projKHModel.RenWuId))
            {
                //记录项目奖金分配调整信息
                string strWhere = string.Format(" proID={0} AND proKHid={1} AND KhNameId={2} ", this.ProjID, this.KaoHeProjID, this.ProKHNameID);
                var kaohechgBll = new TG.BLL.cm_KaoHeMemsAllotChangeList();
                var kaohechgList = kaohechgBll.GetModelList(strWhere);

                if (kaohechgList.Count == 0)
                {
                    var kaohechgModel = new TG.Model.cm_KaoHeMemsAllotChangeList()
                    {
                        proID = this.ProjID,
                        proName = proModel.pro_name,
                        renwuID = projKHModel.RenWuId,
                        renwuName = renwuName,
                        proKHid = this.KaoHeProjID,
                        KhNameId = this.ProKHNameID,
                        KhName = KhName
                    };

                    kaohechgBll.Add(kaohechgModel);
                }

                //发送消息
                var bllmsg = new TG.BLL.cm_SysMsg();
                //实例消息实体
                var sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("proid={0}&prokhid={1}&khnameid={2}&action=2", this.ProjID, this.KaoHeProjID, this.ProKHNameID),
                    FromUser = 1442,//给张维发消息
                    InUser = this.SendUserID,
                    MsgType = 34,//项目比例设置
                    MessageContent = string.Format("关于\"{0}\"的{1}！", renwuName, msgType),
                    QueryCondition = proModel.pro_name + "的" + msgType,
                    Status = "A",
                    IsDone = "A",
                    ToRole = "0"
                };

                //发消息
                count = bllmsg.InsertSysMessage(sysMessageViewEntity);
            }
            else
            {
                count = 1;
            }

            if (count > 0)
            {
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 考核是否调整完成
        /// </summary>
        protected bool IsCompleteKaoHeName(int renwuid)
        {

            string strSql = string.Format(@"select COUNT(*)
                                                From cm_KaoHeProjName N left join cm_KaoHeRenwuProj P on N.KaoHeProjId=P.ID
                                                Where P.RenWuId={0}", renwuid);
            int count = Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(strSql));


            strSql = string.Format(@"Select COUNT(*) 
                                                From dbo.cm_KaoHeProjAllot
                                                Where ProKHNameId IN (select distinct N.ID
                                                From cm_KaoHeProjName N left join cm_KaoHeRenwuProj P on N.KaoHeProjId=P.ID
                                                Where P.RenWuId={0}) AND Stat=1", renwuid);
            int count2 = Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(strSql));

            //设置完成最后一条奖金调整发送消息
            if (count == (count2 + 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}