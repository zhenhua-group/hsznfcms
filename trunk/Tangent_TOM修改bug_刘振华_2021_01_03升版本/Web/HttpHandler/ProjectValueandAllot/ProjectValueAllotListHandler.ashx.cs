﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    /// <summary>
    /// ProjectValueAllotListHandler1 的摘要说明
    /// </summary>
    public class ProjectValueAllotListHandler1 : ProjectValueAllotListHandler
    {
          
        public override string ProcName
        {
            get
            {
                return "P_cm_TranProjectValuelNoList_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_TranProjectValuelYesList_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_ProjectValueList_jq";
            }
        }
        public override string ProcName4
        {
            get
            {
                return "P_cm_ProjectSecondValuelList_jq";
            }
        }
    }
}