﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TG.DBUtility;
using System.Text;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    /// <summary>
    /// SaveAllot 的摘要说明
    /// </summary>
    public class SaveAllotHandler : IHttpHandler
    {
        public string JsonStringData
        {
            get
            {
                return HttpContext.Current.Request["data"] ?? "{}";
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            AddAllotData();
        }

        public void AddAllotData()
        {
            AuditDesignCoefficient allotData = Newtonsoft.Json.JsonConvert.DeserializeObject<AuditDesignCoefficient>(JsonStringData);
            if (allotData != null)
            {
                string delsql = string.Format(@"delete from cm_AuditDesignCoefficient
                                                where proid={0} AND allotyear={1} and allottype='{2}' AND SpeID={3} ", allotData.ProID, allotData.AllotYear, allotData.AllotType, allotData.SpeID);
                //删除旧数据
                DbHelperSQL.ExecuteSql(delsql);
                //插入分配数据
                StringBuilder strSql = new StringBuilder("");
                allotData.AllotItems.ForEach(item =>
                {
                    strSql.AppendFormat(@"insert into cm_AuditDesignCoefficient(ProID,UserID,UserName,AllotPrt,AllotMoney,AllotType,InserUser,AllotYear,SpeID) 
                                        values({0},{1},'{2}',{3},{4},'{5}',{6},{7},{8});", allotData.ProID, item.UserID, item.UserName, item.AllotPrt, item.AllotMoney, allotData.AllotType, allotData.InserUser, allotData.AllotYear, allotData.SpeID);
                });
                //插入数据
                try
                {
                    DbHelperSQL.ExecuteSql(strSql.ToString());
                    HttpContext.Current.Response.Write("0");
                }
                catch (System.Exception ex)
                {
                    HttpContext.Current.Response.Write("1");
                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class AuditDesignCoefficient
    {
        public int ProID { get; set; }
        public string AllotType { get; set; }
        public int InserUser { get; set; }
        public int AllotYear { get; set; }
        public int SpeID { get; set; }
        public List<AllotItem> AllotItems { get; set; }

    }

    public class AllotItem
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public decimal AllotPrt { get; set; }
        public decimal AllotMoney { get; set; }
    }
}