﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    public abstract class CostDetailsHandler : IHttpHandler
    {

        //存储过程名
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            TG.BLL.cm_CostDetails costBll = new TG.BLL.cm_CostDetails();
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "del")//删除
            {
                string costid = context.Request.Params["costid"];
                if (costBll.Delete(int.Parse(costid)))
                {

                    string unit = context.Request.Params["unit"];
                    string year1 = context.Request.Params["year1"];
                    string year2 = context.Request.Params["year2"];
                    string keyname = context.Request.Params["keyname"];
                    StringBuilder strsql = new StringBuilder("");
                    string costtypeall = context.Request.Params["costtypeall"];
                    //按名称查询
                    if (keyname.Trim() != "")
                    {
                        keyname = TG.Common.StringPlus.SqlSplit(keyname);
                        strsql.AppendFormat(" AND costSub like '%{0}%'", keyname);
                    }
                    //按生产部门查询
                    if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                    {
                        strsql.AppendFormat(" AND (costUnit= '{0}')", unit);
                    }
                    //类别查询
                    if (!string.IsNullOrEmpty(costtypeall) && costtypeall != "-1" && !costtypeall.Contains("选择类型"))
                    {
                        strsql.AppendFormat(" AND (costTypeID= '{0}')", costtypeall);
                    }
                    //年份
                    if (year1 != "" && year2 != "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, year2);
                    }
                    //年份
                    if (year1 == "" && year2 != "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), year2);
                    }
                    //年份
                    if (year1 != "" && year2 == "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, DateTime.Now.ToString("yyyy-MM-dd"));
                    }

                    strsql.Append(strWhere);
                    parameters.Where = strsql.ToString();

                    this.PagingParameters = parameters;
                    context.Response.Write(GetJsonResult());
                }

            }
            else if (strAction == "sel")
            {

                string unit = context.Request.Params["unit"];
                string costtypeall = context.Request.Params["costtypeall"];
                string year1 = context.Request.Params["year1"];
                string year2 = context.Request.Params["year2"];
                string keyname = context.Request.Params["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND costSub like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (costUnit= '{0}')", unit);
                }
                if (!string.IsNullOrEmpty(costtypeall) && costtypeall != "-1" && !costtypeall.Contains("选择类型"))
                {
                    strsql.AppendFormat(" AND (costTypeID= '{0}')", costtypeall);
                }
                //年份
                if (year1 != "" && year2 != "")
                {
                    strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, year2);
                }
                //年份
                if (year1 == "" && year2 != "")
                {
                    strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), year2);
                }
                //年份
                if (year1 != "" && year2 == "")
                {
                    strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, DateTime.Now.ToString("yyyy-MM-dd"));
                }

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "Add")
            {
                //
                TG.Model.cm_CostDetails model = new TG.Model.cm_CostDetails();
                string userid = context.Request.Params["userid"];
                string unitid = context.Request.Params["unitid"];
                string costtype = context.Request.Params["costtype"];
                string costtime = context.Request.Params["costtime"];
                string chargemoney = context.Request.Params["chargemoney"];
                string outchangemoney = context.Request.Params["outchangemoney"];
                string costnum = context.Request.Params["costnum"];
                string remark = context.Request.Params["remark"];
                model.costCharge = Convert.ToDecimal(chargemoney);
                model.costNum = costnum;
                model.costOutCharge = Convert.ToDecimal(outchangemoney);
                if (costtype == "138")
                {
                    costtype = "99";
                }
                if (costtype == "140")
                {
                    if (unitid == "252")//2
                    {
                        costtype = "126";
                    }
                    else if (unitid == "250")//1
                    {
                        costtype = "122";
                    }
                    else if (unitid == "257")//1
                    {
                        costtype = "133";
                    }
                    else//4
                    {
                        costtype = "117";
                    }
                }
                if (costtype == "139")
                {
                    if (unitid == "252")//2
                    {
                        costtype = "128";
                    }
                    else if (unitid == "250")//1
                    {
                        costtype = "124";
                    }
                    else//4
                    {
                        costtype = "135";
                    }
                }
                model.costTypeID = int.Parse(costtype);
                model.costUnit = int.Parse(unitid);
                model.costUserId = int.Parse(userid);
                model.costTime = Convert.ToDateTime(costtime);
                model.costSub = remark;
                model.isexpord = 1;
                if (costBll.Add(model) > 0)
                {

                    string unit = context.Request.Params["unit"];
                    string costtypeall = context.Request.Params["costtypeall"];
                    string year1 = context.Request.Params["year1"];
                    string year2 = context.Request.Params["year2"];
                    string keyname = context.Request.Params["keyname"];
                    StringBuilder strsql = new StringBuilder("");
                    if (!string.IsNullOrEmpty(costtypeall) && costtypeall != "-1" && !costtypeall.Contains("选择类型"))
                    {
                        strsql.AppendFormat(" AND (costTypeID= '{0}')", costtypeall);
                    }
                    //按名称查询
                    if (keyname.Trim() != "")
                    {
                        keyname = TG.Common.StringPlus.SqlSplit(keyname);
                        strsql.AppendFormat(" AND costSub like '%{0}%'", keyname);
                    }
                    //按生产部门查询
                    if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                    {
                        strsql.AppendFormat(" AND (costUnit= '{0}')", unit);
                    }
                    //年份
                    if (year1 != "" && year2 != "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, year2);
                    }
                    //年份
                    if (year1 == "" && year2 != "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), year2);
                    }
                    //年份
                    if (year1 != "" && year2 == "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, DateTime.Now.ToString("yyyy-MM-dd"));
                    }

                    strsql.Append(strWhere);
                    parameters.Where = strsql.ToString();
                    this.PagingParameters = parameters;
                    context.Response.Write(GetJsonResult());
                }


            }
            else if (strAction == "Edit")
            {
                //
                string userid = context.Request.Params["userid"];
                string costid = context.Request.Params["costid"];
                string unitid = context.Request.Params["unitid"];
                string costtype = context.Request.Params["costtype"];
                string costtime = context.Request.Params["costtime"];
                string chargemoney = context.Request.Params["chargemoney"];
                string outchangemoney = context.Request.Params["outchangemoney"];
                string costnum = context.Request.Params["costnum"];
                string remark = context.Request.Params["remark"];
                TG.Model.cm_CostDetails model = costBll.GetModel(int.Parse(costid));
                model.costCharge = Convert.ToDecimal(chargemoney);
                model.costNum = costnum;
                model.costOutCharge = Convert.ToDecimal(outchangemoney);
                if (costtype == "138")
                {
                    costtype = "99";
                }
                if (costtype == "140")
                {
                    if (unitid == "252")//2
                    {
                        costtype = "126";
                    }
                    else if (unitid == "250")//1
                    {
                        costtype = "122";
                    }
                    else if (unitid == "257")//1
                    {
                        costtype = "133";
                    }
                    else//4
                    {
                        costtype = "117";
                    }
                }
                if (costtype == "139")
                {
                    if (unitid == "252")//2
                    {
                        costtype = "128";
                    }
                    else if (unitid == "250")//1
                    {
                        costtype = "124";
                    }
                    else//4
                    {
                        costtype = "135";
                    }
                }
                model.costTypeID = int.Parse(costtype);
                model.costUnit = int.Parse(unitid);
                model.costUserId = int.Parse(userid);
                model.costTime = Convert.ToDateTime(costtime);
                model.costSub = remark;
                if (costBll.Update(model))
                {

                    string unit = context.Request.Params["unit"];
                    string costtypeall = context.Request.Params["costtypeall"];
                    string year1 = context.Request.Params["year1"];
                    string year2 = context.Request.Params["year2"];
                    string keyname = context.Request.Params["keyname"];
                    StringBuilder strsql = new StringBuilder("");
                    if (!string.IsNullOrEmpty(costtypeall) && costtypeall != "-1" && !costtypeall.Contains("选择类型"))
                    {
                        strsql.AppendFormat(" AND (costTypeID= '{0}')", costtypeall);
                    }
                    //按名称查询
                    if (keyname.Trim() != "")
                    {
                        keyname = TG.Common.StringPlus.SqlSplit(keyname);
                        strsql.AppendFormat(" AND costSub like '%{0}%'", keyname);
                    }
                    //按生产部门查询
                    if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                    {
                        strsql.AppendFormat(" AND (costUnit= '{0}')", unit);
                    }
                    //年份
                    if (year1 != "" && year2 != "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, year2);
                    }
                    //年份
                    if (year1 == "" && year2 != "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), year2);
                    }
                    //年份
                    if (year1 != "" && year2 == "")
                    {
                        strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", year1, DateTime.Now.ToString("yyyy-MM-dd"));
                    }

                    strsql.Append(strWhere);
                    parameters.Where = strsql.ToString();
                    this.PagingParameters = parameters;
                    context.Response.Write(GetJsonResult());
                }
            }
        }

        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        public bool IsReusable
        {
            get { return false; }
        }
    }
}