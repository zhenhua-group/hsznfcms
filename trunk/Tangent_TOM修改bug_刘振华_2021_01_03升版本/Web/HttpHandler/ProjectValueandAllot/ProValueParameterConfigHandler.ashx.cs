﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using Newtonsoft.Json;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProValueParameterConfigHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        TG.BLL.cm_ProjectStageValueConfig bll = new TG.BLL.cm_ProjectStageValueConfig();
        TG.Model.cm_ProjectStageValueConfig model = new TG.Model.cm_ProjectStageValueConfig();



        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            string action = context.Request.Params["action"] ?? "no";
            if (action != "no")
            {
                string str_id = context.Request.Params["id"] ?? "0";
                string str_programPercent = context.Request.Params["programPercent"] ?? "0";
                string str_preliminaryPercent = context.Request.Params["preliminaryPercent"] ?? "0";
                string str_WorkDrawPercent = context.Request.Params["WorkDrawPercent"] ?? "0";
                string str_LateStagePercent = context.Request.Params["LateStagePercent"] ?? "0";
                string str_ProjectStatues = context.Request.Params["ProjectStatues"] ?? "0";
                string str_ItemType = context.Request.Params["itemType"] ?? "";

                if (action == "0")//各阶段
                {
                    model.ID = int.Parse(str_id);
                    model.ProgramPercent = decimal.Parse(str_programPercent);
                    model.preliminaryPercent = decimal.Parse(str_preliminaryPercent);
                    model.WorkDrawPercent = decimal.Parse(str_WorkDrawPercent);
                    model.LateStagePercent = decimal.Parse(str_LateStagePercent);
                    model.ProjectStatues = int.Parse(str_ProjectStatues);
                    model.ItemType = str_ItemType;

                    int count = bll.Update(model);

                    Response.Write(count);
                }
                else if (action == "1")//方案+初设
                {
                    model.ID = int.Parse(str_id);
                    model.ProgramPercent = decimal.Parse(str_programPercent);
                    model.preliminaryPercent = decimal.Parse(str_preliminaryPercent);
                    model.ProjectStatues = int.Parse(str_ProjectStatues);
                    model.ItemType = str_ItemType;

                    int count = bll.Update(model);

                    Response.Write(count);

                }
                else if (action == "2")//施工图+后期
                {
                    model.ID = int.Parse(str_id);
                    model.WorkDrawPercent = decimal.Parse(str_WorkDrawPercent);
                    model.LateStagePercent = decimal.Parse(str_LateStagePercent);
                    model.ProjectStatues = int.Parse(str_ProjectStatues);
                    model.ItemType = str_ItemType;

                    int count = bll.Update(model);

                    Response.Write(count);

                }
                else if (action == "3")//初设+施工图+后期
                {
                    model.ID = int.Parse(str_id);
                    model.preliminaryPercent = decimal.Parse(str_preliminaryPercent);
                    model.WorkDrawPercent = decimal.Parse(str_WorkDrawPercent);
                    model.LateStagePercent = decimal.Parse(str_LateStagePercent);
                    model.ProjectStatues = int.Parse(str_ProjectStatues);
                    model.ItemType = str_ItemType;

                    int count = bll.Update(model);

                    Response.Write(count);

                }
                else if (action == "10")//方案+施工图+后期
                {
                    model.ID = int.Parse(str_id);
                    model.ProgramPercent = decimal.Parse(str_programPercent);
                    model.WorkDrawPercent = decimal.Parse(str_WorkDrawPercent);
                    model.LateStagePercent = decimal.Parse(str_LateStagePercent);
                    model.ProjectStatues = int.Parse(str_ProjectStatues);
                    model.ItemType = str_ItemType;

                    int count = bll.Update(model);

                    Response.Write(count);

                }
                //专业
                else if (action == "spe")
                {

                    string data = Request["data"];
                    TG.Model.cm_ProjectStageSpeValueConfigEntity obj = null;
                    if (!string.IsNullOrEmpty(data))
                    {
                        obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectStageSpeValueConfigEntity>(data);
                    }


                    TG.BLL.cm_ProjectStageSpeValueConfig bllSpe = new TG.BLL.cm_ProjectStageSpeValueConfig();

                    int count = 0;
                    if (obj != null)
                    {
                        count = bllSpe.UpdateValueDetatil(obj);
                    }

                    Response.Write(count);

                }
                else if (action == "design")
                {
                    string str_designid = context.Request.Params["id"] ?? "0";
                    string Specialty = context.Request.Params["Specialty"] ?? "";
                    string AuditPercent = context.Request.Params["AuditPercent"] ?? "0";
                    string SpecialtyHeadPercent = context.Request.Params["SpecialtyHeadPercent"] ?? "0";
                    string ProofreadPercent = context.Request.Params["ProofreadPercent"] ?? "0";
                    string DesignPercent = context.Request.Params["DesignPercent"] ?? "0";

                    TG.BLL.cm_ProjectDesignProcessValueConfig bllDesign = new TG.BLL.cm_ProjectDesignProcessValueConfig();
                    TG.Model.cm_ProjectDesignProcessValueConfig modelDesign = new TG.Model.cm_ProjectDesignProcessValueConfig();

                    modelDesign.ID = int.Parse(str_designid);
                    modelDesign.Specialty = Specialty;
                    modelDesign.AuditPercent = decimal.Parse(AuditPercent);
                    modelDesign.SpecialtyHeadPercent = decimal.Parse(SpecialtyHeadPercent);
                    modelDesign.ProofreadPercent = decimal.Parse(ProofreadPercent);
                    modelDesign.DesignPercent = decimal.Parse(DesignPercent);


                    int count = bllDesign.Update(modelDesign);

                    Response.Write(count);

                }
                //室外工程以及其他类似的
                else if (action == "out")
                {

                    string data = Request["data"];
                    TG.Model.cm_ProjectDesignProcessValueConfigEntity obj = null;
                    if (!string.IsNullOrEmpty(data))
                    {
                        obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectDesignProcessValueConfigEntity>(data);
                    }

                    TG.BLL.cm_ProjectOutDoorValueConfig bllSpe = new TG.BLL.cm_ProjectOutDoorValueConfig();

                    int count = 0;
                    if (obj != null)
                    {
                        count = bllSpe.UpdateValueDetatil(obj);
                    }

                    Response.Write(count);

                }
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}