﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    public abstract class ProjectValueAllotListHandler : IHttpHandler
    {
        //存储过程名
        public abstract string ProcName { get; }
        //已分配的存储过程名
        public abstract string ProcName2 { get; }

        //产值分配的存储过程名
        public abstract string ProcName3 { get; }
        //二次分配产值的存储过程名
        public abstract string ProcName4 { get; }
        //存储过程参数
        protected TG.BLL.ProjectValuePagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.ProjectValuePagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];
            string type = context.Request.QueryString["type"];
            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.ProjectValuePagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere,
                    Type = type
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                string year = DateTime.Now.Year.ToString();
                string sqlwhere = " ";
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters.Year = year;
                }
                else
                {
                    parameters.Year = "null";
                }
                parameters.Where = sqlwhere + strWhere;
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {

            }
            //转部门产值的未分配查询
            else if (strAction == "sel")
            {

                //string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                //if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
                //{
                //    strsql.AppendFormat(" AND Unit= '{0}'", unit);
                //}
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters.Year = year;
                }
                else
                {
                    parameters.Year = "null";
                }

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            //转部门产值的已分配查询
            else if (strAction == "sel2")
            {

                // string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters.Year = year;
                }
                else
                {
                    parameters.Year = "null";
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName2;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            //部门产值的分配查询
            else if (strAction == "ProValueandAlltList")
            {
                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string allottype = context.Request.QueryString["allottype"];//区分哪个所分配产值
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //绑定单位
                if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND Unit='" + unit + "' ");
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters.Year = year;
                }
                else
                {
                    parameters.Year = "null";
                }
                if (allottype == "economy")
                {
                    strsql.AppendFormat(" AND Unit like '%经济%' ");
                }
                else if (allottype == "Have")
                {
                    strsql.AppendFormat(" AND Unit like '%暖通%'");
                }
                else
                {
                    strsql.AppendFormat(" AND Unit not like '%暖通%' and Unit  not like '%经济所%'");
                }

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName3;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            //二次产值的分配查询
            else if (strAction == "ProjectSecond")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //绑定单位
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND Unit='" + unit + "' ");
                }
                //年份
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    // strsql.AppendFormat(" AND year(pro_startTime)={0} ", year);
                    parameters.Year = year;
                }
                else
                {
                    parameters.Year = "null";
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName4;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}