﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.ProjectValueandAllot.ProjectWageBenefits
{
    public abstract class ProjectWageBenefitsHandler : IHttpHandler
    {
        //效益工资发放表
        public abstract string ProcName { get; }

        //存储过程参数
        protected TG.Model.ProjectWageBenefitsParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.Model.ProjectWageBenefitsParameters parameters = null;
            string strWhere = "", unitname = "", unitid = "0";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];
            string year = context.Request.QueryString["year"];
            string unit = context.Request.QueryString["unit"];
            string keyname = context.Request.QueryString["keyname"];
            string name = context.Request.QueryString["name"];
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb1 = new StringBuilder("");
            StringBuilder sb2 = new StringBuilder("");
            StringBuilder sb3 = new StringBuilder("");
            if (strOper != "del")
            {

                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.Model.ProjectWageBenefitsParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = ""
                };
            }

            if (strAction == "member")
            {

                unitid = unit;

                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    sb1.AppendFormat(" AND cs.ValueYear='{0}'", year);
                    sb2.AppendFormat(" AND b.ActualAllountTime='{0}'", year);
                    sb3.AppendFormat(" AND yearWang='{0}'", year);
                }

                //绑定单位
                if (!string.IsNullOrEmpty(unitid) && unitid != "-1")
                {
                    sb.Append(" AND me.mem_Unit_ID=" + unitid.Trim() + "");
                    //sb1.Append(" and cs.UnitId=" + unitid.Trim() + "");
                }

                if (!string.IsNullOrEmpty(name))
                {
                    sb.Append(" AND me.mem_name like '%" + name.Trim() + "%'");
                }

                sb.Append(strWhere);
                parameters.Query = sb.ToString();
                parameters.Query1 = sb1.ToString();
                parameters.Query2 = sb2.ToString();
                parameters.Query3 = sb3.ToString();
                parameters.ProcedureName = ProcName;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}