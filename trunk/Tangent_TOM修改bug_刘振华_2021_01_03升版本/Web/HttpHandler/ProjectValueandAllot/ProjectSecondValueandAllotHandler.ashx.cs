﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using Newtonsoft.Json;
using TG.Model;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{

    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProjectSecondValueandAllotHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {

        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();

        #region QueryString

        public TG.Model.cm_ProjectValueAuditRecord ProjectValueAuditRecordEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectValueAuditRecord obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectValueAuditRecord>(data);
                }
                return obj;
            }
        }

        public TG.Model.ProjectValueAllotDetailEntity ProjectValueAllotDetail
        {
            get
            {
                string data = Request["data"];
                TG.Model.ProjectValueAllotDetailEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.ProjectValueAllotDetailEntity>(data);
                }
                return obj;
            }
        }

        public TG.Model.ProjectValueAllotByMemberEntity ProjectValueAllotByMemberEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.ProjectValueAllotByMemberEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.ProjectValueAllotByMemberEntity>(data);
                }
                return obj;
            }
        }
        /// <summary>
        /// 审核记录SysNo
        /// </summary>
        public int CoperationAuditSysNo
        {
            get
            {
                int coperationAuditSysNo = 0;
                int.TryParse(Request["CoperationAuditSysNo"], out coperationAuditSysNo);
                return coperationAuditSysNo;
            }
        }


        //消息ID
        public string MegID { get; set; }
        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }
        //查询或更新
        public int Flag
        {
            get
            {
                int flag = 0;
                int.TryParse(Request["flag"], out flag);
                return flag;
            }
        }
        #endregion

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            MegID = context.Request.Params["msgID"] ?? "0";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                    case 0:
                        InsertProjectValueAllotRecord();
                        break;
                    case 1:
                    case 2:
                        UpdateProjectValueAllotRecord();
                        break;
                    case 4://更新产值分配明细
                        UpdataProjectValueAllotDetail();
                        break;
                    case 5://更新人员产值信息
                        UpdataProjectValueAllotByMemberDetail();
                        break;
                    case 3:
                        DoAuditAgain();
                        break;
                }
                Response.End();
            }
        }

        /// <summary>
        /// 新规分配信息
        /// </summary>
        private void InsertProjectValueAllotRecord()
        {

            TG.Model.cm_ProjectValueAuditRecord model = new cm_ProjectValueAuditRecord();
            model.ProSysNo = ProjectValueAuditRecordEntity.ProSysNo;
            model.InUser = ProjectValueAuditRecordEntity.InUser;
            model.SecondValue = "2";
            model.Status = "A";
            int sysNo = bll.Add(model);

            Response.Write(sysNo);
        }

        /// <summary>
        /// 修改审核记录
        /// </summary>
        private void UpdateProjectValueAllotRecord()
        {
            TG.Model.cm_ProjectValueAuditRecord oldEntity = bll.GetModel(ProjectValueAuditRecordEntity.SysNo);
            //查询合同信息
            TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(oldEntity.ProSysNo);

            if (Flag == 0)
            {
                string roleUserString = "";


                if (ProjectValueAuditRecordEntity.Status == "B")
                { //取得设计总负责
                    roleUserString = GetDesignHead(pro.bs_project_Id);
                }
                else
                {
                    //最后一步
                    string roleSysNo = GetStatusProcess(9);
                    roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));
                }

                if (!string.IsNullOrEmpty(roleUserString))
                {
                    Response.Write(roleUserString);
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                #region EntityParameter
                TG.Model.cm_ProjectValueAuditRecord EntityParameter = new TG.Model.cm_ProjectValueAuditRecord
                {
                    SysNo = oldEntity.SysNo,
                    OneSuggestion = oldEntity.OneSuggestion,
                    TwoSuggstion = oldEntity.TwoSuggstion,
                    ThreeSuggsion = oldEntity.ThreeSuggsion,
                    FourSuggion = oldEntity.FourSuggion,
                    FiveSuggion = oldEntity.FiveSuggion,
                    SixSuggsion = oldEntity.SixSuggsion,
                    SevenSuggsion = oldEntity.SevenSuggsion,
                    InDate = oldEntity.InDate,
                    InUser = oldEntity.InUser,
                    AllotID = oldEntity.AllotID,
                    ProSysNo = oldEntity.ProSysNo,
                    SprcialtyAuditCount = oldEntity.SprcialtyAuditCount,
                    HeadAuditCount = oldEntity.HeadAuditCount,
                    TwoAuditUser = oldEntity.TwoAuditUser,
                    TwoAuditDate = oldEntity.TwoAuditDate,
                    TwoIsPass = oldEntity.TwoIsPass,
                    ThreeAuditUser = oldEntity.ThreeAuditUser,
                    ThreeAuditDate = oldEntity.ThreeAuditDate,
                    ThreeIsPass = oldEntity.ThreeIsPass,
                    FourAuditUser = oldEntity.FourAuditUser,
                    FourAuditDate = oldEntity.FourAuditDate,
                    FourIsPass = oldEntity.FourIsPass,
                    AuditUser = oldEntity.AuditUser,
                    AuditDate = oldEntity.AuditDate
                };
                #endregion

                #region 状态设置
                string status = ProjectValueAuditRecordEntity.Status;
                string second = oldEntity.SecondValue;
                switch (status)
                {
                    case "A":
                        EntityParameter.Status = Action == 1 ? "B" : "C";
                        break;
                    case "B"://和产值分配状态保持一致。 

                        EntityParameter.Status = Action == 1 ? "H" : "I";
                        EntityParameter.TwoSuggstion = ProjectValueAuditRecordEntity.TwoSuggstion;
                        break;
                    case "F":
                        EntityParameter.Status = Action == 1 ? "H" : "I";
                        break;
                    case "H":
                        EntityParameter.Status = Action == 1 ? "J" : "K";
                        break;
                    case "J":
                        EntityParameter.Status = Action == 1 ? "L" : "M";
                        break;
                    case "L":
                        EntityParameter.Status = Action == 1 ? "N" : "O";
                        EntityParameter.SevenSuggsion = ProjectValueAuditRecordEntity.SevenSuggsion;
                        break;
                    case "N":
                        EntityParameter.Status = Action == 1 ? "P" : "Q";
                        EntityParameter.EightSuggstion = ProjectValueAuditRecordEntity.EightSuggstion;
                        break;
                }
                #endregion


                if (EntityParameter.Status == "K")//专业负责人
                {
                    // 专业负责人审核不通过
                    int specialtyCount = oldEntity.SprcialtyAuditCount;

                    if (specialtyCount == 1)
                    {
                        EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAuditRecordEntity.AuditUser : oldEntity.AuditUser + "," + "{" + ProjectValueAuditRecordEntity.AuditUser;
                    }
                    else
                    {
                        EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAuditRecordEntity.AuditUser : oldEntity.AuditUser + ";" + ProjectValueAuditRecordEntity.AuditUser;
                    }

                    if (specialtyCount == 1)
                    {
                        EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + "{" + DateTime.Now.ToString();
                    }
                    else
                    {
                        EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + ";" + DateTime.Now.ToString();
                    }
                }
                else
                {
                    EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAuditRecordEntity.AuditUser : oldEntity.AuditUser + "," + ProjectValueAuditRecordEntity.AuditUser;
                    EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + DateTime.Now.ToString();
                }
                int userSysno = ProjectValueAuditRecordEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAuditRecordEntity.AuditUser);
                int count = bll.Update(EntityParameter);

                TG.Model.cm_ProjectValueAuditRecord tempEntity = new TG.BLL.cm_ProjectValueAuditRecord().GetModel(oldEntity.SysNo);

                if (tempEntity.Status == "P")
                {
                    //更新分配表状态
                    TG.BLL.cm_ProjectValueAllot bllAllot = new TG.BLL.cm_ProjectValueAllot();
                    TG.Model.cm_ProjectValueAllot oldAllot = bllAllot.GetModel((int)tempEntity.AllotID);

                    int countAllot = bllAllot.UpdateAllotDataInfo((int)tempEntity.AllotID, tempEntity.ProSysNo, "S");
                    int resultcount = 0;
                    if (countAllot > 0)
                    {

                        //流程走完之后要给申请人发一条消息
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                            FromUser = oldEntity.InUser,
                            InUser = userSysno,
                            MsgType = 14,
                            ToRole = "0",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "二次产值分配全部通过"),
                            QueryCondition = pro.pro_name,
                            IsDone = "B",
                            Status = "A"
                        };
                        resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    }
                    if (resultcount > 0)
                    {
                        //再给 分配钱的人都发一条消息

                        SendMessageToUser((int)tempEntity.AllotID, pro.bs_project_Id, pro.pro_name, userSysno);

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    //是审核通过的场合
                    if (Action == 1)
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                            FromUser = userSysno,
                            InUser = userSysno,
                            MsgType = 14,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "二次产值分配"),
                            QueryCondition = pro.pro_name,
                            IsDone = "A",
                            ToRole = "0",
                            Status = "A"
                        };

                        if (count < 0)
                        {
                            Response.Write("0");
                        }
                        else
                        {
                            string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                            if (!string.IsNullOrEmpty(sysMsgString))
                            {
                                Response.Write(sysMsgString);
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }

                    }
                    else //审核不通过
                    {

                        //更新分配表状态
                        TG.BLL.cm_ProjectValueAllot bllAllot = new TG.BLL.cm_ProjectValueAllot();
                        TG.Model.cm_ProjectValueAllot oldAllot = bllAllot.GetModel((int)tempEntity.AllotID);

                        int countAllotNO = bllAllot.UpdateAllotDataInfo((int)tempEntity.AllotID, tempEntity.ProSysNo, "D");
                        int resultcountNo = 0;
                        if (countAllotNO > 0)
                        {

                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                                InUser = userSysno,
                                MsgType = 14,
                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "二次产值分配不通过"),
                                QueryCondition = pro.pro_name,
                                ToRole = "0",
                                FromUser = oldEntity.InUser,
                                IsDone = "B",
                                Status = "A"
                            };
                            resultcountNo = sysMsgBLL.InsertSysMessage(sysMessageViewEntity);
                        }
                        if (resultcountNo > 0)
                        {
                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("0");
                        }

                    }
                }
            }

        }


        /// <summary>
        /// 更新产值分配明细
        /// </summary>
        private void UpdataProjectValueAllotDetail()
        {
            TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(ProjectValueAllotDetail.ProNo));

            //取得专业负责人
            ArrayList arrayList = GetSpecialtyHead(ProjectValueAllotDetail.ProNo, pro.Unit);

            if (Flag == 0)
            {
                string roleUserString = "";
                if (arrayList.Count > 0)
                {
                    roleUserString = GetSpecialtyHead(arrayList);
                }
                else
                {
                    string roleSysNo = GetStatusProcess(6);
                    roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));
                }

                if (!string.IsNullOrEmpty(roleUserString))
                {
                    Response.Write(roleUserString);
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {

                int count = bll.InsertProjectAllotValueDetatil(ProjectValueAllotDetail);

                if (count > 0)
                {
                    //向二次产值分配更新信息

                    TG.BLL.cm_ProjectSecondValueAllot secondBll = new BLL.cm_ProjectSecondValueAllot();

                    TG.Model.cm_ProjectValueAuditRecord oldEntity = bll.GetModel(ProjectValueAllotDetail.SysNo);

                    TG.Model.cm_ProjectSecondValueAllot secondModel = secondBll.GetModel(ProjectValueAllotDetail.ProNo, oldEntity.AllotID);

                    //取得建筑结构审核金额
                    decimal auditAmount = bll.GetProcessBySpeJzJgAuditAmount(oldEntity.AllotID);

                    secondBll.UpdateAuditCount(secondModel.ID, auditAmount, null);


                    #region EntityParameter
                    TG.Model.cm_ProjectValueAuditRecord EntityParameter = new TG.Model.cm_ProjectValueAuditRecord
                    {
                        SysNo = oldEntity.SysNo,
                        OneSuggestion = oldEntity.OneSuggestion,
                        TwoSuggstion = oldEntity.TwoSuggstion,
                        ThreeSuggsion = oldEntity.ThreeSuggsion,
                        FourSuggion = oldEntity.FourSuggion,
                        FiveSuggion = oldEntity.FiveSuggion,
                        SixSuggsion = oldEntity.SixSuggsion,
                        SevenSuggsion = oldEntity.SevenSuggsion,
                        InDate = oldEntity.InDate,
                        InUser = oldEntity.InUser,
                        AllotID = oldEntity.AllotID,
                        ProSysNo = oldEntity.ProSysNo,
                        HeadAuditCount = oldEntity.HeadAuditCount,
                        TwoAuditUser = oldEntity.TwoAuditUser,
                        TwoAuditDate = oldEntity.TwoAuditDate,
                        TwoIsPass = oldEntity.TwoIsPass,
                        ThreeAuditUser = oldEntity.ThreeAuditUser,
                        ThreeAuditDate = oldEntity.ThreeAuditDate,
                        ThreeIsPass = oldEntity.ThreeIsPass,
                        FourAuditUser = oldEntity.FourAuditUser,
                        FourAuditDate = oldEntity.FourAuditDate,
                        FourIsPass = oldEntity.FourIsPass
                    };
                    #endregion

                    EntityParameter.Status = "H";
                    EntityParameter.SprcialtyAuditCount = 1;
                    EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAllotDetail.AuditUser : oldEntity.AuditUser + "," + ProjectValueAllotDetail.AuditUser;
                    EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + DateTime.Now.ToString();


                    int countRecord = bll.Update(EntityParameter);
                    if (countRecord > 0)
                    {
                        //发送消息
                        TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&MessageStatus={1}", ProjectValueAllotDetail.SysNo.ToString(), EntityParameter.Status),
                            FromUser = ProjectValueAllotDetail.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotDetail.AuditUser),
                            InUser = ProjectValueAllotDetail.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotDetail.AuditUser),
                            MsgType = 14,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "二次产值分配"),
                            QueryCondition = pro.pro_name,
                            IsDone = "A",
                            Status = "A"
                        };

                        string roleSysNo = string.Empty;
                        sysMessageViewEntity.ToRole = roleSysNo;

                        sysMessageViewEntity.ToRole = "0";

                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            Response.Write(sysMsgString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }

            }
        }

        /// <summary>
        /// 保存人员产值分配
        /// </summary>
        private void UpdataProjectValueAllotByMemberDetail()
        {
            TG.Model.cm_ProjectValueAuditRecord oldEntity = bll.GetModel(ProjectValueAllotByMemberEntity.SysNo);
            TG.Model.tg_member memberModel = new TG.BLL.tg_member().GetModel(int.Parse(ProjectValueAllotByMemberEntity.AuditUser));
            //该专业已审批完毕
            if (bll.UserValueIsSaved(memberModel.mem_Speciality_ID, int.Parse(oldEntity.AllotID.ToString())))
            {
                Response.Write("10");
                //更新 状态 为代办事项   
                UpdateMessageIsDone(int.Parse(MegID));
            }
            else
            {
                //发送消息
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(ProjectValueAllotByMemberEntity.ProNo));

                int specialtyCount = oldEntity.SprcialtyAuditCount;
                //所有参与的专业
                DataTable dtPlanSpecialty = new DataTable();
                if (pro.Unit.Contains("暖通"))
                {
                    dtPlanSpecialty = GetHavcDatablePlanSpecialty(oldEntity.ProSysNo);
                }
                else
                {
                    dtPlanSpecialty = GetDatablePlanSpecialty(oldEntity.ProSysNo);
                }
                #region EntityParameter
                TG.Model.cm_ProjectValueAuditRecord EntityParameter = new TG.Model.cm_ProjectValueAuditRecord
                {
                    SysNo = oldEntity.SysNo,
                    OneSuggestion = oldEntity.OneSuggestion,
                    TwoSuggstion = oldEntity.TwoSuggstion,
                    ThreeSuggsion = oldEntity.ThreeSuggsion,
                    FourSuggion = oldEntity.FourSuggion,
                    FiveSuggion = oldEntity.FiveSuggion,
                    SixSuggsion = oldEntity.SixSuggsion,
                    SevenSuggsion = oldEntity.SevenSuggsion,
                    InDate = oldEntity.InDate,
                    InUser = oldEntity.InUser,
                    AllotID = oldEntity.AllotID,
                    ProSysNo = oldEntity.ProSysNo,
                    HeadAuditCount = oldEntity.HeadAuditCount,
                    TwoAuditUser = oldEntity.TwoAuditUser,
                    TwoAuditDate = oldEntity.TwoAuditDate,
                    TwoIsPass = oldEntity.TwoIsPass,
                    ThreeAuditUser = oldEntity.ThreeAuditUser,
                    ThreeAuditDate = oldEntity.ThreeAuditDate,
                    ThreeIsPass = oldEntity.ThreeIsPass,
                    FourAuditUser = oldEntity.FourAuditUser,
                    FourAuditDate = oldEntity.FourAuditDate,
                    FourIsPass = oldEntity.FourIsPass,
                    SprcialtyAuditCount = oldEntity.SprcialtyAuditCount
                };
                #endregion

                if (specialtyCount == dtPlanSpecialty.Rows.Count)
                {


                    int count = bll.InsertProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity);
                    EntityParameter.Status = "J";
                    if (specialtyCount == 1)
                    {
                        EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAllotByMemberEntity.AuditUser : oldEntity.AuditUser + "," + "{" + ProjectValueAllotByMemberEntity.AuditUser + "}";
                        EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + "{" + DateTime.Now.ToString() + "}";
                    }
                    else
                    {
                        EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAllotByMemberEntity.AuditUser : oldEntity.AuditUser + ";" + ProjectValueAllotByMemberEntity.AuditUser + "}";
                        EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + ";" + DateTime.Now.ToString() + "}";
                    }

                    int countRecord = bll.Update(EntityParameter);

                    if (countRecord > 0)
                    {
                        //给所有人发送消息

                        if (countRecord > 0)
                        {
                            DataTable dt = bll.GetAuditMemberList(oldEntity.ProSysNo, oldEntity.AllotID, "").Tables[0];
                            //给人员新增信息
                            int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitMemberUserInfo(dt, oldEntity.ProSysNo, oldEntity.AllotID, ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser));
                            if (countAuditMember > 0)
                            {

                                if (dt.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        //发送消息给每个人员
                                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                                        {
                                            ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}", oldEntity.AllotID, oldEntity.ProSysNo, ProjectValueAllotByMemberEntity.SysNo.ToString(), "second"),
                                            FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                            InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                                            MsgType = 25,//项目产值分配明细
                                            ToRole = "0",
                                            MessageContent = string.Format("关于{0}的二次产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                            QueryCondition = pro == null ? "" : pro.pro_name,
                                            IsDone = "A",
                                            Status = "A"
                                        };
                                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                                    }

                                }

                                //更新 状态 为代办事项   
                                UpdateMessageIsDone(int.Parse(MegID));

                                Response.Write("4");

                            }
                            else
                            {
                                Response.Write("0");
                            }

                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        Response.Write("0");
                    }

                }
                else
                {
                    int count = bll.InsertProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity);

                    EntityParameter.Status = "H";

                    if (specialtyCount == 1)
                    {
                        EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAllotByMemberEntity.AuditUser : oldEntity.AuditUser + "," + "{" + ProjectValueAllotByMemberEntity.AuditUser;
                    }
                    else
                    {
                        EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAllotByMemberEntity.AuditUser : oldEntity.AuditUser + ";" + ProjectValueAllotByMemberEntity.AuditUser;
                    }

                    if (specialtyCount == 1)
                    {
                        EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + "{" + DateTime.Now.ToString();
                    }
                    else
                    {
                        EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + ";" + DateTime.Now.ToString();
                    }

                    EntityParameter.SprcialtyAuditCount = specialtyCount + 1;

                    int countRecord = bll.Update(EntityParameter);


                    if (countRecord > 0)
                    {
                        //更新 状态 为代办事项   
                        UpdateMessageIsDone(int.Parse(MegID));
                        Response.Write("3");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }

            }
        }

        /// <summary>
        /// 重新审核修改消息状态
        /// </summary>
        private void DoAuditAgain()
        {
            //修改消息状态为已读
            new TG.BLL.cm_SysMsg().UpDateSysMsgStatusByCoperationSysNo(CoperationAuditSysNo);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string GetStatusProcess(int postion)
        {
            string sql = "select RoleSysNo from cm_ProjectFeeAllotConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            return resultObj == null ? "0" : resultObj.ToString();
        }


        /// <summary>
        /// 得到专业负责人
        /// </summary>
        /// <param name="proid"></param>
        /// <returns></returns>
        private ArrayList GetSpecialtyHead(int proid, string unitName)
        {
            DataTable dtPlanSpecialty = new DataTable();

            // 得到项目参人员的所有专业
            if (unitName.Contains("暖通"))
            {
                dtPlanSpecialty = GetHavcDatablePlanSpecialty(proid);
            }
            else
            {
                dtPlanSpecialty = GetDatablePlanSpecialty(proid);
            }

            ArrayList arrayList = new ArrayList();

            for (int i = 0; i < dtPlanSpecialty.Rows.Count; i++)
            {
                string speName = dtPlanSpecialty.Rows[i]["spe_Name"].ToString();
                //判断该专业下面是否有专业负责人

                DataTable dtPlanHeadUserInfo = GetPlanUserInfoBySpeName(speName, proid, 2);
                if (dtPlanHeadUserInfo.Rows.Count > 0)
                {
                    for (int j = 0; j < dtPlanHeadUserInfo.Rows.Count; j++)
                    {
                        arrayList.Add(new { UserSysNo = dtPlanHeadUserInfo.Rows[j]["mem_ID"].ToString(), UserName = dtPlanHeadUserInfo.Rows[j]["mem_Name"].ToString(), RoleName = dtPlanHeadUserInfo.Rows[j]["spe_Name"].ToString() });
                    }
                }
                else
                {
                    //取得该专业下面的所有人员
                    DataTable dtPlanNoHeadUserInfo = GetPlanUserInfoBySpeName(speName, proid, 0);
                    for (int k = 0; k < dtPlanNoHeadUserInfo.Rows.Count; k++)
                    {
                        arrayList.Add(new { UserSysNo = dtPlanNoHeadUserInfo.Rows[k]["mem_ID"].ToString(), UserName = dtPlanNoHeadUserInfo.Rows[k]["mem_Name"].ToString(), RoleName = dtPlanNoHeadUserInfo.Rows[k]["spe_Name"].ToString() });
                    }
                }
            }

            return arrayList;
        }


        /// <summary>
        /// 专业负责人
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        private string GetSpecialtyHead(ArrayList array)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = "专业负责人", UserList = array });
        }

        /// <summary>
        /// 更新产值分配上一个状态
        /// </summary>
        /// <param name="status"></param>
        /// <param name="auditUser"></param>
        /// <param name="auditDate"></param>
        private void UpdataValueRecordPrevStatus(string status, string auditUser, string auditDate, int id)
        {
            string sql = @"UPDATE cm_ProjectValueAuditRecord SET Status='" + status + "' , AuditUser='" + auditUser + "',  AuditDate='" + auditDate + "' WHERE SysNo=" + id + " ";

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 取得项目参与人员的所有专业
        /// </summary>
        /// <param name="cprID"></param>
        /// <returns></returns>
        private DataTable GetDatablePlanSpecialty(int proID)
        {
            // 得到项目参人员的所有专业
            DataTable dtPlanSpecialty = bll.GetPlanUserSpecialty(proID).Tables[0];

            return dtPlanSpecialty;
        }

        /// <summary>
        /// 取得项目参与人员的所有专业
        /// </summary>
        /// <param name="cprID"></param>
        /// <returns></returns>
        private DataTable GetHavcDatablePlanSpecialty(int proID)
        {
            // 得到项目参人员的所有专业
            DataTable dtPlanSpecialty = bll.GetPlanUserSpecialtyByHaveProject(proID).Tables[0];

            return dtPlanSpecialty;
        }

        /// <summary>
        /// 给每个人分配到的人员发送消息
        /// </summary>
        /// <param name="allotID"></param>
        protected void SendMessageToUser(int allotID, int cprID, string cprName, int userSysno)
        {

            string sql = string.Format("select  distinct mem_ID  from cm_ProjectValueByMemberDetails where AllotID={0} and IsExternal='0' and TranType is null", allotID);
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //发送消息给每个人员
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("AllotID={0}&proID={1}", allotID, cprID),
                        FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                        InUser = userSysno,
                        MsgType = 10,//项目产值分配明细
                        ToRole = "0",
                        MessageContent = string.Format("关于{0}的二次产值分配个人明细信息！", cprName),
                        QueryCondition = cprName,
                        IsDone = "B",
                        Status = "A"
                    };
                    new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                }

            }

        }

        /// <summary>
        /// 得到改项目的设计总负责
        /// </summary>
        /// <param name="proid"></param>
        private string GetDesignHead(int proid)
        {
            ArrayList arrayList = new ArrayList();
            string sql = @"SELECT p.pro_ID,M.mem_ID, mem_Name  FROM   dbo.tg_relation R
                                INNER JOIN tg_member M
                                 ON R.mem_ID=M.mem_ID  
                                 INNER JOIN cm_Project p 
                                 ON R.pro_ID=p.ReferenceSysNo
                                WHERE (mem_RoleIDs like N'%1%' or mem_RoleIDs like N'%6%') and p.pro_ID=" + proid + "";
            //所长
            SqlDataReader reader = TG.DBUtility.DbHelperSQL.ExecuteReader(sql);

            while (reader.Read())
            {
                arrayList.Add(new { UserSysNo = reader["mem_ID"], UserName = reader["mem_Name"] });
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = "设总", UserList = arrayList });
        }

        /// <summary>
        /// 根据专业获取该专业下面的人员信息
        /// </summary>
        /// <param name="speName"></param>
        /// <param name="proID"></param>
        /// <returns></returns>
        private DataTable GetPlanUserInfoBySpeName(string speName, int proID, int roleID)
        {
            string sql = "";
            if (roleID == 0)
            {
                sql = @" SELECT p.pro_ID,M.mem_ID, mem_Name, mem_RoleIDs ,s.spe_Name,s.spe_ID FROM   dbo.tg_relation R
                                INNER JOIN tg_member M
                                 ON R.mem_ID=M.mem_ID
                                 join tg_speciality  s on s.spe_ID = m.mem_Speciality_ID
                                 INNER JOIN cm_Project p 
                                 ON R.pro_ID=p.ReferenceSysNo
                                WHERE  s.spe_Name='" + speName + "' AND  p.pro_ID=" + proID;
            }
            else
            {
                sql = @" SELECT p.pro_ID,M.mem_ID, mem_Name, mem_RoleIDs ,s.spe_Name,s.spe_ID FROM   dbo.tg_relation R
                                INNER JOIN tg_member M
                                 ON R.mem_ID=M.mem_ID
                                 join tg_speciality  s on s.spe_ID = m.mem_Speciality_ID
                                 INNER JOIN cm_Project p 
                                 ON R.pro_ID=p.ReferenceSysNo
                                WHERE mem_RoleIDs like N'%2%' and s.spe_Name='" + speName + "' AND  p.pro_ID=" + proID;
            }
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];

            return dt;

        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="mesID"></param>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        private void UpdateMessageIsDone(int mesID)
        {
            string sql = "update cm_SysMsg set IsDone =N'D' Where SysNo=" + mesID + "";

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 获取所长
        /// </summary>
        /// <returns></returns>
        public string GetProjectPrincipalship(string unitName)
        {
            ArrayList arrayList = new ArrayList();

            //经济所长
            string sqlnts = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_Name like'%" + unitName + "%' and   C.pri_Name LIKE '%所长%' ";

            SqlDataReader readernts = TG.DBUtility.DbHelperSQL.ExecuteReader(sqlnts);

            while (readernts.Read())
            {
                arrayList.Add(new { UserSysNo = readernts["UserSysNo"], UserName = readernts["mem_Name"], RoleName = "所长" });
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = "所长", UserList = arrayList });

        }
    }
}