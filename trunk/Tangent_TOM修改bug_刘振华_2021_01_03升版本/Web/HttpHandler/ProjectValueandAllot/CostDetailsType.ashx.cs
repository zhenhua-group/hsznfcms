﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    /// <summary>
    /// CostDetailsType 的摘要说明
    /// </summary>
    public class CostDetailsType : IHttpHandler
       {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //不让浏览器保存缓存
            context.Response.Buffer = true;
            context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            string action = context.Request.Params["action"];
            string unitid = context.Request.Params["unitid"];
            string typeid = context.Request.Params["typeid"];
            string starttime = context.Request.Params["starttime"];
            string endtime = context.Request.Params["endtime"];
            string unitName = context.Request.Params["unit"];
            if (action == "consttype")
            {

                StringBuilder builder = new StringBuilder();
                TG.BLL.cm_CostType costBll = new TG.BLL.cm_CostType();
                if (unitid == "250")//252/257
                {
                    builder.Append(" isinput=0 and costgroup in (0,1,5,6,7)");
                }
                else if (unitid == "252")
                {
                    builder.Append(" isinput=0 and costgroup in (0,2,5,6,7)");
                }
                else if (unitid == "257")
                {
                    builder.Append(" isinput=0 and costgroup in (0,3,5,7)");
                }
                else if (unitid == "-1")
                {
                    builder.Append(" isinput=0 and costgroup in (0,5,6,7)");
                }
                else
                {
                    builder.Append(" isinput=0 and costgroup in (0,4,5,6)");
                }
                DataTable dt = costBll.GetList(builder.ToString()).Tables[0];
                context.Response.Write(TableToJson(dt));
            }
            else if (action == "consttypeall")
            {

                StringBuilder builder = new StringBuilder();
                TG.BLL.cm_CostType costBll = new TG.BLL.cm_CostType();
                if (unitid == "250")//252/257
                {
                    builder.Append(" costgroup in (0,1)");
                }
                else if (unitid == "252")
                {
                    builder.Append("  costgroup in (0,2)");
                }
                else if (unitid == "257")
                {
                    builder.Append(" costgroup in (0,3)");
                }
                else if (unitid == "-1")
                {
                    builder.Append(" costgroup in (0)");
                }
                else
                {
                    builder.Append(" costgroup in (0,4)");
                }
                DataTable dt = costBll.GetList(builder.ToString()).Tables[0];
                context.Response.Write(TableToJson(dt));
            }
            //根据部门查询
            else if (action == "costUnit")
            {
                string sqlCost = @" SELECT c.* ,(SELECT SUM(s.costcharge) from cm_CostDetails as s where s.costTypeID=c.id {0}
  )as charge FROM  cm_CostType as c where costGroup IN (0,{1})";
                StringBuilder builder1 = new StringBuilder();
                StringBuilder builder2 = new StringBuilder();
                //年份
                if (starttime != "" && endtime != "")
                {
                    builder1.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", starttime, endtime);
                }
                //年份
                if (starttime == "" && endtime != "")
                {
                    builder1.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), endtime);
                }
                //年份
                if (starttime != "" && endtime == "")
                {
                    builder1.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", starttime, DateTime.Now.ToString("yyyy-MM-dd"));
                }
                if (unitid == "250")//252/257
                {
                    sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "1");
                }
                else if (unitid == "252")
                {
                    sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "2");
                }
                else if (unitid == "257")
                {
                    sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "3");
                }
                else if (unitid == "-1")
                {
                    sqlCost = string.Format(sqlCost, builder1, "4");
                }
                else
                {
                    sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "4");
                }
                if (!string.IsNullOrEmpty(typeid) && typeid != "-1")
                {
                    sqlCost += "and c.id=" + typeid;
                }
                DataTable dtCost = TG.DBUtility.DbHelperSQL.Query(sqlCost) == null ? null : TG.DBUtility.DbHelperSQL.Query(sqlCost).Tables[0];
                List<CostListModel> GZlist = new List<CostListModel>();
                List<CostListModel> FYlist = new List<CostListModel>();
                bool isAddGZCost = false;
                bool isAddFYCost = false;
                if (dtCost != null)
                {
                    for (int i = 0; i < dtCost.Rows.Count; i++)
                    {
                        CostListModel model = new CostListModel();
                        if (dtCost.Rows[i]["costName"].ToString().Contains("工资"))
                        {
                            isAddGZCost = true;
                            model.CostName = dtCost.Rows[i]["costName"].ToString();
                            model.CosttypeID = dtCost.Rows[i]["id"].ToString();
                            if (dtCost.Rows[i]["charge"] != null && dtCost.Rows[i]["charge"].ToString() != "")
                            {
                                model.AllCharge = Convert.ToDecimal(dtCost.Rows[i]["charge"].ToString());
                            }
                            else
                            {
                                model.AllCharge = 0.00m;
                            }
                            GZlist.Add(model);
                        }
                        else
                        {
                            isAddFYCost = true;
                            model.CostName = dtCost.Rows[i]["costName"].ToString();
                            model.CosttypeID = dtCost.Rows[i]["id"].ToString();
                            if (dtCost.Rows[i]["charge"] != null && dtCost.Rows[i]["charge"].ToString() != "")
                            {
                                model.AllCharge = Convert.ToDecimal(dtCost.Rows[i]["charge"].ToString());
                            }
                            else
                            {
                                model.AllCharge = 0.00m;
                            }
                            FYlist.Add(model);
                        }
                    }
                }
                CostListModel GZmodel = new CostListModel();
                CostListModel FYmodel = new CostListModel();
                CostListModel Allmodel = new CostListModel();
                foreach (var item in GZlist)
                {
                    GZmodel.AllCharge += item.AllCharge;
                }
                GZmodel.CostName = "工资合计";
                GZmodel.CosttypeID = "gz";
                if (typeid == "-1")
                {
                    GZlist.Add(GZmodel);
                }
                foreach (var item in FYlist)
                {
                    FYmodel.AllCharge += item.AllCharge;
                }
                FYmodel.CostName = "费用成本合计";
                FYmodel.CosttypeID = "fy";
                if (typeid == "-1")
                {
                    FYlist.Add(FYmodel);
                }
                Allmodel.AllCharge = FYmodel.AllCharge + GZmodel.AllCharge;
                Allmodel.CostName = "总合计";
                Allmodel.CosttypeID = "all";
                if (typeid == "-1")
                {
                    FYlist.Add(Allmodel);
                }

                GZlist.AddRange(FYlist);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(GZlist);
                context.Response.Write(json);
            }
            else if (action == "unittypeall")
            {
                StringBuilder builder = new StringBuilder();
                TG.BLL.tg_unit bllunit = new TG.BLL.tg_unit();
                if (unitid == "4")//生产部门
                {
                    string strlist = "0,";
                    List<TG.Model.cm_DropUnitList> list = new TG.BLL.cm_DropUnitList().GetModelList(" flag=1 ");
                    if (list.Count > 0)
                    {
                        foreach (TG.Model.cm_DropUnitList model in list)
                        {
                            strlist += model.unit_ID.ToString() + ",";
                        }
                    }
                    strlist += "236,250,251,252,253,254,255,256,257,258,259";
                    strlist = strlist.Remove(strlist.Length - 1);
                    builder.Append(" unit_ID not in(" + strlist + ")");

                }
                else if (unitid == "3")//承包
                {
                    builder.Append(" unit_ID =257");
                }
                else if (unitid == "2")//施工图
                {
                    builder.Append(" unit_ID =252");
                }
                else if (unitid == "1")//勘察分院
                {
                    builder.Append(" unit_ID =250");
                }
                DataTable dt = bllunit.GetList(builder.ToString()).Tables[0];
                context.Response.Write(TableToJson(dt));
            }
            else if (action == "unittypevalue")
            {
                StringBuilder srtime = new StringBuilder();
                StringBuilder sbunit = new StringBuilder();
                if (starttime != "" && endtime != "")
                {
                    srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);
                    //   srtime.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", starttime, endtime);
                }
                //年份
                if (starttime == "" && endtime != "")
                {
                    srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);
                    // srtime.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), endtime);
                }
                //年份
                if (starttime != "" && endtime == "")
                {
                    srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);
                    // srtime.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", starttime, DateTime.Now.ToString("yyyy-MM-dd"));
                }
                if (unitName != "-1")
                {
                    sbunit.Append("and unit_Name like '%" + unitName + "%'");

                }
                else
                {
                    if (unitid == "4")
                    {
                        string strlist = "0,";
                        List<TG.Model.cm_DropUnitList> list = new TG.BLL.cm_DropUnitList().GetModelList(" flag=1 ");
                        if (list.Count > 0)
                        {
                            foreach (TG.Model.cm_DropUnitList model in list)
                            {
                                strlist += model.unit_ID.ToString() + ",";
                            }
                        }
                        strlist += "236,250,251,252,253,254,255,256,257,258,259";
                        strlist = strlist.Remove(strlist.Length - 1);
                        sbunit.Append("and unit_ID not in(" + strlist + ")");
                        //sbunit.Append("and unit_ID not in (236,250,251,252,253,254,255,256,257,258,259)");
                    }
                }
                string SRsql = @"SELECT *,(SELECT isnull(SUM(ch.acount),0.00) from cm_ProjectCharge ch left JOIN cm_Coperation cop on ch.cprID=cop.cpr_Id
WHERE cop.cpr_Unit=unit_Name AND ch.Status='E' {0}
 ) as ChargeAcount
 FROM tg_unit where 1=1 {1}";
                SRsql = string.Format(SRsql, srtime.ToString(), sbunit.ToString());
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(SRsql).Tables[0];
                context.Response.Write(TableToJson(dt));
            }
        }
        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
    public partial class CostListModel
    {
        private string costName;

        public string CostName
        {
            get { return costName; }
            set { costName = value; }
        }
        private decimal allCharge;

        public decimal AllCharge
        {
            get { return allCharge; }
            set { allCharge = value; }
        }
        private string costtypeID;

        public string CosttypeID
        {
            get { return costtypeID; }
            set { costtypeID = value; }
        }
    }
}