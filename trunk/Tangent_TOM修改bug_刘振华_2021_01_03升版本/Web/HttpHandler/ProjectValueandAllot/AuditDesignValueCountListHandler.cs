﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    public abstract class AuditDesignValueCountListHandler : IHttpHandler
    {
        //存储过程名
        public abstract string ProcName { get; }

        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }

        //存储过程参数
        protected TG.BLL.ProjectValueLendBorrowParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.ProjectValueLendBorrowParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];
            string type = context.Request.QueryString["type"];
            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.ProjectValueLendBorrowParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }
           
            //加载数据
            if (strOper == null && strAction == null)
            {
                string year = DateTime.Now.Year.ToString();
                string sqlwhere = " ";
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters.Year = year;
                }
                else
                {
                    parameters.Year = "null";
                }
                parameters.Where = sqlwhere + strWhere;
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {

            }

            //部门产值的分配查询
            else if (strAction == "sel")
            {
                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];

                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //绑定单位
                if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND Unit='" + unit + "' ");
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters.Year = year;
                }
                else
                {
                    parameters.Year = "null";
                }


                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }

        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }

    }
}
