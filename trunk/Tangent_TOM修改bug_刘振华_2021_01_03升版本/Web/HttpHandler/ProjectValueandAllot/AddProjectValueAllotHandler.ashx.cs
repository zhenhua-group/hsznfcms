﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using TG.Model;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using TG.DBUtility;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    /// <summary>
    /// AddProjectValueAllotHandler 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class AddProjectValueAllotHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {

        public TG.Model.cm_ProjectValueAuditRecord ProjectValueAuditRecordEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectValueAuditRecord obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectValueAuditRecord>(data);
                }
                return obj;
            }
        }

        public TG.Model.cm_ProjectValueAllot ProjectValueAllotEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectValueAllot obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    obj = js.Deserialize<TG.Model.cm_ProjectValueAllot>(data);
                    //obj = Newtonsoft.Json.JsonConvert.DeserializeObject(data, typeof(TG.Model.cm_ProjectValueAllot)) as TG.Model.cm_ProjectValueAllot;
                }
                return obj;
            }
        }
        public string Flag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            if (Action == "0")
            {
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(ProjectValueAllotEntity.pro_ID));
                if (Flag == "0")
                {
                    // string isTrunEconomy = ProjectValueAllotEntity.IsTrunEconomy;
                    //string IsTrunHavc = ProjectValueAllotEntity.IsTrunHavc;
                    string isTrunEconomy = "1";//转暖通和转经济 不管是否参与 都要确认
                    string IsTrunHavc = "1";
                    string roleSysNoString = GetProjectValueRoleName(pro.Unit.Trim(), isTrunEconomy, IsTrunHavc);

                    if (!string.IsNullOrEmpty(roleSysNoString))
                    {
                        Response.Write(roleSysNoString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    var modelEntity = ProjectValueAllotEntity;

                    TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();
                    TG.Model.cm_ProjectValueAllot model = new TG.Model.cm_ProjectValueAllot();
                    TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();

                    string proid = modelEntity.pro_ID.ToString();

                    model.pro_ID = modelEntity.pro_ID;
                    //分配次数
                    int i_count = 0;
                    string str_where = " Select Count(ID) From cm_ProjectValueAllot Where SecondValue='1' and  pro_ID=" + proid;
                    DataSet ds = bll_db.GetList(str_where);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            i_count = int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1;
                        }
                    }
                    model.AllotTimes = "第" + i_count + "次产值分配";

                    decimal i_persent = 0;
                    decimal payshicount = Convert.ToDecimal(modelEntity.Payshicount);
                    decimal allotCount = modelEntity.AllotCount;
                    if (payshicount > 0 && allotCount > 0)
                    {
                        i_persent = (allotCount / payshicount) * 100;
                    }
                    #region 属性赋值
                    model.persent = System.Math.Round(i_persent, 2);
                    model.AllotUser = modelEntity.AllotUser;
                    model.AllotDate = DateTime.Now;
                    model.AllotCount = allotCount;
                    model.PaidValuePercent = modelEntity.PaidValuePercent;
                    model.PaidValueCount = modelEntity.PaidValueCount;
                    model.DesignManagerPercent = modelEntity.DesignManagerPercent;
                    model.DesignManagerCount = modelEntity.DesignManagerCount;
                    model.EconomyValuePercent = modelEntity.EconomyValuePercent == null ? 0 : modelEntity.EconomyValuePercent;
                    model.EconomyValueCount = modelEntity.EconomyValueCount;
                    model.UnitValuePercent = modelEntity.UnitValuePercent;
                    model.UnitValueCount = modelEntity.UnitValueCount;
                    model.AllotValuePercent = modelEntity.AllotValuePercent;
                    model.Allotvaluecount = modelEntity.Allotvaluecount;
                    model.Itemtype = modelEntity.Itemtype;
                    model.Otherdeptallotpercent = modelEntity.Otherdeptallotpercent;
                    model.Otherdeptallotcount = modelEntity.Otherdeptallotcount;
                    model.Thedeptallotpercent = modelEntity.Thedeptallotpercent;
                    model.Thedeptallotcount = modelEntity.Thedeptallotcount;
                    model.HavcValuePercent = modelEntity.HavcValuePercent == null ? 0 : modelEntity.HavcValuePercent;
                    model.HavcValueCount = modelEntity.HavcValueCount;
                    model.ProgramPercent = modelEntity.ProgramPercent;
                    model.ProgramCount = modelEntity.ProgramCount;
                    model.ShouldBeValuePercent = modelEntity.ShouldBeValuePercent;
                    model.ShouldBeValueCount = modelEntity.ShouldBeValueCount;
                    model.Status = "A";
                    model.mark = "";
                    model.SecondValue = modelEntity.SecondValue;
                    model.ActualAllountTime = modelEntity.ActualAllountTime;
                    model.FinanceValuePercent = modelEntity.FinanceValuePercent;
                    model.FinanceValueCount = modelEntity.FinanceValueCount;
                    model.TheDeptShouldValuePercent = modelEntity.TheDeptShouldValuePercent;
                    model.TheDeptShouldValueCount = modelEntity.TheDeptShouldValueCount;
                    model.DividedPercent = modelEntity.DividedPercent;
                    #endregion
                    int affectRow = 0;
                    affectRow = bll.Add(model);
                    if (affectRow > 0)
                    {
                        //向二次产值分配添加信息
                        TG.Model.cm_ProjectSecondValueAllot secondModel = new cm_ProjectSecondValueAllot();
                        TG.BLL.cm_ProjectSecondValueAllot secondBll = new BLL.cm_ProjectSecondValueAllot();
                        secondModel.AllotID = affectRow;
                        secondModel.ProID = modelEntity.pro_ID;
                        secondModel.TheDeptValueCount = modelEntity.Thedeptallotcount;
                        secondModel.HavcCount = 0;
                        secondModel.Status = "A";
                        secondModel.SecondValue = "1";
                        secondModel.ActualAllountTime = modelEntity.ActualAllountTime;
                        secondBll.Add(secondModel);

                        //更改分配记录状态
                        TG.BLL.cm_ProjectValueAuditRecord bllRecord = new TG.BLL.cm_ProjectValueAuditRecord();

                        TG.Model.cm_ProjectValueAuditRecord modelRecord = new cm_ProjectValueAuditRecord();
                        modelRecord.ProSysNo = proid == "" ? 0 : int.Parse(proid);
                        modelRecord.InUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser);
                        modelRecord.SecondValue = "1";
                        modelRecord.Status = "A";
                        modelRecord.AllotID = affectRow;
                        modelRecord.AuditUser = modelEntity.AllotUser;
                        modelRecord.AuditDate = DateTime.Now.ToString();

                        int recordRow = bllRecord.Add(modelRecord);

                        if (recordRow > 0)
                        {
                            //发送消息
                            TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                            string sysMsgString = "";
                            if (pro.Unit.Contains("暖通"))
                            {
                                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&MessageStatus={1}", recordRow.ToString(), "A"),
                                    FromUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                                    InUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                                    MsgType = 15,
                                    MessageContent = string.Format("关于暖通所 \"{0}\" 的{1}消息！", pro.pro_name, "产值分配"),
                                    QueryCondition = pro.pro_name,
                                    IsDone = "A",
                                    Status = "A",
                                    ToRole = "0"
                                };
                                sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                            }
                            else
                            {
                                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&MessageStatus={1}", recordRow.ToString(), "A"),
                                    FromUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                                    InUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                                    MsgType = 5,
                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "产值分配"),
                                    QueryCondition = pro.pro_name,
                                    IsDone = "A",
                                    Status = "A",
                                    ToRole = "0"
                                };
                                sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                            }

                            if (!string.IsNullOrEmpty(sysMsgString))
                            {
                                Response.Write(sysMsgString);
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }

            }
            else if (Action == "1")
            {
                int count = TG.DBUtility.DbHelperSQL.ExecuteSql("delete cm_ProjectValueAuditRecord where SysNo=" + ProjectValueAuditRecordEntity.SysNo);
                Response.Write(count);
            }//二次产值分配
            else if (Action == "2")
            {
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(ProjectValueAllotEntity.pro_ID));
                if (Flag == "0")
                {
                    //string roleSysNoString = GetProjectValueRoleName(pro.Unit.Trim(), "0", "0");
                    //取得设总
                    string roleSysNoString = GetDesignHead(pro.bs_project_Id); ;

                    if (!string.IsNullOrEmpty(roleSysNoString))
                    {
                        Response.Write(roleSysNoString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    var modelEntity = ProjectValueAllotEntity;

                    TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();
                    TG.Model.cm_ProjectValueAllot model = new TG.Model.cm_ProjectValueAllot();
                    TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();

                    string proid = modelEntity.pro_ID.ToString();

                    model.pro_ID = modelEntity.pro_ID;
                    //分配次数
                    int i_count = 0;
                    string str_where = " Select Count(ID) From cm_ProjectValueAllot Where SecondValue='2' and  pro_ID=" + proid;
                    DataSet ds = bll_db.GetList(str_where);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            i_count = int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1;
                        }
                    }
                    model.AllotTimes = "第" + i_count + "次产值二次分配";
                    model.AllotUser = modelEntity.AllotUser;
                    model.AllotDate = DateTime.Now;
                    model.AllotCount = modelEntity.AllotCount;
                    model.SecondValue = "2";
                    model.Status = "A";
                    model.Itemtype = modelEntity.Itemtype;
                    model.ActualAllountTime = modelEntity.ActualAllountTime;
                    model.BulidType = modelEntity.BulidType;
                    string mark = context.Request.Params["mark"] ?? "";
                    model.mark = mark;
                    int affectRow = 0;
                    affectRow = bll.Add(model);

                    if (affectRow > 0)
                    {
                        //向二次产值分配添加信息
                        TG.Model.cm_ProjectSecondValueAllot secondModel = new cm_ProjectSecondValueAllot();
                        TG.BLL.cm_ProjectSecondValueAllot secondBll = new BLL.cm_ProjectSecondValueAllot();
                        secondModel.AllotID = affectRow;
                        secondModel.ProID = modelEntity.pro_ID;
                        secondModel.TheDeptValueCount = 0;
                        secondModel.HavcCount = 0;
                        secondModel.Status = "A";
                        secondModel.SecondValue = "2";
                        secondModel.ActualAllountTime = modelEntity.ActualAllountTime;
                        secondBll.Add(secondModel);

                        //更改分配记录状态
                        TG.BLL.cm_ProjectValueAuditRecord bllRecord = new TG.BLL.cm_ProjectValueAuditRecord();

                        TG.Model.cm_ProjectValueAuditRecord modelRecord = new cm_ProjectValueAuditRecord();
                        modelRecord.ProSysNo = proid == "" ? 0 : int.Parse(proid);
                        modelRecord.InUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser);
                        modelRecord.SecondValue = "2";
                        modelRecord.Status = "F";
                        modelRecord.AllotID = affectRow;
                        modelRecord.AuditUser = modelEntity.AllotUser;
                        modelRecord.AuditDate = DateTime.Now.ToString();

                        int recordRow = bllRecord.Add(modelRecord);

                        if (recordRow > 0)
                        {
                            //发送消息
                            TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&MessageStatus={1}", recordRow.ToString(), "F"),
                                FromUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                                InUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                                MsgType = 14,
                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "二次产值分配"),
                                QueryCondition = pro.pro_name,
                                IsDone = "A",
                                Status = "A",
                                ToRole = "0"
                            };

                            string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                            if (!string.IsNullOrEmpty(sysMsgString))
                            {
                                Response.Write(sysMsgString);
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }//经济所产值分配
            else if (Action == "3")
            {
                var modelEntity = ProjectValueAllotEntity;

                TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();
                TG.Model.cm_ProjectValueAllot model = new TG.Model.cm_ProjectValueAllot();
                TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();

                string proid = modelEntity.pro_ID.ToString();

                model.pro_ID = modelEntity.pro_ID;
                //分配次数
                int i_count = 0;
                string str_where = " Select Count(ID) From cm_ProjectValueAllot Where SecondValue='1' and  pro_ID=" + proid;
                DataSet ds = bll_db.GetList(str_where);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        i_count = int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1;
                    }
                }
                model.AllotTimes = "第" + i_count + "次产值二次分配";

                decimal i_persent = 0;
                decimal payshicount = Convert.ToDecimal(modelEntity.Payshicount);
                decimal allotCount = modelEntity.AllotCount;
                if (payshicount > 0 && allotCount > 0)
                {
                    i_persent = (allotCount / payshicount) * 100;
                }
                model.persent = System.Math.Round(i_persent, 2);
                model.AllotUser = modelEntity.AllotUser;
                model.AllotDate = DateTime.Now;
                model.AllotCount = allotCount;
                model.PaidValuePercent = modelEntity.PaidValuePercent;
                model.PaidValueCount = modelEntity.PaidValueCount;
                model.DesignManagerPercent = modelEntity.DesignManagerPercent;
                model.DesignManagerCount = modelEntity.DesignManagerCount;
                model.Itemtype = modelEntity.Itemtype;
                model.Thedeptallotpercent = modelEntity.Thedeptallotpercent;
                model.Thedeptallotcount = modelEntity.Thedeptallotcount;
                model.ProgramPercent = modelEntity.ProgramPercent;
                model.ProgramCount = modelEntity.ProgramCount;
                model.ShouldBeValuePercent = modelEntity.ShouldBeValuePercent;
                model.ShouldBeValueCount = modelEntity.ShouldBeValueCount;
                model.Status = "A";
                model.SecondValue = "1";
                model.ActualAllountTime = modelEntity.ActualAllountTime;
                int affectRow = 0;
                affectRow = bll.Add(model);

                Response.Write(affectRow);

            }//获取实收产值
            else if (Action == "4")
            {
                string cprid = context.Request.Params["cprId"] ?? "0";
                string year = context.Request.Params["year"] ?? "0";
                string proID = context.Request.Params["pro_id"] ?? "0";
                TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();
                decimal payShiCount = bll.GetPayShiValueCurrent(year, int.Parse(cprid));
                decimal allountAcount = bll.GetAllotCount(year, int.Parse(proID));
                Response.Write("{\"payShiCount\":" + payShiCount + ",\"allountAcount\":" + allountAcount + "}");
            }
            //获取二次产值
            else if (Action == "5")
            {
                string year = context.Request.Params["year"] ?? "0";
                string unitName = context.Request.Params["unitName"] ?? "0";
                TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();
                DataTable dt = bll.GetSecondValueCurrent(year, unitName).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
            }
            else if (Action == "6")
            {
                string allotID = context.Request.Params["allotID"] ?? "0";
                string proID = context.Request.Params["proID"] ?? "0";
                string loanCount = context.Request.Params["loanCount"] ?? "0";
                string stage = context.Request.Params["stage"] ?? "0";
                string type = context.Request.Params["type"] ?? "";
                string actulCount = context.Request.Params["actulCount"] ?? "0";
                string mark = context.Request.Params["mark"] ?? "";
                string borrowCount = context.Request.Params["borrowCount"] ?? "0";

                string strWhere = "update cm_ProjectValueAllot set ItemType=" + stage + " ,BulidType='" + type + "',LoanValueCount=" + decimal.Parse(loanCount) + ",ActualAllotCount=" + decimal.Parse(actulCount) + " ,mark='" + mark + "',BorrowValueCount=" + borrowCount + ",IsTAllPass=0 where ID=" + allotID + "";

                int rows = DbHelperSQL.ExecuteSql(strWhere);
                context.Response.Write(rows);

                //TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();                
                //int count = bll.UpdateProjectAllotValueProcess(strWhere, int.Parse(proID), int.Parse(allotID), decimal.Parse(loanCount));

            }
            else if (Action == "8")//全部通过
            {
                string allotID = context.Request.Params["allotID"] ?? "0";
                string proID = context.Request.Params["proID"] ?? "0";
                string loanCount = context.Request.Params["loanCount"] ?? "0";
                string actulCount = context.Request.Params["actulCount"] ?? "0";


                string strWhere = "update cm_ProjectValueAllot set LoanValueCount=" + decimal.Parse(loanCount) + ",ActualAllotCount=" + decimal.Parse(actulCount) + " ,IsTAllPass=1,BorrowValueCount=0,ItemType=null,BulidType=null where ID=" + allotID + "";

                int rows = DbHelperSQL.ExecuteSql(strWhere);
                context.Response.Write(rows);

                //TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();                
                //int count = bll.UpdateProjectAllotValueProcess(strWhere, int.Parse(proID), int.Parse(allotID), decimal.Parse(loanCount));

            }
            else if (Action == "7")
            {
                //或者院分成系数
                string year = context.Request.Params["year"] ?? DateTime.Now.Year.ToString();
                string unitName = context.Request.Params["unitName"] ?? "0";
                TG.BLL.cm_Divideinfo bll = new TG.BLL.cm_Divideinfo();
                DataTable dt = bll.GetDividedByYear(year, unitName).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
                else
                {
                    context.Response.Write("");
                }
            }
            else if (Action == "IsPlaned")
            {
                string proID = context.Request.Params["proID"] ?? "0";
                string sql1 = @"select distinct p1.pro_ID from cm_Project  p1
                           inner join  tg_project p2 on p1.ReferenceSysNo=p2.pro_ID inner join tg_relation r on r.pro_ID=p2.pro_ID  left join cm_ProjectPlanAudit au on au.ProjectSysNo=p1.pro_ID  and au.Status not in ('C','E','G')  where 1=1 and p1.pro_id=" + proID + "";
                object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql1);
                if (resultObj == null)
                {
                    context.Response.Write("0");//未策划过
                }
                else
                {
                    context.Response.Write("1");//已策划过
                }
                //                string sql = @"  select COUNT(pro_ID) from cm_Project where  Pro_number 
                //                              in (select Pro_number from cm_Project where pro_ID=" + proID + ")  and Pro_number is not null     ";
                //                DataTable dt_proNumber = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
                //                int count = int.Parse(dt_proNumber.Rows[0][0].ToString());
                //                if (count > 1)
                //                {
                //                    context.Response.Write("2");//多个编号
                //                }
                //                else
                //                {
                //                    string sql1 = @"select distinct p1.pro_ID from cm_Project  p1
                //                           inner join  tg_project p2 on p1.ReferenceSysNo=p2.pro_ID inner join tg_relation r on r.pro_ID=p2.pro_ID  left join cm_ProjectPlanAudit au on au.ProjectSysNo=p1.pro_ID  and au.Status not in ('C','E','G')  where 1=1 and p1.pro_id=" + proID + "";
                //                    object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql1);
                //                    if (resultObj == null)
                //                    {
                //                        context.Response.Write("0");//未策划过
                //                    }
                //                    else
                //                    {
                //                        context.Response.Write("1");//已策划过
                //                    }
                //                }

            }
            else if (Action == "deleteAuditValue")//删除
            {
                string pro_id = context.Request.Params["pro_id"] ?? "0";
                string allot_id = context.Request.Params["allot_id"] ?? "0";
                string audit_id = context.Request.Params["audit_id"] ?? "0";
                string type = context.Request.Params["type"] ?? "";
                TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();
                int count = bll.DeleteProjectValue(pro_id, allot_id, audit_id, type);
                context.Response.Write(count);
            }
        }


        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        public string GetProjectValueRoleName(string unitName, string isTrunEconomy, string isTranHave)
        {
            ArrayList arrayList = new ArrayList();

            //查询该所长SysNo
            string departmentChargeManSql = string.Format(@"select isnull(a.mem_ID,0) as UserSysNo,mem_Name  from tg_member  a 
                                                        join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where B.unit_Name ='{0}' AND C.pri_Name LIKE '%所长%' ", unitName == null ? " " : unitName.Trim());
            //所长
            SqlDataReader readerSZ = TG.DBUtility.DbHelperSQL.ExecuteReader(departmentChargeManSql);


            while (readerSZ.Read())
            {
                arrayList.Add(new { UserSysNo = readerSZ["UserSysNo"], UserName = readerSZ["mem_Name"], RoleName = "所长" });
            }

            //转经济
            if (isTrunEconomy.Equals("1"))
            {
                //经济所长
                string sqlnts = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_Name like'%经济%' and   C.pri_Name LIKE '%所长%' ";

                SqlDataReader readernts = TG.DBUtility.DbHelperSQL.ExecuteReader(sqlnts);

                while (readernts.Read())
                {
                    arrayList.Add(new { UserSysNo = readernts["UserSysNo"], UserName = readernts["mem_Name"], RoleName = "经济所" });
                }

            }

            if (!unitName.Contains("暖通"))//如果暖通所项目，不去判断是否转暖通所
            {
                //转暖通
                if (isTranHave.Equals("1"))
                {
                    //暖通所长 
                    string sqljjs = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_Name like'%暖通%' and   C.pri_Name LIKE '%所长%' ";

                    SqlDataReader readerJJS = TG.DBUtility.DbHelperSQL.ExecuteReader(sqljjs);


                    while (readerJJS.Read())
                    {
                        arrayList.Add(new { UserSysNo = readerJJS["UserSysNo"], UserName = readerJJS["mem_Name"], RoleName = "暖通所" });
                    }

                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = "所长", UserList = arrayList });

        }


        /// <summary>
        /// 得到改项目的设计总负责
        /// </summary>
        /// <param name="proid"></param>
        private string GetDesignHead(int proid)
        {
            ArrayList arrayList = new ArrayList();
            string sql = @"SELECT p.pro_ID,M.mem_ID, mem_Name  FROM   dbo.tg_relation R
                                INNER JOIN tg_member M
                                 ON R.mem_ID=M.mem_ID  
                                 INNER JOIN cm_Project p 
                                 ON R.pro_ID=p.ReferenceSysNo
                                WHERE (mem_RoleIDs like N'%1%' or mem_RoleIDs like N'%6%') and p.pro_ID=" + proid + "";
            //所长
            SqlDataReader reader = TG.DBUtility.DbHelperSQL.ExecuteReader(sql);

            while (reader.Read())
            {
                arrayList.Add(new { UserSysNo = reader["mem_ID"], UserName = reader["mem_Name"] });
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = "设总", UserList = arrayList });
        }

        private string GetStatusProcess(int postion)
        {
            string sql = "select RoleSysNo from cm_ProjectFeeAllotConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            return resultObj == null ? "0" : resultObj.ToString();
        }

        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}