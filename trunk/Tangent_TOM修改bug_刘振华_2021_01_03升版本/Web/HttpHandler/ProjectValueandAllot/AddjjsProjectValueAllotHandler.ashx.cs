﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using TG.Model;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using TG.DBUtility;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    /// <summary>
    /// AddjjsProjectValueAllotHandler 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class AddjjsProjectValueAllotHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {

        public TG.Model.cm_ProjectValueAuditRecord ProjectValueAuditRecordEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectValueAuditRecord obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    //JavaScriptSerializer js = new JavaScriptSerializer();
                    //obj = js.Deserialize<TG.Model.cm_ProjectValueAuditRecord>(data);
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectValueAuditRecord>(data);
                }
                return obj;
            }
        }
        public TG.Model.cm_ProjectValueAllot ProjectValueAllotEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectValueAllot obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    obj = js.Deserialize<TG.Model.cm_ProjectValueAllot>(data);
                    //obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectValueAllot>(data);
                }
                return obj;
            }
        }


        public TG.Model.jjsProjectValueAllotByMemberEntity ProjectValueAllotByMemberEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.jjsProjectValueAllotByMemberEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    obj = js.Deserialize<TG.Model.jjsProjectValueAllotByMemberEntity>(data);
                    //obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.jjsProjectValueAllotByMemberEntity>(data);
                }
                return obj;
            }
        }
        /// <summary>
        /// 人员审批
        /// </summary>
        public TG.Model.ProjectValueByMemberAuditStatusEntity projectValueByMemberAuditStatusEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.ProjectValueByMemberAuditStatusEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.ProjectValueByMemberAuditStatusEntity>(data);
                }
                return obj;
            }
        }
        public TG.Model.cm_TranjjsProjectValueAllot TranjjsProjectValueAllot
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_TranjjsProjectValueAllot obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    obj = js.Deserialize<TG.Model.cm_TranjjsProjectValueAllot>(data);
                    //obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_TranjjsProjectValueAllot>(data);
                }
                return obj;
            }
        }

        public TG.Model.cm_TranProjectValueAuditRecord TranProjectValueAuditRecordEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_TranProjectValueAuditRecord obj = null;
                if (!string.IsNullOrEmpty(data))
                {

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    obj = js.Deserialize<TG.Model.cm_TranProjectValueAuditRecord>(data); //obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_TranProjectValueAuditRecord>(data);
                }
                return obj;
            }
        }

        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        public string AllotUser
        {
            get
            {
                return Request["AllotUser"] ?? "";
            }
        }
        public string Flag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Action == "1")
            {
                if (Flag == "0")
                {

                    string roleSysNoString = GetProjectValueRoleName();

                    if (!string.IsNullOrEmpty(roleSysNoString))
                    {
                        Response.Write(roleSysNoString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    var modelEntity = ProjectValueAllotEntity;

                    TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();
                    TG.Model.cm_ProjectValueAllot model = new TG.Model.cm_ProjectValueAllot();
                    TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();

                    string proid = modelEntity.pro_ID.ToString();

                    model.pro_ID = modelEntity.pro_ID;
                    //分配次数
                    int i_count = 0;
                    string str_where = " Select Count(ID) From cm_ProjectValueAllot Where SecondValue='1' and  pro_ID=" + proid;
                    DataSet ds = bll_db.GetList(str_where);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            i_count = int.Parse(ds.Tables[0].Rows[0][0].ToString()) + 1;
                        }
                    }
                    model.AllotTimes = "第" + i_count + "次产值分配";

                    decimal i_persent = 0;
                    decimal payshicount = Convert.ToDecimal(modelEntity.Payshicount);
                    decimal allotCount = modelEntity.AllotCount;
                    if (payshicount > 0 && allotCount > 0)
                    {
                        i_persent = (allotCount / payshicount) * 100;
                    }
                    model.persent = System.Math.Round(i_persent, 2);
                    model.AllotUser = modelEntity.AllotUser;
                    model.AllotDate = DateTime.Now;
                    model.AllotCount = allotCount;
                    model.PaidValuePercent = modelEntity.PaidValuePercent;
                    model.PaidValueCount = modelEntity.PaidValueCount;
                    model.DesignManagerPercent = modelEntity.DesignManagerPercent;
                    model.DesignManagerCount = modelEntity.DesignManagerCount;
                    model.Itemtype = modelEntity.Itemtype;
                    model.Thedeptallotpercent = modelEntity.Thedeptallotpercent;
                    model.Thedeptallotcount = modelEntity.Thedeptallotcount;
                    model.ProgramPercent = modelEntity.ProgramPercent;
                    model.ProgramCount = modelEntity.ProgramCount;
                    model.ShouldBeValuePercent = modelEntity.ShouldBeValuePercent;
                    model.ShouldBeValueCount = modelEntity.ShouldBeValueCount;
                    model.TranBulidingPercent = modelEntity.TranBulidingPercent;
                    model.TranBulidingCount = modelEntity.TranBulidingCount;
                    model.Status = "A";
                    model.SecondValue = "1";
                    model.UnitId = modelEntity.UnitId;
                    model.ActualAllountTime = modelEntity.ActualAllountTime;
                    model.FinanceValuePercent = modelEntity.FinanceValuePercent;
                    model.FinanceValueCount = modelEntity.FinanceValueCount;
                    model.TheDeptShouldValuePercent = modelEntity.TheDeptShouldValuePercent;
                    model.TheDeptShouldValueCount = modelEntity.TheDeptShouldValueCount;
                    model.DividedPercent = modelEntity.DividedPercent;
                    int affectRow = 0;
                    affectRow = bll.Add(model);

                    if (affectRow > 0)
                    {
                        //添加审核状态
                        TG.BLL.cm_ProjectValueAuditRecord bllRecord = new TG.BLL.cm_ProjectValueAuditRecord();
                        TG.Model.cm_ProjectValueAuditRecord modelRecord = new cm_ProjectValueAuditRecord();
                        modelRecord.ProSysNo = int.Parse(modelEntity.pro_ID.ToString());
                        modelRecord.InUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser);
                        modelRecord.InDate = DateTime.Now;
                        modelRecord.Status = "A";
                        modelRecord.AllotID = affectRow;
                        modelRecord.AuditUser = modelEntity.AllotUser;
                        modelRecord.AuditDate = DateTime.Now.ToString();
                        modelRecord.SecondValue = "1";
                        int sysNo = bllRecord.Add(modelRecord);

                        //发送消息
                        TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                        string sysMsgString = "";
                        TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(ProjectValueAllotEntity.pro_ID));

                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&proid={1}&MessageStatus={2}", sysNo.ToString(), modelEntity.pro_ID, "A"),
                            FromUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                            InUser = modelEntity.AllotUser == "" ? 0 : int.Parse(modelEntity.AllotUser),
                            MsgType = 18,
                            MessageContent = string.Format("关于经济所 \"{0}\" 的{1}消息！", pro.pro_name, "产值分配"),
                            QueryCondition = pro.pro_name,
                            IsDone = "A",
                            Status = "A",
                            ToRole = "0"
                        };
                        sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);

                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            Response.Write(sysMsgString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
            else if (Action == "2") //经济所所长审批
            {
                //if (Flag == "0")
                //{
                //    string roleSysNo = GetStatusProcess(3);
                //    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));

                //    if (!string.IsNullOrEmpty(roleUserString))
                //    {
                //        Response.Write(roleUserString);
                //    }
                //    else
                //    {
                //        Response.Write("0");
                //    }
                //}
                //else
                //{
                string auditID = context.Request.Params["auditID"] ?? "0";

                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();
                int count = bll.InsertJjsProjectAllotValueByMemberDetail(ProjectValueAllotByMemberEntity, "0");
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(ProjectValueAllotByMemberEntity.ProNo);

                if (count > 0)
                {
                    //更新状态

                    TG.Model.cm_ProjectValueAuditRecord oldEntity = bll.GetModel(ProjectValueAllotByMemberEntity.SysNo);
                    #region EntityParameter
                    TG.Model.cm_ProjectValueAuditRecord EntityParameter = new TG.Model.cm_ProjectValueAuditRecord
                    {
                        SysNo = oldEntity.SysNo,
                        OneSuggestion = oldEntity.OneSuggestion,
                        TwoSuggstion = oldEntity.TwoSuggstion,
                        ThreeSuggsion = oldEntity.ThreeSuggsion,
                        FourSuggion = oldEntity.FourSuggion,
                        FiveSuggion = oldEntity.FiveSuggion,
                        SixSuggsion = oldEntity.SixSuggsion,
                        SevenSuggsion = oldEntity.SevenSuggsion,
                        InDate = oldEntity.InDate,
                        InUser = oldEntity.InUser,
                        AllotID = oldEntity.AllotID,
                        ProSysNo = oldEntity.ProSysNo,
                        HeadAuditCount = oldEntity.HeadAuditCount,
                        TwoAuditUser = oldEntity.TwoAuditUser,
                        TwoAuditDate = oldEntity.TwoAuditDate,
                        TwoIsPass = oldEntity.TwoIsPass,
                        ThreeAuditUser = oldEntity.ThreeAuditUser,
                        ThreeAuditDate = oldEntity.ThreeAuditDate,
                        ThreeIsPass = oldEntity.ThreeIsPass,
                        FourAuditUser = oldEntity.FourAuditUser,
                        FourAuditDate = oldEntity.FourAuditDate,
                        FourIsPass = oldEntity.FourIsPass,
                        SecondValue = oldEntity.SecondValue
                    };
                    #endregion

                    EntityParameter.Status = "B";
                    EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAuditRecordEntity.AuditUser : oldEntity.AuditUser + "," + ProjectValueAuditRecordEntity.AuditUser;
                    EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + DateTime.Now.ToString();


                    int countRecord = bll.Update(EntityParameter);

                    if (countRecord > 0)
                    {
                        DataTable dt = bll.GetAuditMemberList(oldEntity.ProSysNo, oldEntity.AllotID, "").Tables[0];
                        //给人员新增信息
                        int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitMemberUserInfo(dt, oldEntity.ProSysNo, oldEntity.AllotID, ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser));
                        if (countAuditMember > 0)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    //发送消息给每个人员
                                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                                    {
                                        ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}", oldEntity.AllotID, oldEntity.ProSysNo, ProjectValueAllotByMemberEntity.SysNo.ToString(), "jjs"),
                                        FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                        InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                                        MsgType = 25,//项目产值分配明细
                                        ToRole = "0",
                                        MessageContent = string.Format("关于{0}的经济所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                        QueryCondition = pro == null ? "" : pro.pro_name,
                                        IsDone = "A",
                                        Status = "A"
                                    };
                                    new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                                }

                            }
                            Response.Write("4");

                        }
                        else
                        {
                            Response.Write("0");
                        }
                        //string msg = "";
                        //msg = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "经济所产值分配");

                        ////发送消息
                        //TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                        //SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        //{
                        //    ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&proid={1}&MessageStatus={2}", ProjectValueAllotByMemberEntity.SysNo.ToString(), ProjectValueAllotByMemberEntity.ProNo, "B"),
                        //    FromUser = string.IsNullOrEmpty(ProjectValueAllotByMemberEntity.AuditUser) ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                        //    InUser = string.IsNullOrEmpty(ProjectValueAllotByMemberEntity.AuditUser) ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                        //    MsgType = 18,
                        //    MessageContent = msg,
                        //    QueryCondition = pro.pro_name,
                        //    IsDone = "A",
                        //    Status = "A"
                        //};

                        //string roleSysNo = string.Empty;
                        //sysMessageViewEntity.ToRole = roleSysNo;

                        //sysMessageViewEntity.ToRole = "0";

                        //string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                        //if (!string.IsNullOrEmpty(sysMsgString))
                        //{
                        //    Response.Write(sysMsgString);
                        //}
                        //else
                        //{
                        //    Response.Write("0");
                        //}
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                //}
            }
            else if (Action == "3")
            {
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();

                int sysNo = bll.UpdateJjsProjectAllotValue(ProjectValueAuditRecordEntity);
                ;
                if (sysNo > 0)
                {
                    //更新此状态
                    TG.Model.cm_ProjectValueAuditRecord oldEntity = bll.GetModel(ProjectValueAuditRecordEntity.SysNo);
                    #region EntityParameter
                    TG.Model.cm_ProjectValueAuditRecord EntityParameter = new TG.Model.cm_ProjectValueAuditRecord
                    {
                        SysNo = oldEntity.SysNo,
                        OneSuggestion = oldEntity.OneSuggestion,
                        TwoSuggstion = oldEntity.TwoSuggstion,
                        ThreeSuggsion = oldEntity.ThreeSuggsion,
                        FourSuggion = oldEntity.FourSuggion,
                        FiveSuggion = oldEntity.FiveSuggion,
                        SixSuggsion = oldEntity.SixSuggsion,
                        SevenSuggsion = oldEntity.SevenSuggsion,
                        InDate = oldEntity.InDate,
                        InUser = oldEntity.InUser,
                        AllotID = oldEntity.AllotID,
                        ProSysNo = oldEntity.ProSysNo,
                        HeadAuditCount = oldEntity.HeadAuditCount,
                        TwoAuditUser = oldEntity.TwoAuditUser,
                        TwoAuditDate = oldEntity.TwoAuditDate,
                        TwoIsPass = oldEntity.TwoIsPass,
                        ThreeAuditUser = oldEntity.ThreeAuditUser,
                        ThreeAuditDate = oldEntity.ThreeAuditDate,
                        ThreeIsPass = oldEntity.ThreeIsPass,
                        FourAuditUser = oldEntity.FourAuditUser,
                        FourAuditDate = oldEntity.FourAuditDate,
                        FourIsPass = oldEntity.FourIsPass,
                        SecondValue = oldEntity.SecondValue
                    };
                    #endregion
                    EntityParameter.OneSuggestion = ProjectValueAuditRecordEntity.OneSuggestion;
                    EntityParameter.Status = ProjectValueAuditRecordEntity.Status;
                    EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAuditRecordEntity.AuditUser : oldEntity.AuditUser + "," + ProjectValueAuditRecordEntity.AuditUser;
                    EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + DateTime.Now.ToString();
                    int countRecord = bll.Update(EntityParameter);
                    //查询项目信息
                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(oldEntity.ProSysNo);
                    string MessageContent = "";
                    if (ProjectValueAuditRecordEntity.Status == "C" || ProjectValueAuditRecordEntity.Status == "E" || ProjectValueAuditRecordEntity.Status == "G")
                    {
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "经济所分配不通过");
                    }
                    else
                    {
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "经济所分配通过");
                    }

                    //流程走完之后要给申请人发一条消息
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&proid={1}&MessageStatus={2}", ProjectValueAuditRecordEntity.SysNo.ToString(), ProjectValueAuditRecordEntity.ProSysNo, ProjectValueAuditRecordEntity.Status),
                        FromUser = oldEntity.InUser,
                        InUser = string.IsNullOrEmpty(ProjectValueAllotByMemberEntity.AuditUser) ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                        MsgType = 18,
                        ToRole = "0",
                        MessageContent = MessageContent,
                        QueryCondition = pro.pro_name,
                        IsDone = "B",
                        Status = "A"
                    };
                    int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    if (resultcount > 0)
                    {
                        //再给 分配钱的人都发一条消息
                        if (EntityParameter.Status == "F")
                        {
                            SendMessageToUser((int)ProjectValueAuditRecordEntity.AllotID, pro.bs_project_Id, pro.pro_name, "");
                        }

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
            else if (Action == "4")//转经济所添加
            {
                //新增分配信息
                var modelEntity = TranjjsProjectValueAllot;
                TG.BLL.cm_TranjjsProjectValueAllot bll = new TG.BLL.cm_TranjjsProjectValueAllot();
                TG.Model.cm_TranjjsProjectValueAllot model = new TG.Model.cm_TranjjsProjectValueAllot();

                model.Pro_ID = modelEntity.Pro_ID;
                model.AllotUser = modelEntity.AllotUser;
                model.AllotDate = DateTime.Now;
                model.TotalCount = modelEntity.AllotCount;
                model.AllotCount = modelEntity.AllotCount;
                model.PaidValuePercent = modelEntity.PaidValuePercent;
                model.PaidValueCount = modelEntity.PaidValueCount;
                model.DesignManagerPercent = modelEntity.DesignManagerPercent;
                model.DesignManagerCount = modelEntity.DesignManagerCount;
                model.ItemType = modelEntity.ItemType;
                model.Thedeptallotpercent = modelEntity.Thedeptallotpercent;
                model.Thedeptallotcount = modelEntity.Thedeptallotcount;
                model.ProgramPercent = modelEntity.ProgramPercent;
                model.ProgramCount = modelEntity.ProgramCount;
                model.ShouldBeValuePercent = modelEntity.ShouldBeValuePercent;
                model.ShouldBeValueCount = modelEntity.ShouldBeValueCount;
                model.AllotID = modelEntity.AllotID;
                model.Status = "A";
                model.ActualAllountTime = modelEntity.ActualAllountTime;
                string allotID = context.Request.Params["AllotID"] ?? "";

                int affectRow = 0;
                if (!string.IsNullOrEmpty(allotID))
                {
                    model.ID = allotID == "" ? 0 : int.Parse(allotID);
                    bll.Update(model);
                }
                else
                {
                    affectRow = bll.Add(model);
                }
                Response.Write(affectRow);
            }
            else if (Action == "5")//转经济所添加第二部分
            {
                //if (Flag == "0")
                //{
                //    string roleSysNo = GetStatusProcess();
                //    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));

                //    if (!string.IsNullOrEmpty(roleUserString))
                //    {
                //        Response.Write(roleUserString);
                //    }
                //    else
                //    {
                //        Response.Write("0");
                //    }
                //}
                //else
                //{
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();
                int count = bll.InsertJjsProjectAllotValueByMemberDetail(ProjectValueAllotByMemberEntity, "1");
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(ProjectValueAllotByMemberEntity.ProNo);

                if (count > 0)
                {
                    //添加审核状态
                    TG.BLL.cm_TranProjectValueAuditRecord bllRecord = new TG.BLL.cm_TranProjectValueAuditRecord();
                    TG.Model.cm_TranProjectValueAuditRecord modelRecord = new cm_TranProjectValueAuditRecord();
                    modelRecord.ProSysNo = int.Parse(ProjectValueAllotByMemberEntity.ProNo.ToString());
                    modelRecord.InUser = string.IsNullOrEmpty(ProjectValueAllotByMemberEntity.AuditUser) ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser);
                    modelRecord.InDate = DateTime.Now;
                    modelRecord.Status = "A";
                    modelRecord.AllotID = ProjectValueAllotByMemberEntity.AllotID;
                    modelRecord.ItemType = "jjs";
                    int sysNo = bllRecord.Add(modelRecord);


                    //给参与的人员发消息
                    DataTable dt = bll.GetAuditTranMemberList(ProjectValueAllotByMemberEntity.ProNo, ProjectValueAllotByMemberEntity.AllotID, " and TranType='jjs'").Tables[0];
                    //给人员新增信息
                    int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitTranMemberUserInfo(dt, ProjectValueAllotByMemberEntity.ProNo, ProjectValueAllotByMemberEntity.AllotID, ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser), "jjs");
                    if (countAuditMember > 0)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //发送消息给每个人员
                                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", ProjectValueAllotByMemberEntity.AllotID, ProjectValueAllotByMemberEntity.ProNo, sysNo, "tranjjs", "jjs"),
                                    FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                    InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                                    MsgType = 25,//项目产值分配明细
                                    ToRole = "0",
                                    MessageContent = string.Format("关于{0}的转经济所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                    QueryCondition = pro == null ? "" : pro.pro_name,
                                    IsDone = "A",
                                    Status = "A"
                                };
                                new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                            }

                        }
                        Response.Write("4");

                    }
                    else
                    {
                        Response.Write("0");
                    }

                    //string msg = "";
                    //string referenceSysNo = "";

                    //msg = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转经济所产值分配");
                    //referenceSysNo = string.Format("proid={0}&allotID={1}&auditSysID={2}", ProjectValueAllotByMemberEntity.ProNo.ToString(), ProjectValueAllotByMemberEntity.AllotID, sysNo);

                    ////发送消息
                    //TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    //SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    //{
                    //    ReferenceSysNo = referenceSysNo,
                    //    FromUser = string.IsNullOrEmpty(ProjectValueAllotByMemberEntity.AuditUser) ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                    //    InUser = string.IsNullOrEmpty(ProjectValueAllotByMemberEntity.AuditUser) ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                    //    MsgType = 16,
                    //    MessageContent = msg,
                    //    QueryCondition = pro.pro_name,
                    //    IsDone = "A",
                    //    Status = "A"
                    //};

                    //string roleSysNo = string.Empty;
                    //sysMessageViewEntity.ToRole = roleSysNo;

                    //sysMessageViewEntity.ToRole = "0";

                    //string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    //if (!string.IsNullOrEmpty(sysMsgString))
                    //{
                    //    Response.Write(sysMsgString);
                    //}
                    //else
                    //{
                    //    Response.Write("0");
                    //}
                }
                else
                {
                    Response.Write("0");
                }
                //}
            }
            else if (Action == "6")//转经济所审核
            {
                TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();

                int sysNo = bll.UpdateTranJjsProjectAllotValue(TranProjectValueAuditRecordEntity);

                if (sysNo > 0)
                {
                    //更新此状态
                    TG.Model.cm_TranProjectValueAuditRecord oldEntity = bll.GetModel(TranProjectValueAuditRecordEntity.SysNo);
                    //查询项目信息
                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(int.Parse(oldEntity.ProSysNo.ToString()));


                    string MessageContent = "";
                    if (TranProjectValueAuditRecordEntity.Status == "S")
                    {
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转经济所分配通过");
                    }
                    else
                    {
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转经济所分配不通过");
                    }

                    //流程走完之后要给申请人发一条消息
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("proid={0}&allotID={1}&auditSysID={2}", oldEntity.ProSysNo.ToString(), oldEntity.AllotID, oldEntity.SysNo),
                        FromUser = int.Parse(oldEntity.InUser.ToString()),
                        InUser = string.IsNullOrEmpty(ProjectValueAllotByMemberEntity.AuditUser) ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                        MsgType = 16,
                        ToRole = "0",
                        MessageContent = MessageContent,
                        QueryCondition = pro.pro_name,
                        IsDone = "B",
                        Status = "A"
                    };
                    int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    if (resultcount > 0)
                    {
                        SendMessageToUser((int)TranProjectValueAuditRecordEntity.AllotID, pro.bs_project_Id, pro.pro_name, "jjs");

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
            else if (Action == "7")//删除
            {
                string proid = context.Request.Params["proID"] ?? "0";
                string allotID = context.Request.Params["AllotID"] ?? "0";
                string sysID = context.Request.Params["sysID"] ?? "0";
                TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();

                int sysNo = bll.DeleteTranJjsProjectAllotValue(int.Parse(proid), int.Parse(allotID), int.Parse(sysID));

                Response.Write(sysNo);
            }
            else if (Action == "8")//经济所删除
            {
                string proid = context.Request.Params["proID"] ?? "0";
                string allotID = context.Request.Params["AllotID"] ?? "0";
                TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();

                int sysNo = bll.DeleteJjsProjectAllotValue(int.Parse(proid), int.Parse(allotID));

                Response.Write(sysNo);
            }
            else if (Action == "9")//个人审核通过
            {
                string msgId = context.Request.Params["msgID"] ?? "0";
                string proType = context.Request.Params["proType"] ?? "";
                int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
                int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
                int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();
                //取得参与项目的人员总数
                DataTable dt = bll.GetAuditMemberList(pro_id, allotID, "").Tables[0];
                int memberCount = dt.Rows.Count;

                //取得审批通过的人员的总数
                TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
                string strWhere = " proID =" + pro_id + " and AllotID=" + allotID + "  and Status='S' and TranType is null ";
                DataTable dt_user = bll_user.GetList(strWhere).Tables[0];
                int auditMemberCount = dt_user.Rows.Count;

                if ((memberCount - 1) == auditMemberCount)
                {
                    if (Flag == "0")
                    {
                        string roleSysNo = GetStatusProcess(4);
                        string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));

                        if (!string.IsNullOrEmpty(roleUserString))
                        {
                            Response.Write(roleUserString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        //更新信息
                        int count = bll_user.Update(projectValueByMemberAuditStatusEntity);

                        //产值审核表更新状态
                        bll.Update(projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo, "D");

                        //更新 状态 为代办事项   
                        UpdateMessageIsDone(int.Parse(msgId));

                        //发送消息
                        TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                        TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&proid={1}&MessageStatus={2}", projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo, projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID, "D"),
                            FromUser = mem_id,
                            InUser = mem_id,
                            MsgType = 18,
                            MessageContent = string.Format("关于经济所\"{0}\" 的产值分配{1}消息！", pro.pro_name, "产值分配"),
                            QueryCondition = pro.pro_name,
                            IsDone = "A",
                            ToRole = "0"
                        };

                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            Response.Write(sysMsgString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }

                }
                else
                {
                    //更新信息
                    int count = bll_user.Update(projectValueByMemberAuditStatusEntity);
                    if (count > 0)
                    {
                        //更新 状态 为代办事项   
                        UpdateMessageIsDone(int.Parse(msgId));
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
            else if (Action == "10")//个人审核不通过
            {
                string msgId = context.Request.Params["msgID"] ?? "0";
                TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
                //更新信息
                int count = bll_user.Update(projectValueByMemberAuditStatusEntity);

                if (count > 0)
                {
                    int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
                    int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
                    int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
                    int sysID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo;


                    TG.Model.tg_member model = new TG.BLL.tg_member().GetModel(mem_id);

                    //给专业负责人发消息
                    ArrayList arrayList = new ArrayList();

                    arrayList = GetProjectValueRoleArrayName();

                    //给专业负责人发送消息
                    if (arrayList.Count > 0)
                    {
                        TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            //发送消息给每个人员
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}", allotID, pro_id, sysID, "jjs"),
                                FromUser = int.Parse(arrayList[i].ToString()),
                                InUser = mem_id,
                                MsgType = 27,//专业负责人审批
                                ToRole = "0",
                                MessageContent = string.Format("关于{0}的经济所产值分配个人不同意，需所长重新分配产值！", pro == null ? "" : pro.pro_name),
                                QueryCondition = pro == null ? "" : pro.pro_name,
                                IsDone = "A",
                                Status = "A"
                            };
                            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        }

                        //更新 状态 为代办事项   
                        UpdateMessageIsDone(int.Parse(msgId));
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == "11")//所长确认通过
            {
                string msgID = context.Request.Params["msgID"] ?? "0";
                int allotID = int.Parse(context.Request.Params["allotID"] ?? "0");
                string sprproType = context.Request.Params["proType"] ?? "";
                int mem_id = int.Parse(context.Request.Params["mem_id"] ?? "0");
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();

                int count = bll.UpdateJjsProjectAllotValueByMemberDetail(ProjectValueAllotByMemberEntity, "0");

                if (count > 0)
                {
                    //删除个人确认表本专业的人员信息
                    new TG.BLL.cm_ProjectValueByMemberAuditStatus().Delete(ProjectValueAllotByMemberEntity.ProNo, allotID);

                    //重新给这些人发送审批信息
                    DataTable dt = bll.GetAuditMemberList(ProjectValueAllotByMemberEntity.ProNo, allotID, "").Tables[0];

                    //删掉本专业所有的审批记录 
                    DeleteMessageByMember(int.Parse(msgID));


                    //给人员新增信息
                    int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitMemberUserInfo(dt, ProjectValueAllotByMemberEntity.ProNo, allotID, mem_id);
                    if (countAuditMember > 0)
                    {
                        TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(ProjectValueAllotByMemberEntity.ProNo));
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //发送消息给每个人员
                                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}", allotID, ProjectValueAllotByMemberEntity.ProNo, ProjectValueAllotByMemberEntity.SysNo.ToString(), "jjs"),
                                    FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                    InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                                    MsgType = 25,//项目产值分配明细
                                    ToRole = "0",
                                    MessageContent = string.Format("关于{0}的经济所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                    QueryCondition = pro == null ? "" : pro.pro_name,
                                    IsDone = "A",
                                    Status = "A"
                                };
                                new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                            }

                        }

                        //把本专业的信息更新
                        UpdateMessageBySpeAudit(int.Parse(msgID), 27);

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == "12")//所长确认不通过
            {
                string msgid = context.Request.Params["msgID"] ?? "0";
                int allotID = int.Parse(context.Request.Params["allotID"] ?? "0");
                int pro_id = int.Parse(context.Request.Params["pro_id"] ?? "0");
                int mem_id = int.Parse(context.Request.Params["mem_id"] ?? "0");
                int sys_no = int.Parse(context.Request.Params["sys_no"] ?? "0");
                string sprproType_no = context.Request.Params["proType"] ?? "";

                //给本人发送消息，
                string where = @" status='D' and proID=" + pro_id + " and AllotID=" + allotID + " and  TranType is null";
                DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetList(where).Tables[0];

                //批量更改人员审批状态
                int count = new TG.BLL.cm_ProjectValueByMemberAuditStatus().Update(dt);
                if (count > 0)
                {
                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //发送消息给每个人员
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}", allotID, pro_id, sys_no, "jjs"),
                            FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                            InUser = mem_id,
                            MsgType = 25,//项目产值分配明细
                            ToRole = "0",
                            MessageContent = string.Format("关于{0}的个人经济所产值确认不通过，所长审批不通过！", pro == null ? "" : pro.pro_name),
                            QueryCondition = pro == null ? "" : pro.pro_name,
                            IsDone = "A",
                            Status = "A"
                        };
                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    }

                    //把本专业的信息更新
                    UpdateMessageBySpeAudit(int.Parse(msgid), 27);

                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == "13")//转经济所个人确认
            {
                string msgId = context.Request.Params["msgID"] ?? "0";
                string proType = context.Request.Params["proType"] ?? "";
                int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
                int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
                int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();
                //取得参与项目的人员总数-转
                DataTable dt = bll.GetAuditTranMemberList(pro_id, allotID, " and TranType='jjs'").Tables[0];
                int memberCount = dt.Rows.Count;

                //取得审批通过的人员的总数
                TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
                string strWhere = " proID =" + pro_id + " and AllotID=" + allotID + "  and Status='S' and TranType='jjs' ";
                DataTable dt_user = bll_user.GetList(strWhere).Tables[0];
                int auditMemberCount = dt_user.Rows.Count;

                if ((memberCount - 1) == auditMemberCount)
                {
                    if (Flag == "0")
                    {
                        string roleSysNo = GetTranStatusProcess(3);
                        string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));

                        if (!string.IsNullOrEmpty(roleUserString))
                        {
                            Response.Write(roleUserString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        //更新信息
                        int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "jjs");

                        TG.BLL.cm_TranProjectValueAuditRecord bll_tran = new TG.BLL.cm_TranProjectValueAuditRecord();

                        //产值审核表更新状态
                        bll_tran.UpdateTranStatus(pro_id, allotID, "B", "jjs");

                        //更新 状态 为代办事项   
                        UpdateMessageIsDone(int.Parse(msgId));

                        string msg = "";
                        string referenceSysNo = "";

                        TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                        msg = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转经济所产值分配");
                        referenceSysNo = string.Format("proid={0}&allotID={1}&auditSysID={2}", pro_id, allotID, projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo);

                        //发送消息                    
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = referenceSysNo,
                            FromUser = mem_id,
                            InUser = mem_id,
                            MsgType = 16,
                            MessageContent = msg,
                            QueryCondition = pro.pro_name,
                            IsDone = "A",
                            Status = "A"
                        };

                        string roleSysNo = string.Empty;
                        sysMessageViewEntity.ToRole = roleSysNo;

                        sysMessageViewEntity.ToRole = "0";

                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            Response.Write(sysMsgString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }

                }
                else
                {
                    //更新信息
                    int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "jjs");
                    if (count > 0)
                    {
                        //更新 状态 为代办事项   
                        UpdateMessageIsDone(int.Parse(msgId));
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
            else if (Action == "14")//转经济所个人确认不通过
            {
                string msgId = context.Request.Params["msgID"] ?? "0";
                TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
                //更新信息
                int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "jjs");

                if (count > 0)
                {
                    int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
                    int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
                    int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
                    int sysID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo;


                    TG.Model.tg_member model = new TG.BLL.tg_member().GetModel(mem_id);

                    //给专业负责人发消息
                    ArrayList arrayList = new ArrayList();

                    arrayList = GetProjectValueRoleArrayName();

                    //给专业负责人发送消息
                    if (arrayList.Count > 0)
                    {
                        TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                        for (int i = 0; i < arrayList.Count; i++)
                        {
                            //发送消息给每个人员
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotID, pro_id, sysID, "tranjjs", "jjs"),
                                FromUser = int.Parse(arrayList[i].ToString()),
                                InUser = mem_id,
                                MsgType = 28,//转经济所
                                ToRole = "0",
                                MessageContent = string.Format("关于{0}的转经济所产值分配个人不同意，需所长重新分配产值！", pro == null ? "" : pro.pro_name),
                                QueryCondition = pro == null ? "" : pro.pro_name,
                                IsDone = "A",
                                Status = "A"
                            };
                            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        }

                        //更新 状态 为代办事项   
                        UpdateMessageIsDone(int.Parse(msgId));
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == "15")//转经济所长通过
            {
                string msgID = context.Request.Params["msgID"] ?? "0";
                int allotID = int.Parse(context.Request.Params["allotID"] ?? "0");
                string sprproType = context.Request.Params["proType"] ?? "";
                int mem_id = int.Parse(context.Request.Params["mem_id"] ?? "0");
                int sys_no = int.Parse(context.Request.Params["sys_no"] ?? "0");
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();

                int count = bll.UpdateJjsProjectAllotValueByMemberDetail(ProjectValueAllotByMemberEntity, "1");

                if (count > 0)
                {
                    //删除个人确认表本专业的人员信息
                    new TG.BLL.cm_ProjectValueByMemberAuditStatus().Delete(ProjectValueAllotByMemberEntity.ProNo, allotID, "jjs");

                    //重新给这些人发送审批信息
                    DataTable dt = bll.GetAuditTranMemberList(ProjectValueAllotByMemberEntity.ProNo, allotID, " and TranType='jjs' ").Tables[0];

                    //删掉本专业所有的审批记录 
                    DeleteMessageByMember(int.Parse(msgID));


                    //给人员新增信息
                    int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitTranMemberUserInfo(dt, ProjectValueAllotByMemberEntity.ProNo, allotID, mem_id, "jjs");
                    if (countAuditMember > 0)
                    {
                        TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(Convert.ToInt32(ProjectValueAllotByMemberEntity.ProNo));
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //发送消息给每个人员
                                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotID, ProjectValueAllotByMemberEntity.ProNo, sys_no, "tranjjs", "jjs"),
                                    FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                    InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                                    MsgType = 25,//项目产值分配明细
                                    ToRole = "0",
                                    MessageContent = string.Format("关于{0}的转经济所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                    QueryCondition = pro == null ? "" : pro.pro_name,
                                    IsDone = "A",
                                    Status = "A"
                                };
                                new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                            }

                        }

                        //把本专业的信息更新
                        UpdateMessageBySpeAudit(int.Parse(msgID), 28);

                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == "16")//转经济所所长不通过
            {
                string msgid = context.Request.Params["msgID"] ?? "0";
                int allotID = int.Parse(context.Request.Params["allotID"] ?? "0");
                int pro_id = int.Parse(context.Request.Params["pro_id"] ?? "0");
                int mem_id = int.Parse(context.Request.Params["mem_id"] ?? "0");
                int sys_no = int.Parse(context.Request.Params["sys_no"] ?? "0");
                string sprproType_no = context.Request.Params["proType"] ?? "";

                //给本人发送消息，
                string where = @" status='D' and proID=" + pro_id + " and AllotID=" + allotID + " and  TranType='jjs'";
                DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetList(where).Tables[0];

                //批量更改人员审批状态
                int count = new TG.BLL.cm_ProjectValueByMemberAuditStatus().Update(dt);
                if (count > 0)
                {
                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //发送消息给每个人员
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotID, pro_id, sys_no, "tranjjs", "jjs"),
                            FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                            InUser = mem_id,
                            MsgType = 25,//项目产值分配明细
                            ToRole = "0",
                            MessageContent = string.Format("关于{0}的个人转经济所产值确认不通过，所长审批不通过！", pro == null ? "" : pro.pro_name),
                            QueryCondition = pro == null ? "" : pro.pro_name,
                            IsDone = "A",
                            Status = "A"
                        };
                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    }

                    //把本专业的信息更新
                    UpdateMessageBySpeAudit(int.Parse(msgid), 28);

                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }

            else if (Action == "17")//经济所全部借出
            {
                string allotID = context.Request.Params["allotID"] ?? "0";
                string proID = context.Request.Params["proID"] ?? "0";
                string loanCount = context.Request.Params["loanCount"] ?? "0";
                string actulCount = context.Request.Params["actulCount"] ?? "0";


                string strWhere = "update cm_ProjectValueAllot set LoanValueCount=" + decimal.Parse(loanCount) + ",ActualAllotCount=" + decimal.Parse(actulCount) + " ,IsTAllPass=1,BorrowValueCount=0 where ID=" + allotID + "";

                int rows = DbHelperSQL.ExecuteSql(strWhere);
                context.Response.Write(rows);
            }

            else if (Action == "18")//经济所借出金额保存
            {
                string allotID = context.Request.Params["allotID"] ?? "0";
                string proID = context.Request.Params["proID"] ?? "0";
                string loanCount = context.Request.Params["loanCount"] ?? "0";
                string actulCount = context.Request.Params["actulCount"] ?? "0";

                string borrowCount = context.Request.Params["borrowCount"] ?? "0";

                string strWhere = "update cm_ProjectValueAllot set LoanValueCount=" + decimal.Parse(loanCount) + ",ActualAllotCount=" + decimal.Parse(actulCount) + " ,BorrowValueCount=" + borrowCount + ",IsTAllPass=0 where ID=" + allotID + "";

                int rows = DbHelperSQL.ExecuteSql(strWhere);
                context.Response.Write(rows);

            }

            else if (Action == "19")//经济所全部通过
            {
               string msgID = context.Request.Params["msgID"] ?? "0";
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();
                TG.Model.cm_ProjectValueAuditRecord oldEntity = bll.GetModel(ProjectValueAuditRecordEntity.SysNo);
                //查询合同信息
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(oldEntity.ProSysNo);

                int userSysNo = ProjectValueAuditRecordEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAuditRecordEntity.AuditUser);

                TG.Model.cm_ProjectValueAuditRecord EntityParameter = new TG.Model.cm_ProjectValueAuditRecord();
                EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) ? ProjectValueAuditRecordEntity.AuditUser : oldEntity.AuditUser + "," + ProjectValueAuditRecordEntity.AuditUser;
                EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) ? DateTime.Now.ToString() : oldEntity.AuditDate + "," + DateTime.Now.ToString();
                EntityParameter.Status = "B";
                EntityParameter.SysNo = ProjectValueAuditRecordEntity.SysNo;
                EntityParameter.ProSysNo = ProjectValueAuditRecordEntity.ProSysNo;

                //更新审批
                int countRecord = bll.UpdateJjsThreeAllPassProcess(EntityParameter, (int)oldEntity.AllotID);

                if (countRecord > 0)
                {
                    TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    //给发起人发起一条消息
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("ValueAllotAuditSysNo={0}&proid={1}&MessageStatus={2}&likeType=detail", oldEntity.SysNo,oldEntity.ProSysNo,"B"),
                        FromUser = oldEntity.InUser,
                        InUser = userSysNo,
                        MsgType =18,
                        MessageContent = string.Format("关于 经济所所的\"{0}\" ，分配产值全部借出。审批完毕！", pro.pro_name),
                        QueryCondition = pro.pro_name,
                        ToRole = "0",
                        IsDone = "B",
                        Status = "A"
                    };
                    sysMsgBLL.InsertSysMessage(sysMessageViewEntity);

                    //更新 状态 为代办事项
                    UpdateMessageIsDone(int.Parse(msgID));

                    Response.Write("1");

                }
                else
                {
                    Response.Write("-1");
                }


            }
        }
        private string GetStatusProcess(int postion)
        {
            string sql = "select RoleSysNo from cm_jjsProjectFeeAllotConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return resultObj == null ? "0" : resultObj.ToString();
        }

        private string GetTranStatusProcess(int postion)
        {
            string sql = "select RoleSysNo from cm_TranProjectFeeAllotConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return resultObj == null ? "0" : resultObj.ToString();
        }

        private string GetStatusProcess()
        {
            string sql = "select * from cm_Role where RoleName ='生产经营部'";
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            return resultObj == null ? "0" : resultObj.ToString();
        }

        /// <summary>
        /// 获取用户列表-所长
        /// </summary>
        /// <returns></returns>
        private string GetProjectValueRoleName()
        {
            ArrayList arrayList = new ArrayList();

            //经济所长
            string sqlnts = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_Name like'%经济%' and   C.pri_Name LIKE '%所长%' ";

            SqlDataReader readernts = TG.DBUtility.DbHelperSQL.ExecuteReader(sqlnts);

            while (readernts.Read())
            {
                arrayList.Add(new { UserSysNo = readernts["UserSysNo"], UserName = readernts["mem_Name"], RoleName = "经济所" });
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = "所长", UserList = arrayList });

        }
        /// <summary>
        /// 获取用户列表-所长
        /// </summary>
        /// <returns></returns>
        private ArrayList GetProjectValueRoleArrayName()
        {
            ArrayList arrayList = new ArrayList();

            //经济所长
            string sqlnts = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_Name like'%经济%' and   C.pri_Name LIKE '%所长%' ";

            SqlDataReader readernts = TG.DBUtility.DbHelperSQL.ExecuteReader(sqlnts);

            while (readernts.Read())
            {
                arrayList.Add(readernts["UserSysNo"]);
            }

            return arrayList;

        }
        /// <summary>
        /// 给每个人分配到的人员发送消息
        /// </summary>
        /// <param name="allotID"></param>
        protected void SendMessageToUser(int allotID, int cprID, string cprName, string type)
        {
            string sql = "";
            if (type == "jjs")
            {
                sql = string.Format("select  distinct mem_ID  from cm_ProjectValueByMemberDetails where AllotID={0} and IsExternal='0' and TranType='jjs'", allotID);
            }
            else
            {
                sql = string.Format("select  distinct mem_ID  from cm_ProjectValueByMemberDetails where AllotID={0} and IsExternal='0' and TranType is null", allotID);
            }
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //发送消息给每个人员
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("AllotID={0}&proID={1}&tranType={2}", allotID, cprID, "jjs"),
                        FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                        InUser = UserSysNo,
                        MsgType = 10,//项目产值分配明细
                        ToRole = "0",
                        MessageContent = string.Format("关于{0}经济所的分配个人明细信息！", cprName),
                        QueryCondition = cprName,
                        IsDone = "B",
                        Status = "A"
                    };
                    new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                }

            }

        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="mesID"></param>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        private void UpdateMessageIsDone(int mesID)
        {
            string sql = "update cm_SysMsg set IsDone =N'D' Where SysNo=" + mesID + "";

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 删除消息根据人员
        /// </summary>
        /// <param name="mem_id"></param>
        /// <param name="msgID"></param>
        private void DeleteMessageByMember(int msgID)
        {
            string sql = @"
                         delete from cm_SysMsg
                         where ReferenceSysNo=(select  ReferenceSysNo  from cm_SysMsg where SysNo=" + msgID + ") " + @"
                         and MsgType=25 ";

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }
        /// <summary>
        /// 更新消息根据人员
        /// </summary>
        /// <param name="mem_id"></param>
        /// <param name="msgID"></param>
        private void UpdateMessageBySpeAudit(int msgID, int msgType)
        {
            string sql = @"
                         update cm_SysMsg set IsDone='D'
                         where ReferenceSysNo=(select  ReferenceSysNo  from cm_SysMsg where SysNo=" + msgID + ") " + @"
                         and MsgType=" + msgType;

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}