﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Services;
using Newtonsoft.Json;
using TG.Model;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Web.Script.Serialization;

namespace TG.Web.HttpHandler.ProjectValueandAllot
{
    /// <summary>
    /// TranProjectValueAllot 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class TranProjectValueAllot : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }
        public string Flag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        public TG.Model.TranProjectValueAllotByMemberEntity ProjectValueAllotByMemberEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.TranProjectValueAllotByMemberEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.TranProjectValueAllotByMemberEntity>(data);
                }
                return obj;
            }
        }

        public TG.Model.cm_TranProjectValueAuditRecord TranProjectValueAuditRecordEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_TranProjectValueAuditRecord obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_TranProjectValueAuditRecord>(data);
                }
                return obj;
            }
        }
        protected int UserSysNo
        {
            get
            {
                return this.UserInfo["memid"] == null ? 0 : Convert.ToInt32(this.UserInfo["memid"]);
            }
        }

        public TG.Model.cm_TranBulidingProjectValueAllot TranBulidingProjectValueAllotEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_TranBulidingProjectValueAllot obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_TranBulidingProjectValueAllot>(data);
                }
                return obj;
            }
        }

        public TG.Model.TranBulidingProjectValueAllotByMemberEntity TranBulidingProjectValueAllotByMemberEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.TranBulidingProjectValueAllotByMemberEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    //s
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    obj = js.Deserialize<TG.Model.TranBulidingProjectValueAllotByMemberEntity>(data);
                    //obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.TranBulidingProjectValueAllotByMemberEntity>(data);
                }
                return obj;
            }
        }

        /// <summary>
        /// 人员审批
        /// </summary>
        public TG.Model.ProjectValueByMemberAuditStatusEntity projectValueByMemberAuditStatusEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.ProjectValueByMemberAuditStatusEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.ProjectValueByMemberAuditStatusEntity>(data);
                }
                return obj;
            }
        }

        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        public void ProcessRequest(HttpContext context)
        {
            if (Request.HttpMethod == "POST")
            {
                string proid = context.Request.Params["proID"] ?? "0";
                string allotID = context.Request.Params["AllotID"] ?? "0";
                string msgId = context.Request.Params["msgID"] ?? "0";
                int mem_id = int.Parse(context.Request.Params["mem_id"] ?? "0");

                switch (Action)
                {
                    case 0:
                        UpdataProjectValueAllotByMemberDetail();
                        break;
                    case 1://转暖通所审批
                        AuditTranHavcProjectValueAllot();
                        break;
                    case 2:
                        string sysID = context.Request.Params["sysID"] ?? "";
                        DeleteTranHanvProjectValue(proid, allotID, sysID);
                        break;
                    case 3:
                        string tempAllotID = context.Request.Params["AllotID"] ?? "";
                        string mark = context.Request.Params["mark"] ?? "";
                        InsertTranBulidingProjectValue(tempAllotID, mark);
                        break;
                    case 4:
                        AddTranBulidingProjectValueInfo();
                        break;
                    case 5:
                        AuditTranBulidingProjectValueInfo();
                        break;
                    case 6://删除转土建
                        string sysID_tj = context.Request.Params["sysID"] ?? "0";
                        DeleteTranBuldingProjectValue(proid, allotID, sysID_tj);
                        break;
                    case 7://转暖通所个人确认通过

                        string proType = context.Request.Params["proType"] ?? "";
                        TranHavcAuditByMember(msgId, proType);
                        break;
                    case 8://转暖通所个人确认不通过
                        string msgId_noAudit = context.Request.Params["msgID"] ?? "0";
                        TranHavcNoAuditByMember(msgId_noAudit);
                        break;
                    case 9://转暖通所所长再次确认通过
                        string sprproType = context.Request.Params["proType"] ?? "";
                        int sys_yes = int.Parse(context.Request.Params["sys_no"] ?? "0");
                        TranHavcAuditByHead(msgId, sprproType, sys_yes);
                        break;
                    case 10://转暖通所所长再次确认不通过                    
                        int sys_no = int.Parse(context.Request.Params["sys_no"] ?? "0");
                        string sprproType_no = context.Request.Params["proType"] ?? "";
                        TranHavcNoAuditByHead(msgId, int.Parse(allotID), int.Parse(proid), mem_id, sys_no, sprproType_no);
                        break;
                    case 11://转土建所个人确认通过
                        string proType_tj = context.Request.Params["proType"] ?? "";
                        TranTjsAuditByMember(msgId, proType_tj);
                        break;
                    case 12://转土建所个人确认不通过
                        TranTjsNoAuditByMember(msgId);
                        break;
                    case 13://土建所所长再次确认通过
                        string sprproType_tj = context.Request.Params["proType"] ?? "";
                        int sys_yes_tj = int.Parse(context.Request.Params["sys_no"] ?? "0");
                        TranTjsAuditByHead(msgId, sprproType_tj, sys_yes_tj);
                        break;
                    case 14://转土建所所长再次确认不通过                    
                        int sys_no_tj = int.Parse(context.Request.Params["sys_no"] ?? "0");
                        string sprproType_no_tj = context.Request.Params["proType"] ?? "";
                        TranTjsNoAuditByHead(msgId, int.Parse(allotID), int.Parse(proid), mem_id, sys_no_tj, sprproType_no_tj);
                        break;
                }
                Response.End();
            }
        }

        /// <summary>
        /// 更新转暖通所产值明细
        /// </summary>
        private void UpdataProjectValueAllotByMemberDetail()
        {

          
            TG.BLL.cm_TranProjectValueAllot bll = new BLL.cm_TranProjectValueAllot();
            int count = bll.InsertProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity);

            if (count > 0)
            {
                //添加审核状态
                TG.BLL.cm_TranProjectValueAuditRecord bllRecord = new TG.BLL.cm_TranProjectValueAuditRecord();
                TG.Model.cm_TranProjectValueAuditRecord modelRecord = new cm_TranProjectValueAuditRecord();
                modelRecord.ProSysNo = int.Parse(ProjectValueAllotByMemberEntity.ProNo.ToString());
                modelRecord.InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser);
                modelRecord.InDate = DateTime.Now;
                modelRecord.Status = "A";
                modelRecord.AllotID = ProjectValueAllotByMemberEntity.AllotID;
                modelRecord.ItemType = "nts";
                int sysNo = bllRecord.Add(modelRecord);



                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(ProjectValueAllotByMemberEntity.ProNo);
                TG.BLL.cm_ProjectValueAuditRecord bll_Aduit = new TG.BLL.cm_ProjectValueAuditRecord();
                //给参与的人员发消息
                DataTable dt = bll_Aduit.GetAuditTranMemberList(ProjectValueAllotByMemberEntity.ProNo, ProjectValueAllotByMemberEntity.AllotID, " and TranType='nts'").Tables[0];
                //给人员新增信息
                int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitTranMemberUserInfo(dt, ProjectValueAllotByMemberEntity.ProNo, ProjectValueAllotByMemberEntity.AllotID, ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser), "nts");
                if (countAuditMember > 0)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //发送消息给每个人员
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", ProjectValueAllotByMemberEntity.AllotID, ProjectValueAllotByMemberEntity.ProNo, sysNo, "trannts", "nts"),
                                FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                                MsgType = 25,//项目产值分配明细
                                ToRole = "0",
                                MessageContent = string.Format("关于{0}的转暖通所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                QueryCondition = pro == null ? "" : pro.pro_name,
                                IsDone = "A",
                                Status = "A"
                            };
                            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        }

                    }
                    Response.Write("4");

                }
                else
                {
                    Response.Write("0");
                }
               

            }
            else
            {
                Response.Write("0");
            }

            // }
        }

        /// <summary>
        /// 审核 转暖通所产值
        /// </summary>
        public void AuditTranHavcProjectValueAllot()
        {
            TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();

            int sysNo = bll.UpdateTranHavcProjectAllotValue(TranProjectValueAuditRecordEntity);

            if (sysNo > 0)
            {
                //更新此状态
                TG.Model.cm_TranProjectValueAuditRecord oldEntity = bll.GetModel(TranProjectValueAuditRecordEntity.SysNo);
                //查询项目信息
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(int.Parse(oldEntity.ProSysNo.ToString()));


                string MessageContent = "";
                if (TranProjectValueAuditRecordEntity.Status == "S")
                {
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转暖通所分配通过");
                }
                else
                {
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转暖通分配不通过");
                }
                int userSysno = TranProjectValueAuditRecordEntity.AuditUser == "" ? 0 : int.Parse(TranProjectValueAuditRecordEntity.AuditUser);
                //流程走完之后要给申请人发一条消息
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("proid={0}&allotID={1}&auditSysID={2}", oldEntity.ProSysNo.ToString(), oldEntity.AllotID, oldEntity.SysNo),
                    FromUser = int.Parse(oldEntity.InUser.ToString()),
                    InUser = userSysno,
                    MsgType = 19,
                    ToRole = "0",
                    MessageContent = MessageContent,
                    QueryCondition = pro.pro_name,
                    IsDone = "B",
                    Status = "A"
                };
                int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                if (resultcount > 0)
                {
                    SendMessageToUser((int)TranProjectValueAuditRecordEntity.AllotID, pro.bs_project_Id, pro.pro_name, "nts", userSysno);
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }

        }

        /// <summary>
        /// 删除转暖通所金额
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="allotID"></param>
        public void DeleteTranHanvProjectValue(string proid, string allotID, string sysID)
        {
            TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();

            int sysNo = bll.DeleteTranHavcProjectAllotValue(int.Parse(proid), int.Parse(allotID), sysID);

            Response.Write(sysNo);
        }

        /// <summary>
        /// 转暖通所-个人确认通过
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="proType"></param>
        private void TranHavcAuditByMember(string msgId, string proType)
        {
            int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
            int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
            int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
            TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();
            //取得参与项目的人员总数-转
            DataTable dt = bll.GetAuditTranMemberList(pro_id, allotID, " and TranType='nts'").Tables[0];
            int memberCount = dt.Rows.Count;

            //取得审批通过的人员的总数
            TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
            string strWhere = " proID =" + pro_id + " and AllotID=" + allotID + "  and Status='S' and TranType='nts' ";
            DataTable dt_user = bll_user.GetList(strWhere).Tables[0];
            int auditMemberCount = dt_user.Rows.Count;

            if ((memberCount - 1) == auditMemberCount)
            {
                if (Flag == "0")
                {
                    string roleSysNo = GetTranStatusProcess(3);
                    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));

                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        Response.Write(roleUserString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    //更新信息
                    int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "nts");

                    TG.BLL.cm_TranProjectValueAuditRecord bll_tran = new TG.BLL.cm_TranProjectValueAuditRecord();

                    //产值审核表更新状态
                    bll_tran.UpdateTranStatus(pro_id, allotID, "B", "nts");

                    //更新 状态 为代办事项   
                    UpdateMessageIsDone(int.Parse(msgId));


                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);

                    string msg = "";
                    string referenceSysNo = "";
                    msg = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转暖通所产值分配");
                    referenceSysNo = string.Format("proid={0}&allotID={1}&auditSysID={2}", pro_id, allotID, projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo);

                    //发送消息
                    TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = referenceSysNo,
                        FromUser = mem_id,
                        InUser = mem_id,
                        MsgType = 19,
                        MessageContent = msg,
                        QueryCondition = pro.pro_name,
                        IsDone = "A",
                        Status = "A"
                    };

                    string roleSysNo = string.Empty;
                    sysMessageViewEntity.ToRole = roleSysNo;

                    sysMessageViewEntity.ToRole = "0";

                    string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        Response.Write(sysMsgString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }

            }
            else
            {
                //更新信息
                int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "nts");
                if (count > 0)
                {
                    //更新 状态 为代办事项   
                    UpdateMessageIsDone(int.Parse(msgId));
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
        }

        /// <summary>
        /// 转暖通所 -个人确认不通过
        /// </summary>
        /// <param name="msgId"></param>
        private void TranHavcNoAuditByMember(string msgId)
        {
            TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
            //更新信息
            int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "nts");

            if (count > 0)
            {
                int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
                int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
                int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
                int sysID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo;

                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                TG.Model.tg_member model = new TG.BLL.tg_member().GetModel(mem_id);

                //给专业负责人发消息
                ArrayList arrayList = new ArrayList();

                arrayList = GetProjectValueRoleArrayName("暖通");

                //给专业负责人发送消息
                if (arrayList.Count > 0)
                {

                    for (int i = 0; i < arrayList.Count; i++)
                    {
                        //发送消息给每个人员
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotID, pro_id, sysID, "trannts", "nts"),
                            FromUser = int.Parse(arrayList[i].ToString()),
                            InUser = mem_id,
                            MsgType = 29,//转暖通所
                            ToRole = "0",
                            MessageContent = string.Format("关于{0}的转暖通所产值分配个人不同意，需所长重新分配产值！", pro == null ? "" : pro.pro_name),
                            QueryCondition = pro == null ? "" : pro.pro_name,
                            IsDone = "A",
                            Status = "A"
                        };
                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    }

                    //更新 状态 为代办事项   
                    UpdateMessageIsDone(int.Parse(msgId));
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 转暖通所-所长确认通过
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <param name="sprproType"></param>
        /// <param name="mem_id"></param>
        /// <param name="sys_no"></param>
        private void TranHavcAuditByHead(string msgID, string sprproType, int sys_no)
        {
            int mem_id = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser);
            int allotId = ProjectValueAllotByMemberEntity.AllotID == null ? 0 : ProjectValueAllotByMemberEntity.AllotID;
            int pro_id = ProjectValueAllotByMemberEntity.ProNo == null ? 0 : ProjectValueAllotByMemberEntity.ProNo;
            TG.BLL.cm_TranProjectValueAllot bll_user = new BLL.cm_TranProjectValueAllot();
            int count = bll_user.UpdateProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity);

            if (count > 0)
            {
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();

                //删除个人确认表本专业的人员信息
                new TG.BLL.cm_ProjectValueByMemberAuditStatus().Delete(pro_id, allotId, "nts");

                //重新给这些人发送审批信息
                DataTable dt = bll.GetAuditTranMemberList(pro_id, allotId, " and TranType='nts' ").Tables[0];

                //删掉本专业所有的审批记录 
                DeleteMessageByMember(int.Parse(msgID));

                //给人员新增信息
                int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitTranMemberUserInfo(dt, pro_id, allotId, mem_id, "nts");
                if (countAuditMember > 0)
                {
                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //发送消息给每个人员
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotId, pro_id, sys_no, "trannts", "nts"),
                                FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                InUser = mem_id,
                                MsgType = 25,//项目产值分配明细
                                ToRole = "0",
                                MessageContent = string.Format("关于{0}的转暖通所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                QueryCondition = pro == null ? "" : pro.pro_name,
                                IsDone = "A",
                                Status = "A"
                            };
                            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        }

                    }

                    //把本专业的信息更新
                    UpdateMessageBySpeAudit(int.Parse(msgID), 29);

                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 转暖通所-所长再次审批不通过
        /// </summary>
        /// <param name="msgid"></param>
        /// <param name="allotID"></param>
        /// <param name="pro_id"></param>
        /// <param name="mem_id"></param>
        /// <param name="sys_no"></param>
        /// <param name="sprproType_no"></param>
        private void TranHavcNoAuditByHead(string msgid, int allotID, int pro_id, int mem_id, int sys_no, string sprproType_no)
        {
            //给本人发送消息，
            string where = @" status='D' and proID=" + pro_id + " and AllotID=" + allotID + " and  TranType='nts'";
            DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetList(where).Tables[0];

            //批量更改人员审批状态
            int count = new TG.BLL.cm_ProjectValueByMemberAuditStatus().Update(dt);
            if (count > 0)
            {
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //发送消息给每个人员
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotID, pro_id, sys_no, "trannts", "nts"),
                        FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                        InUser = mem_id,
                        MsgType = 25,//项目产值分配明细
                        ToRole = "0",
                        MessageContent = string.Format("关于{0}的个人转暖通所产值确认不通过，所长审批不通过！", pro == null ? "" : pro.pro_name),
                        QueryCondition = pro == null ? "" : pro.pro_name,
                        IsDone = "A",
                        Status = "A"
                    };
                    new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                }

                //把本专业的信息更新
                UpdateMessageBySpeAudit(int.Parse(msgid), 29);

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }


        /// <summary>
        /// 添加转土建所
        /// </summary>
        public void InsertTranBulidingProjectValue(string tempAllotID, string mark)
        {

            TG.BLL.cm_TranBulidingProjectValueAllot bll = new TG.BLL.cm_TranBulidingProjectValueAllot();
            TG.Model.cm_TranBulidingProjectValueAllot model = new TG.Model.cm_TranBulidingProjectValueAllot();


            var modelEntity = TranBulidingProjectValueAllotEntity;
            model.Pro_ID = modelEntity.Pro_ID;
            model.AllotUser = modelEntity.AllotUser;
            model.AllotDate = DateTime.Now;
            model.AllotPercent = modelEntity.AllotPercent;
            model.AllotCount = modelEntity.AllotCount;
            model.Thedeptallotpercent = modelEntity.Thedeptallotpercent;
            model.Thedeptallotcount = modelEntity.Thedeptallotcount;
            model.ProgramPercent = 0;
            model.ProgramCount = 0;
            model.ItemType = modelEntity.ItemType;
            model.AllotID = modelEntity.AllotID;
            model.TotalCount = modelEntity.TotalCount;
            model.ActualAllountTime = modelEntity.ActualAllountTime;
            model.DesignUserID = modelEntity.DesignUserID;
            model.DesignUserName = modelEntity.DesignUserName;
            model.DesignManagerPercent = modelEntity.DesignManagerPercent;
            model.DesignManagerCount = modelEntity.DesignManagerCount;
            model.Mark = mark;
            int affectRow = 0;
            if (!string.IsNullOrEmpty(tempAllotID))
            {
                model.ID = int.Parse(tempAllotID);

                bll.Update(model);
            }
            else
            {
                //model.LastItemType = modelEntity.ItemType;
                affectRow = bll.Add(model);
            }
            Response.Write(affectRow);
        }

        /// <summary>
        /// 转土建明细
        /// </summary>
        public void AddTranBulidingProjectValueInfo()
        {

    
            TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();
            int count = bll.InsertTranBulidingProjectAllotValueByMemberDetail(TranBulidingProjectValueAllotByMemberEntity);
            TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(TranBulidingProjectValueAllotByMemberEntity.ProNo);

            if (count > 0)
            {
                int userSysNo = TranBulidingProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(TranBulidingProjectValueAllotByMemberEntity.AuditUser);
                //添加审核状态
                TG.Model.cm_TranProjectValueAuditRecord modelRecord = new cm_TranProjectValueAuditRecord();
                modelRecord.ProSysNo = int.Parse(TranBulidingProjectValueAllotByMemberEntity.ProNo.ToString());
                modelRecord.InUser = userSysNo;
                modelRecord.InDate = DateTime.Now;
                modelRecord.Status = "A";
                modelRecord.AllotID = TranBulidingProjectValueAllotByMemberEntity.AllotID;
                modelRecord.ItemType = "tjs";
                int sysNo = bll.Add(modelRecord);

                TG.BLL.cm_ProjectValueAuditRecord bll_Aduit = new TG.BLL.cm_ProjectValueAuditRecord();
                //给参与的人员发消息
                DataTable dt = bll_Aduit.GetAuditTranMemberList(ProjectValueAllotByMemberEntity.ProNo, ProjectValueAllotByMemberEntity.AllotID, " and TranType='tjs'").Tables[0];
                //给人员新增信息
                int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitTranMemberUserInfo(dt, ProjectValueAllotByMemberEntity.ProNo, ProjectValueAllotByMemberEntity.AllotID, ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser), "tjs");
                if (countAuditMember > 0)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //发送消息给每个人员
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", ProjectValueAllotByMemberEntity.AllotID, ProjectValueAllotByMemberEntity.ProNo, sysNo, "trantjs", "tjs"),
                                FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                InUser = ProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(ProjectValueAllotByMemberEntity.AuditUser),
                                MsgType = 25,//项目产值分配明细
                                ToRole = "0",
                                MessageContent = string.Format("关于{0}的转土建所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                QueryCondition = pro == null ? "" : pro.pro_name,
                                IsDone = "A",
                                Status = "A"
                            };
                            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        }

                    }
                    Response.Write("4");

                }
                else
                {
                    Response.Write("0");
                }

                
            }
            else
            {
                Response.Write("0");
            }
            // }
        }

        /// <summary>
        /// 转土建审核
        /// </summary>
        public void AuditTranBulidingProjectValueInfo()
        {
            TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();

            int sysNo = bll.UpdateTranBulidingProjectAllotValue(TranProjectValueAuditRecordEntity);

            if (sysNo > 0)
            {
                //更新此状态
                TG.Model.cm_TranProjectValueAuditRecord oldEntity = bll.GetModel(TranProjectValueAuditRecordEntity.SysNo);
                //查询项目信息
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(int.Parse(oldEntity.ProSysNo.ToString()));


                string MessageContent = "";
                if (TranProjectValueAuditRecordEntity.Status == "S")
                {
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转土建所分配通过");
                }
                else
                {
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转土建所分配不通过");
                }
                int userSysno = TranProjectValueAuditRecordEntity.AuditUser == "" ? 0 : int.Parse(TranProjectValueAuditRecordEntity.AuditUser);

                //流程走完之后要给申请人发一条消息
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("proid={0}&allotID={1}&auditSysID={2}", oldEntity.ProSysNo.ToString(), oldEntity.AllotID, oldEntity.SysNo),
                    FromUser = int.Parse(oldEntity.InUser.ToString()),
                    InUser = userSysno,
                    MsgType = 20,
                    ToRole = "0",
                    MessageContent = MessageContent,
                    QueryCondition = pro.pro_name,
                    IsDone = "B",
                    Status = "A"
                };
                int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                if (resultcount > 0)
                {
                    SendMessageToUser((int)TranProjectValueAuditRecordEntity.AllotID, pro.bs_project_Id, pro.pro_name, "tjs", userSysno);
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
        }

        /// <summary>
        /// 删除转土建所金额
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="allotID"></param>
        public void DeleteTranBuldingProjectValue(string proid, string allotID, string sysID)
        {
            TG.BLL.cm_TranProjectValueAuditRecord bll = new TG.BLL.cm_TranProjectValueAuditRecord();

            int sysNo = bll.DeleteTranBudlingProjectAllotValue(int.Parse(proid), int.Parse(allotID), sysID);

            Response.Write(sysNo);
        }

        /// <summary>
        /// 转土建所-个人确认通过
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="proType"></param>
        private void TranTjsAuditByMember(string msgId, string proType)
        {
            int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
            int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
            int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
            TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();
            //取得参与项目的人员总数-转
            DataTable dt = bll.GetAuditTranMemberList(pro_id, allotID, " and TranType='tjs'").Tables[0];
            int memberCount = dt.Rows.Count;

            //取得审批通过的人员的总数
            TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
            string strWhere = " proID =" + pro_id + " and AllotID=" + allotID + "  and Status='S' and TranType='tjs' ";
            DataTable dt_user = bll_user.GetList(strWhere).Tables[0];
            int auditMemberCount = dt_user.Rows.Count;

            if ((memberCount - 1) == auditMemberCount)
            {
                if (Flag == "0")
                {
                    string roleSysNo = GetTranStatusProcess(3);
                    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNo));

                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        Response.Write(roleUserString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    //更新信息
                    int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "tjs");

                    TG.BLL.cm_TranProjectValueAuditRecord bll_tran = new TG.BLL.cm_TranProjectValueAuditRecord();

                    //产值审核表更新状态
                    bll_tran.UpdateTranStatus(pro_id, allotID, "B", "tjs");

                    //更新 状态 为代办事项   
                    UpdateMessageIsDone(int.Parse(msgId));


                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);

                    string msg = "";
                    string referenceSysNo = "";
                    msg = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "转土建所产值分配");
                    referenceSysNo = string.Format("proid={0}&allotID={1}&auditSysID={2}", pro_id, allotID, projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo);

                    //发送消息
                    TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = referenceSysNo,
                        FromUser = mem_id,
                        InUser = mem_id,
                        MsgType = 20,
                        MessageContent = msg,
                        QueryCondition = pro.pro_name,
                        IsDone = "A",
                        Status = "A"
                    };

                    string roleSysNo = string.Empty;
                    sysMessageViewEntity.ToRole = roleSysNo;

                    sysMessageViewEntity.ToRole = "0";

                    string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        Response.Write(sysMsgString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }

            }
            else
            {
                //更新信息
                int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "tjs");
                if (count > 0)
                {
                    //更新 状态 为代办事项   
                    UpdateMessageIsDone(int.Parse(msgId));
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
        }

        /// <summary>
        /// 转土建所 -个人确认不通过
        /// </summary>
        /// <param name="msgId"></param>
        private void TranTjsNoAuditByMember(string msgId)
        {
            TG.BLL.cm_ProjectValueByMemberAuditStatus bll_user = new TG.BLL.cm_ProjectValueByMemberAuditStatus();
            //更新信息
            int count = bll_user.TranUpdate(projectValueByMemberAuditStatusEntity, "tjs");

            if (count > 0)
            {
                int pro_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.ProID;
                int mem_id = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.mem_id;
                int allotID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.AllotID;
                int sysID = projectValueByMemberAuditStatusEntity == null ? 0 : projectValueByMemberAuditStatusEntity.SysNo;

                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                TG.Model.tg_member model = new TG.BLL.tg_member().GetModel(mem_id);

                //给专业负责人发消息
                ArrayList arrayList = new ArrayList();

                arrayList = GetTranTjsRoleArrayName(allotID);

                //给专业负责人发送消息
                if (arrayList.Count > 0)
                {

                    for (int i = 0; i < arrayList.Count; i++)
                    {
                        //发送消息给每个人员
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotID, pro_id, sysID, "trantjs", "tjs"),
                            FromUser = int.Parse(arrayList[i].ToString()),
                            InUser = mem_id,
                            MsgType = 30,//转土建所
                            ToRole = "0",
                            MessageContent = string.Format("关于{0}的转土建所产值分配个人不同意，需所长重新分配产值！", pro == null ? "" : pro.pro_name),
                            QueryCondition = pro == null ? "" : pro.pro_name,
                            IsDone = "A",
                            Status = "A"
                        };
                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    }

                    //更新 状态 为代办事项   
                    UpdateMessageIsDone(int.Parse(msgId));
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 转土建所-所长确认通过
        /// </summary>
        /// <param name="msgID"></param>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <param name="sprproType"></param>
        /// <param name="mem_id"></param>
        /// <param name="sys_no"></param>
        private void TranTjsAuditByHead(string msgID, string sprproType, int sys_no)
        {
            int mem_id = TranBulidingProjectValueAllotByMemberEntity.AuditUser == "" ? 0 : int.Parse(TranBulidingProjectValueAllotByMemberEntity.AuditUser);
            int allotId = TranBulidingProjectValueAllotByMemberEntity.AllotID == null ? 0 : TranBulidingProjectValueAllotByMemberEntity.AllotID;
            int pro_id = TranBulidingProjectValueAllotByMemberEntity.ProNo == null ? 0 : TranBulidingProjectValueAllotByMemberEntity.ProNo;
            TG.BLL.cm_TranProjectValueAuditRecord bll_user = new BLL.cm_TranProjectValueAuditRecord();
            int count = bll_user.UpdateTranBulidingProjectAllotValueByMemberDetail(TranBulidingProjectValueAllotByMemberEntity);

            if (count > 0)
            {
                TG.BLL.cm_ProjectValueAuditRecord bll = new TG.BLL.cm_ProjectValueAuditRecord();

                //删除个人确认表本专业的人员信息
                new TG.BLL.cm_ProjectValueByMemberAuditStatus().Delete(pro_id, allotId, "tjs");

                //重新给这些人发送审批信息
                DataTable dt = bll.GetAuditTranMemberList(pro_id, allotId, " and TranType='tjs' ").Tables[0];

                //删掉本专业所有的审批记录 
                DeleteMessageByMember(int.Parse(msgID));

                //给人员新增信息
                int countAuditMember = new TG.BLL.cm_ProjectValueByMemberAuditStatus().AddAduitTranMemberUserInfo(dt, pro_id, allotId, mem_id, "tjs");
                if (countAuditMember > 0)
                {
                    TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //发送消息给每个人员
                            SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotId, pro_id, sys_no, "trantjs", "tjs"),
                                FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                                InUser = mem_id,
                                MsgType = 25,//项目产值分配明细
                                ToRole = "0",
                                MessageContent = string.Format("关于{0}的转土建所产值分配个人明细确认信息！", pro == null ? "" : pro.pro_name),
                                QueryCondition = pro == null ? "" : pro.pro_name,
                                IsDone = "A",
                                Status = "A"
                            };
                            new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        }

                    }

                    //把本专业的信息更新
                    UpdateMessageBySpeAudit(int.Parse(msgID), 30);

                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 转土建所-所长再次审批不通过
        /// </summary>
        /// <param name="msgid"></param>
        /// <param name="allotID"></param>
        /// <param name="pro_id"></param>
        /// <param name="mem_id"></param>
        /// <param name="sys_no"></param>
        /// <param name="sprproType_no"></param>
        private void TranTjsNoAuditByHead(string msgid, int allotID, int pro_id, int mem_id, int sys_no, string sprproType_no)
        {
            //给本人发送消息，
            string where = @" status='D' and proID=" + pro_id + " and AllotID=" + allotID + " and  TranType='tjs'";
            DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetList(where).Tables[0];

            //批量更改人员审批状态
            int count = new TG.BLL.cm_ProjectValueByMemberAuditStatus().Update(dt);
            if (count > 0)
            {
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(pro_id);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //发送消息给每个人员
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("allotID={0}&proID={1}&ValueAllotAuditSysNo={2}&proType={3}&tranType={4}", allotID, pro_id, sys_no, "trantjs", "tjs"),
                        FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                        InUser = mem_id,
                        MsgType = 25,//项目产值分配明细
                        ToRole = "0",
                        MessageContent = string.Format("关于{0}的个人转土建所产值确认不通过，所长审批不通过！", pro == null ? "" : pro.pro_name),
                        QueryCondition = pro == null ? "" : pro.pro_name,
                        IsDone = "A",
                        Status = "A"
                    };
                    new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                }

                //把本专业的信息更新
                UpdateMessageBySpeAudit(int.Parse(msgid), 30);

                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }

        private string GetStatusProcess()
        {
            string sql = "select * from cm_Role where RoleName ='生产经营部'";
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            return resultObj == null ? "0" : resultObj.ToString();
        }

        private string GetTranStatusProcess(int postion)
        {
            string sql = "select RoleSysNo from cm_TranProjectFeeAllotConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return resultObj == null ? "0" : resultObj.ToString();
        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="mesID"></param>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        private void UpdateMessageIsDone(int mesID)
        {
            string sql = "update cm_SysMsg set IsDone =N'D' Where SysNo=" + mesID + "";

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }
        /// <summary>
        /// 获取用户列表-所长
        /// </summary>
        /// <returns></returns>
        private ArrayList GetProjectValueRoleArrayName(string unit_name)
        {
            ArrayList arrayList = new ArrayList();

            //经济所长
            string sqlnts = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_Name like'%" + unit_name.Trim() + "%' and   C.pri_Name LIKE '%所长%' ";

            SqlDataReader readernts = TG.DBUtility.DbHelperSQL.ExecuteReader(sqlnts);

            while (readernts.Read())
            {
                arrayList.Add(readernts["UserSysNo"]);
            }

            return arrayList;

        }

        /// <summary>
        /// 得到转经济所
        /// </summary>
        /// <param name="allotId"></param>
        /// <returns></returns>
        private ArrayList GetTranTjsRoleArrayName(int allotId)
        {
            ArrayList arrayList = new ArrayList();
            TG.Model.cm_ProjectValueAllot model = new TG.BLL.cm_ProjectValueAllot().GetModel(allotId);
            if (model != null)
            {
                string sqlnts = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_ID =" + model.UnitId + " and   C.pri_Name LIKE '%所长%' ";
                SqlDataReader readernts = TG.DBUtility.DbHelperSQL.ExecuteReader(sqlnts);

                while (readernts.Read())
                {
                    arrayList.Add(readernts["UserSysNo"]);
                }
            }


            return arrayList;
        }
        /// <summary>
        /// 删除消息根据人员
        /// </summary>
        /// <param name="mem_id"></param>
        /// <param name="msgID"></param>
        private void DeleteMessageByMember(int msgID)
        {
            string sql = @"
                         delete from cm_SysMsg
                         where ReferenceSysNo=(select  ReferenceSysNo  from cm_SysMsg where SysNo=" + msgID + ") " + @"
                         and MsgType=25 ";

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 更新消息根据人员
        /// </summary>
        /// <param name="mem_id"></param>
        /// <param name="msgID"></param>
        private void UpdateMessageBySpeAudit(int msgID, int msgType)
        {
            string sql = @"
                         update cm_SysMsg set IsDone='D'
                         where ReferenceSysNo=(select  ReferenceSysNo  from cm_SysMsg where SysNo=" + msgID + ") " + @"
                         and MsgType=" + msgType;

            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 给每个人分配到的人员发送消息
        /// </summary>
        /// <param name="allotID"></param>
        protected void SendMessageToUser(int allotID, int cprID, string cprName, string itemType, int userSysno)
        {

            string sql = string.Format("select  distinct mem_ID  from cm_ProjectValueByMemberDetails where AllotID={0} and IsExternal='0' and TranType='" + itemType + "'", allotID);
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];

            string messageContent = "";
            if (itemType == "nts")
            {
                messageContent=string.Format("关于{0}的转暖通所分配个人明细信息！", cprName);
            }
            else
            {
                messageContent=string.Format("关于{0}的转土建所分配个人明细信息！", cprName);
            }
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //发送消息给每个人员
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("AllotID={0}&proID={1}&tranType={2}", allotID, cprID, itemType),
                        FromUser = int.Parse(dt.Rows[i]["mem_ID"].ToString()),
                        InUser = userSysno,
                        MsgType = 10,//项目产值分配明细
                        ToRole = "0",
                        MessageContent = messageContent,
                        QueryCondition = cprName,
                        IsDone = "B",
                        Status = "A"
                    };
                    new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                }

            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}