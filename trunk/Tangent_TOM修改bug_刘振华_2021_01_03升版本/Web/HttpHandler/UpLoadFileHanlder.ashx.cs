﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using TG.BLL;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class UpLoadFileHanlder : IHttpHandler
    {

        public HttpRequest Request { get { return HttpContext.Current.Request; } }

        public HttpResponse Response { get { return HttpContext.Current.Response; } }

        /// <summary>
        /// 系统自增号
        /// </summary>
        public int SysNo
        {
            get
            {
                int sysNo = 0;
                int.TryParse(Request["SysNo"], out sysNo);
                return sysNo;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            GetFileAttachInfo();
        }


        /// <summary>
        /// 得到用户附近信息
        /// </summary>
        private void GetFileAttachInfo()
        {
            //正确的场合
            if (SysNo > 0)
            {
                TG.Model.cm_AttachInfo attachInfo = new cm_AttachInfo().GetModel(SysNo);
                if (new TG.BLL.tg_member().GetModel(int.Parse(attachInfo.UploadUser)) != null)
                {
                    attachInfo.UploadUser = new TG.BLL.tg_member().GetModel(int.Parse(attachInfo.UploadUser)).mem_Name;
                }
                string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(attachInfo);
                Response.Write(jsonResult);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// 将一个实体对象转换成JSON格式字符串
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string ToJSON(TG.Model.cm_AttachInfo model)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{");
            Type type = model.GetType();
            foreach (var propertyInfo in type.GetProperties())
            {
                var propertyValue = propertyInfo.GetValue(model, null);
                jsonbuild.Append(propertyInfo.Name);
                jsonbuild.Append(":");
                if (propertyValue == null)
                    jsonbuild.Append("null");
                else
                    jsonbuild.Append(propertyValue);
                jsonbuild.Append(",");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }

    }
}
