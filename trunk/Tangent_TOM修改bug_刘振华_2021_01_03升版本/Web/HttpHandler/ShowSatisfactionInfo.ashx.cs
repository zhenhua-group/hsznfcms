﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ShowSatisfactionInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "text/plain";
            string str_sat = "";
            string str_satid = context.Request.Params["sat_id"];


            TG.BLL.cm_SatisfactionInfo bll = new TG.BLL.cm_SatisfactionInfo();
            TG.Model.cm_SatisfactionInfo model = bll.GetModel(int.Parse(str_satid));
            string str_satNo = model.Sat_No;
            str_sat += str_satNo + "|";
            string str_Sch_Context = model.Sch_Context;
            str_sat += str_Sch_Context + "|";
            if (model.Remark != null)
            {
                string str_Remark = model.Remark.Trim();
                str_sat += str_Remark + "|";
            }
            string str_satBest = model.Sat_Best != null ? model.Sat_Best.Trim() : "";
            str_sat += str_satBest + "|";
            string str_satNotBest = model.Sat_NotBest != null ? model.Sat_NotBest.Trim() : "";
            str_sat += str_satNotBest + "|";
            string str_satLeve = string.Empty;
            switch (model.Sat_Leve.ToString())
            {
                case "1":
                    str_satLeve = "满意";
                    str_sat += str_satLeve + "|";
                    break;
                case "2":
                    str_satLeve = "非常满意";
                    str_sat += str_satLeve + "|";
                    break;
                case "3":
                    str_satLeve = "不满意";
                    str_sat += str_satLeve + "|";
                    break;
                case "4":
                    str_satLeve = "非常不满意";
                    str_sat += str_satLeve + "|";
                    break;

                default:
                    break;
            }

            string str_schTime = string.Format("{0:d}", model.Sch_Time);
            str_sat += str_schTime + "|";
            string str_schWay = string.Empty;
            //this.lblSch_Way.Text = model.Sch_Way.ToString();
            switch (model.Sch_Way.ToString())
            {
                case "1":
                    str_schWay = "问卷调查";
                    str_sat += str_schWay + "|";
                    break;
                case "2":
                    str_schWay = "统计调查";
                    str_sat += str_schWay + "|";
                    break;
                case "3":
                    str_schWay = "询问调查";
                    str_sat += str_schWay + "|";
                    break;
                case "4":
                    str_schWay = "电话调查";
                    str_sat += str_schWay + "|";
                    break;
                default:
                    break;
            }
            context.Response.Write(str_sat);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
