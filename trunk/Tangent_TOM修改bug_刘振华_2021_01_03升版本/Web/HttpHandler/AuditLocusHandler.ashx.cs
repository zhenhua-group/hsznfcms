﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using TG.BLL;
using Newtonsoft.Json;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class AuditLocusHandler : HandlerCommon, IHttpHandler
    {
        /// <summary>
        /// 引用的ReferenceSysNo
        /// </summary>
        public int ReferenceSysNo
        {
            get
            {
                int referenceSysNo = 0;
                int.TryParse(Request["ReferenceSysNo"], out referenceSysNo);
                return referenceSysNo;
            }
        }

        /// <summary>
        /// 请求审核类型，C为合同、
        /// </summary>
        public string Action
        {
            get
            {
                return Request["Action"];
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            GetAuditLocusList();
            Response.End();
        }
        /// <summary>
        /// 是否转经济所 -- 项目产值分配用到
        /// </summary>
        public string IstranEconomy
        {
            get
            {
                return Request["IstranEconomy"];
            }
        }
        /// <summary>
        /// 是否转暖通 -- 项目产值分配用到
        /// </summary>
        public string ISTrunHavc
        {
            get
            {
                return Request["ISTrunHavc"];
            }
        }
        private void GetAuditLocusList()
        {
            object auditLocusList = null;
            switch (Action)
            {
                case "C":
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_CoperationAudit>(ReferenceSysNo);
                    break;
                case "EC":
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_CoperationAuditEdit>(ReferenceSysNo);
                    break;
                case "PN":
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_ProjectPlanAuditEntity>(ReferenceSysNo);
                    break;
                case "P":
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.ProjectAuditListViewDataEntity>(ReferenceSysNo);
                    break;
                case "EP":
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.ProjectAuditEditEntity>(ReferenceSysNo);
                    break;
                case "A":
                    auditLocusList = AuditLocusBP.GetProjectAllotAuditRecordByReferenceSysNo<TG.Model.cm_ProjectValueAuditRecord>(ReferenceSysNo, "1", "1");
                    break;
                case "M":
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_ProImaAudit>(ReferenceSysNo);
                    break;
                case "S":
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_ProjectValueAuditRecord>(ReferenceSysNo);
                    break;
                case "H":
                    auditLocusList = AuditLocusBP.GetHavcProjectAllotAuditRecordByReferenceSysNo<TG.Model.cm_ProjectValueAuditRecord>(ReferenceSysNo, "1");
                    break;
                case "JJS"://经济所
                    auditLocusList = AuditLocusBP.GetJjsProjectAllotAuditRecordByReferenceSysNo<TG.Model.cm_ProjectValueAuditRecord>(ReferenceSysNo);
                    break;
                case "TranJJS"://转经济所
                    auditLocusList = AuditLocusBP.GetTranJjsProjectAllotAuditRecordByReferenceSysNo<TG.Model.cm_TranProjectValueAuditRecord>(ReferenceSysNo);
                    break;
                case "PLOT"://工程出图
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_ProjectPlotInfoAudit>(ReferenceSysNo);
                    break;
                case "File"://归档资料
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_ProjectFileInfoAudit>(ReferenceSysNo);
                    break;
                case "SuperC": //监理公司
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_SuperCoperationAudit>(ReferenceSysNo);
                    break;
                case "SuperEC": //监理公司修改申请
                    auditLocusList = AuditLocusBP.GetAuditRecordByReferenceSysNo<TG.Model.cm_SuperCoperationAuditRepeat>(ReferenceSysNo);
                    break;
            }

            Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(auditLocusList));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
