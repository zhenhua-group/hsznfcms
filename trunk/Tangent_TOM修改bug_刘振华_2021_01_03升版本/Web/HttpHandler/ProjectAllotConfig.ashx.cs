﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProjectAllotConfig : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string id = context.Request.Params["id"] ?? "";
            string type = context.Request.Params["type"] ?? "";

            if (type == "cmp")
            {
                TG.BLL.cm_CostCompanyConfig bll = new TG.BLL.cm_CostCompanyConfig();
                if (bll.Delete(int.Parse(id)))
                {
                    context.Response.Write("success");
                }
                else
                {
                    context.Response.Write("failed");
                }
            }
            else if (type == "spe")
            {
                TG.BLL.cm_CostSpeConfig bll = new TG.BLL.cm_CostSpeConfig();
                if (bll.Delete(int.Parse(id)))
                {
                    context.Response.Write("success");
                }
                else
                {
                    context.Response.Write("failed");
                }
            }
            else if (type == "user")
            {
                TG.BLL.cm_CostUserConfig bll = new TG.BLL.cm_CostUserConfig();
                if (bll.Delete(int.Parse(id)))
                {
                    context.Response.Write("success");
                }
                else
                {
                    context.Response.Write("failed");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
