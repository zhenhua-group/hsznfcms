﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.ProjectMamage.ProImageAudit
{
    /// <summary>
    /// ProjectDesignHandler1 的摘要说明
    /// </summary>
    public class ProjectDesignHandler1 : ProjectDesignHandler
    {

        public override string ProcName
        {
            get
            {
                return "P_cm_ProjectPlotInfoNo_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_ProjectPlotInfoYes_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_ProjectFileInfoNo_jq";
            }
        }
        public override string ProcName4
        {
            get
            {
                return "P_cm_ProjectFileInfoYes_jq";
            }
        }
    }
}