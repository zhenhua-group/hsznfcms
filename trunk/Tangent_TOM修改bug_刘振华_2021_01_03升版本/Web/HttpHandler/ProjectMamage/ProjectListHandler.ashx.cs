﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.ProjectMamage
{
    /// <summary>
    /// ProjectListHandler1 的摘要说明
    /// </summary>
    public class ProjectListHandler1 : ProjectListHandler
    {
        public override string ProcName
        {
            get
            {
                return "P_cm_Project_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_ProjectEdit_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_tg_Project_jq";
            }
        }
    }
}