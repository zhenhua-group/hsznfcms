﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;


namespace TG.Web.HttpHandler.ProjectMamage
{
    public abstract class ProjectListHandler : IHttpHandler
    {//项目和项目评审存储过程名
        public abstract string ProcName { get; }
        //项目申请修改存储过程名
        public abstract string ProcName2 { get; }
        //项目概况查询存储过程名
        public abstract string ProcName3 { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.Params["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.Params["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                string year = DateTime.Now.Year.ToString();
                string sqlwhere = "";
                //年份
                if (year != "-1")
                {
                    sqlwhere = " AND year(pro_startTime)=" + year;
                }
                sqlwhere = sqlwhere + strWhere;
                parameters.Where = sqlwhere;

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                string strResponse = "";
                TG.BLL.cm_Project bll = new TG.BLL.cm_Project();
                TG.Model.cm_Project model = new TG.Model.cm_Project();
                string strid = forms.Get("EmpId");

                ArrayList listAudit = bll.DeleteList(strid);
                //存在审批中的合同
                if (listAudit.Count > 0)
                {
                    string message = "";

                    for (int i = 0; i < listAudit.Count; i++)
                    {
                        model = bll.GetModel(int.Parse(listAudit[i].ToString()));
                        if (model != null)
                        {
                            message = message + model.pro_name + "项目在审批中，或者审批完毕\r\n ";
                        }
                    }
                    message = message + "不能删除";
                    strResponse = message;
                }
                else
                {
                    strResponse = "删除成功";
                }

                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {

                string unit = context.Request.Params["unit"];
                string year = context.Request.Params["year"];
                string keyname = context.Request.Params["keyname"];
                string startTime = context.Request.Params["startTime"];//录入时间
                string endTime = context.Request.Params["endTime"];//录入时间  
                string txtproAcount = context.Request.Params["txtproAcount"];
                string txtproAcount2 = context.Request.Params["txtproAcount2"];
                string txt_buildArea = context.Request.Params["txt_buildArea"];
                string txt_buildArea2 = context.Request.Params["txt_buildArea2"];
                string ddrank = context.Request.Params["ddrank"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname) && keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname.Trim());
                }

                //按生产部门查询0.230，321，

                if (!string.IsNullOrEmpty(unit))
                {
                    string[] arrayunit = unit.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    strsql.Append("and   (");
                    string str = "";
                    for (int i = 0; i < arrayunit.Length; i++)
                    {

                        if (!arrayunit[i].Contains("全部部门"))
                        {
                            str += "(Unit= " + arrayunit[i].Trim() + ") OR";
                        }
                    }
                    str = str.Substring(0, str.Length - 2);
                    strsql.Append(str);
                    strsql.Append(")");
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(pro_startTime)={0} ", year);
                }
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }

                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                //合同额
                if (!string.IsNullOrEmpty(txtproAcount) && string.IsNullOrEmpty(txtproAcount2))
                {

                    strsql.Append("and ( cpr_Acount >= " + txtproAcount.Trim() + " )");
                }
                else if (string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {

                    strsql.Append(" and ( cpr_Acount <= " + txtproAcount2.Trim() + " )");
                }
                else if (!string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {

                    strsql.Append("and (cpr_Acount>=" + txtproAcount.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
                }
                //建设规模
                if (!string.IsNullOrEmpty(txt_buildArea) && string.IsNullOrEmpty(txt_buildArea2))
                {

                    strsql.Append(" and ( ProjectScale >=" + txt_buildArea.Trim() + " )");
                }
                else if (string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
                {

                    strsql.Append(" and ( ProjectScale <=" + txt_buildArea2.Trim() + " )");
                }
                else if (!string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
                {

                    strsql.Append(" and (ProjectScale >=" + txt_buildArea.Trim() + " AND ProjectScale <=" + txt_buildArea2.Trim() + ") ");
                }
                if (!string.IsNullOrEmpty(ddrank) && !ddrank.Contains("选择类别"))
                {
                    strsql.Append(" and ( BuildType=N'" + ddrank.Trim() + "' )");
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "relationcpr")
            {

                string unit = context.Request.Params["unit"];
                string keyname = context.Request.Params["keyname"];
                string startTime = context.Request.Params["startTime"];//录入时间
                string endTime = context.Request.Params["endTime"];//录入时间 
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (unit != "-1" && unit != null && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (Unit= '{0}')", unit);
                }

                strsql.Append("  AND CoperationSysNo=0 ");
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "select")
            {
                string andor = context.Request["andor"] ?? "and";//查询方式
                string txt_proName = context.Request.Params["txt_proName"];
                string txt_buildUnit = context.Request.Params["txt_buildUnit"];
                string txtaddress = context.Request.Params["txtaddress"];
                string drp_unit = context.Request.Params["drp_unit"];
                string year = context.Request.Params["year"];
                string ddType = context.Request.Params["ddType"];
                string ddsource = context.Request.Params["ddsource"];
                string txt_Aperson = context.Request.Params["txt_Aperson"];
                string txt_phone = context.Request.Params["txt_phone"];
                string txt_PMName = context.Request.Params["txt_PMName"];
                string txt_PMPhone = context.Request.Params["txt_PMPhone"];
                string txtproAcount = context.Request.Params["txtproAcount"];
                string txtproAcount2 = context.Request.Params["txtproAcount2"];
                string txt_buildArea = context.Request.Params["txt_buildArea"];
                string txt_buildArea2 = context.Request.Params["txt_buildArea2"];
                string txt_signdate = context.Request.Params["txt_signdate"];
                string txt_signdate2 = context.Request.Params["txt_signdate2"];
                string txt_finishdate = context.Request.Params["txt_finishdate"];
                string txt_finishdate2 = context.Request.Params["txt_finishdate2"];
                string ddrank = context.Request.Params["ddrank"];
                string managerLevelDropDownList = context.Request.Params["managerLevelDropDownList"];
                string str_jd = context.Request.Params["str_jd"];
                string cybm = context.Request.Params["cybm"];
                string asTreeviewStruct = context.Request.Params["asTreeviewStruct"];
                string asTreeviewStructType = context.Request.Params["asTreeviewStructType"];

                string startTime = context.Request.Params["startTime"];//录入时间
                string endTime = context.Request.Params["endTime"];//录入时间  
                StringBuilder sb = new StringBuilder("");
                //组合查询
                if (!string.IsNullOrEmpty(txt_proName))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_proName.Trim());
                    sb.Append(" AND ( pro_name LIKE '%" + keyname + "%' ");
                }
                if (!string.IsNullOrEmpty(txt_buildUnit))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_buildUnit.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  pro_buildUnit LIKE '%" + keyname + "%'  ");
                }
                if (year != "-1" && !string.IsNullOrEmpty(year))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }

                    sb.AppendFormat(" year(InsertDate)={0} ", year);
                }
                if (!string.IsNullOrEmpty(txtaddress))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txtaddress.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  BuildAddress LIKE '%" + keyname + "%'  ");
                }
                if (!string.IsNullOrEmpty(drp_unit) && !drp_unit.Contains("请选择"))
                {
                    drp_unit = drp_unit.Substring(0, drp_unit.Length - 1);
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Unit  in (" + drp_unit.Trim() + ")");
                }
                if (!string.IsNullOrEmpty(ddType) && !ddType.Contains("请选择"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append("  Industry LIKE '%" + ddType.Trim() + "%'  ");
                }
                if (!string.IsNullOrEmpty(ddsource) && ddsource != "-1")
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Pro_src=" + ddsource.Trim() + "  ");
                }
                if (!string.IsNullOrEmpty(txt_Aperson))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_Aperson.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ChgJia LIKE '%" + keyname + "%'  ");
                }
                if (!string.IsNullOrEmpty(txt_phone))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_phone.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" Phone LIKE '%" + keyname + "%'  ");
                }
                if (!string.IsNullOrEmpty(txt_PMName))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_PMName.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" PMName LIKE '%" + keyname + "%'  ");
                }
                if (!string.IsNullOrEmpty(txt_PMPhone))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_PMPhone.Trim());
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" PMPhone LIKE '%" + keyname + "%'  ");
                }
                //合同额
                if (!string.IsNullOrEmpty(txtproAcount) && string.IsNullOrEmpty(txtproAcount2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Acount >= " + txtproAcount.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" cpr_Acount <= " + txtproAcount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (cpr_Acount>=" + txtproAcount.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
                }
                //建设规模
                if (!string.IsNullOrEmpty(txt_buildArea) && string.IsNullOrEmpty(txt_buildArea2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ProjectScale >=" + txt_buildArea.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ProjectScale <=" + txt_buildArea2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (ProjectScale >=" + txt_buildArea.Trim() + " AND ProjectScale <=" + txt_buildArea2.Trim() + ") ");
                }
                //时间
                if (!string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (pro_startTime BETWEEN '" + txt_signdate.Trim() + "' AND '" + txt_signdate2.Trim() + "') ");
                }
                if (!string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" (pro_finishTime BETWEEN '" + txt_finishdate.Trim() + "' AND '" + txt_finishdate2.Trim() + "') ");
                }
                if (!string.IsNullOrEmpty(ddrank) && !ddrank.Contains("选择类别"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" BuildType like N'%" + ddrank.Trim() + "%' ");
                }
                if (!string.IsNullOrEmpty(managerLevelDropDownList) && managerLevelDropDownList.Trim() != "-1")
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" pro_level=" + managerLevelDropDownList.Trim() + "  ");
                }
                //if (string.IsNullOrEmpty(txtproAcount) == false && new Regex("^[0-9]+$").Match(txtproAcount.Trim()).Success && txtproAcount.Trim() != "0")
                //{
                //    if (sb.ToString() != "")
                //    {
                //        sb.Append(" " + andor + " ");
                //    }
                //    else
                //    {
                //        sb.Append(" and ( ");
                //    }
                //    sb.Append(" Cpr_Acount = " + txtproAcount.Trim() + " ");
                //}

                //项目阶段  
                if (!string.IsNullOrEmpty(str_jd))
                {
                    str_jd = str_jd.IndexOf(',') > -1 ? str_jd.Remove(str_jd.Length - 1) : "";
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" pro_status LIKE '%" + str_jd + "%' ");
                }
                //参与部门
                if (!string.IsNullOrEmpty(cybm))
                {
                    string[] arr = cybm.Split(',');
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ISTrunEconomy='" + arr[0].Trim() + "'");
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ISHvac='" + arr[1].Trim() + "'");
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" ISArch='" + arr[2].Trim() + "'");
                }
                if (!string.IsNullOrEmpty(asTreeviewStruct) && !asTreeviewStruct.Contains("请选择结构形式"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" replace(replace(replace(pro_StruType,'+',','),'*',','),'^',',')  LIKE '%" + asTreeviewStruct + "%' ");
                }
                if (!string.IsNullOrEmpty(asTreeviewStructType) && !asTreeviewStructType.Contains("请选择建筑分类"))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.Append(" replace(replace(replace(pro_kinds,'+',','),'*',','),'^',',') LIKE '%" + asTreeviewStructType + "%' ");
                }


                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat(" InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    if (sb.ToString() != "")
                    {
                        sb.Append(" " + andor + " ");
                    }
                    else
                    {
                        sb.Append(" and ( ");
                    }
                    sb.AppendFormat("  (InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59)");
                }

                if (sb.ToString() != "")
                {
                    sb.Append(")");
                }

                sb.Append(strWhere);

                parameters.Where = sb.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "selAllot")
            {
                string unit = context.Request.Params["unit"];
                string year = context.Request.Params["year"];
                string year1 = context.Request.Params["year1"];
                string year2 = context.Request.Params["year2"];
                string txt_proName = context.Request.Params["txt_proName"];
                string txt_buildUnit = context.Request.Params["txt_buildUnit"];
                string txt_Aperson = context.Request.Params["txt_Aperson"];
                string txtproAcount = context.Request.Params["txtproAcount"];
                string txtproAcount2 = context.Request.QueryString["txtproAcount2"];
                string txt_signdate = context.Request.QueryString["txt_signdate"];
                string txt_signdate2 = context.Request.QueryString["txt_signdate2"];
                string txt_finishdate = context.Request.QueryString["txt_finishdate"];
                string txt_finishdate2 = context.Request.QueryString["txt_finishdate2"];
                string ddrank = context.Request.QueryString["ddrank"];
                //查询类型 gj代表高级查询，否则是普通查询
                string cxtype = context.Request.QueryString["cxtype"] ?? "";

                StringBuilder sb = new StringBuilder("");
                StringBuilder sb2 = new StringBuilder();
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    unit = unit.Substring(0, unit.Length - 1);
                    sb.AppendFormat(" AND (Unit in ({0})) ", unit.Trim());
                }
                if (cxtype == "gj")
                {
                    //年份               
                    if (!string.IsNullOrEmpty(year) && year != "-1" && (string.IsNullOrEmpty(year2) || year2 == "-1"))
                    {
                        sb.AppendFormat(" AND year(pro_startTime)>={0} ", year.Trim());
                    }
                    else if ((string.IsNullOrEmpty(year) || year == "-1") && !string.IsNullOrEmpty(year2) && year2 != "-1")
                    {
                        sb.AppendFormat(" AND year(pro_startTime)<={0} ", year2.Trim());
                    }
                    else if (!string.IsNullOrEmpty(year) && year != "-1" && !string.IsNullOrEmpty(year2) && year2 != "-1")
                    {
                        sb.AppendFormat(" AND year(pro_startTime) BETWEEN {0} and {1} ", year.Trim(), year2.Trim());
                    }
                }

                if (!string.IsNullOrEmpty(year1) && year1 != "-1")
                {
                    sb2.AppendFormat(" and ActualAllountTime='{0}'  ", year1.Trim());
                }
                //组合查询
                if (!string.IsNullOrEmpty(txt_proName))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_proName.Trim());
                    sb.Append(" AND pro_name LIKE '%" + keyname + "%' ");
                }
                if (!string.IsNullOrEmpty(txt_buildUnit))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_buildUnit.Trim());
                    sb.Append(" AND pro_buildUnit LIKE '%" + keyname + "%'  ");
                }
                if (!string.IsNullOrEmpty(txt_Aperson))
                {
                    string keyname = TG.Common.StringPlus.SqlSplit(txt_Aperson.Trim());
                    sb.Append(" AND ChgJia LIKE '%" + keyname + "%'  ");
                }
                //合同额
                if (!string.IsNullOrEmpty(txtproAcount) && string.IsNullOrEmpty(txtproAcount2))
                {
                    sb.Append(" AND cpr_Acount >= " + txtproAcount.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    sb.Append(" AND cpr_Acount <= " + txtproAcount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
                {
                    sb.Append(" AND (cpr_Acount>=" + txtproAcount.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
                }
                //时间
                if (!string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
                {
                    sb.Append(" AND (pro_startTime BETWEEN '" + txt_signdate.Trim() + "' AND '" + txt_signdate2.Trim() + "') ");
                }
                if (!string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
                {
                    sb.Append(" AND (pro_finishTime BETWEEN '" + txt_finishdate.Trim() + "' AND '" + txt_finishdate2.Trim() + "') ");
                }
                if (!string.IsNullOrEmpty(ddrank) && !ddrank.Contains("请选择"))
                {
                    sb.Append(" AND BuildType=N'" + ddrank.Trim() + "' ");
                }
                if (strWhere == "")
                {
                    strWhere = " ";
                }
                sb.Append(strWhere);

                parameters.Where = sb.ToString() + " |" + sb2.ToString();
                parameters.ProcedureName = "P_cm_ProjectAllotShow_jq";
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "Apply")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间 
                StringBuilder strsql = new StringBuilder("");
                //按名称查询 
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (Unit= '{0}')", unit);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(pro_startTime)={0} ", year);
                }

                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "ApplyEdit")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                string startTime = context.Request.QueryString["startTime"];//录入时间
                string endTime = context.Request.QueryString["endTime"];//录入时间 
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (Unit= '{0}')", unit);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(pro_startTime)={0} ", year);
                }
                //录入时间  

                if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
                }
                else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
                }
                else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
                {
                    strsql.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName2;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "ProjectAnalysis")
            {

                string unit = context.Request.QueryString["unit"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && unit != "-1" && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (pro_DesignUnit= '{0}')", unit);
                }

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName3;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}