﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ShowProHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string str_rel = "";
            string proid = context.Request.Params["pro_id"] ?? "0";
            TG.BLL.cm_Project proE = new TG.BLL.cm_Project();
            TG.Model.cm_Project proE_modol = proE.GetModel(int.Parse(proid));

            string pro_name = proE_modol.pro_name == null ? "" : proE_modol.pro_name.Trim();
            str_rel += pro_name + "|";
            string structType = proE_modol.pro_StruType == null ? "" : proE_modol.pro_StruType.Trim();
            str_rel += proE.CreateULHTML(proE.ResolveProjectStruct(structType)) + "|";
            int rank = proE_modol.pro_level == null ? 0 : int.Parse(proE_modol.pro_level.ToString());
            string pro_rank = "";
            switch (rank)
            {
                case 1: pro_rank = "一级";
                    break;
                case 2: pro_rank = "二级";
                    break;
                default: ;
                    break;
            };
            str_rel += pro_rank + "|";

            int src = proE_modol.Pro_src == null ? 0 : int.Parse(proE_modol.Pro_src.ToString());
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            DataSet ds = dic.GetList("id=" + src);
            string strSrc = ds.Tables[0].Rows[0]["dic_name"].ToString();
            str_rel += strSrc + "|";
            string buildUnit = proE_modol.pro_buildUnit == null ? "" : proE_modol.pro_buildUnit.Trim();
            str_rel += buildUnit + "|";
            string buildAddress = proE_modol.BuildAddress == null ? "" : proE_modol.BuildAddress.Trim();
            str_rel += buildAddress + "|";
            string chgJia = proE_modol.ChgJia == null ? "" : proE_modol.ChgJia.Trim();
            str_rel += chgJia + "|";
            string startdate = proE_modol.pro_startTime.ToString() == null ? "" : proE_modol.pro_startTime.ToString();
            str_rel += startdate.Substring(0, startdate.LastIndexOf(" ")) + "|";
            string reletive = proE_modol.Project_reletive == null ? "" : proE_modol.Project_reletive.Trim();
            str_rel += reletive + "|";
            string type = proE_modol.pro_kinds == null ? "" : proE_modol.pro_kinds.Trim();
            str_rel += type + "|";

            string jd = proE_modol.pro_status == null ? "" : proE_modol.pro_status.Trim();
            str_rel += jd + "|";
            string account = proE_modol.Cpr_Acount == null ? "" : proE_modol.Cpr_Acount.ToString().Trim();
            str_rel += account + "|";
            string unit = proE_modol.Unit == null ? "" : proE_modol.Unit.Trim();
            str_rel += unit + "|";
            string scale = proE_modol.ProjectScale == null ? "" : proE_modol.ProjectScale.ToString();
            str_rel += scale + "|";
            string phone = proE_modol.Phone == null ? "" : proE_modol.Phone.Trim();
            str_rel += phone + "|";
            string finishdate = proE_modol.pro_finishTime == null ? "" : proE_modol.pro_finishTime.ToString().Trim();
            finishdate.Substring(0, finishdate.LastIndexOf(""));
            str_rel += finishdate.Substring(0, finishdate.LastIndexOf(" ")) + "|";
            string remark = proE_modol.pro_Intro == null ? "" : proE_modol.pro_Intro.Trim();
            str_rel += remark + "|";
            str_rel += proE_modol.PMName + "|"; ;
            str_rel += string.IsNullOrEmpty(proE_modol.PMPhone) == true ? "" : proE_modol.PMPhone.Trim() + "|";

            context.Response.Write(str_rel);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
