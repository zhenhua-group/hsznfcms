﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class Show_proInfo : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string cpr_id = context.Request.Params["cpr_id"];
            TG.BLL.cm_Coperation cpr = new TG.BLL.cm_Coperation();
            TG.Model.cm_Coperation modol = cpr.GetModel(int.Parse ( cpr_id ));
            string result = "";
            string str_contractId = modol.cpr_Id.ToString ();
            result += str_contractId+"|";
            string str_cprNo = modol.cpr_No;
            result += str_cprNo+"|";
            string str_Name=modol .cpr_Name ;
            result += str_Name+"|";
            string str_Acount = modol.cpr_Acount.ToString ();
            result += str_Acount + "|";
            string str_signDate = modol.cpr_SignDate.ToString();
            result += str_signDate + "|";
            string str_DontDate = modol.cpr_DoneDate.ToString();
            result += str_DontDate + "|";
            string str_chgPeople = modol.ChgPeople;
            result += str_chgPeople + "|";
            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
