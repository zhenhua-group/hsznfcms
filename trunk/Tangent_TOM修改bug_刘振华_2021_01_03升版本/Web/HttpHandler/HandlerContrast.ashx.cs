﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Text;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// HandlerContrast 的摘要说明
    /// </summary>
    public class HandlerContrast : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            TG.BLL.CopContrast copcon = new BLL.CopContrast();
            TG.BLL.ChargeAndCoper chargebll = new BLL.ChargeAndCoper();
            TG.BLL.StandBookBp standbll = new BLL.StandBookBp();
            TG.BLL.cpr_AcountAndIndustry acountbll = new BLL.cpr_AcountAndIndustry();
            context.Response.ContentType = "text/plain";
            string action = context.Request.QueryString["action"];
            string previewPower = context.Request.QueryString["previewPower"];
            string userUnitNum = context.Request.QueryString["userUnitNum"];
            if (action == "1")
            {
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                string month = context.Request["month"];

                string beginTime = month + "-01";
                string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.CopContrast> listSC = copcon.GetList("unitallot", unitId, year, beginTime, endTime);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "2")
            {
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                string month = context.Request["month"];
                string beginTime = month + "-01";
                string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.CopContrast> listSC = copcon.GetList("unitcharge", unitId, year, beginTime, endTime);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "3")
            {
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                string month = context.Request["month"];
                string beginTime = month + "-01";
                string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.CopContrast> listSC = copcon.GetList("unitcomm", unitId, year, beginTime, endTime);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "4")
            {
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                string month = context.Request["month"];
                string endmonth = context.Request["endmonth"];
                string begintime = month + "-01";
                string endtime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(int.Parse(year), int.Parse(month));
                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.ChargeAndCoper> listSC = chargebll.GetList(unitId, year, begintime, endtime, endmonth);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "5")
            {
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                string cprName = context.Request["cprname"];
                string cprInfoName = "";
                if (cprName.Trim() != "")
                {
                    cprInfoName = string.Format("and a.cpr_name like '%{0}%'", cprName);
                }


                //

                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.StandBookEntity> listSC = standbll.GetList("2", unitId, year, cprInfoName);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "6")
            {
                string cprName = context.Request.QueryString["cprName"];
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.Outsummary> listSC = new List<TG.Model.Outsummary>();
                if (cprName.Trim() != "")
                {
                    listSC = standbll.GetListSummary(unitId, year, cprName);
                }
                else
                {
                    listSC = standbll.GetListSummary(unitId, year);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "7")
            {
                string year = context.Request.QueryString["year"];
                string month = context.Request["month"];
                string beginmonth = year + "-" + month + "-01";
                string endmonth = year + "-" + month + "-" + TG.Common.TimeParser.GetMonthLastDate(int.Parse(year), int.Parse(month));
                List<TG.Model.Collection> listSC = standbll.GetListCollection(beginmonth, endmonth);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "8")
            {
                string year = context.Request.QueryString["year"];
                string month = context.Request["month"];
                string beginmonth = year + "-" + month + "-01";
                string endmonth = year + "-" + month + "-" + TG.Common.TimeParser.GetMonthLastDate(int.Parse(year), int.Parse(month));
                List<TG.Model.Collection> listSC = standbll.GetListCollection(beginmonth, endmonth);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "9")
            {
                string cprName = context.Request.QueryString["cprName"];
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.ChargeDate> listSC = new List<TG.Model.ChargeDate>();
                if (cprName.Trim() != "")
                {
                    listSC = standbll.GetListChargeDate(unitId, year, cprName);
                }
                else
                {
                    listSC = standbll.GetListChargeDate(unitId, year);
                }
                //List<TG.Model.ChargeDate> listSC = standbll.GetListChargeDate(unitId, year);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }
            else if (action == "10")
            {
                string year = context.Request.QueryString["year"];
                string unitId = context.Request["unitid"];
                string month = context.Request["month"];

                string beginTime = month + "-01";
                string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
                if (unitId == "-1")
                {
                    if (previewPower == "0" || previewPower == "2")
                    {
                        //
                        unitId = userUnitNum;
                    }
                }
                List<TG.Model.cpr_AcountAndIndustry> listSC = acountbll.GetList(unitId, year, beginTime, endTime);
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = js.Serialize(listSC);
                context.Response.Write(json);
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}