﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Text;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProjectAnalysis : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string flag = context.Request.Params["flag"] ?? "";
            if (flag == "getjd")
            {
                string proid = context.Request.Params["proid"] ?? "0";
                //Purpose对象
                TG.BLL.tg_purpose bll_pur = new TG.BLL.tg_purpose();
                //图纸信息
                TG.BLL.tg_package bll_page = new TG.BLL.tg_package();
                //子项目信息
                TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                string strSql = string.Format(" SELECT DISTINCT purposeID AS PurPoseID FROM tg_subproject sb INNER JOIN tg_endSubItem ed ON sb.pro_ID=ed.subproID WHERE pro_parentID={0} ", proid);
                //设计阶段
                //声明一个空数据表
                DataTable dt_jd = new DataTable("ds");
                DataRow dr = null;
                dt_jd.Columns.Add("PurName");
                dt_jd.Columns.Add("StartTime");
                dt_jd.Columns.Add("ReadySTime");
                dt_jd.Columns.Add("EndTime");
                dt_jd.Columns.Add("ReadyETime");
                dt_jd.Columns.Add("Url");
                //查询那设计阶段
                DataSet ds_pur = bll_db.GetList(strSql);

                if (ds_pur.Tables.Count > 0)
                {
                    if (ds_pur.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds_pur.Tables[0].Rows.Count; i++)
                        {
                            string purid = ds_pur.Tables[0].Rows[i][0].ToString();
                            string purname = bll_pur.GetList(" purpose_ID=" + purid + "").Tables[0].Rows[0]["purpose_Name"].ToString().Trim();

                            if (purname != "往来公函" && purname != "工程表单" && purname != "变更" && purname != "会议纪要")
                            {
                                string starttime = "";
                                string endtime = "";
                                string r_starttime = "";
                                string r_endtime = "";
                                //查询图纸
                                strSql = " SELECT max(ver_date) AS MaxDate ,min(ver_date) AS MinDate FROM tg_package WHERE subpro_ID IN ( SELECT pro_ID FROM tg_subproject WHERE pro_parentID=" + proid + ") AND purpose_ID=" + purid;
                                //得到阶段的开始时间和结束时间
                                DataSet ds_time = bll_db.GetList(strSql);
                                if (ds_time.Tables.Count > 0)
                                {
                                    if (ds_time.Tables[0].Rows.Count > 0)
                                    {
                                        starttime = ds_time.Tables[0].Rows[0][1].ToString();
                                        endtime = ds_time.Tables[0].Rows[0][0].ToString();
                                    }
                                }
                                //计划开始时间
                                strSql = " SELECT StartDate, EndDate FROM cm_ProjectPlanSubItem Where  ProjectSysNo = " + proid + " AND DesignLevel like '%" + purname + "%'";
                                DataSet ds_rtime = bll_db.GetList(strSql);
                                if (ds_rtime.Tables.Count > 0)
                                {
                                    if (ds_rtime.Tables[0].Rows.Count > 0)
                                    {
                                        r_starttime = ds_rtime.Tables[0].Rows[0][1].ToString();
                                        r_endtime = ds_rtime.Tables[0].Rows[0][0].ToString();
                                    }
                                }
                                //创建表格
                                dr = dt_jd.NewRow();
                                dr["PurName"] = purname;
                                dr["StartTime"] = r_starttime;
                                dr["ReadySTime"] = starttime;
                                dr["EndTime"] = r_endtime;
                                dr["ReadyETime"] = endtime;
                                dr["Url"] = purid;
                                //添加新行
                                dt_jd.Rows.Add(dr);
                            }
                        }
                    }
                }
                //转化为json格式
                if (dt_jd.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt_jd));
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            else if (flag == "getspe")
            {
                string proid = context.Request.Params["proid"] ?? "0";
                string purid = context.Request.Params["purid"] ?? "";
                //专业对象
                TG.BLL.tg_speciality bll_spe = new TG.BLL.tg_speciality();
                //图纸信息
                TG.BLL.tg_package bll_pack = new TG.BLL.tg_package();
                //查询包含专业
                TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                //查询专业
                string strSql = " SELECT DISTINCT class_ID FROM tg_package WHERE subpro_ID IN (SELECT pro_ID FROM tg_subproject WHERE pro_parentID=" + proid + ") AND purpose_ID=" + purid;
                //创建表格
                DataTable dt_spe = new DataTable("ds");
                DataRow spe_row = null;
                dt_spe.Columns.Add("speName");
                dt_spe.Columns.Add("speStart");
                dt_spe.Columns.Add("speEnd");
                dt_spe.Columns.Add("Url");
                //查询所有专业
                DataSet ds_spe = bll_db.GetList(strSql);

                if (ds_spe.Tables.Count > 0)
                {
                    int icount = ds_spe.Tables[0].Rows.Count;
                    for (int i = 0; i < icount; i++)
                    {
                        string speid = ds_spe.Tables[0].Rows[i][0].ToString();
                        string spename = bll_spe.GetModel(int.Parse(speid)).spe_Name;
                        string start = "";
                        string end = "";
                        //查询图纸
                        strSql = " SELECT max(ver_date) AS EndTime ,min(ver_date) AS StartTime FROM tg_package WHERE subpro_ID IN (SELECT pro_ID FROM tg_subproject WHERE pro_parentID=" + proid + ") AND purpose_ID=" + purid + " AND class_ID=" + speid;
                        //得到开始时间和结束时间
                        DataSet ds_time = bll_db.GetList(strSql);

                        if (ds_time.Tables.Count > 0)
                        {
                            start = ds_time.Tables[0].Rows[0]["StartTime"].ToString();
                            end = ds_time.Tables[0].Rows[0]["EndTime"].ToString();
                        }

                        spe_row = dt_spe.NewRow();
                        spe_row["speName"] = spename;
                        spe_row["speStart"] = start;
                        spe_row["speEnd"] = end;
                        spe_row["Url"] = speid;
                        //添加新行
                        dt_spe.Rows.Add(spe_row);
                    }
                }
                //输出表格
                if (dt_spe.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt_spe));
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            else if (flag == "getuser")
            {
                string proid = context.Request.Params["proid"] ?? "";
                string purid = context.Request.Params["purid"] ?? "";
                string classid = context.Request.Params["classid"] ?? "";
                //人员信息
                TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                //图纸信息
                TG.BLL.tg_package bll_pack = new TG.BLL.tg_package();
                //查询包含专业
                TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                //查询专业
                string strSql = " SELECT DISTINCT lastModifyUser_ID FROM tg_package WHERE subpro_ID IN (SELECT pro_ID FROM tg_subproject WHERE pro_parentID=" + proid + ") AND purpose_ID=" + purid + " AND class_ID=" + classid;
                //创建表格
                DataTable dt_user = new DataTable("ds");
                DataRow user_row = null;
                dt_user.Columns.Add("userName");
                dt_user.Columns.Add("userStart");
                dt_user.Columns.Add("userEnd");

                DataSet ds_user = bll_db.GetList(strSql);
                if (ds_user.Tables.Count > 0)
                {
                    int icount = ds_user.Tables[0].Rows.Count;
                    for (int i = 0; i < icount; i++)
                    {
                        string memid = ds_user.Tables[0].Rows[i][0].ToString();
                        string memname = bll_mem.GetModel(int.Parse(memid)).mem_Name;
                        string start = "";
                        string end = "";
                        strSql = " SELECT max(ver_date) AS EndTime ,min(ver_date) AS StartTime FROM tg_package WHERE subpro_ID IN (SELECT pro_ID FROM tg_subproject WHERE pro_parentID=" + proid + ") AND purpose_ID=" + purid + " AND class_ID=" + classid + " AND lastModifyUser_ID=" + memid;

                        DataSet ds_time = bll_db.GetList(strSql);
                        if (ds_time.Tables.Count > 0)
                        {
                            start = ds_time.Tables[0].Rows[0][1].ToString();
                            end = ds_time.Tables[0].Rows[0][0].ToString();
                        }

                        user_row = dt_user.NewRow();
                        user_row["userName"] = memname;
                        user_row["userStart"] = start;
                        user_row["userEnd"] = end;

                        //添加新行
                        dt_user.Rows.Add(user_row);
                    }
                }
                //输出数据
                if (dt_user.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt_user));
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            else if (flag == "getall")
            {
                TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
                string result = "";
                //项目树
                string strSql = "SELECT COUNT(*) as CountProj FROM tg_project";
                DataTable dt = bll_db.GetList(strSql).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result += dt.Rows[0][0].ToString() + "$";
                }
                //统计图指数
                string strSql2 = "SELECT COUNT(*) AS PageCount FROM tg_package WHERE (filename LIKE '%.dwg%')";
                DataTable dt3 = bll_db.GetList(strSql2).Tables[0];
                if (dt3.Rows.Count > 0)
                {
                    result += dt3.Rows[0][0].ToString() + "$";
                }
                //统计部门
                string strSql4 = "SELECT COUNT(*) AS PartCount FROM tg_unit";
                DataTable dt4 = bll_db.GetList(strSql4).Tables[0];
                if (dt4.Rows.Count > 0)
                {
                    result += dt4.Rows[0][0].ToString() + "$";
                }
                //统计多少人
                string strSql5 = "SELECT Count(*) FROM tg_member WHERE (mem_Principalship_ID <> '1')";
                DataTable dt5 = bll_db.GetList(strSql5).Tables[0];
                if (dt5.Rows.Count > 0)
                {
                    result += dt5.Rows[0][0].ToString() + "$";
                }

                context.Response.Write(result);
            }
            
        }
        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName);
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(dt.Columns[j].ColumnName);
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(dt.Rows[i][j].ToString());
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            if (dt.Rows.Count > 0)
            {
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
            }

            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
