﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace TG.Web.HttpHandler.ProjectIma
{
    /// <summary>
    /// ProjectPlotInfo 的摘要说明
    /// </summary>
    public class ProjectPlotInfo : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }

        public TG.Model.ProjectPlotInfoEntity ProjectPlotInfoEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.ProjectPlotInfoEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.ProjectPlotInfoEntity>(data);
                }
                return obj;
            }
        }
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        public void ProcessRequest(HttpContext context)
        {
            if (Request.HttpMethod == "POST")
            {

                switch (Action)
                {
                    case 0:
                        AddProjectPlotInfo();
                        break;
                    case 1:
                        string proid = context.Request.Params["proID"] ?? "0";
                        GetPlotSubInfo(proid);
                        break;
                }
                Response.End();
            }
        }

        /// <summary>
        /// 新增出图设计信息
        /// </summary>    
        private void AddProjectPlotInfo()
        {
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            int count = bll.InsertProjectPlotInfo(ProjectPlotInfoEntity);

            if (count > 0)
            {
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }

        /// <summary>
        /// 得到子项出图卡信息
        /// </summary>
        /// <param name="proID"></param>
        private void GetPlotSubInfo(string proID)
        {
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            DataTable dt = bll.GetSubPlotInfo(int.Parse(proID)).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                Response.Write(TableToJson(dt));
            }
        }

        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


    }
}