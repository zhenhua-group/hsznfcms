﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.SessionState;
using TG.Model;

namespace TG.Web.HttpHandler.ProjectIma
{
    /// <summary>
    /// ProImaAuditHandler 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProImaAuditHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }

        #region QueryString

        public TG.Model.cm_ProImaAudit ProImaAuditEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProImaAudit obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProImaAudit>(data);
                }
                int i = obj.InUser;
                string str = obj.InDate.ToString();
                return obj;
            }
        }

        /// <summary>
        /// 审核记录SysNo
        /// </summary>
        public int CoperationAuditSysNo
        {
            get
            {
                int coperationAuditSysNo = 0;
                int.TryParse(Request["CoperationAuditSysNo"], out coperationAuditSysNo);
                return coperationAuditSysNo;
            }
        }

        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }
        //查询or更新
        public int Flag
        {
            get
            {
                int flag = 0;
                int.TryParse(Request["flag"], out flag);
                return flag;
            }
        }
        #endregion

        private TG.BLL.cm_ProImaAudit proImaBLL = new TG.BLL.cm_ProImaAudit();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                    case 0:
                        InsertProImaAuditRecord();
                        break;
                    case 1:
                    case 2:
                        UpdateCoperatioAuditRecord();
                        break;
                    case 3:
                        DoAuditAgain();
                        break;
                }
                Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 新规审核信息
        /// </summary>
        private void InsertProImaAuditRecord()
        {
            if (Flag == 0)
            {
                //返回下阶段用户列表
                GetNextProcessRoleUser();
            }
            else
            {
                TG.BLL.cm_ProImaAudit proImaAuditBLL = new TG.BLL.cm_ProImaAudit();
                int count = 0;

                //查询合同信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(ProImaAuditEntity.ProjectSysNo);

                TG.Model.cm_ProImaAudit model = new cm_ProImaAudit();
                model.ProjectSysNo = ProImaAuditEntity.ProjectSysNo;
                model.InUser = ProImaAuditEntity.InUser;
                model.InDate = DateTime.Now;
                model.Status = "A";
                int sysNo = proImaAuditBLL.Add(model);

                if (sysNo > 0)
                {
                    //获取审批实体
                    TG.Model.cm_CoperationAudit tempEntity = new TG.BLL.cm_CoperationAudit().GetModel(sysNo);
                    //实例消息实体
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = tempEntity.SysNo.ToString(),
                        ReferenceSysNo = string.Format("projectImaAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 9,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "工程设计出图卡审核"),
                        QueryCondition = project.pro_name,
                        Status = "A",
                        IsDone = "A",
                        ToRole = "0"
                    };
                    //声明
                    string sysMsgString = "";
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        Response.Write(sysMsgString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
              
            }
        }

        /// <summary>
        /// 修改审核记录
        /// </summary>
        private void UpdateCoperatioAuditRecord()
        {
            if (Flag == 0)
            {
                GetNextProcessRoleUser();
            }
            else
            {
                TG.Model.cm_ProImaAudit oldEntity = proImaBLL.GetModel(ProImaAuditEntity.SysNo);

                TG.Model.cm_ProImaAudit EntityParameter = new TG.Model.cm_ProImaAudit
                {
                    SysNo = oldEntity.SysNo,
                    InUser = oldEntity.InUser,
                    ProjectSysNo = oldEntity.ProjectSysNo,
                    InDate = oldEntity.InDate,
                    OneSuggestion = oldEntity.OneSuggestion,
                    TwoSuggestion = oldEntity.TwoSuggestion
                };

                switch (ProImaAuditEntity.Status)
                {
                    case "A":
                        EntityParameter.OneSuggestion = ProImaAuditEntity.OneSuggestion;
                        EntityParameter.Status = Action == 1 ? "B" : "C";
                        break;
                    case "B":
                        EntityParameter.TwoSuggestion = ProImaAuditEntity.TwoSuggestion;
                        EntityParameter.Status = Action == 1 ? "D" : "E";
                        break;

                }

                EntityParameter.AuditUser = string.IsNullOrEmpty(oldEntity.AuditUser) == true ? ProImaAuditEntity.AuditUser : oldEntity.AuditUser + "," + ProImaAuditEntity.AuditUser;
                EntityParameter.AuditDate = string.IsNullOrEmpty(oldEntity.AuditDate) == true ? DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") : oldEntity.AuditDate + "," + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

                int count = proImaBLL.Update(EntityParameter);

                //查询合同信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(oldEntity.ProjectSysNo);
                TG.Model.cm_ProImaAudit tempEntity = new TG.BLL.cm_ProImaAudit().GetModel(oldEntity.SysNo);

                if (tempEntity.Status == "D")
                {

                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = EntityParameter.SysNo.ToString(),
                        ReferenceSysNo = string.Format("projectImaAuditSysNo={0}&MessageStatus={1}", EntityParameter.SysNo.ToString(), tempEntity.Status),
                        FromUser = oldEntity.InUser,
                        InUser = UserSysNo,
                        MsgType = 9,
                        ToRole = "0",
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "工程设计出图卡审核完毕"),
                        QueryCondition = project.pro_name,
                        IsDone = "B",
                        Status = "A"
                    };
                    int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    if (resultcount > 0)
                    {
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
                    }

                }
                else
                {
                    TG.BLL.cm_SysMsg sysMsgBLL = new TG.BLL.cm_SysMsg();
                    //是审核通过的场合
                    if (Action == 1)
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            //ReferenceSysNo = oldEntity.SysNo.ToString(),
                            ReferenceSysNo = string.Format("projectImaAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                            FromUser = UserSysNo,
                            InUser = UserSysNo,
                            MsgType = 9,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "工程设计出图卡审核通过"),
                            QueryCondition = project.pro_name,
                            IsDone = "B",
                            Status = "A"
                        };

                        string roleName = string.Empty;
                        string roleSysNo = string.Empty;
                        //发送消息给相关人员
                        switch (tempEntity.Status)
                        {
                            case "B":
                                roleSysNo = GetStatusProcess(2);
                                sysMessageViewEntity.ToRole = roleSysNo;
                                break;

                        }
                        if (count > 0)
                        {
                            string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                            if (!string.IsNullOrEmpty(sysMsgString))
                            {
                                Response.Write(sysMsgString);
                            }
                            else
                            {
                                Response.Write("0");
                            }
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            //ReferenceSysNo = oldEntity.SysNo.ToString(),
                            ReferenceSysNo = string.Format("projectImaAuditSysNo={0}&MessageStatus={1}", oldEntity.SysNo.ToString(), tempEntity.Status),
                            InUser = UserSysNo,
                            MsgType = 9,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "工程设计出图卡审核不通过"),
                            QueryCondition = project.pro_name,
                            ToRole = "0",
                            FromUser = oldEntity.InUser,
                            IsDone = "B",
                            Status = "A"
                        };
                        int resultCount = sysMsgBLL.InsertSysMessage(sysMessageViewEntity);
                        if (resultCount > 0)
                        {
                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 重新审核修改消息状态
        /// </summary>
        private void DoAuditAgain()
        {
            //修改消息状态为已读
            new TG.BLL.cm_SysMsg().UpDateSysMsgStatusByCoperationSysNo(CoperationAuditSysNo);
        }

        private string GetStatusProcess(int postion)
        {
            string sql = "select RoleSysNo from cm_ProImageAuditConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            return resultObj == null ? "0" : resultObj.ToString();
        }

        //返回下一阶段用户列表
        private void GetNextProcessRoleUser()
        {
            //发起流程
            if (Action == 0)
            {
                //得到审批用户的实体
                string roleSysNoString = GetStatusProcess(1);
                string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                if (!string.IsNullOrEmpty(roleUserString))
                {
                    Response.Write(roleUserString);
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == 1)//审批流程
            {
                string roleSysNoString = "";
                switch (ProImaAuditEntity.Status)
                {
                    case "A":
                        roleSysNoString = GetStatusProcess(2);
                        break;
                }
                //获取用户列表
                if (roleSysNoString == "1")
                {
                    Response.Write("1");
                }
                else
                {
                    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        Response.Write(roleUserString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
        }
    }
}