﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace TG.Web.HttpHandler.ProjectIma
{
    /// <summary>
    /// ProjectFileInfo 的摘要说明
    /// </summary>
    public class ProjectFileInfo : HandlerCommon, IHttpHandler, IRequiresSessionState
    {

        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }

        public TG.Model.ProjectFileInfoEntity ProjectPlotInfoEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.ProjectFileInfoEntity obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.ProjectFileInfoEntity>(data);
                }
                return obj;
            }
        }
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        public void ProcessRequest(HttpContext context)
        {
            if (Request.HttpMethod == "POST")
            {

                switch (Action)
                {
                    case 0:
                        AddProjectFileInfo();
                        break;

                }
                Response.End();
            }
        }

        /// <summary>
        /// 新增出图设计信息
        /// </summary>    
        private void AddProjectFileInfo()
        {
            TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
            int count = bll.InsertProjectFileInfo(ProjectPlotInfoEntity);

            if (count > 0)
            {
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}