﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Data.SqlClient;
using TG.Model;
using System.Collections;

namespace TG.Web.HttpHandler.ProjectIma
{
    /// <summary>
    /// ProjectPlotAuditInfo 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProjectPlotAuditInfo : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        public HttpRequest Request { get { return HttpContext.Current.Request; } }
        public HttpResponse Response { get { return HttpContext.Current.Response; } }
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }

        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }
        //查询or更新
        public int Flag
        {
            get
            {
                int flag = 0;
                int.TryParse(Request["flag"], out flag);
                return flag;
            }
        }

        public TG.Model.cm_ProjectPlotInfoAudit ProjectPlotInfoAuditEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectPlotInfoAudit obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectPlotInfoAudit>(data);
                }
                return obj;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                    case 0:
                        InsertProjectPlotAuditRecord();
                        break;
                    case 1:
                        string fileNum = context.Request.Params["FileNum"] ?? "";
                        UpdateProjectPlotAuditRecord(fileNum);
                        break;
                    case 2:
                        string fileNum2 = context.Request.Params["FileNum"] ?? "";
                        DisAgreeProjectPlotAuditRecord(fileNum2);
                        break;
                }
            }
        }
        /// <summary>
        /// 发起一条新的审批信息
        /// </summary>
        private void InsertProjectPlotAuditRecord()
        {
            //审核实体
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            int proID = ProjectPlotInfoAuditEntity.ProjectSysNo;
            TG.Model.cm_ProjectPlotInfo model = bll.GetPlotInfo(proID);
            TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(proID);
            string unitName = pro == null ? "" : pro.Unit;
            string plotType = "";
            if (model != null)
            {
                plotType = model.PlotType;
            }

            if (Flag == 0)
            {
                //返回下阶段用户列表
                GetNextProcessRoleUser(plotType, unitName);
            }
            else
            {
                //保存审批阶段轨迹
                int sysNo = bll.InsertProjectPlotAuditRecord(ProjectPlotInfoAuditEntity);
                //给对应角色发送审批消息
                if (sysNo > 0)
                {
                    //实例消息实体
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("ProjectPlotAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 21,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计出图卡审核信息"),
                        QueryCondition = pro.pro_name,
                        Status = "A",
                        IsDone = "A",
                        ToRole = "0"
                    };
                    //声明
                    string sysMsgString = "";
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        Response.Write(sysMsgString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }

            }
        }

        /// <summary>
        /// 更新审核信息
        /// </summary>
        private void UpdateProjectPlotAuditRecord(string fileNum)
        {
            //审核实体
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            int proID = ProjectPlotInfoAuditEntity.ProjectSysNo;
            TG.Model.cm_ProjectPlotInfo model = bll.GetPlotInfo(proID);
            TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(proID);
            string unitName = pro == null ? "" : pro.Unit;
            string plotType = "";
            if (model != null)
            {
                plotType = model.PlotType;
            }
            if (Flag == 0)
            {
                GetNextProcessRoleUser(plotType, unitName);
            }
            else
            {
                //状态
                string status = ProjectPlotInfoAuditEntity.Status;
                int asciiCode = (int)Convert.ToChar(status);
                if (asciiCode % 2 != 0)
                {
                    asciiCode++;
                }
                else
                {
                    asciiCode = asciiCode + 2;
                }
                TG.Model.cm_ProjectPlotInfoAudit projectPlotInfoAuditTemp = bll.GetProjectPlotInfoAuditEntity(new cm_ProjectPlotAuditQueryEntity { ProjectPlotAuditSysNo = ProjectPlotInfoAuditEntity.SysNo });

                projectPlotInfoAuditTemp.Status = ((char)asciiCode).ToString();
                projectPlotInfoAuditTemp.Suggestion = projectPlotInfoAuditTemp.Suggestion + ProjectPlotInfoAuditEntity.Suggestion;
                projectPlotInfoAuditTemp.AuditUser = projectPlotInfoAuditTemp.AuditUser + UserSysNo;
                projectPlotInfoAuditTemp.AuditDate = projectPlotInfoAuditTemp.AuditDate + DateTime.Now;

                //最后一步更新 档案号
                if (((plotType.Trim().Equals("特殊输出") || plotType.Trim().Equals("结构延迟校审")) && ProjectPlotInfoAuditEntity.Status == "N") || ((plotType.Trim().Equals("正常输出") || plotType.Trim().Equals("补充图") || plotType.Trim().Equals("变更图")) && ProjectPlotInfoAuditEntity.Status == "D"))
                {
                    bll.UpdateFileNum(proID, fileNum);
                }

                int count = bll.UpdateProjectPlanAudit(projectPlotInfoAuditTemp);

                if (count > 0)
                {
                    if (((plotType.Trim().Equals("特殊输出") || plotType.Trim().Equals("结构延迟校审")) && projectPlotInfoAuditTemp.Status == "P") || ((plotType.Trim().Equals("正常输出") || plotType.Trim().Equals("补充图") || plotType.Trim().Equals("变更图")) && projectPlotInfoAuditTemp.Status == "F"))
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ProjectPlotAuditSysNo={0}&MessageStatus={1}", ProjectPlotInfoAuditEntity.SysNo.ToString(), projectPlotInfoAuditTemp.Status),
                            FromUser = projectPlotInfoAuditTemp.InUser,
                            InUser = UserSysNo,
                            MsgType = 21,
                            ToRole = "0",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计出图卡审核全部通过审核"),
                            QueryCondition = pro.pro_name,
                            IsDone = "B",
                            Status = "A"
                        };
                        int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        if (resultcount > 0)
                        {
                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ProjectPlotAuditSysNo={0}&MessageStatus={1}", ProjectPlotInfoAuditEntity.SysNo.ToString(), projectPlotInfoAuditTemp.Status),
                            FromUser = UserSysNo,
                            InUser = UserSysNo,
                            MsgType = 21,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计出图卡审核信息"),
                            QueryCondition = pro.pro_name,
                            IsDone = "A",
                            Status = "A",
                            ToRole = "0"
                        };
                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            Response.Write(sysMsgString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }

                }

            }
        }

        /// <summary>
        /// 不同意
        /// </summary>
        private void DisAgreeProjectPlotAuditRecord(string fileNum)
        {
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            int proID = ProjectPlotInfoAuditEntity.ProjectSysNo;
            TG.Model.cm_ProjectPlotInfo model = bll.GetPlotInfo(proID);
            TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(proID);
            string plotType = "";
            if (model != null)
            {
                plotType = model.PlotType;
            }
            //状态
            string status = ProjectPlotInfoAuditEntity.Status;
            int asciiCode = (int)Convert.ToChar(status);
            if (asciiCode % 2 != 0)
            {
                asciiCode = asciiCode + 2;
            }
            else
            {
                asciiCode = asciiCode + 3;
            }
            TG.Model.cm_ProjectPlotInfoAudit projectPlotInfoAuditTemp = bll.GetProjectPlotInfoAuditEntity(new cm_ProjectPlotAuditQueryEntity { ProjectPlotAuditSysNo = ProjectPlotInfoAuditEntity.SysNo });

            projectPlotInfoAuditTemp.Status = ((char)asciiCode).ToString();
            projectPlotInfoAuditTemp.Suggestion = projectPlotInfoAuditTemp.Suggestion + ProjectPlotInfoAuditEntity.Suggestion;
            projectPlotInfoAuditTemp.AuditUser = projectPlotInfoAuditTemp.AuditUser + UserSysNo;
            projectPlotInfoAuditTemp.AuditDate = projectPlotInfoAuditTemp.AuditDate + DateTime.Now;

            //最后一步更新 档案号
            if ((plotType.Trim().Equals("特殊输出") || plotType.Trim().Equals("结构延迟校审")) && status == "N")
            {
                bll.UpdateFileNum(proID, fileNum);
            }

            int count = bll.UpdateProjectPlanAudit(projectPlotInfoAuditTemp);

            if (count > 0)
            {
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("ProjectPlotAuditSysNo={0}&MessageStatus={1}", ProjectPlotInfoAuditEntity.SysNo.ToString(), projectPlotInfoAuditTemp.Status),
                    FromUser = projectPlotInfoAuditTemp.InUser,
                    InUser = UserSysNo,
                    MsgType = 21,
                    ToRole = "0",
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计出图卡审核未通过审核"),
                    QueryCondition = pro.pro_name,
                    IsDone = "B",
                    Status = "A"
                };
                int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                if (resultcount > 0)
                {
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        //返回下一阶段用户列表
        private void GetNextProcessRoleUser(string plotType, string unitName)
        {
            //发起流程
            if (Action == 0)
            {
                string roleUserString = "";
                if (plotType.Trim().Equals("特殊输出") || plotType.Trim().Equals("结构延迟校审"))
                {
                    //取得所长
                    roleUserString = GetProjectPrincipalship(unitName);
                }
                else
                {
                    //取得生产经营部
                    string roleSysNoString = GetStatusProcess(6);
                    roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                }

                if (!string.IsNullOrEmpty(roleUserString))
                {
                    Response.Write(roleUserString);
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == 1)//审批流程
            {
                string roleSysNoString = "";
                if (plotType.Trim().Equals("特殊输出") || plotType.Trim().Equals("结构延迟校审"))
                {
                    switch (ProjectPlotInfoAuditEntity.Status)
                    {
                        case "A":
                            roleSysNoString = GetStatusProcess(2);
                            break;
                        case "B":
                            roleSysNoString = GetStatusProcess(3);
                            break;
                        case "D":
                            roleSysNoString = GetStatusProcess(4);
                            break;
                        case "F":
                            roleSysNoString = GetStatusProcess(5);
                            break;
                        case "H":
                            roleSysNoString = GetStatusProcess(6);
                            break;
                        case "J":
                            roleSysNoString = GetStatusProcess(7);
                            break;
                        case "L":
                            roleSysNoString = GetStatusProcess(8);
                            break;

                    }
                }
                else
                {
                    switch (ProjectPlotInfoAuditEntity.Status)
                    {
                        case "A":
                            roleSysNoString = GetStatusProcess(7);
                            break;
                        case "B":
                            roleSysNoString = GetStatusProcess(8);
                            break;
                    }
                }

                //获取用户列表
                if (roleSysNoString == "1")
                {
                    Response.Write("1");
                }
                else
                {
                    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        Response.Write(roleUserString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
        }

        //获取审批阶段对应角色ID
        private string GetStatusProcess(int postion)
        {
            string sql = " Select RoleSysNo from cm_ProjectPlotInfoConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return resultObj == null ? "0" : resultObj.ToString();
        }

        /// <summary>
        /// 获取所长
        /// </summary>
        /// <returns></returns>
        public string GetProjectPrincipalship(string unitName)
        {
            ArrayList arrayList = new ArrayList();

            //经济所长
            string sqlnts = @"select isnull(a.mem_ID,0) as UserSysNo,mem_Name,b.unit_Name  from tg_member  a 
                                                        left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID
                                                        left join   tg_principalship c on 
                                                        a.mem_Principalship_ID=c.pri_ID
                                                        where  b.unit_Name like'%" + unitName + "%' and   C.pri_Name LIKE '%所长%' ";

            SqlDataReader readernts = TG.DBUtility.DbHelperSQL.ExecuteReader(sqlnts);

            while (readernts.Read())
            {
                arrayList.Add(new { UserSysNo = readernts["UserSysNo"], UserName = readernts["mem_Name"], RoleName = "所长" });
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { RoleName = "所长", UserList = arrayList });

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}