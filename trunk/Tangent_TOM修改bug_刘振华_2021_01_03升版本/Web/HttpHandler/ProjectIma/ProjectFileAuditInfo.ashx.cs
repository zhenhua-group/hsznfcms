﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.SessionState;
using Newtonsoft.Json;
using TG.Model;

namespace TG.Web.HttpHandler.ProjectIma
{
    /// <summary>
    /// ProjectPlotAuditInfo 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProjectFileAuditInfo : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        public HttpRequest Request { get { return HttpContext.Current.Request; } }
        public HttpResponse Response { get { return HttpContext.Current.Response; } }
        public HttpSessionState Session { get { return HttpContext.Current.Session; } }


        /// <summary>
        /// 操作类型。0为新规。1为审核通过，2为审核不通过,3为点击重新审核按钮修改消息状态
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["Action"], out action);
                return action;
            }
        }
        //查询or更新
        public int Flag
        {
            get
            {
                int flag = 0;
                int.TryParse(Request["flag"], out flag);
                return flag;
            }
        }

        public TG.Model.cm_ProjectFileInfoAudit ProjectFileInfoAuditEntity
        {
            get
            {
                string data = Request["data"];
                TG.Model.cm_ProjectFileInfoAudit obj = null;
                if (!string.IsNullOrEmpty(data))
                {
                    obj = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ProjectFileInfoAudit>(data);
                }
                return obj;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                    case 0:
                        InsertProjectFileAuditRecord();
                        break;
                    case 1:
                        UpdateProjectFileAuditRecord();
                        break;
                    case 2:
                        DisAgreeProjectFileAuditRecord();
                        break;
                }
            }
        }

        /// <summary>
        /// 发起一条新的审批信息
        /// </summary>
        private void InsertProjectFileAuditRecord()
        {

            if (Flag == 0)
            {
                //返回下阶段用户列表
                GetNextProcessRoleUser();
            }
            else
            {
                //审核实体
                TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
                int proID = ProjectFileInfoAuditEntity.ProjectSysNo;
                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(proID);

                //保存审批阶段轨迹
                int sysNo = bll.InsertProjectFileAuditRecord(ProjectFileInfoAuditEntity);
                //给对应角色发送审批消息
                if (sysNo > 0)
                {
                    //实例消息实体
                    SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                    {
                        ReferenceSysNo = string.Format("ProjectFileAuditSysNo={0}&MessageStatus={1}", sysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        InUser = UserSysNo,
                        MsgType = 22,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计归档资料审核信息"),
                        QueryCondition = pro.pro_name,
                        Status = "A",
                        IsDone = "A",
                        ToRole = "0"
                    };
                    //声明
                    string sysMsgString = "";
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        Response.Write(sysMsgString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }

            }
        }

        /// <summary>
        /// 更新审核信息
        /// </summary>
        private void UpdateProjectFileAuditRecord()
        {

            if (Flag == 0)
            {
                GetNextProcessRoleUser();
            }
            else
            {
                //审核实体
                TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
                int proID = ProjectFileInfoAuditEntity.ProjectSysNo;

                TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(proID);
                //状态
                string status = ProjectFileInfoAuditEntity.Status;
                int asciiCode = (int)Convert.ToChar(status);
                if (asciiCode % 2 != 0)
                {
                    asciiCode++;
                }
                else
                {
                    asciiCode = asciiCode + 2;
                }
                TG.Model.cm_ProjectFileInfoAudit projectPlotInfoAuditTemp = bll.GetProjectFileInfoAuditEntity(new cm_ProjectFileAuditQueryEntity {  ProjectPlotAuditSysNo = ProjectFileInfoAuditEntity.SysNo });

                projectPlotInfoAuditTemp.Status = ((char)asciiCode).ToString();
                projectPlotInfoAuditTemp.Suggestion = projectPlotInfoAuditTemp.Suggestion + ProjectFileInfoAuditEntity.Suggestion;
                projectPlotInfoAuditTemp.AuditUser = projectPlotInfoAuditTemp.AuditUser + UserSysNo;
                projectPlotInfoAuditTemp.AuditDate = projectPlotInfoAuditTemp.AuditDate + DateTime.Now;


                int count = bll.UpdateProjectFileAudit(projectPlotInfoAuditTemp);

                if (count > 0)
                {
                    if (projectPlotInfoAuditTemp.Status == "D")
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ProjectFileAuditSysNo={0}&MessageStatus={1}", ProjectFileInfoAuditEntity.SysNo.ToString(), projectPlotInfoAuditTemp.Status),
                            FromUser = projectPlotInfoAuditTemp.InUser,
                            InUser = UserSysNo,
                            MsgType = 22,
                            ToRole = "0",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计归档资料审核全部通过"),
                            QueryCondition = pro.pro_name,
                            IsDone = "B",
                            Status = "A"
                        };
                        int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        if (resultcount > 0)
                        {
                            Response.Write("1");
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }
                    else
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("ProjectFileAuditSysNo={0}&MessageStatus={1}", ProjectFileInfoAuditEntity.SysNo.ToString(), projectPlotInfoAuditTemp.Status),
                            FromUser = UserSysNo,
                            InUser = UserSysNo,
                            MsgType = 22,
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计归档资料审核信息"),
                            QueryCondition = pro.pro_name,
                            IsDone = "A",
                            Status = "A",
                            ToRole = "0"
                        };
                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageViewEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            Response.Write(sysMsgString);
                        }
                        else
                        {
                            Response.Write("0");
                        }
                    }

                }

            }
        }

        /// <summary>
        /// 不同意
        /// </summary>
        private void DisAgreeProjectFileAuditRecord()
        {
            TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
            int proID = ProjectFileInfoAuditEntity.ProjectSysNo;
            TG.Model.cm_Project pro = new TG.BLL.cm_Project().GetModel(proID);
            //状态
            string status = ProjectFileInfoAuditEntity.Status;
            int asciiCode = (int)Convert.ToChar(status);
            if (asciiCode % 2 != 0)
            {
                asciiCode = asciiCode + 2;
            }
            else
            {
                asciiCode = asciiCode + 3;
            }
            TG.Model.cm_ProjectFileInfoAudit projectPlotInfoAuditTemp = bll.GetProjectFileInfoAuditEntity(new cm_ProjectFileAuditQueryEntity { ProjectPlotAuditSysNo = ProjectFileInfoAuditEntity.SysNo });

            projectPlotInfoAuditTemp.Status = ((char)asciiCode).ToString();
            projectPlotInfoAuditTemp.Suggestion = projectPlotInfoAuditTemp.Suggestion + ProjectFileInfoAuditEntity.Suggestion;
            projectPlotInfoAuditTemp.AuditUser = projectPlotInfoAuditTemp.AuditUser + UserSysNo;
            projectPlotInfoAuditTemp.AuditDate = projectPlotInfoAuditTemp.AuditDate + DateTime.Now;


            int count = bll.UpdateProjectFileAudit(projectPlotInfoAuditTemp);

            if (count > 0)
            {
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("ProjectFileAuditSysNo={0}&MessageStatus={1}", ProjectFileInfoAuditEntity.SysNo.ToString(), projectPlotInfoAuditTemp.Status),
                    FromUser = projectPlotInfoAuditTemp.InUser,
                    InUser = UserSysNo,
                    MsgType = 22,
                    ToRole = "0",
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", pro.pro_name, "工程设计归档资料审核未通过"),
                    QueryCondition = pro.pro_name,
                    IsDone = "B",
                    Status = "A"
                };
                int resultcount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                if (resultcount > 0)
                {
                    Response.Write("1");
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        //返回下一阶段用户列表
        private void GetNextProcessRoleUser()
        {
            //发起流程
            if (Action == 0)
            {
                string roleUserString = "";

                //取得生产经营部
                string roleSysNoString = GetStatusProcess(1);
                roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));

                if (!string.IsNullOrEmpty(roleUserString))
                {
                    Response.Write(roleUserString);
                }
                else
                {
                    Response.Write("0");
                }
            }
            else if (Action == 1)//审批流程
            {
                string roleSysNoString = "";

                roleSysNoString = GetStatusProcess(2);

                //获取用户列表
                if (roleSysNoString == "1")
                {
                    Response.Write("1");
                }
                else
                {
                    string roleUserString = CommonAudit.GetRoleName(int.Parse(roleSysNoString));
                    if (!string.IsNullOrEmpty(roleUserString))
                    {
                        Response.Write(roleUserString);
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
            }
        }

        //获取审批阶段对应角色ID
        private string GetStatusProcess(int postion)
        {
            string sql = " Select RoleSysNo from cm_ProjectFileAuditConfig where Position =" + postion;
            object resultObj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            return resultObj == null ? "0" : resultObj.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}