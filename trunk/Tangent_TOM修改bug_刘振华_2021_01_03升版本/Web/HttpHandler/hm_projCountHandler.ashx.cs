﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// hm_projCountHandler1 的摘要说明
    /// </summary>
    public class hm_projCountHandler1 : hm_projCountHandler
    {
        public override string ProcName
        {
            get
            {
                return "P_cm_projCount_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_showProject_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_showCount_jq";
            }
        }
        public override string ProcName4
        {
            get
            {
                return "P_cm_Member_jq";
            }
        }
        public override string ProcName5
        {
            get
            {
                return "P_cm_Unit_jq";
            }
        }
        public override string ProcName6
        {
            get
            {
                return "P_cm_showProjectStatus_jq";
            }
        }
        public override string ProcName7
        {
            get
            {
                return "P_cm_showprojectAboutStatus_jq";
            }
        }
    }
}