﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// ProMapshow 的摘要说明
    /// </summary>
    public class ProMapshow : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string cityname = context.Request.QueryString["name"];
            string linkaspx = context.Request.QueryString["aspx"];
            TG.BLL.TblAreaBll bll = new BLL.TblAreaBll();
            if (bll.Update(cityname, linkaspx) > 0)
            {
                context.Response.Write("ok");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}