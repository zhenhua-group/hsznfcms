﻿<%@ WebHandler Language="C#" Class="Getnum" %>

using System;
using System.Web;
using TG.BLL;
using TG.DAL;
using TG.DBUtility;
using TG.Model;

public class Getnum : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string city = context.Request.QueryString["city"];
        string option = context.Request.QueryString["option"];
        city = context.Server.UrlDecode(city);

        TG.DAL.cm_Project dal = new TG.DAL.cm_Project();

        if (option == "all")
        {
            string sql = "SELECT Count(*) FROM cm_Project WHERE BuildAddress LIKE '%甘肃" + city + "%'";
            object obj = TGsqlHelp.ExecuteScalar(sql);
            context.Response.Write(obj.ToString());
        }
        else if (option == "some")
        {
            string sql = "SELECT COUNT(*) FROM cm_Project WHERE BuildAddress LIKE '%甘肃%'OR BuildAddress LIKE'%xiao%'";
            object obj = TGsqlHelp.ExecuteScalar(sql);
            context.Response.Write(obj.ToString());
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}