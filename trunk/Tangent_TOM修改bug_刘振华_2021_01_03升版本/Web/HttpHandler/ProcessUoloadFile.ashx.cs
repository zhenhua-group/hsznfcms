﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using TG.Model;
using System.IO;
using System.Web.SessionState;


namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ProcessUoloadFile : IHttpHandler, IRequiresSessionState
    {

        public HttpRequest Request { get { return HttpContext.Current.Request; } }

        public HttpResponse Response { get { return HttpContext.Current.Response; } }

        public HttpSessionState Session { get { return HttpContext.Current.Session; } }

        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        /// <summary>
        /// 操作类型,1为删除，2为下载
        /// </summary>
        public string Action
        {
            get
            {
                return Request["Action"];
            }
        }

        /// <summary>
        /// 附件系统号
        /// </summary>
        public int SysNo
        {
            get
            {
                int sysNo = 0;
                int.TryParse(Request["SysNo"], out sysNo);
                return sysNo;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (Request.HttpMethod == "POST")
            {
                switch (Action)
                {
                    case "1":
                        DeteleFile();
                        break;
                    case "2":
                        DownLoadFile();
                        break;
                }
            }
        }


        /// <summary>
        /// 删除文件
        /// </summary>
        private void DeteleFile()
        {
            TG.BLL.cm_AttachInfo acttachInfoBLL = new TG.BLL.cm_AttachInfo();
            TG.Model.cm_AttachInfo attachInfo = acttachInfoBLL.GetModel(SysNo);

            string fold = UserInfo["memlogin"].ToString() + "\\";
            string filePath = HttpContext.Current.Server.MapPath(@"..\Attach_User\filedata\" + fold + "\\" + attachInfo.FileName);
            if (System.IO.File.Exists(filePath))
            {
                //删除物理文件
                System.IO.File.Delete(filePath);
                //删除数据库记录
                acttachInfoBLL.Delete(SysNo);
                Response.Write("1");
            }
            else
            {
                //删除数据库记录
                acttachInfoBLL.Delete(SysNo);
                Response.Write("1");
            }
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        private void DownLoadFile()
        {
            TG.BLL.cm_AttachInfo acttachInfoBLL = new TG.BLL.cm_AttachInfo();
            TG.Model.cm_AttachInfo attachInfo = acttachInfoBLL.GetModel(SysNo);

            string fold = UserInfo["memlogin"].ToString() + "\\";
            string filePath = HttpContext.Current.Server.MapPath(@"..\Attach_User\filedata\" + fold + "\\" + attachInfo.FileName);

            if (System.IO.File.Exists(filePath))
            {
                Response.ContentType = "application/x-zip-compressed";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + attachInfo.FileName);
                Response.WriteFile(filePath);
            }
            else
            {
                Response.Clear();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
