﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace TG.Web.HttpHandler
{
	/// <summary>
	/// $codebehindclassname$ 的摘要说明
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	public class CoperationAuditConfigHandler : IHttpHandler
	{
		public HttpRequest Request { get { return HttpContext.Current.Request; } }
		public HttpResponse Response { get { return HttpContext.Current.Response; } }

		#region QueryString
		/// <summary>
		/// 操作类型，1为修改用户
		/// </summary>
		public string Action
		{
			get
			{
				return Request["Action"];
			}
		}
		#endregion


		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			switch (Action)
			{
				case "1":
					UpdateCoperationAuditConfigUsers();
					break;
			}
		}


		private void UpdateCoperationAuditConfigUsers()
		{

		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}
