﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Newtonsoft.Json;
using System.Collections.Generic;
using TG.Model;

namespace TG.Web.HttpHandler
{
	/// <summary>
	/// $codebehindclassname$ 的摘要说明
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	public class CoperationAuditConfigHandler : IHttpHandler
	{
		public HttpRequest Request { get { return HttpContext.Current.Request; } }
		public HttpResponse Response { get { return HttpContext.Current.Response; } }

		#region QueryString
		/// <summary>
		/// 操作类型，1为修改修改工作流，2为修改工作流程对应的角色，3为对应角色的用户,4为修改工作流顺序
		/// </summary>
		public string Action
		{
			get
			{
				return Request["Action"];
			}
		}
		#endregion


		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			switch (Action)
			{
				case "1":
					UpdateCoperationAuditConfigWorkflow();
					break;
				case "2":
					UpdateCoperationAuditConfigRoleRelationship();
					break;
				case "3":
					UpdateRoleUsersRelationship();
					break;
				case "4":
					UpdateWorkflowPosition();
					break;
			}
			Response.End();
		}

		/// <summary>
		/// 修改工作流程名称
		/// </summary>
		private void UpdateCoperationAuditConfigWorkflow()
		{
			TG.BLL.cm_CoperationAuditConfig coperationAuditConfigBLL = new TG.BLL.cm_CoperationAuditConfig();
			int sysNo = 0;
			int.TryParse(Request["SysNo"], out sysNo);
			//得到角色实体
			TG.Model.cm_CoperationAuditConfig coperationAuditConfig = coperationAuditConfigBLL.GetCoperationAuditConfig(sysNo);

			//修改工作流
			coperationAuditConfig.Workflow = Request["Workflow"];

			int count = coperationAuditConfigBLL.UpdateCoperationAuditConfig(coperationAuditConfig);

			Response.Write(count);
		}

		/// <summary>
		/// 修改工作流对应的角色
		/// </summary>
		private void UpdateCoperationAuditConfigRoleRelationship()
		{
			TG.BLL.cm_CoperationAuditConfig coperationAuditConfigBLL = new TG.BLL.cm_CoperationAuditConfig();
			//得到前台要修改的参数
			List<AuditConfigEntity> auditConfigEntityList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AuditConfigEntity>>(Request["data"]);
			int count = 0;
			foreach (var auditParameter in auditConfigEntityList)
			{
				//得到一次实体
				TG.Model.cm_CoperationAuditConfig finalEntity = coperationAuditConfigBLL.GetCoperationAuditConfig(auditParameter.AuditConfigSysNo);

				finalEntity.RoleSysNo = auditParameter.RoleSysNo;

				count = coperationAuditConfigBLL.UpdateCoperationAuditConfig(finalEntity);
			}
			Response.Write(count);
		}

		/// <summary>
		/// 修改角色所包含的用户
		/// </summary>
		private void UpdateRoleUsersRelationship()
		{
			TG.BLL.cm_Role roleBLL = new TG.BLL.cm_Role();

			int roleSysNo = int.Parse(Request["RoleSysNo"]);
			string usersString = Request["Users"];

			TG.Model.cm_Role finalEntity = roleBLL.GetRole(roleSysNo);

			finalEntity.Users = usersString;

			int count = roleBLL.UpdateRole(finalEntity);

			Response.Write(count);
		}

		/// <summary>
		/// 修改工作流顺序
		/// </summary>
		private void UpdateWorkflowPosition()
		{
			TG.BLL.cm_CoperationAuditConfig coperationAuditConfigBLL = new TG.BLL.cm_CoperationAuditConfig();
			List<WorkflowPositionEntity> paramentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<WorkflowPositionEntity>>(Request["data"]);
			int count = 0;
			foreach (var parament in paramentList)
			{
				TG.Model.cm_CoperationAuditConfig finalEntity = coperationAuditConfigBLL.GetCoperationAuditConfig(parament.AuditConfigSysNo);
				finalEntity.Position = parament.Position;
				count = coperationAuditConfigBLL.UpdateCoperationAuditConfig(finalEntity);
			}
			Response.Write(count);
		}


		/// <summary>
		/// 前台用参数载体
		/// </summary>
		public class WorkflowPositionEntity
		{
			public int AuditConfigSysNo { get; set; }

			public int Position { get; set; }
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}
