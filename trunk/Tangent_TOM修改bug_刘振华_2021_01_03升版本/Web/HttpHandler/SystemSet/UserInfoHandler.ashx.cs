﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.SystemSet
{

    /// <summary>
    /// UserInfoHandler 的摘要说明
    /// </summary>
    public class UserInfoHandler : IHttpHandler
    {
        public class unitRoles
        {
            public string unitID { get; set; }
            public string MngRoleID { get; set; }
            public string TecRoleID { get; set; }
            public string MemID { get; set; }
            public string IsSelected { get; set; }
        }
        public HttpRequest Request
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }
        public HttpResponse Response
        {
            get
            {
                return HttpContext.Current.Response;
            }
        }
        /// <summary>
        /// 动作
        /// </summary>
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        /// <summary>
        /// 身份证前六位
        /// </summary>
        public int CardCode
        {
            get
            {
                int code = 0;
                int.TryParse(Request["cardcode"] ?? "0", out code);
                return code;
            }
        }

        /// <summary>
        /// 加密key
        /// </summary>
        private const int key = 1314;

        private TG.BLL.tg_member bllmem = new TG.BLL.tg_member();
        private TG.BLL.tg_memberRole bllrole = new TG.BLL.tg_memberRole();
        private TG.BLL.tg_memberExt bllext = new TG.BLL.tg_memberExt();
        private TG.BLL.cm_MemArchLevelRelation bllrelation = new TG.BLL.cm_MemArchLevelRelation();
        private TG.BLL.tg_memberExtInfo bllextinfo = new TG.BLL.tg_memberExtInfo();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";


            switch (this.Action)
            {
                case "getaddress":
                    GetAddressByCode(this.CardCode);
                    break;
                case "saveData":
                    SaveUserInfo();
                    break;
                case "editData":
                    EditUserInfo();
                    break;
                case "savepositionchg":
                    SavePosition();
                    break;
                case "savegongzichg":
                    SaveGongZiChange();
                    break;
                case "saveshebaochg":
                    SaveSheBaoChange();
                    break;
                case "savejiangli":
                    SaveJiangli();
                    break;
                case "savechengfa":
                    SaveChengfa();
                    break;
                case "savesigncpr":
                    SaveSignCpr();
                    break;
                case "delsigncpr":
                    DeleteSignCpr();
                    break;
                case "savememroles":
                    SaveMemRoles();
                    break;
                case "savememlevel":
                    SaveMemLevel();
                    break;
                case "savememout":
                    SaveMemOut();
                    break;
                case "saveorder":
                    SaveOrder();
                    break;
            }
        }
        /// <summary>
        /// 保存排序
        /// </summary>
        private void SaveOrder()
        {
            int order=Convert.ToInt32(Request["order"] ?? "0");
            int uid = Convert.ToInt32(Request["uid"] ?? "0");

            if (uid > 0)
            {
                var bllmem=new TG.BLL.tg_member();
                var memModel = bllmem.GetModel(uid);

                if (memModel != null)
                {
                    memModel.mem_Order = order;

                    if (bllmem.Update(memModel))
                    {
                        Response.Write("1");
                    }
                    else
                    {
                        Response.Write("0");
            }
        }
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 保存旅游安排表
        /// </summary>
        private void SaveMemOut()
        {
            string json = Request["data"];

            if (!string.IsNullOrEmpty(json))
            {
                var bll = new TG.BLL.cm_MemOutHis();

                var listmodel = JsonConvert.DeserializeObject<List<TG.Model.cm_MemOutHis>>(json);
                if (listmodel != null && listmodel.Count > 0)
                {
                    int number=0;
                    listmodel.ForEach(model =>
                    {
                        string strWhere = string.Format(" SetYear={0} AND Mem_ID={1} ", model.SetYear, model.Mem_ID);

                        int count = bll.GetRecordCount(strWhere);
                        if (count == 0)
                        {
                            bll.Add(model);
                            number++;
                           
                        }
                        else
                        {
                            strWhere = string.Format(" SetYear={0} AND Mem_ID={1} ", model.SetYear, model.Mem_ID);
                            var list = bll.GetModelList(strWhere);
                            model.ID = list[0].ID;

                            bll.Update(model);
                            number++;
                           
                        }
                    });
                    if (number == listmodel.Count)
                    {
                        Response.Write("1");
                    }
                    else if (number > 0)
                    {
                        Response.Write("2");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                   
                }
                else
                {
                    Response.Write("0");
                }
               
            }
            else
            {
                Response.Write("0");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SaveMemLevel()
        {
            string json = Request["data"];

            if (!string.IsNullOrEmpty(json))
            {
                var bll = new TG.BLL.tg_memberLevel();

                var listmodel = JsonConvert.DeserializeObject<List<TG.Model.tg_memberLevel>>(json);
                if (listmodel != null && listmodel.Count > 0)
                {
                    int number = 0;
                    listmodel.ForEach(model =>
                    {
                        string strWhere = string.Format(" mem_Year={0} AND mem_ID={1} ", model.mem_Year, model.mem_ID);

                        int count = bll.GetRecordCount(strWhere);
                        if (count == 0)
                        {
                            bll.Add(model);

                            number++;
                        }
                        else
                        {
                            strWhere = string.Format(" mem_Year={0} AND mem_ID={1} ", model.mem_Year, model.mem_ID);
                            var list = bll.GetModelList(strWhere);
                            model.ID = list[0].ID;

                            bll.Update(model);

                            number++;
                        }
                    });
                    if (number == listmodel.Count)
                    {
                        Response.Write("1");
                    }
                    else if (number > 0)
                    {
                        Response.Write("2");
                    }
                    else
                    {
                        Response.Write("0");
                    }
                }
                else
                {
                    Response.Write("0");
                }
            }
            else
            {
                Response.Write("0");
            }
        }

        private void SaveMemRoles()
        {
            string json = Request["data"];

            if (!string.IsNullOrEmpty(json))
            {
                var unitList = JsonConvert.DeserializeObject<List<unitRoles>>(json);

                unitList.Where(p => p.unitID != null).ToList().ForEach(p =>
                {

                    string strWhere = string.Format(" mem_Id={0} AND mem_Unit_ID={1}", p.MemID, p.unitID);
                    var roleModelList = new TG.BLL.tg_memberRole().GetModelList(strWhere);
                    if (roleModelList.Count > 0)
                    {
                        if (p.IsSelected == "checked")
                        {
                            var roleModel = roleModelList[0];
                            roleModel.RoleIdMag = p.MngRoleID != null ? int.Parse(p.MngRoleID) : 0;
                            roleModel.RoleIdTec = p.TecRoleID != null ? int.Parse(p.TecRoleID) : 0;
                            roleModel.mem_Unit_ID = int.Parse(p.unitID);
                            //更新权重
                            roleModel.Weights = GetWeightsByRoleType(p.MngRoleID, p.TecRoleID);

                            new TG.BLL.tg_memberRole().Update(roleModel);
                        }
                        else
                        {
                            new TG.BLL.tg_memberRole().Delete(roleModelList[0].ID);
                        }
                    }
                    else
                    {
                        if (p.IsSelected == "checked")
                        {
                            var roleModel = new TG.Model.tg_memberRole();

                            roleModel.mem_Id = int.Parse(p.MemID);
                            roleModel.RoleIdMag = p.MngRoleID != null ? int.Parse(p.MngRoleID) : 0;
                            roleModel.RoleIdTec = p.TecRoleID != null ? int.Parse(p.TecRoleID) : 0;
                            roleModel.mem_Unit_ID = int.Parse(p.unitID);
                            //更新权重
                            roleModel.Weights = GetWeightsByRoleType(p.MngRoleID, p.TecRoleID);

                            new TG.BLL.tg_memberRole().Add(roleModel);
                        }
                    }
                });


                Response.Write("1");
            }
        }
        /// <summary>
        /// 得到用户的权重
        /// </summary>
        /// <param name="roleMagID"></param>
        /// <param name="roleTecID"></param>
        /// <param name="roleMagName"></param>
        /// <returns></returns>
        public int GetWeightsByRoleType(string roleMagID, string roleTecID)
        {
            int result = 0;

            //首选判断是否为管理职位
            if (roleMagID != "0")
            {
                //如果管理不为空
                var roleModel = new TG.BLL.cm_RoleMag().GetModel(int.Parse(roleMagID));
                if (roleModel != null)
                {
                    result = roleModel.Weights;
                }
            }
            else
            {
                //如果管理不为空
                var roleModel = new TG.BLL.cm_RoleMag().GetModel(int.Parse(roleTecID));
                if (roleModel != null)
                {
                    result = roleModel.Weights;
                }
            }

            return result;
        }
        private void DeleteSignCpr()
        {
            int id = Convert.ToInt32(Request["id"] ?? "0");

            bool isSuccess = new TG.BLL.cm_SignCprRecord().Delete(id);

            if (isSuccess)
            {
                Response.Write("{\"result\":\"success\"}");
            }
            else
            {
                Response.Write("{\"result\":\"error\"}");
            }
        }
        /// <summary>
        /// 签订合同
        /// </summary>
        private void SaveSignCpr()
        {
            string jsonData = Request["data"] ?? "";

            var record = JsonConvert.DeserializeObject<TG.Model.cm_SignCprRecord>(jsonData);

            if (record != null)
            {
                if (record.ID == 0)
                {

                    int affectRow = new TG.BLL.cm_SignCprRecord().Add(record);

                    if (affectRow > 0)
                    {
                        Response.Write("{\"result\":\"success\",\"id\":\"" + affectRow + "\"}");
                    }
                    else
                    {
                        Response.Write("{\"result\":\"error\"}");
                    }
                }
                else
                {
                    bool isSuccess = new TG.BLL.cm_SignCprRecord().Update(record);

                    if (isSuccess)
                    {
                        Response.Write("{\"result\":\"success\",\"id\":\"" + record.ID + "\"}");
                    }
                    else
                    {
                        Response.Write("{\"result\":\"error\"}");
                    }
                }
            }
        }
        /// <summary>
        /// 惩罚
        /// </summary>
        private void SaveChengfa()
        {
            string jsonData = Request["data"] ?? "";

            var record = JsonConvert.DeserializeObject<TG.Model.cm_ChangfaRecord>(jsonData);

            if (record != null)
            {
                int affectRow = new TG.BLL.cm_ChangfaRecord().Add(record);

                if (affectRow > 0)
                {
                    Response.Write("{\"result\":\"success\"}");
                }
                else
                {
                    Response.Write("{\"result\":\"error\"}");
                }
            }
        }
        //
        private void SaveJiangli()
        {
            string jsonData = Request["data"] ?? "";

            var record = JsonConvert.DeserializeObject<TG.Model.cm_JiangliRecord>(jsonData);

            if (record != null)
            {
                int affectRow = new TG.BLL.cm_JiangliRecord().Add(record);

                if (affectRow > 0)
                {

                    Response.Write("{\"result\":\"success\"}");
                }
                else
                {
                    Response.Write("{\"result\":\"error\"}");
                }
            }
        }
        /// <summary>
        /// 社保调整
        /// </summary>
        private void SaveSheBaoChange()
        {
            string jsonData = Request["data"] ?? "";

            var record = JsonConvert.DeserializeObject<TG.Model.cm_SheBaoChangeRecord>(jsonData);

            if (record != null)
            {
                int affectRow = new TG.BLL.cm_SheBaoChangeRecord().Add(record);

                if (affectRow > 0)
                {
                    //更新人员的ID
                    var tgMember = bllmem.GetModel(record.mem_ID);

                    //插入扩展信息表
                    string Where = string.Format(" mem_ID={0} ", tgMember.mem_ID);
                    var listextinfo = bllextinfo.GetModelList(Where);
                    //不存在插入
                    if (listextinfo.Count > 0)
                    {
                        var oldtgMemExtInfo = listextinfo[0];

                        oldtgMemExtInfo.mem_SheBaobase = record.mem_SheBaobase2;
                        oldtgMemExtInfo.mem_Gongjijin = record.mem_Gongjijin2;

                        bllextinfo.Update(oldtgMemExtInfo);
                    }


                    Response.Write("{\"result\":\"success\"}");
                }
                else
                {
                    Response.Write("{\"result\":\"error\"}");
                }
            }
        }
        /// <summary>
        /// 保存工资
        /// </summary>
        private void SaveGongZiChange()
        {
            string jsonData = Request["data"] ?? "";

            var record = JsonConvert.DeserializeObject<TG.Model.cm_ChargeChangeRecord>(jsonData);

            if (record != null)
            {
                int affectRow = new TG.BLL.cm_ChargeChangeRecord().Add(record);

                if (affectRow > 0)
                {
                    //更新人员的ID
                    var tgMember = bllmem.GetModel(record.mem_ID);

                    //插入扩展信息表
                    string Where = string.Format(" mem_ID={0} ", tgMember.mem_ID);
                    var listextinfo = bllextinfo.GetModelList(Where);
                    //不存在插入
                    if (listextinfo.Count > 0)
                    {
                        var oldtgMemExtInfo = listextinfo[0];

                        oldtgMemExtInfo.mem_GongZi = record.mem_GongZi2;
                        oldtgMemExtInfo.mem_Yufa = record.mem_Yufa2;
                        //同时更新应发 2018年1月2日 
                        oldtgMemExtInfo.mem_YingFa = record.mem_GongZi2 + record.mem_Yufa2 + oldtgMemExtInfo.mem_Jiaotong + oldtgMemExtInfo.mem_Tongxun;
                        bllextinfo.Update(oldtgMemExtInfo);
                    }


                    Response.Write("{\"result\":\"success\"}");
                }
                else
                {
                    Response.Write("{\"result\":\"error\"}");
                }
            }
        }
        /// <summary>
        /// 保存职务调整
        /// </summary>
        private void SavePosition()
        {
            string jsonData = Request["data"] ?? "";

            var record = JsonConvert.DeserializeObject<TG.Model.cm_PositionChangeRecord>(jsonData);

            if (record != null)
            {
                int affectRow = new TG.BLL.cm_PositionChangeRecord().Add(record);

                if (affectRow > 0)
                {
                    //更新人员的ID
                    var tgMember = bllmem.GetModel(record.mem_ID);

                    string Where = string.Format(" mem_Id={0} AND mem_Unit_ID={1} ", tgMember.mem_ID, tgMember.mem_Unit_ID);
                    var listrole = bllrole.GetModelList(Where);
                    //查询
                    if (listrole.Count > 0)
                    {
                        var oldtgMemRole = listrole[0];

                        oldtgMemRole.RoleIdMag = record.MagRole2;
                        oldtgMemRole.RoleIdTec = record.TecRole2;
                        //更新
                        bllrole.Update(oldtgMemRole);
                    }


                    //插入扩展信息表
                    Where = string.Format(" mem_ID={0} ", tgMember.mem_ID);
                    var listextinfo = bllextinfo.GetModelList(Where);
                    //不存在插入
                    if (listextinfo.Count > 0)
                    {
                        var oldtgMemExtInfo = listextinfo[0];

                        oldtgMemExtInfo.mem_Jiaotong = record.JiaoTong2;
                        oldtgMemExtInfo.mem_Tongxun = record.Tongxun2;

                        bllextinfo.Update(oldtgMemExtInfo);
                    }


                    Response.Write("{\"result\":\"success\"}");
                }
                else
                {
                    Response.Write("{\"result\":\"error\"}");
                }
            }
        }
        /// <summary>
        /// 编辑用户
        /// </summary>
        private void EditUserInfo()
        {
            #region 接收数据
            //tgmember
            string strJson = Request["tgmember"] ?? "";
            var tgMember = JsonConvert.DeserializeObject<TG.Model.tg_member>(strJson);
            //tgmemRole
            strJson = Request["tgmemrole"] ?? "";
            var tgMemRole = JsonConvert.DeserializeObject<TG.Model.tg_memberRole>(strJson);
            //tgmemext
            strJson = Request["tgmemext"] ?? "";
            var tgMemExt = JsonConvert.DeserializeObject<TG.Model.tg_memberExt>(strJson);
            //tgmemrelation
            strJson = Request["tgmemrelation"] ?? "";
            var tgMemRelation = JsonConvert.DeserializeObject<TG.Model.cm_MemArchLevelRelation>(strJson);
            //tgmemextinfo
            strJson = Request["tgmemextinfo"] ?? "";
            var tgMemExtInfo = JsonConvert.DeserializeObject<TG.Model.tg_memberExtInfo>(strJson);
            #endregion

            //查询原有的人员数据
            var oldtgMember = bllmem.GetModel(tgMember.mem_ID);

            //赋值改变的项
            //原密码与现密码是否相同,相同无需加密
            if (oldtgMember.mem_Password == tgMember.mem_Password)
            {
                oldtgMember.mem_Password = tgMember.mem_Password;
            }
            else
            {
                oldtgMember.mem_Password = GetPwdEncryString(tgMember.mem_Password);
            }            
            oldtgMember.mem_isFired = tgMember.mem_isFired;
            oldtgMember.mem_Sex = tgMember.mem_Sex;
            oldtgMember.mem_Unit_ID = tgMember.mem_Unit_ID;
            oldtgMember.mem_Speciality_ID = tgMember.mem_Speciality_ID;
            oldtgMember.mem_Birthday = tgMember.mem_Birthday;
            oldtgMember.mem_Mobile = tgMember.mem_Mobile;
            oldtgMember.mem_Telephone = tgMember.mem_Telephone;

            //更新  
            bool isUpdated = bllmem.Update(oldtgMember);

            if (isUpdated)
            {
                //更新人员的ID
                int newMemID = tgMember.mem_ID;

                string Where = string.Format(" mem_Id={0} AND mem_Unit_ID={1} ", newMemID, tgMemRole.mem_Unit_ID);
                var listrole = bllrole.GetModelList(Where);
                //不存在插入
                if (listrole.Count == 0)
                {
                    //查询是否存在
                    tgMemRole.mem_Id = newMemID;
                    bllrole.Add(tgMemRole);
                }
                else
                {
                    var oldtgMemRole = listrole[0];

                    oldtgMemRole.RoleIdMag = tgMemRole.RoleIdMag;
                    oldtgMemRole.RoleIdTec = tgMemRole.RoleIdTec;

                    //获取权重
                    var bllRoleMag = new TG.BLL.cm_RoleMag();
                    if (oldtgMemRole.RoleIdMag != -1)
                    {
                        var roleMag = bllRoleMag.GetModel(Convert.ToInt32(oldtgMemRole.RoleIdMag));

                        int weights = roleMag.Weights;

                        oldtgMemRole.Weights = weights;
                    }
                    else
                    {
                        if (oldtgMemRole.RoleIdTec != -1)
                        {
                            var roleMag = bllRoleMag.GetModel(Convert.ToInt32(oldtgMemRole.RoleIdTec));

                            int weights = roleMag.Weights;

                            oldtgMemRole.Weights = weights;
                        }
                    }
                    //更新人员权重部门
                    oldtgMemRole.mem_Unit_ID = tgMember.mem_Unit_ID;

                    //更新
                    bllrole.Update(oldtgMemRole);
                }

                //用户入职离职信息
                Where = string.Format(" mem_ID={0} ", newMemID);
                var listext = bllext.GetModelList(Where);
                //不存在插入
                if (listext.Count == 0)
                {
                    //查询是否存在
                    tgMemExt.mem_ID = newMemID;
                    bllext.Add(tgMemExt);
                }
                else
                {
                    var oldtgMemExt = listext[0];

                    oldtgMemExt.mem_InTime = tgMemExt.mem_InTime;
                    oldtgMemExt.mem_OutTime = tgMemExt.mem_OutTime;
                    //更新
                    bllext.Update(oldtgMemExt);
                }
                //注册关联表
                Where = string.Format(" mem_Id={0} ", newMemID);
                var listrelation = bllrelation.GetModelList(Where);
                //不存在插入
                if (listrelation.Count == 0)
                {
                    //查询是否存在
                    tgMemRelation.Mem_ID = newMemID;
                    bllrelation.Add(tgMemRelation);
                }
                else
                {
                    var oldtgMemRelation = listrelation[0];

                    oldtgMemRelation.ArchLevel_ID = tgMemRelation.ArchLevel_ID;
                    //更新
                    bllrelation.Update(oldtgMemRelation);
                }

                //插入扩展信息表
                Where = string.Format(" mem_ID={0} ", newMemID);
                var listextinfo = bllextinfo.GetModelList(Where);
                //不存在插入
                if (listextinfo.Count == 0)
                {
                    //查询是否存在
                    tgMemExtInfo.mem_ID = newMemID;
                    bllextinfo.Add(tgMemExtInfo);
                }
                else
                {
                    var oldtgMemExtInfo = listextinfo[0];

                    int oldID = oldtgMemExtInfo.ID;
                    //赋值
                    oldtgMemExtInfo = tgMemExtInfo;
                    oldtgMemExtInfo.ID = oldID;

                    bllextinfo.Update(oldtgMemExtInfo);
                }
            }

            Response.Write("{\"result\":\"edit\"}");
        }
        /// <summary>
        /// 保存用户数据
        /// </summary>
        private void SaveUserInfo()
        {
            #region 接收数据
            //tgmember
            string strJson = Request["tgmember"] ?? "";
            var tgMember = JsonConvert.DeserializeObject<TG.Model.tg_member>(strJson);
            //tgmemRole
            strJson = Request["tgmemrole"] ?? "";
            var tgMemRole = JsonConvert.DeserializeObject<TG.Model.tg_memberRole>(strJson);
            //tgmemext
            strJson = Request["tgmemext"] ?? "";
            var tgMemExt = JsonConvert.DeserializeObject<TG.Model.tg_memberExt>(strJson);
            //tgmemrelation
            strJson = Request["tgmemrelation"] ?? "";
            var tgMemRelation = JsonConvert.DeserializeObject<TG.Model.cm_MemArchLevelRelation>(strJson);
            //tgmemextinfo
            strJson = Request["tgmemextinfo"] ?? "";
            var tgMemExtInfo = JsonConvert.DeserializeObject<TG.Model.tg_memberExtInfo>(strJson);
            #endregion

            #region 查询登录名是否重复
            string strWhere = string.Format(" mem_Login='{0}'", tgMember.mem_Login);
            var listmem = bllmem.GetModelList(strWhere);

            if (listmem.Count > 0)
            {
                Response.Write("{\"result\":\"exsit\"}");
                return;
            }
            #endregion

            //加密用户密码
            tgMember.mem_Password = GetPwdEncryString(tgMember.mem_Password);

            //插入用户数据
            int newMemID = bllmem.Add(tgMember);

            if (newMemID > 0)
            {
                string Where = string.Format(" mem_Id={0} AND mem_Unit_ID={1} ", newMemID, tgMemRole.mem_Unit_ID);
                var listrole = bllrole.GetModelList(Where);
                //不存在插入
                if (listrole.Count == 0)
                {
                    //查询是否存在
                    tgMemRole.mem_Id = newMemID;
                    //获取权重
                    var bllRoleMag = new TG.BLL.cm_RoleMag();
                    if (tgMemRole.RoleIdMag != -1)
                    {
                        var roleMag = bllRoleMag.GetModel(Convert.ToInt32(tgMemRole.RoleIdMag));

                        int weights = roleMag.Weights;

                        tgMemRole.Weights = weights;
                    }
                    else
                    {
                        if (tgMemRole.RoleIdTec != -1)
                        {
                            var roleMag = bllRoleMag.GetModel(Convert.ToInt32(tgMemRole.RoleIdTec));

                            int weights = roleMag.Weights;

                            tgMemRole.Weights = weights;
                        }
                    }
                    //添加
                    bllrole.Add(tgMemRole);
                }

                //用户入职离职信息
                Where = string.Format(" mem_ID={0} ", newMemID);
                var listext = bllext.GetModelList(Where);
                //不存在插入
                if (listext.Count == 0)
                {
                    //查询是否存在
                    tgMemExt.mem_ID = newMemID;
                    bllext.Add(tgMemExt);
                }

                //注册关联表
                Where = string.Format(" mem_Id={0} ", newMemID);
                var listrelation = bllrelation.GetModelList(Where);
                //不存在插入
                if (listrelation.Count == 0)
                {
                    //查询是否存在
                    tgMemRelation.Mem_ID = newMemID;
                    bllrelation.Add(tgMemRelation);
                }

                //插入扩展信息表
                Where = string.Format(" mem_ID={0} ", newMemID);
                var listextinfo = bllextinfo.GetModelList(Where);
                //不存在插入
                if (listextinfo.Count == 0)
                {
                    //查询是否存在
                    tgMemExtInfo.mem_ID = newMemID;
                    bllextinfo.Add(tgMemExtInfo);
                }
            }

            Response.Write("{\"result\":\"success\"}");
        }
        /// <summary>
        /// 获取地址
        /// </summary>
        /// <param name="codeid"></param>
        private void GetAddressByCode(int codeid)
        {
            var bll = new TG.BLL.cm_IdentityCard();
            string strWhere = string.Format(" CardID={0} ", this.CardCode);
            var list = bll.GetModelList(strWhere);

            if (list.Count > 0)
            {
                Response.Write("{\"result\":\"" + list[0].CardAddress + "\"}");
            }
            else
            {
                Response.Write("{\"result\":\"暂时无法获取信息\"}");
            }
        }
        /// <summary>
        /// 加密密码
        /// </summary>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public string GetPwdEncryString(string pwd)
        {
            string str_rlt = "";
            string str_array = pwd;
            for (int i = 0; i < str_array.Length; i++)
            {
                char a = Convert.ToChar(str_array.Substring(i, 1));
                str_rlt += (char)((int)a ^ (key >> 8));
            }

            str_array = str_rlt;
            str_rlt = "";
            string str_rlt2 = "";
            int index = 0;
            for (int i = 0; i < str_array.Length; i++)
            {
                str_rlt2 = "";
                index = Convert.ToByte(Convert.ToChar(str_array.Substring(i, 1)));
                str_rlt2 += (char)(65 + index / 26);
                str_rlt2 += (char)(65 + index % 26);
                str_rlt += str_rlt2;
            }

            return str_rlt;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}