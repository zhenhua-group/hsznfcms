﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace TG.Web.HttpHandler.SystemSet
{
    /// <summary>
    /// PersonAttendSetHandler 的摘要说明
    /// </summary>
    public class PersonAttendSetHandler : IHttpHandler
    {

        public string data
        {
            get { return HttpContext.Current.Request.Form["data"] ?? ""; }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            TG.BLL.cm_PersonAttendSet bll_pas = new TG.BLL.cm_PersonAttendSet();
            List<TG.Model.cm_PersonAttendSet> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.cm_PersonAttendSet>>(data);
            if (list != null&&list.Count>0)
            {
                int count = 0;
                list.ForEach(pas =>
                    {                      
                        int attid = bll_pas.ExistsBackID(pas.mem_ID, pas.attend_month,pas.attend_year);
                        if (attid > 0)
                        {
                            pas.attend_id = attid;
                            if (bll_pas.Update(pas))
                            {
                                count++;
                            }

                        }
                        else
                        {
                            int tempid = bll_pas.Add(pas);
                            if (tempid > 0)
                            {
                                count++;
                            }
                        }
                      
                    });
                if (count == list.Count)
                {
                    context.Response.Write("1");
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            else
            {
                context.Response.Write("0");
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}