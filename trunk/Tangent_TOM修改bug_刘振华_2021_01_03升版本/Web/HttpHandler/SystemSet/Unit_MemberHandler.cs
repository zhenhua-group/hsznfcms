﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;

namespace TG.Web.HttpHandler.SystemSet
{
    public abstract class Unit_MemberHandler : IHttpHandler
    {//部门存储过程名
        public abstract string ProcName { get; }
        //人员存储过程名
        public abstract string ProcName2 { get; }
        //方案补贴存储过程名
        public abstract string ProcName3 { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {
                string strResponse = "删除失败";
                string SysType = forms.Get("SysType");
                string strid = forms.Get("EmpId");
                if (SysType == "member")
                {
                    strResponse = "用户删除成功";
                    TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                    if (!bll_mem.DeleteList(strid))
                    {
                        strResponse = "用户删除失败";
                    }
                    else
                    {
                        new TG.BLL.cm_EleSign().DeleteListMemID(strid);
                    }

                }
                else if (SysType == "compensationset")
                {
                    strResponse = "所补产值配置删除成功";
                    TG.BLL.cm_CompensationSet bll = new TG.BLL.cm_CompensationSet();
                    if (!bll.DeleteList(strid))
                    {
                        strResponse = "所补产值配置删除失败";
                    }
                }
                else
                {
                    strResponse = "部门删除成功";
                    TG.BLL.tg_unit bll = new TG.BLL.tg_unit();
                    TG.Model.tg_unit model = new TG.Model.tg_unit();
                    string[] str = strid.Split(',');
                    bool flag = true;
                    for (int i = 0; i < str.Length; i++)
                    {
                        //检查是否该部门下存在成员
                        string existsUserSql = "select top 1 1 from tg_member where mem_Unit_ID = " + str[i];

                        object existsObjResult = TG.DBUtility.DbHelperSQL.GetSingle(existsUserSql);

                        if (existsObjResult == null)
                        {
                            bll.Delete(int.Parse(str[i]));
                        }
                        else
                        {
                            flag = false;
                        }
                    }


                    if (flag == false)
                    {
                        strResponse += "，其中一些部门包含用户，不能删除！";
                    }
                }

                context.Response.Write(strResponse);
            }
            else if (strAction == "sel")
            {

                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (keyname.Trim() != "")
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND unit_Name like '%{0}%'", keyname);
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "member")
            {

                string unit = context.Request.QueryString["unit"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND (mem_Name like '%{0}%'  or mem_Login Like '%{0}%')", keyname);
                }
                //按生产部门查询
                if (unit != "-1" && unit != null)
                {
                    strsql.AppendFormat(" AND (mem_Unit_ID= {0})", unit);
                }

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName2;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "compensationset")
            {

                string ValueYear = context.Request.QueryString["ValueYear"];
                string unit = context.Request.QueryString["unit"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");

                strsql.Append(strWhere);

                //按名称查询
                if (!string.IsNullOrEmpty(ValueYear) && ValueYear != "-1")
                {
                   // ValueYear = DateTime.Now.Year.ToString();
                    strsql.AppendFormat(" AND ValueYear='{0}'", ValueYear);
                }

                //按生产部门查询
                if (unit != "-1" && unit != null)
                {
                    strsql.AppendFormat(" AND (UnitId= {0})", unit);
                }
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND mem_Name like '%{0}%' ", keyname);
                }
                parameters.Where = strsql.ToString();
                parameters.ProcedureName = ProcName3;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}