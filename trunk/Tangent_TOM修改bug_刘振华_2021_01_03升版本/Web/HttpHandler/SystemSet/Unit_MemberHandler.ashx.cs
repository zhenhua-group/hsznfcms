﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace TG.Web.HttpHandler.SystemSet
{
    /// <summary>
    /// Unit_MemberHandler1 的摘要说明
    /// </summary>
    public class Unit_MemberHandler1 : Unit_MemberHandler
    {
        public override string ProcName
        {
            get
            {
                return "P_cm_Unit_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_Member_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_CompensationSet_jq";
            }
        }
    }
}