﻿using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.SystemSet
{
    public abstract class SysUserInfoHandler : IHttpHandler
    {
        /// <summary>
        /// 请求
        /// </summary>
        public HttpRequest Request
        {
            get
            {
                return HttpContext.Current.Request;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public HttpResponse Response
        {
            get
            {
                return HttpContext.Current.Response;
            }
        }
        /// <summary>
        /// 用户列表存储过程名
        /// </summary>
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";

            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            if (strAction != "export")
            {
                if (strOper != "del")
                {
                    strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                    parameters = new TG.BLL.QueryPagingParameters
                    {
                        PageIndex = int.Parse(context.Request["PageIndex"]),
                        PageSize = int.Parse(context.Request["PageSize"]),
                        OrderBy = context.Request["OrderBy"],
                        Sort = context.Request["Sort"],
                        ProcedureName = ProcName,
                        Where = strWhere
                    };
                }

                if (strOper == null && strAction == null)
                {
                    //传入查询语句
                    string sqlwhere = "";

                    sqlwhere = sqlwhere + strWhere;
                    parameters.Where = sqlwhere;
                    parameters.ProcedureName = ProcName;
                    this.PagingParameters = parameters;
                    context.Response.Write(GetJsonResult());
                }
                else if (strAction == "search")
                {
                    SearchUserInfoList(strWhere, parameters);
                }
            }

        }
        

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="strWhere"></param>
        private void SearchUserInfoList(string strWhere, TG.BLL.QueryPagingParameters parameters)
        {
            strWhere = Request["strwhere"] ?? "";
            string memLogin = Request["mem_Login"] ?? "";
            string memName = Request["mem_Name"] ?? "";
            string memBirthday = Request["mem_Birthday"] ?? "";
            string memSex = Request["mem_Sex"] ?? "";
            string memUnitName = Request["memUnitName"] ?? "";
            string memSpeName = Request["memSpeName"] ?? "";
            string isFiredName = Request["isFiredName"] ?? "";
            string roleTecName = Request["RoleTecName"] ?? "";
            string roleMagName = Request["RoleMagName"] ?? "";
            string memArchReg = Request["mem_ArchReg"] ?? "";
            string memCode = Request["mem_Code"] ?? "";
            string memAge = Request["mem_Age"] ?? "";
            string memCoperationDate = Request["mem_CoperationDate"] ?? "";
            string memList = Request["mem_List"] ?? "";


            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(memLogin))
            {
                sb.AppendFormat(" AND mem_Login like '%{0}%'", memLogin);
            }

            if (!string.IsNullOrEmpty(memName))
            {
                sb.AppendFormat(" AND mem_Name like '%{0}%'", memName);
            }

            if (!string.IsNullOrEmpty(memBirthday))
            {
                sb.AppendFormat(" AND mem_Birthday='{0}'", memBirthday);
            }

            if (memSex != "-1")
            {
                sb.AppendFormat(" AND mem_Sex='{0}'", memSex);
            }

            if (memUnitName != "")
            {
                sb.AppendFormat(" AND memUnitName IN ({0})", memUnitName);
            }

            if (memList != "")
            {
                sb.AppendFormat(" AND memID IN ({0})", memList);
            }

            if (memSpeName != "-1")
            {
                sb.AppendFormat(" AND memSpeName = '{0}'", memSpeName);
            }

            if (isFiredName != "-1")
            {
                sb.AppendFormat(" AND isFiredName='{0}'", isFiredName);
            }

            if (roleTecName != "-1")
            {
                sb.AppendFormat(" AND RoleTecName = '{0}'", roleTecName);
            }

            if (roleMagName != "-1")
            {
                sb.AppendFormat(" AND RoleMagName = '{0}'", roleMagName);
            }

            if (memArchReg != "-1")
            {
                sb.AppendFormat(" AND mem_ArchReg = '{0}'", memArchReg);
            }

            if (!string.IsNullOrEmpty(memCode))
            {
                sb.AppendFormat(" AND mem_Code = '{0}'", memCode);
            }

            if (!string.IsNullOrEmpty(memAge))
            {
                sb.AppendFormat(" AND mem_Age = {0}", memAge);
            }

            if (!string.IsNullOrEmpty(memCoperationDate))
            {
                sb.AppendFormat(" AND mem_CoperationDate = '{0}'", memCoperationDate);
            }

            sb.Append(strWhere);

            parameters.Where = sb.ToString();
            parameters.ProcedureName = ProcName;
            //查询
            this.PagingParameters = parameters;

            Response.Write(GetJsonResult());
        }

        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];

            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        
        bool IHttpHandler.IsReusable
        {
            get { return false; }
        }
    }
}