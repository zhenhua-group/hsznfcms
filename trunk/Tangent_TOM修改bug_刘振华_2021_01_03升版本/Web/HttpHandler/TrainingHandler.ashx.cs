﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using TG.Model;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// ProjectChargeHandler 的摘要说明
    /// </summary>
    public class TrainingHandler : HandlerCommon, IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //Action 
            string flag = context.Request.Params["action"] ?? "";
            if (flag == "add")
            {
                //添加收费
                string property = context.Request.Params["property"];
                string title = context.Request.Params["title"];
                DateTime beginDate = Convert.ToDateTime(context.Request.Params["beginDate"]);
                DateTime endDate =Convert.ToDateTime(context.Request.Params["endDate"]);
                string peoples = context.Request.Params["peoples"];

                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                List<TG.Model.Training> trains = jsonSerializer.Deserialize<List<TG.Model.Training>>(peoples);


                TG.BLL.Training bll = new TG.BLL.Training();
                TG.Model.Training model = new TG.Model.Training();
                model.Property = property;
                model.Title = title;
                model.BeginDate = beginDate;
                model.EndDate = endDate;

                int trainingInfoId = bll.AddTrainingInfo(model);
                int result=bll.AddTrainingInfoDetail(trains, trainingInfoId);
                if (result > 0)
                {
                    context.Response.Write("0");
                }
                else
                {
                    context.Response.Write("1");
                }



            }
            else if (flag == "delete")
            {
                //添加收费
                int id = Convert.ToInt32(context.Request.Params["id"]);
                TG.BLL.Training bll = new TG.BLL.Training();
           

                bool result= bll.Delete(id);
                if (result)
                {
                    context.Response.Write("0");
                }
                else
                {
                    context.Response.Write("1");
                }



            }
            else if (flag == "edit")
            {
            
                int id =Convert.ToInt32(context.Request.Params["id"]);
                string property = context.Request.Params["property"];
                string title = context.Request.Params["title"];
                DateTime beginDate = Convert.ToDateTime(context.Request.Params["beginDate"]);
                DateTime endDate = Convert.ToDateTime(context.Request.Params["endDate"]);
                string peoples = context.Request.Params["peoples"];

                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                List<TG.Model.Training> trains = jsonSerializer.Deserialize<List<TG.Model.Training>>(peoples);



                TG.BLL.Training bll = new TG.BLL.Training();
                TG.Model.Training model = new TG.Model.Training();
                model.Property = property;
                model.Title = title;
                model.BeginDate = beginDate;
                model.EndDate = endDate;
                model.Id = id;
                if (bll.UpdateTrainingInfo(model))
                {
                    if (bll.UpdateTrainingInfoDetail(id,trains))
                    {
                        context.Response.Write("0");
                    }
                }
                else
                {
                    context.Response.Write("1");
                }
                
            }

        }
    
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}