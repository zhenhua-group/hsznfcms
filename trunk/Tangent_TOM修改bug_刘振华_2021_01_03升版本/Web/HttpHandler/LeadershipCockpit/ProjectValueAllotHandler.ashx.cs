﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler.LeadershipCockpit
{
    /// <summary>
    /// ProjectValueAllotHandler1 的摘要说明
    /// </summary>
    public class ProjectValueAllotHandler1 : ProjectValueAllotHandler
    {

        public override string ProcName
        {
            get
            {
                return "P_cm_ProjectValueAllot_jq";
            }
        }
        public override string ProcName2
        {
            get
            {
                return "P_cm_MembeValueAllot_jq";
            }
        }
        public override string ProcName3
        {
            get
            {
                return "P_cm_MembeValueAllot2_jq";
            }
        }
    }
}