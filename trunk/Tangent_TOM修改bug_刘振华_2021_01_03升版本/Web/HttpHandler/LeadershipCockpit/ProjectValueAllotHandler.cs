﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;

namespace TG.Web.HttpHandler.LeadershipCockpit
{
    public abstract class ProjectValueAllotHandler : IHttpHandler
    { //项目产值分配表存储过程名
        public abstract string ProcName { get; }
        //个人明细查询一存储过程名
        public abstract string ProcName2 { get; }
        //个人明细查询二存储过程名
        public abstract string ProcName3 { get; }
        //存储过程参数
        protected TG.BLL.ProjectAllotPagingParameters PagingParameters { get; set; }
        protected TG.BLL.ProjectValueLendBorrowParameters PagingParameters1 { get; set; }
     
        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.ProjectAllotPagingParameters parameters = null;
            TG.BLL.ProjectValueLendBorrowParameters parameters1 = null;
            string strWhere = "",unitname="",unitid="0";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];
            string year = context.Request.QueryString["year"];
            string unit= context.Request.QueryString["unit"];
            string keyname = context.Request.QueryString["keyname"];
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb1 = new StringBuilder("");
            StringBuilder sb2 = new StringBuilder("");   
            if (strOper != "del")
            {
                
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.ProjectAllotPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = ""
                };

                parameters1 = new TG.BLL.ProjectValueLendBorrowParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName3,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                unitname = unit;


                //名字不为空
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname.Trim());
                    sb.Append(" AND p.pro_Name LIKE '%" + keyname + "%'  ");
                }
                //绑定单位
                if (!string.IsNullOrEmpty(unitname))
                {
                    sb.Append(" AND p.Unit='" + unitname.Trim() + "' ");
                }
                //按照年份
                if (string.IsNullOrEmpty(year))
                {
                    year = DateTime.Now.Year.ToString();
                }
                //sb.AppendFormat(" AND year(p.pro_startTime)={0}", this.drp_year.SelectedValue);
                sb1.AppendFormat(" AND year(InAcountTime)={0}", year);
                sb2.AppendFormat(" AND ActualAllountTime='{0}'", year);

                sb.Append(strWhere);
                parameters.Query = sb.ToString();
                parameters.Query1 = sb1.ToString();
                parameters.Query2 = sb2.ToString();

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strOper == "del")//删除
            {

            }
            else if (strAction == "sel")
            {
                unitname = unit;
               
                //名字不为空
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname.Trim());
                    sb.Append(" AND p.pro_Name LIKE '%" + keyname + "%'  ");
                }
                //绑定单位
                if (!string.IsNullOrEmpty(unitname) && !unitname.Contains("全院部门"))
                {
                    sb.Append(" AND p.Unit='" + unitname.Trim() + "' ");
                }
                //按照年份
                if (!string.IsNullOrEmpty(year)&&year!="-1")
                {
                    //sb.AppendFormat(" AND year(p.pro_startTime)={0}", this.drp_year.SelectedValue);
                    sb1.AppendFormat(" AND year(InAcountTime)={0}", year);
                    sb2.AppendFormat(" AND ActualAllountTime='{0}'", year);
                }
               

                sb.Append(strWhere);
                parameters.Query = sb.ToString();
                parameters.Query1= sb1.ToString();
                parameters.Query2 = sb2.ToString();
                //查询
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "member")
            {

                unitid = unit;

                //绑定单位
                if (!string.IsNullOrEmpty(unitid) && unitid != "-1")
                {
                    sb.Append(" AND me.mem_Unit_ID=" + unitid.Trim() + "");
                }

                string name = context.Request.QueryString["name"];

                if (!string.IsNullOrEmpty(name))
                {
                    sb.Append(" AND me.mem_name like '%" + name + "%'");
                }
                sb.Append(strWhere);

                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters1.Year = year;
                }
                else
                {
                    parameters1.Year = "null";
                }


                parameters1.Where = sb.ToString();


                parameters1.ProcedureName = ProcName2;
                //查询
                this.PagingParameters1 = parameters1;


                context.Response.Write(GetJsonResult1());
            }
            else if (strAction == "member2")
            {
                unitid = unit;
                

                //绑定单位
                if (!string.IsNullOrEmpty(unitid)&&unitid != "-1")
                {
                    sb.Append(" AND me.mem_Unit_ID=" + unitid.Trim() + "");
                }

                string name = context.Request.QueryString["name"];

                if (!string.IsNullOrEmpty(name))
                {
                    sb.Append(" AND me.mem_name like '%"+name+"%'");
                }
                sb.Append(strWhere);

                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    parameters1.Year = year;
                }
                else
                {
                    parameters1.Year = "null";
                }


                parameters1.Where = sb.ToString();


                parameters1.ProcedureName = ProcName3;
                //查询
                this.PagingParameters1 = parameters1;

                context.Response.Write(GetJsonResult1());
            }

        }


        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public virtual string GetJsonResult1()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters1.ProcedureName, PagingParameters1.ProcedureParameters).Tables[0];


            int total = PagingParameters1.Total;
            int pageCount = total % PagingParameters1.PageSize == 0 ? total / PagingParameters1.PageSize : total / PagingParameters1.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters1.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}