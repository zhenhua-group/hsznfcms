﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;
using System.Data;

namespace TG.Web.HttpHandler.LeadershipCockpit
{
    /// <summary>
    /// ProjectCountBymasterHandler 的摘要说明
    /// </summary>
    public class ProjectCountBymasterHandler : HandlerCommon, IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            string action = Request["action"] ?? "";
            string drptype = Request["drptype"] ?? "0";
            string unitnamelist = Request["unitnamelist"] ?? "";
            string unitidlist = Request["unitidlist"] ?? "";
            if (action == "sel")
            {
                string timewhere = "";
                string syeartime = Request["drpyear1"] ?? "";
                string eyeartime = Request["drpyear2"] ?? "";
                string drpyear1 = "";
                string drpyear2 = "";

                StringBuilder sb = new StringBuilder();
                //部门id
                if (!string.IsNullOrEmpty(unitidlist) && unitidlist != "0" && unitidlist != "-1")
                {
                    sb.AppendFormat(" and ID in ({0})", unitidlist);
                }
                //年份
                if ((!string.IsNullOrEmpty(syeartime) && syeartime != "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                }
                else if ((!string.IsNullOrEmpty(eyeartime) && eyeartime != "-1") && (string.IsNullOrEmpty(syeartime) || syeartime == "-1"))
                {
                    drpyear2 = drpyear2 + "-12-31 23:59:59";
                }
                else if ((string.IsNullOrEmpty(syeartime) || syeartime == "-1") && (string.IsNullOrEmpty(eyeartime) || eyeartime == "-1"))
                {
                    drpyear1 = "";
                    drpyear2 = "";
                }
                else
                {
                    drpyear1 = syeartime + "-01-01 00:00:00";
                    drpyear2 = eyeartime + "-12-31 23:59:59";
                }
                if (!string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between '" + drpyear1 + "' and '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else if (!string.IsNullOrEmpty(drpyear1) && string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime > '" + drpyear1 + "' )";
                }
                else if (string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between < '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else
                {
                    timewhere = "(1=1)";
                }

                //项目审批
                if (drptype == "0")
                {
                    string projectcount1 = Request["projectcount1"] ?? "";
                    string projectcount2 = Request["projectcount2"] ?? "";
                    string projectarea1 = Request["projectarea1"] ?? "";
                    string projectarea2 = Request["projectarea2"] ?? "";
                    string projectauditcount1 = Request["projectauditcount1"] ?? "";
                    string projectauditcount2 = Request["projectauditcount2"] ?? "";
                    string projectingcount1 = Request["projectingcount1"] ?? "";
                    string projectingcount2 = Request["projectingcount2"] ?? "";
                    string projectstopcount1 = Request["projectstopcount1"] ?? "";
                    string projectstopcount2 = Request["projectstopcount2"] ?? "";
                    string projectfinishcount1 = Request["projectfinishcount1"] ?? "";
                    string projectfinishcount2 = Request["projectfinishcount2"] ?? "";
                    //项目总数
                    if (!string.IsNullOrEmpty(projectcount1) && string.IsNullOrEmpty(projectcount2))
                    {
                        sb.AppendFormat(" and A>={0}", projectcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectcount2) && string.IsNullOrEmpty(projectcount1))
                    {
                        sb.AppendFormat(" and A<={0}", projectcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectcount2) && !string.IsNullOrEmpty(projectcount1))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", projectcount1, projectcount2);
                    }
                    //项目规模
                    if (!string.IsNullOrEmpty(projectarea1) && string.IsNullOrEmpty(projectarea2))
                    {
                        sb.AppendFormat(" and B>={0}", projectarea1);
                    }
                    else if (!string.IsNullOrEmpty(projectarea2) && string.IsNullOrEmpty(projectarea1))
                    {
                        sb.AppendFormat(" and B<={0}", projectarea2);
                    }
                    else if (!string.IsNullOrEmpty(projectarea1) && !string.IsNullOrEmpty(projectarea2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", projectarea1, projectarea2);
                    }
                    //立项通过数
                    if (!string.IsNullOrEmpty(projectauditcount1) && string.IsNullOrEmpty(projectauditcount2))
                    {
                        sb.AppendFormat(" and C>={0}", projectauditcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectauditcount2) && string.IsNullOrEmpty(projectauditcount1))
                    {
                        sb.AppendFormat(" and C<={0}", projectauditcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectauditcount2) && !string.IsNullOrEmpty(projectauditcount1))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", projectauditcount1, projectauditcount2);
                    }
                    //进行中项目
                    if (!string.IsNullOrEmpty(projectingcount1) && string.IsNullOrEmpty(projectingcount2))
                    {
                        sb.AppendFormat(" and D>={0}", projectingcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectingcount2) && string.IsNullOrEmpty(projectingcount1))
                    {
                        sb.AppendFormat(" and D<={0}", projectingcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectingcount2) && !string.IsNullOrEmpty(projectingcount1))
                    {
                        sb.AppendFormat(" and D between {0} and {1}", projectingcount1, projectingcount2);
                    }
                    //暂停项目
                    if (!string.IsNullOrEmpty(projectstopcount1) && string.IsNullOrEmpty(projectstopcount2))
                    {
                        sb.AppendFormat(" and E>={0}", projectstopcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectstopcount2) && string.IsNullOrEmpty(projectstopcount1))
                    {
                        sb.AppendFormat(" and E<={0}", projectstopcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectstopcount2) && !string.IsNullOrEmpty(projectstopcount1))
                    {
                        sb.AppendFormat(" and E between {0} and {1}", projectstopcount1, projectstopcount2);
                    }
                    //完成项目
                    if (!string.IsNullOrEmpty(projectfinishcount1) && string.IsNullOrEmpty(projectfinishcount2))
                    {
                        sb.AppendFormat(" and F>={0}", projectfinishcount1);
                    }
                    else if (!string.IsNullOrEmpty(projectfinishcount2) && string.IsNullOrEmpty(projectfinishcount1))
                    {
                        sb.AppendFormat(" and F<={0}", projectfinishcount2);
                    }
                    else if (!string.IsNullOrEmpty(projectfinishcount2) && !string.IsNullOrEmpty(projectfinishcount1))
                    {
                        sb.AppendFormat(" and F between {0} and {1}", projectfinishcount1, projectfinishcount2);
                    }

                }
                //项目等级
                else if (drptype == "1")
                {
                    string levelcount_1 = Request["levelcount_1"] ?? "";
                    string levelcount_2 = Request["levelcount_2"] ?? "";
                    string levelcpr_1 = Request["levelcpr_1"] ?? "";
                    string levelcpr_2 = Request["levelcpr_2"] ?? "";
                    string levelcount1_1 = Request["levelcount1_1"] ?? "";
                    string levelcount1_2 = Request["levelcount1_2"] ?? "";
                    string levelcpr1_1 = Request["levelcpr1_1"] ?? "";
                    string levelcpr1_2 = Request["levelcpr1_2"] ?? "";
                    string levelcount2_1 = Request["levelcount2_1"] ?? "";
                    string levelcount2_2 = Request["levelcount2_2"] ?? "";
                    string levelcpr2_1 = Request["levelcpr2_1"] ?? "";
                    string levelcpr2_2 = Request["levelcpr2_2"] ?? "";
                    string levelcount3_1 = Request["levelcount3_1"] ?? "";
                    string levelcount3_2 = Request["levelcount3_2"] ?? "";
                    string levelcpr3_1 = Request["levelcpr3_1"] ?? "";
                    string levelcpr3_2 = Request["levelcpr3_2"] ?? "";

                    //特级数量
                    if (!string.IsNullOrEmpty(levelcount_1) && string.IsNullOrEmpty(levelcount_2))
                    {
                        sb.AppendFormat(" and A>={0}", levelcount_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount_2) && string.IsNullOrEmpty(levelcount_1))
                    {
                        sb.AppendFormat(" and A<={0}", levelcount_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcount_1) && !string.IsNullOrEmpty(levelcount_2))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", levelcount_1, levelcount_2);
                    }
                    //特级合同额
                    if (!string.IsNullOrEmpty(levelcpr_1) && string.IsNullOrEmpty(levelcpr_2))
                    {
                        sb.AppendFormat(" and A1>={0}", levelcpr_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr_2) && string.IsNullOrEmpty(levelcpr_1))
                    {
                        sb.AppendFormat(" and A1<={0}", levelcpr_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr_1) && !string.IsNullOrEmpty(levelcpr_2))
                    {
                        sb.AppendFormat(" and A1 between {0} and {1}", levelcpr_1, levelcpr_2);
                    }
                    //一级数量
                    if (!string.IsNullOrEmpty(levelcount1_1) && string.IsNullOrEmpty(levelcount1_2))
                    {
                        sb.AppendFormat(" and B>={0}", levelcount1_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount1_2) && string.IsNullOrEmpty(levelcount1_1))
                    {
                        sb.AppendFormat(" and B<={0}", levelcount1_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcount1_1) && !string.IsNullOrEmpty(levelcount1_2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", levelcount1_1, levelcount1_2);
                    }
                    //一级合同额
                    if (!string.IsNullOrEmpty(levelcpr1_1) && string.IsNullOrEmpty(levelcpr1_2))
                    {
                        sb.AppendFormat(" and B1>={0}", levelcpr1_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr1_2) && string.IsNullOrEmpty(levelcpr1_1))
                    {
                        sb.AppendFormat(" and B1<={0}", levelcpr1_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr1_1) && !string.IsNullOrEmpty(levelcpr1_2))
                    {
                        sb.AppendFormat(" and B1 between {0} and {1}", levelcpr1_1, levelcpr1_2);
                    }
                    //二级数量
                    if (!string.IsNullOrEmpty(levelcount2_1) && string.IsNullOrEmpty(levelcount2_2))
                    {
                        sb.AppendFormat(" and C>={0}", levelcount2_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount2_2) && string.IsNullOrEmpty(levelcount2_1))
                    {
                        sb.AppendFormat(" and C<={0}", levelcount2_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcount2_1) && !string.IsNullOrEmpty(levelcount2_2))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", levelcount2_1, levelcount2_2);
                    }
                    //二级合同额
                    if (!string.IsNullOrEmpty(levelcpr2_1) && string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and C1>={0}", levelcpr2_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_2) && string.IsNullOrEmpty(levelcpr2_1))
                    {
                        sb.AppendFormat(" and C1<={0}", levelcpr2_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_1) && !string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and C1 between {0} and {1}", levelcpr2_1, levelcpr2_2);
                    }
                    //三级数量
                    if (!string.IsNullOrEmpty(levelcount3_1) && string.IsNullOrEmpty(levelcount3_2))
                    {
                        sb.AppendFormat(" and D>={0}", levelcount3_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcount3_2) && string.IsNullOrEmpty(levelcount3_1))
                    {
                        sb.AppendFormat(" and D<={0}", levelcount3_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcount3_1) && !string.IsNullOrEmpty(levelcount3_2))
                    {
                        sb.AppendFormat(" and D between {0} and {1}", levelcount3_1, levelcount3_2);
                    }
                    //三级合同额
                    if (!string.IsNullOrEmpty(levelcpr2_1) && string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and D1>={0}", levelcpr2_1);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_2) && string.IsNullOrEmpty(levelcpr2_1))
                    {
                        sb.AppendFormat(" and D1<={0}", levelcpr2_2);
                    }
                    else if (!string.IsNullOrEmpty(levelcpr2_1) && !string.IsNullOrEmpty(levelcpr2_2))
                    {
                        sb.AppendFormat(" and D1 between {0} and {1}", levelcpr2_1, levelcpr2_2);
                    }
                }
                //项目建筑性质
                else if (drptype == "2" || drptype == "3")
                {
                    string txt_gj1 = Request["txt_gj1"] ?? "";
                    string txt_gj2 = Request["txt_gj2"] ?? "";
                    string txt_fdc1 = Request["txt_fdc1"] ?? "";
                    string txt_fdc2 = Request["txt_fdc2"] ?? "";
                    string txt_sz1 = Request["txt_sz1"] ?? "";
                    string txt_sz2 = Request["txt_sz2"] ?? "";
                    string txt_yy1 = Request["txt_yy1"] ?? "";
                    string txt_yy2 = Request["txt_yy2"] ?? "";
                    string txt_dl1 = Request["txt_dl1"] ?? "";
                    string txt_dl2 = Request["txt_dl2"] ?? "";
                    string txt_tx1 = Request["txt_tx1"] ?? "";
                    string txt_tx2 = Request["txt_tx2"] ?? "";
                    string txt_yh1 = Request["txt_yh1"] ?? "";
                    string txt_yh2 = Request["txt_yh2"] ?? "";
                    string txt_xx1 = Request["txt_xx1"] ?? "";
                    string txt_xx2 = Request["txt_xx2"] ?? "";
                    string txt_sw1 = Request["txt_sw1"] ?? "";
                    string txt_sw2 = Request["txt_sw2"] ?? "";
                    //公建
                    if (!string.IsNullOrEmpty(txt_gj1) && string.IsNullOrEmpty(txt_gj2))
                    {
                        sb.AppendFormat(" and A>={0}", txt_gj1);
                    }
                    else if (!string.IsNullOrEmpty(txt_gj2) && string.IsNullOrEmpty(txt_gj1))
                    {
                        sb.AppendFormat(" and A<={0}", txt_gj2);
                    }
                    else if (!string.IsNullOrEmpty(txt_gj1) && !string.IsNullOrEmpty(txt_gj2))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", txt_gj1, txt_gj2);
                    }
                    //房地产
                    if (!string.IsNullOrEmpty(txt_fdc1) && string.IsNullOrEmpty(txt_fdc2))
                    {
                        sb.AppendFormat(" and B>={0}", txt_fdc1);
                    }
                    else if (!string.IsNullOrEmpty(txt_fdc2) && string.IsNullOrEmpty(txt_fdc1))
                    {
                        sb.AppendFormat(" and B<={0}", txt_fdc2);
                    }
                    else if (!string.IsNullOrEmpty(txt_fdc1) && !string.IsNullOrEmpty(txt_fdc2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", txt_fdc1, txt_fdc2);
                    }
                    //市政
                    if (!string.IsNullOrEmpty(txt_sz1) && string.IsNullOrEmpty(txt_sz2))
                    {
                        sb.AppendFormat(" and C>={0}", txt_sz1);
                    }
                    else if (!string.IsNullOrEmpty(txt_sz2) && string.IsNullOrEmpty(txt_sz1))
                    {
                        sb.AppendFormat(" and C<={0}", txt_sz2);
                    }
                    else if (!string.IsNullOrEmpty(txt_sz1) && !string.IsNullOrEmpty(txt_sz2))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", txt_sz1, txt_sz2);
                    }
                    //医院
                    if (!string.IsNullOrEmpty(txt_yy1) && string.IsNullOrEmpty(txt_yy2))
                    {
                        sb.AppendFormat(" and D>={0}", txt_yy1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yy2) && string.IsNullOrEmpty(txt_yy1))
                    {
                        sb.AppendFormat(" and D<={0}", txt_yy2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yy1) && !string.IsNullOrEmpty(txt_yy2))
                    {
                        sb.AppendFormat(" and D between {0} and {1}", txt_yy1, txt_yy2);
                    }
                    //电力
                    if (!string.IsNullOrEmpty(txt_dl1) && string.IsNullOrEmpty(txt_dl2))
                    {
                        sb.AppendFormat(" and E>={0}", txt_dl1);
                    }
                    else if (!string.IsNullOrEmpty(txt_dl2) && string.IsNullOrEmpty(txt_dl1))
                    {
                        sb.AppendFormat(" and E<={0}", txt_dl2);
                    }
                    else if (!string.IsNullOrEmpty(txt_dl1) && !string.IsNullOrEmpty(txt_dl2))
                    {
                        sb.AppendFormat(" and E between {0} and {1}", txt_dl1, txt_dl2);
                    }
                    //通信
                    if (!string.IsNullOrEmpty(txt_tx1) && string.IsNullOrEmpty(txt_tx2))
                    {
                        sb.AppendFormat(" and F>={0}", txt_tx1);
                    }
                    else if (!string.IsNullOrEmpty(txt_tx2) && string.IsNullOrEmpty(txt_tx1))
                    {
                        sb.AppendFormat(" and F<={0}", txt_tx2);
                    }
                    else if (!string.IsNullOrEmpty(txt_tx1) && !string.IsNullOrEmpty(txt_tx2))
                    {
                        sb.AppendFormat(" and F between {0} and {1}", txt_tx1, txt_tx2);
                    }
                    //银行
                    if (!string.IsNullOrEmpty(txt_yh1) && string.IsNullOrEmpty(txt_yh2))
                    {
                        sb.AppendFormat(" and G>={0}", txt_tx1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yh2) && string.IsNullOrEmpty(txt_yh1))
                    {
                        sb.AppendFormat(" and G<={0}", txt_yh2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yh1) && !string.IsNullOrEmpty(txt_yh2))
                    {
                        sb.AppendFormat(" and G between {0} and {1}", txt_yh1, txt_yh2);
                    }
                    //学校
                    if (!string.IsNullOrEmpty(txt_xx1) && string.IsNullOrEmpty(txt_xx2))
                    {
                        sb.AppendFormat(" and H>={0}", txt_xx1);
                    }
                    else if (!string.IsNullOrEmpty(txt_xx2) && string.IsNullOrEmpty(txt_xx1))
                    {
                        sb.AppendFormat(" and H<={0}", txt_xx2);
                    }
                    else if (!string.IsNullOrEmpty(txt_xx1) && !string.IsNullOrEmpty(txt_xx2))
                    {
                        sb.AppendFormat(" and H between {0} and {1}", txt_xx1, txt_xx2);
                    }
                    //涉外 
                    if (!string.IsNullOrEmpty(txt_sw1) && string.IsNullOrEmpty(txt_sw2))
                    {
                        sb.AppendFormat(" and I>={0}", txt_sw1);
                    }
                    else if (!string.IsNullOrEmpty(txt_sw2) && string.IsNullOrEmpty(txt_sw1))
                    {
                        sb.AppendFormat(" and I<={0}", txt_sw2);
                    }
                    else if (!string.IsNullOrEmpty(txt_sw1) && !string.IsNullOrEmpty(txt_sw2))
                    {
                        sb.AppendFormat(" and I between {0} and {1}", txt_sw1, txt_sw2);
                    }

                }
                //项目来源
                else if (drptype == "4")
                {
                    string txt_gkzbcount1 = Request["txt_gkzbcount1"] ?? "";
                    string txt_gkzbcount2 = Request["txt_gkzbcount2"] ?? "";
                    string txt_gkzbscale1 = Request["txt_gkzbscale1"] ?? "";
                    string txt_gkzbscale2 = Request["txt_gkzbscale2"] ?? "";
                    string txt_yqzbcount1 = Request["txt_yqzbcount1"] ?? "";
                    string txt_yqzbcount2 = Request["txt_yqzbcount2"] ?? "";
                    string txt_yqzbscale1 = Request["txt_yqzbscale1"] ?? "";
                    string txt_yqzbscale2 = Request["txt_yqzbscale2"] ?? "";
                    string txt_zxwtcount1 = Request["txt_zxwtcount1"] ?? "";
                    string txt_zxwtcount2 = Request["txt_zxwtcount2"] ?? "";
                    string txt_zxwtscale1 = Request["txt_zxwtscale1"] ?? "";
                    string txt_zxwtscale2 = Request["txt_zxwtscale2"] ?? "";
                    //项目总数
                    if (!string.IsNullOrEmpty(txt_gkzbcount1) && string.IsNullOrEmpty(txt_gkzbcount2))
                    {
                        sb.AppendFormat(" and A>={0}", txt_gkzbcount1);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbcount2) && string.IsNullOrEmpty(txt_gkzbcount1))
                    {
                        sb.AppendFormat(" and A<={0}", txt_gkzbcount2);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbcount1) && !string.IsNullOrEmpty(txt_gkzbcount2))
                    {
                        sb.AppendFormat(" and A between {0} and {1}", txt_gkzbcount1, txt_gkzbcount2);
                    }
                    //项目规模
                    if (!string.IsNullOrEmpty(txt_gkzbscale1) && string.IsNullOrEmpty(txt_gkzbscale2))
                    {
                        sb.AppendFormat(" and A1>={0}", txt_gkzbscale1);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbscale2) && string.IsNullOrEmpty(txt_gkzbscale1))
                    {
                        sb.AppendFormat(" and A1<={0}", txt_gkzbscale2);
                    }
                    else if (!string.IsNullOrEmpty(txt_gkzbscale1) && !string.IsNullOrEmpty(txt_gkzbscale2))
                    {
                        sb.AppendFormat(" and A1 between {0} and {1}", txt_gkzbscale1, txt_gkzbscale2);
                    }
                    //立项通过数
                    if (!string.IsNullOrEmpty(txt_yqzbcount1) && string.IsNullOrEmpty(txt_yqzbcount2))
                    {
                        sb.AppendFormat(" and B>={0}", txt_yqzbcount1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbcount2) && string.IsNullOrEmpty(txt_yqzbcount1))
                    {
                        sb.AppendFormat(" and B<={0}", txt_yqzbcount2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbcount1) && !string.IsNullOrEmpty(txt_yqzbcount2))
                    {
                        sb.AppendFormat(" and B between {0} and {1}", txt_yqzbcount1, txt_yqzbcount2);
                    }
                    //进行中项目
                    if (!string.IsNullOrEmpty(txt_yqzbscale1) && string.IsNullOrEmpty(txt_yqzbscale2))
                    {
                        sb.AppendFormat(" and B1>={0}", txt_yqzbscale1);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbscale2) && string.IsNullOrEmpty(txt_yqzbscale1))
                    {
                        sb.AppendFormat(" and B1<={0}", txt_yqzbscale2);
                    }
                    else if (!string.IsNullOrEmpty(txt_yqzbscale1) && !string.IsNullOrEmpty(txt_yqzbscale2))
                    {
                        sb.AppendFormat(" and B1 between {0} and {1}", txt_yqzbscale1, txt_yqzbscale2);
                    }
                    //暂停项目
                    if (!string.IsNullOrEmpty(txt_zxwtcount1) && string.IsNullOrEmpty(txt_zxwtcount2))
                    {
                        sb.AppendFormat(" and C>={0}", txt_zxwtcount1);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtcount2) && string.IsNullOrEmpty(txt_zxwtcount1))
                    {
                        sb.AppendFormat(" and C<={0}", txt_zxwtcount2);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtcount1) && !string.IsNullOrEmpty(txt_zxwtcount2))
                    {
                        sb.AppendFormat(" and C between {0} and {1}", txt_zxwtcount1, txt_zxwtcount2);
                    }
                    //完成项目
                    if (!string.IsNullOrEmpty(txt_zxwtscale1) && string.IsNullOrEmpty(txt_zxwtscale2))
                    {
                        sb.AppendFormat(" and C1>={0}", txt_zxwtscale1);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtscale2) && string.IsNullOrEmpty(txt_zxwtscale1))
                    {
                        sb.AppendFormat(" and C1<={0}", txt_zxwtscale2);
                    }
                    else if (!string.IsNullOrEmpty(txt_zxwtscale1) && !string.IsNullOrEmpty(txt_zxwtscale2))
                    {
                        sb.AppendFormat(" and C1 between {0} and {1}", txt_zxwtscale1, txt_zxwtscale2);
                    }

                }

                DataTable dt = new TG.BLL.cm_Project().CountProjectByCustom(timewhere, drptype, sb.ToString()).Tables[0];
                context.Response.Write(TG.Web.Common.JsonHelper.Serialize(dt));
            }
            else
            {
                string timewhere = "";
                string drpyear = Request["drpyear"] ?? "";
                string drpyear1 = "";
                string drpjd = Request["drpjd"] ?? "";
                string drpmth = Request["drpmth"] ?? "";
                string txt_year1 = Request["txt_year1"] ?? "";
                string txt_year2 = Request["txt_year2"] ?? "";
                string ischeck = Request["ischeck"] ?? "";
                string drpyear2 = "";
                StringBuilder sb = new StringBuilder();
                //部门名称
                if (!string.IsNullOrEmpty(unitnamelist) && !unitnamelist.Contains("全院部门"))
                {
                    sb.AppendFormat(" and Name in ({0})", unitnamelist);
                }
                //部门id
                if (!string.IsNullOrEmpty(unitidlist) && unitidlist != "0")
                {
                    sb.AppendFormat(" and ID in ({0})", unitidlist);
                }
                if (ischeck == "1")
                {
                    if (!string.IsNullOrEmpty(txt_year1) && !string.IsNullOrEmpty(txt_year2))
                    {

                        drpyear1 = txt_year1;
                        drpyear2 = txt_year2;
                    }
                    else if (!string.IsNullOrEmpty(txt_year1) && string.IsNullOrEmpty(txt_year2))
                    {
                        drpyear1 = txt_year1;
                        drpyear2 = "";
                    }
                    else if (string.IsNullOrEmpty(txt_year1) && !string.IsNullOrEmpty(txt_year2))
                    {
                        drpyear1 = "";
                        drpyear2 = txt_year2;
                    }
                    else
                    {
                        drpyear1 = "";
                        drpyear2 = "";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(drpyear) && drpyear != "-1")
                    {
                        if (drpjd == "0" && drpmth == "0")
                        {
                            drpyear1 = drpyear + "-01-01 00:00:00";
                            drpyear2 = drpyear + "-12-31 23:59:59 ";
                        }
                        else if (drpjd != "0" && drpmth == "0") //年季度
                        {
                            drpyear1 = drpyear;
                            drpyear2 = drpyear;
                            switch (drpjd)
                            {
                                case "1":
                                    drpyear1 += "-01-01 00:00:00";
                                    drpyear2 += "-03-31 23:59:59";
                                    break;
                                case "2":
                                    drpyear1 += "-04-01 00:00:00";
                                    drpyear2 += "-06-30 23:59:59";
                                    break;
                                case "3":
                                    drpyear1 += "-07-01 00:00:00";
                                    drpyear2 += "-09-30 23:59:59";
                                    break;
                                case "4":
                                    drpyear1 += "-10-01 00:00:00";
                                    drpyear2 += "-12-31 23:59:59";
                                    break;
                            }
                        }
                        else if (drpjd == "0" && drpmth != "0")//年月份
                        {
                            //当月有几天
                            int days = DateTime.DaysInMonth(int.Parse(drpyear), int.Parse(drpmth));
                            drpyear1 = drpyear + "-" + drpmth + "-01 00:00:00";
                            drpyear2 = drpyear + "-" + drpmth + "-" + days + " 23:59:59";
                        }
                        else
                        {
                            drpyear1 = "null";
                            drpyear2 = "null";
                        }
                    }
                }
                ////年份
                if (!string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between '" + drpyear1 + "' and '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else if (!string.IsNullOrEmpty(drpyear1) && string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime > '" + drpyear1 + "' )";
                }
                else if (string.IsNullOrEmpty(drpyear1) && !string.IsNullOrEmpty(drpyear2))
                {
                    timewhere = "(pro_startTime between < '" + drpyear2 + "') or ('" + drpyear2 + "' is null)";
                }
                else
                {
                    timewhere = "(1=1)";
                }

                DataTable dt = new TG.BLL.cm_Project().CountProjectByCustom(timewhere, drptype, sb.ToString()).Tables[0];
                context.Response.Write(TG.Web.Common.JsonHelper.Serialize(dt));
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}