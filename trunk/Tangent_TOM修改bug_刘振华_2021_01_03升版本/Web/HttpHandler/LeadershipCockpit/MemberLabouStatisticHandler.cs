﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;

namespace TG.Web.HttpHandler.LeadershipCockpit
{
    public abstract class MemberLabouStatisticHandler : IHttpHandler
    {
        public abstract string ProcName { get; }

        //存储过程参数
        protected TG.BLL.QueryPagingMeberParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingMeberParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
            parameters = new TG.BLL.QueryPagingMeberParameters
            {
                PageIndex = int.Parse(context.Request["PageIndex"]),
                PageSize = int.Parse(context.Request["PageSize"]),
                OrderBy = context.Request["OrderBy"],
                Sort = context.Request["Sort"],
                Where = strWhere,
                ProcedureName = ProcName
            };


            //加载数据
            if (strOper == null && strAction == null)
            {
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strAction == "statistic")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);

                string unit = context.Request.QueryString["unit"];
                string keyname = context.Request.QueryString["keyname"];
                string spename = context.Request.QueryString["spename"];
                string signdate = context.Request.QueryString["signdate"];
                string finishdate = context.Request.QueryString["finishdate"];
                string projectCount1 = context.Request.QueryString["projectCount1"];
                string projectCount2 = context.Request.QueryString["projectCount2"];
                string jinXingCount1 = context.Request.QueryString["jinXingCount1"];
                string jinXingCount2 = context.Request.QueryString["jinXingCount2"];
                string zanTingCount1 = context.Request.QueryString["zanTingCount1"];
                string zanTingCount2 = context.Request.QueryString["zanTingCount2"];
                string wangongCount1 = context.Request.QueryString["wangongCount1"];
                string wangongCount2 = context.Request.QueryString["wangongCount2"];

                StringBuilder strsql = new StringBuilder("");
                StringBuilder strDate = new StringBuilder("");

                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND mem_name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (unit != "-1" && unit != null && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND unit_name= '{0}'", unit);
                }

                //专业 
                if (!string.IsNullOrEmpty(spename))
                {
                    strsql.Append(" AND spe_Name LIKE '%" + spename + "%' ");
                }

                //项目开始日期

                if (!string.IsNullOrEmpty(signdate) && string.IsNullOrEmpty(finishdate))
                {
                    strDate.Append(" AND pro_StartTime >= '" + signdate.Trim() + "' ");
                }
                else if (string.IsNullOrEmpty(signdate) && !string.IsNullOrEmpty(finishdate))
                {
                    strDate.Append(" AND pro_StartTime <= '" + finishdate.Trim() + " '");
                }
                else if (!string.IsNullOrEmpty(signdate) && !string.IsNullOrEmpty(finishdate))
                {
                    strDate.Append(" AND ( pro_StartTime>='" + signdate.Trim() + "' AND pro_StartTime <= '" + finishdate.Trim() + "') ");
                }


                //项目总数
                if (!string.IsNullOrEmpty(projectCount1) && string.IsNullOrEmpty(projectCount2))
                {
                    strsql.Append(" AND TotalCount >= " + projectCount1.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(projectCount1) && !string.IsNullOrEmpty(projectCount2))
                {
                    strsql.Append(" AND TotalCount <= " + projectCount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(projectCount1) && !string.IsNullOrEmpty(projectCount2))
                {
                    strsql.Append(" AND (TotalCount>=" + projectCount1.Trim() + " AND TotalCount <= " + projectCount2.Trim() + ") ");
                }

                //进行中总数
                if (!string.IsNullOrEmpty(jinXingCount1) && string.IsNullOrEmpty(jinXingCount2))
                {
                    strsql.Append(" AND JinXingCount >= " + jinXingCount1.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(jinXingCount1) && !string.IsNullOrEmpty(jinXingCount2))
                {
                    strsql.Append(" AND JinXingCount <= " + jinXingCount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(jinXingCount1) && !string.IsNullOrEmpty(jinXingCount2))
                {
                    strsql.Append(" AND (JinXingCount>=" + jinXingCount1.Trim() + " AND JinXingCount <= " + jinXingCount2.Trim() + ") ");
                }

                //暂停中总数
                if (!string.IsNullOrEmpty(zanTingCount1) && string.IsNullOrEmpty(zanTingCount2))
                {
                    strsql.Append(" AND ZantingCount >= " + zanTingCount1.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(zanTingCount1) && !string.IsNullOrEmpty(zanTingCount2))
                {
                    strsql.Append(" AND ZantingCount <= " + zanTingCount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(zanTingCount1) && !string.IsNullOrEmpty(zanTingCount2))
                {
                    strsql.Append(" AND (ZantingCount>=" + zanTingCount1.Trim() + " AND ZantingCount <= " + zanTingCount2.Trim() + ") ");
                }

                //完工总数
                if (!string.IsNullOrEmpty(wangongCount1) && string.IsNullOrEmpty(wangongCount2))
                {
                    strsql.Append(" AND WanGongCount >= " + wangongCount1.Trim() + " ");
                }
                else if (string.IsNullOrEmpty(wangongCount1) && !string.IsNullOrEmpty(wangongCount2))
                {
                    strsql.Append(" AND WanGongCount <= " + wangongCount2.Trim() + " ");
                }
                else if (!string.IsNullOrEmpty(wangongCount1) && !string.IsNullOrEmpty(wangongCount2))
                {
                    strsql.Append(" AND (WanGongCount>=" + wangongCount1.Trim() + " AND WanGongCount <= " + wangongCount2.Trim() + ") ");
                }

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                parameters.ProjectDate = strDate.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }

        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];

            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}