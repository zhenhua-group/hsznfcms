﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Collections;
namespace TG.Web.HttpHandler.LeadershipCockpit
{
    public abstract class MemberLabouStatisticDetailHandler : IHttpHandler
    {
        public abstract string ProcName2 { get; }

        //存储过程参数
        protected TG.BLL.QueryPagingLabouStatisticDetailParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingLabouStatisticDetailParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];


            strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
            parameters = new TG.BLL.QueryPagingLabouStatisticDetailParameters
            {
                PageIndex = int.Parse(context.Request["PageIndex"]),
                PageSize = int.Parse(context.Request["PageSize"]),
                OrderBy = context.Request["OrderBy"],
                Sort = context.Request["Sort"],
                Where = strWhere,
                ProcedureName = ProcName2
            };


            //加载数据
            if (strOper == null && strAction == null)
            {
                string mem_id = context.Request.QueryString["mem_id"];
                parameters.memID = mem_id;
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "search")//查询
            {

            }
            else if (strAction == "statisticDetail")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                string mem_id = context.Request.QueryString["mem_id"];
                string strTempWhere = "";
                if (strWhere.Trim() == "0")
                {
                    strTempWhere = " AND mem_id=" + mem_id+ "";
                }
                if (strWhere.Trim() == "1")
                {
                    strTempWhere = "  AND mem_id=" + mem_id + "  AND ( pro_Status='进行中' OR  pro_Status='')";
                }
                else if (strWhere.Trim() == "2")
                {
                    strTempWhere = "  AND mem_id=" + mem_id + "  AND pro_Status='完成'";
                }
                else if (strWhere.Trim() == "3")
                {
                    strTempWhere = "  AND mem_id=" + mem_id + "  AND pro_Status='暂停'";
                }
                parameters.Where = strTempWhere;
                parameters.memID = mem_id;
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }

        }

        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];

            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}