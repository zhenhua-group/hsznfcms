﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using TG.Model;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace TG.Web.HttpHandler.LeadershipCockpit
{
    /// <summary>
    /// AddProjectValueAllotHandler 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class CoperationCountBymasterHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {

        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string year = context.Request.Params["year"];
            string startyear = context.Request.Params["startyear"];
            string endyear = context.Request.Params["endyear"];
            string hidtime=context.Request.Params["hidtime"];
            string unit = context.Request.Params["unit"];
            string userUnit = context.Request.Params["userUnit"];
            string hidPower = context.Request.Params["hidPower"];
            string start = context.Request.Params["start"];
            string end = context.Request.Params["end"];
            string type = context.Request.Params["type"];
            string status = context.Request.Params["status"];
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();

            //创建时间查询条件
            string ChargeStartTime = "";
            string ChargeEndTime = "";
            string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            if (start == "")
            {
                start = Convert.ToDateTime(curtime).Year+"-01-01";
            }
            if (end == "")
            {
                end = Convert.ToDateTime(curtime).Year + "-12-31";
            }           

            ChargeStartTime = start;
            ChargeEndTime = end;         


            if (Action == "search")
            {
                DataTable dt = new DataTable();

                //年份
                string tempYear =DateTime.Now.Year.ToString();
                //基本年份条件
                string sqlwhere = "";

                //合同年份时间段
                if (hidtime == "1")
                {
                    if (!string.IsNullOrEmpty(startyear))
                    {
                        sqlwhere = " and cpr_SignDate>='" + startyear + "'";
                        tempYear = Convert.ToDateTime(startyear).Year.ToString();
                    }
                    if (!string.IsNullOrEmpty(endyear))
                    {
                        sqlwhere = " and cpr_SignDate<='" + endyear + "'";
                        if (string.IsNullOrEmpty(startyear))
                        {
                            tempYear = Convert.ToDateTime(endyear).Year.ToString();
                        }
                    }                    
                }
                else
                {
                     tempYear = getYear(year);
                    //不是全年份
                    if (!string.IsNullOrEmpty(year) && year != "-1")
                    {
                        if (string.IsNullOrEmpty(startyear))
                        {
                            startyear = year + "-01-01 00:00:00"; ;
                        }
                       
                        if (string.IsNullOrEmpty(endyear))
                        {
                            endyear = year + "-12-31 23:59:59";
                        }
                    }
                    else
                    {
                        startyear = tempYear + "-01-01 00:00:00";
                        endyear=tempYear + "-12-31 23:59:59";
                    }
                    if (!string.IsNullOrEmpty(startyear))
                    {
                        sqlwhere = " and cpr_SignDate>='" + startyear + "'";
                    }
                    if (!string.IsNullOrEmpty(endyear))
                    {
                        sqlwhere = sqlwhere+" and cpr_SignDate<='" + endyear + "'";
                    }

                }

                //合同目标值
                string cprTaretAcount1 = context.Request.Params["cprTaretAcount1"];
                string cprTaretAcount2 = context.Request.Params["cprTaretAcount2"];
                //完成合同额
                string cprAcount1 = context.Request.Params["cprAcount1"];
                string cprAcount2 = context.Request.Params["cprAcount2"];
                //合同额完成比例
                string cprAcountPercent1 = context.Request.Params["cprAcountPercent1"];
                string cprAcountPercent2 = context.Request.Params["cprAcountPercent2"];
                //目标产值
                string cprTarentCharge1 = context.Request.Params["cprTarentCharge1"];
                string cprTarentCharge2 = context.Request.Params["cprTarentCharge2"];
                //完成产值
                string cprCharge1 = context.Request.Params["cprCharge1"];
                string cprCharge2 = context.Request.Params["cprCharge2"];
                //完成产值比例
                string cprChargePercent1 = context.Request.Params["cprChargePercent1"];
                string cprChargePercent2 = context.Request.Params["cprChargePercent2"];

                //查看
                string strWhere ="";
                //高级查询
                if (status!="0")
                {
                    strWhere = getCountCoperationTargetWhere(cprTaretAcount1, cprTaretAcount2, cprAcount1, cprAcount2, cprAcountPercent1, cprAcountPercent2, cprTarentCharge1, cprTarentCharge2, cprCharge1, cprCharge2, cprChargePercent1, cprChargePercent2);
                }                

                string strUnit = getUnitId(hidPower, unit, userUnit, status);
                strWhere = strWhere + strUnit;

                dt = bll.CountCoperationTargetBySql(tempYear, sqlwhere, ChargeStartTime, ChargeEndTime, strWhere).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
            }
            else if (Action == "search1")
            {
                DataTable dt = new DataTable();

                //承接部门
                int? unitId;

                //已签合同数
                string cprCount1 = context.Request.Params["cprCount1"];
                string cprCount2 = context.Request.Params["cprCount2"];

                //签订合同额
                string cprSignAcount1 = context.Request.Params["cprSignAcount1"];
                string cprSignAcount2 = context.Request.Params["cprSignAcount2"];

                //建筑规模
                string bulid1 = context.Request.Params["bulid1"];
                string bulid2 = context.Request.Params["bulid2"];

                //收款金额
                string chargeCount1 = context.Request.Params["chargeCount1"];
                string chargeCount2 = context.Request.Params["chargeCount2"];

                //审批听过合同数
                string passCount1 = context.Request.Params["passCount1"];
                string passCount2 = context.Request.Params["passCount2"];

                //正在审批合同数
                string passingCount1 = context.Request.Params["passingCount1"];
                string passingCount2 = context.Request.Params["passingCount2"];

                //完成产值比例
                string notPassingCount1 = context.Request.Params["notPassingCount1"];
                string notPassingCount2 = context.Request.Params["notPassingCount2"];

                //合同区间值
                string startTime = context.Request.Params["startTime"];
                string endTime = context.Request.Params["endTime"];

                //查看
                string strWhere = "";
                //高级查询
                if (status != "0")                
                {               
                   strWhere = getCountCoperationTargetWhere1(cprCount1, cprCount2, cprSignAcount1, cprSignAcount2, bulid1, bulid2, chargeCount1, chargeCount2, passCount1, passCount2, passingCount1, passingCount2, notPassingCount1, notPassingCount2);
                }

                string strUnit = getUnitId(hidPower, unit, userUnit, status);
                strWhere = strWhere + strUnit;

                //年份
                string tempYear = yearWhere(status, year, startyear, endyear, hidtime, startTime, endTime);

                dt = bll.CountCoperationAuditBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
            }
            else if (Action == "search2")
            {
                DataTable dt = new DataTable();

                //承接部门
                int? unitId;

                //特级数量
                string tJCount1 = context.Request.Params["tJCount1"];
                string tJCount2 = context.Request.Params["tJCount2"];

                //特级合同额
                string tJCprAcount1 = context.Request.Params["tJCprAcount1"];
                string tJCprAcount2 = context.Request.Params["tJCprAcount2"];

                //特级收款额
                string tJCprChargeAcount1 = context.Request.Params["tJCprChargeAcount1"];
                string tJCprChargeAcount2 = context.Request.Params["tJCprChargeAcount2"];

                //一级数量
                string yJCount1 = context.Request.Params["yJCount1"];
                string yJCount2 = context.Request.Params["yJCount2"];

                //一级合同额
                string yJCprAcount1 = context.Request.Params["yJCprAcount1"];
                string yJCprAcount2 = context.Request.Params["yJCprAcount2"];

                //一级收款额
                string yJCprChargeAcount1 = context.Request.Params["yJCprChargeAcount1"];
                string yJCprChargeAcount2 = context.Request.Params["yJCprChargeAcount2"];


                //二级数量
                string eJCount1 = context.Request.Params["eJCount1"];
                string eJCount2 = context.Request.Params["eJCount2"];

                //二级合同额
                string eJCprAcount1 = context.Request.Params["eJCprAcount1"];
                string eJCprAcount2 = context.Request.Params["eJCprAcount2"];

                //二级收款额
                string eJCprChargeAcount1 = context.Request.Params["eJCprChargeAcount1"];
                string eJCprChargeAcount2 = context.Request.Params["eJCprChargeAcount2"];

                //三级数量
                string sJCount1 = context.Request.Params["sJCount1"];
                string sJCount2 = context.Request.Params["sJCount2"];

                //三级合同额
                string sJCprAcount1 = context.Request.Params["sJCprAcount1"];
                string sJCprAcount2 = context.Request.Params["sJCprAcount2"];

                //三级收款额
                string sJCprChargeAcount1 = context.Request.Params["sJCprChargeAcount1"];
                string sJCprChargeAcount2 = context.Request.Params["sJCprChargeAcount2"];

                //合同区间值
                string startTime = context.Request.Params["startTime"];
                string endTime = context.Request.Params["endTime"];


                //查看
                string strWhere = "";
                //高级查询
                if (status!="0")
                {                
                 strWhere = getCountCoperationTargetWhere2(tJCount1, tJCount2, tJCprAcount1, tJCprAcount2, tJCprChargeAcount1, tJCprChargeAcount2, yJCount1, yJCount2, yJCprAcount1, yJCprAcount2, yJCprChargeAcount1, yJCprChargeAcount2, eJCount1, eJCount2, eJCprAcount1, eJCprAcount2, eJCprChargeAcount1, eJCprChargeAcount2, sJCount1, sJCount2, sJCprAcount1, sJCprAcount2, sJCprChargeAcount1, sJCprChargeAcount2);
                }
                string strUnit = getUnitId(hidPower, unit, userUnit, status);
                strWhere = strWhere + strUnit;

                //年份
                string tempYear = yearWhere(status, year, startyear, endyear, hidtime, startTime, endTime);

                dt = bll.CountCoperationLevelBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
            }
            else if (Action == "search3")
            {
                DataTable dt = new DataTable();

                //承接部门
                int? unitId;

                string type1_01 = context.Request.Params["type1_01"];
                string type1_02 = context.Request.Params["type1_02"];
                string type2_01 = context.Request.Params["type2_01"];
                string type2_02 = context.Request.Params["type2_02"];
                string type3_01 = context.Request.Params["type3_01"];
                string type3_02 = context.Request.Params["type3_02"];
                string type4_01 = context.Request.Params["type4_01"];
                string type4_02 = context.Request.Params["type4_02"];
                string type5_01 = context.Request.Params["type5_01"];
                string type5_02 = context.Request.Params["type5_02"];
                string type6_01 = context.Request.Params["type6_01"];
                string type6_02 = context.Request.Params["type6_02"];
                string type7_01 = context.Request.Params["type7_01"];
                string type7_02 = context.Request.Params["type7_02"];
                string type8_01 = context.Request.Params["type8_01"];
                string type8_02 = context.Request.Params["type8_02"];
                //合同区间值
                string startTime = context.Request.Params["startTime"];
                string endTime = context.Request.Params["endTime"];

                //查看
                string strWhere = "";
                //高级查询
                if (status!="0")
                {                
                    strWhere = getCountCoperationTargetWhere3(type1_01, type1_02, type2_01, type2_02, type3_01, type3_02, type4_01, type4_02, type5_01, type5_02, type6_01, type6_02, type7_01, type7_02, type8_01, type8_02);
                }
                string strUnit = getUnitId(hidPower, unit, userUnit, status);
                strWhere = strWhere + strUnit;

                //年份
                string tempYear = yearWhere(status, year, startyear, endyear, hidtime, startTime, endTime);


                int tempType = int.Parse(type) - 2;
                dt = bll.CountCoperationTypeBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere, tempType).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
            }
            else if (Action == "search4")
            {
                DataTable dt = new DataTable();

                //承接部门
                int? unitId;

                string property1_01 = context.Request.Params["property1_01"];
                string property1_02 = context.Request.Params["property1_02"];
                string property2_01 = context.Request.Params["property2_01"];
                string property2_02 = context.Request.Params["property2_02"];
                string property3_01 = context.Request.Params["property3_01"];
                string property3_02 = context.Request.Params["property3_02"];
                string property4_01 = context.Request.Params["property4_01"];
                string property4_02 = context.Request.Params["property4_02"];
                string property5_01 = context.Request.Params["property5_01"];
                string property5_02 = context.Request.Params["property5_02"];
                string property6_01 = context.Request.Params["property6_01"];
                string property6_02 = context.Request.Params["property6_02"];
                string property7_01 = context.Request.Params["property7_01"];
                string property7_02 = context.Request.Params["property7_02"];
                string property8_01 = context.Request.Params["property8_01"];
                string property8_02 = context.Request.Params["property8_02"];
                string property9_01 = context.Request.Params["property9_01"];
                string property9_02 = context.Request.Params["property9_02"];
                //合同区间值
                string startTime = context.Request.Params["startTime"];
                string endTime = context.Request.Params["endTime"];

                //查看
                string strWhere = "";
                if (status!="0")
                {                
                 strWhere = getCountCoperationTargetWhere4(property1_01, property1_02, property2_01, property2_02, property3_01, property3_02, property4_01, property4_02, property5_01, property5_02, property6_01, property6_02, property7_01, property7_02, property8_01, property8_02, property9_01, property9_02);
                }
                string strUnit = getUnitId(hidPower, unit, userUnit, status);
                strWhere = strWhere + strUnit;

                //年份
                string tempYear = yearWhere(status, year, startyear, endyear, hidtime, startTime, endTime);


                int tempType = int.Parse(type) - 6;

                dt = bll.CountCoperationIndustryBySql(tempYear, ChargeStartTime, ChargeEndTime, strWhere, tempType).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    context.Response.Write(TableToJson(dt));
                }
            }
        }

        //得到年份
        private string getYear(string year)
        {
            string tempYear = "";
            if (string.IsNullOrEmpty(year))
            {
                tempYear = DateTime.Now.Year.ToString();
            }
            else
            {
                tempYear = year;
            }
            return tempYear;
        }

        //得到年份的 查询条件
        private string yearWhere(string status1, string year1,string startyear1,string endyear1,string hidtime1,string startTime1, string endTime1)
        {
            string sqlWhere = "";
            //基本查询
            if (status1 == "0")
            {
                //合同年份时间段
                if (hidtime1 == "1")
                {
                    if (!string.IsNullOrEmpty(startyear1))
                    {
                        sqlWhere = " and cpr_SignDate>='" + startyear1 + "'";                       
                    }
                    if (!string.IsNullOrEmpty(endyear1))
                    {
                        sqlWhere = sqlWhere + " and cpr_SignDate<='" + endyear1 + "'";                        
                    }
                }
                else
                {
                    //全年份
                    if (!string.IsNullOrEmpty(year1) && year1 != "-1")
                    {
                        if (!string.IsNullOrEmpty(startyear1))
                        {
                            sqlWhere = " and cpr_SignDate>='" + startyear1 + "'";
                        }
                        if (!string.IsNullOrEmpty(endyear1))
                        {
                            sqlWhere = sqlWhere + " and cpr_SignDate<='" + endyear1 + "'";
                        }
                    }                 

                }

            }
            else
            {
                if (startTime1 != "-1" && endTime1 != "-1")
                {
                    sqlWhere = "AND year(cpr_SignDate) >='" + startTime1 + "' AND  year(cpr_SignDate)<='" + endTime1 + "'";
                }
                else if (startTime1 != "-1" && endTime1 == "-1")
                {
                    sqlWhere = "AND year(cpr_SignDate)>='" + startTime1 + "'";
                }
                else if (startTime1 == "-1" && endTime1 != "-1")
                {
                    sqlWhere = "AND year(cpr_SignDate)<='" + endTime1 + "'";
                }
            }
            return sqlWhere;
        }

        //取得ID
        private string getUnitId(string hidPower, string unit, string userUnit, string status)
        {
            string strUnit = "";
            //if (status == "0")
            //{
                if (string.IsNullOrEmpty(unit))
                {
                    //全部
                    if (hidPower == "1")
                    {
                        strUnit = "";
                    }
                    else//本部门
                    {
                        strUnit = " AND ID= " + userUnit + "";
                    }
                }
                else
                {
                    strUnit = string.Format(" AND ID IN ({0}) ", unit);
                }
            //}
            //else
            //{
            //    //全部的时候
            //    if (unit == "-1,")
            //    {
            //        //全部
            //        if (hidPower == "1")
            //        {
            //            strUnit = "";
            //        }
            //        else//本部门
            //        {
            //            strUnit = " AND ID =" + userUnit + " ";
            //        }
            //    }
            //    else
            //    {
            //        if (!string.IsNullOrEmpty(unit))
            //        {
            //            string unitTemp = unit.Substring(0, unit.Length - 1);
            //            strUnit = " AND ( ID in (" + unitTemp + ") ) ";
            //        }
            //    }
            //}

            return strUnit;
        }


        /// <summary>
        /// 得到合同目标的查询条件
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere(string cprTaretAcount1, string cprTaretAcount2, string cprAcount1, string cprAcount2, string cprAcountPercent1, string cprAcountPercent2, string cprTarentCharge1, string cprTarentCharge2, string cprCharge1, string cprCharge2, string cprChargePercent1, string cprChargePercent2)
        {

            StringBuilder sb = new StringBuilder();

            //合同目标值
            if (!string.IsNullOrEmpty(cprTaretAcount1) && string.IsNullOrEmpty(cprTaretAcount2))
            {
                sb.Append(" AND cprTarget >= " + cprTaretAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprTaretAcount1) && !string.IsNullOrEmpty(cprTaretAcount2))
            {
                sb.Append(" AND cprTarget <= " + cprTaretAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprTaretAcount1) && !string.IsNullOrEmpty(cprTaretAcount2))
            {
                sb.Append(" AND (cprTarget>= " + cprTaretAcount1.Trim() + " AND cprTarget <= " + cprTaretAcount2.Trim() + " ) ");
            }

            //完成合同额

            if (!string.IsNullOrEmpty(cprAcount1) && string.IsNullOrEmpty(cprAcount2))
            {
                sb.Append(" AND cprAllCount >= " + cprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprAcount1) && !string.IsNullOrEmpty(cprAcount2))
            {
                sb.Append(" AND cprAllCount <= " + cprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprAcount1) && !string.IsNullOrEmpty(cprAcount2))
            {
                sb.Append(" AND (cprAllCount>=" + cprAcount1.Trim() + " AND cprAllCount <= " + cprAcount2.Trim() + " ) ");
            }

            //合同额完成比例

            if (!string.IsNullOrEmpty(cprAcountPercent1) && string.IsNullOrEmpty(cprAcountPercent2))
            {
                sb.Append(" AND Cprprt >= " + cprAcountPercent1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprAcountPercent1) && !string.IsNullOrEmpty(cprAcountPercent2))
            {
                sb.Append(" AND Cprprt <= " + cprAcountPercent2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprAcountPercent1) && !string.IsNullOrEmpty(cprAcountPercent2))
            {
                sb.Append(" AND (Cprprt>=" + cprAcountPercent1.Trim() + " AND Cprprt <= " + cprAcountPercent2.Trim() + " ) ");
            }

            //目标产值

            if (!string.IsNullOrEmpty(cprTarentCharge1) && string.IsNullOrEmpty(cprTarentCharge2))
            {
                sb.Append(" AND AllotTarget >= " + cprTarentCharge1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprTarentCharge1) && !string.IsNullOrEmpty(cprTarentCharge2))
            {
                sb.Append(" AND AllotTarget <= " + cprTarentCharge2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprTarentCharge1) && !string.IsNullOrEmpty(cprTarentCharge2))
            {
                sb.Append(" AND (AllotTarget>=" + cprTarentCharge1.Trim() + " AND AllotTarget <= " + cprTarentCharge2.Trim() + " ) ");
            }

            //完成产值
            if (!string.IsNullOrEmpty(cprCharge1) && string.IsNullOrEmpty(cprCharge2))
            {
                sb.Append(" AND AllotCount >= " + cprCharge1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprCharge1) && !string.IsNullOrEmpty(cprCharge2))
            {
                sb.Append(" AND AllotCount <= " + cprCharge2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprCharge1) && !string.IsNullOrEmpty(cprCharge2))
            {
                sb.Append(" AND (AllotCount>=" + cprCharge1.Trim() + " AND AllotCount <= " + cprCharge2.Trim() + " ) ");
            }

            //完成产值比例
            if (!string.IsNullOrEmpty(cprChargePercent1) && string.IsNullOrEmpty(cprChargePercent2))
            {
                sb.Append(" AND AllotPrt >= " + cprChargePercent1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprChargePercent1) && !string.IsNullOrEmpty(cprChargePercent2))
            {
                sb.Append(" AND AllotPrt <= " + cprChargePercent2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprChargePercent1) && !string.IsNullOrEmpty(cprChargePercent2))
            {
                sb.Append(" AND (AllotPrt>=" + cprChargePercent1.Trim() + " AND AllotPrt <= " + cprChargePercent2.Trim() + " ) ");
            }

            return sb.ToString();
        }

        /// <summary>
        /// 得到合同审批
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere1(string cprCount1, string cprCount2, string cprSignAcount1, string cprSignAcount2, string bulid1, string bulid2, string chargeCount1, string chargeCount2, string passCount1, string passCount2, string passingCount1, string passingCount2, string notPassingCount1, string notPassingCount2)
        {

            StringBuilder sb = new StringBuilder();

            //已签合同数
            if (!string.IsNullOrEmpty(cprCount1) && string.IsNullOrEmpty(cprCount2))
            {
                sb.Append(" AND A >= " + cprCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprCount1) && !string.IsNullOrEmpty(cprCount2))
            {
                sb.Append(" AND A <= " + cprCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprCount1) && !string.IsNullOrEmpty(cprCount2))
            {
                sb.Append(" AND (A>= " + cprCount1.Trim() + " AND A <= " + cprCount2.Trim() + " ) ");
            }

            //已签合同额

            if (!string.IsNullOrEmpty(cprSignAcount1) && string.IsNullOrEmpty(cprSignAcount2))
            {
                sb.Append(" AND B >= " + cprSignAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(cprSignAcount1) && !string.IsNullOrEmpty(cprSignAcount2))
            {
                sb.Append(" AND B <= " + cprSignAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(cprSignAcount1) && !string.IsNullOrEmpty(cprSignAcount2))
            {
                sb.Append(" AND (B>=" + cprSignAcount1.Trim() + " AND B <= " + cprSignAcount2.Trim() + " ) ");
            }

            //建筑规模

            if (!string.IsNullOrEmpty(bulid1) && string.IsNullOrEmpty(bulid2))
            {
                sb.Append(" AND C >= " + bulid1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(bulid1) && !string.IsNullOrEmpty(bulid2))
            {
                sb.Append(" AND C <= " + bulid2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(bulid1) && !string.IsNullOrEmpty(bulid2))
            {
                sb.Append(" AND (C>=" + bulid1.Trim() + " AND C <= " + bulid2.Trim() + " ) ");
            }

            //收款金额

            if (!string.IsNullOrEmpty(chargeCount1) && string.IsNullOrEmpty(chargeCount2))
            {
                sb.Append(" AND D >= " + chargeCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(chargeCount1) && !string.IsNullOrEmpty(chargeCount2))
            {
                sb.Append(" AND D <= " + chargeCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(chargeCount1) && !string.IsNullOrEmpty(chargeCount2))
            {
                sb.Append(" AND (D>=" + chargeCount1.Trim() + " AND D <= " + chargeCount2.Trim() + " ) ");
            }

            //审批合同数
            if (!string.IsNullOrEmpty(passCount1) && string.IsNullOrEmpty(passCount2))
            {
                sb.Append(" AND E >= " + passCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(passCount1) && !string.IsNullOrEmpty(passCount2))
            {
                sb.Append(" AND E <= " + passCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(passCount1) && !string.IsNullOrEmpty(passCount2))
            {
                sb.Append(" AND (E>=" + passCount1.Trim() + " AND E <= " + passCount2.Trim() + " ) ");
            }

            //审批中
            if (!string.IsNullOrEmpty(passingCount1) && string.IsNullOrEmpty(passingCount2))
            {
                sb.Append(" AND F >= " + passingCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(passingCount1) && !string.IsNullOrEmpty(passingCount2))
            {
                sb.Append(" AND F <= " + passingCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(passingCount1) && !string.IsNullOrEmpty(passingCount2))
            {
                sb.Append(" AND (F>=" + passingCount1.Trim() + " AND F <= " + passingCount2.Trim() + " ) ");
            }

            //未通过
            if (!string.IsNullOrEmpty(notPassingCount1) && string.IsNullOrEmpty(notPassingCount2))
            {
                sb.Append(" AND G >= " + notPassingCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(notPassingCount1) && !string.IsNullOrEmpty(notPassingCount2))
            {
                sb.Append(" AND G <= " + notPassingCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(notPassingCount1) && !string.IsNullOrEmpty(notPassingCount2))
            {
                sb.Append(" AND (G>=" + notPassingCount1.Trim() + " AND G <= " + notPassingCount2.Trim() + " ) ");
            }
            return sb.ToString();
        }

        /// <summary>
        /// 得到合同等级
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere2(string tJCount1, string tJCount2, string tJCprAcount1, string tJCprAcount2, string tJCprChargeAcount1, string tJCprChargeAcount2, string yJCount1, string yJCount2, string yJCprAcount1, string yJCprAcount2, string yJCprChargeAcount1, string yJCprChargeAcount2, string eJCount1, string eJCount2, string eJCprAcount1, string eJCprAcount2, string eJCprChargeAcount1, string eJCprChargeAcount2, string sJCount1, string sJCount2, string sJCprAcount1, string sJCprAcount2, string sJCprChargeAcount1, string sJCprChargeAcount2)
        {

            StringBuilder sb = new StringBuilder();

            //特级
            if (!string.IsNullOrEmpty(tJCount1) && string.IsNullOrEmpty(tJCount2))
            {
                sb.Append(" AND A >= " + tJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(tJCount1) && !string.IsNullOrEmpty(tJCount2))
            {
                sb.Append(" AND A <= " + tJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(tJCount1) && !string.IsNullOrEmpty(tJCount2))
            {
                sb.Append(" AND (A>= " + tJCount1.Trim() + " AND A <= " + tJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(tJCprAcount1) && string.IsNullOrEmpty(tJCprAcount2))
            {
                sb.Append(" AND A1 >= " + tJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(tJCprAcount1) && !string.IsNullOrEmpty(tJCprAcount2))
            {
                sb.Append(" AND A1 <= " + tJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(tJCprAcount1) && !string.IsNullOrEmpty(tJCprAcount2))
            {
                sb.Append(" AND (A1>=" + tJCprAcount1.Trim() + " AND A1 <= " + tJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(tJCprChargeAcount1) && string.IsNullOrEmpty(tJCprChargeAcount2))
            {
                sb.Append(" AND A2 >= " + tJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(tJCprChargeAcount1) && !string.IsNullOrEmpty(tJCprChargeAcount2))
            {
                sb.Append(" AND A2 <= " + tJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(tJCprChargeAcount1) && !string.IsNullOrEmpty(tJCprChargeAcount2))
            {
                sb.Append(" AND (A2>=" + tJCprChargeAcount1.Trim() + " AND A2 <= " + tJCprChargeAcount2.Trim() + " ) ");
            }

            //一级
            if (!string.IsNullOrEmpty(yJCount1) && string.IsNullOrEmpty(yJCount2))
            {
                sb.Append(" AND B >= " + yJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(yJCount1) && !string.IsNullOrEmpty(yJCount2))
            {
                sb.Append(" AND B <= " + yJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(yJCount1) && !string.IsNullOrEmpty(yJCount2))
            {
                sb.Append(" AND (B>= " + yJCount1.Trim() + " AND B <= " + yJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(yJCprAcount1) && string.IsNullOrEmpty(yJCprAcount2))
            {
                sb.Append(" AND B1 >= " + yJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(yJCprAcount1) && !string.IsNullOrEmpty(yJCprAcount2))
            {
                sb.Append(" AND B1 <= " + yJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(yJCprAcount1) && !string.IsNullOrEmpty(yJCprAcount2))
            {
                sb.Append(" AND (B1>=" + yJCprAcount1.Trim() + " AND B1 <= " + yJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(yJCprChargeAcount1) && string.IsNullOrEmpty(yJCprChargeAcount2))
            {
                sb.Append(" AND B2 >= " + yJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(yJCprChargeAcount1) && !string.IsNullOrEmpty(yJCprChargeAcount2))
            {
                sb.Append(" AND B2 <= " + yJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(yJCprChargeAcount1) && !string.IsNullOrEmpty(yJCprChargeAcount2))
            {
                sb.Append(" AND (B2>=" + yJCprChargeAcount1.Trim() + " AND B2 <= " + yJCprChargeAcount2.Trim() + " ) ");
            }

            //二级
            if (!string.IsNullOrEmpty(eJCount1) && string.IsNullOrEmpty(eJCount2))
            {
                sb.Append(" AND C >= " + eJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(eJCount1) && !string.IsNullOrEmpty(eJCount2))
            {
                sb.Append(" AND C <= " + eJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(eJCount1) && !string.IsNullOrEmpty(eJCount2))
            {
                sb.Append(" AND (C>= " + eJCount1.Trim() + " AND C <= " + eJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(eJCprAcount1) && string.IsNullOrEmpty(eJCprAcount2))
            {
                sb.Append(" AND C1 >= " + eJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(eJCprAcount1) && !string.IsNullOrEmpty(eJCprAcount2))
            {
                sb.Append(" AND C1 <= " + eJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(eJCprAcount1) && !string.IsNullOrEmpty(eJCprAcount2))
            {
                sb.Append(" AND (C1>=" + eJCprAcount1.Trim() + " AND C1 <= " + eJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(eJCprChargeAcount1) && string.IsNullOrEmpty(eJCprChargeAcount2))
            {
                sb.Append(" AND C2 >= " + eJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(eJCprChargeAcount1) && !string.IsNullOrEmpty(eJCprChargeAcount2))
            {
                sb.Append(" AND C2 <= " + eJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(eJCprChargeAcount1) && !string.IsNullOrEmpty(eJCprChargeAcount2))
            {
                sb.Append(" AND (C2>=" + eJCprChargeAcount1.Trim() + " AND C2 <= " + eJCprChargeAcount2.Trim() + " ) ");
            }

            //三级
            if (!string.IsNullOrEmpty(sJCount1) && string.IsNullOrEmpty(sJCount2))
            {
                sb.Append(" AND D >= " + sJCount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(sJCount1) && !string.IsNullOrEmpty(sJCount2))
            {
                sb.Append(" AND D <= " + sJCount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(sJCount1) && !string.IsNullOrEmpty(sJCount2))
            {
                sb.Append(" AND (D>= " + sJCount1.Trim() + " AND D <= " + sJCount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(sJCprAcount1) && string.IsNullOrEmpty(sJCprAcount2))
            {
                sb.Append(" AND D1 >= " + sJCprAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(sJCprAcount1) && !string.IsNullOrEmpty(sJCprAcount2))
            {
                sb.Append(" AND D1 <= " + sJCprAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(sJCprAcount1) && !string.IsNullOrEmpty(sJCprAcount2))
            {
                sb.Append(" AND (D1>=" + sJCprAcount1.Trim() + " AND D1 <= " + sJCprAcount2.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(sJCprChargeAcount1) && string.IsNullOrEmpty(sJCprChargeAcount2))
            {
                sb.Append(" AND D2 >= " + sJCprChargeAcount1.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(sJCprChargeAcount1) && !string.IsNullOrEmpty(sJCprChargeAcount2))
            {
                sb.Append(" AND D2 <= " + sJCprChargeAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(sJCprChargeAcount1) && !string.IsNullOrEmpty(sJCprChargeAcount2))
            {
                sb.Append(" AND (D2>=" + sJCprChargeAcount1.Trim() + " AND D2 <= " + sJCprChargeAcount2.Trim() + " ) ");
            }
            return sb.ToString();
        }

        /// <summary>
        /// 得到合同类型
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere3(string type1_01, string type1_02, string type2_01, string type2_02, string type3_01, string type3_02, string type4_01, string type4_02, string type5_01, string type5_02, string type6_01, string type6_02, string type7_01, string type7_02, string type8_01, string type8_02)
        {

            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(type1_01) && string.IsNullOrEmpty(type1_02))
            {
                sb.Append(" AND A >= " + type1_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type1_01) && !string.IsNullOrEmpty(type1_02))
            {
                sb.Append(" AND A <= " + type1_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type1_01) && !string.IsNullOrEmpty(type1_02))
            {
                sb.Append(" AND (A>= " + type1_01.Trim() + " AND A <= " + type1_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type2_01) && string.IsNullOrEmpty(type2_02))
            {
                sb.Append(" AND B >= " + type2_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type2_01) && !string.IsNullOrEmpty(type2_02))
            {
                sb.Append(" AND B <= " + type2_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type2_01) && !string.IsNullOrEmpty(type2_02))
            {
                sb.Append(" AND (B>= " + type2_01.Trim() + " AND B <= " + type2_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type3_01) && string.IsNullOrEmpty(type3_02))
            {
                sb.Append(" AND C>= " + type3_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type3_01) && !string.IsNullOrEmpty(type3_02))
            {
                sb.Append(" AND C <= " + type3_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type3_01) && !string.IsNullOrEmpty(type3_02))
            {
                sb.Append(" AND (C>= " + type3_01.Trim() + " AND C <= " + type3_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type4_01) && string.IsNullOrEmpty(type4_02))
            {
                sb.Append(" AND D >= " + type4_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type4_01) && !string.IsNullOrEmpty(type4_02))
            {
                sb.Append(" AND D <= " + type4_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type4_01) && !string.IsNullOrEmpty(type4_02))
            {
                sb.Append(" AND (D>= " + type4_01.Trim() + " AND D <= " + type4_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type5_01) && string.IsNullOrEmpty(type5_02))
            {
                sb.Append(" AND E>= " + type5_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type5_01) && !string.IsNullOrEmpty(type5_02))
            {
                sb.Append(" AND E <= " + type5_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type5_01) && !string.IsNullOrEmpty(type5_02))
            {
                sb.Append(" AND (E>= " + type5_01.Trim() + " AND E <= " + type5_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type6_01) && string.IsNullOrEmpty(type6_02))
            {
                sb.Append(" AND F >= " + type6_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type6_01) && !string.IsNullOrEmpty(type6_02))
            {
                sb.Append(" AND F <= " + type6_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type6_01) && !string.IsNullOrEmpty(type6_02))
            {
                sb.Append(" AND (F>= " + type6_01.Trim() + " AND F <= " + type6_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type7_01) && string.IsNullOrEmpty(type7_02))
            {
                sb.Append(" AND G >= " + type7_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type7_01) && !string.IsNullOrEmpty(type7_02))
            {
                sb.Append(" AND G <= " + type7_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type7_01) && !string.IsNullOrEmpty(type7_02))
            {
                sb.Append(" AND (G>= " + type7_01.Trim() + " AND G<= " + type7_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(type8_01) && string.IsNullOrEmpty(type8_02))
            {
                sb.Append(" AND H >= " + type8_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(type8_01) && !string.IsNullOrEmpty(type8_02))
            {
                sb.Append(" AND H <= " + type8_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(type8_01) && !string.IsNullOrEmpty(type8_02))
            {
                sb.Append(" AND (H>= " + type8_01.Trim() + " AND H <= " + type8_02.Trim() + " ) ");
            }
            return sb.ToString();
        }


        /// <summary>
        /// 得到合同性质
        /// </summary>
        /// <returns></returns>
        private string getCountCoperationTargetWhere4(string property1_01, string property1_02, string property2_01, string property2_02, string property3_01, string property3_02, string property4_01, string property4_02, string property5_01, string property5_02, string property6_01, string property6_02, string property7_01, string property7_02, string property8_01, string property8_02, string property9_01, string property9_02)
        {

            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(property1_01) && string.IsNullOrEmpty(property1_02))
            {
                sb.Append(" AND A >= " + property1_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property1_01) && !string.IsNullOrEmpty(property1_02))
            {
                sb.Append(" AND A <= " + property1_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property1_01) && !string.IsNullOrEmpty(property1_02))
            {
                sb.Append(" AND (A>= " + property1_01.Trim() + " AND A <= " + property1_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property2_01) && string.IsNullOrEmpty(property2_02))
            {
                sb.Append(" AND B >= " + property2_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property2_01) && !string.IsNullOrEmpty(property2_02))
            {
                sb.Append(" AND B <= " + property2_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property2_01) && !string.IsNullOrEmpty(property2_02))
            {
                sb.Append(" AND (B>= " + property2_01.Trim() + " AND B <= " + property2_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property3_01) && string.IsNullOrEmpty(property3_02))
            {
                sb.Append(" AND C >= " + property3_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property3_01) && !string.IsNullOrEmpty(property3_02))
            {
                sb.Append(" AND C <= " + property3_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property3_01) && !string.IsNullOrEmpty(property3_02))
            {
                sb.Append(" AND (C>= " + property3_01.Trim() + " AND C <= " + property3_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property4_01) && string.IsNullOrEmpty(property4_02))
            {
                sb.Append(" AND D >= " + property4_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property4_01) && !string.IsNullOrEmpty(property4_02))
            {
                sb.Append(" AND D <= " + property4_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property4_01) && !string.IsNullOrEmpty(property4_02))
            {
                sb.Append(" AND (D>= " + property4_01.Trim() + " AND D <= " + property4_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property5_01) && string.IsNullOrEmpty(property5_02))
            {
                sb.Append(" AND E >= " + property5_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property5_01) && !string.IsNullOrEmpty(property5_02))
            {
                sb.Append(" AND E <= " + property5_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property5_01) && !string.IsNullOrEmpty(property5_02))
            {
                sb.Append(" AND (E>= " + property5_01.Trim() + " AND E <= " + property5_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property6_01) && string.IsNullOrEmpty(property6_02))
            {
                sb.Append(" AND F >= " + property6_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property6_01) && !string.IsNullOrEmpty(property6_02))
            {
                sb.Append(" AND F <= " + property6_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property6_01) && !string.IsNullOrEmpty(property6_02))
            {
                sb.Append(" AND (F>= " + property6_01.Trim() + " AND F <= " + property6_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property7_01) && string.IsNullOrEmpty(property7_02))
            {
                sb.Append(" AND G >= " + property7_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property7_01) && !string.IsNullOrEmpty(property7_02))
            {
                sb.Append(" AND G<= " + property7_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property7_01) && !string.IsNullOrEmpty(property7_02))
            {
                sb.Append(" AND (G>= " + property7_01.Trim() + " AND G <= " + property7_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property8_01) && string.IsNullOrEmpty(property8_02))
            {
                sb.Append(" AND H >= " + property8_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property8_01) && !string.IsNullOrEmpty(property8_02))
            {
                sb.Append(" AND H <= " + property8_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property8_01) && !string.IsNullOrEmpty(property8_02))
            {
                sb.Append(" AND (H>= " + property8_01.Trim() + " AND H <= " + property8_02.Trim() + " ) ");
            }

            if (!string.IsNullOrEmpty(property9_01) && string.IsNullOrEmpty(property9_02))
            {
                sb.Append(" AND I >= " + property9_01.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(property9_01) && !string.IsNullOrEmpty(property9_02))
            {
                sb.Append(" AND I <= " + property9_02.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(property9_01) && !string.IsNullOrEmpty(property9_02))
            {
                sb.Append(" AND (I>= " + property9_01.Trim() + " AND I <= " + property9_02.Trim() + " ) ");
            }
            return sb.ToString();
        }

        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}