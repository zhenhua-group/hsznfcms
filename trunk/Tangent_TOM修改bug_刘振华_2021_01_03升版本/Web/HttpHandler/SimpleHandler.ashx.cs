﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class SimpleHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string flag = context.Request["flag"] ?? "";
            string id = context.Request["id"] ?? "";
            if (flag == "getcprlevel")
            {
                string strSql = " SELECT TOP 1 ManageLevel FROM cm_AuditRecord WHERE (CoperationSysNo = 58) ORDER BY SysNo DESC";
                TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
                DataSet ds = bll.GetList(strSql);
                //默认级别
                string strlevel = "0";
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "1")
                    {
                        strlevel = ds.Tables[0].Rows[0][0].ToString();
                    }
                }
                context.Response.Write(strlevel);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}