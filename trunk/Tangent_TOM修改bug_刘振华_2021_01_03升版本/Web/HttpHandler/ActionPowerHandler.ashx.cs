﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using TG.Model;
using TG.BLL;
using Newtonsoft.Json;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class ActionPowerHandler : HandlerCommon, IHttpHandler
    {
        public string PageName
        {
            get
            {
                return Request["pageName"];
            }
        }

        public LeftMenuType leftMenuType
        {
            get
            {
                int type = 0;
                int.TryParse(Request["type"], out type);
                LeftMenuType leftType = LeftMenuType.All;
                if (type != 0)
                {
                    leftType = (LeftMenuType)type;
                }
                return leftType;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            CheckPower();
            Response.End();
        }

        private void CheckPower()
        {
            if (UserSysNo != 0)
            {
                //取得当前登陆用户RoleLis
                List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList(UserSysNo);

                //取得当前页面ActionPowerEntity
                ActionPowerViewEntity actionPowerEntity = new ActionPowerBP().GetActionPowerViewEntity(new ActionPowerQueryEntity { PageName = PageName, Type = leftMenuType });

                if (actionPowerEntity == null)
                    return;

                string[] roleSysNoArray = actionPowerEntity.Role.Split(',');

                bool flag = false;

                for (int i = 0; i < roleSysNoArray.Length; i++)
                {
                    for (int j = 0; j < roleList.Count; j++)
                    {
                        if (int.Parse(roleSysNoArray[i]) == roleList[j].SysNo)
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag)
                    {
                        break;
                    }
                }

                ActionPowerParameterEntity parameterEntity = new ActionPowerParameterEntity();
                string[] powerArray = actionPowerEntity.Power.Split('|');
                parameterEntity.AllowEdit = int.Parse(powerArray[0]);
                parameterEntity.AllowDelete = int.Parse(powerArray[1]);

                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(parameterEntity));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
