﻿using System;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Linq;
using System.Web;
using System.Text;
using Newtonsoft.Json;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace TG.Web.HttpHandler.Calendar
{
    /// <summary>
    /// CalendarHandler 的摘要说明
    /// </summary>
    public class CalendarHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request["action"] ?? "";
            if (action == "add")
            {
                string data = context.Request["data"] ?? "";
                TG.Model.cm_ApplyInfo applymodel = new TG.Model.cm_ApplyInfo();
                //保存按钮
                if (!string.IsNullOrEmpty(data))
                {
                    applymodel = Newtonsoft.Json.JsonConvert.DeserializeObject<TG.Model.cm_ApplyInfo>(data);
                    string aid = context.Request["applyID"] ?? "0";
                    //修改
                    if (!string.IsNullOrEmpty(aid) && aid != "0")
                    {
                        TG.Model.cm_ApplyInfo aif = new TG.BLL.cm_ApplyInfo().GetModel(Convert.ToInt32(aid));
                        if (aif != null)
                        {
                            applymodel.ID = aif.ID;
                            applymodel.adduser = aif.adduser;
                            applymodel.addtime = aif.addtime;
                            applymodel.IsDone = aif.IsDone;
                            applymodel.RelationID = aif.RelationID;
                            bool flag = new TG.BLL.cm_ApplyInfo().Update(applymodel);
                            if (flag)
                            {
                                context.Response.Write("3");
                            }
                        }

                    }
                    else
                    {
                        //特殊人处理
                        string SpecialPerson = System.Configuration.ConfigurationManager.AppSettings["SpecialPerson"];
                        string SpecialPersonAudit = System.Configuration.ConfigurationManager.AppSettings["SpecialPersonAudit"];
                        string[] SpecialPersonAuditList = SpecialPersonAudit.Split(',');                       
                        TG.Model.tg_member mem = new TG.BLL.tg_member().GetModel(UserSysNo);
                        List<TG.Model.tg_member> model_memlist = new List<TG.Model.tg_member>();
                        string strSpecialPerson = SpecialPerson;
                        strSpecialPerson = strSpecialPerson.Replace("|", ",") + ",";
                        if (strSpecialPerson.Contains(mem.mem_Name.Trim() + ","))
                        {
                            //特殊审批人   
                            string[] SpecialPersonList = SpecialPerson.Split('|');
                            int ind = 0;
                            for (int i = 0; i < SpecialPersonList.Length; i++)
                            {
                                if (SpecialPersonList[i].IndexOf(mem.mem_Name) > -1)
                                {
                                    ind = i;
                                    break;
                                }
                            }
                            model_memlist = new TG.BLL.tg_member().GetModelList(" mem_Name like '%" + SpecialPersonAuditList[ind] + "%' order by mem_ID asc");
                        }
                        //添加
                        applymodel.adduser = UserSysNo;
                        applymodel.addtime = DateTime.Now;
                        applymodel.IsDone = "A";
                        int applyid = new TG.BLL.cm_ApplyInfo().Add(applymodel);
                        if (applyid > 0)
                        {
                           
                            #region 动态添加时间
                            string addtimestr = context.Request["addtimestr"] ?? "";
                            if (!string.IsNullOrEmpty(addtimestr))
                            {
                                //自身关联ID
                                TG.DBUtility.DbHelperSQL.ExecuteSql("update cm_ApplyInfo set RelationID=" + applyid + " where ID=" + applyid);

                                List<TG.Model.cm_ApplyInfo> applylist = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.cm_ApplyInfo>>(addtimestr);
                                applylist.ForEach(am =>
                                {
                                    am.adduser = UserSysNo;
                                    am.addtime = DateTime.Now;
                                    am.IsDone = "A";
                                    am.RelationID = applyid;
                                    int subapplyid = new TG.BLL.cm_ApplyInfo().Add(am);
                                    if (subapplyid>0)
                                    {
                                        //发起审批
                                        //插入一条申请审批记录
                                        TG.Model.cm_ApplyInfoAudit subaia = new TG.Model.cm_ApplyInfoAudit();
                                        subaia.ApplyID = subapplyid;
                                        subaia.Status = "A";
                                        subaia.InUser = UserSysNo;
                                        subaia.InDate = DateTime.Now;
                                        int subappauditid = new TG.BLL.cm_ApplyInfoAudit().Add(subaia);
                                        if (subappauditid > 0)
                                        {
                                            if (strSpecialPerson.Contains(mem.mem_Name.Trim() + ","))
                                            {                                               
                                                if (model_memlist != null && model_memlist.Count > 0)
                                                {
                                                    //消息给张兵
                                                    TG.Model.SysMessageViewEntity msg1 = new TG.Model.SysMessageViewEntity
                                                    {
                                                        ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", am.applytype, subapplyid.ToString(), subappauditid.ToString(), "A"),
                                                        FromUser = model_memlist[0].mem_ID,
                                                        InUser = UserSysNo,
                                                        MsgType = 35,
                                                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + am.addtime + am.applyTypeName), "考勤审批"),
                                                        QueryCondition = (am.addtime + mem.mem_Name + am.applyTypeName),
                                                        ToRole = "0",
                                                        Status = "A",
                                                        IsDone = "A"
                                                    };
                                                    int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg1);
                                                    //if (count > 0)
                                                    //{
                                                    //    context.Response.Write("1");
                                                    //}
                                                    //else
                                                    //{
                                                    //    context.Response.Write("2");
                                                    //}
                                                }
                                            }
                                            else
                                            {
                                                List<TG.Model.cm_UnitManagerConfig> umc = new TG.BLL.cm_UnitManagerConfig().GetModelList(" unit_id=" + mem.mem_Unit_ID);
                                                if (umc != null && umc.Count > 0)
                                                {
                                                    //部门经理是否存在
                                                    if (!string.IsNullOrEmpty(umc[0].unitusers))
                                                    {
                                                        string[] unituser = umc[0].unitusers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                        //发送消息给部门经理
                                                        TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                                        {
                                                            ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", am.applytype, subapplyid.ToString(), subappauditid.ToString(), "A"),
                                                            FromUser = Convert.ToInt32(unituser[0]),
                                                            InUser = UserSysNo,
                                                            MsgType = 35,
                                                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + am.addtime + am.applyTypeName), "考勤部门经理审批"),
                                                            QueryCondition = (am.addtime + mem.mem_Name + am.applyTypeName),
                                                            ToRole = "0",
                                                            Status = "A",
                                                            IsDone = "A"
                                                        };
                                                        int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                                        //if (count > 0)
                                                        //{
                                                        //    context.Response.Write("1");
                                                        //}
                                                        //else
                                                        //{
                                                        //    context.Response.Write("2");
                                                        //}
                                                    }
                                                   
                                                }
                                               
                                            }

                                        }
                                        
                                    }

                                });
                            }
                            #endregion
                            //插入一条申请审批记录
                            TG.Model.cm_ApplyInfoAudit aia = new TG.Model.cm_ApplyInfoAudit();
                            aia.ApplyID = applyid;
                            aia.Status = "A";
                            aia.InUser = UserSysNo;
                            aia.InDate = DateTime.Now;
                            int appauditid = new TG.BLL.cm_ApplyInfoAudit().Add(aia);
                            if (appauditid > 0)
                            {
                               // TG.Model.tg_member mem = new TG.BLL.tg_member().GetModel(UserSysNo);
                                //特殊人处理
                               // string SpecialPerson = System.Configuration.ConfigurationManager.AppSettings["SpecialPerson"];
                              //  string SpecialPersonAudit = System.Configuration.ConfigurationManager.AppSettings["SpecialPersonAudit"];
                                if (strSpecialPerson.Contains(mem.mem_Name.Trim()+","))
                                {
                                    //特殊审批人   
                                    //string[] SpecialPersonList = SpecialPerson.Split('|');
                                    //int ind = 0;
                                    //for (int i = 0; i < SpecialPersonList.Length; i++)
                                    //{
                                    //    if (SpecialPersonList[i].IndexOf(mem.mem_Name) > -1)
                                    //    {
                                    //        ind = i;
                                    //        break;
                                    //    }
                                    //}
                                  //  string[] SpecialPersonAuditList = SpecialPersonAudit.Split(',');

                                   // List<TG.Model.tg_member> model_memlist = new TG.BLL.tg_member().GetModelList(" mem_Name like '%" + SpecialPersonAuditList[ind] + "%' order by mem_ID asc");
                                    if (model_memlist != null && model_memlist.Count > 0)
                                    {   
                                        //消息给张兵
                                        TG.Model.SysMessageViewEntity msg1 = new TG.Model.SysMessageViewEntity
                                        {
                                            ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", applymodel.applytype, applyid.ToString(), appauditid.ToString(), "A"),
                                            FromUser = model_memlist[0].mem_ID,
                                            InUser = UserSysNo,
                                            MsgType = 35,
                                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + applymodel.addtime + applymodel.applyTypeName), "考勤审批"),
                                            QueryCondition = (applymodel.addtime + mem.mem_Name + applymodel.applyTypeName),
                                            ToRole = "0",
                                            Status = "A",
                                            IsDone = "A"
                                        };
                                        int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg1);
                                        if (count > 0)
                                        {
                                            context.Response.Write("1");
                                        }
                                        else
                                        {
                                            context.Response.Write("2");
                                        }
                                    }
                                }
                                else
                                {
                                    List<TG.Model.cm_UnitManagerConfig> umc = new TG.BLL.cm_UnitManagerConfig().GetModelList(" unit_id=" + mem.mem_Unit_ID);
                                    if (umc != null && umc.Count > 0)
                                    {
                                        //部门经理是否存在
                                        if (!string.IsNullOrEmpty(umc[0].unitusers))
                                        {
                                            string[] unituser = umc[0].unitusers.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                            //发送消息给部门经理
                                            TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                            {
                                                ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", applymodel.applytype, applyid.ToString(), appauditid.ToString(), "A"),
                                                FromUser = Convert.ToInt32(unituser[0]),
                                                InUser = UserSysNo,
                                                MsgType = 35,
                                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + applymodel.addtime + applymodel.applyTypeName), "考勤部门经理审批"),
                                                QueryCondition = (applymodel.addtime + mem.mem_Name + applymodel.applyTypeName),
                                                ToRole = "0",
                                                Status = "A",
                                                IsDone = "A"
                                            };
                                            int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                            if (count > 0)
                                            {
                                                context.Response.Write("1");
                                            }
                                            else
                                            {
                                                context.Response.Write("2");
                                            }
                                        }
                                        else
                                        {
                                            context.Response.Write("2");
                                        }
                                    }
                                    else
                                    {
                                        context.Response.Write("2");
                                    }
                                }

                            }
                            else
                            {
                                context.Response.Write("2");
                            }

                        }
                    }

                }

            }
            else if (action == "audit")
            {
                string auditsysno = context.Request.QueryString["auditsysno"] ?? "0";
                string auditstatus = context.Request.QueryString["auditstatus"] ?? "";
                string applybtn = context.Request.QueryString["applybtn"] ?? "";
                string managerid = context.Request.QueryString["managerid"] ?? "";
                TG.BLL.cm_ApplyInfoAudit bll_aia = new TG.BLL.cm_ApplyInfoAudit();
                TG.Model.cm_ApplyInfoAudit aia = bll_aia.GetModel(Convert.ToInt32(auditsysno));
                if (aia != null)
                {
                    int asciiCode = (int)Convert.ToChar(auditstatus);
                    //通过
                    if (applybtn == "Agree")
                    {
                        if (asciiCode % 2 != 0)
                        {
                            asciiCode++;
                        }
                        else
                        {
                            asciiCode = asciiCode + 2;
                        }
                    }
                    else //不通过
                    {
                        if (asciiCode % 2 != 0)
                        {
                            asciiCode = asciiCode + 2;
                        }
                        else
                        {
                            asciiCode = asciiCode + 3;
                        }
                    }

                    if (auditstatus == "A")
                    {
                        aia.Unitmanager = UserSysNo;
                        aia.AudtiDate = DateTime.Now.ToString();
                    }
                    else
                    {
                        aia.Generalmanager = UserSysNo;
                        aia.AudtiDate = aia.AudtiDate + "," + DateTime.Now.ToString();
                    }
                    aia.Status = ((char)asciiCode).ToString();
                    if (bll_aia.Update(aia))
                    {
                       
                        //更新消息已办
                        TG.DBUtility.DbHelperSQL.ExecuteSql("update cm_SysMsg set isDone='D',Status='D' where ReferenceSysNo like '%applyAuditID=" + aia.SysNo + "%' and MsgType=35 and isDone='A'");
                       
                        TG.Model.cm_ApplyInfo ap = new TG.BLL.cm_ApplyInfo().GetModel(aia.ApplyID);
                        TG.Model.tg_member mem = new TG.BLL.tg_member().GetModel(ap.adduser);
                        if (ap != null)
                        {

                            int count = 0;
                            //发送消息，
                            if (!string.IsNullOrEmpty(managerid) && UserSysNo.ToString() != managerid)
                            {                               
                                //消息给总经理
                                TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                    FromUser = Convert.ToInt32(managerid),
                                    InUser = UserSysNo,
                                    MsgType = 35,
                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime + ap.applyTypeName), "考勤总经理审批"),
                                    QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName),
                                    ToRole = "0",
                                    Status = "A",
                                    IsDone = "A"
                                };
                                count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);

                                // TG.Model.tg_unit model_unit=new TG.BLL.tg_unit().GetModel(mem.mem_Unit_ID);
                                //发送总经理抄送给张兵
                                //if (model_unit!=null&&(model_unit.unit_Name.IndexOf("规划")>-1))
                                //{
                                //    string SpecialPersonAudit = System.Configuration.ConfigurationManager.AppSettings["SpecialPersonAudit"];
                                //    List<TG.Model.tg_member> model_memlist = new TG.BLL.tg_member().GetModelList(" mem_Name like '%" + SpecialPersonAudit + "%' order by mem_ID asc");
                                //   if (model_memlist != null && model_memlist.Count>0)
                                //   {
                                //       //消息给张兵
                                //       TG.Model.SysMessageViewEntity msg1 = new TG.Model.SysMessageViewEntity
                                //       {
                                //           ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                //           FromUser = model_memlist[0].mem_ID,
                                //           InUser = UserSysNo,
                                //           MsgType = 35,
                                //           MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime + ap.applyTypeName), "考勤总经理审批"),
                                //           QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName ),
                                //           ToRole = "0",
                                //           Status = "A",
                                //           IsDone = "A"
                                //       };
                                //       count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg1);
                                //   }

                                //}

                                if (count > 0)
                                {
                                    context.Response.Write("3");
                                }
                                else
                                {
                                    context.Response.Write("2");
                                }
                            }
                            else
                            {
                                //部门审批和总经理是一个人，审批一次即可通过
                                if (!string.IsNullOrEmpty(managerid) && UserSysNo.ToString() == managerid)
                                {                                 
                                    //通过
                                    if (asciiCode % 2 != 0)
                                    {
                                        asciiCode++;
                                    }
                                    else
                                    {
                                        asciiCode = asciiCode + 2;
                                    }
                                    aia.Generalmanager = UserSysNo;
                                    aia.AudtiDate = aia.AudtiDate + "," + DateTime.Now.ToString();
                                    aia.Status = ((char)asciiCode).ToString();
                                    bll_aia.Update(aia);
                                }

                                string strpass = "审批已通过";
                                if (aia.Status == "C" || aia.Status == "E")
                                {
                                    strpass = "审批不通过";
                                }
                                //消息给申请人
                                TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                    FromUser = ap.adduser,
                                    InUser = UserSysNo,
                                    MsgType = 35,
                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime + ap.applyTypeName), strpass),
                                    QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName),
                                    ToRole = "0",
                                    Status = "A",
                                    IsDone = "B"
                                };
                                count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                if (count > 0)
                                {
                                    context.Response.Write("1");
                                }
                                else
                                {
                                    context.Response.Write("2");
                                }
                            }


                        }
                        else
                        {
                            context.Response.Write("2");
                        }
                    }

                }

            }
            else if (action == "auditlist")
            {
                string auditsysnolist = context.Request.QueryString["auditsysnolist"] ?? "0";
                string auditstatus = context.Request.QueryString["auditstatus"] ?? "";
                string applybtn = context.Request.QueryString["applybtn"] ?? "";
                string manageridlist = context.Request.QueryString["manageridlist"] ?? "";
                string nextprocesslist = context.Request.QueryString["nextprocesslist"] ?? "";
                TG.BLL.cm_ApplyInfoAudit bll_aia = new TG.BLL.cm_ApplyInfoAudit();

                if (!string.IsNullOrEmpty(auditsysnolist))
                {
                    int j = 0;
                    string[] list = auditsysnolist.Split(',');
                    if (list != null && list.Length > 0)
                    {
                        //循环审批ID
                        for (int i = 0; i < list.Length; i++)
                        {
                            TG.Model.cm_ApplyInfoAudit aia = bll_aia.GetModel(Convert.ToInt32(list[i]));
                            if (aia != null)
                            {
                                int asciiCode = (int)Convert.ToChar(auditstatus);
                                //通过
                                if (applybtn == "Agree")
                                {
                                    if (asciiCode % 2 != 0)
                                    {
                                        asciiCode++;
                                    }
                                    else
                                    {
                                        asciiCode = asciiCode + 2;
                                    }
                                }
                                else //不通过
                                {
                                    if (asciiCode % 2 != 0)
                                    {
                                        asciiCode = asciiCode + 2;
                                    }
                                    else
                                    {
                                        asciiCode = asciiCode + 3;
                                    }
                                }

                                if (auditstatus == "A")
                                {
                                    aia.Unitmanager = UserSysNo;
                                    aia.AudtiDate = DateTime.Now.ToString();
                                }
                                else
                                {
                                    aia.Generalmanager = UserSysNo;
                                    aia.AudtiDate = aia.AudtiDate + "," + DateTime.Now.ToString();
                                }
                                aia.Status = ((char)asciiCode).ToString();
                                if (bll_aia.Update(aia))
                                {
                                    //更新消息已办
                                    TG.DBUtility.DbHelperSQL.ExecuteSql("update cm_SysMsg set isDone='D',Status='D' where ReferenceSysNo like '%applyAuditID=" + aia.SysNo + "%' and MsgType=35 and isDone='A'");

                                    TG.Model.cm_ApplyInfo ap = new TG.BLL.cm_ApplyInfo().GetModel(aia.ApplyID);
                                    TG.Model.tg_member mem = new TG.BLL.tg_member().GetModel(ap.adduser);
                                    if (ap != null)
                                    {
                                        int count = 0;
                                        //是否需要总经理负责
                                        if (!string.IsNullOrEmpty(nextprocesslist))
                                        {
                                            string[] nextlist = nextprocesslist.Split(',');
                                            string[] managerlist = manageridlist.Split(',');
                                            //发送消息给总经理
                                            if (!string.IsNullOrEmpty(nextlist[i]) && UserSysNo.ToString() != nextlist[i])
                                            {
                                                //消息给总经理
                                                TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                                {
                                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                                    FromUser = Convert.ToInt32(managerlist[i]),
                                                    InUser = UserSysNo,
                                                    MsgType = 35,
                                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime + ap.applyTypeName), "考勤总经理审批"),
                                                    QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName),
                                                    ToRole = "0",
                                                    Status = "A",
                                                    IsDone = "A"
                                                };
                                                count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                                if (count > 0)
                                                {
                                                    j++;
                                                }

                                                //TG.Model.tg_unit model_unit = new TG.BLL.tg_unit().GetModel(mem.mem_Unit_ID);
                                                ////发送总经理抄送给张兵
                                                //if (model_unit != null && (model_unit.unit_Name.IndexOf("规划") > -1))
                                                //{
                                                //    string SpecialPersonAudit = System.Configuration.ConfigurationManager.AppSettings["SpecialPersonAudit"];
                                                //    List<TG.Model.tg_member> model_memlist = new TG.BLL.tg_member().GetModelList(" mem_Name like '%" + SpecialPersonAudit + "%' order by mem_ID asc");
                                                //    if (model_memlist != null && model_memlist.Count > 0)
                                                //    {
                                                //        //消息给张兵
                                                //        TG.Model.SysMessageViewEntity msg1 = new TG.Model.SysMessageViewEntity
                                                //        {
                                                //            ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                                //            FromUser = model_memlist[0].mem_ID,
                                                //            InUser = UserSysNo,
                                                //            MsgType = 35,
                                                //            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime +ap.applyTypeName), "考勤总经理审批"),
                                                //            QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName),
                                                //            ToRole = "0",
                                                //            Status = "A",
                                                //            IsDone = "A"
                                                //        };
                                                //        count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg1);
                                                //    }

                                                //}

                                            }
                                            else
                                            {
                                                //部门审批和总经理是一个人，审批一次即可通过
                                                if (!string.IsNullOrEmpty(nextlist[i]) && UserSysNo.ToString() == nextlist[i])
                                                {
                                                    //通过
                                                    if (asciiCode % 2 != 0)
                                                    {
                                                        asciiCode++;
                                                    }
                                                    else
                                                    {
                                                        asciiCode = asciiCode + 2;
                                                    }
                                                    aia.Generalmanager = UserSysNo;
                                                    aia.AudtiDate = aia.AudtiDate + "," + DateTime.Now.ToString();
                                                    aia.Status = ((char)asciiCode).ToString();
                                                    bll_aia.Update(aia);
                                                }

                                                string strpass = "审批已通过";
                                                if (aia.Status == "C" || aia.Status == "E")
                                                {
                                                    strpass = "审批不通过";
                                                }
                                                //消息给申请人
                                                TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                                {
                                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                                    FromUser = ap.adduser,
                                                    InUser = UserSysNo,
                                                    MsgType = 35,
                                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime + ap.applyTypeName), strpass),
                                                    QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName),
                                                    ToRole = "0",
                                                    Status = "A",
                                                    IsDone = "B"
                                                };
                                                count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                                if (count > 0)
                                                {
                                                    j++;
                                                }

                                            }

                                        }
                                        else
                                        {
                                            string strpass = "审批已通过";
                                            if (aia.Status == "C" || aia.Status == "E")
                                            {
                                                strpass = "审批不通过";
                                            }
                                            //消息给申请人
                                            TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                            {
                                                ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                                FromUser = ap.adduser,
                                                InUser = UserSysNo,
                                                MsgType = 35,
                                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime + ap.applyTypeName), strpass),
                                                QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName),
                                                ToRole = "0",
                                                Status = "A",
                                                IsDone = "B"
                                            };
                                            count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                            if (count > 0)
                                            {
                                                j++;
                                            }

                                        }


                                    }

                                }

                            }
                        }

                        if (j > 0)
                        {
                            context.Response.Write("1");
                        }

                    }

                }
            }
            else if (action == "auditreturn")//审批退回
            {
                string auditsysno = context.Request.QueryString["auditsysno"] ?? "0";             
                TG.BLL.cm_ApplyInfoAudit bll_aia = new TG.BLL.cm_ApplyInfoAudit();
                TG.Model.cm_ApplyInfoAudit aia = bll_aia.GetModel(Convert.ToInt32(auditsysno));
                if (aia != null)
                {
                   aia.Status = "N";
                   int rows = DBUtility.DbHelperSQL.ExecuteSql("update cm_ApplyInfoAudit set Status='" + aia.Status + "' where SysNo=" + aia.SysNo);
                   if (rows>0)
                    {
                        TG.Model.cm_ApplyInfo ap = new TG.BLL.cm_ApplyInfo().GetModel(aia.ApplyID);
                        TG.Model.tg_member mem = new TG.BLL.tg_member().GetModel(ap.adduser);
                        if (ap != null)
                        {
                                int count = 0;
                                string strpass = "申请退回需重新填";
                               
                                //消息给申请人
                                TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", ap.applytype, aia.ApplyID.ToString(), aia.SysNo.ToString(), aia.Status),
                                    FromUser = ap.adduser,
                                    InUser = UserSysNo,
                                    MsgType = 35,
                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + ap.addtime + ap.applyTypeName), strpass),
                                    QueryCondition = (ap.addtime + mem.mem_Name + ap.applyTypeName),
                                    ToRole = "0",
                                    Status = "A",
                                    IsDone = "B"
                                };
                                count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                if (count > 0)
                                {
                                    context.Response.Write("1");
                                }
                                else
                                {
                                    context.Response.Write("2");
                                }
                            }

                    }

                }

            }
            else if (action == "restart")
            {
                string applyid = context.Request.QueryString["applyid"] ?? "";
                string unitid = context.Request.QueryString["unitid"] ?? "";
                if (!string.IsNullOrEmpty(applyid))
                {
                    //插入一条申请审批记录
                    TG.Model.cm_ApplyInfoAudit aia = new TG.Model.cm_ApplyInfoAudit();
                    aia.ApplyID = Convert.ToInt32(applyid);
                    aia.Status = "A";
                    aia.InUser = UserSysNo;
                    aia.InDate = DateTime.Now;
                    int appauditid = new TG.BLL.cm_ApplyInfoAudit().Add(aia);
                    if (appauditid > 0)
                    {

                        TG.Model.cm_ApplyInfo applymodel = new TG.BLL.cm_ApplyInfo().GetModel(Convert.ToInt32(applyid));
                        TG.Model.tg_member mem = new TG.BLL.tg_member().GetModel(applymodel.adduser);
                        //特殊人处理
                        string SpecialPerson = System.Configuration.ConfigurationManager.AppSettings["SpecialPerson"];
                        string SpecialPersonAudit = System.Configuration.ConfigurationManager.AppSettings["SpecialPersonAudit"];
                        string strSpecialPerson = SpecialPerson;
                        strSpecialPerson = strSpecialPerson.Replace("|", ",") + ",";
                        if (strSpecialPerson.Contains(mem.mem_Name.Trim() + ","))                      
                        {
                            //特殊审批人   
                            string[] SpecialPersonList = SpecialPerson.Split('|');
                            int ind = 0;
                            for (int i = 0; i < SpecialPersonList.Length; i++)
                            {
                                if (SpecialPersonList[i].IndexOf(mem.mem_Name) > -1)
                                {
                                    ind = i;
                                    break;
                                }
                            }
                            string[] SpecialPersonAuditList = SpecialPersonAudit.Split(',');

                            List<TG.Model.tg_member> model_memlist = new TG.BLL.tg_member().GetModelList(" mem_Name like '%" + SpecialPersonAuditList[ind] + "%' order by mem_ID asc");
                            if (model_memlist != null && model_memlist.Count > 0)
                            {
                                //消息给张兵
                                TG.Model.SysMessageViewEntity msg1 = new TG.Model.SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", applymodel.applytype, applyid.ToString(), appauditid.ToString(), "A"),
                                    FromUser = model_memlist[0].mem_ID,
                                    InUser = UserSysNo,
                                    MsgType = 35,
                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + applymodel.addtime + applymodel.applyTypeName), "考勤审批"),
                                    QueryCondition = (applymodel.addtime + mem.mem_Name + applymodel.applyTypeName),
                                    ToRole = "0",
                                    Status = "A",
                                    IsDone = "A"
                                };
                                int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg1);
                                if (count > 0)
                                {
                                    context.Response.Write("1");
                                }
                                else
                                {
                                    context.Response.Write("2");
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(unitid))
                            {

                                //发送消息给部门经理
                                TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}&applyAuditID={2}&MessageStatus={3}", applymodel.applytype, applyid.ToString(), appauditid.ToString(), "A"),
                                    FromUser = Convert.ToInt32(unitid),
                                    InUser = UserSysNo,
                                    MsgType = 35,
                                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (mem.mem_Name + applymodel.addtime + applymodel.applyTypeName), "考勤部门经理审批"),
                                    QueryCondition = (applymodel.addtime + mem.mem_Name + applymodel.applyTypeName),
                                    ToRole = "0",
                                    Status = "A",
                                    IsDone = "A"
                                };
                                int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                if (count > 0)
                                {
                                    context.Response.Write("1");
                                }
                                else
                                {
                                    context.Response.Write("2");
                                }

                            }
                            else
                            {
                                context.Response.Write("2");
                            }
                        }
                    }
                }

            }
            else if (action == "done")
            {
                string idlist = context.Request.QueryString["idlist"] ?? "";
                if (!string.IsNullOrEmpty(idlist))
                {
                    string sql = "update cm_ApplyInfo set isDone='D' where ID in (" + idlist + ")";
                    int count = TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
                    if (count > 0)
                    {
                        context.Response.Write("1");
                    }
                }
            }
            else if (action == "delete")
            {
                string applyid = context.Request.QueryString["applyID"] ?? "0";
                string msgID = context.Request.QueryString["msgID"] ?? "0";
                string ispass = context.Request.QueryString["ispass"] ?? "N";
                if (!string.IsNullOrEmpty(applyid))
                {
                    object obj = TG.DBUtility.DbHelperSQL.GetSingle("select applytype from cm_ApplyInfo where ID=" + applyid);
                    if (obj != null)
                    {
                        //申请人
                        DataTable dt = TG.DBUtility.DbHelperSQL.Query("select * from cm_SysMsg where SysNo=" + msgID).Tables[0];
                        if (dt != null && dt.Rows.Count > 0)
                        {

                            //更新消息已办 
                            new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(int.Parse(msgID));

                            if (ispass == "N")
                            {
                                //发送消息给申请人
                                TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                {
                                    ReferenceSysNo = string.Format("applyType={0}&applyID={1}", obj.ToString(), applyid),
                                    FromUser = Convert.ToInt32(dt.Rows[0]["InUser"]),
                                    InUser = Convert.ToInt32(dt.Rows[0]["FromUser"]),
                                    MsgType = 36,
                                    MessageContent = dt.Rows[0]["MessageContent"].ToString() + "不通过！",
                                    QueryCondition = dt.Rows[0]["QueryCondition"].ToString(),
                                    ToRole = "0",
                                    Status = "A",
                                    IsDone = "B"
                                };
                                new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                            }
                            else
                            {
                                string sql = "update cm_ApplyInfo set isDone='B' where ID=" + applyid + "";
                                int count = TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
                                if (count > 0)
                                {
                                    //发送消息给申请人
                                    TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                                    {
                                        ReferenceSysNo = string.Format("applyType={0}&applyID={1}", obj.ToString(), applyid),
                                        FromUser = Convert.ToInt32(dt.Rows[0]["InUser"]),
                                        InUser = Convert.ToInt32(dt.Rows[0]["FromUser"]),
                                        MsgType = 36,
                                        MessageContent = dt.Rows[0]["MessageContent"].ToString() + "已通过！",
                                        QueryCondition = dt.Rows[0]["QueryCondition"].ToString(),
                                        ToRole = "0",
                                        Status = "A",
                                        IsDone = "B"
                                    };
                                    new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                                }

                            }

                            context.Response.Write("1");
                        }
                    }

                }
            }
            else if (action == "seltime")//查询弹性上下班
            {
                string userid = context.Request.QueryString["userid"] ?? "0";
                string months = context.Request.QueryString["months"] ?? "0";
                string years = context.Request.QueryString["years"] ?? "0";
                if (!string.IsNullOrEmpty(userid))
                {
                    List<TG.Model.cm_PersonAttendSet> list_pas = new TG.BLL.cm_PersonAttendSet().GetModelList(" attend_month=" + months + " and attend_year=" + years + " and mem_ID=" + userid);
                    if (list_pas != null && list_pas.Count > 0)
                    {
                        context.Response.Write("{\"towork\":\"" + list_pas[0].ToWork + "\",\"offwork\":\"" + list_pas[0].OffWork + "\"}");
                    }
                }
            }
            else if (action == "seluser")//部门ID查询人员
            {
                string unitid = context.Request.QueryString["unitid"] ?? "0";
                string year = context.Request.QueryString["year"] ?? (DateTime.Now.Year.ToString());
                string pp = context.Request.QueryString["previewPower"] ?? "0";
                string userid = context.Request.QueryString["userid"] ?? "0";
                string month = context.Request.QueryString["month"] ?? "";
                if (!string.IsNullOrEmpty(unitid))
                {
                    
                    string where = " 1=1 ";
                    if (unitid!="-1")
                    {
                        where = where+" and mem_Unit_ID=" + unitid + " ";
                    }
                   //个人权限
                   if (pp=="0")
                   {
                       where = where+" and mem_Unit_ID=" + unitid + " and mem_id=" + userid + " ";
                   }
                    if (!string.IsNullOrEmpty(month))
                    {
                        where = where + " and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where convert(varchar(10),mem_OutTime,120)>='" + year + "-"+(month.PadLeft(2,'0'))+"-16')) order by mem_Order asc,mem_ID asc";

                    }
                    else
                    {
                        where = where + " and (mem_isFired=0 or mem_ID in (select mem_ID from tg_memberExt where year(mem_OutTime)>=" + year + ")) order by mem_Order asc,mem_ID asc";
                    }
                    DataTable dt = new TG.BLL.tg_member().GetList(where).Tables[0];
                    context.Response.Write(TableToJson(dt));
                }
            }
            else if (action == "addUnitOvertime")//部门加班补助统计
            {
                TG.BLL.cm_ApplyUnitOvertime bll_auo=new TG.BLL.cm_ApplyUnitOvertime();
                List<TG.Model.cm_ApplyUnitOvertime> list_auo=new List<TG.Model.cm_ApplyUnitOvertime>();
                string data = context.Request["data"] ?? "";
                if (!string.IsNullOrEmpty(data))
                {
                    list_auo = JsonConvert.DeserializeObject<List<TG.Model.cm_ApplyUnitOvertime>>(data);
                    if (list_auo != null)
                    {
                        list_auo.ForEach(auo =>
                        {
                            TG.Model.cm_ApplyUnitOvertime model_auo = bll_auo.GetModelWhere(" mem_id=" + auo.mem_ID + " and OverYear=" + auo.OverYear + " and OverMonth=" + auo.OverMonth + "");
                            if (model_auo != null)
                            {
                                model_auo.QuotaMoney = auo.QuotaMoney;
                                model_auo.Quotiety = auo.Quotiety;
                                bll_auo.Update(model_auo);
                            }
                            else
                            {
                                auo.addUserId = UserSysNo;
                                auo.addDate = DateTime.Now;
                                bll_auo.Add(auo);
                            }


                        });
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("2");
                    }
                }
                else
                {
                    context.Response.Write("2");
                }
            }
            else if (action == "updatedata") //统计表数据手动修改
            {
                TG.BLL.cm_ApplyStatisData bll_auo = new TG.BLL.cm_ApplyStatisData();
                List<TG.Model.cm_ApplyStatisData> list_auo = new List<TG.Model.cm_ApplyStatisData>();
                string data = context.Request["data"] ?? "";
                if (!string.IsNullOrEmpty(data))
                {
                    list_auo = JsonConvert.DeserializeObject<List<TG.Model.cm_ApplyStatisData>>(data);
                    if (list_auo != null)
                    {
                        list_auo.ForEach(auo =>
                        {
                            string sqlwhere = "mem_id=" + auo.mem_id + " and dataYear=" + auo.dataYear + " and dataMonth=" + auo.dataMonth + " and dataType='" + auo.dataType + "' and dataSource='"+auo.dataSource+"' ";
                            //考勤统计详细
                            if (auo.dataSource == "StatisDetail")
                            {
                                sqlwhere = sqlwhere + " and dataDay="+auo.dataDay+"";
                            }
                            TG.Model.cm_ApplyStatisData model_auo = bll_auo.GetModelWhere(sqlwhere);
                            if (model_auo != null)
                            {
                                model_auo.dataValue = auo.dataValue;
                                model_auo.dataContent = auo.dataContent;
                                bll_auo.Update(model_auo);
                            }
                            else
                            {
                                auo.addUserId = UserSysNo;
                                auo.addDate = DateTime.Now;
                                bll_auo.Add(auo);
                            }

                        });
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("2");
                    }
                }
                else
                {
                    context.Response.Write("2");
                }
            }
            else if (action == "GetDownloadState") //获取导出Excel状态
            {
                if (context.Session["SamplePictures"] != null && ((bool)context.Session["SamplePictures"]))
                {
                    context.Response.Write("1");
                    context.Session.Remove("SamplePictures");
                }
                else
                {
                    context.Response.Write("0");
                }

            }
            else if (action == "lock")//锁定
            {                 
                string data = context.Request["data"] ?? "";
                string source = context.Request["source"] ?? "";
                if (!string.IsNullOrEmpty(data))
                {
                    
                    //考勤统计
                    if (source == "ApplyStatisDetail")
                    {
                        TG.BLL.cm_ApplyStatisDetailHis bll_auo = new TG.BLL.cm_ApplyStatisDetailHis();
                        List<TG.Model.cm_ApplyStatisDetailHis> list_auo = JsonConvert.DeserializeObject<List<TG.Model.cm_ApplyStatisDetailHis>>(data);

                        if (list_auo != null)
                        {
                            string memids = string.Join(",", list_auo.Select(b => b.mem_id.ToString()).Distinct().ToArray<string>());                           
                            //最后一行是合计
                            string sqlwhere = "mem_id in (" + memids + ") and (convert(varchar(10),dataDate,23) between '" + list_auo[0].dataDate + "' and '" + list_auo[list_auo.Count - 2].dataDate + "' or  dataDate='" + (list_auo[list_auo.Count - 2].dataDate.Substring(0,7)) + "合计')";
                            //删除
                           int count= bll_auo.GetModelList(sqlwhere).Count;
                           if (count > 0)
                           {
                               TG.DBUtility.DbHelperSQL.ExecuteSql("delete from cm_ApplyStatisDetailHis where " + sqlwhere);
                           }
                            //添加
                            list_auo.ForEach(auo =>
                            {                             
                                bll_auo.Add(auo);
                            });

                            context.Response.Write("1");
                        }
                    }
                    else if (source == "YearPaidStatis") //年假以及带薪假统计
                    {
                        TG.BLL.cm_YearPaidStatisHis bll_auo = new TG.BLL.cm_YearPaidStatisHis();
                        List<TG.Model.cm_YearPaidStatisHis> list_auo = JsonConvert.DeserializeObject<List<TG.Model.cm_YearPaidStatisHis>>(data);

                        if (list_auo != null)
                        {
                            string memids = string.Join(",", list_auo.Select(b => b.mem_id.ToString()).ToArray<string>());                           
                            //最后一行是合计
                            string sqlwhere = "mem_id in (" + memids + ") and dataDate='" + list_auo[0].dataDate + "'";
                            //删除
                           int count= bll_auo.GetModelList(sqlwhere).Count;
                           if (count > 0)
                           {
                               TG.DBUtility.DbHelperSQL.ExecuteSql("delete from cm_YearPaidStatisHis where " + sqlwhere);
                           }
                            //添加
                            list_auo.ForEach(auo =>
                            {                             
                                bll_auo.Add(auo);
                            });

                            context.Response.Write("1");
                        }
                    }
                    else if (source == "YearAllStatis") //年假统计
                    {
                        TG.BLL.cm_YearAllStatisHis bll_auo = new TG.BLL.cm_YearAllStatisHis();
                        List<TG.Model.cm_YearAllStatisHis> list_auo = JsonConvert.DeserializeObject<List<TG.Model.cm_YearAllStatisHis>>(data);

                        if (list_auo != null)
                        {
                            string memids = string.Join(",", list_auo.Select(b => b.mem_id.ToString()).ToArray<string>());                           
                            //最后一行是合计
                            string sqlwhere = " mem_id in (" + memids + ") and dataDate='" + list_auo[0].dataDate + "'";
                            //删除
                           int count= bll_auo.GetModelList(sqlwhere).Count;
                           if (count > 0)
                           {
                               TG.DBUtility.DbHelperSQL.ExecuteSql("delete from cm_YearAllStatisHis where " + sqlwhere);
                           }
                            //添加
                            list_auo.ForEach(auo =>
                            {                             
                                bll_auo.Add(auo);
                            });

                            context.Response.Write("1");
                        }
                    }
                    else if (source == "MonthSummary") //月加班
                    {
                        TG.BLL.cm_MonthSummaryHis bll_auo = new TG.BLL.cm_MonthSummaryHis();
                        List<TG.Model.cm_MonthSummaryHis> list_auo = JsonConvert.DeserializeObject<List<TG.Model.cm_MonthSummaryHis>>(data);

                        if (list_auo != null)
                        {
                            string memids = string.Join(",", list_auo.Select(b => b.mem_id.ToString()).ToArray<string>());                           
                            //最后一行是合计
                            string sqlwhere = " mem_id in (" + memids + ") and dataDate='" + list_auo[0].dataDate + "'";
                            //删除
                           int count= bll_auo.GetModelList(sqlwhere).Count;
                           if (count > 0)
                           {
                               TG.DBUtility.DbHelperSQL.ExecuteSql("delete from cm_MonthSummaryHis where " + sqlwhere);
                           }
                            //添加
                            list_auo.ForEach(auo =>
                            {                             
                                bll_auo.Add(auo);
                            });

                            context.Response.Write("1");
                        }
                    }
               
                }
                else
                {
                    context.Response.Write("2");
                }
            }
            else if (action == "SelApplyInfo")//消息ID查询考勤申请记录
            {
                string msgSysNo = Request["msgSysNo"] ?? "";
                if (!string.IsNullOrEmpty(msgSysNo))
                {
                    DataTable dt = TG.DBUtility.DbHelperSQL.Query("select a.*,(select mem_Name from tg_member where mem_ID=a.adduser) as memname from cm_ApplyInfo a inner join cm_SysMsg m on m.ReferenceSysNo='applyID='+cast(a.ID as varchar(8000)) where m.SysNo=" + msgSysNo).Tables[0];
                    context.Response.Write(TableToJson(dt));
                }
            }


        }
        //table转化为json
        protected string TableToJson(DataTable dt)
        {
            StringBuilder jsonbuild = new StringBuilder();
            jsonbuild.Append("{\"");
            jsonbuild.Append(dt.TableName.Trim());
            jsonbuild.Append("\":[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonbuild.Append("\"");
                    jsonbuild.Append(Regex.Replace(dt.Columns[j].ColumnName.Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\":\"");
                    jsonbuild.Append(Regex.Replace(dt.Rows[i][j].ToString().Trim(), "[\"]", "'").Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"));
                    jsonbuild.Append("\",");
                }
                jsonbuild.Remove(jsonbuild.Length - 1, 1);
                jsonbuild.Append("},");
            }
            jsonbuild.Remove(jsonbuild.Length - 1, 1);
            jsonbuild.Append("]");
            jsonbuild.Append("}");
            return jsonbuild.ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}