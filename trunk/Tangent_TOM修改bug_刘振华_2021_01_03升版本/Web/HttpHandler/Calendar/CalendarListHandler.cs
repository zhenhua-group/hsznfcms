﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections.Specialized;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
using System.Configuration;


namespace TG.Web.HttpHandler.Calendar
{
    public abstract class CalendarListHandler : IHttpHandler
    {//请假申请列表
        public abstract string ProcName { get; }
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }
        //登录人
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.Params["action"];
            //申请类型
            string applytype = context.Request.Params["applytype"] ?? "";
            //是否已处理
            string isdone = context.Request.Params["isdone"] ?? "";

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.Params["strwhere"] ?? "");
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {
                string year = DateTime.Now.Year.ToString();
                string sqlwhere = " and year(addtime)=" + year + " and month(addtime)="+DateTime.Now.Month+" and applytype='" + applytype + "' and (isdone<>'D' or isdone is null) ";


                sqlwhere = sqlwhere + strWhere;
                parameters.Where = sqlwhere;

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "sel")//查询
            {
                string unit = context.Request.Params["unit"];
                string year = context.Request.Params["year"];
                string month = context.Request.Params["month"];
                string user = context.Request.Params["user"];
                string drpaudit = context.Request.Params["drpaudit"] ?? "";
                StringBuilder strsql = new StringBuilder(" and applytype='" + applytype + "' and (isdone<>'D' or isdone is null)");

                //按生产部门查询         
                if (!string.IsNullOrEmpty(unit) && unit != "-1")
                {
                    strsql.AppendFormat(" AND (adduser in (select mem_ID from tg_member where mem_Unit_ID={0})) ", unit);
                }
                //人员
                if (!string.IsNullOrEmpty(user) && user != "-1")
                {
                    strsql.AppendFormat(" AND (adduser={0}) ", user);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(addtime)={0} ", year);
                }
                //月份
                if (!string.IsNullOrEmpty(month) && month != "-1")
                {
                    strsql.AppendFormat(" AND month(addtime)={0} ", month);
                }

                //查看需审批记录
                if (!string.IsNullOrEmpty(drpaudit) && drpaudit != "-1")
                {
                    TG.Model.tg_member mem = new TG.BLL.tg_member().GetModel(Convert.ToInt32(UserInfo["memid"]));
                    if (mem != null)
                    {
                        //特殊人
                        string SpecialPerson = ConfigurationManager.AppSettings["SpecialPerson"];
                        string SpecialPersonAudit = ConfigurationManager.AppSettings["SpecialPersonAudit"];
                        string[] SpecialPersonList = SpecialPerson.Split('|');
                        string[] SpecialPersonAuditList = SpecialPersonAudit.Split(',');
                        int ind = -1;
                        for (int i = 0; i < SpecialPersonAuditList.Length; i++)
                        {
                            if (SpecialPersonAuditList[i] == mem.mem_Name)
                            {
                                ind = i;
                            }
                        }
                        SpecialPerson = (ind > -1 ? SpecialPersonList[ind] : "''");
                        SpecialPerson = "'" + (SpecialPerson.Replace(",", "','")) + "'";
                        strWhere = " and ((pa.Status='A' and umc.unitusers=" + UserInfo["memid"] + ") or (umc.managerusers=" + UserInfo["memid"] + " and (P.totaltime/7.5)>=5 and pa.Status='B')  or (pa.Status='A' and m.mem_Name in (" + SpecialPerson + ")))";
                    }
                }

                parameters.Where = strsql.ToString() + strWhere;

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "selDone")//查询
            {
                string unit = context.Request.Params["unit"];
                string year = context.Request.Params["year"];
                string month = context.Request.Params["month"];
                string user = context.Request.Params["user"];

                StringBuilder strsql = new StringBuilder(" and applytype='" + applytype + "' and isdone='D'");

                //按生产部门查询         
                if (!string.IsNullOrEmpty(unit) && unit != "-1")
                {
                    strsql.AppendFormat(" AND (adduser in (select mem_ID from tg_member where mem_Unit_ID={0})) ", unit);
                }
                //人员
                if (!string.IsNullOrEmpty(user) && user != "-1")
                {
                    strsql.AppendFormat(" AND (adduser={0}) ", user);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(addtime)={0} ", year);
                }
                //月份
                if (!string.IsNullOrEmpty(month) && month != "-1")
                {
                    strsql.AppendFormat(" AND month(addtime)={0} ", month);
                }

                parameters.Where = strsql.ToString() + strWhere;

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strOper == "del")//删除
            {
                string strResponse = "";
                TG.BLL.cm_ApplyInfo bll = new TG.BLL.cm_ApplyInfo();
                TG.Model.cm_ApplyInfo model = new TG.Model.cm_ApplyInfo();
                string strid = forms.Get("EmpId");
                string applymanager = System.Configuration.ConfigurationManager.AppSettings["ApplyManager"] ?? "";
                int fruser = 0;
                if (!string.IsNullOrEmpty(applymanager))
                {
                    object o = TG.DBUtility.DbHelperSQL.GetSingle("select mem_ID from tg_member where mem_Name='" + applymanager + "'");
                    if (o != null)
                    {
                        fruser = Convert.ToInt32(o);
                    }
                }
                ArrayList listAudit = bll.DeleteList(strid);
                //存在审批中的合同
                if (listAudit.Count > 0)
                {
                    string message = "";

                    for (int i = 0; i < listAudit.Count; i++)
                    {
                        model = bll.GetModel(int.Parse(listAudit[i].ToString()));
                        if (model != null)
                        {
                            string memname = new TG.BLL.tg_member().GetModel(model.adduser).mem_Name;
                            message = message + memname + model.addtime + "该申请已被审批，请向考勤管理员确认！\r\n ";
                            //发送消息给管理员
                            TG.Model.SysMessageViewEntity msg = new TG.Model.SysMessageViewEntity
                            {
                                ReferenceSysNo = string.Format("applyID={0}", listAudit[i]),
                                FromUser = fruser,
                                InUser = Convert.ToInt32(UserInfo["memid"]),
                                MsgType = 36,
                                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", (memname + model.addtime + "请假申请"), "删除审批"),
                                QueryCondition = (model.addtime + memname + "申请的请假删除"),
                                ToRole = "0",
                                Status = "A",
                                IsDone = "A"
                            };
                            int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
                        }
                    }
                    // message = message + "不能删除";
                    strResponse = message;
                }
                else
                {
                    strResponse = "删除成功";
                }

                context.Response.Write(strResponse);
            }
        }
        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        public bool IsReusable
        {
            get { return false; }
        }

    }
}