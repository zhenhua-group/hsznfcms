﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Collections.Specialized;
using System.Collections;
using System.Text.RegularExpressions;

namespace TG.Web.HttpHandler.Calendar
{
    /// <summary>
    /// ApplyStatisHandler 的摘要说明
    /// </summary>
    public class ApplyStatisHandler : IHttpHandler
    {
        //人员列表
        public string ProcName { get { return "P_cm_Member_jq"; } }
     
        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }
        //登录人
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.Params["action"];
          

            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.Params["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = ProcName,
                    Where = strWhere
                };
            }


            //加载数据
            if (strOper == null && strAction == null)
            {

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "sel")//查询
            {
                string unit = context.Request.QueryString["unit"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND (mem_Name like '%{0}%'  or mem_Login Like '%{0}%')", keyname);
                }
                //按生产部门查询
                if (unit != "-1" && unit != null)
                {
                    strsql.AppendFormat(" AND (mem_Unit_ID= {0})", unit);
                }

                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();

                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
        }
        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        public bool IsReusable
        {
            get { return false; }
        }

    }
}