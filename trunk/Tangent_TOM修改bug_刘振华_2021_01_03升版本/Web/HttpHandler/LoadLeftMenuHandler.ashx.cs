﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using TG.BLL;
using TG.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.Configuration;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class LoadLeftMenuHandler : HandlerCommon, IHttpHandler, IRequiresSessionState
    {
        public LeftMenuType LeftMenu
        {
            get
            {
                LeftMenuType leftMenuType = (LeftMenuType)Enum.Parse(typeof(LeftMenuType), Request["leftMenuType"]);
                return leftMenuType;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            if (Request.HttpMethod == "POST")
            {
                string flag = Request.Params["action"] ?? "";
                if (flag == "chk")
                {
                    string config_user = ConfigurationManager.AppSettings["MenuControl"].ToString();
                    if (UserSysName == config_user)
                    {

                        Response.Write("true");
                    }
                    else
                    {
                        Response.Write("false");
                    }
                }
                else
                {
                    GetLeftMenu();
                }
            }
            Response.End();
        }

        private void GetLeftMenu()
        {
            LoadLeftMenuBP bp = new LoadLeftMenuBP();
            List<LeftMenuViewEntity> leftMenuList = bp.GetLeftMenuList(LeftMenu, UserSysNo);
            string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(leftMenuList);
            Response.Write(jsonResult);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
