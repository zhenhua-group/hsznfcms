﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.SessionState;

namespace TG.Web.HttpHandler
{
    public class HandlerCommon : IRequiresSessionState
    {
        public HttpRequest Request { get { return HttpContext.Current.Request; } }

        public HttpResponse Response { get { return HttpContext.Current.Response; } }

        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        protected int UserSysNo
        {
            get
            {
                return UserInfo["memid"] == null ? 1 : Convert.ToInt32(UserInfo["memid"]);
            }
        }

        protected string UserSysName
        {
            get
            {
                return Convert.ToString(UserInfo["memlogin"] ?? "");
            }
        }
    }
}
