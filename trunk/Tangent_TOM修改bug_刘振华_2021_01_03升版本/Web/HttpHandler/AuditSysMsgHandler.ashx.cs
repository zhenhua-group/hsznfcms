﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.SessionState;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// $codebehindclassname$ 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class AuditSysMsgHandler : IHttpHandler, IRequiresSessionState
    {

        public HttpRequest Request { get { return HttpContext.Current.Request; } }

        public HttpResponse Response { get { return HttpContext.Current.Response; } }

        public HttpSessionState Session { get { return HttpContext.Current.Session; } }
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        /// <summary>
        /// 系统消息编号
        /// </summary>
        public int SysMsgSysNo
        {
            get
            {
                int sysNo = 0;
                int.TryParse(Request["SysMsgSysNo"], out sysNo);
                return sysNo;
            }
        }

        /// <summary>
        /// 用户SysNo
        /// </summary>
        public int UserSysNo
        {
            get
            {
                return this.UserInfo["memid"] == null ? 1 : Convert.ToInt32(this.UserInfo["memid"]);
            }
        }

        /// <summary>
        /// 合同当前状态
        /// </summary>
        public string Status
        {
            get
            {
                return Request["Status"];
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            UpdateSysMsgStatus();
        }

        private void UpdateSysMsgStatus()
        {
            TG.BLL.cm_SysMsg sysMsgBll = new TG.BLL.cm_SysMsg();

            //表示当前合同状态为总经理审核完毕
            //if ((!string.IsNullOrEmpty(Status)) && (Status == "H" || Status == "I"))
            //{
            sysMsgBll.UpdateSysMsgStatus(SysMsgSysNo);
            //}

            //查询消息记录
            int count = sysMsgBll.GetSysMsgCount(UserSysNo);

            Response.Write(count);
            Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
