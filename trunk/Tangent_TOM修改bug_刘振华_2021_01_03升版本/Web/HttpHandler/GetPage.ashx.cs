﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// GetPage 的摘要说明
    /// </summary>
    public class GetPage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            TG.BLL.TblAreaBll bll = new BLL.TblAreaBll();
            string str = bll.GetToAspx();
            str = "../LeadershipCockpit/" + str;
            context.Response.Write(str);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}