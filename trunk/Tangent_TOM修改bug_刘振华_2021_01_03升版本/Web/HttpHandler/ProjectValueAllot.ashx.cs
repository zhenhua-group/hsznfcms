﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Text;



namespace TG.Web.HttpHandler
{
    /// <summary>
    /// ProjectValueAllot 的摘要说明
    /// </summary>
    public class ProjectValueAllot : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            TG.BLL.tg_member copcon = new BLL.tg_member();

            TG.BLL.StandBookBp standbll = new BLL.StandBookBp();

            context.Response.ContentType = "text/plain";
            string action = context.Request.QueryString["action"];
            if (action == "1")
            {
                string year = context.Request["year"].ToString().Trim();
                int unitid = Convert.ToInt32(context.Request["unitid"]);
                int UserSysNo = Convert.ToInt32(context.Request["memid"]);
                int UserUnitNo = Convert.ToInt32(context.Request["UserUnitNo"]);
                int PreviewPattern = int.Parse(context.Request["pp"]);
                StringBuilder strWhere = new StringBuilder("");

                string unitName = "", sqlwhere = "",strwhere1 = "", strwhere2 = "";
             
                TG.Model.tg_unit tgunit = new TG.Model.tg_unit();
                if (unitid > 0)
                {
                    //sqlwhere = sqlwhere+" and mem_Unit_ID=" + unitid + "";
                    tgunit = new TG.BLL.tg_unit().GetModel(unitid);
                }
                if (tgunit != null)
                {
                    unitName = tgunit.unit_Name.Trim();
                }
                //按照部门
                if (!string.IsNullOrEmpty(unitName))
                {
                    strWhere.AppendFormat(" AND (p.Unit= '{0}')", unitName);
                }
                //按照年份
                if (int.Parse(year) > 0)
                {
                    strwhere1 = " and year(InAcountTime)=" + year;
                    strwhere2 = " and ActualAllountTime='" + year + "'";
                    sqlwhere = " and pva.ActualAllountTime='" + year + "'";
                }
#region  权限

                //个人
               // if (PreviewPattern == 0)
              //  {
               //     sqlwhere = sqlwhere + " and pvb.mem_ID=" + UserSysNo + " and  pvb.IsExternal=0";
               //     strWhere.Append(" AND p.InsertUserID =" + UserSysNo + "");
               // }
                //部门
               // else if (PreviewPattern == 2)
               // {
                //    sqlwhere = sqlwhere + " and pvb.mem_ID in (select mem_ID from tg_member where mem_Unit_ID=" + UserUnitNo + " union all select mem_ID from cm_ExternalMember where mem_Unit_ID=" + UserUnitNo + ")";
                 //   strWhere.Append(" AND p.InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
                //  }
#endregion
                List<TG.Model.ProjectValueAllot> listSC = standbll.GetMemberProjectValueAllotProc(strWhere.ToString(), strwhere1, strwhere2);

                string td1 = "<tr><td colspan='2' style='font-weight:bold;width:120px;'>项目</td>";
                string td2 = "<tr><td colspan='2' style='font-weight:bold;'>实收产值(万元)</td>";
                string td3 = "<tr><td colspan='2' style='font-weight:bold;'>转经济所(万元)</td>";
                string td3_1 = "<tr><td colspan='2' style='font-weight:bold;'>转暖通所(万元)</td>";
                string td3_2 = "<tr><td colspan='2' style='font-weight:bold;'>转土建所(万元)</td>";
                string td4 = "<tr><td colspan='2' style='font-weight:bold;'>本所产值(万元)</td>";
                string td5 = "<tr><td colspan='2' style='font-weight:bold;'>所留(万元)</td>";
                string td6 = "<tr style='font-weight:bold;'><td>序号</td><td>姓名</td>";
                string tdend = "<tr style='font-weight:bold;'><td colspan='2'>总计：</td>";
                decimal SumCharge = 0, SumWeiFenValue = 0, SumToOtherValue = 0, SumActualValue = 0, SumHavcValue = 0, SumTranBulidingValue = 0;
                foreach (TG.Model.ProjectValueAllot pv in listSC)
                {
                    td1 = td1 + "<td style='width:120px;'>" + pv.CprName + "</td>";
                    td2 = td2 + "<td>" + pv.Charge.ToString("f4") + "</td>";
                    td3 = td3 + "<td>" + pv.EconomyValue.ToString("f4") + "</td>";
                    td3_1 = td3_1 + "<td>" + pv.HavcValue.ToString("f4") + "</td>";
                    td3_2 = td3_2 + "<td>" + pv.TranBulidingValue.ToString("f4") + "</td>";
                    td4 = td4 + "<td>" + pv.UnitValue.ToString("f4") + "</td>";
                    td5 = td5 + "<td>" + pv.TheDeptValue.ToString("f4") + "</td>";                   
                    td6 = td6 + "<td>(元)</td>";
                    SumCharge = SumCharge + pv.Charge;
                    SumWeiFenValue = SumWeiFenValue + pv.EconomyValue;
                    SumHavcValue = SumHavcValue + pv.HavcValue;
                    SumTranBulidingValue = SumTranBulidingValue + pv.TranBulidingValue;
                    SumToOtherValue = SumToOtherValue + pv.UnitValue;
                    SumActualValue = SumActualValue + pv.TheDeptValue;
                }
                td1 = td1 + "<td style='width:120px;'>一次分配产值</td><td style='width:120px;'>二次分配产值</td><td style='width:120px;'>方案补贴</td><td style='font-weight:bold;width:120px;'>总计(万元)</td><td style='width:100px;'>人员名称</td></tr>";
                td2 = td2 + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumCharge.ToString("f2") + "</td><td>&nbsp;</td></tr>";
                td3 = td3 + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumWeiFenValue.ToString("f2") + "</td><td>&nbsp;</td></tr>";
                td3_1 = td3_1 + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumHavcValue.ToString("f2") + "</td><td>&nbsp;</td></tr>";
                td3_2 = td3_2 + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumTranBulidingValue.ToString("f2") + "</td><td>&nbsp;</td></tr>";
                td4 = td4 + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumToOtherValue.ToString("f2") + "</td><td>&nbsp;</td></tr>";
                td5 = td5 + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style='font-weight:bold;'>" + SumActualValue.ToString("f2") + "</td><td>&nbsp;</td></tr>";
                td6 = td6 + "<td>(元)</td><td>(元)</td><td>(元)</td><td>(元)</td><td>&nbsp;</td></tr>";

                string td = "";
                //总纵向统计
                decimal sumall = 0;
                //总二次分配产值
                decimal sumercz = 0;
                //总方案补贴
                decimal sumsbcz = 0;
                //总一次分配产值
                decimal sumonecz = 0;
#region 暂无用

                //描述:统计所有项目参与的所有人员 zxq  20131224
//                string sql = string.Format(@"select * from (
//                                            select pvb.mem_ID as mem_ID,
//                                            (case when IsExternal='0' then b.mem_Name else c.mem_Name end) as mem_Name,
//				  		                    (case when IsExternal='0' then b.mem_Unit_ID else c.mem_Unit_ID end) as mem_Unit_ID,
//                                            (case when IsExternal='1' then '2' when IsExternal='0' and b.mem_Unit_ID<>{0} then '1' else '0' end ) as ordtype,
//                                            IsExternal as mem_type
//                                            From cm_project p join cm_ProjectValueAllot pva on pva.pro_ID=p.pro_ID join  cm_ProjectValueByMemberDetails pvb on pva.ID=pvb.AllotID left join tg_member b on pvb.mem_ID=b.mem_ID left join cm_externalMember c on pvb.mem_ID=c.mem_ID 
//                                            Where pvb.[Status]='S' {1} {2} group by pvb.IsExternal,b.mem_Unit_ID,b.mem_Name,c.mem_Unit_ID,c.mem_Name,pvb.mem_ID
//                                        ) aa 
//                                    order by aa.ordtype asc,aa.mem_ID asc", unitid, strWhere.ToString(), sqlwhere);
//                DataTable listme = DBUtility.DbHelperSQL.Query(sql).Tables[0];
                // List<TG.Model.tg_member> listme = copcon.GetModelList(sqlwhere);
#endregion

                //描述:统计所有项目参与的所有人员 zxq  20131225
                DataTable listme = new TG.BLL.StandBookBp().GetMemberProjectValueAllotDetailProc(unitName, unitid, year);
                if (listme!=null&&listme.Rows.Count>0)
                {
              
                    for (int i = 0; i < listme.Rows.Count; i++)
                    {
                        string uname = listme.Rows[i]["mem_Name"].ToString();
                        if (listme.Rows[i]["ordtype"].ToString() == "2")
                        {
                            uname = uname + "(外聘)";
                        }
                        else if (listme.Rows[i]["ordtype"].ToString() == "1")
                        {
                            uname = uname + "(外所)";
                        }
                        string innertr = "<tr><td>" + (i + 1) + "</td><td>" + uname + "</td>";
                        decimal summoney = 0; //一次分配产值项目横向总和
                        decimal summoneyend = 0;//最后一列横向之和
                        foreach (TG.Model.ProjectValueAllot pv in listSC)
                        {
                            //描述:第一次分配产值 zxq  20131225
                            //decimal sumvalue = standbll.GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(), year);
                           
                            if ( listme.Columns.Contains("pro_" + pv.CprId.ToString() + ""))
                            {
                                decimal sumvalue = Convert.ToDecimal(listme.Rows[i]["pro_" + pv.CprId.ToString() + ""]);
                                innertr = innertr + "<td>" + sumvalue.ToString("f2") + "</td>";
                                summoney = summoney + sumvalue;
                            }
                            else
                            {
                                decimal sumvalue = 0;
                                innertr = innertr + "<td>0</td>";
                                summoney = summoney + sumvalue;
                            }
                    
                        }
                        //查询人员的方案补贴
                       // decimal sbcz = standbll.GetMemberProjectComValue(" and ValueYear='" + year + "' and MemberId=" + listme.Rows[i]["mem_ID"] + " and UnitId=" + unitid + "");
                        //描述:查询人员的方案补贴  zxq  20131225
                        decimal sbcz = Convert.ToDecimal(listme.Rows[i]["ComValue"]);
                        //查询人员二次分配产值
                       // decimal ercz = standbll.GetMemberProjectValueAllotSql("2", listme.Rows[i]["mem_ID"].ToString(), "", year);
                        //描述:查询人员二次分配产值  zxq  20131225
                        decimal ercz = Convert.ToDecimal(listme.Rows[i]["totalcount2"]);
                        //最后一行一次分配产值之和
                        sumonecz = sumonecz + summoney;
                        //最后一行方案补贴之和
                        sumsbcz = sumsbcz + sbcz;
                        //最后一行二次分配产值之和
                        sumercz = sumercz + ercz;
                        //最后一列横向总计
                        summoneyend = summoney + sbcz + ercz;
                        //添加到tr中。
                        innertr = innertr + "<td>" + summoney.ToString("f2") + "</td><td>" + ercz.ToString("f2") + "</td><td>" + sbcz.ToString("f2") + "</td><td style='font-weight:bold;'>" + summoneyend.ToString("f2") + "</td><td>" + uname + "</td></tr>";
                        td = td + innertr;
                        //最后一列总计纵向统计之和
                        sumall = sumall + summoneyend;
                    }

                    //最后一行横向统计
                    foreach (TG.Model.ProjectValueAllot pv in listSC)
                    {
                        decimal summoney = 0;
                        for (int i = 0; i < listme.Rows.Count; i++)
                        {
                           // decimal sumvalue = standbll.GetMemberProjectValueAllotSql("1", listme.Rows[i]["mem_ID"].ToString(), pv.CprId.ToString(), year);

                            //描述:横向统计项目之和  zxq  20131225
                            if (listme.Columns.Contains("pro_" + pv.CprId.ToString() + ""))
                            {
                                decimal sumvalue = Convert.ToDecimal(listme.Rows[i]["pro_" + pv.CprId.ToString() + ""]);
                                //   summoney = summoney + sumvalue;
                                summoney = summoney + sumvalue;
                            }
                            else
                            {
                                decimal sumvalue = 0;
                                //   summoney = summoney + sumvalue;
                                summoney = summoney + sumvalue;
                            }
                        }
                        tdend = tdend + "<td>" + summoney.ToString("f2") + "</td>";
                    }
                   
                 }
                    //所补产值总计和纵向统计的显示
                tdend = tdend + "<td>" + sumonecz.ToString("f2") + "</td><td>" + sumercz.ToString("f2") + "</td><td>" + sumsbcz.ToString("f2") + "</td><td>" + sumall.ToString("f2") + "</td><Td>&nbsp;</td></tr>";
                JavaScriptSerializer js = new JavaScriptSerializer();
                string json = td1 + td2 + td3 + td3_1 + td3_2 + td4 + td5 + td6 + td + tdend;
                context.Response.Write(json);
            }
           

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}