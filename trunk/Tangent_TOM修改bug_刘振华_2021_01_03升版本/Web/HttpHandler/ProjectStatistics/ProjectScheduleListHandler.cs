﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler.ProjectStatistics
{
    public abstract class ProjectScheduleListHandler : IHttpHandler
    { //存储过程名
        public abstract string ProcName { get; }

        //存储过程参数
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }

        public virtual void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            //获取jqGrid对象提交参数
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            string strWhere = "";
            //自定义url参数
            string strAction = context.Request.QueryString["action"];

            strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
            parameters = new TG.BLL.QueryPagingParameters
            {
                PageIndex = int.Parse(context.Request["PageIndex"]),
                PageSize = int.Parse(context.Request["PageSize"]),
                OrderBy = context.Request["OrderBy"],
                Sort = context.Request["Sort"],
                Where = strWhere,
                ProcedureName = ProcName
            };

            //加载数据
            if (strOper == null && strAction == null)
            {
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            else if (strAction == "sel")
            {

                string unit = context.Request.QueryString["unit"];
                string year = context.Request.QueryString["year"];
                string keyname = context.Request.QueryString["keyname"];
                StringBuilder strsql = new StringBuilder("");
                //按名称查询
                if (!string.IsNullOrEmpty(keyname))
                {
                    keyname = TG.Common.StringPlus.SqlSplit(keyname);
                    strsql.AppendFormat(" AND a.pro_Name like '%{0}%'", keyname);
                }
                //按生产部门查询
                if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
                {
                    strsql.AppendFormat(" AND (Unit= '{0}')", unit);
                }
                //年份
                if (!string.IsNullOrEmpty(year) && year != "-1")
                {
                    strsql.AppendFormat(" AND year(a.pro_startTime)={0} ", year);
                }
                strsql.Append(strWhere);

                parameters.Where = strsql.ToString();
                //查询
                this.PagingParameters = parameters;

                context.Response.Write(GetJsonResult());
            }
        }

        //返回json
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}