﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// RemindHandler 的摘要说明
    /// </summary>
 
    public class RemindHandler : HandlerCommon, IHttpHandler
    {
        protected TG.BLL.QueryPagingParameters PagingParameters { get; set; }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //Action 
            string flag = context.Request.Params["action"] ?? "";
            NameValueCollection forms = context.Request.Form;
            string strOper = forms.Get("oper");
            TG.BLL.QueryPagingParameters parameters = null;
            TG.BLL.InterView bll = new TG.BLL.InterView();
            TG.Model.InterView model = new TG.Model.InterView();
            string strWhere = "";
            //加载数据   
            if (strOper != "del")
            {
                strWhere = System.Web.HttpUtility.UrlDecode(context.Request.QueryString["strwhere"]);
                parameters = new TG.BLL.QueryPagingParameters
                {
                    PageIndex = int.Parse(context.Request["PageIndex"]),
                    PageSize = int.Parse(context.Request["PageSize"]),
                    OrderBy = context.Request["OrderBy"],
                    Sort = context.Request["Sort"],
                    ProcedureName = "RemindDate_jq",
                    Where = strWhere
                };
            }
            if (strOper == null && flag == "")
            {
                parameters.Where = " ";
                parameters.ProcedureName = "RemindDate_jq";
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
            if (flag == "sel")
            {
                StringBuilder sub = new StringBuilder(" ");
                string Unit = context.Request.Params["unit"] ?? "";
                string keyname = context.Request.Params["keyname"] ?? "";
                if (Unit.Trim() != "全院部门")
                {
                    sub.Append("AND Interview_Unit='" + Unit + "'");
                }
                 if (keyname.Trim() != "")
                {
                    sub.Append("AND Interview_Name='" + keyname + "'");
                }
                parameters.Where = sub.ToString();
                parameters.ProcedureName = "RemindDate_jq";
                this.PagingParameters = parameters;
                context.Response.Write(GetJsonResult());
            }
        }
        public virtual string GetJsonResult()
        {
            DataTable dt = TG.BLL.HttpHandlerBLL.ExecuteDataset(PagingParameters.ProcedureName, PagingParameters.ProcedureParameters).Tables[0];


            int total = PagingParameters.Total;
            int pageCount = total % PagingParameters.PageSize == 0 ? total / PagingParameters.PageSize : total / PagingParameters.PageSize + 1;

            var dataResult = new DataResult
            {
                PageIndex = PagingParameters.PageIndex,
                PageCount = pageCount,
                Total = total,
                Data = dt
            };

            string json = TG.Web.Common.JsonHelper.Serialize(dataResult);
            return json;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}