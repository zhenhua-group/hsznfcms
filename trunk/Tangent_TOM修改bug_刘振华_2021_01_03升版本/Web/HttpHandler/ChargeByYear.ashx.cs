﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TG.Web.HttpHandler
{
    /// <summary>
    /// Handler1 的摘要说明
    /// </summary>
    public class ChargeByYear : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "text/plain";

            string year = context.Request.QueryString["year"];
            string oop = context.Request.QueryString["unitid"];
            TG.BLL.ChargeByYear bllyear = new BLL.ChargeByYear();
            List<TG.Model.ChargeByYear> listSC = bllyear.ListGet(year, oop);
            JavaScriptSerializer js = new JavaScriptSerializer();
            string json = js.Serialize(listSC);
            context.Response.Write(json);
        }
        private string GetJson(string[] strObj)
        {
            string str = "[";
            for (int i = 0; i < strObj.Length; i++)
            {
                str += strObj[i] + ",";
            }

            return str.Substring(0, str.Length - 1) + "]";
        }

        private string GetJsonSingle(string[] strObj)
        {
            string str = "[";
            for (int i = 0; i < strObj.Length; i++)
            {
                str += strObj[i] + ",";
            }

            return str.Substring(0, str.Length - 2) + "]";
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}