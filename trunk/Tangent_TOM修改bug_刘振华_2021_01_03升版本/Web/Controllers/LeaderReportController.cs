﻿using Geekees.Common.Controls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TG.Model;
using TG.Web.Models;

namespace TG.Web.Controllers
{
    public class LeaderReportController : BaseController
    {
        // GET: LeaderReport
        public ActionResult ContractTotalReport()
        {
            DataTable dt = GetUnit();
            List<object> deptJson = new List<object>();
            deptJson.Add(new
            {
                id = "263",
                pId = 0,
                name = "全部部门",
            });
            //    { id: 6, pId: 0, name: "重庆" },
            //{ id: 4, pId: 0, name: "河北省", open: true, nocheck: true },
            foreach (DataRow item in dt.Rows)
            {
                deptJson.Add(new
                {
                    id = item["unit_ID"].ToString(),
                    pId = item["unit_ParentID"].ToString().ToString() == "0" ? "263" : item["unit_ParentID"].ToString(),
                    name = item["unit_Name"],
                });
            }
            ViewBag.DeptTree = JsonConvert.SerializeObject(deptJson);
            ViewBag.Year = new TG.BLL.cm_Coperation().GetCoperationYear();
            return View();
        }

        [HttpPost]
        public ActionResult ContractTotalReportSearch(ContractTotalReportSearch search)
        {
            ContractTotalReportSearchResult result = new ContractTotalReportSearchResult();
            result.StrTip = GetCountCondition(search);
            result.ContractTotalData = GetCountDataByUnit(search);
            result.DeptNames = result.ContractTotalData.Select(c => c.UnitName.Trim()).ToList();
            result.CustCount= result.ContractTotalData.Select(c =>Convert.ToDecimal( c.CustCount)).ToList();
            result.CprCount = result.ContractTotalData.Select(c => Convert.ToDecimal(c.CprCount)).ToList();
            result.ProjCount = result.ContractTotalData.Select(c => Convert.ToDecimal(c.ProjCount)).ToList();
            result.NoPlanCount = result.ContractTotalData.Select(c => Convert.ToDecimal(c.NoProjPlanCount)).ToList();
            result.PlanCount = result.ContractTotalData.Select(c => Convert.ToDecimal(c.ProjPlanCount)).ToList();
            return Json(result);
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";
            //if (base.RolePowerParameterEntity != null)
            //{
            //    //个人
            //    if (base.RolePowerParameterEntity.PreviewPattern == 0)
            //    {
            //        strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            //    }
            //    else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            //    {
            //        strWhere = " unit_ID =" + UserUnitNo;
            //    }
            //    else
            //    {
            //        strWhere = " 1=1 ";
            //    }
            //}
            //else
            //{
            //    strWhere = " unit_ID =" + UserUnitNo;
            //}
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }


        /// <summary>
        /// 统计提示
        /// </summary>
        protected string GetCountCondition(ContractTotalReportSearch search)
        {
            string strTip = "";
          
            if (search.depts==null||search.depts.Count==0)
            {
                strTip += "全院部门";
                
            }
            else
            {
                foreach (var item in search.depts)
                {
                    if (item.Key != "263")
                    {
                        strTip += "[" + item.Value + "]";
                    }
                   
                }
            }
            if (search.year > 0)
            {
                strTip += search.year.ToString() + "年";
            }
            if (search.quarter > 0)
            {
                strTip += "第" + search.quarter.ToString() + "季度";
            }
            else {
                if (search.month > 0)
                {
                    strTip += "第" + search.month.ToString() + "月份";
                }
            }
           
            strTip += "合同信息与项目信息统计表";
            return strTip;
        }

        /// <summary>
        /// 统计部门数据
        /// </summary>
        protected List<CoperationAndProjectCountExcel> GetCountDataByUnit(ContractTotalReportSearch search)
        {

            List<string> deptName = new List<string>();
            List<CoperationAndProjectCountExcel> dataList = new List<CoperationAndProjectCountExcel>();
            //全没选
            if (search.depts!=null && search.depts.Count>0)
            {
                foreach (var node in search.depts)
                {
                    if (node.Key == "0"|| node.Value=="全部部门")
                        continue;
                    //部门名称
                    string unitname = node.Value.Trim();
                    dataList.Add(CreateTableRowByUnit(unitname,search));
                }
            }
            else
            {
                //循环部门
                DataTable dt = GetUnit();
                //部门名称

                foreach (DataRow dr in dt.Rows)
                {
                    string unitname = dr["unit_Name"].ToString();
                    dataList.Add(CreateTableRowByUnit(unitname, search));
                }
               
            }
            return dataList;
        }

        /// <summary>
        /// 创建统计行
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        private TG.Model.CoperationAndProjectCountExcel CreateTableRowByUnit(string unitname, ContractTotalReportSearch search)
        {
            StringBuilder sbTable = new StringBuilder();

            DataTable dtData = GetCountData(unitname, search);
            TG.Model.CoperationAndProjectCountExcel model;
            if (dtData.Rows.Count > 0)
            {
                DataRow drData = dtData.Rows[0];
                model = new TG.Model.CoperationAndProjectCountExcel()
                {
                    UnitName = unitname,
                    CustCount = drData["CustCount"].ToString(),
                    CprCount = drData["CprCount"].ToString(),
                    ProjCount = drData["ProjCount"].ToString(),
                    ProjPlanCount = drData["NoPlanCount"].ToString(),
                    NoProjPlanCount = drData["PlanCount"].ToString()
                };
            }
            else {
                 model = new TG.Model.CoperationAndProjectCountExcel()
                {
                    UnitName = unitname,
                    CustCount = "0",
                    CprCount = "0",
                    ProjCount = "0",
                    ProjPlanCount = "0",
                    NoProjPlanCount = "0"
                };
            }
           
            return model;

        }
        /// <summary>
        /// 统计数据
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        protected DataTable GetCountData(string unitname, ContractTotalReportSearch search)
        {
            StringBuilder sbSql = new StringBuilder();
            //客户
            StringBuilder sbSqlCust = new StringBuilder();
            //合同
            StringBuilder sbSqlCop = new StringBuilder();
            //项目
            StringBuilder sbSqlProj = new StringBuilder();
            //年
            string stryear = search.year.ToString();
            //季度
            string strjidu = search.quarter.ToString();
            //月
            string stryue = search.month.ToString();

            //全部收款
            if (stryear != "0" && strjidu == "0" && stryue == "0")//全年
            {
                //客户
                sbSqlCust.AppendFormat(" AND year(InsertDate)={0} ", stryear);
                //合同
                sbSqlCop.AppendFormat(" AND year(cpr_SignDate)={0} ", stryear);
                //项目
                sbSqlProj.AppendFormat(" AND year(pro_startTime)={0} ", stryear);
            }
            else if (stryear != "0" && strjidu != "0" && stryue == "0")//某年某季度
            {
                string start = stryear;
                string end = stryear;
                switch (strjidu)
                {
                    case "1":
                        start += "-01-01 00:00:00";
                        end += "-3-31 23:59:59";
                        break;
                    case "2":
                        start += "-04-01 00:00:00";
                        end += "-6-30 23:59:59";
                        break;
                    case "3":
                        start += "-7-01 00:00:00";
                        end += "-9-30 23:59:59";
                        break;
                    case "4":
                        start += "-10-01 00:00:00";
                        end += "-12-31 23:59:59";
                        break;
                }
                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, end);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
            }
            else if (stryear != "0" && strjidu == "0" && stryue != "0") //某年某月
            {
                //当月有几天
                int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                string start = stryear + "-" + stryue + "-01 00:00:00";
                string end = stryear + "-" + stryue + "-" + days + " 00:00:00";

                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, end);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, end);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, end);
            }
            else
            {
                //默认当年收款
                string start = stryear + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "  00:00:00";
                string curtime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //客户
                sbSqlCust.AppendFormat(" AND (InsertDate BETWEEN '{0}' AND '{1}')", start, curtime);
                //合同
                sbSqlCop.AppendFormat(" AND (cpr_SignDate BETWEEN '{0}' AND '{1}')", start, curtime);
                //项目
                sbSqlProj.AppendFormat(" AND (pro_startTime BETWEEN '{0}' AND '{1}')", start, curtime);

            }

            //执行查询语句
            sbSql.AppendFormat(@"Select
	                            (
		                            Select COUNT(*)
		                            From cm_CustomerInfo
		                            Where InsertUserID IN (
								                            Select mem_ID
								                            From tg_member
								                            Where mem_Unit_ID=U.unit_ID
							        ) {0}
	                            ) AS CustCount
	                            ,(
		                            Select COUNT(*)
		                            From cm_Coperation 
		                            Where cpr_Unit=	U.unit_Name {1}
	                            ) AS CprCount
	                            ,(
		                            Select COUNT(*)
		                            From cm_Project 
		                            Where Unit=U.unit_Name {2}
	                            ) AS ProjCount
	                            ,(
		                            Select COUNT(*)
		                            From cm_Project P
		                            Where Unit=U.unit_Name AND (
									                            Select COUNT(*)
									                            From cm_ProjectPlanAudit
									                            Where ProjectSysNo= P.pro_ID
		                            )=0 {2}
	                            ) AS NoPlanCount 
	                            ,(
		                            Select COUNT(*)
		                            From cm_Project P
		                            Where Unit=U.unit_Name AND (
									                            Select COUNT(*)
									                            From cm_ProjectPlanAudit
									                            Where ProjectSysNo= P.pro_ID
		                            )>0 {2}
	                            ) AS PlanCount
                            From tg_unit U 
                            Where U.unit_Name ='{3}'", sbSqlCust.ToString(), sbSqlCop.ToString(), sbSqlProj.ToString(), unitname);

            DataTable dt = DBUtility.DbHelperSQL.Query(sbSql.ToString()).Tables[0];
            return dt;
        }


    }
}