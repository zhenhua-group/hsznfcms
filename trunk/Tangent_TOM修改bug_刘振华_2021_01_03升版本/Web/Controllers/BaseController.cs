﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TG.Model;

namespace TG.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding)
        {
            return new CustomJsonResult { Data = data, ContentType = contentType, ContentEncoding = contentEncoding };
        }

        public new JsonResult Json(object data, JsonRequestBehavior jsonRequest)
        {
            return new CustomJsonResult { Data = data, JsonRequestBehavior = jsonRequest };
        }

        public new JsonResult Json(object data)
        {
            return new CustomJsonResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        /// <summary>
        /// 
        /// </summary>
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = Request.Cookies["userinfo"];
                return user;
            }
        }
        /// <summary>
        /// 登录用户简称
        /// </summary>
        protected string UserShortName
        {
            get
            {
                object objUserShortName = this.UserInfo["memlogin"];
                return objUserShortName == null ? "" : objUserShortName.ToString();
            }
        }
        public string CurrentUserLogin
        {
            get
            {
                return ConfigurationManager.AppSettings["ShowUserName"].ToString();
            }
        }
        /// <summary>
        /// 获取不显示的部门IDList
        /// </summary>
        protected string NotShowUnitList
        {
            get
            {
                string sqlwhere = "";
                var pointerIndex = Request.Path.IndexOf("/");
                var pointerIndex2 = Request.Path.LastIndexOf("/");
                var FoldName = Request.Path.Substring((pointerIndex + 1), (pointerIndex2 - pointerIndex - 1));
                if (FoldName == "Calendar")
                {
                    sqlwhere = " unit_ID=232 ";
                }
                else
                {
                    sqlwhere = " flag=1 ";
                }
                //获取显示的列表
                string strlist = "0,";
                List<TG.Model.cm_DropUnitList> list = new TG.BLL.cm_DropUnitList().GetModelList(sqlwhere);
                if (list.Count > 0)
                {
                    foreach (TG.Model.cm_DropUnitList model in list)
                    {
                        strlist += model.unit_ID.ToString() + ",";
                    }
                }
                strlist = strlist.Remove(strlist.Length - 1);
                return strlist;
            }

        }
        /// <summary>
        /// 用户登录系统自增号
        /// </summary>
        protected int UserSysNo
        {
            get
            {
                object objUserSysNo = this.UserInfo["memid"];
                return objUserSysNo == null ? 1 : Convert.ToInt32(objUserSysNo);
            }
        }
        /// <summary>
        /// 获取用户部门ID
        /// </summary>
        protected int UserUnitNo
        {
            get
            {

                TG.Model.tg_member user = new TG.BLL.tg_member().GetModel(Convert.ToInt32(this.UserInfo["memid"] ?? "0"));
                if (user != null)
                {
                    return user.mem_Unit_ID;
                }
                else
                {
                    return 1;
                }


            }
        }
        /// <summary>
        /// 角色功能权限
        /// </summary>
        protected ActionPowerParameterEntity RolePowerParameterEntity { get; private set; }
    }
    public class CustomJsonResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;

            if (Data != null)
            {
                var timeConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" };//这里使用自定义日期格式，默认是ISO8601格式        
                response.Write(JsonConvert.SerializeObject(Data, Formatting.Indented, timeConverter));
            }
        }
    }
}