﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace TG.Web.Controllers
{
    public class DeptBpmController : BaseController
    {
        // GET: DeptBpm
        public ActionResult DeptBpmList()
        {
            List<TG.Model.cm_KaoHeRenWu> list = new TG.BLL.cm_KaoHeRenWu().GetModelList("");

            var orderlist = list.Where(c => c.StartDate.Year > 2015).OrderByDescending(c => c.StartDate).ToList();

            string htmlRow = "";
            orderlist.ForEach(c =>
            {
                htmlRow += GetRowHtml(c);
            });

            string otherRow = GetOtherRowHtml();
            if (!string.IsNullOrEmpty(otherRow))
            {
                htmlRow += otherRow;
            }
            ViewBag.Khtj = htmlRow;
            return View();
        }
        public ActionResult GetDeptBpmList()
        {
            List<TG.Model.cm_KaoHeRenWu> list = new TG.BLL.cm_KaoHeRenWu().GetModelList("");

            var orderlist = list.OrderByDescending(c => c.StartDate).ToList();

            //查询已归档任务

            var hisList = new TG.BLL.cm_KaoheRenwuHistory().GetModelList("");

            List<object> jsonList = new List<object>();
            foreach (var o in orderlist)
            {
                string se = o.StartDate.ToShortDateString() + "-" + o.EndDate.ToShortDateString();
                bool isGd = hisList.Exists(a => a.RenWuID == o.ID);
                var obj = new
                {
                    RenWuNo = o.RenWuNo,
                    InsertDate = o.InsertDate.ToShortDateString(),
                    StartDate=o.StartDate,
                    EndDate=o.EndDate,
                    Se = se,
                    ProjStatus =o.ProjStatus,
                    DeptStatus=o.DeptStatus,
                    ID=o.ID,
                    Sub=o.Sub,
                    isGd= isGd
                };
                jsonList.Add(obj);
               

            }
            return Json(jsonList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Save(TG.Model.cm_KaoHeRenWu model)
        {
            if (model != null)
            {
                TG.BLL.cm_KaoHeRenWu bll = new BLL.cm_KaoHeRenWu();
                model.InsertDate = DateTime.Now;
                model.InsertUser = UserSysNo;
                if (model.ID < 1)
                {
                    //是否存在
                    string strWhere = string.Format(" RenWuNo='{0}'", model.RenWuNo);
                    int exsitCount = bll.GetModelList(strWhere).Count;
                    if (exsitCount == 0)
                    {
                        int count = bll.Add(model);
                        if (count > 0)
                        {
                            return Json(true);
                        }
                    }
                }
                else
                {
                    if (bll.Update(model))
                    {
                        return Json(true);
                    }
                }
               
               
            }
            return Json(false);
        }

        public ActionResult Delete(int id)
        {
            if (id > 0)
            {
                TG.BLL.cm_KaoHeRenWu bll = new BLL.cm_KaoHeRenWu();
                if (bll.Delete(id))
                {
                    return Json(true);
                }
            }
            return Json(false);

        }

        /// <summary>
        /// 归档
        /// </summary>
        /// <returns></returns>
        public ActionResult Bak()
        {
            return new ContentResult();
        }


     
        public string GetOtherRowHtml()
        {
            StringBuilder sb = new StringBuilder();

            string strSql = string.Format(@" Select renwuID,renwuName, ShijiFafang, ChushiLuru, JiShuBuMen, JiangJinJiShu
                                             FROM cm_KaoHeHistoryRW Order By ID DESC");

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //2017年5月16日  适应 考核历史数据的发起归档
                    sb.AppendFormat("<tr data-rid=\"{0}\">", dr["renwuID"].ToString());

                    sb.AppendFormat("<td><a href=\"#\" class=\"badge badge-info\">{0}</a></td>", dr["renwuName"].ToString());
                    sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", dr["ShijiFafang"].ToString());
                    sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", dr["ChushiLuru"].ToString());
                    sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", dr["JiShuBuMen"].ToString());
                    sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", dr["JiangJinJiShu"].ToString());
                    sb.AppendFormat("<td></td>");
                    sb.AppendFormat("<td></td>");
                    sb.AppendFormat("</tr>");
                }
            }

            return sb.ToString();
        }
        /// <summary>
        /// 股东ID
        /// </summary>
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        public TG.BLL.cm_KaoHeRenWuCountHis bllHis = new TG.BLL.cm_KaoHeRenWuCountHis();
        private string GetRowHtml(TG.Model.cm_KaoHeRenWu RenwuModel)
        {
            StringBuilder sb = new StringBuilder();
            //查询当前考核是否已经归档
            string strWhereHis = string.Format(" RenwuID={0}", RenwuModel.ID);
            var hisList = bllHis.GetModelList(strWhereHis);

            if (hisList.Count == 0)
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }
                //补救的语句
                string strUnionSql = "";
                string strUnionSql2 = "";
                if (RenwuModel.ID == 77)
                {
                    strUnionSql += string.Format(@"union all
                                                select (select SUM(yfgz)-SUM(yfjj) 
				                                from dbo.cm_KaoHeMemsWages a left join cm_KaoHeRenwuMem KM on KM.memID=a.MemID left join tg_member b on a.MemID= b.mem_ID 
				                                where KM.Statu=0 
				                                AND (KM.KaoHeUnitID IN ( Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo=78))     
				                                AND (KM.memID Not IN (Select mem_ID From tg_memberRole Where RoleIdMag=5)) 
				                                AND a.MemID NOT IN (1442,1577)
				                                AND b.mem_Unit_ID =281 
				                                --AND KM.memUnitID =unit.unit_ID 
				                                AND a.SendYear=year('2016/2/1 0:00:00') 
				                                AND (a.SendMonth>= month('2016/2/1 0:00:00') 
				                                AND a.SendMonth<=month('2016/7/31 0:00:00'))) AS SumCount 
				                                from tg_unit unit where unit.unit_ID=281");


                    strUnionSql2 = ",281";
                }

                //AND b.mem_isFired=0   离职人员会导致数据错误  2017年1月19日
                //添加 特务三部 西安分公司 两个部门
                string strSql = string.Format(@"select RenWuNo,
	                                    isnull((select SUM(xmjj)+SUM(bmnjj) from dbo.cm_KaoHeAllMemsAllotDetails K left join tg_member M on K.MemID= M.mem_ID where    (K.MemID NOT IN({2})) AND  RenwuID=R.ID AND (K.UnitID IN (231,232,233,234,236,237,238,239,281,282{4}))),0) AS ProjCount,
	                                    JianAllCount,
	                                    isnull((select SUM(A.SumCount) 
                                            from (select (select SUM(yfgz)-SUM(yfjj) 
                                                  from dbo.cm_KaoHeMemsWages a left join cm_KaoHeRenwuMem KM on KM.memID=a.MemID left join tg_member b on a.MemID= b.mem_ID 
                                                  where KM.Statu=0 
                                                  AND (KM.KaoHeUnitID IN ( Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo=R.ID))     
                                                  --AND (KM.memID Not IN (Select mem_ID From tg_memberRole Where RoleIdMag=5))
                                                  AND (KM.memID NOT IN ({2}))
                                                  AND a.MemID NOT IN (1442,1577)
                                                  AND b.mem_Unit_ID =unit.unit_ID AND KM.memUnitID =unit.unit_ID {1}) AS SumCount 
                                                  from tg_unit unit where unit.unit_ID IN (231,232,233,234,236,237,238,239,281,282)
                                                  {3}) A),0)/6 AS AvgGz,
                                        R.ID
                                        From cm_KaoHeRenWu R
                                        Where R.ID={0}
                                        order by ID desc   
                                        ", RenwuModel.ID, strWhere, GuDongIdStr, strUnionSql, strUnionSql2);

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                if (dt.Rows.Count > 0)
                {
                    sb.AppendFormat("<tr data-rid=\"{0}\" id=\"{1}\">", dt.Rows[0][4].ToString(), dt.Rows[0][4].ToString());
                    sb.AppendFormat("<td><a href=\"#\" class=\"badge badge-info\">{0}</a></td>", dt.Rows[0][0].ToString());
                    sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", dt.Rows[0][1].ToString());
                    //如果是张维 改为文本框 2016年9月28日
                    if (UserShortName == CurrentUserLogin)
                    {
                        // sb.AppendFormat("<td style=\"text-align:right;\"><input id=\"txtLuru\" type=\"text\" style=\"width:90px;\" value=\"{0}\" cid=\"{1}\"/><input type=\"hidden\" value=\"{0}\"/></td>", Math.Round(Convert.ToDecimal(dt.Rows[0][2].ToString()), 2), dt.Rows[0][4].ToString());
                        sb.AppendFormat("<td style=\"text-align:right;\"><input id=\"txtLuru_{1}\"  type=\"text\" style=\"width:90px;\" value=\"{0}\" cid=\"{1}\"/><input type=\"hidden\" value=\"{0}\"/></td>", Math.Round(Convert.ToDecimal(dt.Rows[0][2].ToString()), 2), dt.Rows[0][4].ToString());

                    }
                    else
                    {
                        sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", Math.Round(Convert.ToDecimal(dt.Rows[0][2].ToString()), 2));
                    }
                    sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", Math.Round(Convert.ToDecimal(dt.Rows[0][3].ToString()), 2));

                    if (Convert.ToDecimal(dt.Rows[0][3].ToString()) == 0)
                    {
                        sb.AppendFormat("<td style=\"text-align:right;\">0</td>");
                    }
                    else
                    {
                        //实际发放/系统计算值 为0时
                        decimal sjcount = Convert.ToDecimal(dt.Rows[0][1].ToString());
                        decimal sccount = Convert.ToDecimal(dt.Rows[0][2].ToString());

                        decimal result = 0;
                        if (sjcount == 0)
                        {
                            result = sccount / Convert.ToDecimal(dt.Rows[0][3].ToString());
                        }
                        else
                        {
                            result = sjcount / Convert.ToDecimal(dt.Rows[0][3].ToString());
                        }

                        sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", result.ToString("f2"));
                    }
                    //如果是张维，增加保存按钮
                    if (UserShortName == CurrentUserLogin)
                    {

                        //sb.AppendFormat("<td><a href=\"#\" class=\"btn default btn-xs red-stripe\" id=\"btnSaveLuru\">保存</a></td><td><a href=\"#\" class=\"btn default btn-xs red-stripe\" id=\"btnSaveLuru2\">确认提交</a></td>");
                        sb.AppendFormat("<td><a href=\"javascript:void(0)\" class=\"btn default btn-xs red-stripe\" onclick=\"DeptBpmListBtnSaveLuruOnClick({0})\"  id=\"btnSaveLuru_{0}\">保存</a></td><td><a href=\"javascript:void(0)\" class=\"btn default btn-xs red-stripe\" onclick=\"DeptBpmListBtnSaveLuruOnClicktijiao({0})\"  id=\"btnSaveLuru_{0}\">确认提交</a></td>", dt.Rows[0][4].ToString());

                    }
                    else
                    {
                        sb.AppendFormat("<td></td>");
                    }
                    sb.AppendFormat("</tr>");
                }
            }
            else
            {
                var kaoheHisModel = hisList[0];
                //加载已归档数据
                sb.AppendFormat("<tr data-rid=\"{0}\">", kaoheHisModel.RenwuID);
                sb.AppendFormat("<td><a href=\"#\" class=\"badge badge-info\">{0}</a></td>", kaoheHisModel.RenwuName);
                sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", kaoheHisModel.ShijiCount);
                sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", kaoheHisModel.InitCount);
                sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", kaoheHisModel.JishuCount);
                sb.AppendFormat("<td style=\"text-align:right;\">{0}</td>", kaoheHisModel.JiangjinCount);
                sb.AppendFormat("<td></td>");
                sb.AppendFormat("<td></td>");
                sb.AppendFormat("</tr>");
            }
            return sb.ToString();
        }
    }
}