﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TG.Web.Controllers
{
    public class IOTController : BaseController
    {
        List<TG.Model.Floor_Model> lstf;
        List<TG.Model.Unit_Model> lstu;
        List<TG.Model.FloorNum_Model> lstfn;
        List<TG.Model.Watermeter_Model> lstw;
        List<TG.Model.Door_Model> lstd;
        List<TG.Model.Electric_Model> lste;
        List<TG.Model.Wind_Model> lstwi;
        public ActionResult Index()
        {
            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;

            ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
            ViewData["Runtime"] = isMono ? "Mono" : ".NET";

            return View();
        }


        #region 数据加载
        /// <summary>
        /// 加载水表数据
        /// </summary>
        public void loadDataW()
        {
            string tmpRootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录
            string filew = tmpRootDir + "/Data/WatermeterData.json";//JSON文件路径
            try
            {

                using (System.IO.StreamReader file = System.IO.File.OpenText(filew))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        lstw = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.Watermeter_Model>>(o["data"].ToString());
                    }
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
        /// <summary>
        /// 加载单元数据
        /// </summary>
        public void loadDataU()
        {
            string tmpRootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录

            string fileu = tmpRootDir + "/Data/UnitData.json";//JSON文件路径

            try
            {


                using (System.IO.StreamReader file = System.IO.File.OpenText(fileu))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        string a = o["data"].ToString();
                        lstu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.Unit_Model>>(o["data"].ToString());
                    }
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// 加载楼层数据
        /// </summary>
        public void loadDataFN()
        {
            string tmpRootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录
            string filefn = tmpRootDir + "/Data/FloorNumData.json";//JSON文件路径
            try
            {

                using (System.IO.StreamReader file = System.IO.File.OpenText(filefn))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        lstfn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.FloorNum_Model>>(o["data"].ToString());
                    }
                }


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// 加载消防门数据
        /// </summary>
        public void loadDataD()
        {
            string tmpRootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录
            string filed = tmpRootDir + "/Data/DoorData.json";//JSON文件路径

            try
            {



                using (System.IO.StreamReader file = System.IO.File.OpenText(filed))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        lstd = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.Door_Model>>(o["data"].ToString());
                    }
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// 加载电表数据
        /// </summary>
        public void loadDataE()
        {
            string tmpRootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录

            string filee = tmpRootDir + "/Data/ElectricData.json";//JSON文件路径

            try
            {


                using (System.IO.StreamReader file = System.IO.File.OpenText(filee))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        lste = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.Electric_Model>>(o["data"].ToString());
                    }
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// 加载楼层数据
        /// </summary>
        public void loadDataF()
        {
            string tmpRootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录

            string filef = tmpRootDir + "/Data/FloorData.json";//JSON文件路径

            try
            {


                using (System.IO.StreamReader file = System.IO.File.OpenText(filef))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        lstf = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.Floor_Model>>(o["data"].ToString());
                    }
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// 加载风机数据
        /// </summary>
        public void loadDataWi()
        {
            string tmpRootDir = Server.MapPath(System.Web.HttpContext.Current.Request.ApplicationPath.ToString());//获取程序根目录

            string filef = tmpRootDir + "/Data/WindData.json";//JSON文件路径

            try
            {


                using (System.IO.StreamReader file = System.IO.File.OpenText(filef))
                {
                    using (JsonTextReader reader = new JsonTextReader(file))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        lstwi = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TG.Model.Wind_Model>>(o["data"].ToString());
                    }
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
        #endregion

        #region 电表
        /// <summary>
        /// 电页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Electricity()
        {
            return View();
        }
        #endregion
        public ActionResult test_cs()
        {
            return View();

            // TG.Model.Model_C c = new TG.Model.Model_C();
            // c.A.id = 1;
            //c.B.id = 1;
        }

        #region  水表
        /// <summary>
        /// 水表页面
        /// </summary>
        public ActionResult Watermeter()
        {

            return View();
        }
        public JsonResult GetWate(int page)
        {
            loadDataW();
            // string temp = Newtonsoft.Json.JsonConvert.SerializeObject(lstw);
            var temp = new { data = lstw.Skip(page * 12).Take(12).ToList(), count = lstw.Count };
            return Json(temp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetWateData(string floorid, string unitid, string fnid, string type_val, string titletext)
        {
            string sf = floorid;
            string su = unitid;
            string sfn = fnid;
            string stype = type_val;
            string stext = titletext;
            List<TG.Model.Watermeter_Model> list = new List<TG.Model.Watermeter_Model>();
            loadDataW();
            if (!string.IsNullOrEmpty(fnid))
                list = lstw.Where(x => x.w_iderty == fnid).ToList();
            if (type_val == "1")
                list = lstw.Where(x => x.w_type == true).ToList();
            if (type_val == "0")
                list = lstw.Where(x => x.w_type == false).ToList();
            if (!string.IsNullOrEmpty(titletext))
                list = lstw.Where(x => x.w_name == titletext).ToList();

           // string temp = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            return Json(list, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region  消防门

        public ActionResult door()
        {
            return View();
        }

        public JsonResult Getdoor(int page)
        {
            loadDataD();
            var temp = new { data = lstd.Skip(page * 12).Take(12).ToList(), count = lstd.Count };
            return Json(temp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetdoorData(string floorid, string unitid, string fnid, string type_val, string titletext)
        {
            string sf = floorid;
            string su = unitid;
            string sfn = fnid;
            string stype = type_val;
            string stext = titletext;
            List<TG.Model.Door_Model> list = new List<TG.Model.Door_Model>();
            loadDataD();
            if (!string.IsNullOrEmpty(fnid))
                list = lstd.Where(x => x.d_iderty == fnid).ToList();
            if (type_val == "1")
                list = lstd.Where(x => x.d_type == true).ToList();
            if (type_val == "0")
                list = lstd.Where(x => x.d_type == false).ToList();
            if (!string.IsNullOrEmpty(titletext))
                list = lstd.Where(x => x.d_name == titletext).ToList();

            //string temp = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            //var temp = new {data=list };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 下拉列表
        public JsonResult GetFloorNum(string u_id)
        {
            loadDataFN();
            List<TG.Model.FloorNum_Model> list = lstfn.Where(x => x.u_id == u_id).ToList();
            var varjson = new { code = 0, data = list, msg = "", count = list.Count };
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(varjson), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Getunit(string f_id)
        {
            loadDataU();
            List<TG.Model.Unit_Model> list = lstu.Where(x => x.f_id == f_id).ToList();
            var varjson = new { code = 0, data = list, msg = "", count = list.Count };
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(varjson), JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetFloor()
        {
            loadDataF();
            var varjson = new { code = 0, data = lstf, msg = "", count = lstf.Count };
            return Json(Newtonsoft.Json.JsonConvert.SerializeObject(varjson), JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region 消防风机
        public ActionResult Wind()
        {
            //load();
            return View();
        }

        public JsonResult GetWind(int page)
        {
            loadDataWi();
            // string str = Newtonsoft.Json.JsonConvert.SerializeObject();

            var temp = new { data = lstwi.Skip(page * 12).Take(12).ToList(), count = lstwi.Count };

            return Json(temp, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWindData(string floorid, string unitid, string fnid, string type_val, string titletext)
        {
            string sf = floorid;
            string su = unitid;
            string sfn = fnid;
            string stype = type_val;
            string stext = titletext;
            List<TG.Model.Wind_Model> list = new List<TG.Model.Wind_Model>();
            loadDataWi();
            if (!string.IsNullOrEmpty(fnid))
                list = lstwi.Where(x => x.w_iderty == fnid).ToList();
            if (type_val == "1")
                list = lstwi.Where(x => x.w_type == true).ToList();
            if (type_val == "0")
                list = lstwi.Where(x => x.w_type == false).ToList();
            if (!string.IsNullOrEmpty(titletext))
                list = lstwi.Where(x => x.w_name == titletext).ToList();

            //string temp = Newtonsoft.Json.JsonConvert.SerializeObject(list);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// 初始化数据
        /// </summary>
        public void load()
        {
            List<TG.Model.Floor_Model> lstf = new List<TG.Model.Floor_Model>();
            List<TG.Model.Unit_Model> lstu = new List<TG.Model.Unit_Model>();
            List<TG.Model.FloorNum_Model> lstfn = new List<TG.Model.FloorNum_Model>();

            List<TG.Model.Watermeter_Model> lstw = new List<TG.Model.Watermeter_Model>();
            List<TG.Model.Door_Model> lstd = new List<TG.Model.Door_Model>();
            List<TG.Model.Electric_Model> lste = new List<TG.Model.Electric_Model>();
            List<TG.Model.Wind_Model> lstwind = new List<TG.Model.Wind_Model>();
           
            Random r = new Random();
            for (int i = 1; i < 10; i++)
            {
                TG.Model.Floor_Model m = new TG.Model.Floor_Model();
                m.f_id = i;
                m.f_name = i + "号楼";
                m.f_code = i.ToString();
                lstf.Add(m);
                // string strsqlf = " INSERT INTO T_Floor (f_id, f_name, f_code) VALUES(" + i + ",'" + i + "号楼','" + i + "') ";

                // help.ExecuteNonQuery(strsqlf);
                for (int j = 1; j < 4; j++)
                {
                    TG.Model.Unit_Model u = new TG.Model.Unit_Model();
                    u.u_id = Convert.ToInt32(i.ToString() + j.ToString());
                    u.u_name = m.f_name + j + "单元";
                    u.u_code = i.ToString() + j.ToString();
                    u.f_id = i.ToString();
                    lstu.Add(u);
                    // string strsqlu = "INSERT INTO T_Unit (u_id, u_name, u_code, f_id) VALUES (" + j + ",'" + m.f_name + j + "单元', '" + i.ToString() + j + "', '" + i + "')";
                    // help.ExecuteNonQuery(strsqlu);
                    for (int k = 1; k < 16; k++)
                    {
                        TG.Model.FloorNum_Model fn = new TG.Model.FloorNum_Model();
                        fn.fn_id = Convert.ToInt32(i.ToString() + j.ToString() + k);
                        fn.fn_name = u.u_name + k + "层";
                        fn.fn_code = i.ToString() + j.ToString() + k.ToString();
                        fn.u_id = i.ToString() + j.ToString();
                        lstfn.Add(fn);
                        TG.Model.Watermeter_Model w = new TG.Model.Watermeter_Model();
                        w.w_id = Convert.ToInt32(i.ToString() + j.ToString() + k);
                        w.w_iderty = i.ToString() + j.ToString() + k.ToString();
                        w.w_use = r.Next(0, 100);
                        w.w_isuse = r.Next(0, 100) + (w.w_use);
                        w.w_name = u.u_name + k + "层";
                        TG.Model.Door_Model d = new TG.Model.Door_Model();
                        d.d_id = Convert.ToInt32(i.ToString() + j.ToString() + k);
                        d.d_iderty = i.ToString() + j.ToString() + k.ToString();
                        d.d_name = u.u_name + k + "层";
                        TG.Model.Electric_Model e = new TG.Model.Electric_Model();
                        e.e_id = Convert.ToInt32(i.ToString() + j.ToString() + k);
                        e.e_iderty = i.ToString() + j.ToString() + k.ToString();
                        e.e_use = r.Next(0, 100);
                        e.e_isuse = r.Next(0, 100) + (w.w_use);
                        e.e_name = u.u_name + k + "层";

                        TG.Model.Wind_Model wi = new TG.Model.Wind_Model();
                        wi.w_id = Convert.ToInt32(i.ToString() + j.ToString() + k);
                        wi.w_name = u.u_name + k + "号消防风机";
                        wi.w_iderty = i.ToString() + j.ToString() + k;
                        if (r.Next(0, 100) % 2 == 0)
                        {
                            d.d_type = false;
                            w.w_type = false;
                            e.e_type = false;
                            wi.w_type = false;
                        }
                        else
                        {
                            d.d_type = true;
                            w.w_type = true;
                            e.e_type = true;
                            wi.w_type = true;
                        }
                        lstd.Add(d);
                        lstw.Add(w);
                        lste.Add(e);
                        lstwind.Add(wi);
                        //    string strsqlfn = "INSERT INTO T_FoorNum (fn_id, fn_name, fn_code, u_id) VALUES (" + k + ",'" + u.u_name + k + "层', '" + i.ToString() + j.ToString() + k + "', '" + i.ToString() + j + "')";
                        //     help.ExecuteNonQuery(strsqlfn);
                    }
                }
            }

            string sf = Newtonsoft.Json.JsonConvert.SerializeObject(lstf);
            string su = Newtonsoft.Json.JsonConvert.SerializeObject(lstu);
            string sfn = Newtonsoft.Json.JsonConvert.SerializeObject(lstfn);
            string sw = Newtonsoft.Json.JsonConvert.SerializeObject(lstw);
            string sd = Newtonsoft.Json.JsonConvert.SerializeObject(lstd);
            string se = Newtonsoft.Json.JsonConvert.SerializeObject(lste);
            string swi = Newtonsoft.Json.JsonConvert.SerializeObject(lstwind);
        }
    }
}