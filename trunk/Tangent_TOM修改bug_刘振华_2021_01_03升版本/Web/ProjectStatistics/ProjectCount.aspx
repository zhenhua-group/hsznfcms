﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectCount.aspx.cs" Inherits="TG.Web.ProjectStatistics.ProjectCount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectStatistics.css" rel="stylesheet" type="text/css" />
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetFormWidth();
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[项目统计]
            </td>
        </tr>
    </table>
    <table style="width: 100%;" class="cls_show_cst_jiben">
        <tr>
            <td>
                显示：<asp:DropDownList ID="ddshow" runat="server" Width="100px">
                    <asp:ListItem Value="0">项目数</asp:ListItem>
                    <asp:ListItem Value="1">项目类型</asp:ListItem>
                    <asp:ListItem Value="2">平米数</asp:ListItem>
                    <asp:ListItem Value="3">项目状态</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                年份：<asp:DropDownList ID="ddyear" runat="server" Width="80px">
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    <asp:ListItem>2016</asp:ListItem>
                    <asp:ListItem>2017</asp:ListItem>
                    <asp:ListItem>2018</asp:ListItem>
                    <asp:ListItem>2019</asp:ListItem>
                    <asp:ListItem>2020</asp:ListItem>
                </asp:DropDownList>
                <input id="Checkbox1" type="checkbox" />按月显示
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="Label1" runat="server" Text="企业项目总数：120"></asp:Label>
                <!--如果显示按平米数的时候，这里改为：企业项目总平米数。这里的数据是可变的-->
            </td>
        </tr>
        <tr>
            <td>
                <div class="cls_chart">
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
