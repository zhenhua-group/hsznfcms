﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectCountListBymaster.aspx.cs" Inherits="TG.Web.ProjectStatistics.ProjectCountListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(function () {
            
            $("#btn_Search").click(function () {
                if ($("#ctl00_ContentPlaceHolder1_drp_unit").get(0).selectedIndex == 0) {
                    if ($("#hiddenischeckallpower").val() == "0") {
                        alert('没有查看全部生产部门权限，请选择本部门查看！');
                        return false;
                    }
                }
            });
            HideTableTr($("#ctl00_ContentPlaceHolder1_GridView1 tbody tr"));
            HideTableTr($("#ctl00_ContentPlaceHolder1_GridView2 tbody tr"));
            HideTableTr($("#ctl00_ContentPlaceHolder1_GridView3 tbody tr"));
            HideTableTr($("#ctl00_ContentPlaceHolder1_GridView4 tbody tr"));
        });  //清空数据  
        var bool = true;
        //隐藏部门
        function HideTableTr(obj) {
            var $tr = "";
            obj.each(function () {
                var tdname = $.trim($(this).find("td").eq(0).text());
                var flag = false;
                if (tdname != "全院总计") {
                    $("#ctl00_ContentPlaceHolder1_drp_unit option").each(function () {

                        if ($.trim($(this).text()) == tdname) {
                            flag = true;
                            return false;
                        }
                    });
                    if (!flag) {
                        $(this).css("display", "none");
                    }
                }
                //把生产经营部放到最后             
                if (tdname == "生产经营部") {
                    $tr = $(this).clone();
                    $(this).remove();
                }

            });

            if (bool) {
                obj.last().after($tr);
                bool = false;
            }
            else {
                obj.last().before($tr);
            }

        }
    </script>
    <style type="text/css">
        .cls_show_cst_jiben
        {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }
        
        .cls_show_cst_jiben td
        {
            border: solid 1px #CCC;
            /*font-size: 12px;*/
            font-family: "微软雅黑";
        }
        
        .cls_chart
        {
            width: 100%;
            height: 500px;
        }
        
        .cls_data_bottom
        {
            width: 100%;
            margin: 0 auto;
            height: 25px;
            border: solid 1px #CCC;
        }
        /*数据基本样式*/
        .cls_content_head
        {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }
        
        .cls_content_head td
        {
            height: 20px;
            background-color: #E6E6E6;
            border: 1px solid Gray;
        }
        
        .cls_data
        {
            width: 99%;
            margin: 0 auto;
        }
        
        
        
        .tr_in
        {
            background-color: #CCC;
        }
        
        .gridView_comm
        {
            margin: 0 auto;
            border-collapse: collapse;
            border-top: none;
        }
        
        .gridView_comm td
        {
            height: 18px;
            border-collapse: collapse;
            border: 1px solid Gray;
            border-top: none;
        }
        
        .gridView_comm a, a:hover, a:visited, a:link, a:active
        {
            color: Blue;
            text-decoration: none;
        }
        
        .gridView_comm .cls_column
        {
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            white-space: nowrap;
        }
        
        .gridView_comm tr:hover
        {
            background-color: #CCC;
        }
        
        #tbl_1 td
        {
            padding: 0 5px;
            height: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目信息管理</a><i class="fa fa-angle-right"> </i><a>项目统计</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询项目统计
                    </div>
                    
                </div>
                <div class="portlet-body " style="display: block;">
                    <table class="table-responsive">
                    	<tr>
                    		<td>生产部门:</td>
                            <td><asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                    </asp:DropDownList></td>
                            <td>年份:</td>
                            <td><asp:DropDownList ID="drp_year" CssClass="form-control " Width="80px" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                    </asp:DropDownList></td>
                            <td>统计类型:</td>
                            <td><asp:DropDownList ID="drp_type" runat="server" CssClass="form-control " Width="120px">
                                        <asp:ListItem Value="0">季度</asp:ListItem>
                                        <asp:ListItem Value="1">月份</asp:ListItem>
                                        <asp:ListItem Value="2">项目性质</asp:ListItem>
                                        <asp:ListItem Value="3">项目状态</asp:ListItem>
                                    </asp:DropDownList></td>
                            <td><input type="submit" class="btn blue" value="查询" id="btn_Search" /></td>
                    	</tr>
                    </table>
                 
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>部门统计列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_Output" runat="server" Text="导出Excel" CssClass="btn red btn-sm" OnClick="btn_Output_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table style="width: 100%;" class="cls_show_cst_jiben">
                                <tr>
                                    <td style="border: none;">
                                        <div class="cls_Container_Report">
                                            <div class="cls_Container_Tip">
                                                <p>
                                                    <%= ChartTip %>项目按<%= CountType %>统计项目个数
                                                </p>
                                            </div>
                                            <div id="div_Container1" runat="server">
                                                  
                                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-grid" AutoGenerateColumns="False" Font-Size="11pt"
                                                    EnableModelValidation="True" ShowHeader="true" Border="0">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="季度" ItemStyle-CssClass="cls_Column">
                                                            <ItemStyle Width="140px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="A" HeaderText="第一季度">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="B" HeaderText="第二季度">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="C" HeaderText="第三季度">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="D" HeaderText="第四季度">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div id="div_Container2" runat="server">
                                               
                                                <asp:GridView ID="GridView2" runat="server" CssClass="table table-bordered table-hover" AutoGenerateColumns="False" Font-Size="11pt"
                                                    EnableModelValidation="True" ShowHeader="true" Border="0">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="月份" ItemStyle-CssClass="cls_Column">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="A" HeaderText="一月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="B" HeaderText="二月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="C" HeaderText="三月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="D" HeaderText="四月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="E" HeaderText="五月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="F" HeaderText="六月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="G" HeaderText="七月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="H" HeaderText="八月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="I" HeaderText="九月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="J" HeaderText="十月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="K" HeaderText="十一月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="L" HeaderText="十二月">
                                                            <ItemStyle Width="60px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div id="div_Container3" runat="server">
                                             
                                                <asp:GridView ID="GridView3" runat="server" CssClass="table table-bordered table-hover" AutoGenerateColumns="False" Font-Size="11pt"
                                                    EnableModelValidation="True" ShowHeader="true" Border="0">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                            <ItemStyle Width="140px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="A" HeaderText="公建">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="B" HeaderText="房地产">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="C" HeaderText="市政">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="D" HeaderText="医院">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="E" HeaderText="电力">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="F" HeaderText="通信">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="G" HeaderText="银行">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="H" HeaderText="学校">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="I" HeaderText="涉外">
                                                            <ItemStyle Width="70px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div id="div_Container4" runat="server">
                                             
                                                <asp:GridView ID="GridView4" runat="server" CssClass="table table-bordered table-hover" AutoGenerateColumns="False" Font-Size="11pt"
                                                    EnableModelValidation="True" ShowHeader="true" Border="0">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                            <ItemStyle Width="140px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="A" HeaderText="项目立项数">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="B" HeaderText="进行中项目">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="C" HeaderText="暂停项目">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="D" HeaderText="完成项目">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 是否有查看全部的权限 -->
    <input type="hidden" id="hiddenischeckallpower" name="name" value="<%= IsCheckAllPower %>" />
</asp:Content>
