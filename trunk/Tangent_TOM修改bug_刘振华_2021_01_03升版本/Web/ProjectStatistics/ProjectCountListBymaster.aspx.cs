﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectStatistics
{
    public partial class ProjectCountListBymaster : PageBase
    {
        //检查是否需要权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //判断是否是全部查看
        protected string IsCheckAllPower
        {
            get
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        //报表名称
        protected string ChartTip { get; set; }
        // 统计类型
        protected string CountType
        {
            get
            {
                return this.drp_type.SelectedItem.Text;
            }
        }

        TG.BLL.cm_Project bll = new TG.BLL.cm_Project();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定单位
                BindUnit();
                //初始化部门
                InitUnitSelect();
                //绑定年份
                BindYear();
                //初始年
                InitDropDownByYear();
                //查询数据
                GetQueryCondition();
            }
            else
            {
                BindDataSetPageIndex();
            }
        }
        //初始选中年
        protected void InitDropDownByYear()
        {
            if (this.drp_year.Items.FindByValue(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //初始单位选中信息
        protected void InitUnitSelect()
        {
            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
        }
        //拼接查询条件
        private void GetQueryCondition()
        {
            //统计类型
            string counttype = this.drp_type.SelectedValue;
            switch (counttype)
            {
                case "0":
                    //季度
                    ShowCountTypeControl("0");
                    ProjectCountJD();
                    break;
                case "1":
                    //月份
                    ShowCountTypeControl("1");
                    ProjectCountMonth();
                    break;
                case "2":
                    //性质
                    ShowCountTypeControl("2");
                    ProjectCountIndustry();
                    break;
                case "3":
                    //项目状态
                    ShowCountTypeControl("3");
                    ProjectCountStatus();
                    break;
            }
        }
        /// <summary>
        /// 项目数
        /// </summary>
        private void ProjectCountJD()
        {

            //绑定
            this.GridView1.DataSource = GetProjectCountJD();
            this.GridView1.DataBind();

            this.GridView1.UseAccessibleHeader = true;
            this.GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        /// <summary>
        /// 项目数
        /// </summary>
        private DataTable GetProjectCountJD()
        {
            //查询提示
            CreateTipQuery();
            //年份
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //生产部门
            int? unitid = int.Parse(this.drp_unit.SelectedValue);
            //全部/部门
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                unitid = null;
            }
            DataTable dt = bll.CountProjectByJD(year, unitid, type).Tables[0];
            return dt;
        }

        /// <summary>
        /// 项目类型
        /// </summary>
        /// <param name="year"></param>
        /// <param name="doMonth"></param>
        private void ProjectCountMonth()
        {
            //绑定
            this.GridView2.DataSource = GetProjectCountMonth();
            this.GridView2.DataBind();

            this.GridView2.UseAccessibleHeader = true;
            this.GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        /// <summary>
        /// 项目类型
        /// </summary>
        /// <returns></returns>
        private DataTable GetProjectCountMonth()
        {
            //查询提示
            CreateTipQuery();
            //年份
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //生产部门
            int? unitid = int.Parse(this.drp_unit.SelectedValue);
            //全部/部门
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                //查看全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }

            DataTable dt = bll.CountProjectByMonth(year, unitid, type).Tables[0];
            return dt;
        }
        /// <summary>
        /// 项目平米数
        /// </summary>
        /// <param name="year"></param>
        /// <param name="doMonth"></param>
        private void ProjectCountIndustry()
        {

            //绑定
            this.GridView3.DataSource = GetProjectCountIndustry();
            this.GridView3.DataBind();

            this.GridView3.UseAccessibleHeader = true;
            this.GridView3.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        /// <summary>
        /// 项目平米数
        /// </summary>
        /// <returns></returns>
        private DataTable GetProjectCountIndustry()
        {
            //查询提示
            CreateTipQuery();
            //年份
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //生产部门
            int? unitid = int.Parse(this.drp_unit.SelectedValue);
            //全部/部门
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }
            DataTable dt = bll.CountProjectByIndustry(year, unitid, type).Tables[0];
            return dt;
        }
        /// <summary>
        /// 项目状态统计
        /// </summary>
        /// <param name="year"></param>
        private void ProjectCountStatus()
        {
            //绑定
            this.GridView4.DataSource = GetProjectCountStatus();
            this.GridView4.DataBind();

            this.GridView4.UseAccessibleHeader = true;
            this.GridView4.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        /// <summary>
        /// 项目状态统计
        /// </summary>
        /// <returns></returns>
        private DataTable GetProjectCountStatus()
        {
            //查询提示
            CreateTipQuery();
            //年份
            string year = this.drp_year.SelectedValue;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = SelectCurrentYear();
            }
            //生产部门
            int? unitid = int.Parse(this.drp_unit.SelectedValue);
            //全部/部门
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }
            DataTable dt = bll.CountProjectByStatus(year, unitid, type).Tables[0];
            return dt;
        }
        //查询提示
        protected void CreateTipQuery()
        {
            string strTip = "";
            if (this.drp_unit.SelectedIndex == 0)
            {
                strTip += "全院";
            }
            else
            {
                strTip += this.drp_unit.SelectedItem.Text.Trim();
            }
            //年份
            if (this.drp_year.SelectedIndex == 0)
            {
                strTip += "全部年份";
            }
            else
            {
                strTip += this.drp_year.SelectedItem.Text.Trim() + "年";
            }

            ChartTip = strTip;
        }
        //选中当前年份
        protected string SelectCurrentYear()
        {
            string year = DateTime.Now.Year.ToString();
            //选择当前年份
            this.drp_year.ClearSelection();
            if (this.drp_year.Items.FindByValue(year) != null)
            {
                this.drp_year.Items.FindByValue(year).Selected = true;
                //统计提示
                CreateTipQuery();
            }
            return year;
        }
        //控制对应统计类型
        protected void ShowCountTypeControl(string type)
        {
            this.div_Container1.Visible = type == "0" ? true : false;
            this.div_Container2.Visible = type == "1" ? true : false;
            this.div_Container3.Visible = type == "2" ? true : false;
            this.div_Container4.Visible = type == "3" ? true : false;
        }

        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            //绑定数据
            GetQueryCondition();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {

            string counttype = this.drp_type.SelectedValue;
            if (counttype == "0")
            {
                exportOne();
            }
            else if (counttype == "1")
            {
                exportTwo();
            }
            else if (counttype == "2")
            {
                exportThree();
            }
            else if (counttype == "3")
            {
                exportFour();
            }
        }

        /// <summary>
        /// 按照季度
        /// </summary>
        private void exportOne()
        {
            DataTable dt = getData();

            DataRow[] drsDel = dt.Select(string.Format("ID in ({0})", base.NotShowUnitList));
            //直接在集合中删除不显示的部门
            foreach (DataRow drDel in drsDel)
            {
                dt.Rows.Remove(drDel);
            }

            //把生产经营部放到最后
            drsDel = dt.Select(string.Format("Name='{0}'", "生产经营部"));
            DataRow dr = dt.NewRow();
            foreach (DataRow drDel in drsDel)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[i] = drDel[i];
                }
                dt.Rows.Remove(drDel);
                //类型判断
                dt.Rows.InsertAt(dr, (dt.Rows.Count - 1));

            }


            string modelPath = " ~/TemplateXls/ProjectQuarter.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            string unitName = "";
            if (drp_unit.SelectedIndex > 0)
            {
                unitName = drp_unit.SelectedItem.Text.ToString();
            }
            else
            {
                unitName = "全院";
            }
            string strName = unitName.Trim() + drp_year.SelectedItem.Value.Trim() + "年项目按季度统计项目个数";
            ws.GetRow(0).GetCell(0).SetCellValue(strName);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["A"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["B"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["C"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["D"].ToString());

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("" + strName + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        /// <summary>
        /// 月份
        /// </summary>
        private void exportTwo()
        {
            DataTable dt = getData();
            DataRow[] drsDel = dt.Select(string.Format("ID in ({0})", base.NotShowUnitList));
            //直接在集合中删除不显示的部门
            foreach (DataRow drDel in drsDel)
            {
                dt.Rows.Remove(drDel);
            }

            //把生产经营部放到最后
            drsDel = dt.Select(string.Format("Name='{0}'", "生产经营部"));
            DataRow dr = dt.NewRow();
            foreach (DataRow drDel in drsDel)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[i] = drDel[i];
                }
                dt.Rows.Remove(drDel);
                //类型判断
                dt.Rows.InsertAt(dr, (dt.Rows.Count - 1));

            }
            string modelPath = " ~/TemplateXls/ProjectMonth.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            string unitName = "";
            if (drp_unit.SelectedIndex > 0)
            {
                unitName = drp_unit.SelectedItem.Text.ToString();
            }
            else
            {
                unitName = "全院";
            }
            string strName = unitName.Trim() + drp_year.SelectedItem.Value.Trim() + "项目按月份统计项目个数";
            ws.GetRow(0).GetCell(0).SetCellValue(strName);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["A"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["B"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["C"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["D"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["E"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["F"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["G"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["H"].ToString());

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["I"].ToString());

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["J"].ToString());

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["K"].ToString());

                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["L"].ToString());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("" + strName + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        /// <summary>
        /// 项目性质
        /// </summary>
        private void exportThree()
        {
            DataTable dt = getData();
            DataRow[] drsDel = dt.Select(string.Format("ID in ({0})", base.NotShowUnitList));
            //直接在集合中删除不显示的部门
            foreach (DataRow drDel in drsDel)
            {
                dt.Rows.Remove(drDel);
            }

            //把生产经营部放到最后
            drsDel = dt.Select(string.Format("Name='{0}'", "生产经营部"));
            DataRow dr = dt.NewRow();
            foreach (DataRow drDel in drsDel)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[i] = drDel[i];
                }
                dt.Rows.Remove(drDel);
                //类型判断
                dt.Rows.InsertAt(dr, (dt.Rows.Count - 1));

            }
            string modelPath = " ~/TemplateXls/ProjectProperty.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            string unitName = "";
            if (drp_unit.SelectedIndex > 0)
            {
                unitName = drp_unit.SelectedItem.Text.ToString();
            }
            else
            {
                unitName = "全院";
            }
            string strName = unitName.Trim() + drp_year.SelectedItem.Value.Trim() + "年项目按项目性质统计项目个数";
            ws.GetRow(0).GetCell(0).SetCellValue(strName);
            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["A"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["B"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["C"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["D"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["E"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["F"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["G"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["H"].ToString());

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["I"].ToString());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("" + strName + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        /// <summary>
        /// 项目状态
        /// </summary>
        private void exportFour()
        {
            DataTable dt = getData();
            DataRow[] drsDel = dt.Select(string.Format("ID in ({0})", base.NotShowUnitList));
            //直接在集合中删除不显示的部门
            foreach (DataRow drDel in drsDel)
            {
                dt.Rows.Remove(drDel);
            }

            //把生产经营部放到最后
            drsDel = dt.Select(string.Format("Name='{0}'", "生产经营部"));
            DataRow dr = dt.NewRow();
            foreach (DataRow drDel in drsDel)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[i] = drDel[i];
                }
                dt.Rows.Remove(drDel);
                //类型判断
                dt.Rows.InsertAt(dr, (dt.Rows.Count - 1));

            }
            string modelPath = " ~/TemplateXls/ProjectStatus.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            string unitName = "";
            if (drp_unit.SelectedIndex > 0)
            {
                unitName = drp_unit.SelectedItem.Text.ToString();
            }
            else
            {
                unitName = "全院";
            }
            string strName = unitName.Trim() + drp_year.SelectedItem.Value.Trim() + "年项目按项目状态统计项目个数";
            ws.GetRow(0).GetCell(0).SetCellValue(strName);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["A"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["B"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["C"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["D"].ToString());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("" + strName + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }
        /// <summary>
        /// 得到数据
        /// </summary>
        private DataTable getData()
        {
            DataTable dt = new DataTable();
            //统计类型
            string counttype = this.drp_type.SelectedValue;
            switch (counttype)
            {
                case "0":
                    //季度
                    dt = GetProjectCountJD();
                    break;
                case "1":
                    //查询提示
                    dt = GetProjectCountMonth();
                    break;
                case "2":
                    //性质
                    dt = GetProjectCountIndustry();
                    break;
                case "3":
                    //项目状态
                    dt = GetProjectCountStatus();
                    break;
            }
            return dt;
        }
    }
}