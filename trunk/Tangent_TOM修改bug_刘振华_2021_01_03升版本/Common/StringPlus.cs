﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TG.Common
{
    public class StringPlus
    {
        //防止Sql注入
        public static string SqlSplit(string inputString) //防止SQL注入方法
        {
            inputString = inputString.Trim();
            inputString = inputString.Replace("'", "");
            inputString = inputString.Replace(";--", "");
            inputString = inputString.Replace("--", "");
            inputString = inputString.Replace("=", "");
            inputString = inputString.Replace("and", "");
            inputString = inputString.Replace("exec", "");
            inputString = inputString.Replace("insert", "");
            inputString = inputString.Replace("select", "");
            inputString = inputString.Replace("delete", "");
            inputString = inputString.Replace("update", "");
            inputString = inputString.Replace("chr", "");
            inputString = inputString.Replace("mid", "");
            inputString = inputString.Replace("master", "");
            inputString = inputString.Replace("or", "");
            inputString = inputString.Replace("truncate", "");
            inputString = inputString.Replace("char", "");
            inputString = inputString.Replace("declare", "");
            inputString = inputString.Replace("join", "");
            inputString = inputString.Replace("count", "");
            inputString = inputString.Replace("*", "");
            inputString = inputString.Replace("%", "");
            inputString = inputString.Replace("union", "");
            return inputString;
        }

        //分隔字符串返回字符串数组
        public static List<string> GetStrArray(string str, char speater, bool toLower)
        {
            List<string> list = new List<string>();
            string[] ss = str.Split(speater);
            foreach (string s in ss)
            {
                if (!string.IsNullOrEmpty(s) && s != speater.ToString())
                {
                    string strVal = s;
                    if (toLower)
                    {
                        strVal = s.ToLower();
                    }
                    list.Add(strVal);
                }
            }
            return list;
        }

        public static string[] GetStrArray(string str)
        {
            return str.Split(new char[',']);
        }
        //字符串数组转化为字符串
        public static string GetArrayStr(List<string> list, string speater)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                if (i == list.Count - 1)
                {
                    sb.Append(list[i]);
                }
                else
                {
                    sb.Append(list[i]);
                    sb.Append(speater);
                }
            }
            return sb.ToString();
        }
        /// <summary>
        /// 删除最后结尾的一个逗号
        /// </summary>
        public static string DelLastComma(string str)
        {
            return str.Substring(0, str.LastIndexOf(","));
        }

        /// <summary>
        /// 删除最后结尾的指定字符后的字符
        /// </summary>
        public static string DelLastChar(string str, string strchar)
        {
            return str.Substring(0, str.LastIndexOf(strchar));
        }
        /// <summary>
        /// 转全角的函数(SBC case)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToSBC(string input)
        {
            //半角转全角：
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 32)
                {
                    c[i] = (char)12288;
                    continue;
                }
                if (c[i] < 127)
                    c[i] = (char)(c[i] + 65248);
            }
            return new string(c);
        }

        /// <summary>
        ///  转半角的函数(SBC case)
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns></returns>
        public static string ToDBC(string input)
        {
            char[] c = input.ToCharArray();
            for (int i = 0; i < c.Length; i++)
            {
                if (c[i] == 12288)
                {
                    c[i] = (char)32;
                    continue;
                }
                if (c[i] > 65280 && c[i] < 65375)
                    c[i] = (char)(c[i] - 65248);
            }
            return new string(c);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="o_str"></param>
        /// <param name="sepeater"></param>
        /// <returns></returns>
        public static List<string> GetSubStringList(string o_str, char sepeater)
        {
            List<string> list = new List<string>();
            string[] ss = o_str.Split(sepeater);
            foreach (string s in ss)
            {
                if (!string.IsNullOrEmpty(s) && s != sepeater.ToString())
                {
                    list.Add(s);
                }
            }
            return list;
        }
        /// <summary>
        /// 将字符串样式转换为纯字符串
        /// </summary>
        /// <param name="StrList"></param>
        /// <param name="SplitString"></param>
        /// <returns></returns>
        public static string GetCleanStyle(string StrList, string SplitString)
        {
            string RetrunValue = "";
            //如果为空，返回空值
            if (StrList == null)
            {
                RetrunValue = "";
            }
            else
            {
                //返回去掉分隔符
                string NewString = "";
                NewString = StrList.Replace(SplitString, "");
                RetrunValue = NewString;
            }
            return RetrunValue;
        }
        /// <summary>
        /// 将字符串转换为新样式
        /// </summary>
        /// <param name="StrList"></param>
        /// <param name="NewStyle"></param>
        /// <param name="SplitString"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static string GetNewStyle(string StrList, string NewStyle, string SplitString, out string Error)
        {
            string ReturnValue = "";
            //如果输入空值，返回空，并给出错误提示
            if (StrList == null)
            {
                ReturnValue = "";
                Error = "请输入需要划分格式的字符串";
            }
            else
            {
                //检查传入的字符串长度和样式是否匹配,如果不匹配，则说明使用错误。给出错误信息并返回空值
                int strListLength = StrList.Length;
                int NewStyleLength = GetCleanStyle(NewStyle, SplitString).Length;
                if (strListLength != NewStyleLength)
                {
                    ReturnValue = "";
                    Error = "样式格式的长度与输入的字符长度不符，请重新输入";
                }
                else
                {
                    //检查新样式中分隔符的位置
                    string Lengstr = "";
                    for (int i = 0; i < NewStyle.Length; i++)
                    {
                        if (NewStyle.Substring(i, 1) == SplitString)
                        {
                            Lengstr = Lengstr + "," + i;
                        }
                    }
                    if (Lengstr != "")
                    {
                        Lengstr = Lengstr.Substring(1);
                    }
                    //将分隔符放在新样式中的位置
                    string[] str = Lengstr.Split(',');
                    foreach (string bb in str)
                    {
                        StrList = StrList.Insert(int.Parse(bb), SplitString);
                    }
                    //给出最后的结果
                    ReturnValue = StrList;
                    //因为是正常的输出，没有错误
                    Error = "";
                }
            }
            return ReturnValue;
        }
        /// <summary>
        /// 加密密码
        /// </summary>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public static string GetPwdEncryString(string pwd)
        {
            string str_rlt = "";
            string str_array = pwd;
            //加密密钥
            int key = 1314;
            for (int i = 0; i < str_array.Length; i++)
            {
                char a = Convert.ToChar(str_array.Substring(i, 1));
                str_rlt += (char)((int)a ^ (key >> 8));
            }

            str_array = str_rlt;
            str_rlt = "";
            string str_rlt2 = "";
            int index = 0;
            for (int i = 0; i < str_array.Length; i++)
            {
                str_rlt2 = "";
                index = Convert.ToByte(Convert.ToChar(str_array.Substring(i, 1)));
                str_rlt2 += (char)(65 + index / 26);
                str_rlt2 += (char)(65 + index % 26);
                str_rlt += str_rlt2;
            }

            return str_rlt;
        }
        /// <summary>
        /// 解析结构样式字符串生成列表菜单
        /// </summary>
        /// <param name="structStr"></param>
        /// <returns></returns>
        public static string ResolveStructString(string structStr)
        {
            StringBuilder sbhtml = new StringBuilder();

            if (structStr.IndexOf('+') > -1)
            {
                string[] firstArray = structStr.Split('+');
                //树开始
                sbhtml.Append("<ul>");
                foreach (string firststr in firstArray)
                {
                    if (firststr != "")
                    {
                        //类名称
                        string firstName = "";
                        //第二级别
                        if (firststr.IndexOf('^') > -1)
                        {
                            //类名称    公共建筑
                            firstName = firststr.Split('^')[0].ToString();
                            //第一层内容项
                            sbhtml.Append("<li>" + firstName + "</li>");
                            //第二层内容项
                            string secstr = firststr.Substring(firststr.IndexOf('^'));
                            //分隔二级
                            string[] secondArray = secstr.Split('^');
                            //判断量
                            string tempscd = "";
                            //拼接第一级外层
                            sbhtml.Append("<ul>");
                            foreach (string secondstr in secondArray)
                            {
                                string secondName = "";
                                if (secondstr != "")
                                {
                                    //二级
                                    if (secondstr.IndexOf('*') > -1)
                                    {
                                        secondName = secondstr.Split('*')[0].ToString();
                                        if (tempscd != secondName)
                                        {
                                            sbhtml.Append("<li>" + secondName + "</li>");
                                        }
                                        //所有子节点
                                        string substr = secondstr.Substring(secondstr.IndexOf('*'));
                                        string[] subArray = substr.Split('*');
                                        sbhtml.Append(GetThreeChildNodeHtml(subArray));
                                        if (tempscd != secondName)
                                        {
                                            tempscd = secondName;
                                        }
                                    }
                                    else
                                    {
                                        secondName = secondstr.Substring(0);
                                        sbhtml.Append("<li>" + secondName + "</li>");
                                    }
                                }
                            }
                        }
                        //第二层
                        sbhtml.Append("</ul>");
                    }

                }
                //最外层
                sbhtml.Append("</ul>");
            }
            else
            {
                string firstArray = structStr;
                //树开始
                sbhtml.Append("<ul>");
                if (!string.IsNullOrEmpty(firstArray))
                {
                    //类名称
                    string firstName = "";
                    //第二级别
                    if (firstArray.IndexOf('^') > -1)
                    {
                        //类名称    公共建筑
                        firstName = firstArray.Split('^')[0].ToString();
                        //第一层内容项
                        sbhtml.Append("<li>" + firstName + "</li>");
                        //第二层内容项
                        string secstr = firstArray.Substring(firstArray.IndexOf('^'));
                        //分隔二级
                        string[] secondArray = secstr.Split('^');
                        //判断量
                        string tempscd = "";
                        //拼接第一级外层
                        sbhtml.Append("<ul>");
                        foreach (string secondstr in secondArray)
                        {
                            string secondName = "";
                            if (secondstr != "")
                            {
                                //二级
                                if (secondstr.IndexOf('*') > -1)
                                {
                                    secondName = secondstr.Split('*')[0].ToString();
                                    if (tempscd != secondName)
                                    {
                                        sbhtml.Append("<li>" + secondName + "</li>");
                                    }
                                    //所有子节点
                                    string substr = secondstr.Substring(secondstr.IndexOf('*'));
                                    string[] subArray = substr.Split('*');
                                    sbhtml.Append(GetThreeChildNodeHtml(subArray));
                                    if (tempscd != secondName)
                                    {
                                        tempscd = secondName;
                                    }
                                }
                                else
                                {
                                    secondName = secondstr.Substring(0);
                                    sbhtml.Append("<li>" + secondName + "</li>");
                                }
                            }
                        }
                    }
                    //第二层
                    sbhtml.Append("</ul>");
                }
                //最外层
                sbhtml.Append("</ul>");
            }
            return sbhtml.ToString();
        }
        //三级节点拼接
        protected static string GetThreeChildNodeHtml(string[] subArray)
        {
            StringBuilder sb = new StringBuilder();

            string starthtml = "";
            string endhtml = "";
            starthtml = "<ul>";
            endhtml = "</ul>";
            for (int i = 0; i < subArray.Length; i++)
            {
                if (subArray[i] != "")
                {
                    if (i < subArray.Length - 1)
                    {
                        starthtml += "<li>" + subArray[i] + "</li><ul><li>" + subArray[i + 1] + "</li></ul>";
                    }
                    else
                    {
                        //判断是否大于二层节点的奇数节点需要缩进
                        if (i > 1)
                        {
                            starthtml += "<ul><li>" + subArray[i] + "</li></ul>";
                        }
                        else//仅一级节点不需要缩进
                        {
                            starthtml += "<li>" + subArray[i] + "</li>";
                        }
                    }
                    //上一个元素是下一个元素的子节点所以递归的时候一次递归上级与下级节点并且索引递增2
                    i++;
                }
            }
            sb.Append(starthtml);
            sb.Append(endhtml);

            return sb.ToString();
        }
        //textarea 赋值变化
        public static string HtmlEncode(string encodeString)
        {
            encodeString = encodeString.Replace("<", "&lt;");
            encodeString = encodeString.Replace(">", "&gt;");
            encodeString = encodeString.Replace(" ", "&nbsp;");
            encodeString = encodeString.Replace("’", "'");
            encodeString = encodeString.Replace(((char)13).ToString(), "<br>");
            return encodeString;
        }

        public static string HtmlDecode(string encodeString)
        {
            encodeString = encodeString.Replace("&lt;", "<");
            encodeString = encodeString.Replace("&gt;", ">");
            encodeString = encodeString.Replace("&nbsp;", " ");
            encodeString = encodeString.Replace("'", "’");
            encodeString = encodeString.Replace("<br>", ((char)13).ToString());
            return encodeString;
        }
    }
}
