﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.SqlClient;

namespace TG.Common.EntityBuilder
{
	/// <summary>
	/// 填充数据实体类
	/// </summary>
	public static class EntityBuilder<T>
	{
		/// <summary>
		/// T类型的属性数组
		/// </summary>
		private static PropertyInfo[] ProertyInfoArray;

		static EntityBuilder()
		{
			ProertyInfoArray = typeof(T).GetProperties();
		}

		/// <summary>
		/// 创建一个实体。并且填充
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public static T BuilderEntity(SqlDataReader reader)
		{
			T entity = default(T);
			if (reader.HasRows)
			{
				entity = Activator.CreateInstance<T>();
				//遍历每个属性，得到该属性所有的特性
				foreach (PropertyInfo propertyInfo in ProertyInfoArray)
				{
					object[] objAttributeArray = propertyInfo.GetCustomAttributes(false);
					for (int i = 0; i < objAttributeArray.Length; i++)
					{
						if (objAttributeArray[i] is DataMappingAttribute)
						{
							try
							{
								DataMappingAttribute att = (DataMappingAttribute)objAttributeArray[i];

								if (reader[att.ColumnName] != DBNull.Value)
								{
									propertyInfo.SetValue(entity, reader[att.ColumnName], null);
								}
							}
							catch (Exception ex)
							{
								throw ex;
							}
							break;
						}
					}
				}
			}
			return entity;
		}

		public static List<T> BuilderEntityList(SqlDataReader reader)
		{
			List<T> resultList = new List<T>();
			while (reader.Read())
			{
				T entity = BuilderEntity(reader);
				resultList.Add(entity);
			}
			reader.Close();
			return resultList;
		}
	}
}
