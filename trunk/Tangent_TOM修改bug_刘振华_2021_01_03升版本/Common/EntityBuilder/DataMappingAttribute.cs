﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.Common.EntityBuilder
{
	/// <summary>
	/// 数据库绑定特性
	/// </summary>
	public class DataMappingAttribute : Attribute
	{
		public DataMappingAttribute(string columenName)
		{
			this.ColumnName = columenName;
		}

		public string ColumnName { get; set; }

	}
}
