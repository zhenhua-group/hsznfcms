﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Common
{
    public class CommCoperation
    {
        /// <summary>
        /// 方案设计是否被选中
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetCheckBox1(string str)
        {
            string[] strArr = str.Split(',');
            if (strArr[0] == "方案设计")
            {
                return "checked='checked'";
            }
            else
            {
                return "";
            }

        }
        /// <summary>
        /// 初步设计是否被选中情况
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetCheckBox2(string str)
        {
            string[] strArr = str.Split(',');
            if (strArr[1] == "初步设计")
            {
                return "checked='checked'";
            }
            else
            {
                return "";
            }

        }
        /// <summary>
        /// 施工图设计是否被选中
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetCheckBox3(string str)
        {
            string[] strArr = str.Split(',');
            if (strArr[2] == "施工图设计")
            {
                return "checked='checked'";
            }
            else
            {
                return "";
            }

        }
        /// <summary>
        /// 判断bool值 ，是否被选中
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static string Getbool(int level)
        {
            if (level == 1)
            {
                return "checked='checked'";
            }

            else
            {
                return "";
            }
        }
       
        /// <summary>
        /// 显示列表中多选框的
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetcheckItem(string str)
        {
            StringBuilder sb = new StringBuilder();
            string[] sArray = str.Split(',');
            foreach (string item in sArray)
            {
                sb.Append("<input type='checkbox'>" + item);
            }
            return sb.ToString();
        }
        /// <summary>
        /// 获得时间的简单格式（包含年月日）
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetEasyTime(DateTime dt)
        {
            return dt.ToShortDateString();
        }


        /// <summary>
        /// 获得地面的层数
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetCengPrestring(string str)
        {
            //3|4
            string[] sArray = str.Split('|');
            return sArray[0].ToString();
        }
        /// <summary>
        /// 获得地下的层数
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetCengNextstring(string str)
        {
            //3|4
            string[] sArray = str.Split('|');
            return sArray[1].ToString();
        }
        /// <summary>
        /// 拼接结构形式的表现层的列表方法
        /// </summary>
        /// <param name="strvalue"></param>
        /// <returns></returns>
        public static string GetToUi(string strvalue)
        {
            try
            {
                if (strvalue.Contains("Root"))
                {
                    strvalue = strvalue.Substring(5, strvalue.Length - 5);
                }
                strvalue = strvalue.Substring(1);
                string[] arrstr = strvalue.Split('^');
                StringBuilder builder = new StringBuilder();
                builder.Append("<fieldset><legend>" + arrstr[0] + "</legend>");
                builder.Append("<ul>");
                for (int i = 1; i < arrstr.Length; i++)
                {
                    builder.Append("<li>" + arrstr[i] + "</li>");
                }
                builder.Append("</ul>");
                builder.Append("</fieldset>");
                return builder.ToString();
            }
            catch (Exception)
            {
                return strvalue;
            }

        }

        /// <summary>
        /// 生成页面效果
        /// </summary>
        /// <param name="dictionaryCategory"></param>
        /// <returns></returns>
        public static string CreateULHTML(Dictionary<string, List<string>> dictionaryCategory)
        {
            string text1 = "";
            text1 = text1 + "<div class=\"menu\"><ul>";
            Dictionary<string, List<string>>.Enumerator enumerator1 = dictionaryCategory.GetEnumerator();
            try
            {
                while (enumerator1.MoveNext())
                {
                    KeyValuePair<string, List<string>> pair1 = enumerator1.Current;
                    text1 = text1 + "<li><a href=\"#\">" + pair1.Key + "<table><tr><td nowrap>";
                    text1 = text1 + "<ul>";
                    List<string>.Enumerator enumerator2 = pair1.Value.GetEnumerator();
                    try
                    {
                        while (enumerator2.MoveNext())
                        {
                            string text2 = enumerator2.Current;
                            text1 = text1 + "<li><a href=\"#\">" + text2 + "</a></li>";
                        }
                    }
                    finally
                    {
                        enumerator2.Dispose();
                    }
                    text1 = text1 + "</ul>";
                    text1 = text1 + "</td></tr></table></a></li>";
                }
            }
            finally
            {
                enumerator1.Dispose();
            }
            return (text1 + "</ul></div>");
        }


        /// <summary>
        /// 解析数据库的数据
        /// </summary>
        /// <param name="structString"></param>
        /// <returns></returns>
        public static Dictionary<string, List<string>> ResolveProjectStruct(string structString)
        {
            Dictionary<string, List<string>> dictionary1 = new Dictionary<string, List<string>>();
            if (string.IsNullOrEmpty(structString))
            {
                List<string> list1 = new List<string>();
                list1.Add("");
                dictionary1.Add("", (List<string>)list1);
                return dictionary1;
            }
            string[] textArray1 = structString.Trim().Split(new char[] { '+' });
            for (int num1 = 0; num1 < textArray1.Length; num1++)
            {
                string text1 = textArray1[num1];
                if (!string.IsNullOrEmpty(text1.Trim()))
                {
                    int num2 = text1.IndexOf('^');
                    string text2 = text1.Substring(0, num2);
                    text1 = text1.Substring(num2);
                    string[] textArray2 = text1.Split(new char[] { '^' });
                    List<string> list2 = new List<string>();
                    string[] textArray3 = textArray2;
                    for (int num3 = 0; num3 < textArray3.Length; num3++)
                    {
                        string text3 = textArray3[num3];
                        if (!string.IsNullOrEmpty(text3))
                        {
                            list2.Add(text3);
                        }
                    }
                    dictionary1.Add(text2, (List<string>)list2);
                }
            }
            return dictionary1;
        }
        /// <summary>
        /// 查询数据中不相同数据
        /// </summary>
        /// <returns></returns>
        public static List<string> SelectDistinct(string sql)
       {
           List<string> list = new List<string>();
          System.Data.DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
          if (dt != null && dt.Rows.Count > 0)
          {
              for (int i = 0; i < dt.Rows.Count; i++)
              {
                  list.Add(dt.Rows[i][0].ToString());
              }
          }
          return list;
       }
    }
}
