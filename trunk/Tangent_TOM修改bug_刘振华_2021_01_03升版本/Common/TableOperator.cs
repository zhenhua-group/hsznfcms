﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Common
{
    public class TableOperator
    {
        /// <summary>
        /// 创建表格行
        /// </summary>
        /// <param name="colums">表格列</param>
        /// <param name="configs">列合并配置</param>
        /// <param name="rowconfigs">行合并配置</param>
        /// <param name="htmls">单元格控件</param>
        /// <returns></returns>
        public string CreateTableRow(int colums, string[] configs, string[] rowconfigs, string[] htmls)
        {
            //声明列数
            int irownum = colums;
            //拼接字符串
            StringBuilder sb_tb = new StringBuilder();
            //行开始
            sb_tb.Append("<tr>");
            int rowcount = 0;
            //生成列
            for (int i = 0; i < irownum; i++)
            {
                //如果出现合并配置信息
                if (configs.Length > 0)
                {
                    //是否穿件和并列
                    bool iscreat = false;
                    //合并列数
                    int icolnum = 0;
                    for (int j = 0; j < configs.Length; j++)
                    {
                        string config = configs[j].ToString();
                        string[] icolnums = config.Split(new char[] { '|' }, StringSplitOptions.None);
                        //判断合并列开始的列序号
                        int istart = int.Parse(icolnums[0]) - 1;
                        if (istart == i)
                        {
                            iscreat = true;
                            icolnum = int.Parse(icolnums[1]);
                            break;
                        }
                    }
                    //合并行
                    bool isrowspan = false;
                    int irow = 0;
                    if (rowconfigs.Length > 0)
                    {
                        for (int k = 0; k < rowconfigs.Length; k++)
                        {
                            string rowconfig = rowconfigs[k].ToString();
                            string[] irowspan = rowconfig.Split(new char[] { '|' }, StringSplitOptions.None);
                            int kstart = int.Parse(irowspan[0]) - 1;
                            if (kstart == i)
                            {
                                isrowspan = true;
                                irow = int.Parse(irowspan[1]);
                                break;
                            }
                        }
                    }

                    if (iscreat)
                    {
                        //创建单元格
                        if (htmls[rowcount] != "NULL")
                        {
                            if (isrowspan)
                            {
                                sb_tb.Append("<td align=\"center\" colspan=\"" + icolnum + "\" rowspan=\"" + irow + "\">");
                            }
                            else
                            {
                                sb_tb.Append("<td align=\"center\" colspan=\"" + icolnum + "\">");
                            }
                            //单元格内容
                            if (htmls.Length > 0)
                            {
                                sb_tb.Append(htmls[rowcount].ToString());
                                rowcount++;
                            }
                            //列结束
                            sb_tb.Append("</td>");
                        }
                        //合并后所剩列数
                        i = i + (icolnum - 1);
                    }
                    else
                    {
                        //创建单元格
                        if (htmls[rowcount] != "NULL")
                        {
                            if (isrowspan)
                            {
                                sb_tb.Append("<td align=\"center\" rowspan=\"" + irow + "\">");
                            }
                            else
                            {
                                sb_tb.Append("<td align=\"center\">");
                            }
                            //单元格内容
                            if (htmls.Length > 0)
                            {
                                sb_tb.Append(htmls[rowcount].ToString());
                            }
                            //结束
                            sb_tb.Append("</td>");
                        }
                        rowcount++;
                    }
                }
                else
                {
                    //合并行
                    bool isrowspan = false;
                    int irow = 0;
                    if (rowconfigs.Length > 0)
                    {
                        for (int k = 0; k < rowconfigs.Length; k++)
                        {
                            string rowconfig = rowconfigs[k].ToString();
                            string[] irowspan = rowconfig.Split(new char[] { '|' }, StringSplitOptions.None);
                            int kstart = int.Parse(irowspan[0]) - 1;
                            if (kstart == i)
                            {
                                isrowspan = true;
                                irow = int.Parse(irowspan[1]);
                                break;
                            }
                        }
                    }
                    //创建单元格
                    if (htmls[rowcount] != "NULL")
                    {
                        //是否有行合并
                        if (isrowspan)
                        {
                            sb_tb.Append("<td align=\"center\" rowspan=\"" + irow + "\">");
                        }
                        else
                        {
                            sb_tb.Append("<td align=\"center\">");
                        }
                        //单元格内容
                        if (htmls.Length > 0)
                        {
                            sb_tb.Append(htmls[rowcount].ToString());
                        }
                        //结束
                        sb_tb.Append("</td>");
                    }
                    rowcount++;
                }
            }
            //行结束
            sb_tb.Append("</tr>");
            //返回结果
            return sb_tb.ToString();
        }
    }
}
