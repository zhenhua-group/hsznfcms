﻿using Aladdin.HASP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetLockBLL
{
    public class HaspLock
    {

        // 内置加密字符串
        private const string CheckCode = "EJECEADMEDECDYEFDSDYEG";
        /// 秘钥
        private const string vendorCodeString =
            "FSDcQ6P1zCeAzJmOKZfjC2vBBUkHlRkqPS5JqmYRtiukZRsCs+y8S/+6F7I4A7Q0Q8Y/OYuSyG8B3nNv" +
    "OkeV4y98IytnVHlLvL4nahhRcPpsZtTHyemLI8z1cU29CB1y2kzX6YqeHarEb29qw64T3PU0o8bMKi+3" +
    "IN4OeL/tMniYMmr6wthCllDrAISnNLLAFNYKh12ikBaNBXU8dL7B0k6KcBuSxjMeVvw8N7kL7LihgQsb" +
    "vm2I0N/cf/n+XXCkPkHOU6nwnMNJewnvdPD0VLuR6UgUwU1SMNonQ6N/w6lJfeC6vdkTZPwy819m0pmG" +
    "UXU+6ODiSELaASa2CPVkbTempn5k5uvc0JBI2ZRxmVTuyqpmF6rn9K12F2GDC3tV5KqSA7L2ckEygRL8" +
    "VsN7ZH3zumFRYagOrULtRh+Tb/2lP4S6+kp0IT09xMovQl9xixPCesJwph078x+zv6Qyx/5mpljlvHA5" +
    "ksGNON9gDHGfF5T5XykYQ/pMejNrEpwwCuCqrsk0pjJ6K9JP7FKtxJ35ccZ11Iww3FG4OxjAtM9bW+E6" +
    "JCTlT6Ax/n50Km6x5Ayz9LJvgdBxhHEiZ4sLoN9wYzWyoSif7/zaxHEt0uvGOSM9kYM26WVq9j9+C5Yg" +
    "c8UVa4T4yRRvP71oAf3biiZFjKo2vX/1p38z5UPzYzPb6OHGWSl+AL05E266txU7t4fH3G/NGgmJDFpy" +
    "N1DrRaQBJN0qzNde/3PeZs+OT6nLY3vKT9f9jq8rYB9wA78OluiAfXDHIzloW48cu4qPYgDzUqmdsSi4" +
    "K/8AcWVwgLAb7I5uxC3YOoPUaL1tHM1e3SkVWw/0KBXFCOUGuctxm9//IpEi3OBpwd7DSwybaMCo/9D6" +
    "yzMoy6UpzuLDZYsw8HQ0aJY/HUqvGmF6IW0umnTdX0sW8BArquNBeqqXTIEJ9upsijvXSzj5ID+Q70tA" +
    "pmfRlm1Ygoeqv5NjnvuD2w==";
        /// <summary>
        /// 配置加密字符串
        /// </summary>
        public string ConfigCode { get; set; }
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string ServerIP { get; set; }
        /// <summary>
        /// 产品ID
        /// </summary>
        public string ProductID { get; set; }
        /// <summary>
        /// 产品特征ID
        /// </summary>
        public int FeatureID { get; set; }
        /// <summary>
        /// 通讯配置文件
        /// </summary>
        private string DefaultScope
        {
            get
            {
                string defaultScope = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                                                                        "<haspscope>" +
                                                                        "<license_manager ip=\"" + ServerIP + "\">" +
                                                                        "<product id=\"" + ProductID + "\"></product>" +
                                                                        "<feature id=\"" + FeatureID.ToString() + "\"></feature>" +
                                                                        "</license_manager></haspscope> ";

                return defaultScope;
            }
        }

        /// <summary>
        /// 返回秘钥
        /// </summary>
        static protected string VendorCode
        {
            get
            {
                return vendorCodeString;
            }
        }
        /// <summary>
        /// 是否需要检查网络锁
        /// </summary>
        /// <returns></returns>
        protected bool IsCheckNetLock()
        {
            //配置文件加密
            string str_ConfigCode = this.ConfigCode;
            //验证
            if (!string.IsNullOrEmpty(str_ConfigCode))
            {
                str_ConfigCode = str_ConfigCode.Substring(0, str_ConfigCode.Length - 3);
                if (CheckCode == str_ConfigCode)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 登陆网络锁是否成功
        /// </summary>
        /// <returns></returns>
        public bool CheckNotLockSuccess()
        {
            //标示
            bool isSuccess = false;
            //需要检查网络锁
            if (IsCheckNetLock())
            {
                //登陆网络锁
                if (LoginDefaultAuto())
                {
                    isSuccess = true;
                }
            }
            else
            {
                isSuccess = true;
            }

            return isSuccess;
        }
        /// <summary>
        /// 检查网络锁是否登陆成功
        /// </summary>
        /// <returns></returns>
        protected bool LoginDefaultAuto()
        {
            //标示
            bool isLogin = false;
            try
            {
                //特性对象
                HaspFeature feature = HaspFeature.FromFeature(FeatureID);

                using (Hasp hasp = new Hasp(feature))
                {
                    //登陆
                    HaspStatus status = hasp.Login(VendorCode, DefaultScope);

                    if (HaspStatus.StatusOk == status)
                    {
                        isLogin = true;
                    }
                }
            }
            catch
            { }


            return isLogin;
        }
    }
}
