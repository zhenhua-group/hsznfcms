﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// 实体对象，收费目标值 
    /// </summary>
    [Serializable]
    public class cm_UnitCharge
    {
        public cm_UnitCharge()
        { }
        #region Model
        private int _id;
        private int? _unitid;
        private decimal? _unitallot;
        private string _allotyear;
        private int? _status;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UnitID
        {
            set { _unitid = value; }
            get { return _unitid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? UnitAllot
        {
            set { _unitallot = value; }
            get { return _unitallot; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AllotYear
        {
            set { _allotyear = value; }
            get { return _allotyear; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Status
        {
            set { _status = value; }
            get { return _status; }
        }
        #endregion Model

    }
}
