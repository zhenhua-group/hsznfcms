﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;


namespace TG.Model
{
	public class cm_ProjectDistributionJobNumberConfigEntity
	{
		[DataMapping("SysNo")]
		public int SysNo { get; set; }

		[DataMapping("ProcessDescription")]
		public string ProcessDescription { get; set; }

		[DataMapping("RoleSysNo")]
		public int RoleSysNo { get; set; }

		/// <summary>
		/// 角色名称
		/// </summary>
		[DataMapping("RoleName")]
		public string RoleName { get; set; }

		[DataMapping("Position")]
		public int Position { get; set; }

		[DataMapping("InUser")]
		public int InUser { get; set; }

		[DataMapping("InDate")]
		public DateTime InDate { get; set; }

		/// <summary>
		/// 所含用户SysNoString
		/// </summary>
		[DataMapping("Users")]
		public string UserSysNoArrayString { get; set; }

		/// <summary>
		/// 所含用户信息集合
		/// </summary>
		public List<TG.Model.tg_member> UserInfoList { get; set; }
	}
}
