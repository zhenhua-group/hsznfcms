﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeSpecMemsHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeSpecMemsHis
	{
		public cm_KaoHeSpecMemsHis()
		{}
		#region Model
		private int _id;
		private int? _actionuserid;
		private string _actioncontent;
		private int? _proid;
		private int? _prokhid;
		private int? _prokhnameid;
		private int? _khtypeid;
		private DateTime? _actiondatetime= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ActionUserID
		{
			set{ _actionuserid=value;}
			get{return _actionuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ActionContent
		{
			set{ _actioncontent=value;}
			get{return _actioncontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ProId
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ProKHId
		{
			set{ _prokhid=value;}
			get{return _prokhid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ProKHNameId
		{
			set{ _prokhnameid=value;}
			get{return _prokhnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? KHTypeId
		{
			set{ _khtypeid=value;}
			get{return _khtypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ActionDateTime
		{
			set{ _actiondatetime=value;}
			get{return _actiondatetime;}
		}
		#endregion Model

	}
}

