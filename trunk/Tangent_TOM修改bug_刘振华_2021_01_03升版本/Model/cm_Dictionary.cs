﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_Dictionary:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_Dictionary
	{
		public cm_Dictionary()
		{}
		#region Model
		private int _id;
		private string _dic_name;
		private string _dic_type;
		private string _dic_info;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dic_Name
		{
			set{ _dic_name=value;}
			get{return _dic_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dic_Type
		{
			set{ _dic_type=value;}
			get{return _dic_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dic_info
		{
			set{ _dic_info=value;}
			get{return _dic_info;}
		}
		#endregion Model

	}
}

