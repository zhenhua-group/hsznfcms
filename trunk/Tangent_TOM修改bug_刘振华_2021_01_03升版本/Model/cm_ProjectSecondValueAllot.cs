﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// 二次产值分配
    /// </summary>
    [Serializable]
    public partial class cm_ProjectSecondValueAllot
    {
        public cm_ProjectSecondValueAllot()
        { }
        #region Model
        private int _id;
        private int? _proid;
        private int? _allotid;
        private decimal? _thedeptvaluecount;
        private decimal? _auditcount;
        private decimal? _designcount;
        private decimal? _havccount;
        private string _status;
        private string _secondValue;
        private string _actualAllountTime;

        /// <summary>
        /// 年份
        /// </summary>
        public string ActualAllountTime
        {
            get { return _actualAllountTime; }
            set { _actualAllountTime = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ProID
        {
            set { _proid = value; }
            get { return _proid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AllotID
        {
            set { _allotid = value; }
            get { return _allotid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TheDeptValueCount
        {
            set { _thedeptvaluecount = value; }
            get { return _thedeptvaluecount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? AuditCount
        {
            set { _auditcount = value; }
            get { return _auditcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DesignCount
        {
            set { _designcount = value; }
            get { return _designcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HavcCount
        {
            set { _havccount = value; }
            get { return _havccount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }

        /// <summary>
        /// 二次产值分配
        /// </summary>
        public string SecondValue
        {
            get { return _secondValue; }
            set { _secondValue = value; }
        }
        #endregion Model

    }
}
