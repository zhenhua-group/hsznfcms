﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class cm_ProjectValueByMemberAuditStatus
    {
        public cm_ProjectValueByMemberAuditStatus()
        { }
        #region Model
        private int _id;
        private int? _proid;
        private int? _allotid;
        private int? _mem_id;
        private string _status;
        private int? _audituser;
        private DateTime? _auditdate;
        private int? _insertuser;
        private DateTime? _insertdate;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? proID
        {
            set { _proid = value; }
            get { return _proid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AllotID
        {
            set { _allotid = value; }
            get { return _allotid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? mem_id
        {
            set { _mem_id = value; }
            get { return _mem_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AuditUser
        {
            set { _audituser = value; }
            get { return _audituser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AuditDate
        {
            set { _auditdate = value; }
            get { return _auditdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? InsertUser
        {
            set { _insertuser = value; }
            get { return _insertuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }

        private string _auditSuggsion;

        public string AuditSuggsion
        {
            get { return _auditSuggsion; }
            set { _auditSuggsion = value; }
        }

        private string _tranType;

        public string TranType
        {
            get { return _tranType; }
            set { _tranType = value; }
        }
        #endregion Model
    }

    [Serializable]
    public class ProjectValueByMemberAuditStatusEntity
    {
        public int ProID { get; set; }
        public int AllotID { get; set; }
        public string Status { get; set; }
        public string AuditUser { get; set; }
        public int mem_id { get; set; }
        public string AuditSuggsion { get; set; }
        public int SysNo { get; set; }
    }
}
