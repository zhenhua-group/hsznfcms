﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_Project:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_Project
    {
        public cm_Project()
        { }
        #region Model
        private int _bs_project_id;
        private string _pro_order;
        private string _pro_name;
        private string _pro_kinds;
        private string _pro_buildunit;
        private string _pro_status;
        private int? _pro_level;
        private DateTime? _pro_starttime;
        private DateTime? _pro_finishtime;
        private int? _pro_src;
        private string _chgjia;
        private string _phone;
        private int _coperationsysno;
        private string _project_reletive;
        private decimal? _cpr_acount;
        private string _unit;
        private decimal? _projectscale;
        private string _buildaddress;
        private string _pro_intro;
        private string _industry;
        private string _buildtype;
        private string _projsub;
        private string _isjjs;
        private string _isnt;
        private string _shjb;
        private string _istj;
        /// <summary>
        /// 
        /// </summary>
        public int bs_project_Id
        {
            set { _bs_project_id = value; }
            get { return _bs_project_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_order
        {
            set { _pro_order = value; }
            get { return _pro_order; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_name
        {
            set { _pro_name = value; }
            get { return _pro_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_kinds
        {
            set { _pro_kinds = value; }
            get { return _pro_kinds; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_buildUnit
        {
            set { _pro_buildunit = value; }
            get { return _pro_buildunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_status
        {
            set { _pro_status = value; }
            get { return _pro_status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? pro_level
        {
            set { _pro_level = value; }
            get { return _pro_level; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? pro_startTime
        {
            set { _pro_starttime = value; }
            get { return _pro_starttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? pro_finishTime
        {
            set { _pro_finishtime = value; }
            get { return _pro_finishtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Pro_src
        {
            set { _pro_src = value; }
            get { return _pro_src; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJia
        {
            set { _chgjia = value; }
            get { return _chgjia; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Phone
        {
            set { _phone = value; }
            get { return _phone; }
        }

        public int CoperationSysNo
        {
            set { _coperationsysno = value; }
            get { return _coperationsysno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Project_reletive
        {
            set { _project_reletive = value; }
            get { return _project_reletive; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Cpr_Acount
        {
            set { _cpr_acount = value; }
            get { return _cpr_acount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Unit
        {
            set { _unit = value; }
            get { return _unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProjectScale
        {
            set { _projectscale = value; }
            get { return _projectscale; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildAddress
        {
            set { _buildaddress = value; }
            get { return _buildaddress; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_Intro
        {
            set { _pro_intro = value; }
            get { return _pro_intro; }
        }
        public string Industry
        {
            set { _industry = value; }
            get { return _industry; }
        }
        public string BuildType
        {
            set { _buildtype = value; }
            get { return _buildtype; }
        }
        public string ProjSub
        {
            set { _projsub = value; }
            get { return _projsub; }
        }
        public string JobNum { get; set; }

        public string cprID { get; set; }

        public int pro_DesignUnitID { get; set; }

        public int pro_category { get; set; }
        public int pro_flag { get; set; }
        public string StartTimeString
        {
            get
            {
                return Convert.ToDateTime(pro_startTime).ToString("yyyy-MM-dd");
            }
        }
        public string FinishTimeString
        {
            get
            {
                return Convert.ToDateTime(pro_finishTime).ToString("yyyy-MM-dd");
            }
        }
        //结构样式
        public string pro_StruType { get; set; }
        //
        public string Pro_number { get; set; }

        public int UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int ReferenceSysNo
        {
            get;
            set;
        }

        public string PMName { get; set; }

        public string PMPhone { get; set; }

        /// <summary>
        /// 录入人ID
        /// </summary>
        public int? InsertUserID
        {
            get;
            set;
        }
        /// <summary>
        /// 录入时间
        /// </summary>
        public DateTime? InsertDate
        {
            get;
            set;
        }
        /// <summary>
        /// 项目总负责ID
        /// </summary>
        public int PMUserID
        {
            get;
            set;
        }
        //经济所参与
        public string ISTrunEconomy
        {
            set { _isjjs = value; }
            get { return _isjjs; }
        }
        
         //暖通参与
        public string ISHvac
        {
            set { _isnt = value; }
            get { return _isnt; }
        }
        //审核级别
        public string AuditLevel
        {
            set { _shjb = value; }
            get { return _shjb; }
        }
        //是否土建所参与
        public string ISArch
        {
            set { _istj = value; }
            get { return _istj; }
        }

        #endregion Model
    }
}

