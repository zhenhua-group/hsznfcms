﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public partial class cm_ProjectInvoice
    {
        public cm_ProjectInvoice()
        { }
        #region Model
        private int _id;
        private int? _projid;
        private int? _cprid;
        private string _status;
        private string _applymark;
        private int? _applyid;
        private string _invoicename;
        private decimal? _invoiceamount;
        private string _invoicecode;
        private string _invoicetype;
        private DateTime? _invoicedate;
        private string _caiwumark;
        private int? _audituserid;
        private DateTime? _auditdate;
        private int? _insertuserid;
        private DateTime? _insertdate;
        private string _countryaddress;
        private string _countryphone;
        private string _bankaccount;
        private string _accountinfo;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? projID
        {
            set { _projid = value; }
            get { return _projid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cprID
        {
            set { _cprid = value; }
            get { return _cprid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ApplyMark
        {
            set { _applymark = value; }
            get { return _applymark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ApplyId
        {
            set { _applyid = value; }
            get { return _applyid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string InvoiceName
        {
            set { _invoicename = value; }
            get { return _invoicename; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? InvoiceAmount
        {
            set { _invoiceamount = value; }
            get { return _invoiceamount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string InvoiceCode
        {
            set { _invoicecode = value; }
            get { return _invoicecode; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string InvoiceType
        {
            set { _invoicetype = value; }
            get { return _invoicetype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InvoiceDate
        {
            set { _invoicedate = value; }
            get { return _invoicedate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CaiwuMark
        {
            set { _caiwumark = value; }
            get { return _caiwumark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AuditUserID
        {
            set { _audituserid = value; }
            get { return _audituserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AuditDate
        {
            set { _auditdate = value; }
            get { return _auditdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? InsertUserID
        {
            set { _insertuserid = value; }
            get { return _insertuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }

        public string InsertUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CountryAddress
        {
            set { _countryaddress = value; }
            get { return _countryaddress; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CountryPhone
        {
            set { _countryphone = value; }
            get { return _countryphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Bankaccount
        {
            set { _bankaccount = value; }
            get { return _bankaccount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Accountinfo
        {
            set { _accountinfo = value; }
            get { return _accountinfo; }
        }
        #endregion Model

    }

    [Serializable]
    public class cm_ProjectInvoiceEntity
    {
        public string pro_id { get; set; }
        public string cpr_id { get; set; }
        public string user_id { get; set; }
        public string ApplyMark { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAmount { get; set; }
        public string InvoiceType { get; set; }
        public string InvoiceDate { get; set; }
        public string CountryAddress { get; set; }
        public string CountryPhone { get; set; }
        public string Bankaccount { get; set; }
        public string Accountinfo { get; set; }
    }


    public class cm_Coperation_Info
    {
        public string cpr_id { get; set; }

        public string cpr_Acount { get; set; }

        public string cpr_Name { get; set; }

        public string Cst_Name { get; set; }

        public string cpr_Unit { get; set; }
        public string cpr_No { get; set; }
        public string pro_Name { get; set; }
       
    }
}
