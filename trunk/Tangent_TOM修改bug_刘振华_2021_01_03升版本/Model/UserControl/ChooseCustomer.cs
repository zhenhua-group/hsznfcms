﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ChooseCustomerQueryEntity
    {
        public int PageSize { get; set; }

        public int PageCurrent { get; set; }

        public string CustomerName { get; set; }

        public string CustomerLevel { get; set; }

        public string ProvinceName { get; set; }

        public string CityName { get; set; }

        public string LinkMan { get; set; }

        public string UserSysNo { get; set; }

        public string PreviewPower { get; set; }
    }

    [Serializable]
    public class CustomerViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("CustomerNo")]
        public string CustomerNo { get; set; }

        [DataMapping("CustomerName")]
        public string CustomerName { get; set; }

        [DataMapping("CustomerShortName")]
        public string CustomerShortName { get; set; }

        [DataMapping("Address")]
        public string Address { get; set; }

        [DataMapping("ZipCode")]
        public int ZipCode { get; set; }

        [DataMapping("LinkMan")]
        public string LinkMan { get; set; }

        [DataMapping("Phone")]
        public string Phone { get; set; }

        [DataMapping("Fax")]
        public string Fax { get; set; }
    }
}
