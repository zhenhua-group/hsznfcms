﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model.UserControl
{
    public class ChooseUserQueryEntity
    {
        public string NameOrLoginName { get; set; }

        public int DepartmentSysNo { get; set; }

        public string PhoneNumber { get; set; }

        public int PageSize { get; set; }

        public int PageCurrent { get; set; }
    }

    /// <summary>
    /// 新增产值专用
    /// </summary>
    public class ChooseProjectValueUserEntity
    {
        public string NameOrLoginName { get; set; }

        public int DepartmentName { get; set; }

        public string SpeName { get; set; }

        public int PageSize { get; set; }

        public int PageCurrent { get; set; }
    }
    [Serializable]
    public class ChooseUserViewEntity
    {
        [DataMapping("UserSysNo")]
        public int UserSysNo { get; set; }

        [DataMapping("Name")]
        public string Name { get; set; }

        [DataMapping("DepartmentName")]
        public string DepartmentName { get; set; }

        [DataMapping("DepartmentSysNo")]
        public int DepartmentSysNo { get; set; }

        [DataMapping("PhoneNumber")]
        public string PhoneNumber { get; set; }
    }

    /// <summary>
    /// 产值应用
    /// </summary>
    [Serializable]
    public class ChooseProjectValueUserViewEntity
    {
        [DataMapping("UserSysNo")]
        public int UserSysNo { get; set; }

        [DataMapping("Name")]
        public string Name { get; set; }

        [DataMapping("DepartmentName")]
        public string DepartmentName { get; set; }

        [DataMapping("DepartmentSysNo")]
        public int DepartmentSysNo { get; set; }

        [DataMapping("SpeName")]
        public string SpeName { get; set; }

        [DataMapping("Principalship")]
        public int Principalship { get; set; }
    }


    /// <summary>
    /// 外聘人员
    /// </summary>
    public class ChooseExternalUserEntity
    {
        public string NameOrLoginName { get; set; }

        public int DepartmentName { get; set; }

        public string SpeName { get; set; }

        public int PageSize { get; set; }

        public int PageCurrent { get; set; }
    }

    /// <summary>
    /// 外聘人员
    /// </summary>
    [Serializable]
    public class ChooseExternalUserViewEntity
    {
        [DataMapping("UserSysNo")]
        public int UserSysNo { get; set; }

        [DataMapping("Name")]
        public string Name { get; set; }

        [DataMapping("DepartmentName")]
        public string DepartmentName { get; set; }

        [DataMapping("DepartmentSysNo")]
        public int DepartmentSysNo { get; set; }

        [DataMapping("SpeName")]
        public string SpeName { get; set; }

    }

    [Serializable]
    public class AddExternalUser
    {

        public string MemName { get; set; }

        public int memSpecialityID { get; set; }

        public int memUnitID { get; set; }
    }
}
