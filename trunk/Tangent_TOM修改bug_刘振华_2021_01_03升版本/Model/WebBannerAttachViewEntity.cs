﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
	public class WebBannerAttachViewEntity
	{
		[DataMapping("SysNo")]
		public int SysNo { get; set; }

		[DataMapping("WebBannerSysNo")]
		public string WebBannerSysNo { get; set; }

		[DataMapping("RelativePath")]
		public string RelativePath { get; set; }

		[DataMapping("FileName")]
		public string FileName { get; set; }

		[DataMapping("FileType")]
		public string FileType { get; set; }

		[DataMapping("FileSize")]
		public int FileSize { get; set; }

		[DataMapping("UpLoadDate")]
		public DateTime UpLoadDate { get; set; }

        [DataMapping("IconPath")]
		public string IconPath { get; set; }

		public string UpLoadDateString
		{
			get
			{
				return UpLoadDate.ToString("yyyy-MM-dd");
			}
		}
	}
}
