﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class CoperationContrastMonthExcel
    {
        /// <summary>
        /// 生产部门
        /// </summary>
        [DataMapping("UnitID")]
        public string UnitID { get; set; }
        /// <summary>
        /// 生产部门
        /// </summary>
        [DataMapping("UnitName")]
        public string UnitName { get; set; }
        /// <summary>
        /// 上年目标值
        /// </summary>
        [DataMapping("BeforeTarget")]
        public string BeforeTarget { get; set; }
        /// <summary>
        /// 上年月完成
        /// </summary>
        [DataMapping("BeforeCurMonth")]
        public string BeforeCurMonth { get; set; }
        /// <summary>
        /// 累计完成百分比
        /// </summary>
        [DataMapping("CompleteBeforeVal")]
        public string CompleteBeforeVal { get; set; }
        /// <summary>
        /// 当年目标
        /// </summary>
        [DataMapping("CurrentTarget")]
        public string CurrentTarget { get; set; }
        /// <summary>
        /// 本月完成
        /// </summary>
        [DataMapping("CurrentMonth")]
        public string CurrentMonth { get; set; }
        /// <summary>
        /// 月同比
        /// </summary>
        [DataMapping("CompareMonthVal")]
        public string CompareMonthVal { get; set; }
        /// <summary>
        /// 当前的合同完成率
        /// </summary>
        [DataMapping("CompleteCurrentVal")]
        public string CompleteCurrentVal { get; set; }
        /// <summary>
        /// 累计所有同比
        /// </summary>
        [DataMapping("CompareAllVal")]
        public string CompareAllVal { get; set; }
    }
}
