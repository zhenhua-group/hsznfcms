﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    /// <summary>
    /// 工程出图卡
    /// </summary>
    public class cm_ProjectPlotInfo
    {
        private int _id;
        private int _pro_id;
        private string _plotType;
        private int? _blueprintCounts;


        private int? _plotUserID;
        private string _plotUserName;
        public int? BlueprintCounts
        {
            get { return _blueprintCounts; }
            set { _blueprintCounts = value; }
        }

        public int? PlotUserID
        {
            get { return _plotUserID; }
            set { _plotUserID = value; }
        }


        public string PlotUserName
        {
            get { return _plotUserName; }
            set { _plotUserName = value; }
        }
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Pro_id
        {
            get { return _pro_id; }
            set { _pro_id = value; }
        }

        public string PlotType
        {
            get { return _plotType; }
            set { _plotType = value; }
        }

        public string FileNum { get; set; }
    }

    public class cm_ProjectPlotAuditQueryEntity
    {
        public int ProjectPlotAuditSysNo { get; set; }

        public int ProjectSysNo { get; set; }
    }

    public class cm_ProjectSubPlotInfo
    {
        private int _id;
        private int _pro_id;
        private string _mapsheet;
        private string _lengthenType;
        private string _specialty;
        private int counts;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Pro_id
        {
            get { return _pro_id; }
            set { _pro_id = value; }
        }

        public string Mapsheet
        {
            get { return _mapsheet; }
            set { _mapsheet = value; }
        }

        public string LengthenType
        {
            get { return _lengthenType; }
            set { _lengthenType = value; }
        }

        public string Specialty
        {
            get { return _specialty; }
            set { _specialty = value; }
        }

        public int Counts
        {
            get { return counts; }
            set { counts = value; }
        }

    }

    /// <summary>
    /// 出图卡
    /// </summary>
    [Serializable]
    public class ProjectPlotInfoEntity
    {
        public int ProNo { get; set; }
        public string PlotType { get; set; }
        public int? BlueprintCounts { get; set; }
        public int PlotUserID { get; set; }
        public string PlotUserName { get; set; }
        public List<ProjectSubPlotInfoEntity> PlotSub { get; set; }
    }

    /// <summary>
    ///  出图卡子项
    /// </summary>
    public class ProjectSubPlotInfoEntity
    {
        public string Mapsheet { get; set; }
        public string LengthenType { get; set; }
        public string Specialty { get; set; }
        public int StandardCount { get; set; }
        public int QuarterCount { get; set; }
        public int SecondQuarterCount { get; set; }
        public int ThreeQuarterCount { get; set; }
        public int FourQuarterCount { get; set; }
        public decimal? TotalCount { get; set; }
    }

    public class ProjectPlotAuditList
    {
        private int? _pro_ID;
        private string _pro_name;
        private string _pro_number;
        private string _pro_level;
        private string _auditLevel;
        private string _unit;
        private string _pMName;
        private string _pro_status;
        private int? _insertUserID;
        private DateTime? _pro_startTime;
        private string _status;
        private int? _plotID;
        private string _Plot_Type;


        public int? Pro_ID
        {
            get { return _pro_ID; }
            set { _pro_ID = value; }
        }

        public string Pro_name
        {
            get { return _pro_name; }
            set { _pro_name = value; }
        }

        public string Pro_number
        {
            get { return _pro_number; }
            set { _pro_number = value; }
        }

        public string Pro_level
        {
            get { return _pro_level; }
            set { _pro_level = value; }
        }

        public string AuditLevel
        {
            get { return _auditLevel; }
            set { _auditLevel = value; }
        }

        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        public string PMName
        {
            get { return _pMName; }
            set { _pMName = value; }
        }

        public string Pro_status
        {
            get { return _pro_status; }
            set { _pro_status = value; }
        }

        public int? InsertUserID
        {
            get { return _insertUserID; }
            set { _insertUserID = value; }
        }

        public DateTime? Pro_startTime
        {
            get { return _pro_startTime; }
            set { _pro_startTime = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int? PlotID
        {
            get { return _plotID; }
            set { _plotID = value; }
        }

        public string Plot_Type
        {
            get { return _Plot_Type; }
            set { _Plot_Type = value; }
        }

        /// <summary>
        /// 审核记录SysNo
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        public string Percent
        {
            get
            {
                int percent = 0;
                if (ProjectPlotInfoAuditEntity != null)
                {
                    if (Plot_Type == "正常输出" || Plot_Type == "补充图" || Plot_Type == "变更图")
                    {
                        switch (ProjectPlotInfoAuditEntity.Status)
                        {
                            case "A":
                                percent = 0;
                                break;
                            case "B":
                            case "C":
                                percent = (100 / 3) * 1;
                                break;
                            case "D":
                            case "E":
                                percent = (100 / 3) * 2;
                                break;
                            case "F":
                            case "G":
                                percent = 100;
                                break;
                        }
                    }
                    else
                    {

                        switch (ProjectPlotInfoAuditEntity.Status)
                        {
                            case "A":
                                percent = 0;
                                break;
                            case "B":
                            case "C":
                                percent = (100 / 8) * 1;
                                break;
                            case "D":
                            case "E":
                                percent = (100 / 8) * 2;
                                break;
                            case "F":
                            case "G":
                                percent = (100 / 8) * 3;
                                break;
                            case "H":
                            case "I":
                                percent = (100 / 8) * 4;
                                break;
                            case "J":
                            case "K":
                                percent = (100 / 8) * 5;
                                break;
                            case "L":
                            case "M":
                                percent = (100 / 8) * 6;
                                break;
                            case "N":
                            case "O":
                                percent = (100 / 8) * 7;
                                break;
                            case "P":
                            case "Q":
                                percent = 100;
                                break;
                        }
                    }
                }
                return percent.ToString("f2");
            }
        }

        /// <summary>
        /// 可执行操作
        /// </summary>
        public string ActionLink
        {
            get
            {
                string actionLinkString = string.Empty;
                if (Plot_Type == "正常输出" || Plot_Type == "补充图" || Plot_Type == "变更图")
                {
                    switch (AuditStatus)
                    {
                        case "":
                        case null:
                        case "C":
                        case "E":
                        case "G":
                        case "K":
                        case "I":
                        case "M":
                        case "O":
                        case "Q":
                            actionLinkString = " <a href='ModifyProjectDesignCards.aspx?project_Id=" + Pro_ID + " ' class=\"allowEdit\"  target=\"_self\">编辑</a>|<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + Pro_ID + "\">发起审批</span>";
                            break;
                        case "A":
                        case "B":
                        case "D":
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" proSysNo=\"" + Pro_ID + "\">审批中</span>";
                            break;
                        case "F":
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" proSysNo=\"" + Pro_ID + "\" ProjectPlotAuditSysNo=\"" + AuditRecordSysNo + "\">审核结果</span>";
                            break;
                    }
                }
                else
                {

                    switch (AuditStatus)
                    {
                        case "":
                        case null:
                        case "C":
                        case "E":
                        case "G":
                        case "K":
                        case "I":
                        case "M":
                        case "O":
                        case "Q":
                            actionLinkString = " <a href='ModifyProjectDesignCards.aspx?project_Id=" + Pro_ID + " ' class=\"allowEdit\"  target=\"_self\">编辑</a>|<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + Pro_ID + "\">发起审批</span>";
                            break;
                        case "A":
                        case "B":
                        case "D":
                        case "F":
                        case "H":
                        case "J":
                        case "L":
                        case "N":
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" proSysNo=\"" + Pro_ID + "\">审批中</span>";
                            break;
                        case "P":
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" proSysNo=\"" + Pro_ID + "\" ProjectPlotAuditSysNo=\"" + AuditRecordSysNo + "\">审核结果</span>";
                            break;
                    }
                }
                return actionLinkString;
            }
        }
        public cm_ProjectPlotInfoAudit ProjectPlotInfoAuditEntity { get; set; }
    }

    public class cm_ProjectPlotInfoAudit
    {
        /// <summary>
        /// 系统号子增
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        /// <summary>
        /// 记录人
        /// </summary>
        [DataMapping("InUser")]
        public int InUser { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 建议
        /// </summary>
        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }


        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        public string[] SuggestionArray
        {
            get
            {
                if (!string.IsNullOrEmpty(Suggestion))
                {
                    return Suggestion.Substring(0, Suggestion.Length - 1).Split('|');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditUserArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditUser))
                {
                    return AuditUser.Substring(0, AuditUser.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditDateArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditDate))
                {
                    return AuditDate.Substring(0, AuditDate.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }

    }
}
