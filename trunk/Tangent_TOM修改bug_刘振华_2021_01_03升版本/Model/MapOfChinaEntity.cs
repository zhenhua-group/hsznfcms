﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class MapOfChinaEntity
    {
        [DataMapping("Province")]
        public string ProvinceName { get; set; }

        [DataMapping("ProjectCount")]
        public int ProjectCount { get; set; }
    }

    public class ProjectForMap
    {
        [DataMapping("pro_ID")]
        public int SysNo { get; set; }

        [DataMapping("ReferenceSysNo")]
        public int ReferenceSysNo { get; set; }

        [DataMapping("pro_Order")]
        public string ProjectNumbder { get; set; }

        public string ProjectNumberString
        {
            get
            {
                return string.IsNullOrEmpty(ProjectNumbder) == true ? "" : ProjectNumbder;
            }
        }

        [DataMapping("pro_name")]
        public string ProjectName { get; set; }

        [DataMapping("pro_kinds")]
        public string ProjectType { get; set; }

        [DataMapping("pro_buildUnit")]
        public string BuildDepartment { get; set; }

        [DataMapping("pro_status")]
        public string Status { get; set; }

        [DataMapping("BuildAddress")]
        public string Address { get; set; }

        [DataMapping("pro_Level")]
        public int ManagerLevel { get; set; }

        public string ManagerLevelString
        {
            get
            {
                string str = "";
                switch (ManagerLevel)
                {
                    case 1: str = "一级"; break;
                    case 2: str = "二级"; break;
                    case 3: str = "三级"; break;
                    default: str = "一级"; break;
                }
                return str;
            }
        }

        [DataMapping("Project_reletive")]
        public string ReferenceCoperationName { get; set; }

        [DataMapping("pro_StartTime")]
        public DateTime StartTime { get; set; }
        [DataMapping("PMName")]
        public string PMNameUser { get; set; }
        public string StartTimeString
        {
            get
            {
                return StartTime.ToString("yyyy-MM-dd");
            }
        }

        [DataMapping("pro_FinishTime")]
        public DateTime EndTime { get; set; }

        public string EndTimeString
        {
            get
            {
                return EndTime.ToString("yyyy-MM-dd");
            }
        }
    }

    public class ProjectDetailForMap
    {
        [DataMapping("pro_ID")]
        public int SysNo { get; set; }

        [DataMapping("ReferenceSysNo")]
        public int ReferenceSysNo { get; set; }

        [DataMapping("pro_Order")]
        public string ProjectNumbder { get; set; }

        public string ProjectNumberString
        {
            get
            {
                return string.IsNullOrEmpty(ProjectNumbder) == true ? "" : ProjectNumbder;
            }
        }

        [DataMapping("pro_name")]
        public string ProjectName { get; set; }

        [DataMapping("pro_kinds")]
        public string ProjectType { get; set; }

        [DataMapping("pro_buildUnit")]
        public string BuildDepartment { get; set; }

        [DataMapping("pro_status")]
        public string Status { get; set; }

        [DataMapping("BuildAddress")]
        public string Address { get; set; }

        [DataMapping("pro_Level")]
        public int ManagerLevel { get; set; }

        public string ManagerLevelString
        {
            get
            {
                string str = "";
                switch (ManagerLevel)
                {
                    case 1: str = "一级"; break;
                    case 2: str = "二级"; break;
                    case 3: str = "三级"; break;
                    default: str = "一级"; break;
                }
                return str;
            }
        }

        [DataMapping("Project_reletive")]
        public string ReferenceCoperationName { get; set; }

        [DataMapping("pro_StartTime")]
        public DateTime StartTime { get; set; }

        public string StartTimeString
        {
            get
            {
                return StartTime.ToString("yyyy-MM-dd");
            }
        }

        [DataMapping("pro_FinishTime")]
        public DateTime EndTime { get; set; }

        public string EndTimeString
        {
            get
            {
                return EndTime.ToString("yyyy-MM-dd");
            }
        }

        [DataMapping("cpr_Name")]
        public string CopertaionName { get; set; }

        [DataMapping("pro_StruType")]
        public string StructStyleString { get; set; }

        public string StructStyleStringHtml
        {
            get
            {
                return TG.Common.StringPlus.ResolveStructString(StructStyleString);
            }
        }
        [DataMapping("dic_Name")]
        public string ProjectFrom { get; set; }

        [DataMapping("cpr_acount")]
        public decimal CoperationFinancial { get; set; }

        [DataMapping("pro_buildUnit")]
        public string BuildUnit { get; set; }

        [DataMapping("Unit")]
        public string UnderTakeDepartment { get; set; }

        [DataMapping("ProjectScale")]
        public decimal BuildScale { get; set; }

        [DataMapping("ChgJia")]
        public string ChargeManJia { get; set; }

        [DataMapping("Phone")]
        public string PhoneNumber { get; set; }

        [DataMapping("pro_Intro")]
        public string Remark { get; set; }
    }
}
