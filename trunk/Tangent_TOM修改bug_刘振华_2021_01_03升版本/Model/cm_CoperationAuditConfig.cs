﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    /// <summary>
    /// 合同审核配置实体
    /// </summary>
    public class cm_CoperationAuditConfig
    {
        /// <summary>
        /// 系统自增号
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary>
        /// 工作流
        /// </summary>
        [DataMapping("ProcessDescription")]
        public string Workflow { get; set; }

        /// <summary>
        /// 角色号
        /// </summary>
        [DataMapping("RoleSysNo")]
        public int RoleSysNo { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [DataMapping("RoleName")]
        public string RoleName { get; set; }

        /// <summary>
        /// 所含用户SysNoString
        /// </summary>
        public string UserSysNoArrayString { get; set; }

        /// <summary>
        /// 所含用户信息集合
        /// </summary>
        public List<TG.Model.tg_member> UserInfoList { get; set; }

        /// <summary>
        /// 索引位置
        /// </summary>
        [DataMapping("Position")]
        public int Position { get; set; }
    }
}
