﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_SubCoperation:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_SubCoperation
    {
        public cm_SubCoperation()
        { }
        #region Model
        private int _id;
        private int? _cpr_id;
        private string _cpr_no;
        private string _item_name;
        private string _item_area;
        private string _updateby;
        private DateTime? _lastupdate;
        private string _moneystatus;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Item_Name
        {
            set { _item_name = value; }
            get { return _item_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Item_Area
        {
            set { _item_area = value; }
            get { return _item_area; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }
        
        public decimal Money { get; set; }

        public string Remark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MoneyStatus
        {
            set
            {
                _moneystatus = value;
            }
            get
            {
                return _moneystatus;
            }
        }
        #endregion Model

    }
}

