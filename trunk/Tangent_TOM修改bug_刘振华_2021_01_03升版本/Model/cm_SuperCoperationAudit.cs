﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
	/// <summary>
	/// cm_SuperCoperationAudit:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_SuperCoperationAudit
	{
		public cm_SuperCoperationAudit()
		{}
		#region Model
		private int _sysno;
        private int _coperationsysno;
		private string _status;
		private string _suggestion;
		private string _audituser;
		private string _auditdate;
		private DateTime _indate;
		private int _inuser;
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("SysNo")]
		public int SysNo
		{
			set{ _sysno=value;}
			get{return _sysno;}
		}
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("CoperationSysNo")]
        public int CoperationSysNo
		{
            set { _coperationsysno = value; }
            get { return _coperationsysno; }
		}
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("Status")]
        public string Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("Suggestion")]
        public string Suggestion
		{
			set{ _suggestion=value;}
			get{return _suggestion;}
		}
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser
		{
			set{ _audituser=value;}
			get{return _audituser;}
		}
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("AuditDate")]
		public string AuditDate
		{
			set{ _auditdate=value;}
			get{return _auditdate;}
		}
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("InDate")]
        public DateTime InDate
		{
			set{ _indate=value;}
			get{return _indate;}
		}
		/// <summary>
		/// 
		/// </summary>
        [DataMapping("InUser")]
        public int InUser
		{
			set{ _inuser=value;}
			get{return _inuser;}
		}
        public string[] SuggestionArray
        {
            get
            {
                if (!string.IsNullOrEmpty(Suggestion))
                {
                    return Suggestion.Substring(0, Suggestion.Length - 1).Split('|');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditUserArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditUser))
                {
                    return AuditUser.Substring(0, AuditUser.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditDateArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditDate))
                {
                    return AuditDate.Substring(0, AuditDate.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }
		#endregion Model

	}
}

