﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    public partial class cm_RemunerationPercentSet
    {
        public cm_RemunerationPercentSet()
        { }
        #region Model
        private int _id;
        private int? _unit_id;
        private string _remuneration_year;
        private decimal? _remuneration_percent;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? unit_id
        {
            set { _unit_id = value; }
            get { return _unit_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Remuneration_Year
        {
            set { _remuneration_year = value; }
            get { return _remuneration_year; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Remuneration_percent
        {
            set { _remuneration_percent = value; }
            get { return _remuneration_percent; }
        }
        #endregion Model

    }
}
