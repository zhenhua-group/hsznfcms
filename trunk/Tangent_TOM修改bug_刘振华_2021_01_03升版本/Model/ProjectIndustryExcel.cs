﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ProjectIndustryExcel
    {

        /// <summary>
        /// 生产部门
        /// </summary>
        [DataMapping("UnitID")]
        public string UnitID { get; set; }
        /// <summary>
        /// 生产部门
        /// </summary>
        [DataMapping("UnitName")]
        public string UnitName { get; set; }
        /// <summary>
        /// 公建
        /// </summary>
        [DataMapping("GongJian")]
        public string GongJian { get; set; }
        /// <summary>
        /// 市政
        /// </summary>
        [DataMapping("ShiZheng")]
        public string ShiZheng { get; set; }
        /// <summary>
        /// 房地产
        /// </summary>
        [DataMapping("FangDiChan")]
        public string FangDiChan { get; set; }
        /// <summary>
        /// 综合
        /// </summary>
        [DataMapping("ZongHe")]
        public string ZongHe { get; set; }
        /// <summary>
        /// 合计
        /// </summary>
        [DataMapping("HeJi")]
        public string HeJi { get; set; }
        /// <summary>
        /// 公建
        /// </summary>
        [DataMapping("GongJianTwo")]
        public string GongJianTwo { get; set; }
        /// <summary>
        /// 市政
        /// </summary>
        [DataMapping("ShiZhengTwo")]
        public string ShiZhengTwo { get; set; }
        /// <summary>
        /// 房地产
        /// </summary>
        [DataMapping("FangDiChanTwo")]
        public string FangDiChanTwo { get; set; }
        /// <summary>
        /// 综合
        /// </summary>
        [DataMapping("ZongHeTwo")]
        public string ZongHeTwo { get; set; }
        /// <summary>
        /// 合计
        /// </summary>
        [DataMapping("HeJiTwo")]
        public string HeJiTwo { get; set; }
        /// <summary>
        /// 公建
        /// </summary>
        [DataMapping("CompareGongJian")]
        public string CompareGongJian { get; set; }
        /// <summary>
        /// 市政
        /// </summary>
        [DataMapping("CompareShiZheng")]
        public string CompareShiZheng { get; set; }
        /// <summary>
        /// 房地产
        /// </summary>
        [DataMapping("CompareFangDiChan")]
        public string CompareFangDiChan { get; set; }
        /// <summary>
        /// 综合
        /// </summary>
        [DataMapping("CompareZongHe")]
        public string CompareZongHe { get; set; }
        /// <summary>
        /// 合计
        /// </summary>
        [DataMapping("CompareHeJi")]
        public string CompareHeJi { get; set; }


    }
}
