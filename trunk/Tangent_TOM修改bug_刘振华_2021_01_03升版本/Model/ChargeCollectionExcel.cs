﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ChargeCollectionExcel
    {
        /// <summary>
        /// 月份
        /// </summary>
        [DataMapping("Month")]
        public string Month { get; set; }
        /// <summary>
        /// 凭单号
        /// </summary>
        [DataMapping("ChargeNo")]
        public string ChargeNo { get; set; }
        /// <summary>
        /// 详情
        /// </summary>
        [DataMapping("ChargeDetails")]
        public string ChargeDetails { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        [DataMapping("ChgDate")]
        public string ChgDate { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        [DataMapping("ChgAcount")]
        public string ChgAcount { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [DataMapping("ChgSub")]
        public string ChgSub { get; set; }
    }
}
