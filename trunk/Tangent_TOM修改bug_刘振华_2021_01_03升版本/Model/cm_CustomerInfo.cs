﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_CustomerInfo:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CustomerInfo
    {
        public cm_CustomerInfo()
        { }
        #region Model
        private int _cst_id;
        private string _cst_no;
        private string _cst_brief;
        private string _cst_name;
        private string _cpy_address;
        private string _code;
        private string _linkman;
        private byte[] _callingcard;
        private string _cpy_phone;
        private string _cpy_fax;
        private int? _updateby;
        private DateTime? _lastupdate;
        /// <summary>
        /// 
        /// </summary>
        public int Cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Cst_No
        {
            set { _cst_no = value; }
            get { return _cst_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Cst_Brief
        {
            set { _cst_brief = value; }
            get { return _cst_brief; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Cst_Name
        {
            set { _cst_name = value; }
            get { return _cst_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Cpy_Address
        {
            set { _cpy_address = value; }
            get { return _cpy_address; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Code
        {
            set { _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Linkman
        {
            set { _linkman = value; }
            get { return _linkman; }
        }
        /// <summary>
        /// 
        /// </summary>
        public byte[] CallingCard
        {
            set { _callingcard = value; }
            get { return _callingcard; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Cpy_Phone
        {
            set { _cpy_phone = value; }
            get { return _cpy_phone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Cpy_Fax
        {
            set { _cpy_fax = value; }
            get { return _cpy_fax; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }
        /// <summary>
        /// 录入人ID
        /// </summary>
        public int? InsertUserID
        {
            get;
            set;
        }
        /// <summary>
        /// 录入时间
        /// </summary>
        public DateTime? InsertDate
        {
            get;
            set;
        }
        #endregion Model

    }
}

