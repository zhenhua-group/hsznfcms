﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TG.Model
{
    /// <summary>
    /// cm_CostType:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CostType
    {
        public cm_CostType()
        { }
        #region Model
        private int? _id;
        private string _costname;
        private int? _costgroup;
        private int? _isinput;
        /// <summary>
        /// 
        /// </summary>
        public int? ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string costName
        {
            set { _costname = value; }
            get { return _costname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? costGroup
        {
            set { _costgroup = value; }
            get { return _costgroup; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? isInput
        {
            set { _isinput = value; }
            get { return _isinput; }
        }
        #endregion Model

    }
}


