﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cm_CoperationAudit
    {
        /// <summary>
        /// 系统号子增
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        [DataMapping("CoperationSysNo")]
        public int CoperationSysNo { get; set; }

        /// <summary>
        /// 记录人
        /// </summary>
        [DataMapping("InUser")]
        public int InUser { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 承接部门建议
        /// </summary>
        [DataMapping("UndertakeProposal")]
        public string UndertakeProposal { get; set; }

        /// <summary>
        /// 经营处建议
        /// </summary>
        [DataMapping("OperateDepartmentProposal")]
        public string OperateDepartmentProposal { get; set; }

        /// <summary>
        /// 技术部门建议
        /// </summary>
        [DataMapping("TechnologyDepartmentProposal")]
        public string TechnologyDepartmentProposal { get; set; }

        /// <summary>
        /// 总经理建议
        /// </summary>
        [DataMapping("GeneralManagerProposal")]
        public string GeneralManagerProposal { get; set; }

        /// <summary>
        /// 董事长建议
        /// </summary>
        [DataMapping("BossProposal")]
        public string BossProposal { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        /// <summary>
        /// 审核人姓名字符串
        /// </summary>
        public string AuditUserString { get; set; }

        /// <summary>
        /// 管理级别
        /// </summary>
        [DataMapping("ManageLevel")]
        public int ManageLevel { get; set; }

        /// <summary>
        /// 法律顾问
        /// </summary>
        [DataMapping("NeedLegalAdviser")]
        public int NeedLegalAdviser { get; set; }

        [DataMapping("LegalAdviserProposal")]
        public string LegalAdviserProposal { get; set; }
    }
    public class cm_CoperationAuditEdit
    {
        /// <summary>
        /// 系统号子增
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        [DataMapping("CoperationSysNo")]
        public int CoperationSysNo { get; set; }

        /// <summary>
        /// 记录人
        /// </summary>
        [DataMapping("InUser")]
        public int InUser { get; set; }

        /// <summary>
        /// 意见
        /// </summary>
        [DataMapping("Options")]
        public string Options { get; set; }
        /// <summary>
        /// 修改内蓉的选择
        /// </summary>
        [DataMapping("ChangeDetails")]
        public string ChangeDetails { get; set; }
        /// <summary>
        /// 记录时间
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 承接部门建议
        /// </summary>
        [DataMapping("UndertakeProposal")]
        public string UndertakeProposal { get; set; }

        /// <summary>
        /// 经营处建议
        /// </summary>
        [DataMapping("OperateDepartmentProposal")]
        public string OperateDepartmentProposal { get; set; }

        /// <summary>
        /// 技术部门建议
        /// </summary>
        [DataMapping("TechnologyDepartmentProposal")]
        public string TechnologyDepartmentProposal { get; set; }

        /// <summary>
        /// 总经理建议
        /// </summary>
        [DataMapping("GeneralManagerProposal")]
        public string GeneralManagerProposal { get; set; }

        /// <summary>
        /// 董事长建议
        /// </summary>
        [DataMapping("BossProposal")]
        public string BossProposal { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        /// <summary>
        /// 审核人姓名字符串
        /// </summary>
        public string AuditUserString { get; set; }

        /// <summary>
        /// 管理级别
        /// </summary>
        [DataMapping("ManageLevel")]
        public int ManageLevel { get; set; }

        /// <summary>
        /// 法律顾问
        /// </summary>
        [DataMapping("NeedLegalAdviser")]
        public int NeedLegalAdviser { get; set; }

        [DataMapping("LegalAdviserProposal")]
        public string LegalAdviserProposal { get; set; }
    }
    public enum RoleEnum
    {
        /// <summary>
        /// 承接部门负责人
        /// </summary>
        Undertake = 2,
        /// <summary>
        /// 经营处负责人
        /// </summary>
        Operate = 3,
        /// <summary>
        /// 技术部门负责人
        /// </summary>
        Technology = 4,
        /// <summary>
        /// 总经理
        /// </summary>
        GeneralManager = 5,
        /// <summary>
        /// 普通员工
        /// </summary>
        Employee = 6
    }

    public class AuditSysMsg
    {
        #region 消息列表要用的
        /// <summary>
        /// 消息记录表的SysNo
        /// </summary>
        public int SysMsgSysNo { get; set; }

        /// <summary>
        /// 合同审核记录SysNo
        /// </summary>
        public int CoperationAuditSysNo { get; set; }

        /// <summary>
        /// 合同名称
        /// </summary>
        public string CoperationName { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public int CoperationSysNo { get; set; }

        /// <summary>
        /// 合同审批状态
        /// </summary>
        public string CoperationStatus { get; set; }

        /// <summary>
        /// 在那个部门
        /// </summary>
        public string AuditDepartment
        {
            get
            {
                string str = "";
                switch (CoperationStatus)
                {
                    case "A":
                    case "C":
                        str = "承接部门";
                        break;
                    case "B":
                    case "E":
                        str = "经营处";
                        break;
                    case "D":
                    case "G":
                        str = "技术部门";
                        break;
                    case "H":
                    case "I":
                    case "F":
                        str = "总经理";
                        break;
                }
                return str;
            }
        }

        public string StatusString
        {
            get
            {
                string str = "";
                switch (CoperationStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        str = "审核中";
                        break;
                    case "H":
                        str = "审核通过";
                        break;
                    case "I":
                        str = "审核拒结";
                        break;
                }
                return str;
            }
        }

        /// <summary>
        /// 提交审核时间
        /// </summary>
        public DateTime CoperationInDate { get; set; }

        public string InDateString
        {
            get
            {
                return CoperationInDate.ToString("yyyy-MM-dd hh:mm:ss");
            }
        }

        /// <summary>
        /// 是否有电子合同即附件
        /// </summary>
        public string HaveElectronicCoperation { get; set; }
        #endregion
    }
}
