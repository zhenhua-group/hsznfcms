﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TG.Common;
using System.IO;


namespace TG.Model
{
    public static class XMLHelper
    {
        /// <summary>
        /// 反序列化XMLStream
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public static T Deserialize<T>(Stream fileStream)
        {
            XmlSerializer xmlserializer = new XmlSerializer(typeof(T));

            T resultEntity = (T)xmlserializer.Deserialize(fileStream);

            fileStream.Close();

            return resultEntity;
        }

        /// <summary>
        /// 序列化对象为XML
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlEntity"></param>
        public static void Serialize<T>(T xmlEntity, Stream fileStream)
        {
            XmlSerializer xmlserializer = new XmlSerializer(typeof(T));

            xmlserializer.Serialize(fileStream, xmlEntity);

            fileStream.Close();
        }

        /// <summary>
        /// 计算百分比
        /// </summary>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static decimal AccountPercent(int count, string status)
        {
            decimal percent = 0;
            switch (status)
            {
                case "A":
                    percent = 0;
                    break;
                case "B":
                case "C":
                    percent = (100 / count) * 1;
                    break;
                case "D":
                case "E":
                    percent = (100 / count) * 2;
                    break;
                case "F":
                case "G":
                    percent = (100 / count) * 3;
                    break;
                case "H":
                case "I":
                    percent = 100;
                    break;
            }
            return percent;
        }

    }
}
