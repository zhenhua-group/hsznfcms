﻿using System;
namespace TG.Model
{
    /// <summary>
    /// tg_unit:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class tg_unit
    {
        public tg_unit()
        { }
        #region Model
        private int _unit_id;
        private string _unit_name;
        private string _unit_intro;
        private int? _unit_parentid;
        private int? _unit_isendunit;
        /// <summary>
        /// 
        /// </summary>
        public int unit_ID
        {
            set { _unit_id = value; }
            get { return _unit_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string unit_Name
        {
            set { _unit_name = value; }
            get { return _unit_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? unit_ParentID
        {
            set { _unit_parentid = value; }
            get { return _unit_parentid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? unit_IsEndUnit
        {
            set { _unit_isendunit = value; }
            get { return _unit_isendunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string unit_Intro
        {
            set { _unit_intro = value; }
            get { return _unit_intro; }
        }
        #endregion Model

    }
}

