﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ProjectNumConfig:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ProjectNumConfig
	{
		public cm_ProjectNumConfig()
		{}
		#region Model
		private int _id;
		private string _prefix;
		private string _startnum;
		private string _endnum;
		private string _mark;
		private string _protype;
		private int? _flag;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PreFix
		{
			set{ _prefix=value;}
			get{return _prefix;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string StartNum
		{
			set{ _startnum=value;}
			get{return _startnum;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EndNum
		{
			set{ _endnum=value;}
			get{return _endnum;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Mark
		{
			set{ _mark=value;}
			get{return _mark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProType
		{
			set{ _protype=value;}
			get{return _protype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Flag
		{
			set{ _flag=value;}
			get{return _flag;}
		}
		#endregion Model

	}
}

