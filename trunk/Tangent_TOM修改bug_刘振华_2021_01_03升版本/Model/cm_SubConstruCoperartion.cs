﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    public class cm_SubConstruCoperartion
    {
        public cm_SubConstruCoperartion()
        { }
        #region Model
        private int _id;
        private int? _cpr_id;
        private string _cpr_no;
        private string _construction_name;
        private decimal? _project_diameter;
        private int? _quantity;
        private decimal? _pile_length;
        private decimal? _pit_area;
        private decimal? _depth;
        private string _support_form;
        private decimal? _well_amount;
        private decimal? _concrete_amount;
        private decimal? _reinforced_amount;
        private string _updateby;
        private DateTime? _lastupdate;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Construction_Name
        {
            set { _construction_name = value; }
            get { return _construction_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Project_Diameter
        {
            set { _project_diameter = value; }
            get { return _project_diameter; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Quantity
        {
            set { _quantity = value; }
            get { return _quantity; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Pile_Length
        {
            set { _pile_length = value; }
            get { return _pile_length; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Pit_Area
        {
            set { _pit_area = value; }
            get { return _pit_area; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Depth
        {
            set { _depth = value; }
            get { return _depth; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Support_Form
        {
            set { _support_form = value; }
            get { return _support_form; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Well_Amount
        {
            set { _well_amount = value; }
            get { return _well_amount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Concrete_Amount
        {
            set { _concrete_amount = value; }
            get { return _concrete_amount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Reinforced_Amount
        {
            set { _reinforced_amount = value; }
            get { return _reinforced_amount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }
        #endregion Model
    }
}
