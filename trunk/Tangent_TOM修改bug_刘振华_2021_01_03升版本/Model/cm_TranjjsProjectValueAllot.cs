﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    public class cm_TranjjsProjectValueAllot
    {
        #region Model
        private int _id;
        private int? _pro_id;
        private int? _allotid;
        private decimal? _totalcount;
        private decimal? _allotcount;
        private int _itemtype;
        private string _status;
        private decimal? _thedeptallotpercent;
        private decimal? _thedeptallotcount;
        private decimal? _programpercent;
        private decimal? _programcount;
        private decimal? _designmanagerpercent;
        private decimal? _designmanagercount;
        private decimal? _shouldbevaluepercent;
        private decimal? _shouldbevaluecount;
        private int? _audituser;
        private DateTime? _auditdate;
        private decimal? _paidvaluepercent;
        private decimal? _paidvaluecount;
        private string _actualAllountTime;

        public string ActualAllountTime
        {
            get { return _actualAllountTime; }
            set { _actualAllountTime = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Pro_ID
        {
            set { _pro_id = value; }
            get { return _pro_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AllotID
        {
            set { _allotid = value; }
            get { return _allotid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TotalCount
        {
            set { _totalcount = value; }
            get { return _totalcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? AllotCount
        {
            set { _allotcount = value; }
            get { return _allotcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ItemType
        {
            set { _itemtype = value; }
            get { return _itemtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }

        /// <summary>
        /// 实收比例
        /// </summary>
        public decimal? PaidValuePercent
        {
            set { _paidvaluepercent = value; }
            get { return _paidvaluepercent; }
        }
        /// <summary>
        /// 实收金额
        /// </summary>
        public decimal? PaidValueCount
        {
            set { _paidvaluecount = value; }
            get { return _paidvaluecount; }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal? Thedeptallotpercent
        {
            set { _thedeptallotpercent = value; }
            get { return _thedeptallotpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Thedeptallotcount
        {
            set { _thedeptallotcount = value; }
            get { return _thedeptallotcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProgramPercent
        {
            set { _programpercent = value; }
            get { return _programpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProgramCount
        {
            set { _programcount = value; }
            get { return _programcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DesignManagerPercent
        {
            set { _designmanagerpercent = value; }
            get { return _designmanagerpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DesignManagerCount
        {
            set { _designmanagercount = value; }
            get { return _designmanagercount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ShouldBeValuePercent
        {
            set { _shouldbevaluepercent = value; }
            get { return _shouldbevaluepercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ShouldBeValueCount
        {
            set { _shouldbevaluecount = value; }
            get { return _shouldbevaluecount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AllotUser
        {
            set { _audituser = value; }
            get { return _audituser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AllotDate
        {
            set { _auditdate = value; }
            get { return _auditdate; }
        }
        #endregion Model
    }
}
