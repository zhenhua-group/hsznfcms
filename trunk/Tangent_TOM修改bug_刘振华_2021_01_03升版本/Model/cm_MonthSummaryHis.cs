﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_MonthSummaryHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_MonthSummaryHis
	{
		public cm_MonthSummaryHis()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private string _mem_name;
		private int _mem_unit_id;
		private string _datadate;
		private int _datayear;
		private int _datamonth;
		private decimal? _overtime;
		private decimal? _weekovertime;
		private string _content;
		private string _isiswdkjl;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_name
		{
			set{ _mem_name=value;}
			get{return _mem_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_unit_ID
		{
			set{ _mem_unit_id=value;}
			get{return _mem_unit_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dataDate
		{
			set{ _datadate=value;}
			get{return _datadate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dataYear
		{
			set{ _datayear=value;}
			get{return _datayear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dataMonth
		{
			set{ _datamonth=value;}
			get{return _datamonth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? overtime
		{
			set{ _overtime=value;}
			get{return _overtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? weekovertime
		{
			set{ _weekovertime=value;}
			get{return _weekovertime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string content
		{
			set{ _content=value;}
			get{return _content;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string isiswdkjl
		{
			set{ _isiswdkjl=value;}
			get{return _isiswdkjl;}
		}
		#endregion Model

	}
}

