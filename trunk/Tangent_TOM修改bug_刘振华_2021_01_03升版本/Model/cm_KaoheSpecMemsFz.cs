﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoheSpecMemsFz:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoheSpecMemsFz
	{
		public cm_KaoheSpecMemsFz()
		{}
		#region Model
		private int _id;
		private int _proid;
		private int _prokhid;
		private int _prokhnameid;
		private int _khtypeid;
		private int _memid;
		private string _memname;
		private string _spename;
		private int _speid;
		private decimal? _jixiao=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProId
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHId
		{
			set{ _prokhid=value;}
			get{return _prokhid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHNameId
		{
			set{ _prokhnameid=value;}
			get{return _prokhnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int KHTypeId
		{
			set{ _khtypeid=value;}
			get{return _khtypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeID
		{
			set{ _speid=value;}
			get{return _speid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Jixiao
		{
			set{ _jixiao=value;}
			get{return _jixiao;}
		}
		#endregion Model

	}
}

