﻿using System;
namespace TG.Model
{
    /// <summary>
    /// tg_member:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class tg_member
    {
        public tg_member()
        { }
        #region Model
        private int _mem_id;
        private string _mem_login;
        private string _mem_name;
        private string _mem_password;
        private DateTime? _mem_birthday;
        private string _mem_sex;
        private int _mem_unit_id;
        private int _mem_speciality_id;
        private int _mem_principalship_id;
        private string _mem_telephone;
        private string _mem_mobile;
        private DateTime? _mem_regtime;
        private DateTime? _mem_outtime;
        private int? _mem_status;
        private string _mem_stats = "0";
        private string _mem_lastip;
        private string _mem_hashkey;
        private int? _mem_postmessage;
        private string _mem_mailbox;
        private string _mem_tpmlastver;
        private int? _mem_isfired = 0;
        private string _mem_sign;
        private string _mem_signpwd;
        private string _mem_stamp;
        private int? _mem_ismanager;
        private int? _mem_ispromanager;
        private string _mem_hometown;
        private string _mem_graduationtime;
        private string _mem_graduationschool;
        private string _mem_title;
        private string _mem_major;
        private string _mem_remark;
        private string _mem_lasthostname;
        /// <summary>
        /// 
        /// </summary>
        public int mem_ID
        {
            set { _mem_id = value; }
            get { return _mem_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Login
        {
            set { _mem_login = value; }
            get { return _mem_login; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Name
        {
            set { _mem_name = value; }
            get { return _mem_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Password
        {
            set { _mem_password = value; }
            get { return _mem_password; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? mem_Birthday
        {
            set { _mem_birthday = value; }
            get { return _mem_birthday; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Sex
        {
            set { _mem_sex = value; }
            get { return _mem_sex; }
        }
        /// <summary>
        /// 单位
        /// </summary>
        public int mem_Unit_ID
        {
            set { _mem_unit_id = value; }
            get { return _mem_unit_id; }
        }
        /// <summary>
        /// 专业
        /// </summary>
        public int mem_Speciality_ID
        {
            set { _mem_speciality_id = value; }
            get { return _mem_speciality_id; }
        }
        /// <summary>
        /// 职位
        /// </summary>
        public int mem_Principalship_ID
        {
            set { _mem_principalship_id = value; }
            get { return _mem_principalship_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Telephone
        {
            set { _mem_telephone = value; }
            get { return _mem_telephone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Mobile
        {
            set { _mem_mobile = value; }
            get { return _mem_mobile; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? mem_RegTime
        {
            set { _mem_regtime = value; }
            get { return _mem_regtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? mem_OutTime
        {
            set { _mem_outtime = value; }
            get { return _mem_outtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? mem_Status
        {
            set { _mem_status = value; }
            get { return _mem_status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Stats
        {
            set { _mem_stats = value; }
            get { return _mem_stats; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_LastIP
        {
            set { _mem_lastip = value; }
            get { return _mem_lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_HashKey
        {
            set { _mem_hashkey = value; }
            get { return _mem_hashkey; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? mem_PostMessage
        {
            set { _mem_postmessage = value; }
            get { return _mem_postmessage; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Mailbox
        {
            set { _mem_mailbox = value; }
            get { return _mem_mailbox; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_TPMlastver
        {
            set { _mem_tpmlastver = value; }
            get { return _mem_tpmlastver; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? mem_isFired
        {
            set { _mem_isfired = value; }
            get { return _mem_isfired; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_sign
        {
            set { _mem_sign = value; }
            get { return _mem_sign; }
        }
        /// <summary>
        /// 签名密码
        /// </summary>
        public string mem_SignPwd
        {
            set { _mem_signpwd = value; }
            get { return _mem_signpwd; }
        }
        /// <summary>
        /// 是否有电子签章
        /// </summary>
        public string mem_Stamp
        {
            set { _mem_stamp = value; }
            get { return _mem_stamp; }
        }
        /// <summary>
        /// 管理层权限
        /// </summary>
        public int? mem_IsManager
        {
            set { _mem_ismanager = value; }
            get { return _mem_ismanager; }
        }
        /// <summary>
        /// 项目管理层权限
        /// </summary>
        public int? mem_IsProManager
        {
            set { _mem_ispromanager = value; }
            get { return _mem_ispromanager; }
        }
        /// <summary>
        /// 籍贯
        /// </summary>
        public string mem_Hometown
        {
            set { _mem_hometown = value; }
            get { return _mem_hometown; }
        }
        /// <summary>
        /// 毕业时间
        /// </summary>
        public string mem_GraduationTime
        {
            set { _mem_graduationtime = value; }
            get { return _mem_graduationtime; }
        }
        /// <summary>
        /// 毕业院校
        /// </summary>
        public string mem_GraduationSchool
        {
            set { _mem_graduationschool = value; }
            get { return _mem_graduationschool; }
        }
        /// <summary>
        /// 职称
        /// </summary>
        public string mem_Title
        {
            set { _mem_title = value; }
            get { return _mem_title; }
        }
        /// <summary>
        /// 所学专业
        /// </summary>
        public string mem_Major
        {
            set { _mem_major = value; }
            get { return _mem_major; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string mem_Remark
        {
            set { _mem_remark = value; }
            get { return _mem_remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_LastHostName
        {
            set { _mem_lasthostname = value; }
            get { return _mem_lasthostname; }
        }

        public int? mem_Order { get; set; }

        #endregion Model

    }
}

