﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cm_ProjectFileInfo
    {
        private int? _id;
        private int? _num;
        private string _name;
        private int? _pageCount;
        private int? _isExist;
        private string _remark;
        private int _inUser;
        private int? _project_Id;
        public int? Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int? Project_Id
        {
            get { return _project_Id; }
            set { _project_Id = value; }
        }

        public int? Num
        {
            get { return _num; }
            set { _num = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int? PageCount
        {
            get { return _pageCount; }
            set { _pageCount = value; }
        }

        public int? IsExist
        {
            get { return _isExist; }
            set { _isExist = value; }
        }

        public string Remark
        {
            get { return _remark; }
            set { _remark = value; }
        }


        public int InUser
        {
            get { return _inUser; }
            set { _inUser = value; }
        }

    }

    /// <summary>
    /// 归档
    /// </summary>
    [Serializable]
    public class ProjectFileInfoEntity
    {
        public int? ProNo { get; set; }
        public int? UserId { get; set; }
        public List<ProjectSubFileInfoEntity> File { get; set; }
    }

    /// <summary>
    ///  归档
    /// </summary>
    public class ProjectSubFileInfoEntity
    {
        public int? num { get; set; }
        public int? PageCount { get; set; }
        public string name { get; set; }
        public int? IsExist { get; set; }
        public string Remark { get; set; }
    }

    public class ProjectFileAuditList
    {
        private int? _pro_ID;
        private string _pro_name;
        private string _pro_number;
        private string _pro_level;
        private string _auditLevel;
        private string _unit;
        private string _pMName;
        private string _pro_status;
        private int? _insertUserID;
        private DateTime? _pro_startTime;
        private string _status;
        private int? _plotID;
        private string _Plot_Type;


        public int? Pro_ID
        {
            get { return _pro_ID; }
            set { _pro_ID = value; }
        }

        public string Pro_name
        {
            get { return _pro_name; }
            set { _pro_name = value; }
        }

        public string Pro_number
        {
            get { return _pro_number; }
            set { _pro_number = value; }
        }

        public string Pro_level
        {
            get { return _pro_level; }
            set { _pro_level = value; }
        }

        public string AuditLevel
        {
            get { return _auditLevel; }
            set { _auditLevel = value; }
        }

        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        public string PMName
        {
            get { return _pMName; }
            set { _pMName = value; }
        }

        public string Pro_status
        {
            get { return _pro_status; }
            set { _pro_status = value; }
        }

        public int? InsertUserID
        {
            get { return _insertUserID; }
            set { _insertUserID = value; }
        }

        public DateTime? Pro_startTime
        {
            get { return _pro_startTime; }
            set { _pro_startTime = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>
        /// 审核记录SysNo
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        public string Percent
        {
            get
            {
                int percent = 0;
                if (ProjectFileInfoAuditEntity != null)
                {

                    switch (ProjectFileInfoAuditEntity.Status)
                    {
                        case "A":
                            percent = 0;
                            break;
                        case "B":
                        case "C":
                            percent = (100 / 2) * 1;
                            break;
                        case "D":
                        case "E":
                            percent = 100;
                            break;
                    }

                }
                return percent.ToString("f2");
            }
        }

        /// <summary>
        /// 可执行操作
        /// </summary>
        public string ActionLink
        {
            get
            {
                string actionLinkString = string.Empty;

                switch (AuditStatus)
                {
                    case "":
                    case null:
                    case "C":
                    case "E":
                        actionLinkString = "  <a href='ModifyProjectFileInfo.aspx?project_Id="+Pro_ID+"' class='allowEdit' target='_self'>编辑</a>|<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + Pro_ID + "\">发起审批</span>";
                        break;
                    case "A":
                    case "B":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" proSysNo=\"" + Pro_ID + "\">审批中</span>";
                        break;
                    case "D":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" proSysNo=\"" + Pro_ID + "\" ProjectFileAuditSysNo=\"" + AuditRecordSysNo + "\">审核结果</span>";
                        break;
                }

                return actionLinkString;
            }
        }
        public cm_ProjectFileInfoAudit ProjectFileInfoAuditEntity { get; set; }
    }

    public class cm_ProjectFileInfoAudit
    {
        /// <summary>
        /// 系统号子增
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        /// <summary>
        /// 记录人
        /// </summary>
        [DataMapping("InUser")]
        public int InUser { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 建议
        /// </summary>
        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }


        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        public string[] SuggestionArray
        {
            get
            {
                if (!string.IsNullOrEmpty(Suggestion))
                {
                    return Suggestion.Substring(0, Suggestion.Length - 1).Split('|');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditUserArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditUser))
                {
                    return AuditUser.Substring(0, AuditUser.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditDateArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditDate))
                {
                    return AuditDate.Substring(0, AuditDate.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }

    }

    public class cm_ProjectFileAuditQueryEntity
    {
        public int ProjectPlotAuditSysNo { get; set; }

        public int ProjectSysNo { get; set; }
    }
}
