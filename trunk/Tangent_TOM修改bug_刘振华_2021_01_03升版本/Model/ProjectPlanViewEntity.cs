﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class PlanProjectViewEntity
    {
        public int ProjectSysNo { get; set; }

        public List<PlanUserViewEntity> Member { get; set; }

        public List<PlanUserViewEntity> RoleUser { get; set; }

        public List<Item> Items { get; set; }
        //设计人员
        public List<PlanUserViewEntity> DesignMember { get; set; }
    }

    [Serializable]
    public class PlanUserViewEntity
    {
        public int UserSysNo { get; set; }

        public string RoleSysNo { get; set; }

        //是否为设计人员
        public int IsDesigner { get; set; }
    }

    [Serializable]
    public class SubItemViewEntity
    {
        public int ProjectSysNo { get; set; }

        public List<Item> Items { get; set; }
    }

    [Serializable]
    public class Item
    {
        public string DesignLevel { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int BuildingCount { get; set; }

        public int StructCount { get; set; }

        public int ElectricCount { get; set; }

        public int HVACCount { get; set; }

        public int DrainCount { get; set; }

        public int MunicipalAdministrationCount { get; set; }

        public string ItemName { get; set; }

    }
}
