﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class cm_TranProjectValueAllot
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private int _pro_ID;

        public int Pro_ID
        {
            get { return _pro_ID; }
            set { _pro_ID = value; }
        }
        private decimal _totalCount;

        public decimal TotalCount
        {
            get { return _totalCount; }
            set { _totalCount = value; }
        }
        private decimal _allotCount;

        public decimal AllotCount
        {
            get { return _allotCount; }
            set { _allotCount = value; }
        }
        private decimal _tranCount;

        public decimal TranCount
        {
            get { return _tranCount; }
            set { _tranCount = value; }
        }
        private string itemType;

        public string ItemType
        {
            get { return itemType; }
            set { itemType = value; }
        }
        private string _status;

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private decimal? _thedeptallotpercent;

        public decimal? Thedeptallotpercent
        {
            get { return _thedeptallotpercent; }
            set { _thedeptallotpercent = value; }
        }
        private decimal? _thedeptallotcount;

        public decimal? Thedeptallotcount
        {
            get { return _thedeptallotcount; }
            set { _thedeptallotcount = value; }
        }
        private decimal? _tranpercent;

        public decimal? Tranpercent
        {
            get { return _tranpercent; }
            set { _tranpercent = value; }
        }
        private decimal? _allotPercent;

        public decimal? AllotPercent
        {
            get { return _allotPercent; }
            set { _allotPercent = value; }
        }

        private DateTime? _allotDate;

        public DateTime? AllotDate
        {
            get { return _allotDate; }
            set { _allotDate = value; }
        }
        private int? allotUser;

        public int? AllotUser
        {
            get { return allotUser; }
            set { allotUser = value; }
        }

        private string _actualAllountTime;

        public string ActualAllountTime
        {
            get { return _actualAllountTime; }
            set { _actualAllountTime = value; }
        }
    }
}
