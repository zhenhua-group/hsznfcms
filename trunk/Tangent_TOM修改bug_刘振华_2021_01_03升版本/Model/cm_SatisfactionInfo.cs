﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_SatisfactionInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_SatisfactionInfo
	{
		public cm_SatisfactionInfo()
		{}
		#region Model
		private int _sat_id;
        private decimal _cst_id;
		private string _sat_no;
		private string _sch_context;
		private int? _sat_leve;
		private DateTime? _sch_time;
		private int? _sch_way;
		private string _sch_reason;
		private string _sat_best;
		private string _sat_notbest;
		private string _remark;
		private int? _updateby;
		private DateTime? _lastupdate;
		/// <summary>
		/// 
		/// </summary>
		public int Sat_Id
		{
			set{ _sat_id=value;}
			get{return _sat_id;}
		}
		/// <summary>
		/// 
		/// </summary>
        public decimal Cst_Id
		{
			set{ _cst_id=value;}
			get{return _cst_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Sat_No
		{
			set{ _sat_no=value;}
			get{return _sat_no;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Sch_Context
		{
			set{ _sch_context=value;}
			get{return _sch_context;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Sat_Leve
		{
			set{ _sat_leve=value;}
			get{return _sat_leve;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? Sch_Time
		{
			set{ _sch_time=value;}
			get{return _sch_time;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Sch_Way
		{
			set{ _sch_way=value;}
			get{return _sch_way;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Sch_Reason
		{
			set{ _sch_reason=value;}
			get{return _sch_reason;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Sat_Best
		{
			set{ _sat_best=value;}
			get{return _sat_best;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Sat_NotBest
		{
			set{ _sat_notbest=value;}
			get{return _sat_notbest;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdateBy
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastUpdate
		{
			set{ _lastupdate=value;}
			get{return _lastupdate;}
		}
		#endregion Model

	}
}

