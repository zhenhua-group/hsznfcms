﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeRenwuProj:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeRenwuProj
	{
		public cm_KaoHeRenwuProj()
		{}
		#region Model
		private int _id;
		private int _renwuid;
		private int _proid;
		private string _proname;
		private string _unitname;
		private int _unitid;
		private int? _isin=0;
		private int? _statu=0;
		private int _insertuserid;
		private DateTime? _insertdate= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int RenWuId
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int proID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string proName
		{
			set{ _proname=value;}
			get{return _proname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsIn
		{
			set{ _isin=value;}
			get{return _isin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Statu
		{
			set{ _statu=value;}
			get{return _statu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InsertUserId
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate
		{
			set{ _insertdate=value;}
			get{return _insertdate;}
		}
		#endregion Model

	}
}

