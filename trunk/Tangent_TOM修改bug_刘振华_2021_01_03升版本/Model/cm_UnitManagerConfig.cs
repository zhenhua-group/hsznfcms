﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_UnitManagerConfig:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_UnitManagerConfig
	{
		public cm_UnitManagerConfig()
		{}
		#region Model
		private int _id;
		private int _unit_id;
		private string _unitusers;
		private string _managerusers;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int unit_id
		{
			set{ _unit_id=value;}
			get{return _unit_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string unitusers
		{
			set{ _unitusers=value;}
			get{return _unitusers;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string managerusers
		{
			set{ _managerusers=value;}
			get{return _managerusers;}
		}
		#endregion Model

	}
}

