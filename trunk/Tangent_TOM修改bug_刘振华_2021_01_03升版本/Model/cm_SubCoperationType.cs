﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_SubCoperationType:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_SubCoperationType
	{
		public cm_SubCoperationType()
		{}
		#region Model
		private int _id;
		private int? _cpr_id=0;
		private string _cpr_no;
		private string _item_name;
		private decimal? _item_area=0M;
		private string _updateby;
		private DateTime? _lastupdate= DateTime.Now;
		private decimal? _money=0M;
		private string _remark;
		private string _subtype;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? cpr_Id
		{
			set{ _cpr_id=value;}
			get{return _cpr_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string cpr_No
		{
			set{ _cpr_no=value;}
			get{return _cpr_no;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Item_Name
		{
			set{ _item_name=value;}
			get{return _item_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Item_Area
		{
			set{ _item_area=value;}
			get{return _item_area;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdateBy
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastUpdate
		{
			set{ _lastupdate=value;}
			get{return _lastupdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Money
		{
			set{ _money=value;}
			get{return _money;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SubType
		{
			set{ _subtype=value;}
			get{return _subtype;}
		}
		#endregion Model

	}
}

