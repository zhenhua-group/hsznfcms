﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ChargeChangeRecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ChargeChangeRecord
	{
		public cm_ChargeChangeRecord()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private int? _mem_gongzi;
		private int? _mem_gongzi2;
		private int? _mem_yufa;
		private int? _mem_yufa2;
		private DateTime? _changedate;
		private int? _insertuserid;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_GongZi
		{
			set{ _mem_gongzi=value;}
			get{return _mem_gongzi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_GongZi2
		{
			set{ _mem_gongzi2=value;}
			get{return _mem_gongzi2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Yufa
		{
			set{ _mem_yufa=value;}
			get{return _mem_yufa;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Yufa2
		{
			set{ _mem_yufa2=value;}
			get{return _mem_yufa2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ChangeDate
		{
			set{ _changedate=value;}
			get{return _changedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		#endregion Model

	}
}

