﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_member:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class InterView
	{
        public InterView()
		{}
		#region Model
        private int _id;
        public int Id
        {
            set { _id = value; }
            get { return _id; }
        }

        private string _Interview_Name;
        public string Interview_Name
        {
            set { _Interview_Name = value; }
            get { return _Interview_Name; }
        }

        private string _Interview_Unit;
        public string Interview_Unit
        {
            set { _Interview_Unit = value; }
            get { return _Interview_Unit; }
        }

        private DateTime _Interview_Date;
        public DateTime Interview_Date
        {
            set { _Interview_Date = value; }
            get { return _Interview_Date; }
        }




        private string _Interview_results;
        public string Interview_results
        {
            set { _Interview_results = value; }
            get { return _Interview_results; }
        }

        private string _Interview_Remark;
        public string Interview_Remark
        {
            set { _Interview_Remark = value; }
            get { return _Interview_Remark; }
        }


		#endregion Model

	}
}

