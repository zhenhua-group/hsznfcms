﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
        public class ProjectStatisticsEntity
        {
                [DataMapping("pro_ID")]
                public int pro_ID { get; set; }

                [DataMapping("pro_order")]
                public string pro_order { get; set; }

                [DataMapping("pro_name")]
                public string pro_name { get; set; }

                public string Pro_number { get; set; }

                [DataMapping("pro_StruType")]
                public string pro_StruType { get; set; }

                [DataMapping("pro_kinds")]
                public string pro_kinds { get; set; }

                [DataMapping("pro_buildUnit")]
                public string pro_buildUnit { get; set; }

                [DataMapping("pro_status")]
                public string pro_status { get; set; }

                [DataMapping("pro_level")]
                public int pro_level { get; set; }

                [DataMapping("pro_startTime")]
                public DateTime pro_startTime { get; set; }

                [DataMapping("pro_finishTime")]
                public DateTime pro_finishTime { get; set; }

                public int Pro_src { get; set; }

                public string ChgJia { get; set; }

                public string Phone { get; set; }

                public int CoperationSysNo { get; set; }

                public string Project_reletive { get; set; }

                public decimal Cpr_Acount { get; set; }

                public string Unit { get; set; }

                public decimal ProjectScale { get; set; }

                public string BuildAddress { get; set; }

                public string pro_Intro { get; set; }
        }

        public class ProjectStatisticsQuarterEntity : ProjectStatisticsEntity
        {
                [DataMapping("Quarter")]
                public int Quarter { get; set; }
        }

        public class ProjectStatisticsMonthEntity : ProjectStatisticsEntity
        {
                [DataMapping("Month")]
                public int Month { get; set; }
        }

        public class ProjectStatisticsType
        {
                [DataMapping("ProjectType")]
                public string ProjectExtend { get; set; }

                [DataMapping("ProjectCount")]
                public int ProjectCount { get; set; }

                [DataMapping("Month")]
                public int Month { get; set; }
        }

        public class ProjectGroupByDepartment
        {
                [DataMapping("Department")]
                public string Department { get; set; }
        }

        public class ProjectStatisticeTypeByMonth : ProjectGroupByDepartment
        {
                /// <summary>
                /// 建筑工程
                /// </summary>
                public int ConstructionEngineeringCount { get; set; }

                /// <summary>
                /// 城乡规划
                /// </summary>
                public int UrbanAndRuralPlanningCount { get; set; }

                /// <summary>
                /// 市政工程
                /// </summary>
                public int MunicipalEngineeringCount { get; set; }

                /// <summary>
                /// 工程勘察
                /// </summary>
                public int EngineeringSurveyCount { get; set; }

                /// <summary>
                /// 咨询
                /// </summary>
                public int ConsultingCount { get; set; }

                /// <summary>
                /// 其他
                /// </summary>
                public int OtherCount { get; set; }
        }

        public class ProjectSquareMetersQuarter
        {
                [DataMapping("Quarter")]
                public int Quarter { get; set; }

                [DataMapping("SquareMeters")]
                public decimal SquareMeters { get; set; }
        }
}
