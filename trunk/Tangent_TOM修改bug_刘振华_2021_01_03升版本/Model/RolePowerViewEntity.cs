﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class RolePowerViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("RoleSysNo")]
        public int RoleSysNo { get; set; }

        [DataMapping("ActionPowerSysNo")]
        public int ActionPowerSysNo { get; set; }

        [DataMapping("Power")]
        public string Power { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }
    }

    public class RolePowerListViewEntity
    {
        [DataMapping("RolePowerSysNo")]
        public int RolePowerSysNo { get; set; }

        [DataMapping("Description")]
        public string Description { get; set; }

        [DataMapping("PageName")]
        public string PageName { get; set; }

        [DataMapping("ActionPowerSysNo")]
        public int ActionPowerSysNo { get; set; }

        [DataMapping("Power")]
        public string Power { get; set; }
    }
}
