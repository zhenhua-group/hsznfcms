﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeProjHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeProjHis
	{
		public cm_KaoHeProjHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _renwuname;
		private string _isfired;
		private string _unitname;
		private int? _unitid;
		private string _spename;
		private string _username;
		private int? _memid;
		private decimal? _projallot;
		private int? _unitorder=0;
		private int? _hporder=0;
		private decimal? _projallot2;
		private int? _isother=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IsFired
		{
			set{ _isfired=value;}
			get{return _isfired;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ProjAllot
		{
			set{ _projallot=value;}
			get{return _projallot;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitOrder
		{
			set{ _unitorder=value;}
			get{return _unitorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? HPOrder
		{
			set{ _hporder=value;}
			get{return _hporder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ProjAllot2
		{
			set{ _projallot2=value;}
			get{return _projallot2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsOther
		{
			set{ _isother=value;}
			get{return _isother;}
		}
		#endregion Model

	}
}

