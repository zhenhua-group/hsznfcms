﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_staffRelation:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_staffRelation
	{
		public tg_staffRelation()
		{}
		#region Model
		private int _id;
		private int _firstleader;
		private int? _secondleader;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int firstLeader
		{
			set{ _firstleader=value;}
			get{return _firstleader;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? secondLeader
		{
			set{ _secondleader=value;}
			get{return _secondleader;}
		}
		#endregion Model

	}
}

