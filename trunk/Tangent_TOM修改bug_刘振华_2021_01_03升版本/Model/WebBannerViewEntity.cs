﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class WebBannerViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("Title")]
        public string Title { get; set; }

        [DataMapping("Content")]
        public string Content { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        public List<WebBannerItemViewEntity> ItemList { get; set; }

    }

    [Serializable]
    public class WebBannerItemViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("WebBannerSysNo")]
        public int WebBannerSysNo { get; set; }

        [DataMapping("ToUser")]
        public int ToUser { get; set; }

        [DataMapping("UserName")]
        public string UserName { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }
    }

    public class WorkLogViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("Title")]
        public string Title { get; set; }

        [DataMapping("Content")]
        public string Content { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }
    }
}
