﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// TblArea:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class TblAreaModel
    {
        public TblAreaModel()
        { }
        #region Model
        private int _areaid;
        private string _areaname;
        private int? _areapid;
        private string _areasrccity;
        private int? _toyjycount;
        /// <summary>
        /// 
        /// </summary>
        public int AreaId
        {
            set { _areaid = value; }
            get { return _areaid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AreaName
        {
            set { _areaname = value; }
            get { return _areaname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AreaPId
        {
            set { _areapid = value; }
            get { return _areapid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AreaSrcCity
        {
            set { _areasrccity = value; }
            get { return _areasrccity; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ToYJYCount
        {
            set { _toyjycount = value; }
            get { return _toyjycount; }
        }
        #endregion Model

    }
}
