﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class AuditLocusViewEntity
    {
        [DataMapping("ProcessDescription")]
        public string ProcessDescription { get; set; }

        [DataMapping("AuditUserName")]
        public string AuditUserName { get; set; }

        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        public string AuditStatus { get; set; }

        public string IsPass { get; set; }
    }
}
