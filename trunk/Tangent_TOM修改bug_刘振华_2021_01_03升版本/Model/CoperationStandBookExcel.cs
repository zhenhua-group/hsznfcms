﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class CoperationStandBookExcel
    {
        /// <summary>
        /// 序号
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string CprNo { get; set; }
        /// <summary>
        /// 工程名称
        /// </summary>
        public string CprName { get; set; }
        /// <summary>
        /// 建筑面积
        /// </summary>
        public string BuildArea { get; set; }

        /// <summary>
        /// 合同额
        /// </summary>
        public string CprAcount { get; set; }
        /// <summary>
        /// 收款额
        /// </summary>
        public string ChgAcount { get; set; }
        /// <summary>
        /// 未收款
        /// </summary>
        public string NoChgAcount { get; set; }
        /// <summary>
        /// 收款次数及时间
        /// </summary>
        public string ChgDate { get; set; }
        /// <summary>
        /// 工程阶段
        /// </summary>
        public string ProjProcess { get; set; }
        /// <summary>
        /// 合同阶段
        /// </summary>
        public string CprProcess { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Sub { get; set; }
    }
}
