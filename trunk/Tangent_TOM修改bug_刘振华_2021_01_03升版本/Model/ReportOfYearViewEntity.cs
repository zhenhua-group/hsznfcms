﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class ReportOfYearViewEntity
    {
        [DataMapping("Year")]
        public int Year { get; set; }

        [DataMapping("ProjectCount")]
        public int ProjectCount { get; set; }

        [DataMapping("ProjectAuditPassCount")]
        public int ProjectAuditPassCount { get; set; }
    }

    public class ProjectOfReportOfYearViewEntity
    {
        [DataMapping("pro_ID")]
        public int ProjectSysNo { get; set; }

        [DataMapping("pro_order")]
        public string ProjectOrder { get; set; }

        [DataMapping("pro_name")]
        public string ProjectName { get; set; }

        [DataMapping("Pro_number")]
        public string ProjctNumber { get; set; }

        [DataMapping("pro_StruType")]
        public string ProjectSturctType { get; set; }

        [DataMapping("pro_kinds")]
        public string ProjectKinds { get; set; }

        [DataMapping("pro_buildUnit")]
        public string BuildUnit { get; set; }

        [DataMapping("pro_status")]
        public string ProjectStatus { get; set; }

        public string ProjectStatusString
        {
            get
            {
                string statusString = "进行中";
                if (ProjectStatus.Contains("暂停"))
                {
                    statusString = "暂停";
                }
                if (ProjectStatus.Contains("完成"))
                {
                    statusString = "完成";
                }
                return statusString;
            }
        }

        [DataMapping("pro_level")]
        public int ProjectLevel { get; set; }

        public string ManagerLevelString
        {
            get
            {
                string str = "";
                switch (ProjectLevel)
                {
                    case 1: str = "一级"; break;
                    case 2: str = "二级"; break;
                    case 3: str = "三级"; break;
                    default: str = "一级"; break;
                }
                return str;
            }
        }

        [DataMapping("pro_startTime")]
        public DateTime StartTime { get; set; }

        public string StartTimeString { get { return StartTime.ToString("yyyy-MM-dd HH-mm"); } }

        [DataMapping("pro_finishTime")]
        public DateTime FinishTime { get; set; }

        public string FinishTimeString { get { return FinishTime.ToString("yyyy-MM-dd HH-mm"); } }

        [DataMapping("Pro_src")]
        public int ProjectSrc { get; set; }

        [DataMapping("ProjectFrom")]
        public string ProjectFrom { get; set; }

        [DataMapping("ChgJia")]
        public string ChargeManJia { get; set; }

        [DataMapping("Phone")]
        public string PhoneNumber { get; set; }

        [DataMapping("CoperationSysNo")]
        public int CoperationSysNo { get; set; }

        [DataMapping("Project_reletive")]
        public string ReferenceCoperationName { get; set; }

        [DataMapping("Cpr_Acount")]
        public decimal Account { get; set; }

        [DataMapping("Unit")]
        public string Unit { get; set; }

        [DataMapping("ProjectScale")]
        public decimal Scale { get; set; }

        [DataMapping("BuildAddress")]
        public string BuildAddress { get; set; }

        [DataMapping("pro_Intro")]
        public string Remark { get; set; }

        [DataMapping("Status")]
        public string AuditStatus { get; set; }

        public string AuditStatusString
        {
            get
            {
                string resultString = "未提交审核";
                switch (AuditStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                        resultString = "审核中";
                        break;
                    case "D":
                        resultString = "审核通过";
                        break;
                    case "E":
                        resultString = "审核不通过";
                        break;
                }
                return resultString;
            }
        }

        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }

        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        public string BuilTypeStruct
        {
            get
            {
                return TG.Common.StringPlus.ResolveStructString(ProjectSturctType);
            }
        }
    }

    public class ProjectCountOfProjectTypeViewEntity
    {
        [DataMapping("ProjectCount")]
        public int ProjectCount { get; set; }

        [DataMapping("ProjectType")]
        public string ProjectType { get; set; }
    }

    public class ProjectScaleViewEntity
    {
        [DataMapping("ProjectScale")]
        public decimal Scale { get; set; }
    }

    public class ProjectStatusViewEntity
    {
        [DataMapping("RegisterProjectCount")]
        public int RegisterProjectCount { get; set; }

        [DataMapping("ProcessingProjectCount")]
        public int ProcessingProjectCount { get; set; }

        [DataMapping("PauseProjectCount")]
        public int PauseProjectCount { get; set; }

        [DataMapping("FinishProjectCount")]
        public int FinishProjectCount { get; set; }

    }


}
