﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_endSubItem:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_endSubItem
	{
		public tg_endSubItem()
		{}
		#region Model
		private int _purposeid;
		private int _subproid;
		/// <summary>
		/// 
		/// </summary>
		public int purposeID
		{
			set{ _purposeid=value;}
			get{return _purposeid;}
		}
		/// <summary>
		/// 子项目ID
		/// </summary>
		public int subproID
		{
			set{ _subproid=value;}
			get{return _subproid;}
		}
		#endregion Model

	}
}

