﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeSpePersent:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeSpePersent
	{
		public cm_KaoHeSpePersent()
		{}
		#region Model
		private int _id;
		private int _prokhnameid;
		private int _speroleid;
		private int _speid;
		private string _spename;
		private decimal _persentold;
		private decimal _persent1;
		private decimal? _persent2=0M;
		private int _inseruserid1;
		private DateTime? _inserdate1= DateTime.Now;
		private int? _inseruserid2=0;
		private DateTime? _inserdate2= DateTime.Now;
		private int? _stat=0;
		private int? _stat2=0;
		private decimal? _persent3;
		private int? _inseruserid3=0;
		private DateTime? _inserdate3;
		private int? _stat3=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHNameID
		{
			set{ _prokhnameid=value;}
			get{return _prokhnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeRoleID
		{
			set{ _speroleid=value;}
			get{return _speroleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeID
		{
			set{ _speid=value;}
			get{return _speid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Persentold
		{
			set{ _persentold=value;}
			get{return _persentold;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Persent1
		{
			set{ _persent1=value;}
			get{return _persent1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Persent2
		{
			set{ _persent2=value;}
			get{return _persent2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InserUserID1
		{
			set{ _inseruserid1=value;}
			get{return _inseruserid1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InserDate1
		{
			set{ _inserdate1=value;}
			get{return _inserdate1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InserUserID2
		{
			set{ _inseruserid2=value;}
			get{return _inseruserid2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InserDate2
		{
			set{ _inserdate2=value;}
			get{return _inserdate2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat2
		{
			set{ _stat2=value;}
			get{return _stat2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Persent3
		{
			set{ _persent3=value;}
			get{return _persent3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InserUserID3
		{
			set{ _inseruserid3=value;}
			get{return _inseruserid3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InserDate3
		{
			set{ _inserdate3=value;}
			get{return _inserdate3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat3
		{
			set{ _stat3=value;}
			get{return _stat3;}
		}
		#endregion Model

	}
}

