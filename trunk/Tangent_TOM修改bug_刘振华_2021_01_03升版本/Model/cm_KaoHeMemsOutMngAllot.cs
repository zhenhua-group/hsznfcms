﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeMemsOutMngAllot:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeMemsOutMngAllot
	{
		public cm_KaoHeMemsOutMngAllot()
		{}
		#region Model
		private int _id;
		private int? _renwuid;
		private int? _memid;
		private string _memname;
		private int? _unitid;
		private string _unitname;
		private decimal? _bmjltz=0M;
		private decimal? _beizhu=0M;
		private int? _stat=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bmjltz
		{
			set{ _bmjltz=value;}
			get{return _bmjltz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? beizhu
		{
			set{ _beizhu=value;}
			get{return _beizhu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

