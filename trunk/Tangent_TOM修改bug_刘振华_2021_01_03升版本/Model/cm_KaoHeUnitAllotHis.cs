﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeUnitAllotHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeUnitAllotHis
	{
		public cm_KaoHeUnitAllotHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _renwuname;
		private string _unitname;
		private decimal? _pjygz=0M;
		private decimal? _bmjx=0M;
		private decimal? _bmjjzb=0M;
		private decimal? _bmzjj=0M;
		private decimal? _ygzbs=0M;
		private decimal? _xmjj=0M;
		private decimal? _xmjjzb=0M;
		private decimal? _bmnjj=0M;
		private decimal? _beforeyear=0M;
		private decimal? _afteryear=0M;
		private decimal? _allcount=0M;
		private int? _unitid=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PJYGZ
		{
			set{ _pjygz=value;}
			get{return _pjygz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMJX
		{
			set{ _bmjx=value;}
			get{return _bmjx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMJJZB
		{
			set{ _bmjjzb=value;}
			get{return _bmjjzb;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMZJJ
		{
			set{ _bmzjj=value;}
			get{return _bmzjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? YGZBS
		{
			set{ _ygzbs=value;}
			get{return _ygzbs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMJJ
		{
			set{ _xmjj=value;}
			get{return _xmjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMJJZB
		{
			set{ _xmjjzb=value;}
			get{return _xmjjzb;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMNJJ
		{
			set{ _bmnjj=value;}
			get{return _bmnjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BeforeYear
		{
			set{ _beforeyear=value;}
			get{return _beforeyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AfterYear
		{
			set{ _afteryear=value;}
			get{return _afteryear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AllCount
		{
			set{ _allcount=value;}
			get{return _allcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		#endregion Model

	}
}

