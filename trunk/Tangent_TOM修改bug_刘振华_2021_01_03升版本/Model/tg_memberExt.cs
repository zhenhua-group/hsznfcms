﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_memberExt:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_memberExt
	{
		public tg_memberExt()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private string _mem_name;
		private DateTime? _mem_intime;//= DateTime.Now;
		private DateTime? _mem_outtime;//= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_Name
		{
			set{ _mem_name=value;}
			get{return _mem_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_InTime
		{
			set{ _mem_intime=value;}
			get{return _mem_intime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_OutTime
		{
			set{ _mem_outtime=value;}
			get{return _mem_outtime;}
		}
		#endregion Model

	}
}

