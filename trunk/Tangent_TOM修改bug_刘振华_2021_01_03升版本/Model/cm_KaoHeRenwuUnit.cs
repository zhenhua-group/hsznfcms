﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeRenwuUnit:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeRenwuUnit
	{
		public cm_KaoHeRenwuUnit()
		{}
		#region Model
		private int _id;
		private string _renwuno;
		private int _unitid;
		private string _unitname;
		private int _isin=0;
		private int _statu=0;
		private int? _issendmng=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 考核任务ID
		/// </summary>
		public string RenWuNo
		{
			set{ _renwuno=value;}
			get{return _renwuno;}
		}
		/// <summary>
		/// 部门id
		/// </summary>
		public int UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 部门名称
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 是否参与
		/// </summary>
		public int IsIn
		{
			set{ _isin=value;}
			get{return _isin;}
		}
		/// <summary>
		/// 是否已经提交考核
		/// </summary>
		public int Statu
		{
			set{ _statu=value;}
			get{return _statu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsSendMng
		{
			set{ _issendmng=value;}
			get{return _issendmng;}
		}
		#endregion Model

	}
}

