﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ProImageAuditConfig:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ProImageAuditConfig
	{
		public cm_ProImageAuditConfig()
		{}
		#region Model
		private int _sysno;
		private string _processdescription;
		private int _rolesysno;
		private int _position;
		private int? _inuser;
		private DateTime? _indate;
		/// <summary>
		/// 
		/// </summary>
		public int SysNo
		{
			set{ _sysno=value;}
			get{return _sysno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProcessDescription
		{
			set{ _processdescription=value;}
			get{return _processdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int RoleSysNo
		{
			set{ _rolesysno=value;}
			get{return _rolesysno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Position
		{
			set{ _position=value;}
			get{return _position;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InUser
		{
			set{ _inuser=value;}
			get{return _inuser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InDate
		{
			set{ _indate=value;}
			get{return _indate;}
		}
		#endregion Model

	}
}

