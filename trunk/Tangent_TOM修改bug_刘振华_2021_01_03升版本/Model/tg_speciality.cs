﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_speciality:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_speciality
	{
		public tg_speciality()
		{}
		#region Model
		private int _spe_id;
		private string _spe_name;
		private string _spe_intro;
		/// <summary>
		/// 
		/// </summary>
		public int spe_ID
		{
			set{ _spe_id=value;}
			get{return _spe_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string spe_Name
		{
			set{ _spe_name=value;}
			get{return _spe_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string spe_Intro
		{
			set{ _spe_intro=value;}
			get{return _spe_intro;}
		}
		#endregion Model

	}
}

