﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_config:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_config
	{
		public tg_config()
		{}
		#region Model
		private string _conf_name;
		private string _conf_value;
		private string _conf_desc;
		/// <summary>
		/// 
		/// </summary>
		public string conf_name
		{
			set{ _conf_name=value;}
			get{return _conf_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string conf_value
		{
			set{ _conf_value=value;}
			get{return _conf_value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string conf_desc
		{
			set{ _conf_desc=value;}
			get{return _conf_desc;}
		}
		#endregion Model

	}
}

