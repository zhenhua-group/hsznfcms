﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_WebBannerAttach:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_WebBannerAttach
	{
		public cm_WebBannerAttach()
		{}
		#region Model
		private int _sysno;
		private string _relativepath;
		private string _filename;
		private string _filetype;
		private int? _filesize;
		private DateTime _uploaddate;
        private int? _tempno;
		/// <summary>
		/// 
		/// </summary>
		public int SysNo
		{
			set{ _sysno=value;}
			get{return _sysno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RelativePath
		{
			set{ _relativepath=value;}
			get{return _relativepath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FileName
		{
			set{ _filename=value;}
			get{return _filename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FileType
		{
			set{ _filetype=value;}
			get{return _filetype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? FileSize
		{
			set{ _filesize=value;}
			get{return _filesize;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime UpLoadDate
		{
			set{ _uploaddate=value;}
			get{return _uploaddate;}
		}

        public int? TempNo
        {
            set { _tempno = value; }
            get { return _tempno; }
        }
		#endregion Model

	}
}

