﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_MemArchLevelRelation:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_MemArchLevelRelation
	{
		public cm_MemArchLevelRelation()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private int _archlevel_id;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ArchLevel_ID
		{
			set{ _archlevel_id=value;}
			get{return _archlevel_id;}
		}
		#endregion Model

	}
}

