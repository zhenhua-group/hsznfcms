﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeMemsWages:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeMemsWages
	{
		public cm_KaoHeMemsWages()
		{}
		#region Model
		private int _id;
		private int _memid;
		private string _memname;
		private int _sendyear;
		private int _sendmonth;
		private decimal? _jbgz;
		private decimal? _gwgz;
		private decimal _yfjj;
		private decimal? _jtbt;
		private decimal? _txbt;
		private decimal? _jbbz;
		private decimal? _cbcb;
		private decimal? _qita;
		private decimal? _qkkk;
		private decimal _yfgz;
		private decimal? _dksb;
		private decimal? _sksj;
		private decimal? _sfgz;
		private decimal? _zxgz;
		private decimal? _gscb;
		private decimal? _gwjt=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SendYear
		{
			set{ _sendyear=value;}
			get{return _sendyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SendMonth
		{
			set{ _sendmonth=value;}
			get{return _sendmonth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? jbgz
		{
			set{ _jbgz=value;}
			get{return _jbgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? gwgz
		{
			set{ _gwgz=value;}
			get{return _gwgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal yfjj
		{
			set{ _yfjj=value;}
			get{return _yfjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? jtbt
		{
			set{ _jtbt=value;}
			get{return _jtbt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? txbt
		{
			set{ _txbt=value;}
			get{return _txbt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? jbbz
		{
			set{ _jbbz=value;}
			get{return _jbbz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? cbcb
		{
			set{ _cbcb=value;}
			get{return _cbcb;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? qita
		{
			set{ _qita=value;}
			get{return _qita;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? qkkk
		{
			set{ _qkkk=value;}
			get{return _qkkk;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal yfgz
		{
			set{ _yfgz=value;}
			get{return _yfgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? dksb
		{
			set{ _dksb=value;}
			get{return _dksb;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? sksj
		{
			set{ _sksj=value;}
			get{return _sksj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? sfgz
		{
			set{ _sfgz=value;}
			get{return _sfgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? zxgz
		{
			set{ _zxgz=value;}
			get{return _zxgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? gscb
		{
			set{ _gscb=value;}
			get{return _gscb;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? gwjt
		{
			set{ _gwjt=value;}
			get{return _gwjt;}
		}
		#endregion Model

	}
}

