﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeMemsAllotChangeList:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeMemsAllotChangeList
	{
		public cm_KaoHeMemsAllotChangeList()
		{}
		#region Model
		private int _id;
		private int? _proid;
		private string _proname;
		private int? _renwuid;
		private string _renwuname;
		private int? _prokhid;
		private int? _khnameid;
		private string _khname;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? proID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string proName
		{
			set{ _proname=value;}
			get{return _proname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? renwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string renwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? proKHid
		{
			set{ _prokhid=value;}
			get{return _prokhid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? KhNameId
		{
			set{ _khnameid=value;}
			get{return _khnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string KhName
		{
			set{ _khname=value;}
			get{return _khname;}
		}
		#endregion Model

	}
}

