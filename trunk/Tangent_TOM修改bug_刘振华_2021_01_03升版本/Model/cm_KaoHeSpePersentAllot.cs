﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeSpePersentAllot:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeSpePersentAllot
	{
		public cm_KaoHeSpePersentAllot()
		{}
		#region Model
		private int _id;
		private int _prokhnameid;
		private int _allotid;
		private int _speid;
		private string _spename;
		private decimal _persentold;
		private decimal _persent1;
		private decimal? _persent2=0M;
		private decimal? _allotcount=0M;
		private decimal? _allotcountend=0M;
		private string _zcstr;
		private string _xmjystr;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHNameId
		{
			set{ _prokhnameid=value;}
			get{return _prokhnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int AllotID
		{
			set{ _allotid=value;}
			get{return _allotid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeID
		{
			set{ _speid=value;}
			get{return _speid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Persentold
		{
			set{ _persentold=value;}
			get{return _persentold;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Persent1
		{
			set{ _persent1=value;}
			get{return _persent1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Persent2
		{
			set{ _persent2=value;}
			get{return _persent2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AllotCount
		{
			set{ _allotcount=value;}
			get{return _allotcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AllotCountEnd
		{
			set{ _allotcountend=value;}
			get{return _allotcountend;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ZcStr
		{
			set{ _zcstr=value;}
			get{return _zcstr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string XmjyStr
		{
			set{ _xmjystr=value;}
			get{return _xmjystr;}
		}
		#endregion Model

	}
}

