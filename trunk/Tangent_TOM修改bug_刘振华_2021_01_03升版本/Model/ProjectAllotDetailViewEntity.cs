﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;


namespace TG.Model
{
    /// <summary>
    /// 项目详细信息最大级父类实体类
    /// </summary>
    [Serializable]
    public class ProjectAllotDetailViewEntity<T>
    {
        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo { get; set; }

        public T Body { get; set; }

    }

    /// <summary>
    /// 第一部分实体
    /// </summary>
    [Serializable]
    public class PartOne
    {
        /// <summary>
        /// 系统编号
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }
        /// <summary>
        /// 项目编号 
        /// </summary>
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }
        //设计编号
        [DataMapping("DesignNum")]
        public string DesignNum { get; set; }
        //建筑面积
        [DataMapping("BuildArea")]
        public decimal BuildArea { get; set; }
        //合同收费
        [DataMapping("CprCharge")]
        public decimal CprCharge { get; set; }
        //合同单价
        [DataMapping("Cprunitprice")]
        public decimal CprUnitprice { get; set; }
        //本次收费
        [DataMapping("CurCharge")]
        public decimal CurChange { get; set; }
        //当前费率
        [DataMapping("CurRate")]
        public decimal CurRate { get; set; }
        //税率
        [DataMapping("TaxRate")]
        public decimal TaxRate { get; set; }
        //最低收费标准分成基数
        [DataMapping("LowDivUnitArea")]
        public decimal LowDivUnitArea { get; set; }
        //最低收费标准分成基数（元）
        [DataMapping("LowDivUnitPrice")]
        public decimal LowDivUnitPrice { get; set; }
        //最低收费标准税后分成基数
        [DataMapping("LowRateUnitArea")]
        public decimal LowRateUnitArea { get; set; }
        //的收费标准税后分成基数（元）
        [DataMapping("LowRateUnitPrice")]
        public decimal LowRateUntiPrice { get; set; }
        [DataMapping("StrucType")]
        public int StrucType { get; set; }
        /// <summary>
        /// 状态 
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 录入日期
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }
    }
    /// <summary>
    /// 第二部分实体
    /// </summary>
    [Serializable]
    public class PartTwo
    {
        public int PartOneSysNo { get; set; }
        //项目分配表id
        [DataMapping("cost_ID")]
        public int cost_id { get; set; }
        //税后单项   本次发放比例
        [DataMapping("TaxSignel")]
        public decimal TaxSignel { get; set; }
        //分成税后金额
        [DataMapping("TaxSignelRate")]
        public decimal TaxSignelRate { get; set; }
        //人员
        [DataMapping("TaxSignelUser")]
        public string TaxSignelUser { get; set; }
        //院务管理   本次发放比例
        [DataMapping("CollegeTax")]
        public decimal CollegeTax { get; set; }
        [DataMapping("CollegeRate")]
        public decimal CollegeRate { get; set; }
        [DataMapping("CollegeUser")]
        public string CollegeUser { get; set; }

        //项目费用  本次发放比例
        [DataMapping("ProjectTax")]
        public decimal ProjectTax { get; set; }
        [DataMapping("ProjectRate")]
        public decimal ProjectRate { get; set; }
        [DataMapping("ProjectUser")]
        public string ProjectUser { get; set; }

        //项目负责  本次发放比例
        [DataMapping("ProChgTax")]
        public decimal ProChgTax { get; set; }
        [DataMapping("ProChgRate")]
        public decimal ProChgRate { get; set; }
        [DataMapping("ProChgUser")]
        public string ProChgUser { get; set; }

        //注册师  本次发放比例
        [DataMapping("RegisterTax")]
        public decimal RegisterTax { get; set; }
        [DataMapping("RegisterRate")]
        public decimal RegisterRate { get; set; }
        [DataMapping("RegisterUser")]
        public string RegisterUser { get; set; }

        //设校人员  本次发放比例
        [DataMapping("ShexTax")]
        public decimal ShexTax { get; set; }
        [DataMapping("ShexRate")]
        public decimal ShexRate { get; set; }
        [DataMapping("ShexUser")]
        public string ShexUser { get; set; }

        //校审核定  本次发放比例
        [DataMapping("ShedTax")]
        public decimal ShedTax { get; set; }
        [DataMapping("ShedRate")]
        public decimal ShedRate { get; set; }
        [DataMapping("ShedUser")]
        public string ShedUser { get; set; }

        //总工   本次发放比例
        [DataMapping("ZonggongTax")]
        public decimal ZonggongTax { get; set; }
        [DataMapping("ZonggongRate")]
        public decimal ZonggongRate { get; set; }
        [DataMapping("ZonggongUser")]
        public string ZonggongUser { get; set; }

        //院长  本次发放比例
        [DataMapping("YuanzTax")]
        public decimal YuanzTax { get; set; }
        [DataMapping("YuanzRate")]
        public decimal YuanzRate { get; set; }
        [DataMapping("YuanzUser")]
        public string YuanzUser { get; set; }

        //合计
        [DataMapping("SumTax")]
        public decimal SumTax { get; set; }
        [DataMapping("SumRate")]
        public decimal SumRate { get; set; }
    }

    /// <summary>
    /// 第三部分实体
    /// </summary>
    [Serializable]
    public class PartThree
    {
        public int PartOneSysNo { get; set; }

        [DataMapping("DesignPay")]
        public string DesignPay { get; set; }

        [DataMapping("ABCDEPay")]
        public string ABCDPay { get; set; }

        [DataMapping("SpecialtyPayPercent")]
        public string SpecialtyPayPercent { get; set; }

        [DataMapping("ProfessionalPercent")]
        public string ProfessionalPercent { get; set; }

        [DataMapping("DesignUserPay")]
        public string DesignUserPay { get; set; }

        [DataMapping("CheckUserPay")]
        public string CheckUserPay { get; set; }

        [DataMapping("SpeRegPay")]
        public string SpeRegPay { get; set; }

        [DataMapping("AuditUserPay")]
        public string AuditUserPay { get; set; }

        [DataMapping("JuageUserPay")]
        public string JuageUserPay { get; set; }

        public Dictionary<string, List<PartThreeTableViewEntity>> ResolveJSON(string jsonData)
        {
            Dictionary<string, List<PartThreeTableViewEntity>> dataObjDictionary=new Dictionary<string, List<PartThreeTableViewEntity>>();
            try
            {
               // dataObjDictionary = JsonConvert.DeserializeObject<Dictionary<string, List<PartThreeTableViewEntity>>>(jsonData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dataObjDictionary;
        }
    }

    public class PartThreeTableViewEntity
    {
        public decimal DesignPercent { get; set; }

        public decimal Money { get; set; }

        public string Member { get; set; }

        public string Signature { get; set; }
    }

    /// <summary>
    /// 第四部分实体
    /// </summary>
    [Serializable]
    public class PartFour
    {
        public int PartOneSysNo { get; set; }

        [DataMapping("Statistics")]
        public string Statistics { get; set; }

        [DataMapping("CourtyardKeep")]
        public decimal CourtyardKeep { get; set; }

        [DataMapping("ProjectKeep")]
        public decimal ProjectKeep { get; set; }

        [DataMapping("DesignUserKeep")]
        public decimal DesignUserKeep { get; set; }

        [DataMapping("Total")]
        public decimal Total { get; set; }

        [DataMapping("Remark")]
        public string Remark { get; set; }

        [DataMapping("Dean")]
        public string Dean { get; set; }

        [DataMapping("Produce")]
        public string Produce { get; set; }

        [DataMapping("ProduceManager")]
        public string ProduceManager { get; set; }

        [DataMapping("PM")]
        public string PM { get; set; }

        [DataMapping("TabulationUser")]
        public string TabulationUser { get; set; }
    }

    public class AllotUserViewEntity
    {
        [DataMapping("Speciality")]
        public string SpecialtyName { get; set; }

        [DataMapping("UserSysNo")]
        public int UserSysNo { get; set; }

        [DataMapping("UserName")]
        public string UserName { get; set; }

        [DataMapping("UserRoleSysNo")]
        public string UserRoleSysNoString { get; set; }

        public string[] userRoleSysNoArray
        {
            get
            {
                if (!string.IsNullOrEmpty(UserRoleSysNoString))
                {
                    return UserRoleSysNoString.Split(',');
                }
                return null;
            }
        }
    }

    public class CostCompanyConfig
    {
        [DataMapping("CmpMngPrt")]
        public decimal CmpMngPrt { get; set; }

        [DataMapping("ProCostPrt")]
        public decimal ProCostPrt { get; set; }

        [DataMapping("ProChgPrt")]
        public decimal ProChgPrt { get; set; }

        [DataMapping("RegUserPrt")]
        public decimal RegUserPrt { get; set; }

        [DataMapping("DesignPrt")]
        public decimal DesignPrt { get; set; }

        [DataMapping("AuditPrt")]
        public decimal AuditPrt { get; set; }

        [DataMapping("TotalUser")]
        public decimal TotalUser { get; set; }

        [DataMapping("CollegeUser")]
        public decimal CollegeUser { get; set; }

        [DataMapping("Used")]
        public int Used { get; set; }
    }

    public class CostUserConfig
    {
        [DataMapping("DisgnUserPrt")]
        public decimal DisgnUserPrt { get; set; }

        [DataMapping("AuditDPrt")]
        public decimal AuditDPrt { get; set; }

        [DataMapping("SpeRegPrt")]
        public decimal SpeRegPrt { get; set; }

        [DataMapping("AuditPrt")]
        public decimal AuditPrt { get; set; }

        [DataMapping("ExamPrt")]
        public decimal ExamPrt { get; set; }

        [DataMapping("Used")]
        public int Used { get; set; }
    }

}
