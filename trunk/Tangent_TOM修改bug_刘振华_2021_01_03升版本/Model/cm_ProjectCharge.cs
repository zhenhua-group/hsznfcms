﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ProjectCharge:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ProjectCharge
	{
		public cm_ProjectCharge()
		{}
		#region Model
		private int _id;
		private int? _projid;
		private int? _cprid;
		private decimal? _acount=0.00M;
		private string _fromuser;
		private string _inacountuser;
		private DateTime? _inacounttime;
		private string _status="A";
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? projID
		{
			set{ _projid=value;}
			get{return _projid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? cprID
		{
			set{ _cprid=value;}
			get{return _cprid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Acount
		{
			set{ _acount=value;}
			get{return _acount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FromUser
		{
			set{ _fromuser=value;}
			get{return _fromuser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string InAcountUser
		{
			set{ _inacountuser=value;}
			get{return _inacountuser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InAcountTime
		{
			set{ _inacounttime=value;}
			get{return _inacounttime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Status
		{
			set{ _status=value;}
			get{return _status;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string InAcountCode
        {
            set;
            get;
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessMark
        {
            set;
            get;
        }

        public string CaiwuMark { get; set; }

        public string SuoZhangMark { get; set; }

		#endregion Model

	}
}

