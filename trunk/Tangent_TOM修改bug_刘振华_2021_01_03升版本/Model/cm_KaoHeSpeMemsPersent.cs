﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeSpeMemsPersent:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeSpeMemsPersent
	{
		public cm_KaoHeSpeMemsPersent()
		{}
		#region Model
		private int _id;
		private int _proid;
		private int _prokhid;
		private int _kaohenameid;
		private int _memid;
		private string _memname;
		private int _speid;
		private string _spename;
		private decimal _zt;
		private decimal _xd;
		private decimal _sh;
		private decimal _fa;
		private decimal _zc;
		private int _insertuser1;
		private DateTime? _insertdate1= DateTime.Now;
		private int _insertuser2;
		private DateTime? _insertdate2= DateTime.Now;
		private int _insertuser3;
		private DateTime? _insertdate3= DateTime.Now;
		private int? _stat=0;
		private int? _stat2=0;
		private int? _stat3=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHID
		{
			set{ _prokhid=value;}
			get{return _prokhid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int KaoHeNameID
		{
			set{ _kaohenameid=value;}
			get{return _kaohenameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeID
		{
			set{ _speid=value;}
			get{return _speid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zt
		{
			set{ _zt=value;}
			get{return _zt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xd
		{
			set{ _xd=value;}
			get{return _xd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Sh
		{
			set{ _sh=value;}
			get{return _sh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Fa
		{
			set{ _fa=value;}
			get{return _fa;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zc
		{
			set{ _zc=value;}
			get{return _zc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InsertUser1
		{
			set{ _insertuser1=value;}
			get{return _insertuser1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate1
		{
			set{ _insertdate1=value;}
			get{return _insertdate1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InsertUser2
		{
			set{ _insertuser2=value;}
			get{return _insertuser2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate2
		{
			set{ _insertdate2=value;}
			get{return _insertdate2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InsertUser3
		{
			set{ _insertuser3=value;}
			get{return _insertuser3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate3
		{
			set{ _insertdate3=value;}
			get{return _insertdate3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat2
		{
			set{ _stat2=value;}
			get{return _stat2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat3
		{
			set{ _stat3=value;}
			get{return _stat3;}
		}
		#endregion Model

	}
}

