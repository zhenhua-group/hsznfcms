﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class Outsummary
    {
        /// <summary>
        /// 项目名称
        /// </summary>

        public string CprName { get; set; }
        /// <summary>
        /// 收费
        /// </summary>
        public decimal Charge { get; set; }
        /// <summary>
        /// 未分产值
        /// </summary>
        public decimal WeiFenValue { get; set; }
        /// <summary>
        /// 转其他所产值
        /// </summary>
        public decimal ToOtherValue { get; set; }
        /// <summary>
        /// 本部门实际产值
        /// </summary>
        public decimal ActualValue
        {
            get;
            set;
        }
        public string Mark { get; set; }
        public string Chargelist { get; set; }
        /// <summary>
        /// 转暖通
        /// </summary>
        public decimal HavcCount { get; set; }
        /// <summary>
        ///转土建所
        /// </summary>
        public decimal TranBuild { get; set; }
    }


}
