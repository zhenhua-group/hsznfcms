﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ApplyInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ApplyInfo
	{
		public cm_ApplyInfo()
		{}
		#region Model
		private int _id;
		private string _applytype;
		private string _reason;
		private string _address;
		private DateTime _starttime= DateTime.Now;
		private DateTime _endtime= DateTime.Now;
		private decimal? _totaltime=0M;
		private string _iscar;
		private decimal? _kilometre=0M;
		private string _remark;
		private int _adduser;
		private DateTime _addtime= DateTime.Now;
        private string _isdone;
        private int? _RelationID;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string applytype
		{
			set{ _applytype=value;}
			get{return _applytype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string reason
		{
			set{ _reason=value;}
			get{return _reason;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime starttime
		{
			set{ _starttime=value;}
			get{return _starttime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime endtime
		{
			set{ _endtime=value;}
			get{return _endtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? totaltime
		{
			set{ _totaltime=value;}
			get{return _totaltime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string iscar
		{
			set{ _iscar=value;}
			get{return _iscar;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? kilometre
		{
			set{ _kilometre=value;}
			get{return _kilometre;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int adduser
		{
			set{ _adduser=value;}
			get{return _adduser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime addtime
		{
			set{ _addtime=value;}
			get{return _addtime;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string IsDone
        {
            set { _isdone = value; }
            get { return _isdone; }
        }
        public string applyTypeName
        {
            get
            {
                string backstr="";
               switch(applytype)
               {
                   case "leave":
                       backstr="请假申请";
                       break;
                   case "travel":
                       backstr= "出差申请";   
                       break;
                   case "gomeet":
                       backstr= "外出开会申请";
                       break;
                    case "forget":
                       backstr=  "忘记打卡申请";
                       break;
                    case "company":
                       backstr = "公司活动申请";
                       break;
                    case "depart":
                       backstr = "部门活动申请";
                       break;
                    case "addwork":
                       backstr = "加班离岗申请";
                       break;
               }
               return backstr;

            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RelationID
        {
            set { _RelationID = value; }
            get { return _RelationID; }
        }
		#endregion Model

	}
}

