﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ApplyUnitOvertime:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ApplyUnitOvertime
	{
		public cm_ApplyUnitOvertime()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private int _overyear;
		private int _overmonth;
		private decimal _quotiety=1M;
		private string _quotamoney;
		private DateTime? _adddate= DateTime.Now;
		private int _adduserid;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int OverYear
		{
			set{ _overyear=value;}
			get{return _overyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int OverMonth
		{
			set{ _overmonth=value;}
			get{return _overmonth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Quotiety
		{
			set{ _quotiety=value;}
			get{return _quotiety;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string QuotaMoney
		{
			set{ _quotamoney=value;}
			get{return _quotamoney;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? addDate
		{
			set{ _adddate=value;}
			get{return _adddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int addUserId
		{
			set{ _adduserid=value;}
			get{return _adduserid;}
		}
		#endregion Model

	}
}

