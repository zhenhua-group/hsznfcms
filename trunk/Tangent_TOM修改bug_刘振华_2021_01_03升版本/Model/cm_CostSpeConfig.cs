﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_CostSpeConfig:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CostSpeConfig
    {
        public cm_CostSpeConfig()
        { }
        #region Model
        private int _id;
        private int? _speid;
        private decimal? _listprt;
        private int? _structype;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SpeID
        {
            set { _speid = value; }
            get { return _speid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ListPrt
        {
            set { _listprt = value; }
            get { return _listprt; }
        }
        public int? StrucType
        {
            set { _structype = value; }
            get { return _structype; }
        }
        #endregion Model

    }
}

