﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
	public class cm_ProjectJobNumber
	{
		[DataMapping("ID")]
		public int SysNo { get; set; }

		[DataMapping("Pro_id")]
		public int ProjectSysNo { get; set; }

		[DataMapping("SubmitDate")]
		public DateTime SubmitDate { get; set; }

		[DataMapping("SubmintPerson")]
		public string SubmitUser { get; set; }

		[DataMapping("ProNumber")]
		public string ProjectNum { get; set; }
	}
}
