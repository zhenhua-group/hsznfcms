﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_RoleMag:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_RoleMag
	{
		public cm_RoleMag()
		{}
		#region Model
		private int _id;
		private string _rolename;
		private int _weights;
		private int? _ismag=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RoleName
		{
			set{ _rolename=value;}
			get{return _rolename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Weights
		{
			set{ _weights=value;}
			get{return _weights;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsMag
		{
			set{ _ismag=value;}
			get{return _ismag;}
		}
		#endregion Model

	}
}

