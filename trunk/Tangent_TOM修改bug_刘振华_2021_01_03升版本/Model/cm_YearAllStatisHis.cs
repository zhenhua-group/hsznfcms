﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_YearAllStatisHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_YearAllStatisHis
	{
		public cm_YearAllStatisHis()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private string _mem_name;
		private int _mem_unit_id;
		private string _datadate;
		private int? _yearday;
		private decimal? _yearhour;
		private decimal? _lastyearhour;
		private decimal? _time_1231;
		private decimal? _time_115;
		private decimal? _time_215;
		private decimal? _time_228;
		private decimal? _time_315;
		private decimal? _time_415;
		private decimal? _time_515;
		private decimal? _time_615;
		private decimal? _time_715;
		private decimal? _time_815;
		private decimal? _time_915;
		private decimal? _time_1015;
		private decimal? _time_1115;
		private decimal? _time_1215;
		private decimal? _nextyearhour;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_name
		{
			set{ _mem_name=value;}
			get{return _mem_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_unit_ID
		{
			set{ _mem_unit_id=value;}
			get{return _mem_unit_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dataDate
		{
			set{ _datadate=value;}
			get{return _datadate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? yearDay
		{
			set{ _yearday=value;}
			get{return _yearday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? yearHour
		{
			set{ _yearhour=value;}
			get{return _yearhour;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? lastyearHour
		{
			set{ _lastyearhour=value;}
			get{return _lastyearhour;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_1231
		{
			set{ _time_1231=value;}
			get{return _time_1231;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_115
		{
			set{ _time_115=value;}
			get{return _time_115;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_215
		{
			set{ _time_215=value;}
			get{return _time_215;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_228
		{
			set{ _time_228=value;}
			get{return _time_228;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_315
		{
			set{ _time_315=value;}
			get{return _time_315;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_415
		{
			set{ _time_415=value;}
			get{return _time_415;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_515
		{
			set{ _time_515=value;}
			get{return _time_515;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_615
		{
			set{ _time_615=value;}
			get{return _time_615;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_715
		{
			set{ _time_715=value;}
			get{return _time_715;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_815
		{
			set{ _time_815=value;}
			get{return _time_815;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_915
		{
			set{ _time_915=value;}
			get{return _time_915;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_1015
		{
			set{ _time_1015=value;}
			get{return _time_1015;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_1115
		{
			set{ _time_1115=value;}
			get{return _time_1115;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_1215
		{
			set{ _time_1215=value;}
			get{return _time_1215;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? nextyearHour
		{
			set{ _nextyearhour=value;}
			get{return _nextyearhour;}
		}
		#endregion Model

	}
}

