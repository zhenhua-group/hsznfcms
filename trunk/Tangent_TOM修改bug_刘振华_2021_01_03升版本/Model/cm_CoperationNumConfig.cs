﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_CoperationNumConfig:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CoperationNumConfig
    {
        public cm_CoperationNumConfig()
        { }
        #region Model
        private int? _id;
        private string _cprpfix;
        private string _cprnumstart;
        private string _cprnumend;
        /// <summary>
        /// 
        /// </summary>
        public int? ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CprPfix
        {
            set { _cprpfix = value; }
            get { return _cprpfix; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CprNumStart
        {
            set { _cprnumstart = value; }
            get { return _cprnumstart; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CprNumEnd
        {
            set { _cprnumend = value; }
            get { return _cprnumend; }
        }

        public string CprType { get; set; }
        #endregion Model

    }
}

