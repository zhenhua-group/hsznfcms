﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeMemsAllotReport:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeMemsAllotReport
	{
		public cm_KaoHeMemsAllotReport()
		{}
		#region Model
		private int _id;
		private int _renwuid;
		private int _unitid;
		private decimal? _weights=0M;
		private int? _stat=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? weights
		{
			set{ _weights=value;}
			get{return _weights;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

