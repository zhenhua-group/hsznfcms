﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_ReconnCoperation:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_ReconnCoperation
    {
        public cm_ReconnCoperation()
        { }
        #region Model
        private int _cpr_id;
        private int? _cst_id;
        private string _cpr_no;
        private string _cpr_type;
        private string _cpr_name;
        private string _buildunit;
        private string _floor;
        private string _chgpeople;
        private string _chgphone;
        private string _chgjia;
        private string _chgjiaphone;
        private string _cpr_unit;
        private string _buildposition;
        private string _industry;
        private string _buildsrc;
        private string _tablemaker;
        private DateTime? _cpr_donedate;
        private string _cpr_mark;
        private string _surveyclass;
        private string _earthunit;
        private decimal? _buildarea;
        private int? _buildnumber;
        private int? _holenumber;
        private decimal? _holeheight;
        private int? _wellsnumber;
        private decimal? _wellsheight;
        private decimal? _totallength;
        private string _buildtype;
        private string _cpr_type2;
        private DateTime? _cpr_signdate;
        private DateTime? _cpr_signdate2;
        private DateTime? _cpr_startdate;
        private string _cpr_process;
        private string _multibuild;
        private string _buildstructtype;
        private int? _pointnumber;
        private int? _wavenumber;
        private decimal? _waveheight;
        private int? _controlnumber;
        private decimal? _controlheight;
        private int? _genernumber;
        private decimal? _generheight;
        private decimal? _projectarea;
        private string _features;
        private int? _projectdate;
        private int? _insertuserid = 0;
        private DateTime? _insertdate = DateTime.Now;
        private int? _isparamteredit = 0;
        private int? _pmuserid = 0;
        private decimal? _cpr_acount;
        private decimal? _cpr_shijiacounnt;
        private DateTime? _regtime;
        private string _updateby;
        private DateTime? _lastupdate;
        private int? _cpr_fid;
        /// <summary>
        /// 
        /// </summary>
        public int cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type
        {
            set { _cpr_type = value; }
            get { return _cpr_type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Name
        {
            set { _cpr_name = value; }
            get { return _cpr_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildUnit
        {
            set { _buildunit = value; }
            get { return _buildunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Floor
        {
            set { _floor = value; }
            get { return _floor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPeople
        {
            set { _chgpeople = value; }
            get { return _chgpeople; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPhone
        {
            set { _chgphone = value; }
            get { return _chgphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJia
        {
            set { _chgjia = value; }
            get { return _chgjia; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJiaPhone
        {
            set { _chgjiaphone = value; }
            get { return _chgjiaphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Unit
        {
            set { _cpr_unit = value; }
            get { return _cpr_unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildPosition
        {
            set { _buildposition = value; }
            get { return _buildposition; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Industry
        {
            set { _industry = value; }
            get { return _industry; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildSrc
        {
            set { _buildsrc = value; }
            get { return _buildsrc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TableMaker
        {
            set { _tablemaker = value; }
            get { return _tablemaker; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_DoneDate
        {
            set { _cpr_donedate = value; }
            get { return _cpr_donedate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Mark
        {
            set { _cpr_mark = value; }
            get { return _cpr_mark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SurveyClass
        {
            set { _surveyclass = value; }
            get { return _surveyclass; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string EarthUnit
        {
            set { _earthunit = value; }
            get { return _earthunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BuildArea
        {
            set { _buildarea = value; }
            get { return _buildarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? BuildNumber
        {
            set { _buildnumber = value; }
            get { return _buildnumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HoleNumber
        {
            set { _holenumber = value; }
            get { return _holenumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HoleHeight
        {
            set { _holeheight = value; }
            get { return _holeheight; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? WellsNumber
        {
            set { _wellsnumber = value; }
            get { return _wellsnumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WellsHeight
        {
            set { _wellsheight = value; }
            get { return _wellsheight; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TotalLength
        {
            set { _totallength = value; }
            get { return _totallength; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildType
        {
            set { _buildtype = value; }
            get { return _buildtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type2
        {
            set { _cpr_type2 = value; }
            get { return _cpr_type2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_StartDate
        {
            set { _cpr_startdate = value; }
            get { return _cpr_startdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_SignDate
        {
            set { _cpr_signdate = value; }
            get { return _cpr_signdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_SignDate2
        {
            set { _cpr_signdate2 = value; }
            get { return _cpr_signdate2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Process
        {
            set { _cpr_process = value; }
            get { return _cpr_process; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MultiBuild
        {
            set { _multibuild = value; }
            get { return _multibuild; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildStructType
        {
            set { _buildstructtype = value; }
            get { return _buildstructtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? PointNumber
        {
            set { _pointnumber = value; }
            get { return _pointnumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? WaveNumber
        {
            set { _wavenumber = value; }
            get { return _wavenumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WaveHeight
        {
            set { _waveheight = value; }
            get { return _waveheight; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ControlNumber
        {
            set { _controlnumber = value; }
            get { return _controlnumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ControlHeight
        {
            set { _controlheight = value; }
            get { return _controlheight; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? GenerNumber
        {
            set { _genernumber = value; }
            get { return _genernumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? GenerHeight
        {
            set { _generheight = value; }
            get { return _generheight; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProjectArea
        {
            set { _projectarea = value; }
            get { return _projectarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Features
        {
            set { _features = value; }
            get { return _features; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ProjectDate
        {
            set { _projectdate = value; }
            get { return _projectdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? InsertUserID
        {
            set { _insertuserid = value; }
            get { return _insertuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? IsParamterEdit
        {
            set { _isparamteredit = value; }
            get { return _isparamteredit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? PMUserID
        {
            set { _pmuserid = value; }
            get { return _pmuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Acount
        {
            set { _cpr_acount = value; }
            get { return _cpr_acount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiAcounnt
        {
            set { _cpr_shijiacounnt = value; }
            get { return _cpr_shijiacounnt; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RegTime
        {
            set { _regtime = value; }
            get { return _regtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cpr_FID
        {
            set { _cpr_fid = value; }
            get { return _cpr_fid; }
        }
        #endregion Model

    }
}


