﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_CostUserConfig:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_CostUserConfig
	{
		public cm_CostUserConfig()
		{}
		#region Model
		private int _id;
		private decimal? _disgnuserprt;
		private decimal? _auditdprt;
		private decimal? _speregprt;
		private decimal? _auditprt;
		private decimal? _examprt;
		private int? _used;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? DisgnUserPrt
		{
			set{ _disgnuserprt=value;}
			get{return _disgnuserprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AuditDPrt
		{
			set{ _auditdprt=value;}
			get{return _auditdprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SpeRegPrt
		{
			set{ _speregprt=value;}
			get{return _speregprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AuditPrt
		{
			set{ _auditprt=value;}
			get{return _auditprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ExamPrt
		{
			set{ _examprt=value;}
			get{return _examprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Used
		{
			set{ _used=value;}
			get{return _used;}
		}
		#endregion Model

	}
}

