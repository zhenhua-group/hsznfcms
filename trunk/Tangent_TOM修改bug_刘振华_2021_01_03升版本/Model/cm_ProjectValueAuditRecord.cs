﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cm_ProjectValueAuditRecord
    {

        /// <summary>
        /// 系统号子增
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary> 
        /// 项目编号编号
        /// </summary>
        [DataMapping("ProSysNo")]
        public int ProSysNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMapping("AllotID")]
        public int? AllotID { get; set; }

        /// <summary>
        /// 记录人
        /// </summary>
        [DataMapping("InUser")]
        public int InUser { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        [DataMapping("InDate")]
        public DateTime? InDate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("OneSuggestion")]
        public string OneSuggestion { get; set; }
        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("TwoSuggstion")]
        public string TwoSuggstion { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("TwoAuditUser")]
        public int TwoAuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("TwoAuditDate")]
        public DateTime? TwoAuditDate { get; set; }

        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("ThreeSuggsion")]
        public string ThreeSuggsion { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("ThreeAuditUser")]
        public int ThreeAuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("ThreeAuditDate")]
        public DateTime? ThreeAuditDate { get; set; }

        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("FourSuggion")]
        public string FourSuggion { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("FourAuditUser")]
        public int FourAuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("FourAuditDate")]
        public DateTime? FourAuditDate { get; set; }

        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("FiveSuggion")]
        public string FiveSuggion { get; set; }

        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("SixSuggsion")]
        public string SixSuggsion { get; set; }

        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("SevenSuggsion")]
        public string SevenSuggsion { get; set; }

        /// <summary>
        /// 评价
        /// </summary>
        [DataMapping("EightSuggstion")]
        public string EightSuggstion { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        /// <summary>
        /// 审核人姓名字符串
        /// </summary>
        public string AuditUserString { get; set; }

        /// <summary>
        /// 专业负责需要审核记录
        /// </summary>
        public int SprcialtyAuditCount { get; set; }

        /// <summary>
        /// 所长审核记录
        /// </summary>
        public int HeadAuditCount { get; set; }

        [DataMapping("TwoIsPass")]
        public string TwoIsPass { get; set; }

        [DataMapping("ThreeIsPass")]
        public string ThreeIsPass { get; set; }

        [DataMapping("FourIsPass")]
        public string FourIsPass { get; set; }

        //二次产值分配
        [DataMapping("SecondValue")]
        public string SecondValue { get; set; }
    }
    public enum RoleEnum_new
    {
        /// <summary>
        /// 承接部门负责人
        /// </summary>
        Undertake = 2,
        /// <summary>
        /// 经营处负责人
        /// </summary>
        Operate = 3,
        /// <summary>
        /// 技术部门负责人
        /// </summary>
        Technology = 4,
        /// <summary>
        /// 总经理
        /// </summary>
        GeneralManager = 5,
        /// <summary>
        /// 普通员工
        /// </summary>
        Employee = 6
    }

    public class AuditSysMsgnew
    {
        #region 消息列表要用的
        /// <summary>
        /// 消息记录表的SysNo
        /// </summary>
        public int SysMsgSysNo { get; set; }

        /// <summary>
        /// 合同审核记录SysNo
        /// </summary>
        public int CoperationAuditSysNo { get; set; }

        /// <summary>
        /// 合同名称
        /// </summary>
        public string CoperationName { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public int CoperationSysNo { get; set; }

        /// <summary>
        /// 合同审批状态
        /// </summary>
        public string CoperationStatus { get; set; }

        /// <summary>
        /// 在那个部门
        /// </summary>
        public string AuditDepartment
        {
            get
            {
                string str = "";
                switch (CoperationStatus)
                {
                    case "A":
                    case "C":
                        str = "承接部门";
                        break;
                    case "B":
                    case "E":
                        str = "经营处";
                        break;
                    case "D":
                    case "G":
                        str = "技术部门";
                        break;
                    case "H":
                    case "I":
                    case "F":
                        str = "总经理";
                        break;
                }
                return str;
            }
        }

        public string StatusString
        {
            get
            {
                string str = "";
                switch (CoperationStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        str = "审核中";
                        break;
                    case "H":
                        str = "审核通过";
                        break;
                    case "I":
                        str = "审核拒结";
                        break;
                }
                return str;
            }
        }

        /// <summary>
        /// 提交审核时间
        /// </summary>
        public DateTime CoperationInDate { get; set; }

        public string InDateString
        {
            get
            {
                return CoperationInDate.ToString("yyyy-MM-dd hh:mm:ss");
            }
        }

        /// <summary>
        /// 是否有电子合同即附件
        /// </summary>
        public string HaveElectronicCoperation { get; set; }
        #endregion
    }

    /// <summary>
    /// 产值分配明细
    /// </summary>
    public class ProjectValueAuditViewEntity
    {
        [DataMapping("ID")]
        public int ID { get; set; }

        [DataMapping("pro_ID")]
        public int pro_ID { get; set; }

        [DataMapping("AllotTimes")]
        public string AllotTimes { get; set; }

        [DataMapping("AllotCount")]
        public decimal AllotCount { get; set; }

        [DataMapping("AllotDate")]
        public DateTime AllotDate { get; set; }

        [DataMapping("persent")]
        public decimal persent { get; set; }

        [DataMapping("PaidValuePercent")]
        public decimal PaidValuePercent { get; set; }

        [DataMapping("AuditSysNo")]
        public int AuditSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("PaidValueCount")]
        public decimal PaidValueCount { get; set; }

        [DataMapping("DesignManagerPercent")]
        public decimal DesignManagerPercent { get; set; }

        [DataMapping("DesignManagerCount")]
        public decimal DesignManagerCount { get; set; }

        [DataMapping("AllotValuePercent")]
        public string AllotValuePercent { get; set; }

        [DataMapping("EconomyValuePercent")]
        public decimal EconomyValuePercent { get; set; }

        [DataMapping("EconomyValueCount")]
        public decimal EconomyValueCount { get; set; }


        /// <summary>
        /// 本部门产值比例
        /// </summary>
        [DataMapping("UnitValuePercent")]
        public decimal UnitValuePercent { get; set; }

        [DataMapping("UnitValueCount")]
        public decimal UnitValueCount { get; set; }

        [DataMapping("PayShiCount")]
        public decimal PayShiCount { get; set; }

        /// <summary>
        /// 方案比例
        /// </summary>
        [DataMapping("ProgramCount")]
        public decimal ProgramCount { get; set; }

        /// <summary>
        /// 所留
        /// </summary>
        [DataMapping("TheDeptValuePercent")]
        public decimal TheDeptValuePercent { get; set; }

        /// <summary>
        /// 是否转经济所
        /// </summary>
        [DataMapping("IsTrunEconomy")]
        public string IsTrunEconomy { get; set; }

        /// <summary>
        /// 是否转暖通
        /// </summary>
        [DataMapping("ISTrunHavc")]
        public string ISTrunHavc { get; set; }

        /// <summary>
        /// 实际分配比例
        /// </summary>
        [DataMapping("ActualAllountTime")]
        public string ActualAllountTime { get; set; }

        /// <summary>
        /// 借入金额
        /// </summary>
        [DataMapping("BorrowValueCount")]
        public decimal BorrowValueCount { get; set; }

        /// <summary>
        /// 借出 金额
        /// </summary>
        [DataMapping("LoanValueCount")]
        public decimal LoanValueCount { get; set; }

        /// <summary>
        /// 应分产值
        /// </summary>
        [DataMapping("ActualAllotCount")]
        public decimal ActualAllotCount { get; set; }

        /// <summary>
        /// 全部转出
        /// </summary>
        [DataMapping("IsTAllPass")]
        public int IsTAllPass { get; set; }

        public string Percent
        {
            get
            {
                int lengthOne = 8, percent = 0, lengthTwo = 6;

                switch (Status)
                {
                    case "A":
                        percent = 0;
                        break;
                    case "B":
                    case "C":
                        percent = (100 / lengthOne) * 1;
                        break;
                    case "D":
                    case "E":
                        percent = (100 / lengthOne) * 2;
                        break;
                    case "F":
                        if (IsTAllPass == 1)
                        {
                            percent = 100;
                        }
                        else
                        {
                            if (IsTrunEconomy.Equals("0"))
                            {
                                percent = (100 / lengthTwo) * 2;
                            }
                            else
                            {
                                percent = (100 / lengthOne) * 3;
                            }
                        }
                        break;
                    case "G":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 2;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 3;
                        }
                        break;
                    case "H":
                    case "I":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 3;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 4;
                        }
                        break;
                    case "J":
                    case "K":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 4;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 5;
                        }
                        break;
                    case "L":
                    case "M":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 5;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 6;
                        }
                        break;
                    case "N":
                    case "O":
                        percent = (100 / lengthOne) * 7;
                        break;
                    case "P":
                    case "Q":
                        percent = 100;
                        break;
                }

                return percent.ToString("f2");
            }
        }

        public string SubmitDateString
        {
            get
            {
                return AllotDate.ToString("yyyy-MM-dd hh:mm");
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (Status == "C" || Status == "E" || Status == "G" || Status == "I" || Status == "K" || Status == "M" || Status == "O" || Status == "Q")
                {
                    //linkAction = "分配被驳回";
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}\'>分配被驳回</a>", AuditSysNo);
                }
                else
                {
                    //linkAction = "分配中";
                    if (Status == "F" && IsTAllPass == 1)
                    {
                        linkAction = string.Format(" <a style=\"color:green;\" href=\'/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}&likeType=detail\'>分配金额全部借出通过</a>", AuditSysNo);
                    }
                    else
                    {
                        linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}&likeType=detail\'>分配中</a>", AuditSysNo);
                    }
                }
                if (Status == "P")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}\'>分配结果</a>", AuditSysNo);
                }

                return linkAction;
            }
        }
    }

    public class ProjectValueAuditViewEntityByPro
    {
        [DataMapping("ID")]
        public int ID { get; set; }

        [DataMapping("pro_ID")]
        public int pro_ID { get; set; }

        [DataMapping("AllotTimes")]
        public string AllotTimes { get; set; }

        [DataMapping("AllotCount")]
        public decimal AllotCount { get; set; }

        [DataMapping("AllotDate")]
        public DateTime AllotDate { get; set; }

        [DataMapping("persent")]
        public decimal persent { get; set; }

        [DataMapping("PaidValuePercent")]
        public decimal PaidValuePercent { get; set; }

        [DataMapping("AuditSysNo")]
        public int AuditSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("PaidValueCount")]
        public decimal PaidValueCount { get; set; }

        [DataMapping("DesignManagerPercent")]
        public decimal DesignManagerPercent { get; set; }

        [DataMapping("DesignManagerCount")]
        public decimal DesignManagerCount { get; set; }

        [DataMapping("AllotValuePercent")]
        public string AllotValuePercent { get; set; }

        [DataMapping("EconomyValuePercent")]
        public decimal EconomyValuePercent { get; set; }

        [DataMapping("EconomyValueCount")]
        public decimal EconomyValueCount { get; set; }

        /// <summary>
        /// 本部门产值比例
        /// </summary>
        [DataMapping("UnitValuePercent")]
        public decimal UnitValuePercent { get; set; }

        [DataMapping("UnitValueCount")]
        public decimal UnitValueCount { get; set; }

        [DataMapping("PayShiCount")]
        public decimal PayShiCount { get; set; }

        /// <summary>
        /// 方案比例
        /// </summary>
        [DataMapping("ProgramCount")]
        public decimal ProgramCount { get; set; }

        /// <summary>
        /// 所留
        /// </summary>
        [DataMapping("TheDeptValuePercent")]
        public decimal TheDeptValuePercent { get; set; }

        /// <summary>
        /// 是否转经济所
        /// </summary>
        [DataMapping("IsTrunEconomy")]
        public string IsTrunEconomy { get; set; }

        /// <summary>
        /// 是否转暖通
        /// </summary>
        [DataMapping("ISTrunHavc")]
        public string ISTrunHavc { get; set; }

        /// <summary>
        /// 实际分配比例
        /// </summary>
        [DataMapping("ActualAllountTime")]
        public string ActualAllountTime { get; set; }
        public string Percent
        {
            get
            {
                int lengthOne = 8, percent = 0, lengthTwo = 6;

                switch (Status)
                {
                    case "A":
                        percent = 0;
                        break;
                    case "B":
                    case "C":
                        percent = (100 / lengthOne) * 1;
                        break;
                    case "D":
                    case "E":
                        percent = (100 / lengthOne) * 2;
                        break;
                    case "F":
                    case "G":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 2;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 3;
                        }
                        break;
                    case "H":
                    case "I":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 3;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 4;
                        }
                        break;
                    case "J":
                    case "K":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 4;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 5;
                        }
                        break;
                    case "L":
                    case "M":
                        if (IsTrunEconomy.Equals("0"))
                        {
                            percent = (100 / lengthTwo) * 5;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 6;
                        }
                        break;
                    case "N":
                    case "O":
                        percent = (100 / lengthOne) * 7;
                        break;
                    case "P":
                    case "Q":
                        percent = 100;
                        break;
                }

                return percent.ToString("f2");
            }
        }

        public string SubmitDateString
        {
            get
            {
                return AllotDate.ToString("yyyy-MM-dd hh:mm");
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (Status == "C" || Status == "E" || Status == "G" || Status == "I" || Status == "K" || Status == "M" || Status == "O" || Status == "Q")
                {
                    linkAction = "分配被驳回";
                }
                else
                {
                    linkAction = "分配中";
                }
                if (Status == "P")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}&prolink=prolink\'>分配结果</a>", AuditSysNo);
                }

                return linkAction;
            }
        }
    }
    /// <summary>
    /// 产值分配明细-暖通所项目
    /// </summary>
    public class HavcProjectValueAuditViewEntity
    {
        [DataMapping("ID")]
        public int ID { get; set; }

        [DataMapping("pro_ID")]
        public int pro_ID { get; set; }

        [DataMapping("AllotTimes")]
        public string AllotTimes { get; set; }

        [DataMapping("AllotCount")]
        public decimal AllotCount { get; set; }

        [DataMapping("AllotDate")]
        public DateTime AllotDate { get; set; }

        [DataMapping("persent")]
        public decimal persent { get; set; }

        [DataMapping("PaidValuePercent")]
        public decimal PaidValuePercent { get; set; }

        [DataMapping("AuditSysNo")]
        public int AuditSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("PaidValueCount")]
        public decimal PaidValueCount { get; set; }

        [DataMapping("DesignManagerPercent")]
        public decimal DesignManagerPercent { get; set; }

        [DataMapping("DesignManagerCount")]
        public decimal DesignManagerCount { get; set; }

        [DataMapping("AllotValuePercent")]
        public string AllotValuePercent { get; set; }

        [DataMapping("EconomyValuePercent")]
        public decimal EconomyValuePercent { get; set; }

        [DataMapping("EconomyValueCount")]
        public decimal EconomyValueCount { get; set; }

        /// <summary>
        /// 本部门产值比例
        /// </summary>
        [DataMapping("UnitValuePercent")]
        public decimal UnitValuePercent { get; set; }

        [DataMapping("UnitValueCount")]
        public decimal UnitValueCount { get; set; }

        [DataMapping("PayShiCount")]
        public decimal PayShiCount { get; set; }

        /// <summary>
        /// 方案比例
        /// </summary>
        [DataMapping("ProgramCount")]
        public decimal ProgramCount { get; set; }

        /// <summary>
        /// 所留
        /// </summary>
        [DataMapping("TheDeptValuePercent")]
        public decimal TheDeptValuePercent { get; set; }

        /// <summary>
        /// 是否转经济所
        /// </summary>
        [DataMapping("IsTrunEconomy")]
        public string IsTrunEconomy { get; set; }

        /// <summary>
        /// 是否转暖通
        /// </summary>
        [DataMapping("ISTrunHavc")]
        public string ISTrunHavc { get; set; }
        /// <summary>
        /// 实际分配比例
        /// </summary>
        [DataMapping("ActualAllountTime")]
        public string ActualAllountTime { get; set; }

        // <summary>
        /// 借入金额
        /// </summary>
        [DataMapping("BorrowValueCount")]
        public decimal BorrowValueCount { get; set; }

        /// <summary>
        /// 借出 金额
        /// </summary>
        [DataMapping("LoanValueCount")]
        public decimal LoanValueCount { get; set; }

        /// <summary>
        /// 应分产值
        /// </summary>
        [DataMapping("ActualAllotCount")]
        public decimal ActualAllotCount { get; set; }

        /// <summary>
        /// 全部转出
        /// </summary>
        [DataMapping("IsTAllPass")]
        public int IsTAllPass { get; set; }
        public string Percent
        {
            get
            {
                int lengthOne = 8, percent = 0;

                switch (Status)
                {
                    case "A":
                        percent = 0;
                        break;
                    case "B":
                    case "C":
                        percent = (100 / lengthOne) * 1;
                        break;
                    case "D":
                    case "E":
                        percent = (100 / lengthOne) * 2;
                        break;
                    case "F":

                        if (IsTAllPass == 1)
                        {
                            percent = 100;
                        }
                        else
                        {
                            percent = (100 / lengthOne) * 3;
                        }
                        break;
                    case "G":
                        percent = (100 / lengthOne) * 3;
                        break;
                    case "H":
                    case "I":
                        percent = (100 / lengthOne) * 4;
                        break;
                    case "J":
                    case "K":
                        percent = (100 / lengthOne) * 5;
                        break;
                    case "L":
                    case "M":
                        percent = (100 / lengthOne) * 6;
                        break;
                    case "N":
                    case "O":
                        percent = (100 / lengthOne) * 7;
                        break;
                    case "P":
                    case "Q":
                        percent = 100;
                        break;
                }

                return percent.ToString("f2");
            }
        }

        public string SubmitDateString
        {
            get
            {
                return AllotDate.ToString("yyyy-MM-dd hh:mm");
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (Status == "C" || Status == "E" || Status == "G" || Status == "I" || Status == "K" || Status == "M" || Status == "O" || Status == "Q")
                {
                    //linkAction = "分配被驳回";
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}\'>分配被驳回</a>", AuditSysNo);
                }
                else
                {
                    //linkAction = "分配中";
                    if (Status == "F" && IsTAllPass == 1)
                    {
                        linkAction = string.Format(" <a style=\"color:green;\" href=\'/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}&likeType=detail\'>分配金额全部借出通过</a>", AuditSysNo);
                    }
                    else
                    {
                        linkAction = string.Format(" <a href=\'/ProjectValueandAllot/HavcProjectValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}&likeType=detail\'>分配中</a>", AuditSysNo);
                    }

                }
                if (Status == "P")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/HavcProjectValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}\'>分配结果</a>", AuditSysNo);
                }

                return linkAction;
            }
        }
    }
    public class HavcProjectValueAuditViewEntityByPro
    {
        [DataMapping("ID")]
        public int ID { get; set; }

        [DataMapping("pro_ID")]
        public int pro_ID { get; set; }

        [DataMapping("AllotTimes")]
        public string AllotTimes { get; set; }

        [DataMapping("AllotCount")]
        public decimal AllotCount { get; set; }

        [DataMapping("AllotDate")]
        public DateTime AllotDate { get; set; }

        [DataMapping("persent")]
        public decimal persent { get; set; }

        [DataMapping("PaidValuePercent")]
        public decimal PaidValuePercent { get; set; }

        [DataMapping("AuditSysNo")]
        public int AuditSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("PaidValueCount")]
        public decimal PaidValueCount { get; set; }

        [DataMapping("DesignManagerPercent")]
        public decimal DesignManagerPercent { get; set; }

        [DataMapping("DesignManagerCount")]
        public decimal DesignManagerCount { get; set; }

        [DataMapping("AllotValuePercent")]
        public string AllotValuePercent { get; set; }

        [DataMapping("EconomyValuePercent")]
        public decimal EconomyValuePercent { get; set; }

        [DataMapping("EconomyValueCount")]
        public decimal EconomyValueCount { get; set; }

        /// <summary>
        /// 本部门产值比例
        /// </summary>
        [DataMapping("UnitValuePercent")]
        public decimal UnitValuePercent { get; set; }

        [DataMapping("UnitValueCount")]
        public decimal UnitValueCount { get; set; }

        [DataMapping("PayShiCount")]
        public decimal PayShiCount { get; set; }

        /// <summary>
        /// 方案比例
        /// </summary>
        [DataMapping("ProgramCount")]
        public decimal ProgramCount { get; set; }

        /// <summary>
        /// 所留
        /// </summary>
        [DataMapping("TheDeptValuePercent")]
        public decimal TheDeptValuePercent { get; set; }

        /// <summary>
        /// 是否转经济所
        /// </summary>
        [DataMapping("IsTrunEconomy")]
        public string IsTrunEconomy { get; set; }

        /// <summary>
        /// 是否转暖通
        /// </summary>
        [DataMapping("ISTrunHavc")]
        public string ISTrunHavc { get; set; }
        /// <summary>
        /// 实际分配比例
        /// </summary>
        [DataMapping("ActualAllountTime")]
        public string ActualAllountTime { get; set; }
        public string Percent
        {
            get
            {
                int lengthOne = 8, percent = 0;

                switch (Status)
                {
                    case "A":
                        percent = 0;
                        break;
                    case "B":
                    case "C":
                        percent = (100 / lengthOne) * 1;
                        break;
                    case "D":
                    case "E":
                        percent = (100 / lengthOne) * 2;
                        break;
                    case "F":
                    case "G":
                        percent = (100 / lengthOne) * 3;
                        break;
                    case "H":
                    case "I":
                        percent = (100 / lengthOne) * 4;
                        break;
                    case "J":
                    case "K":
                        percent = (100 / lengthOne) * 5;
                        break;
                    case "L":
                    case "M":
                        percent = (100 / lengthOne) * 6;
                        break;
                    case "N":
                    case "O":
                        percent = (100 / lengthOne) * 7;
                        break;
                    case "P":
                    case "Q":
                        percent = 100;
                        break;
                }

                return percent.ToString("f2");
            }
        }

        public string SubmitDateString
        {
            get
            {
                return AllotDate.ToString("yyyy-MM-dd hh:mm");
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (Status == "C" || Status == "E" || Status == "G" || Status == "I" || Status == "K" || Status == "M" || Status == "O" || Status == "Q")
                {
                    linkAction = "分配被驳回";
                }
                else
                {
                    linkAction = "分配中";
                }
                if (Status == "P")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/HavcProjectValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}&prolink=prolink\'>分配结果</a>", AuditSysNo);
                }

                return linkAction;
            }
        }
    }

    /// <summary>
    /// 产值分配明细-经济所所项目
    /// </summary>
    public class JjsProjectValueAuditViewEntity
    {
        [DataMapping("ID")]
        public int ID { get; set; }

        [DataMapping("pro_ID")]
        public int pro_ID { get; set; }

        [DataMapping("AllotTimes")]
        public string AllotTimes { get; set; }

        [DataMapping("AllotCount")]
        public decimal AllotCount { get; set; }

        [DataMapping("AllotDate")]
        public DateTime AllotDate { get; set; }

        [DataMapping("persent")]
        public decimal persent { get; set; }

        [DataMapping("PaidValuePercent")]
        public decimal PaidValuePercent { get; set; }

        [DataMapping("AuditSysNo")]
        public int AuditSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("PaidValueCount")]
        public decimal PaidValueCount { get; set; }

        [DataMapping("DesignManagerPercent")]
        public decimal DesignManagerPercent { get; set; }

        [DataMapping("DesignManagerCount")]
        public decimal DesignManagerCount { get; set; }

        [DataMapping("AllotValuePercent")]
        public string AllotValuePercent { get; set; }

        [DataMapping("EconomyValuePercent")]
        public decimal EconomyValuePercent { get; set; }

        [DataMapping("EconomyValueCount")]
        public decimal EconomyValueCount { get; set; }

        /// <summary>
        /// 本部门产值比例
        /// </summary>
        [DataMapping("UnitValuePercent")]
        public decimal UnitValuePercent { get; set; }

        [DataMapping("UnitValueCount")]
        public decimal UnitValueCount { get; set; }

        [DataMapping("PayShiCount")]
        public decimal PayShiCount { get; set; }

        /// <summary>
        /// 方案比例
        /// </summary>
        [DataMapping("ProgramPercent")]
        public decimal ProgramPercent { get; set; }

        /// <summary>
        /// 所留
        /// </summary>
        [DataMapping("TheDeptValuePercent")]
        public decimal TheDeptValuePercent { get; set; }

        [DataMapping("StatusString")]
        public string StatusString { get; set; }

        /// <summary>
        /// 借入金额
        /// </summary>
        [DataMapping("BorrowValueCount")]
        public decimal BorrowValueCount { get; set; }

        /// <summary>
        /// 借出 金额
        /// </summary>
        [DataMapping("LoanValueCount")]
        public decimal LoanValueCount { get; set; }

        /// <summary>
        /// 应分产值
        /// </summary>
        [DataMapping("ActualAllotCount")]
        public decimal ActualAllotCount { get; set; }

        /// <summary>
        /// 全部转出
        /// </summary>
        [DataMapping("IsTAllPass")]
        public int IsTAllPass { get; set; }

        /// <summary>
        /// 实际分配比例
        /// </summary>
        [DataMapping("ActualAllountTime")]
        public string ActualAllountTime { get; set; }


        public string SubmitDateString
        {
            get
            {
                return AllotDate.ToString("yyyy-MM-dd hh:mm");
            }
        }
        public string Percent
        {
            get
            {
                int lengthOne = 4, percent = 0;
                switch (Status)
                {
                    case "A":
                        percent = (100 / lengthOne) * 1;
                        break;
                    case "B":
                        if (Status == "B" && IsTAllPass == 1)
                        {
                            percent = 100;
                         }
                        else
                        {
                            percent = (100 / lengthOne) * 2;
                        }
                        break;

                    case "C":
                        percent = (100 / lengthOne) * 2;
                        break;
                    case "D":
                    case "E":
                        percent = (100 / lengthOne) * 3;
                        break;
                    case "F":
                    case "G":
                        percent = (100 / lengthOne) * 4;
                        break;
                    case "H":
                    case "I":
                        percent = (100 / lengthOne) * 5;
                        break;
                    case "J":
                    case "K":
                        percent = (100 / lengthOne) * 6;
                        break;
                    case "L":
                    case "M":
                        percent = (100 / lengthOne) * 7;
                        break;
                    case "N":
                    case "O":
                        percent = (100 / lengthOne) * 8;
                        break;
                    case "P":
                    case "Q":
                        percent = 100;
                        break;
                }
                return percent.ToString("f2");
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (Status == "C" || Status == "E" || Status == "G")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?ValueAllotAuditSysNo={0}&proid={1}&type=tran\'>分配被驳回</a>", AuditSysNo, pro_ID);
                }
                else if (Status == "F")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?ValueAllotAuditSysNo={0}&proid={1}&type=tran\'>分配结果</a>", AuditSysNo, pro_ID);
                }
                else
                {
                    if (Status == "B" && IsTAllPass == 1)
                    {
                        linkAction = string.Format(" <a style=\"color:green;\" href=\'/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?ValueAllotAuditSysNo={0}&proid={1}&type=tran\'>分配金额全部借出通过</a>", AuditSysNo, pro_ID);
                    }
                    else
                    {

                        linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?ValueAllotAuditSysNo={0}&proid={1}&type=tran\'>分配中</a>", AuditSysNo, pro_ID);

                    }

                }
                return linkAction;
            }
        }
    }
    public class JjsProjectValueAuditViewEntityByPro
    {
        [DataMapping("ID")]
        public int ID { get; set; }

        [DataMapping("pro_ID")]
        public int pro_ID { get; set; }

        [DataMapping("AllotTimes")]
        public string AllotTimes { get; set; }

        [DataMapping("AllotCount")]
        public decimal AllotCount { get; set; }

        [DataMapping("AllotDate")]
        public DateTime AllotDate { get; set; }

        [DataMapping("persent")]
        public decimal persent { get; set; }

        [DataMapping("PaidValuePercent")]
        public decimal PaidValuePercent { get; set; }

        [DataMapping("AuditSysNo")]
        public int AuditSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("PaidValueCount")]
        public decimal PaidValueCount { get; set; }

        [DataMapping("DesignManagerPercent")]
        public decimal DesignManagerPercent { get; set; }

        [DataMapping("DesignManagerCount")]
        public decimal DesignManagerCount { get; set; }

        [DataMapping("AllotValuePercent")]
        public string AllotValuePercent { get; set; }

        [DataMapping("EconomyValuePercent")]
        public decimal EconomyValuePercent { get; set; }

        [DataMapping("EconomyValueCount")]
        public decimal EconomyValueCount { get; set; }

        /// <summary>
        /// 本部门产值比例
        /// </summary>
        [DataMapping("UnitValuePercent")]
        public decimal UnitValuePercent { get; set; }

        [DataMapping("UnitValueCount")]
        public decimal UnitValueCount { get; set; }

        [DataMapping("PayShiCount")]
        public decimal PayShiCount { get; set; }

        /// <summary>
        /// 方案比例
        /// </summary>
        [DataMapping("ProgramPercent")]
        public decimal ProgramPercent { get; set; }

        /// <summary>
        /// 所留
        /// </summary>
        [DataMapping("TheDeptValuePercent")]
        public decimal TheDeptValuePercent { get; set; }

        [DataMapping("StatusString")]
        public string StatusString { get; set; }

        public string SubmitDateString
        {
            get
            {
                return AllotDate.ToString("yyyy-MM-dd hh:mm");
            }
        }
        public string Percent
        {
            get
            {
                int lengthOne = 4, percent = 0;
                switch (Status)
                {
                    case "A":
                        percent = (100 / lengthOne) * 1;
                        break;
                    case "B":
                    case "C":
                        percent = (100 / lengthOne) * 2;
                        break;
                    case "D":
                    case "E":
                        percent = (100 / lengthOne) * 3;
                        break;
                    case "F":
                    case "G":
                        percent = (100 / lengthOne) * 4;
                        break;
                    case "H":
                    case "I":
                        percent = (100 / lengthOne) * 5;
                        break;
                    case "J":
                    case "K":
                        percent = (100 / lengthOne) * 6;
                        break;
                    case "L":
                    case "M":
                        percent = (100 / lengthOne) * 7;
                        break;
                    case "N":
                    case "O":
                        percent = (100 / lengthOne) * 8;
                        break;
                    case "P":
                    case "Q":
                        percent = 100;
                        break;
                }
                return percent.ToString("f2");
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (Status == "C" || Status == "E" || Status == "G")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?ValueAllotAuditSysNo={0}&proid={1}&type=tran&prolink=prolink\'>分配被驳回</a>", AuditSysNo, pro_ID);
                }
                else if (Status == "F")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?ValueAllotAuditSysNo={0}&proid={1}&type=tran&prolink=prolink\'>分配结果</a>", AuditSysNo, pro_ID);
                }
                else
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?ValueAllotAuditSysNo={0}&proid={1}&type=tran&prolink=prolink\'>分配中</a>", AuditSysNo, pro_ID);
                }

                return linkAction;
            }
        }
    }
    /// <summary>
    /// 产值分配明细
    /// </summary>
    public class ProjectSecondValueAuditViewEntity
    {
        [DataMapping("ID")]
        public int ID { get; set; }

        [DataMapping("pro_ID")]
        public int pro_ID { get; set; }

        [DataMapping("AllotTimes")]
        public string AllotTimes { get; set; }

        [DataMapping("AllotCount")]
        public decimal AllotCount { get; set; }

        [DataMapping("AllotDate")]
        public DateTime AllotDate { get; set; }

        [DataMapping("AuditSysNo")]
        public int AuditSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("AuditCount")]
        public decimal AuditCount { get; set; }

        [DataMapping("DesignCount")]
        public decimal DesignCount { get; set; }

        /// <summary>
        /// 百分比
        /// </summary>
        public string Percent
        {
            get
            {
                int lengthOne = 6, percent = 0, lengthTwo = 6;

                switch (Status)
                {
                    case "A":
                        percent = 0;
                        break;
                    case "B":
                    case "C":
                        percent = (100 / lengthOne) * 1;
                        break;
                    case "D":
                    case "E":

                        break;
                    case "F":
                    case "G":

                        break;
                    case "H":
                    case "I":
                        percent = (100 / lengthOne) * 2;
                        break;
                    case "J":
                    case "K":
                        percent = (100 / lengthOne) * 3;
                        break;
                    case "L":
                    case "M":
                        percent = (100 / lengthOne) * 4;
                        break;
                    case "N":
                    case "O":
                        percent = (100 / lengthOne) * 6;
                        break;
                    case "P":
                    case "Q":
                        percent = 100;
                        break;
                }

                return percent.ToString("f2");
            }
        }

        public string SubmitDateString
        {
            get
            {
                return AllotDate.ToString("yyyy-MM-dd hh:mm");
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (Status == "C" || Status == "E" || Status == "G" || Status == "I" || Status == "K" || Status == "M" || Status == "O" || Status == "Q")
                {
                    // linkAction = "分配被驳回";
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProSecondValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}\'>分配被驳回</a>", AuditSysNo);
                }
                else
                {
                    //linkAction = "分配中";
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProSecondValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}&likeType=detail\'>分配中</a>", AuditSysNo);
                }
                if (Status == "P")
                {
                    linkAction = string.Format(" <a href=\'/ProjectValueandAllot/ProSecondValueAllotAuditBymaster.aspx?ValueAllotAuditSysNo={0}\'>分配结果</a>", AuditSysNo);
                }

                return linkAction;
            }
        }
    }
}
