﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cm_TranProjectValueAuditRecord
    {
        #region Model
        private int _sysno;
        private int? _prosysno;
        private int? _allotid;
        private string _status;
        private string _itemType;

        private string _onesuggestion;
        private string _audituser;
        private string _auditdate;
        private int? _inuser;
        private DateTime? _indate;
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo
        {
            set { _sysno = value; }
            get { return _sysno; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("ProSysNo")]
        public int? ProSysNo
        {
            set { _prosysno = value; }
            get { return _prosysno; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("AllotID")]
        public int? AllotID
        {
            set { _allotid = value; }
            get { return _allotid; }
        }
        [DataMapping("ItemType")]
        public string ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMapping("Status")]
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("OneSuggestion")]
        public string OneSuggestion
        {
            set { _onesuggestion = value; }
            get { return _onesuggestion; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser
        {
            set { _audituser = value; }
            get { return _audituser; }
        }
        /// <summary>
        /// 
        /// </summary>

        [DataMapping("AuditDate")]
        public string AuditDate
        {
            set { _auditdate = value; }
            get { return _auditdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("InUser")]
        public int? InUser
        {
            set { _inuser = value; }
            get { return _inuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("InDate")]
        public DateTime? InDate
        {
            set { _indate = value; }
            get { return _indate; }
        }

        public decimal? TranCount { get; set; }
        #endregion Model

    }
}
