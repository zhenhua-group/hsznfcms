﻿using System;
using TG.Common.EntityBuilder;
namespace TG.Model
{
    /// <summary>
    /// cm_ProImaAudit:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_ProImaAudit
    {

        public cm_ProImaAudit()
        { }
        #region Model
        /// <summary>
        /// 系统号子增
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        /// <summary>
        /// 记录人
        /// </summary>
        [DataMapping("InUser")]
        public int InUser { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }


        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("OneSuggestion")]
        public string OneSuggestion { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("TwoSuggestion")]
        public string TwoSuggestion { get; set; }

        /// <summary>
        /// 审核人姓名字符串
        /// </summary>
        public string AuditUserString { get; set; }
        #endregion Model

    }

    public class cm_ProImaAuditEntity
    {
        /// <summary>
        /// 系统号子增
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        /// <summary>
        /// 记录人
        /// </summary>
        [DataMapping("InUser")]
        public int InUser { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }


        /// <summary>
        /// 审核时间
        /// </summary>
        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }

        /// <summary>
        /// 审核人姓名字符串
        /// </summary>
        public string AuditUserString { get; set; }


    }
}

