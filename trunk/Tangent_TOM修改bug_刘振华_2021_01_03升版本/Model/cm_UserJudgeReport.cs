﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_UserJudgeReport:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_UserJudgeReport
    {
        public cm_UserJudgeReport()
        { }
        #region Model
        private int _id;
        private int? _memid;
        private string _memname;
        private decimal? _judge1;
        private decimal? _judge2;
        private decimal? _judge3;
        private decimal? _judge4;
        private decimal? _judge5;
        private decimal? _judge6;
        private decimal? _judge7;
        private decimal? _judge8;
        private decimal? _judge9;
        private decimal? _judge10;
        private decimal? _judge11;
        private decimal? _judge12;
        private int? _savestatu = 0;
        private int _kaoheunitid;
        private int _insertuserid;
        private DateTime _insertdate = DateTime.Now;
        private decimal? _judgecount = 0M;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? MemID
        {
            set { _memid = value; }
            get { return _memid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MemName
        {
            set { _memname = value; }
            get { return _memname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge1
        {
            set { _judge1 = value; }
            get { return _judge1; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge2
        {
            set { _judge2 = value; }
            get { return _judge2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge3
        {
            set { _judge3 = value; }
            get { return _judge3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge4
        {
            set { _judge4 = value; }
            get { return _judge4; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge5
        {
            set { _judge5 = value; }
            get { return _judge5; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge6
        {
            set { _judge6 = value; }
            get { return _judge6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge7
        {
            set { _judge7 = value; }
            get { return _judge7; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge8
        {
            set { _judge8 = value; }
            get { return _judge8; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge9
        {
            set { _judge9 = value; }
            get { return _judge9; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge10
        {
            set { _judge10 = value; }
            get { return _judge10; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge11
        {
            set { _judge11 = value; }
            get { return _judge11; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge12
        {
            set { _judge12 = value; }
            get { return _judge12; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SaveStatu
        {
            set { _savestatu = value; }
            get { return _savestatu; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int KaoHeUnitID
        {
            set { _kaoheunitid = value; }
            get { return _kaoheunitid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int InsertUserId
        {
            set { _insertuserid = value; }
            get { return _insertuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? JudgeCount
        {
            set { _judgecount = value; }
            get { return _judgecount; }
        }
        #endregion Model

    }
    public partial class cm_UserJudgeReportEntityOrder
    {
        public cm_UserJudgeReportEntityOrder()
        { }
        #region Model
        private int _id;
        private int? _memid;
        private string _memname;
        private decimal? _judge1;
        private decimal? _judge2;
        private decimal? _judge3;
        private decimal? _judge4;
        private decimal? _judge5;
        private decimal? _judge6;
        private decimal? _judge7;
        private decimal? _judge8;
        private decimal? _judge9;
        private decimal? _judge10;
        private decimal? _judge11;
        private decimal? _judge12;
        private int? _savestatu = 0;
        private int _kaoheunitid;
        private int _insertuserid;
        private DateTime _insertdate = DateTime.Now;
        private decimal? _judgecount = 0M;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? MemID
        {
            set { _memid = value; }
            get { return _memid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MemName
        {
            set { _memname = value; }
            get { return _memname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge1
        {
            set { _judge1 = value; }
            get { return _judge1; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge2
        {
            set { _judge2 = value; }
            get { return _judge2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge3
        {
            set { _judge3 = value; }
            get { return _judge3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge4
        {
            set { _judge4 = value; }
            get { return _judge4; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge5
        {
            set { _judge5 = value; }
            get { return _judge5; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge6
        {
            set { _judge6 = value; }
            get { return _judge6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge7
        {
            set { _judge7 = value; }
            get { return _judge7; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge8
        {
            set { _judge8 = value; }
            get { return _judge8; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge9
        {
            set { _judge9 = value; }
            get { return _judge9; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge10
        {
            set { _judge10 = value; }
            get { return _judge10; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge11
        {
            set { _judge11 = value; }
            get { return _judge11; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Judge12
        {
            set { _judge12 = value; }
            get { return _judge12; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SaveStatu
        {
            set { _savestatu = value; }
            get { return _savestatu; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int KaoHeUnitID
        {
            set { _kaoheunitid = value; }
            get { return _kaoheunitid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int InsertUserId
        {
            set { _insertuserid = value; }
            get { return _insertuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? JudgeCount
        {
            set { _judgecount = value; }
            get { return _judgecount; }
        }

        public int? OrderID
        { get; set; }

        public int? IndexID
        { get; set; }
        #endregion Model

    }
}

