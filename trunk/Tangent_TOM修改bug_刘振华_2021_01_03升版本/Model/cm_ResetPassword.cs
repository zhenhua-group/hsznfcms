﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_ResetPassword:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_ResetPassword
    {
        public cm_ResetPassword()
        { }
        #region Model
        private int _id;
        private string _mem_login;
        private DateTime? _inserttime;
        private int? _isreset;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mem_Login
        {
            set { _mem_login = value; }
            get { return _mem_login; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InsertTime
        {
            set { _inserttime = value; }
            get { return _inserttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? IsReset
        {
            set { _isreset = value; }
            get { return _isreset; }
        }
        #endregion Model

    }
}