﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeRenWuCountHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeRenWuCountHis
	{
		public cm_KaoHeRenWuCountHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _renwuname;
		private decimal? _shijicount;
		private decimal? _initcount;
		private decimal? _jishucount;
		private decimal? _jiangjincount;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ShijiCount
		{
			set{ _shijicount=value;}
			get{return _shijicount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? InitCount
		{
			set{ _initcount=value;}
			get{return _initcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JishuCount
		{
			set{ _jishucount=value;}
			get{return _jishucount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JiangjinCount
		{
			set{ _jiangjincount=value;}
			get{return _jiangjincount;}
		}
		#endregion Model

	}
}

