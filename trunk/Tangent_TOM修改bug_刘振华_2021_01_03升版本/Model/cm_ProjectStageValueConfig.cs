﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_ProjectStageValueConfig:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_ProjectStageValueConfig
    {
        public cm_ProjectStageValueConfig()
        { }
        #region Model
        private int _id;
        private int? _projectstatues;
        private string _itemtype;
        private decimal? _programpercent;
        private decimal? _preliminarypercent;
        private decimal? _workdrawpercent;
        private decimal _latestagepercent;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ProjectStatues
        {
            set { _projectstatues = value; }
            get { return _projectstatues; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ItemType
        {
            set { _itemtype = value; }
            get { return _itemtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProgramPercent
        {
            set { _programpercent = value; }
            get { return _programpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? preliminaryPercent
        {
            set { _preliminarypercent = value; }
            get { return _preliminarypercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WorkDrawPercent
        {
            set { _workdrawpercent = value; }
            get { return _workdrawpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal LateStagePercent
        {
            set { _latestagepercent = value; }
            get { return _latestagepercent; }
        }
        #endregion Model

    }
}
