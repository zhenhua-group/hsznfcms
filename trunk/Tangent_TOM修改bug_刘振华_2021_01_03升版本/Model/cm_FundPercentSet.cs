﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    public partial class cm_FundPercentSet
    {
        public cm_FundPercentSet()
        { }
        #region Model
        private int _id;
        private int? _unit_id;
        private string _fund_year;
        private decimal? _fund_percent;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? unit_id
        {
            set { _unit_id = value; }
            get { return _unit_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string fund_Year
        {
            set { _fund_year = value; }
            get { return _fund_year; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? fund_percent
        {
            set { _fund_percent = value; }
            get { return _fund_percent; }
        }
        #endregion Model

    }
}
