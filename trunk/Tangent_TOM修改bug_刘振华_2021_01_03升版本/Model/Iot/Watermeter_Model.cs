﻿using System;
namespace TG.Model
{
    public class Watermeter_Model
    {
        /// <summary>
        /// 编号
        /// </summary>
       public int w_id{ get; set; }
        /// <summary>
        /// 名称
        /// </summary>
       public string w_name{ get; set; }
        /// <summary>
        /// 编码号
        /// </summary>
       public string w_iderty{ get; set; }
        /// <summary>
        /// 已经使用
        /// </summary>
        public int w_use{ get; set; }
        /// <summary>
        /// 未使用
        /// </summary>
        public int w_isuse{ get; set; }
        /// <summary>
        /// 是否开启
        /// </summary>
        public bool w_type{ get; set; }

        
    }
}
