﻿using System;
namespace TG.Model
{
    public class Wind_Model
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int w_id{ get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public string w_name{ get; set; }
        /// <summary>
        /// 编码号
        /// </summary>
        public string w_iderty { get; set; }
        /// <summary>
        /// 是否开启
        /// </summary>
        public bool w_type { get; set; }
    }
}
