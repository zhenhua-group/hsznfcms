﻿using System;
namespace TG.Model
{
    public class FloorNum_Model
    {
        /// <summary>
        /// 楼层编号
        /// </summary>
        public int fn_id{ get; set; }
        /// <summary>
        /// 楼层名称
        /// </summary>
        public string fn_name{ get; set; }
        /// <summary>
        /// 楼层编码
        /// </summary>
        public string fn_code{ get; set; }
        /// <summary>
        /// 单元ID
        /// </summary>
        public string u_id{ get; set; }
    }
}
