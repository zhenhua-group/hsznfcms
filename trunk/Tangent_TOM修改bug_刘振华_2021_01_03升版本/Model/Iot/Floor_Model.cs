﻿using System;
namespace TG.Model
{
    public class Floor_Model
    {
        /// <summary>
        /// 楼号编号
        /// </summary>
        public int f_id{ get; set; }
        /// <summary>
        /// 楼号名称
        /// </summary>
        public string f_name{ get; set; }

        /// <summary>
        /// 楼号编码
        /// </summary>
        public string f_code{ get; set; }

    }
}
