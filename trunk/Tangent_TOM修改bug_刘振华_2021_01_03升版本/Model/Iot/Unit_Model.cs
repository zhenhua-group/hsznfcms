﻿using System;
namespace TG.Model
{
    public class Unit_Model
    {
        /// <summary>
        /// 单元编号
        /// </summary>
        public int u_id{ get; set; }
        /// <summary>
        /// 单元名称
        /// </summary>
        public string u_name{ get; set; }
        /// <summary>
        /// 单元编码
        /// </summary>
        public string u_code{ get; set; }
        /// <summary>
        /// 楼层ID
        /// </summary>
        public string f_id { get; set; }
    }
}
