﻿using System;
namespace TG.Model
{
    public class Door_Model
    {
        /// <summary>
        /// 门的编号
        /// </summary>
        public int d_id { get; set; }
        /// <summary>
        /// 门的位置名称
        /// </summary>
        public string d_name { get; set; }
        /// <summary>
        /// 编码号
        /// </summary>
        public string d_iderty { get; set; }
        /// <summary>
        /// 门的状态
        /// </summary>
        public bool d_type { get; set; }

        
    }
}
