﻿using System;
namespace TG.Model
{
    public class Electric_Model
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int e_id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string e_name { get; set; }
        /// <summary>
        /// 编码号
        /// </summary>
        public string e_iderty { get; set; }
        /// <summary>
        /// 已经使用
        /// </summary>
        public int e_use { get; set; }
        /// <summary>
        /// 未使用
        /// </summary>
        public int e_isuse { get; set; }
        /// <summary>
        /// 是否开启
        /// </summary>
        public bool e_type { get; set; }
    }
}
