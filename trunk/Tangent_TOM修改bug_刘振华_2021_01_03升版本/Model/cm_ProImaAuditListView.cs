﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    public class cm_ProImaAuditListView
    {

        #region Model
        private int pro_id;
        private string _pro_order;
        private string _pro_name;
        private string _pro_kinds;
        private string _pro_buildunit;
        private string _pro_status;
        private int? _pro_level;
        private DateTime? _pro_starttime;
        private DateTime? _pro_finishtime;
        private int? _pro_src;
        private string _chgjia;
        private string _phone;
        private int _coperationsysno;
        private string _project_reletive;
        private decimal? _cpr_acount;
        private string _unit;
        private decimal? _projectscale;
        private string _buildaddress;
        private string _pro_intro;
        private string _industry;
        private string _buildtype;
        private string _projsub;
        /// <summary>
        /// 
        /// </summary>
        public int pro_ID
        {
            set { pro_id = value; }
            get { return pro_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_order
        {
            set { _pro_order = value; }
            get { return _pro_order; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_name
        {
            set { _pro_name = value; }
            get { return _pro_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_kinds
        {
            set { _pro_kinds = value; }
            get { return _pro_kinds; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_buildUnit
        {
            set { _pro_buildunit = value; }
            get { return _pro_buildunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_status
        {
            set { _pro_status = value; }
            get { return _pro_status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? pro_level
        {
            set { _pro_level = value; }
            get { return _pro_level; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? pro_startTime
        {
            set { _pro_starttime = value; }
            get { return _pro_starttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? pro_finishTime
        {
            set { _pro_finishtime = value; }
            get { return _pro_finishtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Pro_src
        {
            set { _pro_src = value; }
            get { return _pro_src; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJia
        {
            set { _chgjia = value; }
            get { return _chgjia; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Phone
        {
            set { _phone = value; }
            get { return _phone; }
        }

        public int CoperationSysNo
        {
            set { _coperationsysno = value; }
            get { return _coperationsysno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Project_reletive
        {
            set { _project_reletive = value; }
            get { return _project_reletive; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Cpr_Acount
        {
            set { _cpr_acount = value; }
            get { return _cpr_acount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Unit
        {
            set { _unit = value; }
            get { return _unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProjectScale
        {
            set { _projectscale = value; }
            get { return _projectscale; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildAddress
        {
            set { _buildaddress = value; }
            get { return _buildaddress; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_Intro
        {
            set { _pro_intro = value; }
            get { return _pro_intro; }
        }
        public string Industry
        {
            set { _industry = value; }
            get { return _industry; }
        }
        public string BuildType
        {
            set { _buildtype = value; }
            get { return _buildtype; }
        }
        public string ProjSub
        {
            set { _projsub = value; }
            get { return _projsub; }
        }
        public string JobNum { get; set; }

        public string cprID { get; set; }

        public int pro_DesignUnitID { get; set; }

        public int pro_category { get; set; }
        public int pro_flag { get; set; }
        public string StartTimeString
        {
            get
            {
                return Convert.ToDateTime(pro_startTime).ToString("yyyy-MM-dd");
            }
        }
        public string FinishTimeString
        {
            get
            {
                return Convert.ToDateTime(pro_finishTime).ToString("yyyy-MM-dd");
            }
        }
        //结构样式
        public string pro_StruType { get; set; }
        //
        public string Pro_number { get; set; }

        public int UpdateBy { get; set; }

        public int ReferenceSysNo
        {
            get;
            set;
        }

        public string PMName { get; set; }

        public string PMPhone { get; set; }


        /// <summary>
        /// 合同审核记录SysNo
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        /// <summary>
        /// 审核部门
        /// </summary>
        public string AuditDepartment
        {
            get
            {
                string str = "";
                switch (AuditStatus)
                {
                    case "A":
                    case "C":
                        str = "承接部门";
                        break;
                    case "B":
                    case "E":
                        str = "技术质量部";
                        break;
                    case "D":
                    case "G":
                        str = "生产经营部";
                        break;
                    case "H":
                    case "I":
                    case "F":
                        str = "总经理";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }

        /// <summary>
        /// 审核状态图
        /// </summary>
        public string AuditStatusImage
        {
            get
            {
                string str = string.Empty;
                switch (AuditStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        str = "审核中";
                        break;
                    case "H":
                        str = "审核通过";
                        break;
                    case "I":
                        str = "审核拒接";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }

        public string Percent
        {
            get
            {
                int length = 2, percent = 0;

                switch (AuditStatus)
                {
                    case "A":
                        percent = 0;
                        break;
                    case "B":
                    case "C":
                        percent = (100 / length) * 1;
                        break;
                    case "D":
                    case "E":
                        percent = (100 / length) * 2;
                        break;
                }

                return percent.ToString("f2");
            }
        }

        /// <summary>
        /// 可执行操作
        /// </summary>
        public string ActionLink
        {
            get
            {
                string actionLinkString = string.Empty;
                switch (AuditStatus)
                {
                    case "":
                    case null:
                    case "C":
                    case "E":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + pro_id + "\">发起审核</span>";
                        break;
                    case "A":
                    case "B":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" proSysNo=\"" + pro_id + "\">审核中</span>";
                        break;
                    case "D":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" proSysNo=\"" + pro_id + "\" proAuditSysNo=\"" + AuditRecordSysNo + "\">查看审核结果</span>";
                        break;
                }
                return actionLinkString;
            }
        }

        public cm_ProImaAudit cm_ProImaAuditEntity { get; set; }
        #endregion Model
    }
}
