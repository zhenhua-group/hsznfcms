﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_subproject:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_subproject
	{
		public tg_subproject()
		{}
		#region Model
		private int _pro_id;
		private string _pro_name;
		private int? _pro_parentid;
		private string _pro_descr;
		private int? _pro_isterminal;
		private int? _pro_parentsubid;
		/// <summary>
		/// 
		/// </summary>
		public int pro_ID
		{
			set{ _pro_id=value;}
			get{return _pro_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_Name
		{
			set{ _pro_name=value;}
			get{return _pro_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? pro_parentID
		{
			set{ _pro_parentid=value;}
			get{return _pro_parentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_descr
		{
			set{ _pro_descr=value;}
			get{return _pro_descr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? pro_IsTerminal
		{
			set{ _pro_isterminal=value;}
			get{return _pro_isterminal;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? pro_parentSubID
		{
			set{ _pro_parentsubid=value;}
			get{return _pro_parentsubid;}
		}
		#endregion Model

	}
}

