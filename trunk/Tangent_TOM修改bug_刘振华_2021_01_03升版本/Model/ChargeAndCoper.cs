﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ChargeAndCoper
    {
        /// <summary>
        /// 单位
        /// </summary>
        [DataMapping("Unit")]
        public string Unit { get; set; }
        /// <summary>
        /// 第一个月份--收费
        /// </summary>
        [DataMapping("Frsitmonthcharge")]
        public decimal Frsitmonthcharge { get; set; }
        /// <summary>
        /// 第二个对比的月份--收费
        /// </summary>
        [DataMapping("Secondmonthcharge")]
        public decimal Secondmonthcharge { get; set; }
        /// <summary>
        /// 收费环比增减
        /// </summary>
        public string ContrastCharge
        {
            get
            {
                try
                {
                    return ((Secondmonthcharge - Frsitmonthcharge) * 100 / Frsitmonthcharge).ToString("f2") + "%";
                }
                catch (Exception)
                {

                    return "";
                }
            }
        }
        /// <summary>
        /// 第一个月份--合同额
        /// </summary>
        [DataMapping("Frsitmonthcoper")]
        public decimal Frsitmonthcoper { get; set; }
        /// <summary>
        /// 第二个对比的月份--合同额
        /// </summary>
        [DataMapping("Secondmonthcoper")]
        public decimal Secondmonthcoper { get; set; }
        /// <summary>
        /// 合同额环比增减
        /// </summary>
        public string ContrastCoper
        {
            get
            {
                try
                {
                    return ((Secondmonthcoper - Frsitmonthcoper) * 100 / Frsitmonthcoper).ToString("f2") + "%";
                }
                catch (Exception)
                {

                    return "";
                }
            }
        }
        /// <summary>
        /// 所求年份--2012年
        /// </summary>
        [DataMapping("UnitAllotLast")]
        public int UnitAllotLast { get; set; }
    }
}
