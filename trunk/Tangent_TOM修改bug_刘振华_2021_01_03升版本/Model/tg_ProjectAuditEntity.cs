﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;
using System.Data;

namespace TG.Model
{
    [Serializable]
    public class ProjectAuditQueryEntity
    {
        /// <summary>
        /// 项目审核编号
        /// </summary>
        public int ProjectAuditSysNo { get; set; }

        /// <summary>
        /// 意见
        /// </summary>
        public string Suggestion { get; set; }
    }

    [Serializable]
    public class ProjectAuditDataEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }


        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }

        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        [DataMapping("AudtiDate")]
        public string AudtiDate { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }
    }
    [Serializable]
    public class ProjectAuditEditEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("options")]
        public string options { get; set; }

        [DataMapping("ChangeDetail")]
        public string ChangeDetail { get; set; }

        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }

        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        [DataMapping("AudtiDate")]
        public string AudtiDate { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }
    }
    [Serializable]
    public class cm_ProjectVolltAuditEdit
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("SysNo")]
        public int SysNo
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("Status")]
        public string Status
        {
            get;
            set;
        }
        /// 
        /// </summary>
        [DataMapping("ProjectUrlDetail")]
        public string ProjectUrlDetail
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("options")]
        public string options
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("InDate")]
        public DateTime InDate
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        [DataMapping("InUser")]
        public int InUser
        {
            get;
            set;
        }


    }
}
