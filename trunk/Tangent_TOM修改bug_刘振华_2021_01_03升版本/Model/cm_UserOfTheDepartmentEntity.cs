﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
        public class TreeDepartmentEntity
        {
                [DataMapping("DepartmentSysNo")]
                public int DepartmentSysNo { get; set; }

                [DataMapping("DepartmentName")]
                public string DepartmentName { get; set; }

                public List<TreeUserEntity> Users { get; set; }
        }

        public class TreeUserEntity
        {
                [DataMapping("UserSysNo")]
                public int UserSysNo { get; set; }

                [DataMapping("UserName")]
                public string UserName { get; set; }
                [DataMapping("UserSex")]
                public string UserSex { get; set; }
                [DataMapping("SpecialtyName")]
                public string SpecialtyName { get; set; }
                [DataMapping("SpecialtyNo")]
                public int SpecialtyNo { get; set; }
        }
}
