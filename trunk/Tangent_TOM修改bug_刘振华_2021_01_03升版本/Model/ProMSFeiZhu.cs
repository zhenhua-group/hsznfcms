﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class ProMSFeiZhu
    {
        public ProMSFeiZhu()
        { }
        #region Model
        private int _sid;
        private string _sconpay;
        private int _scount;
        private int _smubiaozhi;
        private decimal? _sleijidangyue;
        private decimal? _sleijibenniandu;
        private decimal _sovermubiaozhi;
        private string _sxianmumingcheng;
        private decimal? _sbenzhoushoufei;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string Sxianmumingcheng
        {
            get { return _sxianmumingcheng; }
            set { _sxianmumingcheng = value; }
        }
        /// <summary>
        /// 项目的本周的收费情况
        /// </summary>
        public decimal? SBenzhoushoufei
        {
            set { _sbenzhoushoufei = value; }
            get { return _sbenzhoushoufei; }
        }
        /// <summary>
        /// 各个所的编号
        /// </summary>
        public int Sid
        {
            set { _sid = value; }
            get { return _sid; }
        }
        /// <summary>
        /// 单位（所）的名称
        /// </summary>
        public string SConpay
        {
            set { _sconpay = value; }
            get { return _sconpay; }
        }
        /// <summary>
        /// 所 对应的的项目数 
        /// </summary>
        public int SCount
        {
            set { _scount = value; }
            get { return _scount; }
        }
        /// <summary>
        /// 所  目标值
        /// </summary>
        public int SMubiaozhi
        {
            set { _smubiaozhi = value; }
            get { return _smubiaozhi; }
        }
        /// <summary>
        /// 累计当月的收费
        /// </summary>
        public decimal? SLeijidangyue
        {
            set { _sleijidangyue = value; }
            get { return _sleijidangyue; }
        }
        /// <summary>
        /// 累计本年度
        /// </summary>
        public decimal? SLeijibenniandu
        {
            set { _sleijibenniandu = value; }
            get { return _sleijibenniandu; }
        }
        /// <summary>
        /// 完成目标值(百分比)本年度/目标值
        /// </summary>
        public decimal SOvermubiaozhi
        {
            set { _sovermubiaozhi = value; }
            get { return _sovermubiaozhi; }
        }
        #endregion Model
    }
}
