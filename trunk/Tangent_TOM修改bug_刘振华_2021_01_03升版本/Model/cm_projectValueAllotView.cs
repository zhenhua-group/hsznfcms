﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    public class cm_projectValueAllotView
    {
        #region Model
        private int _pro_id;
        private int? _cst_id;
        private string _cpr_no;
        private string _cpr_type;
        private string _cpr_name;
        private string _cpr_unit;
        private decimal? _cpr_acount;
        private decimal? _cpr_shijiacount;
        private string _cpr_process;
        private DateTime? _cpr_signdate;
        private DateTime? _cpr_donedate;
        private string _cpr_mark;
        private string _buildarea;
        private string _chgpeople;
        private string _chgphone;
        private string _chgjia;
        private string _chgjiaphone;
        private string _buildtype;
        private decimal _payshicount;
        private decimal _allotcount;


        /// <summary>
        /// 
        /// </summary>
        public int pro_Id
        {
            set { _pro_id = value; }
            get { return _pro_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type
        {
            set { _cpr_type = value; }
            get { return _cpr_type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Pro_Name
        {
            set { _cpr_name = value; }
            get { return _cpr_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Unit
        {
            set { _cpr_unit = value; }
            get { return _cpr_unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Acount
        {
            set { _cpr_acount = value; }
            get { return _cpr_acount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiAcount
        {
            set { _cpr_shijiacount = value; }
            get { return _cpr_shijiacount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Process
        {
            set { _cpr_process = value; }
            get { return _cpr_process; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_SignDate
        {
            set { _cpr_signdate = value; }
            get { return _cpr_signdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_DoneDate
        {
            set { _cpr_donedate = value; }
            get { return _cpr_donedate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Mark
        {
            set { _cpr_mark = value; }
            get { return _cpr_mark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildArea
        {
            set { _buildarea = value; }
            get { return _buildarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPeople
        {
            set { _chgpeople = value; }
            get { return _chgpeople; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPhone
        {
            set { _chgphone = value; }
            get { return _chgphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJia
        {
            set { _chgjia = value; }
            get { return _chgjia; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJiaPhone
        {
            set { _chgjiaphone = value; }
            get { return _chgjiaphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildType
        {
            set { _buildtype = value; }
            get { return _buildtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal PayShiCount
        {
            set { _payshicount = value; }
            get { return _payshicount; }
        }


        public decimal Allotcount
        {
            get { return _allotcount; }
            set { _allotcount = value; }
        }

        /// <summary>
        /// 合同审核记录SysNo
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        /// <summary>
        /// 审核部门
        /// </summary>
        public string AuditDepartment
        {
            get
            {
                string str = "";
                switch (AuditStatus)
                {
                    case "A":
                    case "C":
                        str = "承接部门";
                        break;
                    case "B":
                    case "E":
                        str = "经营处";
                        break;
                    case "D":
                    case "G":
                        str = "技术部门";
                        break;
                    case "H":
                    case "I":
                    case "F":
                        str = "总经理";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }

        /// <summary>
        /// 审核状态图
        /// </summary>
        public string AuditStatusImage
        {
            get
            {
                string str = string.Empty;
                switch (AuditStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        str = "审核中";
                        break;
                    case "H":
                        str = "审核通过";
                        break;
                    case "I":
                        str = "审核拒接";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }
        public string Percent
        {
            get
            {
                decimal percent = 0;
                if (CoperationAllotEntity != null)
                {
                    if (PayShiCount != 0)
                    {
                        percent = (Allotcount / PayShiCount) * 100;
                    }
                }
                return percent.ToString("f2");
            }
        }

        /// <summary>
        /// 可执行操作
        /// </summary>
        public string ActionLink
        {
            get
            {
                string actionLinkString = string.Empty;
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + pro_Id + "\">发起分配</span>";
                return actionLinkString;
            }
        }

        public cm_ProjectValueAuditRecord CoperationAllotEntity { get; set; }
        #endregion Model

    }
}
