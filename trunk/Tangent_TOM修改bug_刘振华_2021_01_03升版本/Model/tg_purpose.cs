﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_purpose:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_purpose
	{
		public tg_purpose()
		{}
		#region Model
		private int _purpose_id;
		private string _purpose_name;
		private int _folder;
		/// <summary>
		/// 
		/// </summary>
		public int purpose_ID
		{
			set{ _purpose_id=value;}
			get{return _purpose_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string purpose_Name
		{
			set{ _purpose_name=value;}
			get{return _purpose_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int folder
		{
			set{ _folder=value;}
			get{return _folder;}
		}
		#endregion Model

	}
}

