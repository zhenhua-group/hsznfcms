﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeProjWeightHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeProjWeightHis
	{
		public cm_KaoHeProjWeightHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _renwuname;
		private string _weightname;
		private decimal? _jz=0M;
		private decimal? _jg=0M;
		private decimal? _sb=0M;
		private decimal? _dq=0M;
		private int? _stat=0;
		private decimal? _projcount=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string WeightName
		{
			set{ _weightname=value;}
			get{return _weightname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JZ
		{
			set{ _jz=value;}
			get{return _jz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JG
		{
			set{ _jg=value;}
			get{return _jg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SB
		{
			set{ _sb=value;}
			get{return _sb;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? DQ
		{
			set{ _dq=value;}
			get{return _dq;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ProjCount
		{
			set{ _projcount=value;}
			get{return _projcount;}
		}
		#endregion Model

	}
}

