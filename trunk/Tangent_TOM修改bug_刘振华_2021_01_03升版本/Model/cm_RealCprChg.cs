﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_RealCprChg:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_RealCprChg
    {
        public cm_RealCprChg()
        { }
        #region Model
        private int _id;
        private int? _chg_id;
        private decimal? _paycount;
        private DateTime? _paytime;
        private string _acceptuser;
        private int? _isover = 0;
        private string _mark;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? chg_id
        {
            set { _chg_id = value; }
            get { return _chg_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? payCount
        {
            set { _paycount = value; }
            get { return _paycount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? payTime
        {
            set { _paytime = value; }
            get { return _paytime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string acceptuser
        {
            set { _acceptuser = value; }
            get { return _acceptuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? isover
        {
            set { _isover = value; }
            get { return _isover; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mark
        {
            set { _mark = value; }
            get { return _mark; }
        }

        /// <summary>
        /// 入账单号
        /// </summary>
        public string BillNo { get; set; }

        /// <summary>
        /// 汇款人姓名
        /// </summary>
        public string Remitter { get; set; }

        /// <summary>
        /// 状态，A为初始状态，S为确认通过状态，D为确认拒结状态
        /// </summary>
        public string Status { get; set; }
        #endregion Model

    }
}

