﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
        public class ProjectPlanAudtiListViewEntity
        {
                [DataMapping("ProjectName")]
                public string ProjectName { get; set; }

                [DataMapping("HasCoperation")]
                public string HasCoperation { get; set; }

                [DataMapping("InDate")]
                public DateTime InDate { get; set; }

                [DataMapping("InUserName")]
                public int InUserName { get; set; }

                [DataMapping("JobNumber")]
                public string JobNumber { get; set; }


        }
}
