﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ProImage:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ProImage
	{
		public cm_ProImage()
		{}
		#region Model
		private int _id;
		private string _pro_id;
		private string _pro_name;
		private string _pro_number;
		private string _pro_unit;
		private string _pro_pmname;
		private string _pro_status;
		private string _jz_image;
		private string _jz_cadimage;
		private string _jg_image;
		private string _jg_cadimage;
		private string _jps_image;
		private string _jps_cadimage;
		private string _nt_image;
		private string _nt_cadimage;
		private string _dq_image;
		private string _dq_cadimage;
		private int? _zh;
		private string _image_type;
		private int? _guidang;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_id
		{
			set{ _pro_id=value;}
			get{return _pro_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_name
		{
			set{ _pro_name=value;}
			get{return _pro_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_number
		{
			set{ _pro_number=value;}
			get{return _pro_number;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_unit
		{
			set{ _pro_unit=value;}
			get{return _pro_unit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_pmname
		{
			set{ _pro_pmname=value;}
			get{return _pro_pmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pro_status
		{
			set{ _pro_status=value;}
			get{return _pro_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jz_image
		{
			set{ _jz_image=value;}
			get{return _jz_image;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jz_cadimage
		{
			set{ _jz_cadimage=value;}
			get{return _jz_cadimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jg_image
		{
			set{ _jg_image=value;}
			get{return _jg_image;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jg_cadimage
		{
			set{ _jg_cadimage=value;}
			get{return _jg_cadimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jps_image
		{
			set{ _jps_image=value;}
			get{return _jps_image;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jps_cadimage
		{
			set{ _jps_cadimage=value;}
			get{return _jps_cadimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string nt_image
		{
			set{ _nt_image=value;}
			get{return _nt_image;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string nt_cadimage
		{
			set{ _nt_cadimage=value;}
			get{return _nt_cadimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dq_image
		{
			set{ _dq_image=value;}
			get{return _dq_image;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dq_cadimage
		{
			set{ _dq_cadimage=value;}
			get{return _dq_cadimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? zh
		{
			set{ _zh=value;}
			get{return _zh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string image_type
		{
			set{ _image_type=value;}
			get{return _image_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? guidang
		{
			set{ _guidang=value;}
			get{return _guidang;}
		}
		#endregion Model

	}
}

