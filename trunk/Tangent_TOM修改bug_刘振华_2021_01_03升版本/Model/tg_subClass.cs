﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_subClass:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_subClass
	{
		public tg_subClass()
		{}
		#region Model
		private int _id;
		private int? _class_id;
		private int? _subproid;
		private int? _purposeid=0;
		private string _name;
		private int? _parentid;
		private string _descr;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? class_ID
		{
			set{ _class_id=value;}
			get{return _class_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? subproID
		{
			set{ _subproid=value;}
			get{return _subproid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? purposeid
		{
			set{ _purposeid=value;}
			get{return _purposeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? parentID
		{
			set{ _parentid=value;}
			get{return _parentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string descr
		{
			set{ _descr=value;}
			get{return _descr;}
		}
		#endregion Model

	}
}

