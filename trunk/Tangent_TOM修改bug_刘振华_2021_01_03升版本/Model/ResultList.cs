﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class ResultList<T>
    {
        public int TotalCount { get; set; }

        public List<T> Body { get; set; }
    }
}
