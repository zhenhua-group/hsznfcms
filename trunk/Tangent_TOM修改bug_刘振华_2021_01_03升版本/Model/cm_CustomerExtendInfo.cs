﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_CustomerExtendInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_CustomerExtendInfo
	{
		public cm_CustomerExtendInfo()
		{}
		#region Model
		private int _cst_extendinfoid;
		private int _cst_id;
		private string _cst_englishname;
		private string _country;
		private string _province;
		private string _city;
		private int? _profession;
		private int? _type;
		private string _email;
		private string _relationdepartment;
		private string _bankname;
		private string _taxaccountno;
		private string _bankaccountno;
		private string _remark;
		private int? _ispartner;
		private string _branchpart;
		private string _cpy_principalsheet;
		private DateTime? _createrelationtime;
		private int? _creditleve;
		private int? _closeleve;
		private string _cpy_code;
		private string _lawperson;
		private int? _updateby;
		private DateTime? _lastupdate;
		/// <summary>
		/// 
		/// </summary>
		public int Cst_ExtendInfoId
		{
			set{ _cst_extendinfoid=value;}
			get{return _cst_extendinfoid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Cst_Id
		{
			set{ _cst_id=value;}
			get{return _cst_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Cst_EnglishName
		{
			set{ _cst_englishname=value;}
			get{return _cst_englishname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Country
		{
			set{ _country=value;}
			get{return _country;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Province
		{
			set{ _province=value;}
			get{return _province;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string City
		{
			set{ _city=value;}
			get{return _city;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Profession
		{
			set{ _profession=value;}
			get{return _profession;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RelationDepartment
		{
			set{ _relationdepartment=value;}
			get{return _relationdepartment;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BankName
		{
			set{ _bankname=value;}
			get{return _bankname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TaxAccountNo
		{
			set{ _taxaccountno=value;}
			get{return _taxaccountno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BankAccountNo
		{
			set{ _bankaccountno=value;}
			get{return _bankaccountno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsPartner
		{
			set{ _ispartner=value;}
			get{return _ispartner;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BranchPart
		{
			set{ _branchpart=value;}
			get{return _branchpart;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Cpy_PrincipalSheet
		{
			set{ _cpy_principalsheet=value;}
			get{return _cpy_principalsheet;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CreateRelationTime
		{
			set{ _createrelationtime=value;}
			get{return _createrelationtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CreditLeve
		{
			set{ _creditleve=value;}
			get{return _creditleve;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CloseLeve
		{
			set{ _closeleve=value;}
			get{return _closeleve;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Cpy_Code
		{
			set{ _cpy_code=value;}
			get{return _cpy_code;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LawPerson
		{
			set{ _lawperson=value;}
			get{return _lawperson;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdateBy
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastUpdate
		{
			set{ _lastupdate=value;}
			get{return _lastupdate;}
		}
		#endregion Model

	}
}

