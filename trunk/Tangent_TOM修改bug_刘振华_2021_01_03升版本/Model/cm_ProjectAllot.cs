﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ProjectAllot:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ProjectAllot
	{
		public cm_ProjectAllot()
		{}
		#region Model
		private int _id;
		private int? _pro_id;
		private decimal? _money;
		private decimal? _percent;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Pro_ID
		{
			set{ _pro_id=value;}
			get{return _pro_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Money
		{
			set{ _money=value;}
			get{return _money;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Percent
		{
			set{ _percent=value;}
			get{return _percent;}
		}
		#endregion Model

	}
}

