﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_Coperation_back:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_Coperation_back
	{
		public cm_Coperation_back()
		{}
		#region Model
		private int _cpr_id;
		private int? _cst_id;
		private string _cpr_type;
		private string _cpr_type2;
		private string _cpr_unit;
		private decimal? _cpr_acount;
		private decimal? _cpr_shijiacount;
		private decimal? _cpr_touzi;
		private decimal? _cpr_shijitouzi;
		private string _cpr_mark;
		private string _cpr_address;
		private string _buildarea;
		private string _chgpeople;
		private string _chgphone;
		private string _chgjia;
		private string _chgjiaphone;
		private string _updateby;
		private string _buildposition;
		private string _industry;
		private string _buildunit;
		private string _buildsrc;
		private DateTime? _regtime;
		private string _buildtype;
		private string _structtype;
		private string _floor="|";
		private string _buildstructtype;
		private string _multibuild;
		private string _cpr_name;
		private int? _insertuserid=-1;
		private DateTime? _insertdate= DateTime.Now;
		private int? _proapproval;
		/// <summary>
		/// 
		/// </summary>
		public int cpr_Id
		{
			set{ _cpr_id=value;}
			get{return _cpr_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? cst_Id
		{
			set{ _cst_id=value;}
			get{return _cst_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string cpr_Type
		{
			set{ _cpr_type=value;}
			get{return _cpr_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string cpr_Type2
		{
			set{ _cpr_type2=value;}
			get{return _cpr_type2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string cpr_Unit
		{
			set{ _cpr_unit=value;}
			get{return _cpr_unit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? cpr_Acount
		{
			set{ _cpr_acount=value;}
			get{return _cpr_acount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? cpr_ShijiAcount
		{
			set{ _cpr_shijiacount=value;}
			get{return _cpr_shijiacount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? cpr_Touzi
		{
			set{ _cpr_touzi=value;}
			get{return _cpr_touzi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? cpr_ShijiTouzi
		{
			set{ _cpr_shijitouzi=value;}
			get{return _cpr_shijitouzi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string cpr_Mark
		{
			set{ _cpr_mark=value;}
			get{return _cpr_mark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string cpr_Address
		{
			set{ _cpr_address=value;}
			get{return _cpr_address;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BuildArea
		{
			set{ _buildarea=value;}
			get{return _buildarea;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ChgPeople
		{
			set{ _chgpeople=value;}
			get{return _chgpeople;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ChgPhone
		{
			set{ _chgphone=value;}
			get{return _chgphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ChgJia
		{
			set{ _chgjia=value;}
			get{return _chgjia;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ChgJiaPhone
		{
			set{ _chgjiaphone=value;}
			get{return _chgjiaphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UpdateBy
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BuildPosition
		{
			set{ _buildposition=value;}
			get{return _buildposition;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Industry
		{
			set{ _industry=value;}
			get{return _industry;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BuildUnit
		{
			set{ _buildunit=value;}
			get{return _buildunit;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BuildSrc
		{
			set{ _buildsrc=value;}
			get{return _buildsrc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? RegTime
		{
			set{ _regtime=value;}
			get{return _regtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BuildType
		{
			set{ _buildtype=value;}
			get{return _buildtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string StructType
		{
			set{ _structtype=value;}
			get{return _structtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Floor
		{
			set{ _floor=value;}
			get{return _floor;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BuildStructType
		{
			set{ _buildstructtype=value;}
			get{return _buildstructtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MultiBuild
		{
			set{ _multibuild=value;}
			get{return _multibuild;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string cpr_Name
		{
			set{ _cpr_name=value;}
			get{return _cpr_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate
		{
			set{ _insertdate=value;}
			get{return _insertdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? proapproval
		{
			set{ _proapproval=value;}
			get{return _proapproval;}
		}
		#endregion Model

	}
}

