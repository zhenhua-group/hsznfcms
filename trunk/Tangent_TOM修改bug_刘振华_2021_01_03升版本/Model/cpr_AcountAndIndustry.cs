﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cpr_AcountAndIndustry
    {
        /// <summary>
        /// 单位
        /// </summary>
        [DataMapping("Unit")]
        public string Unit { get; set; }
        /// <summary>
        /// 统计项目
        /// </summary>
        public string AcountName
        {
            get
            {
                return "合同额";
            }
        }
        /// <summary>
        /// last公建
        /// </summary>
        [DataMapping("LastPublicBuild")]
        public decimal LastPublicBuild { set; get; }
        /// <summary>
        /// last保障房
        /// </summary>
        [DataMapping("LastAffordHouse")]
        public decimal LastAffordHouse { set; get; }
        /// <summary>
        /// last房地产
        /// </summary>
        [DataMapping("LastEstate")]
        public decimal LastEstate { set; get; }
        /// <summary>
        /// last综合
        /// </summary>
        [DataMapping("LastComprehensive")]
        public decimal LastComprehensive { set; get; }
        /// <summary>
        ///last合计 
        /// </summary>
        [DataMapping("LastTotal")]
        public decimal LastTotal { set; get; }

        /// <summary>
        /// now公建
        /// </summary>
        [DataMapping("NowPublicBuild")]
        public decimal NowPublicBuild { set; get; }
        /// <summary>
        /// now保障房
        /// </summary>
        [DataMapping("NowAffordHouse")]
        public decimal NowAffordHouse { set; get; }
        /// <summary>
        /// 房地产
        /// </summary>
        [DataMapping("NowEstate")]
        public decimal NowEstate { set; get; }
        /// <summary>
        /// 综合
        /// </summary>
        [DataMapping("NowComprehensive")]
        public decimal NowComprehensive { set; get; }
        /// <summary>
        /// now合计
        /// </summary>
        [DataMapping("NowTotal")]
        public decimal NowTotal
        {
            get;
            set;
        }

        /// <summary>
        /// 公建房同比增减
        /// </summary>
        public string PublicBuildContrast
        {
            get
            {
                try
                {
                    return ((NowPublicBuild - LastPublicBuild)*100 / LastPublicBuild).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 保障房同比增减
        /// </summary>
        public string AffordHouseContrast
        {
            get
            {
                try
                {
                    return ((NowAffordHouse - LastAffordHouse)*100 / LastAffordHouse).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 房地产同比增减
        /// </summary>
        public string EstateContrast
        {
            get
            {
                try
                {
                    return ((NowEstate - LastEstate)*100 / LastEstate).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 综合同比增减
        /// </summary>
        public string ComprehensiveContrast
        {
            get
            {
                try
                {
                    return ((NowComprehensive - LastComprehensive)*100 / LastComprehensive).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 合计同比增减
        /// </summary>
        public string TotalContrast
        {
            get
            {
                try
                {
                    return ((NowTotal - LastTotal)*100 / LastTotal).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }
    }
}
