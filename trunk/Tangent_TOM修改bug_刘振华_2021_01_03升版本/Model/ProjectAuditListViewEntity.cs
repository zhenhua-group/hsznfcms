﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ProjectAuditListViewDataEntity
    {
        [DataMapping("SysNo")]
        public int ProjectAuditSysNo { get; set; }

        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("Status")]
        public string ProjectStatus { get; set; }

        [DataMapping("MsgStatus")]
        public string MsgStatus { get; set; }

        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }

        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        [DataMapping("AudtiDate")]
        public string AudtiDate { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("pro_Name")]
        public string ProjectName { get; set; }

        [DataMapping("FromUserName")]
        public string InUserName { get; set; }

        [DataMapping("ExtendField")]
        public string ExtendField { get; set; }

        public string InDateString
        {
            get
            {
                return InDate.ToString("yyyy-MM-dd HH:mm");
            }
        }
        public string Percent
        {
            get
            {
                return XMLHelper.AccountPercent(2, ProjectStatus).ToString("f2");
            }
        }
    }
}
