﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeProjAllot:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeProjAllot
	{
		public cm_KaoHeProjAllot()
		{}
		#region Model
		private int _id;
		private int _proid;
		private int _prokhid;
		private int _prokhnameid;
		private decimal _xnzcz;
		private decimal _xnzcz2;
		private decimal _wcbl;
		private decimal _xmjjjs;
		private decimal _xmjjjs2;
		private decimal _jjbl;
		private decimal _jsjjzs;
		private decimal _jsjjzs2;
		private decimal _xmztjx;
		private decimal _sjjjzs;
		private decimal _sjjjzs2;
		private decimal _fashjtbl;
		private decimal _fashjj;
		private decimal _fashjj2;
		private decimal _zczjjtbl;
		private decimal _zcjx;
		private decimal _zcjj;
		private decimal _zcjj2;
		private decimal _xmjj;
		private decimal _xmjj2;
		private int? _stat=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProId
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHId
		{
			set{ _prokhid=value;}
			get{return _prokhid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHNameId
		{
			set{ _prokhnameid=value;}
			get{return _prokhnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xnzcz
		{
			set{ _xnzcz=value;}
			get{return _xnzcz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xnzcz2
		{
			set{ _xnzcz2=value;}
			get{return _xnzcz2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal WcBl
		{
			set{ _wcbl=value;}
			get{return _wcbl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xmjjjs
		{
			set{ _xmjjjs=value;}
			get{return _xmjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xmjjjs2
		{
			set{ _xmjjjs2=value;}
			get{return _xmjjjs2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Jjbl
		{
			set{ _jjbl=value;}
			get{return _jjbl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Jsjjzs
		{
			set{ _jsjjzs=value;}
			get{return _jsjjzs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Jsjjzs2
		{
			set{ _jsjjzs2=value;}
			get{return _jsjjzs2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xmztjx
		{
			set{ _xmztjx=value;}
			get{return _xmztjx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Sjjjzs
		{
			set{ _sjjjzs=value;}
			get{return _sjjjzs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Sjjjzs2
		{
			set{ _sjjjzs2=value;}
			get{return _sjjjzs2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Fashjtbl
		{
			set{ _fashjtbl=value;}
			get{return _fashjtbl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Fashjj
		{
			set{ _fashjj=value;}
			get{return _fashjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Fashjj2
		{
			set{ _fashjj2=value;}
			get{return _fashjj2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zczjjtbl
		{
			set{ _zczjjtbl=value;}
			get{return _zczjjtbl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zcjx
		{
			set{ _zcjx=value;}
			get{return _zcjx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zcjj
		{
			set{ _zcjj=value;}
			get{return _zcjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zcjj2
		{
			set{ _zcjj2=value;}
			get{return _zcjj2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xmjj
		{
			set{ _xmjj=value;}
			get{return _xmjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xmjj2
		{
			set{ _xmjj2=value;}
			get{return _xmjj2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

