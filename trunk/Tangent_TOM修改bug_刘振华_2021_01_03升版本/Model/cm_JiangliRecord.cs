﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_JiangliRecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_JiangliRecord
	{
		public cm_JiangliRecord()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private DateTime? _mem_jianglidate;
		private string _mem_jiangliwhy;
		private string _mem_jianglisub;
		private int? _insertuserid;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_JiangliDate
		{
			set{ _mem_jianglidate=value;}
			get{return _mem_jianglidate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_JiangliWhy
		{
			set{ _mem_jiangliwhy=value;}
			get{return _mem_jiangliwhy;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_JiangliSub
		{
			set{ _mem_jianglisub=value;}
			get{return _mem_jianglisub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		#endregion Model

	}
}

