﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class CoperationAndProjectCountExcel
    {
        [DataMapping("UnitName")]
        public string UnitName { get; set; }

        [DataMapping("CustCount")]
        public string CustCount { get; set; }

        [DataMapping("CprCount")]
        public string CprCount { get; set; }

        [DataMapping("ProjCount")]
        public string ProjCount { get; set; }

        [DataMapping("ProjPlanCount")]
        public string ProjPlanCount { get; set; }

        [DataMapping("NoProjPlanCount")]
        public string NoProjPlanCount { get; set; }
    }
}
