﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cm_SysMsg
    {
        /// <summary>
        /// 消息系统自增号
        /// </summary>
        public int MsgSysNo { get; set; }

        /// <summary>
        /// 合同审核记录系统自增号
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 发起用户
        /// </summary>
        public int FromUser { get; set; }

        /// <summary>
        /// 发送角色
        /// </summary>
        public int ToRole { get; set; }

        /// <summary>
        /// 消息状态
        /// </summary>
        public string MsgStatus { get; set; }

        /// <summary>
        /// 消息录入时间
        /// </summary>
        public DateTime MsgInDate { get; set; }

        /// <summary>
        /// 合同系统自增号
        /// </summary>
        public int CoperationSysNo { get; set; }

        /// <summary>
        /// 合同审核记录录入人
        /// </summary>
        public int AuditRecordInUser { get; set; }

        /// <summary>
        /// 合同审核记录录入时间 
        /// </summary>
        public DateTime AuditRecordInDate { get; set; }

        /// <summary>
        /// 合同审核状态 
        /// </summary>
        public string AuditRecordStatus { get; set; }

        /// <summary>
        /// 承接部门意见 
        /// </summary>
        public string UndertakeProposal { get; set; }

        /// <summary>
        /// 经营部门意见 
        /// </summary>
        public string OperateDepartmentProposal { get; set; }

        /// <summary>
        /// 技术部门意见 
        /// </summary>
        public string TechnologyDepartmentProposal { get; set; }

        /// <summary>
        /// 总经理意见 
        /// </summary>
        public string GeneralManagerProposal { get; set; }

        ///// <summary>
        ///// 承接部门意见 
        ///// </summary>
        //public string UndertakeProposal { get; set; }

        /// <summary>
        /// 合同审核人
        /// </summary>
        public string AuditUser { get; set; }

        /// <summary>
        /// 合同审核时间 
        /// </summary>
        public string AuditDate { get; set; }

        /// <summary>
        /// 合同名称
        /// </summary>
        public string CoperationName { get; set; }

        /// <summary>
        /// 是否有电子合同即附件
        /// </summary>
        public string HaveElectronicCoperation { get; set; }

        /// <summary>
        /// 在那个部门
        /// </summary>
        public string AuditDepartment
        {
            get
            {
                string str = "";
                switch (AuditRecordStatus)
                {
                    case "A":
                    case "C":
                        str = "承接部门";
                        break;
                    case "B":
                    case "E":
                        str = "经营处";
                        break;
                    case "D":
                    case "G":
                        str = "技术部门";
                        break;
                    case "H":
                    case "I":
                    case "F":
                        str = "总经理";
                        break;
                }
                return str;
            }
        }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string StatusString
        {
            get
            {
                string str = "";
                switch (AuditRecordStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        str = "审核中";
                        break;
                    case "H":
                        str = "审核通过";
                        break;
                    case "I":
                        str = "审核拒结";
                        break;
                }
                return str;
            }
        }

        /// <summary>
        /// 消息状态String
        /// </summary>
        public string MsgStatusString
        {
            get
            {
                string str = "";
                switch (MsgStatus)
                {
                    case "A":
                        str = "未读";
                        break;
                    case "D":
                        str = "已读";
                        break;
                }
                return str;
            }
        }
    }

    /// <summary>
    /// 消息公用实体类
    /// </summary>
    public class SysMsgEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("FromUser")]
        public int FromUser { get; set; }

        [DataMapping("ToRole")]
        public string ToRole { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        /// <summary>
        /// 消息类型  1为合同审核，2为项目审核
        /// </summary>
        [DataMapping("MsgType")]
        public int MsgType { get; set; }

        /// <summary>
        /// 消息所指数据系统自增号
        /// </summary>
        [DataMapping("ReferenceSysNo")]
        public string ReferenceSysNo { get; set; }

        [DataMapping("ProjectAuditStatus")]
        public string ProjectAuditStatus { get; set; }
    }

    /// <summary>
    /// 查询实体
    /// </summary>
    [Serializable]
    public class SysMsgQueryEntity : SagoPager
    {
        public int UserSysNo { get; set; }

        public string Status { get; set; }

        public int MsgType { get; set; }

        public string ProjectAuditStatus { get; set; }

        public string CoperationAuditStatus { get; set; }
    }

    [Serializable]
    public class SYSMsgQueryEntityByMain
    {
        //--待办A 已办D 不存在B
        [DataMapping("IsDone")]
        public string IsDone { get; set; }
        //--已读D  未读A  	
        [DataMapping("Status")]
        public string Status { get; set; }
        //消息内容
        [DataMapping("MessageContent")]
        public string MessageContent { get; set; }
        //--消息类型 
        [DataMapping("MsgType")]
        public int MsgType { get; set; }
        // --消息接收人
        [DataMapping("FromUser")]
        public int FromUser { get; set; }
        public string MsgTypeName
        {

            get
            {
                string msgTypeString = "";
                switch (MsgType)
                {
                    case 1: msgTypeString = "合同审核"; break;
                    case 2: msgTypeString = "项目审核"; break;
                    case 3: msgTypeString = "工号申请"; break;
                    case 4: msgTypeString = "策划审核"; break;
                    case 5: msgTypeString = "产值分配"; break;
                    case 6: msgTypeString = "合同入账"; break;
                    case 7: msgTypeString = "项目入账"; break;
                    case 8: msgTypeString = "项目策划"; break;
                    case 9: msgTypeString = "工程出图"; break;
                    case 10: msgTypeString = "个人分配明细"; break;
                    case 11: msgTypeString = "项目修改审核"; break;
                    case 12: msgTypeString = "合同修改审核"; break;
                    case 13: msgTypeString = "策划修改审核"; break;
                    case 14: msgTypeString = "二次分配审核"; break;
                    case 15: msgTypeString = "暖通所分配"; break;
                    case 16: msgTypeString = "转经济所分配"; break;
                    case 17: msgTypeString = "分配系数审核"; break;
                    case 18: msgTypeString = "经济所分配"; break;
                    case 19: msgTypeString = "转暖通所分配"; break;
                    case 20: msgTypeString = "转土建所分配"; break;
                    case 21: msgTypeString = "工程设计出图卡"; break;
                    case 22: msgTypeString = "工程设计归档资料"; break;
                }
                return msgTypeString;
            }
        }

        public int PrecentAcountModel { get; set; }

    }
    public class PrecentAcount
    {
        [DataMapping("PrecentName")]
        public int PrecentName { get; set; }
        [DataMapping("PrecentAcount")]
        public int PrecentAcountchild { get; set; }
    }
    public class TypeAcountClass
    {
        public int MsgTypeName { get; set; }

        public int MsgTypeAcount { get; set; }
    }
}
