﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// 合同审核列表实体类
    /// </summary>
    public class cm_CoperationAuditListView
    {
        #region Model
        private int _cpr_id;
        private int? _cst_id;
        private string _cpr_no;
        private string _cpr_type;
        private string _cpr_type2;
        private string _cpr_name;
        private string _cpr_unit;
        private decimal? _cpr_acount;
        private decimal? _cpr_shijiacount;
        private decimal? _cpr_touzi;
        private decimal? _cpr_shijitouzi;
        private string _cpr_process;
        private DateTime? _cpr_signdate;
        private DateTime? _cpr_donedate;
        private string _cpr_mark;
        private string _buildarea;
        private string _chgpeople;
        private string _chgphone;
        private string _chgjia;
        private string _chgjiaphone;
        private string _buildposition;
        private string _industry;
        private string _buildunit;
        private string _buildsrc;
        private string _tablemaker;
        private DateTime? _regtime;
        private string _updateby;
        private DateTime? _lastupdate;
        private string _buildtype;
        private string _structtype;
        private string _floor;
        private string _buildstructtype;
        private string _multibuild;
        /// <summary>
        /// 
        /// </summary>
        public int cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type
        {
            set { _cpr_type = value; }
            get { return _cpr_type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type2
        {
            set { _cpr_type2 = value; }
            get { return _cpr_type2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Name
        {
            set { _cpr_name = value; }
            get { return _cpr_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Unit
        {
            set { _cpr_unit = value; }
            get { return _cpr_unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Acount
        {
            set { _cpr_acount = value; }
            get { return _cpr_acount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiAcount
        {
            set { _cpr_shijiacount = value; }
            get { return _cpr_shijiacount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Touzi
        {
            set { _cpr_touzi = value; }
            get { return _cpr_touzi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiTouzi
        {
            set { _cpr_shijitouzi = value; }
            get { return _cpr_shijitouzi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Process
        {
            set { _cpr_process = value; }
            get { return _cpr_process; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_SignDate
        {
            set { _cpr_signdate = value; }
            get { return _cpr_signdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_DoneDate
        {
            set { _cpr_donedate = value; }
            get { return _cpr_donedate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Mark
        {
            set { _cpr_mark = value; }
            get { return _cpr_mark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildArea
        {
            set { _buildarea = value; }
            get { return _buildarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPeople
        {
            set { _chgpeople = value; }
            get { return _chgpeople; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPhone
        {
            set { _chgphone = value; }
            get { return _chgphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJia
        {
            set { _chgjia = value; }
            get { return _chgjia; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJiaPhone
        {
            set { _chgjiaphone = value; }
            get { return _chgjiaphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildPosition
        {
            set { _buildposition = value; }
            get { return _buildposition; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Industry
        {
            set { _industry = value; }
            get { return _industry; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildUnit
        {
            set { _buildunit = value; }
            get { return _buildunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildSrc
        {
            set { _buildsrc = value; }
            get { return _buildsrc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TableMaker
        {
            set { _tablemaker = value; }
            get { return _tablemaker; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RegTime
        {
            set { _regtime = value; }
            get { return _regtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }

        /// <summary>
        /// 合同审核记录SysNo
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        /// <summary>
        /// 审核部门
        /// </summary>
        public string AuditDepartment
        {
            get
            {
                string str = "";
                switch (AuditStatus)
                {
                    case "A":
                    case "C":
                        str = "承接部门";
                        break;
                    case "B":
                    case "E":
                        str = "经营处";
                        break;
                    case "D":
                    case "G":
                        str = "技术部门";
                        break;
                    case "H":
                    case "I":
                    case "F":
                        str = "总经理";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }

        /// <summary>
        /// 审核状态图
        /// </summary>
        public string AuditStatusImage
        {
            get
            {
                string str = string.Empty;
                switch (AuditStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        str = "评审中";
                        break;
                    case "H":
                        str = "评审通过";
                        break;
                    case "I":
                        str = "评审拒接";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }
        public string Percent
        {
            get
            {
                int length = 5, percent = 0;
                if (CoperationAuditEntity != null)
                {
                    if (CoperationAuditEntity.ManageLevel == 1) length--;
                    if (CoperationAuditEntity.NeedLegalAdviser == 0) length--;

                    switch (CoperationAuditEntity.Status)
                    {
                        case "A":
                            percent = 0;
                            break;
                        case "B":
                        case "C":
                            percent = (100 / length) * 1;
                            break;
                        case "D":
                        case "E":
                            percent = (100 / length) * 2;
                            break;
                        case "F":
                        case "G":
                            percent = (100 / length) * 3;
                            break;
                        case "H":
                        case "I":
                            if (CoperationAuditEntity.ManageLevel == 1)
                            {
                                percent = 100;
                            }
                            else
                            {
                                if (CoperationAuditEntity.NeedLegalAdviser == 0)
                                {
                                    percent = (100 / length) * 3;
                                }
                                else
                                {
                                    percent = (100 / length) * 4;
                                }
                            }
                            break;
                        case "J":
                        case "K":
                            percent = 100;
                            break;
                    }
                }
                return percent.ToString("f2");
            }
        }

        /// <summary>
        /// 可执行操作
        /// </summary>
        public string ActionLink
        {
            get
            {
                string actionLinkString = string.Empty;
                switch (AuditStatus)
                {
                    case "":
                    case null:
                    case "C":
                    case "E":
                    case "G":
                    case "K":
                    case "I":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" coperationSysNo=\"" + cpr_Id + "\">发起审批</span>";
                        break;
                    case "A":
                    case "B":
                    case "D":
                    case "F":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" coperationSysNo=\"" + cpr_Id + "\">查看</span>";
                        break;
                    case "H":
                        if (CoperationAuditEntity.ManageLevel == 1)
                        {
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" coperationSysNo=\"" + cpr_Id + "\" coperationAuditSysNo=\"" + AuditRecordSysNo + "\">评审结果</span>";
                        }
                        else
                        {
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" coperationSysNo=\"" + cpr_Id + "\">查看</span>";
                        }
                        break;
                    case "J":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" coperationSysNo=\"" + cpr_Id + "\" coperationAuditSysNo=\"" + AuditRecordSysNo + "\">评审结果</span>";
                        break;
                }
                return actionLinkString;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildType
        {
            set { _buildtype = value; }
            get { return _buildtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string StructType
        {
            set { _structtype = value; }
            get { return _structtype; }
        }
        public string StructTypeString
        {
            get
            {
                if (!string.IsNullOrEmpty(StructType))
                {
                    return TG.Common.StringPlus.ResolveStructString(StructType);
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Floor
        {
            set { _floor = value; }
            get { return _floor; }
        }

        public string FloorString
        {
            get
            {
                if (Floor.IndexOf("|") > -1)
                {
                    return "地上：" + Floor.Split('|')[0].ToString() + "层 " + "地下：" + Floor.Split('|')[1].ToString() + "层";
                }
                else
                {
                    return "";
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildStructType
        {
            set { _buildstructtype = value; }
            get { return _buildstructtype; }
        }
        public string BuildStructTypeString
        {
            get
            {
                if (!string.IsNullOrEmpty(BuildStructType))
                {
                    return TG.Common.StringPlus.ResolveStructString(BuildStructType);
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MultiBuild
        {
            set { _multibuild = value; }
            get { return _multibuild; }
        }
        public cm_CoperationAudit CoperationAuditEntity { get; set; }
        #endregion Model
    }

    public class cm_CoperationAuditEditListView
    {
        #region Model
        private int _cpr_id;
        private int? _cst_id;
        private string _cpr_no;
        private string _cpr_type;
        private string _cpr_type2;
        private string _cpr_name;
        private string _cpr_unit;
        private decimal? _cpr_acount;
        private decimal? _cpr_shijiacount;
        private decimal? _cpr_touzi;
        private decimal? _cpr_shijitouzi;
        private string _cpr_process;
        private DateTime? _cpr_signdate;
        private DateTime? _cpr_donedate;
        private string _cpr_mark;
        private string _buildarea;
        private string _chgpeople;
        private string _chgphone;
        private string _chgjia;
        private string _chgjiaphone;
        private string _buildposition;
        private string _industry;
        private string _buildunit;
        private string _buildsrc;
        private string _tablemaker;
        private DateTime? _regtime;
        private string _updateby;
        private DateTime? _lastupdate;
        private string _buildtype;
        private string _structtype;
        private string _floor;
        private string _buildstructtype;
        private string _multibuild;
        /// <summary>
        /// 
        /// </summary>
        public int cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type
        {
            set { _cpr_type = value; }
            get { return _cpr_type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type2
        {
            set { _cpr_type2 = value; }
            get { return _cpr_type2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Name
        {
            set { _cpr_name = value; }
            get { return _cpr_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Unit
        {
            set { _cpr_unit = value; }
            get { return _cpr_unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Acount
        {
            set { _cpr_acount = value; }
            get { return _cpr_acount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiAcount
        {
            set { _cpr_shijiacount = value; }
            get { return _cpr_shijiacount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Touzi
        {
            set { _cpr_touzi = value; }
            get { return _cpr_touzi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiTouzi
        {
            set { _cpr_shijitouzi = value; }
            get { return _cpr_shijitouzi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Process
        {
            set { _cpr_process = value; }
            get { return _cpr_process; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_SignDate
        {
            set { _cpr_signdate = value; }
            get { return _cpr_signdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_DoneDate
        {
            set { _cpr_donedate = value; }
            get { return _cpr_donedate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Mark
        {
            set { _cpr_mark = value; }
            get { return _cpr_mark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildArea
        {
            set { _buildarea = value; }
            get { return _buildarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPeople
        {
            set { _chgpeople = value; }
            get { return _chgpeople; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPhone
        {
            set { _chgphone = value; }
            get { return _chgphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJia
        {
            set { _chgjia = value; }
            get { return _chgjia; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJiaPhone
        {
            set { _chgjiaphone = value; }
            get { return _chgjiaphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildPosition
        {
            set { _buildposition = value; }
            get { return _buildposition; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Industry
        {
            set { _industry = value; }
            get { return _industry; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildUnit
        {
            set { _buildunit = value; }
            get { return _buildunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildSrc
        {
            set { _buildsrc = value; }
            get { return _buildsrc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TableMaker
        {
            set { _tablemaker = value; }
            get { return _tablemaker; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RegTime
        {
            set { _regtime = value; }
            get { return _regtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }

        /// <summary>
        /// 合同审核记录SysNo
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        /// <summary>
        /// 审核部门
        /// </summary>
        public string AuditDepartment
        {
            get
            {
                string str = "";
                switch (AuditStatus)
                {
                    case "A":
                    case "C":
                        str = "承接部门";
                        break;
                    case "B":
                    case "E":
                        str = "经营处";
                        break;
                    case "D":
                    case "G":
                        str = "技术部门";
                        break;
                    case "H":
                    case "I":
                    case "F":
                        str = "总经理";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }

        /// <summary>
        /// 审核状态图
        /// </summary>
        public string AuditStatusImage
        {
            get
            {
                string str = string.Empty;
                switch (AuditStatus)
                {
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        str = "评审中";
                        break;
                    case "H":
                        str = "评审通过";
                        break;
                    case "I":
                        str = "评审拒接";
                        break;
                    default: str = "未提交申请";
                        break;

                }
                return str;
            }
        }
        public string Percent
        {
            get
            {
                int length = 5, percent = 0;
                if (CoperationAuditEntity != null)
                {
                    if (CoperationAuditEntity.ManageLevel == 1) length--;
                    if (CoperationAuditEntity.NeedLegalAdviser == 0) length--;

                    switch (CoperationAuditEntity.Status)
                    {
                        case "A":
                            percent = 0;
                            break;
                        case "B":
                        case "C":
                            percent = (100 / length) * 1;
                            break;
                        case "D":
                        case "E":
                            percent = (100 / length) * 2;
                            break;
                        case "F":
                        case "G":
                            percent = (100 / length) * 3;
                            break;
                        case "H":
                        case "I":
                            if (CoperationAuditEntity.ManageLevel == 1)
                            {
                                percent = 100;
                            }
                            else
                            {
                                if (CoperationAuditEntity.NeedLegalAdviser == 0)
                                {
                                    percent = (100 / length) * 3;
                                }
                                else
                                {
                                    percent = (100 / length) * 4;
                                }
                            }
                            break;
                        case "J":
                        case "K":
                            percent = 100;
                            break;
                    }
                }
                return percent.ToString("f2");
            }
        }

        /// <summary>
        /// 可执行操作
        /// </summary>
        public string ActionLink
        {
            get
            {
                string actionLinkString = string.Empty;
                switch (AuditStatus)
                {
                    case "":
                    case null:
                    case "C":
                    case "E":
                    case "G":
                    case "K":
                    case "I":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" coperationSysNo=\"" + cpr_Id + "\">修改申请</span>";
                        break;
                    case "A":
                    case "B":
                    case "D":
                    case "F":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" coperationSysNo=\"" + cpr_Id + "\">查看</span>";
                        break;
                    case "H":
                        if (CoperationAuditEntity.ManageLevel == 1)
                        {
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" coperationSysNo=\"" + cpr_Id + "\">修改申请</span>";
                        }
                        else
                        {
                            actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" coperationSysNo=\"" + cpr_Id + "\">查看</span>";
                        }
                        break;
                    case "J":
                        actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" coperationSysNo=\"" + cpr_Id + "\">修改申请</span>";
                        break;
                }
                return actionLinkString;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildType
        {
            set { _buildtype = value; }
            get { return _buildtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string StructType
        {
            set { _structtype = value; }
            get { return _structtype; }
        }
        public string StructTypeString
        {
            get
            {
                if (!string.IsNullOrEmpty(StructType))
                {
                    return TG.Common.StringPlus.ResolveStructString(StructType);
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Floor
        {
            set { _floor = value; }
            get { return _floor; }
        }

        public string FloorString
        {
            get
            {
                if (Floor.IndexOf("|") > -1)
                {
                    return "地上：" + Floor.Split('|')[0].ToString() + "层 " + "地下：" + Floor.Split('|')[1].ToString() + "层";
                }
                else
                {
                    return "";
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildStructType
        {
            set { _buildstructtype = value; }
            get { return _buildstructtype; }
        }
        public string BuildStructTypeString
        {
            get
            {
                if (!string.IsNullOrEmpty(BuildStructType))
                {
                    return TG.Common.StringPlus.ResolveStructString(BuildStructType);
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MultiBuild
        {
            set { _multibuild = value; }
            get { return _multibuild; }
        }
        public cm_CoperationAuditEdit CoperationAuditEntity { get; set; }
        #endregion Model
    }
}
