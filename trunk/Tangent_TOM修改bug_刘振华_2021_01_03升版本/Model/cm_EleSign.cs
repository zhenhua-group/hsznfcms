﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_EleSign:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_EleSign
	{
		public cm_EleSign()
		{}
		#region Model
		private int _id;
		private int _mem_id;
        private string _elesign_min;
		private string _elesign;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string elesign_min
        {
            set { _elesign_min = value; }
            get { return _elesign_min; }
        }
		/// <summary>
		/// 
		/// </summary>
		public string elesign
		{
			set{ _elesign=value;}
			get{return _elesign;}
		}
		#endregion Model

	}
}

