﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class SysMessageQueryEntity
    {
        public int UserSysNo { get; set; }

        public string MessageStatus { get; set; }

        public string MessageType { get; set; }

        /// <summary>
        /// QueryCondition
        /// </summary>
        public string QueryCondition { get; set; }

        public string MessageDone { get; set; }

        public string MessageDoneStatus { get; set; }
    }

    /// <summary>
    /// 系统消息类。Sago修改后。2012-9-27
    /// </summary>
    public class SysMessageViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("FromUser")]
        public int FromUser { get; set; }

        [DataMapping("ToRole")]
        public string ToRole { get; set; }

        [DataMapping("MsgType")]
        public int MsgType { get; set; }

        [DataMapping("ReferenceSysNo")]
        public string ReferenceSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("FromUserName")]
        public string FromUserName { get; set; }

        [DataMapping("IsDone")]
        public string IsDone { get; set; }
        /// <summary>
        /// 消息提示内容
        /// </summary>
        [DataMapping("MessageContent")]
        public string MessageContent { get; set; }

        /// <summary>
        /// 查询条件
        /// </summary>
        public string QueryCondition { get; set; }

        public string ExtendField { get; set; }

        public string RoleUserName { get; set; }

        /// <summary>
        /// 消息类型String
        /// </summary>
        public string MsgTypeString
        {
            get
            {
                string msgTypeString = "";
                switch (MsgType)
                {
                    case 1: msgTypeString = "合同审核"; break;
                    case 2: msgTypeString = "项目审核"; break;
                    case 3: msgTypeString = "工号申请"; break;
                    case 4: msgTypeString = "策划审核"; break;
                    case 5: msgTypeString = "产值分配"; break;
                    case 6: msgTypeString = "合同入账"; break;
                    case 7: msgTypeString = "项目入账"; break;
                    case 8: msgTypeString = "项目策划"; break;
                    case 9: msgTypeString = "工程出图"; break;
                    case 10: msgTypeString = "个人分配明细"; break;
                    case 11: msgTypeString = "项目修改审核"; break;
                    case 12: msgTypeString = "合同修改审核"; break;
                    case 13: msgTypeString = "策划修改审核"; break;
                    case 14: msgTypeString = "二次分配审核"; break;
                    case 15: msgTypeString = "暖通所分配"; break;
                    case 16: msgTypeString = "转经济所分配"; break;
                    case 17: msgTypeString = "分配系数审核"; break;
                    case 18: msgTypeString = "经济所分配"; break;
                    case 19: msgTypeString = "转暖通所分配"; break;
                    case 20: msgTypeString = "转土建所分配"; break;
                    case 21: msgTypeString = "工程设计出图卡"; break;
                    case 22: msgTypeString = "工程设计归档资料"; break;
                }
                return msgTypeString;
            }
        }

        public string MsgStatusString
        {
            get
            {
                string msgStatusString = "";
                switch (Status)
                {
                    case "A": msgStatusString = "<img src=\"/Images/unread.gif\" align=\"center\"> 未读"; break;
                    case "D": msgStatusString = "<img src=\"/Images/read.gif\" align=\"center\">已读"; break;
                }
                return msgStatusString;
            }
        }

        public string InDateString
        {
            get
            {
                return InDate.ToString("yyyy-MM-dd");
            }
        }

        /// <summary>
        /// 页面跳转路径
        /// </summary>
        public string RedirectURL
        {
            get
            {
                string redirectString = "";
                switch (MsgType)
                {
                    //合同审核
                    case 1:
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "cpr_CoperationAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        }
                        else
                        {
                            redirectString = "cpr_CoperationAudit.aspx?MsgNo=" + SysNo + "&CoperationAuditSysNo=" + ReferenceSysNo;
                        }
                        break;
                    //项目审核
                    case 2:
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "../ProjectManage/ProjectAudit/ProjectAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        }
                        else
                        {
                            redirectString = "../ProjectManage/ProjectAudit/ProjectAudit.aspx?MsgNo=" + SysNo + "&projectAuditSysNo=" + ReferenceSysNo;
                        }
                        break;
                    //工号申请
                    case 3:
                        redirectString = "/ProjectNumber/ProNumberAllot.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    //审核策划
                    case 4:
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "/ProjectPlan/ProjectPlanAudit/ProPlanAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        }
                        else
                        {
                            redirectString = "/ProjectPlan/ProjectPlanAudit/ProPlanAudit.aspx?MsgNo=" + SysNo + "&projectPlanAuditSysNo=" + ReferenceSysNo;
                        }
                        break;
                    //产值分配
                    case 5:
                        // xiugai
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "/ProjectValueandAllot/ProValueAllotAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        }
                        else
                        {
                            redirectString = "/ProjectValueandAllot/ProjectIsEdit.aspx?ValueAllotAuditSysNo=" + ReferenceSysNo;
                        }
                        break;
                    //入账确认
                    case 6:
                        redirectString = "/Coperation/AccountedForConfirmation.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    //项目收费入账确认
                    case 7:
                        redirectString = "/Coperation/ProjChgWin/ProjectAcountConfirm.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    //项目完成立项发送消息给项目总负责
                    case 8:
                        redirectString = "../ProjectPlan/ProPlanList.aspx?" + "projectSysNo=" + ReferenceSysNo + "&PMUserID=" + FromUser + "&MsgNo=" + SysNo;
                        break;
                    //工程出图
                    case 9:
                        //  xiugai
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "/ProjectManage/ProImageAudit/ProImageAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        }
                        else
                        {
                            redirectString = "/ProjectManage/ProImageAudit/ProImageAudit.aspx?projectImaAuditSysNo=" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        }
                        break;
                    case 10:
                        redirectString = "../ProjectValueandAllot/ShowUserValueDatail.aspx?" + ReferenceSysNo;
                        break;
                    case 17:
                        redirectString = "../ProjectValueandAllot/ShowMsgValueEdit.aspx?" + ReferenceSysNo;
                        break;
                    //项目审核
                    case 11:
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "../ProjectManage/ProjectAudit/ProjectAuditEdit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&msyType=11";
                        }
                        else
                        {
                            redirectString = "../ProjectManage/ProjectAudit/ProjectAuditEdit.aspx?msyType=11&MsgNo=" + SysNo + "&projectAuditSysNo=" + ReferenceSysNo;
                        }
                        break;
                    //合同审核
                    case 12:
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "cpr_CoperationAuditEdit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&msyType=12";
                        }
                        else
                        {
                            redirectString = "cpr_CoperationAuditEdit.aspx?msyType=12&MsgNo=" + SysNo + "&CoperationAuditSysNo=" + ReferenceSysNo;
                        }
                        break;
                    case 13:
                        if (ReferenceSysNo.Contains("&"))
                        {
                            redirectString = "/ProjectPlan/ProjectPlanAudit/ProPlanAuditEdit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&msyType=13";
                        }
                        else
                        {
                            redirectString = "/ProjectPlan/ProjectPlanAudit/ProPlanAuditEdit.aspx?msyType=13&MsgNo=" + SysNo + "&projectPlanAuditSysNo=" + ReferenceSysNo;
                        }
                        break;
                    case 14:
                        // xiugai
                        redirectString = "/ProjectValueandAllot/ProSecondValueAllotAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    case 15:
                        // xiugai
                        redirectString = "/ProjectValueandAllot/HavcProjectValueAllotAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    case 16:
                        // xiugai
                        redirectString = "/ProjectValueandAllot/TranjjsProjectValueAllotAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    case 18:
                        // xiugai
                        redirectString = "/ProjectValueandAllot/ShowjjsProjectValueAllot.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    case 19:
                        // xiugai
                        redirectString = "/ProjectValueandAllot/ShowTranProjectValueandAllot.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    case 20:
                        // xiugai
                        redirectString = "/ProjectValueandAllot/TranBulidingProjectValueAllotAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    case 21:
                        // xiugai
                        redirectString = "/ProjectManage/ProImageAudit/ProjectDesignCardsAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                    case 22:
                        // xiugai
                        redirectString = "/ProjectManage/ProImageAudit/ProjectFileInfoAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
                        break;
                }
                return redirectString;
            }
        }
    }
}
