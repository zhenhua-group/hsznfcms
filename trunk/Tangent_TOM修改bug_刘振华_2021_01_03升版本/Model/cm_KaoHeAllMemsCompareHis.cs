﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeAllMemsCompareHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeAllMemsCompareHis
	{
		public cm_KaoHeAllMemsCompareHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _unitname;
		private string _username;
		private string _isfired;
		private decimal? _jjhj=0M;
		private decimal? _pjygz=0M;
		private decimal? _pjygzbs=0M;
		private decimal? _pjygzpm=0M;
		private decimal? _bmnhppm=0M;
		private decimal? _sbn=0M;
		private decimal? _sbnzf=0M;
		private decimal? _xbn=0M;
		private decimal? _xbnzf=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IsFired
		{
			set{ _isfired=value;}
			get{return _isfired;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JJHJ
		{
			set{ _jjhj=value;}
			get{return _jjhj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PJYGZ
		{
			set{ _pjygz=value;}
			get{return _pjygz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PJYGZBS
		{
			set{ _pjygzbs=value;}
			get{return _pjygzbs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PJYGZPM
		{
			set{ _pjygzpm=value;}
			get{return _pjygzpm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMNHPPM
		{
			set{ _bmnhppm=value;}
			get{return _bmnhppm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SBN
		{
			set{ _sbn=value;}
			get{return _sbn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SBNZF
		{
			set{ _sbnzf=value;}
			get{return _sbnzf;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XBN
		{
			set{ _xbn=value;}
			get{return _xbn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XBNZF
		{
			set{ _xbnzf=value;}
			get{return _xbnzf;}
		}
		#endregion Model

	}
}

