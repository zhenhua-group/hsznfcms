﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cm_ProjectPlanAuditQueryEntity
    {
        public int ProjectPlanAuditSysNo { get; set; }

        public int ProjectSysNo { get; set; }
    }
    public class cm_ProjectPlanEditRecordEntityQueryEntity
    {
        public int ProjectPlanEditRecordSysNo { get; set; }

        public int ProjectSysNo { get; set; }
    }
    public class cm_ProjectPlanAuditEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }

        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        public string[] SuggestionArray
        {
            get
            {
                if (!string.IsNullOrEmpty(Suggestion))
                {
                    return Suggestion.Substring(0, Suggestion.Length - 1).Split('|');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditUserArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditUser))
                {
                    return AuditUser.Substring(0, AuditUser.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditDateArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditDate))
                {
                    return AuditDate.Substring(0, AuditDate.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }
    }
    public class cm_ProjectPlanEditRecordEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("Status")]
        public string Status { get; set; }

        [DataMapping("optionsedit")]
        public string optionsedit { get; set; }

        [DataMapping("Suggestion")]
        public string Suggestion { get; set; }

        [DataMapping("AuditUser")]
        public string AuditUser { get; set; }

        [DataMapping("AuditDate")]
        public string AuditDate { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        public string[] SuggestionArray
        {
            get
            {
                if (!string.IsNullOrEmpty(Suggestion))
                {
                    return Suggestion.Substring(0, Suggestion.Length - 1).Split('|');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditUserArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditUser))
                {
                    return AuditUser.Substring(0, AuditUser.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }

        public string[] AuditDateArray
        {
            get
            {
                if (!string.IsNullOrEmpty(AuditDate))
                {
                    return AuditDate.Substring(0, AuditDate.Length - 1).Split(',');
                }
                else
                {
                    return null;
                }
            }
        }
    }
    [Serializable]
    public class ProjectPlanAuditViewParameterEntity
    {
        public int ProjectPlanAuditSysNo { get; set; }

        public string Suggestion { get; set; }

        public string Status { get; set; }

        public string CoperationName { get; set; }
    }
    [Serializable]
    public class ProjectPlanEditRecordViewParameterEntity
    {
        public int ProjectPlanAuditSysNo { get; set; }

        public string optionsedit { get; set; }

        public string Suggestion { get; set; }

        public string Status { get; set; }

        public string CoperationName { get; set; }
    }

    public class ProjectPlanSubItem
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("DesignLevel")]
        public string DesignLevel { get; set; }

        [DataMapping("StartDate")]
        public DateTime StartDate { get; set; }

        [DataMapping("EndDate")]
        public DateTime EndDate { get; set; }

        [DataMapping("BuildingCount")]
        public int BuildingCount { get; set; }

        [DataMapping("StructCount")]
        public int StructCount { get; set; }

        [DataMapping("ElectricCount")]
        public int ElectricCount { get; set; }

        [DataMapping("HVACCount")]
        public int HVACCount { get; set; }

        [DataMapping("DrainCount")]
        public int DrainCount { get; set; }

        [DataMapping("MunicipalAdministrationCount")]
        public int MunicipalAdministrationCount { get; set; }

        public string PaperObjString
        {
            get
            {
                return "{\"BuildingCount\" : " + BuildingCount + " ,\"StructCount\" : " + StructCount + " ,\"ElectricCount\" : " + ElectricCount + " ,\"HVACCount\" : " + HVACCount + ",\"DrainCount\" : " + DrainCount + ",\"MunicipalAdministrationCount\":" + MunicipalAdministrationCount + "}";
            }
        }

        public string StartDateString
        {
            get
            {
                return StartDate.ToString("yyyy-MM-dd");
            }
        }

        public string EndDateString
        {
            get
            {
                return EndDate.ToString("yyyy-MM-dd");
            }
        }

        public int SumPaper
        {
            get
            {
                return BuildingCount + StructCount + ElectricCount + HVACCount + DrainCount + MunicipalAdministrationCount;
            }
        }


    }

    public class ProjectPlanViewParameterEntity
    {
        [DataMapping("CoperationName")]
        public string CoperationName { get; set; }

        [DataMapping("JoBNumber")]
        public string JobNumber { get; set; }
    }
}
