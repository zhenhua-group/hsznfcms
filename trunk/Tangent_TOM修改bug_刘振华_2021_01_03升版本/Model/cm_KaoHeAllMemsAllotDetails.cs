﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeAllMemsAllotDetails:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeAllMemsAllotDetails
	{
		public cm_KaoHeAllMemsAllotDetails()
		{}
		#region Model
		private int _id;
		private int _renwuid;
		private int _unitid;
		private string _unitname;
		private int _memid;
		private string _memname;
		private int? _isfired=0;
		private decimal? _bmjl=0M;
		private decimal? _gstz=0M;
		private decimal? _yfjj=0M;
		private decimal? _xmjjjs=0M;
		private decimal? _bmnjjjs=0M;
		private decimal? _xmjj=0M;
		private decimal? _bmnjj=0M;
		private decimal? _xmglj=0M;
		private decimal? _yxyg=0M;
		private decimal? _tcgx=0M;
		private decimal? _bimjj=0M;
		private decimal? _ghbt=0M;
		private decimal? _hj=0M;
        private decimal? _zcfy = 0M; //注册费用
		private int? _insertuserid=0;
		private DateTime? _insertdate= DateTime.Now;
		private int? _stat=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}

        public decimal? zcfy
        {
            set { _zcfy = value; }
            get { return _zcfy; }
        }


		/// <summary>
		/// 
		/// </summary>
		public int RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsFired
		{
			set{ _isfired=value;}
			get{return _isfired;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bmjl
		{
			set{ _bmjl=value;}
			get{return _bmjl;}
		}
      
		/// <summary>
		/// 
		/// </summary>
		public decimal? gstz
		{
			set{ _gstz=value;}
			get{return _gstz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? yfjj
		{
			set{ _yfjj=value;}
			get{return _yfjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? xmjjjs
		{
			set{ _xmjjjs=value;}
			get{return _xmjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bmnjjjs
		{
			set{ _bmnjjjs=value;}
			get{return _bmnjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? xmjj
		{
			set{ _xmjj=value;}
			get{return _xmjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bmnjj
		{
			set{ _bmnjj=value;}
			get{return _bmnjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? xmglj
		{
			set{ _xmglj=value;}
			get{return _xmglj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? yxyg
		{
			set{ _yxyg=value;}
			get{return _yxyg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? tcgx
		{
			set{ _tcgx=value;}
			get{return _tcgx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bimjj
		{
			set{ _bimjj=value;}
			get{return _bimjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ghbt
		{
			set{ _ghbt=value;}
			get{return _ghbt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? hj
		{
			set{ _hj=value;}
			get{return _hj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? insertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? insertDate
		{
			set{ _insertdate=value;}
			get{return _insertdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

