﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_SysProjAllotXiShu:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_SysProjAllotXiShu
	{
		public cm_SysProjAllotXiShu()
		{}
		#region Model
		private int _id;
		private string _jianc;
		private string _jiancname;
		private decimal _bili;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jianc
		{
			set{ _jianc=value;}
			get{return _jianc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string jiancName
		{
			set{ _jiancname=value;}
			get{return _jiancname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal bili
		{
			set{ _bili=value;}
			get{return _bili;}
		}
		#endregion Model

	}
}

