﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ChargeAndCoperationRatioExcel
    {
        /// <summary>
        /// 生产部门
        /// </summary>
        [DataMapping("UnitID")]
        public string UnitID { get; set; }
        /// <summary>
        /// 生产部门
        /// </summary>
        [DataMapping("UnitName")]
        public string UnitName { get; set; }
        /// <summary>
        /// 上年目标值
        /// </summary>
        [DataMapping("ChgAcountBeforeM")]
        public string ChgAcountBeforeM { get; set; }
        /// <summary>
        /// 上年月完成
        /// </summary>
        [DataMapping("ChgAcountCurrentM")]
        public string ChgAcountCurrentM { get; set; }
        /// <summary>
        /// 累计完成百分比
        /// </summary>
        [DataMapping("CompareChgVal")]
        public string CompareChgVal { get; set; }
        /// <summary>
        /// 当年目标
        /// </summary>
        [DataMapping("CprAcountBeforeM")]
        public string CprAcountBeforeM { get; set; }
        /// <summary>
        /// 本月完成
        /// </summary>
        [DataMapping("CprAcountCurrentM")]
        public string CprAcountCurrentM { get; set; }
        /// <summary>
        /// 月同比
        /// </summary>
        [DataMapping("CompareCprVal")]
        public string CompareCprVal { get; set; }
        /// <summary>
        /// 收费目标值
        /// </summary>
        public string ChgTargetCurrent { get; set; }
        /// <summary>
        /// 合同目标值
        /// </summary>
        public string CprTargetCurrent { get; set; }
        
    }
}
