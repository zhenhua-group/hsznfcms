﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class ChargeByYear
    {
        /// <summary>
        /// 单位
        /// </summary>

        public string unit_Name { get; set; }
        /// <summary>
        /// 合同目标值
        /// </summary>

        public int UnitAllot { get; set; }
        /// <summary>
        /// 已签合同数
        /// </summary>
        public decimal Count { get; set; }
        /// <summary>
        /// 面积
        /// </summary>

        public decimal BuildArea { get; set; }
        /// <summary>
        /// 合同额
        /// </summary>

        public decimal HeTonge { get; set; }

        /// <summary>
        /// 已完成合同额
        /// </summary>

        public decimal YiWanCHeTonge { get; set; }
        /// <summary>
        /// 合同欠费
        /// </summary>
        public decimal HTQianfei
        {
            get
            {
                return (HeTonge - YiWanCHeTonge);
            }
        }

        /// <summary>
        /// 完成目标值%
        /// </summary>
        public decimal WanChengMBZ
        {
            get
            {
                try
                {
                    return (HeTonge * 100 / UnitAllot);
                }
                catch (Exception)
                {
                    return 0;
                }

            }

        }
        /// <summary>
        /// 完成1#纸张数
        /// </summary>
        public int ZhiZhangShu
        {
            get;
            set;
        }
        /// <summary>
        /// 年度欠款
        /// </summary>
        public decimal QianKuan
        {
            get;
            set;
        }
        /// <summary>
        /// 本年度收费。ND：本年度
        /// </summary>
        public decimal NDShouFei
        {
            get;
            set;
        }
        ///<summary>
        /// 尚欠
        ///</summary>
        public decimal NDShangQian
        {
            get
            {
                return (QianKuan - NDShouFei);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get;
            set;
        }
    }
}
