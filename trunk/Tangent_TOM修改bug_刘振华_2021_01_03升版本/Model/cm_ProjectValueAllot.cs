﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_ProjectValueAllot:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_ProjectValueAllot
    {
        public cm_ProjectValueAllot()
        { }
        #region Model
        private int _id;
        private int? _pro_id;
        private string _allottimes;
        private decimal _allotcount;
        private DateTime? _allotdate;
        private decimal? _persent;
        private decimal? _paidvaluepercent;
        private decimal? _paidvaluecount;
        private decimal? _designmanagerpercent;
        private decimal? _designmanagercount;
        private string _allotvaluepercent;
        private decimal? _allotvaluecount;


        private decimal? _economyvaluepercent;
        private decimal? _economyvaluecount;
        private decimal? _unitvaluepercent;
        private decimal? _unitvaluecount;
        private string _allotuser;
        private string _status;
        private string _mark;
        private int? _itemtype;
        private decimal _otherdeptallotpercent;
        private decimal _otherdeptallotcount;
        private decimal _thedeptallotpercent;
        private decimal _thedeptallotcount;
        private decimal _payshicount;
        private decimal? _havcValuePercent;
        private decimal? _havcValueCount;
        private decimal _programPercent;
        private decimal _programCount;
        private string _secondValue;
        private decimal? _tranBulidingPercent;
        private decimal? _tranBulidingCount;
        private int? unitid;
        private string _actualAllountTime;
        private string _bulidType;
        private decimal? _actualAllotCount;
        private decimal? _loanValueCount;
        private decimal? _financeValuePercent;
        private decimal? _financeValueCount;
        private decimal? _theDeptShouldValuePercent;
        private decimal? _theDeptShouldValueCount;
        private decimal? _dividedPercent;


        public int? UnitId
        {
            get { return unitid; }
            set { unitid = value; }
        }
        /// <summary>
        /// 系统自增ID
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 合同ID
        /// </summary>
        public int? pro_ID
        {
            set { _pro_id = value; }
            get { return _pro_id; }
        }
        /// <summary>
        /// 分配次数
        /// </summary>
        public string AllotTimes
        {
            set { _allottimes = value; }
            get { return _allottimes; }
        }
        /// <summary>
        /// 分配时间
        /// </summary>
        public decimal AllotCount
        {
            set { _allotcount = value; }
            get { return _allotcount; }
        }
        /// <summary>
        /// 分配时间
        /// </summary>
        public DateTime? AllotDate
        {
            set { _allotdate = value; }
            get { return _allotdate; }
        }
        /// <summary>
        /// 分配所占比例
        /// </summary>
        public decimal? persent
        {
            set { _persent = value; }
            get { return _persent; }
        }
        /// <summary>
        /// 实收比例
        /// </summary>
        public decimal? PaidValuePercent
        {
            set { _paidvaluepercent = value; }
            get { return _paidvaluepercent; }
        }
        /// <summary>
        /// 实收金额
        /// </summary>
        public decimal? PaidValueCount
        {
            set { _paidvaluecount = value; }
            get { return _paidvaluecount; }
        }
        /// <summary>
        /// 设总比例
        /// </summary>
        public decimal? DesignManagerPercent
        {
            set { _designmanagerpercent = value; }
            get { return _designmanagerpercent; }
        }
        /// <summary>
        /// 设总金额
        /// </summary>
        public decimal? DesignManagerCount
        {
            set { _designmanagercount = value; }
            get { return _designmanagercount; }
        }
        /// <summary>
        /// 分配产值100%-2
        /// </summary>
        public string AllotValuePercent
        {
            set { _allotvaluepercent = value; }
            get { return _allotvaluepercent; }
        }


        /// <summary>
        /// 分配产值
        /// </summary>
        public decimal? Allotvaluecount
        {
            get { return _allotvaluecount; }
            set { _allotvaluecount = value; }
        }

        /// <summary>
        /// 转经济所比例
        /// </summary>
        public decimal? EconomyValuePercent
        {
            set { _economyvaluepercent = value; }
            get { return _economyvaluepercent; }
        }
        /// <summary>
        /// 转经济所产值
        /// </summary>
        public decimal? EconomyValueCount
        {
            set { _economyvaluecount = value; }
            get { return _economyvaluecount; }
        }
        /// <summary>
        /// 本部门产值合计比例
        /// </summary>
        public decimal? UnitValuePercent
        {
            set { _unitvaluepercent = value; }
            get { return _unitvaluepercent; }
        }
        /// <summary>
        /// 本部门产值合计金额
        /// </summary>
        public decimal? UnitValueCount
        {
            set { _unitvaluecount = value; }
            get { return _unitvaluecount; }
        }
        /// <summary>
        /// 分配人
        /// </summary>
        public string AllotUser
        {
            set { _allotuser = value; }
            get { return _allotuser; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string mark
        {
            set { _mark = value; }
            get { return _mark; }
        }


        public int? Itemtype
        {
            get { return _itemtype; }
            set { _itemtype = value; }
        }

        /// <summary>
        /// 其他部门比例
        /// </summary>
        public decimal Otherdeptallotpercent
        {
            get { return _otherdeptallotpercent; }
            set { _otherdeptallotpercent = value; }
        }
        /// <summary>
        /// 其他部门产值
        /// </summary>
        public decimal Otherdeptallotcount
        {
            get { return _otherdeptallotcount; }
            set { _otherdeptallotcount = value; }
        }

        /// <summary>
        /// 本部门自留产值
        /// </summary>
        public decimal Thedeptallotpercent
        {
            get { return _thedeptallotpercent; }
            set { _thedeptallotpercent = value; }
        }

        /// <summary>
        /// 本部门自留产值金额
        /// </summary>
        public decimal Thedeptallotcount
        {
            get { return _thedeptallotcount; }
            set { _thedeptallotcount = value; }
        }

        /// <summary>
        /// 实收金额
        /// </summary>
        public decimal Payshicount
        {
            get { return _payshicount; }
            set { _payshicount = value; }
        }

        public int AuditSysID { get; set; }

        /// <summary>
        /// 是否转经济所
        /// </summary>
        public string IsTrunEconomy { get; set; }

        /// <summary>
        /// 是否转暖通
        /// </summary>
        public string IsTrunHavc { get; set; }


        /// <summary>
        /// 转暖通比例
        /// </summary>
        public decimal? HavcValuePercent
        {
            get { return _havcValuePercent; }
            set { _havcValuePercent = value; }
        }

        /// <summary>
        /// 转暖通金额
        /// </summary>
        public decimal? HavcValueCount
        {
            get { return _havcValueCount; }
            set { _havcValueCount = value; }
        }

        /// <summary>
        /// 方案比例
        /// </summary>
        public decimal ProgramPercent
        {
            get { return _programPercent; }
            set { _programPercent = value; }
        }

        /// <summary>
        /// 方案金额
        /// </summary>
        public decimal ProgramCount
        {
            get { return _programCount; }
            set { _programCount = value; }
        }

        private decimal _shouldBeValuePercent;

        /// <summary>
        /// 应分产值比例
        /// </summary>
        public decimal ShouldBeValuePercent
        {
            get { return _shouldBeValuePercent; }
            set { _shouldBeValuePercent = value; }
        }

        private decimal _shouldBeValueCount;

        /// <summary>
        /// 应分产值
        /// </summary>
        public decimal ShouldBeValueCount
        {
            get { return _shouldBeValueCount; }
            set { _shouldBeValueCount = value; }
        }

        /// <summary>
        /// 二次产值分配
        /// </summary>
        public string SecondValue
        {
            get { return _secondValue; }
            set { _secondValue = value; }
        }

        public decimal? TranBulidingPercent
        {
            get { return _tranBulidingPercent; }
            set { _tranBulidingPercent = value; }
        }
        public decimal? TranBulidingCount
        {
            get { return _tranBulidingCount; }
            set { _tranBulidingCount = value; }
        }

        /// <summary>
        /// 实际分配年份
        /// </summary>
        public string ActualAllountTime
        {
            get { return _actualAllountTime; }
            set { _actualAllountTime = value; }
        }

        /// <summary>
        /// 建筑分类
        /// </summary>
        public string BulidType
        {
            get { return _bulidType; }
            set { _bulidType = value; }
        }

        /// <summary>
        /// 实际分配产值
        /// </summary>
        public decimal? ActualAllotCount
        {
            get { return _actualAllotCount; }
            set { _actualAllotCount = value; }
        }

        /// <summary>
        /// 借出金额
        /// </summary>
        public decimal? LoanValueCount
        {
            get { return _loanValueCount; }
            set { _loanValueCount = value; }
        }

        /// <summary>
        /// 财务统计产值比例
        /// </summary>
        public decimal? FinanceValuePercent
        {
            get { return _financeValuePercent; }
            set { _financeValuePercent = value; }
        }
        /// <summary>
        /// 财务统计产值
        /// </summary>
        public decimal? FinanceValueCount
        {
            get { return _financeValueCount; }
            set { _financeValueCount = value; }
        }
        /// <summary>
        /// 本部门应留产值比例
        /// </summary>
        public decimal? TheDeptShouldValuePercent
        {
            get { return _theDeptShouldValuePercent; }
            set { _theDeptShouldValuePercent = value; }
        }
        //本部门应留产值
        public decimal? TheDeptShouldValueCount
        {
            get { return _theDeptShouldValueCount; }
            set { _theDeptShouldValueCount = value; }
        }
        /// <summary>
        /// 院所分成比例
        /// </summary>
        public decimal? DividedPercent
        {
            get { return _dividedPercent; }
            set { _dividedPercent = value; }
        }
        #endregion Model

    }
}
