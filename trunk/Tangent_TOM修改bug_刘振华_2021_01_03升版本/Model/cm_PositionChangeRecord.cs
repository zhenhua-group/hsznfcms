﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_PositionChangeRecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_PositionChangeRecord
	{
		public cm_PositionChangeRecord()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private int? _magrole;
		private string _magrolename;
		private int? _magrole2;
		private string _magrolename2;
		private int? _tecrole;
		private string _tecrolename;
		private int? _tecrole2;
		private string _tecrolename2;
		private int? _jiaotong;
		private int? _jiaotong2;
		private int? _tongxun;
		private int? _tongxun2;
		private DateTime? _changedate;
		private int? _insertuserid;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MagRole
		{
			set{ _magrole=value;}
			get{return _magrole;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MagRoleName
		{
			set{ _magrolename=value;}
			get{return _magrolename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MagRole2
		{
			set{ _magrole2=value;}
			get{return _magrole2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MagRoleName2
		{
			set{ _magrolename2=value;}
			get{return _magrolename2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TecRole
		{
			set{ _tecrole=value;}
			get{return _tecrole;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TecRoleName
		{
			set{ _tecrolename=value;}
			get{return _tecrolename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? TecRole2
		{
			set{ _tecrole2=value;}
			get{return _tecrole2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TecRoleName2
		{
			set{ _tecrolename2=value;}
			get{return _tecrolename2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? JiaoTong
		{
			set{ _jiaotong=value;}
			get{return _jiaotong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? JiaoTong2
		{
			set{ _jiaotong2=value;}
			get{return _jiaotong2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Tongxun
		{
			set{ _tongxun=value;}
			get{return _tongxun;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Tongxun2
		{
			set{ _tongxun2=value;}
			get{return _tongxun2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ChangeDate
		{
			set{ _changedate=value;}
			get{return _changedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		#endregion Model

	}
}

