﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeSpecMemsAllot:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeSpecMemsAllot
	{
		public cm_KaoHeSpecMemsAllot()
		{}
		#region Model
		private int _id;
		private int _prokhnameid;
		private int _allotid;
		private int _speid;
		private string _spename;
		private decimal? _persent2=0M;
		private decimal? _jixiao;
		private decimal? _jixiaocount=0M;
		private decimal? _jixiaocountend=0M;
		private decimal? _persentend=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHNameId
		{
			set{ _prokhnameid=value;}
			get{return _prokhnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int AllotID
		{
			set{ _allotid=value;}
			get{return _allotid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeID
		{
			set{ _speid=value;}
			get{return _speid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Persent2
		{
			set{ _persent2=value;}
			get{return _persent2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Jixiao
		{
			set{ _jixiao=value;}
			get{return _jixiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JixiaoCount
		{
			set{ _jixiaocount=value;}
			get{return _jixiaocount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JixiaoCountEnd
		{
			set{ _jixiaocountend=value;}
			get{return _jixiaocountend;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PersentEnd
		{
			set{ _persentend=value;}
			get{return _persentend;}
		}
		#endregion Model

	}
}

