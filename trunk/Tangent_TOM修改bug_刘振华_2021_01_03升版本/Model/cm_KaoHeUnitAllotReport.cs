﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeUnitAllotReport:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeUnitAllotReport
	{
		public cm_KaoHeUnitAllotReport()
		{}
		#region Model
		private int _id;
		private int _renwuid;
		private int _unitid;
		private string _unitname;
		private decimal _avggongzi;
		private decimal _unitjx;
		private decimal _unitjiangbi;
		private decimal _unitjiangall;
		private decimal _monthbei;
		private decimal _projcount;
		private decimal _projcountbi;
		private decimal _unitjiang;
		private int? _stat=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal AvgGongzi
		{
			set{ _avggongzi=value;}
			get{return _avggongzi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Unitjx
		{
			set{ _unitjx=value;}
			get{return _unitjx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Unitjiangbi
		{
			set{ _unitjiangbi=value;}
			get{return _unitjiangbi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Unitjiangall
		{
			set{ _unitjiangall=value;}
			get{return _unitjiangall;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal MonthBei
		{
			set{ _monthbei=value;}
			get{return _monthbei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal ProjCount
		{
			set{ _projcount=value;}
			get{return _projcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal ProjCountbi
		{
			set{ _projcountbi=value;}
			get{return _projcountbi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Unitjiang
		{
			set{ _unitjiang=value;}
			get{return _unitjiang;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

