﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace TG.Model
{
    /// <summary>
    /// 建立项目XML规则文件
    /// </summary>
    [XmlRootAttribute("Project")]
    public class TPMTemplateXMLEntity
    {
        [XmlElement("SubProject")]
        public List<SubProject> SubProjects { get; set; }
    }

    public class SubProject
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("Purpose")]
        public List<Purpose> Purposes { get; set; }
    }

    public class Purpose
    {
        [XmlAttribute("descr")]
        public string Descr { get; set; }

        public string PurposeName
        {
            get
            {
                return DescrArray[0];
            }

        }

        [XmlElement("Folder")]
        public List<Folder> Folders { get; set; }

        [XmlElement("File")]
        public List<File> Files { get; set; }

        [XmlIgnore]
        public string[] DescrArray
        {
            get
            {
                return Descr.Split(',');
            }
        }

        [XmlIgnore]
        public string this[int index]
        {
            get
            {
                return DescrArray[index];
            }
        }
    }

    public class Folder
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
    }

    public class File
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
    }
}
