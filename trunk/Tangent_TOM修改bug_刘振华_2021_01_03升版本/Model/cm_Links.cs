﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_Links:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_Links
	{
		public cm_Links()
		{}
		#region Model
		private int _id;
		private string _linkname;
		private string _linkaddr;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LinkName
		{
			set{ _linkname=value;}
			get{return _linkname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LinkAddr
		{
			set{ _linkaddr=value;}
			get{return _linkaddr;}
		}
		#endregion Model

	}
}

