﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ApplyInfoAudit:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ApplyInfoAudit
	{
		public cm_ApplyInfoAudit()
		{}
		#region Model
		private int _sysno;
		private int _applyid;
		private string _status;
		private int? _unitmanager=0;
		private int? _generalmanager=0;
		private string _audtidate;
		private string _suggestion;
		private DateTime _indate= DateTime.Now;
		private int _inuser;
		/// <summary>
		/// 
		/// </summary>
		public int SysNo
		{
			set{ _sysno=value;}
			get{return _sysno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ApplyID
		{
			set{ _applyid=value;}
			get{return _applyid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Unitmanager
		{
			set{ _unitmanager=value;}
			get{return _unitmanager;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Generalmanager
		{
			set{ _generalmanager=value;}
			get{return _generalmanager;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AudtiDate
		{
			set{ _audtidate=value;}
			get{return _audtidate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Suggestion
		{
			set{ _suggestion=value;}
			get{return _suggestion;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime InDate
		{
			set{ _indate=value;}
			get{return _indate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InUser
		{
			set{ _inuser=value;}
			get{return _inuser;}
		}
		#endregion Model

	}
}

