﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public partial class cm_TranBulidingProjectValueAllot
    {
        public cm_TranBulidingProjectValueAllot()
        { }
        #region Model
        private int _id;
        private int? _pro_id;
        private int? _allotid;
        private decimal? _totalcount;
        private decimal? _thedeptallotpercent;
        private decimal? _thedeptallotcount;
        private decimal? _allotpercent;
        private decimal? _allotcount;
        private decimal? _programpercent;
        private decimal? _programcount;
        private string _itemtype;
        private string _status;
        private DateTime? _allotdate;
        private int? _allotuser;
        private string _actualAllountTime;
        private decimal? _designManagerPercent;
        private decimal? _designManagerCount;
        private string _designUserName;
        private int? _designUserID;

        public string DesignUserName
        {
            get { return _designUserName; }
            set { _designUserName = value; }
        }
      
        public int? DesignUserID
        {
            get { return _designUserID; }
            set { _designUserID = value; }
        }
        public decimal? DesignManagerPercent
        {
            get { return _designManagerPercent; }
            set { _designManagerPercent = value; }
        }
        public decimal? DesignManagerCount
        {
            get { return _designManagerCount; }
            set { _designManagerCount = value; }
        }

        /// <summary>
        /// 实际分配时间
        /// </summary>
        public string ActualAllountTime
        {
            get { return _actualAllountTime; }
            set { _actualAllountTime = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Pro_ID
        {
            set { _pro_id = value; }
            get { return _pro_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AllotID
        {
            set { _allotid = value; }
            get { return _allotid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TotalCount
        {
            set { _totalcount = value; }
            get { return _totalcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Thedeptallotpercent
        {
            set { _thedeptallotpercent = value; }
            get { return _thedeptallotpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Thedeptallotcount
        {
            set { _thedeptallotcount = value; }
            get { return _thedeptallotcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? AllotPercent
        {
            set { _allotpercent = value; }
            get { return _allotpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? AllotCount
        {
            set { _allotcount = value; }
            get { return _allotcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProgramPercent
        {
            set { _programpercent = value; }
            get { return _programpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProgramCount
        {
            set { _programcount = value; }
            get { return _programcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ItemType
        {
            set { _itemtype = value; }
            get { return _itemtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AllotDate
        {
            set { _allotdate = value; }
            get { return _allotdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AllotUser
        {
            set { _allotuser = value; }
            get { return _allotuser; }
        }

        private string lastItemType;

        public string LastItemType
        {
            get { return lastItemType; }
            set { lastItemType = value; }
        }

        private string _mark;

        public string Mark
        {
            get { return _mark; }
            set { _mark = value; }
        }
        #endregion Model

    }
}
