﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_memberRole:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_memberRole
	{
		public tg_memberRole()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private int? _roleidtec=0;
		private int? _roleidmag=0;
		private decimal? _weights=0.00M;
		private int? _mem_unit_id=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_Id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RoleIdTec
		{
			set{ _roleidtec=value;}
			get{return _roleidtec;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RoleIdMag
		{
			set{ _roleidmag=value;}
			get{return _roleidmag;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Weights
		{
			set{ _weights=value;}
			get{return _weights;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Unit_ID
		{
			set{ _mem_unit_id=value;}
			get{return _mem_unit_id;}
		}
		#endregion Model

	}
}

