﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeMemsMngAllot:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeMemsMngAllot
	{
		public cm_KaoHeMemsMngAllot()
		{}
		#region Model
		private int _id;
		private int _renwuid;
		private int _memid;
		private string _memname;
		private int _unitid;
		private string _unitname;
		private decimal? _jbgz=0M;
		private decimal? _gzsj=0M;
		private decimal? _pjgz=0M;
		private decimal? _zphu=0M;
		private decimal? _orderid=0M;
		private decimal? _bmjlpj=0M;
		private decimal? _orderid2=0M;
		private decimal? _jqxs=0M;
		private decimal? _bmnjs=0M;
		private decimal? _xmjjjs=0M;
		private decimal? _xmjjmng=0M;
		private decimal? _xmjjtz=0M;
		private decimal? _yxyg=0M;
		private decimal? _hj=0M;
		private decimal? _beizhu=0M;
		private decimal? _beforyear=0M;
		private decimal? _chazhi=0M;
		private decimal? _beforyear2=0M;
		private decimal? _chazhi2=0M;
		private int _insertuserid;
		private decimal? _weights;
		private int? _stat=0;
		private decimal? _hjyf;
		/// <summary>
		/// 方案部门中为空
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? jbgz
		{
			set{ _jbgz=value;}
			get{return _jbgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? gzsj
		{
			set{ _gzsj=value;}
			get{return _gzsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? pjgz
		{
			set{ _pjgz=value;}
			get{return _pjgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? zphu
		{
			set{ _zphu=value;}
			get{return _zphu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? orderid
		{
			set{ _orderid=value;}
			get{return _orderid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bmjlpj
		{
			set{ _bmjlpj=value;}
			get{return _bmjlpj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? orderid2
		{
			set{ _orderid2=value;}
			get{return _orderid2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? jqxs
		{
			set{ _jqxs=value;}
			get{return _jqxs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bmnjs
		{
			set{ _bmnjs=value;}
			get{return _bmnjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? xmjjjs
		{
			set{ _xmjjjs=value;}
			get{return _xmjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? xmjjmng
		{
			set{ _xmjjmng=value;}
			get{return _xmjjmng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? xmjjtz
		{
			set{ _xmjjtz=value;}
			get{return _xmjjtz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? yxyg
		{
			set{ _yxyg=value;}
			get{return _yxyg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? hj
		{
			set{ _hj=value;}
			get{return _hj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? beizhu
		{
			set{ _beizhu=value;}
			get{return _beizhu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? beforyear
		{
			set{ _beforyear=value;}
			get{return _beforyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? chazhi
		{
			set{ _chazhi=value;}
			get{return _chazhi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? beforyear2
		{
			set{ _beforyear2=value;}
			get{return _beforyear2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? chazhi2
		{
			set{ _chazhi2=value;}
			get{return _chazhi2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int insertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? weights
		{
			set{ _weights=value;}
			get{return _weights;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? hjyf
		{
			set{ _hjyf=value;}
			get{return _hjyf;}
		}
		#endregion Model

	}
}

