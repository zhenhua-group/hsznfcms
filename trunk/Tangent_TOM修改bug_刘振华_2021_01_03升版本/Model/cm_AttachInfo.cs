﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_AttachInfo:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_AttachInfo
    {
        public cm_AttachInfo()
        { }
        #region Model
        private int _id;
        private decimal? _cst_id;
        private decimal? _cpr_id;
        private string _temp_no;
        private string _filename;
        private DateTime? _uploadtime;
        private string _filesize;
        private string _filetype;
        private string _uploaduser;
        private string _fileurl;
        private string _filetypeimg;
        private string _owntype;
        private decimal? _proj_id;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Temp_No
        {
            set { _temp_no = value; }
            get { return _temp_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            set { _filename = value; }
            get { return _filename; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UploadTime
        {
            set { _uploadtime = value; }
            get { return _uploadtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FileSize
        {
            set { _filesize = value; }
            get { return _filesize; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FileType
        {
            set { _filetype = value; }
            get { return _filetype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UploadUser
        {
            set { _uploaduser = value; }
            get { return _uploaduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FileUrl
        {
            set { _fileurl = value; }
            get { return _fileurl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FileTypeImg
        {
            set { _filetypeimg = value; }
            get { return _filetypeimg; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string OwnType
        {
            set { _owntype = value; }
            get { return _owntype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Proj_Id
        {
            set { _proj_id = value; }
            get { return _proj_id; }
        }
        /// <summary>
        /// 上传时间格式化后的字符串
        /// </summary>
        public string UploadTimeString
        {
            get
            {
                return Convert.ToDateTime(UploadTime).ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public string FileSizeString
        {
            get
            {
                return Convert.ToString(int.Parse(FileSize) / 1024) + "KB";
            }
        }
        #endregion Model

    }
}

