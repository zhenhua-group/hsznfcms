﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_IdentityCard:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_IdentityCard
	{
		public cm_IdentityCard()
		{}
		#region Model
		private int _id;
		private int? _cardid;
		private string _cardaddress;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CardID
		{
			set{ _cardid=value;}
			get{return _cardid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CardAddress
		{
			set{ _cardaddress=value;}
			get{return _cardaddress;}
		}
		#endregion Model

	}
}

