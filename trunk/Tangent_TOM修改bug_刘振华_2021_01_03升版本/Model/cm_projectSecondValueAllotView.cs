﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    public class cm_projectSecondValueAllotView
    {
        #region Model
        private int _pro_id;
        private string _pro_name;
        private string _pro_unit;
        private decimal? _theDeptValueCount;
        private decimal? _auditCount;
        private decimal? _designCount;
        private decimal? _havcCount;
        private decimal? _totalCount;
        private decimal? _allotCount;
        private decimal _loanCount;

        public decimal LoanCount
        {
            get { return _loanCount; }
            set { _loanCount = value; }
        }
      
        public decimal? TheDeptValueCount
        {
            get { return _theDeptValueCount; }
            set { _theDeptValueCount = value; }
        }
      
        public decimal? AuditCount
        {
            get { return _auditCount; }
            set { _auditCount = value; }
        }
       
        public decimal? DesignCount
        {
            get { return _designCount; }
            set { _designCount = value; }
        }
        

        public decimal? HavcCount
        {
            get { return _havcCount; }
            set { _havcCount = value; }
        }
       
        public decimal? TotalCount
        {
            get { return _totalCount; }
            set { _totalCount = value; }
        }

        public decimal? AllotCount
        {
            get { return _allotCount; }
            set { _allotCount = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int pro_Id
        {
            set { _pro_id = value; }
            get { return _pro_id; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Pro_Name
        {
            set { _pro_name = value; }
            get { return _pro_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Unit
        {
            set { _pro_unit = value; }
            get { return _pro_unit; }
        }
      
        /// <summary>
        /// 合同审核记录SysNo
        /// </summary>
        public int AuditRecordSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }


        public string Percent
        {
            get
            {
                decimal percent = 0;
                if (proAllotEntity != null)
                {
                    if (TotalCount != 0)
                    {
                        percent = (decimal.Parse(AllotCount.ToString()) / decimal.Parse(TotalCount.ToString())) * 100;
                    }
                }
                return percent.ToString("f2");
            }
        }

        /// <summary>
        /// 可执行操作
        /// </summary>
        public string ActionLink
        {
            get
            {
                string actionLinkString = string.Empty;
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + pro_Id + "\">发起分配</span>";
                return actionLinkString;
            }
        }

        public cm_ProjectValueAuditRecord proAllotEntity { get; set; }
        #endregion Model
    }
}
