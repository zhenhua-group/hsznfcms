﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_CostDetails:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CostDetails
    {
        public cm_CostDetails()
        { }
        #region Model
        private int _id;
        private DateTime? _costtime;
        private string _costnum;
        private string _costsub;
        private decimal? _costcharge;
        private decimal? _costoutcharge;
        private int? _costunit;
        private int? _costuserid;
        private int? _costtypeid;
        private int? _isexpord = 0;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? costTime
        {
            set { _costtime = value; }
            get { return _costtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string costNum
        {
            set { _costnum = value; }
            get { return _costnum; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string costSub
        {
            set { _costsub = value; }
            get { return _costsub; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? costCharge
        {
            set { _costcharge = value; }
            get { return _costcharge; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? costOutCharge
        {
            set { _costoutcharge = value; }
            get { return _costoutcharge; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? costUnit
        {
            set { _costunit = value; }
            get { return _costunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? costUserId
        {
            set { _costuserid = value; }
            get { return _costuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? costTypeID
        {
            set { _costtypeid = value; }
            get { return _costtypeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? isexpord
        {
            set { _isexpord = value; }
            get { return _isexpord; }
        }
        #endregion Model

    }
}


