﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeProjTypeEnum:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeProjTypeEnum
	{
		public cm_KaoHeProjTypeEnum()
		{}
		#region Model
		private int _id;
		private string _typename;
		private decimal _type1;
		private decimal _type2;
		private decimal _type3;
		private decimal _type4;
		private decimal _type5;
		private decimal _type6;
		private decimal _type7;
		private decimal _type8;
		private decimal _type9;
		private decimal _type10;
		private decimal _type11;
		private decimal _jianzhu;
		private decimal _jiegou;
		private decimal _nuantong;
		private decimal _shui;
		private decimal _dianqi;
		private decimal _shinei;
		private decimal _jianzhu2;
		private decimal _shebei;
		private decimal _dianqi2;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TypeName
		{
			set{ _typename=value;}
			get{return _typename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type1
		{
			set{ _type1=value;}
			get{return _type1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type2
		{
			set{ _type2=value;}
			get{return _type2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type3
		{
			set{ _type3=value;}
			get{return _type3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type4
		{
			set{ _type4=value;}
			get{return _type4;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type5
		{
			set{ _type5=value;}
			get{return _type5;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type6
		{
			set{ _type6=value;}
			get{return _type6;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type7
		{
			set{ _type7=value;}
			get{return _type7;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type8
		{
			set{ _type8=value;}
			get{return _type8;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type9
		{
			set{ _type9=value;}
			get{return _type9;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type10
		{
			set{ _type10=value;}
			get{return _type10;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Type11
		{
			set{ _type11=value;}
			get{return _type11;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Jianzhu
		{
			set{ _jianzhu=value;}
			get{return _jianzhu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Jiegou
		{
			set{ _jiegou=value;}
			get{return _jiegou;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Nuantong
		{
			set{ _nuantong=value;}
			get{return _nuantong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Shui
		{
			set{ _shui=value;}
			get{return _shui;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Dianqi
		{
			set{ _dianqi=value;}
			get{return _dianqi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Shinei
		{
			set{ _shinei=value;}
			get{return _shinei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Jianzhu2
		{
			set{ _jianzhu2=value;}
			get{return _jianzhu2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Shebei
		{
			set{ _shebei=value;}
			get{return _shebei;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Dianqi2
		{
			set{ _dianqi2=value;}
			get{return _dianqi2;}
		}
		#endregion Model

	}
}

