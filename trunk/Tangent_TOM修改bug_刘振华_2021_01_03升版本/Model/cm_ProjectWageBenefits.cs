﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public partial class cm_ProjectWageBenefits
    {
        public cm_ProjectWageBenefits()
        { }
        #region Model
        private int _id;
        private int? _mem_id;
        private string _valuepercent;
        private decimal? _valuewage;
        private decimal? _possessionmemwange;
        private decimal? _yearwange;
        private string _remunerationpercent;
        private decimal? _remunerationwange;
        private string _taxpercent;
        private decimal? _yeartaxcount;
        private decimal? _shouldwange;
        private string _yearwang;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? mem_id
        {
            set { _mem_id = value; }
            get { return _mem_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ValuePercent
        {
            set { _valuepercent = value; }
            get { return _valuepercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ValueWage
        {
            set { _valuewage = value; }
            get { return _valuewage; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? PossessionMemWange
        {
            set { _possessionmemwange = value; }
            get { return _possessionmemwange; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? YearWange
        {
            set { _yearwange = value; }
            get { return _yearwange; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RemunerationPercent
        {
            set { _remunerationpercent = value; }
            get { return _remunerationpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? RemunerationWange
        {
            set { _remunerationwange = value; }
            get { return _remunerationwange; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TaxPercent
        {
            set { _taxpercent = value; }
            get { return _taxpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? YearTaxCount
        {
            set { _yeartaxcount = value; }
            get { return _yeartaxcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ShouldWange
        {
            set { _shouldwange = value; }
            get { return _shouldwange; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string yearWang
        {
            set { _yearwang = value; }
            get { return _yearwang; }
        }
        #endregion Model

    }
}
