﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
	/// <summary>
	/// 前台用参数载体
	/// </summary>
	public class AuditConfigEntity
	{
		/// <summary>
		/// 审核配置记录SysNo
		/// </summary>
		public int AuditConfigSysNo { get; set; }

		/// <summary>
		/// 角色SysNo
		/// </summary>
		public int RoleSysNo { get; set; }
	}

	/// <summary>
	/// 分页共同类
	/// </summary>
	[Serializable]
	public class SagoPager
	{
		public int PageSize { get; set; }

		public int PageCurrent { get; set; }

	}

	/// <summary>
	/// 前台用返回实体
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public class ResultEntityList<T>
	{
		public ResultEntityList() { }

		public ResultEntityList(int pageSize)
		{
			this.PageSize = pageSize;
			Body = new List<T>();
		}

		public int TotalCount { get; set; }

		public int PageSize { get; set; }

		public int PageCount
		{
			get
			{
				int pageCount = 1;
				if (TotalCount != 0)
				{
					pageCount = TotalCount / PageSize;
					if (TotalCount % PageSize != 0)
					{
						pageCount++;
					}
				}
				return pageCount;
			}
		}

		public List<T> Body { get; set; }
	}
}
