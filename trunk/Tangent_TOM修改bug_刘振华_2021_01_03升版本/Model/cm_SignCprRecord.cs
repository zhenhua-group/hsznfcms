﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_SignCprRecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_SignCprRecord
	{
		public cm_SignCprRecord()
		{}
		#region Model
		private int _id;
		private int? _mem_id;
		private DateTime? _mem_coperationdate;
		private int? _mem_coperationtype;
		private DateTime? _mem_coperationdateend;
		private string _mem_coperationsub;
		private int? _insertuserid;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_CoperationDate
		{
			set{ _mem_coperationdate=value;}
			get{return _mem_coperationdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_CoperationType
		{
			set{ _mem_coperationtype=value;}
			get{return _mem_coperationtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_CoperationDateEnd
		{
			set{ _mem_coperationdateend=value;}
			get{return _mem_coperationdateend;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_CoperationSub
		{
			set{ _mem_coperationsub=value;}
			get{return _mem_coperationsub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? insertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		#endregion Model

	}
}

