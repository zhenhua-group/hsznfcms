﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_unitExt:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_unitExt
	{
		public tg_unitExt()
		{}
		#region Model
		private int _unit_id;
		private string _unit_name;
		private int _unit_type;
		private int? _unit_order=0;
		/// <summary>
		/// 
		/// </summary>
		public int unit_ID
		{
			set{ _unit_id=value;}
			get{return _unit_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string unit_Name
		{
			set{ _unit_name=value;}
			get{return _unit_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int unit_Type
		{
			set{ _unit_type=value;}
			get{return _unit_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? unit_Order
		{
			set{ _unit_order=value;}
			get{return _unit_order;}
		}
		#endregion Model

	}
}

