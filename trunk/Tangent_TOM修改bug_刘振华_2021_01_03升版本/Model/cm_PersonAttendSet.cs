﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_PersonAttendSet:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_PersonAttendSet
	{
		public cm_PersonAttendSet()
		{}
		#region Model
		private int _attend_id;
		private int _mem_id;
		private int _attend_month;
		private string _towork;
		private string _offwork;
        private int _attend_year;
		/// <summary>
		/// 
		/// </summary>
		public int attend_id
		{
			set{ _attend_id=value;}
			get{return _attend_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int attend_month
		{
			set{ _attend_month=value;}
			get{return _attend_month;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ToWork
		{
			set{ _towork=value;}
			get{return _towork;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string OffWork
		{
			set{ _offwork=value;}
			get{return _offwork;}
		}
        /// <summary>
        /// 
        /// </summary>
        public int attend_year
        {
            set { _attend_year = value; }
            get { return _attend_year; }
        }
		#endregion Model

	}
}

