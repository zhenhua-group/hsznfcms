﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_RelativeContractInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_RelativeContractInfo
	{
		public cm_RelativeContractInfo()
		{}
		#region Model
		private int _rc_id;
		private int _cst_id;
		private string _contractid;
		private string _contractno;
		private string _contractname;
		private decimal? _contractaccount;
		private DateTime? _signedate;
		private DateTime? _completedate;
		private string _functionary;
		private int? _updateby;
		private DateTime? _lastupdate;
		/// <summary>
		/// 
		/// </summary>
		public int RC_Id
		{
			set{ _rc_id=value;}
			get{return _rc_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Cst_Id
		{
			set{ _cst_id=value;}
			get{return _cst_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ContractId
		{
			set{ _contractid=value;}
			get{return _contractid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ContractNo
		{
			set{ _contractno=value;}
			get{return _contractno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ContractName
		{
			set{ _contractname=value;}
			get{return _contractname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ContractAccount
		{
			set{ _contractaccount=value;}
			get{return _contractaccount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SigneDate
		{
			set{ _signedate=value;}
			get{return _signedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CompleteDate
		{
			set{ _completedate=value;}
			get{return _completedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Functionary
		{
			set{ _functionary=value;}
			get{return _functionary;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdateBy
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastUpdate
		{
			set{ _lastupdate=value;}
			get{return _lastupdate;}
		}
		#endregion Model

	}
}

