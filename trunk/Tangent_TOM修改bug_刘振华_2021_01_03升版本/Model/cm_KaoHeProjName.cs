﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeProjName:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeProjName
	{
		public cm_KaoHeProjName()
		{}
		#region Model
		private int _id;
		private int _kaoheprojid;
		private string _kname;
		private decimal _allscale;
		private decimal _donescale;
		private string _sub;
		private decimal _leftscale;
		private decimal _hisscale;
		private int _proid;
		private int _inseruserid;
		private DateTime? _insertdate= DateTime.Now;
		private int? _stat=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int KaoHeProjId
		{
			set{ _kaoheprojid=value;}
			get{return _kaoheprojid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string KName
		{
			set{ _kname=value;}
			get{return _kname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal AllScale
		{
			set{ _allscale=value;}
			get{return _allscale;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal DoneScale
		{
			set{ _donescale=value;}
			get{return _donescale;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Sub
		{
			set{ _sub=value;}
			get{return _sub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal LeftScale
		{
			set{ _leftscale=value;}
			get{return _leftscale;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal HisScale
		{
			set{ _hisscale=value;}
			get{return _hisscale;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int proID
		{
			set{ _proid=value;}
			get{return _proid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InserUserID
		{
			set{ _inseruserid=value;}
			get{return _inseruserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate
		{
			set{ _insertdate=value;}
			get{return _insertdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

