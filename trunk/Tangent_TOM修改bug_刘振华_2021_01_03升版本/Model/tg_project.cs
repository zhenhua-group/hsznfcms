﻿using System;
namespace TG.Model
{
    /// <summary>
    /// tg_project:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class tg_project
    {
        public tg_project()
        { }
        #region Model
        private int _pro_id;
        private string _pro_name;
        private string _pro_order;
        private string _pro_intro;
        private int? _pro_count;
        private string _pro_kinds;
        private int? _pro_icon;
        private DateTime? _pro_starttime;
        private DateTime? _pro_finishtime;
        private string _pro_status;
        private int? _pro_postarticle;
        private int? _pro_postcomms;
        private string _pro_buildunit;
        private decimal? _pro_archarea;
        private int? _pro_floornum;
        private string _pro_strutype;
        private decimal? _pro_archheight;
        private string _pro_serial;
        private string _pro_designunit;
        private int? _pro_level;
        private decimal? _pro_usedspace;
        private decimal? _pro_allspace;
        private int _pro_category = -1;
        private string _pro_adress;
        private string _pro_summary;
        //标示新版程序
        private int? _pro_floorup;
        private int? _pro_floordown;
        private int? _pro_flag;
        private int? _pro_exinfo1;
        private string _pro_exinfo2;
        private string _pro_exinfo3;
        private string _pro_phone;
        private string _pro_contractno;
        /// <summary>
        /// 项目ID
        /// </summary>
        public int pro_ID
        {
            set { _pro_id = value; }
            get { return _pro_id; }
        }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string pro_Name
        {
            set { _pro_name = value; }
            get { return _pro_name; }
        }
        /// <summary>
        /// 项目序号
        /// </summary>
        public string pro_Order
        {
            set { _pro_order = value; }
            get { return _pro_order; }
        }
        /// <summary>
        /// 项目介绍
        /// </summary>
        public string pro_Intro
        {
            set { _pro_intro = value; }
            get { return _pro_intro; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? pro_count
        {
            set { _pro_count = value; }
            get { return _pro_count; }
        }
        /// <summary>
        /// 项目类型
        /// </summary>
        public string pro_Kinds
        {
            set { _pro_kinds = value; }
            get { return _pro_kinds; }
        }
        /// <summary>
        /// 项目图标
        /// </summary>
        public int? pro_Icon
        {
            set { _pro_icon = value; }
            get { return _pro_icon; }
        }
        /// <summary>
        /// 项目开始时间
        /// </summary>
        public DateTime? pro_StartTime
        {
            set { _pro_starttime = value; }
            get { return _pro_starttime; }
        }
        /// <summary>
        /// 项目实际完成时间
        /// </summary>
        public DateTime? pro_FinishTime
        {
            set { _pro_finishtime = value; }
            get { return _pro_finishtime; }
        }
        /// <summary>
        /// 项目状态
        /// </summary>
        public string pro_Status
        {
            set { _pro_status = value; }
            get { return _pro_status; }
        }
        /// <summary>
        /// 项目发表文章数目
        /// </summary>
        public int? pro_PostArticle
        {
            set { _pro_postarticle = value; }
            get { return _pro_postarticle; }
        }
        /// <summary>
        /// 项目发表评论数目
        /// </summary>
        public int? pro_PostComms
        {
            set { _pro_postcomms = value; }
            get { return _pro_postcomms; }
        }
        /// <summary>
        /// 项目建设单位
        /// </summary>
        public string pro_BuildUnit
        {
            set { _pro_buildunit = value; }
            get { return _pro_buildunit; }
        }
        /// <summary>
        /// 建筑面积
        /// </summary>
        public decimal? pro_ArchArea
        {
            set { _pro_archarea = value; }
            get { return _pro_archarea; }
        }
        /// <summary>
        /// 建筑层数
        /// </summary>
        public int? pro_FloorNum
        {
            set { _pro_floornum = value; }
            get { return _pro_floornum; }
        }
        /// <summary>
        /// 结构形式
        /// </summary>
        public string pro_StruType
        {
            set { _pro_strutype = value; }
            get { return _pro_strutype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? pro_ArchHeight
        {
            set { _pro_archheight = value; }
            get { return _pro_archheight; }
        }
        /// <summary>
        /// 工号
        /// </summary>
        public string pro_Serial
        {
            set { _pro_serial = value; }
            get { return _pro_serial; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_DesignUnit
        {
            set { _pro_designunit = value; }
            get { return _pro_designunit; }
        }
        /// <summary>
        /// 工程级别
        /// </summary>
        public int? pro_Level
        {
            set { _pro_level = value; }
            get { return _pro_level; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? pro_UsedSpace
        {
            set { _pro_usedspace = value; }
            get { return _pro_usedspace; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? pro_AllSpace
        {
            set { _pro_allspace = value; }
            get { return _pro_allspace; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int pro_category
        {
            set { _pro_category = value; }
            get { return _pro_category; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_Adress
        {
            set { _pro_adress = value; }
            get { return _pro_adress; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_Summary
        {
            set { _pro_summary = value; }
            get { return _pro_summary; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? pro_FloorUp
        {
            set { _pro_floorup = value; }
            get { return _pro_floorup; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? pro_FloorDown
        {
            set { _pro_floordown = value; }
            get { return _pro_floordown; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? pro_flag
        {
            set { _pro_flag = value; }
            get { return _pro_flag; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? pro_ExInfo1
        {
            set { _pro_exinfo1 = value; }
            get { return _pro_exinfo1; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_ExInfo2
        {
            set { _pro_exinfo2 = value; }
            get { return _pro_exinfo2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pro_ExInfo3
        {
            set { _pro_exinfo3 = value; }
            get { return _pro_exinfo3; }
        }
        public string pro_Phone
        {
            set { _pro_phone = value; }
            get { return _pro_phone; }
        }
        public string pro_ContractNo
        {
            set { _pro_contractno = value; }
            get { return _pro_contractno; }
        }
        public string StartTimeString
        {
            get
            {
                return Convert.ToDateTime(pro_StartTime).ToString("yyyy-MM-dd");
            }
        }
        public string FinishTimeString
        {
            get
            {
                return Convert.ToDateTime(pro_FinishTime).ToString("yyyy-MM-dd");
            }
        }

        public int pro_DesignUnitID { get; set; }
        #endregion Model

    }
}

