﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class cm_jjsProjectValueAllotConfig
    {
        public cm_jjsProjectValueAllotConfig()
        { }
        #region Model
        private int _id;
        private int? _typestatus;
        private string _typedetail;
        private decimal? _proofreadpercent;
        private decimal? _buildingpercent;
        private decimal? _structurepercent;
        private decimal? _drainpercent;
        private decimal? _havcpercent;
        private decimal? _electricpercent;
        private decimal _totalbuildingpercent;
        private decimal _totalInstallationpercent;

        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? typeStatus
        {
            set { _typestatus = value; }
            get { return _typestatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string typeDetail
        {
            set { _typedetail = value; }
            get { return _typedetail; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProofreadPercent
        {
            set { _proofreadpercent = value; }
            get { return _proofreadpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BuildingPercent
        {
            set { _buildingpercent = value; }
            get { return _buildingpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? StructurePercent
        {
            set { _structurepercent = value; }
            get { return _structurepercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DrainPercent
        {
            set { _drainpercent = value; }
            get { return _drainpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HavcPercent
        {
            set { _havcpercent = value; }
            get { return _havcpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ElectricPercent
        {
            set { _electricpercent = value; }
            get { return _electricpercent; }
        }
        /// <summary>
        /// 合计
        /// </summary>
        public decimal Totalbuildingpercent
        {
            get { return _totalbuildingpercent; }
            set { _totalbuildingpercent = value; }
        }


        /// <summary>
        /// 合计
        /// </summary>
        public decimal TotalInstallationpercent
        {
            get { return _totalInstallationpercent; }
            set { _totalInstallationpercent = value; }
        }
        #endregion Model
    }
}
