﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_MemOutHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_MemOutHis
	{
		public cm_MemOutHis()
		{}
		#region Model
		private int _id;
		private int? _mem_id=0;
		private string _mem_name;
		private decimal? _yue=0M;
		private decimal? _setcount=0M;
		private int? _setyear=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Mem_Name
		{
			set{ _mem_name=value;}
			get{return _mem_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Yue
		{
			set{ _yue=value;}
			get{return _yue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SetCount
		{
			set{ _setcount=value;}
			get{return _setcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SetYear
		{
			set{ _setyear=value;}
			get{return _setyear;}
		}
		#endregion Model

	}
}

