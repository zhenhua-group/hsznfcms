﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_HolidayConfig:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_HolidayConfig
	{
		public cm_HolidayConfig()
		{}
		#region Model
		private int _id;
		private DateTime _holiday;
		private int? _daytype;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime holiday
		{
			set{ _holiday=value;}
			get{return _holiday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? daytype
		{
			set{ _daytype=value;}
			get{return _daytype;}
		}
		#endregion Model

	}
}

