﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_package:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_package
	{
		public tg_package()
		{}
		#region Model
		private decimal _package_id;
		private int _subpro_id;
		private int _class_id;
		private int _user_id;
		private string _filename;
		private byte[] _package;
		private string _remember;
		private DateTime? _ver_date;
		private int? _save_flag;
		private int? _file_size;
		private int? _file_state;
		private int? _lastmodifyuser_id;
		private int _purpose_id;
		private string _version;
		/// <summary>
		/// 
		/// </summary>
		public decimal package_ID
		{
			set{ _package_id=value;}
			get{return _package_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int subpro_ID
		{
			set{ _subpro_id=value;}
			get{return _subpro_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int class_ID
		{
			set{ _class_id=value;}
			get{return _class_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int user_ID
		{
			set{ _user_id=value;}
			get{return _user_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string filename
		{
			set{ _filename=value;}
			get{return _filename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public byte[] package
		{
			set{ _package=value;}
			get{return _package;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string remember
		{
			set{ _remember=value;}
			get{return _remember;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ver_date
		{
			set{ _ver_date=value;}
			get{return _ver_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? save_flag
		{
			set{ _save_flag=value;}
			get{return _save_flag;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? file_size
		{
			set{ _file_size=value;}
			get{return _file_size;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? file_state
		{
			set{ _file_state=value;}
			get{return _file_state;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? lastModifyUser_ID
		{
			set{ _lastmodifyuser_id=value;}
			get{return _lastmodifyuser_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int purpose_ID
		{
			set{ _purpose_id=value;}
			get{return _purpose_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string version
		{
			set{ _version=value;}
			get{return _version;}
		}
		#endregion Model

	}
}

