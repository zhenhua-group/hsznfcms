﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_YearPaidStatisHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_YearPaidStatisHis
	{
		public cm_YearPaidStatisHis()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private string _mem_name;
		private int _mem_unit_id;	
		private string _datadate;
		private decimal? _time_leave;
		private decimal? _time_sick;
		private decimal? _time_over;
		private decimal? _time_marry;
		private decimal? _time_mater;
		private decimal? _time_die;
		private int? _time_late;
		private decimal? _time_year;
		private decimal? _time_sum;
		private decimal? _privmonthday;
		private decimal? _currentmonthday;
        private decimal? _priMonthCurrDay;
        private decimal? _lastMonthDay;
		private string _content;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_name
		{
			set{ _mem_name=value;}
			get{return _mem_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_unit_ID
		{
			set{ _mem_unit_id=value;}
			get{return _mem_unit_id;}
		}		
		/// <summary>
		/// 
		/// </summary>
		public string dataDate
		{
			set{ _datadate=value;}
			get{return _datadate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_leave
		{
			set{ _time_leave=value;}
			get{return _time_leave;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_sick
		{
			set{ _time_sick=value;}
			get{return _time_sick;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_over
		{
			set{ _time_over=value;}
			get{return _time_over;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_marry
		{
			set{ _time_marry=value;}
			get{return _time_marry;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_mater
		{
			set{ _time_mater=value;}
			get{return _time_mater;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_die
		{
			set{ _time_die=value;}
			get{return _time_die;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? time_late
		{
			set{ _time_late=value;}
			get{return _time_late;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_year
		{
			set{ _time_year=value;}
			get{return _time_year;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? time_sum
		{
			set{ _time_sum=value;}
			get{return _time_sum;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? privMonthDay
		{
			set{ _privmonthday=value;}
			get{return _privmonthday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? currentMonthDay
		{
			set{ _currentmonthday=value;}
			get{return _currentmonthday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string content
		{
			set{ _content=value;}
			get{return _content;}
		}
        /// <summary>
        /// 
        /// </summary>
        public decimal? priMonthCurrDay
        {
            set { _priMonthCurrDay = value; }
            get { return _priMonthCurrDay; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? lastMonthDay
        {
            set { _lastMonthDay = value; }
            get { return _lastMonthDay; }
        }
		#endregion Model

	}
}

