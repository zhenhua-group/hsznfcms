﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
	/// <summary>
	/// 国家实体
	/// </summary>
	public class Country
	{
		public int ID { get; set; }

		public string CountryID { get; set; }

		public string CountryName { get; set; }
	}

	/// <summary>
	/// 省份
	/// </summary>
	public class Province
	{
		public int ID { get; set; }

		public string ProvinceID { get; set; }

		public string ProvinceName { get; set; }

		public string CountryID { get; set; }
	}

	/// <summary>
	/// 城市
	/// </summary>
	public class City
	{
		public int ID { get; set; }

		public string CityID { get; set; }

		public string CityName { get; set; }

		public string ProvinceID { get; set; }
	}


	public class DropDownListQueryEntity
	{
		public string CountryName { get; set; }

		public string CountryID { get; set; }
	}
}
