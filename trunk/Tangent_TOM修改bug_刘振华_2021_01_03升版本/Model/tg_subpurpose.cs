﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_subpurpose:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_subpurpose
	{
		public tg_subpurpose()
		{}
		#region Model
		private int _id;
		private int _purposeid;
		private int _parentid;
		private int _subproid;
		private string _name;
		private string _descr;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int purposeid
		{
			set{ _purposeid=value;}
			get{return _purposeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int parentid
		{
			set{ _parentid=value;}
			get{return _parentid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int subproid
		{
			set{ _subproid=value;}
			get{return _subproid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string descr
		{
			set{ _descr=value;}
			get{return _descr;}
		}
		#endregion Model

	}
}

