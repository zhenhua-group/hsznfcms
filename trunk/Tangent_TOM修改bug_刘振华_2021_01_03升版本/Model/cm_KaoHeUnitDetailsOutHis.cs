﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeUnitDetailsOutHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeUnitDetailsOutHis
	{
		public cm_KaoHeUnitDetailsOutHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _renwuname;
		private int? _unitid=0;
		private string _unitname;
		private string _username;
		private string _rzsj;
		private string _lzsj;
		private decimal? _jbgz=0M;
		private decimal? _yfjj=0M;
		private decimal? _pjgz=0M;
		private decimal? _bmjjtz=0M;
		private decimal? _gzbs=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RZSJ
		{
			set{ _rzsj=value;}
			get{return _rzsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LZSJ
		{
			set{ _lzsj=value;}
			get{return _lzsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JBGZ
		{
			set{ _jbgz=value;}
			get{return _jbgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? YFJJ
		{
			set{ _yfjj=value;}
			get{return _yfjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PJGZ
		{
			set{ _pjgz=value;}
			get{return _pjgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMJJTZ
		{
			set{ _bmjjtz=value;}
			get{return _bmjjtz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? GZBS
		{
			set{ _gzbs=value;}
			get{return _gzbs;}
		}
		#endregion Model

	}
}

