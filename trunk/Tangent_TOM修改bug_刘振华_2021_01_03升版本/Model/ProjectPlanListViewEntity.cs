﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class ProjectPlanListViewEntityNo
    {
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("ProjectName")]
        public string ProjectName { get; set; }

        [DataMapping("IsBackup")]
        public string IsBackup { get; set; }

        [DataMapping("CoperationName")]
        public string CoperationName { get; set; }

        [DataMapping("SubmitDate")]
        public DateTime SubmitDate { get; set; }

        [DataMapping("ProjectJobNumber")]
        public string ProjectJobNumber { get; set; }

        [DataMapping("ProjectPlanAuditSysNo")]
        public int ProjectPlanAuditSysNo { get; set; }

        [DataMapping("IsHave")]
        public int IsHave { get; set; }

        [DataMapping("ProjectPlanAuditStatus")]
        public string ProjectPlanAuditStatus { get; set; }

        [DataMapping("PMName")]
        public string PMName { get; set; }

        [DataMapping("PMUserID")]
        public int PMUserID { get; set; }

        public string ApplyAuditUserName { get; set; }

        public string Percent
        {
            get
            {
                return XMLHelper.AccountPercent(3, ProjectPlanAuditStatus).ToString("f2");
            }
        }

        public string SubmitDateString
        {
            get
            {
                return SubmitDate.ToString("yyyy-MM-dd hh:mm");
            }
        }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatusString
        {
            get
            {
                string auditString = "";
                if (ProjectPlanAuditSysNo == 0)
                {
                    auditString = "未提交评审";
                }
                else
                {
                    switch (ProjectPlanAuditStatus)
                    {
                        case "A":
                            auditString = "承接部门";
                            break;
                        case "B":
                            auditString = "生产经营部";
                            break;
                        case "D":
                            auditString = "总工办";
                            break;
                        case "F":
                            auditString = "总经理";
                            break;
                        case "C":
                        case "E":
                        case "G":
                        case "I":
                            auditString = "评审不通过";
                            break;
                        case "H":
                            auditString = "评审通过";
                            break;
                        case "R":
                            auditString = "申请修改中";
                            break;
                    }
                }
                return auditString;
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                linkAction = "<a class=\"cls_addplan\" href=\"/ProjectPlan/ProPlanAdd.aspx?projectSysNo=" + ProjectSysNo + "&CoperationName=" + CoperationName + "&Action=1&JobNumber=" + ProjectJobNumber.Trim() + "\">添加策划</a>";
                return linkAction;
            }
        }
    }

    public class ProjectPlanListViewEntityYes
    {
        [DataMapping("ProjectSysNo")]
        public int ProjectSysNo { get; set; }

        [DataMapping("ProjectName")]
        public string ProjectName { get; set; }

        [DataMapping("IsBackup")]
        public string IsBackup { get; set; }

        [DataMapping("CoperationName")]
        public string CoperationName { get; set; }

        [DataMapping("SubmitDate")]
        public DateTime SubmitDate { get; set; }

        [DataMapping("ProjectJobNumber")]
        public string ProjectJobNumber { get; set; }

        [DataMapping("ProjectPlanAuditSysNo")]
        public int ProjectPlanAuditSysNo { get; set; }

        [DataMapping("IsHave")]
        public int IsHave { get; set; }

        [DataMapping("ProjectPlanAuditStatus")]
        public string ProjectPlanAuditStatus { get; set; }

        [DataMapping("ApplyAuditUserName")]
        public string ApplyAuditUserName { get; set; }

        [DataMapping("PMName")]
        public string PMName { get; set; }

        [DataMapping("PMUserID")]
        public int PMUserID { get; set; }

        public string Percent
        {
            get
            {
                return XMLHelper.AccountPercent(3, ProjectPlanAuditStatus).ToString("f2");
            }
        }

        public string SubmitDateString
        {
            get
            {
                return SubmitDate.ToString("yyyy-MM-dd hh:mm");
            }
        }


        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatusString
        {
            get
            {
                string auditString = "";
                if (ProjectPlanAuditSysNo == 0)
                {
                    auditString = "未提交评审";
                }
                else
                {
                    switch (ProjectPlanAuditStatus)
                    {
                        case "A":
                            auditString = "承接部门";
                            break;
                        case "B":
                            auditString = "生产经营部";
                            break;
                        case "D":
                            auditString = "总工办";
                            break;
                        case "F":
                            auditString = "总经理";
                            break;
                        case "C":
                        case "E":
                        case "G":
                        case "I":
                            auditString = "评审不通过";
                            break;
                        case "H":
                            auditString = "评审通过";
                            break;
                        case "R":
                            auditString = "申请修改中";
                            break;
                    }
                }
                return auditString;
            }
        }

        public string LinkAction
        {
            get
            {
                string linkAction = "";
                if (ProjectPlanAuditSysNo == 0 || ProjectPlanAuditStatus == "C" || ProjectPlanAuditStatus == "E" || ProjectPlanAuditStatus == "G" || ProjectPlanAuditStatus == "I")
                {
                    linkAction = "<a class=\"cls_editplan\" href=\"/ProjectPlan/ProPlanAdd.aspx?projectSysNo=" + ProjectSysNo + "&Action=3&JobNumber=" + ProjectJobNumber.Trim() + "&CoperationName=" + CoperationName + "\">编辑</a><a  href=\"###\" style=\"margin-left:5px;\" id=\"applyAuditImgButton\" projectsysno=\"" + ProjectSysNo + "\" coperationname=\"" + CoperationName + "\">发起审批</a>";
                }
                else
                {
                    linkAction = "审核中";
                }
                if (ProjectPlanAuditStatus == "F")
                {
                    linkAction = string.Format("已完成 | <a href=\'/ProjectPlan/ProjectPlanAudit/ProPlanAudit.aspx?projectPlanAuditSysNo={0}\'>评审结果</a>", ProjectPlanAuditSysNo);
                }
                if (ProjectPlanAuditStatus == "R")
                {
                    linkAction = "<a class=\"cls_editplan\" href=\"/ProjectPlan/ProPlanAdd.aspx?projectSysNo=" + ProjectSysNo + "&Action=3&JobNumber=" + ProjectJobNumber.Trim() + "&CoperationName=" + CoperationName + "\">编辑</a> | <a href=\'/ProjectPlan/ProjectPlanAudit/ProPlanAudit.aspx?projectPlanAuditSysNo=" + ProjectPlanAuditSysNo + "\'>评审结果</a>";
                }
                return linkAction;
            }
        }
    }
}
