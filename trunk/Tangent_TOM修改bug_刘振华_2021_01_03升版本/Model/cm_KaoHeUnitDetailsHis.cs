﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeUnitDetailsHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeUnitDetailsHis
	{
		public cm_KaoHeUnitDetailsHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _renwuname;
		private int? _unitid=0;
		private string _unitname;
		private decimal? _unitallcount=0M;
		private decimal? _ztcjxs=0M;
		private string _username;
		private decimal? _jbgz=0M;
		private decimal? _gzsj=0M;
		private decimal? _pjygz=0M;
		private decimal? _yfjj=0M;
		private decimal? _zphp=0M;
		private decimal? _pm=0M;
		private decimal? _bmjlpj=0M;
		private decimal? _pm2=0M;
		private decimal? _jqxs=0M;
		private decimal? _bmnjjjs=0M;
		private decimal? _xmjjjs=0M;
		private decimal? _xmjjjltz=0M;
		private decimal? _xmjjtz=0M;
		private decimal? _yxyg=0M;
		private decimal? _hj=0M;
		private decimal? _hjyf=0M;
		private decimal? _gzbs=0M;
		private decimal? _sbn=0M;
		private decimal? _sbn2=0M;
		private decimal? _xbn=0M;
		private decimal? _xbn2=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? UnitAllCount
		{
			set{ _unitallcount=value;}
			get{return _unitallcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ZTCJXS
		{
			set{ _ztcjxs=value;}
			get{return _ztcjxs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JBGZ
		{
			set{ _jbgz=value;}
			get{return _jbgz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? GZSJ
		{
			set{ _gzsj=value;}
			get{return _gzsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PJYGZ
		{
			set{ _pjygz=value;}
			get{return _pjygz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? YFJJ
		{
			set{ _yfjj=value;}
			get{return _yfjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ZPHP
		{
			set{ _zphp=value;}
			get{return _zphp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PM
		{
			set{ _pm=value;}
			get{return _pm;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMJLPJ
		{
			set{ _bmjlpj=value;}
			get{return _bmjlpj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PM2
		{
			set{ _pm2=value;}
			get{return _pm2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? JQXS
		{
			set{ _jqxs=value;}
			get{return _jqxs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMNJJJS
		{
			set{ _bmnjjjs=value;}
			get{return _bmnjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMJJJS
		{
			set{ _xmjjjs=value;}
			get{return _xmjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMJJJLTZ
		{
			set{ _xmjjjltz=value;}
			get{return _xmjjjltz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMJJTZ
		{
			set{ _xmjjtz=value;}
			get{return _xmjjtz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? YXYG
		{
			set{ _yxyg=value;}
			get{return _yxyg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? HJ
		{
			set{ _hj=value;}
			get{return _hj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? HJYF
		{
			set{ _hjyf=value;}
			get{return _hjyf;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? GZBS
		{
			set{ _gzbs=value;}
			get{return _gzbs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SBN
		{
			set{ _sbn=value;}
			get{return _sbn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? SBN2
		{
			set{ _sbn2=value;}
			get{return _sbn2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XBN
		{
			set{ _xbn=value;}
			get{return _xbn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XBN2
		{
			set{ _xbn2=value;}
			get{return _xbn2;}
		}
		#endregion Model

	}
}

