﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_relation:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_relation
	{
		public tg_relation()
		{}
		#region Model
		private int? _pro_id;
		private int? _mem_id;
		private int? _mem_ismanage;
		private int? _mem_isspeclead;
		private int _mem_isassistant;
		private int _mem_isassessor;
		private int _mem_iscorrector=0;
		private int _mem_isvalidator=0;
		private int _mem_isdirector=0;
		private string _mem_roleids;
		private int _mem_bcanviewspec=1;
		private int _mem_bcanviewallspec=0;
		/// <summary>
		/// 是否是校对人
		/// </summary>
		public int? pro_ID
		{
			set{ _pro_id=value;}
			get{return _pro_id;}
		}
		/// <summary>
		/// 人员id
		/// </summary>
		public int? mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 是否是工程主持人
		/// </summary>
		public int? mem_isManage
		{
			set{ _mem_ismanage=value;}
			get{return _mem_ismanage;}
		}
		/// <summary>
		/// 是否是专业负责人
		/// </summary>
		public int? mem_isSpecLead
		{
			set{ _mem_isspeclead=value;}
			get{return _mem_isspeclead;}
		}
		/// <summary>
		/// 是否是项目助理
		/// </summary>
		public int mem_isAssistant
		{
			set{ _mem_isassistant=value;}
			get{return _mem_isassistant;}
		}
		/// <summary>
		/// 是否是审核人
		/// </summary>
		public int mem_isAssessor
		{
			set{ _mem_isassessor=value;}
			get{return _mem_isassessor;}
		}
		/// <summary>
		/// 是否是校对人
		/// </summary>
		public int mem_isCorrector
		{
			set{ _mem_iscorrector=value;}
			get{return _mem_iscorrector;}
		}
		/// <summary>
		/// 是否是审定人
		/// </summary>
		public int mem_isValidator
		{
			set{ _mem_isvalidator=value;}
			get{return _mem_isvalidator;}
		}
		/// <summary>
		/// 是否是经营总监
		/// </summary>
		public int mem_isDirector
		{
			set{ _mem_isdirector=value;}
			get{return _mem_isdirector;}
		}
		/// <summary>
		/// 项目角色ID
		/// </summary>
		public string mem_RoleIDs
		{
			set{ _mem_roleids=value;}
			get{return _mem_roleids;}
		}
		/// <summary>
		/// 浏览本专业的权限（默认为1）
		/// </summary>
		public int mem_bCanViewSpec
		{
			set{ _mem_bcanviewspec=value;}
			get{return _mem_bcanviewspec;}
		}
		/// <summary>
		/// 浏览全专业的权限（默认为0）
		/// </summary>
		public int mem_bCanViewAllSpec
		{
			set{ _mem_bcanviewallspec=value;}
			get{return _mem_bcanviewallspec;}
		}
		#endregion Model

	}
}

