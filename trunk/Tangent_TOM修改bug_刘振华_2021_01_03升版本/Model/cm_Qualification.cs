﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_qualification:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class cm_Qualification
    {
        #region Model
        private int _qua_id;
        private int _cst_id;
        private int _cm_attaInfoId;

        public int cm_AttaInfoId
        {
            get { return _cm_attaInfoId; }
            set { _cm_attaInfoId = value; }
        }
        private string _qua_name;
        private DateTime? _qua_zztime;
        private string _qua_content;
        private string _qua_imagesrc;
        private DateTime? _qua_sptime;
        private string _qua_jb;
        private DateTime? _qua_yeartime;
        /// <summary>
        /// 
        /// </summary>
        public int qua_Id
        {
            set { _qua_id = value; }
            get { return _qua_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string qua_Name
        {
            set { _qua_name = value; }
            get { return _qua_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? qua_ZzTime
        {
            set { _qua_zztime = value; }
            get { return _qua_zztime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string qua_Content
        {
            set { _qua_content = value; }
            get { return _qua_content; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string qua_ImageSrc
        {
            set { _qua_imagesrc = value; }
            get { return _qua_imagesrc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? qua_SpTime
        {
            set { _qua_sptime = value; }
            get { return _qua_sptime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string qua_jb
        {
            set { _qua_jb = value; }
            get { return _qua_jb; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? qua_yeartime
        {
            set { _qua_yeartime = value; }
            get { return _qua_yeartime; }
        }
        #endregion Model
    }
}
