﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ApplyStatisData:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ApplyStatisData
	{
		public cm_ApplyStatisData()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private int _datayear;
		private int _datamonth;
		private int _dataday=0;
		private decimal _datavalue=0M;
		private string _datatype;
		private string _datasource;
		private string _datacontent;
		private int _adduserid;
		private DateTime _adddate= DateTime.Now;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dataYear
		{
			set{ _datayear=value;}
			get{return _datayear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dataMonth
		{
			set{ _datamonth=value;}
			get{return _datamonth;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int dataDay
		{
			set{ _dataday=value;}
			get{return _dataday;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal dataValue
		{
			set{ _datavalue=value;}
			get{return _datavalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dataType
		{
			set{ _datatype=value;}
			get{return _datatype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dataSource
		{
			set{ _datasource=value;}
			get{return _datasource;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dataContent
		{
			set{ _datacontent=value;}
			get{return _datacontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int addUserId
		{
			set{ _adduserid=value;}
			get{return _adduserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime addDate
		{
			set{ _adddate=value;}
			get{return _adddate;}
		}
		#endregion Model

	}
}

