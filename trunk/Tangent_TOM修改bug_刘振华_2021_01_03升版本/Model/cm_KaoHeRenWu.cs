﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_KaoHeRenWu:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_KaoHeRenWu
    {
        public cm_KaoHeRenWu()
        { }
        #region Model
        private int _id;
        private string _renwuno;
        private DateTime _startdate;
        private DateTime _enddate;
        private string _sub;
        private string _projstatus;
        private string _deptstatus;
        private DateTime _insertdate;
        private int _insertuser;
        private decimal? _jianallcount = 0M;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RenWuNo
        {
            set { _renwuno = value; }
            get { return _renwuno; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime StartDate
        {
            set { _startdate = value; }
            get { return _startdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndDate
        {
            set { _enddate = value; }
            get { return _enddate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Sub
        {
            set { _sub = value; }
            get { return _sub; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProjStatus
        {
            set { _projstatus = value; }
            get { return _projstatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DeptStatus
        {
            set { _deptstatus = value; }
            get { return _deptstatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int InsertUser
        {
            set { _insertuser = value; }
            get { return _insertuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? JianAllCount
        {
            set { _jianallcount = value; }
            get { return _jianallcount; }
        }
        #endregion Model

    }
}

