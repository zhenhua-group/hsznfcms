﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_ProRole:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_ProRole
	{
		public tg_ProRole()
		{}
		#region Model
		private int _id;
		private string _rolename;
		private string _subprolevel;
		private string _stagelevel;
		private string _purposelevel;
		private string _speclevel;
		private string _designer;
		private string _level3;
		private string _level4;
		private string _level5;
		/// <summary>
		/// 项目角色ID
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 项目角色名
		/// </summary>
		public string RoleName
		{
			set{ _rolename=value;}
			get{return _rolename;}
		}
		/// <summary>
		/// 子项的操作权限
		/// </summary>
		public string SubProLevel
		{
			set{ _subprolevel=value;}
			get{return _subprolevel;}
		}
		/// <summary>
		/// 设计阶段的操作权限
		/// </summary>
		public string StageLevel
		{
			set{ _stagelevel=value;}
			get{return _stagelevel;}
		}
		/// <summary>
		/// 作业的操作权限
		/// </summary>
		public string PurposeLevel
		{
			set{ _purposelevel=value;}
			get{return _purposelevel;}
		}
		/// <summary>
		/// 专业的操作权限
		/// </summary>
		public string SpecLevel
		{
			set{ _speclevel=value;}
			get{return _speclevel;}
		}
		/// <summary>
		/// 普通设计人员的权限
		/// </summary>
		public string Designer
		{
			set{ _designer=value;}
			get{return _designer;}
		}
		/// <summary>
		/// 预留权限
		/// </summary>
		public string Level3
		{
			set{ _level3=value;}
			get{return _level3;}
		}
		/// <summary>
		/// 预留权限
		/// </summary>
		public string Level4
		{
			set{ _level4=value;}
			get{return _level4;}
		}
		/// <summary>
		/// 预留权限
		/// </summary>
		public string Level5
		{
			set{ _level5=value;}
			get{return _level5;}
		}
		#endregion Model

	}
}

