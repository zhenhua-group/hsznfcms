﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class ProMSFeiWai
    {
        public ProMSFeiWai()
        { }
        #region Model
        private string _unitid;
        private string _sxianmumingcheng;



        private decimal? _sbenzhoushoufei;
        /// <summary>
        /// 项目收费情况model所对应的ID
        /// </summary>
        public string Unitid
        {
            set { _unitid = value; }
            get { return _unitid; }
        }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string Sxianmumingcheng
        {
            get { return _sxianmumingcheng; }
            set { _sxianmumingcheng = value; }
        }

        /// <summary>
        /// 项目的本周的收费情况
        /// </summary>
        public decimal? SBenzhoushoufei
        {
            set { _sbenzhoushoufei = value; }
            get { return _sbenzhoushoufei; }
        }
        #endregion Model

    }
}

