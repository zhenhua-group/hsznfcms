﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeRenwuMem:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeRenwuMem
	{
		public cm_KaoHeRenwuMem()
		{}
		#region Model
		private int _id;
		private int _kaoheunitid;
		private int _memid;
		private string _memname;
		private int _memunitid;
		private string _memunitname;
		private int _statu=0;
		private int? _magrole;
		private int? _normalrole;
		private decimal? _weights;
		private int? _isoutunitid=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int KaoHeUnitID
		{
			set{ _kaoheunitid=value;}
			get{return _kaoheunitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int memID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string memName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int memUnitID
		{
			set{ _memunitid=value;}
			get{return _memunitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string memUnitName
		{
			set{ _memunitname=value;}
			get{return _memunitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int Statu
		{
			set{ _statu=value;}
			get{return _statu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? MagRole
		{
			set{ _magrole=value;}
			get{return _magrole;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? NormalRole
		{
			set{ _normalrole=value;}
			get{return _normalrole;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Weights
		{
			set{ _weights=value;}
			get{return _weights;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsOutUnitID
		{
			set{ _isoutunitid=value;}
			get{return _isoutunitid;}
		}
		#endregion Model

	}

}

