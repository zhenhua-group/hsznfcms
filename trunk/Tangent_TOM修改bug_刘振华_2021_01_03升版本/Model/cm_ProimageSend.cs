﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ProimageSend:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ProimageSend
	{
		public cm_ProimageSend()
		{}
		#region Model
		private int _id;
		private int? _pro_id;
		private int? _pro_image;
		private int? _send_number;
		private string _send_company;
		private decimal? _send_a0;
		private decimal? _send_a1;
		private decimal? _send_a2;
		private decimal? _send_a2_1;
		private decimal? _send_a3;
		private decimal? _send_a4;
		private int? _adduser;
		private DateTime? _addtime;
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Pro_id
		{
			set{ _pro_id=value;}
			get{return _pro_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Pro_image
		{
			set{ _pro_image=value;}
			get{return _pro_image;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Send_number
		{
			set{ _send_number=value;}
			get{return _send_number;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Send_company
		{
			set{ _send_company=value;}
			get{return _send_company;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Send_A0
		{
			set{ _send_a0=value;}
			get{return _send_a0;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Send_A1
		{
			set{ _send_a1=value;}
			get{return _send_a1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Send_A2
		{
			set{ _send_a2=value;}
			get{return _send_a2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Send_A2_1
		{
			set{ _send_a2_1=value;}
			get{return _send_a2_1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Send_A3
		{
			set{ _send_a3=value;}
			get{return _send_a3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Send_A4
		{
			set{ _send_a4=value;}
			get{return _send_a4;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Adduser
		{
			set{ _adduser=value;}
			get{return _adduser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? Addtime
		{
			set{ _addtime=value;}
			get{return _addtime;}
		}
		#endregion Model

	}
}

