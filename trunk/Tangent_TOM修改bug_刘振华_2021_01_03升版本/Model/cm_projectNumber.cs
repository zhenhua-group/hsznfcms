﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_projectNumber:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_projectNumber
	{
		public cm_projectNumber()
		{}
		#region Model
		private int _pronum_id;
		private int _pro_id;
		private DateTime? _submitdate;
		private string _submintperson;
		private string _pronumber;
		/// <summary>
		/// 
		/// </summary>
		public int proNum_id
		{
			set{ _pronum_id=value;}
			get{return _pronum_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int pro_id
		{
			set{ _pro_id=value;}
			get{return _pro_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SubmitDate
		{
			set{ _submitdate=value;}
			get{return _submitdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SubmintPerson
		{
			set{ _submintperson=value;}
			get{return _submintperson;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProNumber
		{
			set{ _pronumber=value;}
			get{return _pronumber;}
		}
		#endregion Model

	}
}

