﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class StandBookEntity
    {
        /// <summary>
        /// 编号
        /// </summary>
        [DataMapping("Cprid")]
        public int Cprid { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [DataMapping("CprNo")]
        public string CprNo { get; set; }
        /// <summary>
        /// 面积
        /// </summary>
        [DataMapping("BuildArea")]
        public string BuildArea { get; set; }
        /// <summary>
        ///项目名称
        /// </summary>
        [DataMapping("CprName")]
        public string CprName { get; set; }
        /// <summary>
        ///合同额
        /// </summary>
        [DataMapping("CprAcount")]
        public decimal CprAcount { get; set; }
        /// <summary>
        /// 收费
        /// </summary>

        public decimal Acount { get; set; }
        public decimal Owe
        {
            get
            {
                return CprAcount - Acount;
            }
        }
        /// <summary>
        /// 收款时间
        /// </summary>

        public DateTime InAcountTime { get; set; }
        /// <summary>
        /// 收费详情
        /// </summary>
        public string Chargedetail
        {

            get;
            set;
        }
        /// <summary>
        /// 工程阶段
        /// </summary>
        public string ProStage
        {
            get;
            set;
        }
        /// <summary>
        /// 合同阶段
        /// </summary>
        public string CopStage
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Mark
        {
            get;
            set;
        }
    }

    [Serializable]
    public class Collection
    {

        /// <summary>
        /// 金额
        /// </summary>
        [DataMapping("Acount")]
        public decimal Acount { get; set; }
        /// <summary>
        /// 设计单位
        /// </summary>
        [DataMapping("Unit")]
        public string Unit { get; set; }
        /// <summary>
        /// 建筑收费单位
        /// </summary>
        [DataMapping("BuildUnit")]
        public string BuildUnit { get; set; }
        /// <summary>
        /// 合同号
        /// </summary>
        [DataMapping("CprNo")]
        public string CprNo { get; set; }
        /// <summary>
        /// 收款日期
        /// </summary>
        [DataMapping("InAcountTime")]
        public string InAcountTime { get; set; }
    }

    [Serializable]
    public class AcoutAndTime
    {
        public decimal Acount { set; get; }
        public DateTime Time { get; set; }
    }

    [Serializable]
    public class ChargeDate
    {
        [DataMapping("cpr_Name")]
        public string CprName { get; set; }
        [DataMapping("acountAdd")]
        public string acountAdd { get; set; }
        [DataMapping("xx")]
        public string XX { get; set; }
        [DataMapping("Acount")]
        public decimal Acount { get; set; }
        [DataMapping("FristTime")]
        public string FristTime { get; set; }
        [DataMapping("SecondTime")]
        public string SecondTime { get; set; }
        [DataMapping("ThirdTime")]
        public string ThirdTime { get; set; }
        [DataMapping("FourthTime")]
        public string FourthTime { get; set; }
        [DataMapping("FifthTime")]
        public string FifthTime { get; set; }
        public string Mark
        {
            get
            {
                return "";
            }
        }
    }
}
