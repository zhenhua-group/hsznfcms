﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_CprCharge:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CprCharge
    {
        public cm_CprCharge()
        { }
        #region Model
        private int _id;
        private int? _cpr_id;
        private string _unit;
        private decimal? _inacount;
        private DateTime? _indate;
        private string _receiver;
        private string _remark;
        private string _cpr_no;
        private string _updateby;
        private DateTime? _lastupdate;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Unit
        {
            set { _unit = value; }
            get { return _unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? InAcount
        {
            set { _inacount = value; }
            get { return _inacount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InDate
        {
            set { _indate = value; }
            get { return _indate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Receiver
        {
            set { _receiver = value; }
            get { return _receiver; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Remark
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }
        #endregion Model

    }

}

