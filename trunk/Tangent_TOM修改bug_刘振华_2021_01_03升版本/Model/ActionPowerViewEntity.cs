﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ActionPowerViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("Description")]
        public string Description { get; set; }

        [DataMapping("PageName")]
        public string PageName { get; set; }

        [DataMapping("Role")]
        public string Role { get; set; }

        [DataMapping("Type")]
        public string Type { get; set; }

        public string TypeString
        {
            get
            {
                string typeString = "";
                switch (Type)
                {
                    case "Coperation":
                        typeString = "合同";
                        break;
                    case "Default":
                        typeString = "首页";
                        break;
                    case "Customer":
                        typeString = "客户";
                        break;
                    case "Project":
                        typeString = "项目";
                        break;
                    case "ProjectShow":
                        typeString = "项目展示";
                        break;
                    case "LeaderShip":
                        typeString = "领导驾驶舱";
                        break;
                    case "SystemConfig":
                        typeString = "系统设置";
                        break;
                    case "JiXiao":
                        typeString = "绩效管理";
                        break;
                    case "KaoQin":
                        typeString = "考勤管理";
                        break;
                }
                return typeString;
            }
        }

        [DataMapping("Power")]
        public string Power { get; set; }

        [DataMapping("PreviewPower")]
        public int PreviewPower { get; set; }

        public string PowerString
        {
            get
            {
                string powerString = "";
                return powerString;
            }
        }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }

        [DataMapping("InUserName")]
        public string InUserName { get; set; }

        public string RoleNameString { get; set; }
    }

    [Serializable]
    public class ActionPowerQueryEntity
    {
        public string PageName { get; set; }

        public LeftMenuType Type { get; set; }
    }

    public class ActionPowerParameterEntity
    {
        public int AllowEdit { get; set; }

        public int AllowDelete { get; set; }

        public int PreviewPattern { get; set; }
    }

    public class RolePowerParameterEntity
    {
        [DataMapping("Power")]
        public string Power { get; set; }

        private string[] powerArray = null;

        public string[] PowerArray
        {
            get
            {
                if (string.IsNullOrEmpty(Power))
                    return null;
                return Power.Split(',');
            }
        }

        public string AllowEdit
        {
            get
            {
                return PowerArray[1];
            }
        }

        public string AllowDelete
        {
            get
            {
                return PowerArray[2];
            }
        }

        public string PreviewPattern
        {
            get
            {
                return PowerArray[0];
            }
        }
    }
}
