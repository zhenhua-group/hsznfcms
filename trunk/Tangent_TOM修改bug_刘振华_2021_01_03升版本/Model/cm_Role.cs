﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class cm_Role
    {
        /// <summary>
        /// 系统号
        /// </summary>
        public int SysNo { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public string Users { get; set; }
    }

    [Serializable]
    public class RoleQueryEntity
    {
        public int RoleSysNo { get; set; }

        public string RoleName { get; set; }
    }

    public class RoleViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("RoleName")]
        public string RoleName { get; set; }

        [DataMapping("Users")]
        public string Users { get; set; }

        public Dictionary<int, string> DictionaryUser { get; set; }

        public string UserNameViewString
        {
            get
            {
                string userName = "";
                int i = 0;
                foreach (KeyValuePair<int, string> user in DictionaryUser)
                {
                    //if (i < 10)
                    //{
                    userName += user.Value + ",  ";
                    //}
                    //i++;
                }
                userName = userName.Trim().Substring(0, userName.Trim().Length - 1);

                return userName;
            }
        }
    }
}
