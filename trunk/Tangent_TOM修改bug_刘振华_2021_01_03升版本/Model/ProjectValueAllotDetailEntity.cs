﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class ProjectValueAllotDetailEntity
    {
        public int SysNo { get; set; }

        public int ProNo { get; set; }

        public string AuditUser { get; set; }

        public List<ProjectStageValueEntity> Stage { get; set; }

        public List<ProjectStageSpeValueEntity> StageSpe { get; set; }

        public List<ProjectDesignProcessValueEntity> DesignProcess { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessTwo { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessThree { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessFour { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessFive { get; set; }

        public List<ProjectOutDoorValueEntity> OutDoor { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public int type { get; set; }

        public List<DesinManagerUserEntity> DesinManagerUser { get; set; }
    }

    /// <summary>
    /// 设总产值
    /// </summary>
    public class DesinManagerUserEntity
    {
        public int mem_ID { get; set; }
        public decimal DesignManagerPercent { get; set; }
        public decimal DesignManagerCount { get; set; }
        public string DesignType { get; set; }
    }
    /// <summary>
    /// 项目各阶段产值
    /// </summary>
    [Serializable]
    public class ProjectStageValueEntity
    {
        public int ID { get; set; }
        public int AllotID { get; set; }
        public string ItemType { get; set; }
        public decimal ProgramPercent { get; set; }
        public decimal ProgramAmount { get; set; }
        public decimal preliminaryPercent { get; set; }
        public decimal preliminaryAmount { get; set; }
        public decimal WorkDrawPercent { get; set; }
        public decimal WorkDrawAmount { get; set; }
        public decimal LateStagePercent { get; set; }
        public decimal LateStageAmount { get; set; }

    }


    /// <summary>
    /// 各专业
    /// </summary>
    [Serializable]
    public class ProjectStageSpeValueEntity
    {
        public int ID { get; set; }
        public int AllotID { get; set; }
        public string Specialty { get; set; }
        public decimal ProgramPercent { get; set; }
        public decimal ProgramCount { get; set; }
        public decimal preliminaryPercent { get; set; }
        public decimal preliminaryCount { get; set; }
        public decimal WorkDrawPercent { get; set; }
        public decimal WorkDrawCount { get; set; }
        public decimal LateStagePercent { get; set; }
        public decimal LateStageCount { get; set; }
    }


    /// <summary>
    /// 室外工程
    /// </summary>
    [Serializable]
    public class ProjectOutDoorValueEntity
    {
        public int ID { get; set; }
        public int ALLOT { get; set; }
        public int Status { get; set; }
        public string Specialty { get; set; }
        public decimal SpecialtyPercent { get; set; }
        public decimal SpecialtyCount { get; set; }

    }

    /// <summary>
    /// 工序
    /// </summary>
    [Serializable]
    public class ProjectDesignProcessValueEntity
    {
        public int ID { get; set; }
        public int AllotID { get; set; }
        public int type { get; set; }
        public string Specialty { get; set; }
        public decimal? AuditPercent { get; set; }
        public decimal AuditAmount { get; set; }
        public decimal? SpecialtyHeadPercent { get; set; }
        public decimal SpecialtyHeadAmount { get; set; }
        public decimal? ProofreadPercent { get; set; }
        public decimal ProofreadAmount { get; set; }
        public decimal? DesignPercent { get; set; }
        public decimal DesignAmount { get; set; }
    }

    /// <summary>
    /// 人员
    /// </summary>
    [Serializable]
    public class ProjectValueAllotByMemberEntity
    {
        public int SysNo { get; set; }

        public int ProNo { get; set; }
        public string AuditUser { get; set; }
        public string Status { get; set; }
        public List<ProjectValueBymember> ValueByMember { get; set; }
    }

    /// <summary>
    /// 人员--经济所
    /// </summary>
    [Serializable]
    public class jjsProjectValueAllotByMemberEntity
    {
        public int SysNo { get; set; }

        public int ProNo { get; set; }
        public string AuditUser { get; set; }
        public List<ProjectValueBymember> ValueByMember { get; set; }

        public int typeStatus { get; set; }
        public int AllotID { get; set; }
        public decimal ProofreadPercent { get; set; }
        public decimal ProofreadCount { get; set; }
        public decimal BuildingPercent { get; set; }
        public decimal BuildingCount { get; set; }
        public decimal StructurePercent { get; set; }
        public decimal StructureCount { get; set; }
        public decimal DrainPercent { get; set; }
        public decimal DrainCount { get; set; }
        public decimal HavcPercent { get; set; }
        public decimal HavcCount { get; set; }
        public decimal ElectricPercent { get; set; }
        public decimal ElectricCount { get; set; }

        public decimal TotalBuildingPercent { get; set; }
        public decimal TotalBuildingCount { get; set; }
        public decimal TotalInstallationPercent { get; set; }
        public decimal TotalInstallationCount { get; set; }

        public List<DesinManagerUserEntity> DesinManagerUser { get; set; }
    }

    /// <summary>
    /// 人员--转经济所 转暖通所 
    /// </summary>
    [Serializable]
    public class TranProjectValueAllotByMemberEntity
    {
        public int SysNo { get; set; }
        public int AllotID { get; set; }
        public int ProNo { get; set; }
        public decimal? TotalCount { get; set; }
        public decimal? AllotCount { get; set; }
        public decimal? TranCount { get; set; }
        public string ItemType { get; set; }
        public string AuditUser { get; set; }
        public decimal? Thedeptallotpercent { get; set; }
        public decimal? Thedeptallotcount { get; set; }
        public decimal? AllotPercent { get; set; }
        public decimal? TranPercent { get; set; }
        public string ActualAllountTime { get; set; }
        public List<ProjectValueBymember> ValueByMember { get; set; }
    }

    /// <summary>
    /// 人员--转土建所 
    /// </summary>
    [Serializable]
    public class TranBulidingProjectValueAllotByMemberEntity
    {
        public int AllotID { get; set; }
        public int ProNo { get; set; }

        public string ItemType { get; set; }
        public string AuditUser { get; set; }

        public List<ProjectStageValueEntity> Stage { get; set; }

        public List<ProjectStageSpeValueEntity> StageSpe { get; set; }

        public List<ProjectDesignProcessValueEntity> DesignProcess { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessTwo { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessThree { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessFour { get; set; }
        public List<ProjectDesignProcessValueEntity> DesignProcessFive { get; set; }

        public List<ProjectOutDoorValueEntity> OutDoor { get; set; }

        public List<ProjectValueBymember> ValueByMember { get; set; }

    }

    /// <summary>
    /// 人员
    /// </summary>
    [Serializable]
    public class ProjectValueBymember
    {
        public int ID { get; set; }
        public int AllotID { get; set; }
        public int mem_ID { get; set; }
        public string DesignPercent { get; set; }
        public decimal DesignCount { get; set; }
        public string SpecialtyHeadPercent { get; set; }
        public decimal SpecialtyHeadCount { get; set; }
        public string AuditPercent { get; set; }
        public decimal AuditCount { get; set; }
        public string ProofreadPercent { get; set; }
        public decimal ProofreadCount { get; set; }
        public int ItemType { get; set; }
        //是否是领导班子
        public string IsHead { get; set; }
        //是否是外聘人员
        public string IsExternal { get; set; }
        //专业
        public string SpeName { get; set; }
    }
}
