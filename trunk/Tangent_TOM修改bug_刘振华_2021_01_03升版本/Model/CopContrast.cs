﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class CopContrast
    {
        /// <summary>
        /// 单位
        /// </summary>
        [DataMapping("Unit")]
        public string Unit { get; set; }
        /// <summary>
        /// 今年目标值
        /// </summary>
        [DataMapping("AllotUnitNow")]
        public int AllotUnitNow { get; set; }
        /// <summary>
        /// 去年--当月合同额
        /// </summary>
        [DataMapping("AcountMonth")]
        public decimal AcountMonth { get; set; }
        /// <summary>
        /// 去年--年度合同额
        /// </summary>
        [DataMapping("AcountYear")]
        public decimal AcountYear { get; set; }
        /// <summary>
        /// 去年---完成的目标值%
        /// </summary>
        public string beforeMubiao
        {
            get
            {
                try
                {
                    return (AcountMonth * 100 / AllotUnitLast).ToString("f2");
                }
                catch (Exception)
                {

                    return "0";
                }

            }

        }
        /// <summary>
        ///今年--当月合同额
        /// </summary>
        [DataMapping("AcountMonthNow")]
        public decimal AcountMonthNow { get; set; }
        /// <summary>
        /// 今年--年度合同额
        /// </summary>
        [DataMapping("AcountYearNow")]
        public decimal AcountYearNow { get; set; }
        /// <summary>
        /// 今年与去年的增减额度---月份
        /// </summary>
        public string TongBiMonthNow
        {
            get
            {
                try
                {
                    return ((AcountMonthNow - AcountMonth) * 100 / AcountMonth).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "0";
                }
            }

        }
        /// <summary>
        /// 今年与去年的增减额度---年份
        /// </summary>
        public string TongBiYearNow
        {
            get
            {
                try
                {
                    return ((AcountYearNow - AcountYear) * 100 / AcountYear).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "0";
                }
            }
        }
        /// <summary>
        ///jinnian dangyue wanghe 
        /// </summary>
        public string nowMubiao2
        {
            get
            {
                try
                {
                    return (AcountMonthNow * 100 / AllotUnitNow).ToString("f2");

                }
                catch (Exception)
                {
                    return "0";
                }
            }
        }
        /// <summary>
        /// 今年---完成目标值
        /// </summary>
        public string nowMubiao
        {
            get
            {
                try
                {
                    return (AcountYearNow * 100 / AllotUnitNow).ToString("f2");

                }
                catch (Exception)
                {
                    return "0";
                }
            }
        }
        /// <summary>
        /// 今年与去年--目标值的增减同比
        /// </summary>
        public string TongBiMuBiao
        {
            get
            {
                try
                {
                    return (Convert.ToDecimal(nowMubiao) - Convert.ToDecimal(beforeMubiao)).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "";
                }

            }

        }
        /// <summary>
        /// 今年与去年--目标值的增减同比2
        /// </summary>
        public string TongBiMuBiao2
        {
            get
            {
                try
                {
                    return (Convert.ToDecimal(nowMubiao2) - Convert.ToDecimal(beforeMubiao)).ToString("f2") + "%";
                }
                catch (Exception)
                {
                    return "";
                }

            }

        }
        /// <summary>
        /// 去年的合同目标值
        /// </summary>
        [DataMapping("AllotUnitLast")]
        public int AllotUnitLast { get; set; }


    }
}
