﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_member:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Training
	{
        public Training()
		{}
		#region Model
        private int _id;
        public int Id
        {
            set { _id = value; }
            get { return _id; }
        }

        private string _property;
        public string Property
        {
            set { _property = value; }
            get { return _property; }
        }

        private string _title;
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }

        private DateTime _beginDate;
        public DateTime BeginDate
        {
            set { _beginDate = value; }
            get { return _beginDate; }
        }

        private DateTime _endDate;
        public DateTime EndDate
        {
            set { _endDate = value; }
            get { return _endDate; }
        }

	    private decimal _baoMingFee;
        public decimal BaoMingFee
        {
            set { _baoMingFee = value; }
            get { return _baoMingFee; }
        }

        private decimal _zhuShuFee;
        public decimal ZhuShuFee
        {
            set { _zhuShuFee = value; }
            get { return _zhuShuFee; }
        }

        private decimal _jiPiaoFee;
        public decimal JiPiaoFee
        {
            set { _jiPiaoFee = value; }
            get { return _jiPiaoFee; }
        }

        private decimal _otherFee;
        public decimal OtherFee
        {
            set { _otherFee = value; }
            get { return _otherFee; }
        }

        private decimal _total;
        public decimal Total
        {
            set { _total = value; }
            get { return _total; }
        } 

        private string _canYuRen;
        public string CanYuRen
        {
            set { _canYuRen = value; }
            get { return _canYuRen; }
        }

        private string _unitName;
        public string UnitName
        {
            set { _unitName = value; }
            get { return _unitName; }
        }

        private string _year;
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }

        private string _month;
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }


        private string _count;
        public string Count
        {
            set { _count = value; }
            get { return _count; }
        }

        private string _totals;
        public string Totals
        {
            set { _totals = value; }
            get { return _totals; }
        }
		#endregion Model

	}
}

