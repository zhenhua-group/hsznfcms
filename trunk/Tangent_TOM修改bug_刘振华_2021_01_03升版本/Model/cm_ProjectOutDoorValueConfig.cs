﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
   public class cm_ProjectOutDoorValueConfig
    {
       #region Model
		private int _id;
		private string _type;
		private decimal? _bulidingpercent;
		private decimal? _structurepercent;
		private decimal? _drainpercent;
		private decimal? _hvacpercent;
		private decimal? _electricpercent;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? bulidingPercent
		{
			set{ _bulidingpercent=value;}
			get{return _bulidingpercent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? structurePercent
		{
			set{ _structurepercent=value;}
			get{return _structurepercent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? drainPercent
		{
			set{ _drainpercent=value;}
			get{return _drainpercent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? hvacPercent
		{
			set{ _hvacpercent=value;}
			get{return _hvacpercent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? electricPercent
		{
			set{ _electricpercent=value;}
			get{return _electricpercent;}
		}
		#endregion Model
    }
}
