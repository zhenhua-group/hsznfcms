﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_memberLevel:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_memberLevel
	{
		public tg_memberLevel()
		{}
		#region Model
		private int _id;
		private int? _mem_id=0;
		private int? _mem_year=0;
		private int? _mem_level=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Year
		{
			set{ _mem_year=value;}
			get{return _mem_year;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Level
		{
			set{ _mem_level=value;}
			get{return _mem_level;}
		}
		#endregion Model

	}
}

