﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    [Serializable]
    public class cm_jjsProjectValueAllotDetail
    {
        public cm_jjsProjectValueAllotDetail()
        { }
        #region Model
        private int _id;
        private int? _proid;
        private int? _typestatus;
        private decimal? _proofreadpercent;
        private decimal? _proofreadcount;
        private decimal? _buildingpercent;
        private decimal? _buildingcount;
        private decimal? _structurepercent;
        private decimal? _structurecount;
        private decimal? _drainpercent;
        private decimal? _draincount;
        private decimal? _havcpercent;
        private decimal? _havccount;
        private decimal? _electricpercent;
        private decimal? _electriccount;
        private decimal _totalbuildingpercent;
        private decimal _totalInstallationpercent;
        private decimal _totalbuildingcount;
        private decimal _totalInstallationcount;
        private int? _allotID;

       
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? proID
        {
            set { _proid = value; }
            get { return _proid; }
        }

        public int? AllotID
        {
            get { return _allotID; }
            set { _allotID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? typeStatus
        {
            set { _typestatus = value; }
            get { return _typestatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProofreadPercent
        {
            set { _proofreadpercent = value; }
            get { return _proofreadpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProofreadCount
        {
            set { _proofreadcount = value; }
            get { return _proofreadcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BuildingPercent
        {
            set { _buildingpercent = value; }
            get { return _buildingpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BuildingCount
        {
            set { _buildingcount = value; }
            get { return _buildingcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? StructurePercent
        {
            set { _structurepercent = value; }
            get { return _structurepercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? StructureCount
        {
            set { _structurecount = value; }
            get { return _structurecount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DrainPercent
        {
            set { _drainpercent = value; }
            get { return _drainpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DrainCount
        {
            set { _draincount = value; }
            get { return _draincount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HavcPercent
        {
            set { _havcpercent = value; }
            get { return _havcpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HavcCount
        {
            set { _havccount = value; }
            get { return _havccount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ElectricPercent
        {
            set { _electricpercent = value; }
            get { return _electricpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ElectricCount
        {
            set { _electriccount = value; }
            get { return _electriccount; }
        }

        /// <summary>
        /// 合计
        /// </summary>
        public decimal Totalbuildingpercent
        {
            get { return _totalbuildingpercent; }
            set { _totalbuildingpercent = value; }
        }


        /// <summary>
        /// 合计
        /// </summary>
        public decimal TotalInstallationpercent
        {
            get { return _totalInstallationpercent; }
            set { _totalInstallationpercent = value; }
        }

        public decimal Totalbuildingcount
        {
            get { return _totalbuildingcount; }
            set { _totalbuildingcount = value; }
        }


        public decimal TotalInstallationcount
        {
            get { return _totalInstallationcount; }
            set { _totalInstallationcount = value; }
        }
        #endregion Model
    }
}
