﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoheRenwuHistory:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoheRenwuHistory
	{
		public cm_KaoheRenwuHistory()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _renwuname;
		/// <summary>
		/// 
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenWuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenWuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		#endregion Model

	}
}

