﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ContactPersionInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ContactPersionInfo
	{
		public cm_ContactPersionInfo()
		{}
		#region Model
		private int _cp_id;
        private decimal _cst_id;
		private string _contactno;
		private string _name;
		private string _duties;
		private string _department;
		private string _phone;
		private string _bphone;
		private string _fphone;
		private string _ffax;
		private string _bfax;
		private string _remark;
		private byte[] _callingcard;
		private string _email;
		private int? _updateby;
		private DateTime? _lastupdate;
		/// <summary>
		/// 
		/// </summary>
		public int CP_Id
		{
			set{ _cp_id=value;}
			get{return _cp_id;}
		}
		/// <summary>
		/// 
		/// </summary>
        public decimal Cst_Id
		{
			set{ _cst_id=value;}
			get{return _cst_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ContactNo
		{
			set{ _contactno=value;}
			get{return _contactno;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Duties
		{
			set{ _duties=value;}
			get{return _duties;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Department
		{
			set{ _department=value;}
			get{return _department;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Phone
		{
			set{ _phone=value;}
			get{return _phone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BPhone
		{
			set{ _bphone=value;}
			get{return _bphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FPhone
		{
			set{ _fphone=value;}
			get{return _fphone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FFax
		{
			set{ _ffax=value;}
			get{return _ffax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BFax
		{
			set{ _bfax=value;}
			get{return _bfax;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 
		/// </summary>
		public byte[] CallingCard
		{
			set{ _callingcard=value;}
			get{return _callingcard;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UpdateBy
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastUpdate
		{
			set{ _lastupdate=value;}
			get{return _lastupdate;}
		}
		#endregion Model

	}
}

