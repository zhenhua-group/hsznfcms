﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
        public class CoperationAcountOfMoneyViewEntity
        {
                [DataMapping("year")]
                public int Year { get; set; }

                [DataMapping("money")]
                public decimal Money { get; set; }
        }

        public class CoperationCountViewEntity
        {
                [DataMapping("year")]
                public int Year { get; set; }

                [DataMapping("count")]
                public int Count { get; set; }
        }

        public class CoperationListViewEntity
        {
                [DataMapping("cpr_Id")]
                public int SysNo { get; set; }

                [DataMapping("cst_Id")]
                public int UserSysNo { get; set; }

                [DataMapping("cpr_No")]
                public string CoperationNo { get; set; }

                [DataMapping("cpr_Type")]
                public string CoperationType { get; set; }

                [DataMapping("cpr_Type2")]
                public string CoperationType2 { get; set; }

                [DataMapping("cpr_Name")]
                public string CoperationName { get; set; }

                [DataMapping("cpr_Unit")]
                public string Unit { get; set; }

                [DataMapping("cpr_Acount")]
                public decimal CoperationAccount { get; set; }

                [DataMapping("cpr_ShijiAcount")]
                public decimal CoperationRealAccount { get; set; }

                [DataMapping("cpr_Touzi")]
                public decimal CoperationInvestment { get; set; }

                [DataMapping("cpr_ShijiTouzi")]
                public decimal CoperationRealInvestment { get; set; }

                [DataMapping("cpr_Process")]
                public string CoperationProcess { get; set; }

                [DataMapping("cpr_SignDate")]
                public DateTime SignDate { get; set; }

                [DataMapping("cpr_DoneDate")]
                public DateTime DoneDate { get; set; }

                [DataMapping("cpr_Mark")]
                public string CoperationRemark { get; set; }

                [DataMapping("BuildArea")]
                public string BuildArea { get; set; }

                [DataMapping("ChgPeople")]
                public string ChargeMan { get; set; }

                [DataMapping("ChgPhone")]
                public string ChargePhone { get; set; }

                [DataMapping("ChgJia")]
                public string ChargeManJia { get; set; }

                [DataMapping("ChgJiaPhone")]
                public string ChargeManJiaPhone { get; set; }

                [DataMapping("BuildPosition")]
                public string BuildPosition { get; set; }

                [DataMapping("Industry")]
                public string Industry { get; set; }

                [DataMapping("BuildUnit")]
                public string BuildUnit { get; set; }

                [DataMapping("BuildSrc")]
                public string BuildSrc { get; set; }

                [DataMapping("TableMaker")]
                public string TableMaker { get; set; }

                [DataMapping("RegTime")]
                public DateTime RegisterTime { get; set; }

                public string RegisterTimeString { get { return SignDate.ToString("yyyy-MM-dd HH:mm"); } }
        }

        public class CoperationListViewEntityTwo : CoperationListViewEntity
        {
                [DataMapping("pro_Status")]
                public string ProjectStatus { get; set; }

                public string ProjectStatusString
                {
                        get
                        {
                                string projectStatusString = "进行中";
                                if (ProjectStatus.Contains("暂停"))
                                {
                                        projectStatusString = "暂停";
                                }
                                else if (ProjectStatus.Contains("完成"))
                                {
                                        projectStatusString = "完成";
                                }
                                return projectStatusString;
                        }
                }
        }
}
