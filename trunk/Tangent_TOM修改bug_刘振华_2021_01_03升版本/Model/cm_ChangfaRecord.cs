﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ChangfaRecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ChangfaRecord
	{
		public cm_ChangfaRecord()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private DateTime? _mem_chengfadate;
		private string _mem_chengfawhy;
		private string _mem_chengfasub;
		private int? _insertuserid;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_ChengfaDate
		{
			set{ _mem_chengfadate=value;}
			get{return _mem_chengfadate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_ChengfaWhy
		{
			set{ _mem_chengfawhy=value;}
			get{return _mem_chengfawhy;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_ChengfaSub
		{
			set{ _mem_chengfasub=value;}
			get{return _mem_chengfasub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		#endregion Model

	}
}

