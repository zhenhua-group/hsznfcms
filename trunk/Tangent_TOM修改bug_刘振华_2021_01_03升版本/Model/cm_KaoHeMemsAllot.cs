﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeMemsAllot:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeMemsAllot
	{
		public cm_KaoHeMemsAllot()
		{}
		#region Model
		private int _id;
		private int _prokhnameid;
		private int _allotid;
		private int _speid;
		private string _spename;
		private int _memid;
		private string _memname;
		private decimal _zt;
		private decimal _xd;
		private decimal _sh;
		private decimal _fa;
		private decimal _zc;
		private decimal _allotcount;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int ProKHNameId
		{
			set{ _prokhnameid=value;}
			get{return _prokhnameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int AllotID
		{
			set{ _allotid=value;}
			get{return _allotid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeID
		{
			set{ _speid=value;}
			get{return _speid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zt
		{
			set{ _zt=value;}
			get{return _zt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Xd
		{
			set{ _xd=value;}
			get{return _xd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Sh
		{
			set{ _sh=value;}
			get{return _sh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Fa
		{
			set{ _fa=value;}
			get{return _fa;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal Zc
		{
			set{ _zc=value;}
			get{return _zc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal AllotCount
		{
			set{ _allotcount=value;}
			get{return _allotcount;}
		}
		#endregion Model

	}
}

