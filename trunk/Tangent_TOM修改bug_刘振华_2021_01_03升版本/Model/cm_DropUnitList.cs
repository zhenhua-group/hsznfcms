﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_DropUnitList:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_DropUnitList
	{
		public cm_DropUnitList()
		{}
		#region Model
		private int _id;
		private int? _unit_id;
		private int? _flag=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? unit_ID
		{
			set{ _unit_id=value;}
			get{return _unit_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? flag
		{
			set{ _flag=value;}
			get{return _flag;}
		}
		#endregion Model

	}
}

