﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_CostCompanyConfig:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_CostCompanyConfig
	{
		public cm_CostCompanyConfig()
		{}
		#region Model
		private int _id;
		private decimal? _cmpmngprt;
		private decimal? _procostprt;
		private decimal? _prochgprt;
		private decimal? _reguserprt;
		private decimal? _designprt;
		private decimal? _auditprt;
		private decimal? _totaluser;
		private decimal? _collegeuser;
		private int? _used;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? CmpMngPrt
		{
			set{ _cmpmngprt=value;}
			get{return _cmpmngprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ProCostPrt
		{
			set{ _procostprt=value;}
			get{return _procostprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ProChgPrt
		{
			set{ _prochgprt=value;}
			get{return _prochgprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? RegUserPrt
		{
			set{ _reguserprt=value;}
			get{return _reguserprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? DesignPrt
		{
			set{ _designprt=value;}
			get{return _designprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AuditPrt
		{
			set{ _auditprt=value;}
			get{return _auditprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? TotalUser
		{
			set{ _totaluser=value;}
			get{return _totaluser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? CollegeUser
		{
			set{ _collegeuser=value;}
			get{return _collegeuser;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Used
		{
			set{ _used=value;}
			get{return _used;}
		}
		#endregion Model

	}
}

