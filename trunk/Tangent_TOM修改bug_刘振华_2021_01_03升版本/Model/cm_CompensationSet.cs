﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_CompensationSet:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CompensationSet
    {
        public cm_CompensationSet()
        { }
        #region Model
        private int _id;
        private int? _unitid;
        private int? _memberid;
        private decimal? _comvalue;
        private string _valueyear;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UnitId
        {
            set { _unitid = value; }
            get { return _unitid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? MemberId
        {
            set { _memberid = value; }
            get { return _memberid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ComValue
        {
            set { _comvalue = value; }
            get { return _comvalue; }
        }      
        /// <summary>
        /// 
        /// </summary>
        public string ValueYear
        {
            set { _valueyear = value; }
            get { return _valueyear; }
        }
        #endregion Model
    }
}