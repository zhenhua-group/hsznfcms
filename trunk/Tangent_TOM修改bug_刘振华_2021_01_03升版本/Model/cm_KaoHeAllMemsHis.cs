﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeAllMemsHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeAllMemsHis
	{
		public cm_KaoHeAllMemsHis()
		{}
		#region Model
		private int _id;
		private int? _renwuid=0;
		private string _unitname;
		private string _username;
		private string _isfired;
		private decimal? _bmjltz=0M;
		private decimal? _gstz=0M;
		private decimal? _yfjj=0M;
		private decimal? _xmjjjs=0M;
		private decimal? _bmjjjs=0M;
		private decimal? _xmjj=0M;
		private decimal? _bmnjj=0M;
		private decimal? _xmglj=0M;
		private decimal? _yxyg=0M;
		private decimal? _tcgx=0M;
		private decimal? _bim=0M;
		private decimal? _ghbz=0M;
		private decimal? _hj=0M;
        private decimal? _zcfy = 0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			set{ _username=value;}
			get{return _username;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string IsFired
		{
			set{ _isfired=value;}
			get{return _isfired;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMJLTZ
		{
			set{ _bmjltz=value;}
			get{return _bmjltz;}
		}
        /// <summary>
        /// 
        /// </summary>
        public decimal? ZCFY
        {
            set { _zcfy = value; }
            get { return _zcfy; }
        }
		/// <summary>
		/// 
		/// </summary>
		public decimal? GSTZ
		{
			set{ _gstz=value;}
			get{return _gstz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? YFJJ
		{
			set{ _yfjj=value;}
			get{return _yfjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMJJJS
		{
			set{ _xmjjjs=value;}
			get{return _xmjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMJJJS
		{
			set{ _bmjjjs=value;}
			get{return _bmjjjs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMJJ
		{
			set{ _xmjj=value;}
			get{return _xmjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BMNJJ
		{
			set{ _bmnjj=value;}
			get{return _bmnjj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? XMGLJ
		{
			set{ _xmglj=value;}
			get{return _xmglj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? YXYG
		{
			set{ _yxyg=value;}
			get{return _yxyg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? TCGX
		{
			set{ _tcgx=value;}
			get{return _tcgx;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? BIM
		{
			set{ _bim=value;}
			get{return _bim;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? GHBZ
		{
			set{ _ghbz=value;}
			get{return _ghbz;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? HJ
		{
			set{ _hj=value;}
			get{return _hj;}
		}
		#endregion Model

	}
}

