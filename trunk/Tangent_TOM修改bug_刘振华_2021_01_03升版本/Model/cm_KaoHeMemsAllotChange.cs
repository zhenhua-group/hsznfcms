﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeMemsAllotChange:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeMemsAllotChange
	{
		public cm_KaoHeMemsAllotChange()
		{}
		#region Model
		private int _id;
		private int _renwuid;
		private int? _isfired=0;
		private int _unitid;
		private string _unitname;
		private int _speid;
		private string _spename;
		private int _memid;
		private string _memname;
		private decimal _allotcount;
		private int? _unitorder=0;
		private int? _unitjudgeorder=0;
		private decimal _allotcountbefore;
		private int? _stat=0;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? IsFired
		{
			set{ _isfired=value;}
			get{return _isfired;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string UnitName
		{
			set{ _unitname=value;}
			get{return _unitname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int SpeID
		{
			set{ _speid=value;}
			get{return _speid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SpeName
		{
			set{ _spename=value;}
			get{return _spename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemID
		{
			set{ _memid=value;}
			get{return _memid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string MemName
		{
			set{ _memname=value;}
			get{return _memname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal AllotCount
		{
			set{ _allotcount=value;}
			get{return _allotcount;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitOrder
		{
			set{ _unitorder=value;}
			get{return _unitorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitJudgeOrder
		{
			set{ _unitjudgeorder=value;}
			get{return _unitjudgeorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal AllotCountBefore
		{
			set{ _allotcountbefore=value;}
			get{return _allotcountbefore;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

