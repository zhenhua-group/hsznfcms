﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
   public  class cm_Divideinfo
	{
		public cm_Divideinfo()
		{}
		#region Model
		private int _id;
		private int? _unitid;
		private string _divideyear;
		private decimal? _dividepercent;
		private int? _insertuserid;
		private DateTime? _insertdate;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UnitID
		{
			set{ _unitid=value;}
			get{return _unitid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DivideYear
		{
			set{ _divideyear=value;}
			get{return _divideyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? DividePercent
		{
			set{ _dividepercent=value;}
			get{return _dividepercent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate
		{
			set{ _insertdate=value;}
			get{return _insertdate;}
		}
		#endregion Model

	}
}

