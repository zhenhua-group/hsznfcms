﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoheProjType:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoheProjType
	{
		public cm_KaoheProjType()
		{}
		#region Model
		private int _id;
		private int _kaohenameid;
		private int _kaohetypeid;
		private string _kaohetypename;
		private decimal _realscale;
		private decimal _typexishu;
		private int _insertuserid;
		private DateTime? _insertdate= DateTime.Now;
		private decimal? _virallotcount=0M;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int KaoheNameID
		{
			set{ _kaohenameid=value;}
			get{return _kaohenameid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int KaoheTypeID
		{
			set{ _kaohetypeid=value;}
			get{return _kaohetypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string KaoheTypeName
		{
			set{ _kaohetypename=value;}
			get{return _kaohetypename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal RealScale
		{
			set{ _realscale=value;}
			get{return _realscale;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal TypeXishu
		{
			set{ _typexishu=value;}
			get{return _typexishu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? InsertDate
		{
			set{ _insertdate=value;}
			get{return _insertdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? virallotCount
		{
			set{ _virallotcount=value;}
			get{return _virallotcount;}
		}
		#endregion Model

	}
}

