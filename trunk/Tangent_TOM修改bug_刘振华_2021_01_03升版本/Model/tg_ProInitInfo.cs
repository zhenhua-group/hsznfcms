﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_ProInitInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_ProInitInfo
	{
		public tg_ProInitInfo()
		{}
		#region Model
		private string _info_name;
		private string _info_val;
		/// <summary>
		/// 
		/// </summary>
		public string Info_Name
		{
			set{ _info_name=value;}
			get{return _info_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Info_Val
		{
			set{ _info_val=value;}
			get{return _info_val;}
		}
		#endregion Model

	}
}

