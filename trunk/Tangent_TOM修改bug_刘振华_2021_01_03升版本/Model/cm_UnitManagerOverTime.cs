﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_UnitManagerOverTime:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_UnitManagerOverTime
	{
		public cm_UnitManagerOverTime()
		{}
		#region Model
		private int _id;
		private int _manager_id;
		private string _manager_name;
		private int _mem_id;
		private string _mem_name;
		private int? _over_year;
		private int? _over_month;
		private string _over_type;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int manager_id
		{
			set{ _manager_id=value;}
			get{return _manager_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string manager_name
		{
			set{ _manager_name=value;}
			get{return _manager_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_name
		{
			set{ _mem_name=value;}
			get{return _mem_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? over_year
		{
			set{ _over_year=value;}
			get{return _over_year;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? over_month
		{
			set{ _over_month=value;}
			get{return _over_month;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string over_type
		{
			set{ _over_type=value;}
			get{return _over_type;}
		}
		#endregion Model

	}
}

