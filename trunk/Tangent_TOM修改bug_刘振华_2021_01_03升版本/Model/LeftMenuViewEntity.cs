﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;


namespace TG.Model
{
    public class LeftMenuViewEntity
    {
        [DataMapping("SysNo")]
        public int SysNo { get; set; }

        [DataMapping("Description")]
        public string Description { get; set; }

        [DataMapping("ElementID")]
        public string ElementID { get; set; }

        [DataMapping("Role")]
        public string Role { get; set; }

        public string RoleNameString { get; set; }

        [DataMapping("Type")]
        public string Type { get; set; }

        public string TypeString
        {
            get
            {
                string typeString = "";
                switch (Type)
                {
                    case "Coperation":
                        typeString = "合同";
                        break;
                    case "Default":
                        typeString = "首页";
                        break;
                    case "Customer":
                        typeString = "客户";
                        break;
                    case "Project":
                        typeString = "项目";
                        break;
                    case "ProjectShow":
                        typeString = "项目展示";
                        break;
                    case "ThechQu":
                        typeString = "技术质量";
                        break;
                    case "LeaderShip":
                        typeString = "领导驾驶舱";
                        break;
                    case "SystemConfig":
                        typeString = "系统设置";
                        break;
                    case "JiXiao":
                        typeString = "绩效管理";
                        break;
                    case "KaoQin":
                        typeString = "考勤管理";
                        break;
                }
                return typeString;
            }
        }

        [DataMapping("InUser")]
        public int InUser { get; set; }

        [DataMapping("InUserName")]
        public string InUserName { get; set; }

        [DataMapping("InDate")]
        public DateTime InDate { get; set; }
    }

    public enum LeftMenuType
    {
        All = 0,
        /// <summary>
        /// 合同
        /// </summary>
        Coperation = 1,
        /// <summary>
        /// 首页
        /// </summary>
        Default = 2,
        /// <summary>
        /// 客户
        /// </summary>
        Customer = 3,
        /// <summary>
        /// 项目
        /// </summary>
        Project = 4,
        /// <summary>
        /// 项目展示
        /// </summary>
        ProjectShow = 5,
        /// <summary>
        /// 领导驾驶舱
        /// </summary>
        LeaderShip = 6,
        /// <summary>
        /// 系统设置
        /// </summary>
        SystemConfig = 7,
        /// <summary>
        /// 技术质量
        /// </summary>
        ThechQu = 8,
        /// <summary>
        /// 绩效
        /// </summary>
        JiXiao = 9,
        /// <summary>
        /// 考勤
        /// </summary>
        KaoQin=10

    }
}
