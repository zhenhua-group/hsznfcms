﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_ApplyStatisDetailHis:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_ApplyStatisDetailHis
	{
		public cm_ApplyStatisDetailHis()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private string _mem_name;
		private int _mem_unit_id;
		private string _datadate;
        private string _time_leave ;
        private string _time_sick ;
        private string _time_over;
        private string _time_marry;
        private string _time_mater;
        private string _time_die;
        private string _time_late;
        private string _isHoliday;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_id
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_name
		{
			set{ _mem_name=value;}
			get{return _mem_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_unit_ID
		{
			set{ _mem_unit_id=value;}
			get{return _mem_unit_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string dataDate
		{
			set{ _datadate=value;}
			get{return _datadate;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string time_leave
		{
			set{ _time_leave=value;}
			get{return _time_leave;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string time_sick
		{
			set{ _time_sick=value;}
			get{return _time_sick;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string time_over
		{
			set{ _time_over=value;}
			get{return _time_over;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string time_marry
		{
			set{ _time_marry=value;}
			get{return _time_marry;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string time_mater
		{
			set{ _time_mater=value;}
			get{return _time_mater;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string time_die
		{
			set{ _time_die=value;}
			get{return _time_die;}
		}
		/// <summary>
		/// 
		/// </summary>
        public string time_late
		{
			set{ _time_late=value;}
			get{return _time_late;}
		}
        /// <summary>
        /// 
        /// </summary>
        public string isHoliday
        {
            set { _isHoliday = value; }
            get { return _isHoliday; }
        }
		#endregion Model

	}
}

