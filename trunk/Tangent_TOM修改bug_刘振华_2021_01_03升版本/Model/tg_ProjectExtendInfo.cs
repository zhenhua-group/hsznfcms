﻿using System;
using TG.Common.EntityBuilder;
using System.Data;

namespace TG.Model
{
	/// <summary>
	/// tg_ProjectExtendInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_ProjectExtendInfo
	{
		public tg_ProjectExtendInfo()
		{ }
		#region Model
		private int _proextendinfoid;
		private int _project_id;
		private int? _pro_src;
		private string _chgjia;
		private string _phone;
		private string _project_reletive;
		private decimal? _cpr_acount;
		private string _unit;
		private decimal? _projectscale;
		private string _buildaddress;
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("ProExtendInfoid")]
		public int ProExtendInfoid
		{
			set { _proextendinfoid = value; }
			get { return _proextendinfoid; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("project_Id")]
		public int project_Id
		{
			set { _project_id = value; }
			get { return _project_id; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("Pro_src")]
		public int? Pro_src
		{
			set { _pro_src = value; }
			get { return _pro_src; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("ChgJia")]
		public string ChgJia
		{
			set { _chgjia = value; }
			get { return _chgjia; }
		}

		/// <summary>
		/// 
		/// </summary>
		[DataMapping("Phone")]
		public string Phone
		{
			set { _phone = value; }
			get { return _phone; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("Project_reletive")]
		public string Project_reletive
		{
			set { _project_reletive = value; }
			get { return _project_reletive; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("Cpr_Acount")]
		public decimal? Cpr_Acount
		{
			set { _cpr_acount = value; }
			get { return _cpr_acount; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("Unit")]
		public string Unit
		{
			set { _unit = value; }
			get { return _unit; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("ProjectScale")]
		public decimal? ProjectScale
		{
			set { _projectscale = value; }
			get { return _projectscale; }
		}
		/// <summary>
		/// 
		/// </summary>
		[DataMapping("BuildAddress")]
		public string BuildAddress
		{
			set { _buildaddress = value; }
			get { return _buildaddress; }
		}
		#endregion Model

	}
}

