﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_ProjectDesignProcessValueConfig:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_ProjectDesignProcessValueConfig
    {
        public cm_ProjectDesignProcessValueConfig()
        { }
        #region Model
        private int _id;
        private string _specialty;
        private decimal? _auditpercent;
        private decimal? _specialtyheadpercent;
        private decimal? _proofreadpercent;
        private decimal? _designpercent;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Specialty
        {
            set { _specialty = value; }
            get { return _specialty; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? AuditPercent
        {
            set { _auditpercent = value; }
            get { return _auditpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? SpecialtyHeadPercent
        {
            set { _specialtyheadpercent = value; }
            get { return _specialtyheadpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ProofreadPercent
        {
            set { _proofreadpercent = value; }
            get { return _proofreadpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DesignPercent
        {
            set { _designpercent = value; }
            get { return _designpercent; }
        }
        #endregion Model


    }

    [Serializable]
    public class cm_ProjectDesignProcessValueConfigEntity
    {

        public int status { get; set; }

        public string type { get; set; }

        public List<cm_ProjectDesignProcessValueEntity> Specialty { get; set; }

        public int IsHaved { get; set; }
    }


    [Serializable]
    public class cm_ProjectDesignProcessValueEntity
    {
        public string SpecialtyName { get; set; }
        public string SpecialtyValue { get; set; }
    }
}
