﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_KaoHeAllMemsAllotRecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_KaoHeAllMemsAllotRecord
	{
		public cm_KaoHeAllMemsAllotRecord()
		{}
		#region Model
		private int _id;
		private int _renwuid;
		private string _renwuname;
		private int _allotuserid;
		private DateTime _inserdate;

		private int _fromuserid;
		private int? _stat=0;
        private int _allotuserids;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int RenwuID
		{
			set{ _renwuid=value;}
			get{return _renwuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RenwuName
		{
			set{ _renwuname=value;}
			get{return _renwuname;}
		}
		/// <summary>
		/// 
		/// </summary>
        public int AllotUserID
        {
             set{ _allotuserid=value;}
             get{return _allotuserid;}
            
        }
        public int Gid
        {
            set { _allotuserids = value; }
            get { return _allotuserids; }

        }
		/// <summary>
		/// 
		/// </summary>
		public DateTime InserDate
		{
			set{ _inserdate=value;}
			get{return _inserdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int FromUserID
		{
			set{ _fromuserid=value;}
			get{return _fromuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? Stat
		{
			set{ _stat=value;}
			get{return _stat;}
		}
		#endregion Model

	}
}

