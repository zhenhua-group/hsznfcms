﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_Level:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_Level
	{
		public tg_Level()
		{}
		#region Model
		private int _id;
		private string _levelname;
		private decimal? _levelcount;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LevelName
		{
			set{ _levelname=value;}
			get{return _levelname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? LevelCount
		{
			set{ _levelcount=value;}
			get{return _levelcount;}
		}
		#endregion Model

	}
}

