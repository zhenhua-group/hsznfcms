﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_SheBaoChangeRecord:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_SheBaoChangeRecord
	{
		public cm_SheBaoChangeRecord()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private int? _mem_shebaobase;
		private int? _mem_shebaobase2;
		private int? _mem_gongjijin;
		private int? _mem_gongjijin2;
		private DateTime? _changedate;
		private int? _insertuserid;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_SheBaobase
		{
			set{ _mem_shebaobase=value;}
			get{return _mem_shebaobase;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_SheBaobase2
		{
			set{ _mem_shebaobase2=value;}
			get{return _mem_shebaobase2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Gongjijin
		{
			set{ _mem_gongjijin=value;}
			get{return _mem_gongjijin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Gongjijin2
		{
			set{ _mem_gongjijin2=value;}
			get{return _mem_gongjijin2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? ChangeDate
		{
			set{ _changedate=value;}
			get{return _changedate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? InsertUserID
		{
			set{ _insertuserid=value;}
			get{return _insertuserid;}
		}
		#endregion Model

	}
}

