﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_memberExtInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_memberExtInfo
	{
		public tg_memberExtInfo()
		{}
		#region Model
		private int _id;
		private int _mem_id;
		private string _mem_code;
		private string _mem_codeaddr;
		private int _mem_age;
		private string _mem_school;
		private string _mem_schlevel;
		private string _mem_schspec;
		private DateTime? _mem_schoutdate;
		private string _mem_school2;
		private string _mem_schlevel2;
		private string _mem_schspec2;
		private DateTime? _mem_schoutdate2;
		private int? _mem_archlevel;
		private DateTime? _mem_archleveltime;
		private int? _mem_archreg;
		private DateTime? _mem_archregtime;
		private int? _mem_archregaddr;
		private DateTime? _mem_worktime;
		private int? _mem_workdiff;
		private string _mem_workdiffsub;
		private int? _mem_workyear;
		private int? _mem_worktemp;
		private DateTime? _mem_incompanytime;
		private int? _mem_incompanydiff;
		private string _mem_incompanydiffsub;
		private int? _mem_incompanyyear;
		private int? _mem_incompanytemp;
		private string _mem_fileaddr;
		private int? _mem_holidaybase;
		private int? _mem_holidayyear;
		private string _mem_magpishi;
		private DateTime? _mem_zhuanzheng;
		private decimal? _mem_gongzi;
		private int? _mem_jiaotong;
		private int? _mem_tongxun;
		private decimal? _mem_yingfa;
		private int? _mem_shebaobase;
		private decimal? _mem_gongjijin;
		private string _mem_gongjijinsub;
		private decimal? _mem_yufa;
		private DateTime? _mem_coperationdate;
		private int? _mem_coperationtype;
		private DateTime? _mem_coperationdateend;
		private string _mem_coperationsub;
		private string _mem_outcompany;
		private DateTime? _mem_outcompanydate;
		private int? _mem_outcompanytype;
		private string _mem_outcompanysub;
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_ID
		{
			set{ _mem_id=value;}
			get{return _mem_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_Code
		{
			set{ _mem_code=value;}
			get{return _mem_code;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_CodeAddr
		{
			set{ _mem_codeaddr=value;}
			get{return _mem_codeaddr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int mem_Age
		{
			set{ _mem_age=value;}
			get{return _mem_age;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_School
		{
			set{ _mem_school=value;}
			get{return _mem_school;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_SchLevel
		{
			set{ _mem_schlevel=value;}
			get{return _mem_schlevel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_SchSpec
		{
			set{ _mem_schspec=value;}
			get{return _mem_schspec;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_SchOutDate
		{
			set{ _mem_schoutdate=value;}
			get{return _mem_schoutdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_School2
		{
			set{ _mem_school2=value;}
			get{return _mem_school2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_SchLevel2
		{
			set{ _mem_schlevel2=value;}
			get{return _mem_schlevel2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_SchSpec2
		{
			set{ _mem_schspec2=value;}
			get{return _mem_schspec2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_SchOutDate2
		{
			set{ _mem_schoutdate2=value;}
			get{return _mem_schoutdate2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_ArchLevel
		{
			set{ _mem_archlevel=value;}
			get{return _mem_archlevel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_ArchLevelTime
		{
			set{ _mem_archleveltime=value;}
			get{return _mem_archleveltime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_ArchReg
		{
			set{ _mem_archreg=value;}
			get{return _mem_archreg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_ArchRegTime
		{
			set{ _mem_archregtime=value;}
			get{return _mem_archregtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_ArchRegAddr
		{
			set{ _mem_archregaddr=value;}
			get{return _mem_archregaddr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_WorkTime
		{
			set{ _mem_worktime=value;}
			get{return _mem_worktime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_WorkDiff
		{
			set{ _mem_workdiff=value;}
			get{return _mem_workdiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_WorkDiffSub
		{
			set{ _mem_workdiffsub=value;}
			get{return _mem_workdiffsub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_WorkYear
		{
			set{ _mem_workyear=value;}
			get{return _mem_workyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_WorkTemp
		{
			set{ _mem_worktemp=value;}
			get{return _mem_worktemp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_InCompanyTime
		{
			set{ _mem_incompanytime=value;}
			get{return _mem_incompanytime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_InCompanyDiff
		{
			set{ _mem_incompanydiff=value;}
			get{return _mem_incompanydiff;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_InCompanyDiffSub
		{
			set{ _mem_incompanydiffsub=value;}
			get{return _mem_incompanydiffsub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_InCompanyYear
		{
			set{ _mem_incompanyyear=value;}
			get{return _mem_incompanyyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_InCompanyTemp
		{
			set{ _mem_incompanytemp=value;}
			get{return _mem_incompanytemp;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_FileAddr
		{
			set{ _mem_fileaddr=value;}
			get{return _mem_fileaddr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Holidaybase
		{
			set{ _mem_holidaybase=value;}
			get{return _mem_holidaybase;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_HolidayYear
		{
			set{ _mem_holidayyear=value;}
			get{return _mem_holidayyear;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_MagPishi
		{
			set{ _mem_magpishi=value;}
			get{return _mem_magpishi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_ZhuanZheng
		{
			set{ _mem_zhuanzheng=value;}
			get{return _mem_zhuanzheng;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? mem_GongZi
		{
			set{ _mem_gongzi=value;}
			get{return _mem_gongzi;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Jiaotong
		{
			set{ _mem_jiaotong=value;}
			get{return _mem_jiaotong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_Tongxun
		{
			set{ _mem_tongxun=value;}
			get{return _mem_tongxun;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? mem_YingFa
		{
			set{ _mem_yingfa=value;}
			get{return _mem_yingfa;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_SheBaobase
		{
			set{ _mem_shebaobase=value;}
			get{return _mem_shebaobase;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? mem_Gongjijin
		{
			set{ _mem_gongjijin=value;}
			get{return _mem_gongjijin;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_GongjijinSub
		{
			set{ _mem_gongjijinsub=value;}
			get{return _mem_gongjijinsub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? mem_Yufa
		{
			set{ _mem_yufa=value;}
			get{return _mem_yufa;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_CoperationDate
		{
			set{ _mem_coperationdate=value;}
			get{return _mem_coperationdate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_CoperationType
		{
			set{ _mem_coperationtype=value;}
			get{return _mem_coperationtype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_CoperationDateEnd
		{
			set{ _mem_coperationdateend=value;}
			get{return _mem_coperationdateend;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_CoperationSub
		{
			set{ _mem_coperationsub=value;}
			get{return _mem_coperationsub;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_OutCompany
		{
			set{ _mem_outcompany=value;}
			get{return _mem_outcompany;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? mem_OutCompanyDate
		{
			set{ _mem_outcompanydate=value;}
			get{return _mem_outcompanydate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? mem_OutCompanyType
		{
			set{ _mem_outcompanytype=value;}
			get{return _mem_outcompanytype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string mem_OutCompanySub
		{
			set{ _mem_outcompanysub=value;}
			get{return _mem_outcompanysub;}
		}

        public decimal? mem_YearCharge
        {
            get;
            set;
        }
		#endregion Model

	}
}

