﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    public class ProjectPlanRole
    {
        [DataMapping("ID")]
        public int SysNo { get; set; }

        [DataMapping("RoleName")]
        public string RoleName { get; set; }

        public List<ProjectPlanUser> Users { get; set; }

        public string UserString
        {
            get
            {
                string userString = "";
                int index = 0;
                Users.ForEach((user) =>
                {
                    userString += "<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]<a href=\"#\" style=\"margin-left:3px;\" class=\"remove\" id=\"resultUserRemoveA\">X</a></span>";
                    //满四个换行
                    index++;
                    if (index % 4 == 0)
                    {
                        userString += "<br/>";
                    }
                });
                return userString;
            }
        }
        public string UserStirngWithOutDeleteLink
        {
            get
            {
                string userString = "";
                int index = 0;
                Users.ForEach((user) =>
                {
                    userString += "<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>";
                    //满四个换行
                    index++;
                    if (index % 4 == 0)
                    {
                        userString += "<br/>";
                    }                
                });
                return userString;
            }
        }
    }

    public class ProjectPlanUser
    {
        [DataMapping("mem_ID")]
        public int UserSysNo { get; set; }

        [DataMapping("userName")]
        public string UserName { get; set; }

        [DataMapping("SpecialtyName")]
        public string SpecialtyName { get; set; }

        [DataMapping("mem_RoleIDs")]
        public string IncludeRoles { get; set; }
    }

    public class ProjectDesignPlanRole
    {

        public List<ProjectPlanUser> Users { get; set; }

        public string UserString
        {
            get
            {
                string userString = "";
                int index = 0;
                Users.ForEach((user) =>
                {
                    userString += "<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" userName=\"" + user.UserName + "\" userSpecialtyname=\"" + user.SpecialtyName + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]<a href=\"#\" style=\"margin-left:3px;\" id=\"resultUserRemoveA\">X</a></span>";
                    //满四个换行
                    index++;
                    if (index % 4 == 0)
                    {
                        userString += "<br/>";
                    }
                });
                return userString;
            }
        }
        public string UserStirngWithOutDeleteLink
        {
            get
            {
                string userString = "";
                int index = 0;
                Users.ForEach((user) =>
                {
                    userString += "<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>";
                    //满四个换行
                    index++;
                    if (index % 4 == 0)
                    {
                        userString += "<br/>";
                    }
                });
                return userString;
            }
        }
    }
}
