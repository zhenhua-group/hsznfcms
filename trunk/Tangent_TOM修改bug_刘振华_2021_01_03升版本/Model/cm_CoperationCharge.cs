﻿using System;
namespace TG.Model
{
    /// <summary>
    /// cm_CoperationCharge:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_CoperationCharge
    {
        public cm_CoperationCharge()
        { }
        #region Model
        private int _id;
        private decimal? _cpr_id;
        private string _times;
        private decimal? _persent;
        private decimal? _paycount;
        private decimal? _payshicount;
        private DateTime? _paytime;
        private string _acceptuser;
        private string _mark;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ID
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Times
        {
            set { _times = value; }
            get { return _times; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? persent
        {
            set { _persent = value; }
            get { return _persent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? payCount
        {
            set { _paycount = value; }
            get { return _paycount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? payShiCount
        {
            set { _payshicount = value; }
            get { return _payshicount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? paytime
        {
            set { _paytime = value; }
            get { return _paytime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string acceptuser
        {
            set { _acceptuser = value; }
            get { return _acceptuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mark
        {
            set { _mark = value; }
            get { return _mark; }
        }
        #endregion Model

    }
}

