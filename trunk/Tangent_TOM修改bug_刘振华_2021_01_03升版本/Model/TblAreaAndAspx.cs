﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{

    /// <summary>
    /// TblAreaAndAspx:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class TblAreaAndAspx
    {
        public TblAreaAndAspx()
        { }
        #region Model
        private int _id;
        private int? _areaid;
        private string _toaspx;
        private string _areaname;
        /// <summary>
        /// 
        /// </summary>
        public int Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AreaId
        {
            set { _areaid = value; }
            get { return _areaid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Toaspx
        {
            set { _toaspx = value; }
            get { return _toaspx; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string areaName
        {
            set { _areaname = value; }
            get { return _areaname; }
        }
        #endregion Model

    }
}

