﻿using System;
namespace TG.Model
{
	/// <summary>
	/// cm_MemArchLevel:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class cm_MemArchLevel
	{
		public cm_MemArchLevel()
		{}
		#region Model
		private int _archlevel_id;
		private string _name;
		private decimal? _allotprt;
		private int? _levelorder=0;
		private string _info;
		/// <summary>
		/// 
		/// </summary>
		public int ArchLevel_ID
		{
			set{ _archlevel_id=value;}
			get{return _archlevel_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? AllotPrt
		{
			set{ _allotprt=value;}
			get{return _allotprt;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LevelOrder
		{
			set{ _levelorder=value;}
			get{return _levelorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Info
		{
			set{ _info=value;}
			get{return _info;}
		}
		#endregion Model

	}
}

