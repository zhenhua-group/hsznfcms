﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_ProjectStageSpeValueConfig:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_ProjectStageSpeValueConfig
    {
        public cm_ProjectStageSpeValueConfig()
        { }
        #region Model
        private int _id;
        private string _projectstage;
        private decimal? _bulidingpercent;
        private decimal? _structurepercent;
        private decimal? _drainpercent;
        private decimal? _hvacpercent;
        private decimal? _electricpercent;
        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProjectStage
        {
            set { _projectstage = value; }
            get { return _projectstage; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? bulidingPercent
        {
            set { _bulidingpercent = value; }
            get { return _bulidingpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? structurePercent
        {
            set { _structurepercent = value; }
            get { return _structurepercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? drainPercent
        {
            set { _drainpercent = value; }
            get { return _drainpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? hvacPercent
        {
            set { _hvacpercent = value; }
            get { return _hvacpercent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? electricPercent
        {
            set { _electricpercent = value; }
            get { return _electricpercent; }
        }
        #endregion Model

    }

    [Serializable]
    public class cm_ProjectStageSpeValueConfigEntity
    {
        public string ProjectStage { get; set; }

        public List<Cm_ProjectStageSpeValeEntity> Specialty { get; set; }
        public int IsHaved { get; set; }
    }

    [Serializable]
    public class Cm_ProjectStageSpeValeEntity
    {
        public string SpecialtyName { get; set; }
        public string SpecialtyValue { get; set; }
    }
}
