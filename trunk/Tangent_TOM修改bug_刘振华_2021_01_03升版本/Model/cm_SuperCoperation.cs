﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.Model
{
    /// <summary>
    /// cm_SuperCoperation:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class cm_SuperCoperation
    {
        public cm_SuperCoperation()
        { }
        #region Model
        private int _cpr_id;
        private int? _cst_id;
        private string _cpr_no;
        private string _cpr_type;
        private string _cpr_type2;
        private string _cpr_name;
        private string _cpr_unit;
        private decimal? _cpr_acount;
        private decimal? _cpr_shijiacount;
        private decimal? _cpr_touzi;
        private decimal? _cpr_shijitouzi;
        private string _cpr_process;
        private DateTime? _cpr_signdate;
        private DateTime? _cpr_signdate2;
        private DateTime? _cpr_donedate;
        private string _cpr_mark;
        private decimal? _buildarea;
        private string _areaunit;
        private string _chgpeople;
        private string _chgphone;
        private string _chgjia;
        private string _chgjiaphone;
        private string _buildposition;
        private string _industry;
        private string _buildunit;
        private string _buildsrc;
        private string _tablemaker;
        private DateTime? _regtime;
        private string _updateby;
        private DateTime? _lastupdate;
        private string _buildtype;
        private string _structtype;
        private string _floor;
        private string _buildstructtype;
        private string _multibuild;
        private string _jiegou;
        private string _geips;
        private string _nuant;
        private string _dianq;
        private int? _insertuserid;
        private DateTime? _insertdate;
        private int? _isparamteredit;
        private int? _pmuserid;
        private int _cpr_fid;
        /// <summary>
        /// 
        /// </summary>
        public int cpr_Id
        {
            set { _cpr_id = value; }
            get { return _cpr_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? cst_Id
        {
            set { _cst_id = value; }
            get { return _cst_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_No
        {
            set { _cpr_no = value; }
            get { return _cpr_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type
        {
            set { _cpr_type = value; }
            get { return _cpr_type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Type2
        {
            set { _cpr_type2 = value; }
            get { return _cpr_type2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Name
        {
            set { _cpr_name = value; }
            get { return _cpr_name; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Unit
        {
            set { _cpr_unit = value; }
            get { return _cpr_unit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Acount
        {
            set { _cpr_acount = value; }
            get { return _cpr_acount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiAcount
        {
            set { _cpr_shijiacount = value; }
            get { return _cpr_shijiacount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_Touzi
        {
            set { _cpr_touzi = value; }
            get { return _cpr_touzi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? cpr_ShijiTouzi
        {
            set { _cpr_shijitouzi = value; }
            get { return _cpr_shijitouzi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Process
        {
            set { _cpr_process = value; }
            get { return _cpr_process; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_SignDate
        {
            set { _cpr_signdate = value; }
            get { return _cpr_signdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_SignDate2
        {
            set { _cpr_signdate2 = value; }
            get { return _cpr_signdate2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cpr_DoneDate
        {
            set { _cpr_donedate = value; }
            get { return _cpr_donedate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cpr_Mark
        {
            set { _cpr_mark = value; }
            get { return _cpr_mark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BuildArea
        {
            set { _buildarea = value; }
            get { return _buildarea; }
        }
        /// <summary>
        /// 建设规模的单位
        /// </summary>
        public string AreaUnit
        {
            set { _areaunit = value; }
            get { return _areaunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPeople
        {
            set { _chgpeople = value; }
            get { return _chgpeople; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgPhone
        {
            set { _chgphone = value; }
            get { return _chgphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJia
        {
            set { _chgjia = value; }
            get { return _chgjia; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChgJiaPhone
        {
            set { _chgjiaphone = value; }
            get { return _chgjiaphone; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildPosition
        {
            set { _buildposition = value; }
            get { return _buildposition; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Industry
        {
            set { _industry = value; }
            get { return _industry; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildUnit
        {
            set { _buildunit = value; }
            get { return _buildunit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildSrc
        {
            set { _buildsrc = value; }
            get { return _buildsrc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TableMaker
        {
            set { _tablemaker = value; }
            get { return _tablemaker; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RegTime
        {
            set { _regtime = value; }
            get { return _regtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UpdateBy
        {
            set { _updateby = value; }
            get { return _updateby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdate
        {
            set { _lastupdate = value; }
            get { return _lastupdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildType
        {
            set { _buildtype = value; }
            get { return _buildtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string StructType
        {
            set { _structtype = value; }
            get { return _structtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Floor
        {
            set { _floor = value; }
            get { return _floor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildStructType
        {
            set { _buildstructtype = value; }
            get { return _buildstructtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MultiBuild
        {
            set { _multibuild = value; }
            get { return _multibuild; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string JieGou
        {
            set { _jiegou = value; }
            get { return _jiegou; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string GeiPs
        {
            set { _geips = value; }
            get { return _geips; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NuanT
        {
            set { _nuant = value; }
            get { return _nuant; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DianQ
        {
            set { _dianq = value; }
            get { return _dianq; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? InsertUserID
        {
            set { _insertuserid = value; }
            get { return _insertuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? InsertDate
        {
            set { _insertdate = value; }
            get { return _insertdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? IsParamterEdit
        {
            set { _isparamteredit = value; }
            get { return _isparamteredit; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? PMUserID
        {
            set { _pmuserid = value; }
            get { return _pmuserid; }
        }

        //外聘人员
        private string _externalMember;
        /// <summary>
        /// 外聘人员
        /// </summary>
        public string ExternalMember
        {
            get { return _externalMember; }
            set { _externalMember = value; }
        }
        /// <summary>
        /// 合同ID
        /// </summary>
        public int cpr_FID
        {
            get
            {
                return _cpr_fid;
            }
            set
            {
                _cpr_fid = value;
            }
        }
        #endregion Model

    }
}

