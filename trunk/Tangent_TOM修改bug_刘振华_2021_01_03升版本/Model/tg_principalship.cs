﻿using System;
namespace TG.Model
{
	/// <summary>
	/// tg_principalship:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class tg_principalship
	{
		public tg_principalship()
		{}
		#region Model
		private int _pri_id;
		private string _pri_name;
		private string _pri_intro;
		/// <summary>
		/// 
		/// </summary>
		public int pri_ID
		{
			set{ _pri_id=value;}
			get{return _pri_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pri_Name
		{
			set{ _pri_name=value;}
			get{return _pri_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string pri_Intro
		{
			set{ _pri_intro=value;}
			get{return _pri_intro;}
		}
		#endregion Model

	}
}

