﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;

namespace TG.Model
{
    [Serializable]
    public class ProjectValueAllot
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        [DataMapping("CprName")]
        public string CprName { get; set; }
        /// <summary>
        /// 实收产值
        /// </summary>
        [DataMapping("Charge")]
        public decimal Charge { get; set; }
        /// <summary>
        /// 转经济所
        /// </summary>
        [DataMapping("EconomyValue")]
        public decimal EconomyValue { get; set; }
        /// <summary>
        /// 转暖通所
        /// </summary>
        [DataMapping("HavcValue")]
        public decimal HavcValue { get; set; }
        /// <summary>
        /// 转土建所
        /// </summary>
        [DataMapping("TranBulidingValue")]
        public decimal TranBulidingValue { get; set; }
        /// <summary>
        /// 本所产值
        /// </summary>
       [DataMapping("UnitValue")]
        public decimal UnitValue { get; set; }
        /// <summary>
       /// 所留
        /// </summary>
       [DataMapping("TheDeptValue")]
       public decimal TheDeptValue
        {
            get;
            set;
        }
       /// <summary>
       /// 项目id
       /// </summary>
       [DataMapping("CprId")]
       public int CprId
       {
           get;
           set;
       }
    }


}
