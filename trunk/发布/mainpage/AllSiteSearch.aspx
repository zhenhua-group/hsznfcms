﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="AllSiteSearch.aspx.cs" Inherits="TG.Web.mainpage.AllSiteSearch" %>

<%@ Register TagPrefix="cc1" Namespace="Geekees.Common.Controls" Assembly="ASTreeView, Version=1.5.9.0, Culture=neutral, PublicKeyToken=521e0b4262a9001c" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css">
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />

    <script src="/js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script src="../js/AllSite_jq.js"></script>
    <script>
        function IsStructCheckNode(obj) {
            nodecount = 0;
            nodeLenght="";    
            <%= asTreeviewdrpunitObjID %>.traverseTreeNode(displayNodeFun);
            <%= asTreeviewdrpunitObjID %>.traverseTreeNode(displayNodeFuns);            
            return nodeLenght;
        }
        //选中与半选中  qpl 20140115
        function displayNodeFun(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodecount++;
            }
        }
        function displayNodeFuns(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodeLenght+=  "'"+elem.getAttribute("treenodevalue")+"',";
            }
        }
    </script>
    <script>
        $(function () {
            $("li",$("#ShowTab ul")).each(function () {
                var classname=$(this).attr("class");
                if (classname=="active"){             
                    var idname=$(this).find("a").attr("id");

                    if (idname=="customer"){
                        $("#gbox_jqGrid").show();
                        $("#gridpager").show();
                        $("#nodata").hide();
                        $("#gbox_jqGrid1").hide();
                        $("#gridpager1").hide();
                        $("#nodata1").hide();
                        $("#gbox_jqGrid2").hide();
                        $("#gridpager2").hide();
                        $("#nodata2").hide();
                        customer();
                    }else if (idname=="coperation"){

                        $("#gbox_jqGrid").hide();
                        $("#gridpager").hide();
                        $("#nodata").hide();
                        $("#gbox_jqGrid1").show();
                        $("#gridpager1").show();
                        $("#nodata1").hide();
                        $("#gbox_jqGrid2").hide();
                        $("#gridpager2").hide();
                        $("#nodata2").hide();
                        coperation();
                    }else if (idname=="project"){
                        $("#gbox_jqGrid").hide();
                        $("#gridpager").hide();
                        $("#nodata").hide();
                        $("#gbox_jqGrid1").hide();
                        $("#gridpager1").hide();
                        $("#nodata1").hide();
                        $("#gbox_jqGrid2").show();
                        $("#gridpager2").show();
                        $("#nodata2").hide();
                        project();
                    }
                }
            });
            $("li",$("#ShowTab ul")).click(function(){
                var idname=$(this).find("a").attr("id"); 
                if (idname=="customer"){
                    $("#gbox_jqGrid").show();
                    $("#gridpager").show();
                    $("#nodata").hide();
                    $("#gbox_jqGrid1").hide();
                    $("#gridpager1").hide();
                    $("#nodata1").hide();
                    $("#gbox_jqGrid2").hide();
                    $("#gridpager2").hide();
                    $("#nodata2").hide();
                    customer();
                }else if (idname=="coperation"){
                    $("#gbox_jqGrid").hide();
                    $("#gridpager").hide();
                    $("#nodata").hide();
                    $("#gbox_jqGrid1").show();
                    $("#gridpager1").show();
                    $("#nodata1").hide();
                    $("#gbox_jqGrid2").hide();
                    $("#gridpager2").hide();
                    $("#nodata2").hide();
                    coperation();
                }else if (idname=="project"){
                    $("#gbox_jqGrid").hide();
                    $("#gridpager").hide();
                    $("#nodata").hide();
                    $("#gbox_jqGrid1").hide();
                    $("#gridpager1").hide();
                    $("#nodata1").hide();
                    $("#gbox_jqGrid2").show();
                    $("#gridpager2").show();
                    $("#nodata2").hide();
                    project();
                }
            });
        })    
              
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首 页 <small>全站查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>全站查询</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue tabbable">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-reorder"></i><span class="hidden-480">查询类型</span></div>
                </div>
                <div class="portlet-body util-btn-margin-bottom-5">
                    <div class="tabbable portlet-tabs" id="ShowTab">
                        <ul class="nav nav-tabs">
                            <li class="">
                                <a href="#btn_Project" data-toggle="tab" id="project">项目</a>
                            </li>
                            <li class="">
                                <a href="#btn_Coperation" data-toggle="tab" id="coperation">合同</a>
                            </li>
                            <li class="active">
                                <a href="#btn_Customer" data-toggle="tab" id="customer">客户</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="btn_Project">
                                <!--项目查询条件-->
                                <table class="table-responsive">
                                    <tr>
                                        <td>生产部门:</td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="proj_drp_unit" runat="server" ClientIDMode="AutoID" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全院部门-------" />
                                        </td>
                                        <td>年份:</td>
                                        <td>
                                            <asp:DropDownList ID="proj_drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                            </asp:DropDownList></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>项目名称:</td>
                                        <td colspan="3">
                                            <input type="text" class="form-control input-sm" id="proj_txt_keyname" runat="Server" style="width: 220px; border: 1px solid #9BA0A6;" /></td>

                                        <td>录入时间:</td>
                                        <td>
                                            <input type="text" name="txt_date" id="proj_txt_start" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>
                                        <td></td>
                                        <td>截止时间:</td>
                                        <td>
                                            <input type="text" name="txt_date" id="proj_txt_end" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>
                                        <td>
                                            <input type="button" class="btn blue" value="查询" id="btn_search" /></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="btn_Coperation">
                                <!--合同查询条件-->

                                <table class="table-responsive">
                                    <tr>
                                        <td>生产部门：</td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="cop_drp_unit" runat="server" ClientIDMode="AutoID" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全院部门-------" />
                                        </td>
                                        <td style="text-align: center;">年份：</td>
                                        <td>
                                            <asp:DropDownList ID="cop_drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                            </asp:DropDownList></td>
                                        <td>工程名称：</td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="cop_txt_keyname" runat="server" style="border: 1px solid #9BA0A6;" /></td>

                                        <td colspan="2">录入时间：<input type="text" name="txt_date" id="cop_txt_startdate" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>
                                        <td colspan="4">截止时间：<input type="text" name="txt_date" id="cop_txt_enddate" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>
                                        <td>
                                            <input type="button" class="btn blue" value="查询" id="btn_basesearch" /></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane active" id="btn_Customer">
                                <!--客户查询条件-->
                                <div class="table-responsive">
                                    <table border="0" cellspacing="0" cellpadding="0" id="tbl_id">
                                        <tr>
                                            <td>生产部门:</td>
                                            <td>
                                                <asp:DropDownList ID="cust_drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>年份:</td>
                                            <td>
                                                <asp:DropDownList ID="cust_drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>客户名称:</td>
                                            <td>
                                                <input type="text" class="form-control input-sm" id="cust_txt_keyname" runat="server" /></td>
                                            <td>录入时间:<input type="text" name="txt_date" id="cust_txt_start" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                                            <td>截止时间:<input type="text" name="txt_date" id="cust_txt_end" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                                            <td>
                                                <input type="button" class="btn blue" id="cust_btn_search" value="查询" /></td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>信息列表
                    </div>
                </div>
                <div class="portlet-body form">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                    <table id="jqGrid1">
                    </table>
                    <div id="gridpager1">
                    </div>
                    <div id="nodata1" class="norecords">
                        没有符合条件数据！
                    </div>
                    <table id="jqGrid2">
                    </table>
                    <div id="gridpager2">
                    </div>
                    <div id="nodata2" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hid_whereproj" runat="server" Value="" />
    
    <asp:HiddenField ID="hid_wherecop" runat="server" Value="" />
    <asp:HiddenField ID="hid_wherecust" runat="server" Value="" />
</asp:Content>
