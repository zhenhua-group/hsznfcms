﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProcessLine.aspx.cs" Inherits="TG.Web.mainpage.ProcessLine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/style-metronic.css" rel="stylesheet" type="text/css">
    <link href="/js/assets/css/style-responsive.css" rel="stylesheet" type="text/css">
    <link href="/js/assets/css/plugins.css" rel="stylesheet" type="text/css">
    <link href="/js/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="/js/assets/css/custom.css" rel="stylesheet" type="text/css">

    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css">
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="/js/assets/css/pages/timeline.css" rel="stylesheet" type="text/css">
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="/js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首 页 <small>工程时间轴</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>全站查询</a> <i class="fa fa-angle-right"></i><a>工程时间轴</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <ul class="timeline">
                <li class="timeline-yellow">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time">
                            <%=Custtime %>
                        </span>
                    </div>
                    <div class="timeline-icon"><i class="fa fa-briefcase"></i></div>
                    <div class="timeline-body">
                        <h2>客户信息</h2>
                        <div class="timeline-content">
                            <table class="table table-bordered" align="center">
                                <tr>
                                    <td style="width: 10%;">编号:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtCst_No" runat="server" Text="" CssClass="form-control-static">010-4545687</asp:Label>
                                    </td>
                                    <td style="width: 12%;">客户简称:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtCst_Brief" runat="server" Text="" CssClass="form-control-static">Tangent</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">客户名称:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtCst_Name" runat="server" Text="" CssClass="form-control-static">天正软件</asp:Label>
                                    </td>
                                    <td style="width: 12%;">公司地址:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtCpy_Address" runat="server" Text="" CssClass="form-control-static">北京市海淀区中关村南大街25号</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">邮政编码:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtCode" runat="server" Text="" CssClass="form-control-static">476400</asp:Label>
                                    </td>
                                    <td style="width: 12%;">联系人:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtLinkman" runat="server" Text="" CssClass="form-control-static">张飞</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">公司电话:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtCpy_Phone" runat="server" Text="" CssClass="form-control-static">010-56944589</asp:Label>
                                    </td>
                                    <td style="width: 12%;">传真号:
                                    </td>
                                    <td>
                                        <asp:Label ID="txtCpy_Fax" runat="server" Text="" CssClass="form-control-static">010-6621156</asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="timeline-footer">
                            <a href="../Customer/cst_CustomerInfoListByMaster.aspx" class="nav-link pull-right">更多客户信息<i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="timeline-blue">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time"><%=Coptime %></span>
                    </div>
                    <div class="timeline-icon"><i class="fa fa-book"></i></div>
                    <div class="timeline-body">
                        <h2>合同信息</h2>
                        <div class="timeline-content">

                            <asp:GridView ID="gv_rContract" runat="server" Width="100%" CellPadding="0" DataKeyNames="cpr_Id"
                                AutoGenerateColumns="False" ShowFooter="true" RowStyle-HorizontalAlign="Left" ShowHeader="true"
                                Font-Size="12px" RowStyle-Height="22px" CssClass="table table-bordered"
                                EnableModelValidation="True" OnRowDataBound="gv_rContract_DataBound">
                                <RowStyle HorizontalAlign="Left" Height="22px"></RowStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            序号
                                        </HeaderTemplate>
                                        <FooterTemplate>
                                            合计:
                                        </FooterTemplate>
                                        <ItemStyle HorizontalAlign="center" />
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="cpr_No" HeaderText="合同编号" SortExpression="cpr_No">
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="合同名称" SortExpression="cpr_Name">
                                        <ItemTemplate>
                                            <a href='../Coperation/cpr_ShowCoprationBymaster.aspx?flag=cstshow&cprid=<%# Eval("cpr_id") %>' title='<%# Eval("cpr_Name") %>'>
                                                <%# Eval("cpr_Name").ToString().Length > 15 ? Eval("cpr_Name").ToString().Substring(0, 15) + "......" : Eval("cpr_Name").ToString()%></a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="cpr_Acount" HeaderText="合同额(万元)" SortExpression="cpr_Acount">

                                        <ItemStyle HorizontalAlign="Left" Width="16%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="cpr_SignDate" HeaderText="签订日期" SortExpression="cpr_SignDate"
                                        DataFormatString="{0:d}">
                                        <ItemStyle HorizontalAlign="Left" Width="12%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="cpr_DoneDate" HeaderText="完成日期" SortExpression="cpr_DoneDate"
                                        DataFormatString="{0:d}">
                                        <ItemStyle HorizontalAlign="Left" Width="12%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ChgPeople" HeaderText="负责人" SortExpression="ChgPeople">
                                        <ItemStyle HorizontalAlign="Left" Width="8%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="查看">
                                        <ItemTemplate>
                                            <a href='../Coperation/cpr_ShowCoprationBymaster.aspx?flag=cstshow&cprid=<%# Eval("cpr_id") %>'>查看</a>
                                        </ItemTemplate>
                                        <ItemStyle Width="6%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    暂时没有关联合同！
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="timeline-footer">
                            <a href="../Coperation/cpr_CorperationListBymaster.aspx" class="nav-link pull-right">查看更多合同 <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="timeline-red">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time"><%=Acounttime %></span>
                    </div>
                    <div class="timeline-icon"><i class="fa fa-rmb"></i></div>
                    <div class="timeline-body">
                        <h2>收款信息</h2>
                        <div class="timeline-content">
                            <%=AcountTable %>
                        </div>
                        <div class="timeline-footer">
                            <a href="../Coperation/ProjectChargeBymaster.aspx" class="nav-link pull-right">查看更多收款信息<i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="timeline-green">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time"><%=Projtime %></span>
                    </div>
                    <div class="timeline-icon"><i class="fa fa-table"></i></div>
                    <div class="timeline-body">
                        <h2>项目信息</h2>
                        <div class="timeline-content">
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" CssClass="table table-bordered" runat="server"
                                    AutoGenerateColumns="False" EnableModelValidation="True" ShowFooter="true" ShowHeader="true"
                                    Border="0" Font-Size="12px" RowStyle-Height="22px" HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" Width="98%" OnRowDataBound="GridView1_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                序号
                                            </HeaderTemplate>
                                            <FooterTemplate>
                                                合计:
                                            </FooterTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                            <FooterStyle Font-Bold="true" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href='../ProjectManage/ShowProjectBymaster.aspx?pro_id=<%#Eval("pro_ID") %>'><%#Eval("pro_name")%></a>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                项目名称
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="300" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Unit" HeaderText="部门">
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PMName" HeaderText="项目经理">
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProjectScale" HeaderText="规模">
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                            <FooterStyle Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="pro_startTime" HeaderText="开始日期" DataFormatString="{0:d}">
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="pro_finishTime" HeaderText="结束日期" DataFormatString="{0:d}">
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="查看">
                                            <ItemTemplate>
                                                <a href='../ProjectManage/ShowProjectBymaster.aspx?pro_id=<%#Eval("pro_ID") %>'>查看</a>
                                            </ItemTemplate>
                                            <ItemStyle Width="6%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        暂时没有关联项目！
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="timeline-footer">
                            <a href="../ProjectManage/ProjectListBymaster.aspx" class="nav-link pull-right">查看更多项目 <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="timeline-purple">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time"><%=Projtime %></span>
                    </div>
                    <div class="timeline-icon"><i class="glyphicon glyphicon-th-large"></i></div>
                    <div class="timeline-body">
                        <h2>项目策划</h2>
                        <div class="timeline-content">
                            <%=CeHuaTable %>
                        </div>
                        <div class="timeline-footer">
                            <a href="../ProjectManage/ProjectListBymaster.aspx" class="nav-link pull-right">查看更多项目 <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="timeline-yellow">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time"><%=Pjdtime %></span>
                    </div>
                    <div class="timeline-icon"><i class="fa fa-qrcode"></i></div>
                    <div class="timeline-body">
                        <h2>项目进度</h2>
                        <div class="timeline-content">
                            <%=JinDuTable %>
                        </div>
                        <div class="timeline-footer">
                            <a href="../ProjectManage/ProjectListBymaster.aspx" class="nav-link pull-right">查看更多项目 <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="timeline-blue">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time"><%=Projtime %></span>
                    </div>
                    <div class="timeline-icon"><i class="glyphicon glyphicon-th"></i></div>
                    <div class="timeline-body">
                        <h2>图纸信息</h2>
                        <div class="timeline-content">
                            <%=TuZhiTable %>
                        </div>
                        <div class="timeline-footer">
                            <a href="../ProjectManage/ProjectListBymaster.aspx" class="nav-link pull-right">查看更多项目 <i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>

                <li class="timeline-grey">
                    <div class="timeline-time">
                        <span class="date"></span>
                        <span class="time"><%=Valuetime %></span>
                    </div>
                    <div class="timeline-icon"><i class="glyphicon glyphicon-align-justify"></i></div>
                    <div class="timeline-body">
                        <h2>产值分配</h2>
                        <div class="timeline-content">
                            暂无产值分配信息
                        </div>
                        <div class="timeline-footer">
                            <a href="#" class="nav-link pull-right">查看更多产值分配<i class="m-icon-swapright m-icon-white"></i>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
