﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectProAdd.aspx.cs" Inherits="TG.Web.LeadershipCockpit.SelectProAdd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .map img
        {
            width: 630px;
            height: 450px;
            position: absolute;
        }
        .mapDiv
        {
            width: 140px;
            height: 61px;
            padding: 5px;
            color: #369;
            background-color: #f0f0f0;
            position: absolute;
            display: none;
            word-break: break-all;
        }
        .imgsrc
        {
            margin-top:120px;
            display:none;
            }
    </style>
    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>

    <script type="text/javascript">
        var where = new Array(35);
        function comefrom(loca, locacity) { this.loca = loca; this.locacity = locacity; }
        where[0] = new comefrom("请选择省份名", "请选择城市名");
        where[1] = new comefrom("北京市", "|东城|西城|崇文|宣武|朝阳|丰台|石景山|海淀|门头沟|房山|通州|顺义|昌平|大兴|平谷|怀柔|密云|延庆");
        where[2] = new comefrom("上海市", "|黄浦|卢湾|徐汇|长宁|静安|普陀|闸北|虹口|杨浦|闵行|宝山|嘉定|浦东|金山|松江|青浦|南汇|奉贤|崇明");
        where[3] = new comefrom("天津市", "|和平|东丽|河东|西青|河西|津南|南开|北辰|河北|武清|红挢|塘沽|汉沽|大港|宁河|静海|宝坻|蓟县");
        where[4] = new comefrom("重庆市", "|万州|涪陵|渝中|大渡口|江北|沙坪坝|九龙坡|南岸|北碚|万盛|双挢|渝北|巴南|黔江|长寿|綦江|潼南|铜梁|大足|荣昌|壁山|梁平|城口|丰都|垫江|武隆|忠县|开县|云阳|奉节|巫山|巫溪|石柱|秀山|酉阳|彭水|江津|合川|永川|南川");
        where[5] = new comefrom("河北省", "|石家庄|邯郸|邢台|保定|张家口|承德|廊坊|唐山|秦皇岛|沧州|衡水");
        where[6] = new comefrom("山西省", "|太原|大同|阳泉|长治|晋城|朔州|吕梁|忻州|晋中|临汾|运城");
        where[7] = new comefrom("内蒙古自治区", "|呼和浩特|包头|乌海|赤峰|呼伦贝尔盟|阿拉善盟|哲里木盟|兴安盟|乌兰察布盟|锡林郭勒盟|巴彦淖尔盟|伊克昭盟");
        where[8] = new comefrom("辽宁省", "|沈阳|大连|鞍山|抚顺|本溪|丹东|锦州|营口|阜新|辽阳|盘锦|铁岭|朝阳|葫芦岛");
        where[9] = new comefrom("吉林省", "|长春|吉林|四平|辽源|通化|白山|松原|白城|延边");
        where[10] = new comefrom("黑龙江省", "|哈尔滨|齐齐哈尔|牡丹江|佳木斯|大庆|绥化|鹤岗|鸡西|黑河|双鸭山|伊春|七台河|大兴安岭");
        where[11] = new comefrom("江苏省", "|南京|镇江|苏州|南通|扬州|盐城|徐州|连云港|常州|无锡|宿迁|泰州|淮安");
        where[12] = new comefrom("浙江省", "|杭州|宁波|温州|嘉兴|湖州|绍兴|金华|衢州|舟山|台州|丽水");
        where[13] = new comefrom("安徽省", "|合肥|芜湖|蚌埠|马鞍山|淮北|铜陵|安庆|黄山|滁州|宿州|池州|淮南|巢湖|阜阳|六安|宣城|亳州");
        where[14] = new comefrom("福建省", "|福州|厦门|莆田|三明|泉州|漳州|南平|龙岩|宁德");
        where[15] = new comefrom("江西省", "|南昌市|景德镇|九江|鹰潭|萍乡|新馀|赣州|吉安|宜春|抚州|上饶");
        where[16] = new comefrom("山东省", "|济南|青岛|淄博|枣庄|东营|烟台|潍坊|济宁|泰安|威海|日照|莱芜|临沂|德州|聊城|滨州|菏泽");
        where[17] = new comefrom("河南省", "|郑州|开封|洛阳|平顶山|安阳|鹤壁|新乡|焦作|濮阳|许昌|漯河|三门峡|南阳|商丘|信阳|周口|驻马店|济源");
        where[18] = new comefrom("湖北省", "|武汉|宜昌|荆州|襄樊|黄石|荆门|黄冈|十堰|恩施|潜江|天门|仙桃|随州|咸宁|孝感|鄂州");
        where[19] = new comefrom("湖南省", "|长沙|常德|株洲|湘潭|衡阳|岳阳|邵阳|益阳|娄底|怀化|郴州|永州|湘西|张家界");
        where[20] = new comefrom("广东省", "|广州|深圳|珠海|汕头|东莞|中山|佛山|韶关|江门|湛江|茂名|肇庆|惠州|梅州|汕尾|河源|阳江|清远|潮州|揭阳|云浮");
        where[21] = new comefrom("广西壮族自治区", "|南宁|柳州|桂林|梧州|北海|防城港|钦州|贵港|玉林|南宁地区|柳州地区|贺州|百色|河池");
        where[22] = new comefrom("海南省", "|海口|三亚");
        where[23] = new comefrom("四川省", "|成都|绵阳|德阳|自贡|攀枝花|广元|内江|乐山|南充|宜宾|广安|达川|雅安|眉山|甘孜|凉山|泸州");
        where[24] = new comefrom("贵州省", "|贵阳|六盘水|遵义|安顺|铜仁|黔西南|毕节|黔东南|黔南");
        where[25] = new comefrom("云南省", "|昆明|大理|曲靖|玉溪|昭通|楚雄|红河|文山|思茅|西双版纳|保山|德宏|丽江|怒江|迪庆|临沧");
        where[26] = new comefrom("西藏自治区", "|拉萨|日喀则|山南|林芝|昌都|阿里|那曲");
        where[27] = new comefrom("陕西省", "|西安|宝鸡|咸阳|铜川|渭南|延安|榆林|汉中|安康|商洛");
        where[28] = new comefrom("甘肃省", "|兰州|嘉峪关|金昌|白银|天水|酒泉|张掖|武威|定西|陇南|平凉|庆阳|临夏|甘南");
        where[29] = new comefrom("宁夏回族自治区", "|银川|石嘴山|吴忠|固原");
        where[30] = new comefrom("青海省", "|西宁|海东|海南|海北|黄南|玉树|果洛|海西");
        where[31] = new comefrom("新疆维吾尔自治区", "|乌鲁木齐|石河子|克拉玛依|伊犁|巴音郭勒|昌吉|克孜勒苏柯尔克孜|博尔塔拉|吐鲁番|哈密|喀什|和田|阿克苏");
        where[32] = new comefrom("香港特别行政区", "");
        where[33] = new comefrom("澳门特别行政区", "");
        where[34] = new comefrom("台湾省", "|台北|高雄|台中|台南|屏东|南投|云林|新竹|彰化|苗栗|嘉义|花莲|桃园|宜兰|基隆|台东|金门|马祖|澎湖");
        where[35] = new comefrom("其它", "|北美洲|南美洲|亚洲|非洲|欧洲|大洋洲");
        //得到省份
        function select() {
            with (document.creator.province) { var loca2 = options[selectedIndex].value; }
            for (i = 0; i < where.length; i++) {
                if (where[i].loca == loca2) {
                    loca3 = (where[i].locacity).split("|");
                    for (j = 0; j < loca3.length; j++) { with (document.creator.city) { length = loca3.length; options[j].text = loca3[j]; options[j].value = loca3[j]; var loca4 = options[selectedIndex].value; } }
                    break;
                }
            }
            document.creator.newlocation.value = loca2 + loca4;
            return loca2;
        }
        function init() {
            with (document.creator.province) {
                length = where.length;
                for (k = 0; k < where.length; k++) { options[k].text = where[k].loca; options[k].value = where[k].loca; }
                options[selectedIndex].text = where[0].loca; options[selectedIndex].value = where[0].loca;
            }
            with (document.creator.city) {
                loca3 = (where[0].locacity).split("|");
                length = loca3.length;
                for (l = 0; l < length; l++) { options[l].text = loca3[l]; options[l].value = loca3[l]; }
                options[selectedIndex].text = loca3[0]; options[selectedIndex].value = loca3[0];
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            CommonControl.SetFormWidth();
            $("#tj").click(function () {
                var city = select();
                $.get("../HttpHandler/GetImg.ashx", { "city": escape(city) }, function (date) {
                    $("#cityPro").attr("src", "city2/" + city + ".png");
                    $("#imgsrc").css("display", "block")
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("area").each(function () {
                var $x = -70;
                var $y = -80;
                var name = $(this).attr("alt");
                $(this).mouseover(function (e) {
                    var numpro = "3";
                    $.get("../HttpHandler/Getnum.ashx", { "city": escape(name), "option": "all" }, function (date) {
                        numpro = date;
                        if (numpro == "0") {
                            $.get("../HttpHandler/Getnum.ashx", { "city": escape(name), "option": "some" }, function (countDate) {
                                var serName = select();
                                numpro = countDate;
                                var dom = "<div class='mapDiv'><p>提示消息<span class='name'></span><span class='num'></span></p></div>";
                                $("body").append(dom);
                                $(".name").text(serName);
                                $(".num").text(numpro);
                                $(".mapDiv").css({
                                    top: (e.pageY + $y) + "px",
                                    left: (e.pageX + $x) + "px"
                                }).show("fast");
                            })
                        } else {
                            var index_num = $(this).index();
                            var dom = "<div class='mapDiv'><p>提示消息<span class='name'></span><span class='num'></span></p></div>";
                            $("body").append(dom);
                            $(".name").text(name);
                            $(".num").text(numpro);
                            $(".mapDiv").css({
                                top: (e.pageY + $y) + "px",
                                left: (e.pageX + $x) + "px"
                            }).show("fast");
                        };
                    });
                }).mouseout(function () {
                    $(".mapDiv").remove();
                }).mousemove(function (e) {
                    $(".mapDiv").css({
                        top: (e.pageY + $y) + "px",
                        left: (e.pageX + $x) + "px"
                    })
                });
            });
        });
    </script>
</head>
<body onload="init()">
    <form method="post" name="creator" enctype="multipart/form-data">
    省份
    <select name="province" onchange="select()">
    </select>
    城市
    <select name="city" onchange="select()">
    </select>
    我在
    <input type="text" name="newlocation" maxlength="12" size="12" style="font-weight: bold">
    <input type="button" id="tj" value="查看" />
    不能超过12个字符（6个汉字）
    <div id="imgsrc" class="imgsrc">
    <div class="map">
    <img id="cityPro" border="0" usemap="#Map" src="city2/甘肃省.png"" alt="全国地图" style="height: 447px; width: 620px; margin-right: 0px; margin-top: 0px; top: 15px; left: 10px;" />
        <map name="Map" id="Map">
             <area id="dingxi" alt="定西区" href="#" coords="444, 298, 492, 332" 
                 shape="rect" />
                 <area id="linxia" alt="临夏自治州" href="#" coords="355, 289, 440, 314" 
                 shape="rect" />
             <area id="lanzhou" alt="兰州市" href="#" coords="410, 259, 450, 288" 
                 shape="rect" />
             <area id="baiyin" alt="白银市" href="#" coords="434, 216, 492, 255" 
                 shape="rect" />
             <area id="qingyang" alt="庆阳市" href="#" coords="542, 203, 610, 268" 
                 shape="rect" />
             <area id="zhangye" alt="张掖市" href="#" coords="251, 139, 323, 201" 
                 shape="rect" />
             <area id="jiuquan" alt="酒泉市" href="#" coords="202, 100, 249, 148" 
                 shape="rect" />
             <area id="wuwei" alt="武威市" href="#" coords="365, 145, 409, 228" 
                 shape="rect" />
             <area id="jinchang" alt="金昌市" href="#" coords="327, 161, 363, 205" 
                 shape="rect" />
             <area id="pingliang" alt="平凉市" href="#" coords="532, 271, 585, 304" 
                 shape="rect" />
             <area id="gannanzangzu" alt="甘南藏族自治州" href="#" coords="380, 345, 476, 384" 
                 shape="rect" />
             <area id="longnan" alt="陇南地区" href="#" coords="485, 358, 549, 405" 
                 shape="rect" />
             <area id="tianshui" alt="天水地区" href="#" coords="502, 313, 551, 348" 
                 shape="rect" />
             <area id="jiayuguan" alt="嘉峪关市" href="#" coords="32, 116, 199, 174" 
                 shape="rect" />
            </map>
    </div>
    </div>
    </form>
</body>
</html>
