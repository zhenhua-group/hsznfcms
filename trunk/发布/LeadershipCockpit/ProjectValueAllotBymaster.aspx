﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectValueAllotBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.ProjectValueAllotBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #ctl00_ContentPlaceHolder1_mytab
        {
            width: 60%;
            border-collapse: collapse;
            font-size: 9pt;
            font-family: 微软雅黑;
            margin-left: 10px;
            margin-right: 20px;
            margin-bottom: 20px;
            margin-top: 10px;
            margin: auto;
        }
        #ctl00_ContentPlaceHolder1_mytab td
        {
            padding: 0px 5px;
            height: 25px;
            text-align: center;
            background: white;
            border: solid 1px Gray;
            font-style: normal;
            font-family: 微软雅黑;
            font-size: 9pt;
            font-weight: 400;
            text-align: center;
            word-wrap: break-word;
            word-break: break-all;
        }
        #ctl00_ContentPlaceHolder1_mytab .cshead
        {
            background: #DBE5F1;
            height: 30px;
        }
        #ctl00_ContentPlaceHolder1_mytab .cscolumn
        {
            background: #D8D8D8;
        }
    </style> 
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/FunctionChart/FusionCharts.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/LeadershipCockpit/ProjectValueAllot.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        项目信息管理 <small>项目产值分配明细表-个人产值</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>项目信息管理</a><i class="fa fa-angle-right"> </i><a>产值分配</a><i class="fa fa-angle-right"> </i><a>项目产值分配明细表-个人产值</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询产值分配明细表</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="yeardiv" class="showclass" style="padding-top: 10px;">
                                    <div style="font-weight: bold; font-size: 14px; text-align: center; padding-bottom: 5px;">
                                        <asp:Label Text="" ID="pro_name" runat="server"></asp:Label>项目的个人产值汇总表
                                    </div>
                                    <table id="mytab" runat="server">
                                        <tr>
                                            <td width="20%" class="cshead">
                                                人员分类
                                            </td>
                                            <td width="20%" class="cshead">
                                                序号
                                            </td>
                                            <td width="30%" class="cshead">
                                                姓名
                                            </td>
                                            <td width="30%" class="cshead">
                                                产值(元)
                                            </td>
                                            <%--<td width="30%">所补产值</td>--%></tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="mytabhtml" runat="server" Value="" />
</asp:Content>
