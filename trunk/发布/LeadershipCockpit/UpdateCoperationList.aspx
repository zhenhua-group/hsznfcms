﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UpdateCoperationList.aspx.cs" Inherits="TG.Web.LeadershipCockpit.UpdateCoperationList" %>

<%@ Register TagPrefix="cc1" Namespace="Geekees.Common.Controls" Assembly="ASTreeView, Version=1.5.9.0, Culture=neutral, PublicKeyToken=521e0b4262a9001c" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
     <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"
        type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/LeadershipCockpit/UpdateCoperationList.js"></script>
    <style type="text/css">
        .borderClass {
            border: 1px solid #9BA0A6;
        }

        .tdvalue {
            min-width: 120px;
            width: 200px;
        }

        .tdtext {
            min-width: 70px;
            width: 90px;
        }

        .tdbetween {
            min-width: 50px;
            width: 90px;
        }

        #tbl_columns > tbody > tr > td {
            padding-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同额修改列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right"> </i><a>合同额修改列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询合同
                    </div>
                    <div class="actions">
                        <div class="btn-group" data-toggle="buttons" id="CommSelectWay">
                            <%--  <span class="btn blue">查询方式：</span>
                            <label rel="and" class="btn yellow btn-sm active" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option1" value="and">必须
                            </label>
                            <label rel="or" class="btn default btn-sm" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option2" value="or">或
                            </label>--%>
                        </div>
                        <%--<input type="button" class="btn red btn-sm" id="btn_export" value="导出" />--%>
                    </div>

                </div>
                <div class="portlet-body" id="selectColumnsParam">
                    <table border="0" cellspacing="0" cellpadding="0" style="display: none;" id="TempTbl">
                    </table>
                    <table class="table-responsive" style="width: 100%; margin: 0 auto;" id="tbl_columns">
                        <tr>
                            <td class="tdtext">生产部门：</td>
                            <td>
                                <cc1:ASDropDownTreeView ID="drp_unit" runat="server" Width="200" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全院部门-------" />
                            </td>
                            <td class="tdtext">年份：</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control borderClass tdvalue" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td class="tdtext">工程名称：</td>
                            <td>
                                <input type="text" class="form-control input-sm tdvalue" id="txt_keyname" runat="server" style="border: 1px solid #9BA0A6;" /></td>
                        </tr>
                        <%-- <tr>
                            <td colspan="2">录入时间：<input type="text" name="txt_date" id="txt_startdate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>
                            <td colspan="4">截止时间：<input type="text" name="txt_date" id="txt_enddate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #9BA0A6;" /></td>

                        </tr>--%>
                        <tr>
                            <td class="tdtext">合同分类：</td>
                            <td>
                                <asp:DropDownList ID="drp_type" CssClass="form-control borderClass tdvalue" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="0">-----选择合同分类-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdtext">建设单位：</td>
                            <td>
                                <input type="text" class="form-control input-sm tdvalue" id="txt_cprbuildunit" runat="server" style="border: 1px solid #9BA0A6;" /></td>
                            <td class="tdtext">合同额：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm tdbetween" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" id="txt_account1" runat="server" />
                                    －
                                    <input type="text" class="form-control  input-sm tdbetween" id="txt_account2" style="width: 90px; min-width: 50px; border: 1px solid #9BA0A6;" runat="server" />
                                    万元
                                </div>


                            </td>
                        </tr>
                    </table>
                    <table id="tbl_sch" class="table-responsive" style="width: 100%; margin: 0 auto; display: ;">
                        <tr>
                            <td colspan="2" align="center">
                                <input type="button" class="btn blue" id="btn_basesearch" value="查询" />&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>

                </div>
            </div>

            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同额修改列表
                    </div>
                    <div class="actions">
                        <input type="button" class="btn btn-sm red" value="批量删除" id="btn_del" data-target="#del_cpr" data-toggle="modal">
                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">

                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="del_cpr" class="modal fade yellow" tabindex="-1" data-width="560" aria-hidden="true"
        style="display: none; width: 560px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">填写删除合同原因</h4>
        </div>
        <div class="modal-body">
            <div id="jffzr_DialogDiv">
                <table class="table table-responsive table-bordered">
                    <tr>
                        <td style="width: 100px;">原因:</td>
                        <td>
                            <textarea id="txt_content" class="form-control input-sm" rows="5"></textarea>
                        </td>
                    </tr>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn green " id="btn_save">
                        确定</button>
                    <button type="button" data-dismiss="modal" id="btn_close" class="btn default">
                        关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_cols" runat="server" Value="" />
    <asp:HiddenField ID="hid_colsvalue" runat="server" Value="" />
    <input type="hidden" id="hid_cxtype" value="0" />
</asp:Content>
