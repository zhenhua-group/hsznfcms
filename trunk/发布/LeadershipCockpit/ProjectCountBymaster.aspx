﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectCountBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.ProjectCountBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />

    <!--JS--->
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script src="../js/LeadershipCockpit/ProjectCount.js" type="text/javascript"></script>
    <style type="text/css">
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

        #tbl_one td, #tbl_two td, #tbl_three td, #tbl_four td {
            border: solid 1px #CCC;
            font-size: 10pt;
            font-family: "微软雅黑";
        }

        .cls_chart {
            width: 100%;
            height: 500px;
        }

        .cls_data_bottom {
            width: 100%;
            margin: 0 auto;
            height: 25px;
            border: solid 1px #CCC;
        }
        /*数据基本样式*/
        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 20px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
            }

        .cls_data {
            width: 99%;
            margin: 0 auto;
        }



        .tr_in {
            background-color: #CCC;
        }

        .gridView_comm {
            margin: 0 auto;
            border-collapse: collapse;
            border-top: none;
        }

            .gridView_comm td {
                height: 18px;
                border-collapse: collapse;
                border: 1px solid Gray;
                border-top: none;
            }

            .gridView_comm a, a:hover, a:visited, a:link, a:active {
                color: Blue;
                text-decoration: none;
            }

            .gridView_comm .cls_column {
                overflow: hidden;
                text-overflow: ellipsis;
                -o-text-overflow: ellipsis;
                white-space: nowrap;
            }

            .gridView_comm tr:hover {
                background-color: #CCC;
            }
    </style>
    <script type="text/javascript">

        $(function () {
            //年份选择
            $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
                var index = $(this).val();
                if (index != "-1" && $("#ctl00_ContentPlaceHolder1_drpMonth").val() == "0" && $("#ctl00_ContentPlaceHolder1_drpJidu").val() == "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
                else if (index == "-1") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                }
            });
            //季度选择
            $("#ctl00_ContentPlaceHolder1_drpJidu").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                }
            });
            //月份选择
            $("#ctl00_ContentPlaceHolder1_drpMonth").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
            });
            //时间段
            $("#ctl00_ContentPlaceHolder1_cbx_time").click(function () {
                if ($(this).is(":checked")) {
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().hide();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                    //  $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();
                    // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();
                    // $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "");
                    //时间段选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("1");

                }
                else {
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().show();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                    // $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().show();
                    // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().show();
                    //  $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "none");
                    //时间段没有选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("0");

                }
            });
            //初始化数据显示
            Init();
        });
        function Init() {
            //时间段隐藏域的值
            if ($("#ctl00_ContentPlaceHolder1_hid_time").val() == "1") {
                //隐藏年
                $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drp_year").parent().hide();

                //隐藏季度
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();

                //隐藏月份
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();

                //显示合同时间段
                $("#ctl00_ContentPlaceHolder1_cbx_time").parents("td").prev().css("display", "");
            }
            if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
                $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>项目综合统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱 </a><i class="fa fa-angle-right"></i><a>数据统计</a><i class="fa fa-angle-right"></i><a>项目综合统计</a>
    </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询项目综合统计
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <cc1:ASDropDownTreeView ID="drpunit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-----全院部门-----" />
                            </td>
                            <td>项目年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control " runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td class="year">季度：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpJidu" runat="server">
                                    <asp:ListItem Value="0">--全部--</asp:ListItem>
                                    <asp:ListItem Value="1" Text="一季度"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二季度"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三季度"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四季度"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="year">月份：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpMonth" runat="server">
                                    <asp:ListItem Value="0" Text="--全部--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="一月"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二月"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三月"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四月"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="五月"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="六月"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="七月"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="八月"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="九月"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="十月"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="十一月"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="十二月"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td style="display: none;">
                                <div class="input-group">
                                    项目时间：
                                    <input type="text" name="txt_year1" id="txt_year1" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />至<input
                                            type="text" name="txt_year2" id="txt_year2" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />
                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <input type="checkbox" id="cbx_time" runat="server" />时间段
                                    <input type="hidden" id="hid_time" value="0" runat="server" />
                                </div>
                            </td>
                            <td>统计类型:</td>
                            <td>
                                <asp:DropDownList ID="drp_type" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">项目审批</asp:ListItem>
                                    <asp:ListItem Value="1">项目建筑等级</asp:ListItem>
                                    <asp:ListItem Value="2">项目建筑性质(数量)</asp:ListItem>
                                    <asp:ListItem Value="3">项目建筑性质(规模)</asp:ListItem>
                                    <asp:ListItem Value="4">项目来源</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>&nbsp;<input type="button" value="查询" id="btn_cx" class="btn blue" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>自定义查询条件
                    </div>
                    <div class="tools">
                    </div>
                    <div class="actions">
                        <a href="ProjectCountRpt.aspx" class="btn red btn-sm">项目统计报表</a>
                        <asp:Button runat="Server" CssClass="btn red btn-sm" Text="导出Excel" ID="btn_export" OnClick="btn_export_Click" />
                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12" id="ckb">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_ckb1" rel="tbl_con1">
                                    <tr>
                                        <%-- <td align="left">
                                            <input id="project_Unit" name="bb" class="cls_audit" type="checkbox" /><span for="project_Unit">生产部门</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="project_Year" name="bb" class="cls_audit" type="checkbox" /><span for="project_Year">项目年份</span>
                                        </td>
                                        <td align="left">
                                            <input id="project_count" name="bb" class="cls_audit" type="checkbox" /><span for="project_count">项目总数</span>
                                        </td>
                                        <td align="left">
                                            <input id="project_area" name="bb" class="cls_audit" type="checkbox" /><span for="project_area">项目规模</span>
                                        </td>
                                        <td align="left">
                                            <input id="project_auditcount" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="project_auditcount">立项通过数</span>
                                        </td>
                                        <td align="left">
                                            <input id="project_ingcount" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="project_ingcount">进行中项目</span>
                                        </td>
                                        <td align="left">
                                            <input id="project_stopcount" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="project_stopcount">暂停项目</span>
                                        </td>
                                        <td align="left">
                                            <input id="project_finishcount" name="bb" class="cls_audit" type="checkbox" /><span
                                                for="project_finishcount">完成项目</span>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_ckb2" rel="tbl_con2" style="display: none;">
                                    <tr>
                                        <%-- <td align="left">
                                            <input id="project_Unit2" name="bb" class="cls_audit" type="checkbox" />生产部门
                                        </td>--%>
                                        <td align="left">
                                            <input id="project_Year2" name="bb" class="cls_audit" type="checkbox" />项目年份
                                        </td>
                                        <td align="left">
                                            <input id="level_count" name="bb" class="cls_audit" type="checkbox" />特级数量
                                        </td>
                                        <td align="left">
                                            <input id="level_cprmoney" name="bb" class="cls_audit" type="checkbox" />特级合同额
                                        </td>
                                        <td align="left">
                                            <input id="level_count1" name="bb" class="cls_audit" type="checkbox" />一级数量
                                        </td>
                                        <td align="left">
                                            <input id="level_cprmoney1" name="bb" class="cls_audit" type="checkbox" />一级合同额
                                        </td>
                                        <td align="left">
                                            <input id="level_count2" name="bb" class="cls_audit" type="checkbox" />二级数量
                                        </td>
                                        <td align="left">
                                            <input id="level_cprmoney2" name="bb" class="cls_audit" type="checkbox" />二级合同额
                                        </td>
                                        <td align="left">
                                            <input id="level_count3" name="bb" class="cls_audit" type="checkbox" />三级数量
                                        </td>
                                        <td align="left">
                                            <input id="level_cprmoney3" name="bb" class="cls_audit" type="checkbox" />三级合同额
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_ckb3" rel="tbl_con3" style="display: none;">
                                    <tr>
                                        <%--  <td align="left">
                                            <input id="project_Unit3" name="bb" class="cls_audit" type="checkbox" />生产部门
                                        </td>--%>
                                        <td align="left">
                                            <input id="project_Year3" name="bb" class="cls_audit" type="checkbox" />项目年份
                                        </td>
                                        <td align="left">
                                            <input id="project_gj" name="bb" class="cls_audit" type="checkbox" />公建
                                        </td>
                                        <td align="left">
                                            <input id="project_fdc" name="bb" class="cls_audit" type="checkbox" />房地产
                                        </td>
                                        <td align="left">
                                            <input id="project_sz" name="bb" class="cls_audit" type="checkbox" />市政
                                        </td>
                                        <td align="left">
                                            <input id="project_yy" name="bb" class="cls_audit" type="checkbox" />医院
                                        </td>
                                        <td align="left">
                                            <input id="project_dl" name="bb" class="cls_audit" type="checkbox" />电力
                                        </td>
                                        <td align="left">
                                            <input id="project_tx" name="bb" class="cls_audit" type="checkbox" />通信
                                        </td>
                                        <td align="left">
                                            <input id="project_yh" name="bb" class="cls_audit" type="checkbox" />银行
                                        </td>
                                        <td align="left">
                                            <input id="project_xx" name="bb" class="cls_audit" type="checkbox" />学校
                                        </td>
                                        <td align="left">
                                            <input id="project_sw" name="bb" class="cls_audit" type="checkbox" />涉外
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_ckb4" rel="tbl_con4" style="display: none;">
                                    <tr>
                                        <%--  <td align="left">
                                            <input id="project_Unit4" name="bb" class="cls_audit" type="checkbox" />生产部门
                                        </td>--%>
                                        <td align="left">
                                            <input id="project_Year4" name="bb" class="cls_audit" type="checkbox" />项目年份
                                        </td>
                                        <td align="left">
                                            <input id="gkzb_count" name="bb" class="cls_audit" type="checkbox" />公开招标数量
                                        </td>
                                        <td align="left">
                                            <input id="gkzb_scale" name="bb" class="cls_audit" type="checkbox" />公开招标规模
                                        </td>
                                        <td align="left">
                                            <input id="yqzb_count" name="bb" class="cls_audit" type="checkbox" />邀请招标数量
                                        </td>
                                        <td align="left">
                                            <input id="yqzb_scale" name="bb" class="cls_audit" type="checkbox" />邀请招标规模
                                        </td>
                                        <td align="left">
                                            <input id="zxwt_count" name="bb" class="cls_audit" type="checkbox" />自行委托数量
                                        </td>
                                        <td align="left">
                                            <input id="zxwt_scale" name="bb" class="cls_audit" type="checkbox" />自行委托规模
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" id="con">
                                    <table class="table table-bordered table-hover" id="tbl_con1">
                                        <tr for="project_Unit">
                                            <td style="width: 100px">生产部门:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <asp:DropDownList ID="drp_unit1" rel="unit" CssClass="form-control" runat="server"
                                                            AppendDataBoundItems="True" Width="160px">
                                                            <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <input type="button" class="btn blue btn-sm" rel="btn_chooes" value="选择" />
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-8" rel="labUnit" style="font-size: 14px;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_Year">
                                            <td>项目年份:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="drp_year1" CssClass="form-control " runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp; 至 &nbsp;
                                                            <asp:DropDownList ID="drp_year2" CssClass="form-control " runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_count">
                                            <td>项目总数:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="project_count1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="project_count2" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_area">
                                            <td>项目规模:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="project_area1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="project_area2" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp;㎡
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_auditcount">
                                            <td>立项通过数:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="project_auditcount1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="project_auditcount2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_ingcount">
                                            <td>进行中项目:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="project_ingcount1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="project_ingcount2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_stopcount">
                                            <td>暂停项目:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="project_stopcount1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="project_stopcount2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_finishcount">
                                            <td>完成项目:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="project_finishcount1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="project_finishcount2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered table-hover" id="tbl_con2">
                                        <tr for="project_Unit2">
                                            <td style="width: 100px">生产部门:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <asp:DropDownList ID="drp_unit2" rel="unit" CssClass="form-control " runat="server"
                                                            AppendDataBoundItems="True" Width="160px">
                                                            <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <input type="button" class="btn blue btn-sm" rel="btn_chooes" value="选择" />
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-8" rel="labUnit" style="font-size: 14px;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_Year2">
                                            <td>项目年份:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="drp_year2_1" CssClass="form-control " runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp; 至 &nbsp;
                                                            <asp:DropDownList ID="drp_year2_2" CssClass="form-control " runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_count">
                                            <td>特级数量:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcount_1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" value="" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcount_2" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" value="" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_cprmoney">
                                            <td>特级合同额:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcpr_1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcpr_2" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp;万元
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_count1">
                                            <td>一级数量:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcount1_1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcount1_2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_cprmoney1">
                                            <td>一级合同额:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcpr1_1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcpr1_2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;万元
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_count2">
                                            <td>二级数量:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcount2_1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcount2_2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_cprmoney2">
                                            <td>二级合同额:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcpr2_1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcpr2_2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;万元
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_count3">
                                            <td>三级数量:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcount3_1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcount3_2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="level_cprmoney3">
                                            <td>三级合同额:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_levelcpr3_1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_levelcpr3_2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;万元
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered table-hover" id="tbl_con3">
                                        <tr for="project_Unit3">
                                            <td style="width: 100px">生产部门:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <asp:DropDownList ID="drp_unit3" rel="unit" CssClass="form-control" runat="server"
                                                            AppendDataBoundItems="True" Width="160px">
                                                            <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <input type="button" class="btn blue btn-sm" rel="btn_chooes" value="选择" />
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-8" rel="labUnit" style="font-size: 14px;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_Year3">
                                            <td>项目年份:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="drp_year3_1" CssClass="form-control " runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp; 至 &nbsp;
                                                            <asp:DropDownList ID="drp_year3_2" CssClass="form-control " runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_gj">
                                            <td>公建:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_gj1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_gj2" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_fdc">
                                            <td>房地产:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_fdc1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_fdc2" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_sz">
                                            <td>市政:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_sz1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_sz2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_yy">
                                            <td>医院:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_yy1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_yy2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_dl">
                                            <td>电力:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_dl1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_dl2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_tx">
                                            <td>通信:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_tx1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_tx2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_yh">
                                            <td>银行:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_yh1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_yh2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_xx">
                                            <td>学校:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_xx1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_xx2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_sw">
                                            <td>涉外:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_sw1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_sw2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;<span rel="CompanyTitle"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered table-hover" id="tbl_con4">
                                        <tr for="project_Unit4">
                                            <td style="width: 100px">生产部门:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <asp:DropDownList ID="drp_unit4" rel="unit" CssClass="form-control " runat="server"
                                                            AppendDataBoundItems="True" Width="160px">
                                                            <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <input type="button" class="btn blue btn-sm" rel="btn_chooes" value="选择" />
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-8" rel="labUnit" style="font-size: 14px;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="project_Year4">
                                            <td>项目年份:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="drp_year4_1" CssClass="form-control" runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp; 至 &nbsp;
                                                            <asp:DropDownList ID="drp_year4_2" CssClass="form-control" runat="server"
                                                                AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="gkzb_count">
                                            <td>公开招标数量:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_gkzbcount1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_gkzbcount2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="gkzb_scale">
                                            <td>公开招标规模:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_gkzbscale1" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_gkzbscale2" name="num" class="form-control input-sm" style="width: 180px;"
                                                                runat="server" />
                                                            &nbsp;㎡
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="yqzb_count">
                                            <td>邀请招标数量:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_yqzbcount1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_yqzbcount2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="yqzb_scale">
                                            <td>邀请招标规模:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_yqzbscale1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_yqzbscale2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;㎡
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="zxwt_count">
                                            <td>自行委托数量:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_zxwtcount1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_zxwtcount2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;个
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="zxwt_scale">
                                            <td>自行委托规模:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txt_zxwtscale1" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                            <input type="text" id="txt_zxwtscale2" name="num" class="form-control input-sm"
                                                                style="width: 180px;" runat="server" />
                                                            &nbsp;㎡
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>
                                    <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_search">
                                        <tr>
                                            <td colspan="2" align="center">
                                                <asp:HiddenField ID="HiddenlabUnit" runat="server" Value="0" />
                                                <asp:HiddenField ID="HiddenlabUnitID" runat="server" Value="0" />
                                                <input type="button" class="btn blue" value="查询" id="btn_Search" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>项目综合统计列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="cls_Container_Report">
                                    <div class="cls_Container_Tip">
                                        <p>
                                            <span id="drpunitname" runat="server"></span>项目综合统计按<span id="drptypename" runat="server"></span>统计
                                        </p>
                                    </div>
                                    <div class="table-responsive">
                                        <div id="div_Container1" runat="Server">
                                            <table id="tbl_one" border="0" cellspacing="0" class="table table-bordered table-grid"
                                                cellpadding="0">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 100px;">生产部门
                                                        </th>
                                                        <th style="width: 100px;">项目总数
                                                        </th>
                                                        <th style="width: 100px;">项目规模(㎡)
                                                        </th>
                                                        <th style="width: 100px;">立项通过数
                                                        </th>
                                                        <th style="width: 100px;">进行中项目
                                                        </th>
                                                        <th style="width: 100px;">暂停项目
                                                        </th>
                                                        <th style="width: 100px;">完成项目
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <%--<asp:GridView ID="GridView1" CssClass="table table-bordered table-grid" runat="server"
                                                AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True" Border="0">
                                                <Columns>
                                                    <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                        <ItemStyle Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="A" HeaderText="项目总数">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="B" HeaderText="项目规模">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="C" HeaderText="立项通过数">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="D" HeaderText="进行中项目">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="E" HeaderText="暂停项目">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="F" HeaderText="完成项目">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>--%>
                                        </div>
                                        <div id="div_Container2" runat="Server">
                                            <table id="tbl_two" border="0" cellspacing="0" class="table table-bordered table-grid"
                                                cellpadding="0">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 100px; vertical-align: middle;" rowspan="2">生产部门
                                                        </th>
                                                        <th style="width: 150px;" colspan="2">特级
                                                        </th>
                                                        <th style="width: 150px;" colspan="2">一级
                                                        </th>
                                                        <th style="width: 150px;" colspan="2">二级
                                                        </th>
                                                        <th style="width: 150px;" colspan="2">三级
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 60px;">数量
                                                        </th>
                                                        <th style="width: 90px;">合同额(万元)
                                                        </th>
                                                        <th style="width: 60px;">数量
                                                        </th>
                                                        <th style="width: 90px;">合同额(万元)
                                                        </th>
                                                        <th style="width: 60px;">数量
                                                        </th>
                                                        <th style="width: 90px;">合同额(万元)
                                                        </th>
                                                        <th style="width: 60px;">数量
                                                        </th>
                                                        <th style="width: 90px;">合同额(万元)
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <%-- <asp:GridView ID="GridView2" runat="server" CssClass="table table-bordered table-hover"
                                                AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True" Border="0">
                                                <Columns>
                                                    <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="A" HeaderText="数量(特级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="A1" HeaderText="合同额(特级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="B" HeaderText="数量(一级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="B1" HeaderText="合同额(一级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="C" HeaderText="数量(二级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="C1" HeaderText="合同额(二级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="D" HeaderText="数量(三级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="D1" HeaderText="合同额(三级)" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>--%>
                                        </div>
                                        <div id="div_Container3" runat="Server">
                                            <div id="div_util3" runat="Server" class="cls_util" style="width: 778px; height: 20px; color: red;">
                                            </div>
                                            <table id="tbl_three" border="0" cellspacing="0" class="table table-bordered table-grid"
                                                cellpadding="0">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 100px;">生产部门
                                                        </th>
                                                        <th style="width: 70px;">公建
                                                        </th>
                                                        <th style="width: 70px;">房地产
                                                        </th>
                                                        <th style="width: 70px;">市政
                                                        </th>
                                                        <th style="width: 70px;">医院
                                                        </th>
                                                        <th style="width: 70px;">电力
                                                        </th>
                                                        <th style="width: 70px;">通信
                                                        </th>
                                                        <th style="width: 70px;">银行
                                                        </th>
                                                        <th style="width: 70px;">学校
                                                        </th>
                                                        <th style="width: 70px;">涉外
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <%--  <asp:GridView ID="GridView3" runat="server" CssClass="table table-bordered table-hover"
                                                AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True" Border="0">
                                                <Columns>
                                                    <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                        <ItemStyle Width="140px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="A" HeaderText=" 公建">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="B" HeaderText="房地产">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="C" HeaderText="市政">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="D" HeaderText="医院">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="E" HeaderText="电力">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="F" HeaderText="通信">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="G" HeaderText="银行">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="H" HeaderText="学校">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="I" HeaderText="涉外">
                                                        <ItemStyle Width="70px" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>--%>
                                        </div>
                                        <div id="div_Container4" runat="Server">
                                            <table id="tbl_four" border="0" cellspacing="0" class="table table-bordered table-grid"
                                                cellpadding="0">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 100px; vertical-align: middle;" rowspan="2">生产部门
                                                        </th>
                                                        <th style="width: 150px;" colspan="2">公开招标
                                                        </th>
                                                        <th style="width: 150px;" colspan="2">邀请招标
                                                        </th>
                                                        <th style="width: 150px;" colspan="2">自行委托
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 60px;">数量
                                                        </th>
                                                        <th style="width: 90px;">规模(㎡)
                                                        </th>
                                                        <th style="width: 60px;">数量
                                                        </th>
                                                        <th style="width: 90px;">规模(㎡)
                                                        </th>
                                                        <th style="width: 60px;">数量
                                                        </th>
                                                        <th style="width: 90px;">规模(㎡)
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <%--<div id="div_util4" runat="Server" class="cls_util" style="width: 922px;">
                                            </div>
                                            <asp:GridView ID="GridView4" runat="server" CssClass="table table-bordered table-hover"
                                                AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True" Border="0">
                                                <Columns>
                                                    <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                        <ItemStyle Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="A" HeaderText="公建">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="B" HeaderText="房地产">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="C" HeaderText="市政">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="D" HeaderText="医院">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="E" HeaderText="电力">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="F" HeaderText="通信">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="G" HeaderText="银行">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="H" HeaderText="学校">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="I" HeaderText="涉外">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>--%>
                                        </div>
                                        <%--<div id="div_Container5" runat="Server">
                                            <asp:GridView ID="GridView5" runat="server" CssClass="table table-bordered table-hover"
                                                AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True" Border="0">
                                                <Columns>
                                                    <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                        <ItemStyle Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="A" HeaderText="公开招标数量" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="A1" HeaderText="公开招标规模" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="B" HeaderText="邀请招标数量" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="B1" HeaderText="邀请招标规模" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="C" HeaderText="自行委托数量" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="C1" HeaderText="自行委托规模" ItemStyle-CssClass="cls_HoverTd">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 是否有查看全部的权限 -->
    <input type="hidden" id="hiddenischeckallpower" name="name" value="<%= IsCheckAllPower %>" />
    <input type="hidden" id="hiddenunitid" name="name" value="<%= UserUnitNo %>" />
    <input type="hidden" id="hiddenunitName" name="unitname" value="<%=unitname%>" />
    <input type="hidden" id="hiddenuserid" name="name" value="<%= UserSysNo %>" />
    <asp:HiddenField ID="titlecount" runat="server" Value="" />
    <asp:HiddenField ID="SelectType" runat="server" Value="0" />
</asp:Content>
