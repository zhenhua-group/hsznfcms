﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CoperationChargeYearDetailsBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationChargeYearDetailsBymaster" %>

<%@ Register TagPrefix="cc1" Namespace="Geekees.Common.Controls" Assembly="ASTreeView, Version=1.5.9.0, Culture=neutral, PublicKeyToken=521e0b4262a9001c" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/LeadershipCockpit/CoperationChargeYearDetails.js"></script>
    <script type="text/javascript">

        $(function () {
            //年份选择
            $("#ctl00_ContentPlaceHolder1_drp_year1").change(function () {
                var index = $(this).val();
                if (index != "-1" && $("#ctl00_ContentPlaceHolder1_drpMonth").val() == "0" && $("#ctl00_ContentPlaceHolder1_drpJidu").val() == "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
                else if (index == "-1") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                }
            });
            //季度选择
            $("#ctl00_ContentPlaceHolder1_drpJidu").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                }
            });
            //月份选择
            $("#ctl00_ContentPlaceHolder1_drpMonth").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
            });
            //时间段
            $("#ctl00_ContentPlaceHolder1_cbx_time").click(function () {
                if ($(this).is(":checked")) {
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year1").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drp_year1").parent().hide();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                    //  $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();
                    // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();
                    // $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "");
                    //时间段选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("1");

                }
                else {
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year1").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drp_year1").parent().show();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                    // $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().show();
                    // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().show();
                    //  $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "none");
                    //时间段没有选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("0");

                }
            });
            //初始化数据显示
            Init();
        });
        function Init() {
            //时间段隐藏域的值
            if ($("#ctl00_ContentPlaceHolder1_hid_time").val() == "1") {
                //隐藏年
                $("#ctl00_ContentPlaceHolder1_drp_year1").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drp_year1").parent().hide();

                //隐藏季度
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();

                //隐藏月份
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();

                //显示合同时间段
                $("#ctl00_ContentPlaceHolder1_cbx_time").parents("td").prev().css("display", "");
            }
            if ($("#ctl00_ContentPlaceHolder1_drp_year1").val() == "-1") {
                $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
            }

        }
    </script>
    <style type="text/css">
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .cls_show_cst_jiben td {
                border: solid 1px #CCC;
                font-size: 12px;
                font-family: "微软雅黑";
            }

        .cls_chart {
            width: 100%;
            height: 500px;
        }

        .cls_data_bottom {
            width: 100%;
            margin: 0 auto;
            height: 25px;
            border: solid 1px #CCC;
        }
        /*数据基本样式*/
        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 20px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
            }

        .cls_data {
            width: 99%;
            margin: 0 auto;
        }



        .tr_in {
            background-color: #CCC;
        }

        .gridView_comm {
            margin: 0 auto;
            border-collapse: collapse;
            border-top: none;
        }

            .gridView_comm td {
                height: 18px;
                border-collapse: collapse;
                border: 1px solid Gray;
                border-top: none;
            }

            .gridView_comm a, a:hover, a:visited, a:link, a:active {
                color: Blue;
                text-decoration: none;
            }

            .gridView_comm .cls_column {
                overflow: hidden;
                text-overflow: ellipsis;
                -o-text-overflow: ellipsis;
                white-space: nowrap;
            }

            .gridView_comm tr:hover {
                background-color: #CCC;
            }

        /*#tbl_1 td {
            padding: 0 5px;
        }*/

        tfoot td {
            font-weight: bold;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>部门产值完成统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right">
    </i><a>部门产值完成统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>简单查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <div class="table-responsive">
                        <table id="tbl_1">
                            <tr>
                                <td>生产部门:
                                </td>
                                <td>
                                    <cc1:ASDropDownTreeView ID="drpunit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全院部门-------" />
                                </td>
                                <td>合同年份:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_year1" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True" AutoPostBack="true" OnSelectedIndexChanged="drp_unitLone_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                 <td class="year">季度：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpJidu" runat="server">
                                    <asp:ListItem Value="0">--全部--</asp:ListItem>
                                    <asp:ListItem Value="1" Text="一季度"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二季度"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三季度"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四季度"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="year">月份：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpMonth" runat="server">
                                    <asp:ListItem Value="0" Text="--全部--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="一月"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二月"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三月"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四月"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="五月"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="六月"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="七月"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="八月"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="九月"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="十月"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="十一月"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="十二月"></asp:ListItem>
                                </asp:DropDownList>

                            </td>   
                             <td style="display:none;">
                                <div class="input-group">
                                    合同时间：
                                    <input type="text" name="txt_year1" id="txt_year1" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />至<input
                                            type="text" name="txt_year2" id="txt_year2" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />
                                </div>
                            </td>                        
                            <td>
                                <div class="input-group">
                                    <input type="checkbox" id="cbx_time" runat="server"/>时间段
                                    <input type="hidden" id="hid_time" value="0" runat="server"/>
                                </div>
                            </td>
                                <td>收费时间:
                                </td>
                                <td>
                                    <input type="text" name="txt_date" id="txtdateStart" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" style="width: 90px; height: 22px; vertical-align: middle; border: 1px solid #e5e5e5;" />
                                </td>
                                <td>截止时间:
                                </td>
                                <td>
                                    <input type="text" name="txt_date" id="txtdateEnd" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" style="width: 90px; vertical-align: middle; height: 22px; border: 1px solid #e5e5e5;" />
                                </td>
                                <td>
                                    <asp:Button runat="Server" CssClass="btn blue" Text="查询" ID="Button1" OnClick="drp_unitLone_SelectedIndexChanged" /></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>自定义高级查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <a href="CoperationChargeYearRpt.aspx" class="btn red btn-sm">收款报表</a>
                        <asp:Button runat="Server" CssClass="btn red btn-sm" Text="导出Excel" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_id">
                                <tr>
                                    <td align="left" style="display:none;">
                                        <input id="cpr_Unit" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Unit">生产部门</span>
                                        <asp:HiddenField ID="Hiddencpr_Unit" runat="server" Value="0" />

                                    </td>
                                    <td align="left">
                                        <input id="cpr_Year" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Year">年份</span>
                                        <asp:HiddenField ID="Hiddencpr_Year" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="cpr_time" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_time">收费时间</span>
                                        <asp:HiddenField ID="Hiddencpr_time" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="doneSignnum" name="bb" class="cls_audit" type="checkbox" /><span for="doneSignnum">已签订合同数</span>
                                        <asp:HiddenField ID="HiddendoneSignnum" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="doneChargenum" name="bb" class="cls_audit" type="checkbox" /><span for="doneChargenum">已收款合同数</span>
                                        <asp:HiddenField ID="HiddendoneChargenum" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="nodoneChargenum" name="bb" class="cls_audit" type="checkbox" /><span for="nodoneChargenum">未收款合同数</span>
                                        <asp:HiddenField ID="HiddennodoneChargenum" runat="server" Value="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <input id="copArea" name="bb" class="cls_audit" type="checkbox" /><span for="copArea">合同面积</span>
                                        <asp:HiddenField ID="HiddencopArea" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="copTarget" name="bb" class="cls_audit" type="checkbox" /><span for="copTarget">合同目标值</span>
                                        <asp:HiddenField ID="HiddencopTarget" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="copAcount" name="bb" class="cls_audit" type="checkbox" /><span for="copAcount">合同总额</span>
                                        <asp:HiddenField ID="HiddencopAcount" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="chargeTarget" name="bb" class="cls_audit" type="checkbox" /><span for="chargeTarget">产值目标值</span>
                                        <asp:HiddenField ID="HiddenchargeTarget" runat="server" Value="0" />
                                    </td>
                                    <td align="left">
                                        <input id="doneChargeacount" name="bb" class="cls_audit" type="checkbox" /><span for="doneChargeacount">已收费额</span>
                                        <asp:HiddenField ID="HiddendoneChargeacount" runat="server" Value="0" />
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="tbl_id2">
                                    <tr for="cpr_Unit">
                                        <td style="width: 100px">生产部门:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="160px">
                                                        <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-1">
                                                    <input type="button" class="btn blue btn-sm" id="btn_chooes" value="选择" />
                                                </div>
                                                <div class="col-md-1">
                                                </div>
                                                <div class="col-md-8" id="labUnit" style="font-size: 14px;" runat="server">
                                                </div>
                                                <asp:HiddenField ID="HiddenlabUnit" runat="server" Value="0" />
                                                <asp:HiddenField ID="HiddenlabUnitID" runat="server" Value="0" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="cpr_Year">
                                        <td>合同年份:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="140px">
                                                            <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                        </asp:DropDownList>
                                                        &nbsp; 至 &nbsp;
                                                             <asp:DropDownList ID="drp_year2" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="140px">
                                                                 <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                             </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="cpr_time">
                                        <td>收费时间:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        &nbsp; 收费时间: &nbsp; 
                                                         <input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                             class="Wdate" runat="Server" style="width: 120px; height: 30px; vertical-align: middle; border: 1px solid #e5e5e5;" />
                                                        &nbsp;截止时间: &nbsp;
                                                     <input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                         class="Wdate" runat="Server" style="width: 120px; vertical-align: middle; height: 30px; border: 1px solid #e5e5e5;" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="doneSignnum">
                                        <td>已签订合同数:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="doneSignnum1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="doneSignnum2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;个
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="doneChargenum">
                                        <td>已收款合同数:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="doneChargenum1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="doneChargenum2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;个
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="nodoneChargenum">
                                        <td>未收款合同数:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="nodoneChargenum1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="nodoneChargenum2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;个
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="copArea">
                                        <td>合同面积:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="copArea1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="copArea2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;㎡
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="copTarget">
                                        <td>合同目标值:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="copTarget1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="copTarget2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="copAcount">
                                        <td>合同总额:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="copAcount1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="copAcount2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr for="chargeTarget">
                                        <td>产值目标值:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="chargeTarget1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="chargeTarget2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="doneChargeacount">
                                        <td>已收费额:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="doneChargeacount1" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="doneChargeacount2" name="num" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                        &nbsp;万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button runat="server" CssClass="btn blue " Text="查询" ID="btn_Search" OnClick="btn_Search_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>合同收费信息列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <h4>
                                        <asp:Label ID="lbl_tip" runat="server" Text="Label" Font-Bold="true" Font-Size="12pt"></asp:Label></h4>
                                    <h4>
                                        <asp:Label ID="lbl_tiptitle" runat="server" Text="Label" Font-Bold="true" Font-Size="10pt"
                                            Style="color: red;"></asp:Label></h4>
                                </center>
                                <div class="cls_data">
                                    <asp:GridView ID="grid_allot" runat="server" CellPadding="0" AutoGenerateColumns="False"
                                        CssClass="table table-bordered" RowStyle-HorizontalAlign="Center" Font-Names="微软雅黑"
                                        Font-Size="10pt" RowStyle-Height="22px" Width="100%" OnRowDataBound="gv_Coperation_RowDataBound"
                                        EnableModelValidation="True" ShowFooter="True" Border="0">
                                        <EmptyDataTemplate>
                                            没有数据！
                                        </EmptyDataTemplate>
                                        <FooterStyle Height="20px" />
                                        <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                                        <Columns>
                                            <asp:BoundField DataField="unit_Name" HeaderText="生产部门">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                                <ItemStyle Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CprCount" HeaderText="已签合同数" ItemStyle-HorizontalAlign="Center">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PayCprCount" HeaderText="已收款合同数" ItemStyle-HorizontalAlign="Center">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NoPayCprCount" HeaderText="未收款合同数" ItemStyle-HorizontalAlign="Center">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CprArea" HeaderText="合同面积(㎡)" ItemStyle-HorizontalAlign="Center">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CprAcountTarget" HeaderText="合同目标值">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="" />
                                            </asp:BoundField>
                                            <asp:BoundField ItemStyle-HorizontalAlign="Center" DataField="CprAcount" HeaderText="合同总额(万元)">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ItemStyle-HorizontalAlign="Center" HeaderText="比例">
                                                <ItemStyle HorizontalAlign="Center" Width="7%"></ItemStyle>
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" CssClass="yc" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CprChargeDoneTarget" HeaderText="产值目标值" ItemStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" CssClass="yc" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UnitChargeDone" HeaderText="已收费额(万元)" ItemStyle-HorizontalAlign="Center">
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" CssClass="yc" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%" CssClass="yc"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ItemStyle-HorizontalAlign="Center" HeaderText="比例">
                                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                                <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 是否有查看全部的权限 -->
    <input type="hidden" id="hiddenischeckallpower" name="name" value="<%= IsCheckAllPower %>" />
     <asp:HiddenField ID="SelectType" runat="server" Value="0" />
    <script type="text/javascript">
        $(function () {
            $(":hidden", $("#tbl_id")).each(function () {
                if ($(this).val() == "1") {
                    $(this).parent().find(":first").find("span").attr("class", "checked");
                    $(this).parent().find(":first").find("span").children().attr("checked", "checked");
                    var hiddenCol = $(this).parent().find(":first").siblings("span").attr("for");
                    $("#tbl_id2 tr[for=" + hiddenCol + "]").show();
                    $("#tbl_id2 tr:last").show();
                }
            })
        })
    </script>
</asp:Content>
