﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="MemberProjectValueAllotBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.MemberProjectValueAllotBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cls_chart {
            font-size: 9pt;
            width: 100%;
        }

        .cls_nav {
            width: 70px;
        }

            .cls_nav a {
                text-decoration: none;
            }

        .show_projectNumber {
            width: 860px;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .show_projectNumber td {
                border: solid 1px #CCC;
                font-size: 9pt;
                font-family: "微软雅黑";
                height: 20px;
            }

        .cls_show_cst_jiben_2 {
            border-collapse: collapse;
            border: solid 0px black;
            font-size: 9pt;
        }

        #labTime {
            float: right;
        }

        #labDanW {
            float: right;
        }

        #ctl00_ContentPlaceHolder1_mytab {
            border-collapse: collapse;
            font-size: 9pt;
            font-family: 微软雅黑;
            margin-left: 10px;
            margin-right: 20px;
            margin-bottom: 20px;
            margin: auto;
        }

            #ctl00_ContentPlaceHolder1_mytab td {
                padding: 0px 4px;
                height: 22px;
                text-align: center;
                background: white;
                border: solid 1px black;
                font-family: 微软雅黑;
                font-size: 9pt;
                text-align: center;
                word-wrap: break-word;
                word-break: break-all;
            }

        #ctl00_ContentPlaceHolder1_monthtab {
            border-collapse: collapse;
            width: 99%;
            font-size: 9pt;
            font-family: 微软雅黑;
            margin-left: 10px;
            margin-right: 20px;
        }

            #ctl00_ContentPlaceHolder1_monthtab td {
                border: solid 1px black;
            }

        .cls_GridView_Style2 .cls_Column_Short {
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/FunctionChart/FusionCharts.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/LeadershipCockpit/MemberProjectValueAllot.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>部门产值分配统计大表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>生产经营统计报表</a><i class="fa fa-angle-right">
    </i><a>部门产值分配统计大表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询财务报表显示
                    </div>
                    <div class="actions">
                        <input type="button" class="btn red btn-sm" value="导出" id="btn_report" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>分配年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                                <input type="button" class="btn blue " value="查询" id="btn_ok" /></td>
                            <td>
                                <input type="hidden" runat="server" value="" id="pp" />
                                <input type="hidden" runat="server" value="" id="memid" />
                                <input type="hidden" runat="server" value="" id="unitid" />
                                <input type="hidden" runat="server" value="" id="mytabhtml" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>财务报表显示列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cls_Container_Report">
                                <div class="cls_Container_Tip" style="margin-top: 5px; font-weight: bold;">
                                    <label id="title">
                                    </label>
                                    <label id="unit">
                                    </label>
                                    部门产值分配统计大表
                                </div>
                                <div style="width: 100%;">
                                    <div id="yeardiv" runat="server">
                                        <table id="mytab" runat="server" style="width: 99%;">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
