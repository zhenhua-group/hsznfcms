<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MapOfChina.aspx.cs" Inherits="TG.Web.LeadershipCockpit.MapOfChina" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>全国地图</title>
    <link rel="stylesheet" type="text/css" href="../css/smoothness/jquery-ui-1.8.20.custom.css" />
    <link href="../css/ProjStructCss4.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        /* The group of people */ #gmap {
            display: block;
            width: 550px;
            height: 617px;
            background: url(images/map.jpg) no-repeat;
            position: relative;
            margin: 0 auto 2em auto;
        }

            #gmap a {
                color: #000;
                font-family: arial, sans-serif;
                font-size: 1.2em;
                font-weight: bold;
                text-transform: uppercase;
            }

        a#title2, a#title2:visited {
            display: block;
            width: 550px;
            height: 356px;
            padding-top: 260px;
            position: absolute;
            left: 0;
            top: 0;
            cursor: default;
            text-decoration: none;
        }

        * html a#title2 {
            height: 617px;
        }
        /*IE6.0以下中显示*/ #gmap a#title2:hover {
            background: transparent url(group_col.gif) no-repeat 0 0;
            overflow: visible;
            color: #c00;
        }
        /*新疆*/ a#xj {
            display: block;
            width: 206px;
            height: 0;
            padding-top: 156px;
            overflow: hidden;
            position: absolute;
            left: 14px;
            top: 63px;
        }

        * html a#xj {
            height: 156px;
            text-indent: -9000px;
        }

            a#xj:hover {
                background: transparent url(images/xj1.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*西藏*/ a#xz {
            display: block;
            width: 200px;
            height: 0;
            padding-top: 124px;
            overflow: hidden;
            position: absolute;
            left: 37px;
            top: 207px;
        }

        * html a#xz {
            height: 124px;
        }

            a#xz:hover {
                background: transparent url(images/xz.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*青海*/ a#qh {
            display: block;
            width: 132px;
            height: 0;
            padding-top: 96px;
            overflow: hidden;
            position: absolute;
            left: 147px;
            top: 184px;
        }

        * html a#qh {
            height: 96px;
        }

            a#qh:hover {
                background: transparent url(images/qh.gif) no-repeat 0 0;
                height: -20px;
                overflow: visible;
                text-indent: -9000px;
            }
        /*甘肃*/ a#gs {
            display: block;
            width: 148px;
            height: 0;
            padding-top: 123px;
            overflow: hidden;
            position: absolute;
            left: 187px;
            top: 149px;
        }

        * html a#gs {
            height: 123px;
        }

            a#gs:hover {
                background: transparent url(images/gsh.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*内蒙古*/ a#lmg {
            display: block;
            width: 226px;
            height: 0;
            padding-top: 196px;
            overflow: hidden;
            position: absolute;
            left: 234px;
            top: 17px;
        }

        * html a#lmg {
            height: 196px;
        }

            a#lmg:hover {
                background: transparent url(images/lm.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*黑龙江*/ a#hlj {
            display: block;
            width: 116px;
            height: 0;
            padding-top: 106px;
            overflow: hidden;
            position: absolute;
            left: 420px;
            top: 13px;
        }

        * html a#hlj {
            height: 106px;
        }

            a#hlj:hover {
                background: transparent url(images/hlj.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*宁夏*/ a#nx {
            display: block;
            width: 34px;
            height: 0;
            padding-top: 47px;
            overflow: hidden;
            position: absolute;
            left: 290px;
            top: 191px;
        }

        * html a#nx {
            height: 47px;
        }

            a#nx:hover {
                background: transparent url(images/nx.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*吉林*/ a#jl {
            display: block;
            width: 88px;
            height: 0;
            padding-top: 59px;
            overflow: hidden;
            position: absolute;
            left: 436px;
            top: 96px;
        }

        * html a#jl {
            height: 59px;
        }

            a#jl:hover {
                background: transparent url(images/jl.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*辽宁*/ a#ln {
            display: block;
            width: 61px;
            height: 0;
            padding-top: 53px;
            overflow: hidden;
            position: absolute;
            left: 423px;
            top: 129px;
        }

        * html a#ln {
            height: 53px;
        }

            a#ln:hover {
                background: transparent url(images/ll.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*山东*/ a#sd {
            display: block;
            width: 69px;
            height: 0;
            padding-top: 45px;
            overflow: hidden;
            position: absolute;
            left: 396px;
            top: 198px;
        }

        * html a#sd {
            height: 45px;
        }

            a#sd:hover {
                background: transparent url(images/sd.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*河北*/ a#hb {
            display: block;
            width: 58px;
            height: 0;
            padding-top: 81px;
            overflow: hidden;
            position: absolute;
            left: 377px;
            top: 146px;
        }

        * html a#hb {
            height: 81px;
        }

            a#hb:hover {
                background: transparent url(images/heb.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*北京*/ a#bj {
            display: block;
            width: 17px;
            height: 0;
            padding-top: 18px;
            overflow: hidden;
            position: absolute;
            left: 394px;
            top: 167px;
        }

        * html a#bj {
            height: 18px;
        }

            a#bj:hover {
                background: transparent url(images/bj.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*天津*/ a#tj {
            display: block;
            width: 15px;
            height: 0;
            padding-top: 20px;
            overflow: hidden;
            position: absolute;
            left: 406px;
            top: 175px;
        }

        * html a#tj {
            height: 20px;
        }

            a#tj:hover {
                background: transparent url(images/tj.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*陕西*/ a#shx {
            display: block;
            width: 55px;
            height: 0;
            padding-top: 93px;
            overflow: hidden;
            position: absolute;
            left: 303px;
            top: 188px;
        }

        * html a#shx {
            height: 93px;
        }

            a#shx:hover {
                background: transparent url(images/shx.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*山西*/ a#sx {
            display: block;
            width: 37px;
            height: 0;
            padding-top: 73px;
            overflow: hidden;
            position: absolute;
            left: 350px;
            top: 173px;
        }

        * html a#sx {
            height: 73px;
        }

            a#sx:hover {
                background: transparent url(images/sx.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*河南*/ a#hn {
            display: block;
            width: 62px;
            height: 0;
            padding-top: 56px;
            overflow: hidden;
            position: absolute;
            left: 351px;
            top: 224px;
        }

        * html a#hn {
            height: 56px;
        }

            a#hn:hover {
                background: transparent url(images/hl.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*江苏*/ a#js {
            display: block;
            width: 56px;
            height: 0;
            padding-top: 50px;
            overflow: hidden;
            position: absolute;
            left: 409px;
            top: 232px;
        }

        * html a#js {
            height: 50px;
        }

            a#js:hover {
                background: transparent url(images/js.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*安徽*/ a#ah {
            display: block;
            width: 52px;
            height: 0;
            padding-top: 63px;
            overflow: hidden;
            position: absolute;
            left: 397px;
            top: 239px;
        }

        * html a#ah {
            height: 63px;
        }

            a#ah:hover {
                background: transparent url(images/ah.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*上海*/ a#sh {
            display: block;
            width: 10px;
            height: 0;
            padding-top: 7px;
            overflow: hidden;
            position: absolute;
            left: 460px;
            top: 273px;
        }

        * html a#sh {
            height: 7px;
        }

            a#sh:hover {
                background: transparent url(images/sh.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*浙江*/ a#zj {
            display: block;
            width: 40px;
            height: 0;
            padding-top: 50px;
            overflow: hidden;
            position: absolute;
            left: 433px;
            top: 275px;
        }

        * html a#zj {
            height: 50px;
        }

            a#zj:hover {
                background: transparent url(images/zj.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*江西*/ a#jx {
            display: block;
            width: 51px;
            height: 0;
            padding-top: 67px;
            overflow: hidden;
            position: absolute;
            left: 388px;
            top: 297px;
        }

        * html a#jx {
            height: 67px;
        }

            a#jx:hover {
                background: transparent url(images/jx.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*福建*/ a#hj {
            display: block;
            width: 47px;
            height: 0;
            padding-top: 59px;
            overflow: hidden;
            position: absolute;
            left: 414px;
            top: 313px;
        }

        * html a#hj {
            height: 59px;
        }

            a#hj:hover {
                background: transparent url(images/hj.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*广东*/ a#gd {
            display: block;
            width: 81px;
            height: 0;
            padding-top: 63px;
            overflow: hidden;
            position: absolute;
            left: 350px;
            top: 352px;
        }

        * html a#gd {
            height: 63px;
        }

            a#gd:hover {
                background: transparent url(images/gd.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*海南*/ a#hl {
            display: block;
            width: 25px;
            height: 0;
            padding-top: 21px;
            overflow: hidden;
            position: absolute;
            left: 338px;
            top: 418px;
        }

        * html a#hl {
            height: 21px;
        }

            a#hl:hover {
                background: transparent url(images/hal.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*广西*/ a#gx {
            display: block;
            width: 80px;
            height: 0;
            padding-top: 59px;
            overflow: hidden;
            position: absolute;
            left: 294px;
            top: 343px;
        }

        * html a#gx {
            height: 59px;
        }

            a#gx:hover {
                background: transparent url(images/gx.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*贵州*/ a#gz {
            display: block;
            width: 62px;
            height: 0;
            padding-top: 52px;
            overflow: hidden;
            position: absolute;
            left: 284px;
            top: 312px;
        }

        * html a#gz {
            height: 52px;
        }

            a#gz:hover {
                background: transparent url(images/gz.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*云南*/ a#yn {
            display: block;
            width: 92px;
            height: 0;
            padding-top: 92px;
            overflow: hidden;
            position: absolute;
            left: 218px;
            top: 313px;
        }

        * html a#yn {
            height: 92px;
        }

            a#yn:hover {
                background: transparent url(images/yn.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*四川*/ a#sc {
            display: block;
            width: 111px;
            height: 0;
            padding-top: 96px;
            overflow: hidden;
            position: absolute;
            left: 222px;
            top: 250px;
        }

        * html a#sc {
            height: 96px;
        }

            a#sc:hover {
                background: transparent url(images/sc.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*重庆*/ a#cq {
            display: block;
            width: 51px;
            height: 0;
            padding-top: 47px;
            overflow: hidden;
            position: absolute;
            left: 299px;
            top: 275px;
        }

        * html a#cq {
            height: 47px;
        }

            a#cq:hover {
                background: transparent url(images/chq.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*湖南*/ a#hun {
            display: block;
            width: 56px;
            height: 0;
            padding-top: 65px;
            overflow: hidden;
            position: absolute;
            left: 339px;
            top: 298px;
        }

        * html a#hun {
            height: 65px;
        }

            a#hun:hover {
                background: transparent url(images/hn.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*湖北*/ a#hub {
            display: block;
            width: 82px;
            height: 0;
            padding-top: 49px;
            overflow: hidden;
            position: absolute;
            left: 332px;
            top: 261px;
        }

        * html a#hub {
            height: 49px;
        }

            a#hub:hover {
                background: transparent url(images/hb.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }
        /*台湾*/ a#tw {
            display: block;
            width: 17px;
            height: 0;
            padding-top: 40px;
            overflow: hidden;
            position: absolute;
            left: 462px;
            top: 347px;
        }

        * html a#tw {
            height: 40px;
        }

            a#tw:hover {
                background: transparent url(images/tw.gif) no-repeat 0 0;
                overflow: visible;
                text-indent: -9000px;
            }

        .absoluteDiv {
            position: absolute;
            background-repeat: no-repeat;
        }

        .textDiv {
            width: 10px;
            height: 20px;
        }

        body {
            font-size: 12px;
        }
        /*jQuery UI fakes 默认字体大小  */ .ui-widget {
            font-size: 1em;
        }

        .ui-dialog .ui-dialog-buttonpane {
            padding-top: .1em;
            padding-bottom: .1em;
        }
        /*表格基本样式*/ .show_projectNumber {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .show_projectNumber td {
                border: solid 1px #CCC;
                font-size: 12px;
                font-family: "微软雅黑";
                height: 20px;
            }

        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            background: url(../images/bg_tdhead.gif) repeat-x;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
            margin-top: 2px;
            table-layout: fixed;
        }

            .cls_content_head td {
                height: 22px;
                border: solid 1px #CCC;
                word-break: keep-all; /* 不换行 */
                white-space: nowrap; /* 不换行 */
                overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
                text-overflow: ellipsis; /* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用。*/
            }

        .tb_row {
            border-collapse: collapse;
            border: solid 1px #CCC;
            width: 100%;
            table-layout: fixed;
        }

            .tb_row td {
                border: solid 1px #CCC;
                word-break: keep-all; /* 不换行 */
                white-space: nowrap; /* 不换行 */
                overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
                text-overflow: ellipsis; /* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用。*/
            }
    </style>
    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/LeadershipCockpit/Map.js" type="text/javascript"></script>
</head>
<body>
    <form runat="server">
        <div>
            <dl id="gmap">
                <dt><a id="title2" href="#nogo"></a></dt>
                <dd id="新疆">
                    <a id="xj" title="新疆" href="#">新疆</a></dd>
                <dd id="西藏">
                    <a id="xz" title="西藏" href="#">西藏</a></dd>
                <dd id="青海">
                    <a id="qh" title="青海" href="#">青海</a></dd>
                <dd id="甘肃">
                    <a id="gs" title="甘肃" href="#">甘肃</a></dd>
                <dd id="内蒙古">
                    <a id="lmg" title="内蒙古" href="#">内蒙古</a></dd>
                <dd id="黑龙江">
                    <a id="hlj" title="黑龙江" href="#">黑龙江</a></dd>
                <dd id="吉林">
                    <a id="jl" title="吉林" href="#">吉林</a></dd>
                <dd id="辽宁">
                    <a id="ln" title="辽宁" href="#">辽宁</a></dd>
                <dd id="山东">
                    <a id="sd" title="山东" href="#">山东</a></dd>
                <dd id="河北">
                    <a id="hb" title="河北" href="#">河北</a></dd>
                <dd id="山西">
                    <a id="sx" title="山西" href="#">山西</a></dd>
                <dd id="北京">
                    <a id="bj" title="北京" href="#">北京</a></dd>
                <dd id="天津">
                    <a id="tj" title="天津" href="#">天津</a></dd>
                <dd id="陕西">
                    <a id="shx" title="陕西" href="#">陕西</a></dd>
                <dd id="宁夏">
                    <a id="nx" title="宁夏" href="#">宁夏</a></dd>
                <dd id="河南">
                    <a id="hn" title="河南" href="#">河南</a></dd>
                <dd id="江苏">
                    <a id="js" title="江苏" href="#">江苏</a></dd>
                <dd id="安徽">
                    <a id="ah" title="安徽" href="#">安徽</a></dd>
                <dd id="上海">
                    <a id="sh" title="上海" href="#">上海</a></dd>
                <dd id="浙江">
                    <a id="zj" title="浙江" href="#">浙江</a></dd>
                <dd id="江西">
                    <a id="jx" title="江西" href="#">江西</a></dd>
                <dd id="福建">
                    <a id="hj" title="福建" href="#">福建</a></dd>
                <dd id="广东">
                    <a id="gd" title="广东" href="#">广东</a></dd>
                <dd id="海南">
                    <a id="hl" title="海南" href="#">海南</a></dd>
                <dd id="广西">
                    <a id="gx" title="广西" href="#">广西</a></dd>
                <dd id="贵州">
                    <a id="gz" title="贵州" href="#">贵州</a></dd>
                <dd id="云南">
                    <a id="yn" title="云南" href="#">云南</a></dd>
                <dd id="四川">
                    <a id="sc" title="四川" href="#">四川</a></dd>
                <dd id="重庆">
                    <a id="cq" title="重庆" href="#l">重庆</a></dd>
                <dd id="湖南">
                    <a id="hun" title="湖南" href="#">湖南</a></dd>
                <dd id="湖北">
                    <a id="hub" title="湖北" href="#">湖北</a></dd>
                <dd id="台湾">
                    <a id="tw" title="台湾" href="#">台湾</a></dd>
                <!--<dd><a id="xg" title="香港" href="#">香港</a></dd>
		<dd><a id="am" title="澳门" href="#">澳门</a></dd>	
		-->
            </dl>
        </div>
        <!--项目列表PopArea-->
        <div id="PopAreaForProjectListDiv" style="display: none;">
            <!--内容Div-->
            <div id="PopContext">
                <table class="cls_content_head">
                    <tr>
                        <td align="center" style="width: 80px;">编号
                        </td>
                        <td align="center" style="width: 200px;">关联合同
                        </td>
                        <td align="center" style="width: 150px;">项目类型
                        </td>
                        <td align="center" style="width: 200px;">建设单位
                        </td>
                        <td align="center" style="width: 50px;">级别
                        </td>
                        <td align="center" style="width: 150px;">开始时间
                        </td>
                        <td align="center" style="width: 150px;">结束时间
                        </td>
                        <td align="center" style="width: 40px;">操作
                        </td>
                    </tr>
                </table>
                <table class="tb_row" id="PopAreaTableForProjectList">
                </table>
            </div>
            <!--分页Div-->
            <div style="height: 10%" id="PopPager">
            </div>
        </div>
        <!--项目详细信息PopArea-->
        <div id="PopAreaForPorProjectDetail" style="display: none;">
            <table class="show_projectNumber" style="height: 200px">
                <tr>
                    <td style="width: 120px">项目名称：
                    </td>
                    <td style="width: 300px">
                        <span id="ProjectName"></span>
                    </td>
                    <td style="width: 120px">合同关联：
                    </td>
                    <td>
                        <span id="CopertaionName"></span>
                    </td>
                </tr>
                <tr>
                    <td>结构样式：
                    </td>
                    <td>
                        <span id="StructStyleStringHtml"></span>
                    </td>
                    <td>项目类型：
                    </td>
                    <td>
                        <span id="ProjectType"></span>
                    </td>
                </tr>
                <tr>
                    <td>管理级别：
                    </td>
                    <td>
                        <span id="ManagerLevelString"></span>
                    </td>
                    <td>设计阶段：
                    </td>
                    <td id="content">
                        <span id="Status"></span>
                    </td>
                </tr>
                <tr>
                    <td>项目来源：
                    </td>
                    <td>
                        <span id="ProjectFrom"></span>
                    </td>
                    <td>合同额：
                    </td>
                    <td>
                        <span id="CoperationFinancial"></span>万元
                    </td>
                </tr>
                <tr>
                    <td>建设单位：
                    </td>
                    <td>
                        <span id="BuildUnit"></span>
                    </td>
                    <td>承接部门：
                    </td>
                    <td>
                        <span id="UnderTakeDepartment"></span>
                    </td>
                </tr>
                <tr>
                    <td>建设地点：
                    </td>
                    <td>
                        <span id="Address"></span>
                    </td>
                    <td>建设规模：
                    </td>
                    <td>
                        <span id="BuildScale"></span>平米
                    </td>
                </tr>
                <tr>
                    <td class="style3">甲方负责人：
                    </td>
                    <td class="style3">
                        <span id="ChargeManJia"></span>
                    </td>
                    <td class="style3">电话：
                    </td>
                    <td class="style3">
                        <span id="PhoneNumber"></span>
                    </td>
                </tr>
                <tr>
                    <td>项目开始日期：
                    </td>
                    <td>
                        <span id="StartTimeString"></span>
                    </td>
                    <td>项目完成日期：
                    </td>
                    <td>
                        <span id="EndTimeString"></span>
                    </td>
                </tr>
                <tr>
                    <td>项目备注：
                    </td>
                    <td colspan="3">
                        <span id="Remark"></span>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
