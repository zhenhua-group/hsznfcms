﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CoperationChargeReportViewBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationChargeReportViewBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/FunctionChart/FusionCharts.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>合同收费详情</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>合同收费详情</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询合同收款明细
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">

                        <h3 class="form-section">合同收费基本信息</h3>
                        <div class="row">
                            <table class="table table-bordered" style="width:95%;" align="center">
                                <tr>
                                    <td>合同名称:</td>
                                    <td><asp:Label ID="coperationName" runat="server"></asp:Label></td>
                                    <td>实收总额:</td>
                                    <td><asp:Label ID="coperationAccount" runat="server"></asp:Label></td>
                                    <td>合同额:</td>
                                    <td><asp:Label ID="coperationRealReceivables" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </div>

                        <h3 class="form-section">收费信息统计报表</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Literal ID="LiteralReportView" runat="server"></asp:Literal>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-offset-12 col-md-12">
                                    <asp:Literal ID="PanelColor" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="row">
                            <asp:Literal ID="LiteralAccountDetail" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回
                            </button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
