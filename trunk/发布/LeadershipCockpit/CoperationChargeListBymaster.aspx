﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CoperationChargeListBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationChargeListBymaster" %>
<%@ Register TagPrefix="cc1" Namespace="Geekees.Common.Controls" Assembly="ASTreeView, Version=1.5.9.0, Culture=neutral, PublicKeyToken=521e0b4262a9001c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />

    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/LeadershipCockpit/CoperationCharge_jq.js"></script>
    <script type="text/javascript">

        $(function () {
            //年份选择
            $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
                var index = $(this).val();
                if (index != "-1" && $("#ctl00_ContentPlaceHolder1_drpMonth").val() == "0" && $("#ctl00_ContentPlaceHolder1_drpJidu").val() == "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
                else if (index == "-1") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                }
            });
            //季度选择
            $("#ctl00_ContentPlaceHolder1_drpJidu").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                }
            });
            //月份选择
            $("#ctl00_ContentPlaceHolder1_drpMonth").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
            });
            //时间段
            $("#ctl00_ContentPlaceHolder1_cbx_time").click(function () {
                if ($(this).is(":checked")) {
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().hide();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                    //  $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();
                    // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();
                    // $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "");
                    //时间段选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("1");

                }
                else {
                    //隐藏年
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drp_year").parent().show();
                    var myDate = new Date();
                    var curryear = myDate.getFullYear();
                    // $("#ctl00_ContentPlaceHolder1_drp_year").val(curryear);
                    //隐藏季度
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpJidu").parent().show();
                    // $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", false);
                    //隐藏月份
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().show();
                    $("#ctl00_ContentPlaceHolder1_drpMonth").parent().show();
                    //  $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", false);

                    //显示合同时间段
                    $(this).parents("td").prev().css("display", "none");
                    //时间段没有选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("0");

                }
            });
            //初始化数据显示
            Init();
        });
        function Init() {
            //时间段隐藏域的值
            if ($("#ctl00_ContentPlaceHolder1_hid_time").val() == "1") {
                //隐藏年
                $("#ctl00_ContentPlaceHolder1_drp_year").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drp_year").parent().hide();

                //隐藏季度
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpJidu").parent().hide();

                //隐藏月份
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().prev().hide();
                $("#ctl00_ContentPlaceHolder1_drpMonth").parent().hide();

                //显示合同时间段
                $("#ctl00_ContentPlaceHolder1_cbx_time").parents("td").prev().css("display", "");
            }
            if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
                $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>合同收款综合统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>合同收款综合统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>合同收款查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                            <cc1:ASDropDownTreeView ID="drpunit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px"  InitialDropdownText="-------全院部门-------" />
                                        </td>
                            <td>合同年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                             <td class="year">季度：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpJidu" runat="server">
                                    <asp:ListItem Value="0">--全部--</asp:ListItem>
                                    <asp:ListItem Value="1" Text="一季度"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二季度"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三季度"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四季度"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="year">月份：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpMonth" runat="server">
                                    <asp:ListItem Value="0" Text="--全部--"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="一月"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二月"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三月"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四月"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="五月"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="六月"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="七月"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="八月"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="九月"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="十月"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="十一月"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="十二月"></asp:ListItem>
                                </asp:DropDownList>

                            </td>   
                             <td style="display:none;">
                                <div class="input-group">
                                    合同时间：
                                    <input type="text" name="txt_year1" id="txt_year1" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />至<input
                                            type="text" name="txt_year2" id="txt_year2" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />
                                </div>
                            </td>                        
                            <td>
                                <div class="input-group">
                                    <input type="checkbox" id="cbx_time" runat="server"/>时间段
                                    <input type="hidden" id="hid_time" value="0" runat="server"/>
                                </div>
                            </td>
                            <td>收费时间:</td>
                            <td>
                                <input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                    class="Wdate" runat="Server" style="width: 120px; height: 30px; vertical-align: middle; border: 1px solid #e5e5e5;" /></td>
                            <td>
                                <input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                    class="Wdate" runat="Server" style="width: 120px; vertical-align: middle; height: 30px; border: 1px solid #e5e5e5;" /></td>
                            <td>&nbsp;<input type="button" value="查询" id="btn_cx" class="btn blue" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>自定义条件查询
                    </div>
                    <div class="tools">
                    </div>
                    <div class="actions">
                        <a href="CoperationChargeRpt.aspx" class="btn red btn-sm">收款报表</a>
                        <asp:Button class="btn red btn-sm" Text="导出Excel" ID="btn_export" runat="server" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_id">
                                    <tr>
                                      <%--  <td align="left">
                                            <input id="cpr_Unit" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Unit">生产部门</span>
                                        </td>--%>
                                        <td align="left">
                                            <input id="cpr_Year" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Year">合同年份</span>
                                        </td>

                                        <td align="left">
                                            <input id="cpr_Name" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Name">合同名称</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Acount" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Acount">合同额</span>
                                        </td>
                                        <td align="left">
                                            <input id="ssze" name="bb" class="cls_audit" type="checkbox" /><span for="ssze">合同收费</span>
                                        </td>

                                        <td align="left">
                                            <input id="progress" name="bb" class="cls_audit" type="checkbox" /><span for="progress">收款进度</span>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <hr />
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="tbl_id2">
                                        <tr for="cpr_Unit">
                                            <td style="width: 100px">生产部门:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <asp:DropDownList ID="drp_unit" CssClass="form-control " runat="server" AppendDataBoundItems="True" Width="160px">
                                                            <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <input type="button" class="btn blue btn-sm" id="btn_chooes" value="选择" />
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-8" id="labUnit" style="font-size: 14px;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="cpr_Year">
                                            <td>年份:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <asp:DropDownList ID="drp_year1" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="140px">
                                                                <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp; 至 &nbsp;
                                                             <asp:DropDownList ID="drp_year2" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="140px">
                                                                 <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                             </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr for="cpr_Name">
                                            <td>合同名称:
                                            </td>
                                            <td>
                                                <input type="text" class="form-control input-sm" id="txt_cprName" style="width: 390px;" runat="server" />
                                            </td>
                                        </tr>
                                        <tr for="cpr_Acount">
                                            <td>合同额:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txtproAcount" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtproAcount2" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp;万元
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr for="ssze">
                                            <td>合同收费:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="projcharge1" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="projcharge2" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp;万元
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr for="progress">
                                            <td>收款进度:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" id="txtprogress" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtprogress2" class="form-control input-sm" style="width: 180px;" runat="server" />
                                                            &nbsp;%
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <input type="button" class="btn
                blue"
                                                    id="btn_Search" value="查询" />&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>合同收款明细列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">

                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>

            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="HiddenUnit" runat="server" Value="" />
     <asp:HiddenField ID="SelectType" runat="server" Value="0" />
    <div id="AuditUnit" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">生产部门选择</h4>
        </div>
        <div class="modal-body" id="auditShow">
            <asp:CheckBoxList ID="Chk_unit" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
            </asp:CheckBoxList>
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" id="btn_CanceAudit" class="btn btn-default">
                关闭</button>
        </div>
    </div>

</asp:Content>
