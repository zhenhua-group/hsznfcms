﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CoperationProjectListBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationProjectListBymaster" %>

<%@ Register Assembly="xgo.Components" Namespace="xgo.Components" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>高级查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>高级查询</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>高级查询
                    </div>
                    
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>查询类型:</td>
                            <td>
                                <asp:DropDownList ID="fltype" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">项目</asp:ListItem>
                                    <asp:ListItem Value="1">合同</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>关键字:</td>
                            <td>
                                <asp:TextBox ID="keywords" runat="server" CssClass="form-control input-sm"></asp:TextBox></td>
                            <td>
                                <asp:Button ID="btn_searchaa" runat="server" CssClass="btn blue" OnClick="btn_search_Click"
                                    Text="查询" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>高级查询列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button runat="Server" CssClass="btn red btn-sm" Text="导出Excel" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Repeater ID="gv_project" runat="server">
                                <ItemTemplate>
                                    <table width="100%" border="0" cellspacing="1" bgcolor="#cccccc" class="table table-bordered" style="font-size: 12px; color: #666; margin: 4px 0;">
                                        <tr>
                                            <td width="10%" align="left" bgcolor="#FFFFFF" height="30">&nbsp;<%#Eval("lbtype").ToString() == "0"?"项目":"合同"%>名称：
                                            </td>
                                            <td width="90%" bgcolor="#FFFFFF" align="left">&nbsp;<a href="<%#Eval("lbtype").ToString()=="0"?"../ProjectManage/ShowProjectBymaster.aspx?flag=coprolist&pro_id="+Eval("pro_id")+"":"../Coperation/cpr_ShowCoprationBymaster.aspx?flag=coprolist&cprid="+Eval("pro_id")+"" %>&keyws=<%=keyws %>&fltype=<%=action %>"
                                                title="<%# Eval("pro_name") %>" rel='<%# Eval("pro_id") %>' class="chk"><%# Eval("pro_name").ToString()%></a>
                                            </td>
                                            <%#Eval("lbtype").ToString() == "0" ? "<Tr bgcolor='#f0f0f0'><td align='left' height='30'>&nbsp;工 程 号：</td><td align='left'>" + Eval("Pro_Number") + "</td></tr>" : ""%>
                                        </tr>
                                        <tr bgcolor="#e6e6e6">
                                            <td align="left" height="30">&nbsp;<%#Eval("lbtype").ToString() == "0"?"项目":"合同"%>备注：
                                            </td>
                                            <td align="left">&nbsp;<%# Eval("pro_Intro").ToString().Length > 200 ? Eval("pro_Intro").ToString().Substring(0, 200) + "......" : Eval("pro_Intro").ToString()%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cls_data_bottom" style="margin: 5px 0; font-size: 12px;">
                                <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="ChangePage"
                                    HorizontalAlign="center" Style="position: relative; bottom: 0px; left: -10px; font-size: 12px;"
                                    ForeColor="Black" FirstPageText="<<首页 " LastPageText=" 尾页>>"
                                    PrevPageText="<上一页" NextPageText=" 下一页>" showinputbox="Never" NumericButtonTextFormatString=" [{0}] "
                                    CustomInfoSectionWidth="35%" AlwaysShow="True" CustomInfoHTML="<div style='padding-top:5px;'>总计：%RecordCount%条 页次：%CurrentPageIndex%/%PageCount%页</div>"
                                    ShowCustomInfoSection="Left" ShowPageIndex="False" PageIndexBoxType="TextBox"
                                    ShowPageIndexBox="Always" SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳到">
                                </webdiyer:AspNetPager>
                                <%-- <cc1:Pager ID="Pager1" runat="server" Width="100%" ControlToPagination="gv_project"
                PageSize="1" />--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
</asp:Content>
