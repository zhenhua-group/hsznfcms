﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CoperationCountBymaster.aspx.cs" Inherits="TG.Web.LeadershipCockpit.CoperationCountBymaster" %>

<%@ Register TagPrefix="cc1" Namespace="Geekees.Common.Controls" Assembly="ASTreeView, Version=1.5.9.0, Culture=neutral, PublicKeyToken=521e0b4262a9001c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />

    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/LeadershipCockpit/CoperationCountBymaster.js"></script>
    <script type="text/javascript">

        $(function () {
            //年份选择
            $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
                var index = $(this).val();
                if (index != "-1" && $("#ctl00_ContentPlaceHolder1_drpMonth").val() == "0" && $("#ctl00_ContentPlaceHolder1_drpJidu").val() == "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
                else if (index == "-1") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                }
            });
            //季度选择
            $("#ctl00_ContentPlaceHolder1_drpJidu").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                }
            });
            //月份选择
            $("#ctl00_ContentPlaceHolder1_drpMonth").change(function () {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
            });
            //时间段
            $("#ctl00_ContentPlaceHolder1_cbx_time").click(function () {
                if ($(this).is(":checked")) {
                    //隐藏年
                    $(".year").hide();

                    //显示合同时间段
                    $(".time").css("display", "");

                    //时间段选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("1");

                }
                else {
                    //隐藏年
                    $(".year").show();

                    //显示合同时间段
                    $(".time").css("display", "none");
                    //时间段没有选中隐藏域值
                    $("#ctl00_ContentPlaceHolder1_hid_time").val("0");

                }
            });
            //初始化数据显示
            Init();
        });
        function Init() {
            //时间段隐藏域的值
            if ($("#ctl00_ContentPlaceHolder1_hid_time").val() == "1") {
                //隐藏年
                $(".year").hide();

                //显示合同时间段
                $(".time").css("display", "");
            }
            if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
                $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled", true);
                $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled", true);
            }

        }
    </script>
    <style type="text/css">
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

        #tbOne td, #tbTwo td, #tbThree td, #tbFour td, #tbFive td {
            border: solid 1px #CCC;
            font-size: 10pt;
            font-family: "微软雅黑";
        }

        .cls_chart {
            width: 100%;
            height: 500px;
        }

        .cls_data_bottom {
            width: 100%;
            margin: 0 auto;
            height: 25px;
            border: solid 1px #CCC;
        }

        .cls_util {
            margin: 0 auto;
            text-align: right;
            height: 20px;
            font-size: 13px;
            color: red;
            font-weight: bold;
            font-family: 宋体;
        }
        /*数据基本样式*/
        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 12px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
            }

        .cls_data {
            width: 99%;
            margin: 0 auto;
        }



        .tr_in {
            background-color: #CCC;
        }

        .gridView_comm {
            margin: 0 auto;
            border-collapse: collapse;
            border-top: none;
        }

            .gridView_comm td {
                height: 18px;
                border-collapse: collapse;
                border: 1px solid Gray;
                border-top: none;
            }

            .gridView_comm a, a:hover, a:visited, a:link, a:active {
                color: Blue;
                text-decoration: none;
            }

            .gridView_comm .cls_column {
                overflow: hidden;
                text-overflow: ellipsis;
                -o-text-overflow: ellipsis;
                white-space: nowrap;
            }

            .gridView_comm tr:hover {
                background-color: #CCC;
            }

        .borderClass {
            border: 1px solid #9BA0A6;
        }

        .tdvalue {
            min-width: 120px;
            width: 200px;
        }

        .tdtext {
            min-width: 70px;
            width: 90px;
        }

        .tdbetween {
            min-width: 50px;
            width: 90px;
        }

        #tbl_columns > tbody > tr > td {
            padding-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>合同综合统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a> <i class="fa fa-angle-right"></i><a>数据统计</a><i class="fa fa-angle-right"></i><a>合同综合统计</a>
    </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>简单查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" style="width: 100%; margin: 0 auto;" id="tbl_columns">
                        <tr>
                            <td class="tdtext">统计类型:</td>
                            <td>
                                <asp:DropDownList ID="drp_type" runat="server" CssClass="form-control" Width="200px">
                                    <asp:ListItem Value="0" Selected="True">合同目标完成</asp:ListItem>
                                    <asp:ListItem Value="1">合同审批情况</asp:ListItem>
                                    <asp:ListItem Value="2">合同等级</asp:ListItem>
                                    <asp:ListItem Value="3">合同类别(数量)</asp:ListItem>
                                    <asp:ListItem Value="4">合同类别(合同额)</asp:ListItem>
                                    <asp:ListItem Value="5">合同类别(规模)</asp:ListItem>
                                    <asp:ListItem Value="6">合同类别(收款额)</asp:ListItem>
                                    <asp:ListItem Value="7">合同行业性质(数量)</asp:ListItem>
                                    <asp:ListItem Value="8">合同行业性质(合同额)</asp:ListItem>
                                    <asp:ListItem Value="9">合同行业性质(规模)</asp:ListItem>
                                    <asp:ListItem Value="10">合同行业性质(收款额)</asp:ListItem>
                                </asp:DropDownList></td>
                            <td class="tdtext">生产部门:</td>

                            <td>
                                <cc1:ASDropDownTreeView ID="drpunit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全院部门-------" />
                            </td>

                            <td class="tdtext">收费时间:</td>
                            <td style="width:250px;">
                                <div class="input-group">
                                    <input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" style="width: 100px; height: 22px; vertical-align: middle; border: 1px solid #e5e5e5;" />&nbsp;至&nbsp;
                                    <input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" style="width: 100px; vertical-align: middle; height: 22px; border: 1px solid #e5e5e5;" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="year">年份：</td>
                            <td class="year">
                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="200px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">------选择年份------</asp:ListItem>
                                </asp:DropDownList></td>
                            <td class="year">季度：</td>
                            <td class="year">
                                <asp:DropDownList ID="drpJidu" runat="server" Width="200px">
                                    <asp:ListItem Value="0">------全部------</asp:ListItem>
                                    <asp:ListItem Value="1" Text="一季度"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二季度"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三季度"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四季度"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="year">月份：
                            </td>
                            <td class="year">
                                <asp:DropDownList ID="drpMonth" runat="server" Width="200px">
                                    <asp:ListItem Value="0" Text="------全部------"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="一月"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="二月"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="三月"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="四月"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="五月"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="六月"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="七月"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="八月"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="九月"></asp:ListItem>
                                    <asp:ListItem Value="10" Text="十月"></asp:ListItem>
                                    <asp:ListItem Value="11" Text="十一月"></asp:ListItem>
                                    <asp:ListItem Value="12" Text="十二月"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td style="display: none;" class="time">合同时间：
                            </td>
                            <td style="display: none;" class="time">
                                <div class="input-group">
                                    <input type="text" name="txt_year1" id="txt_year1" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />&nbsp;至&nbsp;<input
                                            type="text" name="txt_year2" id="txt_year2" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />
                                </div>
                            </td>
                            <td>

                                <input type="checkbox" id="cbx_time" runat="server" />时间段
                                    <input type="hidden" id="hid_time" value="0" runat="server" />

                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" style="text-align: center;">
                                <input type="button" value="查询" id="btn_cx" class="btn blue" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>自定义高级查询
                    </div>
                    <div class="tools">
                    </div>
                    <div class="actions">
                        <a href="CoperationCountRpt.aspx" class="btn red btn-sm">合同统计报表</a>
                        <asp:Button runat="Server" CssClass="btn red btn-sm" Text="导出Excel" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chkDiv" style="width: 100%;">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tb_check0">
                            <tr>

                                <%-- <td align="left">
                                    <input id="chk_CprYear" name="bb" class="cls_audit" type="checkbox" clo="0" /><span for="CprYear">生产部门</span>
                                </td>--%>
                                <td align="left">
                                    <input id="chk_CprTargetAcount" name="bb" class="cls_audit" type="checkbox" clo="1" /><span for="CprTargetAcount">合同目标值</span>
                                </td>
                                <td align="left">
                                    <input id="chk_CprAcount" name="bb" class="cls_audit" type="checkbox" clo="2" /><span for="CprAcount">完成合同额</span>
                                </td>
                                <td align="left">
                                    <input id="chk_CprPercent" name="bb" class="cls_audit" type="checkbox" clo="3" /><span for="CprPercent">合同额完成比例</span>
                                </td>

                                <td align="left">
                                    <input id="chk_TargetChageAcount" name="bb" class="cls_audit" type="checkbox" clo="4" /><span for="TargetChageAcount">目标产值</span>
                                </td>
                                <td align="left">
                                    <input id="chk_ChageAcount" name="bb" class="cls_audit" type="checkbox" clo="5" /><span for="ChageAcount">完成产值</span>
                                </td>
                                <td align="left">
                                    <input id="chk_ChargePercent" name="bb" class="cls_audit" type="checkbox" clo="6" /><span for="ChargePercent">产值完成比例</span>
                                </td>

                            </tr>
                        </table>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tb_check1" style="display: none;">
                            <tr>
                                <%--  <td align="left">
                                    <input id="Checkbox28" name="bb" class="cls_audit" type="checkbox" clo="0" /><span for="CprYear">生产部门</span>
                                </td>--%>
                                <td align="left">
                                    <input id="Checkbox32" name="bb" class="cls_audit" type="checkbox" clo="1" /><span for="CprYear">年份</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox1" name="bb" class="cls_audit" type="checkbox" clo="2" /><span for="ChargeDate">已签合同数量</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox2" name="bb" class="cls_audit" type="checkbox" clo="3" /><span for="CprTargetAcount">签订合同额</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox3" name="bb" class="cls_audit" type="checkbox" clo="4" /><span for="CprAcount">建筑规模</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox4" name="bb" class="cls_audit" type="checkbox" clo="5" /><span for="CprPercent">收款金额</span>
                                </td>

                                <td align="left">
                                    <input id="Checkbox5" name="bb" class="cls_audit" type="checkbox" clo="6" /><span for="TargetChageAcount">审批通过合同数</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox6" name="bb" class="cls_audit" type="checkbox" clo="7" /><span for="ChageAcount">正在审批合同数</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox7" name="bb" class="cls_audit" type="checkbox" clo="8" /><span for="ChargePercent">未发起审批合同数</span>
                                </td>

                            </tr>
                        </table>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tb_check2" style="display: none;">
                            <tr>
                                <%--  <td align="left">
                                    <input id="Checkbox29" name="bb" class="cls_audit" type="checkbox" clo="0" /><span for="ChargeDate">生产部门</span>
                                </td>--%>
                                <td align="left">
                                    <input id="Checkbox33" name="bb" class="cls_audit" type="checkbox" clo="1" /><span for="CprYear">年份</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox8" name="bb" class="cls_audit" type="checkbox" clo="2" /><span for="ChargeDate">特级数量</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox9" name="bb" class="cls_audit" type="checkbox" clo="3" /><span for="CprTargetAcount">特级合同额</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox10" name="bb" class="cls_audit" type="checkbox" clo="4" /><span for="CprAcount">特级收款额</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox11" name="bb" class="cls_audit" type="checkbox" clo="5" /><span for="CprPercent">一级数量</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox12" name="bb" class="cls_audit" type="checkbox" clo="6" /><span for="TargetChageAcount">一级合同额</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox13" name="bb" class="cls_audit" type="checkbox" clo="7" /><span for="ChageAcount">一级收款额</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox14" name="bb" class="cls_audit" type="checkbox" clo="8" /><span for="ChargePercent">二级数量</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox15" name="bb" class="cls_audit" type="checkbox" clo="9" /><span for="ChargePercent">二级合同额</span>
                                </td>


                            </tr>
                            <tr>
                                <td align="left">
                                    <input id="Checkbox16" name="bb" class="cls_audit" type="checkbox" clo="10" /><span for="ChargePercent">二级收款额</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox17" name="bb" class="cls_audit" type="checkbox" clo="11" /><span for="ChargePercent">三级数量</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox18" name="bb" class="cls_audit" type="checkbox" clo="12" /><span for="ChargePercent">三级合同额</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox19" name="bb" class="cls_audit" type="checkbox" clo="13" /><span for="ChargePercent">三级收款额</span>
                                </td>

                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>

                            </tr>
                        </table>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tb_check3" style="display: none;">
                            <tr>
                                <%-- <td align="left">
                                    <input id="Checkbox30" name="bb" class="cls_audit" type="checkbox" clo="0" /><span for="ChargeDate">生产部门</span>
                                </td>--%>
                                <td align="left">
                                    <input id="Checkbox34" name="bb" class="cls_audit" type="checkbox" clo="1" /><span for="CprYear">年份</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox20" name="bb" class="cls_audit" type="checkbox" clo="2" /><span for="ChargeDate">民用建筑合同</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox21" name="bb" class="cls_audit" type="checkbox" clo="3" /><span for="CprTargetAcount">工业建筑合同</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox22" name="bb" class="cls_audit" type="checkbox" clo="4" /><span for="CprAcount">工程勘察合同</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox23" name="bb" class="cls_audit" type="checkbox" clo="5" /><span for="CprPercent">工程监理合同</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox24" name="bb" class="cls_audit" type="checkbox" clo="6" /><span for="TargetChageAcount">工程施工合同</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox25" name="bb" class="cls_audit" type="checkbox" clo="7" /><span for="ChageAcount">工程咨询合同</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox26" name="bb" class="cls_audit" type="checkbox" clo="8" /><span for="ChargePercent">工程总承包合同</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox27" name="bb" class="cls_audit" type="checkbox" clo="9" /><span for="ChargePercent">工程代建合同</span>
                                </td>

                            </tr>
                        </table>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tb_check4" style="display: none;">
                            <tr>
                                <%--<td align="left">
                                    <input id="Checkbox31" name="bb" class="cls_audit" type="checkbox" clo="0" /><span for="ChargeDate">生产部门</span>
                                </td>--%>
                                <td align="left">
                                    <input id="Checkbox35" name="bb" class="cls_audit" type="checkbox" clo="1" /><span for="CprYear">年份</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox52" name="bb" class="cls_audit" type="checkbox" clo="2" /><span for="ChargeDate">公建</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox53" name="bb" class="cls_audit" type="checkbox" clo="3" /><span for="CprTargetAcount">房地产</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox54" name="bb" class="cls_audit" type="checkbox" clo="4" /><span for="CprAcount">市政</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox55" name="bb" class="cls_audit" type="checkbox" clo="5" /><span for="CprPercent">医院</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox56" name="bb" class="cls_audit" type="checkbox" clo="6" /><span for="TargetChageAcount">电力</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox57" name="bb" class="cls_audit" type="checkbox" clo="7" /><span for="ChageAcount">通信</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox58" name="bb" class="cls_audit" type="checkbox" clo="8" /><span for="ChargePercent">银行</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox59" name="bb" class="cls_audit" type="checkbox" clo="9" /><span for="ChargePercent">学校</span>
                                </td>
                                <td align="left">
                                    <input id="Checkbox60" name="bb" class="cls_audit" type="checkbox" clo="10" /><span for="ChargePercent">涉外</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <hr />
                    <div id="selectDiv" style="width: 100%;">
                        <table class="table table-bordered table-hover" id="tb_select0" style="display: none;">
                            <tr clo="0" style="display: none;">
                                <td style="width: 20%;">生产部门:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="drp_unit1" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" class="btn blue btn-sm" choose="choose" id="btn_chooes" value="选择" />
                                        </div>

                                        <div class="col-md-8" id="divUnit0" style="font-size: 14px;">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="1" style="display: none;">
                                <td>合同目标值:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="input-group">
                                                <input type="text" id="txt_CprTaretAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_CprTaretAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="2" style="display: none;">
                                <td>完成合同额:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txt_CprAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_CprAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="3" style="display: none;">
                                <td>合同额完成比例:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txt_CprAcountPercent1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_CprAcountPercent2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;%
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="4" style="display: none;">
                                <td>目标产值:
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txt_CprTarentCharge1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_CprTarentCharge2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="5" style="display: none;">
                                <td>完成产值:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txt_CprCharge1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_CprCharge2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="6" style="display: none;">
                                <td>产值完成比例:
                             
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txt_CprChargePercent1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_CprChargePercent2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;%
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="table table-bordered table-hover" id="tb_select1" style="display: none;">
                            <tr clo="0" style="display: none;">
                                <td style="width: 20%;">生产部门:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="drp_unit2" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" class="btn blue btn-sm" choose="choose" id="Button1" value="选择" />
                                        </div>

                                        <div class="col-md-8" id="divUnit1" style="font-size: 14px;">
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr clo="1" style="display: none;">
                                <td>年份:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <asp:DropDownList ID="drp_year2_01" CssClass="form-control " runat="server" AppendDataBoundItems="True" Width="180px">
                                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                </asp:DropDownList>
                                                &nbsp; 至 &nbsp;
                                                             <asp:DropDownList ID="drp_year2_02" CssClass="form-control " runat="server" AppendDataBoundItems="True" Width="180px">
                                                                 <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                             </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="2" style="display: none;">
                                <td>已签合同数量:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtCprCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtCprCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="3" style="display: none;">
                                <td>签订合同额:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtCprSignAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtCprSignAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="4" style="display: none;">
                                <td>建筑规模:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtBulid1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtBulid2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;㎡
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="5" style="display: none;">
                                <td>收款金额:
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtChargeCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtChargeCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="6" style="display: none;">
                                <td>审批通过合同数:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtPassCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtPassCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="7" style="display: none;">
                                <td>正在审批合同数:
                             
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtPassingCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtPassingCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="8" style="display: none;">
                                <td>未发起审批合同数:
                             
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtNotPassingCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtNotPassingCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="table table-bordered table-hover" id="tb_select2" style="display: none;">
                            <tr clo="0" style="display: none;">
                                <td style="width: 20%;">生产部门:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="drp_unit3" CssClass="form-control " runat="server" AppendDataBoundItems="True" Width="180px">
                                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" class="btn blue btn-sm" choose="choose" id="Button2" value="选择" />
                                        </div>

                                        <div class="col-md-8" id="divUnit2" style="font-size: 14px;">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="1" style="display: none;">
                                <td>年份:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <asp:DropDownList ID="drp_year3_01" CssClass="form-control " runat="server" AppendDataBoundItems="True" Width="180px">
                                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                </asp:DropDownList>
                                                &nbsp; 至 &nbsp;
                                                             <asp:DropDownList ID="drp_year3_02" CssClass="form-control " runat="server" AppendDataBoundItems="True" Width="180px">
                                                                 <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                             </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="2" style="display: none;">
                                <td>(特级)数量:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtTJCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtTJCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="3" style="display: none;">
                                <td>(特级)合同额:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtTJCprAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtTJCprAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="4" style="display: none;">
                                <td>(特级)收款额:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtTJCprChargeAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtTJCprChargeAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="5" style="display: none;">
                                <td>(一级)数量:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtYJCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtYJCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="6" style="display: none;">
                                <td>(一级)合同额:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtYJCprAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtYJCprAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="7" style="display: none;">
                                <td>(一级)收款额:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtYJCprChargeAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtYJCprChargeAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="8" style="display: none;">
                                <td>(二级)数量:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtEJCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtEJCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="9" style="display: none;">
                                <td>(二级)合同额:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtEJCprAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtEJCprAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="10" style="display: none;">
                                <td>(二级)收款额:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtEJCprChargeAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtEJCprChargeAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="11" style="display: none;">
                                <td>(三级)数量:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtSJCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtSJCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;个
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="12" style="display: none;">
                                <td>(三级)合同额:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtSJCprAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtSJCprAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="13" style="display: none;">
                                <td>(三级)收款额:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtSJCprChargeAcount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtSJCprChargeAcount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;万元
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </table>
                        <table class="table table-bordered table-hover" id="tb_select3" style="display: none;">
                            <tr clo="0" style="display: none;">
                                <td style="width: 20%;">生产部门:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="drp_unit4" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" class="btn blue btn-sm" choose="choose" id="Button4" value="选择" />
                                        </div>

                                        <div class="col-md-8" id="divUnit3" style="font-size: 14px;">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="1" style="display: none;">
                                <td style="width: 20%;">年份:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <asp:DropDownList ID="drp_year4_01" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                </asp:DropDownList>
                                                &nbsp; 至 &nbsp;
                                                             <asp:DropDownList ID="drp_year4_02" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                                 <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                             </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="2" style="display: none;">
                                <td style="width: 20%;">民用建筑合同:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType1_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType1_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="3" style="display: none;">
                                <td>工业建筑合同:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType2_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType2_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="4" style="display: none;">
                                <td>工程勘察合同:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType3_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType3_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="5" style="display: none;">
                                <td>工程监理合同:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType4_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType4_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="6" style="display: none;">
                                <td>工程施工合同:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType5_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType5_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="7" style="display: none;">
                                <td>工程咨询合同:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType6_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType6_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="8" style="display: none;">
                                <td>工程总承包合同:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType7_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType7_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="9" style="display: none;">
                                <td>工程代建合同:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtType8_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtType8_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </table>
                        <table class="table table-bordered table-hover" id="tb_select4" style="display: none;">
                            <tr clo="0" style="display: none;">
                                <td style="width: 20%;">生产部门:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="drp_unit5" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="button" class="btn blue btn-sm" choose="choose" id="Button3" value="选择" />
                                        </div>

                                        <div class="col-md-8" id="divUnit4" style="font-size: 14px;">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="1" style="display: none;">
                                <td>公建:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <asp:DropDownList ID="drp_year5_01" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                </asp:DropDownList>
                                                &nbsp; 至 &nbsp;
                                                             <asp:DropDownList ID="drp_year5_02" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180px">
                                                                 <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                                             </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="2" style="display: none;">
                                <td>公建:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty1_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty1_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="3" style="display: none;">
                                <td>房地产:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty2_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty2_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="4" style="display: none;">
                                <td>市政:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty3_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty3_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="5" style="display: none;">
                                <td>医院:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty4_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty4_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="6" style="display: none;">
                                <td>电力:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty5_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty5_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="7" style="display: none;">
                                <td>通信:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty6_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty6_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr clo="8" style="display: none;">
                                <td>银行:
                               
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty7_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty7_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="9" style="display: none;">
                                <td>学校:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty8_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty8_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr clo="10" style="display: none;">
                                <td>涉外:
                              
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <input type="text" id="txtProperty9_01" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                                &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtProperty9_02" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                                &nbsp;<span></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table align="center" style="width: 100%; display: none;" id="tbSearch">
                        <tr>
                            <td align="center">
                                <input type="button" class="btn blue"
                                    id="btn_search" value="查询" />
                                <%--<input type="submit" class="btn blue" value="查询1" id="btn_searchs" />
                                <input type="submit" class="btn blue" value="查询" id="btn_Search" />--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>合同综合统计列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cls_Container_Report">
                                <div class="cls_Container_Tip">
                                    <p>
                                        <span id="spanYear"></span>合同综合统计按<span id="spanType"></span>统计
                                       <%-- <%= ChartTip %>合同综合统计按<%= CountType %>统计--%>
                                    </p>
                                </div>
                            </div>
                            <div class="table-responsive" id="div_Table">
                                <div id="div_Container1" runat="Server">
                                    <table class="table table-bordered table-grid" width="100%" id="tbOne">
                                        <thead>
                                            <tr>
                                                <th style="width: 80px;">生产部门</th>
                                                <th style="width: 70px;">合同年份</th>
                                                <th style="width: 120px;">合同目标值(万元)</th>
                                                <th style="width: 150px;">完成合同额(万元)</th>
                                                <th style="width: 100px;">完成比例(%)</th>
                                                <th style="width: 100px;">收费开始时间</th>
                                                <th style="width: 100px;">收费截止时间</th>
                                                <th style="width: 100px;">目标产值(万元)</th>
                                                <th style="width: 100px;">完成产值(万元)</th>
                                                <th style="width: 90px;">完成比例(%)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <%--<asp:GridView ID="GridView1" CssClass="table table-bordered table-grid" runat="server"
                                        AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True">
                                        <Columns>
                                            <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cpryear" HeaderText="合同年份">
                                                <ItemStyle Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cprTarget" HeaderText="合同目标值(万元)">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cprAllCount" HeaderText="完成合同额(万元)">
                                                <ItemStyle Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Cprprt" HeaderText="完成比例(%)">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Starttime" HeaderText="收费开始时间">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EndTime" HeaderText="收费截止时间">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AllotTarget" HeaderText="目标产值(万元)">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AllotCount" HeaderText="完成产值(万元)">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AllotPrt" HeaderText="完成比例(%)">
                                                <ItemStyle Width="70px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>--%>
                                </div>
                                <div id="div_Container2" runat="Server">
                                    <table class="table table-bordered table-grid" width="100%" id="tbTwo">
                                        <thead>
                                            <tr>
                                                <th style="width: 100px;">生产部门</th>
                                                <th style="width: 100px;">已签合同数量</th>
                                                <th style="width: 120px;">签订合同额(万元)</th>
                                                <th style="width: 120px;">建筑规模(㎡)</th>
                                                <th style="width: 100px;">收款金额(万元)</th>
                                                <th style="width: 100px;">审批通过合同数</th>
                                                <th style="width: 120px;">正在审批合同数</th>
                                                <th style="width: 120px;">未发起审批合同数</th>

                                            </tr>
                                        </thead>
                                    </table>
                                    <%--<asp:GridView ID="GridView2" CssClass="table table-bordered table-hover" runat="server"
                                        AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True">
                                        <Columns>
                                            <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="A" HeaderText="已签合同数量">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="B" HeaderText="签订合同额(万元)">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="C" HeaderText=" 建筑规模(㎡)">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="D" HeaderText="收款金额(万元)">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="E" HeaderText="审批通过合同数">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="F" HeaderText="正在审批合同数">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="G" HeaderText="未发起审批合同数">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>--%>
                                </div>
                                <div id="div_Container3" runat="Server">
                                    <table class="table table-bordered table-grid " width="100%" id="tbThree">
                                        <thead>
                                            <tr>
                                                <th style="width: 90px;" align="center">生产部门</th>
                                                <th style="width: 210px;" colspan="3" align="center">特级</th>
                                                <th style="width: 210px;" colspan="3" align="center">一级</th>
                                                <th style="width: 210px;" colspan="3" align="center">二级</th>
                                                <th style="width: 210px;" colspan="3" align="center">三级</th>
                                            </tr>
                                            <tr>
                                                <th style="width: 90px;">生产部门</th>
                                                <th style="width: 50px;">数量</th>
                                                <th style="width: 70px;">合同额</th>
                                                <th style="width: 90px;">收款额</th>
                                                <th style="width: 50px;">数量</th>
                                                <th style="width: 70px;">合同额</th>
                                                <th style="width: 90px;">收款额</th>
                                                <th style="width: 50px;">数量</th>
                                                <th style="width: 70px;">合同额</th>
                                                <th style="width: 90px;">收款额</th>
                                                <th style="width: 50px;">数量</th>
                                                <th style="width: 70px;">合同额</th>
                                                <th style="width: 90px;">收款额</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <%--<asp:GridView ID="GridView3" runat="server" CssClass="table table-bordered table-hover"
                                        AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True">
                                        <Columns>
                                            <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="A" HeaderText="数量(特级)" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="50px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="A1" HeaderText="合同额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="A2" HeaderText="收款额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="B" HeaderText="数量(一级)" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="50px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="B1" HeaderText="合同额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="B2" HeaderText="收款额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="C" HeaderText="数量(二级)" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="50px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="C1" HeaderText="合同额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="C2" HeaderText="收款额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="D" HeaderText="数量(三级)" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="50px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="D1" HeaderText="合同额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="D2" HeaderText="收款额" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="90px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>--%>
                                </div>
                                <div id="div_Container4" runat="Server">
                                    <div id="div_util4" runat="Server" class="cls_util" style="width: 920px;">
                                    </div>
                                    <table class="table table-bordered table-grid" width="100%" id="tbFour">
                                        <thead>
                                            <tr>
                                                <th style="width: 120px;">生产部门</th>
                                                <th style="width: 100px;">民用建筑合同</th>
                                                <th style="width: 120px;">工业建筑合同</th>
                                                <th style="width: 120px;">工程勘察合同</th>
                                                <th style="width: 100px;">工程监理合同</th>
                                                <th style="width: 100px;">工程施工合同</th>
                                                <th style="width: 100px;">工程咨询合同</th>
                                                <th style="width: 120px;">工程总承包合同</th>
                                                <th style="width: 100px;">工程代建合同</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <%--<asp:GridView ID="GridView4" runat="server" CssClass="table table-bordered table-hover"
                                        AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True">
                                        <Columns>
                                            <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="A" HeaderText="民用建筑合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="B" HeaderText="工业建筑合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="C" HeaderText="工程勘察合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="D" HeaderText="工程监理合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="E" HeaderText="工程施工合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="F" HeaderText="工程咨询合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="G" HeaderText="工程总承包合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="H" HeaderText="工程代建合同" ItemStyle-CssClass="cls_HoverTd">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>--%>
                                </div>
                                <div id="div_Container5" runat="Server">

                                    <div id="div_util5" runat="Server" class="cls_util" style="width: 865px;">
                                    </div>
                                    <table class="table table-bordered table-grid" width="100%" id="tbFive">
                                        <thead>
                                            <tr>
                                                <th style="width: 120px;">生产部门</th>
                                                <th style="width: 80px;">公建</th>
                                                <th style="width: 80px;">房地产</th>
                                                <th style="width: 80px;">市政</th>
                                                <th style="width: 80px;">医院</th>
                                                <th style="width: 80px;">电力</th>
                                                <th style="width: 80px;">通信</th>
                                                <th style="width: 80px;">银行</th>
                                                <th style="width: 80px;">学校</th>
                                                <th style="width: 80px;">涉外</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <%--<asp:GridView ID="GridView5" runat="server" CssClass="table table-bordered table-hover"
                                        AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="True">
                                        <Columns>
                                            <asp:BoundField DataField="Name" HeaderText="生产部门" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="A" HeaderText="公建">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="B" HeaderText="房地产">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="C" HeaderText="市政">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="D" HeaderText="医院">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="E" HeaderText="电力">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="F" HeaderText="通信">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="G" HeaderText="银行">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="H" HeaderText="学校">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="I" HeaderText="涉外">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="hiddenischeckallpower" name="name" value="<%= IsCheckAllPower %>" />

            <input type="hidden" id="hiddenUserUnit" name="name" value="<%= UserUnitNo %>" />
            <input type="hidden" id="hidUnitName" value="<%=UnitName %>" />
            <input type="hidden" id="hiddenUnit" name="name" runat="Server" />
            <input type="hidden" id="hiddenTitle" name="name" runat="Server" />
        </div>
    </div>
    <!--隐藏部门-->
    <input type="hidden" value='' id="hidUnitListVal" />
    <asp:HiddenField ID="SelectType" runat="server" Value="0" />
</asp:Content>
