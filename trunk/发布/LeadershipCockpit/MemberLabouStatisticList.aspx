﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="MemberLabouStatisticList.aspx.cs" Inherits="TG.Web.LeadershipCockpit.MemberLabouStatisticList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/LeadershipCockpit/MemberLabouStatistic_jq.js"></script>
    <style type="text/css">
        .norecords {
            border-width: 2px !important;
            display: none;
            font-weight: bold;
            left: 45%;
            margin: 5px;
            padding: 6px;
            position: absolute;
            text-align: center;
            top: 45%;
            width: auto;
            z-index: 102;
            color: red;
        }

        #jqgh_jqGrid_cb {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>劳动负荷统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目信息管理</a><i class="fa fa-angle-right">
    </i><a>劳动负荷统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>请选择查询项
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tb_check">
                                <tr>
                                    <td align="left">
                                        <input id="txt_Unit" name="bb" class="cls_audit" type="checkbox" clo="0" /><span for="Unit">生产部门</span>
                                    </td>
                                    <td align="left">
                                        <input id="chk_UserName" name="bb" class="cls_audit" type="checkbox" clo="1" /><span for="UserName">姓名</span>
                                    </td>
                                    <td align="left">
                                        <input id="chk_SpeName" name="bb" class="cls_audit" type="checkbox" clo="2" /><span for="SpeName">专业</span>
                                    </td>
                                    <td align="left">
                                        <input id="chk_ProjectStartDate" name="bb" class="cls_audit" type="checkbox" clo="3" /><span for="ProjectStartDate">项目时间</span>
                                    </td>

                                    <td align="left">
                                        <input id="chk_ProjectCount" name="bb" class="cls_audit" type="checkbox" clo="4" /><span for="ProjectCount">项目总数</span>
                                    </td>
                                    <td align="left">
                                        <input id="chk_ProjectJinXingCount" name="bb" class="cls_audit" type="checkbox" clo="5" /><span for="ProjectJinXingCount">进行中项目</span>
                                    </td>
                                    <td align="left">
                                        <input id="chk_ProjectZanTingCount" name="bb" class="cls_audit" type="checkbox" clo="6" /><span for="ProjectZanTingCount">暂停项目</span>
                                    </td>
                                    <td align="left">
                                        <input id="chk_ProjectWanGongCount" name="bb" class="cls_audit" type="checkbox" clo="7" /><span for="_ProjectWanGongCount">完工项目</span>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>劳动负荷统计
                    </div>

                    <div class="tools">
                    </div>

                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-hover" id="tb_select">
                        <tr clo="0" style="display: none;">
                            <td style="width: 20%;">生产部门:
                               
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="180">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr clo="1" style="display: none;">
                            <td>姓名:
                              
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="Server" style="width: 180px;" />
                            </td>
                        </tr>
                        <tr clo="2" style="display: none;">
                            <td>专业:
                               
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_SpeName" runat="Server" style="width: 180px;" />
                            </td>
                        </tr>

                        <tr clo="3" style="display: none;">
                            <td>项目开始日期:
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="txt_signdate" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 180px; height: 22px; border: 1px solid #e5e5e5" />
                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 22px; border: 1px solid  #e5e5e5" />
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr clo="4" style="display: none;">
                            <td>参与项目总数:
                               
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="txt_ProjectCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_ProjectCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                            &nbsp;个 
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr clo="5" style="display: none;">
                            <td>进行中项目总数:
                             
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="txt_ProjectJinXingCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_ProjectJinXingCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                            &nbsp;个 
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr clo="6" style="display: none;">
                            <td>暂停项目总数:
                             
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="txt_ProjectZanTingCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_ProjectZanTingCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                            &nbsp;个 
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr clo="7" style="display: none;">
                            <td>完工项目总数:
                           
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="txt_ProjectWanGongCount1" class="form-control input-sm " style="width: 180px;" runat="Server" />
                                            &nbsp; 至&nbsp;
                                                        <input type="text" id="txt_ProjectWanGongCount2" class="form-control input-sm" style="width: 180px;" runat="Server" />
                                            &nbsp;个 
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td colspan="2" align="center">
                                <input type="button" class="btn blue"
                                    id="btn_search" value="查询" />
                            </td>
                        </tr>

                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询劳动负荷统计明细列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <label>
                                    <input type="checkbox" value="all" checked />全选
                                </label>
                                <%=ColumnsContent %>
                            </div>
                        </div>
                        <asp:Button ID="btn_Output" runat="server" Text="导出Excel" CssClass="btn red btn-sm" OnClick="btn_Output_Click" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <input type="hidden" id="hid_checkbox" name="name" />
    <asp:HiddenField ID="hid_cols" runat="server"  Value=""/>
</asp:Content>
