﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AstvStruct.ascx.cs" Inherits="TG.Web.ProjectValueandAllot.AstvStruct" %>
<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>

  <cc1:ASDropDownTreeView ID="asTreeviewStruct"
         runat="server" 
        BasePath="~/js/astreeview/astreeview/"
						DataTableRootNodeValue="0"
						EnableRoot="false" 
						EnableNodeSelection="false" 
						EnableCheckbox="true" 
						EnableDragDrop="true" 
						EnableTreeLines="true"
						EnableNodeIcon="false"
						EnableCustomizedNodeIcon="false"
						EnableDebugMode="false"
						EnableRequiredValidator="true"
						InitialDropdownText="请选择" 
						Width="300"
						EnableCloseOnOutsideClick="true" 
						EnableHalfCheckedAsChecked="true" 
						RequiredValidatorValidationGroup="vgCheck"
						EnableContextMenuAdd="false" 
            DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" 
            DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" 
            DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" 
            
            DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" 
            Font-Size="12px" Font-Strikeout="False"  />
            
              <style type ="text/css" >
    #asTreeviewStruct a
    {
    	 font-size :12px;
    	}
    	*{ font-size :12px;}
    </style>
    
        
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />

    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
