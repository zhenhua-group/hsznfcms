﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="AuditDesignValueList.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.AuditDesignValueList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>个人审核、设计金额系数表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">产值分配</a><i
        class="fa fa-angle-right"> </i><a href="#">个人审核、设计金额系数表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询分配项目
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>分配年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="查询" CssClass="btn blue btn-xs" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>建筑(<%=AllotTypeString %>)费用
                    </div>
                    <div class="actions">
                        <asp:Button ID="btnArchExport" runat="server" Text="导出" OnClick="btnArchExport_Click" CssClass="btn default btn-sm"/>
                        <a href="AuditDesignValueCountListBymaster.aspx" class="btn default btn-sm">返回</a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" style="width: 380px;">
                            <thead>
                                <tr>
                                    <th style="width: 120px;">人员</th>
                                    <th style="width: 100px;">分配总额(元)</th>
                                    <th style="width: 80px;">分配年份</th>
                                    <th style="width: 80px;">类型</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%=ArchUserHtml %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>结构(<%=AllotTypeString %>)费用
                    </div>
                    <div class="actions">
                        <asp:Button ID="btnStructExport" runat="server" Text="导出" OnClick="btnStructExport_Click" CssClass="btn default btn-sm" />
                        <a href="AuditDesignValueCountListBymaster.aspx" class="btn default btn-sm">返回</a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" style="width: 380px;">
                            <thead>
                                <tr>
                                    <th style="width: 120px;">人员</th>
                                    <th style="width: 100px;">分配总额(元)</th>
                                    <th style="width: 80px;">分配年份</th>
                                    <th style="width: 80px;">类型</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%=StructUserHtml %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
