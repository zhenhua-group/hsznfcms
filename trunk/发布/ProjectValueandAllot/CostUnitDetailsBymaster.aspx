﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="CostUnitDetailsBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.CostUnitDetailsBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/CostUnitDetailsBymaster.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>所成本明细统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>财务管理 </a><i class="fa fa-angle-right"></i><a>所成本明细统计</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询所成本明细统计
                    </div>
                    
                </div>
                <div class="portlet-body" style="display: block;">

                    <table id="tbl_id" class="table-responsive">
                        <tr>
                            <td>部门:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control " runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>成本类型:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_costTypeAll" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----选择类型-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>日期:
                            </td>
                            <td>
                                <input id="txt_costdate1" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" runat="server" />至
                                         <input id="txt_costdate2" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" style="width: 90px; height: 22px;border: 1px solid #e5e5e5;" runat="server" />
                            </td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_search" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>所成本明细统计
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button Text="导出Excel" runat="server" CssClass="btn red btn-sm" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <label id="costTitle"></label>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <table border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered table-hover dataTable" width="100%" id="costTable">
                                    <tr>
                                        <th>成本类型</th>
                                        <th>合计(元)</th>
                                        <th>详情</th>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <%-- 隐藏域-默认单位。 --%>
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="hidUserId" Value="" />
    <asp:HiddenField runat="server" ID="HiddenTypeID" Value="-1" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
