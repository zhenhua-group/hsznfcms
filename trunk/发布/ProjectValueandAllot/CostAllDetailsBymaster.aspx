﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="CostAllDetailsBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.CostAllDetailsBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/CostAllDetailsBymaster.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>部门成本统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>财务管理 </a><i class="fa fa-angle-right"></i><a>部门成本统计</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询成本统计
                    </div>
                    
                </div>
                <div class="portlet-body" style="display: block;">
                    <table id="tbl_id" class="table-responsive">
                        <tr>
                            <td>部门类别:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unitType" CssClass="form-control " Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="4">生产部门</asp:ListItem>
                                    <asp:ListItem Value="3">承包公司</asp:ListItem>
                                    <asp:ListItem Value="2">施工图审查室</asp:ListItem>
                                    <asp:ListItem Value="1">勘察分院</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>部门:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----选择部门-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>成本类型:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_costTypeAll" CssClass="form-control " Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----选择类型-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>日期:
                            </td>
                            <td>
                                <input id="txt_costdate1" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" runat="server" />至
                                         <input id="txt_costdate2" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" runat="server" />
                            </td>
                            <td>
                                <asp:Button Text="查询" runat="server" CssClass="btn blue btn-sm" ID="btn_search" OnClick="btn_search_Click" />
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>部门成本统计
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <%--OnClick="btn_export_Click"--%>
                        <asp:Button Text="导出Excel" runat="server" CssClass="btn red btn-sm" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <center> 
                                    <asp:Label id="costTitle" runat="server">部门成本统计</asp:Label>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                            </div>
                            <div class="col-md-1">
                                单位(元)
                            </div>
                        </div>
                        <div class="row">
                            <div style="width: 98%; margin: 0 auto;">
                                <asp:GridView ID="grid_Financial" runat="server" ShowHeader="true"
                                    Width="100%" EnableModelValidation="True" CssClass="table table-bordered table-grid" AutoGenerateColumns="true">
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 隐藏域--控制加载的时候drowdown的显示 -->
    <asp:HiddenField ID="hiddenUnit" runat="server" Value="-1" />
    <asp:HiddenField ID="hiddenType" runat="server" Value="-1" />
</asp:Content>
