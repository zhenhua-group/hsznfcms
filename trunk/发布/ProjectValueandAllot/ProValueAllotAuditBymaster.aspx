﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProValueAllotAuditBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster" %>

<%@ Register Src="../UserControl/ChooseProjectValueUser.ascx" TagName="ChooseProjectValueUser"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/AddExternalMember.ascx" TagName="AddExternalMember"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.cookie.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseProjectValueUser.js" type="text/javascript"></script>
    <script src="../js/UserControl/AddExternalMember.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/ProjectValueStatusDetail.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/CommonAddProjectMember.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/ProValueAllotAudit.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/ProValueAllotAuditByMember.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/ProValueAllotAuditDetail.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/ProjectValueMeaaageSend.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        $(function () {

            $("#btnPrintValue").click(function () {
                var openUrl = "PrintProjectValue.aspx?ProjectSysNo=" + $("#HiddenCoperationSysNo").val() + "&AllotId=" + $("#HiddenAllotID").val() + "";

                var feature = "dialogWidth:960px;dialogHeight:550px;center:yes";

                window.open(openUrl, '打印', feature);
            });

        });
    </script>
    <style type="text/css">
        .display {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>产值分配审核</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>产值分配审核</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>项目信息
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center" id="tb_project">
                                            <tr>
                                                <td style="width: 15%;">项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">合同关联:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">审核级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">项目总负责:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">合同额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">其他参与部门:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="scjy">
                                                <td style="width: 15%;">实收金额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblPayShiCount" runat="server" Width="100px"></asp:Label>万元
                                                </td>
                                                <td style="width: 15%;">已分配金额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblAllotAccount" runat="server" Text=""></asp:Label>万元
                                                </td>
                                            </tr>
                                            <tr class="scjy">
                                                <td style="width: 15%;">未分配金额:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lblNotAllotAccount" runat="server"></asp:Label>万元
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>审批信息
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--评审内容-->
                                    <asp:Literal ID="lblNotData" runat="server"></asp:Literal>
                                    <div id="MainDiv">
                                        <table style="width: 100%; height: auto;" class="cls_content_head">
                                            <tr>
                                                <td style="width: 10%; text-align: center;">评审部门
                                                </td>
                                                <td style="width: 80%; text-align: center;">评审内容
                                                </td>
                                                <td style="width: 10%; text-align: center;">评审人/日期
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="TableContainer">
                                            <!--生产经营部Table-->
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="OneTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">生产经营部
                                                    </td>
                                                    <td style="width: 80%;">
                                                        <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                            <tr>
                                                                <td style="width: 20%;">项目分配年份：
                                                                </td>
                                                                <td style="width: 30%;" class="cls_ProjAllot_label">

                                                                    <table width="100%" style="border: none;" id="tb_ysfc">
                                                                        <tr>
                                                                            <td style="border: none; width: 35%;">
                                                                                <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>(年)</td>
                                                                            <td style="border: none; width: 30%;">院所分成比例</td>
                                                                            <td style="border: none; width: 30%;">
                                                                                <asp:Label ID="lblDividedPercent" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="width: 20%;">分配金额：
                                                                </td>
                                                                <td style="width: 30%;" class="cls_ProjAllot_label">
                                                                    <asp:Label ID="txt_AllotAccount" runat="server"></asp:Label>(万元)
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="cls_ProjAllotHead" id="alloutCount" style="width: 98%;">
                                                            <tr>
                                                                <td align="center" style="width: 20%;">序号
                                                                </td>
                                                                <td align="center" style="width: 30%;">内容
                                                                </td>
                                                                <td align="center" style="width: 20%;">比例
                                                                </td>
                                                                <td align="center" style="width: 30%;">产值
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="98%"
                                                            id="tbAllotDeatil">
                                                            <tr>
                                                                <td style="width: 20%;" class="cls_Column">1
                                                                </td>
                                                                <td style="width: 30%;" align="left">实&nbsp;收&nbsp;产&nbsp;值
                                                                </td>
                                                                <td style="width: 20%;">
                                                                    <asp:Label ID="txt_PaidValuePercent" runat="server" Text="100"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td style="width: 30%;">
                                                                    <asp:Label ID="txt_PaidValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">2
                                                                </td>
                                                                <td>设&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_DesignManagerPercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_DesignManagerCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">3
                                                                </td>
                                                                <td>分&nbsp;配&nbsp;产&nbsp;值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtAllotValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtAllotValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">4
                                                                </td>
                                                                <td>转经济所产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_EconomyValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_EconomyValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">5
                                                                </td>
                                                                <td>转暖通部门产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_HavcValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_HavcValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">6
                                                                </td>
                                                                <td>转出其他部门产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtOtherDeptValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtOtherDeptValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">7
                                                                </td>
                                                                <td>本部门自留产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtTheDeptValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtTheDeptValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">8
                                                                </td>
                                                                <td>扣&nbsp;发&nbsp;产&nbsp;值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_ProgramPercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_ProgramCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">9
                                                                </td>
                                                                <td>本部门产值合计
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_UnitValueCountPercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtUnitValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">10
                                                                </td>
                                                                <td>应&nbsp;分&nbsp;产&nbsp;值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_ShouldBeValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">11
                                                                </td>
                                                                <td>财务统计产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFinanceValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFinanceValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">12
                                                                </td>
                                                                <td>部门应留产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblTheDeptShouldValuePercent" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblTheDeptShouldValueCount" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <!--所长-->
                                            <asp:Literal ID="lblNotPass" runat="server"></asp:Literal>
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="TwoTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">所长
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <fieldset id="saveProcessInfo" style="text-align: left; display: none;">
                                                            <legend style="font-size: 12px;">保存项目阶段信息</legend>
                                                            <table style="width: 98%; display: none;" class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label"
                                                                id="tbSaveProcessInfo">
                                                                <tr>
                                                                    <td style="width: 20%;">分配金额是否全部借出:
                                                                    </td>
                                                                    <td colspan="3" style="text-align: left;">
                                                                        <div class="col-md-12">
                                                                            <div class="radio-list">
                                                                                <label class="radio-inline">
                                                                                    <span class="">
                                                                                        <input type="radio" name="aa" id="radio_yes" runat="server" value="1" />
                                                                                    </span>是
                                                                                </label>
                                                                                <label class="radio-inline">
                                                                                    <span class="checked">
                                                                                        <input type="radio" name="aa" id="radio_no" runat="server" value="0" checked />
                                                                                    </span>否
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%;">项目等级：
                                                                    </td>
                                                                    <td style="width: 30%;">
                                                                        <asp:DropDownList ID="drp_buildtype" runat="server" Width="100%" AppendDataBoundItems="true"
                                                                            CssClass="form-control">
                                                                            <asp:ListItem Value="-1">------选择类别------</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 20%;">项目阶段:
                                                                    </td>
                                                                    <td style="width: 30%;">
                                                                        <select id="stage" class="form-control" style="width: 100%;" name="D1" runat="server">
                                                                            <option value="-1">----------请选择分配阶段----------</option>
                                                                            <option value="0">方案+初设+施工图+后期</option>
                                                                            <option value="1">方案+初设</option>
                                                                            <option value="10">方案+施工图+后期</option>
                                                                            <option value="2">施工图+后期</option>
                                                                            <option value="3">初设+施工图+后期</option>
                                                                            <option value="4">室外工程</option>
                                                                            <option value="5">锅炉房(4吨以下)</option>
                                                                            <option value="5">锅炉房(6吨以上)</option>
                                                                            <option value="6">地上单建水泵房</option>
                                                                            <option value="7">地上单建变配电所(室)</option>
                                                                            <option value="8">单建地下室（车库）</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>项目借入金额：</td>
                                                                    <td>
                                                                        <input id="txt_BorrowCount" maxlength="15" type="text" runat="server" style="width: 100px;" />(元)</td>

                                                                    <td>可用借入金额：
                                                                    </td>
                                                                    <td>

                                                                        <asp:Label ID="txt_ActulloanCount" runat="server" Text="0.00"></asp:Label>
                                                                        (元)
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>项目借出金额：
                                                                    </td>
                                                                    <td>
                                                                        <input id="txt_loanCount" maxlength="15" type="text" runat="server" style="width: 100px;" />(元)
                                                                    </td>
                                                                    <td>实际分配金额：
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="txt_ActulCount" runat="server" Text="0.00"></asp:Label>
                                                                        (元)
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 98%; display: none;" class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label"
                                                                id="tbSaveed">
                                                                <tr>
                                                                    <td>分配金额是否全部借出:
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:Label ID="lbl_IsTAllPass" runat="server" Text="0.00"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td style="width: 20%;">项目等级：
                                                                    </td>
                                                                    <td style="width: 30%;">
                                                                        <asp:Label ID="lblType" runat="server" ></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%;">项目阶段:
                                                                    </td>
                                                                    <td style="width: 30%;">
                                                                        <asp:Label ID="lblStage" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>项目借入金额</td>
                                                                    <td>
                                                                        <asp:Label ID="lblBorrowAccount" runat="server"></asp:Label>(元)</td>

                                                                    <td>可用借入金额：
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblActalLoanAcount" runat="server" Text="0.00"></asp:Label>
                                                                        (元)
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>项目借出金额：
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblLoanAcount" runat="server"></asp:Label>(元)
                                                                    </td>
                                                                    <td>实际分配金额：
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblActalAcount" runat="server" Text="0.00"></asp:Label>
                                                                        (元)
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 98%; margin: auto;" class="cls_show_cst_jiben" id="tbSave">
                                                                <tr>
                                                                    <td align="center">
                                                                        <input type="button" id="btnSaveProcess" name="controlBtn" class="btn green btn-default"
                                                                            value="保存" style="display: none;" />
                                                                        <input type="button" id="btnCancelProcess" name="controlBtn" class="btn btn-default"
                                                                            value="取消" style="display: none;" />
                                                                        <span id="spanSuccessInfo" style="display: none; color: Red;">保存成功，请填写审批意见！</span><span
                                                                            id="addExternal" style="color: Blue; cursor: pointer; display: none;">修改项目阶段信息</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                        <fieldset id="auditSuggstion" style="text-align: left; margin: 0 auto; display: none;">
                                                            <legend style="font-size: 12px;">审批意见</legend>
                                                            <textarea style="width: 98%; height: 60px; margin: 0 auto;" id="TwoSuggstion" class=" form-control"></textarea>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <!--经济所-->
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="ThreeTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">经济所
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="98%"
                                                            id="tb_jjs" style="display: none;">
                                                            <tr>
                                                                <td style="width: 20%;" class="cls_Column">1
                                                                </td>
                                                                <td style="width: 30%;" align="left">实&nbsp;收&nbsp;产&nbsp;值
                                                                </td>
                                                                <td style="width: 20%;">
                                                                    <asp:Label ID="txt_PaidValuePercent1" runat="server" Text="100"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td style="width: 30%;">
                                                                    <asp:Label ID="txt_PaidValueCount1" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">2
                                                                </td>
                                                                <td>转经济所产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_EconomyValuePercent1" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_EconomyValueCount1" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>

                                                        </table>
                                                        <fieldset id="Fieldset1" style="text-align: left; margin: 0 auto;">
                                                            <legend style="font-size: 12px;">审批意见</legend>
                                                            <textarea style="width: 98%; height: 60px; margin: 0 auto;" id="ThreeSuggstion" class=" form-control"
                                                                cols="20" rows="1"></textarea>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <!--暖通所-->
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="FourTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">暖通所
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="98%"
                                                            id="tb_nts" style="display: none;">
                                                            <tr>
                                                                <td style="width: 20%;" class="cls_Column">1
                                                                </td>
                                                                <td style="width: 30%;" align="left">实&nbsp;收&nbsp;产&nbsp;值
                                                                </td>
                                                                <td style="width: 20%;">
                                                                    <asp:Label ID="Label1" runat="server" Text="100"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td style="width: 30%;">
                                                                    <asp:Label ID="txt_PaidValueCount2" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="cls_Column">2
                                                                </td>
                                                                <td>转暖通部门产值
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_HavcValuePercent1" runat="server"></asp:Label>
                                                                    %
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txt_HavcValueCount1" runat="server"></asp:Label>
                                                                    (元)
                                                                </td>
                                                            </tr>

                                                        </table>
                                                        <fieldset id="Fieldset2" style="text-align: left; margin: 0 auto;">
                                                            <legend style="font-size: 12px;">审批意见</legend>
                                                            <textarea style="width: 98%; height: 60px; margin: 0 auto;" id="FourSuggstion" class=" form-control"
                                                                cols="20" rows="1"></textarea>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="SCYJTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">生产经营部
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <fieldset id="Fieldset3" style="text-align: left; margin: 0 auto;">
                                                            <legend style="font-size: 12px;">审批意见</legend>
                                                            <textarea style="width: 98%; height: 60px; margin: 0 auto;" id="SCJYSuggstion" class=" form-control"></textarea>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <!--设总Table-->

                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="FiveTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">设总
                                                    </td>
                                                    <td style="width: 80%;">

                                                        <asp:Literal ID="lbl_NoData" runat="server"></asp:Literal>
                                                        <fieldset id="file_szxs" style="text-align: left; display: none;">
                                                            <legend style="font-size: 12px;">设总系数分配</legend>
                                                            <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="98%" id="tb_szxs">
                                                                <tr>
                                                                    <td style="width: 20%; font-weight: bold;">序号
                                                                    </td>
                                                                    <td style="width: 30%; font-weight: bold;">姓名
                                                                    </td>
                                                                    <td style="width: 20%; font-weight: bold;">设总分配比例
                                                            <asp:Label ID="lbl_DesignManagerPercent" runat="server"></asp:Label>
                                                                        %
                                                                    </td>
                                                                    <td style="width: 30%; font-weight: bold;">设总金额: 
                                                            <asp:Label ID="lbl_DesignManagerCount" runat="server"></asp:Label>
                                                                        (元)
                                                                    </td>
                                                                </tr>
                                                                <asp:Literal ID="lbl_DesignManagerValue" runat="server"></asp:Literal>

                                                            </table>
                                                        </fieldset>
                                                        <div id="stagetablezero" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目各设计阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" style="width: 98%;">
                                                                <tr>
                                                                    <td width="10%" style="text-align: center;">项目类别
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">方案设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">初设设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("ProgramPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("preliminaryPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stagetableone" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案+初步设计)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; height: 30px; text-align: center; background-color: #B8CCE4;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; height: 30px; text-align: center; background-color: #B8CCE4;">方案
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; height: 30px; text-align: center; background-color: #B8CCE4;">初设
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center; height: 30px; text-align: center; background-color: #B8CCE4;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("ProgramPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("preliminaryPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stagetabletwo" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; text-align: center;">施工图
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center; text-align: center;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stagetablethree" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(初步设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">初设设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("preliminaryPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stagetableten" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">方案设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvTen" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("ProgramPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <asp:Literal ID="lbl_StageFive" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_stagespetable" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_Tab" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_out" runat="server"></asp:Literal>
                                                        <div id="stageamountOne" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目各设计阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td width="10%" style="text-align: center;">项目类别
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">方案设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">初设设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("ProgramPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("preliminaryPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent") %>
                                                                            </span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountTwo" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案+初步设计)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">方案比例
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">初设比例
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("ProgramPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("preliminaryPercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountThree" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">施工图
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">后期服务
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent") %>
                                                                            </span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountFour" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(初步设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">初设设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("preliminaryPercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountTen" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="Table2" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">方案设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageTen" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("ProgramPercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <asp:Literal ID="lbl_SpeAmount" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_outAmount" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_ProcessAmount" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_processAmountFiveTable" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="SixTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">专业负责人
                                                    </td>
                                                    <td style="width: 80%;">
                                                        <asp:Literal ID="lbl_Member" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_MemberCount" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="SevenTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">所长
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <fieldset id="Fieldset4" style="text-align: left; margin: 0 auto;">
                                                            <legend style="font-size: 12px;">审批意见</legend>
                                                            <textarea style="width: 98%; height: 60px; margin: 0 auto;" id="SevenSuggstion" class=" form-control"></textarea>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <div class="NoPrint">
                                                <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                    id="EightTable" audittable="auditTable">
                                                    <tr>
                                                        <td style="width: 10%; text-align: center;">生产经营部
                                                        </td>
                                                        <td style="width: 80%; text-align: center;">
                                                            <fieldset id="Fieldset5" style="text-align: left; margin: 0 auto;">
                                                                <legend style="font-size: 12px;">审批意见</legend>
                                                                <textarea style="width: 98%; height: 60px; margin: 0 auto;" id="EightSuggstion" class=" form-control"></textarea>
                                                            </fieldset>
                                                        </td>
                                                        <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!--没有权限审核-->
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="NoPowerTable">
                                                <tr>
                                                    <td style="width: 100%; text-align: center; height: 100px; line-height: 100px;">您没有权限审核该项
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--保存按钮-->
                                            <%-- </fieldset>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="NoPrint">
                <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table1">
                    <tr>
                        <td style="width: 100%; text-align: center;">
                            <input type="button" id="btnApproval" name="controlBtn" href="#AuditUserDiv" class="btn green btn-default"
                                value="通过" />
                            <input type="button" id="btn_AllApproval" name="controlBtn" class="btn  yellow btn-default" style="display: none;"
                                value="全部通过" />
                            <input type="button" id="btnRefuse" name="controlBtn" class="btn red btn-default"
                                value="不通过" />
                            <input type="button" id="Button1" name="controlBtn" class="btn  btn-default" value="返回"
                                onclick="javascript: history.back();" />&nbsp;<input type="button" id="btnEditALL"
                                    name="controlBtn" class="btn green btn-default" value="特殊申请" style="width: 85px;" />
                            <input type="button" value="返回产值分配页面，重新添加分配" class="btn green btn-default" id="FallBackCoperaion"
                                style="display: none;" />
                            <input type="button" id="print" name="controlBtn" onclick="openWindow();" style="display: none;" />
                            <input id="btnPrintValue" type="button" value="打印产值"
                                class="btn purple btn-default" style="display: none;" />
                            <input id="btnExportValue" type="button" onclick="" value="导出产值" runat="server" class="btn green btn-default"
                                onserverclick="btnExport_click" style="display: none;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenSysMsgSysNo" value="0" />
    <input type="hidden" id="AuditRecordStatus" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=projectAuditRecordEntity ==null?0:projectAuditRecordEntity.SysNo %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenOneProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.OneSuggestion %>" />
    <input type="hidden" id="HiddenTwoProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.TwoSuggstion %>" />
    <input type="hidden" id="HiddenTwoAuditUser" value="<%=projectAuditRecordEntity ==null?0:projectAuditRecordEntity.TwoAuditUser %>" />
    <input type="hidden" id="HiddenTwoAuditDate" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.TwoAuditDate.ToString() %>" />
    <input type="hidden" id="HiddenTwoIsPass" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.TwoIsPass %>" />
    <input type="hidden" id="HiddenThreeProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.ThreeSuggsion %>" />
    <input type="hidden" id="HiddenThreeAuditUser" value="<%=projectAuditRecordEntity ==null?0:projectAuditRecordEntity.ThreeAuditUser %>" />
    <input type="hidden" id="HiddenThreeAuditDate" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.ThreeAuditDate.ToString() %>" />
    <input type="hidden" id="HiddenThreeIsPass" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.ThreeIsPass %>" />
    <input type="hidden" id="HiddenFourProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.FourSuggion %>" />
    <input type="hidden" id="HiddenFourAuditUser" value="<%=projectAuditRecordEntity ==null?0:projectAuditRecordEntity.FourAuditUser %>" />
    <input type="hidden" id="HiddenFourAuditDate" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.FourAuditDate.ToString() %>" />
    <input type="hidden" id="HiddenFourIsPass" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.FourIsPass %>" />
    <input type="hidden" id="HiddenFiveProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.FiveSuggion  %>" />
    <input type="hidden" id="HiddenSevenSuggsion" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.SevenSuggsion  %>" />
    <input type="hidden" id="HiddenEightSuggstion" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.EightSuggstion  %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.AuditUserString %>" />
    <input type="hidden" id="HiddenAuditDate" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.AuditDate %>" />
    <input type="hidden" id="HiddenCoperationSysNo" value="<%=ProSysNo %>" />
    <input type="hidden" id="HiddenCoperationProcess" value="<%=CoperationProcess %>" />
    <input type="hidden" id="HiddenIsTrunEconomy" value="<%=IsTrunEconomy %>" />
    <input type="hidden" id="HiddenIsTrunHavc" value="<%=IsTrunHavc %>" />
    <input type="hidden" id="HiddenIsEdit" value="<%=IsParamterEdit %>" />
    <input type="hidden" id="HiddenAllotID" value="<%=projectAuditRecordEntity ==null?0:projectAuditRecordEntity.AllotID %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <input type="hidden" id="hiddenSpetityHead" value="<%=SpetailtyHeadUser %>" />
    <input type="hidden" id="hiddenSpeName" value="<%=SpeName %>" />
    <input type="hidden" id="hiddenProUrl" value="<%=ProUrl%>" />
    <!--项目转过来连接-->
    <input type="hidden" id="hiddenProlink" value="<%=Prolink%>" />
    <!--分配中的连接-->
    <input type="hidden" id="hidLikeType" value="<%=LikeType%>" />
    <input type="hidden" id="hiddenEdit" value="0" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <!--是否全部转出-->
    <input type="hidden" id="hidIsTAllPass" value="<%= IsTAllPass %>" />
    <!--是否四舍五入-->
    <input type="hidden" id="HiddenIsRounding" value="<%=IsRounding %>" />
    <!--专业审核人-->
    <input type="hidden" id="HiddenSpecialityAuditUser" value="<%=SpecialityAuditUser %>" />
    <!--二次分配-->
    <input type="hidden" id="HiddenSecondValue" value="<%=SecondValue %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--PopArea-->
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <!--专业负责人选择消息接收着-->
    <div id="msgValueReceiverContainer" style="width: 450px; height: 200px; display: none;">
    </div>
    <!--专业负责人选择消息接收着-->
    <div id="showEdit" style="width: 100%; display: none;">
        <fieldset style="font-size: 12px;">
            <legend>申请修改原因</legend>
            <table class="show_project" style="font-size: 12px;">
                <tr>
                    <td style="width: 10%">修改原因:
                    </td>
                    <td align="left" style="width: 96%">
                        <textarea id="opinion" option="a" style="height: 100px; color: Gray; width: 99%; border: solid 1px #CCC; resize: none;">请填写修改原因....</textarea><br />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <input type="button" name="name" id="btnsub" value="提交" href="#AuditUserDiv" data-toggle="modal"
                            class="btn green btn-default" />
                        <input type="button" name="name" id="btnCancl" value="取消" class="btn btn-default" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <!--选择用户-->
    <div id="chooseUserMainDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择用户</h4>
        </div>
        <div class="modal-body" id="chooseUserDiv">
            <div id="chooseUserMain">
                <uc1:ChooseProjectValueUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <%-- <div class="modal-footer">
            <button type="button" id="btn_UserMain" data-dismiss="modal" datamodal="user" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>--%>
    </div>
    <div id="chooseExtUserDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加外聘人员</h4>
        </div>
        <div class="modal-body" id="chooseExtDiv">
            <div id="chooseExternalUserDiv">
                <uc2:AddExternalMember ID="chooseExternalUser1" runat="server" />
            </div>
        </div>
        <%-- <div class="modal-footer">
            <button type="button" id="btnChooseExt" datamodalext="user"  data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>--%>
    </div>
    <div id="AuditUserDiv" class="modal fade yellow" tabindex="-1" data-width="490" aria-hidden="true"
        style="display: none; width: 490px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_close">
                关闭</button>
        </div>
    </div>
</asp:Content>
