﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjAllotDetailsTable.aspx.cs"
    Inherits="TG.Web.ProjectValueandAllot.ProjAllotDetailsTable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjAllotDetailsTable.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript" src="../js/ProjectValueandAllot/ProjAllotDetailsTable.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[项目立项]-[设计项目费用表]-[项目人员分配]
            </td>
        </tr>
    </table>
    <div class="cls_data">
         <asp:Literal ID="Literal1" runat="server" Text=""></asp:Literal>
    </div>
    <asp:HiddenField ID="hid_costid" runat="server" Value="0" />
    <asp:HiddenField ID="hid_cprid" runat="server" Value="0" />
    <asp:HiddenField ID="hid_proid" runat="server" Value="0" />
    <asp:HiddenField ID="hid_allcount" runat="server" Value="0" />
    <asp:HiddenField ID="hid_lowerallot" runat="server" Value="0" />
    <asp:HiddenField ID="hid_collegemng" runat="server" Value="0" />
    <asp:HiddenField ID="hid_projallot" runat="server" Value="0" />
    <asp:HiddenField ID="hid_projchrg" runat="server" Value="0" />
    <asp:HiddenField ID="hid_dusercount" runat="server" Value="0" />
    <asp:HiddenField ID="hid_projallcount" runat="server" Value="0" />
    <asp:HiddenField ID="hid_proname" runat="server" Value="0" />
     <asp:HiddenField ID="HiddenPartOneRecordInUserSysNo" runat="server" Value="0" />
    
   
    <asp:HiddenField ID="hid_structype" runat="server" />
    
   
    </form>
    <!--Sciprt Area-->

    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>

    <script src="/js/Global.js" type="text/javascript"></script>

    <script src="/js/ProjectValueandAllot/ProjectAllot.js" type="text/javascript"></script>

    <script src="/js/ProjectValueandAllot/PartThreeSaveData.js" type="text/javascript"></script>

    <script src="../js/jquery.alerts.js" type="text/javascript"></script>

    <script type="text/javascript">
        var projectAllotDetail = new ProjectAllotDetail();
    </script>

</body>
</html>
