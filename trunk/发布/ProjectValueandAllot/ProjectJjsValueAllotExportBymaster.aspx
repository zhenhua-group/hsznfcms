﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectJjsValueAllotExportBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProjectJjsValueAllotExportBymaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">产值分配 <small>项目分配表导出</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>产值分配</a><i class="fa fa-angle-right">
    </i><a>项目分配表导出</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>产值分配明细
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="width: 80%;" align="center">
                                <tr>
                                    <td colspan="6"><span style="font-weight: bold;">部门:</span>
                                        <asp:Label ID="lbl_UnitName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">项目名称:
                                        <asp:Label ID="lblproName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">分配年份:
                                        <asp:Label ID="lbl_AllotYear" runat="server"></asp:Label>年
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 5%; text-align: center;">序号</td>
                                    <td style="width: 20%; text-align: center;">内容</td>
                                    <td style="width: 20%; text-align: center;">比例</td>
                                    <td style="width: 20%; text-align: center;">产值(元)</td>
                                    <td style="width: 25%; text-align: center;">计算栏</td>
                                    <td style="width: 10%; text-align: center;">备注</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">1</td>
                                    <td style="text-align: center;">转土建产值:&nbsp;部门:<asp:Label ID="lblName" runat="server"  Font-Bold="true"></asp:Label></td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblTranBulidingPercent" runat="server" ></asp:Label>
                                        (%)</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblTranBulidingCount" runat="server"></asp:Label>
                                        (元) </td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td style="text-align: center;">2</td>
                                    <td style="text-align: center;">实收产值</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblPaidValuePercent" runat="server" Text="100"></asp:Label>
                                        (%)</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lbl_PaidValueCount" runat="server"></asp:Label>
                                        (元)</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td style="text-align: center;">3</td>
                                    <td style="text-align: center;">本部门自留产值</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txtTheDeptValuePercent" runat="server" Text="Label"></asp:Label>
                                        (%)
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txtTheDeptValueCount" runat="server" Text="Label"></asp:Label>
                                        (元)</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td style="text-align: center;">4</td>
                                    <td style="text-align: center;">方案产值</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txt_ProgramPercent" runat="server" Text="Label"></asp:Label>
                                        (%)</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txt_ProgramCount" runat="server" Text="Label"></asp:Label>
                                        (元)</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td style="text-align: center;">5</td>
                                    <td style="text-align: center;">项目总负责</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txt_DesignManagerPercent" runat="server" Text="Label"></asp:Label>
                                        (%)</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txt_DesignManagerCount" runat="server" Text="Label"></asp:Label>
                                        (元)</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td style="text-align: center;">6</td>
                                    <td style="text-align: center;">应分产值</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txt_ShouldBeValuePercent" runat="server" Text="Label"></asp:Label>
                                        (%)</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>(元)</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td style="text-align: center;">7</td>
                                    <td style="text-align: center;">财务统计产值</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblFinanceValuePercent" runat="server"></asp:Label>
                                        %</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblFinanceValueCount" runat="server"></asp:Label>
                                        (元)</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td style="text-align: center;">8</td>
                                    <td style="text-align: center;">部门应留产值</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblTheDeptShouldValuePercent" runat="server"></asp:Label>
                                        %</td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblTheDeptShouldValueCount" runat="server"></asp:Label>
                                        (元)</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td colspan="6" style="letter-spacing: 2px">本部门领导签字： &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;产值转入部门领导签字：&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; 生产经营部：</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">本<br />
                                        部<br />
                                        门<br />
                                        产<br />
                                        值<br />
                                        分<br />
                                        配
                                    </td>
                                    <td colspan="5"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <asp:Button ID="btn_Export" runat="server" Text="导出" CssClass="btn  green" OnClick="btn_Export_Click" />

                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
