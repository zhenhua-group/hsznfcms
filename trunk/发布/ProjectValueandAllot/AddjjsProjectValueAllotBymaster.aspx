﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="AddjjsProjectValueAllotBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.AddjjsProjectValueAllotBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <%-- <link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseProjectValueUser.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/AddjjsProjectValueAllotBymaster.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>经济所产值分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>经济所产值分配</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>项目信息</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">项目信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 15%;">项目名称:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同关联:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">管理级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">审核级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目类别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">建设规模:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">承接部门:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">执行设总:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目阶段:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">实收金额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblPayShiCount" runat="server" Width="100px"></asp:Label>万元
                                        </td>
                                        <td style="width: 15%;">已分配金额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblAllotAccount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目分配年份:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                                <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 15%;">未分配金额:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNotAllotAccount" runat="server"></asp:Label>万元
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>经济所项目分配表填写</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="cls_ProjAllot_Table" style="width: 100%;" id="tbIsNotTranJjs">
                                <tr>
                                    <td style="width: 15%;">项目分配阶段：
                                    </td>
                                    <td style="width: 35%;">

                                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td style="width: 50%; border: none;">
                                                    <select id="stageIsNotTranJjs" class="form-control input-sm"
                                                        runat="server">
                                                        <option value="-1">--请选择分配阶段--</option>
                                                        <option value="18">预算</option>
                                                        <option value="19">预算 (锅炉房)</option>
                                                        <option value="20">预算(变电所)</option>
                                                        <option value="21">预算(水泵房)</option>
                                                        <option value="22">预算(空调机房)</option>
                                                        <option value="23">结算审核</option>
                                                        <option value="24">调整概算、造价鉴定</option>
                                                        <option value="25">建议书、可研</option>
                                                    </select>
                                                </td>
                                                <td style="width: 25%; border: none;" align="center">院所分成比例
                                                </td>
                                                <td style="width: 20%; border: none;">
                                                    <input id="txt_DividedPercent" class="form-control input-sm" maxlength="15" type="text"
                                                       value="1"  disabled="disabled" />
                                                </td>
                                                <td style="border: none;"><a id="sch_reletive" href="#div_Divided" class="  blue "
                                                    data-toggle="modal">选择</a></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 15%;">项目分配金额：
                                    </td>
                                    <td style="width: 35%;" class="cls_ProjAllot_Input">
                                        <input id="txt_AllotAccount" maxlength="15" type="text" />(万元)
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="cls_ProjAllotHead" width="100%">
                                <tr>
                                    <td align="center" style="width: 15%;">序号
                                    </td>
                                    <td align="center" style="width: 35%;">内容
                                    </td>
                                    <td align="center" style="width: 15%;">比例
                                    </td>
                                    <td align="center" style="width: 35%;">产值
                                    </td>
                                </tr>
                            </table>
                            <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="100%"
                                id="tbInfo">
                                <tr style="display: none;">
                                    <td style="width: 15%;">0
                                    </td>
                                    <td style="width: 35%;">
                                        <label class="control-label col-md-6">
                                            <font><font>转土建产值 &nbsp;部门</font></font>
                                        </label>
                                        <div class="col-md-6">
                                            <select id="drp_unit" class="form-control input-sm">
                                                <option value="-1">----请选择----</option>
                                                <asp:Repeater ID="RepeaterDepartment" runat="server">
                                                    <ItemTemplate>
                                                        <option value="<%#Eval("unit_ID") %>">
                                                            <%#Eval("unit_Name") %></option>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </select>
                                        </div>
                                    </td>
                                    <td style="width: 15%;">
                                        <input id="txtTranBulidingPercent" maxlength="15" type="text" />(%)
                                    </td>
                                    <td style="width: 35%;">
                                        <span id="TranBulidgingCount"></span>(元)
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">1
                                    </td>
                                    <td style="width: 35%;">实收产值
                                    </td>
                                    <td style="width: 15%;">
                                        <input id="txt_PaidValuePercent" maxlength="15" type="text" disabled="disabled" value="100" />(%)
                                    </td>
                                    <td style="width: 35%;">
                                        <span id="txt_PaidValueCount"></span>(元)
                                    </td>
                                </tr>
                                <tr>
                                    <td>2
                                    </td>
                                    <td>本部门自留产值
                                    </td>
                                    <td>
                                        <input id="txtTheDeptValuePercent" maxlength="8" type="text" />(%)
                                    </td>
                                    <td>
                                        <span id="txtTheDeptValueCount"></span>(元)
                                    </td>
                                </tr>
                                <tr>
                                    <td>3
                                    </td>
                                    <td>方案产值
                                    </td>
                                    <td>
                                        <input id="txt_ProgramPercent" maxlength="15" type="text" />(%)
                                    </td>
                                    <td>
                                        <span id="txt_ProgramCount"></span>(元)
                                    </td>
                                </tr>
                                <tr>
                                    <td>4
                                    </td>
                                    <td>项目总负责
                                    </td>
                                    <td>
                                        <input id="txt_DesignManagerPercent" maxlength="8" type="text" />(%)
                                    </td>
                                    <td>
                                        <span id="txt_DesignManagerCount"></span>(元)
                                    </td>
                                </tr>
                                <tr>
                                    <td>5
                                    </td>
                                    <td>应分产值
                                    </td>
                                    <td>
                                        <input id="txt_ShouldBeValuePercent" maxlength="15" type="text" disabled="disabled" />(%)
                                    </td>
                                    <td>
                                        <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>(元)
                                        <%--                        <span id="txt_ShouldBeValueCount" runat="server">0.00</span>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6
                                    </td>
                                    <td>财务统计产值
                                    </td>
                                    <td>
                                        <input id="txt_FinanceValuePercent" maxlength="15" type="text" disabled="disabled" />
                                        (%)                                       
                                    </td>
                                    <td>
                                        <span id="txt_FinanceValueCount">0.00</span>(元)                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td>7
                                    </td>
                                    <td>部门应留产值
                                    </td>
                                    <td>
                                        <input id="txt_TheDeptShouldValuePercent" maxlength="15" type="text" disabled="disabled">
                                        (%)                                        
                                    </td>
                                    <td>
                                        <span id="txt_TheDeptShouldValueCount">0.00</span>(元)                                     
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn green" id="btnApproval" href="#AuditUser" data-toggle="modal">
                                确定
                            </button>
                            <button type="button" class="btn default" id="btnRefuse">
                                返回
                            </button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HiddenIsRounding" value="<%=IsRounding %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenProSysNo" value="<%=proSysNo %>" />
    <input type="hidden" id="HiddenCprID" value="<%=CprID
    %>" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="560" aria-hidden="true"
        style="display: none; width: 560px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_close">
                关闭</button>
        </div>
    </div>
    <div id="div_Divided" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">院所分成比例</h4>
        </div>
        <div class="modal-body">
            <div id="cprReletiveDiv">
                <div class="form-body">
                    <table class="table-responsive">
                        <tr>
                            <td>年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year1" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                </asp:DropDownList>
                            <td>
                                <input type="button" class="btn blue" id="btn_search" value="查询" /></td>

                        </tr>
                    </table>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tb_Divided" class="table table-bordered table-striped table-condensed
    flip-content"
                                align="center">
                                <tr class="trBackColor">
                                    <td align="left">部门
                                    </td>
                                    <td align="center">年份
                                    </td>
                                    <td align="center">比例
                                    </td>
                                    <td align="center">操作
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
