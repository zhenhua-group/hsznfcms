﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="TranjjsProjectValueAllotAuditBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.TranjjsProjectValueAllotAuditBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/TranjjsProjectValueAllotAudit.js"></script>
    <script type="text/javascript" language="javascript">

        function openValueWindow() {
            var openUrl = "PrintValue.aspx?type=tranjjs&ProjectSysNo=" + $("#hidProID").val() + "&AllotId=" + $("#HidAllotID").val() + "&totalCount=" + $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text();

            var feature = "dialogWidth:960px;dialogHeight:550px;center:yes";

            window.open(openUrl, '打印', feature);

        }
    </script>
    <style type="text/css">
        .display
        {
            display: none;
        }
        
        /* 表格基本样式*/
        .cls_show_cst_jiben
        {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }
        .cls_show_cst_jiben td
        {
            border: solid 1px #CCC;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        .cls_show_cst_jiben tr
        {
            height: 22px;
        }
        .cls_content_head
        {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }
        .cls_content_head td
        {
            height: 20px;
            background-color: #E6E6E6;
            border: 1px solid Gray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        项目信息管理 <small>转经济所产值分配审核</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>转经济所产值分配审核</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>项目信息</font></font></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">
                                项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    合同关联:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    审核级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    项目总负责:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    合同额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目分配年份:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
                                                    (年)
                                                </td>
                                                <td style="width: 15%;">
                                                    其他参与部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    转经济所金额:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lblTotalCount" runat="server" Text=""></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>经济所项目分配表填写</font></font></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Literal ID="lblNotData" runat="server"></asp:Literal>
                                    <div style="margin-top: 5px;" class="cls_data">
                                        <table class="cls_ProjAllot_Table" style="width: 100%;">
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目分配阶段：
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblStage" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    项目分配金额：
                                                </td>
                                                <td style="width: 35%;" class="cls_ProjAllot_Input">
                                                    <asp:Label ID="lblAllotAmount" runat="server" Text="Label"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllotHead" width="100%">
                                            <tr>
                                                <td align="center" style="width: 15%;">
                                                    序号
                                                </td>
                                                <td align="center" style="width: 35%;">
                                                    内容
                                                </td>
                                                <td align="center" style="width: 15%;">
                                                    比例
                                                </td>
                                                <td align="center" style="width: 35%;">
                                                    产值
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="100%">
                                            <tr>
                                                <td style="width: 15%;">
                                                    1
                                                </td>
                                                <td style="width: 35%;">
                                                    实&nbsp;收&nbsp;产&nbsp;值
                                                </td>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="lblPaidValuePercent" runat="server" Text="100"></asp:Label>
                                                    (%)
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_PaidValueCount" runat="server"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    2
                                                </td>
                                                <td>
                                                    本部门自留产值
                                                </td>
                                                <td>
                                                    <asp:Label ID="txtTheDeptValuePercent" runat="server" Text="Label"></asp:Label>
                                                    (%)
                                                </td>
                                                <td>
                                                    <asp:Label ID="txtTheDeptValueCount" runat="server" Text="Label"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    3
                                                </td>
                                                <td>
                                                    方&nbsp;案&nbsp;产&nbsp;值
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_ProgramPercent" runat="server" Text="Label"></asp:Label>
                                                    (%)
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_ProgramCount" runat="server" Text="Label"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr style="display:none;">
                                                <td>
                                                    4
                                                </td>
                                                <td>
                                                    项目总负责
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_DesignManagerPercent" runat="server" Text="Label"></asp:Label>
                                                    (%)
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_DesignManagerCount" runat="server" Text="Label"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    4
                                                </td>
                                                <td>
                                                    应&nbsp;分&nbsp;产&nbsp;值
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_ShouldBeValuePercent" runat="server" Text="Label"></asp:Label>
                                                    (%)
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>(元)
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--评审内容-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>策划人员信息</font></font></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Literal ID="lblMember" runat="server"></asp:Literal>
                                    <!--评审内容-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>评审内容</font></font></div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <table style="width: 98%; height: auto; display: none;" class="cls_content_head"
                                        id="tbTopAudit">
                                        <tr>
                                            <td style="width: 15%; text-align: center;">
                                                评审部门
                                            </td>
                                            <td style="width: 70%; text-align: center;">
                                                评审内容
                                            </td>
                                            <td style="width: 15%; text-align: center;">
                                                评审人/日期
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 98%; height: auto; margin: auto; : none;" class="cls_show_cst_jiben"
                                        id="OneTable" audittable="auditTable">
                                        <tr>
                                            <td style="width: 15%; text-align: center;">
                                                生产经营部
                                            </td>
                                            <td style="width: 70%; text-align: center;">
                                                <textarea style="width: 98%; height: 60px;" id="OneSuggstion" class="TextBoxBorder"></textarea>
                                            </td>
                                            <td style="width: 15%; text-align: center;" id="AuditUser">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="tbAudit">
                <tr>
                    <td style="width: 100%; text-align: center;">
                        <input type="button" id="btnPass" name="controlBtn" class="btn green btn-default"
                            value="通过" />
                        <input type="button" id="btnNotPass" name="controlBtn" class="btn red btn-default"
                            value="不通过" />
                        <input id="btnPrintValue" type="button" onclick="openValueWindow();" value="打印产值"
                            class="btn purple btn-default" style="" />
                        <input type="button" id="btnRefuse" name="controlBtn" class="btn  btn-default"
                            value="返回" onclick="javascript:history.back();" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenOneSuggesiton" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.OneSuggestion %>" />
    <input type="hidden" id="HiddenStatus" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=AuditUser %>" />
    <input type="hidden" id="HiddenAuditData" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.AuditDate %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" id="hidProID" value="<%= proSysNo %>" />
    <input type="hidden" id="HidAllotID" value="<%= allotID %>" />
    <input type="hidden" id="HiddenSysNo" value="<%= ValueAllotAuditSysNo %>" />
    <input type="hidden" id="hiddenType" value="<%= Type %>" />
    <input type="hidden" id="hiddenTotalAcount" value="<%= TotalCount %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
</asp:Content>
