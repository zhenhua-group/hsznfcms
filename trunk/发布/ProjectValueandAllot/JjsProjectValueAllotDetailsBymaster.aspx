﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="JjsProjectValueAllotDetailsBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.JjsProjectValueAllotDetailsBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            CommonControl.SetFormWidth();
            ChangedBackgroundColor();
            //行背景
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr").hover(function () {
                $(this).addClass("tr_in");
            }, function () {
                $(this).removeClass("tr_in");
            });
            //隔行变色
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr:even").css({ background: "White" });

            $.each($(".progressbar"), function (index, item) {
                var percent = parseFloat($(item).attr("percent"));
                $(item).progressbar({
                    value: percent
                });
            });

            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr").each(function () {
                if ($.trim($(this).children(":eq(9)").text()) == "分配被驳回") {
                    $(this).children(":eq(9)").find("a").css({ color: "Red" });
                    $(this).children().children(".progressbar").children("div:first").css({ background: "Red" });
                } else if ($.trim($(this).children(":eq(9)").text()) == "分配结果") {
                    $(this).children(":eq(11)").text("---");
                }
                if ($.trim($(this).children(":eq(9)").text()) == "分配金额全部借出通过") {
                    $(this).children(":eq(11)").text("---");
                }
                if ($("#hid_isFlag").val() != "true") {
                    $(this).children(":eq(11)").text("---");
                }
            });

            //删除
            $("a[class=deleteAuditValue]").live("click", function () {

                if (confirm("是否要删除此条产值分配信息，删除之后不再恢复?")) {

                    var pro_id = $(this).attr("pro_id");
                    var allot_id = $(this).attr("allot_id");
                    var audit_id = $(this).attr("audit_id");

                    $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "action": "deleteAuditValue", "pro_id": pro_id, "allot_id": allot_id, "audit_id": audit_id, "type": "jjs" }, function (result) {

                        if (result == 1) {
                            alert("删除成功");
                            window.location.reload();
                        }
                        else {
                            alert("删除失败");
                        }
                    });
                }
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>经济所产值分配明细</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>领导驾驶舱<i class="fa fa-angle-right"> </i>数据统计<i class="fa fa-angle-right"> </i>经济所产值分配明细</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>经济所产值分配明细
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="width: 95%;" align="center">
                                <tr>
                                    <td style="width: 150px;">项目名称:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCprName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                产值分配明细如下
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="gv_ProjectValue" runat="server" CellPadding="0" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-hover" RowStyle-HorizontalAlign="Center"
                                ShowHeader="true" Font-Size="12px" RowStyle-Height="22px" Width="100%">
                                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                                <Columns>
                                   <asp:TemplateField HeaderText="分配次数" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            第 <%# Container.DataItemIndex+1 %>产值分配
                                        </ItemTemplate>
                                        <ItemStyle Width="8%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ActualAllountTime" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="分配年份(年)">
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PayShiCount" ItemStyle-HorizontalAlign="Center" HeaderText="实收金额(万元)"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotCount" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center" HeaderText=" 分配金额(万元)">
                                        <ItemStyle HorizontalAlign="Center" Width="9%"></ItemStyle>
                                    </asp:BoundField>
                                   
                                    <asp:BoundField DataField="ActualAllotCount" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="应分产值(元)">
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundField>
                                  
                                    <asp:BoundField DataField="LoanValueCount" ItemStyle-HorizontalAlign="Center" HeaderText="借出金额(元)"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="9%"></ItemStyle>
                                    </asp:BoundField>
                                     <asp:BoundField DataField="BorrowValueCount" ItemStyle-HorizontalAlign="Center" HeaderText="借入金额(元)"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotDate" ItemStyle-HorizontalAlign="Center" HeaderText="分配时间"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="状态" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div class="progressbar" id="auditLocusContainer" action="JJS" percent="<%#Eval("Percent") %>"
                                                referencesysno="<%# Eval("AuditSysNo") %>" style="cursor: pointer; height：98%;">
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="分配状态" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Eval("LinkAction")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="8%" />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="系数查看" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a  href="ProjectJjsValueAllotExportBymaster.aspx?pro_id=<%#Eval("pro_ID") %>&allot_id=<%#Eval("ID")%>" style="cursor: pointer;">查看</a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="6%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="删除" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a class="deleteAuditValue" pro_id="<%#Eval("pro_ID") %>" allot_id="<%#Eval("ID")%>" audit_id="<%# Eval("AuditSysNo") %>" style="cursor: pointer;">删除</a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="6%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    无分配记录！
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
     <input type="hidden" id="hid_isFlag" value="<%=isFlag %>" />
</asp:Content>
