﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProAllot.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProAllot" %>

<%@ Register Assembly="xgo.Components" Namespace="xgo.Components" TagPrefix="cc2" %>
<%@ Register Src="../UserControl/UserOfTheDepartmentTree.ascx" TagName="UserOfTheDepartmentTree"
    TagPrefix="uc1" %>
<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjStructCss2.css" rel="stylesheet" type="text/css" />

    <script src="../js/wdate/WdatePicker.js" type="text/javascript"></script>

    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>

    <script src="../js/jquery.alerts.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>

    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>

    <script src="../js/ProjectValueandAllot/ProAllot.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/UserControl/UserOfTheDepartmentTree.js"></script>

    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>

    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var chooseUserOfTheDepartmentControl = new UserOfTheDepartmentTree($("#manager"));

            //选择用户回掉
            function ChooseUserOfTheDepartmentCallBack(userArray) {
                var showResultTable = $("#txtproManager");
                $.each(userArray, function (index, item) {
                    var trString = item.userName;
                    $("#txtproManager").val(trString);
                    $("#hiddenuserNo").val(item.userSysNo)
                });
            }

            $("#schmanager").click(function () {
                $("#manager").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 550,
                    height: 400,
                    resizable: false,
                    buttons:
			        {
			            "确定": function () {
			                //调用处理事件
			                chooseUserOfTheDepartmentControl.SaveUser(ChooseUserOfTheDepartmentCallBack);
			                $(this).dialog("close");
			            },
			            "取消": function () { $(this).dialog("close"); }
			        }
                }).dialog("open");
            });
        })
    </script>

    <style type="text/css">
        #asTreeviewStruct a {
            font-size: 9pt;
        }
    </style>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
        <table class="cls_container">
            <tr>
                <td class="style2">&nbsp;&nbsp;当前位置：[项目产值分配]
                </td>
            </tr>
        </table>
        <table style="width: 100%;" class="show_gvproValueandAllot">
            <tr>
                <td align="left" colspan="4">
                    <asp:ImageButton ID="btnSave" runat="server"
                        ImageUrl="~/Images/buttons/btn_save2.gif" />
                    <a href="ProValueandAlltList.aspx">
                        <img src="../Images/buttons/btn_back2.gif" style="width: 64px; height: 22px; border: none;" /></a>

                </td>
            </tr>
            <tr>
                <td style="width: 100px;">项目名称:
                </td>
                <td style="width: 300px;">
                    <asp:TextBox ID="txt_proName" runat="server" CssClass="TextBoxBorder" Width="280px"></asp:TextBox>
                </td>
                <td style="width: 100px;">合同名称:
                </td>
                <td>
                    <asp:TextBox ID="txtcpr" runat="server" CssClass="TextBoxBorder"></asp:TextBox>
                    &nbsp;<asp:HiddenField ID="hid_addr" runat="server" />
                </td>
            </tr>
            <tr>
                <td>项目工号:
                </td>
                <td>
                    <asp:TextBox ID="txtproNo" runat="server" CssClass="TextBoxBorder"></asp:TextBox>
                </td>
                <td style="width: 100px;">合同编号:
                </td>
                <td>
                    <asp:TextBox ID="txtcprNo" runat="server" CssClass="TextBoxBorder"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>结构样式:
                </td>
                <td>
                    <%= StrStructType %>
                </td>
                <td>合同额:
                </td>
                <td>
                    <asp:TextBox ID="txtcprAcount" runat="server" CssClass="TextBoxBorder"></asp:TextBox>万元</td>
            </tr>
            <tr>
                <td>管理级别:
                </td>
                <td>
                    <input id="Radio1" type="radio" value="一级" name="jb" checked="checked" />一级
                <input id="Radio2" type="radio" value="二级" name="jb" />二级
                </td>
                <td>建设规模:
                </td>
                <td>
                    <asp:TextBox ID="txtscale" runat="server" CssClass="TextBoxBorder"></asp:TextBox>
                    平米
                </td>
            </tr>
            <tr>
                <td>建设单位:
                </td>
                <td>
                    <asp:TextBox ID="txtbuildUnit" runat="server" CssClass="TextBoxBorder" Width="280px"></asp:TextBox>
                </td>
                <td>项目经理:
                </td>
                <td>
                    <input type="hidden" id="hiddenuserNo" runat="server" class="text" />
                    <asp:TextBox ID="txtproManager" runat="server" CssClass="TextBoxBorder"></asp:TextBox>
                    <a href="#" id="schmanager">查找</a>
                </td>
            </tr>
            <tr>
                <td>项目开始日期:
                </td>
                <td>
                    <asp:Label ID="txtstarttime" runat="server"></asp:Label>
                </td>
                <td>项目完成日期:
                </td>
                <td>
                    <asp:Label ID="txtfinishtime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <%--  <a href='ProjectFeeAllotInfo.aspx?pro_id=<%=StrProjectId %>&cprid=<%=StrCoperationId %>&ProcessType=0&IsAllot=<%= IsAllot %>' id="btn_allot"
                        style="font-weight: bold;">
                        <img src="../Images/buttons/btn_fenpei.gif" style="width: 64px; height: 22px; border: none;" /></a>--%>
                    <asp:ImageButton ID="btn_allot" runat="server" ImageUrl="/Images/buttons/btn_fenpei.gif" OnClick="btn_allot_Click" />
                </td>
            </tr>
        </table>
        <div class="cls_data" runat="server" id="Allot">
            <table class="cls_content_head">
                <tr>
                    <td style="width: 25%;" align="center">分配次数
                    </td>
                    <td style="width: 30%" align="center">本次金额
                    </td>
                    <td style="width: 15%" align="center">占合同额百分比
                    </td>
                    <td style="width: 15%" align="center">分配日期
                    </td>
                    <%--<td style="width: 5%" align="center">
                    编辑
                </td>--%>
                    <td style="width: 5%" align="center">查看
                    </td>
                    <td style="width: 5%" align="center">删除
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gvAllot" runat="server" AutoGenerateColumns="False" Font-Size="12px"
                ShowHeader="False" Width="100%" OnRowDataBound="gvAllot_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="分配次数">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text="<%#Container .DataItemIndex +1 %>"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="25%" />
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="金额" DataField="CurCharge">
                        <ItemStyle HorizontalAlign="Center" Width="30%" />
                    </asp:BoundField>
                    <%-- <asp:TemplateField HeaderText="编辑">
                    <ItemTemplate>
                        <a href='ProjectFeeAllot.aspx?pro_id=<%=StrProjectId %>&cprid=<%=StrCoperationId %>&ProcessType=1&cost_id=<%# Eval("cost_id") %>'
                            target="_blank">编辑</a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>--%>
                    <asp:BoundField DataField="CurRate" HeaderText="百分比">
                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="InDate" DataFormatString="{0:d}" HeaderText="收款日期">
                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="查看">
                        <ItemTemplate>
                            <a href='ShowProjAllotDetailsTable.aspx?proid=<%=StrProjectId %>&cprid=<%=StrCoperationId %>&costid=<%# Eval("ID") %>' id="overviewAllot">查看</a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="删除">
                        <ItemTemplate>
                            <a href="#" rel='<%# Eval("ID") %>' class="delete">删除</a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条" CustomInfoTextAlign="Left"
            FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
            PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
            ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
            TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="10" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
        </div>

        <div id="manager" style="display: none;">
            <uc1:UserOfTheDepartmentTree ID="UserOfTheDepartmentTree1" runat="server" IsRadio="true" />
        </div>
        <!-- 隐藏按钮区域 -->
        <asp:HiddenField ID="hid_cprid" runat="server" Value="0" />
        <input id="hid_isallot" type="hidden" value='<%= IsAllot %>' />
        <input id="hid_iscompelete" type="hidden" value='<%= IsComplete %>' />
        <input id="hiddenAction" type="hidden" value='<%= ActionFlag %>' />
    </form>
</body>
</html>
