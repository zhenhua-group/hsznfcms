﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="EconomyProjectValueAllotListBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.EconomyProjectValueAllotListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/ProjectValueandAllot/EconomyProjectValueAllotList_jq.js"></script>
    <script type="text/javascript">
        $(function () {
            AllotList3();
            AllotList();
            AllotList2();

        });  //清空数据  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>经济所产值分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">产值分配</a><i
        class="fa fa-angle-right"> </i><a href="#">经济所产值分配</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询经济所产值分配
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>分配年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control " Width="90px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>项目名称:</td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_cprName" runat="server" /></td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_Search" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>经济所产值分配列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <ul class="nav nav-tabs">
                         <li class="active"><a href="#tab_1_3" data-toggle="tab">分配中项目</a></li>
                        <li class=""><a href="#tab_1_1" data-toggle="tab">分配中项目</a></li>
                        <li class=""><a href="#tab_1_2" data-toggle="tab">分配完毕项目</a></li>
                    </ul>
                    <div class="tab-content">
                         <div class="tab-pane fade active in" id="tab_1_3" style="width: 100%">

                            <table id="jqGrid3">
                            </table>
                            <div id="gridpager3">
                            </div>
                            <div id="nodata3" class="norecords">
                            </div>

                        </div>
                        <div class="tab-pane fade  " id="tab_1_1" style="width: 100%">

                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                            </div>

                        </div>
                        <div class="tab-pane fade " id="tab_1_2" style="width: 100%">

                            <table id="jqGrid2">
                            </table>
                            <div id="gridpager2">
                            </div>
                            <div id="nodata2" class="norecords">
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HiddenUserSysNo" value="<%=UserSysNo %>" />
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_where2" runat="server" Value="" />

     <input type="hidden" id="hid_isFlag" value="<%=isFlag %>" />
</asp:Content>
