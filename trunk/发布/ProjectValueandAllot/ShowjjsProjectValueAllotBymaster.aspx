﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ShowjjsProjectValueAllotBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ShowjjsProjectValueAllotBymaster" %>

<%@ Register Src="../UserControl/ChooseProjectValueUser.ascx" TagName="ChooseProjectValueUser"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/AddExternalMember.ascx" TagName="AddExternalMember"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <%--<link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseProjectValueUser.js" type="text/javascript"></script>
    <script src="../js/UserControl/AddExternalMember.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.js"></script>
    <script type="text/javascript" language="javascript">

        function openValueWindow() {
            var openUrl = "PrintValue.aspx?type=jjs&ProjectSysNo=" + $("#hidProID").val() + "&AllotId=" + $("#HiddenAllot").val() + "&totalCount=" + $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text();

            var feature = "dialogWidth:960px;dialogHeight:550px;center:yes";

            window.open(openUrl, '打印', feature);

        }
    </script>
    <style type="text/css">
        .display {
            display: none;
        }

        /* 表格基本样式*/
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .cls_show_cst_jiben td {
                border: solid 1px #CCC;
                font-size: 12px;
                font-family: "微软雅黑";
            }

            .cls_show_cst_jiben tr {
                height: 22px;
            }

        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 20px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>经济所产值分配明细</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>经济所产值分配明细</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>项目信息</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center" id="tb_project">
                                            <tr>
                                                <td style="width: 15%;">项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">合同关联:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">审核级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">执行设总:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">合同额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">其他参与部门:
                                                </td>
                                                <td style="width: 35%;" colspan="3">
                                                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr class="scjy">
                                                <td style="width: 15%;">实收金额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblPayShiCount" runat="server" Width="100px"></asp:Label>万元
                                                </td>
                                                <td style="width: 15%;">已分配金额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblAllotAccount" runat="server" Text=""></asp:Label>万元
                                                </td>
                                            </tr>
                                            <tr class="scjy">
                                                <td style="width: 15%;">项目分配年份:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblYear" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">未分配金额:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lblNotAllotAccount" runat="server"></asp:Label>万元
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>经济所项目分配表
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Literal ID="lblNotData" runat="server"></asp:Literal>

                                    <fieldset style="font-size: 12px;" id="fieldset">
                                        <legend style="font-size: 12px;">经济所项目分配表填写</legend>
                                        <div style="margin-top: 5px;" class="cls_data">
                                            <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" style="width: 100%;">
                                                <tr>
                                                    <td style="width: 15%;">项目分配阶段：
                                                    </td>
                                                    <td style="width: 35%;">

                                                        <table width="100%" style="border: none;" id="tb_ysfc">
                                                            <tr>
                                                                <td style="border: none; width: 35%;">
                                                                    <asp:Label ID="lblStage" runat="server"></asp:Label>
                                                                <td style="border: none; width: 30%;">院所分成比例</td>
                                                                <td style="border: none; width: 30%;">
                                                                    <asp:Label ID="lblDividedPercent" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 15%;">项目分配金额：
                                                    </td>
                                                    <td style="width: 35%;" class="cls_ProjAllot_Input">
                                                        <asp:Label ID="lblAllotAmount" runat="server" Text="Label"></asp:Label>
                                                        (万元)
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="cls_ProjAllotHead" width="100%">
                                                <tr>
                                                    <td align="center" style="width: 15%;">序号
                                                    </td>
                                                    <td align="center" style="width: 35%;">内容
                                                    </td>
                                                    <td align="center" style="width: 15%;">比例
                                                    </td>
                                                    <td align="center" style="width: 35%;">产值
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="100%"
                                                id="tbInfo">
                                                <tr style="display: none;">
                                                    <td style="width: 15%;">0
                                                    </td>
                                                    <td style="width: 35%;">转土建产值&nbsp;部门：<asp:Label ID="lblName" runat="server" Text="100"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%;">
                                                        <asp:Label ID="lblTranBulidingPercent" runat="server" Text="100"></asp:Label>
                                                        (%)
                                                    </td>
                                                    <td style="width: 35%;">
                                                        <asp:Label ID="lblTranBulidingCount" runat="server"></asp:Label>
                                                        (元)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%;">1
                                                    </td>
                                                    <td style="width: 35%;">实&nbsp;收&nbsp;产&nbsp;值
                                                    </td>
                                                    <td style="width: 15%;">
                                                        <asp:Label ID="lblPaidValuePercent" runat="server" Text="100"></asp:Label>
                                                        (%)
                                                    </td>
                                                    <td style="width: 35%;">
                                                        <asp:Label ID="lbl_PaidValueCount" runat="server"></asp:Label>
                                                        (元)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2
                                                    </td>
                                                    <td>本部门自留产值
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txtTheDeptValuePercent" runat="server" Text="Label"></asp:Label>
                                                        (%)
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txtTheDeptValueCount" runat="server" Text="Label"></asp:Label>
                                                        (元)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3
                                                    </td>
                                                    <td>方&nbsp;案&nbsp;产&nbsp;值
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txt_ProgramPercent" runat="server" Text="Label"></asp:Label>
                                                        (%)
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txt_ProgramCount" runat="server" Text="Label"></asp:Label>
                                                        (元)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4
                                                    </td>
                                                    <td>项目总负责
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txt_DesignManagerPercent" runat="server" Text="Label"></asp:Label>
                                                        (%)
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txt_DesignManagerCount" runat="server" Text="Label"></asp:Label>
                                                        (元)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>5
                                                    </td>
                                                    <td>应&nbsp;分&nbsp;产&nbsp;值
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txt_ShouldBeValuePercent" runat="server" Text="Label"></asp:Label>
                                                        (%)
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>(元)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>6
                                                    </td>
                                                    <td>财务统计产值
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFinanceValuePercent" runat="server"></asp:Label>
                                                        %
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFinanceValueCount" runat="server"></asp:Label>
                                                        (元)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>7
                                                    </td>
                                                    <td>部门应留产值
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTheDeptShouldValuePercent" runat="server"></asp:Label>
                                                        %
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTheDeptShouldValueCount" runat="server"></asp:Label>
                                                        (元)
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <fieldset id="saveProcessInfo" style="text-align: left; ">
                                        <legend style="font-size: 12px;">保存借出借入金额</legend>
                                        <table style="width: 98%; display: none;" class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label"
                                            id="tbSaveProcessInfo">
                                            <tr>
                                                <td style="width: 20%;">分配金额是否全部借出:
                                                </td>
                                                <td colspan="3" style="text-align: left;">
                                                    <div class="col-md-12">
                                                        <div class="radio-list">
                                                            <label class="radio-inline">
                                                                <span class="">
                                                                    <input type="radio" name="aa" id="radio_yes" runat="server" value="1" />
                                                                </span>是
                                                            </label>
                                                            <label class="radio-inline">
                                                                <span class="checked">
                                                                    <input type="radio" name="aa" id="radio_no" runat="server" value="0" checked />
                                                                </span>否
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 20%;">项目借入金额：</td>
                                                <td style="width: 30%;">
                                                    <input id="txt_BorrowCount" maxlength="15" type="text" runat="server" style="width: 100px;" />(元)</td>

                                                <td style="width: 20%;">可用借入金额：
                                                </td>
                                                <td style="width: 30%;">

                                                    <asp:Label ID="txt_ActulloanCount" runat="server" Text="0.00"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>项目借出金额：
                                                </td>
                                                <td>
                                                    <input id="txt_loanCount" maxlength="15" type="text" runat="server" style="width: 100px;" />(元)
                                                </td>
                                                <td>实际分配金额：
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_ActulCount" runat="server" Text="0.00"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="width: 98%; display: none;" class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label"
                                            id="tbSaveed">
                                            <tr>
                                                <td>分配金额是否全部借出:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lbl_IsTAllPass" runat="server" Text="0.00"></asp:Label>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 20%;">项目借入金额</td>
                                                <td style="width: 30%;">
                                                    <asp:Label ID="lblBorrowAccount" runat="server"></asp:Label>(元)</td>

                                                <td style="width: 20%;">可用借入金额：
                                                </td>
                                                <td style="width: 30%;">
                                                    <asp:Label ID="lblActalLoanAcount" runat="server" Text="0.00"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>项目借出金额：
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLoanAcount" runat="server"></asp:Label>(元)
                                                </td>
                                                <td>实际分配金额：
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblActalAcount" runat="server" Text="0.00"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="width: 98%; margin: auto;" class="cls_show_cst_jiben" id="tbSave">
                                            <tr>
                                                <td align="center">
                                                    <input type="button" id="btnSaveProcess" name="controlBtn" class="btn green btn-default"
                                                        value="保存" style="display: none;" />
                                                    <input type="button" id="btnCancelProcess" name="controlBtn" class="btn btn-default"
                                                        value="取消" style="display: none;" />
                                                    <span id="spanSuccessInfo" style="display: none; color: Red;">保存成功，请填写人员系数！</span><span
                                                        id="addExternal" style="color: Blue; cursor: pointer; display: none;">修改项目阶段信息</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <fieldset id="file_szxs" style="text-align: left; display: none;">
                                        <legend style="font-size: 12px;">设总系数分配</legend>
                                        <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="98%" id="tb_szxs">
                                            <tr>
                                                <td style="width: 20%; font-weight: bold;">序号
                                                </td>
                                                <td style="width: 30%; font-weight: bold;">姓名
                                                </td>
                                                <td style="width: 20%; font-weight: bold;">设总分配比例
                                                 <asp:Label ID="lbl_DesignManagerPercent" runat="server"></asp:Label>
                                                    %
                                                </td>
                                                <td style="width: 30%; font-weight: bold;">设总金额: 
                                                            <asp:Label ID="lbl_DesignManagerCount" runat="server"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <asp:Literal ID="lbl_DesignManagerValue" runat="server"></asp:Literal>

                                        </table>
                                    </fieldset>

                                    <asp:Literal ID="lblMemberEidt" runat="server"></asp:Literal>
                                    <asp:Literal ID="lblMemberShow" runat="server"></asp:Literal>
                                    <fieldset style="font-size: 12px; display: none" id="fieldAudit">
                                        <legend style="font-size: 12px;">评审内容</legend>
                                        <table style="width: 98%; height: auto;" class="cls_content_head" id="tbTopAudit">
                                            <tr>
                                                <td style="width: 15%; text-align: center;">评审部门
                                                </td>
                                                <td style="width: 70%; text-align: center;">评审内容
                                                </td>
                                                <td style="width: 15%; text-align: center;">评审人/日期
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="width: 98%; height: auto; margin: auto;" class="cls_show_cst_jiben"
                                            id="OneTable" audittable="auditTable">
                                            <tr>
                                                <td style="width: 15%; text-align: center;">生产经营部
                                                </td>
                                                <td style="width: 70%; text-align: center;">
                                                    <textarea style="width: 98%; height: 60px;" id="OneSuggstion" class="TextBoxBorder"></textarea>
                                                </td>
                                                <td style="width: 15%; text-align: center;" id="AuditUser"></td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <input type="button" id="btn_AllApproval" name="controlBtn" class="btn  yellow btn-default" style="display: none;"
                                value="全部通过" />
                            <input type="button" id="btnPass" name="controlBtn" href="#AuditUserDiv"
                                class="btn green btn-default" value="通过" />

                            <input type="button" id="btnNotPass" name="controlBtn" class="btn red btn-default"
                                value="不通过" />
                            <input id="btnPrintValue" type="button" onclick="openValueWindow();" value="打印产值"
                                class="btn purple btn-default" style="display: none;" />
                            <input type="button" id="btnRefuse" name="controlBtn" class="btn btn-default" value="返回"
                                onclick="javascript: history.back();" />
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo
    %>" />
    <input type="hidden" id="HiddenOneSuggesiton" value="<%=projectAuditRecordEntity
    ==null?"":projectAuditRecordEntity.OneSuggestion %>" />
    <input type="hidden" id="HiddenStatus" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=AuditUser %>" />
    <input type="hidden" id="HiddenAuditData" value="<%=AuditDate %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=projectAuditRecordEntity
    ==null?0:projectAuditRecordEntity.SysNo %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" id="hidProID" value="<%=
    proSysNo %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus
    %>" />
    <input type="hidden" id="HiddenSysNo" value="<%= ValueAllotAuditSysNo %>" />
    <input type="hidden" id="HiddenItemType" value="<%= ProjectValueAllot==null?"":ProjectValueAllot.Itemtype.ToString()
    %>" />
    <input type="hidden" id="HiddenAllot" value="<%=projectAuditRecordEntity
    ==null?"":projectAuditRecordEntity.AllotID.ToString() %>" />
    <input type="hidden" id="hiddenType" value="<%= Type %>" />
    <!--项目转过来连接-->
    <input type="hidden" id="hiddenProlink" value="<%=Prolink%>" />
    <input type="hidden" id="stage" value="<%=
    Stage %>" />
    <!--是否全部转出-->
    <input type="hidden" id="hidIsTAllPass" value="<%= IsTAllPass %>" />
    <input type="hidden" id="HiddenIsRounding" value="<%=IsRounding %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--弹出层-->
    <div id="chooseUserMainDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加人员</h4>
        </div>
        <div class="modal-body" id="chooseUserDiv">
            <div id="chooseUserMain">
                <uc1:ChooseProjectValueUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_UserMain" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="chooseExtUserDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加外聘人员</h4>
        </div>
        <div class="modal-body" id="chooseExtDiv">
            <div id="chooseExternalUserDiv">
                <uc2:AddExternalMember ID="chooseExternalUser1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btnChooseExt" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="AuditUserDiv" class="modal
    fade yellow"
        tabindex="-1" data-width="450" aria-hidden="true" style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_close">
                关闭</button>
        </div>
    </div>
</asp:Content>
