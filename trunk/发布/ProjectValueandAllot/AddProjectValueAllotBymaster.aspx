﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="AddProjectValueAllotBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.AddProjectValueAllotBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/AddProjectValueAllotBymaster.js"></script>
    <script src="../js/ProjectValueandAllot/ProjectValueMeaaageSend.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>土建所产值分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>土建所产值分配</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">项目信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 15%;">项目名称:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同关联:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">管理级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">审核级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目类别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">建设规模:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">承接部门:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">项目总负责:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目阶段:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">实收金额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblPayShiCount" runat="server" Width="100px"></asp:Label>万元
                                        </td>
                                        <td style="width: 15%;">已分配金额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblAllotAccount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">未分配金额:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lblNotAllotAccount" runat="server"></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目分配表填写
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <td style="width: 15%;">项目分配年份：
                                    </td>
                                    <td style="width: 35%;">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td style="width: 33%;">
                                                    <asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                                        <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 30%;" align="center">院所分成比例
                                                </td>
                                                <td style="width: 28%;">
                                                    <input id="txt_DividedPercent" class="form-control input-sm" maxlength="15" type="text"  value="1"
                                                        disabled="disabled" />
                                                </td>
                                                <td><a id="sch_reletive" href="#div_Divided" class="  blue "
                                                    data-toggle="modal">选择</a></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 15%;">项目分配金额：
                                    </td>
                                    <td style="width: 35%;" class="cls_ProjAllot_Input">
                                        <div class=" input-large">
                                            <div class="col-md-8">
                                                <input id="txt_AllotAccount" maxlength="15" type="text" class="form-control input-sm"
                                                    runat="server" />
                                            </div>
                                            <div class="col-md-4">
                                                (万元)
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover" width="100%">
                                <tr>
                                    <th align="center" style="width: 15%;">序号
                                    </th>
                                    <th align="center" style="width: 35%;">内容
                                    </th>
                                    <th align="center" style="width: 15%;">比例
                                    </th>
                                    <th align="center" style="width: 35%;">产值
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">&nbsp;1
                                    </td>
                                    <td style="width: 35%;">实&nbsp;收&nbsp;产&nbsp;值
                                    </td>
                                    <td style="width: 15%;">
                                        <div class="col-md-8">
                                            <input id="txt_PaidValuePercent" maxlength="15" type="text" disabled="disabled" class="form-control input-sm"
                                                value="100" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td style="width: 35%;">
                                        <div class="col-md-8">
                                            <span id="txt_PaidValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2
                                    </td>
                                    <td>设&nbsp;&nbsp;&nbsp;&nbsp;总
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_DesignManagerPercent" class="form-control input-sm" maxlength="8"
                                                type="text" value="3" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txt_DesignManagerCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3
                                    </td>
                                    <td>分&nbsp;配&nbsp;产&nbsp;值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txtAllotValuePercent" class="form-control input-sm" maxlength="15" type="text"
                                                disabled="disabled" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txtAllotValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4
                                    </td>
                                    <td>转经济所产值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_EconomyValuePercent" maxlength="8" class="form-control input-sm" type="text"
                                                runat="server" value="0.00" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txt_EconomyValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5
                                    </td>
                                    <td>转暖通部门产值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_HavcValuePercent" class="form-control input-sm" maxlength="8" type="text"
                                                runat="server" value="10" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txt_HavcValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6
                                    </td>
                                    <td>转出其他部门产值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txtOtherDeptValuePercent" class="form-control input-sm" maxlength="8"
                                                type="text" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txtOtherDeptValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7
                                    </td>
                                    <td>本部门自留产值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txtTheDeptValuePercent" class="form-control input-sm" maxlength="8" type="text"
                                                runat="server" value="10" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txtTheDeptValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8
                                    </td>
                                    <td>扣&nbsp;发&nbsp;产&nbsp;值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_ProgramPercent" class="form-control input-sm" maxlength="15" type="text"
                                                disabled="disabled" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_ProgramCount" class="form-control input-sm" maxlength="15" type="text"
                                                runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9
                                    </td>
                                    <td>本部门产值合计
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_UnitValueCountPercent" class="form-control input-sm" maxlength="15"
                                                type="text" disabled="disabled" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txtUnitValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10
                                    </td>
                                    <td>应&nbsp;分&nbsp;产&nbsp;值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_ShouldBeValuePercent" class="form-control input-sm" maxlength="15"
                                                type="text" disabled="disabled" runat="server" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txt_ShouldBeValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11
                                    </td>
                                    <td>财务统计产值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_FinanceValuePercent" class="form-control input-sm" maxlength="15"
                                                type="text" disabled="disabled" value="0.00" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txt_FinanceValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>12
                                    </td>
                                    <td>部门应留产值
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <input id="txt_TheDeptShouldValuePercent" class="form-control input-sm" maxlength="15"
                                                type="text" disabled="disabled" value="0.00" />
                                        </div>
                                        <div class="col-md-2">
                                            (%)
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-8">
                                            <span id="txt_TheDeptShouldValueCount">0.00</span>(元)
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn green" id="btnApproval" href="#AuditUser" data-toggle="modal">
                                确定
                            </button>
                            <button type="button" class="btn default" id="btnRefuse">
                                返回
                            </button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="msgReceiverContainer" style="width: 450px; height: 200px; display: none;">
    </div>
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenProSysNo" value="<%=proSysNo %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=ValueAllotAuditSysNo %>" />
    <input type="hidden" id="HiddenIsTrunEconomy" value="<%=IsTrunEconomy %>" />
    <input type="hidden" id="HiddenIsTrunHavc" value="<%=IsTrunHavc %>" />
    <input type="hidden" id="HiddenCprID" value="<%=CprID
    %>" />
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_close">
                关闭</button>
        </div>
    </div>
    <div id="div_Divided" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">院所分成比例</h4>
        </div>
        <div class="modal-body">
            <div id="cprReletiveDiv">
                <div class="form-body">
                    <table class="table-responsive">
                        <tr>
                            <td>年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year1" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                </asp:DropDownList>
                            <td>
                                <input type="button" class="btn blue" id="btn_search" value="查询" /></td>

                        </tr>
                    </table>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tb_Divided" class="table table-bordered table-striped table-condensed
    flip-content"
                                align="center">
                                <tr class="trBackColor">
                                    <td align="left">部门
                                    </td>
                                    <td align="center">年份
                                    </td>
                                    <td align="center">比例
                                    </td>
                                    <td align="center">操作
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
