﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="CostImportBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.CostImportBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            var tempRandom = Math.random() + new Date().getMilliseconds();
            $("#<%=btnUpload.ClientID%>").click(function () {
                var name = $("#<%=FileUpload.ClientID%>").val();
                if (name == '') {
                    alert("请选择上传的文件");
                    return false;
                }
                var extend = name.substring(name.lastIndexOf('.') + 1);

                if (extend == "xls" || extend == "xlsx") {

                }
                else {
                    alert("请上传excel的文件格式");
                    return false;
                }
                var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
                if (year == "-1") {
                    alert("请选择年份!");
                    return false;
                }

                var type = $("#ctl00_ContentPlaceHolder1_drp_costType").val();
                if (type == "-1") {
                    alert("请选择类型!");
                    return false;
                }
            });
            //$("label", "#").attr("class", "radio-inline")
            //$(".no").click(function () {
            //    var unitid = $(this).attr("unitid");
            //    var data = "action=consttype&unitid=" + unitid;
            //    $("#ctl00_ContentPlaceHolder1_drp_costType").empty();
            //    $.ajax({
            //        type: "Get",
            //        dataType: "json",
            //        url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            //        data: data,
            //        success: function (result) {
            //            if (result != null) {
            //                var obj = result.ds;
            //                var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
            //                $.each(obj, function (i, n) {
            //                    gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
            //                });
            //                $("#ctl00_ContentPlaceHolder1_drp_costType").html(gcFzr_UnitOptionHtml);
            //            }
            //        },
            //        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //            alert("系统错误!");
            //        }
            //    });
            //});

        });
    </script>
    <script type="text/javascript" src="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>设计所成本导入</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>财务管理 </a><i class="fa fa-angle-right"></i><a>设计所成本导入</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>成本明细上传
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a id="btn_back" class="btn red btn-sm" href="CostDetailsBymaster.aspx">返回明细列表</a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td colspan="4">
                                <div class="radio-list">
                                    <asp:RadioButtonList runat="Server" ID="radiolist" RepeatDirection="Horizontal" CausesValidation="false" CellPadding="10" CellSpacing="10" AutoPostBack="true" RepeatColumns="12" OnSelectedIndexChanged="radiolist_SelectedIndexChanged">
                                        <asp:ListItem Value="4">生产部门</asp:ListItem>
                                        <asp:ListItem Value="1">勘察分院 </asp:ListItem>
                                        <asp:ListItem Value="2">施工图审查室</asp:ListItem>
                                        <asp:ListItem Value="3">承包公司</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>成本年份:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True" Width="90px">
                                    <asp:ListItem Value="-1">-----选择年份-----</asp:ListItem>
                                    <asp:ListItem>2010</asp:ListItem>
                                    <asp:ListItem>2011</asp:ListItem>
                                    <asp:ListItem>2012</asp:ListItem>
                                    <asp:ListItem>2013</asp:ListItem>
                                    <asp:ListItem>2014</asp:ListItem>
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>成本类型:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_costType" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----选择类型-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>成本数据文件:
                            </td>
                            <td colspan="1">
                                <asp:FileUpload ID="FileUpload" runat="server" Width="100%" />
                            </td>
                            <td colspan="1">(文件类型：.xls)</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="1" style="text-align: left;">
                                <asp:Button ID="btnUpload" runat="server" Text="上 传" CssClass="btn blue btn-sm" OnClick="btnUpload_Click" /></td>
                            <%--   <td colspan="4" style="text-align: left;">
                                <asp:Button ID="btnUpload" runat="server" Text="上 传" CssClass="btn blue btn-sm" OnClick="btnUpload_Click" /></td>--%>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>上传详情
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div style="width: 98%; margin: 0 auto;">
                                <asp:GridView ID="grid_Financial" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                    Width="100%" EnableModelValidation="True" CssClass="table table-bordered table-hover">
                                    <Columns>
                                        <asp:BoundField DataField="costTimeS" HeaderText="日期">
                                            <ItemStyle Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="costNum" HeaderText="凭单号">
                                            <ItemStyle Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="costSub" HeaderText="明细">
                                            <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="costCharge" HeaderText="金额(元)">
                                            <ItemStyle Width="10%" HorizontalAlign="left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="typeName" HeaderText="类别">
                                            <ItemStyle Width="10%" HorizontalAlign="left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="unitName" HeaderText="部门">
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="userName" HeaderText="录入人">
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有数据
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                    CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                    CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                    OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                    PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                    SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                                    PageSize="12" SubmitButtonClass="submitbtn">
                                </webdiyer:AspNetPager>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
