﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProSecondValueAllotAuditBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProSecondValueAllotAuditBymaster" %>

<%@ Register Src="../UserControl/ChooseProjectValueUser.ascx" TagName="ChooseProjectValueUser"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/AddExternalMember.ascx" TagName="AddExternalMember"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" /> 
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.cookie.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseProjectValueUser.js" type="text/javascript"></script>
    <script src="../js/UserControl/AddExternalMember.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/CommonAddProjectMember.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/ProSecondValueAllotAudit.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/ProjectSecondValueStatusDetail.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/ProValueAllotAuditDetail.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/HaveProjectValueAllotAuditDetail.js" type="text/javascript"></script>
    <script src="/js/ProjectValueandAllot/ProValueAllotAuditByMember.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/ProjectValueMeaaageSend.js" type="text/javascript"></script>
    <style type="text/css" media="print">
        .NoPrint {
            display: none;
        }
    </style>
    <style type="text/css">
        .display {
            display: none;
        } 
    </style>
    <script type="text/javascript" language="javascript">

        function openValueWindow() {
            var openUrl = "SecondPrintProjectValue.aspx?ProjectSysNo=" + $("#HiddenCoperationSysNo").val() + "&AllotId=" + $("#HiddenAllotID").val();

            var feature = "dialogWidth:960px;dialogHeight:550px;center:yes";

            window.open(openUrl, '打印', feature);

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目二次产值分配审核</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>项目二次产值分配审核</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>项目信息</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                            <tr>
                                                <td style="width: 15%;">项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">合同关联:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">审核级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">项目总负责:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">合同额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">二次分配可用金额:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblNotAllotAccount" runat="server"></asp:Label>元
                                                </td>

                                                <td style="width: 15%;">已分配金额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblAllotAccount" runat="server" Text=""></asp:Label>元
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td style="width: 15%;">二次分配总金额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblTotalCount" runat="server" Width="100px"></asp:Label>元
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">其他参与部门:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>审批信息</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--评审内容-->
                                    <asp:Literal ID="lblNotData" runat="server"></asp:Literal>
                                    <div id="MainDiv">
                                        <table style="width: 100%; height: auto;" class="cls_content_head">
                                            <tr>
                                                <td style="width: 10%; text-align: center;">评审部门
                                                </td>
                                                <td style="width: 80%; text-align: center;">评审内容
                                                </td>
                                                <td style="width: 10%; text-align: center;">评审人/日期
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="TableContainer">
                                            <!--生产经营部Table-->
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="OneTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">所长
                                                    </td>
                                                    <td style="width: 80%;">
                                                        <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                            <tr>
                                                                <td style="width: 20%;">项目分配年份：
                                                                </td>
                                                                <td style="width: 30%;" class="cls_ProjAllot_label">
                                                                    <asp:Label ID="lblYear" runat="server"></asp:Label>
                                                                    (年)
                                                                </td>
                                                                <td style="width: 20%;">项目等级：
                                                                </td>
                                                                <td style="width: 30%;" class="cls_ProjAllot_label">
                                                                    <asp:Label ID="lblType" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%;">项目分配阶段：
                                                                </td>
                                                                <td style="width: 30%;" class="cls_ProjAllot_label">
                                                                    <asp:Label ID="lblStage" runat="server" Text="Label"></asp:Label>
                                                                </td>
                                                                <td style="width: 20%;">分配金额：
                                                                </td>
                                                                <td style="width: 30%;" class="cls_ProjAllot_label">
                                                                    <asp:Label ID="txt_ActulCount" runat="server"></asp:Label>(元)
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <!--所长-->
                                            <asp:Literal ID="lblNotPass" runat="server"></asp:Literal>
                                            <table style="width: 100%; display: none;" class="cls_show_cst_jiben" id="TwoTable"
                                                audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">所长
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <textarea style="width: 98%; height: 60px;" id="TwoSuggstion" class="TextBoxBorder"></textarea>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <!--设总Table-->
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="FiveTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">设总
                                                    </td>
                                                    <td style="width: 80%;">
                                                        <asp:Literal ID="lbl_NoData" runat="server"></asp:Literal>
                                                        <div id="stagetablezero" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目各设计阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" style="width: 98%;">
                                                                <tr>
                                                                    <td width="10%" style="text-align: center;">项目类别
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">方案设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">初设设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("ProgramPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("preliminaryPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="Text5" maxlength="6" type="text" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stagetableone" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案+初步设计)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; height: 30px; text-align: center; background-color: #B8CCE4;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; height: 30px; text-align: center; background-color: #B8CCE4;">方案
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; height: 30px; text-align: center; background-color: #B8CCE4;">初设
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center; height: 30px; text-align: center; background-color: #B8CCE4;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("ProgramPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("preliminaryPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stagetabletwo" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; text-align: center;">施工图
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center; text-align: center;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stagetablethree" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(初步设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">初设设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("preliminaryPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                         <div id="stagetableten" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">方案设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvTen" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("ProgramPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageCOUNT">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" BackColor="#D2F3CB" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="AllotCount">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <asp:Literal ID="lbl_StageFive" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_stagespetable" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_Tab" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_out" runat="server"></asp:Literal>
                                                        <div id="stageamountOne" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目各设计阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td width="10%" style="text-align: center;">项目类别
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">方案设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">初设设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">施工图设计
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">后期服务
                                                                    </td>
                                                                    <td colspan="2" style="width: 18%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("ProgramPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("preliminaryPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent") %>
                                                                            </span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountTwo" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案+初步设计)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">方案比例
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">初设比例
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("ProgramPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("preliminaryPercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountThree" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">施工图
                                                                    </td>
                                                                    <td style="text-align: center; width: 30%;">后期服务
                                                                    </td>
                                                                    <td style="width: 30%; text-align: center;">闭合
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent") %>
                                                                            </span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountFour" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(初步设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">初设设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("preliminaryPercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <div id="stageamountTen" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="Table2" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">方案设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageTen" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("ProgramPercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                        <asp:Literal ID="lbl_SpeAmount" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_outAmount" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_ProcessAmount" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_processAmountFiveTable" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="SixTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">专业负责人
                                                    </td>
                                                    <td style="width: 80%;">
                                                        <asp:Literal ID="lbl_Member" runat="server"></asp:Literal>
                                                        <asp:Literal ID="lbl_MemberCount" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="SevenTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">所长
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <fieldset id="Fieldset1" style="text-align: left; margin: 0 auto;">
                                                            <legend style="font-size: 12px;">审批意见</legend>
                                                            <textarea style="width: 98%; height: 60px;" id="SevenSuggstion" class="TextBoxBorder"></textarea>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="EightTable" audittable="auditTable">
                                                <tr>
                                                    <td style="width: 10%; text-align: center;">生产经营部
                                                    </td>
                                                    <td style="width: 80%; text-align: center;">
                                                        <fieldset id="Fieldset2" style="text-align: left; margin: 0 auto;">
                                                            <legend style="font-size: 12px;">审批意见</legend>
                                                            <textarea style="width: 98%; height: 60px;" id="EightSuggstion" class="TextBoxBorder"></textarea>
                                                        </fieldset>
                                                    </td>
                                                    <td style="width: 10%; text-align: center;" id="AuditUser"></td>
                                                </tr>
                                            </table>
                                            <!--没有权限审核-->
                                            <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                                id="NoPowerTable">
                                                <tr>
                                                    <td style="width: 100%; text-align: center; height: 100px; line-height: 100px;">您没有权限审核该项
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table1">
                <tr>
                    <td style="width: 100%; text-align: center;">
                        <input type="button" id="btnApproval" name="controlBtn" class="btn green btn-default"
                            href="#AuditUserDiv" value="通过" />
                        <input type="button" id="btnRefuse" name="controlBtn" class="btn red btn-default"
                            value="不通过" />
                        <input type="button" id="Button1" name="controlBtn" class="btn  btn-default" value="返回"
                            onclick="javascript: history.back();" />
                        <input type="button" value="返回产值分配页面，重新添加分配" id="FallBackCoperaion" style="display: none;"
                            class="btn green btn-default" />
                        <input type="button" id="print" class="btn green btn-default" name="controlBtn" onclick="openWindow();"
                            style="display: none;" />
                        <input id="btnPrintValue" type="button" onclick="openValueWindow();" value="打印产值"
                            class="btn purple btn-default" style="display: none;" />
                        <input id="btnExportValue" type="button" class="btn green btn-default" onclick=""
                            value="导出产值" runat="server" onserverclick="btnExport_click" style="display: none;" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenSysMsgSysNo" value="0" />
    <input type="hidden" id="AuditRecordStatus" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=projectAuditRecordEntity ==null?0:projectAuditRecordEntity.SysNo %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenOneProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.OneSuggestion %>" />
    <input type="hidden" id="HiddenTwoProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.TwoSuggstion %>" />
    <input type="hidden" id="HiddenThreeProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.ThreeSuggsion %>" />
    <input type="hidden" id="HiddenFiveProposal" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.FiveSuggion  %>" />
    <input type="hidden" id="HiddenSevenSuggsion" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.SevenSuggsion  %>" />
    <input type="hidden" id="HiddenEightSuggstion" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.EightSuggstion  %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.AuditUserString %>" />
    <input type="hidden" id="HiddenAuditDate" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.AuditDate %>" />
    <input type="hidden" id="HiddenCoperationSysNo" value="<%=ProSysNo %>" />
    <input type="hidden" id="HiddenCoperationProcess" value="<%=CoperationProcess %>" />
    <input type="hidden" id="HiddenIsEdit" value="<%=IsParamterEdit %>" />
    <input type="hidden" id="HiddenAllotID" value="<%=projectAuditRecordEntity ==null?0:projectAuditRecordEntity.AllotID %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <input type="hidden" id="hiddenSpetityHead" value="<%=SpetailtyHeadUser %>" />
    <input type="hidden" id="hiddenSpeName" value="<%=SpeName %>" />
      <!--分配中的连接-->
    <input type="hidden" id="hidLikeType" value="<%=LikeType%>" />
    <!--是否四舍五入-->
    <input type="hidden" id="HiddenIsRounding" value="<%=IsRounding %>" />
    <!--专业审核人-->
    <input type="hidden" id="HiddenSpecialityAuditUser" value="<%=SpecialityAuditUser %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <!--选择用户-->
    <div id="chooseUserMainDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加人员</h4>
        </div>
        <div class="modal-body" id="chooseUserDiv">
            <div id="chooseUserMain">
                <uc1:ChooseProjectValueUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <%-- <div class="modal-footer">
            <button type="button" id="btn_UserMain" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>--%>
    </div>
    <div id="chooseExtUserDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加外聘人员</h4>
        </div>
        <div class="modal-body" id="chooseExtDiv">
            <div id="chooseExternalUserDiv">
                <uc2:AddExternalMember ID="chooseExternalUser1" runat="server" />
            </div>
        </div>
        <%-- <div class="modal-footer">
            <button type="button" id="btnChooseExt" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>--%>
    </div>
    <div id="AuditUserDiv" class="modal fade yellow" tabindex="-1" data-width="490" aria-hidden="true"
        style="display: none; width: 490px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_close">
                关闭</button>
        </div>
    </div>
</asp:Content>
