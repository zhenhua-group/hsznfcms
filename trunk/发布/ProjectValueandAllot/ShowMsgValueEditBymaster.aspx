﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ShowMsgValueEditBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ShowMsgValueEditBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../../css/CprChargStatus.css" rel="stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <script src="../../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../../js/MessageComm.js"></script>
    <script type="text/javascript" src="/js/jquery.alerts.js"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            var reson = $("#Hiddenreson").val();
            if (reson.length > 1) {
                $("#btnApproval").hide();
            }

            $("#btnApproval").click(function () {
                var url = $("#hiddenProUrl").val();
                window.location.href = "ProValueAllotAuditBymaster.aspx?" + url;
            });
            $("#btnRefuse").click(function () {
                window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx";
            });
        })
        //http:/ProjectValueandAllot/ProValueAllotAudit.aspx?ValueAllotAuditSysNo=169&MessageStatus=H&MsgNo=14374
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        项目信息管理 <small>特殊申请修改产值系数消息结果</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>特殊申请修改产值系数消息结果</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>产值系数特殊申请</font></font></div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover" style="margin: 0 auto;" id="td_input">
                                <tr>
                                    <td style="width: 15%">
                                        项目名称：
                                    </td>
                                    <td>
                                        <asp:Label Text="" runat="server" ID="proname" />
                                    </td>
                                    <td style="width: 15%">
                                        承接部门：
                                    </td>
                                    <td>
                                        <asp:Label Text="" runat="server" ID="CJBM" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        是否通过：
                                    </td>
                                    <td colspan="3" class="TextBoxBorder">
                                        <asp:Label Text="" runat="server" ID="labNOPass" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <input type="button" id="btnApproval" name="controlBtn" class="btn blue" value="产值分配" />
                                        <input type="button" id="btnRefuse" name="controlBtn" class="btn default" value="返回" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hiddenProUrl" value="<%=ProUrl%>" />
    <input type="hidden" id="Hiddenreson" value="<%=Reson%>" />
</asp:Content>
