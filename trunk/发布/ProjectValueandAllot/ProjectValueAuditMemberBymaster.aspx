﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectValueAuditMemberBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProjectValueAuditMemberBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/ProjectValueAuditMemberBymaster.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>个人产值分配明细确认</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">产值分配</a><i
        class="fa fa-angle-right"> </i><a href="#">个人产值分配明细确认</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" style="width: 98%;" align="center" id="tb_project">
                                <tr>
                                    <td style="width: 15%;">项目名称:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%;">合同关联:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">管理级别:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%;">审核级别:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">项目类别:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%;">建设规模:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">承接部门:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%;">项目总负责:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">项目阶段:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%;">其他部门参与:
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>个人产值信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <span style="color: Red; font-weight: bold;">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span>

                            <asp:Literal ID="lbl_Detail" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset id="Fieldset4" style="text-align: left; margin: 0 auto;">
                                <legend style="font-size: 12px;">个人确认意见</legend>
                                <textarea style="width: 98%; height: 60px; margin: 0 auto;" id="AuditSuggsion" class=" form-control" runat="Server"></textarea>
                            </fieldset>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table style="width: 100%; height: auto; border: none;" class="cls_show_cst_jiben" id="Table1">
                                <tr>
                                    <td style="width: 100%; text-align: center; border: none;">
                                        <input type="button" id="btnApproval" name="controlBtn" href="#AuditUserDiv" class="btn green "
                                            value="同意" />
                                        <input type="button" id="btnRefuse" name="controlBtn" class="btn red"
                                            value="不同意" />
                                        <button type="button" class="btn default" onclick="javascript:history.back();">
                                            返回</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <input type="hidden" value="<%=UserSysNo %>" id="hid_user">
    <input type="hidden" value="<%=MessageID %>" id="msgno">
    <input type="hidden" value="<%=Pro_ID %>" id="hid_proID">
    <input type="hidden" value="<%=AllotID %>" id="hid_AllotID">
    <input type="hidden" value="<%=ValueAllotAuditSysNo %>" id="HiddenAuditRecordSysNo">
    <input type="hidden" value="<%=IsDone %>" id="hid_IsDone">
    <!--产值类型-->
    <input type="hidden" value="<%=proType %>" id="hid_proType" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <div id="AuditUserDiv" class="modal fade yellow" tabindex="-1" data-width="490" aria-hidden="true"
        style="display: none; width: 490px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_close">
                关闭</button>
        </div>
    </div>
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>

</asp:Content>
