﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="TranBulidingProjectValueAllotBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.TranBulidingProjectValueAllotBymaster" %>

<%@ Register Src="../UserControl/ChooseProjectValueUser.ascx" TagName="ChooseProjectValueUser"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/AddExternalMember.ascx" TagName="AddExternalMember"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControl/ChooseUser.ascx" TagName="ChooseUser" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseProjectValueUser.js" type="text/javascript"></script>
    <script src="../js/UserControl/AddExternalMember.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/TranBulidingProjectValueAllot.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/TranBulidingAuditUserDetail.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/TranBulidingProValueAllotAuditDetail.js"
        type="text/javascript"></script>
    <style type="text/css">
        .display {
            display: none;
        }

        /* 表格基本样式*/
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .cls_show_cst_jiben td {
                border: solid 1px #CCC;
                font-size: 12px;
                font-family: "微软雅黑";
            }

            .cls_show_cst_jiben tr {
                height: 22px;
            }

        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 20px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>转土建所产值分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>转土建所产值分配</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">项目信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 15%;">项目名称:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同关联:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">管理级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">审核级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目类别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">建设规模:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">承接部门:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">项目总负责:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目阶段:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">其他参与部门:
                                        </td>
                                        <td style="width: 35%;" colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目分配年份:
                                        </td>
                                        <td style="width: 35%; text-align: center">
                                            <asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" Width="220px"
                                                CssClass="form-control input-sm">
                                                <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 15%;">转土建所金额:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotalCount" runat="server" Text=""></asp:Label>
                                            (元)
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>转土建所项目分配表填写</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="margin-top: 5px;" class="cls_data">
                                    <table class="cls_ProjAllot_Table" style="width: 100%;" id="tbTranJjs">
                                        <tr>
                                            <td style="width: 15%;">项目分配阶段：
                                            </td>
                                            <td style="width: 35%; text-align: center">
                                                <select id="stage" runat="server" class="form-control input-sm" style="width: 90%;">
                                                    <option value="-1">----------请选择分配阶段----------</option>
                                                    <option value="0">方案+初设+施工图+后期</option>
                                                    <option value="1">方案+初设</option>
                                                    <option value="10">方案+施工图+后期</option>
                                                    <option value="2">施工图+后期</option>
                                                    <option value="3">初设+施工图+后期</option>
                                                    <option value="4">室外工程</option>
                                                    <option value="5">锅炉房(4吨以下)</option>
                                                    <option value="5">锅炉房(6吨以上)</option>
                                                    <option value="6">地上单建水泵房</option>
                                                    <option value="7">地上单建变配电所(室)</option>
                                                    <option value="8">单建地下室（车库）</option>
                                                    <%-- <option value="9">市政道路工程</option>--%>
                                                </select>
                                            </td>
                                            <td style="width: 15%;">项目分配金额：
                                            </td>
                                            <td style="width: 35%;" class="cls_ProjAllot_Input">
                                                <asp:Label ID="lblAllotCount" runat="server" Text="Label"></asp:Label>
                                                (元)
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllotHead" width="100%">
                                        <tr>
                                            <td align="center" style="width: 15%;">序号
                                            </td>
                                            <td align="center" style="width: 35%;">内容
                                            </td>
                                            <td align="center" style="width: 15%;">比例
                                            </td>
                                            <td align="center" style="width: 35%;">产值
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="100%"
                                        id="tbInfo">
                                        <tr>
                                            <td style="width: 15%;">1
                                            </td>
                                            <td style="width: 35%;">设总
                                                <input id="designUserName" type="text" href="#chooseUserOtherDiv" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.DesignUserName.ToString() %>" />
                                                <input type="hidden" id="designUserID" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.DesignUserID.ToString() %>" />
                                            </td>
                                            <td style="width: 15%;">
                                                <input id="txt_DesignManagerPercent" maxlength="15" type="text" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.DesignManagerPercent.ToString()%>" />(%)
                                            </td>
                                            <td style="width: 35%;">
                                                <span id="txt_DesignManagerCount">
                                                    <%=ProjectValueAllot == null ? "" : ProjectValueAllot.DesignManagerCount.ToString()%></span>(元)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">2
                                            </td>
                                            <td style="width: 35%;">本部门自留产值
                                            </td>
                                            <td style="width: 15%;">
                                                <input id="txtTheDeptValuePercent" maxlength="8" type="text" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.Thedeptallotpercent.ToString() %>" />(%)
                                            </td>
                                            <td style="width: 35%;">
                                                <span id="txtTheDeptValueCount">
                                                    <%=ProjectValueAllot ==null?"":ProjectValueAllot.Thedeptallotcount.ToString() %></span>(元)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3
                                            </td>
                                            <td>应分产值
                                            </td>
                                            <td>
                                                <input id="txt_ShouldBeValuePercent" maxlength="15" type="text" disabled="disabled"
                                                    value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.AllotPercent.ToString() %>" />(%)
                                            </td>
                                            <td>
                                                <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>(元)
                                                <%--                        <span id="txt_ShouldBeValueCount" runat="server">0.00</span>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="tbOneSave">
                                    <tr>
                                        <td style="width: 100%; text-align: center;">
                                            <input type="button" id="btnApprovalOne" name="controlBtn" class="btn green" value="确定" />
                                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                                返回</button>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Literal ID="lbl_NoData" runat="server"></asp:Literal>
                                <div id="stagetablezero" class="cls_Container_Report" style="display: none;">
                                    <table class="cls_ProjAllot_Table" style="width: 98%;">
                                        <tr>
                                            <td>项目各设计阶段产值分配比例%
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllotHead" style="width: 98%;">
                                        <tr>
                                            <td width="10%" style="text-align: center;">项目类别
                                            </td>
                                            <td colspan="2" style="width: 18%; text-align: center;">方案设计
                                            </td>
                                            <td colspan="2" style="width: 18%; text-align: center;">初设设计
                                            </td>
                                            <td colspan="2" style="width: 18%; text-align: center;">施工图设计
                                            </td>
                                            <td colspan="2" style="width: 18%; text-align: center;">后期服务
                                            </td>
                                            <td colspan="2" style="width: 18%; text-align: center;">合计
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="gvOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                        CssClass="cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2" Width="98%">
                                        <Columns>
                                            <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="Text5" maxlength="6" type="text" value='<%# Eval("ProgramPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="9%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ProgramCOUNT">
                                                <ItemStyle Width="9%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="Text5" maxlength="6" type="text" value='<%# Eval("preliminaryPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="9%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="preliminaryCOUNT">
                                                <ItemStyle Width="9%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="Text5" maxlength="6" type="text" value='<%# Eval("WorkDrawPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="9%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="WorkDrawCOUNT">
                                                <ItemStyle Width="9%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="Text5" maxlength="6" type="text" value='<%# Eval("LateStagePercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="9%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LateStageCOUNT">
                                                <ItemStyle Width="9%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <span>100</span>%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="9%" BackColor="#D2F3CB" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AllotCount">
                                                <ItemStyle Width="9%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            没有信息!
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <div id="stagetableone" class="cls_Container_Report" style="display: none;">
                                    <table class="cls_ProjAllot_Table" style="width: 98%;">
                                        <tr>
                                            <td>项目(方案+初步设计)二阶段产值分配比例%
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                        <tr>
                                            <td style="text-align: center; width: 10%; height: 30px; text-align: center; background-color: #B8CCE4;">项目类别
                                            </td>
                                            <td style="text-align: center; width: 30%; height: 30px; text-align: center; background-color: #B8CCE4;">方案
                                            </td>
                                            <td style="text-align: center; width: 30%; height: 30px; text-align: center; background-color: #B8CCE4;">初设
                                            </td>
                                            <td style="width: 30%; text-align: center; height: 30px; text-align: center; background-color: #B8CCE4;">闭合
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="gvTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                        CssClass="cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2" Width="98%">
                                        <Columns>
                                            <asp:BoundField DataField="ItemType">
                                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("ProgramPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ProgramCOUNT">
                                                <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("preliminaryPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="preliminaryCOUNT">
                                                <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <span>100</span>%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="15%" BackColor="#D2F3CB" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AllotCount">
                                                <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            没有信息!
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <div id="stagetabletwo" class="cls_Container_Report" style="display: none;">
                                    <table class="cls_ProjAllot_Table" style="width: 98%;">
                                        <tr>
                                            <td>项目(施工图设计+后期服务)二阶段产值分配比例%
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllotHead" style="width: 98%;">
                                        <tr>
                                            <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                            </td>
                                            <td style="text-align: center; width: 30%; text-align: center;">施工图
                                            </td>
                                            <td style="text-align: center; width: 30%; text-align: center;">后期服务
                                            </td>
                                            <td style="width: 30%; text-align: center; text-align: center;">闭合
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="gvThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                        Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                        <Columns>
                                            <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="WorkDrawCOUNT">
                                                <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LateStageCOUNT">
                                                <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <span>100</span>%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="15%" BackColor="#D2F3CB" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AllotCount">
                                                <ItemStyle Width="15%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            没有信息!
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <div id="stagetablethree" class="cls_Container_Report" style="display: none;">
                                    <table class="cls_ProjAllot_Table" style="width: 98%;">
                                        <tr>
                                            <td>项目(初步设计+施工图设计+后期服务)二阶段产值分配比例%
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                        <tr>
                                            <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                            </td>
                                            <td style="text-align: center; width: 22.5%; text-align: center;">初设设计
                                            </td>
                                            <td style="text-align: center; width: 22.5%; text-align: center;">施工图设计
                                            </td>
                                            <td style="text-align: center; width: 22.5%; text-align: center;">后期服务
                                            </td>
                                            <td style="width: 22.5%; text-align: center; text-align: center;">合计
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="gvFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                        Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                        <Columns>
                                            <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("preliminaryPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="preliminaryCOUNT">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="WorkDrawCOUNT">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LateStageCOUNT">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <span>100</span>%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" BackColor="#D2F3CB" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AllotCount">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            没有信息!
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <div id="stagetableten" class="cls_Container_Report" style="display: none;">
                                    <table class="cls_ProjAllot_Table" style="width: 98%;">
                                        <tr>
                                            <td>项目(方案设计+施工图设计+后期服务)二阶段产值分配比例%
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllotHead" style="width: 98%;">
                                        <tr>
                                            <td style="text-align: center; width: 10%; text-align: center;">项目类别
                                            </td>
                                            <td style="text-align: center; width: 22.5%; text-align: center;">方案设计
                                            </td>
                                            <td style="text-align: center; width: 22.5%; text-align: center;">施工图设计
                                            </td>
                                            <td style="text-align: center; width: 22.5%; text-align: center;">后期服务
                                            </td>
                                            <td style="width: 22.5%; text-align: center; text-align: center;">合计
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="gvTen" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                        Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                        <Columns>
                                            <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("ProgramPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ProgramCOUNT">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("WorkDrawPercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="WorkDrawCOUNT">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <input id="txtbulidingPercent" maxlength="6" type="text" runat="server" value='<%# Eval("LateStagePercent") %>' />%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LateStageCOUNT">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <span>100</span>%
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="11.25%" BackColor="#D2F3CB" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AllotCount">
                                                <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            没有信息!
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                                <asp:Literal ID="lbl_StageFive" runat="server"></asp:Literal>
                                <asp:Literal ID="lbl_stagespetable" runat="server"></asp:Literal>
                                <asp:Literal ID="lbl_Tab" runat="server"></asp:Literal>
                                <asp:Literal ID="lbl_out" runat="server"></asp:Literal>
                                <asp:Literal ID="lblMember" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--保存按钮-->
            <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="tbTwoSave">
                <tr>
                    <td style="width: 100%; text-align: center;">
                        <input type="button" id="btn_Up" name="controlBtn" class="btn green"
                            value="上一步" />
                        <input type="button" id="btnApprovalTwo" name="controlBtn" class="btn green" href="#AuditUserDiv"
                            value="确定" />
                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                            返回</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!--弹出层-->
    <div id="chooseUserOtherDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加人员</h4>
        </div>
        <div class="modal-body" id="chooseUserotherDiv">
            <uc3:ChooseUser ID="ChooseUser2" runat="server" />
        </div>
        <div class="modal-footer">
            <button type="button" id="Button1" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_user_close">
                关闭</button>
        </div>
    </div>
    <div id="chooseUserMainDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加人员</h4>
        </div>
        <div class="modal-body" id="chooseUserDiv">
            <div id="chooseProjectValueUserMain">
                <uc1:ChooseProjectValueUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <%-- <div class="modal-footer">
            <button type="button" id="btn_UserMain" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>--%>
    </div>
    <div id="chooseExtUserDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加外聘人员</h4>
        </div>
        <div class="modal-body" id="chooseExtDiv">
            <div id="chooseExternalUserDiv">
                <uc2:AddExternalMember ID="chooseExternalUser1" runat="server" />
            </div>
        </div>
        <%-- <div class="modal-footer">
            <button type="button" id="btnChooseExt" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>--%>
    </div>
    <div id="AuditUserDiv" class="modal fade yellow" tabindex="-1" data-width="490" aria-hidden="true"
        style="display: none; width: 490px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--是否四舍五入-->
    <input type="hidden" id="HiddenIsRounding" value="<%=IsRounding %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenProSysNo" value="<%=proSysNo %>" />
    <input type="hidden" id="HiddenAllotID" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.ID.ToString() %>" />
    <input type="hidden" id="HiddenAllotUser" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.AllotUser.ToString() %>" />
    <input type="hidden" id="HiddenTranAllotID" value="<%=allotID %>" />
    <input type="hidden" id="HiddenIsEdit" value="<%=IsParamterEdit %>" />
    <input type="hidden" id="HiddenCoperationProcess" value="<%=CoperationProcess %>" />
    <input type="hidden" id="HiddenUnitName" value="<%=UnitName %>" />
    <!--选择消息接收着-->
</asp:Content>
