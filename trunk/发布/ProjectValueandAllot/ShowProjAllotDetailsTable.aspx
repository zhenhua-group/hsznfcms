﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowProjAllotDetailsTable.aspx.cs"
    Inherits="TG.Web.ProjectValueandAllot.ShowProjAllotDetailsTable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ShowProjAllotDetailsTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjStructCss4.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript" src="../js/ProjectValueandAllot/ShowProjAllotTable.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[项目分配表预览]
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
