﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="InterviewManage.aspx.cs" Inherits="TG.Web.Interview.InterviewManage" %>

<%@ Register Src="../UserControl/UserOfTheDepartmentTree.ascx" TagName="UserOfTheDepartmentTree"
    TagPrefix="uc1" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>    
    
    <script src="../js/Interview/InterviewManage.js"></script>

    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>


    <script type="text/javascript">
        var cprid = '<%=GetCoperationID() %>';
        //用户ID
        var userid = '<%=GetCurMemID() %>';
        if (userid == "0") {
            window.parent.parent.document.location.href = "../index.html";
        }
        var swfu;
        function ActionPic(memid, param) {

            var divid = "divFileProgressContainer" + param;
            var spanid = "spanButtonPlaceholder" + param;

            //附件高清图
            swfu = new SWFUpload({

                upload_url: "../ProcessUpload/upload_Interview.aspx?type=interview&id=" + cprid + "&userid=" + userid,
                flash_url: "../js/swfupload/swfupload.swf",
                post_params: {
                    "ASPSESSID": "<%=Session.SessionID %>"
                },
                file_size_limit: "10 MB",
                file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                file_types_description: "上传",
                file_upload_limit: "0",
                file_queue_limit: "1",

                //Events
                file_queued_handler: fileQueued,
                file_queue_error_handler: fileQueueError,
                file_dialog_complete_handler: fileDialogComplete,
                upload_progress_handler: uploadProgress,
                upload_error_handler: uploadError,
                upload_success_handler: uploadSuccess,
                upload_complete_handler: uploadComplete,

                // Button
                button_placeholder_id: spanid,
                button_image_url: "../images/swfupload/XPButtonNoText_61x22.png",
                button_width: 61,
                button_height: 22,
                button_text: '<span class="btnFile">选择文件</span>',
                button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 11pt;background-color:Black; } ',
                button_text_top_padding: 1,
                button_text_left_padding: 5,

                custom_settings: {
                    upload_target: divid
                },
                debug: false
            });

        }




    </script>

    <style type="text/css">
        .mycenter, #ctl00_ContentPlaceHolder1_grid_mem {
            text-align: center;
        }

        .cs, .cs1, .cs2 {
            display: none;
        }

        td {
            vertical-align: middle !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">面试管理 <small>面试管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>面试管理</a><i
        class="fa fa-angle-right"> </i><a>面试管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>查询面试信息
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="tbl_id">
                        <tr>
                            <td>部门名称:
                            </td>
                            <td>
                                <asp:DropDownList ID="Department" Width="120px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----请选择-----</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                            <td>姓名:
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="Interview_Name"></asp:TextBox>
                            </td>
                            <td>面试时间:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>面试结果</td>
                            <td>
                                <asp:DropDownList ID="drp_state" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="成功">全部</asp:ListItem>
                                    <asp:ListItem Value="成功">成功</asp:ListItem>
                                    <asp:ListItem Value="失败">失败</asp:ListItem>
                                    <asp:ListItem Value="入库">入库</asp:ListItem>
                                </asp:DropDownList>
                            </td>

                            <td>&nbsp;
                                     <asp:Button ID="btn_search" class="btn blue" type="button" runat="server" OnClick="Button1_Click" Text="查询" />
                                <%--   <input type="button" class="btn blue" value="查询" id="btn_search" runat="server" OnClick="btn_Search_Click"/>--%>
                                <input type="button" class="btn blue " id="btn_add" value="添加面试记录" href="#addTraining" data-toggle="modal" />
                                <input type="button" class="btn blue" value="培训统计" id="StaticTraining" style="display: none" />
                                <%--<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />--%>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
            <div class="cls_data" id="div_edit" style="display: none;">
                <div class="portlet box blue">
                    <div class="portlet-title ">
                        <div class="caption">
                            <i class="fa fa-user"></i>编辑面试记录
                        </div>
                        <div class="tools">
                            <a class="collapse" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-bordered">
                            <tr>
                                <td>部门名称:</td>
                                <td colspan="3">
                                    <asp:DropDownList ID="drp_Interviewunitedit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>姓名:</td>
                                <td colspan="3">
                                    <input id="txt_internameedit" />
                                    <span style="color: red; display: none;" id="Span1">请输入姓名!</span>
                                </td>
                            </tr>
                            <tr>
                                <td>面试时间:</td>
                                <td colspan="3">
                                    <input type="text" name="txt_date" id="txt_interdateedit" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                                    <span style="color: red; display: none;" id="Span2">请输入面试时间!</span>
                                </td>

                                <td>面试结果:</td>
                                <td colspan="3">
                                    <select id="txt_interresultsedit">
                                        <option value="成功">成功</option>
                                        <option value="失败">失败</option>
                                        <option value="失败">入库</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>面试备注:</td>
                                <td colspan="6">
                                    <textarea id="txt_interremarkedit" style="width: 100%"></textarea>

                                </td>
                            </tr>
                            <tr>
                                <td>上传附件:
                                </td>
                                <td colspan="6">
                                    <asp:TextBox ID="txt_pic0" runat="server" CssClass="cls_input_text" Width="300px" ReadOnly="true"> </asp:TextBox>
                                    <span class="btn default btn-sm"><span id="spanButtonPlaceholder0"></span></span><span style="color: red">(注:一个人只能上传一个附件)</span><br />
                                    <div id="divFileProgressContainer0">
                                    </div>
                                    <asp:HiddenField ID="txt_pic0_s" runat="server"></asp:HiddenField>
                                </td>
                            </tr>


                        </table>

                        <button type="button" class="btn red btn-default" id="btn_Interviewedit">
                            确定</button>
                        <input type="button" class="btn btn-default" value="取消" id="btn_Cancle" />

                    </div>

                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>面试信息列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_Interview" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                HeaderStyle-HorizontalAlign="Center">

                                <Columns>


                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter cs1" />

                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" />
                                        </HeaderTemplate>

                                        <ItemStyle Width="5%" CssClass="cs1" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:BoundField DataField="interview_Id" HeaderText="id">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter cs" />
                                        <ItemStyle Width="5%" CssClass="cs" />
                                    </asp:BoundField>


                                    <asp:BoundField DataField="Interview_Unit" HeaderText="部门名称">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="bmmc" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Interview_Name" HeaderText="姓名">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="xm" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Interview_Date" DataFormatString="{0:d}" HeaderText="面试时间">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="mssj" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Interview_results" HeaderText="面试结果">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="msjg" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Interview_Remark" HeaderText="备注">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="10%" CssClass="bz" />
                                    </asp:BoundField>


                                    <asp:BoundField DataField="FileName" HeaderText="附件">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="10%" CssClass="fj" />
                                    </asp:BoundField>


                                    <asp:TemplateField HeaderText="附件操作" ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <a href='../Attach_User/filedata/cprfile/<%#Eval("FileUrl") %>' data-toggle="modal" target='_blank'>查看</a>
                                        </ItemTemplate>
                                        <ItemStyle Width="3%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="操作" ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <%-- <a href="#editinterview" class="cls_selectedit" data-toggle="modal">编辑</a>--%>
                                            <a href="javascript:void(0);" class="cls_selectedit">编辑</a>

                                        </ItemTemplate>
                                        <ItemStyle Width="3%" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText=" " ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <a href="#" class="cls_selectDelete" data-toggle="modal">删除</a>
                                        </ItemTemplate>
                                        <ItemStyle Width="3%" />
                                    </asp:TemplateField>




                                </Columns>
                            </asp:GridView>



                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px; height:23px;"
                                PageSize="10" SubmitButtonClass="btn green">
                            </webdiyer:AspNetPager>
                            <asp:HiddenField ID="hid_where" runat="server" Value="" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>




    <!--添加面试-->
    <div id="addTraining" class="modal fade yellow"
        tabindex="-1" data-width="660" aria-hidden="true" style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加面试
            </h4>
        </div>
        <div class="modal-body">
            <div id="addTrainingDiv" style="color: #222222;">
                <table class="table table-bordered">
                    <tr>
                        <td>部门名称:</td>
                        <td colspan="3">
                            <asp:DropDownList ID="drp_Interviewunit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>姓名:</td>
                        <td colspan="3">
                            <input id="title" />
                            <span style="color: red; display: none;" id="txt_title_valide">请输入姓名!</span>
                        </td>
                    </tr>
                    <tr>
                        <td>面试时间:</td>
                        <td colspan="3">
                            <input type="text" name="txt_date" id="BeginDate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                            <span style="color: red; display: none;" id="txt_BeginDate_valide">请输入面试时间!</span>
                        </td>

                        <td>面试结果:</td>
                        <td colspan="3">
                            <select id="select1">
                                <option value="成功">成功</option>
                                <option value="失败">失败</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>面试备注:</td>
                        <td colspan="6">
                            <textarea id="Remark" style="width: 100%"></textarea>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">上传附件:
                        </td>
                        <td colspan="6">
                            <asp:TextBox ID="txt_pic" runat="server" CssClass="cls_input_text" Width="300px" ReadOnly="true"> </asp:TextBox>
                            <span class="btn default btn-sm"><span id="spanButtonPlaceholder"></span></span><span style="color: red;">(注:一个人只能上传一个附件)</span><br />
                            <div id="divFileProgressContainer">
                            </div>
                            <asp:HiddenField ID="txt_pic_s" runat="server"></asp:HiddenField>
                        </td>
                    </tr>


                </table>

                <div class="row" style="display: none">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-condensed flip-contentr" id="datas_att" style="width: 98%;" align="center">
                                    <thead class="flip-content">
                                        <tr id="att_row">
                                            <td style="width: 40px;" align="center" id="att_id">序号
                                            </td>
                                            <td style="width: 300px;" align="center" id="att_filename">文件名称
                                            </td>
                                            <td style="width: 80px" align="center" id="att_filesize">文件大小
                                            </td>
                                            <td style="width: 80px;" align="center" id="att_filetype">文件类型
                                            </td>
                                            <td style="width: 120px;" align="center" id="att_uptime">上传时间
                                            </td>
                                            <td style="width: 40px;" align="center" id="att_oper">&nbsp;
                                            </td>
                                            <td style="width: 40px;" align="center" id="att_oper2">&nbsp;
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn red btn-default" id="btn_addsure">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                取消</button>
        </div>
    </div>



    <div id="editinterview" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true" style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">编辑面试
            </h4>
        </div>
        <div class="modal-body">
            <div id="editsTrainingDiv" style="color: #222222;">
            </div>
        </div>

    </div>


    <input type="hidden" id="selectedid" />
    <asp:HiddenField ID="hid_cprid" runat="server" />
</asp:Content>
