﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="Sys_MemTravel.aspx.cs" Inherits="TG.Web.SystemSet.Sys_MemTravel" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery.alerts.js"></script>

    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="/js/SystemSet/Sys_MemLevel.js" type="text/javascript"></script>


    <style type="text/css">
        .mycenter {
            text-algin: center;
        }

        #ctl00$ContentPlaceHolder1$AspNetPager1_input {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>机构设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>机构设置</a><i class="fa fa-angle-right"> </i><a>出国考察游管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>出国考察游管理
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <table class="table-responsive">
                        <tr>

                            <td>年份:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="form-control" ID="drp_year" runat="server"
                                    AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drp_year_SelectedIndexChanged">
                                     <asp:ListItem Value="2011">2011</asp:ListItem>
                                    <asp:ListItem Value="2012">2012</asp:ListItem>
                                    <asp:ListItem Value="2013">2013</asp:ListItem>
                                    <asp:ListItem Value="2014">2014</asp:ListItem>
                                    <asp:ListItem Value="2015">2015</asp:ListItem>
                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <a href="Sys_MemTravel.aspx" class="btn btn-sm blue">出国旅游资格单</a>
                            </td>
                            <td>
                                <a href="Sys_MemTravelTwo.aspx" class="btn btn-sm blue">出国旅游安排计算表</a>
                            </td>
                            <td>
                                <a href="Sys_MemTravelThree.aspx" class="btn btn-sm blue">出国旅游汇总表</a>
                            </td>
                            <td></td>
                        </tr>
                    </table>

                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>人员信息列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dataTable" id="tbData">
                                <thead>
                                    <tr>
                                        <th style="width: 120px;">级别</th>
                                        <th style="width: 80px;">人数</th>
                                        <th style="width: 80px;">年/次</th>
                                        <th style="width: 80px;">次/年</th>
                                        <th style="width: 80px;">人次</th>
                                        <th>人员名单</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td rowspan="2">一级股东</td>
                                        <td>1</td>
                                        <td></td>
                                        <td>1.50</td>
                                        <td>1.50</td>
                                        <td>张兵</td>
                                    </tr>
                                    <% bool isfirst = true;
                                       int sumuser = 1;
                                       decimal sumcount = 1.5m;
                                       this.ResultList.ForEach(m =>
                                      {
                                          sumuser = sumuser + m.MemCount;
                                          sumcount = sumcount + Convert.ToDecimal(m.MemCountTime3);
                                           %>
                                       
                                    <tr>
                                        <% if (isfirst)
                                           {
                                               isfirst = false;%>

                                        <%}
                                           else
                                           {%>
                                        <td><%= m.Mem_Level %></td>
                                        <%} %>
                                        <td><%= m.MemCount %></td>
                                        <td><%= m.MemCountTime %></td>
                                        <td><%= m.MemCountTime2 %></td>
                                        <td><%= m.MemCountTime3 %></td>
                                        <% string memStr = "";

                                           m.MemList.ForEach(c =>
                                           {
                                               memStr += c.mem_Name + ",";
                                           });
                                        %>
                                        <td><%=memStr %></td>

                                    </tr>
                                    <%  }); %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <Td>合计</Td>
                                        <Td><%=sumuser %></Td>
                                        <td></td>
                                        <td></td>
                                        <td><%=sumcount %></td>
                                        <td></td>
                                    </tr>
                                </tfoot>

                            </table>



                          <%--  <table>
                                <tr>
                                    <td rowspan="2"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>--%>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>



    <input type="hidden" name="" value="0" id="hidUserSysNo" />
</asp:Content>
