﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectNumConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ProjectNumConfigBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../js/SystemSet/ProjectNumConfig.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>工程号生成配置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用管理</a><i class="fa fa-angle-right"> </i><a>工程号生成配置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>项目类型</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <tr>
                                        <td>
                                            民用建筑：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工业建筑：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix0" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark0" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum0" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum0" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程勘察：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix1" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark1" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum1" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum1" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程监理：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix2" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark2" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum2" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum2" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程施工：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix3" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark3" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum3" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum3" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程咨询：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix4" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark4" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum4" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum4" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程总承包：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix5" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark5" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum5" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum5" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程代建：
                                        </td>
                                        <td>
                                            前缀：<asp:TextBox ID="txt_Prefix6" runat="server" CssClass="cls_input_text" ToolTip="prefix"
                                                Width="100px"></asp:TextBox>
                                        </td>
                                        <td>
                                            间隔符：<asp:TextBox ID="txt_mark6" runat="server" CssClass="cls_input_text" ToolTip="mark"
                                                Width="30px">-</asp:TextBox>
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum6" runat="server" CssClass="cls_input_text" ToolTip="start"
                                                Width="50px">1</asp:TextBox>
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum6" runat="server" CssClass="cls_input_text" ToolTip="end"
                                                Width="50px">100</asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="5">
                                            <asp:Button ID="btn_save" runat="server" CssClass="btn blue" OnClick="btn_save_Click"
                                                Text="确定" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
