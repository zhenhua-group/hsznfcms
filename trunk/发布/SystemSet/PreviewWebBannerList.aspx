﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewWebBannerList.aspx.cs"
    Inherits="TG.Web.SystemSet.PreviewWebBannerList" %>

<!DOCTYPE html>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../js/SystemSet/MsgComm.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;当前位置：[公告管理]
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head">
            <tr>
                <td align="center" style="width: 5%;">
                    <input id="chk_all" type="checkbox" />
                </td>
                <td align="center" style="width: 60%;">
                    公告标题
                </td>
                <td align="center" style="width: 15%;">
                    发布时间
                </td>
                <td align="center" style="width: 5%;">
                    发布用户
                </td>
                <td align="center" style="width: 5%;">
                    查看
                </td>
            </tr>
        </table>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowHeader="False"
            CssClass="gridView_comm" Width="100%" OnRowDataBound="GridView1_RowDataBound"
            EnableModelValidation="True">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" CssClass="cls_chk" />
                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("SysNo") %>' />
                    </ItemTemplate>
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="Title" HeaderText="标题">
                    <ItemStyle HorizontalAlign="Left" Width="60%" />
                </asp:BoundField>
                <asp:BoundField DataField="InDate" HeaderText="时间">
                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                </asp:BoundField>
                <asp:BoundField DataField="inUser" HeaderText="用户">
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href='<%#"ShowMsgView.aspx?type=0&itemSysNo"+Eval("WebBannerItemSysNo")+"&sysno="+Eval("SysNo") %>'
                            target="_self">查看</a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
            CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
            OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
            PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
            SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
            PageSize="30" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
    </div>
    </form>
</body>
</html>
