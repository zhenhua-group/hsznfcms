﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePwd.aspx.cs" Inherits="TG.Web.SystemSet.ChangePwd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //表单变色
            $(".cls_show_cst_jiben tr:odd").attr("style", "background-color:#FFF");
            //修改按钮事件
            $("#btn_save").click(function () {
                var msg = "";
                if ($("#txt_old").val() == "") {
                    msg += "旧密码不能为空！<br/>";
                }
                if ($("#txt_new").val() == "") {
                    msg += "新密码不能为空！<br/>";
                }
                if ($("#txt_new1").val() != $("#txt_new").val()) {
                    msg += "两次密码输入不一致！";
                }
                if (msg != "") {
                    jAlert(msg, "提示");
                    return false;
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "../HttpHandler/LoginHandler.ashx?action=chgpwd",
                        data: "oldpwd=" + escape($("#txt_old").val()) + "&newpwd=" + escape($("#txt_new").val()),
                        success: function (rlt) {
                            if (rlt == "yes") {
                                jAlert("密码修改成功！", "提示");
                                $("#txt_old").val("");
                                $("#txt_new").val("");
                                $("#txt_new1").val("");
                            }
                            else if (rlt == "no") {
                                jAlert("原始密码不正确！", "提示");
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            jAlert("数据库连接错误！", "提示");
                        }
                    });
                }

            });
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[修改密码]
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td style="width: 120px;">
                原始密码：
            </td>
            <td>
                <asp:TextBox ID="txt_old" runat="server" CssClass="cls_input_text" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                新密码：
            </td>
            <td>
                <asp:TextBox ID="txt_new" runat="server" CssClass="cls_input_text" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                重复新密码：
            </td>
            <td>
                <asp:TextBox ID="txt_new1" runat="server" CssClass="cls_input_text" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <input id="btn_save" type="image" src="../Images/buttons/btn_ok.gif" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
