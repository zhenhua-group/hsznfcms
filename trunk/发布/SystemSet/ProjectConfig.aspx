﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectConfig.aspx.cs"
	Inherits="TG.Web.SystemSet.ProjectConfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>无标题页</title>
	<link type="text/css" href="../../css/m_comm.css" rel="stylesheet" />
	<link href="../../css/Corperation.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>

	<script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>

	<script type="text/javascript">
	    $(function () {
	        CommonControl.SetFormWidth();
			$("#SubmitButton").click(function(){
				if($("#FacultyFileUoLoadButton").val().length <=0){
					alert("请选择文件！");
					return false;
				}
				if($("#FacultyNameTextBox").val().length <=0){
					alert("请填写院系名称！");
					return false;
				}
				window.document.forms[0].submit();
			});
		});	
		
		
//		function Class1(name){
//			this.Name = name;
//			this.Introduction = function(){
//				alert(this.Name);
//			}
//		}
//		
//		function Class2(name){
//			Class1.call(this,name);
//		}
//		
//		var class2 = new Class2("Sago");
//		class2.Introduction();
		
		
	</script>

</head>
<body>
	<form id="form1" runat="server" enctype="multipart/form-data" action="ProjectConfig.aspx"
	method="post">
	<table class="cls_container">
		<tr>
			<td class="cls_head">
				&nbsp;&nbsp;当前位置：[项目配置文件上传]
			</td>
		</tr>
	</table>
	<fieldset style="height: auto;">
		<legend>项目审核流程配置 </legend>
		<table class="cls_show_cst_jiben" align="center" id="containerTable">
			<tr>
				<td style="width: 20%;">
					院系名称
				</td>
				<td style="width: 30%;">
					<input type="text" name="FacultyNameTextBox" style="width: 300px;" id="FacultyNameTextBox" />
				</td>
				<td style="width: 10%;">
					浏览文件
				</td>
				<td style="width: 40%;">
					<input type="file" name="FacultyFileUoLoadButton" id="FacultyFileUoLoadButton" />
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center">
					<input type="button" value="提交文件" id="SubmitButton" />
				</td>
			</tr>
		</table>
	</fieldset>
	</form>
</body>
</html>
