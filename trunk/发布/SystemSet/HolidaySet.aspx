﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="HolidaySet.aspx.cs" Inherits="TG.Web.SystemSet.HolidaySet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datepicker/css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-timepicker/compiled/timepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-colorpicker/css/colorpicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css">
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="/js/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="/js/assets/scripts/form-components.js"></script>
    <script src="/js/assets/scripts/search.js"></script>
    <script src="/js/assets/scripts/form-validation.js"></script>
    <script src="/js/assets/scripts/ui-extended-modals.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
     <script src="../js/SystemSet/holidayset.js"></script>
    <script type="text/javascript">

        $(function () {

            Search.init();

            //删除
            $("img[name=delete]").live("click", function () {
                if (confirm("确定删除？")) {
                    var id = $(this).parent().attr("sysno");
                    window.location.href = "HolidaySet.aspx?id=" + id;
                }
            });


        });

    </script>
    <style type="text/css">
        .kd {
            width: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>节假日设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>系统设置</a><i class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>节假日设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>节假日设置
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width">
                            <tr>

                                <td style="width: 60px;">年份：</td>
                                <td style="width: 80px;">
                                    <asp:DropDownList runat="server" ID="drpYear">
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                                </td>

                            </tr>
                        </table>
                        <div class="portlet box blue ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-user"></i>日期列表
                                </div>
                                <div class="actions">
                                </div>
                            </div>
                            <div class="portlet-body" style="display: block;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 10px;">
                                            <tr>
                                                <td style="width: 80px;">选择日期：</td>
                                                <td style="width: 200px;">
                                                    <div class="input-icon">
                                                        <i class="fa fa-calendar" style="margin: 7px 2px 4px 10px"></i>
                                                        <input id="date" name="date" class="form-control input-medium date-picker" runat="server" style="width: 180px !important;" readonly="true" type="text"
                                                            data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Button CssClass="btn blue" ID="btn_jjr" runat="server" Text="添加节假日" OnClick="btn_jjr_Click" />&nbsp;&nbsp;<asp:Button CssClass="btn blue" ID="btn_tsr" runat="server" Text="添加特殊工作日" OnClick="btn_tsr_Click" /></td>
                                            </tr>
                                        </table>
                                        <table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
                                            <tr>
                                                <td style="width: 100px;"><strong>节假日：</strong></td>
                                                <td>
                                                    <asp:Literal ID="ltl_jjr" runat="server"></asp:Literal></td>
                                            </tr>
                                            <tr>
                                                <td><strong>特殊工作日：</strong></td>
                                                <td>
                                                    <asp:Literal ID="ltl_tsr" runat="server"></asp:Literal></td>
                                            </tr>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>年假设置
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">部门:
                                </td>
                                <td style="width: 130px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 60px;">年份：</td>
                                <td style="width: 80px;">
                                    <asp:DropDownList runat="server" ID="drp_year3">
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <asp:Button CssClass="btn blue" ID="btn_cx" runat="server" Text="查询" OnClick="btn_cx_Click" />
                                </td>

                            </tr>
                        </table>
                        <div class="portlet box blue ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-user"></i>人员列表
                                </div>
                                <div class="actions">
                                    <input type="button" id="btn_save" class="btn btn-sm red" value="保存" />
                                </div>
                            </div>
                            <div class="portlet-body" style="display: block;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="grid_mem" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover dataTable" Width="100%" HeaderStyle-HorizontalAlign="Center" EnableModelValidation="True" OnRowDataBound="grid_mem_RowDataBound">
                                            <Columns>

                                                <asp:BoundField DataField="mem_ID" HeaderText="编号">
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                                    <ItemStyle Width="30px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="mem_Name" HeaderText="姓名">
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                                    <ItemStyle Width="50px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="年假天数">
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="days" runat="server" CssClass="kd" Text=""  />                                                       
                                                    </ItemTemplate>
                                                    
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
