﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ImportBasicGongZi.aspx.cs" Inherits="TG.Web.SystemSet.ImportBasicGongZi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(function () {
            var tempRandom = Math.random() + new Date().getMilliseconds();
            $("#<%=btnImport.ClientID%>").click(function () {
                var name = $("#<%=FileUpload.ClientID%>").val();
                if (name == '') {
                    alert("请选择上传的文件");
                    return false;
                }
                var extend = name.substring(name.lastIndexOf('.') + 1);

                if (extend == "xls" || extend == "xlsx") {

                }
                else {
                    alert("请上传excel的文件格式");
                    return false;
                }

            });
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>人员基本工资导入</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>系统设置</a><i class="fa fa-angle-right"> </i><a>人员基本工资导入</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>工资导入
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">项目部门:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server" AppendDataBoundItems="True"
                                        OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 60px;">年份：</td>
                                <td style="width: 80px;">
                                    <asp:DropDownList runat="server" ID="drpYear">
                                        <asp:ListItem Text="2015" Value="2015" />
                                        <asp:ListItem Text="2016" Value="2016" />
                                        <asp:ListItem Text="2017" Value="2017" />
                                        <asp:ListItem Text="2018" Value="2018" />
                                        <asp:ListItem Text="2019" Value="2019" />
                                        <asp:ListItem Text="2020" Value="2020" />
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 60px;">月份：</td>
                                <td style="width: 80px;">
                                    <asp:DropDownList runat="server" ID="drpMonth">
                                        <asp:ListItem Text="1" Value="1" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />
                                        <asp:ListItem Text="4" Value="4" />
                                        <asp:ListItem Text="5" Value="5" />
                                        <asp:ListItem Text="6" Value="6" />
                                        <asp:ListItem Text="7" Value="7" />
                                        <asp:ListItem Text="8" Value="8" />
                                        <asp:ListItem Text="9" Value="9" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:Button Text="查询" runat="server" ID="btnSearch" CssClass="btn btn-sm red" OnClick="btnSearch_Click" />
                                </td>
                                <td></td>
                            </tr>
                        </table>

                        <table class="table table-hover table-bordered table-full-width" id="tbData">
                            <tr>
                                <td style="width: 100px;">选择数据文件：</td>
                                <td style="width: 150px;">
                                    <asp:FileUpload ID="FileUpload" runat="server" Width="100%" /></td>
                                <td>
                                    <asp:Button Text="导入" runat="server" ID="btnImport" CssClass="btn btn-sm red" OnClick="btnImport_Click" /></td>
                            </tr>
                        </table>


                        <table class="table table-hover table-bordered table-full-width">
                            <thead>
                                <tr>
                                    <th style="width: 80px;">真实姓名</th>
                                    <th style="width: 80px;">发放年份</th>
                                    <th style="width: 80px;">发放月份</th>
                                    <th style="width: 80px;">基本工资</th>
                                    <th style="width: 80px;">岗位工资</th>
                                    <th style="width: 80px;">预发奖金</th>
                                    <th style="width: 80px;">岗位津贴</th>
                                    <th style="width: 80px;">交通,通讯</th>
                                    <th style="width: 80px;">加班补助</th>
                                    <th style="width: 80px;">车补差补</th>
                                    <th style="width: 80px;">缺勤扣款</th>
                                    <th style="width: 80px;">应发工资</th>
                                    <th style="width: 80px;">代扣社保</th>
                                    <th style="width: 80px;">实扣税金</th>
                                    <th style="width: 80px;">实发工资</th>
                                    <th style="width: 80px;">中心工资</th>
                                    <th style="width: 80px;">本所工资</th>
                                    <th style="width: 80px;">公司成本</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <% if (MemsWagesList != null && MemsWagesList.Count > 0)
                                   {

                                       MemsWagesList.ForEach(c =>
                                       { %>
                                <tr>
                                    <td><%=c.MemName %></td>
                                    <td><%=c.SendYear %></td>
                                    <td><%=c.SendMonth %></td>
                                    <td><%=c.jbgz %></td>
                                    <td><%=c.gwgz %></td>
                                    <td><%=c.yfjj %></td>
                                    <td><%=c.gwjt %></td>
                                    <td><%=c.jtbt %></td>
                                    <td><%=c.jbbz %></td>
                                    <td><%=c.cbcb %></td>
                                    <td><%=c.qkkk %></td>
                                    <td><%=c.yfgz %></td>
                                    <td><%=c.dksb %></td>
                                    <td><%=c.sksj %></td>
                                    <td><%=c.sfgz %></td>
                                    <td><%=c.zxgz %></td>
                                    <td><%=c.qita %></td>
                                    <td><%=c.gscb %></td>
                                    <td></td>
                                </tr>
                                <%});
                                   } %>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
