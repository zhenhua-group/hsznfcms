﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="Sys_UnitListByMaster.aspx.cs" Inherits="TG.Web.SystemSet.Sys_UnitListByMaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/SystemSet/Sys_UnitList.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>部门管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>机构设置</a><i class="fa fa-angle-right"> </i><a>部门管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title ">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>部门搜索
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">

                    <table class="table-responsive">
                        <tr>
                            <td>部门名称:
                            </td>
                            <td>
                                <input class="form-control input-sm" type="text" id="txt_keyname" runat="Server" />
                            </td>
                            <td>&nbsp;
                                    <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                            </td>
                            <td>
                                <button class="btn blue" type="button" data-toggle="modal" id="btn_showadd" style="display: none;">添加</button>
                                <a class="btn blue" data-toggle="modal" href="#full2">添加</a>
                            </td>
                            <td>
                                <asp:Button CssClass="btn blue" ID="btn_DelCst" runat="server" Text="删除" OnClick="btn_DelCst_Click" />
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>信息列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_unit" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                EnableModelValidation="True" OnRowDataBound="grid_unit_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="unit_ID" HeaderText="编号">
                                        <ItemStyle HorizontalAlign="Left" Width="40px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="部门名称">
                                        <ItemTemplate>
                                            <img src="../Images/part.png" style="width: 16px; height: 14px;" />
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("unit_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="上级部门">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("unit_ParentID") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_prtid" runat="server" Value='<%# Bind("unit_ParentID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="部门列表">
                                        <ItemTemplate>
                                            <a href="###" class="cls_unitshow" id="EditShow" alt='<%# Eval("unit_ID") %>'>
                                                <asp:Label ID="IsShow" runat="Server" Text='<%# Eval("unit_ID") %>'></asp:Label></a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="unit_intro" HeaderText="部门描述">
                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="unit_Type" HeaderText="部门类型">
                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="unit_Order" HeaderText="排序">
                                        <ItemStyle HorizontalAlign="Left" Width="60px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="选择">
                                        <ItemTemplate>
                                            <a class="cls_select" data-toggle="modal" href="#full">编辑</a>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate></ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条" CustomInfoTextAlign="Left"
                                FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
                                PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
                                ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
                                TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="18" SubmitButtonClass="submitbtn">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                    <asp:HiddenField ID="hid_where" runat="server" Value="" />

                    <%--<input type="hidden" name="name" value="<%= UnitString %>" />--%>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <!----编辑部门---->

    <div class="modal fade" id="full" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">编辑部门</h4>
                </div>
                <div class="modal-body">

                    <div class="cls_data" id="div_edit" style="display: block;">

                        <table class="table table-hover table-bordered">
                            <tbody>
                                <tr>
                                    <td style="width: 120px;">部门ID：</td>
                                    <td style="width: 200px;">
                                        <asp:Label ID="lbl_unitid" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hid_unitid" runat="server" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">上级部门：</td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList Width="120px" ID="drp_unitlist0"
                                            runat="server" AppendDataBoundItems="True">
                                            <asp:ListItem Value="0">上级部门</asp:ListItem>
                                        </asp:DropDownList>

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">部门名称：</td>
                                    <td style="width: 200px;">
                                        <asp:TextBox ID="txt_unitName0" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">部门类型：</td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList runat="server" ID="drpUnitType1">
                                            <asp:ListItem Text="施工图部门" Value="0" />
                                            <asp:ListItem Text="方案部门" Value="1" />
                                            <asp:ListItem Text="特殊部门(设计)" Value="2" />
                                            <asp:ListItem Text="特殊部门(行政)" Value="3" />
                                            <asp:ListItem Text="其他部门" Value="4" />
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">部门描述：</td>
                                    <td style="width: 200px;">
                                        <asp:TextBox ID="txt_unitInfo0" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">是否显示：</td>
                                    <td style="width: 200px;">
                                        <asp:RadioButton ID="raBtnshow" runat="server" Text="显示" GroupName="show" />
                                        <asp:RadioButton ID="raBtnHidd" runat="server" Text="隐藏" GroupName="show" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">排序：</td>
                                    <td style="width: 200px;">
                                        <asp:TextBox ID="txt_Order2" runat="server" CssClass="form-control input-sm" Width="170px"></asp:TextBox>

                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="btn_edit" runat="server" CssClass="btn green" Text="保存" OnClick="btn_edit_Click" />
                                        <input type="button" name="btn_cancel" value="取消" class="btn btn_cancel" style="display: none;" />
                                        <button type="button" class="btn default" data-dismiss="modal">取消</button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- 添加部门--->





    <div class="modal fade" id="full2" tabindex="-1" role="basic2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">添加部门</h4>
                </div>
                <div class="modal-body">

                    <%--<div class="cls_data" id="div_add" style="display: block">--%>

                        <table class="table table-hover table-bordered">
                            <tbody>
                                <tr>
                                    <td style="width: 120px;">上级部门：</td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList Width="120px" ID="drp_unitlist" runat="server"
                                            AppendDataBoundItems="True">
                                            <asp:ListItem Value="0">上级部门</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">部门名称：</td>
                                    <td style="width: 200px;">
                                        <asp:TextBox ID="txt_unitName" runat="server" CssClass="form-control input-sm" Width="170px"></asp:TextBox>

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">部门类型：</td>
                                    <td style="width: 200px;">
                                        <asp:DropDownList runat="server" ID="drpUnitType">
                                            <asp:ListItem Text="施工图部门" Value="0" />
                                            <asp:ListItem Text="方案部门" Value="1" />
                                            <asp:ListItem Text="特殊部门(设计)" Value="2" />
                                            <asp:ListItem Text="特殊部门(行政)" Value="3" />
                                            <asp:ListItem Text="其他部门" Value="4" />
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">部门描述：</td>
                                    <td style="width: 200px;">
                                        <asp:TextBox ID="txt_unitInfo" runat="server" CssClass="form-control input-sm" Width="170px"></asp:TextBox>

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 120px;">排序：</td>
                                    <td style="width: 200px;">
                                        <asp:TextBox ID="txt_Order" runat="server" CssClass="form-control input-sm" Width="170px"></asp:TextBox>

                                    </td>
                                    <td></td>
                                </tr>

                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="btn_save" runat="server" CssClass="btn green" Text="保存" OnClick="btn_save_Click" />
                                        <input type="button" name="btn_cancel" value="取消" class="btn  btn_cancel" style="display: none;" />
                                        <button type="button" class="btn default" data-dismiss="modal">取消</button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    <%--</div>--%>
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>

</asp:Content>
