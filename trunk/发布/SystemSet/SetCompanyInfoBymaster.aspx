﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="SetCompanyInfoBymaster.aspx.cs" Inherits="TG.Web.SystemSet.SetCompanyInfoBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <meta content="text/html; charset=utf-8" http-equiv="content-type" />
    <link href="../js/ckeditor/contents.css" rel="stylesheet" type="text/css" />
    <script src="../js/assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    
    <style type="text/css">
        body, html
        {
            font-size: 9pt;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            CommonControl.SetFormWidth();
            CKEDITOR.replace('ContentTextBox');
            $("#SaveButton").click(function () {
                var insertcontent = CKEDITOR.instances.ContentTextBox.getData();
                if (insertcontent.length == 0) {
                    alert("请输入介绍内容！");
                    return false;
                }

                $.post("../HttpHandler/SetCompanyInfo.ashx", { action: "info", content: "" + insertcontent + "" }, function (data) {
                    if (data == "ok") {
                        alert("首页信息设置成功！");
                    }
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>院基本介绍</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>机构管理</a><i class="fa fa-angle-right"> </i><a>院基本介绍</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>院基本介绍</div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea id="ContentTextBox" name="S1" cols="30" rows="200" class="ckeditor form-control">
                             <%=TextContent%>
                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-offset-12 col-md-12">
                                        <input id="SaveButton" class="btn blue" type="button" value="保存" />
                                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                            返回</button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
