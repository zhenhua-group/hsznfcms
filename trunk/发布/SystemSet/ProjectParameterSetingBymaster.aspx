﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectParameterSetingBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ProjectParameterSetingBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.tablesort.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(function () {

            CommonControl.SetFormWidth();
            //Grid 高度指定
            $("#ctl00_ContentPlaceHolder1_gv_Coperation tr").hover(function () {
                $(this).addClass("tr_in");
            }, function () {
                $(this).removeClass("tr_in");
            });

            //隔行变色
            $("#ctl00_ContentPlaceHolder1_gv_Coperation tr:even").css({ background: "White" });


            $(".cls_btn_comm_w").click(function () {

                var id = $(this).parent().prev().text();
                var status = $(this).prev().val();

                var count = TG.Web.SystemSet.ProjectParameterSetingBymaster.UpdateProjectValueParamterIsEidt(id, status);

                if (count.value > 0) {
                    alert("修改成功");
                    window.location.href = "/SystemSet/ProjectParameterSetingBymaster.aspx";
                }
                else {
                    alert("修改失败");
                }

            });


            //列头排序
            $("#sorttable").tablesort({
                schbtn: $("#ctl00_ContentPlaceHolder1_btn_Search"),
                hidclm: $("#hid_column"),
                hidord: $("#hid_order")
            });
        });

    </script>

    <style type="text/css">
        .display
        {
            display: none;
        }
        .cls_content_head
        {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }
        .cls_content_head td
        {
            height: 34px;
            background-color: #E6E6E6;
            border: 1px solid Gray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>项目产值系数设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用管理</a><i class="fa fa-angle-right"> </i><a>项目产值系数设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>查询项目信息</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body " style="display: block;">
                    <table class="table-responsive">
                    	<tr>
                    		<td>生产部门:</td>
                            <td><asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                    </asp:DropDownList></td>
                            <td>年份:</td>
                            <td><asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                    </asp:DropDownList></td>
                            <td>工程名称:</td>
                            <td><input type="text" class="form-control input-sm" id="txt_keyname" runat="server" /></td>
                            <td><asp:Button ID="btn_Search" runat="server" OnClick="btn_Search_Click" CssClass="btn blue"
                                        Text="查询" /></td>
                    	</tr>
                    </table>
                 
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>项目信息列表</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="cls_data">
                                    <asp:GridView ID="gv_Coperation" runat="server" CellPadding="0" DataKeyNames="cpr_Id"
                                        AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" OnRowDataBound="gv_Coperation_RowDataBound"
                                        Font-Size="12px" RowStyle-Height="34px" Width="100%" CssClass="table table-bordered table-hover"
                                        ShowHeader="true">
                                        <RowStyle HorizontalAlign="Center" Height="34px"></RowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="序号">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# (Container.DataItemIndex+1).ToString() %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="cpr_No" HeaderText="合同编号" SortExpression="cpr_No" ItemStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" Width="9%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="工程名称" SortExpression="ProjectName">
                                                <ItemTemplate>
                                                    <img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px;">
                                                    <a href='/Coperation/cpr_ShowCopration.aspx?flag=cprlist&cprid=<%# Eval("cpr_Id") %>'
                                                        title='<%# Eval("cpr_Name") %>'>
                                                        <%# Eval("cpr_Name").ToString().Length > 16? Eval("cpr_Name").ToString().Substring(0, 16) + "..." : Eval("cpr_Name").ToString()%></a>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="24%" />
                                            </asp:TemplateField>
                                            
                                            <asp:BoundField DataField="BuildType" HeaderText="  建筑类别" SortExpression="BuildType"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BuildArea" HeaderText=" 建设规模(㎡)" SortExpression="BuildArea"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" Width="9%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cpr_Acount" HeaderText="  合同额(万元)" SortExpression="cpr_Acount"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cpr_Unit" HeaderText=" 承接部门" SortExpression="ReciveDepartment"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" Width="7%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cpr_SignDate" HeaderText=" 签订日期" SortExpression="SigneDate"
                                                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}">
                                                <ItemStyle HorizontalAlign="Center" Width="7%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cpr_DoneDate" HeaderText="完成日期" SortExpression="CompleteDate"
                                                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}">
                                                <ItemStyle HorizontalAlign="Center" Width="6%"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IsParamterEdit" HeaderStyle-CssClass="display">
                                                <ItemStyle CssClass="display" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cpr_Id"  HeaderStyle-CssClass="display">
                                                <ItemStyle CssClass="display" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText=" 系数是否可以编辑">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="selectID" runat="server">
                                                        <asp:ListItem Value="0">否</asp:ListItem>
                                                        <asp:ListItem Value="1">是</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <input type="button" name="controlBtn" class="cls_btn_comm_w" value="设置" />
                                                </ItemTemplate>
                                                <ItemStyle Width="15%" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                        CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                        CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                        OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                        PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                        SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                                        PageSize="30" SubmitButtonClass="submitbtn">
                                    </webdiyer:AspNetPager>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 排序指定 -->
    <asp:HiddenField ID="hid_column" runat="server" Value="cpr_Id" />
    <asp:HiddenField ID="hid_order" runat="server" Value="DESC" />
</asp:Content>
