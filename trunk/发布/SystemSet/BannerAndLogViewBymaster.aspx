﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="BannerAndLogViewBymaster.aspx.cs" Inherits="TG.Web.SystemSet.BannerAndLogViewBymaster"
    ValidateRequest="false" %>

<%@ Register Src="../UserControl/UserOfTheDepartmentTree.ascx" TagName="UserOfTheDepartmentTree"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <%--<link href="../css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_cpr.js"></script>
    <script src="../js/UserControl/UserOfTheDepartmentTree.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <link href="../js/ckeditor/contents.css" rel="stylesheet" type="text/css" />
    <%--<script src="../js/ckeditor/ckeditor.js" type="text/javascript"></script>--%>
    <%--<script src="../js/assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>--%>
    <%--<script src="../js/jquery-1.8.0.min.js"></script>--%>
    <script type="text/javascript" src="../js/xheditor/xheditor/xheditor.js"></script>

    <script type="text/javascript" src="../js/xheditor/xheditor/xheditor_lang/zh-cn.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#' + '<%=elm1.ClientID %>').xheditor({ upLinkUrl: "/js/xheditor/upload.aspx", upLinkExt: "zip,rar,txt", upImgUrl: "/js/xheditor/upload.aspx", upImgExt: "jpg,jpeg,gif,png", upFlashUrl: "/js/xheditor/upload.aspx", upFlashExt: "swf", upMediaUrl: "/js/xheditor/upload.aspx", upMediaExt: "avi" });
        });
    </script>
    <script type="text/javascript">
        $(function () {

            if ($('#HiddenAction').val() == '0') {
                $('.page_AddOrEdit').html('添加<%=PageTitle%>');

            } else { $('.page_AddOrEdit').html('编辑<%=PageTitle%>'); }

            //编辑时显示
            if ($('#HiddenAction').val() == '1') {
                $("#roleUserDiv").css("display", "block");
            }
            else {
                $("#roleUserDiv").css("display", "none");
            }

            //导航栏跳转
            $('.menu_Href').click(function () {
                if ($('#HiddenType').val() == '0') {
                    window.location = "WorkLogListBymaster.aspx";
                }
                else { window.location = "PubMsgListBymaster.aspx"; }
            });

            if ($("#HiddenType").val() == "1") {
                $('#webBannerTr').show();
                $('#addAttach').show();
            }

            //返回
            $("#btn_back").click(function () {
                if ($("#HiddenType").val() == "1") {
                    document.location.href = "PubMsgListBymaster.aspx";
                }
                else {
                    document.location.href = "WorkLogListBymaster.aspx";
                }
            });



            $("#SaveButton").click(function () {

                var tilteString = $.trim($("#TitleTextBox").val());
                var time = $("#txt_date").val();
                var contentString = $('#' + '<%=elm1.ClientID %>').val();

                if (tilteString.length == 0 || tilteString.length >= 100) {
                    alert("请确认标题必须小于100字符且不能为空！");
                    return false;
                }
                if (time.length == 0) {
                    alert("请选择提醒时间！");
                    return false;
                }
                if (contentString.length == 0) {
                    alert("请确认内容不能为空！");
                    return false;
                }

                var userItemArray = new Array();
                //是公告编辑的场合
                if ($("#HiddenType").val() == "1") {
                    var userSpans = $("span[id=userSpan]", $("#roleUserDiv"));

                    $.each(userSpans, function (index, item) {
                        userItemArray[index] =
                                                {
                                                    "SysNo": 0,
                                                    "WebBannerSysNo": $("#HiddenSysNo").val(),
                                                    "ToUser": $(item).attr("usersysno"),
                                                    "InDate": $("#txt_date").val(),
                                                    "UserName": " "
                                                };
                    });
                }

                var data = { "insertTitle": tilteString, "viewType": $("#HiddenType").val(), "insertContent": contentString, "SysNo": $("#HiddenSysNo").val(), "action": $("#HiddenAction").val(), "ItemList": Global.toJSON(userItemArray), "InDate": time, "tempSysNo": $("#hiddenTempWebBannerSysNo").val() };

                $.post("BannerAndLogViewBymaster.aspx", data, function (jsonResult) {
                    var urlString = "";
                    if ($("#HiddenType").val() == "0") {
                        urlString = "/SystemSet/WorkLogListBymaster.aspx";
                    } else {
                        urlString = "/SystemSet/PubMsgListBymaster.aspx";
                    }
                    if (parseInt(jsonResult, 10) > 0) {
                        if ($("#HiddenAction").val() == "0") {
                            alert("添加成功！");
                        } else {
                            alert("修改成功！");
                        }
                        window.location.href = urlString;
                    } else {
                        alert("保存失败，请重新登录！");
                    }
                });
            });
            //var userOfTheDepartmentTree = new UserOfTheDepartmentTree(null, $("#ChooseUserInDepartment"));
            //$(".AddUserButton").click(function () {
            //    userOfTheDepartmentTree 
            //    userOfTheDepartmentTree.Clear();
            //    $("#ChooseUserInDepartment").dialog({
            //        autoOpen: false,
            //        modal: true,
            //        width: 500,
            //        height: 400,
            //        resizable: false,
            //        buttons:
            //                {
            //                    "确定": function () {
            //                        //调用处理事件
            //                        userOfTheDepartmentTree.SaveUser(ChooseUserOfTheDepartmentCallBack);
            //                        $(this).dialog("close");
            //                    },
            //                    "取消": function () { $(this).dialog("close"); }
            //                }
            //    }).dialog("open");
            //    return false;
            //});

            $("#Btn_Save").live("click", function () {
                var userOfTheDepartmentTree = new UserOfTheDepartmentTree($("#ChooseUserInDepartment"));
                userOfTheDepartmentTree.SaveUser(ChooseUserOfTheDepartmentCallBack);
                //$("#close").click();
                $("#btn_Hide").click();
            });

            ////鼠标移动显示按
            //$("span[id=userSpan]").hover(function () {
            //    $("#deleteUserlinkButton", $(this)).show();
            //}, function () {
            //    $("#deleteUserlinkButton", $(this)).hide();
            //});
            $("span[id=userSpan]").live("mouseover", function () {
                $("#deleteUserlinkButton", $(this)).show();
            }).live("mouseout", function () {
                $("#deleteUserlinkButton", $(this)).hide();
            });

            $("img[id=deleteUserlinkButton]").live("click", function () {
                if (confirm("确认要删除这个用户吗？")) {
                    //删除用户
                    $(this).parent("span[id=userSpan]:first").remove();
                }
                return false;
            });

            $("a[id=deleteLinkButton]").live("click", function () {
                var sysno = parseInt($(this).attr("sysno"), 10);
                var count = TG.Web.SystemSet.BannerAndLogViewBymaster.DeleteAttach(sysno).value;
                if (parseInt(count, 10) > 0) {
                    $(this).parents("tr:first").remove();
                }
                return false;
            });
        });
        //从部门选择用户回调函数
        function ChooseUserOfTheDepartmentCallBack(userArray) {
            //获取Div下所有的用户Span
            var userSpans = $("span[id=userSpan]", $("#roleUserDiv"));
            $.each(userArray, function (index, user) {

                var flag = true;
                //去除重复
                for (var i = 0; i < userSpans.length; i++) {
                    if ($(userSpans[i]).attr("usersysno") == user.userSysNo) {
                        flag = false;
                        break;
                    }
                }
                if (flag == true) {
                    var userSpan = $("<span style=\"padding-right:10px;\" id=\"userSpan\" usersysno=\"" + user.userSysNo + "\">" + user.userName + "<img style=\"margin-left:5px;display:none;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
                    //添加完成 显示用户
                    $("#roleUserDiv").append(userSpan).show();
                }
            });
        }
    </script>
    <script type="text/javascript">
        var swfu;
        window.onload = function () {
            swfu = new SWFUpload({
                upload_url: "/ProcessUpload/upload_webbanner.aspx?webBannerSysNo=" + $("#hiddenTempWebBannerSysNo").val(),
                flash_url: "../js/swfupload/swfupload.swf",
                post_params: {
                    "ASPSESSID": "<%=Session.SessionID %>"
                },
                file_size_limit: "10 MB",
                file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                file_types_description: "文件资料上传",
                file_upload_limit: "0",
                file_queue_limit: "1",

                //Events
                file_queued_handler: fileQueued,
                file_queue_error_handler: fileQueueError,
                file_dialog_complete_handler: fileDialogComplete,
                upload_progress_handler: uploadProgress,
                upload_error_handler: uploadError,
                upload_success_handler: BannerUploadSuccess,
                upload_complete_handler: uploadComplete,

                // Button
                button_placeholder_id: "spanButtonPlaceholder",
                button_image_url: "../images/swfupload/XPButtonNoText_61x22.png",
                button_width: 61,
                button_height: 22,
                button_text: '<span class="btnFile">选择文件</span>',
                button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 11pt;background-color:Black; } ',
                button_text_top_padding: 1,
                button_text_left_padding: 5,

                custom_settings: {
                    upload_target: "divFileProgressContainer"
                },
                debug: false
            });
        }

        function BannerUploadSuccess(file, serverData) {
            try {
                var progress = new FileProgress(file, this.customSettings.upload_target);
                progress.setStatus("上传成功！");
                progress.toggleCancel(false);
                LoadAttach();
            } catch (ex) {
                this.debug(ex);
            }
        }

        function LoadAttach() {
            if ($("#HiddenAction").val() == "1" && $("#HiddenType").val() == "1") {
                $("#attachTableResult tr:gt(0)").remove();
            }
            else {
                $("#attachTableResult tr:gt(0)").remove();
            }

            var webBannerSysNo = $("#hiddenTempWebBannerSysNo").val();
            var attachListString = TG.Web.SystemSet.BannerAndLogViewBymaster.GetAttachList(webBannerSysNo).value;

            var attachList = eval("(" + attachListString + ")");

            $.each(attachList, function (index, attach) {
                var trString = "<tr>";
                trString += "<td align=\"center\">";
                trString += "<img style='width: 16px; height: 16px;' src=\"" + attach.IconPath + "\" />" + attach.FileName + "</td>";
                trString += "<td align=\"center\">" + attach.FileSize + "</td>";
                trString += "<td align=\"center\">" + attach.FileType + "</td>";
                trString += "<td align=\"center\">" + attach.UpLoadDateString + "</td>";
                trString += "<td align=\"center\"><a href=\"/SystemSet/DownLoadFile.aspx?fileName=" + attach.FileName + "&filePath=" + attach.RelativePath + " \" id=\"downLoadFileButton\">下载</a></td>";
                trString += "<td align=\"center\"><a href=\"#\" sysno=\"" + attach.SysNo + "\" id=\"deleteLinkButton\">删除</a></td>";
                trString += "</tr>";

                $("#attachTableResult").append(trString);
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">公告管理 <small>
        <%=PageTitle%>管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a href="javascript:void(0);"
        class="menu_Open">系统设置</a> <i class="fa fa-angle-right"></i><a href="javascript:;"
            class="menu_Open">公告管理</a> <i class="fa fa-angle-right"></i><a href="javascript:;"
                class="menu_Href">
                <%=PageTitle%>管理</a><i class="fa fa-angle-right"></i><a href="javascript:;" class="menu_Open page_AddOrEdit">添加</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i><span class="page_AddOrEdit"></span>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body " style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                        <tr>
                                            <td style="width: 13%;">
                                                <%=PageTitle%>标题:
                                            </td>
                                            <td>
                                                <input type="text" id="TitleTextBox" value="<%=PreViewTitle %>" class="form-control input-sm" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 13%;">提醒时间:
                                            </td>
                                            <td>
                                                <input id="txt_date" class="Wdate" type="text" onclick="WdatePicker({ readOnly: true })"
                                                    style="width: 150px; height: 30px; border: 1px solid #e5e5e5" value="<%=PreViewDateString%>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>消息内容:
                                            </td>
                                            <td>
                                                <%-- <textarea id="ContentTextBox" name="S1" cols="30" rows="200" class="form-control"><%=PreViewContent %></textarea>--%>
                                                <textarea id="elm1" runat="server" name="txtContent" rows="15" cols="80" style="width: 100%"><%=PreViewContent %></textarea>
                                            </td>
                                        </tr>
                                        <tr id="addAttach" style="display: none;">
                                            <td>附件:
                                            </td>
                                            <td>
                                                <table class="cls_content_head" width="100%" id="attachTableResult">
                                                    <tr id="Tr1">
                                                        <td style="width: 180px;" align="center" id="Td1">文件名称
                                                        </td>
                                                        <td style="width: 80px" align="center" id="Td2">文件大小
                                                        </td>
                                                        <td style="width: 80px;" align="center" id="Td3">文件类型
                                                        </td>
                                                        <td style="width: 120px;" align="center" id="Td4">上传时间
                                                        </td>
                                                        <td style="width: 40px;" align="center" id="Td5">&nbsp;
                                                        </td>
                                                        <td style="width: 40px;" align="center" id="Td6">&nbsp;
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="RepeaterAttach" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td align="center">
                                                                    <img style='width: 16px; height: 16px;' src="<%#Eval("IconPath") %>" /><%#Eval("FileName") %>
                                                                </td>
                                                                <td align="center">
                                                                    <%#Eval("FileSize") %>
                                                                </td>
                                                                <td align="center">
                                                                    <%#Eval("FileType") %>
                                                                </td>
                                                                <td align="center">
                                                                    <%#Eval("UpLoadDateString") %>
                                                                </td>
                                                                <td align="center">
                                                                    <a href="/SystemSet/DownLoadFile.aspx?fileName=<%#Eval("FileName") %>&filePath=<%#Eval("RelativePath") %>"
                                                                        target="_blank">下载</a>
                                                                </td>
                                                                <td align="center">
                                                                    <a href="#" sysno="<%#Eval("SysNo") %>" id="deleteLinkButton">删除</a>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                                <table class="cls_show_cst_jiben" style="" align="center">
                                                    <tr>
                                                        <td align="center">
                                                            <span id="spanButtonPlaceholder"></span>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <div id="divFileProgressContainer" style="height: 45px; width: 560px;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="webBannerTr" style="display: none;">
                                            <td>接收人员：
                                            </td>
                                            <td valign="top">
                                                <h3>
                                                    <div class="col-md-2">
                                                        人员
                                                    </div>
                                                    <div class="col-md-8">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="#responsive" data-toggle="modal" class="btn default AddUserButton">选择用户</a>
                                                    </div>
                                                </h3>
                                                <!--选择人员显示主Div-->
                                                <div style="width: 100%; height: 100px;" id="roleUserDiv">
                                                    <asp:Repeater ID="RepeaterUsers" runat="server">
                                                        <ItemTemplate>
                                                            <span style="margin-right: 10px;" id="userSpan" usersysno="<%#Eval("ToUser") %>">
                                                                <%#Eval("UserName") %><img style="margin-left: 5px; display: none; cursor: pointer;"
                                                                    id="deleteUserlinkButton" src="/Images/pro_icon_03.gif" /></span>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="form-group">
                                            <input type="button" value="保存" id="SaveButton" class="btn blue" />
                                            <input type="button" value="返回" id="btn_back" class="btn default" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenType" value="<%=Type%>" />
    <input type="hidden" id="HiddenSysNo" value="<%=SysNo%>" />
    <input type="hidden" id="HiddenAction" value="<%=Action%>" />
    <input type="hidden" id="hiddenTempWebBannerSysNo" value="<%=TempWebbanerSysNo%>" />
    <!--按部门选择用户-->
    <div id="responsive" class="modal fade" tabindex="-1" data-width="600">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择用户</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="ChooseUserInDepartment">
                        <div style="overflow: auto; height: 300px;">
                            <uc1:UserOfTheDepartmentTree ID="UserOfTheDepartmentTree1" runat="server" IsRadio="false" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_Hide" class="btn btn-default">
                关闭</button>
            <button type="button" class="btn
    blue"
                id="Btn_Save">
                保存</button>
        </div>
    </div>
</asp:Content>
