﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="JJSValueParameterConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.JJSValueParameterConfigBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/SystemSet/ProValueParameterConfig.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Global.js"></script>
    <script src="../js/SystemSet/JJSValueConfig.js" type="text/javascript"></script>
    <style type="text/css">
        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 20px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
                text-align: center;
            }

        .gridView_comm {
            margin: 0 auto;
            border-collapse: collapse;
            border-top: none;
        }

            .gridView_comm td {
                height: 18px;
                border-collapse: collapse;
                border: 1px solid Gray;
                border-top: none;
                text-align: center;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>经济所项目产值分配细则配置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用管理</a><i class="fa fa-angle-right"> </i><a>经济所项目产值分配细则配置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>经济所产值配置明细表</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                                <div id="divone">
                                    <table class="cls_content_head">
                                        <tr>
                                            <td colspan="11" align="center" style="background-color: White;">
                                                各专业编制、校对分配比例表(%)
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td rowspan="3" style="width: 30%">
                                                项目分类
                                            </td>
                                            <td colspan="8" style="width: 56%">
                                                编制
                                            </td>
                                            <td rowspan="3" style="width: 7%">
                                                校对
                                            </td>
                                            <td rowspan="3" style="width: 7%">
                                                编辑
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td rowspan="2">
                                                编制<br />
                                                合计
                                            </td>
                                            <td colspan="3">
                                                建筑
                                            </td>
                                            <td colspan="4">
                                                安装
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td style="width: 7%">
                                                建筑<br />
                                                合计
                                            </td>
                                            <td style="width: 7%">
                                                土建
                                            </td>
                                            <td style="width: 7%">
                                                结构
                                            </td>
                                            <td style="width: 7%">
                                                安装<br />
                                                合计
                                            </td>
                                            <td style="width: 7%">
                                                给排水
                                            </td>
                                            <td style="width: 7%">
                                                采暖
                                            </td>
                                            <td style="width: 7%">
                                                电气
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="gridView_comm" style="width: 100%" id="AddTable">
                                    </table>
                                </div>
                                <div id="EditJJSValue" style="width: 100%; display: none">
                                    <fieldset>
                                        <legend>
                                            <label id="title">
                                            </label>
                                        </legend>
                                        <table width="100%" style="text-align: left;">
                                            <tr>
                                                <td style="width: 70px;">
                                                    土建：
                                                </td>
                                                <td>
                                                    <input type="text" id="TJ" style="margin-left: 0px;" />%<span style="display: none;
                                                        color: Red" id="TJSpan"></span> 
                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    结构：
                                                </td>
                                                <td>
                                                    <input type="text" id="JG" style="margin-left: 0px;" />%<span style="display: none;
                                                        color: Red" id="JGSpan"></span> 
                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    给排水：
                                                </td>
                                                <td>
                                                    <input type="text" id="GPS" style="margin-left: 0px;" />%<span style="display: none;
                                                        color: Red" id="GPSSpan"></span> 
                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    采暖：
                                                </td>
                                                <td>
                                                    <input type="text" id="CN" style="margin-left: 0px;" />%<span style="display: none;
                                                        color: Red" id="CNSpan"></span> 
                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    电气：
                                                </td>
                                                <td>
                                                    <input type="text" id="DQ" style="margin-left: 0px;" />%<span style="display: none;
                                                        color: Red" id="DQSpan"></span> 
                                                        
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    校对：
                                                </td>
                                                <td>
                                                    <input type="text" id="JD" style="margin-left: 0px;" />%<span style="display: none;
                                                        color: Red" id="JDSpan"></span> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <input type="button" id="btnOK" name="controlBtn" class="btn blue" value="确定" />
                                                    <input type="button" id="btnCancl" name="controlBtn" class="btn default"" value="取消" />
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                                <div id="EditJJSValue2" style="width: 100%; display: none">
                                    <fieldset>
                                        <legend>
                                            <label id="title2">
                                            </label>
                                        </legend>
                                        <table width="100%" style="text-align: left;">
                                            <tr>
                                                <td style="width: 70px;">
                                                    建筑：
                                                </td>
                                                <td>
                                                    <input type="text" id="JZH" />%<span style="display: none; color: Red" id="JZHSpan"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    安装：
                                                </td>
                                                <td>
                                                    <input type="text" id="AZ" />%<span style="display: none; color: Red" id="AZSpan"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    校对：
                                                </td>
                                                <td>
                                                    <input type="text" id="JD2" />%<span style="display: none; color: Red" id="JD2Span"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <input type="button" id="btnOK2" name="controlBtn" class="btn blue btn-sm" value="确定" />
                                                    <input type="button" id="btnCancl2" name="controlBtn" class="btn default btn-sm" value="取消" />
                                                    <%-- <img src="../Images/buttons/btn_ok.gif" id="btnOK2" style="cursor: pointer; border: 0;">
                                <input type="button" name="name" value="取消" id="btnCancl2" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                    <input type="hidden" value="1" id="typestatus" />
                    </div>
            </div>
            </div>
        </div>
    
</asp:Content>
