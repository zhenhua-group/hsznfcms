﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="LeftMenuConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.LeftMenuConfigBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="../js/SystemSet/LeftMenuConfigBymaster.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(function () {

            var leftMenuConfig = new LeftMenuConfigBymaster($("#PopAreaLeftMenuDetail"));
            //角色详细
            $(".cls_showRole").toggle(function () {
                $(this).text("隐藏角色").next("div").show();
            }, function () {
                $(this).text("显示角色").next("div").hide();
            });
        });  //清空数据  
    </script>
    <style type="text/css">
        /*数据基本样式*/
        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 14px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 23px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
            }

        .gridView_comm {
            margin: 0 auto;
            border-collapse: collapse;
            border-top: none;
        }

            .gridView_comm td {
                height: 23px;
                border-collapse: collapse;
                border: 1px solid Gray;
                border-top: none;
            }

            .gridView_comm a, a:hover, a:visited, a:link, a:active {
                color: Blue;
                text-decoration: none;
            }

            .gridView_comm .cls_column {
                overflow: hidden;
                text-overflow: ellipsis;
                -o-text-overflow: ellipsis;
                white-space: nowrap;
            }

            .gridView_comm tr:hover {
                background-color: #CCC;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>功能权限设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>权限管理</a><i class="fa fa-angle-right"> </i><a>功能权限设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>选择功能模块
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <table class="table-responsive">
                        <tr>
                            <td width="20%">功能权限:
                            </td>
                            <td width="45%">
                                <asp:DropDownList ID="TypeDropDownList" runat="server" OnSelectedIndexChanged="TypeDropDownList_SelectedIndexChanged"
                                    AutoPostBack="true" CssClass=" form-control">
                                    <asp:ListItem Value="0">-----全部菜单-----</asp:ListItem>
                                    <asp:ListItem Value="1">合同</asp:ListItem>
                                    <asp:ListItem Value="2">首页</asp:ListItem>
                                    <asp:ListItem Value="3">客户</asp:ListItem>
                                    <asp:ListItem Value="4">项目</asp:ListItem>
                                    <asp:ListItem Value="9">绩效管理</asp:ListItem>
                                    <asp:ListItem Value="10">考勤管理</asp:ListItem>
                                    <asp:ListItem Value="5">项目展示</asp:ListItem>
                                    <asp:ListItem Value="8">技术质量</asp:ListItem>
                                    <asp:ListItem Value="6">领导驾驶舱</asp:ListItem>
                                    <asp:ListItem Value="7">系统设置</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="35%" align="center">
                                <button class="btn blue" type="button" id="AddLeftMenu" href="#EditMenuConfigDiv"
                                    data-toggle="modal">
                                    添加功能权限</button>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>功能菜单列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <asp:Repeater ID="RepeaterLeftMenu" runat="server">
                        <HeaderTemplate>
                            <table class="table table-striped table-hover table-bordered dataTable" id="GridView1">
                                <tr>
                                    <th style="width: 25%;" align="center">描述
                                    </th>
                                    <th style="width: 15%;" align="center">元素ID
                                    </th>
                                    <th style="width: 15%;" align="center">角色
                                    </th>
                                    <th style="width: 10%;" align="center">类型
                                    </th>
                                    <th style="width: 10%;" align="center">录入人
                                    </th>
                                    <th style="width: 15%;" align="center">录入时间
                                    </th>
                                    <th style="width: 10%;" align="center">操作
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="width: 25%;" align="left">&nbsp;<%#Eval("Description")%>
                                </td>
                                <td style="width: 15%;" align="left">
                                    <%#Eval("ElementID")%>
                                </td>
                                <td style="width: 15%;" align="center">
                                    <a href="###" class="cls_showRole">角色显示</a>
                                    <div style="display: none; text-align: left; width: 130px;">
                                        <%#Eval("RoleNameString")%>
                                    </div>
                                </td>
                                <td style="width: 10%;" align="center">
                                    <%#Eval("TypeString")%>
                                </td>
                                <td style="width: 10%;" align="center">
                                    <%#Eval("InUserName")%>
                                </td>
                                <td style="width: 15%;" align="center">
                                    <%#Eval("InDate")%>
                                </td>
                                <td style="width: 10%;" align="center">
                                    <a href="#EditMenuConfigDiv" data-toggle="modal" leftmenusysno="<%#Eval("SysNo")%>"
                                        id="editLeftMenuLinkButton">编辑</a> <a href="#" leftmenusysno="<%#Eval("SysNo")%>"
                                            id="deleteLeftMetnuLinkButton">删除</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
    <div id="EditMenuConfigDiv" class="modal fade yellow" tabindex="-1" data-width="660"
        aria-hidden="true" style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="PopAreaActionPowerDetail">
                        <div id="PopAreaLeftMenuDetail">
                            <table class="show_projectNumber table-bordered">
                                <tr>
                                    <td style="width: 120px">描述：
                                    </td>
                                    <td>
                                        <input type="text" id="DescriptionTextBox" class=" form-control" />
                                    </td>
                                    <td style="width: 120px;">元素ID：
                                    </td>
                                    <td>
                                        <input type="text" id="ElementIDTextBox" class=" form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>类型：
                                    </td>
                                    <td>
                                        <select id="typeDropDownList" class="form-control  input-small">
                                            <option value="Default" selected="selected">首页</option>
                                            <option value="Customer">客户</option>
                                            <option value="Coperation" >合同</option>
                                            <option value="Project">项目</option>
                                            <option value="JiXiao">绩效管理</option>
                                            <option value="KaoQin">考勤管理</option>
                                            <option value="ProjectShow">项目展示</option>
                                            <option value="ThechQu">技术质量</option>
                                            <option value="LeaderShip">领导驾驶舱</option>
                                            <option value="SystemConfig">系统设置</option>
                                        </select>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>角色：
                                    </td>
                                    <td colspan="3" style="width: 380px;">
                                        <asp:Repeater ID="RepeaterRole" runat="server">
                                            <ItemTemplate>
                                                <input type="checkbox" value="<%#Eval("SysNo") %>" name="roleCheckBox" /><%#Eval("RoleName")%>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_saveActionPower" data-dismiss="modal" class="btn green btn-default">
                保存</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
