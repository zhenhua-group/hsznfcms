﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="PreviewWebBannerListBymaster.aspx.cs" Inherits="TG.Web.SystemSet.PreviewWebBannerListBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />   
     <link href="../css/jquery.alerts.css" rel="stylesheet" />

    <script src="/js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
   
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //加载系统公告页面
            //ajax demo:
            // general settings
            $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
              '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                '<div class="progress progress-striped active">' +
                  '<div class="progress-bar" style="width: 100%;"></div>' +
                '</div>' +
              '</div>';

            //$.fn.modalmanager.defaults.resize = false;

            var $model = $("#ajax-modal");

            $("a[msgtitle]").live('click', function () {

                var msgsysno = $.trim($(this).attr("msgid"));

                $('body').modalmanager('loading');
                var url = "/mainpage/ShowContent.aspx?type=0&sysno=" + msgsysno + "&n=" + Math.random();
                $model.load(url, "", function () {
                    $model.modal();
                });
                //  }, 1000);

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页<small>更多公告</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a>
        <i class="fa fa-angle-right"></i><a href="javascript:;" class="menu_Open">公告</a><i class="fa fa-angle-right">
        </i><a href="javascript:;" class="menu_Open">更多公告</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>公告列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowHeader="true" CssClass="table table-striped table-bordered table-hover dataTable"
                        Width="100%" OnRowDataBound="GridView1_RowDataBound" EnableModelValidation="True">
                        <Columns>

                            <asp:BoundField DataField="Title" HeaderText="公告标题">
                                <ItemStyle HorizontalAlign="Left" Width="70%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="InDate" HeaderText="发布时间">
                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="inUser" HeaderText="发布用户">
                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="查看">
                                <ItemTemplate>
                                    <a href='javascript:void(0)' msgid="<%#Eval("SysNo") %>" msgtitle="<%#Eval("Title") %>" data-toggle="modal">查看</a>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                        CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                        CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                        OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                        PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                        SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                        PageSize="30" SubmitButtonClass="submitbtn">
                    </webdiyer:AspNetPager>
                </div>
            </div>
        </div>
    </div>
       <!--公告信息-->
    <div id="ajax-modal" class="modal fade" data-width="800" aria-hidden="true"
        style="display: none; width: 800px; margin-left: -379px; margin-top: -300px;"  tabindex="-1"></div>
</asp:Content>
