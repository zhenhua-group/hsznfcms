﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="Sys_dictionaryBymaster.aspx.cs" Inherits="TG.Web.SystemSet.Sys_dictionaryBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            //隐藏
            $(" .btn_cancel").click(function () {
                $("#div_add").hide("slow");
                $("#div_edit").hide("slow");
            });

            //全选和反选
            $('#ctl00_ContentPlaceHolder1_grid_unit_ctl01_chkAll').click(function () {

                var checks = $('#ctl00_ContentPlaceHolder1_grid_unit :checkbox');

                if ($('#ctl00_ContentPlaceHolder1_grid_unit_ctl01_chkAll').attr("checked") == "checked") {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.setAttribute("class", "checked");
                        checks[i].setAttribute("checked", "checked");
                    }
                }
                else {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.removeAttribute("class");
                        checks[i].removeAttribute("checked");
                    }
                }
            });

            //CommonControl.SetFormWidth();
            //行背景
            $("#ctl00_ContentPlaceHolder1_grid_unit tr").hover(function () {
                $(this).addClass("tr_in");
            }, function () {
                $(this).removeClass("tr_in");
            });
            //隔行变色
            $("#ctl00_ContentPlaceHolder1_grid_unit tr:even").css({ background: "White" });
            //添加
            $("#btn_showadd").click(function () {
                //window.scrollTo(0, document.body.scrollHeight);//移动到屏幕最下方
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#div_add").show("slow");
                $("#div_edit").hide("slow");
            });

            $(".cls_select").click(function () {
                $("#div_add").hide("slow");
                $("#div_edit").show("slow");
            });
            //添加
            $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
                var msg = "";

                if ($("#ctl00_ContentPlaceHolder1_txt_dicName").val() == "") {
                    msg += "字典名不能为空！<br/>";
                }
                else {
                    if ($("#ctl00_ContentPlaceHolder1_txt_dicType").val().length > 25) {
                        msg += "字典类型太长！<br/>";
                    }
                }
                if ($("#ctl00_ContentPlaceHolder1_txt_dicDescrip").val().length > 50) {
                    msg += "字典介绍过长！";
                }
                if (msg != "") {
                    jAlert(msg, "提示");
                    return false;
                }
            });
            //修改
            $("#ctl00_ContentPlaceHolder1_btn_edit").click(function () {
                var msg = "";
                if ($("#ctl00_ContentPlaceHolder1_txt_dicName0").val() == "") {
                    msg += "字典名不能为空！<br/>";
                }
                else {
                    if ($("#ctl00_ContentPlaceHolder1_txt_dicType0").val().length > 25) {
                        msg += "字典类型太长！<br/>";
                    }
                }
                if ($("#ctl00_ContentPlaceHolder1_txt_dicDescript").val().length > 50) {
                    msg += "字典介绍过长！";
                }
                if (msg != "") {
                    jAlert(msg, "提示");
                    return false;
                }
            });



            //选择信息
            $(".cls_select").click(function () {
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#ctl00_ContentPlaceHolder1_lbl_dicid").text($(this).parent().parent().find("TD").eq(1).text());
                $("#ctl00_ContentPlaceHolder1_hid_dicid").val($(this).parent().parent().find("TD").eq(1).text());
                $("#ctl00_ContentPlaceHolder1_txt_dicName0").val($(this).parent().parent().find("TD").eq(2).text());
                $("#ctl00_ContentPlaceHolder1_txt_dicType0").val($(this).parent().parent().find("TD").eq(3).text());
                $("#ctl00_ContentPlaceHolder1_txt_dicDescript").val($(this).parent().parent().find("TD").eq(4).text());
            });
            //删除是判断有无记录选中
            $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {
                if ($("#ctl00_ContentPlaceHolder1_grid_unit  :checkbox:checked").length == 0) {
                    jAlert("请选择要删除的字典！", "提示");
                    return false;
                }
                //判断是否要删除
                return confirm("是否要删除字典信息？");
            });

        });

        function scrollToBottom() {
            window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>字典设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>字典设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>字典搜索
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">

                            <table class="table-responsive">
                                <tr>
                                    <td>字典名称:
                                    </td>
                                    <td>
                                        <input id="txt_keyname" class="form-control input-sm" name="txt_keyname" type="text"
                                            runat="Server" />
                                    </td>
                                    <td>字典类型:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drp_typelist" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                            OnSelectedIndexChanged="drp_typelist_SelectedIndexChanged" CssClass="form-control">
                                            <asp:ListItem Value="-1">----请选择----</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>&nbsp;
                                        <asp:Button ID="btn_Search" Text="查询" runat="server" CssClass="btn blue" OnClick="btn_Search_Click" />
                                    </td>
                                    <td>
                                        <input id="btn_showadd" class="btn blue" type="button" name="name" value="添加" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btn_DelCst" runat="server" Text="删除" CssClass="btn blue" OnClick="btn_DelCst_Click" />
                                    </td>
                                </tr>
                            </table>
                        
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>字典详细
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_unit" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                            &nbsp;
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ID" HeaderText="编号">
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dic_name" HeaderText="字典名称">
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dic_type" HeaderText="字典类型">
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="dic_info" HeaderText="描述">
                                        <ItemStyle HorizontalAlign="Left" Width="25%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="编辑">
                                        <ItemTemplate>
                                            <a href="javascript:void();" class="cls_select">编辑</a>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                                PageSize="30" SubmitButtonClass="submitbtn">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_add" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>添加字典
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">
                                    <font>字典名称:</font>
                                </label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txt_dicName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="help-block"><font><font></font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">
                                    <font>字典类型:</font>
                                </label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txt_dicType" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="help-block"><font><font></font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">
                                    <font>描述:</font>
                                </label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txt_dicDescrip" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="help-block"><font><font></font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="col-md-offset-12 col-md-12">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn green" Text="保存" OnClick="btn_save_Click" />&nbsp;
                            <input type="button" name="btn_cancel" value="取消" class="btn   btn_cancel" />
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_edit" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>编辑字典
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">
                                    <font>ID:</font>
                                </label>
                            </div>
                            <div class="col-md-7">
                                <asp:Label ID="lbl_dicid" runat="server"></asp:Label>
                                <asp:HiddenField ID="hid_dicid" runat="server" />
                                <span class="help-block"><font><font></font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">
                                    <font>字典名称:</font>
                                </label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txt_dicName0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="help-block"><font><font></font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">
                                    <font>字典类型:</font>
                                </label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txt_dicType0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="help-block"><font><font></font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="control-label">
                                    <font>描述:</font>
                                </label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txt_dicDescript" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="help-block"><font><font></font></font></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <div class="col-md-offset-12 col-md-12">
                            <asp:Button ID="btn_edit" runat="server" CssClass="btn green" Text="保存" OnClick="btn_edit_Click" />&nbsp;
                            <input type="button" name="btn_cancel" value="取消" class="btn   btn_cancel" />
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
