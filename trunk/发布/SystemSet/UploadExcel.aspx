﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UploadExcel.aspx.cs" Inherits="TG.Web.SystemSet.UploadExcel" %>

<%@ Register Src="../Coperation/SelectUserControl.ascx" TagName="SelectUserControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datepicker/css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-timepicker/compiled/timepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-colorpicker/css/colorpicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css">
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
     <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
      <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="/js/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="/js/assets/scripts/form-components.js"></script>
    <script src="/js/assets/scripts/search.js"></script>
    <script src="/js/assets/scripts/form-validation.js"></script>
    <script src="/js/assets/scripts/ui-extended-modals.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/select2/select2_metro.css" />
    <script type="text/javascript" src="/js/assets/plugins/select2/select2.min.js"></script>
    <link href="/css/swfupload/default_cpr.css" rel="stylesheet" />
    <script src="/js/swfupload/swfupload.js"></script>
    <script src="/js/swfupload/handlers_proj.js"></script>
        <script src="../js/Coperation/SelectRoleUser.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/UserControl/EditRolePowerControl.js"></script>
    <script src="../js/SystemSet/uploadexcel.js"></script>
    <script type="text/javascript">
        var year, month;
        $(function () {


            year = $("#ctl00_ContentPlaceHolder1_drpYear").val();
            month = $("#ctl00_ContentPlaceHolder1_drp_month").val();


        });
        var swfu;
        window.onload = function () {
            //附件高清图
            swfu = new SWFUpload({

                upload_url: "/ProcessUpload/upload_excel.aspx?type=qual&year=" + year + "&month=" + month,
                flash_url: "/js/swfupload/swfupload.swf",
                post_params: {
                    "ASPSESSID": "<%=Session.SessionID %>"
                },
                file_size_limit: "200MB",
                file_types: "*.xls;*.xlsx",
                file_types_description: "文件资料上传",
                file_upload_limit: "0",
                file_queue_limit: "1",

                //Events
                file_queued_handler: fileQueued,
                file_queue_error_handler: fileQueueError,
                file_dialog_complete_handler: fileDialogComplete,
                upload_progress_handler: uploadProgress,
                upload_error_handler: uploadError,
                upload_success_handler: uploadSuccess,
                upload_complete_handler: uploadComplete,

                // Button
                button_placeholder_id: "spanButtonPlaceholder",
                button_image_url: "/img/swfupload/XPButtonNoText_61x22.png",
                button_width: 61,
                button_height: 22,
                button_text: '<span class="btnFile">选择文件</span>',
                button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 11pt;background-color:Black; } ',
                button_text_top_padding: 1,
                button_text_left_padding: 5,

                custom_settings: {
                    upload_target: "divFileProgressContainer"
                },
                debug: false
            });
        }
    </script>
    <style type="text/css">
        .kd {
            width: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>上传考勤</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>系统设置</a><i class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>上传考勤</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>上传考勤
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width">
                            <tr>

                                <td style="width: 60px;">年份：</td>
                                <td style="width: 80px;">
                                    <asp:DropDownList runat="server" ID="drpYear" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>

                                <td style="width: 60px;">月份:</td>
                                <td style="width: 80px;">

                                    <asp:DropDownList runat="server" ID="drp_month" CssClass="form-control" AutoPostBack="true">

                                        <asp:ListItem Value="01">01</asp:ListItem>
                                        <asp:ListItem Value="02">02</asp:ListItem>
                                        <asp:ListItem Value="03">03</asp:ListItem>
                                        <asp:ListItem Value="04">04</asp:ListItem>
                                        <asp:ListItem Value="05">05</asp:ListItem>
                                        <asp:ListItem Value="06">06</asp:ListItem>
                                        <asp:ListItem Value="07">07</asp:ListItem>
                                        <asp:ListItem Value="08">08</asp:ListItem>
                                        <asp:ListItem Value="09">09</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 100px;"><span class="btn default btn-file" style="display: block; height: 30px; width: 80px;"><span class="fileupload-new" id="spanButtonPlaceholder">
                                    <i class="fa fa-paper-clip"></i>选择文件</span> </span></td>
                                <td>
                                    <div id="divFileProgressContainer">
                                    </div>
                                </td>

                            </tr>
                        </table>

                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>部门经理加班设置
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table style="width:100%" border="0">
                            <tr>
                                 <td style="width: 50px;">年份：</td>
                                <td style="width: 100px;">
                                    <asp:DropDownList runat="server" ID="DropDownList1" CssClass="form-control" Width="80px" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>

                                <td style="width: 50px;">月份:</td>
                                <td style="width: 100px;">
                                    <asp:DropDownList runat="server" ID="DropDownList2" CssClass="form-control" Width="80px" AutoPostBack="true">
                                         <asp:ListItem Value="0">初始</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="Width: 100px">
                                    <asp:DropDownList ID="userlist" runat="Server" CssClass="form-control input-small select2me" Width="100px" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">&nbsp;&nbsp;请选择成员</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:Button id="btn_AddExe" Text="添加权限人员" runat="server" CssClass="btn blue" OnClick="btn_AddCar_Click" />
                                  
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <asp:GridView ID="gridList" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%" HeaderStyle-HorizontalAlign="Center" EnableModelValidation="True" OnRowDataBound="grid_mem_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="manager_id" HeaderText="编号">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="manager_name" HeaderText="权限人员">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>  
                                    <asp:TemplateField HeaderText="部门经理">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                          <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><asp:Literal ID="lit_users" runat="server"></asp:Literal></td>
                                                    <td style="width: 30px;">
                                                        &nbsp;&nbsp;<img src="../../Images/AddUser.png" style="cursor: pointer;" id="addUserBtn"
                                                            href="#UserDiv" data-toggle="modal" sysno="<%#Eval("manager_id")%>" rel="<%#Eval("manager_name")%>" /></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="60%" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="操作">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>                                         
                                              <a href="javascript:void(0)" class="btn red btn-xs" rel="<%#Eval("manager_id") %>" id="delete">删除</a>
                                        </ItemTemplate>
                                        <ItemStyle Width="20%" />
                                    </asp:TemplateField>
                                      </Columns>
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:GridView>
                                    
                                </td>
                            </tr>
                            <tr>
                                <Td colspan="6"><asp:Button id="btn_Save" Text="同步初始" runat="server" OnClick="btn_Save_Click" CssClass="btn red"/></Td>
                               
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="UserDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; height: 530px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择用户</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="chooseUserMain">
                        <uc1:SelectUserControl ID="SelectUserControl1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn green btn-default" id="btn_SaveUser">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="user_close">
                关闭</button>
        </div>
    </div>
</asp:Content>
