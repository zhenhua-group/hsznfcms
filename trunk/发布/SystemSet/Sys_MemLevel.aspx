﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="Sys_MemLevel.aspx.cs" Inherits="TG.Web.SystemSet.Sys_MemLevel" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery.alerts.js"></script>

    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="/js/SystemSet/Sys_MemLevel.js" type="text/javascript"></script>

   
    <style type="text/css">
        .mycenter {
            text-algin: center;
        }

        #ctl00$ContentPlaceHolder1$AspNetPager1_input {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>常用设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>级别设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>查询人员信息
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                    AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged">
                                    <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>用户:
                            </td>
                            <td>
                                <input class="form-control input-sm" type="text" id="txt_keyname"
                                    runat="Server" />
                            </td>
                            <td>年份:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="form-control" ID="drp_year" runat="server"
                                    AppendDataBoundItems="True">
                                    <asp:ListItem Value="2011">2011</asp:ListItem>
                                    <asp:ListItem Value="2012">2012</asp:ListItem>
                                    <asp:ListItem Value="2013">2013</asp:ListItem>
                                    <asp:ListItem Value="2014">2014</asp:ListItem>
                                     <asp:ListItem Value="2015">2015</asp:ListItem>
                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                            </td>
                             <Td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" id="btn_save" class="btn red" data-action="setdata">当前页保存</a></Td>
                        </tr>
                    </table>

                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>人员信息列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dataTable" id="tbData">
                                <thead>
                                    <tr>
                                        <th style="width: 60px;">年份</th>
                                        <th style="width: 60px;">编号</th>
                                        <th style="width: 60px;">登录名</th>
                                        <th style="width: 60px;">姓名</th>
                                        <th style="width: 100px;">部门名称</th>
                                        <th style="width: 120px;">级别</th>
                                        <th style="width: 50px;"></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% foreach (System.Data.DataRow dr in ResultTable.Rows)
                                       { %>
                                    <tr>
                                        <td>
                                            <%= dr["memYear"].ToString()=="0"?"无":dr["memYear"].ToString() %>
                                        </td>
                                        <td>
                                            <%= dr["mem_ID"].ToString() %>
                                        </td>
                                        <td>
                                            <%= dr["mem_Login"].ToString() %>
                                        </td>
                                        <td>
                                            <%= dr["mem_Name"].ToString() %>
                                        </td>
                                        <td>
                                            <%= dr["UnitName"].ToString() %>
                                        </td>
                                        <td>
                                            <select>
                                                <option value="1" <%= dr["memLevel"].ToString()=="1"?"selected='selected'":dr["mem_Name"].ToString().Trim()=="张兵"?"selected='selected'":"" %>>一级股东</option>
                                                <option value="2" <%= dr["memLevel"].ToString()=="2"?"selected='selected'":"" %>>二级股东A</option>
                                                <option value="3" <%= dr["memLevel"].ToString()=="3"?"selected='selected'":"" %>>二级股东B</option>
                                                <option value="4" <%= dr["memLevel"].ToString()=="4"?"selected='selected'":"" %>>二级股东C</option>
                                                <option value="5" <%= dr["memLevel"].ToString()=="5"?"selected='selected'":"" %>>设计总监</option>
                                                <option value="6" <%= dr["memLevel"].ToString()=="6"?"selected='selected'":"" %>>主任工程师A</option>
                                                <option value="7" <%= dr["memLevel"].ToString()=="7"?"selected='selected'":"" %>>主任工程师B</option>
                                                <option value="8" <%= dr["memLevel"].ToString()=="8"?"selected='selected'":"" %>>主任工程师C</option>
                                                <option value="9" <%= dr["memLevel"].ToString()=="9"?"selected='selected'":"" %>>主任工程师D</option>
                                                <option value="10" <%= dr["memLevel"].ToString()=="10"?"selected='selected'":"" %>>工程师</option>
                                                <option value="11" <%= dr["memLevel"].ToString()=="11"?"selected='selected'":"" %>>主创设计师</option>
                                                <option value="12" <%= dr["memLevel"].ToString()=="12"?"selected='selected'":"" %>>设计师A</option>
                                                <option value="13" <%= dr["memLevel"].ToString()=="13"?"selected='selected'":"" %>>设计师B</option>
                                                <option value="14" <%= dr["memLevel"].ToString()=="14"?"selected='selected'":"" %>>部门经理</option>
                                                <option value="15" <%= dr["memLevel"].ToString()=="15"?"selected='selected'":"" %>>其他</option>
                                                <option value="16" <%= (dr["memLevel"].ToString()=="16"||(dr["memLevel"].ToString()=="0"&&dr["mem_Name"].ToString().Trim()!="张兵"))?"selected='selected'":"" %>>无</option>
                                            </select>
                                        </td>
                                        <td>
                                            <a href="#" class="btn default btn-xs green-stripe" data-action="set">设置</a>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <%} %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="8">
                                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                                CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px; height:23px;"
                                                PageSize="30" SubmitButtonClass="btn green">
                                            </webdiyer:AspNetPager>
                                        </td>
                                    </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>



    <input type="hidden" name="" value="0" id="hidUserSysNo" />
</asp:Content>
