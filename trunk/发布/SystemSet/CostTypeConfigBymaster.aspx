﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="CostTypeConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.CostTypeConfigBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/SystemSet/CostTypeConfig.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>成本项设置</small>
    </h3>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>成本项设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>查询成本类型
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <table border="0" cellspacing="0" cellpadding="0" width="80%">
                            <tr>
                                <td>成本项类型:
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="form-control input-sm" ID="drp_unit3" runat="server"
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">--全部类型--</asp:ListItem>
                                        <asp:ListItem Value="0">共同成本</asp:ListItem>
                                        <asp:ListItem Value="1">勘察分院</asp:ListItem>
                                        <asp:ListItem Value="2">施工图审查室</asp:ListItem>
                                        <asp:ListItem Value="3">承包公司</asp:ListItem>
                                        <asp:ListItem Value="4">生产部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>操作类型:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_operation" runat="server" CssClass="form-control input-sm">
                                        <asp:ListItem Value="-1">--选择操作类型--</asp:ListItem>
                                        <asp:ListItem Value="0">录入</asp:ListItem>
                                        <asp:ListItem Value="1">导入</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>成本项名称:
                                </td>
                                <td>
                                    <input class="form-control input-sm" type="text" id="txt_keyname"
                                        runat="Server" />
                                </td>
                                <td>
                                    <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                                </td>
                                <td>
                                    <button class="btn blue" type="button" id="btn_showadd">
                                        添加</button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>成本类型列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_cost" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                HeaderStyle-HorizontalAlign="Center">
                                <Columns>

                                    <asp:TemplateField HeaderText="编号">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex+1%>'></asp:Label>
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="5%" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="costName" HeaderText="成本名称">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GroupName" HeaderText="成本类型">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InputName" HeaderText="操作类型">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="操作">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <a href="javascript:void(0);" class="cls_select" typeid="<%# Eval("ID")%>" isinput="<%# Eval("isInput")%>" costgroup="<%# Eval("costGroup")%>">编辑</a>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px; height:23px;"
                                PageSize="30" SubmitButtonClass="btn green">
                            </webdiyer:AspNetPager>
                            <asp:HiddenField ID="hid_where" runat="server" Value="" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="cls_data" id="div_add" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title ">
                <div class="caption">
                    <i class="fa fa-cogs"></i>新增成本类型
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table border="0" cellspacing="0" cellpadding="0" width="50%" class="table-bordered"
                    align="center">
                    <tr>
                        <td width="40%" align="center"><span>成本类型：</span>
                        </td>
                        <td width="60%">
                            <asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm">
                                <asp:ListItem Value="-1">--选择成本类型--</asp:ListItem>
                                <asp:ListItem Value="0">共同成本</asp:ListItem>
                                <asp:ListItem Value="1">勘察分院</asp:ListItem>
                                <asp:ListItem Value="2">施工图审查室</asp:ListItem>

                                <asp:ListItem Value="3">承包公司</asp:ListItem>
                                <asp:ListItem Value="4">生产部门</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">操作类型:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_operationType" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="-1">--选择操作类型--</asp:ListItem>
                                <asp:ListItem Value="0">录入</asp:ListItem>
                                <asp:ListItem Value="1">导入</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">成本名称:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_name" runat="server" CssClass="form-control input-sm" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btn_save" runat="server" Text="保存" CssClass="btn green" OnClick="btn_save_Click" />
                            <input type="button" class="btn default" value="取消" id="btn_Cancle1" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_edit" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title ">
                <div class="caption">
                    <i class="fa fa-cogs"></i>编辑成本类型
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table border="0" cellspacing="0" cellpadding="0" width="50%" class=" table-bordered" align="center">
                    <tr>

                        <td width="40%" align="center"><span>成本类型：</span>
                        </td>
                        <td width="60%">
                            <asp:DropDownList ID="drp_unit1" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm">
                                <asp:ListItem Value="-1">--选择成本类型--</asp:ListItem>
                                <asp:ListItem Value="0">共同成本</asp:ListItem>
                                <asp:ListItem Value="1">勘察分院</asp:ListItem>
                                <asp:ListItem Value="2">施工图审查室</asp:ListItem>
                                <asp:ListItem Value="3">承包公司</asp:ListItem>
                                <asp:ListItem Value="4">生产部门</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">操作类型:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_operationType1" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="-1">--选择操作类型--</asp:ListItem>
                                <asp:ListItem Value="0">录入</asp:ListItem>
                                <asp:ListItem Value="1">导入</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">成本名称:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_name1" runat="server" CssClass="form-control input-sm" MaxLength="100"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btn_edit" runat="server" Text="保存" CssClass="btn green " OnClick="btn_edit_Click" />
                            <input type="button" class="btn btn-default" value="取消" id="btn_Cancle2" />
                            <asp:HiddenField ID="hid_id" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <input type="button" class="btn default" value="取消" id="btn_hide" style="display: none;" />
</asp:Content>
