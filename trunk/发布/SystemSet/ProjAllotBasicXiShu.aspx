﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjAllotBasicXiShu.aspx.cs" Inherits="TG.Web.SystemSet.ProjAllotBasicXiShu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门奖金 <small>奖金分配系数</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门奖金</a><i class="fa fa-angle-right"> </i><a>奖金分配系数</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>项目奖金分配计提系数设置
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                    <th>系数设置</th>
                                    <th><a href="#" class="btn btn-sm red" id="btnSave">保存系数</a></th>
                                    <th colspan="2"></th>
                                </tr>

                                <tr>
                                    <th style="width: 100px;">序号</th>
                                    <th style="width: 200px;">名称</th>
                                    <th style="width: 80px;">比例</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% if (XiShuList.Count > 0)
                                   {
                                       int index = 1;
                                       XiShuList.ForEach(c =>
                                       {%>
                                <tr>
                                    <td><%= index++ %></td>
                                    <td><span id="<%= c.ID %>" jc="<%= c.jianc %>"><%= c.jiancName%></span></td>
                                    <td>
                                        <input type="text" name="name" value="<%= c.bili %>" style="width:60px;"/>%</td>
                                    <td></td>
                                </tr>
                                <%});
                                   } %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
