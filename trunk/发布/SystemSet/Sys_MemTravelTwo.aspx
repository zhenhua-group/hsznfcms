﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="Sys_MemTravelTwo.aspx.cs" Inherits="TG.Web.SystemSet.Sys_MemTravelTwo" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery.alerts.js"></script>

    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/SystemSet/Sys_MemLevelTwo.js" type="text/javascript"></script>


    <style type="text/css">
        .mycenter {
            text-algin: center;
        }

        #ctl00$ContentPlaceHolder1$AspNetPager1_input {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>机构设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>机构设置</a><i class="fa fa-angle-right"> </i><a>出国考察游管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>出国考察游管理
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <table class="table-responsive">
                        <tr>

                            <td>当前年份:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="form-control" ID="drp_year" runat="server"
                                    AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drp_year_SelectedIndexChanged">
                                    <asp:ListItem Value="2011">2011</asp:ListItem>
                                    <asp:ListItem Value="2012">2012</asp:ListItem>
                                    <asp:ListItem Value="2013">2013</asp:ListItem>
                                    <asp:ListItem Value="2014">2014</asp:ListItem>
                                    <asp:ListItem Value="2015">2015</asp:ListItem>
                                    <asp:ListItem Value="2016">2016</asp:ListItem>
                                    <asp:ListItem Value="2017">2017</asp:ListItem>
                                    <asp:ListItem Value="2018">2018</asp:ListItem>
                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <a href="Sys_MemTravel.aspx" class="btn btn-sm blue">出国旅游资格单</a>
                            </td>
                            <td>
                                <a href="Sys_MemTravelTwo.aspx" class="btn btn-sm blue">出国旅游安排计算表</a>
                            </td>
                            <td>
                                <a href="Sys_MemTravelThree.aspx" class="btn btn-sm blue">出国旅游汇总表</a>
                            </td>                           
                            <Td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" id="btn_save" class="btn red btn-sm" data-action="setdata">全部保存</a></Td>
                            <td></td>
                        </tr>
                    </table>

                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>人员信息列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover dataTable" id="tbData">
                                <thead>
                                    <tr>
                                        <th style="width: 120px;">名单</th>
                                        <th style="width: 80px;">起始年</th>
                                        <th style="width: 80px;">发生值</th>
                                        <th style="width: 80px;">计算值</th>
                                        <th style="width: 80px;">次/年</th>
                                        <th style="width: 80px;"><%= this.drp_year.SelectedValue %>余额</th>
                                        <th style="width: 80px;">安排计划</th>
                                        <th style="width: 80px;"><%= this.drp_year.SelectedValue %></th>
                                        <td style="width: 45px;"></td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% 
                                        this.ResultList.ForEach(m =>
                                       { %>

                                    <tr>
                                        <td data-memid="<%= m.Mem_ID %>"><%= m.Mem_Name %></td>
                                        <td><%= m.StartYear %></td>
                                        <td><%= m.BeginCount %></td>
                                        <td>
                                            <% 
                                           decimal? result=0;
                                           if (m.Mem_ID == 1440)
                                           {
                                               result = Convert.ToDecimal(1.5 * (int.Parse(this.drp_year.SelectedValue) - m.StartYear + 1));
                                           }
                                           else
                                           {
                                               if (m.LevelCount != null && m.LevelCount != 0)
                                               {
                                                   result = Math.Round(Convert.ToDecimal(1 / m.LevelCount), 2) * (int.Parse(this.drp_year.SelectedValue) - m.StartYear + 1);
                                               }
                                           }%>

                                            <%= result %>
                                        </td>
                                        <td>
                                            <%= m.LevelCount==0?0:Math.Round(Convert.ToDecimal(1 / m.LevelCount), 2) %>
                                        </td>
                                        <td>
                                            <%= m.CurYue %>
                                        </td>
                                        <td>
                                            <% if (m.CurYue >= 1)
                                               { %>
                                            <span class="badge badge-success">应安排 </span>
                                            <%}
                                               else if (m.CurYue >= 0.5m && m.CurYue < 1)
                                               { %>
                                            <span class="badge badge-info">宜安排</span>
                                            <%}
                                               else
                                               { %>
                                            <span class="badge badge-warning">空 </span>
                                            <%} %>
                                        </td>
                                        <td>
                                            <input type="text" name="name" value="<%= m.SetCount %>" style="width: 60px;" />
                                        </td>
                                        <td>
                                            <a href="#" class="btn default btn-xs green-stripe" data-action="setdata">保存</a>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <%  }); %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10"></td>
                                    </tr>
                                </tfoot>

                            </table>



                            <table>
                                <tr>
                                    <td rowspan="2"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>



    <input type="hidden" name="" value="0" id="hidUserSysNo" />
</asp:Content>
