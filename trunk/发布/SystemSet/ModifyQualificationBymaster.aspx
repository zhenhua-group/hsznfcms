﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ModifyQualificationBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ModifyQualificationBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="Stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="Stylesheet" type="text/css" />
    <link href="../css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_proj.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/SystemSet/ModifyQua.js" type="text/javascript"></script>
    <script type="text/javascript">
        //获得ID
        var projid = '<%=GetProjectID() %>';
        //用户ID
        var userid = '<%=GetCurMemID() %>';
        if (userid == "") {
            window.parent.parent.document.location.href = "../index.html";
        }
        if (projid != "0") {
            //缩略图
            //var swfut;
            //附件高清图
            var swfu;
            window.onload = function () {
                //附件高清图
                swfu = new SWFUpload({

                    upload_url: "../ProcessUpload/upload_qua.aspx?type=qual&id=" + projid + "&userid=" + userid,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.jpg;*.gif;*.bmp;.png",
                    file_types_description: "文件资料上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccess,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_image_url: "../images/swfupload/XPButtonNoText_61x22.png",
                    button_width: 61,
                    button_height: 22,
                    button_text: '<span class="btnFile">选择文件</span>',
                    button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 11pt;background-color:Black; } ',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,

                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
        }
    </script>
    <script type="text/javascript">
        var hid_projid = '<%=GetProjectID() %>';
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>编辑资质</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>系统设置</a><i class="fa fa-angle-right"> </i><a>编辑资质</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>编辑资质</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">
                        资质信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            资质名称:
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="lbl_title" runat="server" CssClass="form-control input-sm" MaxLength="300"
                                                placeholder="必填项"></asp:TextBox>
                                            <asp:HiddenField ID="hiddenSrc" runat="server" />
                                            <asp:HiddenField ID="hid_projid" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            资质开始时间:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_Zztime" runat="server" onclick="WdatePicker({readOnly:true})"
                                                class="Wdate" size="20" Height="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            资质结束时间:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_Sptime" runat="server" onclick="WdatePicker({readOnly:true})"
                                                class="Wdate" size="20" Height="30px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            资质级别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="lbl_jb" runat="Server" CssClass="form-control input-sm" AppendDataBoundItems="True">
                                                <asp:ListItem Value="">--请选择级别--</asp:ListItem>
                                                <asp:ListItem Value="甲级">甲级</asp:ListItem>
                                                <asp:ListItem Value="乙级">乙级</asp:ListItem>
                                                <asp:ListItem Value="丙级">丙级</asp:ListItem>
                                                <asp:ListItem Value="丁级">丁级</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            年审时间:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_yeartime" runat="server" onclick="WdatePicker({readOnly:true})"
                                                class="Wdate" size="20" Height="30px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            资质内容:
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="lbl_content" runat="server" Height="68px" Width="98%" TextMode="MultiLine"
                                                MaxLength="250" CssClass="form-control input-sm"></asp:TextBox>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <h3 class="form-section">
                        缩略图</h3>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                项目效果缩略图:</label>
                            <div class="col-md-10" id="img_container" style="text-align: center">
                                <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                    style="width: 250px; height: 150px;" />
                                <span class="help-block"><font><font class=""></font></font></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                资质信息图:</label>
                            <div class="col-md-10">
                                <table class="table table-bordered table-hover" id="datas_att">
                                    <tr id="att_row">
                                        <td style="width: 40px;" align="center" id="att_id">
                                            序号
                                        </td>
                                        <td style="width: 300px;" align="center" id="att_filename">
                                            文件名称
                                        </td>
                                        <td style="width: 80px" align="center" id="att_filesize">
                                            文件大小
                                        </td>
                                        <td style="width: 80px;" align="center" id="att_filetype">
                                            文件类型
                                        </td>
                                        <td style="width: 120px;" align="center" id="att_uptime">
                                            上传时间
                                        </td>
                                        <td style="width: 40px;" align="center" id="att_oper">
                                            &nbsp;
                                        </td>
                                        <td style="width: 40px;" align="center" id="att_oper2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <span class="help-block"><font><font class=""></font></font></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div id="divFileProgressContainer" style="float: right;">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder">
                                <i class="fa fa-paper-clip"></i>选择文件</span> </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <asp:Button ID="btn_save" class="btn green" runat="server" OnClick="btn_submin_Click"
                                Text="保存" />
                            <a href="QualificationListBymaster.aspx" class="btn default" target="_self">返回 </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value='1' id="hid_type" />
</asp:Content>
