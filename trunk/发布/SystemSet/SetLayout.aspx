﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SetLayout.aspx.cs" Inherits="TG.Web.SystemSet.SetLayout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>系统布局</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>布局管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>布局设置
            </div>
        </div>
        <div class="portlet-body" style="display: block;">
            <table class="table-responsive">
                <tr>
                    <td>当前布局：</td>
                    <td><asp:Label ID="lbl_Status" runat="server" Text=""></asp:Label></td>
                    
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btn_SetLayout" runat="server" Text=""  CssClass="btn red btn-sm" OnClick="btn_SetLayout_Click"/></td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
