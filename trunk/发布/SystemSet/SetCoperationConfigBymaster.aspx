﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="SetCoperationConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.SetCoperationConfigBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>合同编号配置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用管理</a><i class="fa fa-angle-right"> </i><a>合同编号配置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>合同类型</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table style="width: 100%;" class="table table-striped table-bordered table-hover dataTable">
                                    <tr class="cls_type">
                                        <td>
                                            民用建筑合同&nbsp;
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr class="cls_type">
                                        <td>
                                            工业建筑合同
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum2" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum2" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr class="cls_type">
                                        <td>
                                            工程勘察合同&nbsp;
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum3" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum3" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr class="cls_type">
                                        <td>
                                            工程监理合同
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum4" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum4" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr class="cls_type">
                                        <td>
                                            工程施工合同
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum5" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum5" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr class="cls_type">
                                        <td>
                                            工程咨询合同
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum6" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum6" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr class="cls_type">
                                        <td>
                                            工程总承包合同
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum7" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum7" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr class="cls_type">
                                        <td>
                                            工程代建合同
                                        </td>
                                        <td>
                                            起始编号：<asp:TextBox ID="txtStartNum8" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-001)
                                        </td>
                                        <td>
                                            结束编号：<asp:TextBox ID="txtEndNum8" runat="server" CssClass="cls_input_text"></asp:TextBox>
                                            (例：XXXX-100)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <asp:Button ID="btn_save" runat="server" OnClick="btn_save_Click" CssClass="btn blue"
                                                Text="确定" />
                                            <asp:HiddenField ID="hid_flag" runat="server" Value="1" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
