﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ShowMsgViewBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ShowMsgViewBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>  
    <script type="text/javascript">
        $(function () {

            //返回
            $("#btn_back").click(function () {
                window.history.back();
            });

            if ($("#hid_type").val() == "0") {
                $(".other").attr("style","display:block");
                
            }
            var sumpeople=<%=allpeople %>;
          
            if (sumpeople<1) {
                $(".divclass1").attr("style", "display:none");
            }
            $("#divopen").click(function () {

                $("#roleUserDiv").attr("style", "display:block");
                $(".divclass1").attr("style", "display:none");
                $(".divclass2").attr("style", "display:block");
            });
            $("#divclose").click(function () {
                $("#roleUserDiv").attr("style", "display:none");
                $(".divclass2").attr("style", "display:none");
                $(".divclass1").attr("style", "display:block");
            });

            //菜单选项
            $("#show8_4_1").parent().attr("class", "active");
            $("#show8").parent().attr("class", " arrow open");
            $("#show8_4").parent().attr("class", " arrow open");
            $("#show8_4").parent().parent().css("display", "block");
            $("#show8_4_1").parent().parent().css("display", "block");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">日志管理 <small>消息查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a href="javascript:;"
        class="menu_Open">系统设置</a><i class="fa fa-angle-right"> </i><a href="javascript:;"
            class="menu_Open">公告管理</a><i class="fa fa-angle-right"> </i><a href="javascript:;"
                class="menu_Open">日志管理</a><i class="fa fa-angle-right"></i><a href="javascript:;"
                    class="menu_Open">消息查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row" id="msgid">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>消息查看
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="newsbt">
                                <asp:Label ID="lbl_title" runat="server"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="newsly">
                                作者：<asp:Label ID="lbl_user" runat="server"></asp:Label>&nbsp;&nbsp;发布时间：
                                <asp:Label ID="lbl_time" runat="server"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="msgcontent">
                                <asp:Literal ID="lbl_content" runat="server"></asp:Literal>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="other" style="display: none">
                                <div class="font13">
                                    接收人员(<strong><%=allpeople%></strong>) <span class="help-block"><font><font class=""></span>
                                </div>
                                <!--选择人员显示主Div-->
                                <div class="divclass1">
                                    &nbsp;&nbsp;&nbsp; &nbsp;<a href="javascript:void(0)" id="divopen">展开↓</a>
                                </div>
                                <div style="width: 100%;" id="roleUserDiv">
                                    <asp:Repeater ID="RepeaterUsers" runat="server">
                                        <ItemTemplate>
                                            <span style="margin-right: 10px;" id="userSpan" usersysno="<%#Eval("ToUser") %>">
                                                <%#Eval("UserName") %><img style="margin-left: 5px; display: none; cursor: pointer;"
                                                    id="deleteUserlinkButton" src="/Images/pro_icon_03.gif" /></span>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <div class="divclass2">
                                        <a href="javascript:void(0)" id="divclose">收起↑</a>
                                    </div>

                                </div>
                                <div id="attachTableResult">
                                    <div class="font13">
                                        附件下载(<strong><%=allfile%></strong>) <span class="help-block"><font><font class=""></font>
                                        </font></span>
                                    </div>
                                    <asp:Repeater ID="RepeaterAttach" runat="server">
                                        <ItemTemplate>
                                            <span style="margin-right: 10px;">
                                                <img style='width: 16px; height: 16px;' src="<%#Eval("IconPath") %>" /><a href="/SystemSet/DownLoadFile.aspx?fileName=<%#Server.UrlEncode(Eval("FileName").ToString()) %>&filePath=<%#Server.UrlEncode(Eval("RelativePath").ToString()) %>"
                                                    target="_blank"><%#Eval("FileName") %></a> </span>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="fh">
                                <input id="btn_back" type="button" value="返回" class="btn default" />

                            </div>
                        </div>
                    </div>
                    <!--接收人员-->
                    <div class="row" id="webBannerTR" style="display: none;">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>接收人员</legend>
                                <!--选择人员显示主Div-->
                            </fieldset>

                        </div>
                    </div>
                    <!--添加附件，公告管理特有-->
                    <div class="row" id="addAttach" style="display: none;">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>附件下载</legend>
                            </fieldset>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value='<%= MsgType %>' id="hid_type" />
</asp:Content>
