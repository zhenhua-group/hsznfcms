﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitAuditSet.aspx.cs" Inherits="TG.Web.SystemSet.UnitAuditSet" %>

<%@ Register Src="../Coperation/SelectUserControl.ascx" TagName="SelectUserControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <%--
    <link href="/css/LeaderManage.css" rel="stylesheet" type="text/css" />--%>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Coperation/SelectRoleUser.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/UserControl/EditRolePowerControl.js"></script>
    <script src="../js/SystemSet/UnitAuditSet.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(function () {


        });  //清空数据  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>部门经理设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>部门经理设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>部门列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">

                        <div class="row">
                            <font color="red">注：<%--除【规划部 - 总经理】以外，其他--%>各部角色只允许设置一人！</font>
                            <asp:GridView ID="gridList" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%" HeaderStyle-HorizontalAlign="Center" EnableModelValidation="True" OnRowDataBound="grid_mem_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="unit_ID" HeaderText="编号">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>  
                                    <asp:BoundField DataField="Unit_Name" HeaderText="部门名称">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>  
                                    <asp:TemplateField HeaderText="部门经理">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                          <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td><asp:Literal ID="lit_users" runat="server"></asp:Literal></td>
                                                    <td style="width: 30px;">
                                                        &nbsp;<img src="../../Images/AddUser.png" style="cursor: pointer;" id="addUserBtn"
                                                            href="#UserDiv" data-toggle="modal" sysno="<%#Eval("unit_ID")%>" rel="unitusers" /></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="40%" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="总经理">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                          <table border="0" cellspacing="0" cellpadding="0" >
                                                <tr>
                                                    <td><asp:Literal ID="lit_manager" runat="server"></asp:Literal></td>
                                                    <td>
                                                        &nbsp;<img src="../../Images/AddUser.png" style="cursor: pointer;" id="addUserManagerBtn"
                                                            href="#UserDiv" data-toggle="modal" sysno="<%#Eval("unit_ID")%>" rel="managerusers" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="40%" />
                                    </asp:TemplateField>
                                      </Columns>
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:GridView> 
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div id="UserDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; height: 530px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择用户</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="chooseUserMain">
                        <uc1:SelectUserControl ID="SelectUserControl1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn green btn-default" id="btn_SaveUser">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="user_close">
                关闭</button>
        </div>
    </div>

</asp:Content>
