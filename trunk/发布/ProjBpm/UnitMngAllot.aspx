﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitMngAllot.aspx.cs" Inherits="TG.Web.ProjBpm.UnitMngAllot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="../js/assets/plugins/data-tables/DT_bootstrap.css" />

    <script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/DT_bootstrap.js"></script>



    <script src="../js/DeptBpm/UnitMngAllot.js?v=20170731"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>部门内奖金计算</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>部门内奖金计算</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>部门名称：<%= KaoHeUnit.unit_Name%>
                    </div>
                    <div class="actions">
                        <a href="#" class="btn btn-sm red" id="btnOrder">排序</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <% if (IsArch)
                           { %>
                        <!--在职人员-->
                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th>部门：</th>
                                    <th><span id="spunitName"><%= KaoHeUnit.unit_Name%></span></th>
                                    <th colspan="5">

                                        <%  //未归档
                               if (!IsBak)
                               {
                                   if (!IsSubmit)
                                   {%>
                                        <a href="#" class="btn btn-sm red" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <%}
                                   else
                                   {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm red" ID="btn_Export" runat="server" Text="导出数据" OnClick="btn_Export_Click" />
                                        <%
                                       if (this.UserShortName == "admin")
                                       {%>
                                        <a href="#" class="btn btn-sm yellow" id="btnBak">发起归档</a>
                                        <%}
                                   }
                               }
                               else
                               {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm default" ID="btn_Export11" runat="server" Text="导出数据" OnClick="btn_Export_Click" />
                                        <a href="#" class="btn btn-sm default">已归档</a>
                                        <%} %>

                                    </th>
                                    <th colspan="15"></th>
                                </tr>
                                <tr>
                                    <th colspan="2">部门奖金总额(元)：</th>
                                    <th>
                                        <% if (!IsBak)
                                           { %>
                                        <span id="spunitAllCount"><%= Math.Round(Convert.ToDecimal(UnitAllCount)) %></span>
                                        <%}
                                           else
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(UnitAllCountBak)) %></span>
                                        <%} %>
                                    </th>
                                    <th colspan="2">整体差距调整系数：</th>
                                    <th colspan="2">
                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= ZhengTiXishu %>" id="txtChazhiXishu" style="width: 60px;" />
                                        <%}
                                           else
                                           { %>
                                        <span id="spChazhiXishu"><%= ZhengTiXishu %></span>
                                        <%} %>
                                    </th>
                                    <th colspan="15"></th>
                                </tr>
                                <tr>
                                    <th style="width: 70px;"><a href="#" id="btnDefault" title="点击恢复默认排序">姓名</a></th>
                                    <th style="width: 70px;">基本工资</th>
                                    <th style="width: 90px;">工作时<br />
                                        间/月</th>
                                    <th style="width: 90px;">平均月<br />
                                        工资</th>

                                    <th style="width: 80px;">自评互评</th>
                                    <th style="width: 40px;">排名</th>
                                    <th style="width: 100px;">部门经<br />
                                        理评价</th>
                                    <th style="width: 40px;">排名</th>
                                    <th style="width: 70px;">加权系数</th>
                                    <th style="width: 80px;">部门内奖<br />
                                        金计算值</th>
                                    <th style="width: 80px;">项目奖金<br />
                                        计算值</th>
                                    <th style="width: 80px;">项目奖金<br />
                                        经理调整</th>
                                    <th style="width: 80px;">项目奖金<br />
                                        调整值</th>
                                    <th style="width: 85px;">优秀员<br />
                                        工奖</th>

                                    <th style="width: 140px;">合计<br />
                                        (含预发)</th>
                                    <th style="width: 90px;">预发奖金</th>
                                    <th style="width: 70px;">合计</th>
                                    <th style="width: 70px;">工资倍数</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year %><br />
                                        上半年<br />
                                        (含预发)
                                        <%} %>
                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        上半年<br />
                                        (含预发)
                                    <%}
                                           else
                                           { %>

                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                    <%} %>

                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                           decimal? hjzz = 0;
                                           decimal? hjzz2 = 0;
                                           decimal? hjzz3 = 0;
                                           decimal? hjzz4 = 0;
                                           decimal? hjzz5 = 0;
                                           decimal? hjzz6 = 0;
                                           decimal? hjzz7 = 0;
                                           decimal? hjzz8 = 0;
                                           decimal? hjzz9 = 0;
                                           decimal? hjzz10 = 0;
                                           decimal? hjzz11 = 0;
                                           decimal? hjzz12 = 0;
                                           //hjyf
                                           decimal? hjzz13 = 0;
                                           //yfjj
                                           decimal? hjzz14 = 0;


                                           if (!IsSave)
                                           {
                                               if (UnitMemsList.Rows.Count > 0)
                                               {

                                                   foreach (System.Data.DataRow dr in UnitMemsList.Rows)
                                                   { %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjzz += Convert.ToDecimal(dr["jbgz"].ToString()); %>

                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>
                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">

                                        <% 
                                           //if (dr["mem_ID"].ToString() == "1473" || dr["mem_ID"].ToString() == "1467")
                                           if (dr["mem_ID"].ToString() == "-1")
                                           { %>
                                        <%--<span>5</span>--%>
                                        <input type="text" value="5" style="width: 80px;" />
                                        <%//}
                                                           //else if (dr["mem_ID"].ToString() == "1468")
                                                           //{ %>
                                        <%--<span>4.75</span>--%>
                                        <input type="text" value="4.75" style="width: 80px;" />

                                        <%//}
                                                           //else if (dr["mem_ID"].ToString() == "1546")
                                                           //{ %>
                                        <%--<span>1.8</span>--%>
                                        <input type="text" value="1.8" style="width: 80px;" />

                                        <%}
                                                       else
                                                       { %>

                                        <%
                                               DateTime curDate = DateTime.Now;
                                               if (!string.IsNullOrEmpty(dr["mem_InTime"].ToString()))
                                               {
                                                   curDate = Convert.ToDateTime(dr["mem_InTime"].ToString());
                                               }
                                               if (curDate > RenwuModel.StartDate && curDate < RenwuModel.EndDate)
                                               {
                                                   //任务截止日期
                                                   var endDate = RenwuModel.EndDate;
                                                   //自然天
                                                   int days = new TimeSpan(endDate.Ticks - curDate.Ticks).Days + 1;
                                                   decimal? result = (decimal)(6 * days) / 183;
                                        %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(result),2) %></span>--%>
                                        <input type="text" value="<%= Math.Round(Convert.ToDecimal(result),2) %>" style="width: 80px;" />
                                        <%}
                                               else if (curDate <= RenwuModel.StartDate)
                                               { %>
                                        <%--<span>6</span>--%>
                                        <input type="text" value="6" style="width: 80px;" />
                                        <%}
                                               else
                                               { %>
                                        <%--<span>0</span>--%>
                                        <input type="text" value="0" style="width: 80px;" />
                                        <%} %>

                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz2 += Convert.ToDecimal(dr["pjgz"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%>
                                    </td>

                                    <td style="text-align: right;">
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID"].ToString()%></span>--%>

                                        <%= dr["OrderID"].ToString()%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["MngCount"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["MngCount"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID2"].ToString()%></span>--%>

                                        <%= dr["OrderID2"].ToString()%>
                                    </td>
                                    <td style="text-align: right;">
                                        <span val="<%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%>" maxval="<%=  Convert.ToDecimal(dr["maxJudgeCount"].ToString()).ToString("f2")%>" minval="<%=  Convert.ToDecimal(dr["minJudgeCount"].ToString()).ToString("f2")%>">0</span>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz3 += 0; %>
                                        <%--<span>0</span>--%>

                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz4 += Convert.ToDecimal(dr["AllotCountBefore"].ToString()); %>
                                        <%--<span><%= dr["AllotCountBefore"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["AllotCountBefore"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mng" /></td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mem" /></td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz14 += Convert.ToDecimal(dr["yfjj"].ToString()); %>

                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td style="text-align: right;">

                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016 && (RenwuModel.StartDate.Month < 8))
                                                       {
                                                           hjzz11 += Convert.ToDecimal(dr["xbn2"].ToString());%>
                                        <span><%= dr["xbn2"].ToString() %></span>
                                        <%}
                                                       else
                                                       {
                                                           hjzz11 += Convert.ToDecimal(dr["xbn"].ToString());%>
                                        <span><%= dr["xbn"].ToString() %></span>

                                        <%} %> 

                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td style="text-align: right;">

                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016)
                                                       {
                                                           if (RenwuModel.StartDate.Month >= 8)
                                                           {
                                                               hjzz9 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>
                                        <%}
                                                           else
                                                           {
                                                               hjzz9 += Convert.ToDecimal(dr["sbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn2"].ToString())) %></span>
                                        <%
                                                           }
                                        %>
                                        <%}
                                                       else
                                                       {
                                                           hjzz9 += Convert.ToDecimal(dr["sbn"].ToString());  %>
                                        <span><%= dr["sbn"].ToString() %></span>
                                        <%} %>
                                               
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           }
                                           else
                                           {//已保存
                                               if (UnitMemsSaveList.Rows.Count > 0)
                                               {

                                                   foreach (System.Data.DataRow dr in UnitMemsSaveList.Rows)
                                                   {
                                %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjzz += Convert.ToDecimal(dr["jbgz"].ToString()); %>

                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>
                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["gzsj"].ToString()%></span>--%>
                                        <% if (dr["Stat"].ToString() == "0")
                                           { %><input type="text" value="<%= dr["gzsj"].ToString()%>" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= dr["gzsj"].ToString()%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz2 += Convert.ToDecimal(dr["pjgz"].ToString()); %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%></span>--%>
                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%>
                                    </td>

                                    <td style="text-align: right;">
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID"].ToString()%></span>--%>

                                        <%= dr["OrderID"].ToString()%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["MngCount"].ToString()%></span>--%>
                                        <%= Convert.ToDecimal(dr["MngCount"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID2"].ToString()%></span>--%>

                                        <%= dr["OrderID2"].ToString()%>
                                    </td>
                                    <td style="text-align: right;"><span val="<%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%>" maxval="<%=  Convert.ToDecimal(dr["maxJudgeCount"].ToString()).ToString("f2")%>" minval="<%=  Convert.ToDecimal(dr["minJudgeCount"].ToString()).ToString("f2")%>"><%= dr["jqxs"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjzz3 += Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()) * (decimal)0.01) * 100; %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString())*(decimal)0.01)*100%></span>--%>
                                        <%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString())*(decimal)0.01)*100%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz4 += Convert.ToDecimal(dr["xmjjjs"].ToString()); %>

                                        <%--<span><%= dr["xmjjjs"].ToString()%></span>--%>
                                        <%= Math.Round(Convert.ToDecimal(dr["xmjjjs"].ToString()))%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz5 += Convert.ToDecimal(dr["xmjjmng"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%= dr["xmjjmng"].ToString()%>" style="width: 60px;" action="mng" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= dr["xmjjmng"].ToString()%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz6 += Convert.ToDecimal(dr["xmjjtz"].ToString()); %>

                                        <%--<span><%= dr["xmjjtz"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["xmjjtz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz7 += Convert.ToDecimal(dr["yxyg"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %><input type="text" name="name" value="<%= dr["yxyg"].ToString()%>" style="width: 60px;" action="mem" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= dr["yxyg"].ToString()%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz13 += Convert.ToDecimal(dr["hjyf"].ToString()); %>
                                        <%--<span><%= Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%></span>--%>
                                        <%= Convert.ToDecimal(dr["hjyf"].ToString()).ToString("f0")%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz14 += Convert.ToDecimal(dr["yfjj"].ToString()); %>

                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjzz8 += Convert.ToDecimal(dr["hj"].ToString()); %>
                                        <%--<span><%= Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%></span>--%>
                                        <%= Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%>
                                    </td>

                                    <td style="text-align: right;"><span><%= dr["beizhu"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjzz11 += Convert.ToDecimal(dr["beforyear2"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["beforyear2"].ToString()).ToString("f0")%></span></td>
                                    <td style="text-align: right;">
                                        <% hjzz10 += Convert.ToDecimal(dr["chazhi2"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["chazhi2"].ToString()).ToString("f0")%></span></td>
                                    <td style="text-align: right;">
                                        <% hjzz9 += Convert.ToDecimal(dr["beforyear"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["beforyear"].ToString()).ToString("f0")%></span></td>
                                    <td style="text-align: right;">
                                        <% hjzz12 += Convert.ToDecimal(dr["chazhi"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["chazhi"].ToString()).ToString("f0")%></span></td>
                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           }%>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz),2) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz2),2) %></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz3),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz4),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz5),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz6),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz7),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz13)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz14),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz8)) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz11)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz10)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz9)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjzz12)) %></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                        <%} %>

                        <%if (IsFangAn)
                          { %>
                        <!--方案部门-->
                        <table class="table table-bordered table-hover" id="tbDataThree">
                            <thead>
                                <tr>
                                    <th>部门：</th>
                                    <th><span id="spunitName3"><%= KaoHeUnit.unit_Name%></span></th>
                                    <th colspan="5">
                                        <%  //未归档
                              if (!IsBak)
                              {
                                  if (!IsSubmit)
                                  {%>
                                        <a href="#" class="btn btn-sm red" id="btnSave3">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit3">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <%}
                                  else
                                  {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm red" ID="btn_Export4" runat="server" Text="导出数据" OnClick="btn_Export4_Click" />
                                        <%
                                      if (this.UserShortName == "admin")
                                      {%>
                                        <a href="#" class="btn btn-sm yellow" id="btnFanganBak">发起归档</a>
                                        <%}
                                  }
                              }
                              else
                              {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm default" ID="Button3" runat="server" Text="导出数据" OnClick="btn_Export4_Click" />
                                        <a href="#" class="btn btn-sm default">已归档</a>
                                        <%} %>

                                    </th>
                                    <th colspan="17"></th>
                                </tr>
                                <tr>
                                    <th colspan="2">部门奖金总额(元)：</th>
                                    <th>
                                        <%if (!IsBak)
                                          { %>
                                        <span id="spunitAllCount3"><%= Math.Round(Convert.ToDecimal(UnitAllCount)) %></span>
                                        <%}
                                          else
                                          { %>
                                        <span><%= Math.Round(Convert.ToDecimal(UnitAllCountBak)) %></span>
                                        <%} %>
                                    </th>
                                    <th colspan="2">整体差距调整系数：</th>
                                    <th colspan="2">
                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= ZhengTiXishu %>" id="txtChazhiXishu2" style="width: 60px;" />
                                        <%}
                                           else
                                           { %>
                                        <span id="spChazhiXishu3"><%= ZhengTiXishu %></span>
                                        <%} %>
                                    </th>
                                    <th colspan="14"></th>
                                </tr>
                                <tr>
                                    <th style="width: 70px;"><a href="#" id="btnDefault" title="点击恢复默认排序">姓名</a></th>
                                    <th style="width: 70px;">基本工资</th>
                                    <th style="width: 90px;">工作时<br />
                                        间/月</th>
                                    <th style="width: 90px;">平均月<br />
                                        工资</th>

                                    <th style="width: 80px;">自评互评</th>
                                    <th style="width: 40px;">排名</th>
                                    <th style="width: 100px;">部门经<br />
                                        理评价</th>
                                    <th style="width: 40px;">排名</th>
                                    <th style="width: 70px;">加权系数</th>
                                    <th style="width: 80px;">部门内奖<br />
                                        金计算值</th>
                                    <th style="width: 90px;">部门内奖<br />
                                        金经理调整</th>
                                    <th style="width: 90px;">部门内奖<br />
                                        金调整值</th>
                                    <th style="width: 85px;">优秀员<br />
                                        工奖</th>
                                    <th style="width: 140px;">合计<br />
                                        (含预发)</th>
                                    <th style="width: 90px;">预发奖金</th>
                                    <th style="width: 70px;">合计</th>

                                    <th style="width: 70px;">工资倍数</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year %><br />
                                        上半年<br />
                                        (含预发)
                                        <%} %>
                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        上半年<br />
                                        (含预发)
                                    <%}
                                           else
                                           { %>

                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                    <%} %>

                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                           decimal? hjfa = 0;
                                           decimal? hjfa2 = 0;
                                           decimal? hjfa3 = 0;
                                           decimal? hjfa4 = 0;
                                           decimal? hjfa5 = 0;
                                           decimal? hjfa6 = 0;
                                           decimal? hjfa7 = 0;
                                           decimal? hjfa8 = 0;
                                           decimal? hjfa9 = 0;
                                           decimal? hjfa10 = 0;
                                           decimal? hjfa11 = 0;
                                           //hjyf
                                           decimal? hjfa12 = 0;
                                           //yfjj
                                           decimal? hjfa13 = 0;

                                           if (!IsSave)
                                           {
                                               if (UnitMemsList.Rows.Count > 0)
                                               {

                                                   foreach (System.Data.DataRow dr in UnitMemsList.Rows)
                                                   { %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjfa += Convert.ToDecimal(dr["jbgz"].ToString()); %>

                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% //if (dr["mem_ID"].ToString() == "1479")
                                           if (dr["mem_ID"].ToString() == "-1")
                                           { %>
                                        <%--<span>3.77</span>--%>
                                        <input type="text" value="3.77" style="width: 80px;" />
                                        <%//}
                                           //else if (dr["mem_ID"].ToString() == "1503")
                                           //{ %>
                                        <%--<span>5</span>--%>
                                        <input type="text" value="5" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>

                                        <%
                                               DateTime curDate = DateTime.Now;
                                               if (!string.IsNullOrEmpty(dr["mem_InTime"].ToString()))
                                               {
                                                   curDate = Convert.ToDateTime(dr["mem_InTime"].ToString());
                                               }
                                               if (curDate > RenwuModel.StartDate && curDate < RenwuModel.EndDate)
                                               {
                                                   //任务截止日期
                                                   var endDate = RenwuModel.EndDate;
                                                   //自然天
                                                   int days = new TimeSpan(endDate.Ticks - curDate.Ticks).Days + 1;
                                                   decimal? result = (decimal)(6 * days) / 183;
                                        %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(result),2) %></span>--%>
                                        <input type="text" value="<%= Math.Round(Convert.ToDecimal(result),2) %>" style="width: 80px;" />
                                        <%}
                                               else if (curDate <= RenwuModel.StartDate)
                                               { %>
                                        <%--<span>6</span>--%>
                                        <input type="text" value="6" style="width: 80px;" />
                                        <%}
                                               else
                                               { %>
                                        <%--<span>0</span>--%>
                                        <input type="text" value="0" style="width: 80px;" />
                                        <%} %>

                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa2 += Convert.ToDecimal(dr["pjgz"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%>
                                    </td>

                                    <td style="text-align: right;"><span><%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID"].ToString()%></span>--%>

                                        <%= dr["OrderID"].ToString()%>
                                    </td>
                                    <td style="text-align: right;"><span><%= dr["MngCount"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID2"].ToString()%></span>--%>

                                        <%= dr["OrderID2"].ToString()%>
                                    </td>
                                    <td style="text-align: right;">
                                        <span val="<%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%>" maxval="<%= Convert.ToDecimal(dr["maxJudgeCount"].ToString()).ToString("f2")%>" minval="<%= Convert.ToDecimal(dr["minJudgeCount"].ToString()).ToString("f2")%>">0</span>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mng" /></td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mem" /></td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa13 += Convert.ToDecimal(dr["yfjj"].ToString()); %>

                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>

                                    <td style="text-align: right;"><span>0</span></td>
                                    <td style="text-align: right;">

                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016 && (RenwuModel.StartDate.Month < 8))
                                                       {
                                                           hjfa10 += Convert.ToDecimal(dr["xbn2"].ToString());%>
                                        <span><%= dr["xbn2"].ToString() %></span>
                                        <%}
                                                       else
                                                       {
                                                           hjfa10 += Convert.ToDecimal(dr["xbn"].ToString());%>
                                        <span><%= dr["xbn"].ToString() %></span>

                                        <%} %> 
                                               
                                    </td>
                                    <%--<td style="text-align:right;">
                                        <% hjfa8 += Convert.ToDecimal(dr["sbn"].ToString()); %>

                                        <span><%= dr["sbn"].ToString() %></span></td>--%>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <%--<td style="text-align:right;">
                                        <% hjfa10 += Convert.ToDecimal(dr["xbn"].ToString()); %>

                                        <span><%= dr["xbn"].ToString() %></span></td>--%>
                                    <td style="text-align: right;">

                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016)
                                                       {
                                                           if (RenwuModel.StartDate.Month >= 8)
                                                           {
                                                               hjfa8 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>
                                        <%}
                                                           else
                                                           {
                                                               hjfa8 += Convert.ToDecimal(dr["sbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn2"].ToString())) %></span>
                                        <%
                                                           }
                                        %>
                                        <%}
                                                       else
                                                       {
                                                           hjfa8 += Convert.ToDecimal(dr["sbn"].ToString());  %>
                                        <span><%= dr["sbn"].ToString() %></span>
                                        <%} %>

                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           }
                                           else
                                           {//以保存
                                               if (UnitMemsSaveList.Rows.Count > 0)
                                               {

                                                   foreach (System.Data.DataRow dr in UnitMemsSaveList.Rows)
                                                   {
                                %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjfa += Convert.ToDecimal(dr["jbgz"].ToString()); %>

                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>
                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["gzsj"].ToString()%></span>--%>
                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" value="<%= dr["gzsj"].ToString()%>" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= dr["gzsj"].ToString()%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa2 += Convert.ToDecimal(dr["pjgz"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%></span>--%>
                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2)%>
                                    </td>

                                    <td style="text-align: right;"><span><%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID"].ToString()%></span>--%>

                                        <%= dr["OrderID"].ToString()%>
                                    </td>
                                    <td style="text-align: right;"><span><%= dr["MngCount"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID2"].ToString()%></span>--%>

                                        <%= dr["OrderID2"].ToString()%>
                                    </td>
                                    <td style="text-align: right;"><span val="<%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%>" maxval="<%=  Convert.ToDecimal(dr["maxJudgeCount"].ToString()).ToString("f2")%>" minval="<%=  Convert.ToDecimal(dr["minJudgeCount"].ToString()).ToString("f2")%>"><%= dr["jqxs"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjfa3 += Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()) * (decimal)0.01) * 100; %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString())*(decimal)0.01)*100%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString())*(decimal)0.01)*100%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa4 += Convert.ToDecimal(dr["xmjjmng"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%= dr["xmjjmng"].ToString()%>" style="width: 60px;" action="mng" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= dr["xmjjmng"].ToString()%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa5 += Convert.ToDecimal(dr["xmjjtz"].ToString()); %>

                                        <%--<span><%= dr["xmjjtz"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["xmjjtz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa6 += Convert.ToDecimal(dr["yxyg"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%= dr["yxyg"].ToString()%>" style="width: 60px;" action="mem" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= dr["yxyg"].ToString()%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa12 += Convert.ToDecimal(dr["hjyf"].ToString()); %>

                                        <%--<span><%= Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%></span>--%>

                                        <%= Convert.ToDecimal(dr["hjyf"].ToString()).ToString("f0")%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa13 += Convert.ToDecimal(dr["yfjj"].ToString()); %>


                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjfa7 += Convert.ToDecimal(dr["hj"].ToString()); %>

                                        <%--<span><%= Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%></span>--%>

                                        <%= Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%>
                                    </td>

                                    <td style="text-align: right;"><span><%= dr["beizhu"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjfa10 += Convert.ToDecimal(dr["beforyear2"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["beforyear2"].ToString()).ToString("f0")%></span></td>
                                    <td style="text-align: right;">
                                        <% hjfa9 += Convert.ToDecimal(dr["chazhi2"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["chazhi2"].ToString()).ToString("f0")%></span></td>
                                    <td style="text-align: right;">
                                        <% hjfa8 += Convert.ToDecimal(dr["beforyear"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["beforyear"].ToString()).ToString("f0")%></span></td>
                                    <td style="text-align: right;">
                                        <% hjfa11 += Convert.ToDecimal(dr["chazhi"].ToString()); %>

                                        <span><%= Convert.ToDecimal(dr["chazhi"].ToString()).ToString("f0")%></span></td>
                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           }%>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa),2) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa2),2) %></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa3),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa4),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa5),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa6),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa12)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa13),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa7)) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa10)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa9)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa8)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjfa11)) %></td>

                                    <td></td>

                                </tr>
                            </tfoot>
                        </table>
                        <%} %>

                        <% if (IsTeShu)
                           { %>
                        <!--特殊部门调整-->
                        <table class="table table-bordered table-hover" id="tbDataTwo">
                            <thead>
                                <tr>
                                    <th>部门：</th>
                                    <th style="width: 100px;"><span id="spunitName2"><%= KaoHeUnit.unit_Name%></span></th>
                                    <th colspan="3">
                                        <%  //未归档
                               if (!IsBak)
                               {
                                   if (!IsSubmit)
                                   {%>
                                        <a href="#" class="btn btn-sm red" id="btnSave2">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit2">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <%}
                                   else
                                   {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm red" ID="btn_Export2" runat="server" Text="导出数据" OnClick="btn_Export2_Click" />
                                        <%
                                       if (this.UserShortName == "admin")
                                       {%>
                                        <a href="#" class="btn btn-sm yellow" id="btnTeshu1">发起归档</a>
                                        <%}
                                   }
                               }
                               else
                               {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm default" ID="Button4" runat="server" Text="导出数据" OnClick="btn_Export2_Click" />
                                        <a href="#" class="btn btn-sm default">已归档</a>
                                        <%} %>

                                    </th>
                                    <th colspan="14"></th>
                                </tr>
                                <tr>
                                    <th style="width: 70px;"><a href="#" id="btnDefault" title="点击恢复默认排序">姓名</a></th>
                                    <th style="width: 70px;">基本工资</th>
                                    <th style="width: 90px;">工作时<br />
                                        间/月</th>
                                    <th style="width: 90px;">平均月<br />
                                        工资</th>

                                    <th style="width: 80px;">自评互评</th>
                                    <th style="width: 40px;">排名</th>
                                    <th style="width: 100px;">部门经<br />
                                        理评价</th>
                                    <th style="width: 40px;">排名</th>
                                    <th style="width: 120px;">部门内经<br />
                                        理调整</th>
                                    <th style="width: 100px;">优秀员<br />
                                        工奖</th>
                                    <th style="width: 70px;">合计<br />
                                        (含预发)</th>
                                    <th style="width: 90px;">预发奖金</th>
                                    <th style="width: 70px;">合计</th>
                                    <th style="width: 70px;">工资倍数</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year %><br />
                                        上半年<br />
                                        (含预发)
                                        <%} %>
                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        上半年<br />
                                        (含预发)
                                    <%}
                                           else
                                           { %>

                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                    <%} %>

                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                           decimal? hjts = 0;
                                           decimal? hjts2 = 0;
                                           decimal? hjts3 = 0;
                                           decimal? hjts4 = 0;
                                           decimal? hjts5 = 0;
                                           decimal? hjts6 = 0;
                                           decimal? hjts7 = 0;
                                           decimal? hjts8 = 0;
                                           decimal? hjts9 = 0;
                                           //hjyf
                                           decimal? hjts10 = 0;
                                           //yfjj
                                           decimal? hjts11 = 0;


                                           if (!IsSave)
                                           {
                                               if (UnitMemsList.Rows.Count > 0)
                                               {
                                                    
                                                   foreach (System.Data.DataRow dr in UnitMemsList.Rows)
                                                   { %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts += Convert.ToDecimal(dr["jbgz"].ToString()); %>
                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                           DateTime curDate = DateTime.Now;
                                           if (!string.IsNullOrEmpty(dr["mem_InTime"].ToString()))
                                           {
                                               curDate = Convert.ToDateTime(dr["mem_InTime"].ToString());
                                           }
                                           if (curDate > RenwuModel.StartDate && curDate < RenwuModel.EndDate)
                                           {
                                               //任务截止日期
                                               var endDate = RenwuModel.EndDate;
                                               //自然天
                                               int days = new TimeSpan(endDate.Ticks - curDate.Ticks).Days + 1;
                                               decimal? result = (decimal)(6 * days) / 183;
                                        %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(result),2) %></span>--%>
                                        <input type="text" value="<%= Math.Round(Convert.ToDecimal(result),2) %>" style="width: 80px;" />
                                        <%}
                                           else if (curDate <= RenwuModel.StartDate)
                                           { %>
                                        <%--<span>6</span>--%>
                                        <input type="text" value="6" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <%--<span>0</span>--%>
                                        <input type="text" value="0" style="width: 80px;" />
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts2 += Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2) %></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2) %>
                                    </td>

                                    <td style="text-align: right;"><span><%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID"].ToString()%></span>--%>

                                        <%= dr["OrderID"].ToString()%>
                                    </td>
                                    <td style="text-align: right;"><span><%= dr["MngCount"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID2"].ToString()%></span>--%>

                                        <%= dr["OrderID2"].ToString()%>
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mng" /></td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mem" /></td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts11 += Math.Round(Convert.ToDecimal(dr["yfjj"].ToString()), 2); %>

                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <%--<td style="text-align:right;">
                                        <% hjts6 += Convert.ToDecimal(dr["sbn"].ToString()); %>

                                        <span><%= dr["sbn"].ToString() %></span></td>--%>
                                    <td style="text-align: right;">
                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016 && (RenwuModel.StartDate.Month < 8))
                                                       {
                                                           hjts8 += Convert.ToDecimal(dr["xbn2"].ToString());%>
                                        <span><%= dr["xbn2"].ToString() %></span>
                                        <%}
                                                       else
                                                       {
                                                           hjts8 += Convert.ToDecimal(dr["xbn"].ToString());%>
                                        <span><%= dr["xbn"].ToString() %></span>

                                        <%} %> 
                                            
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <%--<td style="text-align:right;">
                                        <% hjts8 += Convert.ToDecimal(dr["xbn"].ToString()); %>

                                        <span><%= dr["xbn"].ToString() %></span></td>--%>
                                    <td style="text-align: right;">

                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016)
                                                       {
                                                           if (RenwuModel.StartDate.Month >= 8)
                                                           {
                                                               hjts6 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>
                                        <%}
                                                           else
                                                           {
                                                               hjts6 += Convert.ToDecimal(dr["sbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn2"].ToString())) %></span>
                                        <%
                                                           }
                                        %>
                                        <%}
                                                       else
                                                       {
                                                           hjts6 += Convert.ToDecimal(dr["sbn"].ToString());%>
                                        <span><%= dr["sbn"].ToString() %></span>

                                        <%} %> 
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           }
                                           else
                                           {//保存后
                                               if (UnitMemsSaveList.Rows.Count > 0)
                                               {
                                                    
                                                   foreach (System.Data.DataRow dr in UnitMemsSaveList.Rows)
                                                   {
                                %>

                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts += Convert.ToDecimal(dr["jbgz"].ToString()); %>

                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>
                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["gzsj"].ToString()))%></span>--%>
                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" value="<%= Math.Round(Convert.ToDecimal(dr["gzsj"].ToString()))%>" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["gzsj"].ToString()))%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts2 += Convert.ToDecimal(dr["pjgz"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2)%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2)%>
                                    </td>

                                    <td style="text-align: right;"><span><%= Math.Round(Convert.ToDecimal(dr["bmpf"].ToString()), 2)%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID"].ToString()%></span>--%>

                                        <%= dr["OrderID"].ToString()%>
                                    </td>
                                    <td style="text-align: right;"><span><%= dr["MngCount"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <%--<span><%= dr["OrderID2"].ToString()%></span>--%>

                                        <%= dr["OrderID2"].ToString()%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts3 += Convert.ToDecimal(dr["bmnjs"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%=Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()))%>" style="width: 60px;" action="mng" />
                                        <%}
                                           else
                                           { %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()))%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()))%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts4 += Convert.ToDecimal(dr["yxyg"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%=Math.Round(Convert.ToDecimal(dr["yxyg"].ToString()))%>" style="width: 60px;" action="mem" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["yxyg"].ToString()))%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts10 += Convert.ToDecimal(dr["hjyf"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["hj"].ToString()))%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["hjyf"].ToString()))%>

                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts11 += Convert.ToDecimal(dr["yfjj"].ToString()); %>

                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts5 += Convert.ToDecimal(dr["hj"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["hj"].ToString()))%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["hj"].ToString()))%>
                                    </td>

                                    <td style="text-align: right;"><span><%=dr["beizhu"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts8 += Math.Round(Convert.ToDecimal(dr["beforyear2"].ToString())); %>

                                        <span><%=dr["beforyear2"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts9 += Math.Round(Convert.ToDecimal(dr["chazhi2"].ToString())); %>

                                        <span><%=Math.Round(Convert.ToDecimal(dr["chazhi2"].ToString()))%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts6 += Math.Round(Convert.ToDecimal(dr["beforyear"].ToString())); %>

                                        <span><%=dr["beforyear"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts7 += Math.Round(Convert.ToDecimal(dr["chazhi"].ToString())); %>

                                        <span><%= Math.Round(Convert.ToDecimal(dr["chazhi"].ToString()))%></span></td>

                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           } %>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts),2) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts2),2) %></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts3)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts4)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts10)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts11),2)%></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts5)) %></td>
                                    <td></td>

                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts8)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts9)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts6)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts7)) %></td>

                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                        <%} %>
                        <% if (IsTeShu2)
                           { %>
                        <!--特殊部门调整-->
                        <table class="table table-bordered table-hover" id="tbDataTwo2">
                            <thead>
                                <tr>
                                    <th>部门：</th>
                                    <th style="width: 100px;"><span id="spunitName2"><%= KaoHeUnit.unit_Name%></span></th>
                                    <th colspan="3">

                                        <%  //未归档
                               if (!IsBak)
                               {
                                   if (!IsSubmit)
                                   {%>
                                        <a href="#" class="btn btn-sm red" id="btnSave4">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit4">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm red" ID="Button8" runat="server" Text="导出数据" OnClick="Button8_Click" /><br /> 
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <%}
                                   else
                                   {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm red" ID="Button1" runat="server" Text="导出数据" OnClick="Button1_Click" />
                                        <%
                                       if (this.UserShortName == "admin")
                                       {%>
                                        <a href="#" class="btn btn-sm yellow" id="btnTeshu2">发起归档</a>
                                        <%}
                                   }
                                }
                               else
                               {%>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <asp:Button CssClass="btn btn-sm default" ID="Button5" runat="server" Text="导出数据" OnClick="Button1_Click" />
                                        <a href="#" class="btn btn-sm default">已归档</a>
                                        <%} %>

                                    </th>
                                    <th colspan="10"></th>
                                </tr>
                                <tr>
                                    <th style="width: 70px;"><a href="#" id="btnDefault" title="点击恢复默认排序">姓名</a></th>
                                    <th style="width: 70px;">基本工资</th>
                                    <th style="width: 90px;">工作时间/月</th>
                                    <th style="width: 90px;">平均月工资</th>
                                    <th style="width: 120px;">部门内经<br />
                                        理调整</th>
                                    <th style="width: 100px;">优秀员工奖</th>
                                    <th style="width: 70px;">合计(含预发)</th>
                                    <th style="width: 90px;">预发奖金</th>
                                    <th style="width: 70px;">合计</th>
                                    <th style="width: 70px;">工资倍数</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                        <%}
                                           else
                                           { %>
                                        <%= RenwuModel.StartDate.Year %><br />
                                        上半年<br />
                                        (含预发)
                                        <%} %>
                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th style="width: 100px;">
                                        <% if (RenwuModel.StartDate.Month < 7)
                                           { %>
                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        上半年<br />
                                        (含预发)
                                    <%}
                                           else
                                           { %>

                                        <%= RenwuModel.StartDate.Year-1 %><br />
                                        下半年<br />
                                        (含预发)
                                    <%} %>

                                    </th>
                                    <th style="width: 70px;">差值</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                           decimal? hjts = 0;
                                           decimal? hjts2 = 0;
                                           decimal? hjts3 = 0;
                                           decimal? hjts4 = 0;
                                           decimal? hjts5 = 0;
                                           decimal? hjts6 = 0;
                                           decimal? hjts7 = 0;
                                           decimal? hjts8 = 0;
                                           decimal? hjts9 = 0;
                                           //hjyf
                                           decimal? hjts10 = 0;
                                           //yfjj
                                           decimal? hjts11 = 0;


                                           if (!IsSave)
                                           {
                                               if (UnitMemsList.Rows.Count > 0)
                                               {

                                                   foreach (System.Data.DataRow dr in UnitMemsList.Rows)
                                                   { %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts += Convert.ToDecimal(dr["jbgz"].ToString()); %>
                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                           DateTime curDate = DateTime.Now;
                                           if (!string.IsNullOrEmpty(dr["mem_InTime"].ToString()))
                                           {
                                               curDate = Convert.ToDateTime(dr["mem_InTime"].ToString());
                                           }
                                           if (curDate > RenwuModel.StartDate && curDate < RenwuModel.EndDate)
                                           {
                                               //任务截止日期
                                               var endDate = RenwuModel.EndDate;
                                               //自然天
                                               int days = new TimeSpan(endDate.Ticks - curDate.Ticks).Days + 1;
                                               decimal? result = (decimal)(6 * days) / 183;
                                        %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(result),2) %></span>--%>
                                        <input type="text" value="<%= Math.Round(Convert.ToDecimal(result),2) %>" style="width: 80px;" />
                                        <%}
                                           else if (curDate <= RenwuModel.StartDate)
                                           { %>
                                        <%--<span>6</span>--%>
                                        <input type="text" value="6" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <%--<span>0</span>--%>
                                        <input type="text" value="0" style="width: 80px;" />
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts2 += Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()), 2); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2) %></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2) %>
                                    </td>

                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mng" /></td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="0" style="width: 60px;" action="mem" /></td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts11 += Math.Round(Convert.ToDecimal(dr["yfjj"].ToString()), 2); %>

                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span>0</span>--%>
                                        0
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <%--<td style="text-align:right;">
                                        <% hjts6 += Convert.ToDecimal(dr["sbn"].ToString()); %>

                                        <span><%= dr["sbn"].ToString() %></span></td>--%>
                                    <td style="text-align: right;">
                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016 && (RenwuModel.StartDate.Month < 8))
                                                       {
                                                           hjts8 += Convert.ToDecimal(dr["xbn2"].ToString());%>
                                        <span><%= dr["xbn2"].ToString() %></span>
                                        <%}
                                                       else
                                                       {
                                                           hjts8 += Convert.ToDecimal(dr["xbn"].ToString());%>
                                        <span><%= dr["xbn"].ToString() %></span>

                                        <%} %> 
                                            
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <%--<td style="text-align:right;">
                                        <% hjts8 += Convert.ToDecimal(dr["xbn"].ToString()); %>

                                        <span><%= dr["xbn"].ToString() %></span></td>--%>
                                    <td style="text-align: right;">

                                        <% 
                                                       if ((RenwuModel.StartDate.Year - 1) < 2016)
                                                       {
                                                           if (RenwuModel.StartDate.Month >= 8)
                                                           {
                                                               hjts6 += Convert.ToDecimal(dr["xbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["xbn2"].ToString())) %></span>
                                        <%}
                                                           else
                                                           {
                                                               hjts6 += Convert.ToDecimal(dr["sbn2"].ToString());
                                        %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["sbn2"].ToString())) %></span>
                                        <%
                                                           }
                                        %>
                                        <%}
                                                       else
                                                       {
                                                           hjts6 += Convert.ToDecimal(dr["sbn"].ToString());%>
                                        <span><%= dr["sbn"].ToString() %></span>

                                        <%} %> 
                                    </td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           }
                                           else
                                           {//保存后
                                               if (UnitMemsSaveList.Rows.Count > 0)
                                               {

                                                   foreach (System.Data.DataRow dr in UnitMemsSaveList.Rows)
                                                   {
                                %>

                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts += Convert.ToDecimal(dr["jbgz"].ToString()); %>

                                        <%--<span><%= dr["jbgz"].ToString()%></span>--%>

                                        <%= Convert.ToDecimal(dr["jbgz"].ToString())%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["gzsj"].ToString()))%></span>--%>
                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" value="<%= Math.Round(Convert.ToDecimal(dr["gzsj"].ToString()))%>" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["gzsj"].ToString()))%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts2 += Convert.ToDecimal(dr["pjgz"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2)%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2)%>
                                    </td>

                                    <td style="text-align: right;">
                                        <% hjts3 += Convert.ToDecimal(dr["bmnjs"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%=Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()))%>" style="width: 60px;" action="mng" />
                                        <%}
                                           else
                                           { %>
                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()))%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["bmnjs"].ToString()))%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts4 += Convert.ToDecimal(dr["yxyg"].ToString()); %>

                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%=Math.Round(Convert.ToDecimal(dr["yxyg"].ToString()))%>" style="width: 60px;" action="mem" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["yxyg"].ToString()))%></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts10 += Convert.ToDecimal(dr["hjyf"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["hj"].ToString()))%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["hjyf"].ToString()))%>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts11 += Convert.ToDecimal(dr["yfjj"].ToString()); %>

                                        <span><%= dr["yfjj"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjts5 += Convert.ToDecimal(dr["hj"].ToString()); %>

                                        <%--<span><%= Math.Round(Convert.ToDecimal(dr["hj"].ToString()))%></span>--%>

                                        <%= Math.Round(Convert.ToDecimal(dr["hj"].ToString()))%>
                                    </td>

                                    <td style="text-align: right;"><span><%=dr["beizhu"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts8 += Math.Round(Convert.ToDecimal(dr["beforyear2"].ToString())); %>

                                        <span><%=dr["beforyear2"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts9 += Math.Round(Convert.ToDecimal(dr["chazhi2"].ToString())); %>

                                        <span><%=Math.Round(Convert.ToDecimal(dr["chazhi2"].ToString()))%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts6 += Math.Round(Convert.ToDecimal(dr["beforyear"].ToString())); %>

                                        <span><%=dr["beforyear"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hjts7 += Math.Round(Convert.ToDecimal(dr["chazhi"].ToString())); %>

                                        <span><%= Math.Round(Convert.ToDecimal(dr["chazhi"].ToString()))%></span></td>

                                    <td></td>
                                </tr>
                                <%}
                                               }
                                           } %>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts),2) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts2),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts3)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts4)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts10)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts11)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts5)) %></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts8)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts9)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts6)) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hjts7)) %></td>

                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                        <%} %>

                        <!--离职人员-->
                        <table class="table table-bordered table-hover" id="tbDataOne2">
                            <thead>
                                <tr>
                                    <th>部门：</th>
                                    <th><span><%= KaoHeUnit.unit_Name%></span></th>
                                    <th colspan="2">
                                        <% if (!IsSubmitOut)
                                           { %>
                                        <%--<a href="#" class="btn btn-sm red" id="btnSaveOut">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmitOut">确认提交</a>--%>
                                        <%}
                                           else
                                           { %>
                                        <%--<a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>--%>


                                        <%} %>
                                        <asp:Button CssClass="btn btn-sm red" ID="btn_Export3" runat="server" Text="导出数据" OnClick="btn_Export3_Click" />
                                    </th>
                                    <th colspan="9"></th>
                                </tr>
                                <tr>
                                    <th style="width: 90px;">离职员工</th>
                                    <th style="width: 130px;">入职时间</th>
                                    <th style="width: 130px;">离职时间</th>
                                    <th style="width: 90px;">基本工资</th>
                                    <th style="width: 90px;">预发奖金</th>
                                    <th style="width: 90px;">平均月工资</th>
                                    <th style="width: 100px;">部门经理调整</th>
                                    <th style="width: 90px;">工资倍数</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    decimal? hj = 0;
                                    decimal? hj2 = 0;
                                    decimal? hj3 = 0;
                                    decimal? hj4 = 0;
                                    if (!IsSaveOut)
                                    {
                                        if (UnitMemsOutList.Rows.Count > 0)
                                        {

                                            foreach (System.Data.DataRow dr in UnitMemsOutList.Rows)
                                            { %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;"><span><%= dr["mem_InTime"]!=DBNull.Value? dr["mem_InTime"].ToString():""%></span></td>
                                    <td style="text-align: right;"><span><%= dr["mem_OutTime"]!=DBNull.Value?dr["mem_OutTime"].ToString():""%></span></td>
                                    <td style="text-align: right;">
                                        <% hj += Convert.ToDecimal(dr["jbgz"].ToString()); %>
                                        <span><%= dr["jbgz"].ToString()%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <% hj2 += Convert.ToDecimal(dr["yfjj"].ToString()); %>
                                        <span><%= dr["yfjj"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hj3 += Convert.ToDecimal(dr["pjgz"].ToString()); %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2)%></span></td>
                                    <td style="text-align: right;">
                                        <input type="text" name="name" value="" style="width: 60px;" action="mng" /></td>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td></td>
                                </tr>
                                <%}
                                        }
                                    }
                                    else
                                    {
                                        if (UnitMemsSaveOutList.Rows.Count > 0)
                                        {

                                            foreach (System.Data.DataRow dr in UnitMemsSaveOutList.Rows)
                                            {
                                %>
                                <tr>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td style="text-align: right;"><span><%=  dr["mem_InTime"]!=DBNull.Value? Convert.ToDateTime(dr["mem_InTime"].ToString()).ToShortDateString():""%></span></td>
                                    <td style="text-align: right;"><span><%= dr["mem_OutTime"]!=DBNull.Value?Convert.ToDateTime(dr["mem_OutTime"].ToString()).ToShortDateString():""%></span></td>
                                    <td style="text-align: right;">
                                        <% hj += Convert.ToDecimal(dr["jbgz"].ToString()); %>
                                        <span><%= dr["jbgz"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hj2 += Convert.ToDecimal(dr["yfjj"].ToString()); %>
                                        <span><%= dr["yfjj"].ToString()%></span></td>
                                    <td style="text-align: right;">
                                        <% hj3 += Convert.ToDecimal(dr["pjgz"].ToString()); %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["pjgz"].ToString()),2)%></span></td>
                                    <td style="text-align: right;">
                                        <% hj4 += Convert.ToDecimal(dr["bmjltz"].ToString()); %>
                                        <% if (dr["Stat"].ToString() == "0")
                                           { %>
                                        <input type="text" name="name" value="<%= Math.Round(Convert.ToDecimal(dr["bmjltz"].ToString())) %>" style="width: 60px;" action="mng" />
                                        <%}
                                           else
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["bmjltz"].ToString())) %></span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;"><span><%= dr["beizhu"].ToString()%></span></td>
                                    <td></td>
                                </tr>
                                <%}
                                        }
                                    }%>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计：</td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hj),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hj2),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hj3),2) %></td>
                                    <td style="text-align: right;"><%= Math.Round(Convert.ToDecimal(hj4)) %></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%= UserSysNo%>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="<%= RenwuNo%>" id="hidRenwuno" />
    <input type="hidden" name="name" value="<%= KaoHeUnitSysNo%>" id="hidUnitID" />
    <input type="hidden" name="name" value="<%= KaoHeUnitID%>" id="hidKaoheUnitID" />
    <input type="hidden" name="name" value="<%= IsSave %>" id="hidIsSaved" />
    <input type="hidden" name="name" value="<%= IsCheck %>" id="hidIsCheck" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />

    <input type="hidden" id="hidIsArch" value="<%= IsArch %>" />
    <input type="hidden" id="hidIsFangAn" value="<%= IsFangAn %>" />
    <input type="hidden" id="hidIsTeShu" value="<%= IsTeShu %>" />
    <input type="hidden" id="hidIsTeShu2" value="<%= IsTeShu2 %>" />

    <input type="hidden" name="name" value="<%= IsSubmit %>" id="hidIsSubmit" />

    <script>
        mngAllot.init();
    </script>
</asp:Content>
