﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectBpmAllot.aspx.cs" Inherits="TG.Web.ProjBpm.ProjectBpmAllot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/ProjBpm/ProjectAllot.js?v=20170117"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>项目奖金计算</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>项目奖金计算</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>项目奖金计算
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <!--项目总体产值-->
                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <%
                                        if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                             <th colspan="4"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)项目奖金信息</th>
                                       <% }
                                        else
                                        {%>
                                             <th colspan="4"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)项目奖金信息</th>
                                        <%}
                                         %>
                                   
                               
                               
                                </tr>
                                <tr>
                                    <th style="width: 150px;">项目</th>
                                    <th style="width: 150px;">计算值</th>
                                    <th style="width: 150px;">最终值</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>虚拟总产值</td>
                                    <td style="text-align: right;"><span id="spXnzcz" class="badge badge-danger"><%= Math.Round(Convert.ToDecimal(ProjVirAllCount),4) %></span>万</td>
                                    <td style="text-align: right;"><span id="spXnzcz2" class="badge badge-success"><%= Math.Round(Convert.ToDecimal(ProjVirAllCount),4) %></span>万</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>完成比例</td>
                                    <td style="text-align: right;"><span id="spWcBl" class="badge badge-danger"><%= DoneScale %></span>%</td>
                                    <td style="text-align: right;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>项目奖金计算基数</td>
                                    <td style="text-align: right;"><span id="spXmjjjs" class="badge badge-danger"><%= Math.Round(Convert.ToDecimal(ProjVirAllCount*DoneScale*100),2) %></span></td>
                                    <td style="text-align: right;"><span id="spXmjjjs2" class="badge badge-success"><%= Math.Floor(Convert.ToDecimal(ProjVirAllCount*DoneScale*100)) %></span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>奖金计提比例</td>
                                    <td style="text-align: right;"><span id="spJjbl" class="badge badge-danger"><%=JiangJinJTBL %></span>%</td>
                                    <td style="text-align: right;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>计算奖金总数</td>
                                    <td style="text-align: right;"><span id="spJsjjzs" class="badge badge-danger"><%=Math.Round(Convert.ToDecimal(ProjVirAllCount*DoneScale*JiangJinJTBL),2)%></span></td>
                                    <td style="text-align: right;"><span id="spJsjjzs2" class="badge badge-success"><%=Math.Round(Convert.ToDecimal(ProjVirAllCount*DoneScale*JiangJinJTBL),2)%></span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>项目整体绩效</td>
                                    <td style="text-align: right;"><span id="spXmztjx" class="badge badge-danger"><%= ProjNameModel.AllScale %></span></td>
                                    <td style="text-align: right;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>实际奖金总数</td>
                                    <% decimal? sjcount = 0; %>
                                    <td style="text-align: right;"><span id="spSjjjzs" class="badge badge-danger"><%= sjcount=Math.Round(Convert.ToDecimal(ProjVirAllCount*DoneScale*JiangJinJTBL*ProjNameModel.AllScale),2) %></span></td>
                                    <td style="text-align: right;"><span id="spSjjjzs2" class="badge badge-success"><%= Math.Round(Convert.ToDecimal(sjcount),2) %></span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>方案深化计提比例</td>
                                    <td style="text-align: right;"><span id="spFashjtbl" class="badge badge-danger"><%=FAJiangJinJTBL %></span>%</td>
                                    <td style="text-align: right;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>方案深化奖金</td>
                                    <% decimal? facount = 0; %>
                                    <td style="text-align: right;"><span id="spFashjj" class="badge badge-danger"><%= FASHJiangJin=facount=Math.Round(Convert.ToDecimal(sjcount*FAJiangJinJTBL*(decimal)0.01),2) %></span></td>
                                    <td style="text-align: right;"><span id="spFashjj2" class="badge badge-success"><%= Math.Round(Convert.ToDecimal(sjcount*FAJiangJinJTBL*(decimal)0.01),2) %></span></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>主持人资金计提比例</td>
                                    <td style="text-align: right;"><span id="spZczjjtbl" class="badge badge-danger"><%= ZCRJiangJinJTBL %></span></td>
                                    <td style="text-align: right;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>主持人绩效</td>
                                    <td style="text-align: right;"><span id="spZcjx" class="badge badge-danger"><%= Math.Round(Convert.ToDecimal(ZCRJiXiao),2) %></span></td>
                                    <td style="text-align: right;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>主持人奖金</td>
                                    <% decimal? zcrjj = 0;
                                       //if (!IsSaveZcr)
                                       if (!IsSubmit)
                                       { %>
                                    <td style="text-align: right;"><span id="spZcjj" class="badge badge-danger"><%= ZCRenJiangJin=zcrjj=Math.Round(Convert.ToDecimal(sjcount*ZCRJiangJinJTBL*ZCRJiXiao*(decimal)0.01),2) %></span></td>
                                    <td style="text-align: right;"><span id="spZcjj2" class="badge badge-success"><%= Math.Round(Convert.ToDecimal(sjcount*ZCRJiangJinJTBL*ZCRJiXiao*(decimal)0.01),2) %></span></td>
                                    <%}
                                       else
                                       {
                                           ZCRenJiangJin = KaoHeProjModel != null ? KaoHeProjModel.Zcjj : 0;
                                           zcrjj = KaoHeProjModel != null ? KaoHeProjModel.Zcjj : 0;
                                    %>
                                    <td style="text-align: right;"><span id="spZcjj" class="badge badge-danger"><%= KaoHeProjModel!=null?KaoHeProjModel.Zcjj:0 %></span></td>
                                    <td style="text-align: right;"><span id="spZcjj2" class="badge badge-success"><%= KaoHeProjModel!=null?KaoHeProjModel.Zcjj2:0 %></span></td>
                                    <%} %>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>项目组奖金</td>
                                    <%
                                        //if (!IsSaveZcr)
                                        if (!IsSubmit)
                                        { %>
                                    <td style="text-align: right;"><span id="spXmjj" class="badge badge-danger"><%= Math.Round(Convert.ToDecimal(sjcount-facount-zcrjj),2) %></span></td>
                                    <td style="text-align: right;"><span id="spXmjj2" class="badge badge-success"><%= ProjCount=Math.Round(Convert.ToDecimal(sjcount-facount-zcrjj))%></span></td>
                                    <%}
                                        else
                                        {
                                            ProjCount = KaoHeProjModel != null ? KaoHeProjModel.Xmjj2 : 0;
                                    %>
                                    <td style="text-align: right;"><span id="spXmjj" class="badge badge-danger"><%= KaoHeProjModel!=null?KaoHeProjModel.Xmjj:0 %></span></td>
                                    <td style="text-align: right;"><span id="spXmjj2" class="badge badge-success"><%= KaoHeProjModel!=null?KaoHeProjModel.Xmjj2:0 %></span></td>
                                    <%} %>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <!---专业产值-->
                        <table class="table table-bordered table-hover" id="tbDataTwo">
                            <thead>
                                <tr>
                                    <th colspan="<%= IsNewVersion ==true?11:9 %>">项目奖金信息</th>
                                </tr>
                                <tr>
                                    <th style="width: 80px;">专业</th>
                                    <th style="width: 150px;">公司规定专业比例</th>
                                    <th style="width: 80px;">主持人</th>
                                    <%if (IsNewVersion)
                                      { %>
                                    <th style="width: 80px">部门经理</th>
                                    <%} %>

                                    <th style="width: 80px;">总建筑师</th>
                                    <th style="width: 120px;">专业奖金-计算值</th>
                                    <th style="width: 120px;">专业奖金-最终值</th>
                                    <th style="width: 130px;">主持人签字</th>

                                    <%if (IsNewVersion)
                                      { %>
                                    <th style="width: 130px;">部门经理签字</th>
                                    <%} %>

                                    <th style="width: 130px;">总建筑师签字</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    
                                    decimal? hj = 0;
                                    decimal? hj2 = 0;
                                    decimal? hj3 = 0;
                                    decimal? hj4 = 0;
                                    decimal? hj5 = 0;
                                    //部门经理合计
                                    decimal? hj6 = 0;

                                    if (KaoHeSpeList.Count > 0)
                                    {

                                        KaoHeSpeList.ForEach(c =>
                                        {%>

                                <tr>
                                    <td><span speid="<%= c.SpeID %>" class="badge badge-info"><%= c.SpeName%></span></td>
                                    <td style="text-align: right;">
                                        <% hj += c.Persentold; %>
                                        <span><%= c.Persentold%></span>%</td>
                                    <td style="text-align: right;">
                                        <% hj2 += c.Persent1; %>
                                        <span><%= c.Persent1%></span>%</td>

                                    <%if (IsNewVersion)
                                      { %>
                                    <td style="text-align: right;">
                                        <% hj6 += c.Persent3; %>
                                        <span><%= c.Persent3%></span>%</td>
                                    <%} %>

                                    <td style="text-align: right;">
                                        <% hj3 += c.Persent2; %>
                                        <span><%= c.Persent2%></span>%</td>
                                    <td style="text-align: right;">
                                        <% hj4 += c.JiangJin; %>
                                        <span><%= Convert.ToDecimal(c.JiangJin).ToString("f2")%></span></td>
                                    <td style="text-align: right;">
                                        <% hj5 += Math.Round(Convert.ToDecimal(c.JiangJinEnd)); %>
                                        <span><%= Math.Round(Convert.ToDecimal(c.JiangJinEnd))%></span></td>
                                    <td><span><%= c.InserUserID1%>[<%= Convert.ToDateTime(c.InserDate1).ToShortDateString() %>]</span></td>

                                    <%if (IsNewVersion)
                                      { %>
                                    <td><span><%= c.InserUserID3%>[<%= Convert.ToDateTime(c.InserDate3).ToShortDateString() %>]</span></td>
                                    <%} %>

                                    <td><span><%= c.InserUserID2%>[<%= Convert.ToDateTime(c.InserDate2).ToShortDateString() %>]</span></td>
                                    <td></td>
                                </tr>
                                <%});
                                    }
                                %>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td style="text-align: right;"><%= hj.ToString() %>%</td>
                                    <td style="text-align: right;"><%= hj2.ToString() %>%</td>

                                    <%if (IsNewVersion)
                                      { %>
                                    <td style="text-align: right;"><%= hj6.ToString() %>%</td>
                                    <%} %>

                                    <td style="text-align: right;"><%= hj3.ToString() %>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hj4).ToString("f2") %></td>
                                    <td style="text-align: right;"><%= hj5.ToString() %></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <%if (IsNewVersion)
                                      { %>
                                    <td></td>
                                    <%} %>
                                </tr>
                            </tfoot>
                        </table>
                        <!--主持人绩效-->
                        <table class="table table-bordered table-hover" id="tbDataThree">
                            <thead>
                                <tr>
                                    <th colspan="7">主持人绩效</th>
                                </tr>
                                <tr>
                                    <th style="width: 80px;">专业</th>
                                    <th style="width: 150px;">专业比例</th>
                                    <th style="width: 80px;">专业打分</th>
                                    <th style="width: 130px;">主持人绩效-计算值</th>
                                    <th style="width: 130px;">主持人绩效-最终值</th>
                                    <th style="width: 150px;">调整专业比例</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    decimal? hjspe = 0;
                                    if (!IsSaveZcr)
                                    {
                                        if (KaoHeSpeJXList.Count > 0)
                                        {
                                            bool vspan = true;
                                            bool vspan2 = true;
                                            KaoHeSpeJXList.ForEach(c =>
                                            {   %>
                                <tr>
                                    <td><span speid="<%= c.SpeID %>" class="badge badge-info"><%= c.SpeName %></span></td>
                                    <td style="text-align: right;">
                                        <% hjspe += c.Persent2;%>
                                        <span><%= c.Persent2 %></span>%</td>
                                    <td style="text-align: right;"><span><%= c.Jixiao %></span></td>
                                    <% if (vspan)
                                       { %>
                                    <td style="text-align: right;" rowspan="5" valign="middle"><span><%= Convert.ToDecimal(ZCRJiXiao).ToString("f2") %></span></td>
                                    <%
                                           vspan = false;
                                       } %>
                                    <% if (vspan2)
                                       { %>
                                    <td style="text-align: right;" rowspan="5" valign="middle"><span><%= Convert.ToDecimal(ZCRJiXiao).ToString("f2") %></span></td>
                                    <%
                                           vspan2 = false;
                                       } %>
                                    <td>
                                        <input type="text" speid="<%= c.SpeID %>" name="name" value="<%= c.Persent2 %>" style="width: 60px; background-color: #FFC; border: solid 1px black;" />%</td>
                                    <td></td>
                                </tr>
                                <%});
                                        }
                                    }
                                    else
                                    {
                                        if (KaoHeZcrList.Count > 0)
                                        {
                                            bool vspan = true;
                                            bool vspan2 = true;
                                            KaoHeZcrList.ForEach(c =>
                                            {   
                                %>
                                <tr>
                                    <td><span speid="<%= c.SpeID %>" class="badge badge-info"><%= c.SpeName %></span></td>
                                    <td style="text-align: right;">
                                        <% hjspe += c.Persent2;%>
                                        <span><%= c.Persent2 %></span>%</td>
                                    <td style="text-align: right;"><span><%= c.Jixiao %></span></td>
                                    <% if (vspan)
                                       { %>
                                    <td style="text-align: right;" rowspan="5" valign="middle"><span><%= Convert.ToDecimal(ZCRJiXiao).ToString("f2")  %></span></td>
                                    <%
                                           vspan = false;
                                       } %>
                                    <% if (vspan2)
                                       { %>
                                    <td style="text-align: right;" rowspan="5" valign="middle"><span><%= Convert.ToDecimal(ZCRJiXiao).ToString("f2")  %></span></td>
                                    <%
                                           vspan2 = false;
                                       } %>
                                    <td>
                                        <% if (IsSubmit)
                                           { %>
                                        <%--<input type="text" speid="<%= c.SpeID %>" name="name" value="<%= c.PersentEnd %>" disabled="disabled" style="width: 60px; background-color: #FFC; border: solid 1px black;" />%--%>
                                        <%}
                                           else
                                           { %>
                                        <input type="text" speid="<%= c.SpeID %>" name="name" value="<%= c.PersentEnd %>" style="width: 60px; background-color: #FFC; border: solid 1px black;" />% 
                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                                <%});
                                        }
                                    }%>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td style="text-align: right;"><%= hjspe.ToString() %>%</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                        <!---员工最后奖金-->
                        <table class="table table-bordered table-hover" id="tbDataFour">
                            <thead>
                                <tr>
                                    <th colspan="9">员工项目奖金

                                        <!-- 开放admin 修改审批后的人员比例--->
                                        <% if (UserShortName == "admin")
                                           { %>
                                        <%--<a href="#full" data-toggle="modal" class="btn btn-xs red" id="btnEditMemsPrt">修改考核人比例</a>--%>
                                        <%} %>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 80px;">专业</th>
                                    <th style="width: 150px;">比例</th>
                                    <th style="width: 80px;">奖金/元</th>
                                    <th style="width: 140px;">负责人</th>
                                    <th style="width: 140px;">部门经理</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    decimal? hjend = 0;
                                    decimal? hjend2 = 0;
                                    decimal? hjend3 = 0;

                                    if (KaoHeSpeMemsList.Count > 0)
                                    {
                                        KaoHeSpeMemsList.ForEach(c =>
                                        {%>
                                    <tr>
                                    <td>
                                        <span class="badge badge-info" id="<%=c.SpeID %>"><%= c.SpeName%></span>
                                     
                                       <%
                                            if (this.UserShortName == "zhangcuizhen")
                                            { %>
                                        <%
                                                if (c.SpeID == 1)
                                            { %>
                                        
                                              <a  speid="-1" spename="<%= c.SpeName %>" memname="<%= Usermemname%>" memid="<%= Usermemid %>" href="#" class="badge badge-danger" id="btnSaveCh">撤回</a>
                                           <%}

                                         if (c.SpeID== 2)
                                            { %>                                     
                                        <a  speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" memname="<%= c.InserUserID2%>" memid="<%= c.MemID %>" href="#" class="badge badge-danger" id="btnSaveChs">撤回</a>
                                    
                                         <%}    
                                                                                  
                                            if (c.SpeID== 3)
                                            { %>                                     
                                           <a  speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" memname="<%= c.InserUserID2%>" memid="<%= c.MemID %>" href="#" class="badge badge-danger" id="btnSaveChs">撤回</a>
                                    
                                         <%} 
                                           if (c.SpeID== 4)
                                            { %>                                     
                                        <a  speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" memname="<%= c.InserUserID2%>" memid="<%= c.MemID %>" href="#" class="badge badge-danger" id="btnSaveCh">撤回</a>
                                    
                                         <%} 
                                          if (c.SpeID== 5)
                                            { %>                                     
                                        <a  speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" memname="<%= c.InserUserID2%>" memid="<%= c.MemID %>" href="#" class="badge badge-danger" id="btnSaveCh">撤回</a>
                                    
                                         <%} 
                                                                                      
                                          %>


                                        <%}
                                            
                                             %> 
                                          
                                    </td>
                                    <td style="text-align: right;">
                                        <% hjend += c.Persent2; %>
                                        <%= c.Persent2%>%</td>
                                    <td style="text-align: right;">
                                        <% hjend2 += Math.Round((decimal)c.JiangJinEnd);%>
                                        <%= Math.Round((decimal)c.JiangJinEnd)%></td>
                                    <td><%= c.InserUserID1%>[<%= Convert.ToDateTime(c.InserDate1).ToShortDateString()%>]</td>
                                    <td><%= c.InserUserID2%>[<%= Convert.ToDateTime(c.InserDate2).ToShortDateString()%>]</td>
                                    <td>
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50px;">姓名</th>
                                                    <th style="width: 80px;">制图</th>
                                                    <th style="width: 80px;">校对</th>
                                                    <th style="width: 80px;">审核</th>
                                                    <th style="width: 80px;">方案深化</th>
                                                    <th style="width: 80px;">主持人</th>
                                                    <th style="width: 80px;">个人奖金</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th><%= ZhiTuBl %>%</th>
                                                    <th><%= XiaoDuiBl %>%</th>
                                                    <th><%= ShenHeBl %>%</th>
                                                    <% if (c.SpeID == 1)
                                                       { %>
                                                    <th><%= Math.Round(Convert.ToDecimal(sjcount*FAJiangJinJTBL*(decimal)0.01),0) %></th>
                                                    <th><%= Math.Round(Convert.ToDecimal(sjcount*ZCRJiangJinJTBL*ZCRJiXiao*(decimal)0.01),0) %></th>
                                                    <% }
                                                       else
                                                       {%>
                                                    <th></th>
                                                    <th></th>
                                                    <%} %>
                                                    <th></th>
                                                    <th></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% c.KaoHeMemList.ForEach(m =>
                                               {
                                                   if (c.SpeID == 1)
                                                   { 
                                                %>
                                                <tr class="success">
                                                    <td><span memid="<%= m.MemID %>"><%= m.MemName %></span></td>
                                                    <td style="text-align: right;"><span colbl="<%= ZhiTuBl %>" celval="<%= m.Zt %>"><%= Math.Round(Convert.ToDecimal(m.ZtEnd))%></span></td>
                                                    <td style="text-align: right;"><span colbl="<%= XiaoDuiBl %>" celval="<%= m.Xd %>"><%= Math.Round(Convert.ToDecimal(m.XdEnd)) %></span></td>
                                                    <td style="text-align: right;"><span colbl="<%= ShenHeBl %>" celval="<%= m.Sh %>"><%= Math.Round(Convert.ToDecimal(m.ShEnd))%></span></td>
                                                    <td style="text-align: right;"><span colbl="0" celval="<%= m.Fa %>"><%= Math.Round(Convert.ToDecimal(m.FaEnd))%></span></td>
                                                    <td style="text-align: right;"><span colbl="0" celval="<%= m.Zc %>"><%= Math.Round(Convert.ToDecimal(m.ZcEnd))%></span></td>
                                                    <td style="text-align: right;">
                                                        <% hjend3 += Math.Round(Convert.ToDecimal(m.AllCount));%>
                                                        <span><%= Math.Round(Convert.ToDecimal(m.AllCount))%></span></td>
                                                    <td></td>
                                                </tr>
                                                <%}
                                                   else
                                                   { 
                                                %>
                                                <tr class="success">
                                                    <td><span memid="<%= m.MemID %>"><%= m.MemName %></span></td>
                                                    <td style="text-align: right;"><span colbl="<%= ZhiTuBl %>" celval="<%= m.Zt %>"><%= Math.Round(Convert.ToDecimal(m.ZtEnd))%></span></td>
                                                    <td style="text-align: right;"><span colbl="<%= XiaoDuiBl %>" celval="<%= m.Xd %>"><%= Math.Round(Convert.ToDecimal(m.XdEnd)) %></span></td>
                                                    <td style="text-align: right;"><span colbl="<%= ShenHeBl %>" celval="<%= m.Sh %>"><%= Math.Round(Convert.ToDecimal(m.ShEnd))%></span></td>
                                                    <td style="background-color: gray; text-align: right;"><span style="color: gray;" colbl="0" celval="<%= m.Fa %>"><%= Math.Round(Convert.ToDecimal(m.FaEnd))%></span></td>
                                                    <td style="background-color: gray; text-align: right;"><span style="color: gray;" colbl="0" celval="<%= m.Zc %>"><%= Math.Round(Convert.ToDecimal(m.ZcEnd))%></span></td>
                                                    <td style="text-align: right;">
                                                        <% hjend3 += Math.Round(Convert.ToDecimal(m.AllCount));%>
                                                        <span><%= Math.Round(Convert.ToDecimal(m.AllCount))%></span></td>
                                                    <td></td>
                                                </tr>

                                                <%
                                                   }
                                               }); %>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td></td>
                                </tr>
                                <%});
                                    } %>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td><%= hjend.ToString() %>%</td>
                                    <td><%= hjend2.ToString() %></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <table>
                                            <thead>
                                                <tr>
                                                    <td style="width: 50px;"></td>
                                                    <td style="width: 80px;"></td>
                                                    <td style="width: 80px;"></td>
                                                    <td style="width: 80px;"></td>
                                                    <td style="width: 80px;"></td>
                                                    <td style="width: 80px;"></td>
                                                    <td style="width: 80px;"><%=hjend3.ToString() %></td>
                                                    <td></td>
                                                </tr>
                                            </thead>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9" align="center">
                                        <% if (IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        <%}
                                           else
                                           { %>
                                        <a href="#" class="btn btn-sm red " id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red " id="btnSubmit">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <%} %>
                                        <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default">取消</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <!--考核人员比例设置界面-->
                        <%--<div class="modal fade" id="full" tabindex="-1" data-width="600" style="display: none;">
                            <div class="modal-dialog modal-wide">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">修改考核人比例</h4>
                                    </div>
                                    <div class="modal-body">

                                        <div class="cls_data" id="div_edit" style="display: block;">
                                            <table class="table table-bordered table-hover" id="tbData">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 100px;">姓名</th>
                                                        <th style="width: 100px;">制图</th>
                                                        <th style="width: 100px;">校对</th>
                                                        <th style="width: 100px;">审核</th>
                                                        <th style="width: 100px;">方案深化</th>
                                                        <th style="width: 100px;">主持人</th>
                                                        <th>专业</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <% KaoHeMemsPrt.ForEach(c =>
                                               { %>

                                                    <tr>
                                                        <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                                        <td>
                                                            <input colindex="1" style="width: 80px;" name="name" type="text" value="<%=c.Zt%>" />%</td>
                                                        <td>
                                                            <input colindex="2" style="width: 80px;" name="name" type="text" value="<%=c.Xd%>" />%</td>
                                                        <td>
                                                            <input colindex="3" style="width: 80px;" name="name" type="text" value="<%=c.Sh%>" />%</td>
                                                        <td>
                                                            <input colindex="4" style="width: 80px;" name="name" type="text" value="<%=c.Fa%>" />%</td>
                                                        <td>
                                                            <input colindex="5" style="width: 80px;" name="name" type="text" value="<%=c.Zc%>" />%</td>
                                                        <td><span class="badge badge-warning"><%=c.SpeName%></span></td>
                                                    </tr>

                                                    <%}); %>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="7">
                                                            <button type="button" class="btn red" id="btnSaveMems">保存</button>
                                                            <button type="button" class="btn default" data-dismiss="modal">关闭</button>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>--%>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%=ProID %>" id="hidProID" />
    <input type="hidden" name="name" value="<%=ProKHID %>" id="hidProKHID" />
    <input type="hidden" name="name" value="<%=KaoHeNameID %>" id="hidKaoHeNameID" />
    <input type="hidden" name="name" value="<%=KaoHeTypeID%>" id="hidKaoHeTypeID" />
    <input type="hidden" name="name" value="<%=UserSysNo %>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="<%= Action %>" id="hidAction" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
   
    <input type="hidden" name="name" value="<%= IsSubmit %>" id="hidIsSubmit" />

 
  <%--  <!--项目ID-->
    <input type="hidden" name="name" value="<%= %>" id="hidTempProid" />--%>


    <%-- <!--项目考核ID-->
    <input type="hidden" name="name" value="0" id="hidTempKaoHeProjId" />

    <!--考核名称与类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeProjNameid" />

       <!--考核类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeId" /> --%>


    <script>
        projAllot.init();
    </script>
</asp:Content>
