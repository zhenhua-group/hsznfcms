﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectBpmHisList2.aspx.cs" Inherits="TG.Web.ProjBpm.ProjectBpmHisList2" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
   <%-- <link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="../js/assets/plugins/data-tables/DT_bootstrap.css" />--%>

    <script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/DT_bootstrap.js"></script>


    <script src="../js/ProjBpm/ProjBpmHis2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>项目消息进度</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>项目人员进度</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>项目人员进度
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">考核任务:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                        AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">--选择任务--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 40px;">
                                    <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                                </td>
                                <td style="width: 40px;">
                                    <a class="btn red" id="btnSendMulti">批量发起</a>
                                </td>
                                <td></td>
                            </tr>
                        </table>

                        <%-- <div class="table-scrollable">--%>
                        <table class="table table-hover table-bordered table-full-width" id="tbNameList">
                            <thead>
                                <tr>
                                    <th colspan="18">项目考核进度列表</th>
                                </tr>
                                <tr>
                                    <%--<th style="width: 150px;">项目名称</th>--%>
                                    <th style="width: 35px;">
                                        <input type="checkbox" name="name" value=" " id="chekALL" /></th>
                                    <th style="width: 200px;">考核名称</th>
                                    <th style="width: 70px;">考核人员</th>
                                    <th style="width: 70px;">主持人</th>
                                    <th style="width: 70px;">部门经理</th>
                                    <th style="width: 80px;">建筑负责人</th>
                                    <th style="width: 70px;">部门经理</th>
                                    <th style="width: 80px;">结构负责人</th>
                                    <th style="width: 70px;">部门经理</th>
                                    <th style="width: 80px;">暖通负责人</th>
                                    <th style="width: 70px;">部门经理</th>
                                    <th style="width: 80px;">给排水负责人</th>
                                    <th style="width: 70px;">部门经理</th>
                                    <th style="width: 80px;">电气负责人</th>
                                    <th style="width: 70px;">考核状态</th>
                                    <th style="width: 70px;">详细</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% foreach (System.Data.DataRow dr in HisList.Rows)
                                   {
                                       int colcount = 0;
                                %>
                                <tr>
                                    <%--<td><%= dr["proName"].ToString() %></td>--%>
                                    <td>
                                        <input type="checkbox" name="name" value=" " />
                                    </td>
                                    <td><%= dr["KName"].ToString() %></td>
                                    <td><a pronameid="<%=dr["KID"].ToString() %>" href="#Mems" data-toggle="modal" class="btn default btn-xs red-stripe" action="check">查看</a></td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["ZhuChi"].ToString()) > 0) { colcount++; }%>
                                        <%= Convert.ToInt32(dr["ZhuChi"].ToString())>0?"<span class=\"badge badge-success\">√</span>":"<span class=\"badge badge-danger\">×</span>" %></td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["JianZhuBumen"].ToString()) > 0)
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else if (Convert.ToInt32(dr["JianZhuBumen"].ToString()) == 0)
                                           {%>
                                        <span class="badge badge-danger">×</span>
                                        <%}
                                           else
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-default">未参与</span>
                                        <%}%>
                                    </td>
                                    <td>

                                        <% if (Convert.ToInt32(dr["JianZhu"].ToString()) > 0)
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else if (Convert.ToInt32(dr["JianZhu"].ToString()) == 0)
                                           {%>
                                        <span class="badge badge-danger">×</span>
                                        <%}
                                           else
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-default">未参与</span>
                                        <%}%>
                                            
                                    </td>

                                    <td>


                                        <% if (Convert.ToInt32(dr["JiegouBumen"].ToString()) > 0)
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else if (Convert.ToInt32(dr["JiegouBumen"].ToString()) == 0)
                                           {%>
                                        <span class="badge badge-danger">×</span>
                                        <%}
                                           else
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-default">未参与</span>
                                        <%}%>
                                    </td>
                                    <td>


                                        <% if (Convert.ToInt32(dr["Jiegou"].ToString()) > 0)
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else if (Convert.ToInt32(dr["Jiegou"].ToString()) == 0)
                                           {%>
                                        <span class="badge badge-danger">×</span>
                                        <%}
                                           else
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-default">未参与</span>
                                        <%}%>
                                    </td>
                                    <td>


                                        <% if (Convert.ToInt32(dr["NuantongBumen"].ToString()) > 0)
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else if (Convert.ToInt32(dr["NuantongBumen"].ToString()) == 0)
                                           {%>
                                        <span class="badge badge-danger">×</span>
                                        <%}
                                           else
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-default">未参与</span>
                                        <%}%>
                                    </td>
                                    <td>


                                        <% if (Convert.ToInt32(dr["Nuantong"].ToString()) > 0)
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-success">√</span>
                                        <%}
                                           else if (Convert.ToInt32(dr["Nuantong"].ToString()) == 0)
                                           {%>
                                        <span class="badge badge-danger">×</span>
                                        <%}
                                           else
                                           {
                                               colcount++;
                                        %>
                                        <span class="badge badge-default">未参与</span>
                                        <%}%>
                                    </td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["ShuiBumen"].ToString()) > 0) { colcount++; }%>
                                        <%= Convert.ToInt32(dr["ShuiBumen"].ToString())>0?"<span class=\"badge badge-success\">√</span>":"<span class=\"badge badge-danger\">×</span>" %></td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["Shui"].ToString()) > 0) { colcount++; }%>
                                        <%= Convert.ToInt32(dr["Shui"].ToString())>0?"<span class=\"badge badge-success\">√</span>":"<span class=\"badge badge-danger\">×</span>" %></td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["DianqiBumen"].ToString()) > 0) { colcount++; }%>
                                        <%= Convert.ToInt32(dr["DianqiBumen"].ToString())>0?"<span class=\"badge badge-success\">√</span>":"<span class=\"badge badge-danger\">×</span>" %></td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["Dianqi"].ToString()) > 0) { colcount++; }%>
                                        <%= Convert.ToInt32(dr["Dianqi"].ToString())>0?"<span class=\"badge badge-success\">√</span>":"<span class=\"badge badge-danger\">×</span>" %></td>
                                    <td>
                                        <%= Convert.ToInt32(dr["MsgCount"].ToString())>=5?"<span class=\"badge badge-success\">已提交</span>":"<span class=\"badge badge-danger\">未提交</span>" %>
                                    </td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["MsgCount"].ToString()) >= 5)
                                           { %>
                                        <a href="#" class="btn default btn-xs red-stripe disabled" proid="<%= dr["proID"].ToString()%>" prokhid="<%= dr["PKHID"].ToString()%>" pronameid="<%= dr["KID"].ToString()%>" action="start">发起审批</a>

                                        <%}
                                           else
                                           { %>
                                        <a href="#" class="btn default btn-xs red-stripe" colcount="<%= colcount %>" proid="<%= dr["proID"].ToString()%>" prokhid="<%= dr["PKHID"].ToString()%>" pronameid="<%= dr["KID"].ToString()%>" action="start">发起审批</a>

                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                                <%
                                       } %>
                            </tbody>
                        </table>
                        <%--</div>--%>
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td>
                                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                        CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                        CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                        OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                        PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Always"
                                        SubmitButtonText="GO" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px; height:23px;"
                                        PageSize="20" SubmitButtonClass="btn green">
                                    </webdiyer:AspNetPager>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="Mems" class="modal fade" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">考核人员</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover table-bordered table-full-width" id="tbSetMems">
                        <thead>
                            <tr>
                                <th colspan="4">项目考核人设置
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 120px;"><span speid="-1" class="bage bage-info">总建筑师</span></td>
                                <td style="width: 200px;"></td>
                            </tr>
                            <tr>
                                <td><span speid="-2" class="bage bage-info">主持人</span></td>
                                <td></td>
                            </tr>
                            <% SpecialList.ForEach(c =>
                                   {
                                       if (c.spe_ID == 1)
                                       {%>
                            <tr style="display: none;">
                                <%}
                                       else
                                       { %>
                            <tr>
                                <%} %>
                                <td><span spename="<%= c.spe_Name %>" speid="<%= c.spe_ID %>" class="bage bage-info"><%= c.spe_Name %></span>负责人</td>
                                <td></td>
                            </tr>
                            <%}); %>
                        </tbody>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn default">关闭</button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <script>
        ProjHis.init();
    </script>
</asp:Content>
