﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjDeptAllot.aspx.cs" Inherits="TG.Web.ProjBpm.ProjDeptAllot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />

    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>

    <script src="../js/ProjBpm/ProjDeptAllot.js?v=20170531"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目奖金 <small>项目奖金调整</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目奖金</a><i class="fa fa-angle-right"> </i><a>项目奖金调整</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>项目奖金调整
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">项目部门:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">考核任务:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drpRenwu" runat="server" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 150px;">
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />

                                    <% if (this.UserShortName == "admin")
                                       {

                                           if (!this.IsBak)
                                           {%>

                                    <a href="#" class="btn btn-sm yellow" id="btnBak">发起归档</a>
                                    <%}
                                           else
                                           { %>
                                    <a href="#" class="btn btn-sm default disabled">已归档</a>
                                    <%}
                                       } %>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                        <!--人员列表-->
                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th style="width: 40px; text-align: center;">状态</th>
                                    <th style="width: 80px; text-align: center;">部门名称</th>
                                    <th style="width: 60px; text-align: center;">专业</th>
                                    <th style="width: 60px; text-align: center;">姓名</th>
                                    <th style="width: 120px; text-align: center;">项目奖金计算值</th>
                                    <th style="width: 100px; text-align: center;">部门内排名</th>
                                    <th style="width: 100px; text-align: center;">互评排名</th>
                                    <th style="width: 120px; text-align: center;">项目奖金调整值</th>
                                    <th style="width: 35px; text-align: center;"></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    if (!this.IsBak)
                                    {
                                        if (KaoHeMemAllotList.Count > 0)
                                        {
                                            int index = 1;
                                            KaoHeMemAllotList.ForEach(c =>
                                            {
                                %>
                                <tr>
                                    <td>
                                        <% if (c.IsFired == 0)
                                           { %>
                                        <span isfired="0">在职</span>
                                        <%}
                                           else
                                           { %>

                                        <span isfired="1">离职</span>
                                        <%} %>
                                    </td>
                                    <td>
                                        <span unitid="<%= c.MemUnitID%>"><%= GetUnitName(c.MemUnitID).Trim()%></span>
                                    </td>
                                    <td>
                                        <span speid="<%= c.SpeID%>"><%= c.SpeName%></span>
                                    </td>
                                    <td>
                                        <span memid="<%=c.MemID%>"><%= c.MemName%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= Math.Round(Convert.ToDecimal(c.AllotCount))%></span>
                                    </td>
                                    <td style="text-align: right;"><span><%= c.OrderID%></span></td>
                                    <td style="text-align: right;"><span><%= c.UnitJudgeOrder%></span></td>
                                    <td style="text-align: right;">
                                        <span class="badge badge-danger" allotcount="0" unitid="<%= c.MemUnitID%>" speid="<%= c.SpeID%>"><%= Math.Round(Convert.ToDecimal(c.AllotCountBefore))%></span>
                                    </td>
                                    <td><a href="#addCharge" data-toggle="modal" class="btn default btn-xs blue-stripe" unitid="<%= c.MemUnitID%>" memid="<%=c.MemID%>" alt="">查看</a></td>
                                    <td></td>
                                </tr>

                                <%});
                                        }
                                    }
                                    else
                                    {

                                        KaoHeProjHis.ForEach(c =>
                                        { %>

                                <tr>
                                    <td>
                                        <%= c.IsFired %>
                                    </td>
                                    <td>
                                        <span unitid="<%= c.UnitID%>"><%= c.UnitName%></span>
                                    </td>
                                    <td>
                                        <span speid="0"><%= c.SpeName%></span>
                                    </td>
                                    <td>
                                        <span memid="<%=c.MemID%>"><%= c.UserName%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= Math.Round(Convert.ToDecimal(c.ProjAllot))%></span>
                                    </td>
                                    <td style="text-align: right;"><span><%= c.UnitOrder%></span></td>
                                    <td style="text-align: right;"><span><%= c.HPOrder%></span></td>
                                    <td style="text-align: right;">
                                        <span class="badge badge-danger" allotcount="0" unitid="<%= c.UnitID%>"><%= Math.Round(Convert.ToDecimal(c.ProjAllot2))%></span>
                                    </td>
                                    <td><a href="#addCharge" data-toggle="modal" class="btn default btn-xs blue-stripe" unitid="<%= c.UnitID%>" memid="<%=c.MemID%>" alt="">查看</a></td>
                                    <td></td>
                                </tr>


                                <%});
                                    } %>

                                <tr class="active">
                                    <td colspan="9">其他人员
                                    </td>
                                </tr>

                                <% 
                                    if (!this.IsBak)
                                    {

                                        if (KaoHeMemAllotListOther.Count > 0)
                                        {
                                            int index = 1;
                                            KaoHeMemAllotListOther.ForEach(c =>
                                            {
                                %>
                                <tr>
                                    <td data-other="1">
                                        <% if (c.IsFired == 0)
                                           { %>
                                        <span isfired="0">在职</span>
                                        <%}
                                           else
                                           { %>

                                        <span isfired="1">离职</span>
                                        <%} %>
                                    </td>
                                    <td>
                                        <span unitid="<%= c.MemUnitID%>"><%= GetUnitName(c.MemUnitID) %></span>
                                    </td>
                                    <td>
                                        <span speid="<%= c.SpeID %>"><%= c.SpeName %></span>
                                    </td>
                                    <td>
                                        <span memid="<%= c.MemID %>"><%= c.MemName %></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= Math.Round(Convert.ToDecimal(c.AllotCount)) %></span>
                                    </td>
                                    <%--<td style="text-align:right;"><span><%=index++ %></span></td>--%>
                                    <td style="text-align: right;"><span>0</span></td>
                                    <td style="text-align: right;"><span><%= c.UnitJudgeOrder %></span></td>
                                    <td style="text-align: right;">
                                        <span class="badge badge-danger" allotcount="0" unitid="<%= c.MemUnitID %>" speid="<%= c.SpeID %>"><%= Math.Round(Convert.ToDecimal(c.AllotCountBefore)) %></span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <%});
                                        }
                                    }
                                    else
                                    {

                                        KaoHeProjHisOther.ForEach(c =>
                                        {%>

                                <tr>
                                    <td>
                                        <%= c.IsFired %>
                                    </td>
                                    <td>
                                        <span unitid="<%= c.UnitID%>"><%= c.UnitName%></span>
                                    </td>
                                    <td>
                                        <span speid="0"><%= c.SpeName%></span>
                                    </td>
                                    <td>
                                        <span memid="<%=c.MemID%>"><%= c.UserName%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= Math.Round(Convert.ToDecimal(c.ProjAllot))%></span>
                                    </td>
                                    <td style="text-align: right;"><span><%= c.UnitOrder%></span></td>
                                    <td style="text-align: right;"><span><%= c.HPOrder%></span></td>
                                    <td style="text-align: right;">
                                        <span class="badge badge-danger" allotcount="0" unitid="<%= c.UnitID%>"><%= Math.Round(Convert.ToDecimal(c.ProjAllot2))%></span>
                                    </td>
                                    <td><a href="#addCharge" data-toggle="modal" class="btn default btn-xs blue-stripe" unitid="<%= c.UnitID%>" memid="<%=c.MemID%>" alt="">查看</a></td>
                                    <td></td>
                                </tr>
                                <% });
                                    }%>
                            </tbody>
                        </table>

                        <%-- <table class="table table-bordered table-hover" id="tbDataTwo">
                            <thead>
                                <tr>
                                    <td>项目奖金调整值</td>
                                    <td colspan="6">
                                        <span class="badge badge-danger"><%=Math.Round(Convert.ToDecimal(ProjAllDeptAllotCountAfter)) %></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width: 100px;">部门名称</th>
                                    <th style="width: 100px;">部门合计</th>
                                    <th style="width: 80px;">奖金占比</th>
                                    <th style="width: 80px;">奖金/人数</th>
                                    <th style="width: 80px;">奖金/工资</th>
                                    <th style="width: 80px;">权重</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% if (ProjAllotUnitListAfter.Rows.Count > 0)
                                   {
                                       decimal? jiangbi = 0;
                                       decimal? renbi = 0;
                                       decimal? gongzbi = 0;
                                       decimal? bmhj = 0;
                                       decimal? jjrenbi = 0;
                                       foreach (System.Data.DataRow dr in ProjAllotUnitListAfter.Rows)
                                       { %>
                                <tr>
                                    <td><span unitid="<%= dr["unit_ID"].ToString() %>"><%= dr["unit_Name"].ToString() %></span></td>
                                    <td>
                                        <% if (Math.Round(Convert.ToDecimal(dr["AllotCountBefore"].ToString())) > 0)
                                           { %>

                                        <%= bmhj=Math.Round(Convert.ToDecimal(dr["AllotCountBefore"].ToString())) %>
                                        <%}
                                           else
                                           { %>
                                        <%= bmhj=Math.Round(Convert.ToDecimal(dr["AllotCount"].ToString())) %>
                                        <%} %>
                                    </td>
                                    <td><% if (ProjAllDeptAllotCountAfter != 0)
                                           {
                                               jiangbi = (bmhj / ProjAllDeptAllotCountAfter) * 100; %>
                                        <%= Convert.ToDecimal(jiangbi).ToString("f2") %>
                                        <%}
                                           else
                                           { %>
                                    0.00
                                    <%} %>%
                                    </td>
                                    <td>
                                        <% 
                                           if (Convert.ToDecimal(dr["AllMemCount"].ToString()) != 0)
                                           {
                                               renbi = Convert.ToDecimal(Convert.ToDecimal(dr["MemCount"].ToString()) / Convert.ToDecimal(dr["AllMemCount"].ToString())) * 100;
                                               if (renbi != 0)
                                               {
                                                   jjrenbi = Convert.ToDecimal(jiangbi / renbi) * 100; %>
                                        <%= Convert.ToDecimal(jjrenbi).ToString("f2")%>
                                        <%}
                                               else
                                               {%> 
                                               0
                                              <% }
                                           }
                                           else
                                           { %>
                                        0
                                         <%} %>%
                                    </td>
                                    <td>
                                        <% 
                                           if (Convert.ToDecimal(dr["AllBumemGz"].ToString()) != 0)
                                           {
                                               gongzbi = Convert.ToDecimal(Convert.ToDecimal(dr["BumenGz"].ToString()) / Convert.ToDecimal(dr["AllBumemGz"].ToString())) * 100;
                                        %>

                                        <%= Convert.ToDecimal((jiangbi/gongzbi)*100).ToString("f2") %>
                                        <%}
                                           else
                                           { %>
                                        0
                                        <%} %>%
                                    </td>
                                    <td>
                                        <input type="text" name="name" value="<%=dr["Weights"].ToString() %>" style="width: 60px;" unitid="<%= dr["unit_ID"].ToString()%>" />
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                   } %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <a href="#" class="btn btn-sm red" id="btnSave">确认提交</a>
                                        <a href="#" class="btn btn-sm default">取消</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <table class="table table-bordered table-hover" id="tbDataThree">
                            <thead>
                                <tr>
                                    <td>项目奖金计算值</td>
                                    <td colspan="10">
                                        <span class="badge badge-danger"><%= ProjAllDeptAllotCount %></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width: 100px;">部门名称</th>
                                    <th style="width: 100px;">部门合计</th>
                                    <th style="width: 80px;">奖金占比</th>
                                    <th style="width: 80px;">部门人数</th>
                                    <th style="width: 80px;">人数占比</th>
                                    <th style="width: 80px;">奖金/人数</th>
                                    <th style="width: 80px;">部门工资</th>
                                    <th style="width: 80px;">工资占比</th>
                                    <th style="width: 80px;">奖金/工资</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% if (ProjAllotUnitList.Rows.Count > 0)
                                   {
                                       decimal? jiangbi = 0;
                                       decimal? memsbi = 0;
                                       decimal? bumengz = 0;
                                       decimal? allbumen = 0;
                                       decimal? gzbi = 0;
                                       decimal? jjgzbi = 0;
                                       foreach (System.Data.DataRow dr in ProjAllotUnitList.Rows)
                                       { %>
                                <tr>
                                    <td><%= dr["unit_Name"].ToString() %></td>
                                    <td><%= Math.Round(Convert.ToDecimal(dr["AllotCount"].ToString())) %></td>
                                    <td><% if (ProjAllDeptAllotCount != 0)
                                           {
                                               jiangbi = Convert.ToDecimal(dr["AllotCount"].ToString()) / ProjAllDeptAllotCount * 100;%>
                                        <%= Convert.ToDecimal(jiangbi).ToString("f2")%>
                                        <%}
                                           else
                                           { %>
                                    0.00
                                    <%} %>%
                                    </td>
                                    <td>
                                        <span><%=dr["MemCount"].ToString() %></span>
                                    </td>
                                    <td>
                                        <% memsbi = Convert.ToDecimal(Convert.ToDecimal(dr["MemCount"].ToString()) / Convert.ToDecimal(dr["AllMemCount"].ToString())) * 100; %>
                                        <span><%= Convert.ToDecimal(memsbi).ToString("f2")  %></span>%
                                    </td>
                                    <td>
                                        <% if (memsbi != 0)
                                           { %>
                                        <%= Convert.ToDecimal((jiangbi/memsbi)*100).ToString("f2") %>
                                        <%}
                                           else
                                           { %>
                                    0
                                    <%} %>%
                                    </td>
                                    <td><% bumengz = Convert.ToDecimal(dr["BumenGz"].ToString());%>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["BumenGz"].ToString())) %></span>
                                    </td>
                                    <td>
                                        <%
                                           allbumen = Convert.ToDecimal(dr["AllBumemGz"].ToString());
                                           if (allbumen != 0)
                                           {
                                               gzbi = Convert.ToDecimal(bumengz / allbumen) * 100;
                                        %>
                                        <span><%= Convert.ToDecimal(gzbi).ToString("f2") %></span>
                                        <%}
                                           else
                                           { %>
                                        0.00
                                        <%} %>%
                                    </td>
                                    <td>
                                        <% if (gzbi != 0)
                                           {
                                               jjgzbi = Convert.ToDecimal(jiangbi / gzbi) * 100;
                                        %>

                                        <%= Convert.ToDecimal(jjgzbi).ToString("f2") %>
                                        <%}
                                           else
                                           { %>
                                        0
                                        <%} %>%
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                   } %>
                            </tbody>

                        </table>--%>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>权重填写
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <!--权重调整值-->
                        <table class="table table-bordered table-hover" id="tbDataTwo">
                            <thead>
                                <tr>
                                    <td>项目奖金调整值</td>
                                    <td colspan="5">
                                        <% if (!this.IsBak)
                                           { %>
                                        <span class="badge badge-danger" id="spProjAllotCount"><%=Math.Round(Convert.ToDecimal(ProjAllDeptAllotCountAfter)) %></span>
                                        <%}
                                           else
                                           {%>
                                        <span class="badge badge-danger" id=""><%=WeightListOne[0].ProjCount %></span>

                                        <%} %>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width: 100px; text-align: center;"></th>
                                    <th style="width: 100px; text-align: center;">建筑</th>
                                    <th style="width: 100px; text-align: center;">结构</th>
                                    <th style="width: 100px; text-align: center;">设备</th>
                                    <th style="width: 100px; text-align: center;">电气</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    if (!this.IsBak)
                                    {
                                        if (ProjAllotUnitColListAfter.Rows.Count > 0)
                                        {
                                            decimal? jianzhu = 0;
                                            decimal? jiegou = 0;
                                            decimal? shebei = 0;
                                            decimal? dianq = 0;

                                            foreach (System.Data.DataRow dr in ProjAllotUnitColListAfter.Rows)
                                            {
                                                if (dr["ID"].ToString() == "1")
                                                {%>

                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["JianZhu"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["JieGou"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["SheBei"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["Dianqi"].ToString()%></td>
                                    <td></td>
                                </tr>
                                <%
                                                }
                                                else if (dr["ID"].ToString() == "2")
                                                {
                                                    if (ProjAllDeptAllotCountAfter != 0)
                                                    {
                                                        decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                        decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                        decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                        decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());
                                                        jianzhu = (jz / ProjAllDeptAllotCountAfter) * 100;
                                                        jiegou = (jg / ProjAllDeptAllotCountAfter) * 100;
                                                        shebei = (sb / ProjAllDeptAllotCountAfter) * 100;
                                                        dianq = (dq / ProjAllDeptAllotCountAfter) * 100;
                                                    }
                                                    else
                                                    {
                                                        jianzhu = 0;
                                                        jiegou = 0;
                                                        shebei = 0;
                                                        dianq = 0;
                                                    }
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jianzhu).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jiegou).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(shebei).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dianq).ToString("f2")%>%</td>
                                    <td></td>
                                </tr>

                                <%
                                                }
                                                else if (dr["ID"].ToString() == "3")
                                                {
                                                    decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                    decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                    decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                    decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());

                                                    decimal count = jz + jg + sb + dq;

                                                    decimal jz1 = 0;
                                                    decimal jg1 = 0;
                                                    decimal sb1 = 0;
                                                    decimal dq1 = 0;
                                                    if (count > 0)
                                                    {
                                                        jz1 = (jz / count) * 100;
                                                        jg1 = (jg / count) * 100;
                                                        sb1 = (sb / count) * 100;
                                                        dq1 = (dq / count) * 100;
                                                    }

                                                    decimal? jianzhu2 = jz1 != 0 ? (jianzhu / jz1) : 0;
                                                    decimal? jiegou2 = jg1 != 0 ? (jiegou / jg1) : 0;
                                                    decimal? shebei2 = sb1 != 0 ? (shebei / sb1) : 0;
                                                    decimal? dianq2 = dq1 != 0 ? (dianq / dq1) : 0; 
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jianzhu2).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jiegou2).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(shebei2).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dianq2).ToString("f2")%></td>
                                    <td></td>
                                </tr>
                                <%
                                                }
                                                else if (dr["ID"].ToString() == "4")
                                                {
                                                    decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                    decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                    decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                    decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());

                                                    decimal? jianzhu3 = jz != 0 ? (jianzhu / jz) : 0;
                                                    decimal? jiegou3 = jg != 0 ? (jiegou / jg) : 0;
                                                    decimal? shebei3 = sb != 0 ? (shebei / sb) : 0;
                                                    decimal? dianq3 = dq != 0 ? (dianq / dq) : 0;  
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jianzhu3).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jiegou3).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(shebei3).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dianq3).ToString("f2")%></td>
                                    <td></td>
                                </tr>
                                <%}
                                            }
                                        } %>

                                <tr>
                                    <td>5.权重</td>
                                    <% if (IsSaved)
                                       { %>

                                    <td style="text-align: right;">
                                        <span><%= GetWeightsByUnitID(231)%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= GetWeightsByUnitID(237)%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= GetWeightsByUnitID(238)%></span>
                                    </td>
                                    <td style="text-align: right;">
                                        <span><%= GetWeightsByUnitID(239)%></span>
                                    </td>

                                    <%}
                                       else
                                       { %>

                                    <td>
                                        <input type="text" name="name" value="<%= GetWeightsByUnitID(231)%>" style="width: 60px;" unitid="231,232" uid="231" speids="1," id="jianzhu" />
                                    </td>
                                    <td>
                                        <input type="text" name="name" value="<%= GetWeightsByUnitID(237)%>" style="width: 60px;" unitid="237," uid="237" speids="2," id="jiegou" />
                                    </td>
                                    <td>
                                        <input type="text" name="name" value="<%= GetWeightsByUnitID(238)%>" style="width: 60px;" unitid="238," uid="238" speids="3,4" id="shebei" />
                                    </td>
                                    <td>
                                        <input type="text" name="name" value="<%= GetWeightsByUnitID(239)%>" style="width: 60px;" unitid="239," uid="239" speids="5," id="dianqi" />
                                    </td>
                                    <%} %>
                                    <td>
                                        <% if (IsSaved)
                                           { %>

                                        <input type="hidden" name="name" value="<%= GetWeightsByUnitID(231)%>" id="jianzhu" />
                                        <input type="hidden" name="name" value="<%= GetWeightsByUnitID(237)%>" id="jiegou" />
                                        <input type="hidden" name="name" value="<%= GetWeightsByUnitID(238)%>" id="shebei" />
                                        <input type="hidden" name="name" value="<%= GetWeightsByUnitID(239)%>" id="dianqi" />
                                        <%} %>

                                    </td>
                                </tr>
                                <%}
                                    else
                                    {
                                        int index = 0;
                                        WeightListOne.ForEach(c =>
                                        { %>
                                <tr>
                                    <td><%= c.WeightName%></td>
                                    <td style="text-align: right;"><%= c.JZ%><%= index==1?"%":"" %></td>
                                    <td style="text-align: right;"><%= c.JG%><%= index==1?"%":"" %></td>
                                    <td style="text-align: right;"><%= c.SB%><%= index==1?"%":"" %></td>
                                    <td style="text-align: right;"><%= c.DQ%><%= index==1?"%":"" %></td>
                                    <td>

                                        <% 
                                            //项目调整后的计算系数
                                            if(index==4){ %>
                                        <input type="hidden" name="name" value="<%= c.JZ%>" id="jianzhu" />
                                        <input type="hidden" name="name" value="<%= c.JG%>" id="jiegou" />
                                        <input type="hidden" name="name" value="<%= c.SB%>" id="shebei" />
                                        <input type="hidden" name="name" value="<%= c.DQ%>" id="dianqi" />
                                        <%}

                                            index++; %>
                                    </td>
                                </tr>
                                <%});
                                    } %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <%  if (!IsSaved)
                                            { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>

                                        <%}
                                            else
                                            { %>
                                        <a href="#" class="btn btn-sm red disabled" id="">保存</a>
                                        <a href="#" class="btn btn-sm red disabled" id="">确认提交</a>
                                        <%} %>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <!--计算值-->
                        <table class="table table-bordered table-hover" id="tbDataThree">
                            <thead>
                                <tr>
                                    <td>项目奖金计算值</td>
                                    <td colspan="5">
                                        <% if (!this.IsBak)
                                           { %>
                                        <span class="badge badge-danger" id="spProjAllotCount2"><%=Math.Round(Convert.ToDecimal(ProjAllDeptAllotCount)) %></span>
                                        <%}
                                           else
                                           {%>
                                        <span class="badge badge-danger" id=""><%=WeightListTwo[0].ProjCount %></span>

                                        <%} %>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width: 100px; text-align: center;"></th>
                                    <th style="width: 100px; text-align: center;">建筑</th>
                                    <th style="width: 100px; text-align: center;">结构</th>
                                    <th style="width: 100px; text-align: center;">设备</th>
                                    <th style="width: 100px; text-align: center;">电气</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    
                                    if (!this.IsBak)
                                    {
                                        if (ProjAllotUnitColList.Rows.Count > 0)
                                        {
                                            decimal? jianzhu = 0;
                                            decimal? jiegou = 0;
                                            decimal? shebei = 0;
                                            decimal? dianq = 0;

                                            foreach (System.Data.DataRow dr in ProjAllotUnitColList.Rows)
                                            {
                                                if (dr["ID"].ToString() == "1")
                                                { 
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["JianZhu"].ToString()).ToString("f0")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["JieGou"].ToString()).ToString("f0")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["SheBei"].ToString()).ToString("f0")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["Dianqi"].ToString()).ToString("f0")%></td>
                                    <td></td>
                                </tr>

                                <%}
                                                else if (dr["ID"].ToString() == "2")
                                                {
                                                    if (ProjAllDeptAllotCount != 0)
                                                    {
                                                        decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                        decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                        decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                        decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());
                                                        jianzhu = (jz / ProjAllDeptAllotCount) * 100;
                                                        jiegou = (jg / ProjAllDeptAllotCount) * 100;
                                                        shebei = (sb / ProjAllDeptAllotCount) * 100;
                                                        dianq = (dq / ProjAllDeptAllotCount) * 100;
                                                    }
                                                    else
                                                    {
                                                        jianzhu = 0;
                                                        jiegou = 0;
                                                        shebei = 0;
                                                        dianq = 0;
                                                    }
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jianzhu).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jiegou).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(shebei).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dianq).ToString("f2")%>%</td>
                                    <td></td>
                                </tr>
                                <%
                                                }
                                                else if (dr["ID"].ToString() == "3")
                                                {%>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["JianZhu"].ToString()).ToString("f0")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["JieGou"].ToString()).ToString("f0")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["SheBei"].ToString()).ToString("f0")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dr["Dianqi"].ToString()).ToString("f0")%></td>
                                    <td></td>
                                </tr>
                                <% }
                                                else if (dr["ID"].ToString() == "4")
                                                {
                                                    decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                    decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                    decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                    decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());

                                                    decimal count = jz + jg + sb + dq;

                                                    decimal jz1 = 0;
                                                    decimal jg1 = 0;
                                                    decimal sb1 = 0;
                                                    decimal dq1 = 0;
                                                    if (count > 0)
                                                    {
                                                        jz1 = (jz / count) * 100;
                                                        jg1 = (jg / count) * 100;
                                                        sb1 = (sb / count) * 100;
                                                        dq1 = (dq / count) * 100;
                                                    }
                                                
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= jz1.ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= jg1.ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= sb1.ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= dq1.ToString("f2")%>%</td>
                                    <td></td>
                                </tr>
                                <% }
                                                else if (dr["ID"].ToString() == "5")
                                                {
                                                    //decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                    //decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                    //decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                    //decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());

                                                    //decimal? jianzhu2 = jz != 0 ? (jianzhu / jz) * (decimal)0.01 : 0;
                                                    //decimal? jiegou2 = jg != 0 ? (jiegou / jg) * (decimal)0.01 : 0;
                                                    //decimal? shebei2 = sb != 0 ? (shebei / sb) * (decimal)0.01 : 0;
                                                    //decimal? dianq2 = dq != 0 ? (dianq / dq) * (decimal)0.01 : 0; 

                                                    decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                    decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                    decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                    decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());

                                                    decimal count = jz + jg + sb + dq;

                                                    decimal jz1 = 0;
                                                    decimal jg1 = 0;
                                                    decimal sb1 = 0;
                                                    decimal dq1 = 0;
                                                    if (count > 0)
                                                    {
                                                        jz1 = (jz / count) * 100;
                                                        jg1 = (jg / count) * 100;
                                                        sb1 = (sb / count) * 100;
                                                        dq1 = (dq / count) * 100;
                                                    }

                                                    decimal? jianzhu2 = jz1 != 0 ? (jianzhu / jz1) : 0;
                                                    decimal? jiegou2 = jg1 != 0 ? (jiegou / jg1) : 0;
                                                    decimal? shebei2 = sb1 != 0 ? (shebei / sb1) : 0;
                                                    decimal? dianq2 = dq1 != 0 ? (dianq / dq1) : 0; 
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jianzhu2).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jiegou2).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(shebei2).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dianq2).ToString("f2")%></td>
                                    <td></td>
                                </tr>
                                <% }
                                                else if (dr["ID"].ToString() == "6")
                                                {%>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["JianZhu"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["JieGou"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["SheBei"].ToString()%></td>
                                    <td style="text-align: right;"><%= dr["Dianqi"].ToString()%></td>
                                    <td></td>
                                </tr>
                                <% }
                                                else if (dr["ID"].ToString() == "7")
                                                {
                                                    decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                    decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                    decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                    decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());

                                                    decimal? jianzhu3 = jz;
                                                    decimal? jiegou3 = jg;
                                                    decimal? shebei3 = sb;
                                                    decimal? dianq3 = dq; 
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jianzhu3).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jiegou3).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(shebei3).ToString("f2")%>%</td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dianq3).ToString("f2")%>%</td>
                                    <td></td>
                                </tr>
                                <% }
                                                else if (dr["ID"].ToString() == "8")
                                                {
                                                    decimal jz = Convert.ToDecimal(dr["JianZhu"].ToString());
                                                    decimal jg = Convert.ToDecimal(dr["JieGou"].ToString());
                                                    decimal sb = Convert.ToDecimal(dr["SheBei"].ToString());
                                                    decimal dq = Convert.ToDecimal(dr["Dianqi"].ToString());

                                                    decimal? jianzhu4 = jz != 0 ? (jianzhu / jz) : 0;
                                                    decimal? jiegou4 = jg != 0 ? (jiegou / jg) : 0;
                                                    decimal? shebei4 = sb != 0 ? (shebei / sb) : 0;
                                                    decimal? dianq4 = dq != 0 ? (dianq / dq) : 0;   
                                %>
                                <tr>
                                    <td><%= dr["Name"].ToString()%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jianzhu4).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(jiegou4).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(shebei4).ToString("f2")%></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(dianq4).ToString("f2")%></td>
                                    <td></td>
                                </tr>
                                <%}
                                            }
                                        }
                                    }
                                    else
                                    {
                                        int index = 0;
                                        WeightListTwo.ForEach(c =>
                                        {%>

                                <tr>
                                    <td><%= c.WeightName%></td>
                                    <td style="text-align: right;"><%= c.JZ%><%= (index==1||index==3||index==6)==true?"%":"" %></td>
                                    <td style="text-align: right;"><%= c.JG%><%= (index==1||index==3||index==6)==true?"%":"" %></td>
                                    <td style="text-align: right;"><%= c.SB%><%= (index==1||index==3||index==6)==true?"%":"" %></td>
                                    <td style="text-align: right;"><%= c.DQ%><%= (index==1||index==3||index==6)==true?"%":"" %></td>
                                    <td></td>
                                </tr>
                                <% index++; 
                                });
                                    } %>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="name" value="<%=RenwuNo %>" id="hidRenwuNo" />
    <input type="hidden" name="name" value="<%= IsCheck %>" id="hidIsCheck" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />

    <input type="hidden" value="<%= IsSubmit %>" id="hidIsSubmit" />


    <div id="addCharge" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">编辑考核名称
            </h4>
        </div>
        <div class="modal-body">
            <div id="addChargeDialogDiv" style="color: #222222;">
                <table class="table table-hover table-bordered table-full-width" id="tbMemdetails">
                    <thead>
                        <tr>
                            <th colspan="4">奖金详情</th>
                        </tr>
                        <tr>
                            <th>项目名称</th>
                            <th>考核名称</th>
                            <th>项目奖金计算值</th>
                            <th>项目奖金调整值</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>


    <script>
        projAllot.init();
    </script>
</asp:Content>
