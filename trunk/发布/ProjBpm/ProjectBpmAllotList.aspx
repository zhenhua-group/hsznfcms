﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectBpmAllotList.aspx.cs" Inherits="TG.Web.ProjBpm.ProjectBpmAllotList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/ProjBpm/ProjectBpmNameList.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>设置考核项目</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>设置考核项目</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>设置考核项目
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">项目部门:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                        AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">项目年份:
                                </td>
                                <td style="width: 90px;">
                                    <asp:DropDownList ID="drp_year" CssClass="form-control" Width="90px" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">考核任务:</td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drprenwu" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drprenwu_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 40px;">
                                    <asp:Button CssClass="btn btn-sm blue" ID="btn_Search" runat="server" Text="刷新" OnClick="btn_Search_Click" />
                                </td>
                                <td style="width: 40px;">
                                    <a href="../DeptBpm/DeptBpmList.aspx" class="btn btn-sm blue">返回</a>
                                </td>

                                <td></td>
                            </tr>
                        </table>


                        <table class="table table-hover table-bordered table-full-width" id="tbNameList">
                            <thead>
                                <tr>
                                    <th style="width: 200px;">项目名称</th>
                                    <th style="width: 100px;">考核名称</th>
                                    <th style="width: 80px;">完成比例</th>
                                    <th style="width: 80px;">比例备注</th>
                                    <th style="width: 80px;">建筑类型</th>
                                    <th style="width: 100px;">规模/万平米</th>
                                    <th style="width: 80px;">难度系数</th>
                                    <th style="width: 110px;">虚拟产值(万元)</th>
                                    <th style="width: 120px;">虚拟总产值(万元)</th>
                                    <th style="width: 80px;">整体绩效</th>
                                    <td style="width: 50px;"></td>
                                    
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (ProjNameList.Rows.Count > 0)
                                    {
                                        int tempID = 0;
                                        int colcount = 0;
                                        bool iscol = false;
                                        foreach (System.Data.DataRow dr in ProjNameList.Rows)
                                        {
                                            if (tempID != Convert.ToInt32(dr["ID"].ToString()))
                                            {
                                                colcount = Convert.ToInt32(dr["ColCount"].ToString());
                                                iscol = colcount > 1 ? true : false;
                                            }
                                            else
                                            {
                                                iscol = false;
                                            }

                                            
                                %>
                                <tr>
                                    <td><%= dr["proName"].ToString() %></td>
                                    <td><%= dr["KName"].ToString() %></td>
                                    <td style="text-align:right;"><%= dr["DoneScale"].ToString() %>%</td>
                                    <td><%= dr["Sub"].ToString() %></td>
                                    <td><%= dr["KaoheTypeName"].ToString() %></td>
                                    <td style="text-align:right;"><%= dr["RealScale"].ToString() %></td>
                                    <td style="text-align:right;"><%= dr["TypeXishu"].ToString() %></td>
                                    <td style="text-align:right;"><%= dr["virallotCount"].ToString() %></td>
                                    <%
                                            if (iscol)
                                            { 
                                    %>
                                    <td  style="text-align:right;" rowspan="<%= dr["ColCount"].ToString()%>"><%= dr["AllAllotCount"].ToString() %></td>
                                    <%}
                                            else
                                            {
                                                if (tempID != Convert.ToInt32(dr["ID"].ToString()))
                                                {%>
                                    <td  style="text-align:right;"><%= dr["AllAllotCount"].ToString() %></td>
                                    <%}
                                            }

                                            tempID = Convert.ToInt32(dr["ID"].ToString()); %>

                                    <td  style="text-align:right;"><%= dr["AllScale"].ToString() %></td>
                                    <td>
                                        <a href="/ProjBpm/ProjectBpmAllot.aspx?proid=<%= dr["proID"].ToString()%>&prokhid=<%= dr["KaoHeProjId"].ToString()%>&khnameid=<%= dr["ID"].ToString()%>&action=check" class="btn default btn-xs blue-stripe">查看</a>
                                    </td>
                                    
                                    <td></td>
                                </tr>
                                <%}
                                    } %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="addCharge" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">编辑考核名称
            </h4>
        </div>
        <div class="modal-body">
            <div id="addChargeDialogDiv" style="color: #222222;">
                <table>
                    <tr>
                        <td style="width: 280px" valign="top">
                            <table class="table table-hover table-bordered table-full-width" id="tbNameAdd">
                                <thead>
                                    <tr>
                                        <th colspan="4"><span id="spProjName"></span>(考核名称添加）
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width: 100px;">考核名称</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtKName" /></td>
                                    </tr>
                                    <tr>
                                        <td>整体绩效</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtAllScale" /></td>
                                    </tr>
                                    <tr>
                                        <td>备注</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtSub" /></td>
                                    </tr>
                                    <tr>
                                        <td>当期完成比例</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtDoneScale" />%</td>
                                    </tr>
                                    <tr>
                                        <td>未完成比例</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtLeftScale" />%</td>

                                    </tr>
                                    <tr>
                                        <td>历史完成比例</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtHisScale" />%</td>
                                    </tr>
                                    <tr>
                                        <td>合计
                                        </td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtHeji" />%</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <a href="#" class="btn btn-sm red" id="btnaddprojName">保存</a>
                                            <button type="button" data-dismiss="modal" class="btn btn-sm ">关闭</button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                        <td style="width: 280px" valign="top">
                            <table class="table table-hover table-bordered table-full-width" id="tbTypeAdd">
                                <thead>
                                    <tr>
                                        <th colspan="2">编辑考核分类
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width: 120px;">建筑类型:</td>
                                        <td>
                                            <select id="selType">
                                                <option value="0">--请选择--</option>
                                                <% TypeEnumList.ForEach(c =>
                                               { %>
                                                <option value="<%=c.ID %>" type1="<%= c.Type1 %>" type5="<%= c.Type5 %>" type6="<%= c.Type6 %>" type10="<%= c.Type10 %>"><%= c.TypeName %></option>
                                                <%}); %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>实际规模</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtRealScale" />(万平米)</td>
                                    </tr>
                                    <tr>
                                        <td>难度系数</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtTypeXishu" /></td>
                                    </tr>
                                    <tr>
                                        <td>虚拟产值</td>
                                        <td><span id="spVirCount">0</span></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <a href="#" class="btn btn-sm red" id="btnSaveType">保存</a>
                                            <button type="button" data-dismiss="modal" class="btn btn-sm ">关闭</button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    <!--考核名称ID-->
    <input type="hidden" name="name" value="0" id="hidTempProKHNameid" />
    <!--项目ID-->
    <input type="hidden" name="name" value="0" id="hidTempProid" />
    <!--项目考核ID-->
    <input type="hidden" name="name" value="0" id="hidTempKaoHeProjId" />
    <!--考核名称与类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeProjNameid" />
    <!--考核类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeId" />

    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <script>
        nameList.init();
    </script>
</asp:Content>

