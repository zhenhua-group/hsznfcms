﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="KaoHeMemsMngAllotList.aspx.cs" Inherits="TG.Web.ProjBpm.KaoHeMemsMngAllotList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>部门内奖金计算</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>部门内奖金计算</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>部门内奖金计算
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px; display: none;">项目部门:
                                </td>
                                <td style="width: 120px; display: none;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">考核任务:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drpRenwu" runat="server" AppendDataBoundItems="True" AutoPostBack="true" OnSelectedIndexChanged="drpRenwu_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 300px;">

                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" Visible="false" />
                                    <% if (this.UserShortName == "admin"|| this.UserShortName=="张翠珍")
                                       {
                                           if (!this.IsBak)
                                           {%>
                                    <a href="#" class="btn btn-sm yellow" id="btnBak">发起归档</a>
                                    <%}
                                           else
                                           { %>

                                    <a href="#" class="btn btn-sm default disabled" id="btnBak2">已归档</a>
                                    <%}%>
                                      
                                      <span class="badge badge-warning">注：此处只归档当年考核历史组织架构！</span>
                                      <% } %>

                                     
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th style="width: 100px;">部门名称</th>
                                    <th style="width: 150px;">任务名称</th>
                                    <th style="width: 60px;">操作</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (this.UserShortName == "admin" || this.UserShortName == "张翠珍")
                                    { 
                                    
                                    if (!this.IsBak)
                                    {
                                        MngAllotList.ForEach(c =>
                                        {%>
                                <tr>
                                    <td data-uid="<%= c.UnitID %>" data-kaoheid="<%= c.KaoHeUnitID %>" data-memid="<%= c.InsertUserID%>"><%=c.UnitName %></td>
                                    <td><%= c.RenwuName %></td>
                                    <td>
                                        <a class="btn default btn-xs red-stripe" target="_blank" href="UnitMngAllot.aspx?uid=<%= c.UnitID %>&renwuno=<%= c.RenwuID %>&id=<%= c.KaoHeUnitID %>&action=3&check=chk&chkuserid=<%= c.InsertUserID %>"><%= this.UserShortName=="admin"?"查看|归档":"查看" %></a>
                                    </td>
                                    <td></td>
                                </tr>
                                <%
                                        });
                                    }
                                    else
                                    {
                                        UnitHisList.ForEach(c =>
                                      {%>
                                <tr>
                                    <td data-uid="<%= c.UnitID %>"><%=c.UnitName %></td>
                                    <td><%= c.RenwuName %></td>
                                    <td>
                                        <a class="btn default btn-xs red-stripe" target="_blank" href="UnitMngAllot.aspx?uid=<%= c.UnitID %>&renwuno=<%= c.RenwuID %>&id=<%= c.KaoHeUnitID %>&action=3&check=chk&chkuserid=<%= c.InsertUserID %>"><%= this.UserShortName=="admin"?"查看|归档":"查看" %></a>
                                    </td>
                                    <td></td>
                                </tr>
                                <%
                                      });

                                    }
                                    }
                                   %>

 
                                 


 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {

            //发起归档
            $("#btnBak").click(function () {

                if (!confirm("确定要发起部门内奖金计算参与部门吗？")) {
                    return false;
                }
                if ($("#tbDataOne tbody tr").length == 0) {
                    alert("没有要归档的部门信息！");
                    return false;
                }

                var renwuid = $("#ctl00_ContentPlaceHolder1_drpRenwu").val();
                var renwuName = $("#ctl00_ContentPlaceHolder1_drpRenwu option:checked").text();
                //获取归档统计记录
                var arryData = new Array();
                $("#tbDataOne tbody tr").each(function (index, domEle) {

                    var rowdata = $(this);
                    arryData[index] = {
                        ID: 0,
                        RenwuID: renwuid,
                        RenwuName: renwuName,
                        UnitID: rowdata.children().eq(0).attr("data-uid"),
                        UnitName: rowdata.children().eq(0).text(),
                        KaoHeUnitID: rowdata.children().eq(0).attr("data-kaoheid"),
                        InsertUserID: rowdata.children().eq(0).attr("data-memid")
                    }
                });

                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "unitbak";

                var jsonData = JSON.stringify(arryData);
                var data = { "action": action, "isNew": 0, "data": jsonData };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result == 1) {
                            alert("部门内奖金参与部门归档成功!");
                        }
                        else if (result == 2) {
                            alert('已归档，无需重复归档！');
                        }
                        else if (result == 3) {
                            alert("考核任务未归档，请先归档考核任务！");
                        }

                        $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
                    }
                    else {
                        alert("错误！");
                    }
                });
            });
        });
    </script>
</asp:Content>
