﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LockScreen.aspx.cs" Inherits="TG.Web.LockScreen" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>弘石产值分配系统</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="MobileOptimized" content="320">
    <!-- 全局定义 -->
    <link href="/js/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="/js/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <link href="/js/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet"
        type="text/css" />
    <!-- 结束 -->
    <!-- 页面定义 -->
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-toastr/toastr.min.css" />
    <!-- 结束 -->
    <!-- 皮肤定义 -->
    <link href="/js/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/js/assets/css/pages/lock.css" rel="stylesheet" type="text/css" />
    <link href="/js/assets/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- 核心JS -->
    <!--[if lt IE 9]>
	<script src="/js/assets/plugins/respond.min.js"></script>
	<script src="/js/assets/plugins/excanvas.min.js"></script> 
	<![endif]-->
    <script src="/js/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
    <script src="/js/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <!-- 结束 -->
    <!-- 插件JS -->
    <script src="/js/assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
    <!-- 结束 -->
    <!-- 页面JS -->
    <script src="/js/assets/scripts/app.js"></script>
    <script src="/js/assets/scripts/lock.js"></script>
    <script>
        jQuery(document).ready(function () {
            App.init();
            Lock.init();
        });
      
    </script>
</head>
<body oncontextmenu="return false">
    <div class="page-lock">
        <div class="page-logo">
            <a class="brand" href="#">
                <img src="/js/assets/img/logo.png" alt="logo" />
            </a>
        </div>
        <div class="page-body">
            <asp:Image onerror="javascript:this.src='/js/assets/img/profile/profile.jpg'" runat="server"
                ID="lbl_img" Width="180" Height="180" />
            <div class="page-lock-info">
                <h1>
                    <%= UserSysName%></h1>
                <span id="username" class="email">
                    <%= UserSysLoginName %></span>
                <input type="hidden" name="name" id="hid_url" value="<%=BackUrl %>" />
                <span class="locked">锁定</span>
          <%--      <form class="form-inline" action="LockScreen.aspx">--%>
                <div class="input-group input-medium">
                    <input id="userpwd" type="password" class="form-control" />
                    <span class="input-group-btn">
                        <button id="btn_login" type="button" class="btn blue icn-only">
                            <i class="m-icon-swapright m-icon-white"></i>
                        </button>
                    </span>
                </div>
              <%--  </form>--%>
                <div class="relogin">
                    <a href="Login.aspx">不是本人?</a>
                </div>
            </div>
        </div>
        <div class="page-footer">
            2016 &copy; 天正软件股份有限公司.
        </div>
    </div>
</body>
</html>
