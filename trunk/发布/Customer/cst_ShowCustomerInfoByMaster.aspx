﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cst_ShowCustomerInfoByMaster.aspx.cs" Inherits="TG.Web.Customer.cst_ShowCustomerInfoByMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $("#arrow2").attr("class", "arrow open");
            $("#show2_2").parent().parent().css("display", "block");
            $(".cls_chk").click(function () {
                var cstid = $(this).attr("rel");
                $.ajax({
                    type: "POST",
                    url: "../HttpHandler/cp_ShowContactPersionInfo.ashx",
                    dataType: "text",
                    data: "id=" + cstid,
                    success: function (msg) {
                        var arrayValues = msg.split("|");
                        $("#No").text(arrayValues[0]);
                        $("#name").text(arrayValues[1]);
                        $("#zhiwu").text(arrayValues[2]);
                        $("#department").text(arrayValues[3]);
                        $("#phone").text(arrayValues[4]);
                        $("#email").text(arrayValues[5]);
                        $("#bphone").text(arrayValues[6]);
                        $("#fphone").text(arrayValues[7]);
                        $("#bfax").text(arrayValues[8]);
                        $("#Ffax").text(arrayValues[9]);
                        $("#bz").text(arrayValues[10]);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("错误");
                    }
                });
            });
            //满意度
            $(".sat_chk").click(function () {
                var sat_id = $(this).attr("rel");
                $.ajax({
                    type: "POST",
                    url: "../HttpHandler/ShowSatisfactionInfo.ashx",
                    data: "sat_id=" + sat_id,
                    success: function (msg) {
                        var sat_arry = msg.split("|");
                        $("#lblSat_No").text(sat_arry[0]);
                        $("#lblSch_Context").text(sat_arry[1]);
                        $("#lblSat_Leve").text(sat_arry[5]);
                        $("#lblSch_Time").text(sat_arry[6]);
                        $("#lblSch_Way").text(sat_arry[7]);
                        $("#lblSat_Best").text(sat_arry[3]);
                        $("#lblSat_NotBest").text(sat_arry[4]);
                        $("#lblRemark").text(sat_arry[2]);
                    },
                    eror: function (XMLHttpRequest, textStatus, errorThrowm) {
                        alert("错误");
                    }
                });
            });
        });
        //清空数据  
    </script>
    <style type="text/css">
        .table {
            margin-bottom: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">客户信息管理 <small>客户信息查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>客户信息管理</a><i class="fa fa-angle-right"> </i><a>客户信息查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户信息查看
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <h4 class="form-section">基本信息</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" align="center">
                                        <tr>
                                            <td style="width: 10%;">编号:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCst_No" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td style="width: 12%;">客户简称:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCst_Brief" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">客户名称:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCst_Name" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td style="width: 12%;">公司地址:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCpy_Address" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">邮政编码:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCode" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td style="width: 12%;">联系人:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtLinkman" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">公司电话:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCpy_Phone" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td style="width: 12%;">传真号:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCpy_Fax" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section">甲方负责人</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <asp:GridView ID="gv_contactPerson" runat="server" Width="100%" CellPadding="0" DataKeyNames="CP_Id"
                                            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Left" ShowHeader="true"
                                            Font-Size="12px" RowStyle-Height="22px" CssClass="table table-bordered table-hover">
                                            <RowStyle HorizontalAlign="Left" Height="22px"></RowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="ContactNo" HeaderText="序号" SortExpression="ContactNo">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="8%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Name" HeaderText="姓名" SortExpression="Name">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Duties" HeaderText="职务" SortExpression="Duties">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Department" HeaderText="部门" SortExpression="Department">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Phone" HeaderText="电话" SortExpression="Phone">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="查看">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemTemplate>
                                                        <a href="#Show_cstInfo" rel='<%# Eval("cp_ID") %>' id="chk" class="cls_chk" data-toggle="modal">查看</a>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="7%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                暂时没有联系人！
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section">扩展信息</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" align="center">
                                        <tr>
                                            <td style="width: 12%;">客户英文名:
                                            </td>
                                            <td colspan="3">
                                                <asp:Label ID="txtCst_EnglishName" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td style="width: 12%;">是否合作:
                                            </td>
                                            <td style="width: 18%;">
                                                <asp:Label ID="lbIsPartner" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>所在国家:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCountry" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td style="width: 15%;">所在省份:
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:Label ID="txtProvince" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>所在城市:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCity" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>客户类型:
                                            </td>
                                            <td>
                                                <asp:Label ID="lbl_Type" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>所属行业:
                                            </td>
                                            <td>
                                                <asp:Label ID="lbl_Profession" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>分支机构:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtBranchPart" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Email:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtEmail" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>公司主页:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCpy_PrincipalSheet" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>关系建立时间:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCreateRelationTime" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>关系部门:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtRelationDepartment" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>信用级别:
                                            </td>
                                            <td>
                                                <asp:Label ID="lbl_CreditLeve" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>亲密度:
                                            </td>
                                            <td>
                                                <asp:Label ID="lbl_CloseLeve" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>开户银行名称:
                                            </td>
                                            <td colspan="3">
                                                <asp:Label ID="txtBankName" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>企业代码:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtCpy_Code" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>开户银行账号:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtBankAccountNo" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>开户银行大额联行号:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtBankBigNo" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                            <td>法定代表:
                                            </td>
                                            <td>
                                                <asp:Label ID="txtLawPerson" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>纳税人识别号:
                                            </td>
                                            <td colspan="5">
                                                <asp:Label ID="txtTaxAccountNo" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>备注:
                                            </td>
                                            <td colspan="5">
                                                <asp:Label ID="txtRemark" runat="server" Text="" CssClass="form-control-static"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section">附件</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <asp:GridView ID="gv_Attach" runat="server" Width="100%" CellPadding="0" DataKeyNames="ID"
                                            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Left" ShowHeader="true"
                                            Font-Size="12px" RowStyle-Height="22px" CssClass="table table-bordered table-hover"
                                            OnRowDataBound="gv_Attach_RowDataBound" EnableModelValidation="True">
                                            <RowStyle HorizontalAlign="Left" Height="22px"></RowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="文件名" SortExpression="FileName">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemTemplate>
                                                        <img style="width: 16px; height: 16px;" src='<%# Eval("FileTypeImg") %>' />
                                                        <asp:Label ID="fileName" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UploadTime" HeaderText="上传时间" SortExpression="UploadTime">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="文件大小" DataField="FileSize">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="文件类型" DataField="FileType">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle Width="12%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UploadUser" HeaderText="用户" SortExpression="UploadUser"
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemTemplate>
                                                        <a href="/Customer/DownLoadFile.aspx?SysNo=<%#Eval("ID") %>">下载</a>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="6%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                暂时没有附件！
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tb_fileupload" style="width: 100%; border: solid 1px #CCC; border-collapse: collapse; height: 30px; display: none;">
                                    <tr>
                                        <td align="center">
                                            <asp:HiddenField ID="hid_parentid" runat="server" Value="0" />
                                            <span id="spanButtonPlaceholder"></span>
                                            <div id="divFileProgressContainer" style="margin-top: -30px; margin-left: 120px; height: 25px; width: 350px;">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <h4 class="form-section">满意度调查</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <asp:GridView ID="gv_satisfaction" runat="server" Width="100%" CellPadding="0" DataKeyNames="Sat_Id"
                                            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Left" ShowHeader="False"
                                            Font-Size="12px" RowStyle-Height="22px" CssClass="table table-bordered table-hover">
                                            <RowStyle HorizontalAlign="Left" Height="22px"></RowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="Sat_No" HeaderText="序号" SortExpression="Sat_No">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="5%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Sch_Context" HeaderText="调查内容" SortExpression="Sch_Context">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="满意度" SortExpression="Sat_Leve">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Sat_Leve") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSat_Leve" runat="server" Text='<%# Bind("Sat_Leve") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="25%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Sch_Time" HeaderText="调查时间" SortExpression="Sch_Time"
                                                    DataFormatString="{0:d}">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="调查方式" SortExpression="Sch_Way">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Sch_Way") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSch_Way" runat="server" Text='<%# Bind("Sch_Way") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="查看">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemTemplate>
                                                        <a href="#Show_sfcInfo" rel='<%# Eval("Sat_ID") %>' id="sat_chk" class="sat_chk"
                                                            data-toggle="modal">查看</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                暂时没有满意度调查数据！
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section">相关合同</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <tr align="center">
                                                <td align="center" style="width: 6%;">序号
                                                </td>
                                                <td align="center" style="width: 10%">合同编号
                                                </td>
                                                <td align="center" style="width: 30%">合同名称
                                                </td>
                                                <td align="center" style="width: 10%">合同额(万元)
                                                </td>
                                                <td align="center" style="width: 15%">签订日期
                                                </td>
                                                <td align="center" style="width: 15%">完成日期
                                                </td>
                                                <td align="center" style="width: 8%">项目经理
                                                </td>
                                                <td align="center" style="width: 6%">操作
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gv_rContract" runat="server" Width="100%" CellPadding="0" DataKeyNames="cpr_Id"
                                            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Left" ShowHeader="False"
                                            Font-Size="12px" RowStyle-Height="22px" CssClass="table table-bordered table-hover"
                                            EnableModelValidation="True">
                                            <RowStyle HorizontalAlign="Left" Height="22px"></RowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="cpr_Id" HeaderText="序号" SortExpression="cpr_Id">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="6%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cpr_No" HeaderText="合同编号" SortExpression="cpr_No">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="合同名称" SortExpression="cpr_Name">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemTemplate>
                                                        <img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px;" />
                                                        <a href="../Coperation/cpr_ShowCoprationBymaster.aspx?flag=cstshow&cprid=<%# Eval("cpr_id") %>" title='<%# Eval("cpr_Name") %>'>
                                                            <%# Eval("cpr_Name").ToString().Length > 15 ? Eval("cpr_Name").ToString().Substring(0, 15) + "......" : Eval("cpr_Name").ToString()%></a>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="cpr_Acount" HeaderText="合同额" SortExpression="cpr_Acount">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cpr_SignDate" HeaderText="签订日期" SortExpression="cpr_SignDate"
                                                    DataFormatString="{0:d}">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cpr_DoneDate" HeaderText="完成日期" SortExpression="cpr_DoneDate"
                                                    DataFormatString="{0:d}">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ChgPeople" HeaderText="负责人" SortExpression="ChgPeople">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemStyle HorizontalAlign="Left" Width="8%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="查看">
                                                    <HeaderStyle BackColor="LightGray" />
                                                    <ItemTemplate>
                                                        <a href='../Coperation/cpr_ShowCoprationBymaster.aspx?flag=cstshow&cprid=<%# Eval("cpr_id") %>'>查看</a>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="6%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                暂时没有关联合同！
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-offset-12 col-md-12">
                                        <button type="submit" class="btn green">
                                            导出</button>
                                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                            返回</button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_cstid" runat="server" Value="0" />
    <div id="Show_cstInfo" class="modal fade yellow" tabindex="-1" data-width="560" aria-hidden="true"
        style="display: none; width: 560px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人信息</h4>
        </div>
        <div class="modal-body">
            <table class=" table table-bordered">
                <tr>
                    <td style="width: 20%;">
                        <label class="control-label ">
                            序号:</label>
                    </td>
                    <td style="width: 30%;"><span id="No"></span></td>
                    <td style="width: 20%;">
                        <label class="control-label ">
                            姓名:</label>
                    </td>
                    <td style="width: 30%;"><span id="name"></span></td>
                </tr>

                <tr>
                    <td>
                        <label class="control-label ">
                            职务:</label></td>
                    <td><span id="zhiwu"></span></td>
                    <td>
                        <label class="control-label ">
                            电话:</label></td>
                    <td><span id="phone"></span></td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label ">
                            部门:</label></td>
                    <td><span id="department"></span></td>
                    <td>
                        <label class="control-label ">
                            商务电话:</label></td>
                    <td><span id="bphone"></span></td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label">
                            住宅电话:</label></td>
                    <td><span id="fphone"></span></td>
                    <td>
                        <label class="control-label ">
                            商务传真:</label></td>
                    <td><span id="bfax"></span></td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label ">
                            住宅传真:</label></td>
                    <td><span id="Ffax"></span></td>
                    <td>
                        <label class="control-label ">
                            Email:</label></td>
                    <td><span id="email"></span></td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label">
                            备注:</label></td>
                    <td><span id="bz"></span></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>

            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">
                    关闭</button>
            </div>
        </div>
    </div>
    <div id="Show_sfcInfo" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">满意度调查信息</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            序号:</label>
                        <div class="col-md-4">
                            <span id="lblSat_No"></span><span class="help-block"><font><font class=""></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            调查内容:</label>
                        <div class="col-md-8">
                            <span id="lblSch_Context"></span><span class="help-block"><font><font class=""></font>
                            </font></span>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/span-->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            调查时间:</label>
                        <div class="col-md-4">
                            <span id="lblSch_Time"></span><span class="help-block"><font><font class=""></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            满意度:</label>
                        <div class="col-md-4">
                            <span id="lblSat_Leve"></span><span class="help-block"><font><font class="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label
    col-md-2">
                            调查方式:</label>
                        <div class="col-md-4">
                            <span id="lblSch_Way"></span><span class="help-block"><font><font class=""></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--/span-->
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            最满意方面:</label>
                        <div class="col-md-8">
                            <span id="lblSat_Best"></span><span class="help-block"><font><font class=""></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            最不满意方面:</label>
                        <div class="col-md-8">
                            <span id="lblSat_NotBest"></span><span class="help-block"><font><font class=""></font>
                            </font></span>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-2">
                            备注:</label>
                        <div class="col-md-10">
                            <span id="lblRemark"></span><span class="help-block"><font><font class=""></span>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">
                    关闭</button>
            </div>
        </div>
    </div>
    </font></font></font></font></font></font></font></font></span></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font></font>
</asp:Content>
