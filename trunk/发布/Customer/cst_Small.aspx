﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cst_Small.aspx.cs" Inherits="TG.Web.Customer.cst_Small" %>

<%@ Register assembly="AspNetPager" namespace="Wuqi.Webdiyer" tagprefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Customer.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html, body {
            width: 690px;
        }
    </style>

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //行变色
            $(".cls_show_cst_jiben tr:odd").attr("style", "background-color:#FFF");
            //GridView 变色
            $("#gridView tr:odd").css({ "background-color": "white" });
            //行选择
            $(".cls_select").click(function () {
                var arrayValue = new Array();
                arrayValue[0] = $(this).parent().parent().find("TD").eq(0).text();
                arrayValue[1] = $(this).parent().parent().find("TD").eq(1).find("a").attr("title");
                arrayValue[2] = $(this).parent().parent().find("TD").eq(2).find("a").attr("title");
                arrayValue[3] = $(this).parent().parent().find("TD").eq(3).text();
                arrayValue[4] = $(this).parent().parent().find("TD").eq(4).text();
                arrayValue[5] = $(this).parent().parent().find("TD").eq(5).text();
                arrayValue[6] = $(this).parent().parent().find("TD").eq(6).text();
                arrayValue[7] = $(this).parent().find(".cls_cstid").val();
                arrayValue[8] = $(this).parent().find(".cls_jc").val();
                window.returnValue = arrayValue;
                window.close();
            });
        });
    </script>

</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
        <div class="cls_data" style="width: 690px;">
            <table class="cls_show_cst_jiben" align="center">
                <tr>
                    <td style="width: 100px;">客户名称
                    </td>
                    <td>
                        <asp:TextBox ID="txt_cstName" runat="server" CssClass="cls_input_text"
                            Width="250px"></asp:TextBox>
                    </td>
                    <td style="width: 100px;">客户类别：
                    </td>
                    <td>
                        <asp:DropDownList ID="drp_cstType" runat="server" Width="120px">
                            <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                            <asp:ListItem Value="0">普通客户</asp:ListItem>
                            <asp:ListItem Value="1">VIP客户</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>所在省份
                    </td>
                    <td>
                        <asp:TextBox ID="txt_cstprince" runat="server" CssClass="cls_input_text"></asp:TextBox>
                    </td>
                    <td>所在城市
                    </td>
                    <td>
                        <asp:TextBox ID="txt_cstCity" runat="server" CssClass="cls_input_text"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>联系人：
                    </td>
                    <td>
                        <asp:TextBox ID="txt_cstLian" runat="server" CssClass="cls_input_text"></asp:TextBox>
                    </td>
                    <td>&nbsp;
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:ImageButton ID="btn_Search" runat="server" ImageUrl="~/Images/buttons/btn_search.gif"
                            OnClick="btn_Search_Click" Style="height: 20px" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="cls_data" style="width: 690px;">
            <table class="cls_content_head">
                <tr>
                    <td style="width: 80px;" align="center">编号</td>
                    <td style="width: 175px;" align="center">客户名称</td>
                    <td style="width: 165px" align="center">公司地址</td>
                    <td style="width: 60px;" align="center">邮编</td>
                    <td style="width: 60px;" align="center">联系人</td>
                    <td style="width: 60px" align="center">电话</td>
                    <td style="width: 60px;" align="center">传真号</td>
                    <td style="width: 30px;">操作
                    </td>
                </tr>
            </table>
            <input id="Hidden1" type="hidden" />
            <asp:GridView ID="gridView" runat="server" Width="690px" CellPadding="0" DataKeyNames="Cst_Id"
                AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" ShowHeader="False"
                BackColor="#F0F0F0" Font-Size="12px" RowStyle-Height="22px"
                EnableModelValidation="True">
                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                <Columns>
                    <asp:BoundField DataField="Cst_No" SortExpression="Cst_No">
                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                    </asp:BoundField>
                    <asp:TemplateField SortExpression="Cst_Name">
                        <ItemTemplate>
                            <img src="../Images/buttons/icon_cst.png" style="width: 16px; height: 16px;">
                            <a href="#" target="_self" title='<%# Eval("Cst_Name").ToString()%>'>
                                <%# Eval("Cst_Name").ToString().Length>10?Eval("Cst_Name").ToString().Substring(0,10)+"....":Eval("Cst_Name").ToString() %></a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="175px" />
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="Cpy_Address">
                        <ItemTemplate>
                            <a href="#" target="_self" title='<%# Eval("Cpy_Address").ToString()%>'>
                                <%# Eval("Cpy_Address").ToString().Length>10?Eval("Cpy_Address").ToString().Substring(0,10)+"....":Eval("Cpy_Address").ToString() %></a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="165px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Code" SortExpression="Code">
                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Linkman" SortExpression="Linkman">
                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Cpy_Phone" SortExpression="Cpy_Phone">
                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Cpy_Fax" SortExpression="Cpy_Fax">
                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <input type="hidden" value='<%# Eval("Cst_Brief") %>' class="cls_jc" />
                            <a href="#" class="cls_select">选择</a>
                            <input type="hidden" value='<%# Eval("Cst_Id") %>' class="cls_cstid" />
                        </ItemTemplate>
                        <ItemStyle ForeColor="Blue" HorizontalAlign="Center" Width="30px" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" 
        CurrentPageButtonClass="cpb" 
        CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条" 
        CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" 
        NextPageText="下一页" onpagechanged="AspNetPager1_PageChanged" 
        PageIndexBoxClass="indexbox" 
        PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10" 
        ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" 
        TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" 
        PageIndexBoxStyle="width:25px;" PageSize="30" SubmitButtonClass="submitbtn">
    </webdiyer:AspNetPager>
        </div>
    </form>
</body>
</html>
