﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cp_ModifyContactPersionInfo.aspx.cs"
    Inherits="TG.Web.Customer.ContactPersionInfo.cp_ModifyContactPersionInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="../../css/m_comm.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/Customer.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/jquery.alerts.css" />
    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../../js/Customer/cp_ModifyContactPersionInfo.js"></script>
</head>
<body bgcolor="f0f0f0">
	<form id="form1" runat="server">
	<%--<table class="cls_container" style="height: 20px;">
		<tr>
			<td class="cls_head">
				&nbsp;&nbsp;当前位置：[客户管理]
			</td>
		</tr>
	</table>
	<table class="cls_show_cst_jiben">
		<tr>
			<td>
				序号：
			</td>
			<td colspan="3">
				<asp:TextBox ID="txtContactNo" runat="server" Width="120px" Style="background: url(../images/bg_tdhead.gif) repeat-x;"
					CssClass="cls_input_text"></asp:TextBox>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				姓名：
			</td>
			<td>
				<asp:TextBox ID="txtName" runat="server" Style="background: url(../images/bg_tdhead.gif) repeat-x;"
					CssClass="cls_input_text" Width="120px"></asp:TextBox>
			</td>
			<td>
				职务：
			</td>
			<td>
				<asp:TextBox ID="txtDuties" runat="server" Width="120px" CssClass="cls_input_text"></asp:TextBox>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				手机：
			</td>
			<td>
				<asp:TextBox ID="txtPhone" runat="server" Width="120px" Style="background: url(../images/bg_tdhead.gif) repeat-x;"
					CssClass="cls_input_text"></asp:TextBox>
			</td>
			<td>
				部门：
			</td>
			<td>
				<asp:TextBox ID="txtDepartment" runat="server" Width="120px" CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				商务电话：
			</td>
			<td>
				<asp:TextBox ID="txtBPhone" runat="server" Width="120px" CssClass="cls_input_text"></asp:TextBox>
			</td>
			<td>
				住宅电话：
			</td>
			<td>
				<asp:TextBox ID="txtFPhone" runat="server" Width="120px" CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				商务传真：
			</td>
			<td>
				<asp:TextBox ID="txtBFax" runat="server" Width="120px" CssClass="cls_input_text"></asp:TextBox>
			</td>
			<td>
				住宅传真：
			</td>
			<td>
				<asp:TextBox ID="txtFFax" runat="server" Width="120px" CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				Email：
			</td>
			<td>
				<asp:TextBox ID="txtEmail" runat="server" Width="120px" CssClass="cls_input_text"></asp:TextBox>
			</td>
			<td>
				&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				备注：
			</td>
			<td colspan="3">
				<asp:TextBox ID="txtRemark" runat="server" Height="50px" TextMode="MultiLine" CssClass="cls_input_text_w"></asp:TextBox>
			</td>
		</tr>
		<tr align="center">
			<td colspan="4">
				<asp:ImageButton ID="btn_Save" runat="server" ImageUrl="~/Images/btn_save.gif" OnClick="btn_Save_Click" />
				&nbsp;<asp:ImageButton ID="btn_back" runat="server" ImageUrl="~/Images/btn_back.gif"
					OnClick="btn_back_Click" />
			</td>
		</tr>
	</table>--%>
	</form>
</body>
</html>
