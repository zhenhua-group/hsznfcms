﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="stf_AddSatisfactionInfo.aspx.cs"
    Inherits="TG.Web.Customer.SatisfactionInfo.stf_AddSatisfactionInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="../../css/m_comm.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/Customer.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/jquery.alerts.css" />
    
    <script language="javascript" src="../../js/wdate/WdatePicker.js"></script>

    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript" src="../../js/jquery.alerts.js"></script>
    
    <script type="text/javascript" src="../../js/Customer/stf_AddSatisfactionInfo.js"></script>

</head>
<body bgcolor="f0f0f0">
	
</body>
</html>
