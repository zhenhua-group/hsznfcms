﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cst_ModifyCustomerInfoByMaster.aspx.cs" Inherits="TG.Web.Customer.cst_ModifyCustomerInfoByMaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet"
        type="text/css" />
    <%--    <script src="../js/assets/plugins/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>--%>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script src="../js/swfupload/handlers_green.js" type="text/javascript"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Customer/Contact.js"></script>
    <script src="../js/Customer/cst_ModifyCustomerInfoBymaster.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Customer/SatisfactionInfo.js"></script>
    <script type="text/javascript" src="../js/Common/DropDownListConutry.js"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>

    <script type="text/javascript">
        //登录过期
        var isLogin = '<%=GetUserFlag() %>';
        if (isLogin == "0") {
            window.parent.parent.document.location.href = "../index.html";
        }
        var cst_id = '<%=getCst_Id()%>';
        //获得ID
        var prtid = '<%=GetParentID() %>';
        //用户id
        var userid = '<%=GetUserFlag() %>';
        if (prtid != "") {
            var swfu;
            window.onload = function () {
                swfu = new SWFUpload({

                    upload_url: "../ProcessUpload/upload.aspx?flag=file&prtid=" + prtid + "&userid=" + userid + "&cst_id=" + cst_id,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                    file_types_description: "上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccessShowResult,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_style: '{background-color:#d8d8d8 }',
                    button_width: 61,
                    button_height: 22,
                    button_text: '<span class="fileupload-new">选择文件</span>',
                    button_text_style: '.fileupload-new {background-color:#d8d8d8 ;}',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,
                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
        }
    </script>
    <style type="text/css">
        .table {
            margin-bottom: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">客户信息管理 <small>客户信息编辑</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>客户信息管理</a><i class="fa fa-angle-right"> </i><a>客户信息编辑</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户信息编辑
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    基本信息
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" align="center">
                                        <tr>
                                            <td colspan="4">
                                                <span style="color: red;">提示:黄色背景文本框为必填项！<i class="fa fa-warning "></i></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">编号:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_No" runat="server" class="form-control input-sm" maxlength="15"
                                                    style="background-color: #FFC;" />
                                            </td>
                                            <td style="width: 12%;">客户简称:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_Brief" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>客户名称:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_Name" runat="Server" class="form-control input-sm"
                                                    maxlength="100" style="background-color: #FFC;" />
                                            </td>
                                            <td>公司地址:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Address" runat="Server" class="form-control input-sm"
                                                    maxlength="100" style="background-color: #FFC;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>邮政编码:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCode" runat="Server" class="form-control input-sm" maxlength="6" />
                                            </td>
                                            <td>联系人:
                                            </td>
                                            <td>
                                                <input type="text" id="txtLinkman" runat="Server" class="form-control input-sm" maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>公司电话:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Phone" runat="Server" class="form-control input-sm"
                                                    maxlength="15" />
                                            </td>
                                            <td>传真号:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Fax" runat="Server" class="form-control input-sm" maxlength="20" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    甲方负责人
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn default" data-toggle="modal" href="#JFresponsive"
                                        id="addLinkManActionLink" url="ContactPersionInfo/cp_AddContactPersionInfo.aspx?Cst_Id=<%=getCst_Id()%>">
                                        添加</button>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="cst_datas">
                                            <thead class="flip-content">
                                                <tr id="cst_row" align="center">
                                                    <td align="center" style="width: 5%" id="id">序号
                                                    </td>
                                                    <td align="center" style="width: 10%" id="name">姓名
                                                    </td>
                                                    <td align="center" style="width: 10%" id="zhiwu">职务
                                                    </td>
                                                    <td align="center" style="width: 15%" id="bumen">部门
                                                    </td>
                                                    <td align="center" style="width: 15%" id="phone">电话
                                                    </td>
                                                    <td align="center" style="width: 15%" id="email">Email
                                                    </td>
                                                    <td style="width: 15%" align="center" id="remark">备注
                                                    </td>
                                                    <td align="center" style="width: 15%" id="oper">操作
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <div class="cls_add_container ui-button ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                            style="font-size: 12px; width: 40px; height: 16px;">
                                        </div>
                                        <div class="cls_data_bottom" id="linkman_nodata" style="display: none;">
                                            没有联系人数据！
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    扩展信息
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" align="center">
                                        <tr>
                                            <td>客户英文名:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txtCst_EnglishName" runat="Server" class="form-control input-sm"
                                                    maxlength="350" />
                                            </td>
                                            <td>是否合作:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddIsPartner" runat="Server" CssClass="form-control">
                                                    <asp:ListItem Value="1">是</asp:ListItem>
                                                    <asp:ListItem Value="2">否</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>所在国家:
                                            </td>
                                            <td>
                                                <select class="form-control" id="DropDownListCountry" onchange="GetProvinces(this);">
                                                    <option value="请选择" countryid="0">----请选择国家----</option>
                                                    <asp:Repeater ID="RepeaterCountry" runat="server">
                                                        <ItemTemplate>
                                                            <option value="<%#Eval("CountryName") %>" countryid="<%#Eval("CountryID") %>">
                                                                <%#Eval("CountryName")%></option>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </select>
                                            </td>
                                            <td>所在省份:
                                            </td>
                                            <td>
                                                <select id="DropDownListProvince" class="form-control" onchange="GetCitys(this);">
                                                    <option value="请选择" provinceid="0">----请选择省份----</option>
                                                </select>
                                            </td>
                                            <td>所在城市:
                                            </td>
                                            <td>
                                                <select id="DropDownListCity" onchange="FillHidden(this);" class="form-control">
                                                    <option value="请选择" cityid="0">----请选择城市----</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>客户类型:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddType" runat="Server" CssClass="form-control" AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>所属行业:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddProfession" runat="Server" CssClass="form-control"
                                                    AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                                <span class="help-block"></td>
                                            <td>分支机构:
                                            </td>
                                            <td>
                                                <input type="text" id="txtBranchPart" runat="Server" class="form-control"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Email:
                                            </td>
                                            <td>
                                                <input type="text" class="form-control input-sm" id="txtEmail" runat="Server" maxlength="50" />
                                            </td>
                                            <td>公司主页:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_PrincipalSheet" runat="Server" class="form-control input-sm"
                                                    maxlength="50" />
                                            </td>
                                            <td>关系建立时间:
                                            </td>
                                            <td>
                                                <input type="text" name="txt_date" id="txtCreateRelationTime" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 100%; height: 22px; border: 1px solid #e5e5e5" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>关系部门:
                                            </td>
                                            <td>
                                                <input type="text" id="txtRelationDepartment" runat="Server" class="form-control input-sm"
                                                    maxlength="50" />
                                            </td>
                                            <td>信用级别:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCreditLeve" runat="Server" CssClass="form-control"
                                                    AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>亲密度:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCloseLeve" runat="Server" CssClass="form-control"
                                                    AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>开户银行名称:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txtBankName" runat="Server" class="form-control input-sm"
                                                    maxlength="100" />
                                            </td>
                                            <td>企业代码:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Code" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>开户银行账号:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txtBankAccountNo" runat="Server" class="form-control input-sm"
                                                    maxlength="20" />
                                            </td>
                                            <td>法定代表:
                                            </td>
                                            <td>
                                                <input type="text" id="txtLawPerson" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>纳税人识别号:
                                            </td>
                                            <td colspan="5">
                                                <input type="text" id="txtTaxAccountNo" runat="Server" class="form-control input-sm"
                                                    maxlength="20" style="width: 150px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>备注:
                                            </td>
                                            <td colspan="5">
                                                <textarea class="form-control input-sm" rows="3" id="txtRemark" runat="server"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- 附件-->
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-2">
                                    附件
                                </div>
                                <div class="col-md-8">
                                    <div id="divFileProgressContainer" style="float: right;">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder">
                                        <i class="fa fa-paper-clip"></i>选择文件</span> </span>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="gv_Attach">
                                            <thead class="flip-content">
                                                <tr id="att_row" align="center">
                                                    <td style="width: 40%;" class="hidden-xs" id="att_filename">文件名
                                                    </td>
                                                    <td style="width: 15%;" id="att_uptime">上传时间
                                                    </td>
                                                    <td style="width: 15%;" id="att_filesize">文件大小
                                                    </td>
                                                    <td style="width: 10%;" class="hidden-xs" id="att_filetype">文件类型
                                                    </td>
                                                    <td style="width: 10%;" id="attr_updateuser">用户
                                                    </td>
                                                    <td style="width: 5%;" class="hidden-xs" id="att_oper"></td>
                                                    <td style="width: 5%;" class="hidden-xs" id="att_oper2"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <%--<asp:GridView ID="gv_Attach" runat="server" Width="100%" CellPadding="0" DataKeyNames="ID"
                                            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" ShowHeader="False"
                                            Font-Size="12px" RowStyle-Height="22px" CssClass="gridView_comm cls_TableRow"
                                            OnRowDataBound="gv_Attach_RowDataBound" EnableModelValidation="True">
                                            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="文件名" SortExpression="FileName">
                                                    <ItemTemplate>
                                                        <img style="width: 16px; height: 16px;" src='<%# Eval("FileTypeImg") %>' />
                                                        <asp:Label ID="fileName" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>
                                                        <input type="hidden" value="<%# Eval("ID") %>">
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UploadTime" HeaderText="上传时间" SortExpression="UploadTime">
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="文件大小" DataField="FileSize">
                                                    <ItemStyle Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="文件类型" DataField="FileType">
                                                    <ItemStyle Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UploadUser" HeaderText="用户" SortExpression="UploadUser"
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span onclick="AttachDetele(this);" style="color: Blue; cursor: pointer;">删除</span>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemTemplate>
                                                        <a href="/Customer/DownLoadFile.aspx?SysNo=<%#Eval("ID") %>">下载</a>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                暂时没有附件！
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                            CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                            CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                            OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                            PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                            SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                                            PageSize="5" SubmitButtonClass="submitbtn">
                                        </webdiyer:AspNetPager>--%>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    满意度调查
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn default" data-toggle="modal" href="#MYresponsive"
                                        id="addSatisfactionActionLink" url="ContactPersionInfo/cp_AddContactPersionInfo.aspx?Cst_Id=<%=getCst_Id()%>">
                                        添加</button>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered " id="static_datas">
                                            <thead class="flip-content">
                                                <tr id="static_row" align="center">
                                                    <td align="center" style="width: 10%" id="st_id">序号
                                                    </td>
                                                    <td align="center" style="width: 20%" id="st_content">调查内容
                                                    </td>
                                                    <td align="center" style="width: 15%" id="st_myd">满意度
                                                    </td>
                                                    <td align="center" style="width: 15%" id="st_time">调查时间
                                                    </td>
                                                    <td align="center" style="width: 15%" id="st_type">调查方式
                                                    </td>
                                                    <td align="center" style="width: 15%" id="st_remark">备注
                                                    </td>
                                                    <td align="center" style="width: 15%" id="st_oper">操作
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <div class="cls_add_container ui-button ui-state-default ui-corner-all ui-button-text-only ui-state-hover"
                                            style="font-size: 12px; width: 40px; height: 16px;">
                                        </div>
                                        <div class="cls_data_bottom" id="static_nodata" style="display: none;">
                                            没有满意度数据！
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section">相关合同</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered ">
                                            <tr align="center">
                                                <td align="center" style="width: 6%;">序号
                                                </td>
                                                <td align="center" style="width: 10%">合同编号
                                                </td>
                                                <td align="center" style="width: 30%">合同名称
                                                </td>
                                                <td align="center" style="width: 10%">合同额(万元)
                                                </td>
                                                <td align="center" style="width: 15%">签订日期
                                                </td>
                                                <td align="center" style="width: 15%">完成日期
                                                </td>
                                                <td align="center" style="width: 8%">项目经理
                                                </td>
                                                <td align="center" style="width: 6%">操作
                                                </td>
                                            </tr>

                                        </table>
                                        <asp:GridView ID="gv_rContract" runat="server" Width="100%" CellPadding="0" DataKeyNames="cpr_Id"
                                            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" ShowHeader="False"
                                            Font-Size="12px" RowStyle-Height="22px" CssClass="table table-bordered"
                                            EnableModelValidation="True">
                                            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="cpr_Id" HeaderText="序号" SortExpression="cpr_Id">
                                                    <ItemStyle HorizontalAlign="Center" Width="6%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cpr_No" HeaderText="合同编号" SortExpression="cpr_No">
                                                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="合同名称" SortExpression="cpr_Name">
                                                    <ItemTemplate>
                                                        <img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px;" />
                                                        <a href="#" title='<%# Eval("cpr_Name") %>'>
                                                            <%# Eval("cpr_Name").ToString().Length > 15 ? Eval("cpr_Name").ToString().Substring(0, 15) + "......" : Eval("cpr_Name").ToString()%></a>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="cpr_Acount" HeaderText="合同额" SortExpression="cpr_Acount">
                                                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cpr_SignDate" HeaderText="签订日期" SortExpression="cpr_SignDate"
                                                    DataFormatString="{0:d}">
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cpr_DoneDate" HeaderText="完成日期" SortExpression="cpr_DoneDate"
                                                    DataFormatString="{0:d}">
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ChgPeople" HeaderText="负责人" SortExpression="ChgPeople">
                                                    <ItemStyle HorizontalAlign="Center" Width="8%" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="查看">
                                                    <ItemTemplate>
                                                        <a href='../Coperation/cpr_ShowCoprationBymaster.aspx?flag=cstshow&cprid=<%# Eval("cpr_id") %>'>查看</a>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="6%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                暂时没有关联合同！
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <webdiyer:AspNetPager ID="AspNetPager2" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                            CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条" CustomInfoTextAlign="Left"
                                            FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
                                            PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
                                            ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
                                            TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="5" SubmitButtonClass="submitbtn">
                                        </webdiyer:AspNetPager>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-offset-12 col-md-12">

                                        <asp:Button Text="保存" runat="server" ID="btn_Save" CssClass="btn green" OnClick="btn_Save_Click" />
                                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                            返回</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="HiddenCustomerSysNo" runat="server" Value="<%=getCst_Id() %>" />
    <!--HiddenArea-->
    <input type="hidden" id="hiddenCountry" runat="server" />
    <input type="hidden" id="hiddenProvince" runat="server" />
    <input type="hidden" id="hiddenCity" runat="server" />
    <asp:HiddenField ID="hid_parentid" runat="server" Value="0" />
    <!-- 联系人 -->
    <div id="JFresponsive" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true"
        style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加甲方联系人</h4>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <td style="width: 80px;">序号:</td>
                            <td>
                                <input type="text" id="txtContactNo" class="form-control input-sm" maxlength="350"
                                    style="background-color: #FFC;" /></td>
                            <td style="width: 80px;">姓名:</td>
                            <td>
                                <input type="text" id="txtName" class="form-control input-sm" maxlength="350" style="background-color: #FFC;" />
                            </td>
                        </tr>
                        <tr>
                            <td>职务:</td>
                            <td>
                                <input type="text" id="txtDuties" class="form-control input-sm" maxlength="350" /></td>
                            <td>电话:</td>
                            <td>
                                <input type="text" id="txtPhone" class="form-control input-sm" maxlength="350" style="background-color: #FFC;" />
                            </td>
                        </tr>
                        <tr>
                            <td>部门:</td>
                            <td>
                                <input type="text" id="txtDepartment" class="form-control input-sm" maxlength="350" /></td>
                            <td>商务电话:</td>
                            <td>
                                <input type="text" id="txtBPhone" class="form-control input-sm" maxlength="350" /></td>
                        </tr>
                        <tr>
                            <td>住宅电话:</td>
                            <td>
                                <input type="text" id="txtFPhone" class="form-control input-sm" maxlength="350" /></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>商务传真:</td>
                            <td>
                                <input type="text" id="txtBFax" class="form-control input-sm" maxlength="350" /></td>
                            <td>住宅传真:</td>
                            <td>
                                <input type="text" id="txtFFax" class="form-control input-sm" maxlength="350" /></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>
                                <input type="text" id="LinkManEmail" class="form-control input-sm" maxlength="350" /></td>
                            <td>备注:</td>
                            <td>
                                <textarea class="form-control input-sm" rows="3" id="LinkManRemark"></textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="jf_close" class="btn btn-default">
                取消</button>
            <button type="button" class="btn blue" id="btnJFAdd">
                保存</button>
        </div>
    </div>
    <!-- 满意度 -->
    <div id="MYresponsive" class="modal fade yellow" tabindex="-1" data-width="500" aria-hidden="true"
        style="display: none; width: 500px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加用户满意度</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100px;">序号:</td>
                                <td>
                                    <input type="text" id="txtSat_No" class="form-control input-sm" maxlength="350" style="background-color: #FFC;" /></td>
                            </tr>
                            <tr>
                                <td>调查内容:</td>
                                <td>
                                    <textarea class="form-control input-sm" rows="3" id="Sch_Context" style="background-color: #FFC;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>调查时间:</td>
                                <td>
                                    <input type="text" name="txt_date" id="txtSch_Time" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" style="width: 100%; height: 34px; border: 1px solid #e5e5e5; background-color: #FFC;" /></td>
                            </tr>
                            <tr>
                                <td>满意度:</td>
                                <td>
                                    <select id="ddSat_Leve" class="form-control input-sm">
                                        <option value="1">满意</option>
                                        <option value="2">非常满意</option>
                                        <option value="3">不满意</option>
                                        <option value="4">非常不满意</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td>调查原因:</td>
                                <td>
                                    <input type="text" id="txtSch_Reason" class="form-control input-sm" maxlength="350" /></td>
                            </tr>
                            <tr>
                                <td>调查方式:</td>
                                <td>
                                    <select id="ddSat_Way" class="form-control input-sm">
                                        <option value="1">问卷调查</option>
                                        <option value="2">统计调查</option>
                                        <option value="3">询问调查</option>
                                        <option value="4">电话调查</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td>最满意方面:</td>
                                <td>
                                    <input type="text" id="txtSat_Best" class="form-control input-sm" maxlength="350" /></td>
                            </tr>
                            <tr>
                                <td>最不满意方面:</td>
                                <td>
                                    <input type="text" id="txtSat_NotBest" class="form-control input-sm" maxlength="350" /></td>
                            </tr>
                            <tr>
                                <td>备注:</td>
                                <td>
                                    <textarea class="form-control input-sm" rows="3" id="Sat_Remark"></textarea></td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" id="sat_close" data-dismiss="modal" class="btn btn-default">
                取消</button>
            <button type="button" class="btn blue" id="btnSaveSatisfaction">
                保存</button>
        </div>
    </div>
    <!--
    显示基本信息 -->
    <div id="Show_cstInfo" class="modal fade yellow" tabindex="-1" data-width="500" aria-hidden="true"
        style="display: none; width: 500px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人信息</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100px;">序号:</td>
                                <td style="width: 100px;"><span id="lbl_No"></span></td>
                                <td style="width: 100px;">姓名:</td>
                                <td style="width: 100px;"><span id="lbl_name"></span></td>
                            </tr>
                            <tr>
                                <td>职务:</td>
                                <td><span id="lbl_duties"></span></td>
                                <td>电话:</td>
                                <td><span id="lbl_phone"></span></td>
                            </tr>
                            <tr>
                                <td>部门:</td>
                                <td><span id="lbl_department"></span></td>
                                <td>商务电话:</td>
                                <td><span id="lbl_bphone"></span></td>
                            </tr>
                            <tr>
                                <td>住宅电话:</td>
                                <td><span id="lbl_fphone"></span></td>
                                <td>商务传真:</td>
                                <td><span id="lbl_bfax"></span></td>
                            </tr>
                            <tr>
                                <td>住宅传真:</td>
                                <td><span id="lbl_Ffax"></span></td>
                                <td>Email:</td>
                                <td><span id="lbl_email"></span></td>
                            </tr>
                            <tr>
                                <td>备注:</td>
                                <td colspan="3">
                                    <span id="lbl_bz"></span>
                                </td>

                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">
                    关闭</button>
            </div>
        </div>
    </div>
    <!--满意度信息-->
    <div id="Show_sfcInfo" class="modal fade yellow" tabindex="-1" data-width="500" aria-hidden="true"
        style="display: none; width: 500px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">满意度调查信息</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100px;">序号:</td>
                                <td><span id="lblSat_No"></span></td>
                            </tr>
                            <tr>
                                <td>调查内容:</td>
                                <td><span id="lblSch_Context"></span></td>
                            </tr>
                            <tr>
                                <td>调查时间:</td>
                                <td><span id="lblSch_Time"></span></td>
                            </tr>
                            <tr>
                                <td>满意度:</td>
                                <td><span id="lblSat_Leve"></span></td>
                            </tr>
                            <tr>
                                <td>调查方式:</td>
                                <td><span id="lblSch_Way"></span></td>
                            </tr>
                            <tr>
                                <td>最满意方面:</td>
                                <td><span id="lblSat_Best"></span></td>
                            </tr>
                            <tr>
                                <td>最不满意方面:</td>
                                <td><span id="lblSat_NotBest"></span></td>
                            </tr>
                            <tr>
                                <td>备注:</td>
                                <td><span id="lblRemark"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">
                    关闭</button>
            </div>
        </div>
    </div>
    <input type="hidden" id="hidCity" runat="server" />
</asp:Content>
