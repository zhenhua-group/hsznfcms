﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cst_CustomerInfoListByMaster.aspx.cs" Inherits="TG.Web.Customer.cst_CustomerInfoListByMaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Customer/cst_CustomerInfoList_jq.js"></script>
    <style type="text/css">
        #tbl_id td {
            height: 40px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">客户信息管理 <small>客户信息列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>客户信息管理</a><i class="fa fa-angle-right"> </i><a>客户信息列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>查询客户
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">选择查询项
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid_sh">
                                <label>
                                    <input type="checkbox" value="all" />全选
                                </label>
                                <%=ColumnsContent_Sh %>
                            </div>
                        </div>
                        <div class="btn-group" data-toggle="buttons" id="div_sch">
                            <span class="btn blue">查询方式：</span>
                            <label rel="and" class="btn yellow btn-sm active" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option1" value="and">必须
                            </label>
                            <label rel="or" class="btn default btn-sm" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option2" value="or">或
                            </label>
                        </div>

                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table border="0" cellspacing="0" cellpadding="0" id="tbl_id" style="width: 92%;">
                                        <tr>
                                            <td>生产部门:</td>
                                            <td style="width: 24%;">
                                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>年份:</td>
                                            <td style="width: 23%;">
                                                <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>客户名称:</td>
                                            <td style="width: 23%;">
                                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="server" /></td>

                                            <%--<td>
                                                <input type="button" class="btn blue" id="btn_search" value="查询" /></td>--%>
                                        </tr>
                                        <tr>
                                            <td >录入时间:
                                            </td>
                                            <td>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                                                            至:<input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </td>
                                            <td>客户编号：</td>
                                            <td>
                                                <asp:TextBox ID="txtCst_No" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            </td>
                                            <td>联系人：</td>
                                            <td>
                                                <asp:TextBox ID="txtLinkman" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" id="tbSearch">
                                        <tr>
                                            <td align="center">
                                                <input type="button" class="btn blue" id="btn_search" value="查询" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户列表信息
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <label>
                                    <input type="checkbox" value="all" />全选
                                </label>
                                <%=ColumnsContent %>
                            </div>
                        </div>
                        <asp:Button ID="btn_Output" runat="server" Text="导出Excel" CssClass="btn red btn-sm"  />
                    </div>

                </div>
                <div class="portlet-body form">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table id="tb_serchDetail">
        <tr style="display: none;" rel="Cpy_Phone">
            <td rel="Cpy_Phone">联系电话：</td>
            <td>
                <asp:TextBox ID="txtCpy_Phone" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="Cpy_Address">
            <td rel="Cpy_Address">公司地址：</td>
            <td>
                <asp:TextBox ID="txtCpy_Address" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="Code">
            <td rel="Code">邮政编码：</td>
            <td>
                <asp:TextBox ID="txtCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="Cpy_Fax">
            <td rel="Cpy_Fax">传真号：</td>
            <td>
                <asp:TextBox ID="txtCpy_Fax" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="InsertUser">
            <td rel="InsertUser">录入人：</td>
            <td>
                <asp:TextBox ID="txt_InsertName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="Cst_Brief">
            <td rel="Cst_Brief">客户简称：</td>
            <td>
                <asp:TextBox ID="txtCst_Brief" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="Cst_EnglishName">
            <td rel="Cst_EnglishName">客户英文名：</td>
            <td>
                <asp:TextBox ID="txtCst_EnglishName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="PartnerName">
            <td rel="PartnerName">是否合作：</td>
            <td>
                <asp:DropDownList ID="ddIsPartner" runat="Server" CssClass="form-control">
                    <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                    <asp:ListItem Value="1">是</asp:ListItem>
                    <asp:ListItem Value="2">否</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="display: none;" rel="Country">
            <td rel="Country">所在国家：</td>
            <td>
                <asp:TextBox ID="txt_Country" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>

        </tr>
        <tr style="display: none;" rel="Province">
            <td rel="Province">所在省份：</td>
            <td>
                <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="City">
            <td rel="City">所在城市：</td>
            <td>
                <asp:TextBox ID="txtCity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="TypeName">
            <td rel="TypeName">客户类型：</td>
            <td>
                <asp:DropDownList ID="ddType" runat="Server" CssClass="form-control" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="display: none;" rel="ProfessionName">

            <td rel="ProfessionName">所属行业：</td>
            <td>
                <asp:DropDownList ID="ddProfession" runat="Server" CssClass="form-control"
                    AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="display: none;" rel="BranchPart">
            <td rel="BranchPart">分支机构：</td>
            <td>
                <asp:TextBox ID="txtBranchPart" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="Email">
            <td rel="Email">邮箱：</td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="Cpy_PrincipalSheet">
            <td rel="Cpy_PrincipalSheet">公司主页：</td>
            <td>
                <asp:TextBox ID="txtCpy_PrincipalSheet" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>
        <tr style="display: none;" rel="RelationTime">
            <td rel="RelationTime"  >关系建立时间：</td>
            <td>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" id="txt_date_Start" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                            至 
                                                        <input type="text" id="txt_date_End" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr style="display: none;" rel="RelationDepartment">
            <td rel="RelationDepartment">关系部门：</td>
            <td>
                <asp:TextBox ID="txtRelationDepartment" runat="server" CssClass="form-control input-sm"></asp:TextBox>
            </td>
        </tr>

        <tr style="display: none;" rel="CreditLeveName">
            <td rel="CreditLeveName">信用级别:
            </td>
            <td>
                <asp:DropDownList ID="ddCreditLeve" runat="Server" CssClass="form-control"
                    AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="display: none;" rel="CloseLeveName">
            <td rel="CloseLeveName">亲密度:
            </td>
            <td>
                <asp:DropDownList ID="ddCloseLeve" runat="Server" CssClass="form-control"
                    AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="display: none;" rel="BankName">
            <td rel="BankName">开户银行名称:
            </td>
            <td>
                <input type="text" id="txtBankName" runat="Server" class="form-control input-sm" />
            </td>

        </tr>
        <tr style="display: none;" rel="Cpy_Code">
            <td rel="Cpy_Code">企业代码:
            </td>
            <td>
                <input type="text" id="txtCpy_Code" runat="Server" class="form-control input-sm" />
            </td>
        </tr>
        <tr style="display: none;" rel="BankAccountNo">
            <td rel="BankAccountNo">开户银行账号:
            </td>
            <td>
                <input type="text" id="txtBankAccountNo" runat="Server" class="form-control input-sm" />
            </td>
        </tr>
        <tr style="display: none;" rel="LawPerson">
            <td rel="LawPerson">法定代表:
            </td>
            <td>
                <input type="text" id="txtLawPerson" runat="Server" class="form-control input-sm" />
            </td>
        </tr>
        <tr style="display: none;" rel="TaxAccountNo">

            <td rel="TaxAccountNo">纳税人识别号:
            </td>
            <td>
                <input type="text" id="txtTaxAccountNo" runat="Server" class="form-control input-sm" />
            </td>
        </tr>
        <tr style="display: none;" rel="Remark">
            <td rel="Remark">备注:
            </td>
            <td>
                <input class="form-control input-sm" id="txtRemark" runat="server"></input>
            </td>
        </tr>
    </table>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_ColumnsID" runat="server" Value="" />
</asp:Content>
