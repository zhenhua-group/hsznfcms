﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cp_ContactPersionInfoBymaster.aspx.cs" Inherits="TG.Web.Customer.ContactPersionInfo.cp_ContactPersionInfoBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Customer/cp_ContactPersionInfo_jq.js"></script>
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">客户信息管理 <small>联系人信息</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>客户信息管理</a><i class="fa fa-angle-right"> </i><a>联系人信息</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>查询联系人
                    </div>
                    
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>联系人姓名:</td>
                            <td>
                                <input type="text" class="form-control input-sm"
                                    id="txt_customerNum" runat="server" /></td>
                            <td>录入时间:<input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>截止时间:<input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>
                                <input type="button" class="btn blue"
                                    id="btn_search" value="查询" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>联系人信息
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <label>
                                    <input type="checkbox" value="all"/>全选
                                </label>
                                <%=ColumnsContent %>                               
                            </div>
                        </div>
                         <asp:Button ID="btn_Output" runat="server" Text="导出Excel" CssClass="btn red btn-sm" OnClick="btn_Output_Click" />
                    </div>
                   
                </div>
                <div class="portlet-body form" style="display: block;">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 显示基本信息 -->
    <div id="Show_cstInfo" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人信息</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <tr>
                                <td style="width: 15%;">序号:</td>
                                <td colspan="3"><span id="No"></span></td>

                            </tr>
                            <tr>
                                <td>姓名:</td>
                                <td style="width: 35%;"><span id="SpanName"></span></td>
                                <td style="width: 15%;">职务:</td>
                                <td><span id="Span2"></span></td>
                            </tr>
                            <tr>
                                <td>电话:</td>
                                <td><span id="Span3"></span></td>
                                <td>部门:</td>
                                <td><span id="department"></span></td>
                            </tr>
                            <tr>
                                <td>商务电话:</td>
                                <td><span id="bphone"></span></td>
                                <td>住宅电话:</td>
                                <td><span id="fphone"></span></td>
                            </tr>
                            <tr>
                                <td>商务传真:</td>
                                <td><span id="bfax"></span></td>
                                <td>住宅传真:</td>
                                <td><span id="Ffax"></span></td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td colspan="3"><span id="Span4"></span></td>

                            </tr>
                            <tr>
                                <td>备注:</td>
                                <td colspan="3"><span id="bz"></span></td>

                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">
                    关闭</button>
            </div>
        </div>
    </div>
    <input type="hidden" id="hiddenUserSysno" value="<%=UserSysNo %>" />
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
