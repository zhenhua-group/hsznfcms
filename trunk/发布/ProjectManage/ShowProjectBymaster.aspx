﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ShowProjectBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.ShowProjectBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/ProjectMamage/ShowProjBymaster.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            CommonControl.SetFormWidth();
            ChangedBackgroundColor();
            //行背景
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr").hover(function () {
                $(this).addClass("tr_in");
            }, function () {
                $(this).removeClass("tr_in");
            });
            //隔行变色
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr:even").css({ background: "White" });

            $.each($(".progressbar"), function (index, item) {
                var percent = parseFloat($(item).attr("percent"));
                $(item).progressbar({
                    value: percent
                });
            });

            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr").each(function () {
                if ($.trim($(this).children(":last").text()) == "分配被驳回") {
                    $(this).children(":last").css({ color: "Red" });
                    $(this).children().children(".progressbar").children("div:first").css({ background: "Red" });
                }
            });
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValueJ tr").hover(function () {
                $(this).addClass("tr_in");
            }, function () {
                $(this).removeClass("tr_in");
            });
            //隔行变色
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValueJ tr:even").css({ background: "White" });

            $.each($(".progressbar"), function (index, item) {
                var percent = parseFloat($(item).attr("percent"));
                $(item).progressbar({
                    value: percent
                });
            });

            $("#ctl00_ContentPlaceHolder1_gv_ProjectValueJ tr").each(function () {
                if ($.trim($(this).children(":last").text()) == "分配被驳回") {
                    $(this).children(":last").css({ color: "Red" });
                    $(this).children().children(".progressbar").children("div:first").css({ background: "Red" });
                }
            });
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValueH tr").hover(function () {
                $(this).addClass("tr_in");
            }, function () {
                $(this).removeClass("tr_in");
            });
            //隔行变色
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValueH tr:even").css({ background: "White" });

            $.each($(".progressbar"), function (index, item) {
                var percent = parseFloat($(item).attr("percent"));
                $(item).progressbar({
                    value: percent
                });
            });

            $("#ctl00_ContentPlaceHolder1_gv_ProjectValueH tr").each(function () {
                if ($.trim($(this).children(":last").text()) == "分配被驳回") {
                    $(this).children(":last").css({ color: "Red" });
                    $(this).children().children(".progressbar").children("div:first").css({ background: "Red" });
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">项目管理</a><i
        class="fa fa-angle-right"> </i><a href="#">项目查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目信息
                    </div>

                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_Output" runat="server" Text="导出" CssClass="btn red btn-sm" OnClick="btn_Output_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">项目信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 120px;">项目名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_name" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                        </td>
                                        <td style="width: 120px;">合同关联:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>管理级别:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td>审核级别 :
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设单位:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtbuildAddress" runat="server"></asp:Label>
                                        </td>
                                        <td>建设规模:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_scale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                        </td>
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_unit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>结构样式:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="structType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                        <td>建筑分类:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="buildStructType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>设计阶段:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td>项目来源:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtproAcount" runat="server" Width="100px"></asp:Label>万元
                                        </td>
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_Aperson" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_phone" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>执行设总:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_PMName" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_PMPhone" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目开始时间:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_startdate" runat="server"></asp:Label>
                                        </td>
                                        <td>项目完成时间:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_finishdate" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目概况:
                                        </td>
                                        <td colspan="3">
                                            <div style="height: auto;">
                                                <span id="txt_sub" runat="Server"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目备注:
                                        </td>
                                        <td colspan="3">
                                            <div style="height: auto;">
                                                <span id="txt_remark" runat="Server"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目高清图:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12" id="img_container" style="text-align: center">
                                                <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                                    style="width: 250px; height: 150px;" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <h4 class="form-section">项目高清图
                    </h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-advance table-hover" align="center" style="width: 98%;" id="datas_att">
                                        <thead>
                                            <tr id="att_row">
                                                <td style="width: 40px;" align="center" id="att_id">序号
                                                </td>
                                                <td style="width: 300px;" align="center" id="att_filename">文件名称
                                                </td>
                                                <td style="width: 80px" align="center" id="att_filesize">文件大小
                                                </td>
                                                <td style="width: 80px;" align="center" id="att_filetype">文件类型
                                                </td>
                                                <td style="width: 120px;" align="center" id="att_uptime">上传时间
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper">&nbsp;
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper2">&nbsp;
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>策划信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Literal ID="PlanInfo" runat="server">
                                无策划信息
                            </asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目进度
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Literal ID="ScheduleInfo" runat="server">
                                无项目进度
                            </asp:Literal>
                            <asp:GridView ID="grid_mem" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                OnRowDataBound="grid_mem_RowDataBound" HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundField DataField="DesignLevel" HeaderText="阶段">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="StartDateString" HeaderText="开始时间">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EndDateString" HeaderText="结束时间">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SumPaper" HeaderText="图纸张数">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>图纸信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <div class="row">
                        <div class="col-md-12">
                            <asp:Literal ID="PagebackInfo" runat="server">
                                无图纸信息
                            </asp:Literal>
                            <asp:GridView ID="grd_pack" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                HeaderStyle-HorizontalAlign="Center">
                                <Columns>
                                    <asp:BoundField DataField="wjsl" HeaderText="文件数量">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="16%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DTJ" HeaderText="待提交">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="16%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DDY" HeaderText="待打印">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="16%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="YDY" HeaderText="已打印">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="16%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="YSC" HeaderText="已删除">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="16%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="YGD" HeaderText="已归档">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="16%" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>分配信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <div class="row">
                        <div class="col-md-12">
                            <asp:Literal ID="Literal3" runat="server">
                                无分配信息
                            </asp:Literal>
                            <asp:GridView ID="gv_ProjectValue" runat="server" CellPadding="0" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-hover" RowStyle-HorizontalAlign="Center"
                                ShowHeader="true" Font-Size="12px" HeaderStyle-HorizontalAlign="Center" RowStyle-Height="22px"
                                Width="100%">
                                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                                <Columns>
                                    <asp:BoundField DataField="AllotTimes" ItemStyle-HorizontalAlign="Center" HeaderText="分配次数"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PayShiCount" ItemStyle-HorizontalAlign="Center" HeaderText="实收金额(万元)"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotCount" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:F}"
                                        HeaderStyle-HorizontalAlign="Center" HeaderText=" 分配金额(万元)">
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotDate" ItemStyle-HorizontalAlign="Center" HeaderText="分配时间"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <%-- <asp:BoundField DataField="UnitValueCount" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                </asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="状态" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div class="progressbar" id="auditLocusContainer" action="A" percent="<%#Eval("Percent") %>"
                                                istranhave="<%#Eval("ISTrunHavc") %>" istraneconomy="<%#Eval("IsTrunEconomy") %>"
                                                referencesysno="<%# Eval("AuditSysNo") %>" style="cursor: pointer; height：98%;">
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Eval("LinkAction")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="8%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    无分配记录！
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <asp:GridView ID="gv_ProjectValueJ" runat="server" CellPadding="0" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-hover" RowStyle-HorizontalAlign="Center"
                                ShowHeader="true" Font-Size="12px" RowStyle-Height="22px" Width="100%">
                                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                                <Columns>
                                    <asp:BoundField DataField="AllotTimes" ItemStyle-HorizontalAlign="Center" HeaderText="分配次数"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PayShiCount" ItemStyle-HorizontalAlign="Center" HeaderText=" 实收金额(万元)"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotCount" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:F}"
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="分配金额(万元)">
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotDate" ItemStyle-HorizontalAlign="Center" HeaderText="分配时间"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="StatusString" ItemStyle-HorizontalAlign="Center" HeaderText="审核状态"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <%-- <asp:TemplateField HeaderText="状态" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div class="progressbar" id="auditLocusContainer" action="JJS" percent="<%#Eval("Percent") %>"
                                                referencesysno="<%# Eval("AuditSysNo") %>" style="cursor: pointer; height：98%;">
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="项目策划" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Eval("LinkAction")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    无分配记录！
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <asp:GridView ID="gv_ProjectValueH" runat="server" CellPadding="0" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-hover" RowStyle-HorizontalAlign="Center"
                                ShowHeader="true" Font-Size="12px" RowStyle-Height="22px" Width="100%">
                                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                                <Columns>
                                    <asp:BoundField DataField="AllotTimes" ItemStyle-HorizontalAlign="Center" HeaderText="分配次数"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PayShiCount" ItemStyle-HorizontalAlign="Center" HeaderText="  实收金额(万元)"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotCount" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:F}"
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="分配金额(万元)">
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundField>

                                    <asp:BoundField DataField="AllotDate" ItemStyle-HorizontalAlign="Center" HeaderText="分配时间"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <%-- <asp:BoundField DataField="UnitValueCount" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                </asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="状态" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div class="progressbar" id="auditLocusContainer" action="H" percent="<%#Eval("Percent") %>"
                                                istranhave="<%#Eval("ISTrunHavc") %>" istraneconomy="<%#Eval("IsTrunEconomy") %>"
                                                referencesysno="<%# Eval("AuditSysNo") %>" style="cursor: pointer;">
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="项目策划" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Eval("LinkAction")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="8%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    无分配记录！
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">

                            <button type="button" class="btn default" onclick="javascript:history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
