﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectAuditEdit.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProjectAudit.ProjectAuditEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../../css/Customer.css" rel="stylesheet" type="text/css" />
    <link href="../../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../../css/ProjStructStyleShow.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../../js/Global.js"></script>
    <script type="text/javascript" src="../../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../../js/Common/SendMessageCommon.js"></script>
    <script src="../../js/ProjectMamage/ProjectAuditEdit.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/ProjectMamage/ProjectAuditShow.js"></script>
    <script type="text/javascript" src="../../js/MessageComm.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <style type="text/css">
        .title
        {
            width: 200px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetFormWidth();
            //行变色
            $(".show_project tr:odd").attr("style", "background-color:#FFF");
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[项目管理]
            </td>
        </tr>
    </table>
    <table class="show_project">
        <tr>
            <td class="title">
                项目名称：
            </td>
            <td class="content" style="width: 400px;">
                <asp:Label ID="txt_name" runat="server"></asp:Label>
                <asp:HiddenField ID="hid_projid" runat="server" Value="" />
            </td>
            <td class="title">
                关联合同：
            </td>
            <td class="content">
                <asp:Label ID="txt_reletive" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                管理级别：
            </td>
            <td class="content">
                <asp:Label ID="lbl_level" runat="server"></asp:Label>
            </td>
            <td class="title">
                审核级别：
            </td>
            <td class="content">
                <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                建筑单位：
            </td>
            <td class="content" colspan="3">
                <asp:Label ID="txtbuildUnit" runat="server" Width="400px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                建设地点：
            </td>
            <td class="content">
                <asp:Label ID="txtbuildAddress" runat="server" Width="400px"></asp:Label>
            </td>
            <td class="title">
                建设规模：
            </td>
            <td class="content">
                <asp:Label ID="txt_scale" runat="server" Width="100px"></asp:Label>
                平米
            </td>
        </tr>
        <tr>
            <td class="title">
                建筑类别：
            </td>
            <td class="content">
                <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
            </td>
            <td class="title">
                承接部门：
            </td>
            <td class="content" id="content">
                <asp:Label ID="txt_unit" runat="server" Width="150px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                结构形式：
            </td>
            <td class="content">
                <div class="struct_tree">
                    <asp:Literal ID="structType" runat="server"></asp:Literal></div>
            </td>
            <td class="title">
                建筑分类：
            </td>
            <td class="content">
                <div class="struct_tree">
                    <asp:Literal ID="buildStructType" runat="server"></asp:Literal></div>
            </td>
        </tr>
        <tr>
            <td class="title">
                设计阶段：
            </td>
            <td class="content">
                <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
            </td>
            <td class="title">
                项目来源：
            </td>
            <td class="content">
                <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                合同额：
            </td>
            <td class="content">
                <asp:Label ID="txtproAcount" runat="server" Width="100px"></asp:Label>万元
            </td>
            <td class="title">
                行业性质：
            </td>
            <td class="content">
                <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                甲方负责人：
            </td>
            <td class="content">
                <asp:Label ID="txt_Aperson" runat="server" Style="width: 150px;"></asp:Label>
            </td>
            <td class="title">
                电话：
            </td>
            <td class="content">
                <asp:Label ID="txt_phone" runat="server" Width="150px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                项目总负责：
            </td>
            <td class="content">
                <asp:Label ID="txt_PMName" runat="server" Width="150px"></asp:Label>
            </td>
            <td class="title">
                电话：
            </td>
            <td class="content">
                <asp:Label ID="txt_PMPhone" runat="server" Width="150px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                项目开始日期：
            </td>
            <td class="content">
                <asp:Label ID="txt_startdate" runat="server" Style="width: 150px;"></asp:Label>
            </td>
            <td class="title">
                项目完成日期：
            </td>
            <td class="content">
                <asp:Label ID="txt_finishdate" runat="server" Style="width: 150px;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                其他参与部门：
            </td>
            <td class="content">
                <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
            </td>
            <td class="title">
                &nbsp;
            </td>
            <td class="content">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="title">
                项目特征概况：
            </td>
            <td class="content" colspan="3">
                <asp:TextBox ID="txt_sub" runat="server" Height="68px" Width="98%" TextMode="MultiLine"
                    MaxLength="250" CssClass="cls_input_text_w" BorderStyle="None"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="title">
                项目效果图：
            </td>
            <td colspan="3" align="center">
                <div id="img_container" style="width: 750px;">
                    <img id="img_small" alt="效果缩略图" src="~/Attach_User/filedata/tempimg/tempimg.jpg"
                        style="width: 250px; height: 150px;" runat="Server" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="title">
                项目高清图：
            </td>
            <td colspan="3">
                <table id="datas_att" class="cls_content_head" style="width: 560px;">
                    <tr id="att_row">
                        <td id="att_id" align="center" style="width: 30px;">
                            序号
                        </td>
                        <td id="att_filename" align="center" style="width: 200px;">
                            文件名称
                        </td>
                        <td id="att_filesize" align="center" style="width: 80px">
                            文件大小
                        </td>
                        <td id="att_filetype" align="center" style="width: 80px;">
                            文件类型
                        </td>
                        <td id="att_uptime" align="center" style="width: 120px;">
                            上传时间
                        </td>
                        <td id="att_oper2" align="center" style="width: 40px;">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="title">
                项目备注：
            </td>
            <td colspan="3">
                <asp:TextBox ID="txt_remark" runat="server" Height="68px" Width="98%" TextMode="MultiLine"
                    MaxLength="250" CssClass="cls_input_text_w" BorderStyle="None"></asp:TextBox>
            </td>
        </tr>
    </table>
    <fieldset id="alterAudit" style="font-size: 12px; font-family: 微软雅黑">
        <legend>修改内容</legend>
        <table style="width: 100%; font-size: 12px" id="optiontab" class="show_project">
            <%=Creattable %>
        </table>
    </fieldset>
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    </form>
    <div id="GOEdit" style="width: 100%; height: 25px; display: none">
        <table style="width: 100%;" class="cls_show_cst_jiben" id="Table1">
            <tr>
                <td align="center">
                    <input type="button" id="Button1" style="background-color: #E8E8E8;" class="cls_btn_comm_w"
                        value="返回" />
                </td>
            </tr>
        </table>
    </div>
    <!--hiddenArea-->
    <input type="hidden" id="ProjectAuditSysNoHidden" value="<%=ProjectAuditSysNo %>" />
    <input type="hidden" id="hiddenProjectSysNo" value="<%=ProjectSysNo %>" />
    <input type="hidden" id="UserSysNoHidden" value="<%=UserSysNo %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="display: none; width: 400px; height: 200px;">
    </div>
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
</body>
<script type="text/javascript">
    $(function () {

        var projectAudit = new ProjectAuditClass($("#containerTable"));
        var stauts = $("#hiddenMessageStatus").val();
        var projectSysno = $("#hiddenProjectSysNo").val();
        //判断当前部门
        var curunitid = $("#userUnitNum").val();
        //经济所
        if (curunitid == '经济所') {
            $("#chk_ISTrunEconomy").hide().next('label').hide();
        }
        else if (curunitid == '暖通热力所') {//暖通所
            $("#chk_ISHvac").hide().next('label').hide();
        }
        else {//土建所
            $("#chk_ISArch").hide().next('label').hide();
        }
        if (stauts == "D") {
            $("#Button1").click(function () {
                window.history.back();
            })
        }

    });
</script>
</html>
