﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectAudit.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectAudit.ProjectAudit" %>

<%@ Register Src="../../UserControl/ChooseUser.ascx" TagName="ChooseUser" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../../css/Customer.css" rel="stylesheet" type="text/css" />
    <link href="../../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../../css/ProjStructStyleShow.css" rel="stylesheet" type="text/css" />
    <link href="../../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>
    <script src="../../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../../js/Global.js"></script>
    <script type="text/javascript" src="../../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../../js/Common/SendMessageCommon.js"></script>
    <script type="text/javascript" src="../../js/ProjectMamage/ProjectAudit.js"></script>
    <script src="../../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../../js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/ProjectMamage/ProjectAuditShow.js"></script>
    <script type="text/javascript" src="../../js/MessageComm.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script src="../../js/Common/ShowDivDialog.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetFormWidth();
            //行变色
            $(".show_project tr:odd").attr("style", "background-color:#FFF");
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <div id="div">
        <table class="cls_container">
            <tr>
                <td class="cls_head">
                    &nbsp;&nbsp;当前位置：[项目管理]
                </td>
            </tr>
        </table>
        <table class="show_project">
            <tr>
                <td style="width: 105px;">
                    项目名称：
                </td>
                <td class="content" style="width: 400px;">
                    <asp:Label ID="txt_name" runat="server"></asp:Label>
                    <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                </td>
                <td style="width: 105px;">
                    关联合同：
                </td>
                <td class="content">
                    <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    管理级别：
                </td>
                <td class="content">
                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                </td>
                <td>
                    审核级别：
                </td>
                <td class="content">
                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    建筑单位：
                </td>
                <td class="content" colspan="3">
                    <asp:Label ID="txtbuildUnit" runat="server" Width="400px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    建设地点：
                </td>
                <td class="content">
                    <asp:Label ID="txtbuildAddress" runat="server" Width="400px"></asp:Label>
                </td>
                <td>
                    建设规模：
                </td>
                <td class="content">
                    <asp:Label ID="txt_scale" runat="server" Width="100px"></asp:Label>
                    平米
                </td>
            </tr>
            <tr>
                <td>
                    建筑类别：
                </td>
                <td class="content">
                    <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                </td>
                <td>
                    承接部门：
                </td>
                <td class="content" id="content">
                    <asp:Label ID="txt_unit" runat="server" Width="150px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    结构形式：
                </td>
                <td class="content">
                    <div class="struct_tree">
                        <asp:Literal ID="structType" runat="server"></asp:Literal></div>
                </td>
                <td>
                    建筑分类：
                </td>
                <td class="content">
                    <div class="struct_tree">
                        <asp:Literal ID="buildStructType" runat="server"></asp:Literal></div>
                </td>
            </tr>
            <tr>
                <td>
                    设计阶段：
                </td>
                <td class="content">
                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                </td>
                <td>
                    项目来源：
                </td>
                <td class="content">
                    <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    合同额：
                </td>
                <td class="content">
                    <asp:Label ID="txtproAcount" runat="server" Width="100px"></asp:Label>万元
                </td>
                <td>
                    行业性质：
                </td>
                <td class="content">
                    <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    甲方负责人：
                </td>
                <td class="content">
                    <asp:Label ID="txt_Aperson" runat="server" Style="width: 150px;"></asp:Label>
                </td>
                <td>
                    电话：
                </td>
                <td class="content">
                    <asp:Label ID="txt_phone" runat="server" Width="150px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    项目总负责：
                </td>
                <td class="content">
                    <asp:Label ID="txt_PMName" runat="server" Width="150px"></asp:Label>
                </td>
                <td>
                    电话：
                </td>
                <td class="content">
                    <asp:Label ID="txt_PMPhone" runat="server" Width="150px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    项目开始日期：
                </td>
                <td class="content">
                    <asp:Label ID="txt_startdate" runat="server" Style="width: 150px;"></asp:Label>
                </td>
                <td>
                    项目完成日期：
                </td>
                <td class="content">
                    <asp:Label ID="txt_finishdate" runat="server" Style="width: 150px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    其他参与部门：
                </td>
                <td class="content">
                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
                <td class="content">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    项目特征概况：
                </td>
                <td class="content" colspan="3">
                    <div style="width: 900px; height: auto;">
                        <span id="txt_sub" runat="Server"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    项目效果图：
                </td>
                <td colspan="3" align="center">
                    <div id="img_container" style="width: 750px;">
                        <img id="img_small" alt="效果缩略图" src="~/Attach_User/filedata/tempimg/tempimg.jpg"
                            style="width: 250px; height: 150px;" runat="Server" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    项目高清图：
                </td>
                <td colspan="3">
                    <table id="datas_att" class="cls_content_head" style="width: 560px;">
                        <tr id="att_row">
                            <td id="att_id" align="center" style="width: 30px;">
                                序号
                            </td>
                            <td id="att_filename" align="center" style="width: 200px;">
                                文件名称
                            </td>
                            <td id="att_filesize" align="center" style="width: 80px">
                                文件大小
                            </td>
                            <td id="att_filetype" align="center" style="width: 80px;">
                                文件类型
                            </td>
                            <td id="att_uptime" align="center" style="width: 120px;">
                                上传时间
                            </td>
                            <td id="att_oper2" align="center" style="width: 40px;">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    项目备注：
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txt_remark" runat="server" Height="68px" Width="98%" TextMode="MultiLine"
                        MaxLength="250" CssClass="cls_input_text_w" BorderStyle="None"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div style="width: 100%" id="auditHtml">
        </div>
        <div id="divShowEdit" style="text-align: center;">
            <table style="width: 100%;" class="cls_show_cst_jiben" id="Table1">
                <tr>
                    <td align="right" style="width: 50%;">
                        <input type="button" class="cls_btn_comm_w" style="width: 80px; background-color: #E8E8E8"
                            id="Button1" name="controlBtn" value="返回" />
                    </td>
                    <td align="left" style="width: 50%;">
                        <input type="button" title="导出勘察任务通知单" class="cls_btn_comm_w" style="width: 80px;
                            background-color: #E8E8E8" id="btnReport" name="controlBtn" value="导出" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <div id="chooseUserMain" style="display: none;">
        <uc1:ChooseUser ID="ChooseUser1" runat="server" />
    </div>
    </form>
    <!--hiddenArea-->
    <input type="hidden" id="ProjectAuditSysNoHidden" value="<%=ProjectAuditSysNo %>" />
    <input type="hidden" id="UserSysNoHidden" value="<%=UserSysNo %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <input type="hidden" id="HiddenAuditStatus" value="<%=HiddenAuditStatus %>" />
    <input type="hidden" id="ProjectSysNo" value="<%=ProjectSysNo %>" />
    <input type="hidden" id="HiddenCoperationName" value="<%=HiddenCoperationName %>" />
    <input type="hidden" id="HiddenProjectPlanAuditSysNo" value="<%=HiddenProjectPlanAuditSysNo %>" />
    <input type="hidden" id="HiddenManageLevel" value="<%=HiddenManageLevel %>" />
    <!--选择消息接收着-->
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
</body>
<script type="text/javascript">
    $(function () {
        var projectAudit = new ProjectAuditClass($("#containerTable"));
    });
</script>
</html>
<script type="text/javascript">


    $(document).ready(function () {
        $("#Button1").click(function () {
            window.history.back();
        });
        $("#btnReport").click(function () {
            var data = "coperationSysNo=" + $.trim($("#ProjectSysNo").val()) + "&level=" + $.trim($("#HiddenManageLevel").val()) + "&auditNo=" + $.trim($("#HiddenProjectPlanAuditSysNo").val()) + "&option=1";
            window.location.href = "../../Coperation/ExportWord.aspx?" + data;
        });

    });
</script>
