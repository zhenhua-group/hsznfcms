﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="AddProjectByMaster.aspx.cs" Inherits="TG.Web.ProjectManage.AddProjectByMaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/ChooseUser.ascx" TagName="ChooseUser" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_proj.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script src="/js/ProjectMamage/AddprojectBymaster.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>

    <script type="text/javascript">
        //获得ID
        var projid = '<%=GetProjectID() %>';
        //用户ID
        var userid = '<%=GetCurMemID() %>';
        if (userid == "0") {
            window.parent.parent.document.location.href = "../index.html";
        }
        if (projid != "0") {
            //附件高清图
            var swfu;
            window.onload = function () {
                //附件高清图
                swfu = new SWFUpload({

                    upload_url: "../ProcessUpload/upload_proj.aspx?type=proj&id=" + projid + "&userid=" + userid,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.jpg;*.wmf;*.jpeg;*.bmp;*.png;*.gif;",
                    file_types_description: "文件资料上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccess,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_style: '{background-color:#d8d8d8 }',
                    button_width: 61,
                    button_height: 22,
                    button_text: '<span class="btnFile">选择文件</span>',
                    button_text_style: '.btnFile { background-color:#d8d8d8 ;} ',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,

                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
        }
    </script>
    <script type="text/javascript">
        var hid_projid = '<%=GetProjectID() %>';
        //判断是否选中节点
        //判断是否选中节点
        var nodecount;
        var nodeLenght;
        function IsStructCheckNode(obj) {
            nodecount = 0;
            nodeLenght="";
           
            if (obj == 'struct') {
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFun);
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFuns);
            }
            else if (obj == 'structtype') {
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFun);
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFuns);
            }
            
       
        if (nodecount > 0 ) {
            if (nodeLenght.length<500) {
                return true;
            }else{
                return false;
            }            
        }
        else {
            return false;
        }
    }
    //选中与半选中  qpl 20140115
    function displayNodeFun(elem) {
        if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
            nodecount++;
        }
    }
    function displayNodeFuns(elem) {
        if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
            nodeLenght+=  elem.getElementsByTagName("checkedState");
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目立项</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">项目管理</a><i
        class="fa fa-angle-right"> </i><a href="#">项目立项</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目录入
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">项目信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td colspan="4">
                                            <span style="color: red;">提示:黄色背景文本框为必填项！<i class="fa fa-warning"></i></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px;">项目名称:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" id="txt_name" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                                <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                            </div>
                                        </td>
                                        <td style="width: 120px;">合同关联:
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <input type="text" id="txt_reletive" runat="server" readonly="readonly" class="form-control input-sm"
                                                        style="background-color: #FFC;" />
                                                </div>
                                                <div class="col-md-3">
                                                    <a id="sch_reletive" href="#HTGL" class="btn  blue btn-sm"
                                                        data-toggle="modal"><i class="fa fa-search"></i></a>
                                                    <input id="txtcpr_id" type="hidden" runat="server" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>管理级别:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="radio-list">
                                                    <label class="radio-inline">
                                                        <span class="">
                                                            <input type="radio" name="aa" id="radio_yuan" runat="server" value="院管" />
                                                        </span>院管
                                                    </label>
                                                    <label class="radio-inline">
                                                        <span class="checked">
                                                            <input type="radio" name="aa" id="radio_suo" runat="server" value="所管" checked />
                                                        </span>所管
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>审核级别 :
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="checkbox-list">
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="audit_yuan" runat="server" class="shen" value="院审" />
                                                        </span>院审
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="audit_suo" runat="server" class="shen" value="所审" />
                                                        </span>所审
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设单位:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <input type="text" id="txtbuildUnit" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设地点:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" id="txtbuildAddress" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                            </div>
                                        </td>
                                        <td>建设规模:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="txt_scale" runat="server" class="form-control input-sm"
                                                        style="background-color: #FFC; width: 91%" />㎡
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="drp_buildtype" runat="server" Width="120px" AppendDataBoundItems="true" CssClass="form-control"
                                                    Style="background-color: #FFC;">
                                                    <asp:ListItem Value="-1">---选择类别---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <input type="text" id="txt_unit" runat="server" class="form-control input-sm"
                                                        style="background-color: #FFC;" readonly />
                                                    <asp:HiddenField ID="hid_unit" runat="server" Value="" />
                                                </div>
                                                <div class="col-md-3">
                                                    <a id="sch_unit" href="#CJBM" class="btn blue btn-sm"
                                                        data-toggle="modal"><i class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>结构样式:
                                        </td>
                                        <td>
                                            <div class="col-md-8" id="StrructContainer">
                                                <cc1:ASDropDownTreeView ID="asTreeviewStruct" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                    DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                    EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                    EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="----------请选择结构形式----------"
                                                    Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                    RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                    DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    Font-Size="12px" Font-Strikeout="False" EnableTheme="true" Height="23px" />
                                            </div>
                                        </td>
                                        <td>建筑分类:
                                        </td>
                                        <td>
                                            <div class="col-md-8" id="buildTypeContainer">
                                                <cc1:ASDropDownTreeView ID="asTreeviewStructType" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                    DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                    EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                    EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="----------请选择建筑分类----------"
                                                    Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                    RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                    DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    Font-Size="12px" Font-Strikeout="False" EnableTheme="true" Height="21px" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>设计阶段:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="checkbox-list">
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="CheckBox1" runat="server" class="duan" value="方案设计" />
                                                        </span>方案设计
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="CheckBox2" runat="server" class="duan" value="初步设计" />
                                                        </span>初步设计
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span>
                                                            <input type="checkbox" id="CheckBox3" runat="server" class="duan" value="施工图设计" />
                                                        </span>施工图设计
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span>
                                                            <input type="checkbox" id="CheckBox4" runat="server" class="duan" value="其他" />
                                                        </span>其他
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>项目来源:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddsource" runat="server" Width="120px" AppendDataBoundItems="True" CssClass="form-control"
                                                    Style="background-color: #FFC;">
                                                    <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" id="txtproAcount" runat="server" class="form-control"
                                                        style="background-color: #FFC; width: 80%;" />万元
                                                </div>
                                            </div>
                                        </td>
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <asp:DropDownList ID="ddProfessionType" Width="120px" runat="Server" AppendDataBoundItems="True"
                                                    CssClass="form-control" Style="background-color: #FFC;">
                                                    <asp:ListItem Value="0">---请选择---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" id="txt_Aperson" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                            </div>
                                            <div class="col-md-3">
                                                <a id="sch_Aperson" href="#JFFZ" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" id="txt_phone" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>执行设总:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" id="txt_PMName" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" readonly />
                                                <asp:HiddenField ID="hid_pmname" runat="server" Value="0" />
                                                <asp:HiddenField ID="hid_pmuserid" runat="server" Value="0" />
                                            </div>
                                            <div class="col-md-3">
                                                <a href="#ZXSZ" id="sch_PM" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" id="txt_PMPhone" runat="server" class="form-control input-sm" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目开始时间:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" name="txt_date" id="txt_startdate" onclick="WdatePicker({
    readOnly: true })"
                                                    class="Wdate" runat="Server" style="background-color: #FFC; width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                            </div>
                                        </td>
                                        <td>项目完成时间:
                                        </td>
                                        <td>
                                            <div class="col-md-9">
                                                <input type="text" name="txt_date" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="background-color: #FFC; width: 120px; height: 22px; border: 1px solid #e5e5e5" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <div class="checkbox-list">
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="chk_ISTrunEconomy" class="can" runat="server" value="经济所" />
                                                        </span>经济所
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="chk_ISHvac" runat="server" class="can" value="暖通热力所" />
                                                        </span>暖通热力所
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span>
                                                            <input type="checkbox" id="chk_ISArch" runat="server" class="can" value="土建所" />
                                                        </span>土建所
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目概况:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_sub" rows="3" runat="Server" placeholder="Enter
        a message ..."
                                                    maxlength="500" style="background-color: #FFC;"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目备注:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12">
                                                <textarea class="form-control input-sm" id="txt_remark" rows="3" runat="Server"> </textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目高清图:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12" id="img_container" style="text-align: center">
                                                <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                                    style="width: 250px; height: 150px;" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <h3 class="form-section">
                        <div class="row">
                            <div class="col-md-2">
                                项目高清图
                            </div>
                            <div class="col-md-8">
                                <div id="divFileProgressContainer" style="float: right;">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder">
                                    <i class="fa fa-paper-clip"></i>选择文件</span> </span>
                            </div>
                        </div>
                    </h3>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE
    PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table
    table-striped table-bordered table-advance table-hover"
                                        id="datas_att" style="width: 98%;" align="center">
                                        <thead>
                                            <tr id="att_row">
                                                <td style="width: 40px;" align="center" id="att_id">序号
                                                </td>
                                                <td style="width: 300px;" align="center" id="att_filename">文件名称
                                                </td>
                                                <td style="width: 80px" align="center" id="att_filesize">文件大小
                                                </td>
                                                <td style="width: 80px;" align="center" id="att_filetype">文件类型
                                                </td>
                                                <td style="width: 120px;" align="center" id="att_uptime">上传时间
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper">&nbsp;
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper2">&nbsp;
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <button type="submit" class="btn green" id="btn_Save">
                            保存</button>
                        <button type="submit" class="btn green" id="btn_SaveApply" style="display:none;">
                            保存并发起评审</button>
                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                            返回</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <input id="hidproId" type="hidden" runat="server" />
    <asp:HiddenField runat="server" ID="hidIsAppay" Value="0" />
    <!-- 弹出框 -->
    <!-- 4关联合同 -->
    <div id="HTGL" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">合同关联</h4>
        </div>
        <div class="modal-body">
            <div id="cprReletiveDiv">
                <div class="form-body">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <select id="select_releCprUnit" class=" form-control " style="width: 150px;">
                                    <option value="-1" selected="selected">-----全院部门-----</option>
                                </select></td>
                            <td>合同名称:</td>
                            <td>
                                <input type="text" id="TxtCprName" value="" class=" form-control input-sm" /></td>
                            <td>
                                <input type="button" class="btn blue" id="btn_SearchByCprName" value="查询" /></td>
                        </tr>
                    </table>

                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table id="cprReletiveTable" class="table table-bordered table-striped table-condensed
    flip-content"
                                align="center">
                                <tr class="trBackColor">
                                    <td align="left">合同名称
                                    </td>
                                    <td align="center">签订日期
                                    </td>
                                    <td align="center">操作
                                    </td>
                                </tr>
                            </table>
                            <!--合同级别绑定-->
                            <input type="hidden" id="cprLevel" value="" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div id="cprReletivePage" class="divNavigation pageDivPosition">
                                总<label id="reletiveCpr_totalCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                                第<label id="reletiveCpr_nowPageIndex">0</label>/<label id="reletiveCpr_PagesCount">0</label>页&nbsp;&nbsp;
                                <span id="reletiveCpr_firstPage">首页</span>&nbsp; <span id="reletiveCpr_prevPage">&lt;&lt;</span>&nbsp;
                                <span id="reletiveCpr_nextPage">&gt;&gt;</span>&nbsp; <span id="reletiveCpr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp; 跳至<input type="text" id="reletiveCpr_gotoPageNum"
                                    maxlength="4" style="height: 3; width: 20px; border-style: none none solid none;" />
                                页&nbsp;&nbsp; <span id="reletiveCpr_gotoPage">GO</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--承接部门-->
    <div id="CJBM" class="modal
    fade yellow"
        tabindex="-1" data-width="400" aria-hidden="true" style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">承接部门</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_cjbmDiv">
                <div class="row">
                    <div class="col-md-12">
                        <table id="cpr_cjbmTable" class="table
    table-bordered table-striped table-condensed flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td style="width: 60px;">序号
                                </td>
                                <td style="width: 340px;">单位名称
                                </td>
                                <td style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="cjbmByPageDiv" class="divNavigation
    pageDivPosition">
                            总<label id="cjbm_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="cjbm_nowPageIndex">0</label>/<label id="cjbm_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="cjbm_firstPage">首页</span>&nbsp; <span id="cjbm_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="cjbm_nextPage">&gt;&gt;</span>&nbsp; <span id="cjbm_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="cjbm_pageIndex" style="height: 3; width: 20px; border-style: hidden; border-bottom-style: solid;" />页&nbsp;&nbsp; <span id="cjbm_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--甲方负责人-->
    <div id="JFFZ" class="modal fade yellow" tabindex="-1" data-width="560" aria-hidden="true"
        style="display: none; width: 560px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人</h4>
        </div>
        <div class="modal-body">
            <div id="jffzr_DialogDiv">
                <table class="table-responsive">
                    <tr>
                        <td style="width: 80px;">姓名:</td>
                        <td>
                            <input type="text" id="txtName" value="" class="form-control input-sm" /></td>
                        <td style="width: 80px;">部门:</td>
                        <td>
                            <input type="text" id="txtCompName" value="" maxlength="25" class="form-control input-sm" /></td>
                    </tr>
                    <tr>
                        <td>电话:</td>
                        <td>
                            <input type="text" id="txtPhone" value="" class="form-control
    input-sm" /></td>
                        <td>
                            <input type="button" id="btn_serch" class="btn blue  btn-sm" value="查询" /></td>
                        <td></td>
                    </tr>
                </table>

                <div class="row">
                    <div class="col-md-12">
                        <table id="jffzr_table" class="table table-bordered table-striped table-condensed
    flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td>联系人
                                </td>
                                <td>电话
                                </td>
                                <td>部门
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="jffzr_forPageDiv" class="divNavigation
    pageDivPosition">
                            总<label id="jffzr_totalCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="jffzr_nowPageIndex">0</label>/<label id="jffzr_PagesCount">0</label>页&nbsp;&nbsp;
                            <span id="jffzr_firstPage">首页</span>&nbsp; <span id="jffzr_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="jffzr_nextPage">&gt;&gt;</span>&nbsp; <span id="jffzr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="jffzr_gotoPageNum" style="height: 3; width: 20px; border-style: none none solid none;" />
                            页&nbsp;&nbsp; <span id="jffzr_gotoPage">GO</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">
                        关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!--执行设总-->
    <div id="ZXSZ" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择设总</h4>
        </div>
        <div class="modal-body">
            <div id="chooseUserMain">
                <uc1:ChooseUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_user_close" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
