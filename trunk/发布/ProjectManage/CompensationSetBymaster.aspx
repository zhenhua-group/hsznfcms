﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="CompensationSetBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.CompensationSetBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/CompensationSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.tablesort.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/ProjectMamage/CompensationSet.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/ProjectMamage/CompensationSet_jq.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>方案补贴产值</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">产值分配</a><i
        class="fa fa-angle-right"> </i><a href="#">方案补贴产值</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询方案补贴产值
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>分配年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control " Width="90px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                    <asp:ListItem>2010</asp:ListItem>
                                    <asp:ListItem>2011</asp:ListItem>
                                    <asp:ListItem>2012</asp:ListItem>
                                    <asp:ListItem>2013</asp:ListItem>
                                    <asp:ListItem>2014</asp:ListItem>
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>姓名:</td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_cprName" runat="server" /></td>
                            <td>
                                <input type="button" class="btn blue btn-default" value="查询" id="btn_Search" /></td>
                            <td>
                                <input type="button" class="btn blue btn-default" value="添加" id="btn_showadd" /></td>

                        </tr>
                    </table>
                    <div class="table-responsive" id="div_add" style="display: none;">
                        <table class="table">
                            <tr>
                                <td colspan="10" align="center">
                                    <h4>添加方案补贴信息</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>生产部门:</td>
                                <td>
                                    <asp:DropDownList ID="drp_unit0" runat="server" Width="120px" AppendDataBoundItems="True" CssClass="form-control">
                                        <asp:ListItem Value="-1">------全院部门------</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>可分配补贴总额:</td>
                                <td><span style="color: Red;" id="totalhtml">0</span> 元</td>
                                <td>方案补贴年份:</td>
                                <td>
                                    <asp:DropDownList ID="drp_allotyear0" runat="server" Width="80px" CssClass="form-control">
                                        <asp:ListItem Value="-1">---年份---</asp:ListItem>
                                        <asp:ListItem Value="2010">2010</asp:ListItem>
                                        <asp:ListItem Value="2011">2011</asp:ListItem>
                                        <asp:ListItem Value="2012">2012</asp:ListItem>
                                        <asp:ListItem Value="2013">2013</asp:ListItem>
                                        <asp:ListItem Value="2014">2014</asp:ListItem>
                                        <asp:ListItem Value="2015">2015</asp:ListItem>
                                        <asp:ListItem Value="2016">2016</asp:ListItem>
                                        <asp:ListItem Value="2017">2017</asp:ListItem>
                                        <asp:ListItem Value="2018">2018</asp:ListItem>
                                        <asp:ListItem Value="2019">2019</asp:ListItem>
                                        <asp:ListItem Value="2020">2020</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>人员部门:</td>
                                <td>
                                    <asp:DropDownList ID="drp_unit0_1" runat="server" Width="120px" AppendDataBoundItems="True" CssClass="form-control">
                                        <asp:ListItem Value="-1">------全院部门------</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>人员:</td>
                                <td>
                                    <select id="drp_mem0" class="form-control" style="width: 120px;" onchange="FillHidden(this);">
                                        <option value="-1">---人员---</option>
                                    </select>
                                    <asp:HiddenField ID="drp_memid" runat="server" />
                                </td>
                                <td>人员方案补贴值:</td>
                                <td>
                                    <asp:TextBox ID="txt_allot0" runat="server" CssClass="form-control input-sm" Width="120px"></asp:TextBox>元(小数点后两位有效)</td>


                            </tr>
                            <tr>
                                <td colspan="10" align="center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn green  btn-default" OnClick="btn_save_Click" Text="确定" />
                                    <input type="button" class="btn btn-default" value="关闭" id="btn_close1" /></td>
                            </tr>
                        </table>
                    </div>
                    <div class="table-responsive" id="div_edit" style="display: none;">
                        <table class="table">
                            <tr>
                                <td colspan="10" align="center">
                                    <h4>编辑方案补贴</h4>
                                </td>
                            </tr>
                            <tr>
                                <td>生产部门:</td>
                                <td>
                                    <asp:DropDownList ID="drp_unit1" runat="server" Width="120px" AppendDataBoundItems="True" CssClass="form-control">
                                        <asp:ListItem Value="-1">------全院部门------</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>可分配补贴总额:</td>
                                <td><span style="color: Red;" id="totalhtml1">0</span> 元</td>
                                <td>方案补贴年份:</td>
                                <td>
                                    <asp:DropDownList ID="drp_allotyear1" runat="server" Width="80px" CssClass="form-control">
                                        <asp:ListItem Value="-1">---年份---</asp:ListItem>
                                        <asp:ListItem Value="2010">2010</asp:ListItem>
                                        <asp:ListItem Value="2011">2011</asp:ListItem>
                                        <asp:ListItem Value="2012">2012</asp:ListItem>
                                        <asp:ListItem Value="2013">2013</asp:ListItem>
                                        <asp:ListItem Value="2014">2014</asp:ListItem>
                                        <asp:ListItem Value="2015">2015</asp:ListItem>
                                        <asp:ListItem Value="2016">2016</asp:ListItem>
                                        <asp:ListItem Value="2017">2017</asp:ListItem>
                                        <asp:ListItem Value="2018">2018</asp:ListItem>
                                        <asp:ListItem Value="2019">2019</asp:ListItem>
                                        <asp:ListItem Value="2019">2020</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>人员:</td>
                                <td>
                                    <asp:DropDownList ID="drp_mem1" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="-1">---姓名---</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>人员方案补贴值:</td>
                                <td>
                                    <asp:TextBox ID="txt_allot1" runat="server" CssClass="form-control input-sm"></asp:TextBox>元(小数点后两位有效)</td>
                            </tr>
                            <tr>
                                <td colspan="10" align="center">
                                    <asp:Button ID="btn_edit" runat="server" class="btn green " OnClick="btn_edit_Click" Text="确定" />
                                    <input type="button" class="btn default" value="关闭" id="btn_close2" />
                                    <asp:HiddenField ID="hid_id" runat="server" />
                                    <asp:HiddenField ID="hid_memid" runat="server" />
                                    <asp:HiddenField ID="hid_unitid" runat="server" />
                                    <asp:HiddenField ID="hid_allotyear" runat="server" />
                                    <asp:HiddenField ID="hid_PreviewPattern" runat="server" />
                                    <asp:HiddenField ID="hid_UserSysNo" runat="server" />
                                    <asp:HiddenField ID="hid_UserUnitNo" runat="server" />
                                    <!-- 编辑时分配总产值 -->
                                    <asp:HiddenField ID="hid_totalcount" runat="server" Value="0" />
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>方案补贴产值列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_Output" runat="server" CssClass="btn red btn-sm"
                            OnClick="out_Click" Text="导出Excel" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">

                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
