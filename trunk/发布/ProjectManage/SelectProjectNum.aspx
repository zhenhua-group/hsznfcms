﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectProjectNum.aspx.cs"
    Inherits="TG.Web.ProjectManage.SelectProjectNum" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#drp_protypenum").change(function() {
                if ($(this).val() == "-1") {
                    $("#txt_pronum").show();
                }
                else {
                    $("#txt_pronum").hide();
                }
            });
            //确定
            $("#btn_close").click(function() {
                var flag = $("#drp_protypenum").val();
                var rltValue = "";
                if (flag == "-1") {
                    rltValue = $("#txt_pronum").val();
                    if (rltValue == "例:XXXXX-001") {
                        alert("请输入工号！");
                        return false;
                    }
                    if (rltValue.indexOf('-') == -1) {
                        alert("输入工号格式错误！");
                        return false;
                    }
                }
                else {
                    rltValue = $("#drp_protypenum option:selected").text();
                }
                window.returnValue = rltValue;
                window.close();
            });
            //文本框focus
            $("#txt_pronum").focus(function() {
                if ($(this).val() == "例:XXXXX-001") {
                    $(this).val("");
                }
            })
            $("#txt_pronum").blur(function() {
                if ($(this).val() == "") {
                    $(this).val("例:XXXXX-001");
                }
            });
        });
    </script>

    <style type="text/css">
        html, body
        {
            width: 300px;
            font-size: 10pt;
        }
        .tb_pronumber
        {
                border: solid 1px #CCC;
                border-collapse:collapse;
        }
        .tb_pronumber td
        {
                border: solid 1px #CCC;
                border-collapse:collapse;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 300px;" class="tb_pronumber">
            <tr>
                <td>
                    项目类型：<asp:DropDownList ID="drp_protype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drp_protype_SelectedIndexChanged"
                        Width="150px" AppendDataBoundItems="True" CssClass="TextBoxBorder">
                        <asp:ListItem Value="-1">-----请选择-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    项目工号：<asp:DropDownList ID="drp_protypenum" runat="server" Width="150px" CssClass="TextBoxBorder">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    自定义&nbsp;&nbsp; ：<asp:TextBox ID="txt_pronum" runat="server" ForeColor="#999999"
                        CssClass="TextBoxBorder" Width="150px">例:XXXXX-001</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input id="btn_close" type="button" style="background-image: url(../images/buttons/btn_ok.gif);
                        border: none; height: 20px; width: 48px;" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
