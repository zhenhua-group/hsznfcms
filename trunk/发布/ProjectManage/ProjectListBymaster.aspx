﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectListBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectListBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectMamage/ProjectList_jq.js"></script>
    <script type="text/javascript" src="../js/ProjectMamage/ProjectList_jqH.js"></script>
    
    <script>
      
        function IsStructCheckNode(obj) {
            nodecount = 0;
            nodeLenght="";    
            <%= asTreeviewdrpunitObjID %>.traverseTreeNode(displayNodeFun);
            <%= asTreeviewdrpunitObjID %>.traverseTreeNode(displayNodeFuns);            
            return nodeLenght;
        }
        //选中与半选中  qpl 20140115
        function displayNodeFun(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodecount++;
            }
        }
        function displayNodeFuns(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodeLenght+=  "'"+elem.getAttribute("treenodevalue")+"',";
            }
        }
    </script>


    <script type="text/javascript">
        //判断是否选中节点       
        var nodecount;
        function IsStructCheckNodeser(obj) {
            nodecount = 0;
            if (obj == 'struct') {
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFun);
            }
            else if (obj == 'structtype') {
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFun);
            }

        if (nodecount > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    function ChooseNodesValue()
    {
        var result="<%= GetDropDownTreeCheckedValue()%>"; 
        return result; 
    }
    function displayNodeFun(elem) {
        if (elem.getAttribute("checkedState") == "0") {
            nodecount++;
        }
    }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">项目管理</a><i class="fa fa-angle-right"> </i><a href="#">项目列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询项目信息
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">选择查询项
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="Colsdrpboxs">
                                <label>
                                    <input type="checkbox" value="all" />全选
                                </label>
                                <label>
                                    <input id="Level" name="bbs" class="cls_audit" type="checkbox" /><span rel="Level">管理级别</span>
                                </label>
                                <label>
                                    <input id="cprBuildUnit" name="bbs" class="cls_audit" type="checkbox" /><span rel="cprBuildUnit">建设单位</span>
                                </label>
                                <label>
                                    <input id="StructType" name="bbs" class="cls_audit" type="checkbox" /><span rel="StructType">结构形式</span>
                                </label>
                                <label>
                                    <input id="BuildStructType" name="bbs" class="cls_audit" type="checkbox" /><span rel="BuildStructType">建筑分类</span>
                                </label>
                                <label>
                                    <input id="proFuze" name="bbs" class="cls_audit" type="checkbox" /><span rel="proFuze">执行设总</span>
                                </label>
                                <label>
                                    <input id="FParty" name="bbs" class="cls_audit" type="checkbox" /><span rel="FParty">甲方负责人</span>
                                </label>
                                <label>
                                    <input id="cpr_Account" name="bbs" class="cls_audit" type="checkbox" /><span rel="cpr_Account">合同额</span>
                                </label>
                                <label>
                                    <input id="ddProjectPosition" name="bbs" class="cls_audit" type="checkbox" /><span rel="ddProjectPosition">建设地点</span>
                                </label>
                                <label>
                                    <input id="ddProfessionTypeCheck" name="bbs" class="cls_audit" type="checkbox" /><span rel="ddProfessionTypeCheck">行业性质</span>
                                </label>
                                <label>
                                    <input id="ddSourceWay" name="bbs" class="cls_audit" type="checkbox" /><span rel="ddSourceWay">工程来源</span>
                                </label>
                                <label>
                                    <input id="ddcpr_Stage" name="bbs" class="cls_audit" type="checkbox" /><span rel="ddcpr_Stage">设计阶段</span>
                                </label>
                                <label>
                                    <input id="cpr_join" name="bbs" class="cls_audit" type="checkbox" /><span rel="cpr_join">参与部门</span>
                                </label>
                                <label>
                                    <input id="SingnDate" name="bbs" class="cls_audit" type="checkbox" /><span rel="SingnDate">项目开始日期</span>
                                </label>
                                <label>
                                    <input id="DoneDate" name="bbs" class="cls_audit" type="checkbox" /><span rel="DoneDate">项目完成日期</span>
                                </label>
                            </div>
                        </div>
                        <div class="btn-group" data-toggle="buttons">
                            <span class="btn blue">查询方式：</span>
                            <label rel="and" class="btn yellow btn-sm active" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="Radio1" value="and">必须
                            </label>
                            <label rel="or" class="btn default btn-sm" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="Radio2" value="or">或
                            </label>
                        </div>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                            <td style="width: 65px;">生产部门:</td>
                            <td style="width: 185px;">
                                <cc1:ASDropDownTreeView ID="drp_unit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全部部门-------" />
                            </td>
                            <td style="width: 65px;">年份:</td>
                            <td style="width: 185px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td style="width: 65px;">项目名称:</td>
                            <td style="width: 185px;">
                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="Server" style="width: 220px; border: 1px solid #9BA0A6;" />
                            </td>
                        </tr>
                        <tr>
                            <td>建设类别:</td>
                            <td>

                                <asp:DropDownList ID="drp_buildtype" runat="server" Width="120px" AppendDataBoundItems="true" CssClass="form-control">
                                    <asp:ListItem Value="-1">-----选择类别-----</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td>建设规模:</td>
                            <td>

                                <div class="input-group">
                                    <input type="text" id="txt_buildArea" class="form-control input-sm " runat="server" style="width: 90px; border: 1px solid #9BA0A6;" /> - <input type="text" id="txt_buildArea2" class="form-control input-sm" runat="server" style="width: 90px; border: 1px solid #9BA0A6;" />平米
                                      
                                </div>
                            </td>
                            <td>录入时间:</td>
                            <td>

                                <div class="input-group">
                                    <input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" style="width: 103px; height: 22px; border: 1px solid #9BA0A6;" /> - <input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 103px; height: 22px; border: 1px solid #9BA0A6;" />

                                </div>
                            </td>
                        </tr>

                    </table>
                    <table class="table-responsive" id="tab_Sch" style="width: 100%">
                        <tr>
                            <td colspan="6" align="center">
                                <input type="button" class="btn blue" value="查询" id="btn_search" /></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div id="hideChoose" style="display: none;">

                <div for="Level">
                    <div for="Level_1" style="float: left;">
                        管理级别:
                    </div>
                    <div for="Level_2">

                        <asp:DropDownList ID="managerLevelDropDownList" runat="server" CssClass="form-control"
                            Width="150px">
                            <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                            <asp:ListItem Value="0">院管</asp:ListItem>
                            <asp:ListItem Value="1">所管</asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>

                <div for="cprBuildUnit">
                    <div for="cprBuildUnit_1" style="float: left;">
                        建设单位:
                    </div>
                    <div for="cprBuildUnit_2">

                        <input type="text" id="txt_buildUnit" class="form-control input-sm" style="width: 220px; border: 1px solid #9BA0A6;" />

                    </div>
                </div>
                <div for="StructType">
                    <div for="StructType_1" style="float: left;">
                        结构形式：
                    </div>
                    <div for="StructType_2">

                        <cc1:ASDropDownTreeView ID="asTreeviewStruct" runat="server" BasePath="~/js/astreeview/astreeview/"
                            DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                            EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                            EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择结构形式--------"
                            Width="180px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                            RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                            DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                            DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                            Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />

                    </div>
                </div>
                <div for="BuildStructType">
                    <div for="BuildStructType_1" style="float: left;">
                        建筑分类：
                    </div>
                    <div for="BuildStructType_2">

                        <cc1:ASDropDownTreeView ID="asTreeviewStructType" runat="server" BasePath="~/js/astreeview/astreeview/"
                            DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                            EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                            EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择建筑分类--------"
                            Width="180px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                            RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                            DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                            DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                            Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />

                    </div>
                </div>
                <div for="proFuze">
                    <div for="proFuze_1" style="float: left;">
                        执行设总:
                    </div>
                    <div for="proFuze_2">

                        <input type="text" id="txt_PMName" class="form-control input-sm " style="width: 220px; border: 1px solid #9BA0A6;" />
                    </div>


                </div>
                <div for="FParty">
                    <div for="FParty_1" style="float: left;">
                        甲方负责人:
                    </div>
                    <div for="FParty_2">

                        <input type="text" id="txt_Aperson" class="form-control input-sm " style="width: 220px; border: 1px solid #9BA0A6;" />

                    </div>
                </div>

                <div for="cpr_Account">
                    <div for="cpr_Account_1" style="float: left;">
                        合同额:
                    </div>
                    <div for="cpr_Account_2">

                        <div class="input-group">
                            <input type="text" id="txtproAcount" class="form-control input-sm" style="width: 103px; border: 1px solid #9BA0A6;" /> - <input type="text" id="txtproAcount2" class="form-control input-sm" style="width: 103px; border: 1px solid #9BA0A6;" />
                            &nbsp;万元
                        </div>

                    </div>
                </div>
                <div for="ddProjectPosition">
                    <div for="ddProjectPosition_1" style="float: left;">
                        建设地点:
                    </div>
                    <div for="ddProjectPosition_2">

                        <input type="text" id="txtaddress" class="form-control input-sm " style="width: 220px; border: 1px solid #9BA0A6;" />
                    </div>

                </div>
                <div for="ddProfessionTypeCheck">
                    <div for="ddProfessionTypeCheck_1" style="float: left;">
                        行业性质:
                    </div>
                    <div for="ddProfessionTypeCheck_2">

                        <asp:DropDownList ID="ddType" runat="server" CssClass="form-control input-sm" Width="150"
                            AppendDataBoundItems="true">
                            <asp:ListItem Value="0">--------请选择--------</asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>
                <div for="ddSourceWay">
                    <div for="ddSourceWay_1" style="float: left;">
                        工程来源:
                    </div>
                    <div for="ddSourceWay_2">

                        <asp:DropDownList ID="ddsource" runat="server" AppendDataBoundItems="True" Width="150"
                            CssClass="form-control input-sm">
                            <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>
                <div for="ddcpr_Stage">
                    <div for="ddcpr_Stage_1" style="float: left;">
                        设计阶段:
                    </div>
                    <div for="ddcpr_Stage_2">

                        <asp:CheckBox ID="CheckBox1" runat="server" Text="方案设计" CssClass="cls_jd" rel="sjjd" />
                        <asp:CheckBox ID="CheckBox2" runat="server" Text="初步设计" CssClass="cls_jd" rel="sjjd" />
                        <asp:CheckBox ID="CheckBox3" runat="server" Text="施工图设计" CssClass="cls_jd" rel="sjjd" />
                        <asp:CheckBox ID="CheckBox4" runat="server" Text="其他" CssClass="cls_jd" rel="sjjd" />

                    </div>
                </div>
                <div for="cpr_join">
                    <div for="cpr_join_1" style="float: left;">
                        参与部门:
                    </div>
                    <div for="cpr_join_2">

                        <div class="checkbox-list">
                            <label class="checkbox-inline">
                                <span class="">
                                    <input type="checkbox" id="chk_ISTrunEconomy" class="can" runat="server" value="经济所" />
                                </span>经济所
                            </label>
                            <label class="checkbox-inline">
                                <span class="">
                                    <input type="checkbox" id="chk_ISHvac" runat="server" class="can" value="暖通热力所" />
                                </span>暖通热力所
                            </label>
                            <label class="checkbox-inline">
                                <span>
                                    <input type="checkbox" id="chk_ISArch" runat="server" class="can" value="土建所" />
                                </span>土建所
                            </label>
                        </div>

                    </div>
                </div>
                <div for="SingnDate">
                    <div for="SingnDate_1" style="float: left;">
                        项目开始日期:
                    </div>
                    <div for="SingnDate_2">

                        <div class="input-group">
                            <input type="text" id="txt_signdate" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                runat="Server" style="width: 103px; height: 22px; border: 1px solid #e5e5e5" /> - <input type="text" id="txt_signdate2" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                                            runat="Server" style="width: 103px; height: 22px; border: 1px solid
                #e5e5e5" />
                        </div>

                    </div>
                </div>
                <div for="DoneDate">
                    <div for="DoneDate_1" style="float: left;">
                        项目完成日期:
                    </div>
                    <div for="DoneDate_2">

                        <div class="input-group">
                            <input type="text" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 103px; height: 22px; border: 1px solid #e5e5e5" /> - <input type="text" id="txt_finishdate2" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 103px; height: 22px; border: 1px solid
                #e5e5e5" />
                        </div>

                    </div>
                </div>

            </div>
            <%-- <tr>
                    <td colspan="2" align="center">
                        <input type="button" class="btn
                blue"
                            id="btn_Hseach" value="查询" />&nbsp;&nbsp;
                    </td>
                </tr>--%>


            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <label>
                                    <input type="checkbox" value="all" />全选
                                </label>
                                <%=ColumnsContent %>
                            </div>
                        </div>
                        <asp:Button ID="btn_Output" runat="server" Text="导出Excel" CssClass="btn  red btn-sm" OnClick="btn_Output_Click" />

                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_IsSeach" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenColsVal" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenColsName" runat="server" Value="0" />
</asp:Content>
