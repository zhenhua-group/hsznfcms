﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectWages.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectAllot.ProjectWages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>2013年度个人账户明细</title>
          <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr>
    <td colspan="9" align="center"><strong>帐户：XXX</strong></td>
    <td width="23%" align="center"><strong style="font-size:14px;">岗位身份号:SC-001-ZJM</strong></td>
    <td width="44%" align="center"><strong>2013年度个人账户明细（一）</strong></td><td width="17%" align="center"><strong>记录清单序号NO:20</strong></td></tr></table>
<table width="98%" cellpadding="1" cellspacing="1" style="background:#000;font-size:12px; font-family:'微软雅黑';" align="center">
      <tr>
        <td height="30" colspan="3" align="center" bgcolor="#FFFFFF">日    期</td>
        <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">计帐凭证</td>
        <td rowspan="2" align="center" bgcolor="#FFFFFF">内      容</td>
        <td height="30" colspan="8" align="center" bgcolor="#FFFFFF">入帐明细</td>
        <td colspan="4" align="center" bgcolor="#FFFFFF">支出明细</td>
        <td rowspan="2" align="center" bgcolor="#FFFFFF"><strong>余 额</strong></td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">年</td>
        <td height="30" align="center" bgcolor="#FFFFFF">月</td>
        <td height="30" align="center" bgcolor="#FFFFFF">日</td>
        <td height="30" align="center" bgcolor="#FFFFFF">字</td>
        <td height="30" align="center" bgcolor="#FFFFFF">号</td>
        <td height="30" align="center" bgcolor="#FFFFFF">设计/院长</td>
        <td height="30" align="center" bgcolor="#FFFFFF">方案/总工</td>
        <td height="30" align="center" bgcolor="#FFFFFF">校核</td>
        <td height="30" align="center" bgcolor="#FFFFFF">审核</td>
        <td height="30" align="center" bgcolor="#FFFFFF">审定</td>
        <td align="center" bgcolor="#FFFFFF">专业负责</td>
        <td align="center" bgcolor="#FFFFFF">项目负责</td>
        <td align="center" bgcolor="#FFFFFF">项目经理</td>
        <td align="center" bgcolor="#FFFFFF">费用</td>
        <td align="center" bgcolor="#FFFFFF">购票</td>
        <td align="center" bgcolor="#FFFFFF">借或领</td>
        <td align="center" bgcolor="#FFFFFF">工资</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">接上页余额</td>
        <td height="30" align="center" bgcolor="#FFFFFF">-</td>
        <td height="30" align="center" bgcolor="#FFFFFF">-</td>
        <td height="30" align="center" bgcolor="#FFFFFF">-</td>
        <td height="30" align="center" bgcolor="#FFFFFF">-</td>
        <td height="30" align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
        <td align="center" bgcolor="#FFFFFF">-</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">2013</td>
        <td height="30" align="center" bgcolor="#FFFFFF">1</td>
        <td height="30" align="center" bgcolor="#FFFFFF">1</td>
        <td height="30" align="center" bgcolor="#FFFFFF">波</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">医康托老服务中心收支</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">本页合计</td>
        <td height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
      </tr>
      <tr>
        <td width="5%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="4%" height="30" align="center" bgcolor="#FFFFFF">
            
        </td>
        <td width="5%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="4%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;
           </td>
        <td width="4%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="8%" height="30" align="center" bgcolor="#FFFFFF">转 下 页</td>
        <td width="3%" height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td width="4%" height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td width="5%" height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td width="5%" height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td width="5%" height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td width="5%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="7%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="8%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="4%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="4%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="6%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="8%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="6%" align="center" bgcolor="#FFFFFF">0</td>
        </tr>
        </table>
        <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr>
      <td width="12%" align="center" height="30">总合计师：</td>
      <td width="18%" align="center">财务部长：</td>
      <td width="16%" align="center">会计：</td>
          <td width="19%" align="center">复核：</td>
          <td width="16%" align="center">记账：</td>
          </tr>
  </table>
    </form>
</body>
</html>
