﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectOutSum.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectAllot.ProjectOutSum" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>设计项目支出划拨表-B-20%（当年结算）</title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="form1" runat="server">
  <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr><td colspan="10" align="center"><strong style="font-size:14px;">设计项目支出划拨表-B-20%（当年结算）</strong></td><td width="20%" align="center">记录清单编号NO：19-B</td><td width="17%" align="center">2013版</td></tr></table>
<table width="98%" cellpadding="1" cellspacing="1" style="background:#000;font-size:12px; font-family:'微软雅黑';" align="center">
            <tr>
                <td width="10%" height="30" align="center" bgcolor="#FFFFFF">项目经理人</td>
                <td width="8%" height="30" align="center" bgcolor="#FFFFFF">
               
                    <asp:Label ID="txt_username" runat="server"   ></asp:Label>
               
                </td>
                <td height="30" colspan="7" align="center" bgcolor="#FFFFFF">建设单位：
                <asp:Label ID="txt_company" runat="server"  ></asp:Label></td>
                <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">合同日期：<asp:Label ID="txt_cprdate" runat="server"></asp:Label></td>
            </tr>
                    <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">
                <strong>控制比例</strong></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_kzbl" runat="server"    Text="20%" style=" font-weight:bold;"></asp:Label></td>
            <td height="30" colspan="7" align="center" bgcolor="#FFFFFF">
             <strong> 项目信息表(本表由经营部填写，并用于财务收入进账用）</strong>
                </td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">收费日期：<asp:Label ID="txt_moenydate" runat="server"    Text="" ></asp:Label></td>
        </tr>
                    <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">
                工程名称</td>
            <td height="30" colspan="6" align="center" bgcolor="#FFFFFF">
                <asp:Label ID="txt_projname" runat="server"   ></asp:Label></td>
            <td width="9%" height="30" align="center" bgcolor="#FFFFFF">定金划拨</td>
            <td width="9%" height="30" align="center" bgcolor="#FFFFFF">合同编号</td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_cprno" runat="server"    Text="0"></asp:Label></td>
        </tr>
      <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">楼号</td>
                <td height="30" align="center" bgcolor="#FFFFFF">
                     办公楼</td>
                <td width="10%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="6%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;
                    </td>
                <td width="9%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="8%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="12%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">合计</td>
                <td height="30" align="center" bgcolor="#FFFFFF">设计编号</td>
                <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_projno" runat="server"    Text="0"></asp:Label></td>
            </tr>
               <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">建筑面积</td>
                <td height="30" align="center" bgcolor="#FFFFFF">
                    <asp:Label ID="txt_mj" runat="server" Text="10000"></asp:Label>
                </td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;
                    </td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_hj" runat="server"    Text="0"></asp:Label></td>
                <td height="30" align="center" bgcolor="#FFFFFF">税  率</td>
                <td width="10%" height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_rl" runat="server"    Text="90%"></asp:Label></td>
                <td width="9%" height="30" align="center" bgcolor="#FFFFFF">(万元)</td>
            </tr>
               
               <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">建筑层数
                </td>
                <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="txt_floor" runat="server" Text="0" ></asp:Label>                    
                </td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">合同单价<br />
                 (元/㎡)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">
                   
                     <asp:Label ID="txt_dj" runat="server"  style="width:50px;" Text="10.00"></asp:Label>
                  </td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">本次收费<br />
                 (万元)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"> <asp:Label ID="txt_money" runat="server"  style="width:50px;" Text="2.00"></asp:Label></td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">最低标准    <br />
                 分成基数</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"> <asp:Label ID="txt_js" runat="server"  style="width:50px;" Text="10.00"></asp:Label>
                  <br />
                 (元/㎡)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_jsmoney" runat="server"    Text="2.00"></asp:Label>
                  <br />
                 (万元)</td>
                <td height="30" align="center" bgcolor="#FFFFFF">院留管理费</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_glfmoney" runat="server"    Text="0.4500"></asp:Label></td>
            </tr>
            <tr>
                <td height="30" align="center" bgcolor="#FFFFFF"  >建筑高度</td>
                <td height="30" align="center" bgcolor="#FFFFFF"  >
                    <asp:Label ID="txt_height" runat="server"  Text="0"></asp:Label></td>
                <td height="30" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_glf" runat="server"    Text="25%"></asp:Label></td>
            </tr>
          
         
            <tr>
                <td height="30" align="center" bgcolor="#FFFFFF"  >建筑规模</td>
                <td height="30" align="center" bgcolor="#FFFFFF"  >
                    <asp:Label ID="txt_gm" runat="server"  Text="0"></asp:Label></td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >合同收费<br />
                (万元)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >
                   <asp:Label ID="txt_htsf" runat="server"    Text="10.0000"></asp:Label>
                </td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >本次收费<br />
比例</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_bcsf" runat="server"    Text="20.00%"></asp:Label></td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >最低标准税后<br />
                分成基数</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_shjs" runat="server"    Text="9.00"></asp:Label>
                  <br />
              (元/㎡)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_shjsmoney" runat="server"    Text="1.8000"></asp:Label>
                  <br />
              (万元)</td>
                <td height="30" align="center" bgcolor="#FFFFFF"  >项目留费用</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_xmmoney" runat="server"    Text="1.3500"></asp:Label></td>
            </tr>
            <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">结构形式</td>
                <td height="30" align="center" bgcolor="#FFFFFF">
                    <asp:Label ID="txt_jgxs" runat="server" Text="0" ></asp:Label></td>
                <td height="30" align="center" bgcolor="#FFFFFF" ><asp:Label ID="txt_xm" runat="server"    Text="75%"></asp:Label></td>
            </tr>
            <tr>
              <td rowspan="2" align="center" bgcolor="#FFFFFF">备注</td>
              <td height="30" colspan="8" align="left" bgcolor="#FFFFFF">1.本表部分由经营部根据项目特性填写，部分为计算公式结果(绝对禁止输入内容)</td>
              <td height="30" align="center" bgcolor="#FFFFFF">中标项目</td>
              <td height="30" align="center" bgcolor="#FFFFFF">一般项目</td>
  </tr>
            <tr>
              <td height="30" colspan="8" align="left" bgcolor="#FFFFFF">2.本表一式三份，院财务、项目经理、经营部各一份</td>
              <td height="30" align="center" bgcolor="#FFFFFF">咨询项目</td>
              <td height="30" align="center" bgcolor="#FFFFFF">冰山项目</td>
            </tr>
            <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">本次发放<br />
比例</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_kzbl2" runat="server"    Text="20%" style=" font-weight:bold;"></asp:Label></td>
            <td height="30" colspan="7" align="center" bgcolor="#FFFFFF"><strong>项目支出明细表(本表由生产部填写，用于财务代支出费用）</strong></td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">出图日期：<asp:Label ID="txt_ctdate" runat="server"    Text="" ></asp:Label></td>
            </tr>
               <tr>
            <td rowspan="2" align="center" bgcolor="#FFFFFF">最低收费标准税后分成基数</td>
            <td height="30" align="center" bgcolor="#FFFFFF">项目费用</td>
            <td height="30" align="center" bgcolor="#FFFFFF">项目负责</td>
            <td height="30" align="center" bgcolor="#FFFFFF">总  工</td>
            <td height="30" align="center" bgcolor="#FFFFFF">院  长</td>
            <td height="30" align="center" bgcolor="#FFFFFF">设计人员 A</td>
            <td height="30" align="center" bgcolor="#FFFFFF">校核人员  <br />
              B</td>
            <td height="30" align="center" bgcolor="#FFFFFF">审核人员<br />
              D </td>
            <td height="30" align="center" bgcolor="#FFFFFF">专业负责<br />
C</td>
            <td height="30" align="center" bgcolor="#FFFFFF">审定人员<br />
E</td>
            <td height="30" align="center" bgcolor="#FFFFFF">支出合计</td>
    </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable10" runat="server"    Text="36%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable11" runat="server"    Text="2.0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable12" runat="server"    Text="0.7%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable13" runat="server"    Text="0.9%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable14" runat="server"    Text="23.5%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable15" runat="server"    Text="1.5%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable16" runat="server"    Text="1.6%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable17" runat="server"    Text="7.0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable18" runat="server"    Text="1.8%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable19" runat="server"    Text="39.0%" ></asp:Label></td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable2" runat="server"    Text="54000" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable21" runat="server"    Text="19440" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable22" runat="server"    Text="1080" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable23" runat="server"    Text="378" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable24" runat="server"    Text="486" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable25" runat="server"    Text="12690" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable26" runat="server"    Text="810" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable27" runat="server"    Text="864" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable28" runat="server"    Text="3780" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable29" runat="server"    Text="978" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable210" runat="server"    Text="-21060" ></asp:Label></td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">（元）</td>
            <td height="30" align="center" bgcolor="#FFFFFF">项目经理</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">王  晔</td>
            <td height="30" align="center" bgcolor="#FFFFFF">李俊奎</td>
            <td height="30" colspan="5" align="center" bgcolor="#FFFFFF">详以下生产部用表</td>
            <td height="30" align="center" bgcolor="#FFFFFF">项目经理</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">签  字</td>
            <td height="30" align="center" bgcolor="#FFFFFF">收入余额</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" colspan="5" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">本次发放<br />
比例</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_bcff2" runat="server"    Text="20%" style=" font-weight:bold;"></asp:Label></td>
            <td height="30" colspan="7" align="center" bgcolor="#FFFFFF"><strong>设计、校核、审核、审定人员及专业注册人设计报酬分配表                  <br />
            (本表由生产部填写，并用于财务代支出用）</strong></td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">出图日期：<asp:Label ID="txt_ctdate2" runat="server"    Text="" ></asp:Label></td>
            </tr>   <tr>
            <td rowspan="2" align="center" bgcolor="#FFFFFF">分成税后  <br />
              金额(元)</td>
            <td rowspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable3" runat="server"    Text="54000" ></asp:Label></td>
            <td height="30" colspan="4" align="center" bgcolor="#FFFFFF">设计人员 </td>
            <td height="30" align="center" bgcolor="#FFFFFF">校核人员 </td>
            <td height="30" align="center" bgcolor="#FFFFFF">审核人员</td>
            <td height="30" align="center" bgcolor="#FFFFFF">专业负责</td>
            <td height="30" align="center" bgcolor="#FFFFFF">审定人员</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><strong>ABCDE</strong></td>
            </tr>   <tr>
            <td height="30" colspan="4" align="center" bgcolor="#FFFFFF">A</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B</td>
            <td height="30" align="center" bgcolor="#FFFFFF">D</td>
            <td height="30" align="center" bgcolor="#FFFFFF">C</td>
            <td height="30" align="center" bgcolor="#FFFFFF">E</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><strong>合计</strong></td>
            </tr>   <tr>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">设计校审分成比例</td>
            <td height="30" colspan="4" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable30" runat="server"    Text="23.5%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable31" runat="server"    Text="1.5%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable32" runat="server"    Text="1.6%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable33" runat="server"    Text="7.0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable34" runat="server"    Text="1.8%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable35" runat="server"    Text="35.4%" ></asp:Label></td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">设计报酬  <br />
              折合元/㎡</td>
            <td height="30" align="center" bgcolor="#FFFFFF">ABCDE<br />
费用</td>
            <td height="30" colspan="4" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable4" runat="server"    Text="12690" ></asp:Label>                           </td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable40" runat="server"    Text="810" ></asp:Label> </td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable41" runat="server"    Text="864" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable42" runat="server"    Text="3780" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable43" runat="server"    Text="972" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable44" runat="server"    Text="19116" ></asp:Label></td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable5" runat="server"    Text="0.70" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable51" runat="server"    Text="33%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">B1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B2</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B3</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B4</td>
            <td height="30" align="center" bgcolor="#FFFFFF">C1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">D</td>
            <td height="30" align="center" bgcolor="#FFFFFF">A</td>
            <td height="30" align="center" bgcolor="#FFFFFF">E</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td rowspan="4" align="center" bgcolor="#FFFFFF">建筑专业</td>
            <td height="30" align="center" bgcolor="#FFFFFF">设计比例</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable52" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable53" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable54" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable55" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable56" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">金  额</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable6" runat="server"    Text="4188" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable61" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable62" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable63" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable64" runat="server"    Text="267" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable65" runat="server"    Text="285" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable66" runat="server"    Text="1247" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable67" runat="server"    Text="321" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable68" runat="server"    Text="6308" ></asp:Label></td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">人  员</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">签  字</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable7" runat="server"    Text="0.72" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable71" runat="server"    Text="34%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">B1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B2</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B3</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B4</td>
            <td height="30" align="center" bgcolor="#FFFFFF">C1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">D</td>
            <td height="30" align="center" bgcolor="#FFFFFF">A</td>
            <td height="30" align="center" bgcolor="#FFFFFF">E</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td rowspan="4" align="center" bgcolor="#FFFFFF">结构专业</td>
            <td height="30" align="center" bgcolor="#FFFFFF">设计比例</td>
              <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable72" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable73" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable74" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable75" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable76" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">金 额</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable8" runat="server"    Text="4135" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable81" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable82" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable83" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable84" runat="server"    Text="275" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable85" runat="server"    Text="294" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable86" runat="server"    Text="1285" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable87" runat="server"    Text="330" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable88" runat="server"    Text="6499" ></asp:Label></td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">人 员</td>
             <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">签 字</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable9" runat="server"    Text="0.4" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable91" runat="server"    Text="19%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">B1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B2</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B3</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B4</td>
            <td height="30" align="center" bgcolor="#FFFFFF">C1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">D</td>
            <td height="30" align="center" bgcolor="#FFFFFF">A</td>
            <td height="30" align="center" bgcolor="#FFFFFF">E</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td rowspan="4" align="center" bgcolor="#FFFFFF">设备专业</td>
           <td height="30" align="center" bgcolor="#FFFFFF">设计比例</td>
              <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable92" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable93" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable94" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable95" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable96" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">金 额</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable100" runat="server"    Text="2411" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable101" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable102" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable103" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable104" runat="server"    Text="154" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable105" runat="server"    Text="164" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable106" runat="server"    Text="718" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable107" runat="server"    Text="185" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable108" runat="server"    Text="3632" ></asp:Label></td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">人 员</td>
             <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">签 字</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
                <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable110" runat="server"    Text="0.3" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable111" runat="server"    Text="14%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">B1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B2</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B3</td>
            <td height="30" align="center" bgcolor="#FFFFFF">B4</td>
            <td height="30" align="center" bgcolor="#FFFFFF">C1</td>
            <td height="30" align="center" bgcolor="#FFFFFF">D</td>
            <td height="30" align="center" bgcolor="#FFFFFF">A</td>
            <td height="30" align="center" bgcolor="#FFFFFF">E</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td rowspan="4" align="center" bgcolor="#FFFFFF">电气专业</td>
            <td height="30" align="center" bgcolor="#FFFFFF">设计比例</td>
              <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable112" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable113" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable114" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable115" runat="server"    Text="0%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable116" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
           <td height="30" align="center" bgcolor="#FFFFFF">金 额</td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable120" runat="server"    Text="1777" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable121" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable122" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable123" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable124" runat="server"    Text="113" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable125" runat="server"    Text="121" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable126" runat="server"    Text="529" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable127" runat="server"    Text="136" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable128" runat="server"    Text="2676" ></asp:Label></td>
            </tr>   <tr>
           <td height="30" align="center" bgcolor="#FFFFFF">人 员</td>
             <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">XXX</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">签 字</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable130" runat="server"    Text="2.12" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable131" runat="server"    Text="100%" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable132" runat="server"    Text="12690" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable133" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable134" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable135" runat="server"    Text="0" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable136" runat="server"    Text="810" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable137" runat="server"    Text="864" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable138" runat="server"    Text="3780" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable139" runat="server"    Text="972" ></asp:Label></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable1310" runat="server"    Text="19116" ></asp:Label></td>
            </tr>   <tr>
            <td rowspan="4" align="center" bgcolor="#FFFFFF">总体核验</td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">院留费用合计</td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable140" runat="server"    Text="13500" ></asp:Label></td>
            <td rowspan="4" align="center" bgcolor="#FFFFFF">备  注</td>
            <td height="30" colspan="5" align="left" bgcolor="#FFFFFF">1、本表部分由生产部门根据项目特性填写</td>
            </tr>   <tr>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">项目费用合计</td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable141" runat="server"    Text="19440" ></asp:Label></td>
            <td height="30" colspan="5" align="left" bgcolor="#FFFFFF">2、本表一式三份，院财务、项目经理、生产部各一份</td>
            </tr>   <tr>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">设计人员合计</td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable142" runat="server"    Text="21060" ></asp:Label></td>
            <td height="30" colspan="5" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>   <tr>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">总    计</td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_lable143" runat="server"    Text="54000" ></asp:Label></td>
            <td height="30" colspan="5" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
        </table>
    <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr>
      <td width="12%" align="center" height="30">院长：</td>
      <td width="18%" align="center">经营院长：</td><td width="16%" align="center">经营部: </td>
          <td width="19%" align="center">财务部:</td>
          <td width="16%" align="center">项目经理:</td>
          <td width="19%" align="center">制表人: </td>
        </tr>
  </table>
</form>
    
</body>
</html>
