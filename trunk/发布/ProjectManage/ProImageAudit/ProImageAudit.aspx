﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProImageAudit.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ProImageAudit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../../css/Customer.css" rel="stylesheet" type="text/css" />
    <link href="../../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../../css/Corperation.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="/js/ProjectMamage/ProImageAudit.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/MessageComm.js"></script>
    <style type="text/css">
        .title
        {
            width: 150px;
        }
    </style>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[项目管理-图纸信息]
            </td>
        </tr>
    </table>
    <table class="show_project">
        <tr>
            <td class="title">
                项目名称：
            </td>
            <td class="content">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
                <asp:HiddenField ID="hid_projid" runat="server" Value="" />
            </td>
            <td class="title">
                工程号：
            </td>
            <td class="content">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                子项名称：
            </td>
            <td class="content">
                <asp:Label ID="txt_proitemname" runat="server"></asp:Label>
                &nbsp;
            </td>
            <td class="title">
                承担部门：
            </td>
            <td class="content">
                <asp:Label ID="txt_prounit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="title">
                工程负责：
            </td>
            <td class="content">
                <asp:Label ID="txt_pm_name" runat="server" Width="400px"></asp:Label>
            </td>
            <td class="title">
                设计阶段：
            </td>
            <td class="content" id="content">
                <asp:Label ID="txt_prostatus" runat="server" Width="150px"></asp:Label>
            </td>
        </tr>
    </table>
    <!-- 建筑图纸 -->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    建筑图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="cst_datas">
            <tr id="cst_row">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="id">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="name">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="zhiwu">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="bumen">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="phone">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="email">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="oper">
                    折合1#图
                </td>
            </tr>
            <tr id="cst_row2">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td1">
                    <asp:Label ID="jz0" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td2">
                    <asp:Label ID="jz1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td3">
                    <asp:Label ID="jz2" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td4">
                    <asp:Label ID="jz2_1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td5">
                    <asp:Label ID="jz3" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td6">
                    <asp:Label ID="jz4" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td7">
                    <asp:Label ID="jz1_1" runat="server" Width="80px" Text="0" />
                </td>
            </tr>
            <tr id="Tr5">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td36">
                    <asp:Label ID="cadjz0" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td37">
                    <asp:Label ID="cadjz1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td38">
                    <asp:Label ID="cadjz2" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td39">
                    <asp:Label ID="cadjz2_1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td40">
                    <asp:Label ID="cadjz3" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td41">
                    <asp:Label ID="cadjz4" runat="server" Text="0" Width="80px" />
                </td>
                <td align="center" style="width: 13%" id="Td42">
                    <asp:Label ID="cadjz1_1" runat="server" Text="0" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
    <!--结构图纸-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    结构图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table1">
            <tr id="Tr1">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td8">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td9">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td10">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td11">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td12">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td13">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td14">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr2">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td15">
                    <asp:Label ID="jg0" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td16">
                    <asp:Label ID="jg1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td17">
                    <asp:Label ID="jg2" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td18">
                    <asp:Label ID="jg2_1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td19">
                    <asp:Label ID="jg3" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td20">
                    <asp:Label ID="jg4" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td21">
                    <asp:Label ID="jg1_1" runat="server" Width="80px" Text="0" />
                </td>
            </tr>
            <tr id="Tr3">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td22">
                    <asp:Label ID="cadjg0" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td23">
                    <asp:Label ID="cadjg1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td24">
                    <asp:Label ID="cadjg2" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td25">
                    <asp:Label ID="cadjg2_1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td26">
                    <asp:Label ID="cadjg3" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td27">
                    <asp:Label ID="cadjg4" runat="server" Text="0" Width="80px" />
                </td>
                <td align="center" style="width: 13%" id="Td28">
                    <asp:Label ID="cadjg1_1" runat="server" Text="0" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
    <!--给排水图纸-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    给排水图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table2">
            <tr id="Tr4">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td29">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td30">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td31">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td32">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td33">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td34">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td35">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr6">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td43">
                    <asp:Label ID="gps0" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td44">
                    <asp:Label ID="gps1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td45">
                    <asp:Label ID="gps2" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td46">
                    <asp:Label ID="gps2_1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td47">
                    <asp:Label ID="gps3" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td48">
                    <asp:Label ID="gps4" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td49">
                    <asp:Label ID="gps1_1" runat="server" Width="80px" Text="0" />
                </td>
            </tr>
            <tr id="Tr7">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td50">
                    <asp:Label ID="cadgps0" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td51">
                    <asp:Label ID="cadgps1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td52">
                    <asp:Label ID="cadgps2" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td53">
                    <asp:Label ID="cadgps2_1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td54">
                    <asp:Label ID="cadgps3" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td55">
                    <asp:Label ID="cadgps4" runat="server" Text="0" Width="80px" />
                </td>
                <td align="center" style="width: 13%" id="Td56">
                    <asp:Label ID="cadgps1_1" runat="server" Text="0" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
    <!--暖通图纸-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    暖通图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table3">
            <tr id="Tr8">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td57">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td58">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td59">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td60">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td61">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td62">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td63">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr9">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td64">
                    <asp:Label ID="nt0" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td65">
                    <asp:Label ID="nt1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td66">
                    <asp:Label ID="nt2" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td67">
                    <asp:Label ID="nt2_1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td68">
                    <asp:Label ID="nt3" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td69">
                    <asp:Label ID="nt4" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td70">
                    <asp:Label ID="nt1_1" runat="server" Width="80px" Text="0" />
                </td>
            </tr>
            <tr id="Tr10">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td71">
                    <asp:Label ID="cadnt0" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td72">
                    <asp:Label ID="cadnt1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td73">
                    <asp:Label ID="cadnt2" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td74">
                    <asp:Label ID="cadnt2_1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td75">
                    <asp:Label ID="cadnt3" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td76">
                    <asp:Label ID="cadnt4" runat="server" Text="0" Width="80px" />
                </td>
                <td align="center" style="width: 13%" id="Td77">
                    <asp:Label ID="cadnt1_1" runat="server" Text="0" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
    <!--电气-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    电气图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table4">
            <tr id="Tr11">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td78">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td79">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td80">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td81">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td82">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td83">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td84">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr12">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td85">
                    <asp:Label ID="dq0" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td86">
                    <asp:Label ID="dq1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td87">
                    <asp:Label ID="dq2" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td88">
                    <asp:Label ID="dq2_1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td89">
                    <asp:Label ID="dq3" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td90">
                    <asp:Label ID="dq4" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td91">
                    <asp:Label ID="dq1_1" runat="server" Width="80px" Text="0" />
                </td>
            </tr>
            <tr id="Tr13">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td92">
                    <asp:Label ID="caddq0" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td93">
                    <asp:Label ID="caddq1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td94">
                    <asp:Label ID="caddq2" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td95">
                    <asp:Label ID="caddq2_1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td96">
                    <asp:Label ID="caddq3" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td97">
                    <asp:Label ID="caddq4" runat="server" Text="0" Width="80px" />
                </td>
                <td align="center" style="width: 13%" id="Td98">
                    <asp:Label ID="caddq1_1" runat="server" Text="0" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
    <!--合计-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    图纸合计
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table5">
            <tr id="Tr14">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td99">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td100">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td101">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td102">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td103">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td104">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td105">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr15">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td106">
                    <asp:Label ID="hj0" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td107">
                    <asp:Label ID="hj1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td108">
                    <asp:Label ID="hj2" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td109">
                    <asp:Label ID="hj2_1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td110">
                    <asp:Label ID="hj3" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td111">
                    <asp:Label ID="hj4" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td112">
                    <asp:Label ID="hj1_1" runat="server" Width="80px" Text="0" />
                </td>
            </tr>
            <tr id="Tr16">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td113">
                    <asp:Label ID="cadhj0" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td114">
                    <asp:Label ID="cadhj1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td115">
                    <asp:Label ID="cadhj2" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td116">
                    <asp:Label ID="cadhj2_1" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td117">
                    <asp:Label ID="cadhj3" runat="server" Text="0" Width="80px" />
                </td>
                <td style="width: 13%" align="center" id="Td118">
                    <asp:Label ID="cadhj4" runat="server" Text="0" Width="80px" />
                </td>
                <td align="center" style="width: 13%" id="Td119">
                    <asp:Label ID="cadhj1_1" runat="server" Text="0" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
    <table id="tb_fujia" class="cls_show_cst_jiben">
        <tr>
            <td style="width: 120px;">
                晒图份数
            </td>
            <td>
                <asp:Label ID="Sum_image" runat="server" Text="0" Width="110px" />
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                图纸类型
            </td>
            <td style="width: 35%">
                <input id="check_new" type="radio" name="ct" value="新出图" <%=ct=="新出图"?"checked":"" %> />
                新出图
                <input id="check_Bc" type="radio" name="ct" value="补充图" <%=ct=="补充图"?"checked":"" %> />
                补充图
                <input id="check_update" type="radio" name="ct" value="变更图" <%=ct=="变更图"?"checked":"" %> />
                变更图
            </td>
            <td style="width: 15%">
                是否归档
            </td>
            <td style="width: 35%">
                <asp:CheckBox ID="check_Gd" disabled="disabled" runat="server" Text="归档" />
            </td>
        </tr>
    </table>
    <table style="width: 100%; height: auto;" class="cls_show_cst_jiben">
        <tr>
            <td style="width: 10%; text-align: center;">
                评审部门
            </td>
            <td style="width: 70%; text-align: center;">
                评审要点
            </td>
            <td style="width: 20%; text-align: center;">
                评审人/日期
            </td>
        </tr>
    </table>
    <div id="TableContainer">
        <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="oneTable"
            audittable="auditTable">
            <tr>
                <td style="width: 10%; text-align: center;">
                    技术质量部
                </td>
                <td style="width: 70%;">
                    <textarea style="width: 100%; height: 100px;" id="txtOneSuggsion" class="TextBoxBorder"></textarea>
                </td>
                <td style="width: 20%; text-align: center;" id="AuditUser">
                </td>
            </tr>
        </table>
        <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
            id="TwoTable" audittable="auditTable">
            <tr>
                <td style="width: 10%; text-align: center;">
                    生产经营部
                </td>
                <td style="width: 70%;">
                    <textarea style="width: 100%; height: 100px;" id="txtTwoSuggsion" class="TextBoxBorder"></textarea>
                </td>
                <td style="width: 20%; text-align: center;" id="AuditUser">
                </td>
            </tr>
        </table>
    </div>
    <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
        id="NoPowerTable">
        <tr>
            <td style="width: 100%; text-align: center; height: 100px; line-height: 100px;">
                您没有权限审核该项
            </td>
        </tr>
    </table>
    <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table6">
        <tr>
            <td style="width: 100%; text-align: center;">
                <input type="button" id="btnApproval" name="controlBtn" sclass="cls_btn_comm_w" value="通过" />
                <input type="button" id="btnRefuse" name="controlBtn" sclass="cls_btn_comm_w" value="不通过" />
                <input type="button" value="返回图纸审核页面，重新申请审核" id="FallBackCoperaion" style="display: none;" />
                <a href="javascript:window.history.back();" id="backBtn" style="display: none; border: none;">
                    <img src="../../images/buttons/btn_print.gif" width="65" height="23" border="0" /></a>
                <a href="javascript:void(0)" onclick="window.open('ProPrint.aspx?ProjectSysNo=<%=ProjectSysNo%>','打印','width=800,height=500,top=50,left=150,scrollbars=yes,resizable=yes,location=no')"
                    id="printBtn" style="display: none; border: none;">
                    <img src="../../images/buttons/btn_ct2.gif" width="85" height="23" border="0" /></a>
                <a href="javascript:void(0)" onclick="window.open('ProForeignPrint.aspx?ProjectSysNo=<%=ProjectSysNo%>','打印','width=600,height=550,top=50,left=150,scrollbars=yes,resizable=no,location=no')"
                    id="dwprintBtn" style="display: none; border: none;">
                    <img src="../../images/buttons/btn_ft2.gif" width="85" height="23" border="0" /></a>
            </td>
        </tr>
    </table>
    </form>
    <!--hiddenArea-->
    <input type="hidden" id="HiddenSysNo" value="<%=proImaAuditRecordEntity.SysNo %>" />
    <input type="hidden" id="HiddenStatus" value="<%=proImaAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenOneSuggestion" value="<%=proImaAuditRecordEntity.OneSuggestion %>" />
    <input type="hidden" id="HiddenTwoSuggestion" value="<%=proImaAuditRecordEntity.TwoSuggestion %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=proImaAuditRecordEntity.AuditUserString %>" />
    <input type="hidden" id="HiddenAuditDate" value="<%=proImaAuditRecordEntity.AuditDate %>" />
    <input type="hidden" id="HiddenProSysNo" value="<%=ProjectSysNo %>" />
    <input type="hidden" id="Hidden1" value="<%=UserSysNo %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
</body>
</html>
