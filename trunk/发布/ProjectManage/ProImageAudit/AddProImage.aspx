﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProImage.aspx.cs" Inherits="TG.Web.ProjectManage.ProImageAudit.AddProImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" rel="Stylesheet" href="/css/m_comm.css" />
    <link type="text/css" rel="Stylesheet" href="~/css/Customer.css" />
    <link type="text/css" href="~/css/jquery.alerts.css" rel="Stylesheet" />
    <link type="text/css" rel="stylesheet" href="/css/swfupload/default_cpr.css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/AddImage.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/ProjectMamage/AddProImage.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[项目信息-出图图纸信息]
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="cls_content" align="center" valign="top">
                <table class="cls_content_2">
                    <tr>
                        <td class="cls_head_2">
                            <div class="cls_1">
                                项目信息
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td style="width: 15%">
                工程名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                工程号:
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                子项名称
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proitemname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                承担部门：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prounit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                设 &nbsp 总：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pm_name" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计阶段：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prostatus" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <!-- 建筑图纸 -->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    建筑图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="cst_datas">
            <tr id="cst_row">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="id">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="name">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="zhiwu">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="bumen">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="phone">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="email">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="oper">
                    折合1#图
                </td>
            </tr>
            <tr id="cst_row2">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td1">
                    <asp:TextBox ID="jz0" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td2">
                    <asp:TextBox ID="jz1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td3">
                    <asp:TextBox ID="jz2" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td4">
                    <asp:TextBox ID="jz2_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td5">
                    <asp:TextBox ID="jz3" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td6">
                    <asp:TextBox ID="jz4" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td7">
                    <asp:TextBox ID="jz1_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
            </tr>
            <tr id="Tr5">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td36">
                    <asp:TextBox ID="cadjz0" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td37">
                    <asp:TextBox ID="cadjz1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td38">
                    <asp:TextBox ID="cadjz2" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td39">
                    <asp:TextBox ID="cadjz2_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td40">
                    <asp:TextBox ID="cadjz3" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td41">
                    <asp:TextBox ID="cadjz4" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td align="center" style="width: 13%" id="Td42">
                    <asp:TextBox ID="cadjz1_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
            </tr>
        </table>
    </div>
    <!--结构图纸-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    结构图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table1">
            <tr id="Tr1">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td8">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td9">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td10">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td11">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td12">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td13">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td14">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr2">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td15">
                    <asp:TextBox ID="jg0" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td16">
                    <asp:TextBox ID="jg1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td17">
                    <asp:TextBox ID="jg2" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td18">
                    <asp:TextBox ID="jg2_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td19">
                    <asp:TextBox ID="jg3" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td20">
                    <asp:TextBox ID="jg4" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td21">
                    <asp:TextBox ID="jg1_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
            </tr>
            <tr id="Tr3">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td22">
                    <asp:TextBox ID="cadjg0" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td23">
                    <asp:TextBox ID="cadjg1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td24">
                    <asp:TextBox ID="cadjg2" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td25">
                    <asp:TextBox ID="cadjg2_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td26">
                    <asp:TextBox ID="cadjg3" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td27">
                    <asp:TextBox ID="cadjg4" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td align="center" style="width: 13%" id="Td28">
                    <asp:TextBox ID="cadjg1_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
            </tr>
        </table>
    </div>
    <!--给排水图纸-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    给排水图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table2">
            <tr id="Tr4">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td29">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td30">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td31">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td32">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td33">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td34">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td35">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr6">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td43">
                    <asp:TextBox ID="gps0" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td44">
                    <asp:TextBox ID="gps1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td45">
                    <asp:TextBox ID="gps2" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td46">
                    <asp:TextBox ID="gps2_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td47">
                    <asp:TextBox ID="gps3" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td48">
                    <asp:TextBox ID="gps4" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td49">
                    <asp:TextBox ID="gps1_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
            </tr>
            <tr id="Tr7">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td50">
                    <asp:TextBox ID="cadgps0" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td51">
                    <asp:TextBox ID="cadgps1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td52">
                    <asp:TextBox ID="cadgps2" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td53">
                    <asp:TextBox ID="cadgps2_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td54">
                    <asp:TextBox ID="cadgps3" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td55">
                    <asp:TextBox ID="cadgps4" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td align="center" style="width: 13%" id="Td56">
                    <asp:TextBox ID="cadgps1_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
            </tr>
        </table>
    </div>
    <!--暖通图纸-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    暖通图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table3">
            <tr id="Tr8">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td57">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td58">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td59">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td60">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td61">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td62">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td63">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr9">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td64">
                    <asp:TextBox ID="nt0" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td65">
                    <asp:TextBox ID="nt1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td66">
                    <asp:TextBox ID="nt2" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td67">
                    <asp:TextBox ID="nt2_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td68">
                    <asp:TextBox ID="nt3" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td69">
                    <asp:TextBox ID="nt4" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td70">
                    <asp:TextBox ID="nt1_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
            </tr>
            <tr id="Tr10">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td71">
                    <asp:TextBox ID="cadnt0" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td72">
                    <asp:TextBox ID="cadnt1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td73">
                    <asp:TextBox ID="cadnt2" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td74">
                    <asp:TextBox ID="cadnt2_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td75">
                    <asp:TextBox ID="cadnt3" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td76">
                    <asp:TextBox ID="cadnt4" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td align="center" style="width: 13%" id="Td77">
                    <asp:TextBox ID="cadnt1_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
            </tr>
        </table>
    </div>
    <!--电气-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    电气图纸
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table4">
            <tr id="Tr11">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td78">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td79">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td80">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td81">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td82">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td83">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td84">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr12">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td85">
                    <asp:TextBox ID="dq0" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td86">
                    <asp:TextBox ID="dq1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td87">
                    <asp:TextBox ID="dq2" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td88">
                    <asp:TextBox ID="dq2_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td89">
                    <asp:TextBox ID="dq3" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td90">
                    <asp:TextBox ID="dq4" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td91">
                    <asp:TextBox ID="dq1_1" runat="server" Width="80px" Text="0" CssClass="cls_input_text_img" />
                </td>
            </tr>
            <tr id="Tr13">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td92">
                    <asp:TextBox ID="caddq0" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td93">
                    <asp:TextBox ID="caddq1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td94">
                    <asp:TextBox ID="caddq2" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td95">
                    <asp:TextBox ID="caddq2_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td96">
                    <asp:TextBox ID="caddq3" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td style="width: 13%" align="center" id="Td97">
                    <asp:TextBox ID="caddq4" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
                <td align="center" style="width: 13%" id="Td98">
                    <asp:TextBox ID="caddq1_1" runat="server" Text="0" Width="80px"  CssClass="cls_input_text_img" />
                </td>
            </tr>
        </table>
    </div>
    <!--合计-->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    图纸合计
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="Table5">
            <tr id="Tr14">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="Td99">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="Td100">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="Td101">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="Td102">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="Td103">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="Td104">
                    4#图
                </td>
                <td style="width: 13%" align="center" id="Td105">
                    折合1#图
                </td>
            </tr>
            <tr id="Tr15">
                <td style="width: 9%" align="center">
                </td>
                <%-- style="width: 13%"--%>
                <td style="width: 13%" align="center" id="Td106">
                    <asp:Label ID="hj0" runat="server" Width="80px" Text="0"  />
                </td>
                <td style="width: 13%" align="center" id="Td107">
                    <asp:Label ID="hj1" runat="server" Width="80px" Text="0"  />
                </td>
                <td style="width: 13%" align="center" id="Td108">
                    <asp:Label ID="hj2" runat="server" Width="80px" Text="0"  />
                </td>
                <td style="width: 13%" align="center" id="Td109">
                    <asp:Label ID="hj2_1" runat="server" Width="80px" Text="0"  />
                </td>
                <td style="width: 13%" align="center" id="Td110">
                    <asp:Label ID="hj3" runat="server" Width="80px" Text="0"  />
                </td>
                <td style="width: 13%" align="center" id="Td111">
                    <asp:Label ID="hj4" runat="server" Width="80px" Text="0"  />
                </td>
                <td style="width: 13%" align="center" id="Td112">
                    <asp:Label ID="hj1_1" runat="server" Width="80px" Text="0"  />
                </td>
            </tr>
            <tr id="Tr16">
                <td style="width: 9%" align="center">
                    CAD
                </td>
                <td style="width: 13%" align="center" id="Td113">
                    <asp:Label ID="cadhj0" runat="server" Text="0" Width="80px"   />
                </td>
                <td style="width: 13%" align="center" id="Td114">
                    <asp:Label ID="cadhj1" runat="server" Text="0" Width="80px"   />
                </td>
                <td style="width: 13%" align="center" id="Td115">
                    <asp:Label ID="cadhj2" runat="server" Text="0" Width="80px"   />
                </td>
                <td style="width: 13%" align="center" id="Td116">
                    <asp:Label ID="cadhj2_1" runat="server" Text="0" Width="80px"   />
                </td>
                <td style="width: 13%" align="center" id="Td117">
                    <asp:Label ID="cadhj3" runat="server" Text="0" Width="80px"   />
                </td>
                <td style="width: 13%" align="center" id="Td118">
                    <asp:Label ID="cadhj4" runat="server" Text="0" Width="80px"   />
                </td>
                <td align="center" style="width: 13%" id="Td119">
                    <asp:Label ID="cadhj1_1" runat="server" Text="0" Width="80px"  />
                </td>
            </tr>
        </table>
    </div>
    <table id="tb_fujia" class="cls_show_cst_jiben">
        <tr>
            <td style="width: 120px;">
                晒图份数
            </td>
            <td>
                <asp:Label ID="Sum_image" runat="server" Text="0" Width="110px" />
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                图纸类型
            </td>
            <td style="width: 35%">
                <input ID="check_new" type="radio" name="ct" value="新出图" <%=ct=="新出图"?"checked":"" %>/> 新出图
                <input ID="check_Bc" type="radio" name="ct" value="补充图" <%=ct=="补充图"?"checked":"" %> /> 补充图
                <input ID="check_update" type="radio" name="ct" value="变更图" <%=ct=="变更图"?"checked":"" %>/> 变更图
            </td>
            <td style="width: 15%">
                是否归档
            </td>
            <td style="width: 35%">
                <asp:CheckBox ID="check_Gd" runat="server" Text="归档" />
            </td>
        </tr>
    </table>
    <table id="tb_fileupload" class="cls_show_cst_jiben">
        <tr align="center">
            <td style="height: 25px;">
                <div id="divFileProgressContainer" style="height: 30px; width: 560px;">
                </div>
            </td>
        </tr>
        <tr align="center">
            <td>
                <asp:ImageButton ID="btn_Save" runat="server" ImageUrl="~/Images/buttons/btn_save2.gif"
                    Height="23px" Width="65px" OnClick="btn_Save_Click" />
                &nbsp;<a href="Pro_imageList.aspx" target="_self"><img src="/Images/buttons/btn_back2.gif"
                    style="width: 65px; height: 23px; border: none;" /></a>
            </td>
        </tr>
        <tr align="center">
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <input type="hidden" id="attachHidden" runat="server" />
    <!--HiddenArea-->
    <input type="hidden" id="hiddenCountry" runat="server" />
    <input type="hidden" id="hiddenProvince" runat="server" />
    <input type="hidden" id="hiddenCity" runat="server" />
    <!--end-->
    </form>
</body>
</html>
