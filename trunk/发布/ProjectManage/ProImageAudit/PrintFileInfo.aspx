﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintFileInfo.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.PrintFileInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .cls_content_head
        {
            width: 100%;
            height: 25px; /*margin: 0 auto;
            border: solid 1px black;*/
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_content_head td
        {
            border: solid 1px black;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_content_head tr
        {
            height: 25px;
        }
        
        #rbtlist td
        {
            border: 0px;
        }
        .display
        {
            display: none;
        }
    </style>
    <style type="text/css" media="print">
        .NoPrint
        {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: 0 auto; width: 95%;">
        <table style="width: 100%;">
            <tr>
                <td align="center" style="font-family: 微软雅黑; font-size: 18px;">
                    工程设计归档资料目录
                </td>
            </tr>
        </table>
        <table class="cls_content_head" align="center">
            <tr>
                <td style="width: 10%;" align="center">
                    项目名称
                </td>
                <td colspan="4" style="width: 50%;" align="center">
                    <asp:Label ID="txt_proname" runat="server"></asp:Label>
                </td>
                <td style="width: 15%;" align="center">
                    工程号
                </td>
                <td style="width: 25%;" align="center">
                    <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    部门
                </td>
                <td style="width: 10%;">
                    <asp:Label ID="txt_prounit" runat="server"></asp:Label>
                </td>
                <td align="center" style="width: 20%;">
                    项目负责人
                </td>
                <td colspan="2" style="width: 30%;">
                    <asp:Label ID="lbl_PMName" runat="server"></asp:Label>
                </td>
                <td style="width: 15%;" align="center">
                    工程级别
                </td>
                <td style="width: 25%;">
                    <asp:Label ID="lblProcess" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 10%;" align="center">
                    序号
                </td>
                <td style="width: 35%;" colspan="3" align="center">
                    名称
                </td>
                <td style="width: 15%" align="center">
                    页数
                </td>
                <td style="width: 15%;" align="center">
                    有无
                </td>
                <td style="width: 25%" align="center">
                    备注
                </td>
            </tr>
            <asp:Literal ID="lblFileInfo" runat="server"></asp:Literal>
        </table>
        <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table1">
            <tr>
                <td style="width: 100%; text-align: center;">
                    <input id="print" type="button" onclick="window.print()" value="打印" class="NoPrint"
                        style="width: 75px; height: 23px; cursor: pointer;" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
