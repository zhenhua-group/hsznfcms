﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateProImage.aspx.cs" Inherits="TG.Web.ProjectManage.ProImageAudit.UpdateProImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link type="text/css" rel="Stylesheet" href="/css/m_comm.css" />
    <link type="text/css" rel="Stylesheet" href="~/css/Customer.css" />
    <link type="text/css" href="~/css/jquery.alerts.css" rel="Stylesheet" />
    <link type="text/css" rel="stylesheet" href="/css/swfupload/default_cpr.css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/ProjectMamage/UpdateProImage.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[项目信息-发图图纸信息]
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="cls_content" align="center" valign="top">
                <table class="cls_content_2">
                    <tr>
                        <td class="cls_head_2">
                            <div class="cls_1">
                                项目信息
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td style="width: 15%">
                工程名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                工程编号:
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                图纸张数：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_tznumber" runat="server" Text="0"></asp:Label>
            </td>
            <td style="width: 15%">
                折1#图纸张数：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_ztnumber" runat="server" Text="0"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                发图份数：
            </td>
            <td style="width: 35%">
                <asp:TextBox ID="txt_send" runat="server" Text="0" CssClass="cls_input_text"/>
            </td>
            <td style="width: 15%">
                出图单位：
            </td>
            <td style="width: 35%">
                <asp:TextBox ID="txt_company" runat="server" Text="" CssClass="cls_input_text" />
            </td>
        </tr>
    </table>
    <!-- 建筑图纸 -->
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    图纸张数
                </div>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" id="cst_datas">
            <tr id="cst_row">
                <td style="width: 9%" align="center">
                    分类
                </td>
                <td style="width: 13%" align="center" id="id">
                    0#图
                </td>
                <td style="width: 13%" align="center" id="name">
                    1#图
                </td>
                <td style="width: 13%" align="center" id="zhiwu">
                    2#图
                </td>
                <td style="width: 13%" align="center" id="bumen">
                    2#图加长
                </td>
                <td style="width: 13%" align="center" id="phone">
                    3#图
                </td>
                <td style="width: 13%" align="center" id="email">
                    4#图
                </td>
               
            </tr>
            <tr id="cst_row2">
                <td style="width: 9%" align="center">
                张数
                </td>
                <%-- style="width: 13%"--%>
                  <td style="width: 13%" align="center" id="Td106">
                    <asp:Label ID="hj0" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td107">
                    <asp:Label ID="hj1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td108">
                    <asp:Label ID="hj2" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td109">
                    <asp:Label ID="hj2_1" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td110">
                    <asp:Label ID="hj3" runat="server" Width="80px" Text="0" />
                </td>
                <td style="width: 13%" align="center" id="Td111">
                    <asp:Label ID="hj4" runat="server" Width="80px" Text="0" />
                </td>
              
            </tr>
            <tr id="Tr5">
                <td style="width: 9%" align="center">
                    价格
                </td>
                <td style="width: 13%" align="center" id="Td36">
                    <asp:TextBox ID="price0" runat="server" Text="0" Width="80px" CssClass="cls_input_text_img"/>
                </td>
                <td style="width: 13%" align="center" id="Td37">
                    <asp:TextBox ID="price1" runat="server" Text="0" Width="80px" CssClass="cls_input_text_img"/>
                </td>
                <td style="width: 13%" align="center" id="Td38">
                    <asp:TextBox ID="price2" runat="server" Text="0" Width="80px" CssClass="cls_input_text_img"/>
                </td>
                <td style="width: 13%" align="center" id="Td39">
                    <asp:TextBox ID="price2_1" runat="server" Text="0" Width="80px" CssClass="cls_input_text_img"/>
                </td>
                <td style="width: 13%" align="center" id="Td40">
                    <asp:TextBox ID="price3" runat="server" Text="0" Width="80px" CssClass="cls_input_text_img"/>
                </td>
                <td style="width: 13%" align="center" id="Td41">
                    <asp:TextBox ID="price4" runat="server" Text="0" Width="80px" CssClass="cls_input_text_img"/>
                </td>
               
            </tr>
        </table>
    </div>   
    
    <table id="tb_fujia" class="cls_show_cst_jiben">
        <tr>
            <td style="width: 15%; height:30px;">
                合 计（张）
            </td>
            <td>
                <asp:Label ID="Sum_image" runat="server" Text="0" Width="110px" />
            </td>
             <td style="width: 15%">
                总 计（元）
            </td>
            <td style="width: 35%">
                 <asp:Label ID="Sum_money" runat="server" Text="0" Width="110px" />
            </td>
        </tr>
      
    </table>
    <table id="tb_fileupload" class="cls_show_cst_jiben">
        <tr align="center">
            <td>
            </td>
            </tr>
        <tr align="center">
            <td>
                <asp:ImageButton ID="btn_Save" runat="server" ImageUrl="~/Images/buttons/btn_save2.gif"
                    Height="23px" Width="65px" OnClick="btn_Save_Click"/>
                &nbsp;<a href="Pro_imageList.aspx" target="_self"><img src="/Images/buttons/btn_back2.gif"
                    style="width: 65px; height: 23px; border: none;" /></a>
            </td>
        </tr>
      
    </table>
    <input type="hidden" id="attachHidden" runat="server" />
    <!--HiddenArea-->
    <input type="hidden" id="hiddenCountry" runat="server" />
    <input type="hidden" id="hiddenProvince" runat="server" />
    <input type="hidden" id="hiddenCity" runat="server" />
    <!--end-->
    </form>
</body>
</html>
