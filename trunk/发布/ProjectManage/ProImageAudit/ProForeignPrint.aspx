﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProForeignPrint.aspx.cs" Inherits="TG.Web.ProjectManage.ProImageAudit.ProForeignPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <title>对外发图通知单</title>
    <link type="text/css" rel="Stylesheet" href="/css/m_comm.css" />
    <link type="text/css" rel="Stylesheet" href="~/css/Customer.css" />
    <link type="text/css" href="~/css/jquery.alerts.css" rel="Stylesheet" />
    <link type="text/css" rel="stylesheet" href="/css/swfupload/default_cpr.css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 29%;
        }
    </style>
 
</head>
<script>
    function preview(oper) {
        if (oper < 10) {
            bdhtml = window.document.body.innerHTML; //获取当前页的html代码
            sprnstr = "<!--startprint" + oper + "-->"; //设置打印开始区域
            eprnstr = "<!--endprint" + oper + "-->"; //设置打印结束区域
            prnhtml = bdhtml.substring(bdhtml.indexOf(sprnstr) + 18); //从开始代码向后取html

            prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr)); //从结束代码向前取html
            window.document.body.innerHTML = prnhtml;
            window.print();
            window.document.body.innerHTML = bdhtml;


        } else {
            window.print();
        }

    }
</script>

<body style="font-size: 12px;font-family: ""微软雅黑"";">
    <form id="form1" runat="server">
<!--startprint1-->
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" colspan="3" align="center"><strong style="font-size:14px;">对外发图通知单</strong></td>
  </tr>
  <tr>
    <td height="30" align="center" class="style1">&nbsp;日 &nbsp;&nbsp;期：<%=DateTime.Now.ToString("yyyy-MM-dd") %></td>
    <td width="19%" align="center"></td>
    <td width="53%" align="center">工程编号：<asp:Label ID="txt_pronumber" runat="server"></asp:Label></td>
  </tr>
</table>
<table width="95%" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#000000">
  
  <tr>
    <td width="27%" rowspan="2" align="center" bgcolor="#FFFFFF">项目名称</td>
    <td height="30" colspan="4" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_proname" runat="server"/></td>
  </tr>
  <tr>
    <td height="30" colspan="4" align="center" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">图纸张数</td>
    <td width="13%" height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_tznumber" runat="server" Text="0"></asp:Label></td>
    <td width="30%" height="30" align="center" bgcolor="#FFFFFF">折1#图纸张数</td>
    <td width="15%" height="30" align="center" bgcolor="#FFFFFF" colspan="2"><asp:Label ID="txt_ztnumber" runat="server" Text="0"></asp:Label></td>
    
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">发图份数</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_send" runat="server" Text="0"/></td>
    <td height="30" align="center" bgcolor="#FFFFFF">出图单位</td>
    <td height="30" align="center" bgcolor="#FFFFFF" colspan="2"><asp:Label ID="txt_company" runat="server" Text="" /></td>
  </tr>
  <tr>
    <td rowspan="6" align="center" bgcolor="#FFFFFF">各类图纸张数</td>
    <td height="30" align="center" bgcolor="#FFFFFF">A0</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj0" runat="server" Width="80px" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF">价格</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="price0" runat="server" Text="0" Width="80px" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">A1</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj1" runat="server" Width="80px" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF">价格</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="price1" runat="server" Text="0" Width="80px" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">A2</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj2" runat="server" Width="80px" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF">价格</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="price2" runat="server" Text="0" Width="80px" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">A2+</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj2_1" runat="server" Width="80px" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF">价格</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="price2_1" runat="server" Text="0" Width="80px" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">A3</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj3" runat="server" Width="80px" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF">价格</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="price3" runat="server" Text="0" Width="80px" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">A4</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj4" runat="server" Width="80px" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF">价格</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="price4" runat="server" Text="0" Width="80px" /> </td>
  </tr>
  <tr>
    <td height="50" align="center" bgcolor="#FFFFFF">合 计（张）</td>
    <td height="50" align="center" bgcolor="#FFFFFF" colspan=2><asp:Label ID="Sum_image" runat="server" Text="0" /></td>
  
    <td height="50" align="center" bgcolor="#FFFFFF">总 计（元）</td>
    <td height="50" align="center" bgcolor="#FFFFFF"><asp:Label ID="Sum_money" runat="server" Text="0" /></td>
  </tr>
</table>
<br />
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="28%" height="30" align="center">生成经营处： </td>
    <td width="10%" align="center">&nbsp;</td>
    <td width="62%" align="center">生产服务公司：</td>
  </tr>
</table>
<!--endprint1-->
<table width="95%" align="center" border=0><tr><td align=center><input type="button" onclick="preview(1);" value=" " style="background:url(../../images/buttons/btn_printgif.gif); width:65px; height:23px; border:0; cursor:pointer;" /></td></tr></table>
    </form>
</body>
</html>
