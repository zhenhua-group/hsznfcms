﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pro_imageList.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.Pro_imageList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.chromatable.js"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            CommonControl.SetFormWidth();
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;当前位置：[项目出图管理]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
            <table class="cls_head_div">
                    <tr>
                        <td>
                            生产部门：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit" runat="server" Width="120px" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            项目名称：<input id="txt_keyname" name="txt_keyname" type="text" runat="Server" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btn_Search" runat="server" ImageUrl="~/images/buttons/btn_search.gif"
                                Height="20px" OnClick="btn_Search_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head">
            <tr>
                <td style="width: 20%;" align="center">
                    项目名称
                </td>
                <td style="width: 10%" align="center">
                    工程号
                </td>
                <td style="width: 10%" align="center">
                    承担部门
                </td>
                <td style="width: 10%" align="center">
                    设计总负责
                </td>
                <td style="width: 15%" align="center">
                    设计阶段
                </td>
                <td style="width: 10%" align="center">
                    开始时间
                </td>
                <td style="width: 10%" align="center">
                    结束时间
                </td>
                <td style="width: 15%" align="center">
                    操作
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_project" runat="server" CellPadding="0" DataKeyNames="pro_ID"  CssClass="gridView_comm"
            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" ShowHeader="False"
            Font-Size="12px" RowStyle-Height="22px" Width="100%">
            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
            <Columns>
                <asp:TemplateField HeaderText="项目名称">
                    <ItemTemplate>
                        <img src="/Images/proj.png" style="width: 16px; height: 16px; margin-left: 1px;" />
                        <a href='ShowProImage.aspx?flag=list&pro_id=<%# Eval("pro_ID") %>' title='<%# Eval("pro_name") %>'
                            rel='<%# Eval("pro_ID") %>' class="chk">
                            <%# Eval("pro_name").ToString().Length > 15 ? Eval("pro_name").ToString().Substring(0, 15) + "......" : Eval("pro_name").ToString()%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                </asp:TemplateField>
                <asp:BoundField DataField="Pro_number" HeaderText="工程号" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Unit" HeaderText="承担部门">
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="设计总负责 ">
                    <ItemTemplate>
                        <%#Eval("PMName")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
                <asp:BoundField DataField="pro_status" HeaderText="设计阶段">
                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="pro_startTime" HeaderText="项目开始时间" DataFormatString="{0:yyyy-MM-dd}">
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="pro_finishTime" HeaderText="项目结束时间" DataFormatString="{0:yyyy-MM-dd}">
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="编辑">
                    <ItemTemplate>
                        <a href='AddProImage.aspx?bs_project_Id=<%# Eval("pro_ID") %>' class='allowEdit'
                            target="_self">出图编辑</a> |<a href='UpdateProImage.aspx?bs_project_Id=<%# Eval("pro_ID") %>'
                                class="allowEdit" target="_self"> 发图编辑</a>
                    </ItemTemplate>
                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条" CustomInfoTextAlign="Left"
            FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
            PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
            ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
            TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="30" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
    </div>
    </form>
</body>
</html>
