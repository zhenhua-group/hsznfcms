﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectFileInfoAudit.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ProjectFileInfoAudit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.alerts.css" rel="Stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <link href="/css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="/css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="/js/Common/SendMessageCommon.js"></script>
    <script src="../../js/MessageComm.js" type="text/javascript"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/ProjectFileInfoAudit.js" type="text/javascript"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[项目信息-工程设计归档资料审核]
            </td>
        </tr>
        <tr>
            <td class="cls_content" align="center" valign="top">
                <table class="cls_content_2">
                    <tr>
                        <td class="cls_head_2">
                            <div class="cls_1">
                                项目信息
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben" id="projectInfo">
        <tr>
            <td style="width: 15%">
                工程名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                工程号:
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                设计部门：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prounit" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计阶段：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prostatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                管理级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_level" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                审核级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                设&nbsp;&nbsp;总：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_PMName" runat="server" Width="150px"></asp:Label>
            </td>
            <td style="width: 15%">
            </td>
            <td style="width: 35%">
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    工程设计归档信息
                </div>
            </td>
        </tr>
    </table>
    <table class="cls_content_head" style="width: 100%;">
        <tr id="Tr1">
            <td style="width: 10%;" align="center">
                序号
            </td>
            <td style="width: 25%;" align="center">
                名称
            </td>
            <td style="width: 15%" align="center">
                页数
            </td>
            <td style="width: 15%;" align="center">
                有无
            </td>
            <td style="width: 35%" align="center">
                备注
            </td>
        </tr>
    </table>
    <asp:Literal ID="lblFileInfo" runat="server"></asp:Literal>
    <!--审核区域-->
    <table style="width: 100%;" class="cls_memlist">
        <tr>
            <td>
                <%=AuditHTML%>
            </td>
        </tr>
    </table>
    <%-- <div class="cls_data_bottom" style="text-align: center;">
        <input type="button" id="Button1" name="controlBtn" class="cls_btn_comm_w" value="返回"
            onclick="javascript:history.back();" />
    </div>--%>
    <input type="hidden" id="ProjectSysNo" value="<%=ProSysNo %>" />
    <input type="hidden" id="HiddenProjectPlotAuditSysNo" value="<%=ProjectFileAuditSysNo %>" />
    <input type="hidden" id="HiddenAuditStatus" value="<%=AuditStatus %>" />
 
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--选择消息接收者-->
    <div id="msgReceiverContainer" style="display: none; width: 400px; height: 200px;">
    </div>
    </form>
</body>
</html>
