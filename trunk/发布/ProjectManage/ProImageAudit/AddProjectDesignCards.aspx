﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProjectDesignCards.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.AddProjectDesignCards" %>

<%@ Register Src="/UserControl/ChooseUser.ascx" TagName="ChooseUser" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>新增工程设计出图卡</title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.alerts.css" rel="Stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <link href="/css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.alerts.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.chromatable.js"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/AddProjectSubPlotInfo.js" type="text/javascript"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/AddProjectDesignCards.js" type="text/javascript"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[项目信息-新增工程设计出图卡]
            </td>
        </tr>
        <tr>
            <td class="cls_content" align="center" valign="top">
                <table class="cls_content_2">
                    <tr>
                        <td class="cls_head_2">
                            <div class="cls_1">
                                项目信息
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben" id="projectInfo">
        <tr>
            <td style="width: 15%">
                工程名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                工程号:
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                子项名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proitemname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计阶段：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prostatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                管理级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_level" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计部门：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prounit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                审核级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设&nbsp;&nbsp;总：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_PMName" runat="server" Width="150px"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    工程设计出图卡
                </div>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td style="width: 15%">
                出图类别：
            </td>
            <td style="width: 35%">
                <%-- <asp:DropDownList ID="drop_PlotType" runat="Server" Width="120px" AppendDataBoundItems="True"
                    CssClass="cls_input_text_valid">
                    <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                </asp:DropDownList>--%>
                <asp:RadioButtonList ID="rbtlist" runat="server" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
            <td style="width: 15%">
                晒图份数：
            </td>
            <td style="width: 35%">
                <input id="txt_BluePrintCounts" type="text" name="number" class="cls_input_text_valid_w"
                    style="width: 120px;" />(份)
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                出图人：
            </td>
            <td style="width: 35%">
                <input id="txt_PMName" type="text" name="number" disabled="disabled" class="cls_input_text_valid_w"
                    style="width: 118px;" />
                <asp:HiddenField ID="hid_pmname" runat="server" Value="0" />
                <asp:HiddenField ID="hid_pmuserid" runat="server" Value="0" />
                <span style="color: blue; cursor: pointer" id="sch_PM">查询</span>
            </td>
            <td style="width: 15%">
            </td>
            <td style="width: 35%">
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                详细信息：
            </td>
            <td colspan="3">
                <table class="cls_content_head" style="width: 100%;">
                    <tr id="Tr1">
                        <td style="width: 10%;" align="center">
                            专业
                        </td>
                        <td style="width: 15%;" align="center">
                            标准长
                        </td>
                        <td style="width: 15%" align="center">
                            加1/4倍长
                        </td>
                        <td style="width: 15%;" align="center">
                            加2/4倍长
                        </td>
                        <td style="width: 15%" align="center">
                            加3/4倍长
                        </td>
                        <td style="width: 15%;" align="center">
                            加1倍长
                        </td>
                        <td style="width: 15%;" align="center">
                            折合1#图
                        </td>
                    </tr>
                </table>
                <table class="show_projectPlot show_projectPlot_Input" style="width: 100%;" id="tbSunPlotInfo">
                    <tr>
                        <td rowspan="5" style="width: 10%;">
                            建筑
                        </td>
                        <td style="width: 15%; border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text208" type="text" runat="server" spe="建筑" lenghts="A0bz" value="0" />(张)
                        </td>
                        <td style="width: 15%; border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text207" type="text" runat="server" spe="建筑" lenghts="A0fzy" value="0" />(张)
                        </td>
                        <td style="width: 15%; border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text209" type="text" runat="server" spe="建筑" lenghts="A0fze" value="0" />(张)
                        </td>
                        <td style="width: 15%; border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text210" type="text" runat="server" spe="建筑" lenghts="A0fzs" value="0" />(张)
                        </td>
                        <td style="width: 15%; border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text211" type="text" runat="server" spe="建筑" lenghts="A0lb" value="0" />(张)
                        </td>
                        <td style="width: 15%;" rowspan="5">
                            <input id="totalJz" type="text" runat="server" value="0" disabled="disabled" />(张)
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            建筑
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text1" type="text" runat="server" spe="建筑" lenghts="A1bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text2" type="text" runat="server" spe="建筑" lenghts="A1fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text3" type="text" runat="server" spe="建筑" lenghts="A1fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text4" type="text" runat="server" spe="建筑" lenghts="A1fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text5" type="text" runat="server" spe="建筑" lenghts="A1lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            建筑
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text6" type="text" runat="server" spe="建筑" lenghts="A2bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text7" type="text" runat="server" spe="建筑" lenghts="A2fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text8" type="text" runat="server" spe="建筑" lenghts="A2fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text9" type="text" runat="server" spe="建筑" lenghts="A2fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text10" type="text" runat="server" spe="建筑" lenghts="A2lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            建筑
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text11" type="text" runat="server" spe="建筑" lenghts="A3bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text12" type="text" runat="server" spe="建筑" lenghts="A3fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text13" type="text" runat="server" spe="建筑" lenghts="A3fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text14" type="text" runat="server" spe="建筑" lenghts="A3fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text15" type="text" runat="server" spe="建筑" lenghts="A3lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            建筑
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text19" type="text" runat="server" spe="建筑" lenghts="A4bz" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text20" type="text" runat="server" spe="建筑" lenghts="A4fzy" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text21" type="text" runat="server" spe="建筑" lenghts="A4fze" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text22" type="text" runat="server" spe="建筑" lenghts="A4fzs" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text23" type="text" runat="server" spe="建筑" lenghts="A4lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5">
                            结构
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text16" type="text" runat="server" spe="结构" lenghts="A0bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text17" type="text" runat="server" spe="结构" lenghts="A0fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text18" type="text" runat="server" spe="结构" lenghts="A0fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text24" type="text" runat="server" spe="结构" lenghts="A0fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text25" type="text" runat="server" spe="结构" lenghts="A0lb" value="0" />(张)
                        </td>
                        <td rowspan="5">
                            <input id="totalJg" type="text" runat="server" value="0" disabled="disabled" />(张)
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            结构
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text27" type="text" runat="server" spe="结构" lenghts="A1bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text28" type="text" runat="server" spe="结构" lenghts="A1fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text29" type="text" runat="server" spe="结构" lenghts="A1fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text30" type="text" runat="server" spe="结构" lenghts="A1fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text31" type="text" runat="server" spe="结构" lenghts="A1lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            结构
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text32" type="text" runat="server" spe="结构" lenghts="A2bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text33" type="text" runat="server" spe="结构" lenghts="A2fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text34" type="text" runat="server" spe="结构" lenghts="A2fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text35" type="text" runat="server" spe="结构" lenghts="A2fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text36" type="text" runat="server" spe="结构" lenghts="A2lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            结构
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text37" type="text" runat="server" spe="结构" lenghts="A3bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text38" type="text" runat="server" spe="结构" lenghts="A3fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text39" type="text" runat="server" spe="结构" lenghts="A3fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text40" type="text" runat="server" spe="结构" lenghts="A3fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text41" type="text" runat="server" spe="结构" lenghts="A3lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            结构
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text42" type="text" runat="server" spe="结构" lenghts="A4bz" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text43" type="text" runat="server" spe="结构" lenghts="A4fzy" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text44" type="text" runat="server" spe="结构" lenghts="A4fze" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text45" type="text" runat="server" spe="结构" lenghts="A4fzs" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text46" type="text" runat="server" spe="结构" lenghts="A4lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5">
                            给排水
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text73" type="text" runat="server" spe="给排水" lenghts="A0bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text74" type="text" runat="server" spe="给排水" lenghts="A0fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text75" type="text" runat="server" spe="给排水" lenghts="A0fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text76" type="text" runat="server" spe="给排水" lenghts="A0fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text77" type="text" runat="server" spe="给排水" lenghts="A0lb" value="0" />(张)
                        </td>
                        <td rowspan="5">
                            <input id="totalGps" type="text" runat="server" lenghts="zhlj" value="0" disabled="disabled" />(张)
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            给排水
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text79" type="text" runat="server" spe="给排水" lenghts="A1bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text80" type="text" runat="server" spe="给排水" lenghts="A1fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text81" type="text" runat="server" spe="给排水" lenghts="A1fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text82" type="text" runat="server" spe="给排水" lenghts="A1fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text83" type="text" runat="server" spe="给排水" lenghts="A1lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            给排水
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text84" type="text" runat="server" spe="给排水" lenghts="A2bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text85" type="text" runat="server" spe="给排水" lenghts="A2fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text86" type="text" runat="server" spe="给排水" lenghts="A2fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text87" type="text" runat="server" spe="给排水" lenghts="A2fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text88" type="text" runat="server" spe="给排水" lenghts="A2lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            给排水
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text89" type="text" runat="server" spe="给排水" lenghts="A3bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text90" type="text" runat="server" spe="给排水" lenghts="A3fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text91" type="text" runat="server" spe="给排水" lenghts="A3fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text92" type="text" runat="server" spe="给排水" lenghts="A3fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text93" type="text" runat="server" spe="给排水" lenghts="A3lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            给排水
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text94" type="text" runat="server" spe="给排水" lenghts="A4bz" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text95" type="text" runat="server" spe="给排水" lenghts="A4fzy" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text96" type="text" runat="server" spe="给排水" lenghts="A4fze" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text97" type="text" runat="server" spe="给排水" lenghts="A4fzs" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text98" type="text" runat="server" spe="给排水" lenghts="A4lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5">
                            暖通
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text99" type="text" runat="server" spe="暖通" lenghts="A0bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text100" type="text" runat="server" spe="暖通" lenghts="A0fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text101" type="text" runat="server" spe="暖通" lenghts="A0fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text102" type="text" runat="server" spe="暖通" lenghts="A0fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text103" type="text" runat="server" spe="暖通" lenghts="A0lb" value="0" />(张)
                        </td>
                        <td rowspan="5">
                            <input id="totalNt" type="text" runat="server" lenghts="zhlj" value="0" disabled="disabled" />(张)
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            暖通
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text105" type="text" runat="server" spe="暖通" lenghts="A1bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text106" type="text" runat="server" spe="暖通" lenghts="A1fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text107" type="text" runat="server" spe="暖通" lenghts="A1fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text108" type="text" runat="server" spe="暖通" lenghts="A1fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text109" type="text" runat="server" spe="暖通" lenghts="A1lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            暖通
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text110" type="text" runat="server" spe="暖通" lenghts="A2bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text111" type="text" runat="server" spe="暖通" lenghts="A2fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text112" type="text" runat="server" spe="暖通" lenghts="A2fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text113" type="text" runat="server" spe="暖通" lenghts="A2fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text114" type="text" runat="server" spe="暖通" lenghts="A2lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            暖通
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text115" type="text" runat="server" spe="暖通" lenghts="A3bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text116" type="text" runat="server" spe="暖通" lenghts="A3fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text117" type="text" runat="server" spe="暖通" lenghts="A3fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text118" type="text" runat="server" spe="暖通" lenghts="A3fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text119" type="text" runat="server" spe="暖通" lenghts="A3lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            暖通
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text120" type="text" runat="server" spe="暖通" lenghts="A4bz" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text121" type="text" runat="server" spe="暖通" lenghts="A4fzy" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text122" type="text" runat="server" spe="暖通" lenghts="A4fze" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text123" type="text" runat="server" spe="暖通" lenghts="A4fzs" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text124" type="text" runat="server" spe="暖通" lenghts="A4lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5">
                            电气
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text125" type="text" runat="server" spe="电气" lenghts="A0bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text126" type="text" runat="server" spe="电气" lenghts="A0fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text127" type="text" runat="server" spe="电气" lenghts="A0fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text128" type="text" runat="server" spe="电气" lenghts="A0fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text129" type="text" runat="server" spe="电气" lenghts="A0lb" value="0" />(张)
                        </td>
                        <td rowspan="5">
                            <input id="totalDq" type="text" runat="server" lenghts="zhlj" value="0" disabled="disabled" />(张)
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            电气
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text131" type="text" runat="server" spe="电气" lenghts="A1bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text132" type="text" runat="server" spe="电气" lenghts="A1fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text133" type="text" runat="server" spe="电气" lenghts="A1fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text134" type="text" runat="server" spe="电气" lenghts="A1fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text135" type="text" runat="server" spe="电气" lenghts="A1lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            电气
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text136" type="text" runat="server" spe="电气" lenghts="A2bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text137" type="text" runat="server" spe="电气" lenghts="A2fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text138" type="text" runat="server" spe="电气" lenghts="A2fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text139" type="text" runat="server" spe="电气" lenghts="A2fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text140" type="text" runat="server" spe="电气" lenghts="A2lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            电气
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text141" type="text" runat="server" spe="电气" lenghts="A3bz" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text142" type="text" runat="server" spe="电气" lenghts="A3fzy" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text143" type="text" runat="server" spe="电气" lenghts="A3fze" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text144" type="text" runat="server" spe="电气" lenghts="A3fzs" value="0" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text145" type="text" runat="server" spe="电气" lenghts="A3lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                            电气
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text146" type="text" runat="server" spe="电气" lenghts="A4bz" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text147" type="text" runat="server" spe="电气" lenghts="A4fzy" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text148" type="text" runat="server" spe="电气" lenghts="A4fze" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text149" type="text" runat="server" spe="电气" lenghts="A4fzs" value="0" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text150" type="text" runat="server" spe="电气" lenghts="A4lb" value="0" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5">
                            合计
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text47" type="text" runat="server" lenghts="TotalA0bz" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text48" type="text" runat="server" lenghts="TotalA0fzy" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text49" type="text" runat="server" lenghts="TotalA0fze" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text50" type="text" runat="server" lenghts="TotalA0fzs" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px;">
                            <span>A0</span>
                            <input id="Text51" type="text" runat="server" lenghts="TotalA0lb" value="0" disabled="disabled" />(张)
                        </td>
                        <td rowspan="5">
                            <input id="totalZh" type="text" runat="server" lenghts="Totalzhlj" value="0" disabled="disabled" />(张)
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text53" type="text" runat="server" lenghts="TotalA1bz" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text54" type="text" runat="server" lenghts="TotalA1fzy" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text55" type="text" runat="server" lenghts="TotalA1fze" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text56" type="text" runat="server" lenghts="TotalA1fzs" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A1</span>
                            <input id="Text57" type="text" runat="server" lenghts="TotalA1lb" value="0" disabled="disabled" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text58" type="text" runat="server" lenghts="TotalA2bz" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text59" type="text" runat="server" lenghts="TotalA2fzy" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text60" type="text" runat="server" lenghts="TotalA2fze" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text61" type="text" runat="server" lenghts="TotalA2fzs" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A2</span>
                            <input id="Text62" type="text" runat="server" lenghts="TotalA2lb" value="0" disabled="disabled" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text63" type="text" runat="server" lenghts="TotalA3bz" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text64" type="text" runat="server" lenghts="TotalA3fzy" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text65" type="text" runat="server" lenghts="TotalA3fze" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text66" type="text" runat="server" lenghts="TotalA3fzs" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px;">
                            <span>A3</span>
                            <input id="Text67" type="text" runat="server" lenghts="TotalA3lb" value="0" disabled="disabled" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                    <tr>
                        <td class="display">
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text68" type="text" runat="server" lenghts="TotalA4bz" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text69" type="text" runat="server" lenghts="TotalA4fzy" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text70" type="text" runat="server" lenghts="TotalA4fze" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text71" type="text" runat="server" lenghts="TotalA4fzs" value="0" disabled="disabled" />(张)
                        </td>
                        <td style="border-top: 0px;">
                            <span>A4</span>
                            <input id="Text72" type="text" runat="server" lenghts="TotalA4lb" value="0" disabled="disabled" />(张)
                        </td>
                        <td class="display">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 23px; background-color: #f0f0f0" colspan="4" align="center">
                <a href="###" id="btn_save">
                    <img src="/Images/buttons/btn_save2.gif" style="width: 64px; height: 23px; border: none;" /></a>
                &nbsp;<a href="ProjectDesignCardsList.aspx" target="_self"><img src="/Images/buttons/btn_back2.gif"
                    style="width: 65px; height: 23px; border: none;" /></a>
            </td>
        </tr>
    </table>
    <!--添加详细信息-->
    <div id="addsubPlotInfo" style="display: none;">
        <table class="cls_show_cst_jiben" style="width: 400px;" align="center" id="addsubDataTable">
            <tr>
                <td style="text-align: right;">
                    专业:
                </td>
                <td>
                    <select style="width: 120px;" id="drop_Specialty" class="cls_input_text_valid">
                        <option value="-1">---请选择---</option>
                        <option value="建筑">建筑</option>
                        <option value="结构">结构</option>
                        <option value="给排水">给排水</option>
                        <option value="暖通">暖通</option>
                        <option value="电气">电气</option>
                    </select>
                    <span id="spanSpecialty" class="valide">请选择专业!</span>
                </td>
            </tr>
            <tr>
                <td style="width: 20%; text-align: right;">
                    图幅:
                </td>
                <td>
                    <asp:DropDownList ID="drop_Mapsheet" runat="Server" Width="120px" AppendDataBoundItems="True"
                        CssClass="cls_input_text_valid">
                        <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                    </asp:DropDownList>
                    <span id="spanSubMapsheet" class="valide">请选择图幅!</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    加长倍数:
                </td>
                <td>
                    <asp:DropDownList ID="drop_LengthenType" runat="Server" Width="120px" AppendDataBoundItems="True"
                        CssClass="cls_input_text_valid">
                        <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                    </asp:DropDownList>
                    <span id="spanLengthenType" class="valide">请选择加长倍数!</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    数量:
                </td>
                <td>
                    <input id="txt_Count" type="text" name="number" class="cls_input_text_valid_w" style="width: 120px;" />
                    <span id="countsNull" class="valide">请填写出图卡数量!</span> <span id="countsNoInt" class="valide">
                        请填写整数!</span>
                </td>
            </tr>
        </table>
    </div>
    <div id="chooseUserMain" style="display: none;">
        <uc1:ChooseUser ID="ChooseUser1" runat="server" />
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <input id="HidProID" type="hidden" value="<%=ProjectSysNo %>" />
    </form>
</body>
</html>
