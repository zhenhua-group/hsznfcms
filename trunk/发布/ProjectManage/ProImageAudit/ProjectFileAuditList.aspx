﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectFileAuditList.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ProjectFileAuditList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.tablesort.css" rel="stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectPlan.css" rel="stylesheet" type="text/css" />
    <link href="/css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="/css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="/css/tipsy/tipsy.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="../../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/SendMessageCommon.js"></script>
    <script type="text/javascript" src="/js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="/js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="/js/jquery.tipsy.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/ProjectFileAuditList.js" type="text/javascript"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="style2">
                当前位置：[工程设计出图卡审批列表]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div">
                    <tr>
                        <td>
                            生产部门：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit" runat="server" Width="120px" AppendDataBoundItems="true"
                                AutoPostBack="True" OnSelectedIndexChanged="drp_unit_SelectedIndexChanged">
                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            年份：<asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" Width="95px">
                                <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            项目名称：<input id="txt_keyname" name="txt_keyname" type="text" runat="Server" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btn_Search" runat="server" ImageUrl="~/Images/buttons/btn_search.gif"
                                Width="64px" Height="22px" OnClick="btn_Search_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head">
            <tr>
                <td style="width: 4%;" align="center">
                    序号
                </td>
                <td style="width: 35%;" align="center">
                    项目名称
                </td>
                <td style="width: 8%" align="center">
                    工程号
                </td>
                <td style="width: 8%" align="center">
                    承接部门
                </td>
                <td style="width: 6%;" align="center">
                    执行设总
                </td>
                <td style="width: 6%;" align="center">
                    管理级别
                </td>
                <td style="width: 10%" align="center">
                    审核级别
                </td>
                <td style="width: 16%" align="center">
                    审批进度
                </td>
                <td style="width: 7%" align="center">
                    操作
                </td>
            </tr>
        </table>
        <asp:GridView ID="gvProjectPlot" runat="server" AutoGenerateColumns="False" ShowHeader="False"
            CssClass="gridView_comm" Width="100%" Font-Size="12px">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# (Container.DataItemIndex+1).ToString() %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="4%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="项目名称">
                    <ItemTemplate>
                        <a href="/ProjectManage/ShowProject.aspx?flag=list&pro_id=<%#Eval("pro_ID") %>" rel='<%# Eval("pro_ID") %>'
                            class="chk">
                            <div style="width: 31em;" class="cls_column" title='<%# Eval("Pro_name").ToString()%>'>
                                <img src="/Images/proj.png" style="width: 16px; height: 16px; margin-left: 1px; border: none;" />
                                <%# Eval("Pro_name").ToString()%></div>
                        </a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                </asp:TemplateField>
                <asp:BoundField DataField="Pro_number">
                    <ItemStyle HorizontalAlign="Center" Width="8%" />
                </asp:BoundField>
                <asp:BoundField DataField="Unit">
                    <ItemStyle HorizontalAlign="Center" Width="8%" />
                </asp:BoundField>
                <asp:BoundField DataField="PMName">
                    <ItemStyle HorizontalAlign="Center" Width="6%" />
                </asp:BoundField>
                <asp:BoundField DataField="Pro_level">
                    <ItemStyle HorizontalAlign="Center" Width="6%" />
                </asp:BoundField>
                <asp:BoundField DataField="AuditLevel">
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="审核状态">
                    <ItemTemplate>
                        <div class="progressbar" percent="<%#Eval("Percent") %> " id="auditLocusContainer"
                            title='<%#Eval("Percent") %>%' action="File" referencesysno="<%#Eval("AuditRecordSysNo") %>"
                            style="cursor: pointer; margin: 1px 1px 1px 1px;">
                        </div>
                    </ItemTemplate>
                    <ItemStyle Width="16%" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("ActionLink") %>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="7%" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
            CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
            OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
            PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
            SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
            PageSize="30" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <!--HiddenArea-->
    <input type="hidden" id="HiddenUserSysNo" value="<%=UserSysNo %>" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    </form>
</body>
</html>
