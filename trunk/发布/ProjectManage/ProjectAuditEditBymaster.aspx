﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectAuditEditBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectAuditEditBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../css/ProjStructStyleShow.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/Global.js"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../../js/Common/SendMessageCommon.js"></script>
    <script src="../js/ProjectMamage/ProjectAuditEdit.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectMamage/ProjectAuditShow.js"></script>
    <script type="text/javascript" src="../js/MessageComm.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        项目信息管理 <small>项目查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>项目信息管理<i class="fa fa-angle-right"> </i>项目管理<i class="fa fa-angle-right"> </i>项目查看</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-table"></i>项目信息</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">
                                项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_name" runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                                </td>
                                                <td style="width: 15%;">
                                                    合同关联:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    审核级别 :
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    建设单位:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    建设地点:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txtbuildAddress" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_scale" runat="server" Width="100px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    建筑类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_unit" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    结构样式:
                                                </td>
                                                <td style="width: 35%;">
                                                    <div class="struct_tree">
                                                        <asp:Literal ID="structType" runat="server"></asp:Literal></div>
                                                </td>
                                                <td style="width: 15%;">
                                                    建筑分类:
                                                </td>
                                                <td style="width: 35%;">
                                                    <div class="struct_tree">
                                                        <asp:Literal ID="buildStructType" runat="server"></asp:Literal></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    设计阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    项目来源:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    合同额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txtproAcount" runat="server" Width="100px"></asp:Label>万元
                                                </td>
                                                <td style="width: 15%;">
                                                    行业性质:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    甲方负责人:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_Aperson" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    电话:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_phone" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    执行设总:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_PMName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    电话:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_PMPhone" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目开始时间:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_startdate" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    项目完成时间:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_finishdate" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    其他参与部门:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目概况:
                                                </td>
                                                <td colspan="3">
                                                    <div style="height: auto;">
                                                        <span id="txt_sub" runat="Server"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目备注:
                                                </td>
                                                <td colspan="3">
                                                    <div style="height: auto;">
                                                        <span id="txt_remark" runat="Server"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    项目高清图:
                                                </td>
                                                <td colspan="3">
                                                    <div class="col-md-12" id="img_container" style="text-align: center">
                                                        <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                                            style="width: 250px; height: 150px;" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">
                                项目高清图
                            </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-advance table-hover" id="datas_att">
                                                <thead>
                                                    <tr id="att_row">
                                                        <td style="width: 40px;" align="center" id="att_id">
                                                            序号
                                                        </td>
                                                        <td style="width: 300px;" align="center" id="att_filename">
                                                            文件名称
                                                        </td>
                                                        <td style="width: 80px" align="center" id="att_filesize">
                                                            文件大小
                                                        </td>
                                                        <td style="width: 80px;" align="center" id="att_filetype">
                                                            文件类型
                                                        </td>
                                                        <td style="width: 120px;" align="center" id="att_uptime">
                                                            上传时间
                                                        </td>
                                                        <td style="width: 40px;" align="center" id="att_oper2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-table"></i>修改意见</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;" id="Div1">
                            <table style="width: 100%;" id="optiontab" class="table table-bordered table-hover">
                                <%=Creattable %>
                            </table>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-table"></i>评审意见</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;" id="auditHtml">
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
            <%--<div id="chooseUserMain" style="display: none;">
                <uc1:chooseuser id="ChooseUser1" runat="server" />
            </div>--%>
        </div>
    </div>
    <!--hiddenArea-->
    <input type="hidden" id="ProjectAuditSysNoHidden" value="<%=ProjectAuditSysNo %>" />
    <input type="hidden" id="hiddenProjectSysNo" value="<%=ProjectSysNo %>" />
    <input type="hidden" id="UserSysNoHidden" value="<%=UserSysNo %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
     <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" id="btn_CanceAudit" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
