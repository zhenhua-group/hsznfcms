﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplyEditProject.aspx.cs"
    Inherits="TG.Web.ProjectManage.ApplyEditProject" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="Stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../js/Common/SendMessageCommon.js"></script>
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    <script src="../js/ProjectMamage/ApplyEditProject.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectMamage/ApplyEditProject_jq.js"></script>
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="/css/cssie9.css" />
    <![endif]-->
    <style type="text/css">
        .style2
        {
            height: 33px;
            border-top: solid 1px #999;
            font-size: 12px;
            background: url(../images/bg_head.gif) repeat-x;
        }
        
        .ui-pg-div
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetFormWidth();
        });

    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="style2">
                &nbsp;当前位置：[项目审批]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div">
                    <tr>
                        <td>
                            生产部门：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit" runat="server" Width="120px" AppendDataBoundItems="true" >
                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            年份：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" Width="90px">
                                <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            项目名称：<input id="txt_keyname" name="txt_keyname" type="text" runat="Server" style="width: 120px" />
                        </td>
                        <td>
                        <input type="button" id="btn_Search" style="background:url(/Images/buttons/btn_search.gif) no-repeat; height:22px; width:64px; border:0; cursor:pointer;" value=" " />
                            <%--<asp:ImageButton ID="btn_Search" runat="server" ImageUrl="~/Images/buttons/btn_search.gif"
                                Width="60px" Height="20px" OnClick="btn_Search_Click" />--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%--<div class="cls_data">
        <table class="cls_content_head">
            <tr>
                <td style="width: 4%" align="center">
                    序号
                </td>
                <td style="width: 8%" align="center">
                    甲方负责人
                </td>
                <td style="width: 23%;" align="center">
                    项目名称
                </td>
                <td style="width: 6%" align="center">
                    建筑分类
                </td>
                <td style="width: 17%" align="center">
                    合同信息
                </td>
                <td style="width: 9%" align="center">
                    开始时间
                </td>
                <td style="width: 9%" align="center">
                    结束时间
                </td>
                <td style="width: 18%" align="center">
                    审批进度
                </td>
                <td style="width: 6%" align="center">
                    操作
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_project" runat="server" CellPadding="0" DataKeyNames="pro_ID"
            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" ShowHeader="False"
            Font-Size="12px" RowStyle-Height="22px" Width="100%" OnRowDataBound="gv_project_RowDataBound"
            CssClass="gridView_comm" Border="0">
            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# (Container.DataItemIndex+1).ToString() %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="4%" />
                </asp:TemplateField>
                <asp:BoundField DataField="chgjia" HeaderText="甲方负责人" SortExpression="cpr_No" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="项目名称">
                    <ItemTemplate>
                        <a href="/ProjectManage/ShowProject.aspx?flag=list&pro_id=<%#Eval("pro_ID") %>" rel='<%# Eval("pro_ID") %>'
                            class="chk">
                            <div class="cls_column" style="width: 20em;" title='<%# Eval("pro_name") %>'>
                                <img src="../Images/proj.png" style="width: 16px; height: 16px; margin-left: 1px;
                                    border: none;" />
                                <%# Eval("pro_name").ToString()%>
                            </div>
                        </a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="23%" />
                </asp:TemplateField>
                <asp:BoundField DataField="BuildType" HeaderText="项目类型">
                    <ItemStyle Width="6%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="合同信息">
                    <ItemTemplate>
                        <a href='../Coperation/cpr_ShowCopration.aspx?flag=projlist2&cprid=<%# Eval("CoperationSysNo") %>'
                            cprid='<%# Eval("CoperationSysNo") %>'>
                            <div style="width: 15em" class="cls_column" title='<%# Eval("Project_reletive") %>'>
                                <img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px; border: none;">
                                <%# Eval("Project_reletive").ToString()%>
                            </div>
                        </a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="17%" />
                </asp:TemplateField>
                <asp:BoundField DataField="pro_startTime" HeaderText="项目开始时间" DataFormatString="{0:yyyy-MM-dd}">
                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="pro_finishTime" HeaderText="项目结束时间" DataFormatString="{0:yyyy-MM-dd}">
                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="审批进度">
                    <ItemTemplate>
                        <asp:HiddenField ID="hflevel" runat="server" Value='<%# Eval("pro_level") %>' />
                        <asp:HiddenField ID="HiddenFieldPercent" runat="server" />
                        <asp:HiddenField ID="HiddenFieldAuditStatus" runat="server" Value='<%#Eval("AuditStatus") %>' />
                        <div class="progressbar" id="auditLocusContainer" action="EP" referencesysno="<%# Eval("AuditSysNo") %>"
                            style="cursor: pointer;">
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="18%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="申请修改">
                    <ItemTemplate>
                        <a href="###" class="cls_startapp">申请修改</a>
                        <asp:HiddenField ID="hid_proid" runat="server" Value='<%# Eval("pro_ID") %>' />
                        <asp:HiddenField ID="hid_proname" runat="server" Value='<%# Eval("pro_name") %>' />
                        <asp:HiddenField ID="hid_AuditSysNo" runat="server" Value='<%# Eval("AuditSysNo") %>' />
                        <a href="###" class="applying" auditsysno='<%# Eval("pro_ID") %>'>审批中<img src="/Images/Expansion.gif"
                            style="border: none;"></a>
                        <asp:HiddenField ID="hid_exsit" runat="server" Value="0" />
                    </ItemTemplate>
                    <ItemStyle Width="6%" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
            CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
            OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
            PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
            SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
            PageSize="30" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
    </div>--%>
    <table id="jqGrid">
    </table>
    <div id="gridpager">
    </div>
    <div id="nodata" class="norecords"></div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    </form>
</body>
</html>
