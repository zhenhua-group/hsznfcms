﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectFixing.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectFixing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="../js/ProjectMamage/ShowProjBymaster.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        项目信息管理 <small>项目修复</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
     <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">项目管理</a><i
        class="fa fa-angle-right"> </i><a href="#">项目修复</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目信息</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">
                        项目信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 120px;">
                                            项目名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_name" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                        </td>
                                        <td style="width: 120px;">
                                            合同关联:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            管理级别:
                                        </td>
                                        <td >
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            审核级别 :
                                        </td>
                                        <td >
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            建设单位:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            建设地点:
                                        </td>
                                        <td >
                                            <asp:Label ID="txtbuildAddress" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            建设规模:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_scale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            建筑类别:
                                        </td>
                                        <td >
                                            <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            承接部门:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_unit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            结构样式:
                                        </td>
                                        <td >
                                            <div class="struct_tree">
                                                <asp:Literal ID="structType" runat="server"></asp:Literal></div>
                                        </td>
                                        <td >
                                            建筑分类:
                                        </td>
                                        <td >
                                            <div class="struct_tree">
                                                <asp:Literal ID="buildStructType" runat="server"></asp:Literal></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            设计阶段:
                                        </td>
                                        <td >
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            项目来源:
                                        </td>
                                        <td >
                                            <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            合同额:
                                        </td>
                                        <td >
                                            <asp:Label ID="txtproAcount" runat="server" Width="100px"></asp:Label>万元
                                        </td>
                                        <td >
                                            行业性质:
                                        </td>
                                        <td >
                                            <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            甲方负责人:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_Aperson" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            电话:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_phone" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            执行设总:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_PMName" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            电话:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_PMPhone" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            项目开始时间:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_startdate" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            项目完成时间:
                                        </td>
                                        <td >
                                            <asp:Label ID="txt_finishdate" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            项目概况:
                                        </td>
                                        <td colspan="3">
                                            <div style="height: auto;">
                                                <span id="txt_sub" runat="Server"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            项目备注:
                                        </td>
                                        <td colspan="3">
                                            <div style="height: auto;">
                                                <span id="txt_remark" runat="Server"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            项目高清图:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12" id="img_container" style="text-align: center">
                                                <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                                    style="width: 250px; height: 150px;" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <h4 class="form-section">
                        项目高清图
                    </h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-advance table-hover" align="center" style="width: 98%;" id="datas_att">
                                        <thead>
                                            <tr id="att_row">
                                                <td style="width: 40px;" align="center" id="att_id">
                                                    序号
                                                </td>
                                                <td style="width: 300px;" align="center" id="att_filename">
                                                    文件名称
                                                </td>
                                                <td style="width: 80px" align="center" id="att_filesize">
                                                    文件大小
                                                </td>
                                                <td style="width: 80px;" align="center" id="att_filetype">
                                                    文件类型
                                                </td>
                                                <td style="width: 120px;" align="center" id="att_uptime">
                                                    上传时间
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 40px;" align="center" id="att_oper2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <asp:Button ID="Btn_Fixed" runat="server" CssClass="btn green" Text="开始修复" OnClick="Btn_Fixed_Click" />
                            <button type="button" class="btn default" onclick="javascript:history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
