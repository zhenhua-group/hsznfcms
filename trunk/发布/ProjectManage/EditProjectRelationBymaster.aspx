﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="EditProjectRelationBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.EditProjectRelationBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjStructStyleShow.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectMamage/EditProjectRelationBymaster.js"></script>
    <script src="/js/Common/ShowDivDialog.js" type="text/javascript"></script>
    <script type="text/javascript">
        var projid = <%= ProjectID %>;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目关联合同</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>项目管理<i class="fa fa-angle-right"> </i>项目关联合同</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>项目信息
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                            <tr>
                                                <td colspan="4" style="width: 15%;">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" id="txt_coperation" runat="server" class="form-control" /><asp:HiddenField
                                                                ID="hid_cprid" runat="server" Value="" />
                                                        </div>
                                                        <label class="control-label col-md-2">
                                                            <button type="button" class="btn purple" id="btn_select" href="#HTGL" data-toggle="modal">
                                                                选择合同
                                                            </button>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_name" runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                                </td>
                                                <td style="width: 15%;">合同关联:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">审核级别 :
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">建设单位:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">建设地点:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txtbuildAddress" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_scale" runat="server" Width="100px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">建筑类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_unit" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">结构样式:
                                                </td>
                                                <td style="width: 35%;">
                                                    <div class="struct_tree">
                                                        <asp:Literal ID="structType" runat="server"></asp:Literal>
                                                    </div>
                                                </td>
                                                <td style="width: 15%;">建筑分类:
                                                </td>
                                                <td style="width: 35%;">
                                                    <div class="struct_tree">
                                                        <asp:Literal ID="buildStructType" runat="server"></asp:Literal>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">设计阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">项目来源:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">合同额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txtproAcount" runat="server" Width="100px"></asp:Label>万元
                                                </td>
                                                <td style="width: 15%;">行业性质:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">甲方负责人:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_Aperson" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">电话:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_phone" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">执行设总:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_PMName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">电话:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_PMPhone" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目开始时间:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_startdate" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">项目完成时间:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_finishdate" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">其他参与部门:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目概况:
                                                </td>
                                                <td colspan="3">
                                                    <div style="height: auto;">
                                                        <span id="txt_sub" runat="Server"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目备注:
                                                </td>
                                                <td colspan="3">
                                                    <div style="height: auto;">
                                                        <span id="txt_remark" runat="Server"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目高清图:
                                                </td>
                                                <td colspan="3">
                                                    <div class="col-md-12" id="img_container" style="text-align: center">
                                                        <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                                            style="width: 250px; height: 150px;" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-section">项目高清图
                            </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-advance table-hover" id="datas_att">
                                                <thead>
                                                    <tr id="att_row">
                                                        <td style="width: 40px;" align="center" id="att_id">序号
                                                        </td>
                                                        <td style="width: 300px;" align="center" id="att_filename">文件名称
                                                        </td>
                                                        <td style="width: 80px" align="center" id="att_filesize">文件大小
                                                        </td>
                                                        <td style="width: 80px;" align="center" id="att_filetype">文件类型
                                                        </td>
                                                        <td style="width: 120px;" align="center" id="att_uptime">上传时间
                                                        </td>
                                                        <td style="width: 40px;" align="center" id="att_oper2">&nbsp;
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="submit" class="btn green" id="btn_Save">
                                保存</button>
                            <button type="button" class="btn default" onclick="javascript:history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <!-- 4关联合同 -->
    <div id="HTGL" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">合同关联</h4>
        </div>
        <div class="modal-body">
            <div id="cprReletiveDiv">
                <table class="table table-bordered table-hover" style="width: 700px; text-align: center;">
                    <tr>
                        <td style="width: 100px; text-align: center;">生产部门:
                        </td>
                        <td>
                            <select id="select_releCprUnit" class="form-control input-sm" style="width: 220px;">
                                <option value="-1" selected="selected">-----全院部门-----</option>
                            </select>
                        </td>
                        <td style="width: 100px; text-align: center;">合同名称:
                        </td>
                        <td>
                            <input type="text" id="TxtCprName" value="" class="form-control input-sm" maxlength="25"
                                style="width: 200px;" />
                        </td>
                        <td align="center">
                            <a id="btn_SearchByCprName" class="btn
        blue btn-sm"><i class="fa fa-search"></i></a>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="cprReletiveTable" class="table table-bordered table-hover" style="width: 700px;">
                    <tr class="trBackColor">
                        <td align="left">合同名称
                        </td>
                        <td align="center">签订日期
                        </td>
                        <td align="center">操作
                        </td>
                    </tr>
                </table>
                <!--合同级别绑定-->
                <input type="hidden" id="cprLevel" value="" />
                <div id="cprReletivePage" style="text-align: center">
                    总<label id="reletiveCpr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                    第<label id="reletiveCpr_nowPageIndex">0</label>/<label id="reletiveCpr_allPageCount">0</label>页&nbsp;&nbsp;
                    <span id="reletiveCpr_firstPage">首页</span>&nbsp; <span id="reletiveCpr_prevPage">&lt;&lt;</span>&nbsp;
                    <span id="reletiveCpr_nextPage">&gt;&gt;</span>&nbsp; <span id="reletiveCpr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp; 跳至<input type="text" id="reletiveCpr_gotoPageNum"
                        maxlength="4" style="height: 3; width: 20px; border-style: none none solid none;" />
                    页&nbsp;&nbsp; <span id="reletiveCpr_gotoPage">GO</span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
