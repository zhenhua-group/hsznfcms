﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ShowSingleCoperationForReportBymaster.aspx.cs" Inherits="TG.Web.Coperation.ShowSingleCoperationForReportBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        合同信息管理 <small>合同报备信息查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>合同信息管理<i class="fa fa-angle-right"> </i>合同报备管理<i class="fa fa-angle-right"> </i>
        合同报备信息查看</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="tab-content">
            <div class="tab-pane  active">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>客户信息</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form" style="display: block;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        客户编号:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txtCst_No" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        客户简称:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txt_JC" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        客户名称:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txtCst_Name" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        公司地址:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txtCpy_Address" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        邮政编码:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txtCode" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        联系人:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txtLinkman" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        公司电话:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txtCpy_Phone" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">
                                        传真号:</label>
                                    <div class="col-md-9">
                                        <asp:Label ID="txt_Fax" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane  active">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>合同信息</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form" style="display: block;">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        合同名称:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_cprName" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        合同分类:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="ddcpr_Type" runat="Server">
                                        </asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        合同类型:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_cprType" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        项目地点:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_cprAddress" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        建筑类别:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_buildType" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        建设规模:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_buildArea" runat="server"></asp:Label>㎡ <span class="help-block">
                                            <font><font class=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        结构形式:</label>
                                    <div class="col-md-8">
                                        <div class="struct_tree">
                                            <asp:Literal ID="lbl_StructType" runat="server"></asp:Literal></div>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        建筑分类:</label>
                                    <div class="col-md-8">
                                        <div class="struct_tree">
                                            <asp:Literal ID="lbl_BuildStructType" runat="server"></asp:Literal></div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        层数:</label>
                                    <div class="col-md-8">
                                        地上：<asp:Label ID="txt_upfloor" runat="server" Text="0"></asp:Label>
                                        层 地下：<asp:Label ID="txt_downfloor" runat="server" Text="0"></asp:Label>
                                        层 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        工程负责人:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_proFuze" runat="server" Width="120px"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        电话:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_fzphone" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        承接部门:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_cjbm" runat="server" Width="120px"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        甲方负责人:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txtFParty" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        电话:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txt_jiafphone" runat="server" Width="120px"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        工程地点:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="ddProjectPosition" runat="Server" Width="60px">
                                        </asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        合同额:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txtcpr_Account" runat="server"></asp:Label>
                                        万元 
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        投资额:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txtInvestAccount" runat="server"></asp:Label>
                                        万元 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        行业性质:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="ddProfessionType" runat="Server" Width="60px">
                                        </asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        实际合同额:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txtcpr_Account0" runat="server"></asp:Label>
                                        万元 
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        实际投资额:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="txtInvestAccount0" runat="server"></asp:Label>
                                        万元 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        工程来源:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="ddSourceWay" runat="Server" Width="60px">
                                        </asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        是否立项:</label>
                                    <div class="col-md-8">
                                        <asp:Label ID="drp_proapproval" runat="server"></asp:Label>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-1">
                                        合同备注:</label>
                                    <div class="col-md-11">
                                        <textarea class="form-control" id="txtcpr_Remark" rows="3" runat="server" placeholder="Enter a message ..."
                                            readonly>Gnome &amp; Growl type non-blocking notifications</textarea>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-1">
                                        甲方沟通记录:</label>
                                    <div class="col-md-11">
                                        <textarea class="form-control" id="txtcpr_recorpt" rows="3" runat="server" placeholder="Enter a message ..."
                                            readonly>Gnome &amp; Growl type non-blocking notifications</textarea>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green" id="btn_output">
                            导出</button>
                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                            返回</button>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_flag" runat="server" />
</asp:Content>
