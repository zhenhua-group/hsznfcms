﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_CoperationAuditEditBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_CoperationAuditEditBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="Stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
        .auditTextBox {
            width: 200px;
        }
    </style>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="/js/Coperation/cpr_CoperationAuditEdit.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/MessageComm.js"></script>
    <script type="text/javascript">
        $(function () {
            //隔行变色
            $(".cls_show_cst_jiben tr:odd").attr("style", "background-color:#FFF;");
            //点击否显示文本框
            //点击否显示文本框
            $(".no").click(function () {
                $(this).parent().parent().parent().parent().next(":text").show();
            });
            $(".yes").click(function () {
                $(this).parent().parent().parent().parent().nextAll(":text").hide();
            });
            //            $("#btnquit").live("click", function () {
            //                $("#showEdit").hide();
            //            })
        });
    </script>
    <style type="text/css">
        /* 表格基本样式*/
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }

            .cls_show_cst_jiben > tbody > tr > td {
                border: solid 1px #CCC;
                height: 18px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页 <small>合同修改审批</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同管理</a><i class="fa fa-angle-right"> </i><a>合同修改审批</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同评审记录
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td style="width: 100px;">建设单位:
                                        </td>
                                        <td>
                                            <asp:Label ID="ConstructionCompany" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 100px;">承接部门:
                                        </td>
                                        <td>
                                            <asp:Label ID="UndertakeDepartment" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="ProjectName" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>建设规模:
                                        </td>
                                        <td>
                                            <asp:Label ID="BuildScope" runat="server" Text=""></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="BuildAddress" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>投资额:
                                        </td>
                                        <td>
                                            <asp:Label ID="Investment" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:Label ID="buildType" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>层数:
                                        </td>
                                        <td>
                                            <asp:Label ID="floorNum" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="CoperationAmount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>行业性质:
                                        </td>
                                        <td colspan="3">
                                            <input type="radio" name="BuildNature" value="公建" disabled="">公建
                                            <input type="radio" name="BuildNature" value="房地产" disabled="">房地产
                                            <input type="radio" name="BuildNature" value="市政" disabled="">市政
                                            <input type="radio" name="BuildNature" value="医院" disabled="">医院
                                            <input type="radio" name="BuildNature" value="电力" disabled="">电力
                                            <input type="radio" name="BuildNature" value="通信" disabled="">通信
                                            <input type="radio" name="BuildNature" value="银行" disabled="">银行
                                            <input type="radio" name="BuildNature" value="学校" disabled="">学校
                                            <input type="radio" name="BuildNature" value="涉外" disabled="">涉外
                                            <input type="hidden" id="HiddenBuildNature" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同分类:
                                        </td>
                                        <td colspan="3">
                                            <asp:Repeater ID="RepeaterCoperationBuildType" runat="server">
                                                <ItemTemplate>
                                                    <input type="radio" name="BuildType" value="<%#Eval("dic_Name") %>" disabled="">
                                                    <%#Eval("dic_Name") %>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <input type="hidden" id="HiddenCoperationType" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table style="width: 100%; font-size: 12px; line-height: 25px;" id="optiontab" class="cls_show_cst_jiben">
                                <tr>
                                    <td colspan="4" align="center">
                                        <b>申请修改意见</b>
                                    </td>
                                </tr>
                                <%=Creattable %>
                            </table>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table style="width: 100%; height: 30px; font-weight: bold;" class="cls_show_cst_jiben">
                                <tr>
                                    <td style="width: 10%; text-align: center;">评审部门
                                    </td>
                                    <td style="width: 70%; text-align: center;">评审意见
                                    </td>
                                    <td style="width: 20%; text-align: center;">评审人/日期
                                    </td>
                                </tr>
                            </table>
                            <div id="TableContainer">
                                <!--所长Table-->
                                <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="undertakeDepartment"
                                    audittable="auditTable">
                                    <tr>
                                        <td style="width: 10%; text-align: center;" rowspan="6">所长
                                        </td>
                                        <td style="width: 70%; text-align: center;" rowspan="6">
                                          <%--  <input type="text" style="width: 99%; border: 0; height: 30px;" />--%>
                                            <textarea id="UndertakeProposal" style="height: 68px; width: 800px; border: solid 1px #CCC;"></textarea>
                                        </td>

                                        <td style="width: 20%; text-align: center;" rowspan="6">
                                            <span id="AuditUser"></span>
                                        </td>
                                    </tr>
                                </table>
                                <!--经营处Table-->
                                <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                    id="OperateDepartment" audittable="auditTable">
                                    <tr>
                                        <td style="width: 10%; text-align: center;" rowspan="8">经营处
                                        </td>
                                        <td style="width: 70%; text-align: center;" rowspan="8">
                                             <textarea id="OperateDepartmentProposal" style="height: 68px; width: 800px; border: solid 1px #CCC;"></textarea>
                                        <%--    <input type="text" style="width: 99%; border: 0; height: 30px;" id="" />--%>
                                        </td>
                                        <td style="width: 20%; text-align: center;" rowspan="6">
                                            <span id="AuditUser"></span>
                                        </td>
                                    </tr>

                                </table>
                                <!--没有权限审核-->
                                <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                                    id="NoPowerTable">
                                    <tr>
                                        <td style="width: 100%; text-align: center; height: 100px; line-height: 100px;">您没有权限审核该项
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--保存按钮-->
                            <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table2">
                                <tr>
                                    <td style="width: 100%; text-align: center; height: 40px;">
                                        <%--<a href="javascript:void(0)" id="exprot" style="display: none;">
                            <img src="/Images/buttons/output.gif" alt="导出" style="border: none; width: 64px;
                                height: 22px;" /></a>--%>
                                        <input type="button" id="btnApproval" name="controlBtn" class="btn btn-sm blue" data-toggle="modal"
                                            href="#AuditUserList" value="通过" />
                                        <input type="button" id="btnRefuse" name="controlBtn" class="btn btn-sm blue" value="不通过" />
                                        <%--    <input type="button" id="showedit" class="cls_btn_comm_w" style="display: none;"
                            value="修改项目" />--%>
                                        <input type="button" id="Button1" class="btn btn-sm blue" value="返回" onclick="javascript: history.back();" />
                                        <input type="button" value="重新申请" id="FallBackCoperaion" style="display: none;" class="btn btn-sm blue" />
                                        <%--  <a href="javascript:history.back();" id="back" style="display: none">
                            <img src="/Images/buttons/btn_back2.gif" alt="返回" style="border: none; display: inline;
                                position: relative; left: 0; top: 0px;" /></a>--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenSysMsgSysNo" value="0" />
    <input type="hidden" id="AuditRecordStatus" value="<%=coperationAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=coperationAuditRecordEntity.SysNo %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenUndertakeProposal" value="<%=coperationAuditRecordEntity.UndertakeProposal %>" />
    <input type="hidden" id="HiddenOperateDepartmentProposal" value="<%=coperationAuditRecordEntity.OperateDepartmentProposal %>" />
    <input type="hidden" id="HiddenLegalAdviserProposal" value="<%=coperationAuditRecordEntity.LegalAdviserProposal %>" />
    <input type="hidden" id="HiddenTechnologyDepartmentProposal" value="<%=coperationAuditRecordEntity.TechnologyDepartmentProposal %>" />
    <input type="hidden" id="HiddenGeneralManagerProposal" value="<%=coperationAuditRecordEntity.GeneralManagerProposal  %>" />
    <input type="hidden" id="HiddenBossProposal" value="<%=coperationAuditRecordEntity.BossProposal   %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=coperationAuditRecordEntity.AuditUserString %>" />
    <input type="hidden" id="HiddenAuditDate" value="<%=coperationAuditRecordEntity.AuditDate %>" />
    <input type="hidden" id="HiddenCoperationSysNo" value="<%=CoperationSysNo %>" />
    <input type="hidden" id="HiddenManageLevel" value="<%=coperationAuditRecordEntity.ManageLevel %>" />
    <input type="hidden" id="HiddenNeedLegalAdviser" value="<%=coperationAuditRecordEntity.NeedLegalAdviser %>" />
    <input type="hidden" id="hiddenProjectSysNo" value="<%=coperationAuditRecordEntity.CoperationSysNo %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--PopArea-->
    <!--选择消息接收着-->
    <div id="AuditUserList" class="modal fade yellow" tabindex="-1" data-width="450"
        aria-hidden="true" style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
