﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_ChargeCountListBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_ChargeCountListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
                var yearValue = $(this).val();
                if (yearValue == "-1") {
                    $("#ctl00_ContentPlaceHolder1_liTwo").hide();
                    $("#ctl00_ContentPlaceHolder1_liThree").hide();
                }
                else {
                    $("#ctl00_ContentPlaceHolder1_liTwo").show();
                    $("#ctl00_ContentPlaceHolder1_liThree").show();
                }
            });
            var allcount = $("tr", $("#ctl00_ContentPlaceHolder1_GridView1")).find("TD").eq(1).text();
            var allclearcount = $("tr", $("#ctl00_ContentPlaceHolder1_GridView1")).find("TD").eq(3).text();
            var minuscount = parseInt(allcount) - parseInt(allclearcount);
            $("tr", $("#ctl00_ContentPlaceHolder1_GridView1")).find("TD").eq(1).text(minuscount);
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>合同信息管理<i class="fa fa-angle-right"> </i>收费管理<i class="fa fa-angle-right"> </i>合同统计</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询合同综合信息
                    </div>
                    
                </div>
                <div class="portlet-body">

                                <div class="table-responsive">
                                    <table class="">
                                        <tr>
                                            <td>生产部门:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server"
                                                    AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>年份:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>收费时间:
                                            </td>
                                            <td>
                                                <input type="text" name="txt_date2" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />至<input
                                                        type="text" name="txt_date1" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                        class="Wdate" runat="Server" size="20" style="width: 100px; height: 22px;" />
                                            </td>
                                            <td>
                                                <asp:Button runat="Server" CssClass="btn blue" Text="查询" OnClick="btn_ok_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                           
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询合同综合信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                      <asp:Button Text="导出Excel" runat="server" CssClass="btn red btn-sm" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1_1" data-toggle="tab">总额</a></li>
                                    <li class="" id="liTwo" runat="server"><a href="#tab_1_2" data-toggle="tab">季度</a></li>
                                    <li class="" id="liThree" runat="server"><a href="#tab_1_3" data-toggle="tab">月份</a></li>
                                    <li class=""><a href="#tab_1_4" data-toggle="tab">类型</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab_1_1">
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-grid"
                                                    AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="true">
                                                    <Columns>
                                                        <asp:BoundField DataField="CprCount" HeaderText="合同总数" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CprChgCount" HeaderText="已付款合同数" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CprNotChgCount" HeaderText="未付款合同数" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CprAllChargeCount" HeaderText="已结算合同数" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CprAllCount" HeaderText="签订合同总额" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CprChargeCount" HeaderText="已收款" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_2">
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView2" runat="server" CssClass="table table-bordered table-grid"
                                                    AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="true" OnRowDataBound="GridView2_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="季度" ItemStyle-CssClass="cls_Column">
                                                            <ItemStyle Width="140px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="A" HeaderText="第一季度" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="B" HeaderText="第二季度" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="C" HeaderText="第三季度" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="D" HeaderText="第四季度" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="120px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_3">
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView3" runat="server" CssClass="table table-bordered table-grid"
                                                    AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="true" OnRowDataBound="GridView2_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="月份" ItemStyle-CssClass="cls_Column">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="A" HeaderText="一月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="B" HeaderText="二月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="C" HeaderText="三月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="D" HeaderText="四月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="E" HeaderText="五月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="F" HeaderText="六月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="G" HeaderText="七月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="H" HeaderText="八月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="I" HeaderText="九月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="J" HeaderText="十月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="K" HeaderText="十一月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="L" HeaderText="十二月" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="80px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_1_4">
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView4" runat="server" CssClass="table table-bordered table-grid"
                                                    AutoGenerateColumns="False" EnableModelValidation="True" ShowHeader="true" OnRowDataBound="GridView2_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="合同类型" ItemStyle-CssClass="cls_Column">
                                                            <ItemStyle Width="140px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="A" HeaderText="民用建筑合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="B" HeaderText="工业建筑合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="C" HeaderText="工程勘察合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="D" HeaderText="工程监理合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="E" HeaderText="工程施工合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="F" HeaderText="工程咨询合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="G" HeaderText="工程总承包合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="H" HeaderText="工程代建合同" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="I" HeaderText="项目协议" ItemStyle-CssClass="cls_HoverTd">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
