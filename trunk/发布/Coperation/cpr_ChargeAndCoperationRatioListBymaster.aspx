﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_ChargeAndCoperationRatioListBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_ChargeAndCoperationRatioListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cls_chart {
            font-size: 9pt;
            width: 100%;
        }

        .cls_nav {
            width: 70px;
        }

            .cls_nav a {
                text-decoration: none;
            }

        .show_projectNumber {
            width: 860px;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .show_projectNumber td {
                border: solid 1px #CCC;
                font-size: 9pt;
                font-family: "微软雅黑";
                height: 20px;
            }

        .cls_show_cst_jiben_2 {
            border-collapse: collapse;
            border: solid 0px black;
            font-size: 9pt;
        }

        #ctl00_ContentPlaceHolder1_labTime {
            float: right;
            font-size: 12px;
            font-family: 微软雅黑;
        }

        #ctl00_ContentPlaceHolder1_labDanW {
            float: right;
            margin-right: 10px;
            font-size: 12px;
            font-family: 微软雅黑;
        }
    </style>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/FunctionChart/FusionCharts.js"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/Coperation/ChargeAndCoperationList.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>收费与合同环比表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>生产经营统计报表</a><i class="fa fa-angle-right">
    </i><a>收费与合同环比表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询收费与合同环比
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_report" runat="server" Text="导出" CssClass="btn red btn-sm"
                            OnClick="btn_report_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control " runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control " runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem>2010</asp:ListItem>
                                    <asp:ListItem>2011</asp:ListItem>
                                    <asp:ListItem>2012</asp:ListItem>
                                    <asp:ListItem>2013</asp:ListItem>
                                    <asp:ListItem>2014</asp:ListItem>
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>对比月份1:</td>
                            <td>
                                <asp:DropDownList ID="drp_month" runat="server" CssClass="form-control ">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>11</asp:ListItem>
                                    <asp:ListItem>12</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>对比月份2:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_month2" runat="server" CssClass="form-control ">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>11</asp:ListItem>
                                    <asp:ListItem>12</asp:ListItem>
                                </asp:DropDownList></td>
                            <td><input type="submit" class="btn blue" value="查询" id="btn_ok" /></td>
                            <Td><asp:HiddenField ID="hidnowmonth" runat="server" /></Td>
                        </tr>
                    </table>
              
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>收费与合同环比表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1_1" data-toggle="tab">数据列表</a></li>
                                    <li class=""><a href="#tab_1_2" data-toggle="tab">图形列表(收费)</a></li>
                                    <li class=""><a href="#tab_1_3" data-toggle="tab">图形列表(合同额)</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="tab_1_1" style="width: 100%">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <%-- <table style="width: 100%;" class="cls_show_cst_jiben">
                                                    <tr>
                                                        <td style="border: none;">--%>
                                                <div class="cls_Container_Report">
                                                    <div class="cls_Container_Tip">
                                                        <label id="title">
                                                        </label>
                                                        年(<label class="month"></label>-<label class="nowmonth"></label>月)【收费与合同额】环比表
                                                    </div>
                                                    <div style="margin: 0 auto; width: 99%;">
                                                        <asp:Label ID="labTime" runat="server" Font-Size="7.5pt"></asp:Label>
                                                        <asp:Label ID="labDanW" runat="server" Font-Size="7.5pt">单位：万元</asp:Label>
                                                        <table id="mytab" runat="server" class="table table-bordered" style="width: 99%;">
                                                            <tr>
                                                                <td rowspan="3" style="width: 14%">单 位
                                                                </td>
                                                                <td colspan="3">已收费
                                                                </td>
                                                                <td colspan="3">已签订合同额
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <label class="year">
                                                                    </label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <label class="year">
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 12%">
                                                                    <label class="month">
                                                                    </label>
                                                                    月
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <label class="nowmonth">
                                                                    </label>
                                                                    月
                                                                </td>
                                                                <td style="width: 12%">环比增减
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <label class="month">
                                                                    </label>
                                                                    月
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <label class="nowmonth">
                                                                    </label>
                                                                    月
                                                                </td>
                                                                <td style="width: 12%">环比增减
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table id="AddTable" class="table table-bordered table-data" style="width: 99%;">
                                                        </table>
                                                    </div>
                                                </div>
                                                <%--  </td>
                                                    </tr>
                                                </table>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade " id="tab_1_2" style="width: 100%">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="chart_2" class="cls_chart">
                                                    <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade " id="tab_1_3" style="width: 100%">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="chart_3" class="cls_chart">
                                                    <asp:Literal ID="Literal5" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" runat="Server" id="userShortName" value="" />
    <input type="hidden" runat="Server" id="previewPower" value="" />
    <input type="hidden" runat="Server" id="userSysNum" value="" />
    <input type="hidden" runat="Server" id="userUnitNum" value="" />
</asp:Content>
