﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectIndustryRpt.aspx.cs" Inherits="TG.Web.Coperation.ProjectIndustryRpt" %>
<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>


    <style type="text/css">
        #tablecontent th {
            text-align: center !important;
            vertical-align: middle !important;
        }

        #tablecontent tbody > tr:hover {
            background-color: #e5e5e5 !important;
            font-weight: 400;
        }

        #tablecontent tbody {
            font-size: 10pt;
            font-weight: 300;
            font-family:"微软雅黑";
        }
        .auto-style1 {
            height: 29px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <h3 class="page-title">领导驾驶舱 <small>项目分类同比统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>项目分类同比统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>统计条件
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table-responsive">
                                    <tr>
                                        <td>生产部门：</td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="drp_unit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全体部门-------" />
                                        </td>
                                        <td>统计：
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpYear" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>年
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpMonth" runat="server">

                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>月
                                        </td>
                                        <td>对比:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpYear2" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>年
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpMonth2" runat="server">

                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>月
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" Text="查询" CssClass="btn blue btn-default" OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>统计数据
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btnExportExcel" Text="导出Excel" CssClass="btn red btn-sm " runat="server" OnClick="btnExportExcel_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#list" data-toggle="tab">统计表</a>
                        </li>
                        <li>
                            <a href="#charts" data-toggle="tab">统计图表</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="list">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="tablecontent">
                                    <thead>
                                        <tr>
                                            <th colspan="16"><span style="font-size: 12pt; height: 30px;"><%= CountTip %></span></th>
                                        </tr>
                                        <tr>
                                            <th colspan="16"><span style="float: right;color:red;">单位:万元</span></th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2" nowarp>生产部门</th>
                                           
                                            <th colspan="5" nowarp><%= BeforeYear %>年项目类别划分</th>

                                           
                                            <th colspan="5" nowarp><%= CurYear %>年项目类别划分</th>
                                            <th colspan="5" nowarp>对比增减率</th>
                                        </tr>
                                        <tr>
                                            <th nowarp class="auto-style1">公建</th>
                                            <th nowarp class="auto-style1">市政</th>
                                            <th nowarp class="auto-style1">房地产</th>
                                            <th nowarp class="auto-style1">综合</th>
                                            <th nowarp class="auto-style1">合计</th>
                                            <th nowarp class="auto-style1">公建</th>
                                            <th nowarp class="auto-style1">市政</th>
                                            <th nowarp class="auto-style1">房地产</th>
                                            <th nowarp class="auto-style1">综合</th>
                                            <th nowarp class="auto-style1">合计</th>
                                            <th nowarp class="auto-style1">公建</th>
                                            <th nowarp class="auto-style1">市政</th>
                                            <th nowarp class="auto-style1">房地产</th>
                                            <th nowarp class="auto-style1">综合</th>
                                            <th nowarp class="auto-style1">合计</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%= HtmlTable %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="charts">
                            <div id="main" style="height: 500px;">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
    <script src="../js/echarts/build/dist/echarts.js"></script>

    <script type="text/javascript">



        $(function () {

            //点击查询后的图
            var clmdata = <%= xAxis %>;
            var seriesdata = [<%= SeriesData %>];
            var legenddata = <%= LegendData %>;
            var yAxisData = [<%= yAxis %>];

            loadChartsData(clmdata, seriesdata,legenddata,yAxisData);


            //季度选择
            $("#ctl00_ContentPlaceHolder1_drpJidu").change(function() {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").val("0").attr("disabled",true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpMonth").attr("disabled", false);
                }
            });
            //月份选择
            $("#ctl00_ContentPlaceHolder1_drpMonth").change(function() {
                var index = $(this).val();
                if (index != "0") {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").val("0").attr("disabled",true);
                } else {
                    $("#ctl00_ContentPlaceHolder1_drpJidu").attr("disabled", false);
                }
            });
            //时间段
            $("#ctl00_ContentPlaceHolder1_chkTime").click(function() {
                var obj = $(this);
     
                if (obj.attr("checked")) {
                    $(".year").hide();
                    $(".noyear").show();
                } else {
                    $(".year").show();
                    $(".noyear").hide();
                }
            });

            //判断时间段是否为选中状态
            if ($("#ctl00_ContentPlaceHolder1_chkTime").attr("checked")) {
                $(".year").hide();
                $(".noyear").show();
            }
           
        });

        var loadChartsData = function (clmdata, seriesdata,legenddata,yAxisData) {

            //配置路径
            require.config({
                paths: {
                    echarts: '../js/echarts/build/dist'
                }
            });
            //加载
            require(
                ['echarts',
                    'echarts/chart/bar',
                'echarts/chart/line'],
                function (ec) {
                    var mycharts = ec.init(document.getElementById('main'));

                    var option = {
                        title: {
                            text:"项目分类合同额统计",
                            subtext:"单位：（万元）"
                        },
                        tooltip: {
                            show: true
                        },
                        legend: {
                            data: legenddata
                        },
                        toolbox: {
                            show : true,
                            feature : {
                                mark : {show: true},
                                //dataView : {show: true, readOnly: false},
                                magicType : {show: true, type: ['line', 'bar']},
                                restore : {show: true},
                                saveAsImage : {show: true}
                            }
                        },
                        calculable : true,
                        xAxis: [
                            {
                                'type': 'category',
                                'axisLabel':{'interval':0},
                                'data': clmdata
                            }
                        ],
                        yAxis: yAxisData,
                        series: seriesdata
                    };

                    mycharts.setOption(option);
                }
            );
        }


    </script>
</asp:Content>
