﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountedForConfirmation.aspx.cs" Inherits="TG.Web.Coperation.AccountedForConfirmation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
     
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/CprChargStatus.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetTableStyle("td_input", "need");
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
        <div class="cls_data">
            <table style="width: 560px; margin: 0 auto;" class="cls_show_cst_jiben">
                <tr>
                    <td>合同名称：<%=CoperationName %>
                    </td>
                    <td>承接部门：<%=DepartmentName %></td>
                </tr>
            </table>
            <table style="width: 560px;" class="cls_content_head">
                <tr>
                    <td style="width: 90px;" align="center">计划收费次数
                    </td>
                    <td style="width: 50px;" align="center">百分比
                    </td>
                    <td style="width: 100px;" align="center">计划付款(万元)
                    </td>
                    <td style="width: 100px;" align="center">计划付款时间
                    </td>
                    <td style="width: 100px;" align="center">实际收款(万元)
                    </td>
                    <td style="width: 60px;" align="center">状态
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                Font-Size="12px" Width="560px" OnRowDataBound="gv_Coperation_RowDataBound">
                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                <Columns>
                    <asp:BoundField DataField="Times" HeaderText="收款次数">
                        <ItemStyle Width="90px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="persent" HeaderText="百分比">
                        <ItemStyle Width="50px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="payCount" HeaderText="计划收款">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="paytime" HeaderText="计划时间">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="实际收款">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("payShiCount") %>'></asp:Label>
                            <asp:HiddenField ID="Hidden1" runat="server" Value='<%# Eval("ID") %>' />
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="paytime" HeaderText="状态">
                        <ItemStyle Width="60px" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <table class="cls_show_cst_jiben" style="width: 560px; margin: 0 auto;"
                id="td_input">
                <tr>
                    <td>入账金额：
                    </td>
                    <td>
                        <asp:Label ID="payCount" runat="server" Text=""></asp:Label>(万元)
                    </td>
                </tr>
                <tr>
                    <td>汇款人：
                    </td>
                    <td>
                        <asp:Label ID="remitter" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>入账时间：
                    </td>
                    <td>
                        <asp:Label ID="times" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>入 账 人：
                    </td>
                    <td class="TextBoxBorder">
                        <asp:Label ID="user" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>入账单据号：
                    </td>
                    <td class="TextBoxBorder">
                        <asp:Label ID="BillNo" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>备注：
                    </td>
                    <td>
                        <asp:Label ID="mark" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:ImageButton ID="sureButton" runat="server" ImageUrl="~/Images/buttons/qr_shoukuan.gif" OnClick="sureButton_Click"/>
                        <asp:ImageButton ID="notSureButton" runat="server" ImageUrl="~/Images/buttons/no_shoukuan.gif" OnClick="notSureButton_Click"/>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
