﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_AcountAndIndustryListBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_AcountAndIndustryListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/FunctionChart/FusionCharts.js"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/Coperation/cpr_AcountAndIndustry.js" type="text/javascript"></script>
    <style type="text/css">
        .cls_chart
        {
            font-size: 9pt;
            width: 100%;
        }
        
        .cls_nav
        {
            width: 70px;
        }
        
        .cls_nav a
        {
            text-decoration: none;
        }
        
        .show_projectNumber
        {
            width: 860px;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }
        
        .show_projectNumber td
        {
            border: solid 1px #CCC;
            font-size: 9pt;
            font-family: "微软雅黑";
            height: 20px;
        }
        
        .cls_show_cst_jiben_2
        {
            border-collapse: collapse;
            border: solid 0px black;
            font-size: 9pt;
        }
        
        #ctl00_ContentPlaceHolder1_labTime
        {
            float: right;
            font-size: 12px;
            font-family: 微软雅黑;
        }
        
        #ctl00_ContentPlaceHolder1_labDanW
        {
            float: right;
            margin-right: 10px;
            font-size: 12px;
            font-family: 微软雅黑;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            //查看合同详情
            $("a[id=previewConperation]").live("click", function () {

                var coperationSysNo = $(this).attr("coperationsysno");
                window.location.href = "/Coperation/cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + coperationSysNo;
                //                                $("#PopAreaForCoperationDetail > iframe").attr("src", "/Coperation/cpr_ShowCopration.aspx?flag=cprlist&cprid=" + coperationSysNo + "");
                //                                $("#PopAreaForCoperationDetail").dialog({
                //                                        autoOpen: false,
                //                                        modal: true,
                //                                        width: 900,
                //                                        height: 800,
                //                                        resizable: false,
                //                                        title: "合同详细信息",
                //                                        buttons:
                //					{
                //					        "关闭": function() { $(this).dialog("close"); }
                //					}
                //                                }).dialog("open");
            });
        });

        var globalArray = new Array();


        function GerCoperationList(year, isPay) {
            var pageSize = parseInt($("#HiddenPageSize").val(), 10);
            globalArray[0] = year;
            globalArray[1] = isPay;
            var result = TG.Web.Coperation.cpr_AcountAndIndustryListBymaster.GetCoperationListByIsPay(year, isPay, 1, $("#HiddenPageSize").val(), $("#ctl00_ContentPlaceHolder1_drp_month").val()).value;

            if (result != null && result.length > 0) {
                var resultArray = eval("(" + result + ")");

                $("#PopAreaCopertionList").html(CreateTable(resultArray[1]));
                CommonControl.SetTableStyle("resultTable", "need");
                $("#PopAreaCopertionListDiv").modal();
                //                $("#PopAreaCopertionList").dialog({
                //                    autoOpen: false,
                //                    modal: true,
                //                    width: 900,
                //                    height: 500,
                //                    resizable: false,
                //                    title: isPay == 1 ? "已付款合同列表" : "未付款合同列表",
                //                    buttons:
                //						{
                //						    "关闭": function () { $(this).dialog("close"); }
                //						}
                //                }).dialog("open");
            }

            var currentPage = 1;
            if (resultArray[0] == undefined) {
                projectCount = 0;
                currentPage = 0;
            }

            var pageCount = 0;
            if (resultArray[0] <= pageSize) {
                pageCount = 1;
            } else {
                if (resultArray[0] % pageSize == 0) {
                    pageCount = resultArray[0] / pageSize;
                } else {
                    pageCount = Math.floor(resultArray[0] / pageSize) + 1;
                }
            }
            $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": resultArray[0], "buttonClickCallback": GerCoperationListCallBack });
        }

        //弹出合同详情列表
        function GerCoperationListCallBack(clickNumber) {
            var pageSize = parseInt($("#HiddenPageSize").val(), 10);
            var result = TG.Web.Coperation.cpr_AcountAndIndustryListBymaster.GetCoperationListByIsPay(globalArray[0], globalArray[1], clickNumber, $("#HiddenPageSize").val(), $("#ctl00_ContentPlaceHolder1_drp_month").val()).value;

            if (result != null && result.length > 0) {
                var resultArray = eval("(" + result + ")");

                $("#PopAreaCopertionList").html(CreateTable(resultArray[1]));
                CommonControl.SetTableStyle("resultTable", "need");
            }

            var currentPage = clickNumber;
            if (resultArray[0] == undefined) {
                projectCount = 0;
                currentPage = 0;
            }

            var pageCount = 0;
            if (resultArray[0] % pageSize == 0) {
                pageCount = 1;
            } else {
                pageCount = Math.floor(resultArray[0] / pageSize) + 1;
            }
            $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": resultArray[0], "buttonClickCallback": GerCoperationListCallBack });
        }

        //拼接Table
        function CreateTable(coperatioinList) {
            var tableString = "";
            tableString += "<div class=\"cls_data\"  style=\"height:90%;\" >";
            tableString += "<table class=\"cls_content_head\" >";
            tableString += "<tr>";
            tableString += "<td style=\"width: 250px;\" align=\"center\">";
            tableString += "合同名称";
            tableString += "</td>";
            tableString += "<td style=\"width: 100px;\" align=\"center\">";
            tableString += "合同额(万元)";
            tableString += "</td>";
            tableString += "<td style=\"width: 150px;\" align=\"center\">";
            tableString += "日期";
            tableString += "</td>";
            tableString += "<td style=\"width: 100px;\" align=\"center\">";
            tableString += "负责人";
            tableString += "</td>";

            var titleText = "";

            titleText = "合同类型";

            tableString += "<td style=\"width: 200px;\" align=\"center\">";
            tableString += titleText;
            tableString += "</td>";

            tableString += "<td style=\"width: 60px;\" align=\"center\">";
            tableString += "查看";
            tableString += "</td>";
            tableString += "</tr>";
            tableString += "</table>";

            //拼接项目列表结果Table
            var resultTableString = "";
            resultTableString += "<table class=\"show_projectNumber\" id=\"resultTable\">";
            $.each(coperatioinList, function (index, coperation) {
                resultTableString += "<tr>";
                resultTableString += "<td style=\"width: 250px;\" align=\"left\" title='" + coperation.CoperationName + "'><img src=\"../Images/buttons/icon_cpr.png\" style=\"width:16px;height:16px;\">";
                resultTableString += coperation.CoperationName;
                resultTableString += "</td>";
                resultTableString += "<td style=\"width: 100px;\" align=\"center\">";
                resultTableString += coperation.CoperationAccount;
                resultTableString += "</td>";
                resultTableString += "<td style=\"width: 150px;\" align=\"center\">";
                resultTableString += coperation.RegisterTimeString;
                resultTableString += "</td>";
                resultTableString += "<td style=\"width: 100px;\" align=\"center\">";
                resultTableString += coperation.ChargeManJia;
                resultTableString += "</td>";

                var valueText = "";

                valueText = coperation.CoperationType;

                resultTableString += "<td style=\"width: 200px;\" align=\"center\">";
                resultTableString += valueText;
                resultTableString += "</td>";

                resultTableString += "<td style=\"width: 60px;\" align=\"center\">";
                resultTableString += "<a href=\"#\" id=\"previewConperation\" coperationsysno=\"" + coperation.SysNo + "\">查看</a>";
                resultTableString += "</td>";
                resultTableString += "</tr>";
            });
            resultTableString += "</table>";
            tableString += resultTableString;
            tableString += "</div>";
            tableString += "<div id=\"pager\" style=\"height:5%; \"></div>";
            return tableString;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        领导驾驶舱 <small>项目分类对比表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>生产经营统计报表</a><i class="fa fa-angle-right">
    </i><a>项目分类对比表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询项目分类对比统计
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_report" runat="server" Text="导出" CssClass="btn red btn-sm"
                            OnClick="btn_report_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                    	<tr>
                    		<td> 生产部门:</td>
                            <td><asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>统计年份:</td>
                            <td><asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem>2010</asp:ListItem>
                                    <asp:ListItem>2011</asp:ListItem>
                                    <asp:ListItem>2012</asp:ListItem>
                                    <asp:ListItem>2013</asp:ListItem>
                                    <asp:ListItem>2014</asp:ListItem>
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>对比月份:</td>
                            <td><asp:DropDownList ID="drp_month" runat="server" CssClass="form-control">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>11</asp:ListItem>
                                    <asp:ListItem>12</asp:ListItem>
                                </asp:DropDownList></td>
                            <td><input type="submit" class="btn blue" value="查询" id="btn_ok" /></td>
                    	</tr>
                    </table>
             
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>项目分类对比
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1_1" data-toggle="tab">数据列表</a></li>
                                <li class=""><a href="#tab_1_2" data-toggle="tab">图形列表</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_1_1" style="width: 100%">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-scrollable">
                                                <div class="cls_Container_Report">
                                                    <div class="cls_Container_Tip">
                                                        <asp:Label ID="labYear" runat="server"></asp:Label>年(1-<label id="titleyue" runat="server"></label>月)已签订合同额【项目分类】与
                                                        <label id="title" runat="server">
                                                        </label>
                                                        年同期对比表
                                                    </div>
                                                    <asp:Label ID="labTime" runat="server" Font-Size="7.5pt"></asp:Label>
                                                    <asp:Label ID="labDanW" runat="server" Font-Size="7.5pt">单位：万元</asp:Label>
                                                    <table id="mytab" runat="server" class="table table-bordered" style="width: 1400px;">
                                                        <tr>
                                                            <td rowspan="2" style="width: 8%;">
                                                                单位
                                                            </td>
                                                            <td rowspan="2" style="width: 5%">
                                                                统计项目
                                                            </td>
                                                            <td colspan="5">
                                                                <label id="year" runat="server">
                                                                </label>
                                                                年度 按类别划分
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:Label ID="labYear2" runat="server"></asp:Label>
                                                                年度 按类别划分
                                                            </td>
                                                            <td colspan="5">
                                                                对比增减率
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 5%;">
                                                                公建
                                                            </td>
                                                            <td style="width: 5%;">
                                                                市政
                                                            </td>
                                                            <td style="width: 5%;">
                                                                房地产
                                                            </td>
                                                            <td style="width: 5%;">
                                                                综合
                                                            </td>
                                                            <td style="width: 5%;">
                                                                合计
                                                            </td>
                                                            <td style="width: 6%;">
                                                                公建
                                                            </td>
                                                            <td style="width: 6%;">
                                                                市政
                                                            </td>
                                                            <td style="width: 6%;">
                                                                房地产
                                                            </td>
                                                            <td style="width: 6%;">
                                                                综合
                                                            </td>
                                                            <td style="width: 6%;">
                                                                合计
                                                            </td>
                                                            <td style="width: 6%;">
                                                                公建
                                                            </td>
                                                            <td style="width: 6%;">
                                                                市政
                                                            </td>
                                                            <td style="width: 6%;">
                                                                房地产
                                                            </td>
                                                            <td style="width: 6%;">
                                                                综合
                                                            </td>
                                                            <td style="width: 6%;">
                                                                合计
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table id="AddTable" class="table table-bordered table-data" style="width: 1400px;">
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade " id="tab_1_2" style="width: 100%">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="chart_2" class="cls_chart">
                                                <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" runat="Server" id="userShortName" value="" />
    <input type="hidden" runat="Server" id="previewPower" value="" />
    <input type="hidden" runat="Server" id="userSysNum" value="" />
    <input type="hidden" runat="Server" id="userUnitNum" value="" />
    <div id="PopAreaCopertionListDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                合同信息</h4>
        </div>
        <div class="modal-body">
            <div id="PopAreaCopertionList">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--合同详细信息PopArea-->
    <div id="PopAreaForCoperationDetail" style="display: none;">
        <iframe src="#" style="width: 100%; height: 100%;"></iframe>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenPageSize" value="<%=PageSize %>" />
</asp:Content>
