﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="SeacherCoperationForReportBymaster.aspx.cs" Inherits="TG.Web.Coperation.SeacherCoperationForReportBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Coperation/cpr_SearchCprBack_jq.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        合同信息管理 <small>报备查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>合同信息管理<i class="fa fa-angle-right"> </i>合同报备管理<i class="fa fa-angle-right"> </i>
        报备查询</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同报备高级查询</div>
                     <div class="actions">
                        <asp:Button Text="导出" runat="server" CssClass="btn red" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-1">
                                    合同报备名称:
                                </label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control input-sm" id="txt_cprname" runat="server" />
                                    
                                </div>
                                <label class="control-label col-md-1">
                                    合同分类:</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="drp_type" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm">
                                        <asp:ListItem Value="0">----请选择----</asp:ListItem>
                                    </asp:DropDownList>
                                    
                                </div>
                                <label class="control-label col-md-1">
                                    报备时间:
                                </label>
                                <div class="col-md-3">
                                    <input type="text" name="txt_date2" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" runat="Server" size="20" style="width: 120px; height: 30px;" />至<input
                                            type="text" name="txt_date1" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 120px; height: 30px;" />
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-1">
                                    生产部门:
                                </label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="drp_unit" CssClass="form-control input-sm" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">----请选择----</asp:ListItem>
                                    </asp:DropDownList>
                                    
                                </div>
                                <div class="col-md-1">
                                    <input type="button" class="btn blue btn-block" value="查询" id="btn_search" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同报备信息列表</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="jqGrid">
                                </table>
                                <div id="gridpager">
                                </div>
                                <div id="nodata" class="norecords">
                                    没有符合条件数据！</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
