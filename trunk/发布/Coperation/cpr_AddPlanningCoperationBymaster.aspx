﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="cpr_AddPlanningCoperationBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_AddPlanningCoperationBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/ChooseCustomer.ascx" TagName="ChooseCustomer" TagPrefix="uc1" %>
<%@ Register Src="../UserControl/CompanyType.ascx" TagName="CompanyType" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_cpr.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="../js/Coperation/cpr_AddPlanningCoperationBymaster.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>

    <script type="text/javascript">
        var cprid = '<%=GetCoperationID() %>';
        //用户ID
        var userid = '<%=GetCurMemID() %>';
        if (userid == "0") {
            window.parent.parent.document.location.href = "../index.html";
        }
        if (cprid != "0") {
            var swfu;
            window.onload = function () {
                swfu = new SWFUpload({

                    upload_url: "../ProcessUpload/upload_othercpr.aspx?type=cpr&cpr_type=planning&id=" + cprid + "&userid=" + userid,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                    file_types_description: "上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccessShowResult,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_style: '{background-color:#d8d8d8 }',
                    button_width: 61,
                    button_height: 22, //<span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder"><i class="fa fa-paper-clip"></i>选择文件</span> </span>
                    button_text: '<span class="fileupload-new">选择文件</span>',
                    button_text_style: '.fileupload-new {background-color:#d8d8d8 ;} ',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,
                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
        }
    </script>
    <script type="text/javascript">
        var hid_cprid = '<%=GetCoperationID() %>';
        //判断是否选中节点
        var nodecount;
        var nodeLenght;
        function IsStructCheckNode(obj) {
            nodecount = 0;
            nodeLenght="";
           
            if (obj == 'struct') {
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFun);
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFuns);
            }
            else if (obj == 'structtype') {
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFun);
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFuns);
            }
            
       
        if (nodecount > 0 ) {
            //选中的节点不能大于500个
            if (nodeLenght.length<500) {
                return true;
            }else{
                return false;
            }            
        }
        else {
            return false;
        }
    }
    //选中与半选中  qpl 20140115
    function displayNodeFun(elem) {
        if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
            nodecount++;
        }
    }
    function displayNodeFuns(elem) {
        if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
            nodeLenght+=  elem.getElementsByTagName("checkedState");
        }
    }
  
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>规划工程合同信息录入</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right">
    </i><a>规划工程合同信息录入</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同录入
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <uc2:CompanyType ID="CompanyType" runat="server" />
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    客户信息
                                </div>
                                <div class="col-md-2">
                                    <a href="#CustomerList" class="btn blue" data-toggle="modal" id="btn_seach"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                        <tr>
                                            <td style="width: 13%;">客户编号:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_No" runat="server" class="form-control input-sm" maxlength="15"
                                                    readonly />
                                            </td>
                                            <td style="width: 15%;">客户简称:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_Brief" runat="Server" class="form-control input-sm"
                                                    maxlength="25" readonly />
                                                <asp:HiddenField ID="hid_cstid" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>客户名称:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_Name" runat="Server" class="form-control input-sm"
                                                    maxlength="100" readonly />
                                            </td>
                                            <td>公司地址:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Address" runat="Server" class="form-control input-sm"
                                                    maxlength="100" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>邮政编码:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCode" runat="Server" class="form-control input-sm" maxlength="6"
                                                    readonly />
                                            </td>
                                            <td>联系人:
                                            </td>
                                            <td>
                                                <input type="text" id="txtLinkman" runat="Server" class="form-control input-sm" maxlength="25"
                                                    readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>公司电话:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Phone" runat="Server" class="form-control input-sm"
                                                    maxlength="15" readonly />
                                            </td>
                                            <td>传真号:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Fax" runat="Server" class="form-control input-sm" maxlength="20"
                                                    readonly />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    合同信息
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                        <tr>
                                            <td colspan="6">
                                                <span style="color: red;">提示:黄色背景文本框为必填项！<i class="fa fa-warning "></i></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100px;">合同编号:
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="col-md-9">
                                                        <input type="text" id="txtcpr_No" runat="server" class="form-control input-sm" maxlength="15"
                                                            placeholder="(例：BJ2012-09)" style="background-color: #FFC;" /><asp:HiddenField ID="hid_cprno"
                                                                runat="server" Value="" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a href="#CprNo" id="btn_getcprnum" class="btn blue btn-sm" data-toggle="modal"><i
                                                            class="fa fa-search"></i></a>
                                                        <asp:HiddenField ID="hid_cprid" runat="server" />
                                                    </div>
                                                </div>

                                            </td>
                                            <td style="width: 100px;">合同分类:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddcpr_Type" runat="Server" AppendDataBoundItems="True" CssClass="form-control"
                                                    BackColor="#F0F0C0">
                                                    <asp:ListItem Value="-1">----选择合同类别----</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 100px;">合同类型:
                                            </td>
                                            <td>
                                                <label runat="server" id="txt_cprType">规划工程合同</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>合同名称:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txt_cprName" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                            </td>
                                            <td>建筑类别:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drp_buildtype" Width="120px" runat="server" AppendDataBoundItems="true" BackColor="#F0F0C0"
                                                    CssClass="form-control">
                                                    <asp:ListItem Value="-1">---选择类别---</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>建设单位:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txt_cprBuildUnit" runat="server" class="form-control input-sm"
                                                    style="background-color: #FFC;" />
                                            </td>
                                            <td>建设规模:
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <div class="col-md-8">
                                                        <input type="text" id="txt_buildArea" runat="Server" class="form-control input-sm"
                                                            maxlength="25" style="background-color: #FFC; width: 100%;" />
                                                    </div>

                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="areaType" runat="Server" AppendDataBoundItems="True" CssClass="form-control"
                                                            BackColor="#F0F0C0" Width="100px">
                                                            <asp:ListItem Value="1">平方米</asp:ListItem>
                                                            <asp:ListItem Value="4">米</asp:ListItem>
                                                            <asp:ListItem Value="2">公顷</asp:ListItem>
                                                            <asp:ListItem Value="3">吨(锅炉房)</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:HiddenField ID="hidtxt_buildArea" runat="server" Value=""></asp:HiddenField>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>结构形式:
                                            </td>
                                            <td>
                                                <cc1:ASDropDownTreeView ID="asTreeviewStruct" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                    DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                    EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                    EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true"
                                                    EnableTheme="true" Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                    RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                    DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    Font-Size="12px" Font-Strikeout="False" Height="23px" InitialDropdownText="----------请选择结构样式--------" />
                                            </td>
                                            <td>建筑分类:
                                            </td>
                                            <td>
                                                <cc1:ASDropDownTreeView ID="asTreeviewStructType" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                    DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                    EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                    EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true"
                                                    EnableTheme="true" Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                    RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                    DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                    Font-Size="12px" Font-Strikeout="False" Height="23px" InitialDropdownText="----------请选择建筑分类--------" />
                                            </td>
                                            <td>层数:
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            地上：
                                                            <input type="text" id="txt_upfloor" runat="Server" class="form-control input-sm"
                                                                maxlength="25" style="width: 60%;" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            地下：
                                                            <input type="text" id="txt_downfloor" runat="Server" class="form-control input-sm"
                                                                maxlength="25" style="width: 60%;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>工程负责人:
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="col-md-9">
                                                        <input type="text" id="txt_proFuze" runat="server" class="form-control input-sm"
                                                            maxlength="15" readonly />
                                                        <span class="help-block"><font><font class=""></font></font></span>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input id="HiddenPMUserID" type="hidden" value="0" runat="Server" />
                                                        <a href="#PMName" id="btn_gcfz" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>电话:
                                            </td>
                                            <td>
                                                <input type="text" id="txt_fzphone" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                            <td>承接部门:
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="col-md-9">
                                                        <input type="text" id="txt_cjbm" runat="Server" class="form-control input-sm" maxlength="25"
                                                            readonly />
                                                        <asp:HiddenField ID="hid_cjbm" runat="server" Value=""></asp:HiddenField>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a href="#CJBM" id="btn_cjbm" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a><span class="help-block"><font><font class=""></font></font></span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>甲方负责人:
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="col-md-9">
                                                        <input type="text" id="txtFParty" runat="server" class="form-control input-sm" maxlength="15"
                                                            readonly />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a href="#JFFZ" id="btn_jffz" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a><span class="help-block"><font><font class=""></font></font></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>电话:
                                            </td>
                                            <td>
                                                <input type="text" id="txt_jiafphone" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                            <td>工程地点:
                                            </td>
                                            <td>
                                                <input type="text" id="txt_ProjectPosition" runat="Server" class="form-control input-sm"
                                                    maxlength="25" style="background-color: #FFC;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>合同额:
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text" id="txtcpr_Account" runat="server" class="form-control input-sm"
                                                        maxlength="15" style="background-color: #FFC; width: 85%;" />万元
                                                    <asp:HiddenField ID="hidtxtcpr_Account" runat="server" Value=""></asp:HiddenField>
                                                </div>
                                            </td>
                                            <td>投资额:
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text" id="txtInvestAccount" runat="Server" class="form-control input-sm"
                                                        maxlength="25" style="width: 85%;" />万元
                                                </div>
                                            </td>
                                            <td>行业性质:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddProfessionType" Width="120px" runat="Server" AppendDataBoundItems="True"
                                                    CssClass="form-control" BackColor="#F0F0C0">
                                                    <asp:ListItem Value=" ">--请选择--</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>实际合同额:
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text" id="txtcpr_Account0" runat="server" class="form-control input-sm"
                                                        style="width: 85%;" maxlength="15" />万元
                                                </div>
                                            </td>
                                            <td>实际投资额:
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input type="text" id="txtInvestAccount0" runat="Server" class="form-control input-sm"
                                                        style="width: 85%;" maxlength="25" />万元
                                                </div>
                                            </td>
                                            <td>工程来源:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddSourceWay" Width="120px" runat="Server" AppendDataBoundItems="True" CssClass="form-control"
                                                    BackColor="#F0F0C0">
                                                    <asp:ListItem Value=" ">--请选择--</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>合同阶段:
                                            </td>
                                            <td colspan="3">
                                                <asp:CheckBoxList ID="chk_cprjd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                </asp:CheckBoxList>
                                            </td>
                                            <td>制表人:
                                            </td>
                                            <td>
                                                <input type="text" id="txt_tbcreate" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>合同签订日期:
                                            </td>
                                            <td>
                                                <input type="text" name="txt_date" id="txtSingnDate2" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5; background-color: #F0F0C0;" />
                                            </td>
                                            <td>合同完成日期:
                                            </td>
                                            <td>
                                                <input type="text" name="txt_date" id="txtCompleteDate" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5; background-color: #F0F0C0;" />
                                            </td>
                                            <td>合同统计年份:</td>
                                            <td>
                                                <input type="text" name="txt_date" id="txtSingnDate" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5; background-color: #F0F0C0;" /></td>
                                        </tr>
                                        <tr>
                                            <td>多栋楼:
                                            </td>
                                            <td colspan="5">
                                                <textarea class="form-control input-sm" id="txt_MultiBuild" rows="3" runat="Server"
                                                    placeholder="Enter a message ..."> </textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>合同备注:
                                            </td>
                                            <td colspan="5">
                                                <textarea class="form-control input-sm" id="txtcpr_Remark" runat="Server" rows="3"
                                                    placeholder="Enter a message ..."> </textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    收费计划
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn default" data-toggle="modal" href="#TJSF" id="btn_AddSf">
                                        添加</button>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-condensed flip-contentr" id="sf_datas" style="width: 98%;" align="center">
                                            <thead class="flip-content">
                                                <tr id="sf_row">
                                                    <td align="center" id="sf_id">收费次数
                                                    </td>
                                                    <td align="center" id="sf_bfb">百分比
                                                    </td>
                                                    <td align="center" id="sf_edu">付费额(万元)
                                                    </td>
                                                    <%-- <th align="center" id="sf_type">
                                                    收费类型
                                                </th>--%>
                                                    <td align="center" id="sf_time">付款时间
                                                    </td>
                                                    <td align="center" id="sf_mark">备注
                                                    </td>
                                                    <td align="center" id="sf_oper">操作
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    合同子项
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn default" data-toggle="modal" href="#ZXObject" id="btn_addSub">
                                        添加</button>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-condensed flip-contentr" id="datas" style="width: 98%;" align="center">
                                            <thead class="flip-content">
                                                <tr id="sub_row">
                                                    <td align="center" id="sub_id">序号
                                                    </td>
                                                    <td align="center" id="sub_name">子项名称
                                                    </td>
                                                    <%--  <th align="center" id="struct_sub">
                                                    结构形式
                                                </th>
                                                <th align="center" id="type_sub">
                                                    建筑分类
                                                </th>--%>
                                                    <td align="center" id="sub_area">建筑面积(<span style="font-size: 10pt" id="span_unit1">平方米</span>)
                                                    </td>
                                                    <td align="center" id="sub_money">金额(万元)
                                                    </td>
                                                    <%--<th align="center" id="sub_livearea">
                                                    规划面积(<span style="font-size: 10pt">公顷</span>)
                                                </th>--%>
                                                    <%--<th align="center" id="sub_moneyunit">
                                                    平米单价(元)
                                                </th>--%>
                                                    <td align="center" id="sub_remark">备注
                                                    </td>
                                                    <td align="center" id="sub_oper">操作
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-2">
                                    合同附件
                                </div>
                                <div class="col-md-8">
                                    <div id="divFileProgressContainer" style="float: right;">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder">
                                        <i class="fa fa-paper-clip"></i>选择文件</span> </span>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-condensed flip-contentr" id="datas_att" style="width: 98%;" align="center">
                                            <thead class="flip-content">
                                                <tr id="att_row">
                                                    <td style="width: 40px;" align="center" id="att_id">序号
                                                    </td>
                                                    <td style="width: 300px;" align="center" id="att_filename">文件名称
                                                    </td>
                                                    <td style="width: 80px" align="center" id="att_filesize">文件大小
                                                    </td>
                                                    <td style="width: 80px;" align="center" id="att_filetype">文件类型
                                                    </td>
                                                    <td style="width: 120px;" align="center" id="att_uptime">上传时间
                                                    </td>
                                                    <td style="width: 40px;" align="center" id="att_oper">&nbsp;
                                                    </td>
                                                    <td style="width: 40px;" align="center" id="att_oper2">&nbsp;
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="submit" class="btn green" id="btn_Save">
                                保存
                            </button>
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回
                            </button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 选择客户弹出层 -->
    <div id="CustomerList" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">查询客户</h4>
        </div>
        <div class="modal-body">
            <div id="chooseCustomerContainer">
                <uc1:ChooseCustomer ID="ChooseCustomer1" runat="server" PageSize="15" UserSysNo="0"
                    PreviewPower="0" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_customer_close">
                关闭</button>
        </div>
    </div>
    <!--选择合同编号弹出层-->
    <div id="CprNo" class="modal fade yellow" tabindex="-1" data-width="350" aria-hidden="true"
        style="display: none; width: 350px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">查询编号</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_Number">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td>合同类型
                                </td>
                                <td>
                                    <select id="cpr_typeSelect" class="form-control">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>合同编号
                                </td>
                                <td>
                                    <select id="cpr_numSelect" class="form-control">
                                    </select>
                                    <span id="noselectMsg" class="valide">*</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_cprNum_close" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--项目经理弹出层-->
    <div id="PMName" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">工程负责人</h4>
        </div>
        <div class="modal-body">
            <div id="gcFzr_Dialog">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>生产部门:
                                    </td>
                                    <td>
                                        <select id="select_gcFzr_Unit" class="from-control">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="gcFzr_MemTable" class="table table-bordered" style="text-align: center">
                                <tr class="trBackColor">
                                    <td style="width: 60px;">序号
                                    </td>
                                    <td style="width: 180px;">人员名称
                                    </td>
                                    <td align="center" style="width: 60px;">操作
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="gcFzr_ForPageDiv" class="divNavigation pageDivPosition">
                            总<label id="gcfzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;第<label id="gcfzr_nowPageIndex">0</label>/<label id="gcfzr_allPageCount">0</label>页&nbsp;&nbsp;<span id="gcfzr_firstPage">首页</span>&nbsp; <span id="gcfzr_prevPage">&lt;&lt;</span>&nbsp;<span id="gcfzr_nextPage">&gt;&gt;</span>&nbsp; <span id="gcfzr_lastPage">末页</span>&nbsp;&nbsp;跳至<input type="text" id="gcfzr_pageIndex" style="width: 20px; border-style: none none solid none;" />页&nbsp;&nbsp; <span id="gcfzr_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 承接部门层 -->
    <div id="CJBM" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">承接部门</h4>
        </div>
        <div class="modal-body">
            <div id="pro_cjbmDiv">
                <div class="row">
                    <div class="col-md-12">
                        <table id="pro_cjbmTable" class="table table-bordered" align="center">
                            <tr class="trBackColor">
                                <td style="width: 60px;" align="center">序号
                                </td>
                                <td style="width: 180px;" align="center">单位名称
                                </td>
                                <td align="center" style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="cjbmByPageDiv" class="divNavigation pageDivPosition">
                            总<label id="cjbm_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="cjbm_nowPageIndex">0</label>/<label id="cjbm_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="cjbm_firstPage">首页</span>&nbsp; <span id="cjbm_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="cjbm_nextPage">&gt;&gt;</span>&nbsp; <span id="cjbm_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="cjbm_pageIndex" style="height: 3; width: 20px; border-style: none none solid
    none;" />
                            页&nbsp;&nbsp; <span id="cjbm_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 甲方负责人 -->
    <div id="JFFZ" class="modal fade yellow" tabindex="-1" data-width="560" aria-hidden="true"
        style="display: none; width: 560px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人</h4>
        </div>
        <div class="modal-body">
            <div id="jffzr_DialogDiv">
                <table class="table-responsive">
                    <tr>
                        <td style="width: 80px;">姓名:</td>
                        <td>
                            <input type="text" id="txtName" value="" class="form-control input-sm" /></td>
                        <td style="width: 80px;">部门:</td>
                        <td>
                            <input type="text" id="txtCompName" value="" maxlength="25" class="form-control input-sm" /></td>
                    </tr>
                    <tr>
                        <td>电话:</td>
                        <td>
                            <input type="text" id="txtPhone" value="" class="form-control
    input-sm" /></td>
                        <td>
                            <input type="button" id="btn_serch" class="btn blue  btn-sm" value="查询" /></td>
                        <td></td>
                    </tr>
                </table>

                <div class="row">
                    <div class="col-md-12">
                        <table id="jffzr_table" class="table table-bordered table-striped table-condensed
    flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td>联系人
                                </td>
                                <td>电话
                                </td>
                                <td>部门
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="jffzr_forPageDiv" class="divNavigation
    pageDivPosition">
                            总<label id="jffzr_totalCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="jffzr_nowPageIndex">0</label>/<label id="jffzr_PagesCount">0</label>页&nbsp;&nbsp;
                            <span id="jffzr_firstPage">首页</span>&nbsp; <span id="jffzr_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="jffzr_nextPage">&gt;&gt;</span>&nbsp; <span id="jffzr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="jffzr_gotoPageNum" style="height: 3; width: 20px; border-style: none none solid none;" />
                            页&nbsp;&nbsp; <span id="jffzr_gotoPage">GO</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">
                        关闭</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 添加计划收费 -->
    <div id="TJSF" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true"
        style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加计划收费</h4>
        </div>
        <div class="modal-body">
            <div id="tjsfPlan_dialogDiv">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td style="width: 150px;">合同金额:
                                    </td>
                                    <td>
                                        <label id="lbl_copMoney">
                                        </label>
                                        (万元)
                                    </td>
                                    <td style="width: 150px;">计划收款金额(万元):
                                    </td>
                                    <td>
                                        <input type="text" id="txt_planChargeNum" class="form-control input-sm" maxlength="10" />
                                        <span id="jine_notnull" class="valide">金额不能为空!</span> <span id="jine_notint" class="valide">请输入数字!</span> <span id="jine_xiaoyu" class="valide">不能大于合同金额!</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>收款金额比例(%):
                                    </td>
                                    <td>
                                        <input type="text" id="planChargeNumPercent" class="form-control input-sm" maxlength="10" />
                                        <span id="span_Percent" class="valide">比例不能为空!</span> <span id="span_PercentNotInt"
                                            class="valide">请输入数字!</span>
                                    </td>
                                    <td>计划收款日期:
                                    </td>
                                    <td>
                                        <input class="Wdate" id="txt_datePicker" type="text" style="height: 22px; width: 120px;"
                                            onclick="WdatePicker({readOnly:true})" />
                                        <span id="date_notnull" class="valide">时间不能为空!</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>收款备注:
                                    </td>
                                    <td colspan="3">
                                        <textarea rows="3" cols="35" id="txt_chargeRemark" style="max-height: 100px; min-height: 10px;"
                                            maxlength="150"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" id="btn_addPlanCharge" class="btn green btn-default">
                    确定</button>
                <button type="button" data-dismiss="modal" class="btn btn-default">
                    关闭</button>
            </div>
        </div>
    </div>
    <!-- 添加子项信息 -->
    <div id="ZXObject" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加工程子项</h4>
        </div>
        <div class="modal-body">
            <div id="addsubDivDialog">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100px;">子项名称:
                                </td>
                                <td colspan="3">
                                    <input type="text" id="addsubName" class="form-control input-sm" maxlength="50" />
                                    <span id="subNameNull" class="valide">请填写子项名称!</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px;">建筑面积(<span id="span_unit">平方米</span>):
                                </td>
                                <td>
                                    <input type="text" id="addsubArea" class="form-control input-sm" maxlength="15" /><span
                                        id="subAreaNull" class="valide">请填写建筑面积!</span> <span id="subAreaNoInt" class="valide">请填写数字!</span> <span id="subAreaErr" class="valide">建筑面积不能大于合同面积!</span>
                                </td>
                                <td style="width: 100px;">金额(万元):
                                </td>
                                <td>
                                    <input type="text" id="addsubJine" class="form-control input-sm" maxlength="15" /><span
                                        id="subJineNull" class="valide">请填写金额!</span> <span id="subJineNoInt" class="valide">请填写数字!</span> <span id="subJineErr" class="valide">金额不能大于合同额!</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px;">备注:
                                </td>
                                <td colspan="3">
                                    <textarea rows="3" cols="50" id="areaTxtAddSubRemark" style="max-height: 100px; min-height: 10px;"
                                        maxlength="150"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_addsubChild" class="btn green  btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>
    <!--合同类型层-->
    <div id="HTLX" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择合同类型</h4>
        </div>
        <div class="modal-body">
            <div id="chooseCustomerCompact">
                <div class="row">
                    <div class="col-md-12">
                        <table id="customerCompactTable" class="table table-bordered" style="text-align: center;">
                            <tr>
                                <td>序号
                                </td>
                                <td>合同类型
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="NotShowUnitList" Value="" />
</asp:Content>

