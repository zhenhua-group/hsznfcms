﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="AlertCoperationForReportBymaster.aspx.cs" Inherits="TG.Web.Coperation.AlertCoperationForReportBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/ChooseCustomer.ascx" TagName="ChooseCustomer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_cpr.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="../js/Coperation/cpr_Coperation_backeditBymaster.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>

    <script type="text/javascript">
        var cpr_no = '<%=GetCoperationID()  %>';
        $(document).ready(function () {
            $("#hid_cprno").val(cpr_no);
        });

        //判断是否选中节点
        var nodecount;
        function IsStructCheckNode(obj) {
            nodecount = 0;
            if (obj == 'struct') {
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFun);
            }
            else if (obj == 'structtype') {
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFun);
            }
            

        if (nodecount > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    //选中与半选中  qpl 20140115
    function displayNodeFun(elem) {
        if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
            nodecount++;
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同报备修改</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>合同信息管理<i class="fa fa-angle-right"> </i>合同报备管理<i class="fa fa-angle-right"> </i>
        合同报备修改</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>编辑合同报备
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <form action="#" class="form-horizontal">
                        <div class="form-body">
                            <h3 class="form-section">客户信息</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            客户编号:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtCst_No" runat="server" class="form-control input-sm" maxlength="15"
                                                readonly />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            客户简称:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtCst_Brief" runat="Server" class="form-control input-sm"
                                                maxlength="25" readonly />
                                            <asp:HiddenField ID="hid_cstid" runat="server" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            客户名称:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtCst_Name" runat="Server" class="form-control input-sm"
                                                maxlength="100" readonly />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            公司地址:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtCpy_Address" runat="Server" class="form-control input-sm"
                                                maxlength="100" readonly />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            邮政编码:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtCode" runat="Server" class="form-control input-sm" maxlength="6"
                                                readonly />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            联系人:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtLinkman" runat="Server" class="form-control input-sm" maxlength="25"
                                                readonly />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            公司电话:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtCpy_Phone" runat="Server" class="form-control input-sm"
                                                maxlength="15" readonly />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            传真号:</label>
                                        <div class="col-md-9">
                                            <input type="text" id="txtCpy_Fax" runat="Server" class="form-control input-sm" maxlength="20"
                                                readonly />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                </div>
                                <div class="col-md-2">
                                    <a href="#CustomerList" class="btn blue" data-toggle="modal" id="btn_seach"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                            <!-- END FORM-->
                            <h3 class="form-section">合同报备信息</h3>
                            <div class="row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-4 has-error">
                                    <label class="control-label col-md-12">
                                        提示:黄色背景文本框为必填项！<i class="fa fa-warning"></i></label>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            合同名称:</label>
                                        <div class="col-md-8">
                                            <asp:HiddenField ID="hid_cprid" runat="server" Value="" />
                                            <input type="text" id="txt_cprName" runat="server" class="form-control input-sm"
                                                style="background-color: #FFC;" />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            合同分类:</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddcpr_Type" runat="Server" AppendDataBoundItems="True" CssClass="form-control input-sm"
                                                BackColor="#F0F0C0">
                                                <asp:ListItem Value="-1">----选择合同类别----</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            合同类型:</label>
                                        <div class="col-md-6">
                                            <input type="text" id="txt_cprType" runat="server" class="form-control input-sm"
                                                style="background-color: #FFC;" />

                                        </div>
                                        <div class="col-md-2">
                                            <a href="#HTLX" id="btn_cprType" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            项目地点:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txt_cprAddress" runat="server" class="form-control input-sm"
                                                style="background-color: #FFC;" />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            建筑类别:</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="drp_buildtype" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                                <asp:ListItem Value="-1">------选择类别------</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            建设规模:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txt_buildArea" runat="Server" class="form-control input-sm"
                                                maxlength="25" placeholder="㎡" style="background-color: #FFC;" />
                                            <asp:HiddenField ID="hidtxt_buildArea" runat="server" Value=""></asp:HiddenField>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            结构形式:</label>
                                        <div class="col-md-8" id="StrructContainer">
                                            <cc1:ASDropDownTreeView ID="asTreeviewStruct" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true"
                                                EnableTheme="true" Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" Height="34px" InitialDropdownText="----------请选择结构样式--------" />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            建筑分类:</label>
                                        <div class="col-md-8" id="buildTypeContainer">
                                            <cc1:ASDropDownTreeView ID="asTreeviewStructType" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true"
                                                EnableTheme="true" Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" Height="34px" InitialDropdownText="----------请选择建筑分类--------" />

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            层数:</label>
                                        <div class="col-md-4">
                                            <input type="text" id="txt_upfloor" runat="Server" class="form-control input-sm"
                                                placeholder="地上" maxlength="25" />
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" id="txt_downfloor" runat="Server" class="form-control input-sm"
                                                placeholder="地下" maxlength="25" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            工程负责人:</label>
                                        <div class="col-md-6">
                                            <input type="text" id="txt_proFuze" runat="server" class="form-control input-sm"
                                                maxlength="15" readonly />

                                        </div>
                                        <div class="col-md-2">
                                            <input id="HiddenPMUserID" type="hidden" value="0" runat="Server" />
                                            <a href="#PMName" id="btn_gcfz" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            电话:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txt_fzphone" runat="Server" class="form-control input-sm"
                                                maxlength="25" />

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            承接部门:</label>
                                        <div class="col-md-6">
                                            <input type="text" id="txt_cjbm" runat="Server" class="form-control input-sm" maxlength="25"
                                                readonly />
                                            <asp:HiddenField ID="hid_cjbm" runat="server" Value=""></asp:HiddenField>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#CJBM" id="btn_cjbm" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            甲方负责人:</label>
                                        <div class="col-md-6">
                                            <input type="text" id="txtFParty" runat="server" class="form-control input-sm" maxlength="15"
                                                readonly />
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#JFFZDiv" id="btn_jffz" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            电话:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txt_jiafphone" runat="Server" class="form-control input-sm"
                                                maxlength="25" />

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            工程地点:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txt_ProjectPosition" runat="Server" class="form-control input-sm input-sm"
                                                maxlength="25" style="background-color: #FFC;" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            合同额:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txtcpr_Account" runat="server" class="form-control input-sm"
                                                placeholder="万元" maxlength="15" style="background-color: #FFC;" />
                                            <asp:HiddenField ID="hidtxtcpr_Account" runat="server" Value=""></asp:HiddenField>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            投资额:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txtInvestAccount" runat="Server" placeholder="万元" class="form-control input-sm"
                                                maxlength="25" style="background-color: #FFC;" />

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            行业性质:</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddProfessionType" runat="Server" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm" BackColor="#F0F0C0">
                                                <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            实际合同额:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txtcpr_Account0" runat="server" placeholder="万元" class="form-control input-sm"
                                                maxlength="15" />

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            实际投资额:</label>
                                        <div class="col-md-8">
                                            <input type="text" id="txtInvestAccount0" runat="Server" placeholder="万元" class="form-control input-sm"
                                                maxlength="25" />

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            工程来源:</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddSourceWay" runat="Server" AppendDataBoundItems="True" CssClass="form-control input-sm"
                                                BackColor="#F0F0C0">
                                                <asp:ListItem Value=" ">--请选择--</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            是否立项:</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="drp_proapproval" runat="Server" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm" BackColor="#F0F0C0">
                                                <asp:ListItem Value="-1">---------选择---------</asp:ListItem>
                                                <asp:ListItem Value="0">是</asp:ListItem>
                                                <asp:ListItem Value="1">否</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--    <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        合同阶段:</label>
                                    <div class="col-md-8">
                                        <asp:CheckBoxList ID="chk_cprjd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                        </asp:CheckBoxList>
                                        
                                    </div>
                                </div>
                            </div>
                             
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        制表人:</label>
                                    <div class="col-md-8">
                                        <input type="text" id="txt_tbcreate" runat="Server" class="form-control input-sm" maxlength="25" />
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        合同签订日期:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txt_date" id="txtSingnDate" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" style="width: 100%; height: 34px; border: 1px solid #e5e5e5;
                                            background-color: #F0F0C0;" />
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        合同签订日期:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txt_date" id="txtCompleteDate" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" style="width: 100%; height: 34px; border: 1px solid #e5e5e5;
                                            background-color: #F0F0C0;" />
                                        
                                    </div>
                                </div>
                            </div>
                             
                        </div>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-1">
                                            合同备注:</label>
                                        <div class="col-md-11">
                                            <textarea class="form-control input-sm" id="txtcpr_Remark" runat="Server" rows="3"
                                                placeholder="Enter a message ..."> </textarea>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-1">
                                            甲方联系记录:</label>
                                        <div class="col-md-11">
                                            <textarea class="form-control input-sm" id="txtcpr_recorpt" rows="3" runat="Server"
                                                placeholder="Enter a message ..."> </textarea>

                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!-- END FORM-->
                        </div>
                    </form>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="submit" class="btn green" id="btn_Save">
                                保存</button>
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 选择客户弹出层 -->
    <div id="CustomerList" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">查询客户</h4>
        </div>
        <div class="modal-body">
            <div id="chooseCustomerContainer">
                <uc1:ChooseCustomer ID="ChooseCustomer1" runat="server" PageSize="15" UserSysNo="0"
                    PreviewPower="0" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_customer_close">
                关闭</button>
        </div>
    </div>
    <!--选择合同编号弹出层-->
    <div id="CprNo" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">查询编号</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_Number">
                <table style="width: 300px;" class="cls_show_cst_jiben">
                    <tr>
                        <td style="text-align: right;">合同类型:&nbsp;
                        </td>
                        <td style="text-align: left;">&nbsp;
                            <select id="cpr_typeSelect" style="width: 150px;">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">合同编号:&nbsp;
                        </td>
                        <td style="text-align: left;">&nbsp;
                            <select id="cpr_numSelect" style="width: 150px;">
                            </select>
                            <span id="noselectMsg" class="valide">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <button type="button" data-dismiss="modal" id="btn_cprNum_close" class="btn btn-default">
                                确定</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--项目经理弹出层-->
    <div id="PMName" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">项目经理</h4>
        </div>
        <div class="modal-body">
            <div id="gcFzr_Dialog">
                <div class="row">
                    <div class="col-md-2">
                        生产部门:
                    </div>
                    <div class="col-md-2">
                        <select id="select_gcFzr_Unit" class="from-control input-sm">
                        </select>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <table id="gcFzr_MemTable" class="table table-bordered table-striped table-condensed flip-content"
                            style="text-align: center">
                            <tr class="trBackColor">
                                <td style="width: 60px;">序号
                                </td>
                                <td style="width: 340px;">人员名称
                                </td>
                                <td align="center" style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="gcFzr_ForPageDiv" class="divNavigation pageDivPosition">
                            总<label id="gcfzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="gcfzr_nowPageIndex">0</label>/<label id="gcfzr_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="gcfzr_firstPage">首页</span>&nbsp; <span id="gcfzr_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="gcfzr_nextPage">&gt;&gt;</span>&nbsp; <span id="gcfzr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="gcfzr_pageIndex" style="height: 3; width: 20px; border-style: none none solid
    none;" />
                            页&nbsp;&nbsp; <span id="gcfzr_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 承接部门层 -->
    <div id="CJBM" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">承接部门</h4>
        </div>
        <div class="modal-body">
            <div id="pro_cjbmDiv">
                <div class="row">
                    <div class="col-md-12">
                        <table id="pro_cjbmTable" class="table table-bordered table-striped table-condensed flip-content"
                            align="center">
                            <tr class="trBackColor">
                                <td style="width: 60px;" align="center">序号
                                </td>
                                <td style="width: 320px;" align="center">单位名称
                                </td>
                                <td align="center" style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="cjbmByPageDiv" class="divNavigation pageDivPosition">
                            总<label id="cjbm_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="cjbm_nowPageIndex">0</label>/<label id="cjbm_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="cjbm_firstPage">首页</span>&nbsp; <span id="cjbm_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="cjbm_nextPage">&gt;&gt;</span>&nbsp; <span id="cjbm_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="cjbm_pageIndex" style="height: 3; width: 20px; border-style: none none solid
    none;" />
                            页&nbsp;&nbsp; <span id="cjbm_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 甲方负责人 -->
    <div id="JFFZDiv" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人</h4>
        </div>
        <div class="modal-body">
            <div id="jffzr_dialogDiv">
                <div class="row">
                    <div class="col-md-2">
                        客户列表:
                    </div>
                    <div class="col-md-8">
                        &nbsp;<select id="select_jffzrMem" style="width: 350px;"></select>
                        &nbsp;<span id="noCustMsg" class="valide">无数据!&nbsp;&nbsp;请先添加客户!</span><span class="help-block"><font><font
                            class=""></span>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <table id="jffzr_dataTable" class="table table-bordered table-striped table-condensed flip-content"
                            align="center">
                            <tr class="trBackColor">
                                <td>序号
                                </td>
                                <td>姓名
                                </td>
                                <td>职务
                                </td>
                                <td>部门
                                </td>
                                <td>商务电话
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                        <br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="jffzrByPageDiv" class="divNavigation pageDivPosition">
                            总<label id="jffzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="jffzr_nowPageIndex">0</label>/<label id="jffzr_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="jffzr_firstPage">首页</span>&nbsp; <span id="jffzr_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="jffzr_nextPage">&gt;&gt;</span>&nbsp; <span id="jffzr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="jffzr_pageIndex" style="height: 3; width: 20px; border-style: none none solid
    none;" />页&nbsp;&nbsp; <span id="jffzr_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 添加计划收费 -->
    <div id="TJSF" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加计划收费</h4>
        </div>
        <div class="modal-body">
            <div id="tjsfPlan_dialogDiv">
                <table class="cls_show_cst_jiben" style="width: 500px;" align="center" id="subCopMoneyItem">
                    <tr>
                        <td width="100px">合同金额:
                        </td>
                        <td width="120px">
                            <label id="lbl_copMoney">
                            </label>
                            (万元)
                        </td>
                    </tr>
                    <tr>
                        <td>计划收款金额:
                        </td>
                        <td colspan="2">
                            <input type="text" id="txt_planChargeNum" class="cls_input_text" maxlength="10" />万元
                            <span id="jine_notnull" class="valide">金额不能为空!</span> <span id="jine_notint" class="valide">请输入数字!</span> <span id="jine_xiaoyu" class="valide">不能大于合同金额!</span>
                        </td>
                    </tr>
                    <tr>
                        <td>收款金额比例:
                        </td>
                        <td colspan="2">
                            <input type="text" id="planChargeNumPercent" class="cls_input_text" maxlength="10" />%
                            <span id="span_Percent" class="valide">比例不能为空!</span> <span id="span_PercentNotInt"
                                class="valide">请输入数字!</span>
                        </td>
                    </tr>
                    <tr>
                        <td>计划收款日期:
                        </td>
                        <td colspan="2">
                            <input class="Wdate" id="txt_datePicker" type="text" style="width: 120px;" onclick="WdatePicker({readOnly:true})" />
                            <span id="date_notnull" class="valide">时间不能为空!</span>
                        </td>
                    </tr>
                    <tr>
                        <td>收款备注:
                        </td>
                        <td colspan="2">
                            <textarea rows="3" cols="35" id="txt_chargeRemark" style="max-height: 100px; min-height: 10px; min-width: 200px; max-width: 350px;"
                                maxlength="150"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: center">
                            <button type="button" data-dismiss="modal" id="btn_addPlanCharge" class="btn btn-default">
                                确定</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 添加子项信息 -->
    <div id="ZXObject" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加工程子项</h4>
        </div>
        <div class="modal-body">
            <div id="addsubDivDialog">
                <table class="cls_show_cst_jiben" style="width: 450px;" align="center" id="addsubDataTable">
                    <tr>
                        <td style="width: 20%; text-align: right;">子项名称:&nbsp;
                        </td>
                        <td>
                            <input type="text" id="addsubName" style="width: 200px;" maxlength="50" />
                            <span id="subNameNull" class="valide">请填写子项名称!</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">建筑面积:&nbsp;
                        </td>
                        <td>
                            <input type="text" id="addsubArea" style="width: 120px;" maxlength="15" />(㎡) <span
                                id="subAreaNull" class="valide">请填写建筑面积!</span> <span id="subAreaNoInt" class="valide">请填写数字!</span> <span id="subAreaErr" class="valide">建筑面积不能大于合同面积!</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">金额:&nbsp;
                        </td>
                        <td>
                            <input type="text" id="addsubJine" style="width: 120px;" maxlength="15" />(万元) <span
                                id="subJineNull" class="valide">请填写金额!</span> <span id="subJineNoInt" class="valide">请填写数字!</span> <span id="subJineErr" class="valide">金额不能大于合同额!</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">备注:&nbsp;
                        </td>
                        <td>
                            <textarea rows="3" cols="35" id="areaTxtAddSubRemark" style="max-height: 100px; min-height: 10px; min-width: 200px; max-width: 350px;"
                                maxlength="150"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button type="button" data-dismiss="modal" id="btn_addsubChild" class="btn btn-default">
                                确定</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="HTLX" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择合同类型</h4>
        </div>
        <div class="modal-body">
            <div id="chooseCustomerCompact">
                <div class="row">
                    <div class="col-md-12">
                        <table id="customerCompactTable" class="table table-bordered table-striped table-condensed flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td>序号
                                </td>
                                <td>合同类型
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="NotShowUnitList" Value="" />
    <!-- 结构形式，建筑分类，建筑等级 -->
    <input type="hidden" id="structstring" name="a" value="<%= StructString %>" />
    <input type="hidden" id="structtypestring" name="b" value="<%= StructTypeString %>" />
    <input type="hidden" id="buildtypestring" name="c" value="<%= BuildTypeString %>" />
</asp:Content>
