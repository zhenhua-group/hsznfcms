﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_ShowCoprationBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_ShowCoprationBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/Coperation/cpr_ShowCoprationBymaster.js" type="text/javascript"></script>
    <style type="text/css">
        .tdcenter {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同信息查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right">
    </i><a>合同信息查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同信息查看
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">客户信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 13%">客户编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCst_No" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%">客户简称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_JC" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>客户名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCst_Name" runat="server"></asp:Label>
                                        </td>
                                        <td>公司地址:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCpy_Address" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>邮政编码:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCode" runat="server"></asp:Label>
                                        </td>
                                        <td>联系人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtLinkman" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>公司电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCpy_Phone" runat="server"></asp:Label>
                                        </td>
                                        <td>传真号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_Fax" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                    <h4 class="form-section">合同信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td>合同编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_No" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_cprid" runat="server" />
                                        </td>
                                        <td>合同文本编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_cprType" runat="Server">
                                            </asp:Label>
                                        </td>
                                        <td>合同分类:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddcpr_Type" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同名称:
                                        </td>
                                        <td colspan="5">
                                            <asp:Label ID="txt_cprName" runat="server"></asp:Label>
                                        </td>
                                       <%-- <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_buildType" runat="Server">
                                            </asp:Label>
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td>建设单位:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txt_cprBuildUnit" runat="server"></asp:Label>
                                        </td>
                                        <td>甲方类型:
                                            </td>
                                            <td>
                                                <asp:Label ID="drp_ChgJiaType"  runat="server"></asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td>结构形式:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="lbl_StructType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                        <td>建筑分类:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="lbl_BuildStructType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                        <td>建筑面积:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_buildArea" runat="Server">
                                            </asp:Label>㎡
                                        </td>
                                        <%--<td>层数:
                                        </td>
                                        <td>地上：<asp:Label ID="lbl_upfloor" runat="server" Text="0"></asp:Label>
                                            层 地下：<asp:Label ID="lbl_downfloor" runat="server" Text="0"></asp:Label>
                                            层
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td>工程负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_proFuze" runat="server" Width="120px"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_fzphone" runat="server"></asp:Label>
                                        </td>
                                        <td>子公司:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_cjbm" runat="server" Width="120px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtFParty" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_jiafphone" runat="server" Width="120px"></asp:Label>
                                        </td>
                                        <td>工程地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProjectPosition" runat="Server" Width="60px">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>原始合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_Account" runat="server"></asp:Label>
                                            万元
                                        </td>
                                         <td>实际合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_Account0" runat="server"></asp:Label>
                                            万元
                                        </td>
                                         <td>合同签订日期:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtSingnDate" runat="server"></asp:Label>
                                           
                                        </td>
                                       <%-- <td>投资额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtInvestAccount" runat="server"></asp:Label>
                                            万元
                                        </td>
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProfessionType" runat="Server" Width="60px">
                                            </asp:Label>
                                        </td>--%>
                                    </tr>
                                  <%--  <tr>
                                       
                                        <td>实际投资额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtInvestAccount0" runat="server"></asp:Label>
                                            万元
                                        </td>
                                        <td>工程来源:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddSourceWay" runat="Server" Width="60px">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同阶段:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="ddcpr_Stage" runat="Server" Width="150px"></asp:Label>
                                        </td>
                                        <td>制表人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_tbcreate" runat="server" Width="120px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                       
                                        <td>合同完成日期:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCompleteDate" runat="server"></asp:Label>
                                        </td>
                                        <td>合同统计年份:
                                        </td>
                                        <td>
                                             <asp:Label ID="txtSingnDate2" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>多栋楼:
                                        </td>
                                        <td colspan="5">
                                            <asp:Literal ID="txt_multibuild" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                      --%>
                                    <tr>
                                         <tr>
                                            <td>图纸套数:</td>
                                            <td>
                                                <div class="input-group">
                                                    总数：<asp:Label ID="txt_sumprint" runat="server" />
                                                    已出：<asp:Label ID="txt_print" runat="server" />
                                                </div>
                                            </td>
                                            <td>差旅次数:</td>
                                            <td>
                                                <div class="input-group">
                                                    总数：<asp:Label ID="txt_sumtravel" runat="server"  />
                                                    已出：<asp:Label ID="txt_travel" runat="server"  />
                                                </div>
                                            </td>
                                            <td>合同允许状态：
                                            </td>
                                            <td>
                                                <asp:Label ID="cpr_status" runat="server">                                                    
                                                </asp:Label>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td>合同备注:
                                        </td>
                                        <td colspan="5">
                                            <asp:Literal ID="txtcpr_Remark" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
        <%--            <h4 class="form-section">收费计划</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="sf_datas" style="width: 98%;" align="center">
                                        <tr id="sf_row">
                                            <td align="center" id="sf_id">收费次数
                                            </td>
                                            <td align="center" id="sf_bfb">百分比
                                            </td>
                                            <td align="center" id="sf_edu">付费额(万元)
                                            </td>                                          
                                            <td align="center" id="sf_time">付款时间
                                            </td>
                                            <td align="center" id="sf_mark">备注
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>--%>
                    <h4 class="form-section">合同签约信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="datas" style="width: 98%;" align="center">
                                        <tr id="sub_row">
                                            <td align="center" id="sub_id">序号
                                            </td>
                                            <td align="center" id="sub_name">协议名称
                                            </td>                                          
                                            <td align="center" id="sub_area">建筑面积(<span style="font-size: 10pt">㎡</span>)
                                            </td>
                                            <td align="center" id="Money">实际合同额(万元)
                                            </td>                                           
                                            <td align="center" id="sub_moneystatus">
                                                收费状态
                                            </td>
                                            <td align="center" id="Remark">备注
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                <%--    <h4 class="form-section">合同附件</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="datas_att" style="width: 98%;" align="center">
                                        <tr id="att_row">
                                            <td align="center" id="att_id">序号
                                            </td>
                                            <td align="center" id="att_filename">文件名称
                                            </td>
                                            <td align="center" id="att_filesize">文件大小
                                            </td>
                                            <td align="center" id="att_filetype">文件类型
                                            </td>
                                            <td align="center" id="att_uptime">上传时间
                                            </td>
                                            <td align="center" id="att_oper">操作
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>--%>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>关联合同收费信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="chargeDataTableSee" style="text-align: center; width: 98%; margin: 0 auto;" class="table table-striped table-bordered table-advance table-hover">
                                    <tr style="background-color: #f5f5f5;">
                                        <th style="width: 50px; text-align: center;">序号
                                        </th>
                                        <th style="width: 120px; text-align: center;">入账额(万元)
                                        </th>
                                        <th style="width: 60px; text-align: center;">比例
                                        </th>
                                        <th style="width: 70px; text-align: center;">汇款人
                                        </th>
                                        <th style="width: 70px; text-align: center;">入账人
                                        </th>
                                        <th style="width: 100px; text-align: center;">收款时间
                                        </th>
                                        <th style="width: 120px; text-align: center;">状态
                                        </th>
                                        <th style="width: 150px; text-align: center;">备注
                                        </th>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>关联项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" CssClass="table table-striped table-bordered table-advance table-hover" runat="server"
                                    AutoGenerateColumns="False" EnableModelValidation="True" ShowFooter="true" ShowHeader="true"
                                    Border="0" HorizontalAlign="Center" HeaderStyle-HorizontalAlign="center" Width="98%" HeaderStyle-BackColor="#f5f5f5" OnRowDataBound="GridView1_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                序号
                                            </HeaderTemplate>
                                            <FooterTemplate>
                                                合计:
                                            </FooterTemplate>
                                            <ItemStyle HorizontalAlign="center" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" CssClass="tdcenter" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href='../ProjectManage/ShowProjectBymaster.aspx?pro_id=<%#Eval("pro_ID") %>'><%#Eval("pro_name")%></a>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                项目名称
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="300" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" CssClass="tdcenter" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Unit" HeaderText="部门">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" CssClass="tdcenter" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PMName" HeaderText="项目经理">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" CssClass="tdcenter" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProjectScale" HeaderText="规模">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" CssClass="tdcenter" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                       

                                        <asp:BoundField DataField="pro_startTime" HeaderText="开始日期" DataFormatString="{0:d}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" CssClass="tdcenter" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="pro_finishTime" HeaderText="结束日期" DataFormatString="{0:d}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" CssClass="tdcenter" />
                                            <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="submit" class="btn green" id="btn_output">
                                导出</button>
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_flag" runat="server" />
</asp:Content>
