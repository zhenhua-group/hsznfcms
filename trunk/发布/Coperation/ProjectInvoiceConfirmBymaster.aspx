﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectInvoiceConfirmBymaster.aspx.cs" Inherits="TG.Web.Coperation.ProjectInvoiceConfirmBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="/js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/MessageComm.js"></script>
    <script src="../js/Coperation/ProjectInvoiceConfirm.js"></script>
    <style>
        .droplist_BackColor {
            background-color: #FFC;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">收费管理 <small>开票管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../../mainpage/WelcomePage.aspx">首页</a> <i
        class="fa fa-angle-right"></i><a>收费管理</a><i class="fa fa-angle-right"> </i><a>开票管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>
                        财务开票管理 <asp:Label ID="lbl_status" runat="server"></asp:Label>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="cls_data">
                        <table class="table table-bordered" style="width: 800px; margin: 0 auto;" id="td_input">
                            <tr>
                                <td style="width: 150px;">合同名称：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_cprname" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>合同号：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_number" runat="server"></asp:Label>
                                </td>
                            </tr>
                             <tr>
                                <td>项目名称：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_proname" runat="server"></asp:Label>
                                </td>
                            </tr>
                             <tr>
                                <td>合同总额：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_allcount" runat="server"></asp:Label>(万元)
                                </td>
                            </tr>
                            <tr>
                                <td>开票金额：
                                </td>
                                <td>
                                    <asp:Label ID="txt_payCount" runat="server"></asp:Label>(万元)
                                </td>
                            </tr>
                            <tr>
                                <td>票据类型：
                                </td>
                                <td>
                                    <asp:Label ID="drop_type" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px; ">开票单位全称：
                                </td>
                                <td>
                                    <asp:Label ID="txt_invoicename" runat="server"></asp:Label>
                                </td>
                            </tr>
                             <tr>
                                <td style="width: 150px; ">开票日期：
                                </td>
                                <td>
                                    <asp:Label ID="txt_times" runat="server"></asp:Label>
                                </td>
                            </tr>
                             <tr>
                                <td style="width: 150px; ">申请人原因：
                                </td>
                                <td>
                                    <asp:Label ID="txt_chargeRemark" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px; ">申请人：
                                </td>
                                <td>
                                    <asp:Label ID="lbl_ApplyUserName" runat="server"></asp:Label>
                                </td>
                            </tr>
                              <tr>
                                <td>增值税税号：
                                </td>
                                <td>
                                    <input type="text" id="txt_InvoiceCode" runat="Server" class="form-control" style="background-color: #FFC; width:50%;">
                                </td>
                            </tr>
                            <tr>
                                <td>国税注册地：
                                </td>
                                <td>
                                    <input type="text" id="txt_CountryAddress" runat="Server" class="form-control" style="background-color: #FFC;width:50%;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;">电话：
                                </td>
                                <td>

                                    <input type="text" id="txt_CountryPhone" runat="Server" class="form-control" style="background-color: #FFC;width:50%;">
                                
                                </td>
                            </tr>
                          
                          
                            <tr>
                                <td>开户银行：
                                </td>
                                <td>
                                    <input type="text" name="txt_Bankaccount" id="txt_Bankaccount" runat="Server" class="form-control" style="background-color: #FFC;width:50%;" />

                                </td>
                            </tr>
                              <tr>
                                <td>账号信息：
                                </td>
                                <td>
                                    <input type="text" name="txt_Accountinfo" id="txt_Accountinfo"  runat="Server" class="form-control" style="background-color: #FFC;width:50%;" />

                                </td>
                            </tr>
                            <tr>
                                <td>备注：
                                </td>
                                <td class="TextBoxBorder">
                                    <asp:TextBox ID="txt_Mark" runat="server"  Width="50%" TextMode="MultiLine" Height="50"
                                        CssClass="form-control"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="sureButton" runat="server" Text="确认通过"
                                        CssClass="btn green" OnClick="sureButton_Click" />
                                    <asp:Button ID="btn_NotSure" runat="server" Text="不通过"
                                        CssClass="btn red" OnClick="btn_NotSure_Click" />
                                    <input type="button" value="返回" onclick="javascript: history.back();" class=" btn default" />
                                </td>
                            </tr>
                        </table>
                        <table class="cls_show_cst_jiben" style="width: 800px; margin: 0 auto; font-size: 10pt; display: none; color: red"
                            id="tb_noDetails">
                            <tr>
                                <td>开票不通过或消息被删除！
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input type="button" value="返回" onclick="javascript: history.back();" class=" btn default" />
                                </td>
                            </tr>
                        </table>
                    </div>


                    <input id="hidMessageID" type="hidden" value='<%=MessageID %>' />
                    <input id="Hiddencm_FinancialNum" type="hidden" runat="server" />
                    <input type="hidden" id="hid_flag" runat="Server" value="0" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
