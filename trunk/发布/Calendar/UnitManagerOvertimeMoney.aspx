﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitManagerOvertimeMoney.aspx.cs" Inherits="TG.Web.Calendar.UnitManagerOvertimeMoney" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" lang="javascript">
        $(function () {

            var drp_year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
            var drp_month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
            var next_year = parseInt(drp_year);
            var next_month = (parseInt(drp_month) - 1);
            if (drp_month == "1") {
                next_year = (parseInt(drp_year) - 1);
                next_month = 12;
            }
            $("#title").html("部门经理加班补助" + next_year + "." + next_month + ".16 - " + drp_year + "." + drp_month + ".15");
            //角色权限为权限人员，可以修改系数
            if ($("#ctl00_ContentPlaceHolder1_isManagerOverTime").val() == "1" || $("#ctl00_ContentPlaceHolder1_previewPower").val() == "1") {
                if ($("#tbl tr>td[rel='bc']").length > 0) {
                    $("#btn_save").show();
                }
            }
            else {
                $(":text", "#tbl").attr("readonly", true);
                //部门经理只能看到自己
                if ($("#ctl00_ContentPlaceHolder1_previewPower").val() != "1") {
                    var memid = $("#ctl00_ContentPlaceHolder1_userSysNum").val();
                    //隐藏其他数据
                    $("#tbl tr:gt(0) td[memid='" + memid + "']").parent().siblings().css("display", "none");
                    //显示标题
                    $("#tbl tr:eq(0)").css("display", "");

                    $("#ctl00_ContentPlaceHolder1_btn_export").hide();
                }
            }
            //角色管理员
            if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "1") {
                //循环总额
                var startindex = 0;
                $("#tbl tr:gt(0) td[overcount]").each(function (j, n) {
                    var $hjtr=$(n).parent();                   
                    var overcount = $(n).attr("overcount");
                        //只有一条记录和一条总合计
                    if ($("#tbl tr:gt(" + startindex + "):lt(" + overcount + ")").length == 1) {
                            //【计算额】=【加班时间】*26 保留到整数                       
                        var $tr = $("#tbl tr:eq(" + (startindex+1) + ")");
                            var jbsj = $tr.children("td:eq(6)").text();
                            var jse = parseFloat(jbsj) * 26;
                            $tr.children("td:eq(8)").text(toThousands(Math.round(jse)));
                            $hjtr.find("td:eq(8)").text(toThousands(Math.round(jse)));

                        }
                        else {
                            // 设置：【加权值】=【月工资】*【加班时间】*【系数】/10000  保留小数点后两位
                            // 设置：【单位分配】=sum【加班时间】*26/sum【加权值】  
                            var $endtr = $hjtr;
                            var sumover = $endtr.find("td:eq(6)").text();//总加班时间
                            var sumjqz = $endtr.find("td:eq(8)").text();//总加权值
                            var dwfp = (parseFloat(sumover) * 26 / parseFloat(sumjqz)).toFixed(2);//单位分配

                            var sumjse = 0;
                            //循环部门经理,排除第一行和最后一行
                            $("#tbl tr:gt(" + startindex + "):lt(" + overcount + ")").each(function (i, k) {
                                var $tr = $(k);                              
                                //【计算额】=【加权值】*【单位分配】 保留到整数    
                                //加权值
                                var jqz = $tr.children("td:eq(8)").text();
                                //计算额
                                var jse = parseFloat(jqz) * parseFloat(dwfp);

                                //重新赋值计算额单元格
                                $tr.children("td:eq(8)").text(toThousands(Math.round(jse)));
                                sumjse = sumjse + Math.round(jse);
                            });
                            $endtr.find("td:eq(8)").text(toThousands(sumjse));
                        }
                    startindex =  $hjtr.index();
                });

            }
            else {
                if ($("#tbl tr:gt(0)").length > 0) {
                    //只有一条记录和一条总合计
                    if ($("#tbl tr:gt(0)").length == 2) {
                        //【计算额】=【加班时间】*26 保留到整数                       
                        var $tr = $("#tbl tr:eq(1)");
                        var jbsj = $tr.children("td:eq(6)").text();
                        var jse = parseFloat(jbsj) * 26;
                        $tr.children("td:eq(8)").text(toThousands(Math.round(jse)));
                        $("#tbl tr:last").find("td:eq(8)").text(toThousands(Math.round(jse)));

                    }
                    else {
                        // 设置：【加权值】=【月工资】*【加班时间】*【系数】/10000  保留小数点后两位
                        // 设置：【单位分配】=sum【加班时间】*26/sum【加权值】  
                        var $endtr = $("#tbl tr:last");
                        var sumover = $endtr.find("td:eq(6)").text();//总加班时间
                        var sumjqz = $endtr.find("td:eq(8)").text();//总加权值
                        var dwfp = (parseFloat(sumover) * 26 / parseFloat(sumjqz)).toFixed(2);//单位分配

                        var sumjse = 0;
                        //循环部门经理,排除第一行和最后一行
                        $("#tbl tr:not(:first,:last)").each(function (i, k) {
                            var $tr = $(k);
                            //【计算额】=【加权值】*【单位分配】 保留到整数    
                            //加权值
                            var jqz = $tr.children("td:eq(8)").text();
                            //计算额
                            var jse = parseFloat(jqz) * parseFloat(dwfp);

                            //重新赋值计算额单元格
                            $tr.children("td:eq(8)").text(toThousands(Math.round(jse)));
                            sumjse = sumjse + Math.round(jse);
                        });
                        $endtr.find("td:eq(8)").text(toThousands(sumjse));
                    }
                }
            }
            //保存数据
            $("#btn_save").click(function () {
                var arr = new Array();
                $("#tbl tr>td[rel='bc']").each(function (i, k) {
                    var $tr = $(k).parent();
                    var auo = {
                        mem_ID: $tr.find("td:eq(7) input").attr("id"),
                        Quotiety: $tr.find("td:eq(7) input").val(),
                        QuotaMoney: $tr.find("td:eq(9) input").val(),
                        OverYear: drp_year,
                        OverMonth: drp_month
                    }
                    arr.push(auo);
                });
                $.post("/HttpHandler/Calendar/CalendarHandler.ashx", { "action": "addUnitOvertime", "data": Global.toJSON(arr) }, function (result) {
                    if (result == "1") {
                        alert("保存成功！");
                        window.location.href = "UnitManagerOvertimeMoney.aspx";
                    }
                    else {
                        alert("保存失败！");
                    }
                });
            });
            //判断整数验证
            // var reg = /^\+?[0-9][0-9]*$/;
            //数字验证正则    
            var reg_math = /^[+-]?\d+(\.\d+)?$/;
            $(":text", "#tbl").live("change", function () {
                var _inputValue = $(this).val();
                if (!reg_math.test(_inputValue)) {
                    alert("请输入数字");
                    if ($(this).attr("id") != undefined && $(this).attr("id") != "") {
                        $(this).val("1");
                    }
                    else {
                        $(this).val("");
                    }
                }


            });

            //显示加载中
            $("#ctl00_ContentPlaceHolder1_btn_search").click(function () {
                $('body').modalmanager('loading');
            });
        });
        //数字千分位形式
        function toThousands(num) {
            var num = (num || 0).toString(), result = '';
            while (num.length > 3) {
                result = ',' + num.slice(-3) + result;
                num = num.slice(0, num.length - 3);
            }
            if (num) { result = num + result; }
            return result;
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>部门经理加班补助</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">加班费计算管理</a><i class="fa fa-angle-right"> </i><a href="#">部门经理加班补助</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询条件
                    </div>
                    <div class="actions"><asp:Button CssClass="btn btn-sm red " ID="btn_export" runat="server" OnClick="btn_export_Click" Text="导出"></asp:Button>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                           <%-- <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>--%>
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>    
                             <td style="width: 50px;">月份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>                       
                            <td>
                                <asp:Button CssClass="btn blue" Text="查询" ID="btn_search" OnClick="btn_search_Click" runat="server" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body form">
                    <table border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto;">
                        <tr>
                            <td id="title" style="text-align: center; font-size: 14px; font-weight: bold; line-height: 35px;"></td>
                        </tr>                       
                    </table>
                    <table id="tbl" class="table table-bordered table-responsive " style="width: 900px;margin: 0 auto; margin-bottom: 0px;">
                        <tr style="background-color:#f5f5f5; text-align:center;font-weight:bold;">  
                             <td style="width:120px;">部门名称</td>
                            <td style="width:60px;">序号</td>
                            <td style="width:100px;">姓名</td> 
                             <td style="width:120px;">月工资</td>                          
                             <td style="width:80px;">请假(h)</td>
                             <td style="width:80px;">迟到(次)</td>
                            <td style="width:80px;">加班(h)</td>
                            <td style="width:80px;">系数</td>
                            <td style="width:100px;">计算额</td>    
                             <td style="width:100px;">调配额</td>  
                             <td style="width:100px;">调整状态</td>            
                        </tr>
                        <asp:Literal ID="lithtml" runat="server"></asp:Literal>
                    </table>
                    <table  border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto; line-height:30px; ">
                        <tr>
                            <td style="text-align: center; "><input id="btn_save" style="display:none;" type="button" value="保存" class="btn btn-xs green" /></td>
                        </tr>                       
                    </table>
                </div>
            </div>
       
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="isManagerOverTime" runat="server" Value="0" />

</asp:Content>