﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="Apply.aspx.cs" Inherits="TG.Web.Calendar.Apply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datepicker/css/datepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-timepicker/compiled/timepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-colorpicker/css/colorpicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css">
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_proj.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/select2/select2_metro.css" />
    <script type="text/javascript" src="/js/assets/plugins/fuelux/js/spinner.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/clockface/js/clockface.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
    <script type="text/javascript" src="/js/assets/plugins/jquery-multi-select/js/jquery.quicksearch.js"></script>
    <script src="/js/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
    <script src="/js/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="/js/assets/scripts/form-components.js"></script>
    <script src="/js/assets/scripts/search.js"></script>
    <script src="/js/assets/scripts/form-validation.js"></script>
    <script src="/js/assets/scripts/ui-extended-modals.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="/js/Calendar/apply.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            FormComponents.init();
            Search.init();
        });

    </script>
    <style type="text/css">
        input[readonly] {
            background-color: #FFFFFF !important;
        }

        .dropdown-menu {
            min-width: 80px;
        }

        .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>考勤申请</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i
        class="fa fa-angle-right"> </i><a href="#">考勤申请</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-edit"></i>考勤申请</div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-striped table-condensed" id="data" style="width: 90%; margin: 0 auto;">
                        <tr>
                            <td style="width: 12%; font-weight: bold;">申请类型：</td>
                            <td>

                                <asp:RadioButtonList ID="RadioType" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="leave" Selected>请假</asp:ListItem>
                                    <asp:ListItem Value="travel">出差</asp:ListItem>
                                    <asp:ListItem Value="gomeet">外勤</asp:ListItem>
                                    <asp:ListItem Value="forget">未打卡</asp:ListItem>
                                    <asp:ListItem Value="company">公司活动</asp:ListItem>
                                    <asp:ListItem Value="depart">部门活动</asp:ListItem>
                                    <asp:ListItem Value="addwork">加班离岗</asp:ListItem>
                                </asp:RadioButtonList>

                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered table-striped table-condensed" id="tbl_leave" style="width: 90%; margin: 5px auto;">
                        <tr>
                            <td colspan="2" style="text-align: center;">请假申请单</td>
                        </tr>
                        <tr>
                            <td>请假事由：</td>
                            <td style="font-size: 12px;">

                                <asp:RadioButtonList ID="applyType" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="年假" Selected>年假</asp:ListItem>
                                    <asp:ListItem Value="事假">事假</asp:ListItem>
                                    <asp:ListItem Value="病假">病假</asp:ListItem>
                                    <asp:ListItem Value="婚假">婚假</asp:ListItem>
                                    <asp:ListItem Value="产/陪产假">产/陪产假</asp:ListItem>
                                    <asp:ListItem Value="丧假">丧假</asp:ListItem>
                                </asp:RadioButtonList>


                            </td>
                        </tr>
                        <tr>
                            <td style="width: 12%">请假时间：</td>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <input type="text" name="starttime" id="starttime" onclick="WdatePicker({  readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { if (dp.cal.getDateStr() != dp.cal.getNewDateStr()) { diff(dp.cal.getNewDateStr(), 'start', 'leave'); } } })"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>

                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime" id="endtime" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { if (dp.cal.getDateStr() != dp.cal.getNewDateStr()) { diff(dp.cal.getNewDateStr(), 'end', 'leave'); } } })"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>

                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td>考勤统计：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" runat="server" readonly="readonly" style="width: 100px;" id="totaltime" name="totaltime" value="7.5" />小时，（<font color="red">数字必填</font>）
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>备 注：</td>
                            <td>
                                <textarea class="form-control input-sm" id="applyContent" runat="server" name="applyContent" rows="3" style="width: 300px;"
                                    placeholder=""></textarea></td>
                        </tr>
                    </table>
                    <table class="table table-bordered table-striped table-condensed" id="tbl_travel" style="width: 90%; margin: 5px auto; display: none;">
                        <tr>
                            <td colspan="2" style="text-align: center;">差旅通知单</td>
                        </tr>
                        <tr>
                            <td style="width: 12%">项目名称：</td>
                            <td><div class="input-group">
                                <input type="text" id="proname" runat="server" value="" class="form-control input-sm" style="width: 300px;" /><font color="red">必填</font></div></td>
                        </tr>
                        <tr>
                            <td>出差地点：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" id="address" runat="server" value="" class="form-control input-sm" style="width: 300px;" /><font color="red">必填</font>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>出差时间：</td>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0" id="tableaddtime">
                                    <tr>
                                        <td>
                                            <input type="text" name="starttime7" id="starttime7" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'start', '',''); } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime7" id="endtime7" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime7\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'end', '','');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime2" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 </div>
                                        </td>

                                <%--        <td style="width: 200px;">
                                            <div class="input-icon">
                                                <i class="fa fa-calendar" style="margin: 7px 2px 4px 10px"></i>
                                                <input id="date" name="date" class="form-control input-medium date-picker" style="width: 180px !important;" readonly="true" type="text" value="<%= datastring %>"
                                                    data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years" />
                                            </div>
                                        </td>
                                        <td style="width: 150px;" class="sj">
                                            <div class="input-group">
                                                <input type="text" id="clockface_2" name="clockface_2" value="<%=towork %>" class="form-control" readonly="true" />
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" id="clockface_2_toggle"><i class="fa fa-clock-o"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td style="width: 150px;" class="sj">
                                            <div class="input-group">
                                                <input readonly="true" type="text" id="clockface_2_modal" name="clockface_2_modal" value="<%=offwork %>" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button" id="clockface_2_modal_toggle"><i class="fa fa-clock-o"></i></button>
                                                </span>
                                            </div>
                                        </td>--%>

                                    </tr>

                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" style="display:none;" id="tableNone">
                                	<tr >
                                		 <td>
                                            <input type="text" name="starttime11" id="starttime11" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'start', '','11');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime11" id="endtime11" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime11\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'end', '','11');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime11" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                    <tr>
                                		 <td>
                                            <input type="text" name="starttime12" id="starttime12" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','12');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime12" id="endtime12" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime12\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','12');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime12" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                    <tr>
                                		 <td>
                                            <input type="text" name="starttime13" id="starttime13" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','13');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime13" id="endtime13" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime13\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','13');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime13" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                    <tr>
                                		 <td>
                                            <input type="text" name="starttime14" id="starttime14" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'start', '','14');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime14" id="endtime14" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime14\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','14');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime14" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                    <tr>
                                		 <td>
                                            <input type="text" name="starttime15" id="starttime15" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','15'); }  }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime15" id="endtime15" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime15\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','15');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime15" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                    <tr>
                                		 <td>
                                            <input type="text" name="starttime16" id="starttime16" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','16');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime16" id="endtime16" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime16\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','16');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime16" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                    <tr>
                                		 <td>
                                            <input type="text" name="starttime17" id="starttime17" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','17');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime17" id="endtime17" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime17\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','17');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime17" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime18" id="starttime18" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','18');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime18" id="endtime18" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime18\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'end', '','18');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime18" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime19" id="starttime19" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','19');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime19" id="endtime19" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime19\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','19');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime19" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime20" id="starttime20" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','20');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime20" id="endtime20" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime20\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','20');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime20" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime21" id="starttime21" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','21');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime21" id="endtime21" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime21\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'end', '','21');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime21" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime22" id="starttime22" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','22');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime22" id="endtime22" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime22\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','22');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime22" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime23" id="starttime23" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', '','23');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime23" id="endtime23" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime23\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','23');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime23" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime24" id="starttime24" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'start', '','24');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime24" id="endtime24" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime24\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','24');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime24" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>
                                     <tr>
                                		 <td>
                                            <input type="text" name="starttime25" id="starttime25" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'start', '','25');  } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime25" id="endtime25" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime25\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', '','25');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                        <td>
                                             <div class="input-group">&nbsp;&nbsp;&nbsp;&nbsp;加班共计<input type="text" id="totaltime25" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时 <a rel="del" style="color:red" href="javascript:void(0)">删除</a></div>
                                        </td>
                                	</tr>

                                </table>
                            </td>
                        </tr>
                        <tr id="tr_btn">
                            <td></td>
                            <td><input type="button" value="添加" class="btn red btn-sm" id="btn_addtime" />                               
                            </td>
                        </tr>
                        <tr>
                            <td>备 注：</td>
                            <td>
                                <div class="input-group">
                                    <textarea class="form-control input-sm" runat="server" id="applyContent2" name="applyContent2" rows="3"
                                        placeholder="" style="width: 300px;"></textarea>(出差事由，甲方参与人员等)
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div class="input-group">
                                    <input type="checkbox" id="iscar" value="s" />自驾 &nbsp;&nbsp;&nbsp;&nbsp;公里数<input type="text" value="0" id="kilometre" runat="server" class="form-control input-sm" style="width: 100px;" />（<font color="red">数字，自驾公里数35（含）起予以补贴</font>）
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered table-striped table-condensed" id="tbl_gomeet" style="width: 90%; margin: 5px auto; display: none;">
                        <tr>
                            <td colspan="2" style="text-align: center;">外勤申请单</td>
                        </tr>
                        <tr>
                            <td style="width: 12%;">项目名称/事由：</td>
                            <td>
                                <input type="text" id="reason" runat="Server" value="" class="form-control input-sm" style="width: 300px;" /></td>
                        </tr>
                        <tr>
                            <td>地点：</td>
                            <td>
                                <input type="text" id="address2" runat="Server" value="" class="form-control input-sm" style="width: 300px;" /></td>
                        </tr>
                        <tr>
                            <td>时间：</td>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <input type="text" name="starttime2" id="starttime2" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'start', 'gomeet'); }, Hchanging: function (dp) { travel(dp.cal.getNewDateStr(), 'start', 'gomeet'); }, mchanging: function (dp) { travel(dp.cal.getNewDateStr(), 'start', 'gomeet'); }, onpicked: function (dp) { travel(dp.cal.getNewDateStr(), 'start', 'gomeet'); } }); "
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                                        <%--  <td>
                                            <input type="text" name="start_sj" id="start_sj" onclick="GoMeetFunc(this, 'start'); travel('', '', 'gomeet');"
                                                class="Wdate" runat="Server" style="width: 100px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>--%>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime" id="endtime2" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime2\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { travel(dp.cal.getNewDateStr(), 'end', 'gomeet'); }, Hchanging: function (dp) { travel(dp.cal.getNewDateStr(), 'end', 'gomeet'); }, mchanging: function (dp) { travel(dp.cal.getNewDateStr(), 'end', 'gomeet'); }, onpicked: function (dp) { travel(dp.cal.getNewDateStr(), 'end', 'gomeet'); } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />
                                        </td>
                                                          <%-- <td>
                                            <input type="text" name="start_sj" id="end_sj" onclick="GoMeetFunc(this, 'end'); travel('', '', 'gomeet');"
                                                class="Wdate" runat="Server" style="width: 100px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>                 --%>                      
                                       
                                    </tr>
                                </table>

                            </td>
                        </tr>
                         <tr>
                            <td></td>
                            <td>
                                <div class="input-group">加班共计<input type="text" id="totaltime3" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时（<font color="red">必填</font>）</div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div class="input-group">
                                    <input type="checkbox" id="iscar2" value="s" />自驾 &nbsp;&nbsp;&nbsp;&nbsp;公里数<input type="text" value="0" id="kilometre2" runat="Server" class="form-control input-sm" style="width: 100px;" />（<font color="red">数字，自驾公里数35（含）起予以补贴</font>）
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table class="table table-bordered table-striped table-condensed" id="tbl_forget" style="width: 90%; margin: 5px auto; display: none;">
                        <tr>
                            <td colspan="2" style="text-align: center;">未打卡申请单</td>
                        </tr>
                        <tr>
                            <td style="width: 12%;">申请时间：</td>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="width: 150px;">
                                            <input type="text" name="starttime3" id="starttime3" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'start', 'forget');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                       <%-- <td>
                                            <input type="text" name="start_sj2" id="start_sj2" onclick="WdatePicker({ readOnly: true, dateFmt: 'HH:mm', isShowClear: false })"
                                                class="Wdate" runat="Server" style="width: 100px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>--%>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td style="width: 150px;">
                                            <input type="text" name="endtime3" id="endtime3" onclick="WdatePicker({ maxDate: '#F{ControlDate(\'starttime3\')}', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) {  travel(dp.cal.getNewDateStr(), 'end', 'forget');  } });"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                      <%--  <td>
                                            <input type="text" name="end_sj2" id="end_sj2" onclick="WdatePicker({ readOnly: true, dateFmt: 'HH:mm', isShowClear: false })"
                                                class="Wdate" runat="Server" style="width: 100px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>--%>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                         <tr>
                            <td></td>
                            <td>
                                <div class="input-group">加班共计<input type="text" id="totaltime7" value="0" runat="server" readonly class="form-control input-sm" style="width: 50px;" />小时（<font color="red">必填</font>）</div>
                            </td>
                        </tr>
                        <tr>
                            <td>原因：</td>
                            <td>
                                <textarea class="form-control input-sm" runat="Server" id="applyContent3" name="applyContent3" rows="3"
                                    placeholder="" style="width: 300px;"></textarea></td>
                        </tr>
                    </table>

                    <table class="table table-bordered table-striped table-condensed" id="tbl_company" style="width: 90%; margin: 5px auto; display: none;">
                        <tr>
                            <td colspan="2" style="text-align: center;">公司活动申请单</td>
                        </tr>
                        <tr>
                            <td>活动事由：</td>
                            <td style="font-size: 12px;">

                                <asp:RadioButtonList ID="applyType4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="考察游" Selected>考察游</asp:ListItem>
                                    <asp:ListItem Value="大游">大游</asp:ListItem>
                                    <asp:ListItem Value="小游">小游</asp:ListItem>
                                    <asp:ListItem Value="年会">年会</asp:ListItem>
                                    <asp:ListItem Value="其它">其它</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 12%">申请时间：</td>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <input type="text" name="starttime" id="starttime4" onclick="WdatePicker({ startDate: '%y-%M-%d 00:00:00', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { if (dp.cal.getDateStr() != dp.cal.getNewDateStr()) { diff(dp.cal.getNewDateStr(), 'start', 'company'); } } })"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>

                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime" id="endtime4" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', startDate: '%y-%M-%d 00:00:00', isShowClear: false, onpicking: function (dp) { if (dp.cal.getDateStr() != dp.cal.getNewDateStr()) { diff(dp.cal.getNewDateStr(), 'end', 'company'); } } })"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>

                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td>考勤统计：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" runat="server" readonly="readonly" style="width: 100px;" id="totaltime4" name="totaltime" value="7.5" />小时，（<font color="red">数字必填</font>）
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>备 注：</td>
                            <td>
                                <textarea class="form-control input-sm" id="applyContent4" runat="server" name="applyContent" rows="3" style="width: 300px;"
                                    placeholder=""></textarea></td>
                        </tr>
                         <tr>
                            <td></td>
                            <td>
                                <div class="input-group">
                                    <input type="checkbox" id="iscar4" value="s" />自驾 &nbsp;&nbsp;&nbsp;&nbsp;公里数<input type="text" value="0" id="kilometre4" runat="Server" class="form-control input-sm" style="width: 100px;" />（<font color="red">数字，自驾公里数35（含）起予以补贴</font>）
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table class="table table-bordered table-striped table-condensed" id="tbl_depart" style="width: 90%; margin: 5px auto; display: none;">
                        <tr>
                            <td colspan="2" style="text-align: center;">部门活动申请单</td>
                        </tr>
                        <tr>
                            <td>活动事由：</td>
                            <td style="font-size: 12px;">

                                <asp:RadioButtonList ID="applyType5" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="组织活动" Selected>组织活动</asp:ListItem>
                                    <asp:ListItem Value="聚餐">聚餐</asp:ListItem>
                                    <asp:ListItem Value="其它">其它</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 12%">申请时间：</td>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <input type="text" name="starttime" id="starttime5" onclick="WdatePicker({ startDate: '%y-%M-%d 00:00:00', readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', isShowClear: false, onpicking: function (dp) { if (dp.cal.getDateStr() != dp.cal.getNewDateStr()) { diff(dp.cal.getNewDateStr(), 'start', 'depart'); } } })"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>

                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="endtime" id="endtime5" onclick="WdatePicker({ readOnly: true, dateFmt: 'yyyy-MM-dd HH:mm', startDate: '%y-%M-%d 00:00:00', isShowClear: false, onpicking: function (dp) { if (dp.cal.getDateStr() != dp.cal.getNewDateStr()) { diff(dp.cal.getNewDateStr(), 'end', 'depart'); } } })"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>

                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td>考勤统计：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" runat="server" readonly="readonly" style="width: 100px;" id="totaltime5" name="totaltime" value="7.5" />小时，（<font color="red">数字必填</font>）
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>备 注：</td>
                            <td>
                                <textarea class="form-control input-sm" id="applyContent5" runat="server" name="applyContent" rows="3" style="width: 300px;"
                                    placeholder=""></textarea></td>
                        </tr>
                    </table>

                    <table class="table table-bordered table-striped table-condensed" id="tbl_addwork" style="width: 90%; margin: 5px auto; display: none;">
                        <tr>
                            <td colspan="2" style="text-align: center;">加班离岗申请单</td>
                        </tr>
                        <tr>
                            <td>申请事由：</td>
                            <td style="font-size: 12px;">

                                <asp:RadioButtonList ID="applyType6" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="加班时中途外出" Selected>加班时中途外出</asp:ListItem>
                                    <asp:ListItem Value="参加活动">参加活动</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 12%">申请时间：</td>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="width: 150px;">
                                            <input type="text" name="starttime6" id="starttime6" onclick="WdatePicker({ startDate: '%y-%M-%d', readOnly: true, dateFmt: 'yyyy-MM-dd', isShowClear: false })"
                                                class="Wdate" runat="Server" style="width: 130px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td>
                                            <input type="text" name="start_sj6" id="start_sj6" onclick="commFunc(this, 'start')"
                                                class="Wdate" runat="Server" style="width: 100px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>
                                        <td style="width: 50px; text-align: center;" class="sj">至
                                        </td>
                                        <td>
                                            <input type="text" name="end_sj6" id="end_sj6" onclick="commFunc(this, 'end')"
                                                class="Wdate" runat="Server" style="width: 100px; height: 22px; border: 1px solid #e5e5e5;" />

                                        </td>

                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td>考勤统计：</td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" runat="server" readonly="readonly" style="width: 100px;" id="totaltime6" name="totaltime" value="7.5" />小时，（<font color="red">数字必填</font>）
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>备 注：</td>
                            <td>
                                <textarea class="form-control input-sm" id="applyContent6" runat="server" name="applyContent" rows="3" style="width: 300px;"
                                    placeholder=""></textarea></td>
                        </tr>
                    </table>

                    <table border="0" cellspacing="0" cellpadding="0" width="90%">
                        <tr>
                            <td align="center" colspan="2">
                                <button type="button" class="btn green" id="btn_save">
                                    申请
                                </button>
                                <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                    返回
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <%-- <input type="hidden" name="daytime" id="applyID" value="<%=applyID %>">--%>

    <input type="hidden" name="daytime" id="hid_clockface_2" value="<%=tx_towork %>">
    <input type="hidden" name="daytime" id="hid_clockface_2_modal" value="<%=tx_offwork %>">
    <input type="hidden" name="userid" id="userid" value="<%=UserSysNo %>" />
    <asp:HiddenField ID="hid_applytype" runat="server" Value="" />
    <asp:HiddenField ID="hid_applyID" runat="server" Value="0" />
    <asp:HiddenField ID="hid_iscar" runat="server" Value="" />
</asp:Content>
