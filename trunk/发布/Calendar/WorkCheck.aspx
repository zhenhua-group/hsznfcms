﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="WorkCheck.aspx.cs" Inherits="TG.Web.Calendar.WorkCheck" %>

<%@ Register TagPrefix="cc1" Namespace="DataControls" Assembly="DataCalendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_proj.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/js/assets/plugins/select2/select2_metro.css" />
    <script type="text/javascript" src="/js/assets/plugins/select2/select2.min.js"></script>
    <script src="/js/assets/scripts/ui-extended-modals.js"></script>

    <link href="/css/common.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/Calendar/workcheck.js"></script>
    <style type="text/css">
        .cssHead {
            /* background-color: rgb(255, 184, 72);*/
            font-size: 18px;
            font-weight: bold;
            background-color: #f9f9f9;
            text-align: center;
        }

        .otherBG {
            /* background-color: #f0f0f0;*/
        }

        .kqdj a:hover {
            text-decoration: underline;
            color: #2a6496;
        }

        .kqdj a {
            font-family: "宋体";
            font-size: 10px;
            color: #2a6496;
        }

        .actions {
            position: relative;
            top: -2px;
            right: 0;
        }

        .portlet > .portlet-title > .tools, .portlet > .portlet-title > .actions {
            margin-top: 0px;
        }

        .select2-container .select2-choice {
            height: 26px;
            padding: 0px 0 0 8px;
        }
    </style>
    <script type="text/javascript">
        my_top = 50;
        my_left = 50;

        function my_note(CAL_ID) {
            my_top += 25;
            my_left += 15;

            window.open("note.aspx?cal_id=" + CAL_ID, "note_win" + CAL_ID, "height=200,width=250,status=0,toolbar=no,menubar=no,location=no,scrollbars=auto,top=" + my_top + ",left=" + my_left + ",resizable=no");
        }

        function My_Submit() {
          
            document.aspnetForm.submit();
        }

        function set_year(op) {

            if (op == -1 && $("#ctl00_ContentPlaceHolder1_selectYear").get(0).selectedIndex == 0)
                return;
            if (op == 1 && $("#ctl00_ContentPlaceHolder1_selectYear").get(0).selectedIndex == ($("#ctl00_ContentPlaceHolder1_selectYear").get(0).options.length - 1))
                return;
            $("#ctl00_ContentPlaceHolder1_selectYear").get(0).selectedIndex = $("#ctl00_ContentPlaceHolder1_selectYear").get(0).selectedIndex + op;

            My_Submit();
        }

        function set_mon(op) {
            if (op == -1 && $("#ctl00_ContentPlaceHolder1_selectMonth").get(0).selectedIndex == 0)
                return;
            if (op == 1 && $("#ctl00_ContentPlaceHolder1_selectMonth").get(0).selectedIndex == ($("#ctl00_ContentPlaceHolder1_selectMonth").get(0).options.length - 1))
                return;
            $("#ctl00_ContentPlaceHolder1_selectMonth").get(0).selectedIndex = $("#ctl00_ContentPlaceHolder1_selectMonth").get(0).selectedIndex + op;

            My_Submit();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>考勤申请</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i
        class="fa fa-angle-right"> </i><a href="#">考勤申请</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box  blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-edit"></i>考勤信息
                    </div>
                    <div class="actions">
                        <div class="input-group" style="line-height: 26px;">
                            选择人员：&nbsp;<asp:DropDownList ID="drp_unit" runat="Server" CssClass="form-control input-small" Width="90px" Height="28px" AppendDataBoundItems="True">
                                <asp:ListItem Value="-1">全部部门</asp:ListItem>
                            </asp:DropDownList>
                            <select id="userlist" runat="server" class="form-control input-small select2me" style="width:90px;">                                  
                                </select>
                           <%-- <asp:DropDownList ID="userlist" runat="Server" CssClass="form-control input-small select2me" Width="90px" >                             
                            </asp:DropDownList>--%>
                        </div>
                    </div>

                </div>
                <div class="portlet-body">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="text-align: center">
                        <tr>
                            <td align="center" width="100%">
                                <div align="center" style="text-align: center; width: 60%; margin: 0px auto; padding-top: 10px; vertical-align: center;">
                                    <div class=" input-group" style="text-align: center;">
                                        <div class=" input-group-btn">
                                            <!-------------- 年 ------------>
                                            <input type="button" value="〈" class="btn  blue" title="上一年" onclick="set_year(-1);">
                                        </div>
                                        <div class=" input-group-btn">
                                            <select name="YEAR" class="form-control" style="width: 90px;" onchange="My_Submit();"
                                                id="selectYear" runat="server">
                                            </select>
                                        </div>
                                        <div class=" input-group-btn">
                                            <input type="button" value="〉" class="btn blue" title="下一年" onclick="set_year(1);">
                                        </div>
                                        <div class=" input-group-btn">
                                            <label class="control-label ">
                                                年</label>
                                        </div>
                                        <!-------------- 月 ------------>
                                        <div class=" input-group-btn">
                                            <input type="button" value="〈" class="btn blue" title="上一月" onclick="set_mon(-1);">
                                        </div>
                                        <div class=" input-group-btn">
                                            <select name="MONTH" style="width: 60px;" class="form-control" onchange="My_Submit();"
                                                runat="server" id="selectMonth">
                                                <option value="1">01</option>
                                                <option value="2">02</option>
                                                <option value="3">03</option>
                                                <option value="4">04</option>
                                                <option value="5">05</option>
                                                <option value="6">06</option>
                                                <option value="7">07</option>
                                                <option value="8">08</option>
                                                <option value="9">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                        <div class=" input-group-btn">
                                            <input type="button" value="〉" class="btn blue" title="下一月" onclick="set_mon(1);">
                                        </div>
                                        <div class=" input-group-btn">
                                            <label class="control-label ">
                                                月</label>
                                        </div>
                                        <div class=" input-group-btn">
                                            <input type="button" value="本月" class="btn blue" title="本月" onclick="location = 'WorkCheck.aspx';">
                                        </div>
                                        <%--<div class=" input-group-btn">
                                                <input type="button" id="btn_count" value="申请统计" class="btn blue" title="申请统计" data-target="#info" data-toggle="modal" />
                                            </div>--%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <cc1:DataCalendar ID="DataCalendar1" runat="server" ShowNextPrevMonth="true" ShowTitle="false" SelectionMode="Day"
                        CssClass="table-zj fc-border-separate" DayField="dateTime" Width="95%" EnableViewState="False"
                        OnSelectionChanged="DataCalendar1_SelectionChanged">
                        <TodayDayStyle BackColor="#FFED95"></TodayDayStyle>
                        <OtherMonthDayStyle ForeColor="DarkGray" CssClass="otherBG"></OtherMonthDayStyle>
                        <TitleStyle Font-Bold="True" CssClass="cssHead"></TitleStyle>
                        <DayStyle Font-Size="15pt" Font-Names="sans-serif" ForeColor="Black" HorizontalAlign="Center"
                            VerticalAlign="Top" CssClass="TableData"></DayStyle>
                        <DayHeaderStyle CssClass="cssHead" Font-Bold="true" Height="40"></DayHeaderStyle>

                        <itemtemplate>
                              
					<%--<table width="100%" class="kqdj">
						<tr>
							<td align="left">	                                						
								<span style="font-size:12px">
									<%# DateTime.Parse(Container.DataItem["StartTime"].ToString()).ToString("t") %>
									-<%# DateTime.Parse(Container.DataItem["EndTime"].ToString()).ToString("t") %></span><br>
								<span style="font-size:12px;font-weight: bold">
									<%# (Convert.ToInt32(Container.DataItem["CalType"].ToString()) ==1)?"工作":"个人" %>
									:</span><a style="CURSOR: hand;font-size:12px;color:#0000ff" onclick='javascript:my_note(<%# Container.DataItem["CalId"] %>);' target="_blank">
									<%# Container.DataItem["Content"]!=DBNull.Value?Container.DataItem["Content"].ToString().Replace("\n","<br>"):""%>
									</a>
									<span style="font-size:12px">
									<%# (Container.DataItem["ManagerUserName"] != DBNull.Value) ? "<br>安排人：" + Container.DataItem["ManagerUserName"].ToString() : "" %>
									</span>
                                	</td>
                            </tr>                       
                      
					</table>--%>
				        </itemtemplate>
                        <noeventstemplate></noeventstemplate>
                    </cc1:DataCalendar>
                </div>
            </div>
        </div>
    </div>
    <!--审批信息查看-->
    <div id="AuditMsg" class="modal fade yellow" tabindex="-1" data-width="600"
        aria-hidden="true" style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title" style="font-weight: bold;" id="divtitle">申请记录</h4>
        </div>
        <div class="modal-body">
            <table class="table table-striped table-bordered" width="100%">
                <tr>
                    <td style="font-weight: bold;">申请状态：
                    </td>
                    <td>
                        <label id="applycontent"></label>
                    </td>

                </tr>
                <tr>
                    <td style="font-weight: bold;">申请类型：
                    </td>
                    <td>
                        <label id="applysort"></label>
                    </td>

                </tr>
                <tr>
                    <td style="font-weight: bold;">申请原因：
                    </td>
                    <td>
                        <label id="reason"></label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">时 间：
                    </td>
                    <td>
                        <label id="applytime"></label>
                    </td>
                </tr>
                <tr id="kqtj">
                    <td style="font-weight: bold;">考勤统计：
                    </td>
                    <td>
                        <label id="totaltime"></label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; width: 100px;">申请人：
                    </td>
                    <td>
                        <label id="applyname"></label>
                        <input type="hidden" id="applyid" value="0">
                    </td>

                </tr>

                <tr>
                    <td style="font-weight: bold;">备 注：
                    </td>
                    <td>
                        <label id="remark"></label>
                    </td>
                </tr>

            </table>
            <div class="ui-dialog-buttonset" style="text-align: right;">

                <%--<button type="button" id="btn_submit" class="btn blue" style="padding-top: 3px; padding-bottom: 3px;">
                        提交</button>--%>
                <button type="button" id="btn_gb" class="btn default" style="padding-top: 3px; padding-bottom: 3px;">
                    关闭</button>
            </div>
        </div>
    </div>
    <!--月统计申请信息查看-->
    <div id="info" class="modal fade yellow" tabindex="-1" data-width="800"
        aria-hidden="true" style="display: none; width: 800px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" id="btn_close2" class="close" data-dismiss="modal" aria-hidden="true">
            </button>

            <h4 class="modal-title" style="font-weight: bold;" id="divtitle2">申请记录</h4>

        </div>
        <div class="modal-body" id="modalAddDiv">
            <table class="table table-bordered table-striped table-condensed" width="100%" style="text-align: center;" id="data">
                <tr>
                    <td style="font-weight: bold; width: 50px;"></td>
                    <td style="font-weight: bold; width: 80px;">申请人</td>
                    <td style="font-weight: bold; width: 200px;">申请时间</td>
                    <td style="font-weight: bold; width: 70px;">类 型</td>
                    <td style="font-weight: bold">原 因</td>
                    <td style="font-weight: bold; width: 100px;">状态</td>
                </tr>
            </table>

            <div class="ui-dialog-buttonset" style="text-align: right;">
                <div class="checkbox-list" style="text-align: left;" id="checkboxdiv">
                    <label class="checkbox-inline">
                        <div class="checker" id="checkalldiv">
                            <span>
                                <input type="checkbox" id="checkall" value="a" name="ckbname"></span>
                        </div>
                        全选</label>
                    <label class="checkbox-inline">
                        <div class="checker" id="checkreturndiv">
                            <span>
                                <input type="checkbox" id="checkreturn" value="b" name="ckbname"></span>
                        </div>
                        反选</label>
                    <label class="checkbox-inline">
                        <div class="checker" id="checkcanclediv">
                            <span>
                                <input type="checkbox" id="checkcancle" value="c" name="ckbname"></span>
                        </div>
                        取消</label>
                </div>

                <button type="button" id="btn_sp" class="btn blue" style="padding-top: 3px; padding-bottom: 3px;">
                    批量审批
                </button>
                <button type="button" id="btn_gb2" class="btn default" style="padding-top: 3px; padding-bottom: 3px;">
                    关闭</button>

            </div>
        </div>
    </div>
    <input type="hidden" id="UserID" value="<%=UserID %>" />
    <input type="hidden" id="currentpassword" value="<%=currentpassword %>" />
    <input type="hidden" id="currentuserid" value="<%=UserSysNo %>" />
    <input type="hidden" id="HidCurrtDate" runat="server" value="" />
    <input type="hidden" id="hiddenIstaster" runat="server" value="" />
    <input type="hidden" id="hiddentaster" runat="server" value="" />
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_userid" runat="server" Value="<%=UserSysNo %>" />

</asp:Content>
