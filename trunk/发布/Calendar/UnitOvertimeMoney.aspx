﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitOvertimeMoney.aspx.cs" Inherits="TG.Web.Calendar.UnitOvertimeMoney" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" lang="javascript">
        $(function () {
            

            var drp_year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
            var drp_month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
            var next_year = parseInt(drp_year);
            var next_month = (parseInt(drp_month) - 1);
            if (drp_month == "1") {
                next_year = (parseInt(drp_year) - 1);
                next_month = 12;
            }
            $("#title").html("部门加班补助"+next_year + "." + next_month + ".16 - " + drp_year + "." + drp_month + ".15");
           
            //隐藏没有人员加班的部门
            //循环部门名称
            $("#tbl tr td[rowspan]").each(function (i, k) {
                var trindex = $(k).parent().index();
                var rownum = $(k).attr("rowspan");
                var unitname = $(k).text();
                var rygs = $("#tbl tr>td[unitname='" + unitname + "']").length;
              
                //等于1，没有人员记录，部门隐藏
                if (rygs == 1) {
                    //gt(3)以后，索引为0，1,2,3的已经被排除了，那么剩下的有重新计数，下标又从0开始。
                    $("#tbl tr:gt("+(trindex - 1)+"):lt("+rownum+")").css("display", "none");
                }
                else if(rygs==2)//等于2，只有一条记录
                {
                    var $tr = $("#tbl tr>td[unitname='" + unitname + "']:eq(0)").parent();
                    //加班时间
                    var jbsj = $tr.children("td:eq(-5)").text();                    
                    //【计算额】=【加班时间】*26 保留到整数
                    var jse = parseFloat(jbsj) * 26;
                    $tr.children("td:eq(-3)").text(toThousands(Math.round(jse)));
                    //总额修改
                    $("#tbl tr>td[unitname='" + unitname + "']:last").parent().children("td:eq(-3)").text(toThousands(Math.round(jse)));
                }
                else
                {
                    // 设置：【加权值】=【月工资】*【加班时间】*【系数】/10000  保留小数点后两位
                    // 设置：【单位分配】=sum【加班时间】*26/sum【加权值】  
                    //【计算额】=【加权值】*【单位分配】 保留到整数
                    var $endtr = $("#tbl tr>td[unitname='" + unitname + "']:last").parent();//总额最后一行
                    var sumover = $endtr.children("td:eq(-5)").text();//总加班时间
                    var sumjqz = $endtr.children("td:eq(-3)").text();//总加权值
                    var dwfp = (parseFloat(sumover)*26 / parseFloat(sumjqz)).toFixed(2);//单位分配
                    var sumjse = 0;//总计算额
                    //部门下每个人
                    $("#tbl tr>td[unitname='" + unitname + "']:not(:last)").each(function (j, n) {
                        //加权值
                        var jqz = $(n).parent().children("td:eq(-3)").text();
                        //计算额
                        var jse = parseFloat(jqz) * parseFloat(dwfp);
                       
                        //重新赋值计算额单元格
                        $(n).parent().children("td:eq(-3)").text(toThousands(Math.round(jse)));
                        sumjse = sumjse + Math.round(jse);
                    });
                    //总额赋值
                    $endtr.children("td:eq(-3)").text(toThousands(sumjse));
                    
                }

                
            });
           
            //部门经理和管理员角色,可以看工资
            if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "5" || $("#ctl00_ContentPlaceHolder1_previewPower").val() == "44" || $("#ctl00_ContentPlaceHolder1_ismanager").val() == "1") {
                
                //默认工资列显示
                $("#tbl tr").each(function () {
                    $(this).find("td:eq(-8)").css("display", "");
                });

            }
            else
            {               
                //隐藏导出按钮
                $("#ctl00_ContentPlaceHolder1_btn_export").hide();

                //只显示自己
                var memid = $("#ctl00_ContentPlaceHolder1_userSysNum").val();
               
                var $tr = $("#tbl tr:gt(0) td[memid='" + memid + "']").parent();
                //隐藏其他数据
                $tr.siblings().css("display", "none");
                //显示标题
                $("#tbl tr:eq(0)").css("display", "");

                if ($tr.index() > 1) {
                    var $td = $tr.find("td:eq(0)");
                    var unitname = $td.attr("unitname");
                    $td.before("<td>" + unitname + "</td>");
                }
          

            }
            //角色权限为显示部门经理
            if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "5" || $("#ctl00_ContentPlaceHolder1_previewPower").val() == "44" || $("#ctl00_ContentPlaceHolder1_ismanager").val() == "1")
            {
                if ($("#tbl tr>td[rel='bc']").length>0) {
                    $("#btn_save").show();
                }
               
            }
            else
            {
                $(":text", "#tbl").attr("readonly", true);
            }
            //保存数据
            $("#btn_save").click(function () {
                var arr=new Array();
                $("#tbl tr>td[rel='bc']").each(function (i,k) {
                    var $tr=$(k).parent();                   
                    var auo={
                        mem_ID:$tr.find("td:eq(-4) input").attr("id"),
                        Quotiety: $tr.find("td:eq(-4) input").val(),
                        QuotaMoney: $tr.find("td:eq(-2) input").val(),
                        OverYear:drp_year,
                        OverMonth:drp_month
                    }
                    arr.push(auo);                    
                    });
                    $.post("/HttpHandler/Calendar/CalendarHandler.ashx", { "action": "addUnitOvertime", "data": Global.toJSON(arr) }, function (result) {
                        if (result == "1") {
                            alert("保存成功！");
                            window.location.href = "UnitOvertimeMoney.aspx";
                        }
                        else {
                            alert("保存失败！");
                        }
                });
            });

            //判断整数验证
           // var reg = /^\+?[0-9][0-9]*$/;
            //数字验证正则    
            var reg_math = /^[+-]?\d+(\.\d+)?$/;
            $(":text", "#tbl").live("change", function () {
                var _inputValue = $(this).val();
                if (!reg_math.test(_inputValue)) {
                    alert("请输入数字");                  
                    if ($(this).attr("id")!=undefined&&$(this).attr("id") != "")
                    {
                        $(this).val("1");
                    }
                    else {
                        $(this).val("");
                    }                   
                }
                

            });

            //显示加载中
            $("#ctl00_ContentPlaceHolder1_btn_search").click(function () {
                $('body').modalmanager('loading');
            });
        });
        //数字千分位形式
        function toThousands(num) {
            var num = (num || 0).toString(), result = '';
            while (num.length > 3) {
                result = ',' + num.slice(-3) + result;
                num = num.slice(0, num.length - 3);
            }
            if (num) { result = num + result; }
            return result;
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>部门加班费管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">加班费计算管理</a><i class="fa fa-angle-right"> </i><a href="#">部门加班费管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询条件
                    </div>
                    <div class="actions"><asp:Button CssClass="btn btn-sm red " ID="btn_export" runat="server" OnClick="btn_export_Click" Text="导出"></asp:Button>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                            <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>    
                             <td style="width: 50px;">月份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>                       
                            <td>
                                <asp:Button CssClass="btn blue" Text="查询" ID="btn_search" OnClick="btn_search_Click" runat="server" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body form">
                    <table border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto;">
                        <tr>
                            <td id="title" style="text-align: center; font-size: 14px; font-weight: bold; line-height: 35px;"></td>
                        </tr>                       
                    </table>
                    <table id="tbl" class="table table-bordered table-responsive " style="width:900px; margin: 0 auto; margin-bottom: 0px;">
                        <tr style="background-color:#f5f5f5; text-align:center;font-weight:bold;">                           
                            <td style="width:120px;">部门名称</td>
                             <td style="width:60px;">序号</td>
                            <td style="width:100px;">姓名</td>
                             <td style="width:120px; display:none;">月工资</td>
                             <td style="width:80px;">请假(h)</td>
                             <td style="width:80px;">迟到(次)</td>
                            <td style="width:80px;">加班(h)</td>
                            <td style="width:80px;">系数</td>
                            <td style="width:100px;">计算额</td>    
                             <td style="width:100px;">调配额</td>    
                            <td style="width:100px;">调整状态</td>                         
                        </tr>
                        <asp:Literal ID="lithtml" runat="server"></asp:Literal>
                    </table>
                     <table  border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto; line-height:30px; ">
                        <tr>
                            <td style="text-align: center; "><input id="btn_save" style="display:none;" type="button" value="保存" class="btn btn-xs green" /></td>
                        </tr>                       
                    </table>
                </div>
            </div>
         <%--  <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                CustomInfoSectionWidth="35%" OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox"
                PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left"
                ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到"
                PageIndexBoxStyle="width:25px;" PageSize="20" SubmitButtonClass="submitbtn">
            </webdiyer:AspNetPager>--%>

        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
     <asp:HiddenField runat="server" ID="ismanager" Value="0" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />


</asp:Content>