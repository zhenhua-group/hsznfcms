﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ForgetTimeListByMaster.aspx.cs" Inherits="TG.Web.Calendar.ForgetTimeListByMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Calendar/ForgetTimeList_jq.js"></script>
    <script type="text/javascript" src="../js/Calendar/LeaveListAudit.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>未打卡申请列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">未打卡申请列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询申请信息
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                            <td style="width: 65px;">生产部门:</td>
                            <td style="width: 150px;">

                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 50px;">人员:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_user" CssClass="form-control" Width="100px" runat="server"
                                    AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="100px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td style="width: 50px;">月份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="100px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                             <td style="width: 60px;">需要审批:</td>
                            <td style="width: 50px;">  
                                <input type="checkbox" name="drp_audit" id="drp_audit" value="1">显示
                            </td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_search" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>未打卡申请列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                       
                        <input type="button" id="btn_unitaudit" value="部门经理批量审批" class="btn  red btn-sm" data-toggle="modal" />
                        <input type="button" id="btn_manageraudit" value="总经理批量审批" class="btn  red btn-sm" data-toggle="modal" />
                    <input type="button" id="btn_done" value="已处理" class="btn  red btn-sm" runat="server" />
                         </div>
                </div>
                <div class="portlet-body form">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1_1" data-toggle="tab">未处理</a></li>
                        <li class=""><a href="#tab_1_2" data-toggle="tab">已处理</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_1" style="width: 100%">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                        <div class="tab-pane fade " id="tab_1_2" style="width: 100%">
                            <table id="jqGrid2">
                            </table>
                            <div id="gridpager2">
                            </div>
                            <div id="nodata2" class="norecords">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="isapplymanager" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField runat="server" ID="SpecialPersonAuditID" Value="0" />
    <asp:HiddenField runat="server" ID="SpecialPersonAudit" Value="0" />
    <asp:HiddenField runat="server" ID="SpecialPerson" Value="" />
    <input type="hidden" id="listType" value="unit" />
    <div id="AuditList" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                <label id="lbl_title"></label>
            </h4>
        </div>
        <div class="modal-body" id="auditShow">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="text-align: center; line-height: 30px;">
                <tr>
                    <td>是否全部通过申请？</td>
                </tr>
                <tr>
                    <td>
                        <input type="button" id="btn_yes" value="Y" class="btn  green" />&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="btn_no" value="N" class="btn  red" /></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_CanceAudit" class="btn btn-default">
                关闭</button>
        </div>
    </div>

</asp:Content>
