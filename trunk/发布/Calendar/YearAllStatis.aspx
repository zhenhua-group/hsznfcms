﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="YearAllStatis.aspx.cs" Inherits="TG.Web.Calendar.YearAllStatis" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/fixtablehead/css/component.css" rel="stylesheet" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script src="../js/fixtablehead/jquery.ba-throttle-debounce.min.js"></script>
    <script src="../js/Calendar/YearAllStatis.js"></script>
    <style type="text/css">
        #tbl th {
        width:60px;  
        font-weight: bold;     
        }
        /*AspNetPager*/
        .pages {
            color: black;
            font-size: 10pt;
            margin-top: 3px;
        }

            .pages a, .pages .cpb {
                text-decoration: none;
                float: left;
                padding: 0 5px;
                border: 1px solid #ddd;
                background: #ffff;
                margin: 0 2px;
                font-size: 10pt;
                color: #000;
            }

                .pages a:hover {
                    background-color: #E61636;
                    color: #fff;
                    border: 1px solid #E61636;
                    text-decoration: none;
                }

            .pages .cpb {
                font-weight: bold;
                color: #fff;
                background: #E61636;
                border: 1px solid #E61636;
            }

            .pages .indexbox {
                height: 20px;
            }

            .pages .submitbtn {
                height: 20px;
                width: 30px;
                border: solid 1px black;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>年假统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">年假统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询条件
                    </div>
                    <div class="actions">
                        <asp:Button CssClass="btn btn-sm red " ID="btn_export" runat="Server" Text="部门导出" OnClick="btn_export_Click"></asp:Button>&nbsp;<asp:Button CssClass="btn btn-sm red " ID="btn_allexport" runat="server" OnClick="btn_allexport_Click" Text="全部导出"></asp:Button>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                            <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 50px;">人员:</td>
                            <td style="width: 100px;">
                                <%-- <asp:DropDownList ID="drp_user" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>--%>
                                <select id="drp_user" runat="server" class="form-control" style="width: 100px;">
                                    <option value="-1">---全部---</option>
                                </select>
                            </td>
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
                            <td>
                                <asp:Button CssClass="btn blue" Text="查询" ID="btn_search" OnClick="btn_search_Click" runat="server" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue" id="aa">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                   <div class="actions">
                        <input class="btn btn-sm red " style="display:none;" id="btn_lock" type="button" value="锁定" />
                    </div>
                </div>
                <div class="portlet-body form">

                    <table border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto;">
                        <tr>
                            <td id="title" style="text-align: center; font-size: 14px; font-weight: bold; line-height: 35px;"></td>
                        </tr>
                    </table>
                    <table id="tbl" class="table table-bordered table-responsive table-hover table-full-width" style="width: 99%; margin: 0 auto; margin-bottom: 0px;">
                        <thead>
                            <tr style="background-color: #f5f5f5; text-align: center; ">
                                <th rowspan="2" style="width:80px;text-align:center;">部门名称</th>
                                <th style="width: 40px;text-align:center;" rowspan="2">序号</th>
                                <th rowspan="2" style="width:70px;text-align:center;">姓名</th>
                                <th rowspan="2" style="width:50px;"><span>2017</span><br />
                                    (天)</th>
                                <th rowspan="2"><span>2017</span>新<br />
                                    (小时)</th>
                                <th rowspan="2" ><span id="past">2016</span>剩<br />
                                    (小时)</th>
                                <th>12.16</th>
                                <th>1.1</th>
                                <th>1.16</th>
                                <th>2.16</th>
                                <th>3.1</th>
                                <th>3.16</th>
                                <th>4.16</th>
                                <th>5.16</th>
                                <th>6.16</th>
                                <th>7.16</th>
                                <th>8.16</th>
                                <th>9.16</th>
                                <th>10.16</th>
                                <th>11.16</th>
                                <th rowspan="2"><span>2017</span>剩<br />
                                    (小时)</th>
                            </tr>
                            <tr style="background-color: #f5f5f5; text-align: center;">
                                <th>12.31</th>
                                <th>1.15</th>
                                <th>2.15</th>
                                <th class="days">2.28</th>
                                <th>3.15</th>
                                <th>4.15</th>
                                <th>5.15</th>
                                <th>6.15</th>
                                <th>7.15</th>
                                <th>8.15</th>
                                <th>9.15</th>
                                <th>10.15</th>
                                <th>11.15</th>
                                <th>12.15</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Literal ID="lithtml" runat="server"></asp:Literal>
                        </tbody>
                    </table>
                    <table id="tbl_btn" border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto; display: none;">
                        <tr>
                            <td style="text-align: center; line-height: 35px;">
                                <input id="btn_save" type="button" class="btn blue" value="保存" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <%--<webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                CustomInfoSectionWidth="35%" OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox"
                PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left"
                ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到"
                PageIndexBoxStyle="width:25px;" PageSize="20" SubmitButtonClass="submitbtn">
            </webdiyer:AspNetPager>--%>

        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="isapplymanager" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_days" runat="server" Value="28" />
    <asp:HiddenField ID="hid_userid" runat="server" Value="-1" />

</asp:Content>
