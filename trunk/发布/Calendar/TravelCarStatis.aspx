﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="TravelCarStatis.aspx.cs" Inherits="TG.Web.Calendar.TravelCarStatis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" lang="javascript">
        $(function () {

            var drp_year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
            var drp_month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
            $("#title").html(drp_year + "年" + drp_month + "月私车公用补贴");
            $("#title2").html(drp_year + "年" + drp_month + "月差旅补贴");

           
        });
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>差旅自驾统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">差旅自驾统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询条件
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                             <td style="width: 50px;">区域:</td>
                            <td style="width: 100px;">
                                <asp:DropDownList ID="drp_qy" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="">---全部---</asp:ListItem>
                                    <asp:ListItem Value="北京">北京</asp:ListItem>
                                    <asp:ListItem Value="西安">西安</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>                           
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px; ">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
                            <td style="width: 50px;">月份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>
                            <td>
                                <asp:Button CssClass="btn blue" Text="查询" ID="btn_search" OnClick="btn_search_Click" runat="server" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_export" runat="server" CssClass="btn btn-sm red" Text="导出私车公用补贴" OnClick="btn_export_Click" />&nbsp;&nbsp;<asp:Button ID="btn_export2" runat="server" CssClass="btn btn-sm red" Text="导出差旅补贴" OnClick="btn_export2_Click" />
                    </div>
                </div>
                <div class="portlet-body form">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1_1" data-toggle="tab">私车公用补贴</a></li>
                        <li class=""><a href="#tab_1_2" data-toggle="tab">差旅补贴</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_1" style="width: 100%">
                            <table border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto;">
                                <tr>
                                    <td id="title" style="text-align: center; font-size: 14px; font-weight: bold; line-height: 35px;">私车公用补贴</td>
                                </tr>
                            </table>
                            <table id="tbl" class="table table-bordered table-responsive table-hover" style="width: 99%; margin: 0 auto; margin-bottom: 0px;">
                                <tr style="background-color: #f5f5f5; text-align: center; font-weight: bold;">                                  
                                    <td style="width: 40px;">序号</td>
                                    <td>姓名</td>
                                    <%-- <td>次数</td>--%>
                                    <td>公里数</td>
                                    <td>日期</td>
                                    <td>费用</td>
                                    <td>小计</td>
                                    <td>合计</td>
                                    <td>合计</td>
                                    <td>项目名称</td>
                                    <td>签领人</td>                                  
                                </tr>
                                <asp:Literal ID="lithtml" runat="server"></asp:Literal>
                            </table>
                        </div>
                        <div class="tab-pane fade " id="tab_1_2" style="width: 100%">
                            <table border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto;">
                                <tr>
                                    <td id="title2" style="text-align: center; font-size: 14px; font-weight: bold; line-height: 35px;">差旅补贴</td>
                                </tr>
                            </table>
                            <table id="tbl2" class="table table-bordered table-responsive table-hover" style="width: 99%; margin: 0 auto; margin-bottom: 0px;">
                                <tr style="background-color: #f5f5f5; text-align: center; font-weight: bold;">                                  
                                    <td style="width: 40px;">序号</td>
                                    <td>姓名</td>
                                   <%-- <td>次数</td>--%>
                                    <td>天数</td>
                                    <td>日期</td>
                                    <td>费用</td>
                                    <td>小计</td>
                                    <td>合计</td>
                                    <td>合计</td>
                                    <td>项目名称</td>
                                    <td>签领人</td>                                  
                                </tr>
                                <asp:Literal ID="lithtml2" runat="server"></asp:Literal>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--权限绑定-->
        <asp:HiddenField runat="server" ID="previewPower" Value="" />
        <asp:HiddenField runat="server" ID="userSysNum" Value="" />
        <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
        <asp:HiddenField runat="server" ID="userShortName" Value="" />
        <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
