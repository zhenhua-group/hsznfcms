﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="MonthSpeAnaly.aspx.cs" Inherits="TG.Web.Calendar.MonthSpeAnaly" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" lang="javascript">
        $(function () {
            var drp_year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
            var drp_month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
            var next_year = parseInt(drp_year);
            var next_month = (parseInt(drp_month) - 1);
            if (drp_month == "1") {
                next_year = (parseInt(drp_year) - 1);
                next_month = 12;
            }
            $("#title").html(next_year + "." + next_month + ".16 - " + drp_year + "." + drp_month + ".15 所有专业组");
        });
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">考勤管理 <small>考勤月专业组分析</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">考勤管理</a><i class="fa fa-angle-right"> </i><a href="#">考勤汇总分析</a><i class="fa fa-angle-right"> </i><a href="#">考勤月专业组分析</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询条件
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="Schtable" style="width: 100%">
                        <tr>
                          <%--  <td style="width: 65px;">生产部门:</td>
                            <td style="width: 130px;">
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>--%>
                            <td style="width: 50px;">年份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_year" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>    
                             <td style="width: 50px;">月份:</td>
                            <td style="width: 100px;">

                                <asp:DropDownList ID="drp_month" CssClass="form-control" Width="80px" runat="server" AppendDataBoundItems="True">
                                </asp:DropDownList>

                            </td>                       
                            <td>
                                <asp:Button CssClass="btn blue" Text="查询" ID="btn_search" OnClick="btn_search_Click" runat="server" /></td>

                        </tr>

                    </table>

                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body form">
                    <table border="0" cellspacing="0" cellpadding="0" style="width: 99%; margin: 0 auto;">
                        <tr>
                            <td id="title" style="text-align: center; font-size: 14px; font-weight: bold; line-height: 35px;"></td>
                        </tr>                       
                    </table>
                    <table id="tbl" class="table table-bordered table-responsive table-hover" style="width:900px; margin: 0 auto; margin-bottom: 0px;">
                        <tr style="background-color:#f5f5f5; text-align:center;font-weight:bold;">                           
                            <td style="width:120px;">平均数</td>
                             <td style="width:120px;">中位数</td>
                            <td style="width:120px;">最大值</td>
                            <td style="width:120px;">最小值</td>
                            <td style="width:120px;">极差</td>
                            <td style="width:120px;">标准差</td>
                            <td style="width:120px;">标准差系数</td>                                
                        </tr>
                        <asp:Literal ID="lithtml" runat="server"></asp:Literal>
                    </table>

                </div>
            </div>
          

        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />


</asp:Content>