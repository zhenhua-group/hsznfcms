﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DwgFileList.aspx.cs" Inherits="TG.Web.Project.DwgFileList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/js/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css">
    <link href="/js/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css">
    <link href="/js/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet"
        type="text/css">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Project.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../js/Project/DwgFileList.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="grid_list" runat="server" AutoGenerateColumns="False" PageSize="30"
                ShowHeader="true" Width="100%" BorderStyle="None" Font-Size="9pt" CssClass="table table-bordered table-hover">
                <PagerSettings FirstPageText="首页" LastPageText="末页" NextPageText="下一页" PreviousPageText="上一页" />
                <RowStyle Font-Size="10pt" HorizontalAlign="Left" VerticalAlign="Middle" />
                <Columns>
                    <asp:TemplateField HeaderText="序号">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text="<%# (Container.DataItemIndex+1).ToString()%> &#13;&#10;"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="4%" Height="21px" Font-Size="10pt" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="文档">
                        <ItemTemplate>
                            <a href="#">
                                <%# Eval("dwgname") %></a>
                        </ItemTemplate>
                        <ItemStyle Width="30%" Height="21px" Font-Size="10pt" />
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="创建者" DataField="createuser">
                        <ItemStyle Width="7%" Height="21px" Font-Size="10pt" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="更新者" DataField="updateuser">
                        <ItemStyle Width="7%" Height="21px" Font-Size="10pt" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="文件大小" DataField="filesize">
                        <ItemStyle Width="10%" Height="21px" Font-Size="10pt" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="更新日期" DataField="createtime">
                        <ItemStyle Width="19%" Height="21px" Font-Size="10pt" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="DWG版本" DataField="version">
                        <ItemStyle Width="7%" Height="21px" Font-Size="10pt" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="备注" DataField="sub">
                        <ItemStyle Width="9%" Height="21px" Font-Size="10pt" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="下载">
                        <ItemTemplate>
                            <a href="<%# Eval("path") %>" target="blank" class="xiazai">下载</a>
                        </ItemTemplate>
                        <ItemStyle Width="7%" Height="21px" Font-Size="10pt" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    没有图纸信息！
                </EmptyDataTemplate>
            </asp:GridView>
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                PageSize="30" SubmitButtonClass="submitbtn">
            </webdiyer:AspNetPager>
        </div>
    </div>
    </form>
</body>
</html>
