﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProPlanAuditEditBymaster.aspx.cs" Inherits="TG.Web.ProjectPlan.ProPlanAuditEditBymaster" %>

<%@ Import Namespace="TG.Model" %>
<%@ Register Src="/UserControl/UserOfTheDepartmentTree.ascx" TagName="UserOfTheDepartmentTree"
    TagPrefix="uc1" %>
<%@ Register Src="/UserControl/ProjectPlan/AddScheduledplan.ascx" TagName="AddScheduledplan"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="/js/wdate/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script src="/js/wdate/WdatePicker.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="/js/ProjectPlan/ProjectPlanAuditEdit.js" type="text/javascript"></script>
    <script src="/js/UserControl/UserOfTheDepartmentTree.js" type="text/javascript"></script>
    <script src="/js/ProjectPlan/AddScheduledplan.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/MessageComm.js"></script>
    <style type="text/css">
        .resultDiv {
            border: solid 1px black;
            width: 98%;
            height: 34px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>设计项目策划申请修改</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>项目管理<i class="fa fa-angle-right"> </i>设计项目策划申请修改</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>项目信息
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                            <tr>
                                                <td style="width: 15%;">项目工程号:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txtJobNumber" runat="server"><%=ProjectPlanViewParameterEntity.JobNumber %></asp:Label>
                                                </td>
                                                <td style="width: 15%;">设计阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_name" runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                                </td>
                                                <td style="width: 15%;">关联合同:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hid_msgid" runat="server" />
                                                    <asp:HiddenField ID="hid_backurl" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">审核级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">建筑单位:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">建设地点:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txtbuildAddress" runat="server" Width="400px"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_scale" runat="server" Width="400px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">建筑类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_unit" runat="server" Width="400px"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">结构形式:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="structType" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">建筑分类:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="buildStructType" runat="server" Width="400px"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">其他参与部门:
                                                </td>
                                                <td colspan="3">
                                                    <asp:Label ID="lbl_isotherprt" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>项目成员
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <%--<div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-6">
                                    <a href="#UserOfTheDepartmentTreeDiv" data-toggle="modal">
                                        <img src="../Images/group_add.png" style="border: none; width: 22px; height: 22px;"
                                            id="ChooseUserImageButton" /></a> <a href="#">
                                                <img src="../Images/group_delete.png" height="21px" id="DeleteUserImageButton" style="border: none;
                                                    width: 22px; height: 22px;" /></a>
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-md-6">
                                    <table style="width: 99%;" class="table table-bordered table-hover">
                                        <tr>
                                            <td align="center" style="width: 5%;">
                                                <input type="checkbox" id="selectAllCheckBox" />
                                            </td>
                                            <td style="width: 10%" align="center">项目成员
                                            </td>
                                            <td align="center" style="width: 10%">专业
                                            </td>
                                            <td style="width: 10%" align="center">部门
                                            </td>
                                        </tr>
                                    </table>
                                    <table id="chooseUserResultTable" width="99%" class="table table-bordered table-hover">
                                        <asp:Repeater ID="RepeaterAllUsers" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 5%;" align="center">
                                                        <input type="checkbox" usersysno="<%#Eval("UserSysNo") %>" name="userCheckBox" username="<%#Eval("UserName") %>"
                                                            userspecialtyname="<%#Eval("SpecialtyName") %>" />
                                                    </td>
                                                    <td style="width: 10%;" align="left">
                                                        <img src="/Images/<%# Eval("UserSex") %>.png" style="width: 16px; height: 16px;" /><%#Eval("UserName") %>
                                                    </td>
                                                    <td style="width: 10%;" align="center">
                                                        <%#Eval("SpecialtyName")%>
                                                    </td>
                                                    <td style="width: 10%;" align="center">
                                                        <%#Eval("DepartmentName")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-1">
                                    <a href="###" id="EngineeringMaterAddButton"><i class="fa fa-hand-o-right" id=""></i>
                                        <%--<img src="/Images/Arrow.gif" id="" style="border: none;" />--%></a>
                                </div>
                                <label class="col-md-1 control-label">
                                    项目总负责</label>
                                <div class="col-md-4">
                                    <div id="EngineeringMater" class="resultDiv" rolesysno="1">
                                        <%=ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 1; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                    <br />
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-1">
                                    <a href="###" id="AssistantAddButton"><i class="fa fa-hand-o-right" id="Img1"></i>
                                        <%--<img src="/Images/Arrow.gif" id="Img1" style="border: none;" />--%></a>
                                </div>
                                <label class="col-md-1 control-label">
                                    执行设总</label>
                                <div class="col-md-4">
                                    <div id="Assistant" class="resultDiv" rolesysno="6">
                                        <%=ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 6; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                    <br />
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-1">
                                    <a href="###" id="ProfessionalPersonAddButton"><i class="fa fa-hand-o-right" id="Img2"></i>
                                        <%--<img src="/Images/Arrow.gif" id="Img2" style="border: none;" />--%></a>
                                </div>
                                <label class="col-md-1 control-label">
                                    专业负责人</label>
                                <div class="col-md-4">
                                    <div id="ProfessionalPerson" class="resultDiv" rolesysno="2">
                                        <%=ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 2; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                    <br />
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-1">
                                    <a href="###" id="ReviewerAddButton"><i class="fa fa-hand-o-right" id="Img3"></i>
                                        <%-- <img src="/Images/Arrow.gif" id="Img3" style="border: none;" />--%></a>
                                </div>
                                <label class="col-md-1 control-label">
                                    校对人</label>
                                <div class="col-md-4">
                                    <div id="Reviewer" class="resultDiv" rolesysno="3">
                                        <%= ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 3; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                    <br />
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-1">
                                    <a href="###" id="AuditUserAddButton"><i class="fa fa-hand-o-right" id="Img4"></i>
                                        <%--<img src="/Images/Arrow.gif" id="Img4" style="border: none;" />--%></a>
                                </div>
                                <label class="col-md-1 control-label">
                                    审核人</label>
                                <div class="col-md-4">
                                    <div id="AuditUser" class="resultDiv" rolesysno="4">
                                        <%= ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 4; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                    <br />
                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-1">
                                    <a href="###" id="ValidatorAddButton"><i class="fa fa-hand-o-right" id="Img5"></i>
                                        <%--<img src="/Images/Arrow.gif" id="Img5" style="border: none;" />--%></a>
                                </div>
                                <label class="col-md-1 control-label">
                                    审定人</label>
                                <div class="col-md-4">
                                    <div id="Validator" class="resultDiv" rolesysno="5">
                                        <%= ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 5; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                    <br />
                                </div>
                                <div class="col-md-6">
                                </div>
                                <%--<div class="col-md-1">
                                    <a href="###" id="DesignAddButton"><i class="fa fa-hand-o-right" id="Img6"></i>
                                        <%--<img src="/Images/Arrow.gif" id="Img6" style="border: none;" /> </a>
                                </div>
                                <label class="col-md-1 control-label">
                                    设计人</label>
                                <div class="col-md-4">
                                    <div id="Design" class="resultDesgionDiv" rolesysno="">
                                        <%= ProjectDesignPlanRoleList == null ? "" : ProjectDesignPlanRoleList.UserStirngWithOutDeleteLink%>
                                    </div>
                                    <br />
                                </div>--%>
                            </div>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>项目进度计划
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <%--<div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-2">
                                    <a href="#SubProjectDiv" id="AddScheduledplanImageButton" data-toggle="modal">
                                        <img src="../images/page_add.png" style="border: none; width: 24px; height: 24px;" /></a>
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-md-12">
                                    <table style="width: 99%;" class="table table-bordered table-hover">
                                        <tr>
                                            <td align="center" style="width: 40%">阶段
                                            </td>
                                            <td align="center" style="width: 23%">开始时间
                                            </td>
                                            <td align="center" style="width: 22%">结束时间
                                            </td>
                                            <td align="center" style="width: 15%">图纸张数
                                            </td>
                                            <%-- <td align="center" style="width: 10%">
                                                操作
                                            </td>--%>
                                        </tr>
                                    </table>
                                    <table width="99%" id="AddSubItemResultTable" class="table table-bordered table-hover">
                                        <asp:Repeater ID="RepeaterProjectPlanSubItem" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="center" style="width: 40%">
                                                        <%#Eval("DesignLevel")%>
                                                    </td>
                                                    <td align="center" style="width: 23%">
                                                        <%#Eval("StartDateString")%>
                                                    </td>
                                                    <td align="center" style="width: 22%">
                                                        <%#Eval("EndDateString")%>
                                                    </td>
                                                    <td align="center" style="width: 15%">
                                                        <%#Eval("SumPaper")%>
                                                        <span style="display: none;" id="paperObjSpan">
                                                            <%#Eval("PaperObjString") %></span>
                                                    </td>
                                                    <%-- <td align="center" style="width: 10%">
                                                        <a href="#" id="DeleteSubItemLinkButton" sysno="<%#Eval("SysNo") %>">删除<a /> | <a
                                                            href="#" id="EditSubItemLinkButton">编辑</a>
                                                    </td>--%>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>修改原因
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <asp:Label Text="text" runat="server" Font-Size="Large" ID="optionsmes" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>审批信息
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--审核区域-->
                                    <%=AuditHTML%>
                                </div>
                            </div>
                            <div class="row" id="Table1" style="display: none;">
                                <div class="col-md-12">
                                    <center>
                                        <a href="###" class="btn blue'" id="ReSrc">修改项目策划</a></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="ProjectSysNo" value="<%=ProjectSysNo %>" />
    <%--<input type="hidden" id="HiddenIsEdit" value="<%=Action %>" />--%>
    <input type="hidden" id="HiddenCoperationName" value="<%=Project.pro_name %>" />
    <input type="hidden" id="HiddenProjectPlanAuditSysNo" value="<%=ProjectPlanAuditSysNo %>" />
    <input type="hidden" id="HiddenAuditStatus" value="<%=AuditStatus %>" />
    <input type="hidden" id="projectKindHidden" value="<%=Project.pro_kinds %>" />
    <input type="hidden" id="ProjectStatusHidden" value="<%=Project.pro_status %>" />
    <input type="hidden" id="ProjectLevelHidden" value="<%=Project.pro_level %>" />
    <input type="hidden" id="ProjectAction" value="3" />
    <input type="hidden" id="JobNumber" value="<%=JobNumber %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <!--选择消息接收者-->
    <div id="msgReceiverContainer" style="display: none; width: 400px; height: 200px;">
    </div>
    <div id="AuditUserDiv" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" id="btn_CanceAudit" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
