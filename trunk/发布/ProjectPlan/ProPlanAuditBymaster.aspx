﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProPlanAuditBymaster.aspx.cs" Inherits="TG.Web.ProjectPlan.ProPlanAuditBymaster" %>

<%@ Import Namespace="TG.Model" %>
<%@ Register Src="~/UserControl/UserOfTheDepartmentTree.ascx" TagName="UserOfTheDepartmentTree"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/ProjectPlan/AddScheduledplan.ascx" TagName="AddScheduledplan"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="/js/wdate/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <link href="/css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="../js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/wdate/WdatePicker.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="/js/ProjectPlan/ProjectPlanAuditBymaster.js" type="text/javascript"></script>
    <script src="/js/UserControl/UserOfTheDepartmentTree.js" type="text/javascript"></script>
    <script src="/js/ProjectPlan/AddScheduledplan.js" type="text/javascript"></script>
    <script src="/js/ProjectPlan/ProjectPlanAuditDoBymaster.js" type="text/javascript"></script>
    <script src="../js/ProjectPlan/ProjectPlanForEditBymaster.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/MessageComm.js"></script>
    <style type="text/css">
        .resultDiv {
            border: solid 1px black;
            width: 98%;
            height: 34px;
        }

        .resultDesgionDiv {
            border: solid 1px black;
            width: 98%;
            height: 112px;
        }

        #ZLCardContent input {
            width: 95%;
            height: 150%;
            border-width: 1;
            border-color: #CCC;
            background-color: White;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目策划审批</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>项目管理<i class="fa fa-angle-right"> </i>项目策划审批</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row" id="Report" style="display: none;">
        <div class="col-md-12">
            <table class="table-responsive">
                <tr>
                    <td>项目设计阶段:</td>
                    <td>
                        <asp:DropDownList ID="drp_status" runat="server" CssClass="form-control"
                            AppendDataBoundItems="true">
                            <asp:ListItem Value="-1">-----设计阶段-----</asp:ListItem>
                        </asp:DropDownList></td>
                    <td>表单类型:</td>
                    <td>
                        <select id="selectID" class="form-control">
                            <option value="-1">-----选择表单-----</option>
                            <option value="3">项目进岗人员审批单</option>
                            <option value="4">项目进度计划表</option>
                            <option value="5">资料记录卡</option>
                        </select></td>
                    <td>
                        <button type="button" id="btn_outReport" class="btn green btn-sm">
                            导出</button></td>
                    <td>
                        <button type="button" id="btn_appeditcpr" class="btn green btn-sm">
                            申请修改</button></td>
                    <td>
                        <div style="display: none;" id="OptionDiv">
                            <input type="text" option="a" class="form-control input-sm" name="name" id="opinion"
                                value="" />
                            <input type="button" name="name" id="btnsub" data-toggle="modal" class="btn green btn-sm" value="提交" />
                            <input type="button" name="name" id="btnquit" class="btn btn-sm" value="取消" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">项目信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 120px;">项目工程号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtJobNumber" runat="server"><%=ProjectPlanViewParameterEntity.JobNumber %></asp:Label>
                                        </td>
                                        <td style="width: 120px;">设计阶段:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_name" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                        </td>
                                        <td>关联合同:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_msgid" runat="server" />
                                            <asp:HiddenField ID="hid_backurl" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>管理级别:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td>审核级别:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建筑单位:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtbuildAddress" runat="server" Width="400px"></asp:Label>
                                        </td>
                                        <td>建设规模:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_scale" runat="server" Width="400px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                        </td>
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_unit" runat="server" Width="400px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>结构形式:
                                        </td>
                                        <td>
                                            <asp:Label ID="structType" runat="server"></asp:Label>
                                        </td>
                                        <td>建筑分类:
                                        </td>
                                        <td>
                                            <asp:Label ID="buildStructType" runat="server" Width="400px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目成员
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <td rowspan="7" style="width: 400px;">
                                    <table style="width: 99%;" class="table table-bordered table-hover">
                                        <tr>
                                            <td align="center" style="width: 5%;">
                                                <input type="checkbox" id="selectAllCheckBox" />
                                            </td>
                                            <td style="width: 10%" align="center">项目成员
                                            </td>
                                            <td align="center" style="width: 10%">专业
                                            </td>
                                            <td style="width: 10%" align="center">部门
                                            </td>
                                        </tr>
                                    </table>
                                    <table id="chooseUserResultTable" width="99%" class="table table-bordered table-hover">
                                        <asp:Repeater ID="RepeaterAllUsers" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 5%;" align="center">
                                                        <input type="checkbox" usersysno="<%#Eval("UserSysNo") %>" name="userCheckBox" username="<%#Eval("UserName") %>"
                                                            userspecialtyname="<%#Eval("SpecialtyName") %>" />
                                                    </td>
                                                    <td style="width: 10%;" align="left">
                                                        <img src="/Images/<%# Eval("UserSex") %>.png" style="width: 16px; height: 16px;" /><%#Eval("UserName") %>
                                                    </td>
                                                    <td style="width: 10%;" align="center">
                                                        <%#Eval("SpecialtyName")%>
                                                    </td>
                                                    <td style="width: 10%;" align="center">
                                                        <%#Eval("DepartmentName")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </td>
                                <td><a href="###" id="EngineeringMaterAddButton"><i class="fa fa fa-arrow-right" id=""></i>
                                </a></td>
                                <td>项目总负责</td>
                                <td>
                                    <div id="EngineeringMater" class="resultDiv" rolesysno="1" style="height: 60px;">
                                        <%=ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 1; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="###" id="AssistantAddButton"><i class="fa fa fa-arrow-right" id="Img1"></i>
                                </a></td>
                                <td>执行设总</td>
                                <td>
                                    <div id="Assistant" class="resultDiv" rolesysno="6" style="height: 60px;">
                                        <%=ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 6; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="###" id="ProfessionalPersonAddButton"><i class="fa fa fa-arrow-right" id="Img2"></i>
                                </a></td>
                                <td>专业负责人</td>
                                <td>
                                    <div id="ProfessionalPerson" class="resultDiv" rolesysno="2" style="height: 60px;">
                                        <%=ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 2; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="###" id="ReviewerAddButton"><i class="fa fa fa-arrow-right" id="Img3"></i>
                                </a></td>
                                <td>校对人</td>
                                <td>
                                    <div id="Reviewer" class="resultDiv" rolesysno="3" style="height: 60px;">
                                        <%= ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 3; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="###" id="AuditUserAddButton"><i class="fa fa fa-arrow-right" id="Img4"></i>
                                </a></td>
                                <td>审核人</td>
                                <td>
                                    <div id="AuditUser" class="resultDiv" rolesysno="4" style="height: 60px;">
                                        <%= ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 4; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="###" id="ValidatorAddButton"><i class="fa fa fa-arrow-right" id="Img5"></i>
                                </a></td>
                                <td>审定人</td>
                                <td>
                                    <div id="Validator" class="resultDiv" rolesysno="5" style="height: 60px;">
                                        <%= ProjectPlanRoleList == null ? "" : ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 5; }).UserStirngWithOutDeleteLink%>
                                    </div>
                                </td>
                            </tr>
                           <%-- <tr>
                                <td><a href="###" id="DesignAddButton"><i class="fa fa fa-arrow-right" id="Img6"></i>
                                </a></td>
                                <td>设计人</td>
                                <td>
                                    <div id="Design" class="resultDesgionDiv" rolesysno="" style="height: 120px;">
                                        <%= ProjectDesignPlanRoleList == null ? "" : ProjectDesignPlanRoleList.UserStirngWithOutDeleteLink%>
                                    </div>
                                </td>
                            </tr>--%>
                        </table>
                    </div>
                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目进度计划
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table style="width: 99%;" class="table table-bordered table-hover">
                                <tr>
                                    <td align="center" style="width: 40%">阶段
                                    </td>
                                    <td align="center" style="width: 23%">开始时间
                                    </td>
                                    <td align="center" style="width: 22%">结束时间
                                    </td>
                                    <td align="center" style="width: 15%">图纸张数
                                    </td>
                                    <%-- <td align="center" style="width: 10%">
                                                操作
                                            </td>--%>
                                </tr>
                            </table>
                            <table width="99%" id="AddSubItemResultTable" class="table table-bordered table-hover">
                                <asp:Repeater ID="RepeaterProjectPlanSubItem" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td align="center" style="width: 40%">
                                                <%#Eval("DesignLevel")%>
                                            </td>
                                            <td align="center" style="width: 23%">
                                                <%#Eval("StartDateString")%>
                                            </td>
                                            <td align="center" style="width: 22%">
                                                <%#Eval("EndDateString")%>
                                            </td>
                                            <td align="center" style="width: 15%">
                                                <%#Eval("SumPaper")%>
                                                <span style="display: none;" id="paperObjSpan">
                                                    <%#Eval("PaperObjString") %></span>
                                            </td>
                                            <%-- <td align="center" style="width: 10%">
                                                        <a href="#" id="DeleteSubItemLinkButton" sysno="<%#Eval("SysNo") %>">删除<a /> | <a
                                                            href="#" id="EditSubItemLinkButton">编辑</a>
                                                    </td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>资料记录卡
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table style="width: 99%" class="table table-bordered table-hover" id="ZLcard">
                                <tr>
                                    <td align="center" style="width: 4%">序号
                                    </td>
                                    <td align="center" style="width: 16%">资料名称
                                    </td>
                                    <td style="width: 8%" align="center">收件人及日期
                                    </td>
                                    <td style="width: 8%" align="center">验证人
                                    </td>
                                    <td style="width: 8%" align="center">合法性
                                    </td>
                                    <td style="width: 7%" align="center">完整性
                                    </td>
                                    <td style="width: 7%" align="center">清晰度
                                    </td>
                                    <td style="width: 8%" align="center">处理情况
                                    </td>
                                    <td style="width: 10%" align="center">二次送达验证意见
                                    </td>
                                    <td style="width: 10%" align="center">三次送达验证意见
                                    </td>
                                    <td style="width: 8%" align="center">交接记录
                                    </td>
                                    <td style="width: 6%" align="center">备注
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered table-hover" width="99%" id="ZLCardContent">
                                <tr>
                                    <td style="width: 4%">
                                        <input id="BM_3149_0_0" value="1" option="a" runat="Server" class="form-control" />
                                    </td>
                                    <td style="width: 16%">
                                        <input id="BM_3149_0_1" value="任务批准文件" option="a" runat="Server" class="form-control" />
                                    </td>
                                    <td style="width: 8%">
                                        <input id="BM_3149_0_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 8%">
                                        <input id="BM_3149_0_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 8%">
                                        <input id="BM_3149_0_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 7%">
                                        <input id="BM_3149_0_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 7%">
                                        <input id="BM_3149_0_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 8%">
                                        <input id="BM_3149_0_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 10%">
                                        <input id="BM_3149_0_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 10%">
                                        <input id="BM_3149_0_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 8%">
                                        <input id="BM_3149_0_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td style="width: 6%">
                                        <input id="BM_3149_0_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_1_0" value="2" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_1" value="设计任务书或设计委托书" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_1_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_2_0" value="3" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_1" value="拨地图红线图" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_2_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_3_0" value="4" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_1" value="规划条件通知书" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_3_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_4_0" value="5" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_1" value="消防规划计划" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_6" value="无" runat="Server" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_4_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_5_0" value="6" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_1" value="地形测量图" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_5_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_6_0" value="7" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_1" value="岩土工程勘察报告" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_6_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_7_0" value="8" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_1" value="工艺图设计要求" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_7_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_8_0" value="9" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_1" value="建设场地周围道路、管网资料" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_8_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_9_0" value="10" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_1" value="改扩建工程原始资料" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_9_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_10_0" value="11" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_1" value="顾客选用产品样本" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_10_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input id="BM_3149_11_0" value="12" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_1" value="主管部门审批书面意见" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_2" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_3" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_4" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_5" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_6" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_7" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_8" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_9" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_10" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                    <td>
                                        <input id="BM_3149_11_11" value="无" runat="Server" option="a" class="form-control" />
                                    </td>
                                </tr>
                                <%-- <tr>
                            <td colspan='12' align="center">
                                <span id="Span1" class="cls_btn_comm_w" style="cursor: pointer">&nbsp;&nbsp;导出&nbsp;&nbsp;</span>&nbsp;&nbsp;
                                <span id="saveZL" style="cursor: pointer" class="cls_btn_comm_w">&nbsp;&nbsp;保存&nbsp;&nbsp;</span>
                            </td>
                        </tr>--%>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>审批信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <!--审核区域-->
                            <%=AuditHTML%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="ProjectSysNo" value="<%=ProjectSysNo %>" />
    <%--<input type="hidden" id="HiddenIsEdit" value="<%=Action %>" />--%>
    <input type="hidden" id="HiddenCoperationName" value="<%=Project.pro_name %>" />
    <input type="hidden" id="HiddenProjectPlanAuditSysNo" value="<%=ProjectPlanAuditSysNo %>" />
    <input type="hidden" id="HiddenAuditStatus" value="<%=AuditStatus %>" />
    <input type="hidden" id="projectKindHidden" value="<%=Project.pro_kinds %>" />
    <input type="hidden" id="ProjectStatusHidden" value="<%=Project.pro_status %>" />
    <input type="hidden" id="ProjectLevelHidden" value="<%=Project.pro_level %>" />
    <input type="hidden" id="ProjectRefNo" value="<%=Project.ReferenceSysNo %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <div id="AuditUserDiv" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" id="btn_CanceAudit" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="Div1" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="Div2">
        </div>
        <div class="modal-footer">
            <button type="button" id="Button1" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>

</asp:Content>
