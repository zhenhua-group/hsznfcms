﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProPlanMiddlePage.aspx.cs" Inherits="TG.Web.ProjectPlan.ProPlanMiddlePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="Stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
     <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/SendMessageCommon.js"></script>
    <script src="../js/ProjectPlan/ProPlanMiddlePage.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>发起项目策划审批
    </small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="ProjectListBymaster.aspx">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="ProjectListBymaster.aspx">项目管理</a><i class="fa fa-angle-right"> </i><a href="#">发起项目策划审批
    </a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>发起项目策划审批
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-md-offset-12 col-md-12">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a data-toggle="modal" href="#AuditUser" class="btn red btn-default" proid="<%=projectSysNo %>" cprname="<%=cprname %>" id="btn_startApply">发起审批</a>
                            <asp:Button ID="btn_ShowPro" runat="server" Text="查看项目策划" CssClass="btn blue btn-default" OnClick="btn_ShowPro_Click" />
                            <asp:Button ID="btn_ProList" runat="server" Text="返回" CssClass="btn blue btn-default" OnClick="btn_ProList_Click" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input id="hidproId" type="hidden" runat="server" />
    <input id="hiduserid" type="hidden" runat="server" />
    <input type="hidden" id="hidcprname" runat="server">
    <input type="hidden" id="hidpronumber" runat="server">
     <div id="AuditUser" class="modal  fade yellow" tabindex="-1" data-width="450" aria-hidden="true" style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>

</asp:Content>
