﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddScheduledplan.aspx.cs"
        Inherits="TG.Web.ProjectPlan.AddScheduledplan" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <title></title>
        <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
        <link href="../css/ProjectPlan.css" rel="stylesheet" type="text/css" />

        <script src="../js/wdate/WdatePicker.js" type="text/javascript"></script>

</head>
<body bgcolor="f0f0f0">
        <form id="form1" runat="server">
        <table class="cls_container">
                <tr>
                        <td class="style2">
                                &nbsp;&nbsp;当前位置：[项目管理]
                        </td>
                </tr>
        </table>
        <table style="width: 100%; margin-bottom: 80px" class="show_gvproPlan">
                <tr>
                        <td align="right" colspan="4">
                                <input id="btnSave" class="btn" type="button" value="保存" /><input id="btnedit" class="btn"
                                        type="button" value="编辑" />
                        </td>
                </tr>
                <tr>
                        <td style="width: 10%">
                                设计阶段
                        </td>
                        <td>
                                <asp:DropDownList ID="DropDownList1" runat="server" Width="120px">
                                        <asp:ListItem>方案设计</asp:ListItem>
                                        <asp:ListItem>方案定案</asp:ListItem>
                                        <asp:ListItem>初设</asp:ListItem>
                                        <asp:ListItem>各专业互提条件</asp:ListItem>
                                        <asp:ListItem>施工图设计</asp:ListItem>
                                        <asp:ListItem>会签</asp:ListItem>
                                        <asp:ListItem>各专业校审</asp:ListItem>
                                        <asp:ListItem>备档打印</asp:ListItem>
                                </asp:DropDownList>
                        </td>
                        <td style="width: 10%">
                                子项名称
                        </td>
                        <td>
                                <input id="txtchildName" type="text" />
                        </td>
                </tr>
               <%-- <tr>
                        <td>
                                开始时间
                        </td>
                        <td>
                                <input id="txtStartTime" runat="Server" class="Wdate" name="txt_date1" onclick="WdatePicker({readOnly:true})"
                                        size="20" style="width: 120px;" type="text" />
                        </td>
                        <td>
                                结束时间
                        </td>
                        <td>
                                <input id="txtFinishtime" runat="Server" class="Wdate" name="txt_date2" onclick="WdatePicker({readOnly:true})"
                                        size="20" style="width: 120px;" type="text" />
                        </td>
                </tr>--%>
                <%--<tr>
                        <td>
                                栋数
                        </td>
                        <td>
                                <input id="txtds" type="text" />
                        </td>
                        <td>
                                层数
                        </td>
                        <td>
                                <input id="txtcs" type="text" />
                        </td>
                </tr>
                <tr>
                        <td>
                                面积
                        </td>
                        <td>
                                <input id="txtArea" type="text" />平米
                        </td>
                        <td>
                                &nbsp;
                        </td>
                        <td>
                                &nbsp;
                        </td>
                </tr>--%>
        </table>
        </form>
</body>
</html>
