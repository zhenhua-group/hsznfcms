﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseCustomer.ascx.cs"
    Inherits="TG.Web.UserControl.ChooseCustomer" %>
<link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
<link href="../css/Customer.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    #customerInfoListTable {
        border-collapse: collapse;
    }

        #customerInfoListTable td {
            font-size: 12px;
            border: 1px solid #CCC;
            border-top: none;
            border-collapse: collapse;
            word-wrap: break-word;
            word-break: break-all;
        }

    #table_container > tbody > tr > td {
        padding: 2px 2px !important;
    }
</style>
<table class="table table-bordered" id="table_container">
    <tr>
        <td>客户名称</td>
        <td>
            <input type="text" id="customerName" class="form-control input-sm" /></td>
        <td>客户类别</td>
        <td>
            <asp:DropDownList ID="customerLevel" runat="server" AppendDataBoundItems="true" Width="120px" CssClass="form-control">
                <asp:ListItem Value="-1">---请选择---</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td>所在省份</td>
        <td>
            <input type="text" id="provinceName" class="form-control input-sm" /></td>
        <td>所在城市</td>
        <td>
            <input type="text" id="cityName" class="form-control input-sm" /></td>
    </tr>
    <tr>
        <td>联系人</td>
        <td>
            <input type="text" id="linkMan" class="form-control input-sm" /></td>
        <td colspan="2"><a href="#" id="serachCustomerButton" class="btn blue btn-sm"><i class="fa fa-search"></i></a></td>

    </tr>

</table>

<table class="cls_content_head" style="font-size: 12px;">
    <tr>
        <td style="width: 100px;" align="center">编号
        </td>
        <td style="width: 230px;" align="center">客户名称
        </td>
        <td style="width: 60px;" align="center">联系人
        </td>
        <td style="width: 160px;" align="center">公司地址
        </td>
        <td style="width: 100px;" align="center">电话
        </td>
        <td style="width: 50px;" align="center">操作
        </td>
    </tr>
</table>
<table id="customerInfoListTable" style="width: 100%;">
</table>
<div id="pager">
</div>

<!--HiddenArea-->
<input type="hidden" id="hiddenPageSize" value="<%=PageSize %>" />
<!-- 隐藏用户控件 -->
<input type="hidden" id="hid_usersysno" value="<%=UserSysNo  %>" />
<input type="hidden" id="hid_previewpower" value="<%=PreviewPower %>" />
<!-- 排序指定 -->
<asp:HiddenField ID="hid_column" runat="server" Value="cpr_Id" />
<asp:HiddenField ID="hid_order" runat="server" Value="DESC" />
