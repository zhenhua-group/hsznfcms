﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompanyType.ascx.cs" Inherits="TG.Web.UserControl.CompanyType" %>
<script type="text/javascript">
    $(function () {
        $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_company").change(function () {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").empty();
            var companyValue = $(this).val();
            if (companyValue == "2") {

                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"-1\">-----合同类型-----</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"0\">岩土工程勘察合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"1\">岩土工程设计合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"2\">基坑及边坡监测合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"3\">测量及沉降观测合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"4\">岩土工程检测合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"5\">岩土工程施工合同</option>");
            }
            else if (companyValue == "1") {
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"-1\">-----合同类型-----</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"0\">工程监理合同</option>");
            } else {
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"-1\">-----合同类型-----</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"0\">基本合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"1\">市政合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"2\">规划工程合同</option>");
                $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"3\">景观规划合同</option>");
            }
        })
        var companyValue = $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_company").val();
        $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").empty();
        if (companyValue == "2") {

            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"-1\">-----合同类型-----</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"0\">岩土工程勘察合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"1\">岩土工程设计合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"2\">基坑及边坡监测合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"3\">测量及沉降观测合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"4\">岩土工程检测合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"5\">岩土工程施工合同</option>");
        }
        else if (companyValue == "1") {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"-1\">-----合同类型-----</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"0\">工程监理合同</option>");
        } else {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"-1\">-----合同类型-----</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"0\">基本合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"1\">市政合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"2\">规划工程合同</option>");
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").append("<option value=\"3\">景观规划合同</option>");
        }
        var pageurl = window.location.pathname;
        pageurl = pageurl.substring(1, pageurl.length);
        if ("Coperation/cpr_ReconnCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("0");
        }
        if ("Coperation/cpr_DesignCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("1");
        }
        if ("Coperation/cpr_PitCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("2");
        }
        if ("Coperation/cpr_TestCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("3");
        }
        if ("Coperation/cpr_MeasureCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("4");
        }
        if ("Coperation/cpr_ConstruCoperartionBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("5");
        }
        if ("Coperation/cpr_SuperCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("0");
        } if ("Coperation/cpr_AddCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("0");
        } if ("Coperation/cpr_AddEngineerCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("1");
        } if ("Coperation/cpr_AddPlanningCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("2");
        }
        if ("Coperation/cpr_AddSceneryCoperationBymaster.aspx".indexOf(pageurl) > -1) {
            $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").val("3");
        }
        $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_companytype").change(function () {

            var companyValue = $(this).val();
            var company = $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_company").val();
            if (company == "2") {
                if (companyValue == "0") {
                    window.location.href = "../Coperation/cpr_ReconnCoperationBymaster.aspx";
                }
                else if (companyValue == "1") {
                    window.location.href = "../Coperation/cpr_DesignCoperationBymaster.aspx";
                } else if (companyValue == "2") {
                    window.location.href = "../Coperation/cpr_PitCoperationBymaster.aspx";
                } else if (companyValue == "3") {
                    window.location.href = "../Coperation/cpr_TestCoperationBymaster.aspx";//cm_MeasureCoperation

                } else if (companyValue == "4") {
                    window.location.href = "../Coperation/cpr_MeasureCoperationBymaster.aspx";//cm_TestCoperation
                } else if (companyValue == "5") {
                    window.location.href = "../Coperation/cpr_ConstruCoperartionBymaster.aspx";//cm_ConstruCoperartion
                }
            } else if (company == "1") {
                if (companyValue == "0") {
                    window.location.href = "../Coperation/cpr_SuperCoperationBymaster.aspx";
                }
            }
            else {
                if (companyValue == "0") {
                    window.location.href = "../Coperation/cpr_AddCoperationBymaster.aspx";
                } else if (companyValue == "1") {
                    window.location.href = "../Coperation/cpr_AddEngineerCoperationBymaster.aspx";
                } else if (companyValue == "2") {
                    window.location.href = "../Coperation/cpr_AddPlanningCoperationBymaster.aspx";//规划合同
                } else if (companyValue == "3") {
                    window.location.href = "../Coperation/cpr_AddSceneryCoperationBymaster.aspx";//景观合同
                }
            }

        })
    })
</script>
<h4 class="form-section">
    <div class="row">
        <div class="col-md-10">
            录入合同类型选择
        </div>
        <div class="col-md-2">
        </div>
    </div>
</h4>

<div class="row">
    <div class="col-md-12">
        <table class="table-responsive">
            <tr>
                <td>部门:
                </td>
                <td>
                    <asp:DropDownList ID="dpr_company" runat="Server" AppendDataBoundItems="True" CssClass="form-control" Width="120px">
                        <asp:ListItem Value="0">生产部门</asp:ListItem>
                        <asp:ListItem Value="1">监理公司</asp:ListItem>
                        <asp:ListItem Value="2">勘察分院</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>合同类型:
                </td>
                <td>
                    <asp:DropDownList ID="dpr_companytype" runat="Server" AppendDataBoundItems="True" CssClass="form-control">
                        <asp:ListItem Value="-1">-----合同类型-----</asp:ListItem>
                        <asp:ListItem Value="0">基本合同</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <br />
</div>
