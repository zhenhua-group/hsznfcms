﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddExternalMember.ascx.cs"
    Inherits="TG.Web.UserControl.AddExternalMember" %>
<link href="/css/UserControl/ChooseUser.css" rel="stylesheet" />
<style type="text/css">
    .style1 {
        width: 25%;
        height: 20px;
    }

    .style2 {
        width: 40%;
        height: 20px;
    }

    .style3 {
        width: 10%;
        height: 20px;
    }
</style>
<div id="chooseUserMainDivContainer">
    <table style="width: 100%;" class="show_project" id="tbAddExternal">
        <tr>
            <td align="center">
                <span id="addExternal" style="color: Blue; cursor: pointer;">添加外聘人员</span>
            </td>
        </tr>
    </table>
    <fieldset id="fidAddExternal" style="display: none;">
        <legend>添加外聘人员</legend>
        <table style="width: 100%;" class="show_project" id="Table1">
            <tr>
                <td>姓名：
                </td>
                <td>
                    <input type="text" id="txtAddUser" class="textBox" />
                </td>
                <td>选择部门：
                </td>
                <td>
                    <select id="selectUnit">
                        <option value="0">----请选择----</option>
                        <asp:Repeater ID="rpUnit" runat="server">
                            <ItemTemplate>
                                <option value="<%#Eval("unit_ID") %>">
                                    <%#Eval("unit_Name") %></option>
                            </ItemTemplate>
                        </asp:Repeater>
                    </select>
                </td>
            </tr>
            <tr>
                <td>专业：
                </td>
                <td>
                    <select id="selectSpe" style="width: 145px;">
                        <option value="0">----请选择----</option>
                        <asp:Repeater ID="rpSpe" runat="server">
                            <ItemTemplate>
                                <option value="<%#Eval("spe_ID") %>">
                                    <%#Eval("spe_Name")%></option>
                            </ItemTemplate>
                        </asp:Repeater>
                    </select>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <input type="button" value="添加" id="btnAdd" class="btn blue btn-sm" />
                    <input type="button" value="取消" id="btnCancel" class="btn blue btn-sm" />
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset id="fidSelectExternal">
        <legend>选择外聘人员</legend>
        <table style="width: 100%;" class="mainTable">
            <tr>
                <td>
                    <!--QueryConditionTable-->
                    <table style="width: 100%;" class="show_project" id="tbSelect">
                        <tr>
                            <td>姓名：
                            </td>
                            <td>
                                <input type="text" id="chooseUserOfNameTextBox" class="textBox" />
                            </td>
                            <td>选择部门：
                            </td>
                            <td>
                                <select id="chooseWpUserDepartmentDropDownList">
                                    <option value="0">----请选择----</option>
                                    <asp:Repeater ID="RepeaterDepartment" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("unit_ID") %>">
                                                <%#Eval("unit_Name") %></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>专业：
                            </td>
                            <td>
                                <select id="chooseWpSpeName" style="width: 145px;">
                                    <asp:Repeater ID="RepeaterSpe" runat="server">
                                        <ItemTemplate>
                                            <option value="<%#Eval("spe_ID") %>">
                                                <%#Eval("spe_Name")%></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </td>
                            <td>角色：
                            </td>
                            <td>
                                <input id="chkWpDesign" type="checkbox" checked="checked" />设计
                                <input id="chkWpHead" type="checkbox" />专业负责
                                <input id="chkWpAudit" type="checkbox" />审核
                                <input id="chkWpProof" type="checkbox" />校对
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="text-align: center;">
                                <input type="button" value="查询" id="chooseUserSearchButton" class="btn blue btn-sm" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </fieldset>
    <table style="width: 100%;" class="mainTable">
        <tr>
            <td>
                <!--ResultTable-->
                <table style="width: 100%;" id="chooseExternalUserResultTable" class="resultTable">
                    <tr>
                        <td align="center" class="style3">
                            <input type="checkbox" id="selectExternalAllCheckBox" />
                        </td>
                        <td align="center" class="style1">姓名
                        </td>
                        <td align="center" class="style2">部门
                        </td>
                        <td align="center" class="style1">专业
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div id="pager">
                </div>
            </td>
        </tr>
    </table>
</div>
