﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseUser.ascx.cs"
    Inherits="TG.Web.UserControl.ChooseUser" %>
<link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
<link href="/css/UserControl/ChooseUser.css" rel="stylesheet" />
<div id="chooseUserMainDivContainer">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            姓名或登录名：
        </div>
        <div class="col-md-3">
            <input type="text" id="chooseUserOfNameTextBox" class="form-control input-sm" />
            
        </div>
        <div class="col-md-2">
            选择部门：
        </div>
        <div class="col-md-3">
            <select id="chooseUserDepartmentDropDownList" class="form-control">
                <option value="0">----请选择----</option>
                <asp:Repeater ID="RepeaterDepartment" runat="server">
                    <ItemTemplate>
                        <option value="<%#Eval("unit_ID") %>">
                            <%#Eval("unit_Name") %></option>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-2">
            电话号码：
        </div>
        <div class="col-md-3">
            <input type="text" id="chooseUserPhoneTextBox" class="form-control input-sm" />
            
        </div>
        <div class="col-md-2">
            <a href="#" id="chooseUserSearchButton" class="btn blue btn-sm"><i class="fa fa-search">
            </i></a>
        </div>
    </div>
    <div class="row">
       <%-- <div class="col-md-1">
        </div>--%>
        <div class="col-md-12">
            <!--ResultTable-->
            <table style="width: 100%;" id="chooseUserResultTable" class="resultTable">
                <tr>
                    <td align="center" style="width: 25%;">
                        姓名
                    </td>
                    <td align="center" style="width: 40%;">
                        部门
                    </td>
                    <td align="center" style="width: 25%;">
                        电话号码
                    </td>
                    <td align="center" style="width: 10%;">
                        操作
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
       <%-- <div class="col-md-1">
        </div>--%>
        <div class="col-md-12">
            <div id="pager">
            </div>
        </div>
    </div>
</div>
