﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuditConfigBase.ascx.cs"
    Inherits="TG.Web.UserControl.AuditConfigBase" %>
<%@ Register Src="/Coperation/SelectUserControl.ascx" TagName="SelectUserControl"
    TagPrefix="uc1" %>
<style type="text/css">
    ul
    {
        list-style-type: none;
    }
    
    .containerli
    {
        width: 100%;
        float: right;
        margin-top: 2px;
        margin-bottom: 2px;
        cursor: all-scroll;
    }
    
    .sortli
    {
        line-height: 20px;
        border-right: dotted 1px #ccc;
        padding: 0 5px;
    }
    
    .inputTextBox
    {
        height: 12px;
        width: 100%;
    }
    
    .RoleSelectShow
    {
        display: block;
    }
    
    body
    {
        font-size: 12px;
    }
    /*jQuery UI fakes 默认字体大小  */.ui-widget
    {
        font-size: 1em;
    }
    
    .ui-dialog .ui-dialog-buttonpane
    {
        padding-top: .1em;
        padding-bottom: .1em;
    }
</style>
<div id="AuditConfigBaseContainer" style="width: 100%;">
    <table class="" align="center" id="projectAuditConfigTable" style="text-align: center;
        font-weight: bold; border: 0px;">
        <tr>
            <td style="width: 25%;" align="center">
                审核流程
            </td>
            <td style="width: 25%;">
                角色
            </td>
            <td style="width: 40%;">
                所属用户
            </td>
            <td style="width: 10%;">
                操作
            </td>
        </tr>
        <tr>
            <td colspan="4" width="100%">
                <ul id="sortTable" style="width: 100%;">
                    <% for (int i = 0; i < AuditConfigBaseList.Count; i++) %>
                    <%{ %>
                    <li class="containerli ui-state-default" style="cursor: pointer;">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <!--流程Li-->
                                <td style="width: 25%;" class="sortli" id="workflowliAction">
                                    <%=AuditConfigBaseList[i].ProcessDescription %>
                                </td>
                                <!--角色Li-->
                                <td style="width: 25%;" class="sortli" id="roleSysNoliAction" rolesysno="<%=AuditConfigBaseList[i].RoleSysNo %>">
                                    <span id="roleNameSpan">
                                        <%=AuditConfigBaseList[i].RoleName %></span>
                                    <select id="roleDropDownList" style="display: none;" projectauditconfigsysno="<%= AuditConfigBaseList[i].SysNo%>">
                                        <%for (int j = 0; j < RoleList.Count; j++) %>
                                        <%{ %>
                                        <option value="<%=RoleList[j].SysNo %>">
                                            <%=RoleList[j].RoleName %></option>
                                        <%} %>
                                    </select>
                                </td>
                                <!--用户Li-->
                                <td style="width: 40%;" class="sortli" userli="true">
                                    <%for (int k = 0; k < AuditConfigBaseList[i].UserInfoList.Count; k++) %>
                                    <%{ %>
                                    <span id="userSpan" usersysno="<%=AuditConfigBaseList[i].UserInfoList[k].mem_ID %>"
                                        style="margin-right: 1px;">
                                        <%= AuditConfigBaseList[i].UserInfoList[k].mem_Name%>
                                        <img src="../../Images/pro_icon_03.gif" style="cursor: pointer;" alt="删除该用户" name="deleteUserImgActionBtn" />
                                    </span>
                                    <%} %>
                                </td>
                                <!--操作Li-->
                                <td style="text-align: center; padding: 5px 5px 5px 5px; width: 10%;">
                                    <img src="../../Images/AddUser.png" style="cursor: pointer;" id="addRoleUserBtn"
                                        href="#UserDiv" data-toggle="modal" />
                                </td>
                            </tr>
                        </table>
                        <%--<ul>
                                <!--流程Li-->
                                <li style="width: 25%;" class="sortli" id="workflowliAction">
                                   </li>
                                <!--角色Li-->
                                <li style="width: 25%;" class="sortli" id="roleSysNoliAction" rolesysno="<%=AuditConfigBaseList[i].RoleSysNo %>">
                                    
                                </li>
                                <!--用户Li-->
                                <li style="width: 40%;" class="sortli" userli="true">
                                   
                                </li>
                               
                                <!--操作Li-->
                                <li >
                                    
                                </li>
                            </ul>--%>
                    </li>
                    <%} %>
                </ul>
            </td>
        </tr>
    </table>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenTableName" value="<%=TableName %>" />
    <div id="UserDiv" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; height: auto; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                选择用户</h4>
        </div>
        <div class="modal-body">
            <div id="DivAddRoleUser" title="选择用户">
                <uc1:SelectUserControl ID="SelectUserControl1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_Done" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Coperation/SelectRoleUser.js" type="text/javascript"></script>
</div>
