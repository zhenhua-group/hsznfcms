﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditRolePowerControl.ascx.cs"
    Inherits="TG.Web.UserControl.EditRolePowerControl" %>
<style type="text/css">
    /*数据基本样式*/
    .cls_content_head
    {
        width: 100%;
        margin: 0 auto;
        font-size: 12px;
        border-collapse: collapse;
        font-family: 微软雅黑;
        margin-top: 2px;
        border-bottom: none;
    }
    .cls_content_head th
    {
        height: 25px;
        border: 1px solid Gray;
    }
    /*表格基本样式*/.show_projectNumber
    {
        width: 100%;
        border: solid 1px #CCC;
        border-collapse: collapse;
        margin: 0 auto;
    }
    
    .show_projectNumber td
    {
        border: solid 1px #CCC;
        font-size: 12px;
        font-family: "微软雅黑";
        height: 25px;
    }
    .style1
    {
        width: 30%;
        height: 20px;
    }
    .style2
    {
        width: 50%;
        height: 20px;
    }
    .style3
    {
        width: 10%;
        height: 20px;
    }
</style>

<div id="editRolePowerControlMain">
    <div class="panel panel-success">
        <div class="panel-heading" style="padding: 0; line-height: 25px;">
            &nbsp;<i class="fa fa-cogs"></i>
            <span class=" " id="editRolePowerControl_Title"></span>
        </div>
        <div class="panel-body">
            <%-- <input type="button" value="保存" id="saveRolePower" />--%>
            <table class="cls_content_head " style="text-align: center; width: 100%">
                <tr>
                    <th style="width: 115px;">
                        描述
                    </th>
                    <th style="width: 285px;">
                        页面名称
                    </th>
                    <th style="width: 148px;">
                        查看权限
                    </th>
                    <th style="width: 115px;">
                        编辑权限
                    </th>
                </tr>
            </table>
            <table style="width: 100%;" class="show_projectNumber" id="editRolePowerControl">
                <asp:Repeater ID="RepeaterEditRolePower" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td style="width: 115px; text-align: center;">
                                <%#Eval("Description") %>
                            </td>
                            <td style="width: 300px; text-align: center;">
                                <%#Eval("PageName") %>
                            </td>
                            <td style="width: 145px;" actionpowersysno="<%#Eval("ActionPowerSysNo") %>" rolepowersysno="<%#Eval("RolePowerSysNo") %>">
                                <input type="radio" id="previewOwnRadio" name="previewPower_<%#Eval("Description") %>" />个人
                                <input type="radio" id="previewUnitRadio" name="previewPower_<%#Eval("Description") %>" />部门
                                <input type="radio" id="previewAllRadio" name="previewPower_<%#Eval("Description") %>"
                                    checked="checked" />全部
                            </td>
                            <td style="width: 115px;">
                                <input type="checkbox" id="editPowerCheckBox" />编辑
                                <input type="checkbox" id="deletePowerCheckBox" />删除
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="editRolePowerControl_RoleSysNo" />
    <input type="hidden" id="editRolePowerControl_RoleName" />
</div>
