﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="left_proj_sub.aspx.cs" Inherits="TG.Web.leftmenu.left_proj_sub" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/astreeview/astreeview/astreeview_packed.js"></script>

    <script type="text/javascript" src="../js/astreeview/contextmenu/contextmenu_packed.js"></script>

    <link href="../css/left_tree.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        //显示图纸列表
        function showContent(proid, classid, id, parentid, item) {
            var argsUrl = "../Project/DwgFileList.aspx?proid=" + proid + "&classid=" + classid + "&id=" + id + "&parentid=" + parentid + "&item=" + item + "&purposeid=" + id;
            window.parent.document.getElementById("rightFrame").src = argsUrl;
        }
</script>
</head>
<body oncontextmenu="window.event.returnValue=false">
    <form id="form1" runat="server">
  <div id="logo">详细信息>></div>
        <cc1:ASTreeView ID="ASTreeView1" runat="server" AddNodeProvider="" DeleteNodeProvider=""
            EditNodeProvider="" DefaultFolderIcon="~/js/astreeview/astreeview/images/dir-close.gif"
            DefaultFolderOpenIcon="~/js/astreeview/astreeview/images/dir-open.gif" DefaultNodeIcon="~/js/astreeview/astreeview/images/dir-close.gif"
            EnableCheckbox="False" EnableDragDrop="False" LoadNodesProvider="~/leftmenu/left_proj_sub.aspx"
            RootNodeText="" RootNodeValue="" Font-Size="9pt" Width="400px" 
        EnableContextMenu="False" EnableContextMenuAdd="False" 
        EnableContextMenuDelete="False" EnableContextMenuEdit="False" 
        EnableParentNodeExpand="False" EnableCustomizedNodeIcon="True" 
        AdditionalLoadNodesRequestParameters="{'t1':'ajaxLoad'}" 
        BasePath="~/js/astreeview/astreeview/" />
    </form>
</body>
</html>
