﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ShowFromCopLeft.aspx.cs" Inherits="TG.Web.leftmenu.ShowFromCopLeft" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#show7_1").parent().attr("class", "active");
            $("#arrow7").attr("class", "arrow open");
            $("#show7_1").parent().parent().css("display", "block");

        });
        //清空数据
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        ISO表单信息 <small>合同表单</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>ISO表单信息<i class="fa fa-angle-right"> </i>合同表单</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-3">
            <iframe name="centerFrame" id="centerFrame" height="700px" width="100%" border="0"
                frameborder="0" src="ShowISOFrom.aspx">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe>
        </div>
        <div class="col-md-9">
            <iframe name="centerFrame" id="Iframe1" height="700px" width="100%" border="0" frameborder="0"
                src="../mainpage/ISOlist.html">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe>
        </div>
    </div>
</asp:Content>
