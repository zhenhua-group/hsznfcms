﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="LeftProj_SubBymaster.aspx.cs" Inherits="TG.Web.leftmenu.LeftProj_SubBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#show5_1").parent().attr("class", "active");
            $("#arrow5").attr("class", "arrow open");
            $("#show5_1").parent().parent().css("display", "block");

        });
        //清空数据
       
    </script>
    <style type="text/css">
        body
        {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        .navPoint
        {
            color: white;
            cursor: hand;
            font-family: Webdings;
            font-size: 9pt;
        }
        #loading
        {
            position: fixed !important;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 999;
            background: #CCC url(images/load.gif) no-repeat center center;
            opacity: 0.3;
            filter: alpha(opacity=60);
            font-size: 14px;
            line-height: 20px;
        }
        #loading-one
        {
            color: #000;
            position: absolute;
            top: 50%;
            left: 50%;
            margin: 20px 0 0 -50px;
            padding: 3px 10px;
        }
    </style>
    <script type="text/javascript">
        function switchSysBar() {
            var locate = location.href.replace('../leftmenu/LeftProj_SubBymaster.aspx', '');
            var ssrc = document.all("img1").src.replace(locate, '');

            var reg = /http:\/\/[^\/]+/;
            ssrc = ssrc.replace(reg, "");

            if (ssrc == "/images/main_55.gif") {
                document.all("img1").src = "../images/main_55_1.gif";
                document.all("frmTitle").style.display = "none";
            }
            else {
                document.all("img1").src = "../images/main_55.gif";
                document.all("frmTitle").style.display = "";
            }
        }
        //初始加载
        $(document).ready(function () {
            $("#frmTitle2").hide();
            $("#frmTitleSub").hide();
            $("#img2").click(function () {
                $("#frmTitle2").hide(500);
                $("#frmTitleSub").hide();
            });
        });
        //显示等待
        function showWait() {
            $("#loading").show();
        }
        //隐藏等待
        function hideWait() {
            $("#loading").hide();
        }
    </script>
    <style media="print" type="text/css">
        .Noprint
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        企业项目展示 <small>项目展示</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a href="#">企业项目展示</a><i class="fa fa-angle-right"> </i><a href="#">项目展示</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <table width="100%" height="700px" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="220" id="frmTitle" nowrap name="frmTitle" align="center" valign="top">
                        <table width="220" height="100%" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;">
                            <tr>
                                <td width="220" class="Noprint">
                                    <iframe name="leftFrame" id="leftFrame" height="700px" width="220" src="left_proj.aspx"
                                        border="0" frameborder="0" scrolling="no">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 6px; background: url(images/slid.gif) repeat-y;cursor:pointer;" valign="middle"
                        bgcolor="#1873aa" onclick="switchSysBar()">
                        <span class="navPoint" id="switchPoint" title="关闭/打开左栏">
                            <img src="../images/main_55.gif" name="img1" id="img1" style="width: 6px; height: 60px;"></span>
                    </td>
                    <td width="220" id="frmTitle2" nowrap name="frmTitle2" align="center" valign="top"
                        style="display: none;">
                        <table width="220" height="700px" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;">
                            <tr>
                                <td width="220" class="Noprint">
                                    <iframe name="leftFrame2" id="leftFrame2" height="700px" width="220" src="../leftmenu/left_proj_sub.aspx"
                                        border="0" frameborder="0" scrolling="no">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 6px; background: url(images/slid.gif) repeat-y;cursor:pointer;display: none;"
                        valign="middle" id="frmTitleSub" bgcolor="#1873aa">
                        <span class="navPoint" id="Span1" title="关闭/打开左栏">
                            <img src="../images/main_55.gif" name="img2" width="6" height="60" id="img2" /></span>
                    </td>
                    <td width="100%" align="center" valign="top">
                        <iframe name="rightFrame" id="rightFrame" height="700px" width="100%" border="0"
                            frameborder="0" src="../mainpage/packlist.html">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe>
                    </td>
                </tr>
            </table>
            <%--        <iframe name="centerFrame" id="centerFrame" height="700px" width="100%" border="0"
                frameborder="0" src="left_proj.aspx">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe>
        </div>
        <div class="col-md-3" style="display: none;" id="frmTitle2">
            <iframe name="centerFrame" id="leftFrame2" height="700px" width="100%" border="0"
                frameborder="0" src="left_proj_sub.aspx">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe>
        </div>
        <div class="col-md-9">
            <iframe name="centerFrame" id="rightFrame" height="700px" width="100%" border="0"
                frameborder="0" src="../mainpage/packlist.html">浏览器不支持嵌入式框架，或被配置为不显示嵌入式框架。</iframe> --%>
        </div>
    </div>
</asp:Content>
