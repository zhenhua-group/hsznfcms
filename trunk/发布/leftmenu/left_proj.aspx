﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="left_proj.aspx.cs" Inherits="TG.Web.leftmenu.left_proj" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/astreeview/astreeview/astreeview_packed.js"></script>
    <script type="text/javascript" src="../js/astreeview/contextmenu/contextmenu_packed.js"></script>
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <link href="../css/left_tree.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/left_proj.js"></script>
</head>
<body oncontextmenu="window.event.returnValue=false">
    <form id="form1" runat="server">
    <table class="tb_top_menu">
        <tr>
            <td>
                <input id="btn_bumen" type="image" src="../Images/btn_top_menu_bumen.gif" />
            </td>
            <td>
                <input id="btn_nianfen" type="image" src="../Images/btn_top_menu_nianfen.gif" />
            </td>
            <td>
                <input id="btn_geren" type="image" src="../Images/btn_top_menu_geren.gif" />
            </td>
        </tr>
    </table>
    <div id="Frist" style="height: 200px;">
        <cc1:ASTreeView ID="ASTreeView1" runat="server" AdditionalLoadNodesRequestParameters="{'t1':'ajaxLoad'}"
            AddNodeProvider="~/leftmenu/left_proj.aspx" DeleteNodeProvider="" EditNodeProvider=""
            EnableCheckbox="False" EnableDragDrop="False" LoadNodesProvider="~/leftmenu/left_proj.aspx"
            DefaultFolderIcon="~/js/astreeview/astreeview/images/dir-close.gif" DefaultFolderOpenIcon="~/js/astreeview/astreeview/images/dir-open.gif"
            DefaultNodeIcon="~/js/astreeview/astreeview/images/progress.bmp" RootNodeText="项目信息列表"
            EnableContextMenu="False" EnableContextMenuAdd="False" EnableContextMenuDelete="False"
            EnableContextMenuEdit="False" Font-Size="12pt" Width="400px" EnableCustomizedNodeIcon="True"
            BasePath="~/js/astreeview/astreeview/" OnNodeSelectedScript="GEtdiv()" OnNodeCheckedScript="Alt()" />
        <div style="width: 100%; height: 23px;">
        </div>
    </div>
    <div id="divscroll" style="background-color: #efeeec; height: 26px; width: 215px;
        position: absolute; bottom: 0px;">
        <center>
            <img src="../Images/shang(1).png" alt="向上滚动" id="up" style="float: none; height: 24px;
                width: 24px;" />
            <img src="../Images/xia(1).png" alt="向下滚动" id="down" style="float: none; height: 24px;
                width: 24px; margin-left: 10px;" />
        </center>
    </div>
    <script type="text/javascript">
        var client = document.documentElement.clientHeight
        function GEtdiv() {

            var Sys = {};

            var ua = navigator.userAgent.toLowerCase();

            if (window.ActiveXObject) {
                Sys.ie = ua.match(/msie ([\d.]+)/)[1];

            }
            else if (document.getBoxObjectFor) {


                Sys.firefox = ua.match(/firefox\/([\d.]+)/)[1]
                alert("FIrefox");
            }

            else if (window.MessageEvent && !document.getBoxObjectFor) {

                Sys.chrome = ua.match(/chrome\/([\d.]+)/)[1];
                window.scrollTo(0, 0);
                $("#divscroll").css("top", client - 26);
            }


            else if (window.opera) {
                Sys.opera = ua.match(/opera.([\d.]+)/)[1];


            }


            else if (window.openDatabase) {
                Sys.safari = ua.match(/version\/([\d.]+)/)[1];
            }
        }

        function Alt() {
            alert("serfff");
        }
        $(document).ready(function () {
            var Sys = {};
            var ua = navigator.userAgent.toLowerCase();
            if (window.ActiveXObject)
                Sys.ie = ua.match(/msie ([\d.]+)/)[1]
            else if (document.getBoxObjectFor)
                Sys.firefox = ua.match(/firefox\/([\d.]+)/)[1]
            else if (window.MessageEvent && !document.getBoxObjectFor)
                Sys.chrome = ua.match(/chrome\/([\d.]+)/)[1];
            var client = document.documentElement.clientHeight
            var heigma = $(window).height();
            var height = $("body").outerHeight();
            var lastup;
            var lastdown;
            var height = $("body").outerHeight();
            $("#divscroll").css("top", client - 26)
            var speeds = 7;
            var ser = 0;
            function Marquee1() {
                //以下进行测试
                if (Sys.ie) {
                    var hei = document.body.scrollTop || document.documentElement.scrollTop;
                    var topde = $("#divscroll").css("top");

                    hei--;
                    document.documentElement.scrollTop = hei;
                    if (parseInt(topde) > parseInt(heigma) - 25) {
                        $("#divscroll").css("top", parseInt(topde) - 25);
                        lastup = parseInt(topde) - 25;
                    }
                }
                else if (Sys.firefox) {
                    document.write('Firefox: ' + Sys.firefox);
                }
                else if (Sys.chrome) {
                    var topde = $("#divscroll").css("top");
                    var hei = window.pageYOffset;
                    hei--;
                    window.scrollTo(0, hei);
                    if (parseInt(topde) > parseInt(heigma) - 26) {
                        $("#divscroll").css("top", parseInt(topde) - 1);
                        lastup = parseInt(topde) - 1;
                    }
                }
                else {
                    alert("换台");
                }
            }


            function Marquee2() {
                if (Sys.ie) {
                    //document.write('IE: ' + Sys.ie);
                    $("#divscroll").css("position", "fixed");
                    var hei = document.body.scrollTop || document.documentElement.scrollTop;
                    var topde = $("#divscroll").css("top");
                    //alert(topde);
                    //var hei = window.pageYOffset;
                    hei++;
                    document.documentElement.scrollTop = hei;
                    if (parseInt(topde) > parseInt(heigma) - 25) {
                        $("#divscroll").css("top", parseInt(topde) - 25);
                        lastup = parseInt(topde) - 1;
                    }
                }
                else if (Sys.firefox) {
                    document.write('Firefox: ' + Sys.firefox);
                }
                else if (Sys.chrome) {
                    $("#divscroll").css("top", heigma - 25);
                    var hei = window.pageYOffset;
                    hei++;
                    window.scrollTo(0, hei);
                    $("#divscroll").css("top", heigma - 25 + hei);
                    lastdown = height - 25 + hei;
                }

            }
            $("#up").mouseover(function () {
                MyMar1 = setInterval(Marquee1, speeds)
            }).mouseout(function () {
                clearInterval(MyMar1);
                $("#divscroll").css("top", lastup);

            });
            $("#down").mouseover(function () {
                MyMar2 = setInterval(Marquee2, speeds)
            }).mouseout(function () {
                clearInterval(MyMar2);
            });
        });
      
    </script>
    </form>
</body>
</html>
