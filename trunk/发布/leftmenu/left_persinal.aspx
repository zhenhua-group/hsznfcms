﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="left_persinal.aspx.cs" Inherits="TG.Web.leftmenu.left_persinal" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link href="../css/left.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../js/left.js"></script>
    <script type="text/javascript" src="../js/left_persinal.js"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table width="214" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="23" background="../images/bg_tle.gif" class="menu_title">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="18%">&nbsp;</td>
                <td width="82%" class="cls_title">个人基本信息</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td>
			<table class="cls_menucont">
              <tr>
                <td>
                <table>
                  <tr>
                    <td><table>
                        <tr>
                          <td class="cls_menu"><span class="cls_menu_title" id="link_persinal">个人资料</span></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table>
                        <tr>
                          <td class="cls_menu"><span class="cls_menu_title" id="link_edit_persinal">修改资料</span></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table>
                        <tr>
                          <td class="cls_menu"><span class="cls_menu_title" id="linke_chgpwd">修改密码</span></td>
                        </tr>
                    </table></td>
                  </tr>
                  
                </table></td>
              </tr>
              <tr>
                <td height="5"></td>
              </tr>
            </table></td>
          </tr>
          
        </table></td>
      </tr>
      

    </table></td>
  </tr>
  <tr>
    <td height="18" background="../images/main_58_gray.gif">
    <table class="cls_menucont">
      <tr>
        <td height="18" valign="bottom"><div align="center" class="cls_vrs">版本：2012V1.0</div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
    </form>
</body>
</html>
