﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProNumberAllotBymaster.aspx.cs" Inherits="TG.Web.ProjectNumber.ProNumberAllotBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/ProjectNumber/ProNumberAllotBymaster.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>工程号申请</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>项目管理<i class="fa fa-angle-right"> </i>工程号申请</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">项目信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 120px;">项目名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_name" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                        </td>
                                        <td style="width: 120px;">合同关联:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_reletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>管理级别:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td>审核级别 :
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设单位:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtbuildAddress" runat="server"></asp:Label>
                                        </td>
                                        <td>建设规模:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_scale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                        </td>
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_unit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>结构样式:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="structType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                        <td>建筑分类:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <asp:Literal ID="buildStructType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>设计阶段:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td>项目来源:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtproAcount" runat="server" Width="100px"></asp:Label>万元
                                        </td>
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_Aperson" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_phone" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>执行设总:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_PMName" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_PMPhone" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目开始时间:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_startdate" runat="server"></asp:Label>
                                        </td>
                                        <td>项目完成时间:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_finishdate" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目概况:
                                        </td>
                                        <td colspan="3">
                                            <div style="height: auto;">
                                                <span id="txt_sub" runat="Server"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目备注:
                                        </td>
                                        <td colspan="3">
                                            <div style="height: auto;">
                                                <span id="txt_remark" runat="Server"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目高清图:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-12" id="img_container" style="text-align: center">
                                                <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                                    style="width: 250px; height: 150px;" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>项目工程号:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-2">
                                                <input type="text" class="form-control input-sm" id="proNumber"
                                                    runat="server" />
                                            </div>
                                            <div class="col-md-6">
                                                <button type="button" class="btn red btn-sm" id="btnfygh" href="#GCBH" data-toggle="modal">
                                                    编号赋予</button>
                                                <button type="button" class="btn red btn-sm" id="btn_modify" href="#GCBH" data-toggle="modal">
                                                    修改</button>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="submit" class="btn green" id="btn_Save">
                                保存</button>
                            <button type="button" class="btn default" onclick="javascript:history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="GCBH" class="modal fade yellow" tabindex="-1" data-width="460" aria-hidden="true"
        style="display: none; width: 460px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择设计编号</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_Number" style="text-align: center">
                <div class="row">
                    <div class="col-md-4">
                        项目类型:
                    </div>
                    <div class="col-md-6">
                        <select id="cpr_typeSelect" class="form-control input-sm">
                        </select>

                    </div>
                    <div class="col-md-4">
                        设计编号:
                    </div>
                    <div class="col-md-6">
                        <select id="cpr_numSelect" class="form-control input-sm">
                        </select>
                        <span id="noselectMsg" style="color: red; display: none;">*</span> <span class="help-block">
                            <font><font class=""></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input id="btn_cprNum_close" type="button" value="确定" data-dismiss="modal" class="btn green" />
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <asp:HiddenField ID="HiddenNum" runat="Server" />
</asp:Content>
