﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProNumberSet.ascx.cs"
    Inherits="TG.Web.ProjectNumber.ProNumberSet" %>
<table style="width: 100%">
    <tr>
        <td>
            <fieldset style="height: 80px; width: 250px">
                <legend>选项</legend>
                <input id="Radio1" checked="true" name="R1" type="radio" value="按照项目类型设置编号" />按照项目类型设置编号<br />
                <input id="Radio2" name="R1" type="radio" value="按照项目类型设置编号" />不按项目类型设置编号</fieldset>
        </td>
        <td align="center">
            <asp:Button ID="btnsave" runat="server" CssClass ="btn" Text="保存" />
            <asp:Button ID="btnedit" runat="server" Text="编辑" CssClass ="btn" />
        </td>
    </tr>
</table>
<fieldset class="xmlx">
    <legend>项目类型</legend>
    <table style="width: 100%;" class="show_projectNumber">
        <tr>
            <td>
                &nbsp;<input id="btnjzgc" type="button" value="建筑工程" />
            </td>
            <td>
                起始编号：
            </td>
            <td>
                <input id="txtjzstartNo" type="text" />
            </td>
            <td>
                结束编号：
            </td>
            <td>
                <input id="txtjzfinishNo" type="text" />
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
                &nbsp;<input id="btncxgh" type="button" value="城乡规划" />
            </td>
            <td style="width: 10%">
                起始编号：
            </td>
            <td>
                <input id="txtcxstartNo" type="text" />
            </td>
            <td style="width: 10%">
                结束编号：
            </td>
            <td>
                <input id="txtcxfinishNo" type="text" />&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<input id="btnszgc" type="button" value="市政工程" />
            </td>
            <td>
                起始编号：
            </td>
            <td>
                <input id="txtszstartNo" type="text" />
            </td>
            <td>
                结束编号：
            </td>
            <td>
                <input id="txtszfinishNo" type="text" />
            </td>
        </tr>
        <tr>
            <td>
                <input id="btngckc" type="button" value="工程勘察" />
            </td>
            <td>
                起始编号：
            </td>
            <td>
                <input id="txtgcstartNo" type="text" />
            </td>
            <td>
                结束编号：
            </td>
            <td>
                <input id="txtgcfinishNo" type="text" />
            </td>
        </tr>
        <tr>
            <td>
                <input id="btnzx" type="button" value="咨询" />
            </td>
            <td>
                起始编号：
            </td>
            <td>
                <input id="txtzxstartNo" type="text" />
            </td>
            <td>
                结束编号：
            </td>
            <td>
                <input id="txtzxfinishNo" type="text" />
            </td>
        </tr>
        <tr>
            <td>
                <input id="btnqt" type="button" value="其他" />
            </td>
            <td>
                起始编号：
            </td>
            <td>
                <input id="txtqtstartNo" type="text" />
            </td>
            <td>
                结束编号：
            </td>
            <td>
                <input id="txtqtfinishNo" type="text" />
            </td>
        </tr>
    </table>
</fieldset>
<fieldset class="xmgh" style="display: none;">
    <legend>项目类型</legend>
    <table style="width: 100%;"  class="show_projectNumber">
        <tr>
            <td style="width: 10%">
                <input id="btnxmgh" type="button" value="项目工号" />
            </td>
            <td style="width: 10%">
                起始编号：
            </td>
            <td>
                <input id="txtxmghstartNo" type="text" />
            </td>
            <td style="width: 10%">
                &nbsp; 结束编号：
            </td>
            <td>
                <input id="txtxmghfinishNo" type="text" />
            </td>
        </tr>
    </table>
</fieldset>

<script type="text/javascript">
    $(function() {
        $('#Radio1').click(function() {
            $('.xmlx').css('display', 'block');
            $('.xmgh').css('display', 'none');
        });

        $('#Radio2').click(function() {
            $('.xmlx').css('display', 'none');
            $('.xmgh').css('display', 'block');
        });

    })
</script>

