﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PronumModify.aspx.cs" Inherits="TG.Web.ProjectNumber.PronumModify" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectNumber.css" rel="stylesheet" type="text/css" />
    <link href="../css/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>

    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script src="../js/ProjectNumber/ProNumberAllot.js" type="text/javascript"></script>

    <script type="text/javascript">

        function check_jd() {

            var operation = $("#jdlist").val();
            var type = $("#typelist").val();
            var jb = $("#projb").val();
            var opera = document.getElementsByTagName("input");
            var list = operation + ',' + type+','+jb;
            var op = list.split(",");
            for (var j = 0; j < opera.length; j++) {
                for (var i = 0; i < op.length; i++) {
                    if ($.trim(op[i]) ==$.trim(opera[j].value)) {
                        opera[j].checked = true;

                        break;
                    }
                }
            }

        }

    </script>

    </head> 
<body bgcolor="f0f0f0" onload="check_jd()">
    <form id="form1" runat="server">
     <table class="cls_container">
        <tr>
            <td class="style2">
                &nbsp;&nbsp;当前位置：[项目管理]-[工号分配]
            </td>
        </tr>
    </table>
    <table style="width: 100%; line-height: 25px" class="show_projectNumber">
        <tr>
            <td style="width: 10%">
                项目名称
            </td>
            <td>
                <asp:TextBox ID="txtproname" runat="server" Width="200px" ReadOnly="True" CssClass ="text"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
                项目工号
            </td>
            <td>
                <asp:TextBox ID="proNumber" runat="server" CssClass ="text"></asp:TextBox>
                <a href ="#" id="btnfygh">编号赋予</a>
                <%--<input id="btnfygh" type="button" value="编号赋予" class="btn" />--%>
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
                设计阶段
            </td>
            <td>
                <input type="hidden" id="jdlist" value="<%=projd %>" />
                <input id="Checkbox1" type="checkbox" class="jd" disabled="disabled" value="方案" />方案
                <input id="Checkbox2" type="checkbox" class="jd" disabled="disabled" value="初设" />初设
                <input id="Checkbox3" type="checkbox" class="jd" disabled="disabled" value="施工图" />施工图
            </td>
        </tr>
        <tr>
            <td>
                管理级别
            </td>
            <td>
            <input type="hidden" id="projb" value ="<%=projb %>" />
                <input id="Radio1" name="R3" type="radio" disabled="disabled" value="1" />一级
                <input id="Radio2" name="R3" type="radio" disabled="disabled" value="2" />二级
                <input id="Radio3" name="R3" type="radio" disabled="disabled" value="3" />三级
            </td>
        </tr>
        <tr>
            <td>
                项目类型
            </td>
            <td>
                <input type="hidden" id="typelist" value="<%=protype %>" />
                <input id="Checkbox4" type="checkbox" disabled="disabled" value="建筑工程" />建筑工程
                <input id="Checkbox5" type="checkbox" disabled="disabled" value="城乡规划" />城乡规划
                <input id="Checkbox6" type="checkbox" disabled="disabled" value="市政工程" />市政工程
                <input id="Checkbox7" type="checkbox" disabled="disabled" value="工程勘察" />工程勘察
                <input id="Checkbox8" type="checkbox" disabled="disabled" value="咨询" />咨询
                <input id="Checkbox9" type="checkbox" disabled="disabled" value="其他" />其他
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" style="height: 50px;">
                <asp:ImageButton ID="btnok" runat="server" ImageUrl="~/Images/buttons/btn_save.gif" 
                    onclick="btnok_Click" Height="23px" Width="65px" />
                   <a href ="ProNumber.aspx" ><img src="../Images/buttons/btn_back.gif" style="width:65px;height:23px;border:none;" /></a>
        </tr>
    </table>
   
   <%-- <div id="proNum" style="height: 200px; width: 350px; display :none ;">
    
     <table style="width: 100%;">
            <tr>
                <td>
                    项目类型：<asp:DropDownList ID="drp_protype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drp_protype_SelectedIndexChanged"
                        Width="150px" AppendDataBoundItems="True" CssClass="TextBoxBorder">
                        <asp:ListItem Value="-1">-----请选择-----</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    项目工号：<asp:DropDownList ID="drp_protypenum" runat="server" Width="150px" CssClass="TextBoxBorder">
                    </asp:DropDownList>
                    <asp:TextBox ID="txt_pronum" runat="server" ForeColor="#999999" CssClass="TextBoxBorder">例:XXXXX-001</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="btnghok" type="button" value="确定" />
                </td>
            </tr>
        </table>
       <%-- <table style="width: 100%;" class="show_projectNumber">
            <tr>
                <td style="width: 20%">
                    项目类型
                </td>
                <td class="style3">
                    <input id="txtprotype" type="text" style="width: 150px"  runat="server" disabled ="disabled"  />
                </td>
            </tr>
            <tr>
                <td style="width: 10%">
                    项目工号
                </td>
                <td class="style3">
                    <asp:DropDownList ID="ddproNumber" runat="server" Width="150px">
                        <asp:ListItem Value="0">---请选择项目工号----</asp:ListItem>
                        <asp:ListItem>JZ2012-001</asp:ListItem>
                        <asp:ListItem>JZ2012-002</asp:ListItem>
                        <asp:ListItem>JZ2012-003</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center" style="height: 50px">
                    &nbsp; &nbsp;
                    <asp:Button ID="btnghok" runat="server" Text="确定" CssClass="btn" />
                    <asp:Button ID="btnghcancle" runat="server" Text="取消" CssClass="btn" />
                </td>
            </tr>
        </table>
    </div> --%>
    </form>
</body>
</html>