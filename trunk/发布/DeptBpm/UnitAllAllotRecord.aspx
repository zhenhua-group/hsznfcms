﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitAllAllotRecord.aspx.cs" Inherits="TG.Web.DeptBpm.UnitAllAllotRecord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="../js/assets/plugins/data-tables/DT_bootstrap.css" />

    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />

    <script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/DT_bootstrap.js"></script>


    <script src="../js/DeptBpm/UnitAllotRecord.js?v=20170516"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>年度总奖金分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>年度总奖金分配</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>年度总奖金分配
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">项目部门:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged"
                                        AppendDataBoundItems="True" AutoPostBack="true">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">职位状态:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drpZw" runat="server" OnSelectedIndexChanged="drpZw_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="0">在职</asp:ListItem>
                                        <asp:ListItem Value="1">离职</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 150px; display: none;">
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" Visible="True" runat="server" Text="查询" OnClick="btn_Search_Click" />


                                </td>
                                <td>
                                    <% if (this.UserShortName == "admin")
                                       {

                                           if (!this.IsBak)
                                           {%>

                                    <a href="#" class="btn btn-sm yellow" id="btnBak">发起归档</a>
                                    <%}
                                           else
                                           {%>
                                    <a href="#" class="btn btn-sm default disabled">已归档</a>

                                    <%}
                                       } %>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th colspan="3">
                                        <a href="<%= RecordUrl %>" class="btn btn-sm blue active">奖金分配</a>
                                        <a href="<%= RecordCompareUrl %>" class="btn btn-sm blue" target="_blank">数据对比</a>
                                    </th>
                                    <th colspan="5">
                                        <% if (!IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                
                                        <%}
                                           else
                                           { %>
                                        <a href="#" class="btn btn-sm red disabled">保存</a>
                                        <a href="#" class="btn btn-sm red disabled">确认提交</a>
                                        
                                        <asp:Button CssClass="btn btn-sm red" ID="btn_Export" runat="server" Text="导出数据" OnClick="btn_Export_Click" />
                                         
                                        <%} %>

                                        <% //if (this.UserShortName == "admin"&&(!IsSubmit))
                                           //重新调整
                                            if (this.UserShortName == "admin")
                                            { %>
                                           
                                       
<%--                                        <a href="#" class="btn btn-sm red" id="btnSubmitss">确认提交</a>--%>
                                            
                                            <a href="#"id="btnSavesss" class="btn red">保存</a>
                                          
                                          <asp:Button  ID="Button1cx" CssClass="btn  red" runat="server" Text="重新调整" OnClick="btn_Export_Clickcx" />
                                                                   
                                          <a href="#addKaoHeMem" data-toggle="modal" class="btn btn-sm green" asetmem="addname" title="添加考核外人员">添加人员</a>
                                        <%} %>
                                    </th>

                                    <th colspan="10"></th>
                                </tr>
                                <tr class="warning">
                                    <th>合计：</th>
                                    <th></th>
                                    <th></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                                    <th style="text-align: right;"></th>
                              
                                </tr>
                                <tr>
                                    <th style="width: 100px;">部门名称</th>
                                    <th style="width: 70px;"><a href="#" title="点击恢复默认排名" id="btnDefault">姓名</a></th>
                                    <th style="width: 70px;">职位状态</th>
                                    <th style="width: 100px;">部门经理调整</th>
                                    <th style="width: 80px;">公司调整</th>
                                    <th style="width: 110px;">项目奖金计算值</th>
                                    <th style="width: 130px;">部门内奖金计算值</th>
                                    <th style="width: 70px;">项目奖金</th>
                                    <th style="width: 90px;">部门内奖金</th>
                                    <th style="width: 90px;">项目管理奖</th>
                                    <th style="width: 90px;">优秀员工奖</th>
                                    <th style="width: 90px;">突出贡献奖</th>
                                    <th style="width: 100px;">BIM及标准化</th>
                                    <th style="width: 70px;">注册费用</th> 
                                    <th style="width: 70px;">工会补助</th>
                                    <th style="width: 70px;">合计(含预发)</th>
                                    <th style="width: 70px;">预发奖金</th>
                                    <th style="width: 70px;">合计</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    decimal? hjcol = 0;
                                    decimal? hjcol2 = 0;
                                    decimal? hjcol3 = 0;
                                    decimal? hjcol4 = 0;
                                    decimal? hjcol5 = 0;
                                    decimal? hjcol6 = 0;
                                    decimal? hjcol7 = 0;
                                    decimal? hjcol8 = 0;
                                    decimal? hjcol9 = 0;                                   
                                    decimal? hjcol10 = 0;
                                    decimal? hjcol11 = 0;
                                    decimal? hjcol12 = 0;
                                    decimal? hjcol13 = 0;
                                    decimal? hjcol14 = 0;
                                    decimal? hjcol20 = 0;

                                    if (!IsSave)
                                    {
                                      //  int x = UnitMemsSaveList.Rows.Count;
                                        if (UnitMemsList.Rows.Count > 0)
                                        {
                                            decimal hj = 0;
                                            decimal xmjj = 0;
                                            decimal bmnjj = 0;
                                            decimal hjyf = 0;
                                            foreach (System.Data.DataRow dr in UnitMemsList.Rows)
                                            {
                                                hj = 0;
                                                hjyf = 0;
                                %>
 
                                 <tr>
                                    <td><span unitid="<%= dr["unitID"].ToString()%>"><%= dr["unitName"].ToString()%></span></td>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td>
                                        <% if (dr["mem_isFired"].ToString() == "0")
                                           { %>
                                        <span isfired="0">在职</span>
                                        <%}
                                           else
                                           { %>
                                        <span isfired="1">离职</span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol += Convert.ToDecimal(dr["bmjl"].ToString());
                                        %>


                                        <% if (!IsSubmit)
                                           { %>
                                        <input  type="text" name="name" value="<%= Convert.ToDecimal(dr["bmjl"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <input type="hidden" name="name" value="<%= Convert.ToDecimal(dr["bmjl"].ToString()).ToString("f0")%>" />
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["bmjl"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                <%--     公司调整--%>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol2 += Convert.ToDecimal(dr["gstz"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                                
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["gstz"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <input type="hidden" name="name" value="<%= Convert.ToDecimal(dr["gstz"].ToString()).ToString("f0")%>" />

                                        <%}
                                          else
                                          {%>
                                         
                                        <%=  Convert.ToDecimal(dr["gstz"].ToString()).ToString("f0")%>

                                        <%}
                                          %>
                                               
                                    </td>
                                    
                                    <%-- 项目奖金计算值--%>
                                    <%
                                                    if (!SpecialUnitList.Contains(int.Parse(dr["unitID"].ToString())))
                                                    { %>
                                    <td style="text-align: right;">
                                        <%
                                                        hjcol4 += Convert.ToDecimal(dr["AllotCountBefore"].ToString());
                                        %>
                                        <span><%= Convert.ToDecimal(dr["AllotCountBefore"].ToString()).ToString("f0")%></span>
                                       
                                    </td>
                                    <%}
                                     else
                                     { %>
                                    <td style="background-color: gray;">
                                        <span style="color: gray;">0</span>
                                    </td>
                                    <%} 
                                   %>

                      <%--               部门内奖金计算--%>
                                     <%--<td style="text-align: right;">
                                        <%
                                                   //hjcol5 += Convert.ToDecimal(dr["bmnjjjs"].ToString());
                                                hjcol5 += Math.Round(Convert.ToDecimal(dr["bmnjjjs"].ToString()) * (decimal)0.01) * 100;
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                         <%=  Convert.ToDecimal(dr["bmnjjjs"].ToString()).ToString("f0")%>
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["bmnjjjs"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>--%>
                                      <td style="text-align: right;">
                                        <%
                                                    hjcol5 += Convert.ToDecimal(dr["bmnjjjs"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["bmnjjjs"].ToString())*(decimal)0.01)*100%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["bmnjjjs"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                     

                                      <%
                                                //项目奖金
                                                if (!SpecialUnitList.Contains(int.Parse(dr["unitID"].ToString())))
                                                { %>
                                    <td style="text-align: right;">
                                        <%
                                                    xmjj = Convert.ToDecimal(dr["AllotCountBefore"].ToString()) + Convert.ToDecimal(dr["MngCount"].ToString()) - Convert.ToDecimal(dr["yfjj"].ToString());
                                                    hj += xmjj;
                                                    hjcol6 += xmjj;
                                        %>
                                        <span><%= xmjj.ToString("f0") %></span>
                                    </td>
                                    <%}
                                                else
                                                { %>
                                    <td style="background-color: gray;">
                                        <span style="color: gray;">0</span>
                                    </td>
                                    <%} %>




                                    <%
                                            //特殊部门为部门内部奖金
                                           if (SpecialUnitList.Contains(int.Parse(dr["unitID"].ToString())))
                                           { %>
                            
                                     <td style="text-align: right;">
                                        <%
                                               bmnjj = Convert.ToDecimal(dr["bmnjj"].ToString()) + Convert.ToDecimal(dr["MngCount"].ToString()) - Convert.ToDecimal(dr["yfjj"].ToString());
                                                    hj += bmnjj;
                                                    hjcol7 += bmnjj;
                                        %>
                                        <span><%= bmnjj.ToString("f0")%></span>
                                    </td>
                                    <%}
                                                else
                                                {
                                                    bmnjj = Convert.ToDecimal(dr["bmnjj"].ToString());
                                                    hj += bmnjj;
                                    %>
                                    <td style="text-align: right;">
                                        <span><%= Convert.ToDecimal(dr["bmnjj"].ToString()).ToString("f0")%></span>
                                    </td>
                                    <%} %>

 
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol8 += Convert.ToDecimal(dr["xmglj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["xmglj"].ToString())%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["xmglj"].ToString()).ToString("f0")%>
                                        <%} %>

                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol9 += Convert.ToDecimal(dr["yxyg"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>

                                        <span><%= Convert.ToDecimal(dr["yxyg"].ToString()).ToString("f0")%></span>

                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["yxyg"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol10 += Convert.ToDecimal(dr["tcgx"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["tcgx"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["tcgx"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol11 += Convert.ToDecimal(dr["bimjj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["bimjj"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["bimjj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>                               
                                     <td style="text-align: right;">
                                        <%
                                                    hjcol20 += Convert.ToDecimal(dr["zcfy"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["zcfy"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" /> 
                                        <%}
                                           else
                                           { %>
                                          <%= Convert.ToDecimal(dr["zcfy"].ToString()).ToString("f0")%> 
                                        <%} %>
                                    </td> 

                                    <td style="text-align: right;">
                                        <%
                                                    hjcol12 += Convert.ToDecimal(dr["ghbt"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["ghbt"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["ghbt"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>

<%--                                    <td style="text-align: right;">
                                        <%
                                                hjyf += hj + Convert.ToDecimal(dr["yfjj"].ToString());
                                                hjcol14 += hjyf;
                                        %>
                                        <span><%= hjyf.ToString("f0")%></span>
                                    </td>--%>

                                     <td style="text-align: right;">
                                        <%
                                                    hjcol14 += Convert.ToDecimal(dr["yfjj"].ToString()) + Convert.ToDecimal(dr["hj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= (Convert.ToDecimal(dr["yfjj"].ToString())+ Convert.ToDecimal(dr["hj"].ToString())).ToString("f0")%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  (Convert.ToDecimal(dr["yfjj"].ToString())+ Convert.ToDecimal(dr["hj"].ToString())).ToString("f0")%>
                                        <%} %>
                                    </td>


                                    <%-- <td style="text-align: right;">
                                        <%
                                           //hj += Convert.ToDecimal(dr["yfjj"].ToString());
                                           hjcol3 += Convert.ToDecimal(dr["yfjj"].ToString()); 
                                        %>
                                        <span><%= Convert.ToDecimal(dr["yfjj"].ToString()).ToString("f0")%></span>
                                    </td>--%>
                                      <td style="text-align: right;">
                                        <%
                                                    hjcol3 += Convert.ToDecimal(dr["yfjj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Convert.ToDecimal(dr["yfjj"].ToString())%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["yfjj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>

                                    <%--<td style="text-align: right;">
                                        <%
                                           //如果人员离职，合计小于0为0
                                        
                                           if (dr["mem_isFired"].ToString() == "1")
                                           {
                                               if (hj < 0)
                                               {
                                                   hj = 0;
                                               }
                                           }
                                           hjcol13 += hj;
                                        %>
                                      <span><%= hj.ToString("f0")%></span>
                                  
                                         
                                    </td>--%>

                                      <td style="text-align: right;">
                                        <%
                                           //如果人员离职，合计小于0为0
                                           decimal? hjss = Convert.ToDecimal(dr["hj"].ToString());
                                           if (dr["mem_isFired"].ToString() == "1")
                                           {
                                               if (hj < 0)
                                               {
                                                   hj = 0;
                                               }
                                           }
                                           hjcol13 += hjss;
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Convert.ToDecimal(hjss).ToString("f0")%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>


                                    <td></td>
                                </tr>
                                <%}
                                        }
                                        
                                      }
                                    else
                                    {
                                        //无改动
                                        if (!this.IsBak)
                                        {

                                            if (UnitMemsSaveList.Rows.Count > 0)
                                            {

                                                foreach (System.Data.DataRow dr in UnitMemsSaveList.Rows)
                                                {
                                               
                                %>
                                <tr>
                                    <td><span unitid="<%= dr["unitID"].ToString()%>"><%= dr["unitName"].ToString()%></span></td>
                                    <td><span memid="<%= dr["mem_ID"].ToString()%>"><%= dr["mem_Name"].ToString()%></span></td>
                                    <td>
                                        <% if (dr["mem_isFired"].ToString() == "0")
                                           { %>
                                        <span isfired="0">在职</span>
                                        <%}
                                           else
                                           { %>
                                        <span isfired="1">离职</span>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol += Convert.ToDecimal(dr["bmjl"].ToString());
                                        %>


                                        <% if (!IsSubmit)
                                           { %>
                                        <input  type="text" name="name" value="<%= Convert.ToDecimal(dr["bmjl"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <input type="hidden" name="name" value="<%= Convert.ToDecimal(dr["bmjl"].ToString()).ToString("f0")%>" />
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["bmjl"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol2 += Convert.ToDecimal(dr["gstz"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                                
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["gstz"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <input type="hidden" name="name" value="<%= Convert.ToDecimal(dr["gstz"].ToString()).ToString("f0")%>" />

                                        <%}
                                          else
                                          {%>
                                         
                                        <%=  Convert.ToDecimal(dr["gstz"].ToString()).ToString("f0")%>

                                        <%}
                                          %>
                                               
                                    </td>
                                    

                                    <%
                                                    if (!SpecialUnitList.Contains(int.Parse(dr["unitID"].ToString())))
                                                    { %>
                                    <td style="text-align: right;">
                                        <%
                                                        hjcol4 += Convert.ToDecimal(dr["AllotCountBefore"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>

                                        <span><%= Convert.ToDecimal(dr["AllotCountBefore"].ToString()).ToString("f0")%></span>
                                        <input type="hidden" name="name" value="<%= Convert.ToDecimal(dr["AllotCountBefore"].ToString()).ToString("f0")%>" />

                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["AllotCountBefore"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <%}
                                                    else
                                                    { %>
                                    <td style="background-color: gray;"><span style="color: gray;">0</span></td>
                                    <%} %>

                                    <td style="text-align: right;">
                                        <%
                                                    hjcol5 += Convert.ToDecimal(dr["bmnjjjs"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Math.Round(Convert.ToDecimal(dr["bmnjjjs"].ToString())*(decimal)0.01)*100%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["bmnjjjs"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>


                                    <%
                                           //施工图部门
                                           if (!SpecialUnitList.Contains(int.Parse(dr["unitID"].ToString())))
                                           { %>
                                    <td style="text-align: right;">
                                        <%
                                               hjcol6 += Convert.ToDecimal(dr["xmjj"].ToString());                                            
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Convert.ToDecimal(dr["xmjj"].ToString()).ToString("f0")%></span>
                                        <input type="hidden" name="name" value="<%= Convert.ToDecimal(dr["xmjj"].ToString()).ToString("f0")%>" />

                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["xmjj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <%}
                                           else
                                           { %>
                                    <td style="background-color: gray;"><span style="color: gray;">0</span></td>
                                    <%} %>

                                    <td style="text-align: right;">
                                        <%
                                                    hjcol7 += Convert.ToDecimal(dr["bmnjj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Convert.ToDecimal(dr["bmnjj"].ToString())%></span>
                                        <input type="hidden" name="name" value="<%= Convert.ToDecimal(dr["bmnjj"].ToString()) %>" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["bmnjj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol8 += Convert.ToDecimal(dr["xmglj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["xmglj"].ToString())%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["xmglj"].ToString()).ToString("f0")%>
                                        <%} %>

                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol9 += Convert.ToDecimal(dr["yxyg"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>

                                        <span><%= Convert.ToDecimal(dr["yxyg"].ToString()).ToString("f0")%></span>

                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["yxyg"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol10 += Convert.ToDecimal(dr["tcgx"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["tcgx"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["tcgx"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol11 += Convert.ToDecimal(dr["bimjj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["bimjj"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["bimjj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>                               
                                     <td style="text-align: right;">
                                        <%
                                                    hjcol20 += Convert.ToDecimal(dr["zcfy"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["zcfy"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" /> 
                                        <%}
                                           else
                                           { %>
                                          <%= Convert.ToDecimal(dr["zcfy"].ToString()).ToString("f0")%> 
                                        <%} %>
                                    </td> 

                                    <td style="text-align: right;">
                                        <%
                                                    hjcol12 += Convert.ToDecimal(dr["ghbt"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <input type="text" name="name" value="<%= Convert.ToDecimal(dr["ghbt"].ToString()).ToString("f0")%>" style="width: 60px; text-align: right;" />
                                        <%}
                                           else
                                           { %>
                                        <%= Convert.ToDecimal(dr["ghbt"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol14 += Convert.ToDecimal(dr["yfjj"].ToString()) + Convert.ToDecimal(dr["hj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= (Convert.ToDecimal(dr["yfjj"].ToString())+ Convert.ToDecimal(dr["hj"].ToString())).ToString("f0")%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  (Convert.ToDecimal(dr["yfjj"].ToString())+ Convert.ToDecimal(dr["hj"].ToString())).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                    hjcol3 += Convert.ToDecimal(dr["yfjj"].ToString());
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Convert.ToDecimal(dr["yfjj"].ToString())%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["yfjj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                           //如果人员离职，合计小于0为0
                                           decimal? hj = Convert.ToDecimal(dr["hj"].ToString());
                                           if (dr["mem_isFired"].ToString() == "1")
                                           {
                                               if (hj < 0)
                                               {
                                                   hj = 0;
                                               }
                                           }
                                           hjcol13 += hj;
                                        %>

                                        <% if (!IsSubmit)
                                           { %>
                                        <span><%= Convert.ToDecimal(hj).ToString("f0")%></span>
                                        <%}
                                           else
                                           { %>
                                        <%=  Convert.ToDecimal(dr["hj"].ToString()).ToString("f0")%>
                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                            }
                                        }  //无改动结束
                                        else  //有改动
                                        {
                                            AllHisList.ForEach(c =>
                                            { %>

                                <tr>
                                    <td><span><%= c.UnitName%></span></td>
                                    <td><span><%= c.UserName%></span></td>
                                    <td><span><%= c.IsFired%></span></td>
                                    <td style="text-align: right;">
                                        <%
                                                hjcol += Convert.ToDecimal(c.BMJLTZ);
                                        %>

                                        <%=  Convert.ToDecimal(c.BMJLTZ).ToString("f0")%>
                                       
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                hjcol2 += Convert.ToDecimal(c.GSTZ);
                                        %>

                                        <%=  Convert.ToDecimal(c.GSTZ).ToString("f0")%>
                                        
                                    </td>
                                   

                                    <td style="text-align: right;">
                                        <%
                                                hjcol4 += Convert.ToDecimal(c.XMJJJS);
                                        %>

                                        <%=  Convert.ToDecimal(c.XMJJJS).ToString("f0")%>
                                        
                                    </td>


                                    <td style="text-align: right;">
                                        <%
                                                hjcol5 += Convert.ToDecimal(c.BMJJJS);
                                        %>

                                        <%=  Convert.ToDecimal(c.BMJJJS).ToString("f0")%>
                                        
                                    </td>

                                    <td style="text-align: right;">
                                        <%
                                                hjcol6 += Convert.ToDecimal(c.XMJJ);                                            
                                        %>

                                        <%= Convert.ToDecimal(c.XMJJ).ToString("f0")%>
                                    </td>

                                    <td style="text-align: right;">
                                        <%
                                                hjcol7 += Convert.ToDecimal(c.BMNJJ);
                                        %>

                                        <%= Convert.ToDecimal(c.BMNJJ).ToString("f0")%>
                                        
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                hjcol8 += Convert.ToDecimal(c.XMGLJ);
                                        %>

                                        <%= Convert.ToDecimal(c.XMGLJ).ToString("f0")%>

                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                hjcol9 += Convert.ToDecimal(c.YXYG);
                                        %>


                                        <%= Convert.ToDecimal(c.YXYG).ToString("f0")%>
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                hjcol10 += Convert.ToDecimal(c.TCGX);
                                        %>


                                        <%= Convert.ToDecimal(c.TCGX).ToString("f0")%>
                                        
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                                hjcol11 += Convert.ToDecimal(c.BIM);
                                        %>

                                        <%= Convert.ToDecimal(c.BIM).ToString("f0")%>
                                    </td>
                                    <%--222--%>
                                    <td style="text-align: right;">
                                   <%
                                               hjcol20 += Convert.ToDecimal(c.ZCFY);
                                        %>

                                    <%= Convert.ToDecimal(c.ZCFY).ToString("f0")%> 
                                    </td>
                                     

                                    <td style="text-align: right;">
                                        <%
                                                hjcol12 += Convert.ToDecimal(c.GHBZ);
                                        %>
                                        <%= Convert.ToDecimal(c.GHBZ).ToString("f0")%>
                                    </td>
                                     <td style="text-align: right;">
                                        <%
                                                hjcol14 += Convert.ToDecimal(c.YFJJ) + Convert.ToDecimal(c.HJ);
                                        %>

                                        <%=  Convert.ToDecimal(c.YFJJ+c.HJ).ToString("f0")%>
                                        
                                    </td>
                                     <td style="text-align: right;">
                                        <%
                                                hjcol3 += Convert.ToDecimal(c.YFJJ);
                                        %>

                                        <%=  Convert.ToDecimal(c.YFJJ).ToString("f0")%>
                                        
                                    </td>
                                    <td style="text-align: right;">
                                        <%
                                          
                                                hjcol13 += Convert.ToDecimal(c.HJ);
                                        %>


                                        <%=  Convert.ToDecimal(c.HJ).ToString("f0")%>
                                    </td>
                                    <td></td>
                                </tr>
                                <%});
                                        }
                                    }%>
                            </tbody>
                            <tfoot>
                                <tr class="warning">
                                    <td>合计:</td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol2).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol4).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol5).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol6).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol7).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol8).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol9).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol10).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol11).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol20).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol12).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol14).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol3).ToString("f0") %></td>
                                    <td style="text-align: right;"><%= Convert.ToDecimal(hjcol13).ToString("f0") %></td>
                                    
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%= IsSubmit %>" id="hidIsSubmit" />
    <input type="hidden" name="name" value="<%= RenwuNo %>" id="hidRenwuno" />
    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo">
    <input type="hidden" name="name" value="<%= IsSave %>" id="hidIsSave">
    <input type="hidden" name="name" value="<%= IsCheck %>" id="hidIsCheck" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
  
    <!--所有施工部门ID-->
    <input type="hidden" name="name" value="<%= UnitArchString %>" id="hidArchUnitStr" />

    <!-- begin 弹出添加考核人员 -->
    <div id="addKaoHeMem" class="modal fade yellow" tabindex="-1" data-width="450px" aria-hidden="true" style="display: none; width: 450px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加未参加考核人员
            </h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-full-width" id="tbNameList">
                    <tbody>
                        <tr>
                            <td style="width: 100px;">姓名</td>
                            <td>
                                <select id="drpMems">
                                    <% UnKaoheMems.ForEach(m =>
                                       { %>
                                    <option value="<%= m.memID %>" uid="<%= m.memUnitID %>" uname="<%= m.memUnitName %>"><%= m.memName %></option>
                                    <%   }); %>
                                </select>
                            </td>
                            <td style="width: 100px;">部门经理调整</td>
                            <td>
                                <input id="bmjl" type="text" class="" value="0" style="width: 80px;" /></td>
                        </tr>
                        <tr>
                            <td>公司调整</td>
                            <td>
                                <input id="gstz" type="text" class="" value="0" style="width: 80px;" /></td>
                            <td>项目奖金计算</td>
                            <td><input id="xmjjjs" type="text" class="" value="0" style="width: 80px;" />
                                </td>
                        </tr>
                        <tr>
                            <td>部门内奖金计算</td>
                            <td>
                                <input id="bmnjjjs" type="text" class="" value="0" style="width: 80px;" /></td>
                            <td>项目奖金</td>
                            <td><input id="xmjj" type="text" class="" value="0" style="width: 80px;" />
                                </td>
                        </tr>
                        <tr>
                            <td>部门内奖金</td>
                            <td><input id="bmnjj" type="text" class="" value="0" style="width: 80px;" />
                                </td>
                            <td>项目管理奖</td>
                            <td><input id="xmglj" type="text" class="" value="0" style="width: 80px;" />
                                </td>
                        </tr>
                        <tr>
                            <td>优秀员工奖</td>
                            <td>
                                <input id="yxyg" type="text" class="" value="0" style="width: 80px;" /></td>
                            <td>突出贡献奖</td>
                            <td><input id="tcgx" type="text" class="" value="0" style="width: 80px;" />
                                </td>
                        </tr>
                        <tr>
                            <td>BIM及标准化</td>
                            <td> <input id="bimjj" type="text" class="" value="0" style="width: 80px;" />
                                </td>
                             <td>注册费用</td>
                            <td><input id="zcfy" type="text" class="" value="0" style="width: 80px;" />
                            </td>
                            
                        </tr>
                        <tr>
                            <td>工会补助</td>
                            <td>
                               <input id="ghbt" type="text" class="" value="0" style="width: 80px;" /></td>

                            <td>合计(含预发)</td>
                            <td><input id="hjyf" type="text" class="" value="0" style="width: 80px;" />
                                </td>                     
                        </tr>
                        <tr>
                             <td>预发奖金</td>
                            <td><input id="yfjj" type="text" class="" value="0" style="width: 80px;" />
                                </td>
                            <td>合计</td>
                            <td><input id="hj" type="text" class="" value="0" style="width: 80px;" /></td>
                           
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" align="center">
                                <a href="#" class="btn btn-sm green" id="btnAddMems">保存</a>
                                <a href="#" class="btn btn-sm default" id="btnCancel" data-dismiss="modal">取消</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <script>
        allotRecord.init();
    </script>
</asp:Content>
