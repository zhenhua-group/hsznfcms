﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UserJudge.aspx.cs" Inherits="TG.Web.DeptBpm.UserJudge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/DeptBpm/UserJudge.js?v=20170117"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>考核评分</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>考核评分</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>部门名称：<%= KaoHeUnit.unit_Name %>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-hover" id="tbData">
                        <thead>
                            <tr>
                                <th colspan="16" align="center"><span><%= KaoHeUnit.unit_Name %>&nbsp;
                                    <%if (RenwuModel.RenWuNo.ToString().ToCharArray()[RenwuModel.RenWuNo.ToString().Length - 1] == '1')
                                      { %>
                                    <b>(<%= RenwuModel.RenWuNo.ToString()%>)</b>
                                    <%}
                                      else
                                      {%>
                                    <b>(<%= RenwuModel.RenWuNo.ToString()%>)</b>
                                    <%} %>
                                    &nbsp;互评与自评打分表</span>
                                </th>

                            </tr>
                            <tr class="success">
                                <th style="width: 75px;">被评价人</th>
                                <th style="width: 75px;">劳动强度</th>
                                <th style="width: 75px;">工作效率</th>
                                <th style="width: 85px;">工作主动性</th>
                                <th style="width: 75px;">工作质量</th>
                                <th style="width: 75px;">工作难度</th>
                                <th style="width: 75px;">劳动纪律</th>
                                <th style="width: 100px;">内部沟通能力</th>
                                <th style="width: 110px;">合作与互助精神</th>
                                <th style="width: 100px;">外部沟通能力</th>
                                <th style="width: 110px;">组织与配合能力</th>
                                <th style="width: 75px;">学习意愿</th>
                                <th style="width: 110px;">公共事务积极性</th>
                                <th style="width: 80px;">得分</th>
                                <th></th>
                            </tr>
                            <tr class="success" style="display:none;">
                                <th style="width: 75px;">权重</th>
                                <th style="width: 75px;">10</th>
                                <th style="width: 75px;">12</th>
                                <th style="width: 85px;">10</th>
                                <th style="width: 75px;">12</th>
                                <th style="width: 75px;">8</th>
                                <th style="width: 75px;">6</th>
                                <th style="width: 100px;">5</th>
                                <th style="width: 110px;">5</th>
                                <th style="width: 100px;">4</th>
                                <th style="width: 110px;">4</th>
                                <th style="width: 75px;">2</th>
                                <th style="width: 110px;">1</th>
                                <th style="width: 80px;"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% 
                                decimal? avg1 = 0;
                                decimal? avg2 = 0;
                                decimal? avg3 = 0;
                                decimal? avg4 = 0;
                                decimal? avg5 = 0;
                                decimal? avg6 = 0;
                                decimal? avg7 = 0;
                                decimal? avg8 = 0;
                                decimal? avg9 = 0;
                                decimal? avg10 = 0;
                                decimal? avg11 = 0;
                                decimal? avg12 = 0;
                                decimal? JudgeAllCount = 0;
                                int userCount = UserJudges.Count;
                                if (IsSubmit)
                                {
                                    UserJudges.ForEach(c =>
                                    {
                                        avg1 += c.Judge1 ?? 0;
                                        avg2 += c.Judge2 ?? 0;
                                        avg3 += c.Judge3 ?? 0;
                                        avg4 += c.Judge4 ?? 0;
                                        avg5 += c.Judge5 ?? 0;
                                        avg6 += c.Judge6 ?? 0;
                                        avg7 += c.Judge7 ?? 0;
                                        avg8 += c.Judge8 ?? 0;
                                        avg9 += c.Judge9 ?? 0;
                                        avg10 += c.Judge10 ?? 0;
                                        avg11 += c.Judge11 ?? 0;
                                        avg12 += c.Judge12 ?? 0;
                                        JudgeAllCount += c.JudgeCount;
                            %>
                            <tr>
                                <td><span memid="<%=c.MemID%>" memname="<%=c.MemName%>" class="badge badge-info"><%= c.MemName%></span></td>
                                <td>
                                    <span><%= c.Judge1%></span></td>
                                <td>
                                    <span><%= c.Judge2%></span></td>
                                <td>
                                    <span><%= c.Judge3%></span></td>
                                <td>
                                    <span><%= c.Judge4%></span></td>
                                <td>
                                    <span><%= c.Judge5%></span></td>
                                <td>
                                    <span><%= c.Judge6%></span></td>
                                <td>
                                    <span><%= c.Judge7%></span></td>
                                <td>
                                    <span><%= c.Judge8%></span></td>
                                <td>
                                    <span><%= c.Judge9%></span></td>
                                <td>
                                    <span><%= c.Judge10%></span></td>
                                <td>
                                    <span><%= c.Judge11%></span></td>
                                <td>
                                    <span><%= c.Judge12%></span></td>
                                <td>
                                    <span class="badge badge-primary badge-roundless"><%= c.JudgeCount%></span></td>
                                <td></td>
                            </tr>
                            <% });
                                }
                                else
                                {
                                    if (IsExsitJudge)
                                    {


                                        UserJudges.ForEach(c =>
                                        {

                                            avg1 += c.Judge1 ?? 0;
                                            avg2 += c.Judge2 ?? 0;
                                            avg3 += c.Judge3 ?? 0;
                                            avg4 += c.Judge4 ?? 0;
                                            avg5 += c.Judge5 ?? 0;
                                            avg6 += c.Judge6 ?? 0;
                                            avg7 += c.Judge7 ?? 0;
                                            avg8 += c.Judge8 ?? 0;
                                            avg9 += c.Judge9 ?? 0;
                                            avg10 += c.Judge10 ?? 0;
                                            avg11 += c.Judge11 ?? 0;
                                            avg12 += c.Judge12 ?? 0;
                                            JudgeAllCount += c.JudgeCount;
                            %>
                            <tr>
                                <td><span memid="<%=c.MemID%>" memname="<%=c.MemName%>" class="badge badge-info"><%= c.MemName%></span></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="10" value="<%=c.Judge1 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="12" value="<%=c.Judge2 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="10" value="<%=c.Judge3 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="12" value="<%=c.Judge4 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="8" value="<%=c.Judge5 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="6" value="<%=c.Judge6 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="5" value="<%=c.Judge7 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="5" value="<%=c.Judge8 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="4" value="<%=c.Judge9 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="4" value="<%=c.Judge10 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="2" value="<%=c.Judge11 %>" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" name="name" weights="1" value="<%=c.Judge12 %>" /></td>
                                <td>
                                    <span class="label label-info"><%=c.JudgeCount %></span></td>
                                <td></td>
                            </tr>

                            <%      });

                                    }
                                    else
                                    {
                                        KaoHeMems.ForEach(c =>
                                       {
                                           //不加载部门经理，外邀人员
                                           if (c.MagRole != 5 && c.IsOutUnitID == 0)
                                           {%>
                            <tr>
                                <td><span memid="<%=c.memID%>" memname="<%=c.memName%>" class="badge badge-info"><%= c.memName%></span></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="10" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="12" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="10" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="12" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="8" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="6" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="5" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="5" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="4" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="4" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="2" name="name" value="" /></td>
                                <td>
                                    <input type="text" style="width: 60px;" weights="1" name="name" value="" /></td>
                                <td>
                                    <span class="label label-info">0</span></td>
                                <td></td>
                            </tr>
                            <%}
                                       });
                                    }
                                }%>
                        </tbody>
                        <tfoot>
                            <% if (IsExsitJudge || IsSubmit)
                               { %>
                            <tr class="warning">
                                <td>平均值</td>
                                <td><%= Convert.ToDecimal(avg1 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg2 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg3 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg4 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg5 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg6 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg7 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg8 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg9 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg10 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg11 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(avg12 / userCount).ToString("f2") %></td>
                                <td><%= Convert.ToDecimal(JudgeAllCount/userCount).ToString("f2") %></td>
                                <td></td>
                            </tr>
                            <%} %>
                            <tr>
                                <td colspan="16" align="center">
                                    <a href="#" class="btn btn-sm green" id="btnSave">保&nbsp;&nbsp;存</a>
                                    <a href="#" class="btn btn-sm red" id="btnSave2">确认提交</a>
                                    <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default">返回</a>
                                    <span style="font-weight:600;color:#d84a38;margin-left:10px;">确认提交后无法编辑<strong>!</strong></span>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <table class="table table-bordered table-hover">
                        <tr>
                            <td>1.	本次打分是对员工与自身岗位的匹配程度进行评估，员工能力高于岗位要求的则得分高，低于岗位要求的则得分低；建议打分时结合每个员工的职务进行综合考虑；
                            </td>
                        </tr>
                        <tr>
                            <td>2.	本次打分共分12项内容，如劳动强度、工作效率……，若各位员工对某项内容的含义存有疑惑，请及时询问各自部门经理，或拨打咨询热线5607；	
                            </td>
                        </tr>
                        <tr>
                            <td>3.	本次打分为5分制：5分-非常优秀；4分-比较优秀；3分-还不错；2分-马马虎虎；1分-比较差劲；0分-绝望；
                            </td>
                        </tr>
                        <tr>
                            <td>4. 本次打分以0.5分为单位递进，比如4.5、4.0、3.5……
                            </td>
                        </tr>
                        <tr>
                            <td>5.	若对部门内某些人的某方面不甚了解，建议空着不打分；如对部门内某些人完全不了解，建议全部空着不打分；
                            </td>
                        </tr>
                        <tr>
                            <td>6.	要求各位员工本着客观公正的原则进行互评与自评；	
                            </td>
                        </tr>
                        <tr>
                            <td>7.	本次打分将以半匿名形式进行，只有该部部门经理与公司领导有权查看打分原始资料。
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--隐藏域-->
    <input type="hidden" name="name" id="hidKaoheUnitSysNo" value="<%= KaoHeUnitSysNo %>" />
    <input type="hidden" name="name" id="hidKaoHeUnitID" value="<%= KaoHeUnitID %>" />
    <input type="hidden" name="name" id="hidRenwuNo" value="<%= RenwuNo %>" />
    <input type="hidden" name="name" id="hidInserUserID" value="<%= UserSysNo %>" />
    <input type="hidden" name="name" id="hidIsSubmit" value="<%= IsSubmit %>" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <script>
        UserJudge.init();
    </script>
</asp:Content>
