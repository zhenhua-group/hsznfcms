﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="TrainingStatics.aspx.cs" Inherits="TG.Web.Training.TrainingStatics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_sign.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/FunctionChart/FusionCharts.js" type="text/javascript"></script>

    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <%--  <script type="text/javascript">
        
        $("#txtBeginDate").val();
        $("#txtEndDate").val();


    </script>--%>

    <style type="text/css">
        .mycenter, #ctl00_ContentPlaceHolder1_grid_mem {
            text-align: center;
        }

        .cs, .cs1, .cs2 {
            display: none;
        }

        td {
            vertical-align: middle !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>培训信息管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>培训信息管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>查询培训统计信息
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="tbl_id">
                        <tr>
                            <td>外部培训统计:
                            </td>
                            <td>
                                <input type="text" name="txt_date" id="txtBeginDate" onclick="WdatePicker({ readOnly: true })"
                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5;" />
                            </td>
                            <td>--
                            </td>
                            <td>
                                <input type="text" name="txt_date" id="txtEndDate" onclick="WdatePicker({ readOnly: true })"
                                    class="Wdate" runat="Server" style="width: 120px; height: 22px; border: 1px solid #e5e5e5;" />
                            </td>
                            <%--   <td>
                                 <asp:DropDownList ID="type" Width="150px" runat="Server"
                                                    CssClass="form-control">
                                                    <asp:ListItem Value="按数量">按数量</asp:ListItem>
                                                    <asp:ListItem Value="按费用">按费用</asp:ListItem>
                                                </asp:DropDownList>
                            </td>--%>
                            <td>&nbsp;
                                     <asp:Button ID="btn_search" class="btn blue" type="button" runat="server" OnClick="Button1_Click" Text="外部培训月份分布统计" />
                                <asp:Button ID="Button2" class="btn blue" type="button" runat="server" OnClick="Button2_Click" Text="外部培训部门分布统计" />
                                <asp:Button ID="Button3" class="btn blue" type="button" runat="server" OnClick="Button3_Click" Text="外部培训员工分布统计" />
                                <asp:Button ID="Button4" class="btn blue" type="button" runat="server" OnClick="Button4_Click" Text="外部培训部门覆盖率" />

                            </td>

                        </tr>
                    </table>
                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>培训统计信息
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table style="width: 100%;" class="cls_show_cst_jiben_2">

                                <tr>
                                    <td align="center">
                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>

                            <asp:HiddenField ID="hid_where" runat="server" Value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-offset-12 col-md-12">
                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                            返回</button>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <input type="hidden" id="selectedid" />

</asp:Content>
