﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="TrainingManage.aspx.cs" Inherits="TG.Web.Training.TrainManage" %>
<%@ Register Src="../UserControl/UserOfTheDepartmentTree.ascx" TagName="UserOfTheDepartmentTree"
    TagPrefix="uc1" %>
 
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_sign.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/Training/Training.js" type="text/javascript"></script>
    <script src="../js/UserControl/UserOfTheDepartmentTree.js" type="text/javascript"></script>
    
    
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
     <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    

    <script type="text/javascript">
        var swfu;

        var userid = '<%=GetCurMemID() %>';
        function ActionPic(memid, param) {

            var divid = "divFileProgressContainer" + param;
            var spanid = "spanButtonPlaceholder" + param;


            //附件高清图
            swfu = new SWFUpload({

                upload_url: "../ProcessUpload/upload_sign.aspx?type=sign&id=" + memid + "&userid=" + userid,
                flash_url: "../js/swfupload/swfupload.swf",
                post_params: {
                    "ASPSESSID": "<%=Session.SessionID %>"
                },
                file_size_limit: "10 MB",
                file_types: "*.jpg;*.gif;*.bmp;.png",
                file_types_description: "文件资料上传",
                file_upload_limit: "0",
                file_queue_limit: "1",

                //Events
                file_queued_handler: fileQueued,
                file_queue_error_handler: fileQueueError,
                file_dialog_complete_handler: fileDialogComplete,
                upload_progress_handler: uploadProgress,
                upload_error_handler: uploadError,
                upload_success_handler: uploadSuccess,
                upload_complete_handler: uploadComplete,
                button_placeholder_id: spanid,

                button_style: '{background-color:#d8d8d8}',
                button_width: 61,
                button_height: 22,
                button_text: '<span class="fileupload-new">选择文件</span>',
                button_text_style: '.fileupload-new {background-color:#d8d8d8; } ',
                button_text_top_padding: 1,
                button_text_left_padding: 5,

                custom_settings: {
                    upload_target: divid
                },
                debug: false
            });

        }


    </script>
    <style type="text/css">
        .mycenter,  #ctl00_ContentPlaceHolder1_grid_mem 
        {
           text-align: center
        } 
       .cs,.cs1,.cs2{display: none}
       td{vertical-align: middle !important}
      
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>培训信息管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>培训信息管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>查询培训信息
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                      <table class="table-responsive" id="tbl_id">
                        <tr>
                             <td>生产部门:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unit" Width="120px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全院部门---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>性质:
                            </td>
                            <td>
                                <asp:DropDownList ID="property" Width="80px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全院---</asp:ListItem>
                                    <asp:ListItem Value="内部">内部</asp:ListItem>
                                    <asp:ListItem Value="外部">外部</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>年份:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                              <td>月份:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_month" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>培训内容:
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="server" />
                            </td>
                            <td>
                                &nbsp;
                                     <asp:Button ID="btn_search" class="btn blue" type="button" runat="server" OnClick="Button1_Click" Text="查询"/>
                                <input type="button" class="btn blue " id="btn_add" value="添加培训" href="#addTraining" data-toggle="modal" />
                                <%--<asp:Button ID="Button1" class="btn blue" runat="server" OnClick="Button1_Click1" Text="培训统计" />--%>
                                <a href="../Training/TrainingStatics.aspx" target="_blank" class="btn blue">培训统计</a>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>培训信息列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_mem" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                OnRowDataBound="grid_mem_RowDataBound" HeaderStyle-HorizontalAlign="Center">
                                
                                <Columns>
                                    

                                    <asp:TemplateField  >
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter cs1" />

                                        <HeaderTemplate >
                                            <asp:CheckBox ID="chkAll" runat="server" />
                                        </HeaderTemplate>
                                        
                                         <ItemStyle Width="5%" CssClass="cs1"/>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    

                                    <asp:BoundField DataField="Id" HeaderText="id" >
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter cs"  />
                                        <ItemStyle Width="5%" CssClass="cs" />
                                    </asp:BoundField>
                                    

                                    <asp:BoundField DataField="Property" HeaderText="性质">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="xz"/>
                                    </asp:BoundField>
                                    
                                       <asp:BoundField DataField="Title" HeaderText="培训内容">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="nr"/>
                                    </asp:BoundField>
                                  
                                      <asp:BoundField DataField="BeginDate" DataFormatString="{0:d}"  HeaderText="培训开始时间">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="ks"/>
                                    </asp:BoundField>
                                    
                                       <asp:BoundField DataField="EndDate" DataFormatString="{0:d}" HeaderText="培训结束时间">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="js"/>
                                    </asp:BoundField>

                                         <asp:BoundField DataField="CanYuRen" HeaderText="参与人">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="cy"/>
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="UnitName" HeaderText="所属部门">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="ss"/>
                                    </asp:BoundField>
                                      <asp:BoundField DataField="BaoMingFee" HeaderText="报名费">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="bm"/>
                                    </asp:BoundField>
                                    
                                      <asp:BoundField DataField="JiPiaoFee" HeaderText="机票">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="jp"/>
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="ZhuShuFee" HeaderText="住宿">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="zs"/>
                                    </asp:BoundField>
                                    
                                      <asp:BoundField DataField="OtherFee" HeaderText="其他">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="qt"/>
                                    </asp:BoundField>

                                      <asp:BoundField DataField="Total" HeaderText="合计">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="hj"/>
                                    </asp:BoundField>
                                    
                                      <asp:BoundField  HeaderText="总合计">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="5%" CssClass="zhj"/>
                                    </asp:BoundField>
                                    

                                    



                                    <asp:TemplateField HeaderText="操作">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <a href="#editTraining" class="cls_select" data-toggle="modal">编辑</a>
                                             <a href="#" class="cls_select delete">删除</a>
                                        </ItemTemplate>
                                        <ItemStyle Width="8%" />
                                    </asp:TemplateField>
                                    
                                
                                    
                                      <asp:BoundField DataField="detailId" HeaderText="明细表id">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter cs2" />
                                        <ItemStyle Width="5%" CssClass="detail cs2"/>
                                    </asp:BoundField>
                                    
                                    
                                </Columns>
                            </asp:GridView>
                            
                            

                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px; height:23px;"
                                PageSize="10" SubmitButtonClass="btn green">
                            </webdiyer:AspNetPager>
                            <asp:HiddenField ID="hid_where" runat="server" Value="" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    
    
    
        <!--添加培训-->
    <div id="addTraining" class="modal
    fade yellow"
        tabindex="-1" data-width="700" aria-hidden="true" style="display: none; width: 700px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加培训
            </h4>
        </div>
        <div class="modal-body">
            <div id="addTrainingDiv" style="color: #222222;">
                <table class="table table-bordered">
                    <tr>
                        <td>性质</td>
                        <td>
                            <select id="select">
                                <option value="内部">内部</option>
                                <option value="外部">外部</option>
                            </select>

                        </td>
                        <td>培训内容</td>
                        <td colspan="3" >
                            <textarea id="title" style="width:300px;height:90px;"></textarea>
                             <span style="color: red; display: none;" id="txt_title_valide">请输入培训内容!</span>
                             </td>
                        <td colspan="2" >
                             <input style="margin-right: 20px" type="button" class="btn blue " id="addperson" value="添加人员" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal"/>
                            <span style="color: red; display: none;" id="txt_addperson_valide">请添加人员!</span>
                        </td>
                       
                           
                    </tr>
                    <tr>
                        <td>起始时间</td>
                        <td>
                            <input type="text" name="txt_date" id="BeginDate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                            <span style="color: red; display: none;" id="txt_BeginDate_valide">请输入起始时间!</span>

                        </td>

                        <td>结束时间:</td>
                        <td  colspan="5">
                            <input  type="text" name="txt_date" id="EndDate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                            <span style="color: red; display: none;" id="txt_EndDate_valide">请输入结束时间!</span>

                        </td>
                      
                 
                      
                    </tr>
                    <tr>
                        <td>参与人</td>
                        <td>所属部门</td>
                        <td>报名费</td>
                        <td>住宿费</td>
                        <td>机票</td>
                        <td>其他</td>
                        <td>合计</td>
                        <td>操作 <span style='color: red; display: none;' class='txt_addfee_valide'>费用至少添加一项!</span></td>
                    </tr>
                  
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn red btn-default" id="btn_addsure">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                取消</button>
        </div>
    </div>
    
        
    
 <%--   编辑培训--%>
        <div id="editTraining" class="modal
    fade yellow"
        tabindex="-1" data-width="900" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">编辑培训
            </h4>
        </div>
        <div class="modal-body">
            <div id="editTrainingDiv" style="color: #222222;">
                <table class="table table-bordered">
                    <tr>
                        <td>性质</td>
                        <td>
                            <select id="editselect">
                                <option value="内部">内部</option>
                                <option value="外部">外部</option>
                            </select>

                        </td>
                        <td>培训内容</td>
                        <td colspan="3" >
                            <input id="edittitle"/>
                             <span style="color: red; display: none;" id="edittxt_title_valide">请输入培训内容!</span>
                             </td>
                        <td colspan="2">
                             <input style="margin-right: 20px" type="button" class="btn blue " id="editperson" value="添加人员" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal"/>
                            <span style="color: red; display: none;" id="edittxt_addperson_valide">请添加人员!</span>
                        </td>
                       
                           
                    </tr>
                    <tr>
                        <td>起始时间</td>
                        <td>
                            <input type="text" name="txt_date" id="editBeginDate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                            <span style="color: red; display: none;" id="edittxt_BeginDate_valide">请输入起始时间!</span>

                        </td>

                        <td>结束时间:</td>
                        <td  colspan="5">
                            <input  type="text" name="txt_date" id="editEndDate" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" />
                            <span style="color: red; display: none;" id="edittxt_EndDate_valide">请输入结束时间!</span>

                        </td>
                      
                 
                      
                    </tr>
                    <tr>
                        <td>参与人</td>
                        <td>所属部门</td>
                        <td>报名费</td>
                        <td>住宿费</td>
                        <td>机票</td>
                        <td>其他</td>
                        <td>合计</td>
                        <td>操作 <span style='color: red; display: none;' class='edittxt_addfee_valide'>费用至少添加一项!</span></td>
                    </tr>
                  
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn red btn-default" id="btn_editsure">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                取消</button>
        </div>
    </div>
    
    

      <div id="UserOfTheDepartmentTreeDiv" class="modal fade yellow" tabindex="-1" data-width="460"
        aria-hidden="true" style="display: none; width: 460px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择用户</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="ChooseUserOfTherDepartmentContainer">
                        <div style="overflow: auto; height: 300px;">
                            <uc1:UserOfTheDepartmentTree ID="UserOfTheDepartmentTree1" runat="server" IsRadio="false" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn
    green btn-default"
                id="btn_OK">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <input type="hidden" id="selectedid"/>
     
</asp:Content>
