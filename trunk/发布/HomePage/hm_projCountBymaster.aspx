﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="hm_projCountBymaster.aspx.cs" Inherits="TG.Web.HomePage.hm_projCountBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/HomePage/hm_projCount_jq.js"></script>

    <style type="text/css">
        .style1 {
            width: 120px;
        }

        #cprName {
            width: 345px;
        }

        .norecords {
            border-width: 2px !important;
            display: none;
            font-weight: bold;
            left: 45%;
            margin: 5px;
            padding: 6px;
            position: absolute;
            text-align: center;
            top: 45%;
            width: auto;
            z-index: 102;
            color: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页 <small>院生产项目统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>院生产项目统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>全院生产项目总数： <span class="badge badge-red"
                            id="projCount" runat="Server">3</span>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_Report" Text="导出Excel" runat="server" CssClass="btn red btn-sm" OnClick="btn_Report_Click" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
