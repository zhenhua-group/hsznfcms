﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="hm_quainfoBymaster.aspx.cs" Inherits="TG.Web.HomePage.hm_quainfoBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="../css/m_comm.css" rel="stylesheet" type="text/css" /> --%>
    <%--<link href="../css/HomePage.css" rel="stylesheet" type="text/css" />--%>
    <%-- <link href="../css/hm_qualification.css" rel="stylesheet" type="text/css" />--%>
    <%--  <link href="../css/jquery.tablesort.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script src="../js/HomePage/hm_quainfo.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        //获得ID
        var projid = '<%=GetProjectID() %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        首页 <small>资质查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>资质查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>资质查看</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td>
                                                        资质名称:
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lbl_title" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hid_proj" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        资质内容:
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lbl_content" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:15%;">
                                                        资质开始时间:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_Zztime" runat="server"></asp:Label>
                                                    </td>
                                                    <td style="width:15%;">
                                                        资质结束时间:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_Sptime" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        资质级别:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_jb" runat="Server" />
                                                    </td>
                                                    <td>
                                                        年审时间:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl_yeartime" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        资质图:
                                                    </td>
                                                    <td id="showImg" colspan="3">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-offset-12 col-md-12">
                                                <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                                    返回</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value='1' id="hid_type" />
</asp:Content>
