﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="hm_ConstBymaster.aspx.cs" Inherits="TG.Web.HomePage.hm_ConstBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="../js/jquery.jOrgChart/prettify.js" type="text/javascript"></script>
    <script src="../js/jquery.jOrgChart/jquery.jOrgChart.js" type="text/javascript"></script>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        首 页 <small>院组织架构</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>院组织架构</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="row">
        <div id="col-md-12">
          <img src="/Images/gansu.jpg" width="1047" height="441" border="0" usemap="#Map">
            <map name="Map" id="Map"><area shape="rect" coords="935, 241, 993, 331" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("总建筑师") %>'
                    alt="总建筑师" /><area shape="rect" coords="870, 241, 929, 331" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("总工程师") %>'
                    alt="总工程师" /><area shape="rect" coords="408, 242, 480, 323" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("副院长") %>'
                    alt="副院长" />
                <area shape="rect" coords="427, 37, 571, 65" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("院领导") %>'
                    alt="院长" onfocus="this.blur();" />
                <area shape="rect" coords="276, 94, 473, 129" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("党委书记") %>'
                    alt="党委书记" />
                <area shape="rect" coords="299, 162, 333, 213" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("政工部") %>'
                    alt="政工部" />
                <area shape="rect" coords="338, 164, 372, 213" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("工会") %>'
                    alt="工会" />
                <area shape="rect" coords="378, 164, 413, 213" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("团委") %>'
                    alt="团委" />
                <area shape="rect" coords="41, 243, 73, 315" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("办公室") %>'
                    alt="院办公室" />
                <area shape="rect" coords="83, 244, 115, 313" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("人力资源部") %>'
                    alt="人力资源部" />
                <area shape="rect" coords="125, 242, 159, 312" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("财务处") %>'
                    alt="财务" />
                <area shape="rect" coords="163, 240, 208, 322" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("副院长") %>'
                    alt="副院长" />
                <area shape="rect" coords="293, 240, 351, 321" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("副院长") %>'
                    alt="副院长" />
                <area shape="rect" coords="529, 240, 600, 322" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("副院长") %>'
                    alt="副院长" />
                <area shape="rect" coords="667, 242, 710, 320" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("副院长") %>'
                    alt="副院长" />
                <area shape="rect" coords="787, 244, 829, 320" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("副院长") %>'
                    alt="副院长" />
                <area shape="rect" coords="124, 359, 155, 417" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计二所") %>'
                    alt="设计二所" />
                <area shape="rect" coords="163, 358, 197, 418" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计五所") %>'
                    alt="设计五所" />
                <area shape="rect" coords="203, 358, 237, 418" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计七所") %>'
                    alt="设计七所" />
                <area shape="rect" coords="770, 358, 805, 418" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计八所") %>'
                    alt="设计八所" />
                <area shape="rect" coords="889, 359, 923, 420" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("技术质量部") %>'
                    alt="技术质量部" />
                 <area shape="rect" coords="856, 356, 886, 422" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("青海分院") %>'
                    alt="青海分院" />
                <area shape="rect" coords="243, 359, 276, 418" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计四所") %>'
                    alt="设计四所" />
                <area shape="rect" coords="285, 358, 319, 419" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("暖通热力所") %>'
                    alt="暖通所" />
                <area shape="rect" coords="813, 359, 848, 421" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("经济所") %>'
                    alt="经济所" />
                <area shape="rect" coords="326, 359, 361, 418" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("勘察分院") %>'
                    alt="勘察分院" />
                <area shape="rect" coords="365, 358, 398, 419" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("岩土公司") %>'
                    alt="岩土公司" />
                <area shape="rect" coords="569, 358, 603, 419" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("基建办") %>'
                    alt="基建办" />
                <area shape="rect" coords="407, 359, 442, 419" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("监理公司") %>'
                    alt="监理公司" />
                <area shape="rect" coords="448, 360, 480, 420" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("图文制作中心") %>'
                    alt="图文制作中心" />
                <area shape="rect" coords="488, 360, 524, 419" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("承包公司") %>'
                    alt="总承包部" />
                <area shape="rect" coords="529, 359, 563, 419" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("装饰公司") %>'
                    alt="装饰公司" />
                <area shape="rect" coords="610, 359, 646, 420" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("代建办") %>'
                    alt="代建办" />
                <area shape="rect" coords="649, 357, 684, 420" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计六所") %>'
                    alt="" />
                <area shape="rect" coords="691, 357, 726, 420" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("上海分院") %>'
                    alt="上海分院" />
                <area shape="rect" coords="732, 358, 767, 421" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计一所") %>'
                    alt="设计一所" />
                <area shape="rect" coords="934, 357, 980, 422" href='hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("设计三所") %>'
                    alt="设计三所" />
              <area shape="rect" coords="420, 163, 451, 213" href="hm_ConstMemsBymaster.aspx?unitname=<%=Server.UrlEncode("物业") %>" alt="物业" />
            </map>
        </div>
    </div>
</asp:Content>
