﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="hm_showMemsBymaster.aspx.cs" Inherits="TG.Web.HomePage.hm_showMemsBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/HomePage/hm_showCount_jq.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页 <small>院项目统计</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">院项目统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>人员列表
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <table class="table table-bordered">
                                <tr>
                                    <td>部门:   </td>
                                    <td>
                                        <asp:Label ID="lbl_unitName" runat="server" CssClass="badge badge-red"></asp:Label>
                                    </td>
                                    <td>所长: </td>
                                    <td>
                                        <asp:Label ID="lbl_sz" runat="server" Text="0" CssClass="badge badge-red"></asp:Label>
                                    </td>
                                    <td>副所长:</td>
                                    <td>
                                        <asp:Label ID="lbl_fsz" runat="server" Text="0" CssClass="badge badge-red "></asp:Label></td>
                                    <td>主任工程师:</td>
                                    <td>
                                        <asp:Label ID="lbl_gcs" runat="server" Text="0" CssClass="badge badge-red "></asp:Label></td>
                                    <td>员工总数:<asp:Label ID="lbl_AllCount" runat="server"></asp:Label></td>
                                    <td>男：<asp:Label
                                        ID="lbl_boy" runat="server" Text=""></asp:Label>&nbsp;&nbsp;&nbsp;女：<asp:Label ID="lbl_gril"
                                            runat="server" Text=""></asp:Label></td>
                                </tr>
                            </table>

                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="unitId" runat="server" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
