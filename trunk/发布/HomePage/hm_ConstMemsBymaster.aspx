﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="hm_ConstMemsBymaster.aspx.cs" Inherits="TG.Web.HomePage.hm_ConstMemsBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <%--    <link href="../css/HomePage.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>

    <style type="text/css">
        body
        {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        .ui-pg-div
        {
            display: none;
        }
        .norecords
        {
            border-width: 2px !important;
            display: none;
            font-weight: bold;
            left: 45%;
            margin: 5px;
            padding: 6px;
            position: absolute;
            text-align: center;
            top: 45%;
            width: auto;
            z-index: 102;
            color: red;
        }
    </style>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/HomePage/hm_ConstMems_jq.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        首 页 <small>院组织架构</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a><i
        class="fa fa-angle-right"> </i><a>院组织架构</a><i
        class="fa fa-angle-right"> </i><a>院组织架构</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>人员查看</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover" style="line-height: 20px;">
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;<b>部门：<font style="color: red;"><asp:Label ID="lbl_unit" runat="server"
                                                        Text=""></asp:Label></font></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;<b>所长：<font style="color: red;"><asp:Label ID="lbl_sz" runat="server"
                                                        Text=""></asp:Label></font></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;<b>副所长：<font style="color: red;"><asp:Label ID="lbl_fsz" runat="server"
                                                        Text=""></asp:Label></font></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;<b>主任工程师：<font style="color: red;"><asp:Label ID="lbl_gcs" runat="server"
                                                        Text=""></asp:Label></font></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;&nbsp;<b>员工总数：<font style="color: red;"><asp:Label ID="lbl_all" runat="server"
                                                        Text=""></asp:Label></font>&nbsp;&nbsp;&nbsp;男：<font style="color: red;"><asp:Label
                                                            ID="lbl_boy" runat="server" Text=""></asp:Label></font>&nbsp;&nbsp;&nbsp;女：<font
                                                                style="color: red;"><asp:Label ID="lbl_gril" runat="server" Text=""></asp:Label></font></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i>人员查看</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body form" style="display: block;">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="jqGrid">
                                        </table>
                                        <div id="gridpager">
                                        </div>
                                        <div id="nodata" class="norecords">
                                        </div>
                                        <asp:HiddenField ID="hid_where" runat="server" Value="" />
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div style="margin-top:10px;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-offset-12 col-md-12">
                                                <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                                    返回</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            </div>
        </div>
    </div>
</asp:Content>
