﻿/*----------------------
created by Sago 2013-03-26
整体重构一部分常用功能插件。不断完善中
*/


/*------------得到页面(Body)实际宽高，包括可见部分和滚动条区域，$.getRealHW------------
*/
(function ($) {
    $.getRealHW = function () {
        var _bH = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
        var _bW = Math.max(document.body.scrollWidth, document.documentElement.scrollWidth);
        return { "height": _bH, "width": _bW };
    }
})(jQuery);

/*------------弹出遮罩层，overlay------------
require list:
    common.css
*/
(function ($) {
    $.overlay = function () {
        return new create();
    }

    function create() {
        var realHW = $.getRealHW();
        var _$overlayDiv = $("<div class=\"overlay-bg\"></div>");
        _$overlayDiv.css(
        {
            "height": realHW.height<1200?1200:realHW.height + "px",
            "width": realHW.width + "px",
            "display": "none"
        });

        $("body").append(_$overlayDiv);
        this.show = function () {
            _$overlayDiv.show();
            return this;
        }
        this.hide = function () {
            _$overlayDiv.hide();
            return this;
        }
        this.remove = function () {
            _$overlayDiv.remove();
        }
        this.setZIndex = function (v) {
            if (v != undefined) {
                _$overlayDiv.css("zIndex", v);
            }
            return this;
        }
    }
})(jQuery);

/*------------弹出消息框，messageDialog------------
require list:
    common.css
*/
(function ($) {
    $.fn.messageDialog = function (options) {
        var _opts = $.extend({}, $.fn.messageDialog.defaults, options);
        var _instance = $(this);
        return new create(_instance, _opts);
    }

    function create(ele, options) {
        var _instance = this;
        var _bRealHW = $.getRealHW(), _eH = ele.height(), _eW = ele.width();
        //计算绝对位置
        var left = (_bRealHW.width - ele.width()) / 2, top = (_bRealHW.height - ele.height()) / 2;
        //创建背景层
        var _$bgDiv = $("<div class=\"opacity-background\"></div>"), _$contentDiv = $("<div class=\"content\"></div>"), _$actionDiv = $("<div class=\"action\"></div>");
        _$contentDiv.css({ "width": (_eW) + "px", "height": (_eH + 35) + "px" }).append(ele[0].outerHTML);
        _$actionDiv.css({ "width": (_eW) + "px", "height": options.actionDivHeight });
        //绑定事件
        if (options.button) {
            for (p in options.button) {
                var _$button = createButton(p).bind("click", options.button[p]);
                _$actionDiv.append(_$button);
            }
        }
        _$bgDiv.css({ "width": (_eW + 2) + "px", "height": (_eH + 38 + 25) + "px", "zIndex": 99999, "left": left, "top": top, "display": "none" }).append(_$contentDiv).append(_$actionDiv);
        $("body").append(_$bgDiv);
        var _$overlayDiv = $.overlay().setZIndex(_$bgDiv.css("zIndex") - 1);
        //创建方法
        this.show = function (msg) {
            if (msg) {
                _$contentDiv.html(msg);
            }
            _$bgDiv.show();
            _$overlayDiv.show();
            return this;
        }

        this.hide = function () {
            _$bgDiv.hide();
            _$overlayDiv.hide();
            return this;
        }

        this.remove = function () {
            _$bgDiv.remove();
            _$overlayDiv.remove();
        }

        this.setTopAndLeft = function (top, left) {
            var _top = _$bgDiv.css("top"), _left = _$bgDiv.css("left");
            if (top != undefined) _top = top;
            if (left != undefined) _left = left;
            _$bgDiv.css({ "left": _left + "px", "top": _top + "px" });
            return this;
        }

        this.setContent = function (content) {
            _$contentDiv.html(content);
            return this;
        }
        this.getZIndex = function () {
            return _$bgDiv.css("zIndex");
        }
        this.setZIndex = function (v) {
            if (v != undefined) {
                _$bgDiv.css("zIndex", v);
            }
            return this;
        }
    }

    function createButton(v) {
        var _$button = $("<input type=\"button\" class=\"button-out\" value=\"" + v + "\">");
        _$button.bind("mouseover", function () {
            _$button.removeClass("button-out").addClass("button-over");
        }).bind("mouseout", function () {
            _$button.removeClass("button-over").addClass("button-out");
        });
        return _$button;
    }
    $.fn.messageDialog.defaults = {
        "contentDivHeight": "100%",
        "actionDivHeight": "25px"
    };
})(jQuery);