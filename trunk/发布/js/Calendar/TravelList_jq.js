﻿
$(function () {
    //未办
    LeaveList();
    //已办
    LeaveListDone();
});
function LeaveList() {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Calendar/CalendarListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        mytype: 'POST',
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '', '', '','','', '', '申请时间', '申请人', '所属部门', '项目名称','出差地点', '出差开始时间', '出差结束时间', '加班统计(小时)', '公里数', '部门经理审批', '总经理审批', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'ID', index: 'ID', hidden: true, editable: true },
                             { name: 'AuditSysNo', index: 'AuditSysNo', hidden: true, editable: true },
                             { name: 'AuditStatus', index: 'AuditStatus', hidden: true, editable: true },
                             { name: 'unitusers', index: 'unitusers', hidden: true, editable: true },
                             { name: 'managerusers', index: 'managerusers', hidden: true, editable: true },
                             { name: 'AuditGeneralmanager', index: 'AuditGeneralmanager', hidden: true, editable: true },
                            { name: 'adduser', index: 'adduser', hidden: true, editable: true },
                             { name: 'adddate', index: 'addtime', width: 80, align: 'center' },
                             { name: 'username', index: 'adduser', width: 60 },
                             { name: 'unitname', index: 'adduser', width: 100, align: 'center' },
                             { name: 'reason', index: 'reason', width: 80, align: 'center' },
                             { name: 'address', index: 'address', width: 80, align: 'center' },
                             { name: 'starttime', index: 'starttime', width: 100, align: 'center' },
                             { name: 'endtime', index: 'endtime', width: 100, align: 'center' },
                             { name: 'totaltime', index: 'totaltime', width: 100, align: 'center' },
                             { name: 'kilometre', index: 'kilometre', width: 80, align: 'center' },
                             { name: 'unitmanager', index: 'unitmanager', width: 100, align: 'center', sorttable: false, editable: false, formatter: colNameShowFormatter },
                             { name: 'generalmanager', index: 'generalmanager', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'ID', index: 'ID', width: 120, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        rowTotals: true,
        colTotals: true,
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "applytype": "travel" },
        loadonce: false,
        sortname: 'addtime',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Calendar/CalendarListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {

                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'ID') + ",";
                            }
                        }

                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
        var user = $("#ctl00_ContentPlaceHolder1_drp_user").val();
        var drpaudit = $("#drp_audit:checked").val();
        drpaudit = (drpaudit == undefined ? "" : drpaudit);
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Calendar/CalendarListHandler.ashx",
            postData: { "action": "sel", "strwhere": strwhere, "unit": unit, "year": year, "month": month, "user": user, "drpaudit": drpaudit },
            page: 1,
            sortname: 'addtime',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //部门经理
    function colNameShowFormatter(celvalue, options, rowData) {
        var backstr = "";
        var SpecialPersonAudit = $("#ctl00_ContentPlaceHolder1_SpecialPersonAudit").val();
        var SpecialPersonAuditID = $("#ctl00_ContentPlaceHolder1_SpecialPersonAuditID").val();
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();
        var applyusername = $.trim(rowData["username"]);
        var str = '<button type="button" class="btn default btn-xs" action="ApplyAgree" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" disabled="disabled">N</button>';
        //特殊人员
        var strSpecialPerson = SpecialPerson;
        strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";
        if (strSpecialPerson.indexOf(applyusername + ",") > -1) {

            var SpecialPersonList = SpecialPerson.split("|");
            var ind = 0;
            for (var i = 0; i < SpecialPersonList.length; i++) {
                if (SpecialPersonList[i].indexOf(applyusername) > -1) {
                    ind = i;
                    break;
                }
            }
            var SpecialPersonAuditList = SpecialPersonAudit.split(",");
            var SpecialPersonAuditIDList = SpecialPersonAuditID.split(",");
            //特殊人员的审批人
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == SpecialPersonAuditIDList[ind]) {
                str = '<button type="button" class="btn dark btn-xs" action="ApplyAgree" >Y</button>&nbsp;<button type="button" class="btn dark btn-xs" action="ApplyDisagree">N</button>';
            }
            switch (rowData["AuditStatus"]) {
                case "A":
                    backstr = SpecialPersonAuditList[ind] + "&nbsp;" + str;
                    break;
                case "B":
                case "D":
                case "E":
                case "N":
                    backstr = "<span class=\"badge badge-success\">√</span>" + SpecialPersonAuditList[ind];
                    break;
                case "C":
                    backstr = "<span class=\"badge badge-red\">×</span>" + SpecialPersonAuditList[ind];
                    break;

            }

        }
        else {

            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowData["unitusers"]) {
                str = '<button type="button" class="btn dark btn-xs" action="ApplyAgree" >Y</button>&nbsp;<button type="button" class="btn dark btn-xs" action="ApplyDisagree">N</button>';
            }
            switch (rowData["AuditStatus"]) {
                case "A":
                    backstr = celvalue + "&nbsp;" + str;
                    break;
                case "B":
                case "D":
                case "E":
                case "N":
                    backstr = "<span class=\"badge badge-success\">√</span>" + celvalue;
                    break;
                case "C":
                    backstr = "<span class=\"badge badge-red\">×</span>" + celvalue;
                    break;

            }
        }
       
        return backstr;

    }

    //总经理
    function colShowFormatter(celvalue, options, rowData) {
        var backstr = "";
        var hours = rowData["totaltime"];
        var days = parseFloat(hours) / 7.5;
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();
        var applyusername = $.trim(rowData["username"]);
        var strSpecialPerson = SpecialPerson;
        strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";
        //大于5天的，不包含在特殊人员里
        if (days >= 5 && strSpecialPerson.indexOf(applyusername + ",") < 0) {
            if ($.trim(celvalue) != "") {
                switch (rowData["AuditStatus"]) {
                    case "B":
                        if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowData["managerusers"]) {
                            backstr = celvalue + '&nbsp;<button type="button" managerid="' + celvalue + '" class="btn dark btn-xs" action="ApplyAgree" isdisbtn="yes" >Y</button>&nbsp;<button type="button" class="btn dark btn-xs" action="ApplyDisagree" isdisbtn="yes" >N</button>';
                        }
                        else {
                            backstr = celvalue + '&nbsp;<button type="button" class="btn default btn-xs" action="ApplyAgree" isdisbtn="yes" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" isdisbtn="yes" disabled="disabled">N</button>';
                        }
                        break;
                    case "D":
                        backstr = "<span class=\"badge badge-success\">√</span>" + celvalue;
                        break;
                    case "E":
                        backstr = "<span class=\"badge badge-red\">×</span>" + celvalue;
                        break;
                    case "N":
                        //退回只有状态为B或D才能操作，状态为B时点击退回，审批表Generalmanager字段为0,若不为0，即状态为D
                        if (rowData["AuditGeneralmanager"] != "0") {
                            backstr = "<span class=\"badge badge-success\">√</span>" + celvalue;
                        }
                        else {
                            backstr = celvalue + '&nbsp;<button type="button" class="btn default btn-xs" action="ApplyAgree" isdisbtn="yes" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" isdisbtn="yes" disabled="disabled">N</button>';
                        }
                        break;
                    default:
                        backstr = celvalue + '<button type="button" class="btn default btn-xs" action="ApplyAgree" isdisbtn="yes" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" isdisbtn="yes" disabled="disabled">N</button>';
                        break;
                }

            }
        }
        else {
            backstr = "";
        }
        return backstr;
    }
    //编辑
    function colEditFormatter(celvalue, options, rowdata) {

        if (celvalue != "") {
            var backstr = '--';
            //只能自己编辑和发起审批
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowdata["adduser"]) {
                switch (rowdata["AuditStatus"]) {

                    case "A":
                        backstr = '<a href="Apply.aspx?applyID=' + celvalue + '" alt="编辑" >编辑</a>';
                        break;
                    case null:
                    case "":
                    case "C":
                    case "E":
                        backstr = '<a href="Apply.aspx?applyID=' + celvalue + '" alt="编辑" >编辑</a>&nbsp;&nbsp;<a href="javascript:void(0);" rel="' + celvalue + '"  >重新发起</a>';
                        break;
                    case "N":
                        backstr = "<span class=\"badge badge-red\">×</span>已退回,需重新填";
                        break;

                }
            }
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowdata["unitusers"] || $("#ctl00_ContentPlaceHolder1_isapplymanager").val() == "True") {
                switch (rowdata["AuditStatus"]) {
                    case "B":
                        backstr = '<button type="button" class="btn red btn-xs" id="btn_return" AuditSysNo="' + rowdata["AuditSysNo"] + '">退回</button>';
                        break;
                    case "N":
                        backstr = "<span class=\"badge badge-red\">×</span>已退回";
                        break;

                }
            }
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowdata["managerusers"] || $("#ctl00_ContentPlaceHolder1_isapplymanager").val() == "True") {
                switch (rowdata["AuditStatus"]) {
                    case "D":
                        backstr = '<button type="button" class="btn red btn-xs"  id="btn_return" AuditSysNo="' + rowdata["AuditSysNo"] + '">退回</button>';
                        break;
                    case "N":
                        backstr = "<span class=\"badge badge-red\">×</span>已退回";
                        break;
                }
            }

            return backstr;
        }
    }
    //统计 
    function completeMethod() {
        //特殊人的审批人
        var SpecialPersonAuditID = $("#ctl00_ContentPlaceHolder1_SpecialPersonAuditID").val();
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();

        var unit_flag = false;
        var manager_flag = false;
        var rowIds = $("#jqGrid").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#" + rowIds[i]).find("td").eq(1).text((i + 1));

            var status = $("#" + rowIds[i]).find("td[aria-describedby='jqGrid_AuditStatus']").text();
            var unitsysno = $("#" + rowIds[i]).find("td[aria-describedby='jqGrid_unitusers']").text();
            var managersysno = $("#" + rowIds[i]).find("td[aria-describedby='jqGrid_managerusers']").text();
            var generalmanager = $("#" + rowIds[i]).find("td[aria-describedby='jqGrid_generalmanager']").text();
            var applyusername = $.trim($("#" + rowIds[i]).find("td[aria-describedby='jqGrid_username']").text());
            var strSpecialPerson = SpecialPerson;
            strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";
            //如果是特殊人
            if (strSpecialPerson.indexOf(applyusername + ",") > -1) {
                var SpecialPersonList = SpecialPerson.split("|");
                var ind = 0;
                for (var k = 0; k < SpecialPersonList.length; k++) {
                    if (SpecialPersonList[k].indexOf(applyusername) > -1) {
                        ind = k;
                        break;
                    }
                }
                var SpecialPersonAuditIDList = SpecialPersonAuditID.split(",");
                //审批人的重新赋值特殊人审批
                unitsysno = SpecialPersonAuditIDList[ind];
            }
            if ((status == "A" && unitsysno == usersysno)) {
                unit_flag = true;
            }
            if (status == "B" && managersysno == usersysno && generalmanager != "") {
                manager_flag = true;
            }
        }
        if (!unit_flag) {
            $("#btn_unitaudit").attr("disabled", true);
        }
        else {
            $("#btn_unitaudit").attr("disabled", false);
        }
        if (!manager_flag) {
            $("#btn_manageraudit").attr("disabled", true);
        }
        else {
            $("#btn_manageraudit").attr("disabled", false);
        }


    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata").text() == '') {
                $("#jqGrid").append("<div id='nodata'>没有查询到数据!</div>");
            }
            else { $("#nodata").show(); }
        }
        else {
            $("#nodata").hide();
        }
    }
    var clearGridPostData = function () {
        var postData = $("#jqGrid").jqGrid("getGridParam", "postData");
        $.each(postData, function (k, v) {
            delete postData[k];
        });
    }
}
function LeaveListDone() {
    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/Calendar/CalendarListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        mytype: 'POST',
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '', '', '', '','','', '申请时间', '申请人', '所属部门', '项目名称','出差地点', '出差开始时间', '出差结束时间', '加班统计(小时)', '公里数', '部门经理审批', '总经理审批', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'ID', index: 'ID', hidden: true, editable: true },
                             { name: 'AuditSysNo', index: 'AuditSysNo', hidden: true, editable: true },
                             { name: 'AuditStatus', index: 'AuditStatus', hidden: true, editable: true },
                             { name: 'unitusers', index: 'unitusers', hidden: true, editable: true },
                             { name: 'managerusers', index: 'managerusers', hidden: true, editable: true },
                             { name: 'AuditGeneralmanager', index: 'AuditGeneralmanager', hidden: true, editable: true },
                             { name: 'adduser', index: 'adduser', hidden: true, editable: true },
                             { name: 'adddate', index: 'addtime', width: 80, align: 'center' },
                             { name: 'username', index: 'adduser', width: 60 },
                             { name: 'unitname', index: 'adduser', width: 100, align: 'center' },
                             { name: 'reason', index: 'reason', width: 80, align: 'center' },
                             { name: 'address', index: 'address', width: 80, align: 'center' },
                             { name: 'starttime', index: 'starttime', width: 100, align: 'center' },
                             { name: 'endtime', index: 'endtime', width: 100, align: 'center' },
                             { name: 'totaltime', index: 'totaltime', width: 100, align: 'center' },
                             { name: 'kilometre', index: 'kilometre', width: 80, align: 'center' },
                             { name: 'unitmanager', index: 'unitmanager', width: 100, align: 'center', sorttable: false, editable: false, formatter: colNameShowFormatter },
                             { name: 'generalmanager', index: 'generalmanager', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'ID', index: 'ID', width: 120, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        rowTotals: true,
        colTotals: true,
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "applytype": "travel", "action": "selDone" },
        loadonce: false,
        sortname: 'addtime',
        sortorder: 'desc',
        pager: "#gridpager2",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Calendar/CalendarListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid2").jqGrid("navGrid", "#gridpager2", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid2").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {

                        var sel_id = $('#jqGrid2').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid2').jqGrid('getCell', sel_id[i], 'ID') + ",";
                            }
                        }

                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
        var user = $("#ctl00_ContentPlaceHolder1_drp_user").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/Calendar/CalendarListHandler.ashx",
            postData: { "strwhere": strwhere, "unit": unit, "year": year, "month": month, "user": user },
            page: 1,
            sortname: 'addtime',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //部门经理
    function colNameShowFormatter(celvalue, options, rowData) {
        var backstr = "";
        var SpecialPersonAudit = $("#ctl00_ContentPlaceHolder1_SpecialPersonAudit").val();
        var SpecialPersonAuditID = $("#ctl00_ContentPlaceHolder1_SpecialPersonAuditID").val();
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();
        var applyusername = $.trim(rowData["username"]);
        var str = '<button type="button" class="btn default btn-xs" action="ApplyAgree" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" disabled="disabled">N</button>';
        //特殊人员
        var strSpecialPerson = SpecialPerson;
        strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";
        if (strSpecialPerson.indexOf(applyusername + ",") > -1) {

            var SpecialPersonList = SpecialPerson.split("|");
            var ind = 0;
            for (var i = 0; i < SpecialPersonList.length; i++) {
                if (SpecialPersonList[i].indexOf(applyusername) > -1) {
                    ind = i;
                    break;
                }
            }
            var SpecialPersonAuditList = SpecialPersonAudit.split(",");
            var SpecialPersonAuditIDList = SpecialPersonAuditID.split(",");
            //特殊人员的审批人
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == SpecialPersonAuditIDList[ind]) {
                str = '<button type="button" class="btn dark btn-xs" action="ApplyAgree" >Y</button>&nbsp;<button type="button" class="btn dark btn-xs" action="ApplyDisagree">N</button>';
            }
            switch (rowData["AuditStatus"]) {
                case "A":
                    backstr = SpecialPersonAuditList[ind] + "&nbsp;" + str;
                    break;
                case "B":
                case "D":
                case "E":
                case "N":
                    backstr = "<span class=\"badge badge-success\">√</span>" + SpecialPersonAuditList[ind];
                    break;
                case "C":
                    backstr = "<span class=\"badge badge-red\">×</span>" + SpecialPersonAuditList[ind];
                    break;

            }

        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowData["unitusers"]) {
                str = '<button type="button" class="btn dark btn-xs" action="ApplyAgree" >Y</button>&nbsp;<button type="button" class="btn dark btn-xs" action="ApplyDisagree">N</button>';
            }
            switch (rowData["AuditStatus"]) {
                case "A":
                    backstr = celvalue + "&nbsp;" + str;
                    break;
                case "B":
                case "D":
                case "E":
                case "N":
                    backstr = "<span class=\"badge badge-success\">√</span>" + celvalue;
                    break;
                case "C":
                    backstr = "<span class=\"badge badge-red\">×</span>" + celvalue;
                    break;

            }
        }
        return backstr;

    }

    //总经理
    function colShowFormatter(celvalue, options, rowData) {
        var backstr = "";
        var hours = rowData["totaltime"];
        var days = parseFloat(hours) / 7.5;
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();
        var applyusername = $.trim(rowData["username"]);
        var strSpecialPerson = SpecialPerson;
        strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";
        if (days >= 5 && strSpecialPerson.indexOf(applyusername + ",") < 0) {
            if ($.trim(celvalue) != "") {
                switch (rowData["AuditStatus"]) {
                    case "B":
                        if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowData["managerusers"]) {
                            backstr = celvalue + '&nbsp;<button type="button" managerid="' + celvalue + '" class="btn dark btn-xs" action="ApplyAgree" isdisbtn="yes" >Y</button>&nbsp;<button type="button" class="btn dark btn-xs" action="ApplyDisagree" isdisbtn="yes" >N</button>';
                        }
                        else {
                            backstr = celvalue + '&nbsp;<button type="button" class="btn default btn-xs" action="ApplyAgree" isdisbtn="yes" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" isdisbtn="yes" disabled="disabled">N</button>';
                        }
                        break;
                    case "D":
                        backstr = "<span class=\"badge badge-success\">√</span>" + celvalue;
                        break;
                    case "E":
                        backstr = "<span class=\"badge badge-red\">×</span>" + celvalue;
                        break;
                    case "N":
                        //退回只有状态为B或D才能操作，状态为B时点击退回，审批表Generalmanager字段为0,若不为0，即状态为D
                        if (rowData["AuditGeneralmanager"] != "0") {
                            backstr = "<span class=\"badge badge-success\">√</span>" + celvalue;
                        }
                        else {
                            backstr = celvalue + '&nbsp;<button type="button" class="btn default btn-xs" action="ApplyAgree" isdisbtn="yes" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" isdisbtn="yes" disabled="disabled">N</button>';
                        }
                        break;
                    default:
                        backstr = celvalue + '<button type="button" class="btn default btn-xs" action="ApplyAgree" isdisbtn="yes" disabled="disabled">Y</button>&nbsp;<button type="button" class="btn default btn-xs" action="ApplyDisagree" isdisbtn="yes" disabled="disabled">N</button>';
                        break;
                }

            }
        }
        else {
            backstr = "";
        }
        return backstr;
    }
    //编辑
    function colEditFormatter(celvalue, options, rowdata) {
        if (celvalue != "") {
            var backstr = '--';
            //只能自己编辑和发起审批
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowdata["adduser"]) {
                switch (rowdata["AuditStatus"]) {

                    case "A":
                       // backstr = '<a href="Apply.aspx?applyID=' + celvalue + '" alt="编辑" >编辑</a>';
                        break;
                    case null:
                    case "":
                    case "C":
                    case "E":
                       // backstr = '<a href="Apply.aspx?applyID=' + celvalue + '" alt="编辑" >编辑</a>&nbsp;&nbsp;<a href="javascript:void(0);" rel="' + celvalue + '"  >重新发起</a>';
                        break;
                    case "N":
                        backstr = "<span class=\"badge badge-red\">×</span>已退回,需重新填";
                        break;

                }
            }
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowdata["unitusers"] || $("#ctl00_ContentPlaceHolder1_isapplymanager").val() == "True") {
                switch (rowdata["AuditStatus"]) {
                    case "B":
                      //  backstr = '<button type="button" class="btn red btn-xs" id="btn_return" AuditSysNo="' + rowdata["AuditSysNo"] + '">退回</button>';
                        break;
                    case "N":
                        backstr = "<span class=\"badge badge-red\">×</span>已退回";
                        break;

                }
            }
            if ($("#ctl00_ContentPlaceHolder1_userSysNum").val() == rowdata["managerusers"] || $("#ctl00_ContentPlaceHolder1_isapplymanager").val() == "True") {
                switch (rowdata["AuditStatus"]) {
                    case "D":
                     //   backstr = '<button type="button" class="btn red btn-xs"  id="btn_return" AuditSysNo="' + rowdata["AuditSysNo"] + '">退回</button>';
                        break;
                    case "N":
                        backstr = "<span class=\"badge badge-red\">×</span>已退回";
                        break;
                }
            }
            return backstr;
        }
    }
    //统计 
    function completeMethod() {
        //已处理按钮不可用
        $("button", $("#jqGrid2")).attr("disabled", true);

        //特殊人的审批人
        var SpecialPersonAuditID = $("#ctl00_ContentPlaceHolder1_SpecialPersonAuditID").val();
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();

        var unit_flag = false;
        var manager_flag = false;
        var rowIds = $("#jqGrid2").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#jqGrid2 tr[id=" + rowIds[i]+"]").find("td").eq(1).text((i + 1));

            var status = $("#jqGrid2 tr[id=" + rowIds[i]+"]").find("td[aria-describedby='jqGrid_AuditStatus']").text();
            var unitsysno = $("#jqGrid2 tr[id=" + rowIds[i]+"]").find("td[aria-describedby='jqGrid_unitusers']").text();
            var managersysno = $("#jqGrid2 tr[id=" + rowIds[i]+"]").find("td[aria-describedby='jqGrid_managerusers']").text();
            var generalmanager = $("#jqGrid2 tr[id=" + rowIds[i]+"]").find("td[aria-describedby='jqGrid_generalmanager']").text();
            var applyusername = $.trim($("#jqGrid2 tr[id=" + rowIds[i]+"]").find("td[aria-describedby='jqGrid_username']").text());
            
            var strSpecialPerson = SpecialPerson;
            strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";
            //如果是特殊人
            if (strSpecialPerson.indexOf(applyusername + ",") > -1) {
                var SpecialPersonList = SpecialPerson.split("|");
                var ind = 0;
                for (var k = 0; k < SpecialPersonList.length; k++) {
                    if (SpecialPersonList[k].indexOf(applyusername) > -1) {
                        ind = k;
                        break;
                    }
                }
                var SpecialPersonAuditIDList = SpecialPersonAuditID.split(",");
                //审批人的重新赋值特殊人审批
                unitsysno = SpecialPersonAuditIDList[ind];
            }
            if ((status == "A" && unitsysno == usersysno)) {
                unit_flag = true;
            }
            if (status == "B" && managersysno == usersysno && generalmanager != "") {
                manager_flag = true;
            }
        }
        //if (!unit_flag) {
        //    $("#btn_unitaudit").attr("disabled", true);
        //}
        //if (!manager_flag) {
        //    $("#btn_manageraudit").attr("disabled", true);
        //}


    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid2").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata2").text() == '') {
                $("#jqGrid2").append("<div id='nodata'>没有查询到数据!</div>");
            }
            else { $("#nodata2").show(); }
        }
        else {
            $("#nodata2").hide();
        }
    }
    var clearGridPostData = function () {
        var postData = $("#jqGrid2").jqGrid("getGridParam", "postData");
        $.each(postData, function (k, v) {
            delete postData[k];
        });
    }
}