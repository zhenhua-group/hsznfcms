﻿$(function () {
   
    //第一行加背景色
    $("table[id^='tbl_']>tbody>tr:first-child > td").css("background-color", "#f0f0f0");
   
    //类型选择
    $(":radio[name='ctl00$ContentPlaceHolder1$RadioType']").click(function () {
        var value = $(this).val();
        $("table[id^='tbl_']").hide();
        $("table[id='tbl_" + value + "']").show();

        if ($(":radio[name='ctl00$ContentPlaceHolder1$RadioType']:checked").val() == "travel") {
            //加班统计
            travel("", "", "","");
        }
        else if ($(":radio[name='ctl00$ContentPlaceHolder1$RadioType']:checked").val() == "gomeet") {
            //外勤加班统计
            travel("", "", "gomeet","");
        }
        else if ($(":radio[name='ctl00$ContentPlaceHolder1$RadioType']:checked").val() == "forget") {
            //未打卡加班统计
            travel("", "", "forget", "");
        }


    });
    //显示哪种申请,首页连接
    if ($("#ctl00_ContentPlaceHolder1_hid_applytype").val()!="")
    {      
        $("table[id^='tbl_']").hide();
        $("table[id='tbl_" + $("#ctl00_ContentPlaceHolder1_hid_applytype").val() + "']").show();
        if ($("#ctl00_ContentPlaceHolder1_hid_applytype").val() == "travel") {
            //加班统计
            travel("", "", "","");
        }
        else if ($("#ctl00_ContentPlaceHolder1_hid_applytype").val() == "gomeet") {
            //外勤加班统计
            travel("", "", "gomeet","");
        }
    }

    //编辑页面，不显示添加按钮
    if ($("#ctl00_ContentPlaceHolder1_hid_applyID").val() != "" && $("#ctl00_ContentPlaceHolder1_hid_applyID").val() != "0") {
        $("#tr_btn").css("display", "none");
    }
    else {
        $("#tr_btn").css("display", "");
    }
    
    //判断是否自驾
    if ($("#ctl00_ContentPlaceHolder1_hid_iscar").val() == "s")
    {
        if ($("#ctl00_ContentPlaceHolder1_hid_applytype").val() == "travel") {
            $(":checkbox[id='iscar']").attr("checked", true);
        }
        else if ($("#ctl00_ContentPlaceHolder1_hid_applytype").val() == "gomeet") {
            $(":checkbox[id='iscar2']").attr("checked", true);
        }
        else if ($("#ctl00_ContentPlaceHolder1_hid_applytype").val() == "company") {
            $(":checkbox[id='iscar4']").attr("checked", true);
        }
        
    }
    //数字验证正则    
    var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

    //$("#ctl00_ContentPlaceHolder1_starttime").change(function () {

    //    diff();
    //});
    //$("#ctl00_ContentPlaceHolder1_endtime").change(function () {
    //    diff();
    //});
    //日期选择，上下班重新设置
    $("#date").change(function () {
        var uid = $("#userid").val();
        var date1 = new Date($(this).val().replace(/-/g, "/"));
        var months = date1.getMonth() + 1;
        var years = date1.getFullYear();
      
        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";        
        $.get(url, { "action": "seltime", "userid": uid, "months": months, "years": years }, function (result) {
          
            if (result != null && result != "") {
               
                var data = eval('(' + result + ')');
                if (data!=null) {
                    $("#clockface_2").val(data.towork);
                    $("#clockface_2_modal").val(data.offwork);
                    $("#hid_clockface_2").val(data.towork);
                    $("#hid_clockface_2_modal").val(data.offwork);

                    $("#ctl00_ContentPlaceHolder1_start_sj").val(data.towork);
                    $("#ctl00_ContentPlaceHolder1_end_sj").val(data.offwork);

                }
               
            }
           
        });

        //加班统计
        travel("", "", "","");
    });

    //选择时间
    $(".clockface").live("click", function () {
        travel("","","","");
    });
    //
    $(":text[id*='kilometre']").blur(function () {
        var value = $(this).val();
        if (!reg_math.test(value)) {
            alert("公里数填为数字!")
            return false;
        }
        else
        {
            if (parseFloat(value) < 35 && parseFloat(value)>0) {
                alert("小于35不能填公里数并不参与自驾统计!");
                $(this).val(0);
                return false;
            }
            
        }
       
    });

    //保存按钮
    $("#btn_save").click(function () {
        var applylx = $(":radio[name='ctl00$ContentPlaceHolder1$RadioType']:checked").val();       
       
        var objApply = {};
        var addtimestr = "";
        //请假申请单
        if (applylx == "leave") {

            var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime").val());
            var end = $.trim($("#ctl00_ContentPlaceHolder1_endtime").val());
            if (start == "") {
                alert("申请开始时间不能为空!");
                return false;
            }
            if (end == "") {
                alert("申请结束时间不能为空!");
                return false;
            }

            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_totaltime").val())) {
                alert("考勤统计格式不正确!");
                return false;
            }
            else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_totaltime").val()) <=0) {
                    alert("考勤统计小时不能小于等于0!");
                    return false;
                }

            }
            objApply = {
                "applytype": applylx,
                "reason": $(":radio[name='ctl00$ContentPlaceHolder1$applyType']:checked").val(),
                "address": "",
                "starttime": start,
                "endtime": end,
                "totaltime": $("#ctl00_ContentPlaceHolder1_totaltime").val(),
                "iscar": "",
                "kilometre": "0",
                "remark": $("#ctl00_ContentPlaceHolder1_applyContent").val()
            }


        }
        else if (applylx == "travel") { //出差
            if ($("#ctl00_ContentPlaceHolder1_proname").val() == "") {
                alert("项目名称不能为空！");
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_address").val() == "") {
                alert("出差地点不能为空！");
                return false;
            }
           // var date1=$("#date").val();
         //   var clock1 = $("#clockface_2").val();
          //  var clock2 = $("#clockface_2_modal").val();

            //if (clock1 == "") {
            //    alert("申请开始时间不能为空!");
            //    return false;
            //}
            //else {
            //    var strlist = clock1.split(":");
            //    if (strlist.length != 2) {
            //        alert("申请开始时间异常!");
            //        return false;
            //    }
            //}
            //if (clock2 != "") {
            //    var strlist2 = clock2.split(":");
            //    if (strlist2.length != 2) {
            //        alert("申请结束时间异常!");
            //        return false;
            //    }
            //}
            var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime7").val());
            var end = $.trim($("#ctl00_ContentPlaceHolder1_endtime7").val());
            if (start == "") {
                alert("申请开始时间不能为空!");
                return false;
            }
            if (end == "") {
                alert("申请结束时间不能为空!");
                return false;
            }
            var starttime = new Date(start.replace(/-/g, "/"));
            var endtime = new Date(end.replace(/-/g, "/"));
            if (endtime <= starttime) {
                alert("开始时间不能大于等于结束时间!");
                return false;
            }
            //结束日期最大值
            var max_endtime = new Date(ControlDate("starttime7").replace(/-/g, "/"));
            if (endtime > max_endtime) {
                alert("结束时间不能超过凌晨6点!");
                return false;
            }

            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_totaltime2").val())) {
                alert("加班统计格式不正确!");
                return false;
            }
            else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_totaltime2").val()) < 0) {
                    alert("加班统计不能小于0!");
                    return false;
                }

            }
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_kilometre").val())) {
                alert("公里数填为数字!")
                return false;
            } else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_kilometre").val()) < 35 && parseFloat($("#ctl00_ContentPlaceHolder1_kilometre").val()) > 0) {
                    alert("小于35不能填公里数并不参与自驾统计!");
                    $("#ctl00_ContentPlaceHolder1_kilometre").val(0);
                    return false;
                }
            }

            objApply = {
                "applytype": applylx,
                "reason": $("#ctl00_ContentPlaceHolder1_proname").val(),
                "address": $("#ctl00_ContentPlaceHolder1_address").val(),
                "starttime": start,
                "endtime": end,
                "totaltime": $("#ctl00_ContentPlaceHolder1_totaltime2").val(),
                "iscar": $(":checkbox[id='iscar']:checked").val(),
                "kilometre": $("#ctl00_ContentPlaceHolder1_kilometre").val(),
                "remark": $("#ctl00_ContentPlaceHolder1_applyContent2").val()
            }

            var arr = new Array();
            //动态时间
            $("#tableaddtime tr:gt(0)").each(function (i,k) {

               var objtime = {
                    "applytype": applylx,
                    "reason": $("#ctl00_ContentPlaceHolder1_proname").val(),
                    "address": $("#ctl00_ContentPlaceHolder1_address").val(),
                    "starttime": $(k).find("td:eq(0) input").val(),
                    "endtime": $(k).find("td:eq(2) input").val(),
                    "totaltime": $(k).find("td:eq(3) input").val(),
                    "iscar": "",
                    "kilometre": "0",
                    "remark": $("#ctl00_ContentPlaceHolder1_applyContent2").val()
                }
               arr.push(objtime);
            });
            if ($("#tableaddtime tr:gt(0)").length>0) {
                addtimestr = Global.toJSON(arr);
            }
            
        }
        else if (applylx == "gomeet") { //外出开会
            if ($("#ctl00_ContentPlaceHolder1_reason").val() == "") {
                alert("事由不能为空！");
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_address2").val() == "") {
                alert("地点不能为空！");
                return false;
            }
           // var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime2").val()) +" "+ $.trim($("#ctl00_ContentPlaceHolder1_start_sj").val());
            //  var end = $.trim($("#ctl00_ContentPlaceHolder1_starttime2").val()) + " " + $.trim($("#ctl00_ContentPlaceHolder1_end_sj").val());
              var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime2").val());
              var end = $.trim($("#ctl00_ContentPlaceHolder1_endtime2").val()) ;
            if (start == "") {
                alert("开始时间不能为空!");
                return false;
            }
            if (end == "") {
                alert("结束时间不能为空!");
                return false;
            }           
            var starttime = new Date(start.replace(/-/g, "/"));
            var endtime = new Date(end.replace(/-/g, "/"));
            if (endtime <= starttime) {
                alert("开始时间不能大于等于结束时间!");                
                return false;
            }
            //结束日期最大值
            var max_endtime = new Date(ControlDate("starttime2").replace(/-/g, "/"));
            if (endtime > max_endtime) {
                alert("结束时间不能超过凌晨6点!");
                return false;
            }

            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_totaltime3").val())) {
                alert("加班统计格式不正确!");
                return false;
            }
            else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_totaltime3").val()) < 0) {
                    alert("加班统计不能小于0!");
                    return false;
                }

            }
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_kilometre2").val())) {
                alert("公里数填为数字!")
                return false;
            } else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_kilometre2").val()) < 35 && parseFloat($("#ctl00_ContentPlaceHolder1_kilometre2").val()) > 0) {
                    alert("小于35不能填公里数并不参与自驾统计!");
                    $("#ctl00_ContentPlaceHolder1_kilometre2").val(0);
                    return false;
                }
            }

            objApply = {
                "applytype": applylx,
                "reason": $("#ctl00_ContentPlaceHolder1_reason").val(),
                "address": $("#ctl00_ContentPlaceHolder1_address2").val(),
                "starttime": start,
                "endtime": end,
                "totaltime": $("#ctl00_ContentPlaceHolder1_totaltime3").val(),
                "iscar": $(":checkbox[id='iscar2']:checked").val(),
                "kilometre": $("#ctl00_ContentPlaceHolder1_kilometre2").val(),
                "remark": ""
            }

        }       
        else if (applylx == "forget") {  //忘记打卡

          //  var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime3").val()) + " " + $.trim($("#ctl00_ContentPlaceHolder1_start_sj2").val());
          //  var end = $.trim($("#ctl00_ContentPlaceHolder1_starttime3").val()) + " " + $.trim($("#ctl00_ContentPlaceHolder1_end_sj2").val());
            var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime3").val());
            var end = $.trim($("#ctl00_ContentPlaceHolder1_endtime3").val());
            if (start == "") {
                alert("开始时间不能为空!");
                return false;
            }
            if (end == "") {
                alert("结束时间不能为空!");
                return false;
            }

            var starttime = new Date(start.replace(/-/g, "/"));
            var endtime = new Date(end.replace(/-/g, "/"));
            if (endtime <= starttime) {
                alert("开始时间不能大于等于结束时间!");
                return false;
            }

            //结束日期最大值
            var max_endtime = new Date(ControlDate("starttime3").replace(/-/g, "/"));
            if (endtime > max_endtime) {
                alert("结束时间不能超过凌晨6点!");
                return false;
            }

            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_totaltime7").val())) {
                alert("加班统计格式不正确!");
                return false;
            }
            else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_totaltime7").val()) < 0) {
                    alert("加班统计不能小于0!");
                    return false;
                }

            }
           
            objApply = {
                "applytype": applylx,
                "reason": "",
                "address": "",
                "starttime": start,
                "endtime": end,
                "totaltime": $("#ctl00_ContentPlaceHolder1_totaltime7").val(),
                "iscar": "",
                "kilometre": "0",
                "remark": $("#ctl00_ContentPlaceHolder1_applyContent3").val()
            }
         
           
        }        
        else if (applylx == "company") { //公司活动

            var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime4").val());
            var end = $.trim($("#ctl00_ContentPlaceHolder1_endtime4").val());
            if (start == "") {
                alert("申请开始时间不能为空!");
                return false;
            }
            if (end == "") {
                alert("申请结束时间不能为空!");
                return false;
            }

            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_totaltime4").val())) {
                alert("考勤统计格式不正确!");
                return false;
            }
            else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_totaltime4").val()) <= 0) {
                    alert("考勤统计小时不能小于等于0!");
                    return false;
                }

            }
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_kilometre4").val())) {
                alert("公里数填为数字!")
                return false;
            } else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_kilometre4").val()) < 35 && parseFloat($("#ctl00_ContentPlaceHolder1_kilometre4").val()) > 0) {
                    alert("小于35不能填公里数并不参与自驾统计!");
                    $("#ctl00_ContentPlaceHolder1_kilometre4").val(0);
                    return false;
                }
            }
            objApply = {
                "applytype": applylx,
                "reason": "公司活动",
                "address": $(":radio[name='ctl00$ContentPlaceHolder1$applyType4']:checked").val(),
                "starttime": start,
                "endtime": end,
                "totaltime": $("#ctl00_ContentPlaceHolder1_totaltime4").val(),
                "iscar": $(":checkbox[id='iscar4']:checked").val(),
                "kilometre": $("#ctl00_ContentPlaceHolder1_kilometre4").val(),
                "remark": $("#ctl00_ContentPlaceHolder1_applyContent4").val()
            }


        }
        else if (applylx == "depart") { //部门活动

            var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime5").val());
            var end = $.trim($("#ctl00_ContentPlaceHolder1_endtime5").val());
            if (start == "") {
                alert("申请开始时间不能为空!");
                return false;
            }
            if (end == "") {
                alert("申请结束时间不能为空!");
                return false;
            }

            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_totaltime5").val())) {
                alert("考勤统计格式不正确!");
                return false;
            }
            else {
                if (parseFloat($("#ctl00_ContentPlaceHolder1_totaltime5").val()) <= 0) {
                    alert("考勤统计小时不能小于等于0!");
                    return false;
                }

            }
            objApply = {
                "applytype": applylx,
                "reason": $(":radio[name='ctl00$ContentPlaceHolder1$applyType5']:checked").val(),
                "address": "",
                "starttime": start,
                "endtime": end,
                "totaltime": $("#ctl00_ContentPlaceHolder1_totaltime5").val(),
                "iscar": "",
                "kilometre": "0",
                "remark": $("#ctl00_ContentPlaceHolder1_applyContent5").val()
            }


        }
        else if (applylx == "addwork") { //加班离岗

            var start = $.trim($("#ctl00_ContentPlaceHolder1_starttime6").val()) + " " + $.trim($("#ctl00_ContentPlaceHolder1_start_sj6").val());
            var end = $.trim($("#ctl00_ContentPlaceHolder1_starttime6").val()) + " " + $.trim($("#ctl00_ContentPlaceHolder1_end_sj6").val());
           
            if (start == "") {
                alert("申请开始时间不能为空!");
                return false;
            }
            if (end == "") {
                alert("申请结束时间不能为空!");
                return false;
            }
            var starttime = new Date(start.replace(/-/g, "/"));
            var endtime = new Date(end.replace(/-/g, "/"));
            if (endtime <= starttime) {
                alert("开始时间不能大于等于结束时间!");
                return false;
            }
            //if (!reg_math.test($("#ctl00_ContentPlaceHolder1_totaltime6").val())) {
            //    alert("考勤统计格式不正确!");
            //    return false;
            //}
            //else {
            //    if (parseFloat($("#ctl00_ContentPlaceHolder1_totaltime6").val()) <= 0) {
            //        alert("考勤统计小时不能小于等于0!");
            //        return false;
            //    }

            //}
            objApply = {
                "applytype": applylx,
                "reason": $(":radio[name='ctl00$ContentPlaceHolder1$applyType6']:checked").val(),
                "address": "",
                "starttime": start,
                "endtime": end,
                "totaltime": $("#ctl00_ContentPlaceHolder1_totaltime6").val(),
                "iscar": "",
                "kilometre": "0",
                "remark": $("#ctl00_ContentPlaceHolder1_applyContent6").val()
            }


        }
        var applyid=$("#ctl00_ContentPlaceHolder1_hid_applyID").val();
        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        var objstr = Global.toJSON(objApply);
        $.post(url, { "action": "add", "data": objstr, "applyID": applyid, "addtimestr": addtimestr }, function (result) {
            if (result == "1") {
                alert("申请成功！消息已发送给部门经理！");
                window.location.reload();
                //if (applylx == "travel") {
                //    window.location.href = 'TravelListByMaster.aspx';
                //}
                //else if (applylx == "gomeet") {
                //    window.location.href = 'GoMeetListByMaster.aspx';
                //}
                //else if (applylx == "forget") {
                //    window.location.href = 'ForgetTimeListByMaster.aspx';
                //}
                //else if (applylx == "company") {
                //    window.location.href = 'CompanyTimeListByMaster.aspx';
                //}
                //else if (applylx == "depart") {
                //    window.location.href = 'DepartTimeListByMaster.aspx';
                //}
                //else if (applylx == "addwork") {
                //    window.location.href = 'AddworkTimeListByMaster.aspx';
                //}
                //else {
                //    window.location.href = 'LeaveListByMaster.aspx';
                //}
            }
            else if (result == "2")
            {
                alert("消息发送失败！");
                window.location.reload();
            }
            else if (result == "3")
            {
                alert("修改成功！");
                var applytype=$("#ctl00_ContentPlaceHolder1_hid_applytype").val();
                if (applytype == "travel")
                {
                    window.location.href = 'TravelListByMaster.aspx';
                }
                else if (applytype == "gomeet")
                {
                    window.location.href = 'GoMeetListByMaster.aspx';
                }
                else if (applytype == "forget")
                {
                    window.location.href = 'ForgetTimeListByMaster.aspx';
                }
                else if (applytype == "company") {
                    window.location.href = 'CompanyTimeListByMaster.aspx';
                }
                else if (applylx == "depart") {
                    window.location.href = 'DepartTimeListByMaster.aspx';
                }
                else if (applylx == "addwork") {
                    window.location.href = 'AddworkTimeListByMaster.aspx';
                }
                else
                {
                    window.location.href = 'LeaveListByMaster.aspx';
                }

            }
            else {
                alert("申请失败！");
            }
        });

    });  

    //日期事件
    //$("#starttime").bind("focus", function () {
    //    WdatePicker({          
    //        onpicked: function (dp) { if (dp.cal.getDateStr() != dp.cal.getNewDateStr()) { alert(1) } }
    //    });
    //});
    //添加时间
    $("#btn_addtime").click(function () {
        var ontime = $("#hid_clockface_2").val();
        var offtime = $("#hid_clockface_2_modal").val();
        var starttime = $("#tableaddtime tr:last>td:first").find("input").val();
        var $tr = $("#tableNone tr:first");       
        $tr.find("td:first input").val(addDate(starttime.substr(0, 10), 1) + " " + ontime);
        $tr.find("td:eq(2) input").val(addDate(starttime.substr(0, 10), 1) + " " + offtime);
        var inputID = $tr.find("td:eq(3) input").get(0).id;
        //控件最后两字符
        var addtimeid = inputID.substr((inputID.length - 2), 2);
        //计算统计
        travel('', '', '', addtimeid);
       // $tr.find("td:eq(3) input").val("0");
         $("#tableaddtime").append($tr);
    });
    //删除时间
    $("a[rel='del']").live("click", function () {
        var $tr = $(this).parent().parent().parent();
        $tr.find("td:eq(3) input").val("0");
        $("#tableNone").append($tr);
      //  $(this).parent().parent().parent().remove();

    });

    $("#dpTimeUp").hide();
    $("#dpTimeDown").hide();

});
//请假计算
function diff(wtime, wtype, applytype) {
   
    var kssjname = "starttime";
    var jssjname = "endtime";
    var tjname = "totaltime";
    if (applytype == "company") {
         kssjname = "starttime4";
         jssjname = "endtime4";
         tjname = "totaltime4";
    }
    else if (applytype == "depart")
    {
        kssjname = "starttime5";
        jssjname = "endtime5";
        tjname = "totaltime5";
    }
    else if (applytype == "addwork") {
        kssjname = "start_sj6";
        jssjname = "end_sj6";
        tjname = "totaltime6";
    }

    if (wtype == "start")
    {
        $("#ctl00_ContentPlaceHolder1_" + kssjname).val(wtime);
    }
    else {
        $("#ctl00_ContentPlaceHolder1_" + jssjname).val(wtime);
    }

    var start = $.trim($("#ctl00_ContentPlaceHolder1_" + kssjname).val());
    var end = $.trim($("#ctl00_ContentPlaceHolder1_" + jssjname).val());

   

    if (applytype == "addwork") {
        start = $("#ctl00_ContentPlaceHolder1_starttime6").val() + " " + start;
        end = $("#ctl00_ContentPlaceHolder1_starttime6").val() + " " + end;
    }    
   

    var starttime = new Date(start.replace(/-/g, "/"));
    var endtime = new Date(end.replace(/-/g, "/"));
   
    if (endtime <= starttime) {
        alert("开始时间不能大于等于结束时间!");
        $("#ctl00_ContentPlaceHolder1_" + tjname).val("0");
        return false;
    }
    //开始时间优化，方便计算
    var start_str = start.substr(start.indexOf(":") + 1);
    var start_fs = parseInt(start_str);
    if (start_fs>0&&start_fs < 30)
    {
        start_str = "00";
    }
    else if (start_fs > 30 && start_fs < 60) {
        start_str = "30";
    }
    start=start.substr(0, start.indexOf(":")+1) + start_str;
    //结束时间优化
    var end_fs = parseInt(end.substr(end.indexOf(":") + 1));  
    if (end_fs > 0 && end_fs < 30) {
        end = end.substr(0, end.indexOf(":") + 1) + "30";       
    }
    else if (end_fs > 30 && end_fs < 60) {
        //小时需要加1，约等于整点
        var end_str = end.substring(11, end.indexOf(":"));       
        end = end.substr(0, 10) +" "+ (parseInt(end_str)+1) + ":00";
    }
    //重新赋值
     starttime = new Date(start.replace(/-/g, "/"));
     endtime = new Date(end.replace(/-/g, "/"));
     var hours = 0;
    //判断是否同一天
    if ((start.substr(0, 10)) == (end.substr(0, 10))) {
        //上班时间
        var ontime = new Date((start.substr(0, 10) + " 09:00").replace(/-/g, "/"));
        //中午下班时间
        var zwxbtime = new Date((start.substr(0, 10) + " 11:50").replace(/-/g, "/"));
        //中午上班时间
        var zwsbtime = new Date((start.substr(0, 10) + " 13:00").replace(/-/g, "/"));
        //下午下班时间
        var offtime = new Date((start.substr(0, 10) + " 17:30").replace(/-/g, "/"));
        var hours = 0;
        //不是加班离岗
        if (applytype != "addwork") {
            if (starttime < ontime) {
                starttime = ontime;
            }
            if (endtime > offtime) {
                endtime = offtime;
            }
        }
       
        if (starttime >= zwxbtime && starttime <= zwsbtime) {
            starttime = zwsbtime;
        }
        if (endtime >= zwxbtime && endtime <= zwsbtime) {
            endtime = zwxbtime;
        }
        

        if (endtime > ontime && starttime < offtime) {

            if (endtime > zwsbtime && starttime < zwxbtime) {
                //请一天假，相减得到毫秒
                var minus = endtime - starttime;
                var hours1 = parseInt(minus / (3600 * 1000));               
                var leave1 = minus % (3600 * 1000);//计算小时数后剩余的毫秒数
                var minutes = parseInt(leave1 / (60 * 1000))//分钟   
               
                //大于30分钟，即加班一个小时
                if (minutes <= 30 && minutes>0) {
                    
                    hours1 = hours1 +0.5;
                }
                else if (minutes > 30) {
                    hours1 = hours1 + 1;
                }

                hours += ((hours1) - 1);
            }
            else {
                //半天假，相减得到毫秒
                var minus = endtime - starttime;
                var hours1 = parseInt(minus / (3600 * 1000));
                var leave1 = minus % (3600 * 1000);//计算小时数后剩余的毫秒数
                var minutes = parseInt(leave1 / (60 * 1000))//分钟   
              
                //大于30分钟，即加班一个小时
                if (minutes <= 30 && minutes > 0) {
                    hours1 = hours1+0.5;
                }
                else if (minutes > 30) {
                    hours1 = hours1 + 1;
                }

                hours += (hours1);

            }

        }
        else{
            //半天假，相减得到毫秒
            var minus = endtime - starttime;
            var hours1 = parseInt(minus / (3600 * 1000));
            var leave1 = minus % (3600 * 1000);//计算小时数后剩余的毫秒数
            var minutes = parseInt(leave1 / (60 * 1000))//分钟   

            //大于30分钟，即加班一个小时
            if (minutes <= 30 && minutes > 0) {
                hours1 = hours1 + 0.5;
            }
            else if (minutes > 30) {
                hours1 = hours1 + 1;
            }

            hours += (hours1);
        }
        $("#ctl00_ContentPlaceHolder1_" + tjname).val(hours.toFixed(1));

    }
    else {
        //请假日期
        var ontime = new Date((start.substr(0, 10)).replace(/-/g, "/"));
        var offtime = new Date((end.substr(0, 10)).replace(/-/g, "/"));
        //请假天数，排除前后当天日期
        var days = (offtime - ontime) / (24 * 3600 * 1000) - 1;
       
        //排除周六日和节假日
        var holiday = TG.Web.Calendar.Apply.IsHoliday(start, end).value;
       
        if (parseInt(holiday) > 0) {
          
            days = days - parseInt(holiday);
        }
        /////////////////////////开始时间
        //排除假期
        if (!TG.Web.Calendar.Apply.isJQ(starttime).value) {
           
            //上班时间
            var ontime = new Date((start.substr(0, 10) + " 09:00").replace(/-/g, "/"));
            //中午下班时间
            var zwxbtime = new Date((start.substr(0, 10) + " 11:50").replace(/-/g, "/"));
            //中午上班时间
            var zwsbtime = new Date((start.substr(0, 10) + " 13:00").replace(/-/g, "/"));
            //下午下班时间
            var offtime = new Date((start.substr(0, 10) + " 17:30").replace(/-/g, "/"));
            //开始时间不超过9点，按9点计算
            if (starttime < ontime) {
                starttime = ontime;
            }
            if (starttime >= zwxbtime && starttime < zwsbtime) {
                starttime = zwsbtime;
            }
            var hours = 0;
            if (starttime < offtime) {
                var minus = offtime - starttime;
                var hours1 = parseInt(minus / (3600 * 1000));
                var leave1 = minus % (3600 * 1000);//计算小时数后剩余的毫秒数
                var minutes = parseInt(leave1 / (60 * 1000))//分钟    
                //大于30分钟，即加班一个小时
                if (minutes <= 30 && minutes > 0) {
                    hours1 = hours1 + 0.5;
                }
                else if (minutes > 30) {
                    hours1 = hours1 + 1;
                }
                hours += (hours1);
                //开始时间不超过12点，需减去1小时中午休息时间
                if (starttime < zwxbtime) {
                    hours = hours - 1;
                }
            }
        }
        //排除假期
        if (!TG.Web.Calendar.Apply.isJQ(endtime).value) {
            /////////////////结束时间
            //上班时间
            ontime = new Date((end.substr(0, 10) + " 09:00").replace(/-/g, "/"));
            //中午下班时间
            zwxbtime = new Date((end.substr(0, 10) + " 11:50").replace(/-/g, "/"));
            //中午上班时间
            zwsbtime = new Date((end.substr(0, 10) + " 13:00").replace(/-/g, "/"));
            //下午下班时间
            offtime = new Date((end.substr(0, 10) + " 17:30").replace(/-/g, "/"));
            //结束时间超过17:30点，按17:30点计算
            if (endtime > offtime) {
                endtime = offtime;
            }
            if (endtime >= zwxbtime && endtime < zwsbtime) {
                endtime = zwxbtime;
            }
            if (endtime > ontime) {
                var minus2 = endtime - ontime;
                var hours2 = parseInt(minus2 / (3600 * 1000));
                var leave2 = minus2 % (3600 * 1000);//计算小时数后剩余的毫秒数
                var minutes2 = parseInt(leave2 / (60 * 1000))//分钟    
                //大于30分钟，即加班一个小时
                if (minutes2 <= 30 && minutes2 > 0) {
                    hours2 = hours2 + 0.5;
                }
                else if (minutes2 > 30) {
                    hours2 = hours2 + 1;
                }
                hours += (hours2);
                //结束时间超过13点，需减去1小时中午休息时间
                if (endtime > zwsbtime) {
                    hours = hours - 1;
                }
            }
        }
      
        ////加上中间请假天数*7.5
        hours = hours + (days * 7.5);

        $("#ctl00_ContentPlaceHolder1_" + tjname).val(hours);

    }


    //计算出相差天数
    // var days = Math.floor(minus / (24 * 3600 * 1000))

    //计算出小时数
    // var leave1 = minus % (24 * 3600 * 1000)    //计算天数后剩余的毫秒数
    //  var hours = Math.floor(leave1 / (3600 * 1000))
    //计算相差分钟数
    // var leave2 = leave1 % (3600 * 1000)        //计算小时数后剩余的毫秒数
    // var minutes = Math.floor(leave2 / (60 * 1000))

    //计算相差秒数
    // var leave3 = leave2 % (60 * 1000)      //计算分钟数后剩余的毫秒数
    //  var seconds = Math.round(leave3 / 1000)
    //  alert(" 相差 " + days + "天 " + hours + "小时 " + minutes + " 分钟" + seconds + " 秒");

    //计算相差小时

    //var hours = parseFloat(minus / (3600 * 1000)).toFixed(1);         
    //alert(hours);
    //return false;
}
//出差计算
function travel(wtime, wtype, applytype,addtimeid)
{
   
    //var date = $.trim($("#date").val());
    //var start = $.trim(date + " " + $.trim($("#clockface_2").val())+":00");
    //var end = $.trim(date + " " + $.trim($("#clockface_2_modal").val()) + ":00");

    var kssjname = "starttime7";
    var jssjname = "endtime7";
    var tjname = "totaltime2";
    //外勤
    if (applytype == "gomeet") {
        kssjname = "starttime2";
        jssjname = "endtime2";
        tjname = "totaltime3";
    }
    else if (applytype == "forget") {
        kssjname = "starttime3";
        jssjname = "endtime3";
        tjname = "totaltime7";
    }
    else
    {
       
        //添加按钮时间
        if (addtimeid!="") {
            kssjname = "starttime" + addtimeid;
            jssjname = "endtime" + addtimeid;
            tjname = "totaltime" + addtimeid;
        }
    }

    if (wtype == "start") {
        $("#ctl00_ContentPlaceHolder1_" + kssjname).val(wtime);
    }
    else if (wtype == "end") {
        $("#ctl00_ContentPlaceHolder1_" + jssjname).val(wtime);
    }

    var start = $.trim($("#ctl00_ContentPlaceHolder1_" + kssjname).val());
    var end = $.trim($("#ctl00_ContentPlaceHolder1_" + jssjname).val());
    var date = start.substr(0, 10);
  
    //外勤 之前写法
    //if (applytype == "gomeet") {       

    //    if (wtype == "start") {
    //        $("#ctl00_ContentPlaceHolder1_start_sj").val(wtime);
    //    }
    //    else if (wtype == "end") {
    //        $("#ctl00_ContentPlaceHolder1_end_sj").val(wtime);
    //    }

    //    date = $("#ctl00_ContentPlaceHolder1_starttime2").val();
    //    start = $.trim(date + " " + $.trim($("#ctl00_ContentPlaceHolder1_start_sj").val()) + ":00");
    //    end = $.trim(date + " " + $.trim($("#ctl00_ContentPlaceHolder1_end_sj").val()) + ":00");
    //    tjname = "totaltime3";
    //}

    //填写时间
    var starttime = new Date(start.replace(/-/g, "/"));
    var endtime = new Date(end.replace(/-/g, "/"));

    if (endtime <= starttime) {
        alert("开始时间不能大于等于结束时间!");
        $("#ctl00_ContentPlaceHolder1_" + tjname).val("0");
        return false;
    }

    //设置上下班时间
    var ontime = new Date(date+" "+$("#hid_clockface_2").val().replace(/-/g, "/"));//
    var offtime = new Date(date + " " + $("#hid_clockface_2_modal").val().replace(/-/g, "/"));//
    var hours = 0;
    //大于15分钟，才算加班
   // ontime.setMinutes(ontime.getMinutes() -15);
    //加上30分钟，加班时间
    offtime.setMinutes(offtime.getMinutes() + 30);
    //大于15分钟，才算加班
   // offtime.setMinutes(offtime.getMinutes() + 15);
   
    //判断假期
    if (TG.Web.Calendar.Apply.isJQ(starttime).value) {
        var minus = endtime - starttime;
        var hours1 = parseInt(minus / (3600 * 1000));//计算小时
        var leave1 = minus % (3600 * 1000);//计算小时数后剩余的毫秒数
        var minutes = leave1 / (60 * 1000)//分钟       

        if (minutes >= 15 && minutes <= 45) {
            hours1 = hours1 + 0.5;
        }
        else if (minutes >= 45 && minutes < 60) {
            hours1 = hours1 + 1;
        }

        hours = parseFloat(hours1);
    }
    else {
        //早于上班时间
        if (starttime < ontime) {
            var minus = ontime - starttime;
            //截止时间小于上班时间
            if (endtime < ontime) {
                minus = endtime - starttime;
            }
            var hours1 = parseInt(minus / (3600 * 1000));//计算小时
            var leave1 = minus % (3600 * 1000);//计算小时数后剩余的毫秒数
            var minutes = leave1 / (60 * 1000)//分钟     

            if (minutes >= 15 && minutes < 45) {
                hours1 = hours1 + 0.5;
            }
            else if (minutes >= 45 && minutes < 60) {
                hours1 = hours1 + 1;
            }

            hours += parseFloat(hours1);
        }
        //晚于下班时间
        if (endtime > offtime) {

            var minus = endtime - offtime;
            if (starttime > offtime) {

                minus = endtime - starttime;
            }
            var hours1 = parseInt(minus / (3600 * 1000));//计算小时
            var leave1 = minus % (3600 * 1000);//计算小时数后剩余的毫秒数
            var minutes = leave1 / (60 * 1000)//分钟       

            if (minutes >= 15 && minutes <= 45) {
                hours1 = hours1 + 0.5;
            }
            else if (minutes >= 45 && minutes < 60) {
                hours1 = hours1 + 1;
            }

            hours += parseFloat(hours1);
        }
    }
    $("#ctl00_ContentPlaceHolder1_" + tjname).val(hours);
}

//加班离岗
function commFunc(element, wtype)
{   
    WdatePicker({ el: element, readOnly: true, dateFmt: 'HH:mm', isShowClear: false,dpTimeUp:false, onpicking: function () { if ($dp.cal.getDateStr() != $dp.cal.getNewDateStr()) { diff($dp.cal.getNewDateStr(), wtype, 'addwork'); } }, Hchanging: function () { if ($dp.cal.getDateStr() != $dp.cal.getNewDateStr()) { diff($dp.cal.getNewDateStr(), wtype, 'addwork'); } }, mchanging: function () { if ($dp.cal.getDateStr() != $dp.cal.getNewDateStr()) { diff($dp.cal.getNewDateStr(), wtype, 'addwork'); } }, onpicked: function () { diff($dp.cal.getNewDateStr(), wtype, 'addwork');  } });
}
//外勤出差
function GoMeetFunc(element, wtype) {
    WdatePicker({ el: element,readOnly: true, dateFmt: 'HH:mm', isShowClear: false, dpTimeUp: false, onpicking: function () { if ($dp.cal.getDateStr() != $dp.cal.getNewDateStr()) { travel($dp.cal.getNewDateStr(), wtype, 'gomeet',''); } }, Hchanging: function () { if ($dp.cal.getDateStr() != $dp.cal.getNewDateStr()) { travel($dp.cal.getNewDateStr(), wtype, 'gomeet',''); } }, mchanging: function () { if ($dp.cal.getDateStr() != $dp.cal.getNewDateStr()) { travel($dp.cal.getNewDateStr(), wtype, 'gomeet',''); } }, onpicked: function () { travel($dp.cal.getNewDateStr(), wtype, 'gomeet',''); } });
}
// Firefox, Google Chrome, Opera, Safari, Internet Explorer from version 9
function OnInput(event) {
    alert("The new content: " + event.target.value);
}
// Internet Explorer
function OnPropChanged(event) {
    if (event.propertyName.toLowerCase() == "value") {
        alert("The new content: " + event.srcElement.value);
    }
}
//自定义控制日期
function ControlDate(startID) {
    var start = $("#ctl00_ContentPlaceHolder1_" + startID).val();
    var end = addDate(start, 1);
    end = end + " 06:00:00";
    return end;
}
//增加days天数，默认增加1天
function addDate(date,days){ 
var d=new Date(date); 
d.setDate(d.getDate()+days); 
var m=d.getMonth()+1; 
return d.getFullYear() + '-' + getFormatDate(m) + '-' + getFormatDate(d.getDate());
}
// 日期月份/天的显示，如果是1位数，则在前面加上'0'
function getFormatDate(arg) {
    if (arg == undefined || arg == '') {
        return '';
    }

    var re = arg + '';
    if (re.length < 2) {
        re = '0' + re;
    }

    return re;
}