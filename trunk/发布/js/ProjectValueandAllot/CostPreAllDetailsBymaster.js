﻿$(function () {
    var tempRandom = Math.random() + new Date().getMilliseconds();
    var tdLenght = $("#ctl00_ContentPlaceHolder1_grid_Financial tr:eq(0)").find("th").length;
    var hiddenUnitID = $("#ctl00_ContentPlaceHolder1_hiddenUnit").val();
    var unitid = $("#ctl00_ContentPlaceHolder1_drp_unitType").val();
    var data = "action=unittypeall&unitid=" + unitid;
    var starttime = $("#ctl00_ContentPlaceHolder1_txt_costdate1").val();
    var endtime = $("#ctl00_ContentPlaceHolder1_txt_costdate2").val();
    var dataValue = "";

    if (tdLenght == 2) {
        var tdName = $("#ctl00_ContentPlaceHolder1_grid_Financial tr:eq(0)").find("th").eq(1).text();
        //alert(tdName);
        dataValue = "action=unittypevalue&unitid=" + unitid + "&unit=" + tdName + "&starttime=" + starttime + "&endtime=" + endtime;
    } else {
        dataValue = "action=unittypevalue&unitid=" + unitid + "&unit=-1&starttime=" + starttime + "&endtime=" + endtime;
    }

    $("#ctl00_ContentPlaceHolder1_drp_unit").empty();
    $.ajax({
        type: "post",
        dataType: "json",
        url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
        data: data,
        success: function (result) {

            if (result != null) {
                var obj = result.ds;
                var gcFzr_UnitOptionHtml = '<option value="-1">-----选择部门-----</option>';
                $.each(obj, function (i, n) {
                    gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                });
                $("#ctl00_ContentPlaceHolder1_drp_unit").html(gcFzr_UnitOptionHtml);
                $("#ctl00_ContentPlaceHolder1_drp_unit").val(hiddenUnitID);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误1!");
        }
    });
    $.ajax({
        type: "post",
        dataType: "json",
        url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
        data: dataValue,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                var shrVal = 0.00;
                var gcFzr_UnitOptionHtml = '<tr id=\"trSRA\"><td  style=\"font-weight:bold \">实际收入</td>';
                $.each(obj, function (i, n) {
                    gcFzr_UnitOptionHtml += '<td>' + (parseFloat(n.ChargeAcount) * 10000).toFixed(2) + '</td>';
                    shrVal = shrVal + parseFloat(n.ChargeAcount);
                });
                gcFzr_UnitOptionHtml += "<td>" + parseFloat(shrVal * 10000).toFixed(2) + "</td></tr>";
                var $trSHR = $(gcFzr_UnitOptionHtml)
                $trSHR.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                var trBDBL = "<tr><td style=\"font-weight:bold \">变动成本收入比</td>";
                var zcbVal1 = 0.00;
                var zcbVal2 = 0.00;
                var zcbValB = 0.00;
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var trCBALL1 = $("#trSRA").find("td").eq(i).text();
                    var trCBALL2 = $("#trBD").find("td").eq(i).text();
                    if (trCBALL1 == 0 || trCBALL1 == 0.00) {
                        trBDBL += "<td>0.00%</td>";

                    } else {
                        trBDBL += "<td>" + (parseFloat((parseFloat(trCBALL2) / parseFloat(trCBALL1) * 100))).toFixed(2) + "%</td>";

                    }
                    zcbVal2 = zcbVal2 + parseFloat(trCBALL2);
                    zcbVal1 = zcbVal1 + parseFloat(trCBALL1);
                }
                if (zcbVal1 == 0 || zcbVal1 == 0.00) {
                    zcbValB = 0.00;
                }
                else {
                    zcbValB = (parseFloat((parseFloat(zcbVal2) / parseFloat(zcbVal1)) * 100)).toFixed(2)
                }
                trBDBL += "<td>" + zcbValB + "%</td></tr>";
                var $trBDBLQ = $(trBDBL);
                $("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trBDBLQ);
                //总成本比例=总成本/总收入
                var trZCBBL = "<tr><td style=\"font-weight:bold \">总成本收入比</td>";
                var zcbValA1 = 0.00;
                var zcbValA2 = 0.00;
                var zcbValAB = 0.00;
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var trCBALL1 = $("#trSRA").find("td").eq(i).text();
                    var trCBALL2 = $("#trCBA").find("td").eq(i).text();
                    if (trCBALL1 == 0 || trCBALL1 == 0.00) {
                        trZCBBL += "<td>0.00%</td>";

                    } else {
                        trZCBBL += "<td>" + (parseFloat((parseFloat(trCBALL2) / parseFloat(trCBALL1) * 100))).toFixed(2) + "%</td>";

                    }
                    zcbValA2 = zcbValA2 + parseFloat(trCBALL2);
                    zcbValA1 = zcbValA1 + parseFloat(trCBALL1);
                }
                if (zcbValA1 == 0 || zcbValA1 == 0.00) {
                    zcbValAB = 0.00;
                }
                else {
                    zcbValAB = (parseFloat((parseFloat(zcbValA2) / parseFloat(zcbValA1)) * 100)).toFixed(2)
                }
                trZCBBL += "<td>" + zcbValAB + "%</td></tr>";
                var $trZCBBLQ = $(trZCBBL);
                $("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trZCBBLQ);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误2!");
        }
    });
    $("#ctl00_ContentPlaceHolder1_drp_unitType").change(function () {
        var unitid = $(this).val();
        var data = "action=unittypeall&unitid=" + unitid;
        $.ajax({
            type: "post",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择部门-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_unit").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误3!");
            }
        });

    })
    /*
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var unitid = $(this).val();
        var data = "action=consttypeall&unitid=" + unitid;
        $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").empty();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        }); 
    })*/

    $("#ctl00_ContentPlaceHolder1_btn_search").click(function () {
        unitType = $("#ctl00_ContentPlaceHolder1_drp_unitType").val();
        var unitid = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        if (unitType != "4") {
            if (unitid == "-1") {
                alert("请选择部门！");
                return false;
            }
        }
    });
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {
        unitType = $("#ctl00_ContentPlaceHolder1_drp_unitType").val();
        var unitid = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        if (unitType != "4") {
            if (unitid == "-1") {
                alert("请选择部门！");
                return false;
            }
        }
    });
    //合并项 style=\"font-weight:bold \"
    var trGZ = "<tr ><td>工资</td>";
    var trWXYJ = "<tr ><td>五险一金</td>";
    var trFLF = "<tr><td>福利费用</td>";
    var trFSDF = "<tr ><td>房水电费</td>";
    var trBGYDF = "<tr ><td>办公邮电费</td>";
    var trDSST = "<tr><td>电算晒图费</td>";
    var trHWZB = "<tr ><td>会务招标费</td>";
    var trCL = "<tr ><td>材料</td>";
    var trBDCB = "<tr id='distr'><td>变动成本合计1</td>";
    //根据列数，分别加上id。然后合并行需要。。
    for (var i = 1; i < parseInt(tdLenght) ; i++) {
        trGZ += "<td id=\"trGZ_" + i + "\">0.00</td>";
        trWXYJ += "<td id=\"trWXYJ_" + i + "\">0.00</td>";
        trFLF += "<td id=\"trFLF_" + i + "\">0.00</td>";
        trFSDF += "<td id=\"trFSDF_" + i + "\">0.00</td>";
        trBGYDF += "<td id=\"trBGYDF_" + i + "\">0.00</td>";
        trDSST += "<td id=\"trDSST_" + i + "\">0.00</td>";
        trHWZB += "<td id=\"trHWZB_" + i + "\">0.00</td>";
        trCL += "<td id=\"trCL_" + i + "\">0.00</td>";
        trBDCB += "<td id=\"trBDCB_" + i + "\">0.00</td>";
    }
    trGZ += "</tr>";
    trWXYJ += "</tr>";
    trFLF += "</tr>";
    trFSDF += "</tr>";
    trBGYDF += "</tr>";
    trDSST += "</tr>";
    trHWZB += "</tr>";
    trCL += "</tr>";
    trBDCB += "</tr>"
    var gzlen = 0;
    var $trGZQ = $(trGZ);
    var $trWXYJQ = $(trWXYJ);
    var $trFLFQ = $(trFLF);
    var $trFSDFQ = $(trFSDF);
    var $trBGYDFQ = $(trBGYDF);
    var $trDSSTQ = $(trDSST);
    var $trHWZBQ = $(trHWZB);
    var $trCLQ = $(trCL);
    var $trBDCBQ = $(trBDCB);
    //$("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trGZQ);
    //$("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trWXYJQ);
    //$("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trFLFQ);
    //$("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trFSDFQ);
    //$("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trBGYDFQ);
    //$("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trDSSTQ);
    //$("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trHWZBQ);
    //把定义的行加到表格上。
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trBDCBQ);
    $trCLQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    $trHWZBQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    $trDSSTQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    $trBGYDFQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    $trFSDFQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    $trFLFQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    $trWXYJQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    $trGZQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");
    //遍历行。合并部门行
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:gt(0)").each(function (i) {
        var $tr = $(this);
        var tdValue = $tr.find("td").eq(0).text();
        //防止重复读取，加条件排除
        if (tdValue != "工资" && tdValue != "五险一金" && tdValue != "福利费用" && tdValue != "房水电费" && tdValue != "办公邮电费" && tdValue != "电算晒图费" && tdValue != "会务招标费" && tdValue != "材料" && tdValue != "变动成本合计1") {
            if (tdValue.indexOf("工资") > 0) {
                $(this).remove();
                //$tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trGZ_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trGZ_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else if (tdValue.indexOf("老金") > 0 || tdValue.indexOf("积金") > 0 || tdValue.indexOf("保费") > 0) {

                $(this).remove();
                //$tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trWXYJ_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trWXYJ_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else if (tdValue.indexOf("房费") == 0 || tdValue.indexOf("水费") == 0 || tdValue.indexOf("电费") == 0 || tdValue.indexOf("物业费") == 0) {

                $(this).remove();
                //$tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trFSDF_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trFSDF_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else if (tdValue.indexOf("暖气费补贴") == 0 || tdValue.indexOf("福利费") == 0) {

                $(this).remove();
                //$tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trFLF_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trFLF_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else if (tdValue.indexOf("办公费") == 0 || tdValue.indexOf("邮电费") == 0) {

                $(this).remove();
                //$tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trBGYDF_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trBGYDF_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else if (tdValue.indexOf("电算制图费") == 0 || tdValue.indexOf("晒图费") == 0) {

                $(this).remove();
                //$tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trDSST_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trDSST_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else if (tdValue.indexOf("培训费") == 0 || tdValue.indexOf("招标及咨询费") == 0 || tdValue.indexOf("会务费") == 0) {

                $(this).remove();
                //$tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trHWZB_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trHWZB_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else if (tdValue.indexOf("折旧费") == 0) {
                $(this).remove();
            }
            else {
                //alert($cloneFY.find("td").eq(2).text());
                //$(this).remove();
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trBDCB_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    $("#trBDCB_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
        }

    });

    //固定成本合计
    var trGDCBHJ = "<tr id='trGD'><td  style=\"font-weight:bold \">固定成本合计</td>";
    for (var i = 1; i < parseInt(tdLenght) ; i++) {

        var textvalHJ = parseFloat($("#trGZ_" + i + "").text()) + parseFloat($("#trWXYJ_" + i + "").text()) + parseFloat($("#trFLF_" + i + "").text()) + parseFloat($("#trFSDF_" + i + "").text());
        trGDCBHJ += "<td>" + textvalHJ.toFixed(2) + "</td>"
    }
    trGDCBHJ += "</tr>";
    var $trGDCBHJQ = $(trGDCBHJ);
    $trGDCBHJQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(4)");
    //变动成本合计2（多变一）
    var trBDCBHJ2 = "<tr><td colspan='2'  style=\"font-weight:bold \">变动成本合计2</td>";
    for (var i = 1; i < parseInt(tdLenght) ; i++) {

        var textvalHJ = parseFloat($("#trBGYDF_" + i + "").text()) + parseFloat($("#trDSST_" + i + "").text()) + parseFloat($("#trHWZB_" + i + "").text()) + parseFloat($("#trCL_" + i + "").text());
        trBDCBHJ2 += "<td>" + textvalHJ.toFixed(2) + "</td>"
    }
    trBDCBHJ2 += "</tr>";
    var $trBDCBHJQ2 = $(trBDCBHJ2);
    var trBFCBHJALL = "<tr id='trBD'><td  style=\"font-weight:bold \">变动成本合计</td>";
    for (var i = 1; i < parseInt(tdLenght) ; i++) {
        var valtrHJ2 = $trBDCBHJQ2.find("td").eq(i).text();
        var valueHJ = parseFloat(valtrHJ2) + parseFloat($("#trBDCB_" + i + "").text());
        trBFCBHJALL += "<td>" + valueHJ.toFixed(2) + "</td>";
    }
    trBFCBHJALL += "</tr>";
    var $trBFCBHJALLQ = $(trBFCBHJALL);
    $("#distr").remove();
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trBFCBHJALLQ);
    //总成本费用
    var trCBALL = "<tr id='trCBA'><td style=\"font-weight:bold \">总成本合计</td>";
    for (var i = 1; i < parseInt(tdLenght) ; i++) {
        var trCBALL1 = $("#trBD").find("td").eq(i).text();
        var trCBALL2 = $("#trGD").find("td").eq(i).text();
        trCBALL += "<td>" + (parseFloat(trCBALL1) + parseFloat(trCBALL2)).toFixed(2) + "</td>"
    }
    trCBALL += "</tr>";
    var $trCBALLQ = $(trCBALL);
    $trCBALLQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
    //变动成本比例 =变动成本/总成本

    //表头加上合计
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)").append("<th>合计</th>");
    //循环加上最后一列的合计
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:gt(0)").each(function (i) {
        //var sedd = $(this).find("td").eq(0).text();
        //alert(sedd);
        if ($(this).find("td").eq(0).text().indexOf("总成本收入比") == 0 || $(this).find("td").eq(0).text().indexOf("变动成本收入比") == 0) {
            var valInt = 0.00;
            for (var i = 1; i < parseInt(tdLenght) ; i++) {
                var valInt = valInt + parseFloat($(this).find("td").eq(i).text());
            }
            $(this).append("<td>" + valInt.toFixed(2) + "%</td>");
        } else {
            var valInt = 0.00;
            for (var i = 1; i < parseInt(tdLenght) ; i++) {
                var valInt = valInt + parseFloat($(this).find("td").eq(i).text());
            }
            $(this).append("<td>" + valInt.toFixed(2) + "</td>");
        }
    });


});