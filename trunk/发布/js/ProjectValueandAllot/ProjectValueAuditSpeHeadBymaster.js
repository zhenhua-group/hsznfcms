﻿
$(function () {

    CommonControl.SetFormWidth();

    $("#tabsMem").tabs({ cookie: { expires: 30 } });
    $("#tabsMemAmount").tabs({ cookie: { expires: 30 } });

    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();


    //设置状态
    setUnitStatus();

    //审核通过按钮
    $("#btnApproval").click(function () {

        var msg = "";
        if (!ValidationMem()) {
            return false;
        }

        var finalDataObj = {
            "ProNo": $("#hid_proID").val(),
            "SysNo": $("#HiddenAuditRecordSysNo").val(),
            "ValueByMember": new Array(),
            "AuditUser": $("#hid_user").val(),
            "Status": "A"
        };

        // 取得
        $.each($("table[memMoney='gvProjectValueBymember'] tr"), function (index, tr) {
            finalDataObj.ValueByMember[index] =
            {
                "mem_ID": $.trim($(tr).children("td:eq(1)").text()),
                "DesignPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                "DesignCount": $.trim($(tr).children("td:eq(4)").text()),
                "SpecialtyHeadPercent": $.trim($(tr).children("td:eq(6)").find("input").val()),
                "SpecialtyHeadCount": $.trim($(tr).children("td:eq(7)").text()),
                "AuditPercent": $.trim($(tr).children("td:eq(9)").find("input").val()),
                "AuditCount": $.trim($(tr).children("td:eq(10)").text()),
                "ProofreadPercent": $.trim($(tr).children("td:eq(12)").find("input").val()),
                "ProofreadCount": $.trim($(tr).children("td:eq(13)").text()),
                "ItemType": $.trim($(tr).children("td:eq(15)").text()),
                "IsHead": $.trim($(tr).children("td:eq(3)").find("input").attr("sz")),
                "IsExternal": $.trim($(tr).children("td:eq(1)").attr("wp")),
                "SpeName": $.trim($(tr).children("td:eq(3)").find("input").attr("zy"))
            };
        });

        if (!confirm("是否通过审核信息?")) {
            return false;
        }

        $("#btnApproval").attr("disabled", true);
        var jsonObj = Global.toJSON(finalDataObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/ProjectValueandAllotHandler.ashx", { "Action": 10, "data": jsonObj, "flag": 1, "msgID": $("#msgno").val(), "allotID": $("#hid_AllotID").val(), "proType": $("#hid_proType").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //消息
                    alert("专业负责人再次审批通过，消息发给对应人员进行个人产值确认！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });

    });

    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {
        if (!confirm("是否不通过审核信息?")) {
            return false;
        }
        $("#btnRefuse").attr("disabled", true);
        Global.SendRequest("/HttpHandler/ProjectValueandAllot/ProjectValueandAllotHandler.ashx", { "Action": 11, "flag": 1, "msgID": $("#msgno").val(), "allotID": $("#hid_AllotID").val(), "pro_id": $("#hid_proID").val(), "mem_id": $("#hid_user").val(), "sys_no": $("#HiddenAuditRecordSysNo").val(), "proType": $("#hid_proType").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //消息
                    alert("专业负责人再次审批不通过！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });
    });


    //删除用户A标签点击事件
    $("#resultUserRemoveA").live("click", function () {
        $(this).parent().parent().parent().remove();
    });

    var isRounding = $("#HiddenIsRounding").val();
    //重新选择用户事情

    $("#txtChooseUser").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputvalue = $(this).val();
        var total = $(this).parent().next().next().text();

        var proresultvalue = parseFloat(inputvalue) * parseFloat(total) / 100;

        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }
    });
});




//设置状态
function setUnitStatus() {
    var isDone = $("#hid_IsDone").val();
    if ($.trim(isDone) == "D") {

        $("#btnApproval").hide();
        $("#btnRefuse").hide();
    }
}


//验证人员
function ValidationMem() {

    var flag = true;

    var cprProcess = $("#HiddenCoperationProcess").val();

    if (cprProcess == 0 || cprProcess == 1) {
        var valueemputy = "0";
        $.each($("input", "#gvProjectValueOneBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputy = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputy).length == 0) {
            alert("方案设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcount = 0;
        var isCheck = false;
        $("input", "#gvProjectValueOneBymember tr td:nth-child(4)").each(function () {
            designcount += parseFloat($(this).val());
            isCheck = true;
        });

        if (designcount.toFixed(0) != 100 && isCheck == true) {
            alert("方案设计-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcount = 0;
        var isheadcount = false;
        $("input", "#gvProjectValueOneBymember  tr td:nth-child(7)").each(function () {
            headcount += parseFloat($(this).val());
            isheadcount = true;
        });

        if (headcount.toFixed(0) != 100 && isheadcount == true) {
            alert("方案设计-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcount = 0;
        var isauditcount = false;
        $("input", "#gvProjectValueOneBymember  tr td:nth-child(10)").each(function () {
            auditcount += parseFloat($(this).val());
            isauditcount = true;
        });


        if (auditcount.toFixed(0) != 100 && isauditcount == true) {
            alert("方案设计-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcount = 0;
        var isproofreadcountCheck = false;
        $("input", "#gvProjectValueOneBymember  tr td:nth-child(13)").each(function () {
            proofreadcount += parseFloat($(this).val());
            isproofreadcountCheck = true;
        });

        if (proofreadcount.toFixed(0) != 100 && isproofreadcountCheck == true) {
            alert("方案设计-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }

    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 3) {
        var valueemputyTwo = "0";
        $.each($("input", "#gvProjectValueTwoBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyTwo = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyTwo).length == 0) {
            alert("初步设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountTwo = 0;
        var isCheckTwo = false;
        $("input", "#gvProjectValueTwoBymember tr td:nth-child(4)").each(function () {
            designcountTwo += parseFloat($(this).val());
            isCheckTwo = true;
        });

        if (designcountTwo.toFixed(0) != 100 && isCheckTwo == true) {
            alert("初步设计-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountTwo = 0;
        var isheadcountTwo = false;
        $("input", "#gvProjectValueTwoBymember  tr td:nth-child(7)").each(function () {
            headcountTwo += parseFloat($(this).val());
            isheadcountTwo = true;
        });


        if (headcountTwo.toFixed(0) != 100 && isheadcountTwo == true) {
            alert("初步设计-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountTwo = 0;
        var isauditcountTwo = false;
        $("input", "#gvProjectValueTwoBymember  tr td:nth-child(10)").each(function () {
            auditcountTwo += parseFloat($(this).val());
            isauditcountTwo = true;
        });

        if (auditcountTwo.toFixed(0) != 100 && isauditcountTwo == true) {
            alert("初步设计-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountTwo = 0;
        var isproofreadcountCheckTwo = false;
        $("input", "#gvProjectValueTwoBymember  tr td:nth-child(13)").each(function () {
            proofreadcountTwo += parseFloat($(this).val());
            isproofreadcountCheckTwo = true;
        });


        if (proofreadcountTwo.toFixed(0) != 100 && isproofreadcountCheckTwo == true) {
            alert("初步设计-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }

    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3) {
        var valueemputyThree = "0";
        $.each($("input", "#gvProjectValueThreeBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyThree = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyThree).length == 0) {
            alert("施工图设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountThree = 0;
        var isCheckThree = false;
        $("input", "#gvProjectValueThreeBymember tr td:nth-child(4)").each(function () {
            designcountThree += parseFloat($(this).val());
            isCheckThree = true;
        });


        if (designcountThree.toFixed(0) != 100 && isCheckThree == true) {
            alert("施工图设计-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountThree = 0;
        var isheadcountThree = false;
        $("input", "#gvProjectValueThreeBymember  tr td:nth-child(7)").each(function () {
            headcountThree += parseFloat($(this).val());
            isheadcountThree = true;
        });


        if (headcountThree.toFixed(0) != 100 && isheadcountThree == true) {
            alert("施工图设计-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountThree = 0;
        var isauditcountThree = false;
        $("input", "#gvProjectValueThreeBymember  tr td:nth-child(10)").each(function () {
            auditcountThree += parseFloat($(this).val());
            isauditcountThree = true;
        });

        if (auditcountThree.toFixed(0) != 100 && isauditcountThree == true) {
            alert("施工图设计-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountThree = 0;
        var isproofreadcountCheckThree = false;
        $("input", "#gvProjectValueThreeBymember  tr td:nth-child(13)").each(function () {
            proofreadcountThree += parseFloat($(this).val());
            isproofreadcountCheckThree = true;
        });

        if (proofreadcountThree.toFixed(0) != 100 && isproofreadcountCheckThree == true) {
            alert("施工图设计-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3) {
        var valueemputyFour = "0";
        $.each($("input", "#gvProjectValueFourBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyFour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyFour).length == 0) {
            alert("后期服务--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountFour = 0;
        var isCheckFour = false;
        $("input", "#gvProjectValueFourBymember tr td:nth-child(4)").each(function () {
            designcountFour += parseFloat($(this).val());
            isCheckFour = true;
        });


        if (designcountFour.toFixed(0) != 100 && isCheckFour == true) {
            alert("后期服务-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountFour = 0;
        var isheadcountFour = false;
        $("input", "#gvProjectValueFourBymember  tr td:nth-child(7)").each(function () {
            headcountFour += parseFloat($(this).val());
            isheadcountFour = true;
        });


        if (headcountFour.toFixed(0) != 100 && isheadcountFour == true) {
            alert("后期服务-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountFour = 0;
        var isauditcountFour = false;
        $("input", "#gvProjectValueFourBymember  tr td:nth-child(10)").each(function () {
            auditcountFour += parseFloat($(this).val());
            isauditcountFour = true;
        });

        if (auditcountFour.toFixed(0) != 100 && isauditcountFour == true) {
            alert("后期服务-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountFour = 0;
        var isproofreadcountCheckFour = false;
        $("input", "#gvProjectValueFourBymember  tr td:nth-child(13)").each(function () {
            proofreadcountFour += parseFloat($(this).val());
            isproofreadcountCheckFour = true;
        });


        if (proofreadcountFour.toFixed(0) != 100 && isproofreadcountCheckFour == true) {
            alert("后期服务-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }

    if (cprProcess > 3) {

        var valueemputyFour = "0";
        $.each($("input", "#gvProjectValueBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyFour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyFour).length == 0) {
            alert("项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountFour = 0;
        var isCheckFour = false;
        $("input", "#gvProjectValueBymember tr td:nth-child(4)").each(function () {
            designcountFour += parseFloat($(this).val());
            isCheckFour = true;
        });



        if (designcountFour.toFixed(0) != 100 && isCheckFour == true) {
            alert("设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountFour = 0;
        var isheadcountFour = false;
        $("input", "#gvProjectValueBymember  tr td:nth-child(7)").each(function () {
            headcountFour += parseFloat($(this).val());
            isheadcountFour = true;
        });


        if (headcountFour.toFixed(0) != 100 && isheadcountFour == true) {
            alert("专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountFour = 0;
        var isauditcountFour = false;
        $("input", "#gvProjectValueBymember  tr td:nth-child(10)").each(function () {
            auditcountFour += parseFloat($(this).val());
            isauditcountFour = true;
        });

        if (auditcountFour == 0 && isauditcountFour == true) {
            alert("审批人产值分配比例不能等于0%");
            flag = false;
            return false;
        }

        if (auditcountFour.toFixed(0) != 100 && isauditcountFour == true) {
            alert("审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountFour = 0;
        var isproofreadcountCheckFour = false;
        $("input", "#gvProjectValueBymember  tr td:nth-child(13)").each(function () {
            proofreadcountFour += parseFloat($(this).val());
            isproofreadcountCheckFour = true;
        });

        if (proofreadcountFour.toFixed(0) != 100 && isproofreadcountCheckFour == true) {
            alert("校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }
    return flag;
}

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}