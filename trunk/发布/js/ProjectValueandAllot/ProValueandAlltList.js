﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //隔行变色
    $("#gvproAlloting tr:odd").css({ background: "#f0f0f0" });
    //隔行变色
    $("#gvproAlloted tr:odd").css({ background: "#f0f0f0" });
    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });

    ChangedBackgroundColor();
    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    // Tabs
    $('#tabs').tabs({ cookie: { expires: 30 } });
    //查看
    $('.check').live("click", function () {
        $('#ShowproInfo').dialog('open');
        $('.ui-widget-overlay').css("display", "block");

        var cpr_id = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/ShowProHandler.ashx",
            data: "cpr_id=" + cpr_id,
            success: function (msg) {
                var sat_arry = msg.split("|");
                $("#txt_name").text(sat_arry[0]);
                $("#txt_projectNo").html(sat_arry[1]);
                $("#txtrank").text(sat_arry[2]);
                $("#txtsrc").text(sat_arry[3]);
                $("#txtbuildUnit").text(sat_arry[4]);
                $("#txtbuildAddress").text(sat_arry[5]);
                $("#txt_Aperson").text(sat_arry[6]);
                $("#txt_startdate").text(sat_arry[7]);
                $("#txt_reletive").text(sat_arry[8]);
                $("#txttype").text(sat_arry[9]);
                $("#txtjd").text(sat_arry[10]);
                $("#txtproAcount").text(sat_arry[11]);
                $("#txt_unit").text(sat_arry[12]);
                $("#txtscale").text(sat_arry[13]);
                $("#txt_phone").text(sat_arry[14]);
                $("#txt_finishdate").text(sat_arry[15]);
                $("#txt_remark").text(sat_arry[16]);
            },
            eror: function (XMLHttpRequest, textStatus, errorThrowm) {
                alert("错误");
            }
        });

        return false;
    });

    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    //发起审核按钮
    $("span[id=InitiatedAudit]").live("click", function () {
        //获取合同SysNo
        var proSysNo = $(this).attr("proSysNo");
        var proname = $(this).attr("proname");
        var year = $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val();
        var objData =
		{
		    ProSysNo: proSysNo,
		    InUser: userSysNo,
		    SecondValue: "1"
		};
        var jsonData = Global.toJSON(objData);
        $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "IsPlaned", "proID": proSysNo }, function (jsonResult) {
            if (jsonResult == "1") {
                window.location.href = "AddProjectValueAllotBymaster.aspx?proid=" + proSysNo + "&ValueAllotAuditSysNo=0&year=" + year + "";
                //} else if (jsonResult == "2") {
                //    alert("项目:" + proname + "’设计编号重复，请修改设计编号！");
            }
            else {
                alert("项目:" + proname + "’：请添加策划信息！");
            }
        });
        //$.post("/HttpHandler/ProjectValueandAllot/ProjectValueandAllotHandler.ashx", { "Action": 0, "data": jsonData }, function (jsonResult) {
        //    if (parseInt(jsonResult) > 0) {
        //        CommonControl.GetSysMsgCount("AddProjectValueAllotBymaster.aspx?proid=" + proSysNo + "&ValueAllotAuditSysNo=" + jsonResult + "");
        //    } else {
        //        alert("此合同无需再次申请或者服务器错误！请联系系统管理员！");
        //    }
        //});
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_cprName"));
    autoComplete.GetAutoAJAX();
    //提示

})