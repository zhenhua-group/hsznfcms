﻿//未分配
function AllotList() {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx?action=sel',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '项目名称', '承接部门', '建筑类别', '合同信息', '转经济所产值(元)', '分配'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'CoperationSysNo', index: 'CoperationSysNo', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center' },
                             { name: 'Project_reletive', index: 'Project_reletive', width: 200, align: 'center', formatter: colStrShowFormatter },
                             { name: 'TotalCount', index: 'TotalCount', width: 80, align: 'center' },
                             { name: 'AllotID', index: 'AllotID', width: 80, align: 'center', formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "type": "jjs", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        caption: "转经济所未分配的项目",
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx?action=sel",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        // var unit = $("#drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '">' + celvalue + '</a>';

}
//静态状态
function colStrShowFormatter(celvalue, options, rowData) {
    var str = "/Coperation/cpr_ShowCoprationBymaster.aspx?flag=projlist1&cprid=" + rowData["CoperationSysNo"];
    return '<img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px; border: none;" /><a href="' + str + '" >' + celvalue + '</a>';
}
//状态
//function colStatusShowFormatter(celvalue, options, rowData) {
//    var str = "";
//    if (celvalue == "1") {
//        str = '<a href="###"  class="process">分配中</a>';
//    }
//    else if (celvalue == "2") {
//        str = '<a href="###" class="done">已分配</a>';
//    }
//    else {
//        str = '<a href="###" rel=' + celvalue + ' class="apply" runat="server">提交申请</a>';
//    }
//    return str;
//}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectValueandAllot/TranjjsProjectValueAllotBymaster.aspx?proid=" + rowData["pro_ID"] + "&totalCount=" + rowData["TotalCount"] + "&allotID=" + celvalue;
    return '<a href="' + pageurl + '" class="cls_chk" style="height: 26px;">分配</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}


//已分配完成
function AllotList2() {
    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '项目名称', '承接部门', '建筑类别', '合同信息', '转经济所产值(元)','审批状态', '查看'],
        colModel: [
                              { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'CoperationSysNo', index: 'CoperationSysNo', hidden: true, editable: true },
                             { name: 'AuditSysNo', index: 'AuditSysNo', hidden: true, editable: true },
                             { name: 'Status', index: 'Status', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter2 },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center' },
                             { name: 'Project_reletive', index: 'Project_reletive', width: 200, align: 'center', formatter: colStrShowFormatter2 },
                             { name: 'TotalCount', index: 'TotalCount', width: 120, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 120, align: 'center', formatter: colStatusShowFormatterPersent },
                             { name: 'AllotID', index: 'AllotID', width: 100, align: 'center', formatter: colShowFormatter2 }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "sel2", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where2").val()), "type": "jjs", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager2",
        viewrecords: true,
        caption: "转经济所分配完毕项目",
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod2,
        loadComplete: loadCompMethod2
    });


    //显示查询
    $("#jqGrid2").jqGrid("navGrid", "#gridpager2", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid2").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid2').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid2').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where2").val());
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter2(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["Pro_id"];
    return '<a href="' + pageurl + '">' + celvalue + '</a>';
}
//静态状态
function colStrShowFormatter2(celvalue, options, rowData) {
    var str = "/Coperation/cpr_ShowCoprationBymaster.aspx?flag=projlist1&cprid=" + rowData["CoperationSysNo"];
    return '<img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px; border: none;" /><a href="' + str + '" >' + celvalue + '</a>';
}

//静态状态
function colStatusShowFormatterPersent(celvalue, options, rowData) {
    var percent = 0;
    var count = 3;
    switch (rowData["Status"]) {
        case "A":
            percent = (100 / count) * 1;
            break;
        case "B":
        case "C":
            percent = (100 / count) * 2;
            break;
        case "D":
        case "E":
            percent = 100;
            break;
    }
    percent = parseFloat(percent).toFixed(2);
    var pageurl = '<div class="progressbar" rel="' + percent + '" title="' + percent + '%" id="auditLocusContainer" action="TranJJS" referencesysno="' + rowData["AuditSysNo"] + '" style="cursor: pointer;height:20px;margin:1px 1px;"></div>';

    return pageurl;
}

//查看
function colShowFormatter2(celvalue, options, rowData) {
    var pageurl = "/ProjectValueandAllot/TranjjsProjectValueAllotAuditBymaster.aspx?proid=" + rowData["pro_ID"] + "&auditSysID=" + rowData["AuditSysNo"] + "&totalCount=" + rowData["TotalCount"] + "&type=tran&allotID=" + celvalue;
    var linkAction = "";
    switch (rowData["Status"]) {
        case "A":
            linkAction = '<a href="' + pageurl + '" class="cls_chk" style="height: 26px;">查看</a>| <a href="###" class="cls_delteTranJjs" rel="' + celvalue + '">删除</a>';
            break;
        case "B":
        case "C":
            linkAction = '<a href="' + pageurl + '" class="cls_chk" style="height: 26px;">查看</a>';
            break;
        case "D":
            linkAction = '<a href="' + pageurl + '" class="cls_chk" style="height: 26px;">评审结果</a>';
            break;
        case "E":
            linkAction = '<a href="' + pageurl + '" class="cls_chk" style="height: 26px;">查看</a>| <a href="###" class="cls_delteTranJjs" rel="' + celvalue + '">删除</a>';
            break;
    }
    return linkAction;
}

//统计 
function completeMethod2() {
    var rowIds = $("#jqGrid2").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid2 [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));

        var percent = parseFloat($("#jqGrid2 [id=" + rowIds[i] + "]").children("td").children(".progressbar").attr("rel"));

        $("#jqGrid2 [id=" + rowIds[i] + "]").children("td").children(".progressbar").progressbar({
            value: percent
        });
        var status = $("#jqGrid2 [id=" + rowIds[i] + "]").children("td:eq(5)").text();
        if (status == "C" || status == "E") {
            $("#jqGrid2 [id=" + rowIds[i] + "]").children("td").children(".progressbar").children().css({ background: "Red" });
        }
    }
}
//无数据
function loadCompMethod2() {
    $("#jqGrid2").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid2").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata2").text() == '') {
            $("#jqGrid2").parent().append("<div id='nodata2'>没有符合条件数据！</div>")
        }
        else { $("#nodata2").show(); }

    }
    else {
        $("#nodata2").hide();
    }
}