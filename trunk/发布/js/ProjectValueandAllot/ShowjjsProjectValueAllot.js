﻿var actionFlag;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;

//返回消息列表
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;

$(function () {

    CommonControl.SetFormWidth();


    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();

    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();
    var isRounding = $("#HiddenIsRounding").val();
    var status = $("#HiddenStatus").val();


    var oneSuggstion = $("#HiddenOneSuggesiton").val();
    if (oneSuggstion && oneSuggstion.length > 0) {
        $("#OneSuggstion").val(oneSuggstion).attr("disabled", true).css("color", "black");
        $("#AuditUser", $("#OneTable")).html($("#HiddenAuditUser").val() + " <br/> " + $("#HiddenAuditData").val());
        $("#fieldAudit").show();
        $("#tbTopAudit").show();
        $("#OneTable").show();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }



    if ($("#HiddenItemType").val() == "25") {
        $("#tbInfo tr:eq(0)").show();
    }
    else {
        $("#tbInfo tr:eq(0)").hide();
    }


    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();

    if (messageStauts == status || messageStauts == "") {
        InitViewStateTemp(status);
    } else {

        InitViewMessageState(messageStauts, status);
    }

    //判断是否删除
    if ($("#tbdelete tr td").text() == "此信息已被删除!") {
        $("#fieldset").children().hide();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }

    //审核通过按钮
    $("#btnPass").click(function () {

        if (status == "A") {

            var msg = "";
            if (!Validation()) {
                return false;
            }

            var _jzTotalPercent = 0;
            var _jzTotalCount = 0;
            var _azTotalPercent = 0;
            var _azTotalCount = 0;
            //设计 --建筑
            var _jzCount = 0;
            var _jzPercent = 0;
            //设计 --结构
            var _jgPercent = 0;
            var _jgCount = 0;
            //设计 --给排水
            var _gpsPercent = 0;
            var _gpsCount = 0;
            //设计 --暖通
            var _ntPercent = 0;
            var _ntCount = 0;
            //设计 --电气
            var _dqPercent = 0;
            var _dqCount = 0;
            //取得校对金额
            var prooPercent = 0;
            var prooAmount = 0;

            var stage = $("#HiddenItemType").val();

            if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {

                _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
                _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();
                _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");
                _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();

                //设计 --建筑
                _jzCount = 0;
                _jzPercent = 0;
                //设计 --结构
                _jgPercent = 0;
                _jgCount = 0;

                //设计 --给排水
                _gpsPercent = 0;
                _gpsCount = 0;

                //设计 --暖通
                _ntPercent = 0;
                _ntCount = 0;

                //设计 --电气
                _dqPercent = 0;
                _dqCount = 0;

                //取得校对金额
                prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(4)").text().replace("%", "");
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();
            }
            else if (stage == "23" || stage == "24" || stage == "25") {

                _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
                _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();

                _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text().replace("%", "");
                _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text();

                //设计 --建筑
                _jzPercent = $("#txtBulidging").val();
                _jzCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
                //设计 --结构
                _jgPercent = $("#txtStructure").val();
                _jgCount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();

                //设计 --给排水
                _gpsPercent = $("#txtDrain").val();
                _gpsCount = $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text();

                //设计 --暖通
                _ntPercent = $("#txtHavc").val();
                _ntCount = $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text();

                //设计 --电气
                _dqPercent = $("#txtElectric").val();
                _dqCount = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

                //取得校对金额
                prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(9)").text().replace("%", "");
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
            }
            else {

                _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
                _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();

                _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text().replace("%", "");
                _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text();

                //设计 --建筑
                _jzCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
                _jzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");
                //设计 --结构
                _jgPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(4)").text().replace("%", "");
                _jgCount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();

                //设计 --给排水
                _gpsPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(6)").text().replace("%", "");
                _gpsCount = $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text();

                //设计 --暖通
                _ntPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(7)").text().replace("%", "");
                _ntCount = $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text();

                //设计 --电气
                _dqPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(8)").text().replace("%", "");
                _dqCount = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

                //取得校对金额
                prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(9)").text().replace("%", "");
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
            }

            var finalDataObj = {
                "ProNo": $("#hidProID").val(),
                "SysNo": $("#HiddenAuditRecordSysNo").val(),
                "ValueByMember": new Array(),
                "AuditUser": $("#HiddenLoginUser").val(),
                "typeStatus": $("#HiddenItemType").val(),
                "AllotID": $("#HiddenAllot").val(),
                "ProofreadPercent": prooPercent,
                "ProofreadCount": prooAmount,
                "BuildingPercent": _jzPercent,
                "BuildingCount": _jzCount,
                "StructurePercent": _jgPercent,
                "StructureCount": _jgCount,
                "DrainPercent": _gpsPercent,
                "DrainCount": _gpsCount,
                "HavcPercent": _ntPercent,
                "HavcCount": _ntCount,
                "ElectricPercent": _dqPercent,
                "ElectricCount": _dqCount,
                "TotalBuildingPercent": _jzTotalPercent,
                "TotalBuildingCount": _jzTotalCount,
                "TotalInstallationPercent": _azTotalPercent,
                "TotalInstallationCount": _azTotalCount
            };

            // 取得
            $.each($("#gvProjectValueBymember tr"), function (index, tr) {
                finalDataObj.ValueByMember[index] =
                { "mem_ID": $.trim($(tr).children("td:eq(1)").text()),
                    "DesignPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "DesignCount": $.trim($(tr).children("td:eq(4)").text()),
                    "SpecialtyHeadPercent": 0,
                    "SpecialtyHeadCount": 0,
                    "AuditPercent": 0,
                    "AuditCount": 0,
                    "ProofreadPercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "ProofreadCount": $.trim($(tr).children("td:eq(6)").text()),
                    "ItemType": $("#HiddenItemType").val(),
                    "IsHead": 0,
                    "IsExternal": $.trim($(tr).children("td:eq(1)").attr("wp"))
                };
            });
            $("#btnPass").attr("disabled", true);
            var jsonObj = Global.toJSON(finalDataObj);
            jsonDataEntity = jsonObj;
            getUserAndUpdateAudit("2", '0', jsonDataEntity);
        }
        else if (status == 'B') {
            var auditObj = {
                SysNo: $("#HiddenAuditRecordSysNo").val(),
                AuditUser: $("#HiddenLoginUser").val(),
                Status: "D",
                ProSysNo: $("#hidProID").val(),
                AllotID: $("#HiddenAllot").val()
            };

            if ($.trim($("#OneSuggstion").val()).length == 0) {
                alert("生产经营部意见不能为空！");
                return false;
            }
            auditObj.OneSuggestion = $.trim($("#OneSuggstion").val());
            $("#btnPass").attr("disabled", true);
            var jsonObj = Global.toJSON(auditObj);

            Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 3, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
                if (jsonResult == "0") {
                    alert("审核失败！");
                } else {
                    if (jsonResult == "1") {
                        //改变消息状态
                        var msg = new MessageCommProjAllot($("#msgno").val());
                        msg.ReadMsg();
                        //消息
                        alert("分配通过成功，消息已发送给申请人！");
                    }
                    //查询系统新消息
                    window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

                    // window.history.back();
                }
            });
        }
    });

    //不通过
    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnNotPass").click(function () {

        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            ProSysNo: $("#hidProID").val(),
            AllotID: $("#HiddenAllot").val()
        };

        if (status == "B") {
            if ($.trim($("#OneSuggstion").val()).length == 0) {
                alert("生产经营部意见不能为空！");
                return false;
            }
            auditObj.OneSuggestion = $.trim($("#OneSuggstion").val());
            auditObj.Status = "E";
        }
        else {
            auditObj.Status = "C";
        }
        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 3, "data": jsonObj, "flag": 1, "AllotUser": $("#HiddenAllotUser").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("分配不通过成功，消息已发送给申请人！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

                // window.history.back();
            }
        });

    });


    //添加人员

    var chooseUserMain = new ChooseProjectValueUserControl($("#chooseUserMain"));
    //选择用户
    $("#chooseUser").click(function () {

        var parObj = {};
        parObj.UnitName = ""
        parObj.SpeName = "预算";
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);
        $("#chooseUserMain").dialog({
            autoOpen: false,
            modal: true,
            width: 700,
            resizable: false,
            title: "添加用户",
            buttons:
			        {
			            "确定": function () {
			                //调用处理事件
			                chooseUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
			                $(this).dialog("close");
			            },
			            "取消": function () { $(this).dialog("close"); }
			        }
        }).dialog("open");
        return false;
    });

    //选择用户
    function ChooseUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        // index = parseInt($(this).parent().index());
                        // lengthTemp = $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueBymember tr").each(function () {
                    var tr = $(this);
                    $("td[class=cls_Column]", tr).each(function () {
                        var td = $(this);
                        if (item.userSpecialtyname == td.text()) {
                            index = parseInt(td.parent().index());
                            lengthTemp = td.attr("rowspan") == undefined ? 1 : td.attr("rowSpan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                        _userLength = _userLength - 1;
                    }
                });

                //不存在添加
                if (IsExisted) {

                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $("#gvProjectValueBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString += "<tr>";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"design\" sz=" + item.userPrincipalship + " spe=" + item.userSpecialtyname + " />%</td>";
                    trString += "<td width= \"10%\"></td>";
                    trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  roles=\"jd\"  />%</td>";
                    trString += "<td width= \"10%\"></td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }

            });

        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加

                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;

                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"15%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                    trString += "<td  width= \"15%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    trString += "<td width= \"17.5%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"design\" sz=" + item.userPrincipalship + " spe=" + item.userSpecialtyname + " />%</td>";
                    trString += "<td width= \"17.5%\"></td>";
                    trString += "<td width= \"17.5%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  roles=\"jd\"  />%</td>";
                    trString += "<td width= \"17.5%\"></td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueBymember").append(trString);

                }
            });
        }
    }

    //添加外聘人员

    var chooseExternalUser = new ChooseExternalUserControl($("#chooseExternalUserDiv"));
    //选择用户
    $("#chooseExternalUser").click(function () {

        var parObj = {};
        parObj.UnitName = "经济所"
        parObj.SpeName = "预算";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        $("#chooseExternalUserDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 700,
            resizable: false,
            title: "外聘人员",
            buttons:
			        {
			            "确定": function () {
			                //调用处理事件
			                chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
			                $(this).dialog("close");
			            },
			            "取消": function () { $(this).dialog("close"); }
			        }
        }).dialog("open");
        return false;
    });


    //删除用户A标签点击事件
    $("#resultUserRemoveA").live("click", function () {
        var spe = $(this).attr("spe");
        var rowspan = 0;
        $($(this).parent().parent().parent().parent().children().find("td:eq(0)")).each(function () {
            if ($(this).text() == spe) {
                rowspan = $(this).attr("rowspan");
                $(this).attr("rowspan", parseInt(rowspan) - 1);
            }
        });
        if (rowspan != 0) {
            $(this).parent().parent().parent().remove();
        }
    });

    //删除用户A标签点击事件
    $("#resultUserRemoveB").live("click", function () {
        $(this).parent().parent().parent().remove();
    });


    var bzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text().replace("%", "");
    var allotCount = $("#txt_ShouldBeValueCount").text();
    //建筑
    $("#txtBulidging").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text(total.toFixed(2));

        var otherPercent = $("#txtStructure").val().length == 0 ? 0 : parseFloat($("#txtStructure").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent);
        $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count.toFixed(2));

    });

    //结构
    $("#txtStructure").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text(total.toFixed(2));

        var otherPercent = $("#txtBulidging").val().length == 0 ? 0 : parseFloat($("#txtBulidging").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent);
        $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text(percent.toFixed(2));
        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count.toFixed(2));

    });

    //给排水
    $("#txtDrain").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text(total.toFixed(2));

        var otherPercent = $("#txtHavc").val().length == 0 ? 0 : parseFloat($("#txtHavc").val());
        var otherPercent1 = $("#txtElectric").val().length == 0 ? 0 : parseFloat($("#txtElectric").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));


    });

    //暖通
    $("#txtHavc").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text(total.toFixed(2));

        var otherPercent = $("#txtDrain").val().length == 0 ? 0 : parseFloat($("#txtDrain").val());
        var otherPercent1 = $("#txtElectric").val().length == 0 ? 0 : parseFloat($("#txtElectric").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));
        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));


    });

    //电气
    $("#txtElectric").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text(total.toFixed(2));

        var otherPercent = $("#txtDrain").val().length == 0 ? 0 : parseFloat($("#txtDrain").val());
        var otherPercent1 = $("#txtHavc").val().length == 0 ? 0 : parseFloat($("#txtHavc").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));

    });

    //取得校对金额
    var prooAmount = 0;
    var stage = $("#HiddenItemType").val();
    if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
        prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();
    } else {
        prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
    }


    //校对--人员金额计算
    $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {

        $(this).change(function () {


            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }

            var inputvalue = $(this).val();
            var proresultvalue = parseFloat(inputvalue) * parseFloat(prooAmount) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //锅炉房 
    if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
        //设计 --建筑
        // var _jz = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();
        //设计 --结构
        // var _gps = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
        var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();
                var proresultvalue = 0;
                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {
            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }
            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });

    }
    else if (stage == "23" || stage == "24" || stage == "25") {
        var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            $(this).change(function () {

                if (!ValidationProcess()) {
                    $(this).val(0);
                    return false;
                }

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var inputvalue = $(this).val();
                var proresultvalue = 0;

                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {
            if (!ValidationProcess()) {
                return false;
            }

            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }

            var inputvalue = $(this).val();
            var proresultvalue = 0;

            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });
    } else {

        //设计 --建
        var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {


            $(this).change(function () {

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var inputvalue = $(this).val();
                var proresultvalue = 0;

                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {

            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }
            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });
    }


    //校对--人员金额计算--后来添加人员
    $("#txtChooseUser[roles=jd]", "#gvProjectValueBymember tr").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var inputvalue = $(this).val();
        var proresultvalue = parseFloat(inputvalue) * parseFloat(prooAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });

    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('2', '1', jsonDataEntity);
            },
            "关闭": function () {
                $("#btnPass").attr("disabled", false);
                messageDialog.hide();
            }
        }
    });
    sendMessageClass = new MessageCommon(messageDialog);
});

//验证工序
function ValidationProcess() {
    var flag = true;

    var budingPercent = $("#txtBulidging").val();
    if (budingPercent == "") {
        alert("建筑配置比例不能为空");
        flag = false;
        return false;

    }
    var structure = $("#txtStructure").val()
    if (structure == "") {
        alert("结构配置比例不能为空");
        flag = false;
        return false;

    }
    var drain = $("#txtDrain").val();
    if (drain == "") {
        alert("给排水配置比例不能为空");
        flag = false;
        return false;

    }
    var havc = $("#txtHavc").val();
    if (havc == "") {
        alert("暖通配置比例不能为空");
        flag = false;
        return false;

    }
    var electric = $("#txtElectric").val();
    if (electric == "") {
        alert("电气配置比例不能为空");
        flag = false;
        return false;

    }

    var total = parseFloat(budingPercent) + parseFloat(structure) + parseFloat(drain) + parseFloat(havc) + parseFloat(electric);

    if (total.toFixed(0) != 100) {
        alert("配置比例没有闭合，请修改");
        flag = false;
        return false;
    }

    return flag;
}

function Validation() {

    var flag = true;

    var stage = $("#HiddenItemType").val();

    if (stage == "23" || stage == "24" || stage == "25") {

        if (!ValidationProcess()) {
            flag = false;
            return false;
        }
    }

    var trLength = $("#gvProjectValueBymember tr").length;
    if (trLength == 0) {
        alert("请添加人员");
        flag = false;
        return false;
    }

    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvProjectValueBymember tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valuespeemputy).length == 0) {
        alert("人员产值分配比例不能为空");
        flag = false;
        return false;
    }

    var designcountOne = 0;
    var isCheckOne = false;
    $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {
        designcountOne += parseFloat($(this).val());
        isCheckOne = true;
    });

    if (designcountOne.toFixed(0) != 100 && isCheckOne == true) {
        alert("人员编制比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }

    var designcountTwo = 0;
    var isCheckTwo = false;
    $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {
        designcountTwo += parseFloat($(this).val());
        isCheckTwo = true;
    });

    if (designcountTwo.toFixed(0) != 100 && isCheckTwo == true) {
        alert("人员校对比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }
    return flag;
}

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}


//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //  地址
    var url = "/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx";

    //数据         
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }
        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}

//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {

        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);

    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        alert("产值分配消息发送成功！");
        // 查询系统新消息
        // window.location.href = "/Coperation/cpr_SysMsgListView.aspx";
        window.history.back();
    } else {
        alert("消息发送失败！");
    }
}

//初始化页面状态
function InitViewStateTemp(auditStatus) {

    switch (auditStatus) {
        case "A":

            if ($("#hiddenType").val() != "tran") {
                $("#memField").show();
            }
            else {
                $("#memField").hide();
                $("#btnPass").hide();
                $("#btnNotPass").hide();
            }
            break;
        case "B":
            if ($("#hiddenType").val() != "tran") {
                $("#memFieldShow").show();
                $("#fieldAudit").show();
            } else {
                $("#btnPass").hide();
                $("#btnNotPass").hide();
                $("#memFieldShow").show();
                $("#btnPrintValue").show();
            }
            break;
        case "C":
            $("#memField").show();
            $("#chooseUser").hide();
            $("#chooseExternalUser").hide();
            $("input", $("#memField")).attr("disabled", true);
            $("#btnPass").hide();
            $("#btnNotPass").hide();
            break;
        case "D":
            $("#memFieldShow").show();
            $("#fieldAudit").show();
            $("#btnPrintValue").show();
            break;
        case "E":
            $("#memFieldShow").show();
            $("#fieldAudit").show();
            $("input", $("#fieldAudit")).attr("disabled", true);
            break;

    }
}
function InitViewMessageState(messageStatus, staus) {

    if (messageStatus == "A") {
        $("#memField").show();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }
    if (messageStatus == "A" && staus == "C") {
        $("#memField").show();
        $("#chooseUser").hide();
        $("#chooseExternalUser").hide();
        $("input", $("#memField")).attr("disabled", true);
    }
    if (messageStatus == "B" && staus != "E") {
        $("#memFieldShow").show();
        $("#fieldAudit").show();
        $("input", $("#fieldAudit")).attr("disabled", true);
    }
}