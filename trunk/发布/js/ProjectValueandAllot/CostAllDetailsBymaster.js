﻿$(function () {
    var tempRandom = Math.random() + new Date().getMilliseconds();
    var unitid = $("#ctl00_ContentPlaceHolder1_drp_unitType").val();
    var data = "action=unittypeall&unitid=" + unitid;
    var HiddenunitID = $("#ctl00_ContentPlaceHolder1_hiddenUnit").val();
    var HiddenTypeID = $("#ctl00_ContentPlaceHolder1_hiddenType").val();
    $("#ctl00_ContentPlaceHolder1_drp_unit").empty();
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
        data: data,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                var gcFzr_UnitOptionHtml = '<option value="-1">-----选择部门-----</option>';
                $.each(obj, function (i, n) {
                    gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                });
                $("#ctl00_ContentPlaceHolder1_drp_unit").html(gcFzr_UnitOptionHtml);
                $("#ctl00_ContentPlaceHolder1_drp_unit").val(HiddenunitID);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
    var unitidS = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    var datas = "action=consttypeall&unitid=" + unitidS;
    $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").empty();
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
        data: datas,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                $.each(obj, function (i, n) {
                    gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';

                });
                $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").html(gcFzr_UnitOptionHtml);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });


    $("#ctl00_ContentPlaceHolder1_drp_unitType").change(function () {
        var unitid = $(this).val();
        var data = "action=unittypeall&unitid=" + unitid;
        $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").empty();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择部门-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_unit").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });

    })
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var unitid = $(this).val();
        var data = "action=consttypeall&unitid=" + unitid;
        $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").empty();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                        
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });

    })

    $("#ctl00_ContentPlaceHolder1_btn_search").click(function () {
        unitType = $("#ctl00_ContentPlaceHolder1_drp_unitType").val();
        var unitid = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        if (unitType != "4") {
            if (unitid == "-1") {
                alert("请选择部门！");
                return false;
            }
        }
    });
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {
        unitType = $("#ctl00_ContentPlaceHolder1_drp_unitType").val();
        var unitid = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        if (unitType != "4") {
            if (unitid == "-1") {
                alert("请选择部门！");
                return false;
            }
        }
    });

    var tdLenght = $("#ctl00_ContentPlaceHolder1_grid_Financial tr:eq(1)").find("td").length;
    var trGZ = "<tr style=\"font-weight:bold \"><td>工资合计</td>";
    var trFY = "<tr style=\"font-weight:bold \"><td>费用合计</td>";
    for (var i = 1; i < parseInt(tdLenght) ; i++) {
        trGZ += "<td id=\"trGZ_" + i + "\">0.00</td>"
        trFY += "<td id=\"trFY_" + i + "\">0.00</td>"
        trHJ += "<td id=\"trFY_" + i + "\">0.00</td>"
    }
    trGZ += "</tr>";
    trFY += "</tr>";
    var gzlen = 0;
    var $trGZQ = $(trGZ);
    var $trFYQ = $(trFY);
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trFYQ);

    $trGZQ.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(" + gzlen + ")");

    //
    //循环表格的行
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:gt(0)").each(function (i) {
        var $tr = $(this);
        var tdValue = $tr.find("td").eq(0).text();
        if (tdValue != "费用合计" && tdValue != "工资合计") {
            if (tdValue.indexOf("工资") > 0) {
                //$(this).remove();
                $tr.insertAfter("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)");
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalGZ = $("#trGZ_" + i + "").text();
                    textvalGZ = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalGZ);
                    //alert($clone.find("td").eq(i).text());
                    //alert(i);
                    $("#trGZ_" + i + "").text(parseFloat(textvalGZ).toFixed(2));
                }
            }
            else {
                //alert($cloneFY.find("td").eq(2).text());
                for (var i = 1; i < parseInt(tdLenght) ; i++) {
                    var textvalFY = $("#trFY_" + i + "").text();
                    textvalFY = parseFloat($tr.find("td").eq(i).text()) + parseFloat(textvalFY);
                    $("#trFY_" + i + "").text(parseFloat(textvalFY).toFixed(2));
                }
            }
        }
    });
    var trHJ = "<tr style=\"font-weight:bold \"><td>合计</td>";
    for (var i = 1; i < parseInt(tdLenght) ; i++) {

        var textvalHJ = parseFloat($("#trGZ_" + i + "").text()) + parseFloat($("#trFY_" + i + "").text());
        trHJ += "<td>" + textvalHJ.toFixed(2) + "</td>"
    }
    trHJ += "</tr>";
    var $trHJQ = $(trHJ);
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody").append($trHJQ);
    //表头加上合计
    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:eq(0)").append("<th>合计</th>");
    //循环加上最后一列的合计

    $("#ctl00_ContentPlaceHolder1_grid_Financial tbody tr:gt(0)").each(function (i) {
        var valInt = 0.00;
        for (var i = 1; i < parseInt(tdLenght) ; i++) {

            var valInt = valInt + parseFloat($(this).find("td").eq(i).text());

        }
        $(this).append("<td>" + valInt.toFixed(2) + "</td>");
    });




})