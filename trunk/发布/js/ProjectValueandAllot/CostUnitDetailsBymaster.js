﻿$(function () {
    var tempRandom = Math.random() + new Date().getMilliseconds();
    var unitIDInit = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    $("#ctl00_ContentPlaceHolder1_drp_unit").val(unitIDInit);
    if ($("#ctl00_ContentPlaceHolder1_drp_unit").val() == null) {
        $("#ctl00_ContentPlaceHolder1_drp_unit").val("-1");
    }
    var hiddenTypeid = $("#ctl00_ContentPlaceHolder1_HiddenTypeID").val();
    unitIDInit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    var dataInit = "action=consttypeall&unitid=" + unitIDInit;
    $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").empty();
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
        data: dataInit,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                $.each(obj, function (i, n) {
                    gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                });
                $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").html(gcFzr_UnitOptionHtml);
                $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").val(hiddenTypeid);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
    //var typeid = $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").val();
    var starttime = $("#ctl00_ContentPlaceHolder1_txt_costdate1").val();
    var endtime = $("#ctl00_ContentPlaceHolder1_txt_costdate2").val();
    var unitName = $.trim($("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text());

    var dateDetails = "action=costUnit&unitid=" + unitIDInit + "&typeid=" + hiddenTypeid + "&starttime=" + starttime + "&endtime=" + endtime;
    $.ajax({
        type: "Get",
        url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
        data: dateDetails,
        success: function (result) {
            if (result != null) {
                var json = eval('(' + result + ')');
                $.each(json, function (key, valObj) {
                    var $tr = $("<tr></tr>");
                    var $td1 = $("<td style=\"width: 8%\" >" + $.trim(valObj.CostName) + "</td>");
                    var $td2 = $("<td style=\"width: 5%\">" + valObj.AllCharge.toFixed(2) + "</td>");
                    var $td3 = $("<td style=\"width: 5%\"><a href='ShowCostdetails.aspx?typeid=" + valObj.CosttypeID + "&unitid=-1&starttime=" + starttime + "&endtime=" + endtime + "'> 查看</a></td>");
                    $tr.append($td1);
                    $tr.append($td2);
                    $tr.append($td3);
                    $("#costTable").append($tr);
                })
                if (unitIDInit == "-1") {
                    $("#costTitle").text("全院部门 " + starttime + "至" + endtime + "成本费用明细统计");
                }
                else {
                    $("#costTitle").text(unitName + " " + starttime + "至" + endtime + " " + "成本费用明细统计");
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var unitid = $(this).val();
        var data = "action=consttypeall&unitid=" + unitid;
        $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").empty();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });

    })
    $("#btn_search").click(function () {
        unitIDInit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var typeid = $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").val();
        var starttime = $("#ctl00_ContentPlaceHolder1_txt_costdate1").val();
        var endtime = $("#ctl00_ContentPlaceHolder1_txt_costdate2").val();
        unitName = $.trim($("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text());
       var  typeName = $.trim($("#ctl00_ContentPlaceHolder1_drp_costTypeAll").find("option:selected").text());
        var dateDetails = "action=costUnit&unitid=" + unitIDInit + "&typeid=" + typeid + "&starttime=" + starttime + "&endtime=" + endtime;
        $("#costTable tr:gt(0)").remove();
        $.ajax({
            type: "Get",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: dateDetails,
            success: function (result) {
                if (result != null) {
                    var json = eval('(' + result + ')');
                    $.each(json, function (key, valObj) {
                        var $tr = $("<tr></tr>");
                        var $td1 = $("<td style=\"width: 8%\" >" + $.trim(valObj.CostName) + "</td>");
                        var $td2 = $("<td style=\"width: 5%\">" + valObj.AllCharge.toFixed(2) + "</td>");
                        var $td3 = $("<td style=\"width: 5%\"><a href='ShowCostdetails.aspx?typeid=" + valObj.CosttypeID + "&unitid=" + unitIDInit + "&starttime=" + starttime + "&endtime=" + endtime + "' >查看</a></td>");
                        $tr.append($td1);
                        $tr.append($td2);
                        $tr.append($td3);
                        $("#costTable").append($tr);
                    })
                    if (unitIDInit == "-1") {
                        if (typeid == "-1") {
                            $("#costTitle").text("全院部门 " + starttime + "至" + endtime + "成本费用明细统计");
                        } else {
                            $("#costTitle").text("全院部门 " + starttime + "至" + endtime + " " + typeName + "费用明细统计");
                        }
                     
                    }
                    else {
                        if (typeid == "-1") {
                            $("#costTitle").text(unitName + " " + starttime + "至" + endtime + " " + "成本费用明细统计");
                        } else {
                            $("#costTitle").text(unitName + " " + starttime + "至" + endtime + " " + typeName+"费用明细统计");
                        }
                       
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });
    })
    //var json = eval('(' + data + ')');
    //$.each(json, function (key, valObj) {
    //    var $tr = $("<tr></tr>");
    //    var $td1 = $("<td style=\"width: 8%\" >" + $.trim(valObj.Unit) + "</td>");
    //    var $td2 = $("<td style=\"width: 5%\">" + valObj.AcountName + "</td>");
    //$tr.append($td16);
    //$tr.append($td17);
    //$("#AddTable").append($tr);
});