﻿$(function () {
    var foolType = false;
    var tempRandom = Math.random() + new Date().getMilliseconds();
    $("#ctl00_ContentPlaceHolder1_drp_unitAdd").change(function () {
        var unitid = $(this).val();
        if ($(this).val() != '-1') {
            var unitname = $("#ctl00_ContentPlaceHolder1_drp_unitAdd").find("option:selected").text();
            $("#remark").val(unitname);
        }
        var data = "action=consttype&unitid=" + unitid;
        $("#ctl00_ContentPlaceHolder1_drp_costType").empty();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_costType").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });

    })
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var unitid = $(this).val();
        var data = "action=consttypeall&unitid=" + unitid;
        $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").empty();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });

    })
})