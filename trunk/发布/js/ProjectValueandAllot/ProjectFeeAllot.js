﻿$(document).ready(function() {

    //本次费率

    $("#txt_thisfee").blur(function() {
        calculate();
    });

    $("#txt_cprfee").blur(function() {
        calculate();
    });

    //税后分成基数

    $("#txt_zdfc").blur(function() {
        feecalculate();

    });

    $("#txt_shuil").blur(function() {
        feecalculate();
    });

    //最近税后分成基数元
    $("#txtBuildArea").blur(function() {
        shfcy_calculate();
    });

    //合同总价
    $("#txt_cprdan").blur(function() {
        coperation_fee();
    });
});
//计算本次费率
function calculate() {
    var fee = $("#txt_thisfee").val();
    var cprfee = $("#txt_cprfee").val();
    if (checkRate(fee) && checkRate(cprfee)) {
        var feel = fee / cprfee;
        $("#txt_thisfeil").val(feel);
    }
}
//计算税后分成基数
function feecalculate() {

    var shuil = $("#txt_shuil").val();
    var zdfc = $("#txt_zdfc").val();
    if (checkRate(shuil) && checkRate(zdfc)) {
        var feel = shuil * zdfc;
        $("#txt_shfc").val(feel);
    }
}
//最低税后分成基数
function shfcy_calculate() {
    var builarea = $("#txtBuildArea").val();
    var shfc = $("#txt_shfc").val();
    if (checkRate(builarea) && checkRate(shfc)) {
        var count = builarea * shfc;
        $("#txt_shfcy").val(count);
    }
}
//合同收费
function coperation_fee() {
    var cprfee = $("#txt_cprdan").val();
    var area = $("#txtBuildArea").val();
    if (checkRate(cprfee) && checkRate(area)) {
        var cout = cprfee * area;
        $("#txt_cprfee").val(cout);
    }
}
//最低收款基数
function low_fee() {
    var area = $("#txtBuildArea").val();
    var lowfee = $("#txt_zdfc").val();
    if (checkRate(area) && checkRate(low_fee)) {
        var cout = area * lowfee;
        $("#txt_zdfcy").val(cout);
    }
}
//浮点数
function checkRate(value) {
    var re = /^[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}