﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //隔行变色
    $("#gvproAlloting tr:odd").css({ background: "#f0f0f0" });
    //隔行变色
    $("#gvproAlloted tr:odd").css({ background: "#f0f0f0" });

    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    // Tabs
    $('#tabs').tabs({ cookie: { expires: 30} });

    //删除
    $(".cls_delteTranNts").live("click", function () {
        var status = $(this).parent().parent().find("td").eq(4).text();
        //var status = $(this).next().next().next(":hidden").val();
        if (status == 'S') {
            alert("项目分配审核完毕，不能删除");
            return false;
        }

        if (confirm("确定要删除本条分配记录吗？")) {
            var proId = $(this).parent().parent().find("td").eq(2).tex();
            var allotID = $(this).attr("rel");
            //            var proId = $(this).next(":hidden").val();
            //            var allotID = $(this).next().next(":hidden").val();
            Global.SendRequest("/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx", { "Action": 2, "proID": proId, "AllotID": allotID }, null, null, function (jsonResult) {
                if (jsonResult == "0") {
                    alert("删除失败！");
                } else {
                    alert("删除成功！");
                    //查询系统新消息
                    window.location.href = "/ProjectValueandAllot/TrunHavcList.aspx";
                }
            });
        }
    });



    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = 0;
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_cprName"));
    autoComplete.GetAutoAJAX();
    //提示
    $(".cls_column").tipsy({ opacity: 1 });
})