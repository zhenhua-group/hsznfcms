﻿
var jsonDataEntity;
var actionFlag;
var IsTrunEconomys;
var IsTrunHavc;
$(function () {

    CommonControl.SetFormWidth();

    //Tab页
    $('#tabs').tabs({ cookie: { expires: 30} });
    $("#tabsMem").tabs({ cookie: { expires: 30} });
    $("#tabsProcess").tabs({ cookie: { expires: 30} });
    $("#tabsMemAmount").tabs({ cookie: { expires: 30} });

    $("input", $("#CoperationBaseInfo")).attr("disabled", true);
    $("#CoperationBaseInfo tr:odd").attr("style", "background-color:#FFF;");


    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    //当前合同审核记录
    var auditRecordStatus = $("#AuditRecordStatus").val();

    //检查用户权限
    var hasPower = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.CheckPower(auditRecordStatus, $("#HiddenLoginUser").val());

    //项目阶段
    var cprProcess = $("#HiddenCoperationProcess").val();


    //是否可以编辑
    var IsEdit = $("#HiddenIsEdit").val();
    var unitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();
  
    if (IsEdit == 0) {
        if (unitName.indexOf("暖通") == -1) {

            $("#ctl00_ContentPlaceHolder1_gvOne input[type=text]").attr("disabled", true);
            $("#ctl00_ContentPlaceHolder1_gvTwo input[type=text]").attr("disabled", true);
            $("#ctl00_ContentPlaceHolder1_gvThree input[type=text]").attr("disabled", true);
            $("#ctl00_ContentPlaceHolder1_gvFour input[type=text]").attr("disabled", true);
        }
    }

    //取得审核用户
    var auditUserArr = $("#HiddenAuditUser").val().split(",");
    //取得审核时间
    var auditDate = $("#HiddenAuditDate").val().split(",");


    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();

    if ($("#hidLikeType").val() == "detail") {
        InitViewStateDetail(auditRecordStatus, cprProcess);
    }
    else {
        if (messageStauts == auditRecordStatus || messageStauts == "") {
            //初始化状态
            InitViewStateTemp(auditRecordStatus, hasPower.value, cprProcess);
        } else {
            //显示状态
            InitViewMessageState(messageStauts, cprProcess, auditRecordStatus);
        }
    }

    //判断是否删除
    if ($("#tbdelete tr td").text() == "此信息已被删除!") {
        $("#MainDiv").children().hide();
        $("#btnEditALL").hide();
    }

    //填充第一位审核人和审核时间
    $("#AuditUser", $("#OneTable")).html(auditUserArr[0] + ":<br/>" + auditDate[0]);

    //填充第5位审核人和审核时间
    if (auditRecordStatus != "F") {
        //二次产值分配赋值
        $("#AuditUser", $("#FiveTable")).html(auditUserArr[1] + ":<br/>" + auditDate[1]);
    }


    //填充第6位审核人和审核时间
    var speHeadUser = $("#hiddenSpetityHead").val();
    if (auditRecordStatus != "H") {

        var datastring = auditDate[2];
        if (datastring != undefined) {

            var data = datastring.replace("{", "").replace("}", "").split(";"); ;
            var spe = speHeadUser.split(";");

            var headstring = "";

            for (var i = 0; i < data.length; i++) {
                headstring = headstring == "" ? spe[i] + "<br/>" + data[i] : headstring + " <br/> " + spe[i] + "<br/>" + data[i];
            }

            $("#AuditUser", $("#SixTable")).html(headstring);
        }

    }

    //填充第7位审核人和审核时间
    var sevenSuggstion = $("#HiddenSevenSuggsion").val();
    if (sevenSuggstion && sevenSuggstion.length > 0) {
        $("#SevenSuggstion").val(sevenSuggstion).attr("disabled", true).css("color", "black");
        $("#AuditUser", $("#SevenTable")).text(auditUserArr[3] + "     /       " + auditDate[3]);
    }

    //填充第8位审核人和审核时间
    var eightSuggstion = $("#HiddenEightSuggstion").val();
    if (eightSuggstion && eightSuggstion.length > 0) {
        $("#EightSuggstion").val(eightSuggstion).attr("disabled", true).css("color", "black");
        $("#AuditUser", $("#EightTable")).text(auditUserArr[4] + "     /       " + auditDate[4]);
    }
});


//审核不通过按钮和审核通过按钮不可用
function DisableButton() {
    $(":button[name=controlBtn]").attr("disabled", true);
}

//重新申请审批按钮
function AuditAgain() {
    $("#FallBackCoperaion").show();
}

//没有权限审核
function NoPowerAudit() {
    $("#NoPowerTable").show();
}


//初始化页面状态
function InitViewStateTemp(auditStatus, hasPower, process) {
    var length = 0;
    switch (auditStatus) {
        case "B":
            $("#OneTable").show();
            //二次产值分配 只有所长情况
            $("#alloutCount").hide();
            $("#tbAllotDeatil").hide();
            $("#TwoTable").hide();

            break;
        case "C":
            length = 3;
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            DisableButton();
            AuditAgain();
            break;
        case "F":

            $("#OneTable").show();
            $("#TwoTable").hide();
            $("#FiveTable").show();

            SetProcess(process);

            if ($("#tbNoData").text() != "") {
                $("#tbNoData").show();
                DisableButton();
            }

            break;

        case "G":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#btnApproval").hide();
            $("#btnRefuse").hide();

            AuditAgain();
            break;
        case "H":

            //专业已经审批完毕
            if ($("#tbIsAuditedBymemberAmount").text() != '') {

                $("#SixTable").show();
                $("#tbIsAuditedBymemberAmount").show();

                var userName = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.GetUserName($("#HiddenSpecialityAuditUser").val());

                $("#AuditUser", $("#SixTable")).text(userName.value);
                $("#btnApproval").hide();
                $("#btnRefuse").hide();

            } else {
                $("#OneTable").show();
                $("#memberProcessTotal").show();

                $("#SixTable").show();

                $("#tbProjectValueBymember").show();
                $("#tbChooseUser").show();
            }

            break;
        case "I":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            if ($("#lbl_NoData").text() != "") {
                $("#lbl_NoData").show();
            }
            SetProcess(process);

            $("input", $("#FiveTable")).attr("disabled", true);
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;

        case "J":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();
            break;
        case "K":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymember").show();
            $("input", $("#SixTable")).attr("disabled", true);

            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
        case "L":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            
            break;

        case "N":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();
            break;
        case "O":
            $("#OneTable").show();
            $("#TwoTable").hide();

            setProcessed(process);

            $("#FiveTable").show();

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();

            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
        case "P":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            $("#EightTable").show();
            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();
            break;
        case "Q":
            $("#OneTable").show();
            $("#TwoTable").hide();

            setProcessed(process);

            $("#FiveTable").show();

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
    }

}


//初始化页面状态
//审核只看自己的
// 1:消息状态 2：审核状态
function InitViewMessageState(messageStatus, process, staus) {

    if (messageStatus == "B") {
        $("#TwoTable").hide();
    }

    if (messageStatus == "F" && staus == "I") {
        $("#FiveTable").show();
        $("input", $("#FiveTable")).attr("disabled", true);
        SetProcess(process);
    }

    if (messageStatus == "F" && staus != "I") {
        $("#FiveTable").show();
        setProcessed(process);
    }

    if (messageStatus == "H" && staus != "K") {
        $("#SixTable").show();

        if ($("#gvIsAuditBymemberAmount").text() != '') {
            $("#tbIsAuditedBymemberAmount").show();
        }
        else {
            $("#tbProjectValueBymemberAmount").show();
        }
    }
    if (messageStatus == "H" && staus == "K") {
        $("#SixTable").show();

        $("#tbProjectValueBymemberNOAmount").show();
        $("input", $("#SixTable")).attr("disabled", true);

    }
    if (messageStatus == "L") {
        $("#SevenTable").show();
    }
    if (messageStatus == "N") {
        $("#EightTable").show();
    }
    $("#btnApproval").hide();
    $("#btnRefuse").hide();
}


//设置分配阶段
function SetProcess(process) {

    if (process == 0) {
        $("#stagetablezero").show();
        $("#stagetableone").hide();
        $("#designProcessOneTable").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    }
    else if (process == 1) {

        $("#stagetablezero").hide();
        $("#stagetableone").show();
        $("#designProcessOneTable").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").hide();
        $("#designProcessFourTable").hide();
        $("#stagespetable").show();
    }
    else if (process == 2) {
        $("#stagetabletwo").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    }
    else if (process == 3) {
        $("#stagetablethree").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    } else if (process == 10) {
        $("#stagetableten").show();
        $("#designProcessOneTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    } else {
        $("#stagetabletfive").show();
        $("#tbOutDoor").show();
    }
}

//设置分配之后的状态
function setProcessed(process) {

    if (process == 0) {
        $("#stageamountOne").show();
        $("#processAmountOneTable").show();
        $("#processAmountTwoTable").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    }
    else if (process == 1) {
        $("#stageamountTwo").show();
        $("#processAmountOneTable").show();
        $("#processAmountTwoTable").show();
        $("#spetamountable").show();
    }
    else if (process == 2) {
        $("#stageamountThree").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    }
    else if (process == 3) {
        $("#stageamountFour").show();
        $("#processAmountTwoTable").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    } else if (process ==10) {
        $("#stageamountTen").show();
        $("#processAmountOneTable").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    } else {
        $("#processAmountFiveTable").show();
        $("#outdooramounttable").show();
    }
}



//查看页面转过来的
function InitViewStateDetail(auditStatus, process) {
    
    var length = 0;
    switch (auditStatus) {
        case "B":
            $("#OneTable").show();
            //二次产值分配 只有所长情况
            $("#alloutCount").hide();
            $("#tbAllotDeatil").hide();
            $("#TwoTable").hide();

            break;
        case "C":
            length = 3;
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            DisableButton();
            AuditAgain();
            break;
        case "F":

            $("#OneTable").show();
            $("#TwoTable").hide();
            $("#FiveTable").show();

            SetProcess(process);

            if ($("#tbNoData").text() != "") {
                $("#tbNoData").show();
                DisableButton();
            }

            break;

        case "G":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#btnApproval").hide();
            $("#btnRefuse").hide();

            AuditAgain();
            break;
        case "H":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            if ($("#lbl_NoData").text() != "") {
                $("#lbl_NoData").show();
            }
            setProcessed(process);

            $("input", $("#FiveTable")).attr("disabled", true);
           

            break;
        case "I":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            if ($("#lbl_NoData").text() != "") {
                $("#lbl_NoData").show();
            }
            SetProcess(process);

            $("input", $("#FiveTable")).attr("disabled", true);
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;

        case "J":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();
            break;
        case "K":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymember").show();
            $("input", $("#SixTable")).attr("disabled", true);

            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
        case "L":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();

            break;

        case "N":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();
            break;
        case "O":
            $("#OneTable").show();
            $("#TwoTable").hide();

            setProcessed(process);

            $("#FiveTable").show();

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();

            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
        case "P":
            $("#OneTable").show();
            $("#TwoTable").hide();

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            $("#EightTable").show();
            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();
            break;
        case "Q":
            $("#OneTable").show();
            $("#TwoTable").hide();

            setProcessed(process);

            $("#FiveTable").show();

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;

    }
    $("#btnApproval").hide();
    $("#btnRefuse").hide();

    $("input[type=text]").attr("disabled", "disabled");
    $("select").attr("disabled", "disabled");
    $("span").attr("disabled", "disabled");

    $("#btn_AllApproval").hide();

}
