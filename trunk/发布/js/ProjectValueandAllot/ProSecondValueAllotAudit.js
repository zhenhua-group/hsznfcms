﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var actionFlag;
var cprProcess;
var projectValueSendMessageClass; //产值发送消息
var auditRecordStatus;
//返回消息列表
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;

$(function () {


    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();


    //当前合同审核记录
    auditRecordStatus = $("#AuditRecordStatus").val();

    cprProcess = $("#HiddenCoperationProcess").val();

    //是否可以编辑
    var IsEdit = $("#HiddenIsEdit").val();
    var isHavc = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();



    //审核通过按钮
    $("#btnApproval").click(function () {

        if (auditRecordStatus == "F") {

            var finalDataObj = {
                "ProNo": $("#HiddenCoperationSysNo").val(),
                "SysNo": $("#HiddenAuditRecordSysNo").val(),
                "Stage": new Array(),
                "AuditUser": $("#HiddenLoginUser").val(),
                "StageSpe": new Array(),
                "DesignProcess": new Array(),
                "DesignProcessTwo": new Array(),
                "DesignProcessThree": new Array(),
                "DesignProcessFour": new Array(),
                "OutDoor": new Array(),
                "DesignProcessFive": new Array(),
                "type": cprProcess
            };
            //项目各阶段
            if (cprProcess == 0) {

                //验证
                if (!ValidationOne()) {
                    return false;
                }

                // 取得 项目各阶段
                $.each($("#ctl00_ContentPlaceHolder1_gvOne tr"), function (index, tr) {
                    finalDataObj.Stage[index] =
                {
                    "ItemType": $.trim($(tr).children("td:eq(0)").text()),
                    "ProgramPercent": $.trim($(tr).children("td:eq(1)").find("input").val()),
                    "ProgramAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "preliminaryPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "preliminaryAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "WorkDrawPercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "WorkDrawAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "LateStagePercent": $.trim($(tr).children("td:eq(7)").find("input").val()),
                    "LateStageAmount": $.trim($(tr).children("td:eq(8)").text())
                };
                });


                // 取得工序
                $.each($("#gvdesignProcessOne tr"), function (index, tr) {
                    finalDataObj.DesignProcess[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
                });

                //初步设计工序
                $.each($("#gvdesignProcessTwo tr"), function (index, tr) {
                    finalDataObj.DesignProcessTwo[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });

                //施工图工序
                $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                    finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });
                //后期服务
                $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                    finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
                });

                //专业
                var arraySpecialty = new Array();
                $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {
                    arraySpecialty.push($(this).text());
                });

                //方案设计
                var arrayProgram = new Array();
                $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                    arrayProgram.push($(this).text());
                });
                var arrayProgramPercent = ArrayProgramPercent();


                //初步设计
                var arrayPreliminary = new Array();

                $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                    arrayPreliminary.push($(this).text());
                });
                var arrayPreliminaryPercent = ArrayPreliminaryPercent();


                //施工图设计
                var arrayWorkDraw = new Array();
                $("#gvProjectStageSpe tr:eq(2) td:even").not(":first").not(":last").each(function () {
                    arrayWorkDraw.push($(this).text());
                });
                var arrayWorkDrawPercent = ArrayWorkDrawPercent();

                //后期服务
                var arrayLateStage = new Array();
                $("#gvProjectStageSpe tr:eq(3) td:even").not(":first").not(":last").each(function () {
                    arrayLateStage.push($(this).text());
                });
                var arrayLateStagePercent = ArrayLateStagePercent();

                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "ProgramPercent": arrayProgramPercent[i],
                    "ProgramCount": arrayProgram[i],
                    "preliminaryPercent": arrayPreliminaryPercent[i],
                    "preliminaryCount": arrayPreliminary[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

                }

            } //方案+初设
            else if (cprProcess == 1) {

                if (!ValidationTwo()) {
                    return false;
                }

                // 取得
                $.each($("#ctl00_ContentPlaceHolder1_gvTwo tr"), function (index, tr) {
                    finalDataObj.Stage[index] =
                { "ItemType": $.trim($(tr).children("td:eq(0)").text()), "ProgramPercent": $.trim($(tr).children("td:eq(1)").find("input").val()), "ProgramAmount": $.trim($(tr).children("td:eq(2)").text()), "preliminaryPercent": $.trim($(tr).children("td:eq(3)").find("input").val()), "preliminaryAmount": $.trim($(tr).children("td:eq(4)").text()) };
                });


                // 取得工序
                $.each($("#gvdesignProcessOne tr"), function (index, tr) {
                    finalDataObj.DesignProcess[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
                });

                //初步设计工序
                $.each($("#gvdesignProcessTwo tr"), function (index, tr) {
                    finalDataObj.DesignProcessTwo[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });

                //专业
                var arraySpecialty = new Array();
                $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {

                    arraySpecialty.push($(this).text());
                });

                //方案设计
                var arrayProgram = new Array();
                $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                    arrayProgram.push($(this).text());
                });
                var arrayProgramPercent = ArrayProgramPercent();


                //初步设计
                var arrayPreliminary = new Array();

                $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                    arrayPreliminary.push($(this).text());
                });
                var arrayPreliminaryPercent = ArrayPreliminaryPercent();

                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "ProgramPercent": arrayProgramPercent[i],
                    "ProgramCount": arrayProgram[i],
                    "preliminaryPercent": arrayPreliminaryPercent[i],
                    "preliminaryCount": arrayPreliminary[i]

                };

                }
            } //施工图+后期
            else if (cprProcess == 2) {

                if (!ValidationThree()) {
                    return false;
                }

                // 取得
                $.each($("#ctl00_ContentPlaceHolder1_gvThree tr"), function (index, tr) {
                    finalDataObj.Stage[index] =
                { "ItemType": $.trim($(tr).children("td:eq(0)").text()), "WorkDrawPercent": $.trim($(tr).children("td:eq(1)").find("input").val()), "WorkDrawAmount": $.trim($(tr).children("td:eq(2)").text()), "LateStagePercent": $.trim($(tr).children("td:eq(3)").find("input").val()), "LateStageAmount": $.trim($(tr).children("td:eq(4)").text()) };
                });


                //施工图工序
                $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                    finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });
                //后期服务
                $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                    finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
                });

                //专业
                var arraySpecialty = new Array();
                $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {

                    arraySpecialty.push($(this).text());
                });

                //施工图设计
                var arrayWorkDraw = new Array();
                $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                    arrayWorkDraw.push($(this).text());
                });
                var arrayWorkDrawPercent = ArrayProgramPercent();

                //后期服务
                var arrayLateStage = new Array();
                $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                    arrayLateStage.push($(this).text());
                });
                var arrayLateStagePercent = ArrayPreliminaryPercent();


                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

                }
            } //施工图+初设+后期
            else if (cprProcess == 3) {

                if (!ValidationFour()) {
                    return false;
                }

                // 取得
                $.each($("#ctl00_ContentPlaceHolder1_gvFour tr"), function (index, tr) {
                    finalDataObj.Stage[index] =
                {
                    "ItemType": $.trim($(tr).children("td:eq(0)").text()),
                    "preliminaryPercent": $.trim($(tr).children("td:eq(1)").find("input").val()),
                    "preliminaryAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "WorkDrawPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "WorkDrawAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "LateStagePercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "LateStageAmount": $.trim($(tr).children("td:eq(6)").text())
                };
                });

                //初步设计工序
                $.each($("#gvdesignProcessTwo tr"), function (index, tr) {
                    finalDataObj.DesignProcessTwo[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });

                //施工图工序
                $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                    finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });
                //后期服务
                $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                    finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
                });

                //专业
                var arraySpecialty = new Array();
                $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {
                    arraySpecialty.push($(this).text());
                });


                //初步设计
                var arrayPreliminary = new Array();

                $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                    arrayPreliminary.push($(this).text());
                });
                var arrayPreliminaryPercent = ArrayProgramPercent();


                //施工图设计
                var arrayWorkDraw = new Array();
                $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                    arrayWorkDraw.push($(this).text());
                });
                var arrayWorkDrawPercent = ArrayPreliminaryPercent();

                //后期服务
                var arrayLateStage = new Array();
                $("#gvProjectStageSpe tr:eq(2) td:even").not(":first").not(":last").each(function () {
                    arrayLateStage.push($(this).text());
                });
                var arrayLateStagePercent = ArrayWorkDrawPercent();


                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "preliminaryPercent": arrayPreliminaryPercent[i],
                    "preliminaryCount": arrayPreliminary[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

                } //其他情况 例如 室外工程
            }
            else if (cprProcess == 10) {

                if (!ValidationTen()) {
                    return false;
                }

                
                // 取得
                $.each($("#ctl00_ContentPlaceHolder1_gvTen tr"), function (index, tr) {
                    finalDataObj.Stage[index] =
                {
                    "ItemType": $.trim($(tr).children("td:eq(0)").text()),
                    "ProgramPercent": $.trim($(tr).children("td:eq(1)").find("input").val()),
                    "ProgramAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "WorkDrawPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "WorkDrawAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "LateStagePercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "LateStageAmount": $.trim($(tr).children("td:eq(6)").text())
                };
                });

                //方案设计工序
                $.each($("#gvdesignProcessOne tr"), function (index, tr) {
                    finalDataObj.DesignProcess[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });

                //施工图工序
                $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                    finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });
                //后期服务
                $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                    finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
                });

                //专业
                var arraySpecialty = new Array();
                $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {
                    arraySpecialty.push($(this).text());
                });

                //方案设计
                var arrayPreliminary = new Array();

                $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                    arrayPreliminary.push($(this).text());
                });
                var arrayPreliminaryPercent = ArrayProgramPercent();


                //施工图设计
                var arrayWorkDraw = new Array();
                $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                    arrayWorkDraw.push($(this).text());
                });
                var arrayWorkDrawPercent = ArrayPreliminaryPercent();

                //后期服务
                var arrayLateStage = new Array();
                $("#gvProjectStageSpe tr:eq(2) td:even").not(":first").not(":last").each(function () {
                    arrayLateStage.push($(this).text());
                });
                var arrayLateStagePercent = ArrayWorkDrawPercent();


                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "ProgramPercent": arrayPreliminaryPercent[i],
                    "ProgramCount": arrayPreliminary[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

                }
            }
            else {

                if (!ValidationFive()) {
                    return false
                }

                $.each($("#gvdesignProcessFive tr"), function (index, tr) {
                    finalDataObj.DesignProcessFive[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
                });

                //专业
                var arraySpecialty = new Array();
                $("#stagetabletfive table:eq(1)  td").not(":last").not(":first").each(function () {

                    arraySpecialty.push($(this).text());
                });

                var arrayPreliminary = new Array();
                var arrayPreliminaryPercent = new Array();
                $("#gvFive  tr:eq(0) td:even").not(":first").not(":last").each(function () {
                    arrayPreliminary.push($(this).text());
                });

                var arrayPreliminaryPercent = ArrayOtherPercent();


                //室外工程
                if (cprProcess == 4) {
                    for (var i = 0; i < arraySpecialty.length; i++) {
                        finalDataObj.OutDoor[i] =
                {
                    "Status": 4,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                    }
                } else if (cprProcess == 5) {

                    for (var i = 0; i < arraySpecialty.length; i++) {
                        finalDataObj.OutDoor[i] =
                {
                    "Status": 5,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                    }
                } else if (cprProcess == 6) {

                    for (var i = 0; i < arraySpecialty.length; i++) {
                        finalDataObj.OutDoor[i] =
                {
                    "Status": 6,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                    }
                }
                else if (cprProcess == 7) {

                    for (var i = 0; i < arraySpecialty.length; i++) {
                        finalDataObj.OutDoor[i] =
                {
                    "Status": 7,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                    }
                }
                else if (cprProcess == 8) {

                    for (var i = 0; i < arraySpecialty.length; i++) {
                        finalDataObj.OutDoor[i] =
                {
                    "Status": 8,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                    }
                }
                else if (cprProcess == 9) {

                    for (var i = 0; i < arraySpecialty.length; i++) {
                        finalDataObj.OutDoor[i] =
                {
                    "Status": 9,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                    }
                }
            }
            if (!confirm("是否通过审核信息?")) {
                return false;
            }

            $("#btnApproval").attr("disabled", true);
            var jsonObj = Global.toJSON(finalDataObj);
            jsonDataEntity = jsonObj;
            actionFlag = '4';
            getUserAndUpdateAudit(actionFlag, '0', jsonDataEntity);

        } //保存人员信息
        else if (auditRecordStatus == "H") {

            var msg = "";
            if (!ValidationMem()) {
                return false;
            }

            var finalDataObj = {
                "ProNo": $("#HiddenCoperationSysNo").val(),
                "SysNo": $("#HiddenAuditRecordSysNo").val(),
                "ValueByMember": new Array(),
                "AuditUser": $("#HiddenLoginUser").val(),
                "Status": "A"
            };

            // 取得
            $.each($("table[memMoney='gvProjectValueBymember'] tr"), function (index, tr) {
                finalDataObj.ValueByMember[index] =
                {
                    "mem_ID": $.trim($(tr).children("td:eq(1)").text()),
                    "DesignPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "DesignCount": $.trim($(tr).children("td:eq(4)").text()),
                    "SpecialtyHeadPercent": $.trim($(tr).children("td:eq(6)").find("input").val()),
                    "SpecialtyHeadCount": $.trim($(tr).children("td:eq(7)").text()),
                    "AuditPercent": $.trim($(tr).children("td:eq(9)").find("input").val()),
                    "AuditCount": $.trim($(tr).children("td:eq(10)").text()),
                    "ProofreadPercent": $.trim($(tr).children("td:eq(12)").find("input").val()),
                    "ProofreadCount": $.trim($(tr).children("td:eq(13)").text()),
                    "ItemType": $.trim($(tr).children("td:eq(15)").text()),
                    "IsHead": $.trim($(tr).children("td:eq(3)").find("input").attr("sz")),
                    "IsExternal": $.trim($(tr).children("td:eq(1)").attr("wp")),
                    "SpeName": $.trim($(tr).children("td:eq(3)").find("input").attr("zy"))
                };
            });

            if (!confirm("是否通过审核信息?")) {
                return false;
            }
            $("#btnApproval").attr("disabled", true);
            var jsonObj = Global.toJSON(finalDataObj);
            jsonDataEntity = jsonObj;
            actionFlag = '5';
            getUserAndUpdateAudit(actionFlag, '0', jsonDataEntity);
        }
        else {

            var auditObj = {
                SysNo: $("#HiddenAuditRecordSysNo").val(),
                AuditUser: $("#HiddenLoginUser").val(),
                Status: auditRecordStatus
            };

            //判断当前是在哪个状态
            switch (auditRecordStatus) {

                //承接部门通过                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                case "B":
                    if ($.trim($("#TwoSuggstion").val()).length == 0) {
                        alert("所长意见不能为空！");
                        return false;
                    }
                    auditObj.TwoSuggstion = $.trim($("#TwoSuggstion").val());

                    break;

                case "H":

                    break;
                case "L":
                    //经营部意见
                    if ($.trim($("#SevenSuggstion").val()).length == 0) {
                        alert("所长意见不能为空！");
                        return false;
                    }
                    auditObj.SevenSuggsion = $.trim($("#SevenSuggstion").val());
                    break;
                case "N":
                    //经营部意见
                    if ($.trim($("#EightSuggstion").val()).length == 0) {
                        alert("生产经营部意见不能为空！");
                        return false;
                    }
                    auditObj.EightSuggstion = $.trim($("#EightSuggstion").val());
                    break;
            }
            if (!confirm("是否通过审核信息?")) {
                return false;
            }
            $("#btnApproval").attr("disabled", true);
            var jsonObj = Global.toJSON(auditObj);
            jsonDataEntity = jsonObj;
            actionFlag = '1';

            if (auditRecordStatus == "N") {
                getUserAndUpdateAudit('1', '1', jsonDataEntity);
            }
            else {
                getUserAndUpdateAudit(actionFlag, '0', jsonDataEntity);
            }
        }
    });

    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: auditRecordStatus
        };
        switch (auditRecordStatus) {
            //新规状态                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
            case "A":
                break;
            case "B":
                //经济所参与
                if ($.trim($("#TwoSuggstion").val()).length == 0) {
                    alert("所长意见不能为空！");
                    return false;
                }
                auditObj.TwoSuggstion = $.trim($("#TwoSuggstion").val());

                break;

            case "H":

                break;
            case "J":
                //

                break;
            case "L":
                //经营部意见
                if ($.trim($("#SevenSuggstion").val()).length == 0) {
                    alert("所长意见不能为空！");
                    return false;
                }
                auditObj.SevenSuggsion = $.trim($("#SevenSuggstion").val());
                break;
            case "N":
                //经营部意见
                if ($.trim($("#EightSuggstion").val()).length == 0) {
                    alert("生产经营部意见不能为空！");
                    return false;
                }
                auditObj.EightSuggstion = $.trim($("#EightSuggstion").val());
                break;
        }
        if (!confirm("是否不通过审核信息?")) {
            return false;
        }

        $("#btnRefuse").attr("disabled", true);
        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/ProjectSecondValueandAllotHandler.ashx", { "Action": 2, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {

                    //改变消息状态
                    var msg = new MessageCommProjAllot1($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("分配不通过成功，消息已发送给申请人！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

                // window.history.back();
            }
        });

    });

    //返回重新审核按钮
    $("#FallBackCoperaion").click(function () {
        window.location.href = "ProjectSecondValueListBymaster.aspx";
    });

    if (auditRecordStatus == "F") {
        //实例化类容
        messageDialog = $("#auditShow").messageDialog;


        projectValueSendMessageClass = new ProjectValueMessageCommon(messageDialog);

    }
    else {

        messageDialog = $("#auditShow").messageDialog;
        sendMessageClass = new MessageCommon(messageDialog);

    }

    $("#btn_Send").click(function () {
        if (auditRecordStatus == "F") {
            //                    //取得复选框中选有的专业
            var spe = new Array();
            $(":checkbox[name=messageUser]").each(function () {

                spe.push($(this).attr("roles"));
            });

            var _speDistinct = spe.reverse().join(",").match(/([^,]+)(?!.*\1)/ig).reverse();

            var _flag = true;
            $.each(_speDistinct, function (key, val) {
                var _length = 0;
                $(":checkbox[name=messageUser]").each(function () {

                    if ($(this).attr("roles") == val) {
                        var _$mesUser = $(":checkbox[name=messageUser]:checked");
                        if ($(this).is(":checked")) {
                            _length = _length + 1;
                        }
                    }
                });
                if (_length == 0) {
                    alert("" + val + "专业,请至少选择一个流程审批人！");
                    _flag = false;
                    return false;
                }
            });

            if (!_flag) {
                return false;
            }
            $(this).attr("disabled", "disabled");
            getUserAndUpdateAudit(actionFlag, '1', jsonDataEntity);
        } else {

            //选中用户
            var _$mesUser = $(":checkbox[name=messageUser]:checked");

            if (_$mesUser.length == 0) {
                alert("请至少选择一个流程审批人！");
                return false;
            }
            $(this).attr("disabled", "disabled");
            getUserAndUpdateAudit(actionFlag, '1', jsonDataEntity);
        }
    });
    //关闭
    $("#btn_close").click(function () {
        $("#btnApproval").attr("disabled", false);
    });

    //删除用户A标签点击事件
    $("#resultUserRemoveA").live("click", function () {
        $(this).parent().parent().parent().remove();
    });

    var isRounding = $("#HiddenIsRounding").val();
    //重新选择用户事情

    $("#txtChooseUser").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputvalue = $(this).val();
        var total = $(this).parent().next().next().text();

        var proresultvalue = parseFloat(inputvalue) * parseFloat(total) / 100;

        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }
    });
});

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/ProjectSecondValueandAllotHandler.ashx";

    //数据         
    var data = { "Action": action, "flag": flag, "data": jsonData, "msgID": $("#msgno").val() };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (flag == "1") {
            var msg = new MessageCommProjAllot1($("#msgno").val());
            msg.ReadMsg();
        }
        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("产值分配完成，已全部通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

        } else if (jsonResult == "2") {
            alert("产值分配,审核消息通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

        } else if (jsonResult == "3") {
            alert("产值分配,人员产值分配通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else if (jsonResult == "4") {
            var msg = new MessageCommProjAllot1($("#msgno").val());
            msg.ReadMsg();
            alert("专业负责人产值分配成功,给对应人员发送产值确认消息成功!");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else if (jsonResult == "10") {
            alert("本专业的其他负责人，已经分配相关人员，不能重复填写！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {

        if (auditRecordStatus == "F") {
            projectValueSendMessageClass.render(obj.UserList, obj.RoleName); $("#AuditUserDiv").modal();
        }
        else {
            sendMessageClass.render(obj.UserList, obj.RoleName); $("#AuditUserDiv").modal();
        }
    }
    else {
        if (auditRecordStatus == "F") {
            projectValueSendMessageClass.setMsgTemplate(obj);
            projectValueSendMessageClass.chooseUserForMessage(sendMessage);
        }
        else {
            sendMessageClass.setMsgTemplate(obj);
            sendMessageClass.chooseUserForMessage(sendMessage);
        }
    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        $("#AuditUserDiv").modal('hide');
        alert("产值分配消息发送成功！");
        //查询系统新消息
        window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

        //window.history.back();
    } else {
        alert("消息发送失败！");
    }
}


function ValidationOne() {

    var flag = true;

    var valueemputystagetwo = "0";
    $.each($("input", "#ctl00_ContentPlaceHolder1_gvOne tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputystagetwo = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputystagetwo).length == 0) {
        alert("项目各设计阶段专业之间产值分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalstagetwo = -1;
    $("#ctl00_ContentPlaceHolder1_gvOne tr").each(function () {
        var valueTotal = 0;
        $("input", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalstagetwo = valueTotal;
            return false;
        }
    });

    if (taotalstagetwo.toFixed(0) != 100 && taotalstagetwo != -1) {
        alert("项目各设计阶段产值分配比例比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;
}



//验证 方案——初设
function ValidationTwo() {

    var flag = true;
    var valueemputy = "0";
    $.each($("input", "#ctl00_ContentPlaceHolder1_gvTwo tr td"), function (index, tr) {
        if ($.trim($(this).val()).length == 0) {
            valueemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputy).length == 0) {
        alert("项目(方案+初步设计)二阶段产值分配比例不能为空");
        flag = false;
        return false;
    }

    var ComValue = 0;
    $("input", "#ctl00_ContentPlaceHolder1_gvTwo").each(function () {
        ComValue += parseFloat($(this).val());
    });

    if (ComValue.toFixed(0) != 100) {
        alert("项目(方案+初步设计)二阶段产值分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }


    return flag;
}

function ValidationThree() {

    var flag = true;
    var valueemputy = "0";
    $.each($("input", "#ctl00_ContentPlaceHolder1_gvThree tr td"), function (index, tr) {
        if ($.trim($(this).val()).length == 0) {
            valueemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputy).length == 0) {
        alert("项目(施工图设计+后期服务)二阶段产值分配比例不能为空");
        flag = false;
        return false;
    }

    var ComValue = 0;
    $("input", "#ctl00_ContentPlaceHolder1_gvThree").each(function () {
        ComValue += parseFloat($(this).val());
    });

    if (ComValue.toFixed(0) != 100) {
        alert("项目(施工图设计+后期服务)二阶段产值分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;

}

function ValidationFour() {
    var flag = true;
    var valueemputystagetwo = "0";
    $.each($("input", "#ctl00_ContentPlaceHolder1_gvFour tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputystagetwo = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputystagetwo).length == 0) {
        alert("(初步设计+施工图+后期)三阶段分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalstagetwo = -1;
    $("#ctl00_ContentPlaceHolder1_gvFour tr").each(function () {
        var valueTotal = 0;
        $("input", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalstagetwo = valueTotal;
            return false;
        }
    });

    if (taotalstagetwo.toFixed(0) != 100 && taotalstagetwo != -1) {
        alert("(初步设计+施工图+后期)三阶段分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;

}

function ValidationTen() {
    var flag = true;
    var valueemputystagetwo = "0";
    $.each($("input", "#ctl00_ContentPlaceHolder1_gvTen tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputystagetwo = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputystagetwo).length == 0) {
        alert("(方案设计+施工图+后期)三阶段分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalstagetwo = -1;
    $("#ctl00_ContentPlaceHolder1_gvTen tr").each(function () {
        var valueTotal = 0;
        $("input", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalstagetwo = valueTotal;
            return false;
        }
    });

    if (taotalstagetwo.toFixed(0) != 100 && taotalstagetwo != -1) {
        alert("(方案设计+施工图+后期)三阶段分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;

}

function Validation() {

    var flag = true;

    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvProjectStageSpe tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valuespeemputy).length == 0) {
        alert("项目各设计阶段专业之间产值分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalspe = -1;
    $("#gvProjectStageSpe tr").each(function () {
        var valueTotal = 0;
        $("input:not(:disabled)", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalspe = valueTotal;
            return false;
        }
    });

    if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
        alert("项目各设计阶段专业之间产值分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
        var valueemputyone = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessOne tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyone = $(this).val();
                return false;
            }
        });



        if ($.trim(valueemputyone).length == 0) {
            alert("方案设计工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotalone = -1;
        $("#gvdesignProcessOne tr").each(function () {
            var valueTotal = 0;
            if ($(this).children().find("input").length > 0) {

                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotalone = valueTotal;
                    return false;
                }
            }
        });


        if (taotalone.toFixed(0) != 100 && taotalone != -1) {
            alert("方案设计工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 3) {
        var valueemputytwo = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessTwo tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputytwo = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputytwo).length == 0) {
            alert("初步设计工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotaltwo = -1;
        $("#gvdesignProcessTwo tr").each(function () {
            if ($(this).children().find("input").length > 0) {
                var valueTotal = 0;
                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotaltwo = valueTotal;
                    return false;
                }
            }
        });

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("初步设计工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10) {
        var valueemputythree = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessThree tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputythree = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputythree).length == 0) {
            alert("施工图工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotalthree = -1;
        $("#gvdesignProcessThree tr").each(function () {
            if ($(this).children().find("input").length > 0) {
                var valueTotal = 0;
                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotalthree = valueTotal;
                    return false;
                }
            }
        });

        if (taotalthree.toFixed(0) != 100 && taotalthree != -1) {
            alert("施工图工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10 ) {
        var valueemputyfour = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessFour tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyfour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyfour).length == 0) {
            alert("后期服务工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotalfour = -1;
        $("#gvdesignProcessFour tr").each(function () {
            if ($(this).children().find("input").length > 0) {
                var valueTotal = 0;
                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotalfour = valueTotal;
                    return false;
                }
            }
        });

        if (taotalfour.toFixed(0) != 100 && taotalfour != -1) {
            alert("后期服务工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    return flag;
}

// 验证 
function ValidationFive() {

    var flag = true;
    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvFive tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });


    var taotalspe = -1;
    $("#gvFive tr").each(function () {
        var valueTotal = 0;
        $("input:not(:disabled)", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalspe = valueTotal;
            return false;
        }
    });



    var valueemputytwo = "0";
    $.each($("input:not(:disabled)", "#gvdesignProcessFive tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputytwo = $(this).val();
            return false;
        }
    });


    var taotaltwo = -1;
    $("#gvdesignProcessFive tr").each(function () {
        if ($(this).children().find("input").length > 0) {
            var valueTotal = 0;
            $("input:not(:disabled)", this).each(function () {
                valueTotal += parseFloat($(this).val());
            });
            if (valueTotal != 100) {
                taotaltwo = valueTotal;
                return false;
            }
        }
    });



    if (cprProcess == 4) {
        if ($.trim(valuespeemputy).length == 0) {
            alert("室外工程产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("室外工程产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("室外工程工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("室外工程工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

    } else if (cprProcess == 5) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("锅炉房产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("锅炉房产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("锅炉房工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("锅炉房工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 6) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("地上单建水泵房产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("地上单建水泵房产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("地上单建水泵房工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("地上单建水泵房工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 7) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("地上单建变配所(室)产值分配比例不能为空");
            flag = false;
            return false;
        }

        if ($.trim(valuespeemputy).length == 0) {
            alert("地上单建变配所(室)产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("地上单建变配所(室)产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("地上单建变配所(室)工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("地上单建变配所(室)工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 8) {

        if ($.trim(valuespeemputy).length == 0) {
            alert(" 单建地下室（车库）产值分配比例不能为空");
            flag = false;
            return false;
        }

        if ($.trim(valuespeemputy).length == 0) {
            alert("单建地下室（车库）产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("单建地下室（车库）产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("单建地下室（车库）工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("单建地下室（车库）工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 9) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("市政道路工程产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("市政道路工程产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("市政道路工程工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("市政道路工程工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }

    return flag;
}

//验证人员
function ValidationMem() {

    var flag = true;

    var cprProcess = $("#HiddenCoperationProcess").val();

    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
        var valueemputy = "0";
        $.each($("input", "#gvProjectValueOneBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputy = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputy).length == 0) {
            alert("方案设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcount = 0;
        var isCheck = false;
        $("input", "#gvProjectValueOneBymember tr td:nth-child(4)").each(function () {
            designcount += parseFloat($(this).val());
            isCheck = true;
        });

        if (designcount.toFixed(0) != 100 && isCheck == true) {
            alert("方案设计-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcount = 0;
        var isheadcount = false;
        $("input", "#gvProjectValueOneBymember  tr td:nth-child(7)").each(function () {
            headcount += parseFloat($(this).val());
            isheadcount = true;
        });

        if (headcount.toFixed(0) != 100 && isheadcount == true) {
            alert("方案设计-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcount = 0;
        var isauditcount = false;
        $("input", "#gvProjectValueOneBymember  tr td:nth-child(10)").each(function () {
            auditcount += parseFloat($(this).val());
            isauditcount = true;
        });


        if (auditcount.toFixed(0) != 100 && isauditcount == true) {
            alert("方案设计-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcount = 0;
        var isproofreadcountCheck = false;
        $("input", "#gvProjectValueOneBymember  tr td:nth-child(13)").each(function () {
            proofreadcount += parseFloat($(this).val());
            isproofreadcountCheck = true;
        });

        if (proofreadcount.toFixed(0) != 100 && isproofreadcountCheck == true) {
            alert("方案设计-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }

    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 3) {
        var valueemputyTwo = "0";
        $.each($("input", "#gvProjectValueTwoBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyTwo = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyTwo).length == 0) {
            alert("初步设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountTwo = 0;
        var isCheckTwo = false;
        $("input", "#gvProjectValueTwoBymember tr td:nth-child(4)").each(function () {
            designcountTwo += parseFloat($(this).val());
            isCheckTwo = true;
        });

        if (designcountTwo.toFixed(0) != 100 && isCheckTwo == true) {
            alert("初步设计-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountTwo = 0;
        var isheadcountTwo = false;
        $("input", "#gvProjectValueTwoBymember  tr td:nth-child(7)").each(function () {
            headcountTwo += parseFloat($(this).val());
            isheadcountTwo = true;
        });


        if (headcountTwo.toFixed(0) != 100 && isheadcountTwo == true) {
            alert("初步设计-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountTwo = 0;
        var isauditcountTwo = false;
        $("input", "#gvProjectValueTwoBymember  tr td:nth-child(10)").each(function () {
            auditcountTwo += parseFloat($(this).val());
            isauditcountTwo = true;
        });

        if (auditcountTwo.toFixed(0) != 100 && isauditcountTwo == true) {
            alert("初步设计-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountTwo = 0;
        var isproofreadcountCheckTwo = false;
        $("input", "#gvProjectValueTwoBymember  tr td:nth-child(13)").each(function () {
            proofreadcountTwo += parseFloat($(this).val());
            isproofreadcountCheckTwo = true;
        });


        if (proofreadcountTwo.toFixed(0) != 100 && isproofreadcountCheckTwo == true) {
            alert("初步设计-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }

    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10) {
        var valueemputyThree = "0";
        $.each($("input", "#gvProjectValueThreeBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyThree = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyThree).length == 0) {
            alert("施工图设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountThree = 0;
        var isCheckThree = false;
        $("input", "#gvProjectValueThreeBymember tr td:nth-child(4)").each(function () {
            designcountThree += parseFloat($(this).val());
            isCheckThree = true;
        });


        if (designcountThree.toFixed(0) != 100 && isCheckThree == true) {
            alert("施工图设计-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountThree = 0;
        var isheadcountThree = false;
        $("input", "#gvProjectValueThreeBymember  tr td:nth-child(7)").each(function () {
            headcountThree += parseFloat($(this).val());
            isheadcountThree = true;
        });


        if (headcountThree.toFixed(0) != 100 && isheadcountThree == true) {
            alert("施工图设计-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountThree = 0;
        var isauditcountThree = false;
        $("input", "#gvProjectValueThreeBymember  tr td:nth-child(10)").each(function () {
            auditcountThree += parseFloat($(this).val());
            isauditcountThree = true;
        });

        if (auditcountThree.toFixed(0) != 100 && isauditcountThree == true) {
            alert("施工图设计-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountThree = 0;
        var isproofreadcountCheckThree = false;
        $("input", "#gvProjectValueThreeBymember  tr td:nth-child(13)").each(function () {
            proofreadcountThree += parseFloat($(this).val());
            isproofreadcountCheckThree = true;
        });

        if (proofreadcountThree.toFixed(0) != 100 && isproofreadcountCheckThree == true) {
            alert("施工图设计-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10) {
        var valueemputyFour = "0";
        $.each($("input", "#gvProjectValueFourBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyFour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyFour).length == 0) {
            alert("后期服务--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountFour = 0;
        var isCheckFour = false;
        $("input", "#gvProjectValueFourBymember tr td:nth-child(4)").each(function () {
            designcountFour += parseFloat($(this).val());
            isCheckFour = true;
        });


        if (designcountFour.toFixed(0) != 100 && isCheckFour == true) {
            alert("后期服务-设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountFour = 0;
        var isheadcountFour = false;
        $("input", "#gvProjectValueFourBymember  tr td:nth-child(7)").each(function () {
            headcountFour += parseFloat($(this).val());
            isheadcountFour = true;
        });


        if (headcountFour.toFixed(0) != 100 && isheadcountFour == true) {
            alert("后期服务-专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountFour = 0;
        var isauditcountFour = false;
        $("input", "#gvProjectValueFourBymember  tr td:nth-child(10)").each(function () {
            auditcountFour += parseFloat($(this).val());
            isauditcountFour = true;
        });

        if (auditcountFour.toFixed(0) != 100 && isauditcountFour == true) {
            alert("后期服务-审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountFour = 0;
        var isproofreadcountCheckFour = false;
        $("input", "#gvProjectValueFourBymember  tr td:nth-child(13)").each(function () {
            proofreadcountFour += parseFloat($(this).val());
            isproofreadcountCheckFour = true;
        });


        if (proofreadcountFour.toFixed(0) != 100 && isproofreadcountCheckFour == true) {
            alert("后期服务-校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }

    if (cprProcess > 3 && cprProcess != 10) {

        var valueemputyFour = "0";
        $.each($("input", "#gvProjectValueBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyFour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyFour).length == 0) {
            alert("项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var designcountFour = 0;
        var isCheckFour = false;
        $("input", "#gvProjectValueBymember tr td:nth-child(4)").each(function () {
            designcountFour += parseFloat($(this).val());
            isCheckFour = true;
        });



        if (designcountFour.toFixed(0) != 100 && isCheckFour == true) {
            alert("设计人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var headcountFour = 0;
        var isheadcountFour = false;
        $("input", "#gvProjectValueBymember  tr td:nth-child(7)").each(function () {
            headcountFour += parseFloat($(this).val());
            isheadcountFour = true;
        });


        if (headcountFour.toFixed(0) != 100 && isheadcountFour == true) {
            alert("专业负责人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var auditcountFour = 0;
        var isauditcountFour = false;
        $("input", "#gvProjectValueBymember  tr td:nth-child(10)").each(function () {
            auditcountFour += parseFloat($(this).val());
            isauditcountFour = true;
        });

        if (auditcountFour == 0 && isauditcountFour == true) {
            alert("审批人产值分配比例不能等于0%");
            flag = false;
            return false;
        }

        if (auditcountFour.toFixed(0) != 100 && isauditcountFour == true) {
            alert("审批人产值分配比例不等于100%！");
            flag = false;
            return false;
        }

        var proofreadcountFour = 0;
        var isproofreadcountCheckFour = false;
        $("input", "#gvProjectValueBymember  tr td:nth-child(13)").each(function () {
            proofreadcountFour += parseFloat($(this).val());
            isproofreadcountCheckFour = true;
        });

        if (proofreadcountFour.toFixed(0) != 100 && isproofreadcountCheckFour == true) {
            alert("校对人产值分配比例不等于100%！");
            flag = false;
            return false;

        }
    }
    return flag;
}

//方案比例
function ArrayProgramPercent() {

    var arrayProgramPercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(0) td:eq(1)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(1)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(0) td:eq(3)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(3)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(0) td:eq(5)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(5)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(0) td:eq(7)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(7)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    return arrayProgramPercent;
}


//初步比例

function ArrayPreliminaryPercent() {

    var arrayPreliminaryPercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(1) td:eq(1)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(1)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(1) td:eq(3)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(3)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(1) td:eq(5)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(5)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(1) td:eq(7)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(7)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    return arrayPreliminaryPercent;

}

//工图比例

function ArrayWorkDrawPercent() {

    var arrayWorkDrawPercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(2) td:eq(1)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(1)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(2) td:eq(3)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(3)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(2) td:eq(5)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(5)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(2) td:eq(7)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(7)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    return arrayWorkDrawPercent;
}

//后期
function ArrayLateStagePercent() {


    var arrayLateStagePercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(3) td:eq(1)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(1)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(3) td:eq(3)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(3)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(3) td:eq(5)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(5)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(3) td:eq(7)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(7)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    return arrayLateStagePercent;
}

//其他
function ArrayOtherPercent() {


    var arrayOtherPercent = new Array();


    if ($("#gvFive tr:eq(0) td:eq(1)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(1)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    if ($("#gvFive tr:eq(0) td:eq(3)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(3)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    if ($("#gvFive tr:eq(0) td:eq(5)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(5)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    if ($("#gvFive tr:eq(0) td:eq(7)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(7)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    return arrayOtherPercent;
}

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}