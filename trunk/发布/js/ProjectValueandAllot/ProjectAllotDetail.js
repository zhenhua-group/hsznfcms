﻿//项目设计费用分配类
function ProjectAllotDetail() {
    this.Dom = {};
    this.BackgroundInvoke = TG.Web.ProjectFeeAllot;
    this.ProjectSysNo = $("#HiddenProjectSysNo").val();
    this.CoperationSysNo = $("#HiddenCoprationSysNo").val();
    this.ProcessType = $("#HiddenProcessType").val();  //操作预览类型 0表示为新规、1表示为编辑、2表示为预览
    this.Status = $("#HiddenPartOneStatus").val();
    this.PartOneSysNo = $("#HiddenPartOneSysNo").val();
    this.HasPower = $("#HiddenHasPower").val();
    this.PartOneRecordInUserSysNo = $("#HiddenPartOneRecordInUserSysNo").val();
    this.IsAllot = $("#HiddenIsAllot").val();

    //第一部分Container
    this.Dom.PartOneDivContainer = $("#partOneDivContainer");
    //第二部分Container
    this.Dom.PartTwoDivContainer = $("#partTwoDivContainer");

    //第一部分保存按钮
    this.Dom.PartOneSaveButton = $("#partOneSaveButton", this.Dom.PartOneDivContainer);
    //第二部分保存按钮
    this.Dom.PartTwoSaveButton = $("#partTwoSaveButton", this.Dom.PartTwoDivContainer);

    var Instance = this;

    //第一部分情报取得
    this.GetRecordFromPartOne = function () {
        var result = {};
        result.SysNo = $("#HiddenPartOneSysNo").val();
        result.ProjectSysNo = Instance.ProjectSysNo;
        result.DesignNum = $("#txtdesignNo").text();
        result.BuildArea = $("#partOneBuildAreatext").text();
        result.CurChange = $("#partOnecprCharge").val();
        result.CprUnitprice = $("#partOnecprunitpricetext").val();
        result.CurChange = $("#partOneCurChangetext").val();
        result.CurRate = $("#partOneCurRatetext").val();
        result.TaxRate = $("#partOneTaxRatetext").val();
        result.LowDivUnitArea = $("#partOneLowDivUnitAreatext").val();
        result.LowDivUnitPrice = $("#partOneLowDivUnitPricetext").val();
        result.LowRateUnitArea = $("#partOneLowRateUnitAreatext").val();
        result.LowRateUntiPrice = $("#partOneLowRateUnitPricetext").val();
        result.CprCharge = $("#partOnecprCharge").val();
        result.Status = "A";
        result.InDate = $("#txtdate").val();
        //结构类型
        result.StrucType = $("#drp_strutype").val();
        return result;
    }

    //第二部分情报取得
    this.GetRecordFromPartTwo = function () {
        var partTwo = {};
        partTwo.TaxSignel = $("#partTwoTaxSigneltext").val();
        partTwo.TaxSignelRate = $("#partTwoTaxSignelRatetext").val();
        partTwo.TaxSignelUser = $("#partTwoTaxSignelUsertext").val();

        partTwo.CollegeTax = $("#partTwoCollegeTaxtext").val();
        partTwo.CollegeRate = $("#partTwoCollegeRatetext").val();
        partTwo.CollegeUser = $("#partTwoCollegeUsertext").val();

        partTwo.ProjectTax = $("#partTwoProjectTaxtext").val();
        partTwo.ProjectRate = $("#partTwoProjectRatetext").val();
        partTwo.ProjectUser = $("#partTwoProjectUsertext").val();

        partTwo.ProChgTax = $("#partTwoProChgTaxtext").val();
        partTwo.ProChgRate = $("#partTwoProChgRatetext").val();
        partTwo.ProChgUser = $("#partTwoProChgUsertext").val();

        partTwo.RegisterTax = $("#partTwoRegisterTaxtext").val();
        partTwo.RegisterRate = $("#partTwoRegisterRatetext").val();
        partTwo.RegisterUser = $("#partTwoRegisterUsertext").val();

        partTwo.ShexTax = $("#partTwoShexTaxtext").val();
        partTwo.ShexRate = $("#partTwoShexRatetext").val();
        partTwo.ShexUser = $("#partTwoShexUsertext").val();

        partTwo.ShedTax = $("#partTwoShedTaxtext").val();
        partTwo.ShedRate = $("#partTwoShedRatetext").val();
        partTwo.ShedUser = $("#partTwoShedUsertext").val();

        partTwo.ZonggongTax = $("#partTwoZonggongTaxtext").val();
        partTwo.ZonggongRate = $("#partTwoZonggongRatetext").val();
        partTwo.ZonggongUser = $("#partTwoZonggongUsertext").val();

        partTwo.YuanzTax = $("#partTwoYuanzTaxtext").val();
        partTwo.YuanzRate = $("#partTwoYuanzRatetext").val();
        partTwo.YuanzUser = $("#partTwoYuanzUsertext").val();

        partTwo.SumTax = $("#partTwoSumTaxtext").val();
        partTwo.SumRate = $("#partTwoSumRatetext").val();
        partTwo.PartOneSysNo = Instance.PartOneSysNo;
        return partTwo;
    }

    //partOne Save event
    Instance.Dom.PartOneSaveButton.click(function () {
        if (Instance.Validation(1)) {
            //保存情报
            var result = Instance.BackgroundInvoke.SavePartOneRecord(Global.toJSON(Instance.GetRecordFromPartOne()));

            if (result.value != null && parseInt(result.value, 10) > 0) {
                $("#HiddenPartOneSysNo").val(result.value);
                alert("数据保存成功，已经发送消息到下一环节！");
                Instance.RedirectToList();
            } else {
                alert("保存失败！");
            }
        }
    });

    //partTwo Save event
    Instance.Dom.PartTwoSaveButton.click(function () {
        if (Instance.Validation(2)) {

            //保存情报
            var result = Instance.BackgroundInvoke.SavePartTwoRecord(Global.toJSON(Instance.GetRecordFromPartTwo()));

            if (result.value != null && parseInt(result.value, 10) > 0) {
                $("#HiddenPartOneSysNo").val(result.value);
                alert("数据保存完成，已经发送消息到下一环节！");
                Instance.RedirectToList();
            } else {
                alert("保存失败！");
            }
        }
    });

    //禁用所有控件 
    this.DisableAllControl = function (container) {
        $(":text", container).attr("disabled", true);
        $(":button", container).attr("disabled", true);
        $("textarea", container).attr("disabled", true);
    }
    //禁用部分空间
    this.UnDisabledSomeControl = function (id) {
        $("#" + id + "").attr("disabled", false);
        $("#" + id + "").val("");
    }
    //Validation
    this.Validation = function (a) {
        if (a == 1) {
            var datepub = $("#txtdate").val();
            if (datepub == "") {
                alert("请填写出图日期！");
                return false;
            }
            var textbox1 = $("#partOnecprunitpricetext");
            if (textbox1[0].value == "") {
                textbox1[0].focus();
                alert("请填写必填项！");
                return false;
            }

            var textbox2 = $("#partOneCurChangetext");
            if (textbox2[0].value == "") {
                textbox2[0].focus();
                alert("请填写必填项！");
                return false;
            }

            var drp_list = $("#drp_strutype").val();
            if (drp_list == "-1") {
                alert("请选择结构类型！");
                return false;
            }

            var textbox3 = $("#partOneTaxRatetext");
            if (textbox3[0].value == "") {
                textbox3[0].focus();
                alert("请填写必填项！");
                return false;
            }

            var textbox4 = $("#partOneLowDivUnitAreatext");
            if (textbox4[0].value == "") {
                textbox4[0].focus();
                alert("请填写必填项！");
                return false;
            }
        }
        else if (a == 2) {
            var textbox5 = $("#partTwoTaxSigneltext");
            if (textbox5[0].value == "") {
                textbox5[0].focus();
                alert("请填写必填项！");
                return false;
            }
        }
        return true;
    }

    //初始化方法
    this.Init = function () {
        if (Instance.HasPower == "0") {
            alert("您没有权限填写该项！");
            Instance.Dom.PartOneDivContainer.hide();
            return false;
        }
        switch (Instance.Status) {
            case "A":
                break;
            case "B":
                Instance.DisableAllControl(Instance.Dom.PartOneDivContainer);
                Instance.Dom.PartTwoDivContainer.show();
                break;
            case "C":
                Instance.Dom.PartTwoDivContainer.show();
                Instance.DisableAllControl(Instance.Dom.PartOneDivContainer);
                Instance.DisableAllControl(Instance.Dom.PartTwoDivContainer);
                window.location.href = "/ProjectValueandAllot/ProjAllotDetailsTable.aspx?cprid=" + Instance.CoperationSysNo + "&proid=" + Instance.ProjectSysNo + "&costid=" + Instance.PartOneSysNo + "&PartOneRecordInUserSysNo=" + Instance.PartOneRecordInUserSysNo;
                break;
        }
        //if (Instance.ProcessType == "0") {
        //    //新规的场合
        //    Instance.Dom.PartOneDivContainer.show();
        //    //判断是否二次分配
        //    if (Instance.IsAllot == "1") {
        //        Instance.DisableAllControl(Instance.Dom.PartOneDivContainer);
        //        $("#partOnecprunitpricetext").hide();
        //        $("#sp_partOnecprunitpricetext").show();
        //        $("#partOneSaveButton").attr("disabled", false);
        //        Instance.UnDisabledSomeControl("partOneCurChangetext");
        //        Instance.UnDisabledSomeControl("partOneTaxRatetext");
        //        Instance.UnDisabledSomeControl("partOneLowDivUnitAreatext");
        //        Instance.UnDisabledSomeControl("txtdate");
        //    }
        //} else if (Instance.ProcessType == "1") {
        //    //编辑的场合
        //    Instance.Dom.PartOneDivContainer.show();

        //    //检查是否有权限
        //    if (Instance.Status == "A" && Instance.HasPower == "1") {
        //        Instance.DisableAllControl(Instance.Dom.PartOneDivContainer);
        //        Instance.Dom.PartTwoDivContainer.show();
        //    } else if (Instance.Status == "B" && Instance.HasPower == "1") {
        //        //第二部分新规完
        //        Instance.DisableAllControl(Instance.Dom.PartOneDivContainer);
        //        Instance.DisableAllControl(Instance.Dom.PartTwoDivContainer);
        //        Instance.Dom.PartTwoDivContainer.show();
        //        window.location.href = "/ProjectValueandAllot/ProjAllotDetailsTable.aspx?cprid=" + Instance.CoperationSysNo + "&proid=" + Instance.ProjectSysNo + "&costid=" + Instance.PartOneSysNo + "&PartOneRecordInUserSysNo=" + Instance.PartOneRecordInUserSysNo;
        //    } else if (Instance.Status == "C" && Instance.HasPower == "1") {
        //        //第三部分新规完
        //        window.location.href = "/ProjectValueandAllot/ShowProjAllotDetailsTable.aspx?proid=" + Instance.ProjectSysNo + "&cprid=" + Instance.CoperationSysNo + "&costid=" + Instance.PartOneSysNo + "&PartOneRecordInUserSysNo=" + Instance.PartOneRecordInUserSysNo;
        //    }
        //}
    }

    this.CutUpTextBox = function (textBoxArray, bindAttributeFlag) {
        if (bindAttributeFlag) {
            //得到所有专业
            var specialityListResult = Instance.BackgroundInvoke.GetAllSpeciality();

            var specialityList = Global.evalJSON(specialityListResult.value);

            var rsultObject = new Object();

            $.each(specialityList, function (index, item) {
                var parameterTextBoxArray = textBoxArray.filter("[containername=" + item + "]");
                rsultObject[item] = Instance.PickDataFromTextBox(parameterTextBoxArray, item, bindAttributeFlag);
            });
            return rsultObject;
        }
        else {
            alert("绑定标识不能为null");
            return false;
        }
    }

    this.PickDataFromTextBox = function (subItemTextBox, containerName, bindAttributeFlag) {
        var resultArray = new Array();
        $.each(subItemTextBox, function (index, item) {
            var objData = {
                "DesignPercent": $.trim(item.value),
                "Money": $.trim($(":text[bindattribute=money_" + bindAttributeFlag + "][containername=" + containerName + "]:eq(" + index + ")").val()),
                "Member": $.trim($(":text[bindattribute=employee_" + bindAttributeFlag + "][containername=" + containerName + "]:eq(" + index + ")").val()),
                "Signature": " "
            };
            resultArray[index] = objData;
        });
        return resultArray;
    }

    //查询项目所有人员
    this.GetAllotUserListAndFillTable = function () {
        var allotUserListString = Instance.BackgroundInvoke.GetAllotUsers(parseInt(Instance.ProjectSysNo)).value;
        if (allotUserListString != null && allotUserListString.length > 0) {
            var allotUserList = Global.evalJSON(allotUserListString);
        }
    }

    this.RedirectToList = function () {
        window.location.href = "/ProjectValueandAllot/ProValueandAlltList.aspx";
    }

    //调用初始化方法
    Instance.Init();
}


