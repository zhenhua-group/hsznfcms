﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var isTrunEconomy;
var isTrunHavc;
$(function () {
    CommonControl.SetFormWidth();


    $("#CoperationBaseInfo tr:odd").css({ background: "White" });
    //是否转经济所
    var IsTrunEconomy = $("#HiddenIsTrunEconomy").val();
    isTrunEconomy = IsTrunEconomy;
    if (IsTrunEconomy == 0) {
        $("#txt_EconomyValuePercent").attr("disabled", "disabled");
        $("#txt_EconomyValuePercent").attr("class", "TextBoxStyle");
    }

    //部门
    var unitName = $("#lblcpr_Unit").text();

    //转暖通
    var IsTrunHavc = $("#HiddenIsTrunHavc").val();
    isTrunHavc = IsTrunHavc;
    if (IsTrunHavc == 0 || unitName.indexOf("暖通热力所") > -1) {
        $("#txt_HavcValuePercent").attr("disabled", "disabled");
        $("#txt_HavcValuePercent").attr("class", "TextBoxStyle");
    }

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    //项目阶段
    var cprProcess = $("#HiddenCoperationProcess").val();

    //合同ID
    var cprId = $("#HiddenCprID").val();

    //年份变化
    $("#drp_year").change(function () {

        var year = $("#drp_year").children("option:selected").val();

        $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "4", "year": year, "cprId": cprId }, function (jsonResult) {

            var payshiAcount = jsonResult;
            $("#lblPayShiCount").text(payshiAcount);
            var allotCount = $("#lblAllotAccount").text();
            var notallotCount = parseFloat(payshiAcount) - parseFloat(allotCount);
            $("#lblNotAllotAccount").text(notallotCount.toFixed(2));

            CalculationValue();

        });
    });


    //设总
    $("#txt_DesignManagerPercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#txt_DesignManagerPercent").val())) {
            alert("设总比例请输入数字！");
//            return false;
        }

        if ($.trim($("#txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
//            return false;
        }

        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("设总比例不能大于100%！");
//            return false;
        }

        CalculationValue();
        return false;
    });

    //转经济所
    $("#txt_EconomyValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#txt_EconomyValuePercent").val())) {
            alert("转经济所产值比例请输入数字！");
//            return false;
        }

        if ($.trim($("#txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
//            return false;
        }

        if (parseFloat($("#txt_EconomyValuePercent").val()) > 100) {
            alert("转经济所产值比例不能大于100%！");
//            return false;
        }

        CalculationValue();
        return false;
    });


    //转暖通
    $("#txt_HavcValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#txt_HavcValuePercent").val())) {
            alert("转暖通产值比例请输入数字！");
            return false;
        }

        if ($.trim($("#txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }

        if (parseFloat($("#txt_HavcValuePercent").val()) > 100) {
            alert("转暖通产值比例不能大于100%！");
            return false;
        }

        CalculationValue();


    });

    //转其他部门
    $("#txtOtherDeptValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#txtOtherDeptValuePercent").val())) {
            alert("转出其他部门产值比例请输入数字！");
            return false;
        }
        if ($.trim($("#txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }
        if (parseFloat($("#txtOtherDeptValuePercent").val()) > 100) {
            alert("转出其他部门比例不能大于100%！");
            return false;
        }

        CalculationValue();


    });


    //本部门自留产值
    $("#txtTheDeptValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            return false;
        }
        if ($.trim($("#txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }
        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            return false;
        }

        CalculationValue();


    });

    //扣发金额
    $("#txt_ProgramCount").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#txt_ProgramCount").val())) {
            alert("扣发产值金额请输入数字！");
            return false;
        }
        if ($.trim($("#txt_ProgramCount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }
        CalculationValue();

    });


    //分配金额
    $("#txt_AllotAccount").change(function () {
        if (!SelectYear()) {
            return false;
        }

        //分配金额
        if (!reg.test($("#txt_AllotAccount").val())) {
            alert("请输入数字！");
            return false;
        }

        var allotAccount = $("#txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#txt_AllotAccount").val());

        if (parseFloat(allotAccount) > parseFloat($("#lblNotAllotAccount").text())) {
            alert("请确认分配金额不超过未分配金额！");
            return false;
        }

        CalculationValue();

    });



    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

    //审核通过按钮
    $("#btnApproval").click(function () {

        if (!SelectYear()) {
            return false;
        }

        //分配金额
        if (!reg.test($("#txt_AllotAccount").val())) {
            this.focus();
            alert("分配金额请输入数字！");
            return false;
        }

        if ($.trim($("#txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }
        var allotAccount = $("#txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#txt_AllotAccount").val());

        if (parseFloat(allotAccount) > parseFloat($("#lblNotAllotAccount").text())) {
            alert("请确认分配金额不超过未分配金额！");
            return false;
        }

        if (!reg.test($("#txt_DesignManagerPercent").val())) {
            alert("设总比例请输入数字！");
            return false;
        }

        if ($.trim($("#txt_DesignManagerPercent").val()).length == 0) {
            alert("设总比例不能为空！");
            return false;
        }

        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("设总比例不能大于100%！");
            return false;
        }

        //转暖通
        if (IsTrunEconomy == 1) {

            if (!reg.test($("#txt_EconomyValuePercent").val())) {
                alert("转经济所产值比例请输入数字！");
                return false;
            }
            if ($.trim($("#txt_EconomyValuePercent").val()).length == 0) {
                alert("转经济所产值不能为空！");
                return false;
            }
            if (parseFloat($("#txt_EconomyValuePercent").val()) > 100) {
                alert("转经济所产值比例不能大于100%！");
                return false;
            }
        }

        //转暖通
        if (unitName.indexOf("暖通") < -1) {
            if (IsTrunHavc == 1) {
                if (!reg.test($("#txt_HavcValuePercent").val())) {
                    alert("转暖通产值比例请输入数字！");
                    return false;
                }
                if ($.trim($("#txt_HavcValuePercent").val()).length == 0) {
                    alert("转暖通产值不能为空！");
                    return false;
                }
                if (parseFloat($("#txt_HavcValuePercent").val()) > 100) {
                    alert("转暖通产值比例不能大于100%！");
                    return false;
                }
            }
        }

        //转其他部门
        if (!reg.test($("#txtOtherDeptValuePercent").val())) {
            alert("转出其他部门产值比例请输入数字！");
            return false;
        }
        if ($.trim($("#txtOtherDeptValuePercent").val()).length == 0) {
            alert("转出其他部门产值不能为空！");
            return false;
        }

        if (parseFloat($("#txtOtherDeptValuePercent").val()) > 100) {
            alert("转出其他部门比例不能大于100%！");
            return false;
        }

        //本部门自留产值
        if (!reg.test($("#txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            return false;
        }

        if ($.trim($("#txtTheDeptValuePercent").val()).length == 0) {
            alert("本部门自留产值不能为空！");
            return false;
        }
        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            return false;
        }

        //方案产值
        if (!reg.test($("#txt_ProgramCount").val())) {
            alert("扣发产值请输入数字！");
            return false;
        }

        if ($.trim($("#txt_ProgramCount").val()).length == 0) {
            alert("扣发产值不能为空！");
            return false;
        }

        if (parseFloat($("#txt_UnitValueCountPercent").val()) < 0) {
            alert("本部门产值比例不能为负数！");
            return false;
        }

        if (parseFloat($("#txt_ShouldBeValueCount").text()) <= 0) {
            alert("应分产值比例不能为负数！");
            return false;
        }

        $("#btnApproval").attr("disabled", true);

        var ViewEntity = {
            "pro_ID": $("#HiddenProSysNo").val(),
            "AuditSysID": $("#HiddenAuditRecordSysNo").val(),
            "AllotCount": $("#txt_AllotAccount").val(),
            "AllotUser": $("#HiddenLoginUser").val(),
            "Payshicount": $("#lblPayShiCount").text(),
            "PaidValuePercent": $("#txt_PaidValuePercent").val(),
            "PaidValueCount": $("#txt_PaidValueCount").text(),
            "DesignManagerPercent": $("#txt_DesignManagerPercent").val(),
            "DesignManagerCount": $("#txt_DesignManagerCount").text(),
            "AllotValuePercent": $("#txtAllotValuePercent").val(),
            "Allotvaluecount": $("#txtAllotValueCount").text(),
            "UnitValuePercent": $("#txt_UnitValueCountPercent").val(),
            "UnitValueCount": $("#txtUnitValueCount").text(),
            "EconomyValuePercent": $("#txt_EconomyValuePercent").val(),
            "EconomyValueCount": $("#txt_EconomyValueCount").text(),
            "Itemtype": "",
            "Otherdeptallotpercent": $("#txtOtherDeptValuePercent").val(),
            "Otherdeptallotcount": $("#txtOtherDeptValueCount").text(),
            "Thedeptallotpercent": $("#txtTheDeptValuePercent").val(),
            "Thedeptallotcount": $("#txtTheDeptValueCount").text(),
            "IsTrunEconomy": $("#HiddenIsTrunEconomy").val(),
            "IsTrunHavc": $("#HiddenIsTrunHavc").val(),
            "HavcValuePercent": $("#txt_HavcValuePercent").val(),
            "HavcValueCount": $("#txt_HavcValueCount").text(),
            "ProgramPercent": $("#txt_ProgramPercent").val(),
            "ProgramCount": $("#txt_ProgramCount").val(),
            "ShouldBeValuePercent": $("#txt_ShouldBeValuePercent").val(),
            "ShouldBeValueCount": $("#txt_ShouldBeValueCount").text(),
            "SecondValue": "1",
            "ActualAllountTime": $("#drp_year").val()
        };

        var jsonObj = Global.toJSON(ViewEntity);
        jsonDataEntity = jsonObj;

        //申请分配
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });


    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val()
        };
        var jsonObj = Global.toJSON(auditObj);
        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "1", "data": jsonObj }, null, null, function (jsonResult) {
            if (jsonResult < 0) {
                alert("审核失败！");
            } else {
                //查询系统新消息
                if ($("#lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
                    window.location.href = "/ProjectValueandAllot/ProValueandAlltList.aspx";
                }
                else {
                    window.location.href = "/ProjectValueandAllot/HaveProjectValueAllotList.aspx";
                }
            }
        });
    });



    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {

                var _$szmesUser = $(":checkbox[roles='所长']:checked");
                if (_$szmesUser.length == 0) {
                    alert("所长审批，请至少选择一个审批人！");
                    return false;
                }
                if (isTrunEconomy == 1) {
                    var _$jjsmesUser = $(":checkbox[roles='经济所']:checked");
                    if (_$jjsmesUser.length == 0) {
                        alert("经济所审批，请至少选择一个审批人！");
                        return false;
                    }
                }
                if ($("#lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
                    if (isTrunHavc == 1) {
                        var _$ntsmesUser = $(":checkbox[roles='暖通所']:checked");
                        if (_$ntsmesUser.length == 0) {
                            alert("暖通所审批，请至少选择一个审批人！");
                            return false;
                        }
                    }
                }
                getUserAndUpdateAudit('0', '1', jsonDataEntity);
            },
            "关闭": function () {
                $("#btnApproval").attr("disabled", false);
                messageDialog.hide();
            }
        }
    });
    sendMessageClass = new ProjectValueMessageCommon(messageDialog);
});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {

        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("产值分配完成，已全部通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_CorperationList.aspx";
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        alert("消息发送成功,等待下一阶段审核人审批！");
        //查询系统新消息
        if ($("#lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
            window.location.href = "/ProjectValueandAllot/ProValueandAlltList.aspx";
        }
        else {
            window.location.href = "/ProjectValueandAllot/HaveProjectValueAllotList.aspx";
        }
    } else {
        alert("消息发送失败！");
    }
}

//计算产值
function CalculationValue() {

    //分配金额
    var allotAccount = $("#txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#txt_AllotAccount").val());

    //实收产值(实)
    var paidValuePercent = $("#txt_PaidValuePercent").val().length == 0 ? 0 : parseFloat($("#txt_PaidValuePercent").val());
    var paidValueCount = parseFloat(paidValuePercent) * parseFloat(allotAccount) * 100;
    $("#txt_PaidValueCount").text(paidValueCount.toFixed(0));

    //设总比例
    var designManagerPercent = $("#txt_DesignManagerPercent").val().length == 0 ? 0 : parseFloat($("#txt_DesignManagerPercent").val());
    //设总金额(设)
    var DesignManagerCount = parseFloat(designManagerPercent) * parseFloat(allotAccount) * 100;
    $("#txt_DesignManagerCount").text(DesignManagerCount.toFixed(0));

    //转其他部门产值比例
    var otherCountPercent = $("#txtOtherDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#txtOtherDeptValuePercent").val());
    //转其他部门产值(其他)
    var otherCount = parseFloat(otherCountPercent) * parseFloat(allotAccount) * 100;
    $("#txtOtherDeptValueCount").text(otherCount.toFixed(0));

    //转经济所比例（经）
    var economy = $("#txt_EconomyValuePercent").val().length == 0 ? 0 : parseFloat($("#txt_EconomyValuePercent").val());
    var economyCount = 0;
    if (parseFloat(economy) == 0.25) {
        var economyCountTemp = parseFloat(economy) * parseFloat(allotAccount) * 100;
        if (parseFloat(economyCountTemp) < 200) {
            economyCount = 200;
        } else {
            economyCount = parseFloat(economy) * parseFloat(allotAccount) * 100;
        }
    }
    else {
        //转经济产值
        economyCount = parseFloat(economy) * parseFloat(allotAccount) * 100;
    }
    if (isTrunEconomy == 1) {
        $("#txt_EconomyValueCount").text(economyCount.toFixed(0));
    }

    //转暖通(暖)
    var havcValuePercent = $("#txt_HavcValuePercent").val().length == 0 ? 0 : parseFloat($("#txt_HavcValuePercent").val());
    //转暖通
    var havcValueCount = parseFloat(havcValuePercent) * (parseFloat(allotAccount) * 10000 - parseFloat(economyCount) - parseFloat(DesignManagerCount)) / 100;
    if (isTrunHavc == 1) {
        $("#txt_HavcValueCount").text(havcValueCount.toFixed(0));
    } else {
        $("#txt_HavcValuePercent").val("0.00");
    }

    //扣发金额（扣发）
    var programValueCount = $("#txt_ProgramCount").val().length == 0 ? 0 : parseFloat($("#txt_ProgramCount").val());
    //扣发比例
    var programValuePercent = 0;
    if (parseFloat(allotAccount) == 0) {
        programValuePercent = 0;
    }
    else {
        programValuePercent = parseFloat(programValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#txt_ProgramPercent").attr("value", programValuePercent.toFixed(2));

    //分配产值（分配产值）
    var AllotValuePercent = 100 - parseFloat(designManagerPercent);
    $("#txtAllotValuePercent").attr("value", AllotValuePercent);
    var allotValueCount = parseFloat(AllotValuePercent) * parseFloat(allotAccount) * 100;
    $("#txtAllotValueCount").text(allotValueCount.toFixed(0));

    // 100%-转经济所 -暖通 -转经济 -其他 =allotCount_A
    var allotCount_A_Prt = paidValuePercent - economy - havcValuePercent - otherCountPercent;
    var allotCount_A = parseFloat(allotAccount) * 10000 - parseFloat(economyCount) - parseFloat(havcValueCount) - parseFloat(otherCount);

    //本部门自留百分比
    var theDeptValuePercent = $("#txtTheDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#txtTheDeptValuePercent").val());
    //本部门自留产值（自留）
    var theDeptValueCount = parseFloat(theDeptValuePercent) * parseFloat(allotCount_A) * 0.01;
    $("#txtTheDeptValueCount").text(theDeptValueCount.toFixed(0));

    // 本部门产值合计 等于 allotCount_A-自留
    var allotCount_B = allotCount_A - parseFloat(theDeptValueCount)
    var unitValueCountCount = allotCount_B; //parseFloat(allotAccount) * 10000 - parseFloat(economyCount) - parseFloat(otherCount) - parseFloat(havcValueCount) - parseFloat(theDeptValueCount);
    $("#txtUnitValueCount").text(unitValueCountCount.toFixed(0));

    //本部门产值合计比例 
    var allotCount_B_Prt = allotCount_A_Prt - parseFloat(allotCount_A_Prt * theDeptValuePercent * 0.01);
    //var unitValueCountPercent = allotCount_B_Prt;
    var unitValueCountPercent = 0;
    if (parseFloat(allotAccount) == 0) {
        unitValueCountPercent = 0;
    } else {
        unitValueCountPercent = parseFloat(unitValueCountCount) / (parseFloat(allotAccount) * 100);
    }
    $("#txt_UnitValueCountPercent").attr("value", unitValueCountPercent.toFixed(2));

    //应分产值金额 等于分配产值 -转经济所-转暖通-转其他部门-本部门自留产值-设总-扣发
    var shouleBeValueCount = parseFloat(allotCount_B) - parseFloat(DesignManagerCount) - parseFloat(programValueCount);
    $("#txt_ShouldBeValueCount").text(shouleBeValueCount.toFixed(0));

    // 应分产值比例 等于100% -转经济所-转暖通-转其他部门-本部门自留产值-设总-扣发
    //var shouldBeValuePercent = parseFloat(allotCount_B_Prt) - parseFloat(designManagerPercent) - parseFloat(programValuePercent);
    var shouldBeValuePercent = 0;
    if (parseFloat(allotAccount) == 0) {
        shouldBeValuePercent = 0;
    } else {
        shouldBeValuePercent = parseFloat(shouleBeValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#txt_ShouldBeValuePercent").attr("value", shouldBeValuePercent.toFixed(2));
}

//选择年份
function SelectYear() {

    var flag = true;
    if ($("#drp_year").val() == "-1") {
        alert("请选择分配年份");
        flag = false;
        return false;
    }
    return flag;
}


