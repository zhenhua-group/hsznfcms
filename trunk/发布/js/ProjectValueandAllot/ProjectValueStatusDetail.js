﻿
var jsonDataEntity;
var actionFlag;
var IsTrunEconomys;
var IsTrunHavc;
var IsEdit;
$(function () {

    CommonControl.SetFormWidth();

    //Tab页
    $('#tabs').tabs({ cookie: { expires: 30 } });
    $("#tabsMem").tabs({ cookie: { expires: 30 } });
    $("#tabsProcess").tabs({ cookie: { expires: 30 } });
    $("#tabsMemAmount").tabs({ cookie: { expires: 30 } });

    $("input", $("#CoperationBaseInfo")).attr("disabled", true);
    $("#CoperationBaseInfo tr:odd").attr("style", "background-color:#FFF;");


    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    //当前合同审核记录
    var auditRecordStatus = $("#AuditRecordStatus").val();

    //检查用户权限
    var hasPower = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.CheckPower(auditRecordStatus, $("#HiddenLoginUser").val());

    //承接部门
    var unitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();

 

    //用户部门
    var userUnitName = "";

    if (auditRecordStatus == 'B' || auditRecordStatus == 'D' || auditRecordStatus == 'F' || auditRecordStatus == 'A' || auditRecordStatus == 'E' || auditRecordStatus == 'G' || auditRecordStatus == 'C') {
        //得到用户所在的部门
        userUnitName = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.getUserUnitName();
    }

    //项目阶段
    var cprProcess = $("#HiddenCoperationProcess").val();

    //是否转经济所
    var IsTrunEconomy = $("#HiddenIsTrunEconomy").val();
    IsTrunEconomys = IsTrunEconomy;

    //是否转暖通
    var isTrunHavc = $("#HiddenIsTrunHavc").val();
    IsTrunHavc = isTrunHavc;

    //是否可以编辑
    IsEdit = $("#HiddenIsEdit").val();

    if (unitName.indexOf("暖通热力所") > -1 || IsEdit == 1) {
        $("#FiveTable  input[type=text]").attr("disabled", false);
    } else {
        $("#ctl00_ContentPlaceHolder1_gvOne input[type=text]").attr("disabled", true);
        $("#ctl00_ContentPlaceHolder1_gvTwo input[type=text]").attr("disabled", true);
        $("#ctl00_ContentPlaceHolder1_gvThree input[type=text]").attr("disabled", true);
        $("#ctl00_ContentPlaceHolder1_gvFour input[type=text]").attr("disabled", true);
    }

    //取得审核用户
    var auditUserArr = $("#HiddenAuditUser").val().split(",");
    //取得审核时间
    var auditDate = $("#HiddenAuditDate").val().split(",");

   
    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();

    if ($("#hidLikeType").val() == "detail") {
        InitViewStateDetail(auditRecordStatus, cprProcess, IsTrunEconomy, userUnitName.value, unitName);
    }
    else {
        if (messageStauts == auditRecordStatus || messageStauts == "") {
            //初始化状态
            InitViewStateTemp(auditRecordStatus, hasPower.value, cprProcess, IsTrunEconomy, userUnitName.value, unitName);
        } else {
            // B D F 做特殊处理
            if ((auditRecordStatus == 'B' || auditRecordStatus == 'D' || auditRecordStatus == 'A')) {
                //初始化状态
                InitViewStateTemp(auditRecordStatus, hasPower.value, cprProcess, IsTrunEconomy, userUnitName.value, unitName);
            } else {
                //显示状态
                InitViewMessageState(messageStauts, cprProcess, IsTrunEconomy, auditRecordStatus, userUnitName.value, unitName);
            }
        }
    }

    //判断是否删除
    if ($("#tbdelete tr td").text() == "此信息已被删除!") {
        $("#MainDiv").children().hide();
        $("#btnEditALL").hide();
    }

    //填充第一位审核人和审核时间-生产经营部
    $("#AuditUser", $("#OneTable")).html(auditUserArr[0] + ":<br/>" + auditDate[0]);


    //填充第2位审核人和审核时间-所长
    var twoSuggstion = $("#HiddenTwoProposal").val();
    if (twoSuggstion && twoSuggstion.length > 0) {
        $("#TwoSuggstion").val(twoSuggstion).attr("disabled", true).css("color", "black");
        var userName = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.GetUserName($("#HiddenTwoAuditUser").val());
        $("#AuditUser", $("#TwoTable")).html(userName.value + ":<br/>" + $("#HiddenTwoAuditDate").val());
    }


    //填充第3位审核人和审核时间-经济所
    var threeSuggstion = $("#HiddenThreeProposal").val();
    if (threeSuggstion && threeSuggstion.length > 0) {
        $("#ThreeSuggstion").val(threeSuggstion).attr("disabled", true).css("color", "black");
        var userName = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.GetUserName($("#HiddenThreeAuditUser").val());
        $("#AuditUser", $("#ThreeTable")).html(userName.value + ":<br/>" + $("#HiddenThreeAuditDate").val());

    }

    //填充第4位审核人和审核时间-暖通所
    var fourSuggstion = $("#HiddenFourProposal").val();
    if (fourSuggstion && fourSuggstion.length > 0) {
        $("#FourSuggstion").val(fourSuggstion).attr("disabled", true).css("color", "black");
        var userName = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.GetUserName($("#HiddenFourAuditUser").val());
        $("#AuditUser", $("#FourTable")).html(userName.value + ":<br/>" + $("#HiddenFourAuditDate").val());
    }

    //生产经营部
    //var oneSuggstion = $("#HiddenOneProposal").val();
    //if (oneSuggstion && oneSuggstion.length > 0) {
    //    $("#SCJYSuggstion").val(oneSuggstion).attr("disabled", true).css("color", "black");
    //    $("#AuditUser", $("#SCYJTable")).text(auditUserArr[1] + "     /       " + auditDate[1]);
    //}

    //填充第5位审核人和审核时间
    if (auditRecordStatus != "F") {
        $("#AuditUser", $("#FiveTable")).html(auditUserArr[1] + ":<br/>" + auditDate[1]);
    }

    //填充第6位审核人和审核时间-专业负责人
    var speHeadUser = $("#hiddenSpetityHead").val();
    if (auditRecordStatus != "H") {

        var datastring = auditDate[2];
        if (datastring != undefined) {

            var data = datastring.replace("{", "").replace("}", "").split(";");;
            var spe = speHeadUser.split(";");

            var headstring = "";

            for (var i = 0; i < data.length; i++) {
                headstring = headstring == "" ? spe[i] + "<br/>" + data[i] : headstring + " <br/> " + spe[i] + "<br/>" + data[i];
            }

            $("#AuditUser", $("#SixTable")).html(headstring);
        }

    }

    //填充第7位审核人和审核时间 -所长
    var sevenSuggstion = $("#HiddenSevenSuggsion").val();
    if (sevenSuggstion && sevenSuggstion.length > 0) {
        $("#SevenSuggstion").val(sevenSuggstion).attr("disabled", true).css("color", "black");
        $("#AuditUser", $("#SevenTable")).text(auditUserArr[3] + "     /       " + auditDate[3]);
    }

    //填充第8位审核人和审核时间-生产经营部
    var eightSuggstion = $("#HiddenEightSuggstion").val();
    if (eightSuggstion && eightSuggstion.length > 0) {
        $("#EightSuggstion").val(eightSuggstion).attr("disabled", true).css("color", "black");
        $("#AuditUser", $("#EightTable")).text(auditUserArr[4] + "     /       " + auditDate[4]);
    }

});


//审核不通过按钮和审核通过按钮不可用
function DisableButton() {
    $(":button[name=controlBtn]").attr("disabled", true);
}

//重新申请审批按钮
function AuditAgain() {
    $("#FallBackCoperaion").show();
}

//没有权限审核
function NoPowerAudit() {
    $("#NoPowerTable").show();
}

//初始化页面状态
function InitViewStateTemp(auditStatus, hasPower, process, IsTrunEconomy, userUnitName, unitName) {
    var length = 0;
    switch (auditStatus) {
        case "A":
            // $("#OneTable").show();
            scjyHide();//产经营金额隐藏 
            //如果暖通所的项目 暖通人进来审核 应该当做所长处理
            if (unitName.indexOf("暖通热力所") > -1) {
                if (userUnitName.indexOf("暖通热力所") > -1) {
                    //所长设置状态显示

                    szShow();//所长显示
                    setSzStatus();
                } else {
                    if ($.trim(userUnitName).indexOf("经济所") > -1) {
                        $("#tb_jjs").show();
                        $("#ThreeTable").show();
                    }
                }
            }
            else {
                //一次产值分配 有经济所 暖通所 所长 三种情况
                if ($.trim(userUnitName).indexOf("经济所") > -1) {
                    $("#tb_jjs").show();
                    $("#ThreeTable").show();
                } else if (userUnitName.indexOf("暖通热力所") > -1) {
                    $("#tb_nts").show();
                    $("#FourTable").show();
                } else {
                    //所长设置状态显示
                    szShow();//所长显示
                    setSzStatus();

                }
            }
            break;
            //case "C":
            //    length = 3;
            //    $("#btnApproval").hide();
            //    $("#btnRefuse").hide();
            //    DisableButton();
            //    AuditAgain();
            //    break;
        case "B":
            scjyHide();//产经营金额隐藏 
            SetJJAndNtAuditStatus(userUnitName, unitName);
            break;
        case "C":
            $("#OneTable").show();
            if ($.trim($("#HiddenTwoProposal").val()) != "") {
                $("#TwoTable").show();
                $("#tbSaveed").show();
                if ($("#hidIsTAllPass").val() == "1") {
                    $("#tbSaveed tr:eq(1)").hide();
                    $("#tbSaveed tr:eq(2)").hide();
                }
                $("#saveProcessInfo").show();
                $("#auditSuggstion").show();
            }
            if ($("#HiddenThreeProposal").val() != "") {
                $("#ThreeTable").show();
            }
            if ($("#HiddenFourProposal").val() != "") {
                $("#FourTable").show();
            }
            AuditAgain();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            break;
        case "D":
            scjyHide();//产经营金额隐藏 
            SetJJAndNtAuditStatus(userUnitName, unitName);
            break;
        case "E":
            $("#OneTable").show();
            if ($.trim($("#HiddenTwoProposal").val()) != "") {
                $("#TwoTable").show();
                $("#tbSaveed").show();
                if ($("#hidIsTAllPass").val() == "1") {
                    $("#tbSaveed tr:eq(1)").hide();
                    $("#tbSaveed tr:eq(2)").hide();
                }
                $("#saveProcessInfo").show();
                $("#auditSuggstion").show();
            }
            if ($("#HiddenThreeProposal").val() != "") {
                $("#ThreeTable").show();
            }
            if ($("#HiddenFourProposal").val() != "") {
                $("#FourTable").show();
            }
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            // setActul();
            AuditAgain();
            break;
            //case "F":

            //    $("#OneTable").show();

            //    setActul();

            //    if (IsTrunEconomy == "1") {
            //        $("#ThreeTable").show();
            //    }
            //    //如果暖通所的项目 暖通人进来审核 应该当做所长处理

            //    if (unitName.indexOf("暖通热力所") == -1) {
            //        if (IsTrunHavc == "1") {
            //            $("#FourTable").show();
            //        }
            //    }
            //    $("#SCYJTable").show();
            //    break;
            //case "G":
            //    $("#OneTable").show();
            //    // setActul();
            //    if ($.trim($("#HiddenTwoProposal").val()) != "") {
            //        $("#TwoTable").show();
            //        $("#tbSaveed").show();
            //        $("#saveProcessInfo").show();
            //        $("#auditSuggstion").show();
            //    }
            //    if (IsTrunEconomy == "1") {
            //        $("#ThreeTable").show();
            //    }
            //    //如果暖通所的项目 暖通人进来审核 应该当做所长处理
            //    if (unitName.indexOf("暖通热力所") == -1) {
            //        if (IsTrunHavc == "1") {
            //            $("#FourTable").show();
            //        }
            //    }
            //    //$("#SCYJTable").show();
            //    AuditAgain();
            //    $("#btnApproval").hide();
            //    $("#btnRefuse").hide();
            //    break;
        case "F":
            //$("#OneTable").show();
            scjyHide();//产经营金额隐藏 
            setActul();


            $("#FiveTable").show();

            SetProcess(process);

            if (IsEdit == "0") {
                $("#btnEditALL").show();
            }
            if ($("#tbNoData").text() != "") {
                $("#tbNoData").show();
                DisableButton();
            }

            break;

        case "G":
            $("#OneTable").show();
            setActul();

            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            //setProcessed(process);
            $("#btnApproval").hide();
            $("#btnRefuse").hide();

            AuditAgain();
            break;
        case "H":
            scjyHide();//产经营金额隐藏 

            $("#FiveTable").show();

            if ($("#lbl_NoData").text() != "") {
                $("#lbl_NoData").show();
            }
            setProcessed(process);

            $("input", $("#FiveTable")).attr("disabled", true);
            //专业已经审批完毕
            if ($("#tbIsAuditedBymemberAmount").text() != '') {

                $("#SixTable").show();
                $("#tbIsAuditedBymemberAmount").show();

                var userName = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.GetUserName($("#HiddenSpecialityAuditUser").val());

                $("#AuditUser", $("#SixTable")).text(userName.value);
                $("#btnApproval").hide();
                $("#btnRefuse").hide();

            } else {
                $("#memberProcessTotal").show();

                $("#SixTable").show();

                $("#tbProjectValueBymember").show();
                $("#tbChooseUser").show();
            }

            break;
        case "I":
            $("#OneTable").show();
            setActul();

            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            $("#FiveTable").show();

            if ($("#lbl_NoData").text() != "") {
                $("#lbl_NoData").show();
            }
            SetProcess(process);

            $("input", $("#FiveTable")).attr("disabled", true);
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;

        case "J":
            // $("#OneTable").show();
            setActul();
            scjyHide();//产经营金额隐藏 
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }
            $("#FiveTable").show();
            szShow();//所长显示

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            break;
        case "K":
            $("#OneTable").show();
            setActul();

            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymember").show();
            $("input", $("#SixTable")).attr("disabled", true);

            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;

        case "L":
            // $("#OneTable").show();
            setActul();
            scjyHide();//产经营金额隐藏 
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }
            $("#FiveTable").show();
            szShow();//所长显示

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();
            break;
        case "M":
            $("#OneTable").show();
            setActul();

            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymember").show();
            $("input", $("#SixTable")).attr("disabled", true);

            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
        case "N":
            $("#OneTable").show();
            setActul();
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();

            break;
        case "O":
            $("#OneTable").show();
            setActul();
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            setProcessed(process);

            $("#FiveTable").show();

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();

            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
        case "P":

            $("#OneTable").show();
            setActul();
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }
            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();

            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();

            if ($("#hiddenProlink").val() == "prolink") {
                scjyHide();//产经营金额隐藏 
                $("#OneTable").hide();
            }
            break;

        case "Q":
            $("#OneTable").show();
            setActul();
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }
            setProcessed(process);

            $("#FiveTable").show();

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();

            break;
    }

}


//初始化页面状态
//审核只看自己的
// 1:消息状态 2：审核状态
function InitViewMessageState(messageStatus, process, IsTrunEconomy, staus, userUnitName, unitName) {

    if (messageStatus == "A") {
        scjyHide();//产经营金额隐藏 
        SetJJAndNtAuditStatus(userUnitName, unitName);
        $("#addExternal").hide();
        $("#TwoSuggstion").attr("disabled", true);
    }

    if (messageStatus == "F" && staus == "I") {
        $("#FiveTable").show();
        $("input", $("#FiveTable")).attr("disabled", true);
        SetProcess(process);
    }
    //if (messageStatus == "F" && staus != "I") {
    //    $("#SCYJTable").show();
    //    $("#btnEditALL").hide();
    //    $("input", $("#SCYJTable")).attr("disabled", true);
    //}

    if (messageStatus == "F" && staus != "I") {
        $("#FiveTable").show();
        setProcessed(process);
    }

    if (messageStatus == "H" && staus != "K") {
        $("#SixTable").show();

        if ($("#gvIsAuditBymemberAmount").text() != '') {
            $("#tbIsAuditedBymemberAmount").show();
        }
        else {
            $("#tbProjectValueBymemberAmount").show();
        }
    }
    if (messageStatus == "H" && staus == "K") {
        $("#SixTable").show();

        $("#tbProjectValueBymemberNOAmount").show();
        $("input", $("#SixTable")).attr("disabled", true);

    }
    if (messageStatus == "L") {
        $("#SevenTable").show();
    }
    if (messageStatus == "N") {
        $("#EightTable").show();
    }
    $("#btnApproval").hide();
    $("#btnRefuse").hide();
    $("#btn_AllApproval").hide();
}


//设置经济所暖通所审核状态
function SetJJAndNtAuditStatus(userUnitName, unitName) {

    if ($.trim(userUnitName).indexOf("经济所") > -1) {

        if (($("#HiddenTwoIsPass").val() == "0" || $("#HiddenFourIsPass").val() == "0") && $("#HiddenThreeIsPass").val() != "1") {
            //$("#OneTable").show();
            if ($("#tbNotPass").text() != '') {
                $("#tbNotPass").show();
                $("#btnApproval").hide();
                $("#btnRefuse").hide();
            }
        } else {
            $("#tb_jjs").show();
            $("#ThreeTable").show();
        }
        //没有审批过
        if ($("#HiddenThreeProposal").val().length > 0) {
            $("#btnApproval").hide();
            $("#btnRefuse").hide();

        } else {
            // $("#OneTable").show();
        }
    } else if ($.trim(userUnitName).indexOf("暖通热力所") > -1) {

        //如果暖通所的项目
        if (unitName.indexOf("暖通热力所") > -1) {
            //只有所长审批
            if (($("#HiddenThreeIsPass").val() == "0" || $("#HiddenFourIsPass").val() == "0") && $("#HiddenTwoIsPass").val() != "1") {
                // $("#OneTable").show();
                //如果 同一级别人没有审批通过
                if ($("#tbNotPass").text() != '') {
                    $("#tbNotPass").show();
                    $("#btnApproval").hide();
                    $("#btnRefuse").hide();
                }
            } else {
                //所长设置状态显示
                szShow();//所长显示
                setSzStatus();
            }
            //没有审批过
            if ($("#HiddenTwoProposal").val().length > 0) {
                $("#tbSaveed").show();
                if ($("#hidIsTAllPass").val() == "1") {
                    $("#tbSaveed tr:eq(1)").hide();
                    $("#tbSaveed tr:eq(2)").hide();
                }
                $("#tbSave").hide();
                $("#tbSaveProcessInfo").hide();
                $("#btnEditALL").hide();
                $("#btnApproval").hide();
                $("#btnRefuse").hide();
            }
            else {
                // $("#OneTable").show();
            }
        }
        else {
            if (($("#HiddenTwoIsPass").val() == "0" || $("#HiddenThreeIsPass").val() == "0") && $("#HiddenFourIsPass").val() != "1") {
                // $("#OneTable").show();
                if ($("#tbNotPass").text() != '') {
                    $("#tbNotPass").show();
                    $("#btnApproval").hide();
                    $("#btnRefuse").hide();
                }
            } else {
                $("#tb_nts").show();
                $("#FourTable").show();
            }
            //没有审批过
            if ($("#HiddenFourProposal").val().length > 0) {
                $("#btnApproval").hide();
                $("#btnRefuse").hide();

            } else {
                //$("#OneTable").show();
            }
        }

    } else {
        if (($("#HiddenThreeIsPass").val() == "0" || $("#HiddenFourIsPass").val() == "0") && $("#HiddenTwoIsPass").val() != "1") {
            //$("#OneTable").show();
            if ($("#tbNotPass").text() != '') {
                $("#tbNotPass").show();
                $("#btnApproval").hide();
                $("#btnRefuse").hide();
            }
        } else {
            //setActul();
            //所长设置状态显示
            setSzStatus();
            szShow();//所长显示
            $("#TwoTable").show();
        }
        //没有审批过
        if ($("#HiddenTwoProposal").val().length > 0) {
            $("#tbSaveed").show();
            if ($("#hidIsTAllPass").val() == "1") {
                $("#tbSaveed tr:eq(1)").hide();
                $("#tbSaveed tr:eq(2)").hide();
            }
            $("#tbSave").hide();
            $("#tbSaveProcessInfo").hide();
            $("#btnEditALL").hide();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
        }
        else {
            // $("#OneTable").show();
        }
    }

}

function setActul() {
    $("#saveProcessInfo").show();
    $("#tbSaveed").show();
    if ($("#hidIsTAllPass").val() == "1") {
        $("#tbSaveed tr:eq(1)").hide();
        $("#tbSaveed tr:eq(2)").hide();
    }
    $("#tbSaveProcessInfo").hide();
    $("#auditSuggstion").show();
    $("#btnEditALL").hide();
    $("#TwoTable").show();
}

//所长显示
function setSzStatus() {
    $("#TwoTable").show();
    if ($("#hidIsTAllPass").val() == "1") {

        $("#saveProcessInfo").show();
        $("#tbSaveProcessInfo").show();
        if ($("#hidIsTAllPass").val() == "1") {
            $("#tbSaveProcessInfo tr:eq(1)").hide();
            $("#tbSaveProcessInfo tr:eq(2)").hide();
        }
       
        $("#tbSaveed").hide();

        $("#ctl00_ContentPlaceHolder1_drp_buildtype").attr("disabled", "disabled");
        $("#ctl00_ContentPlaceHolder1_stage").attr("disabled", "disabled");
        $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", "disabled");
        $("#auditSuggstion").show();
        $("#spanSuccessInfo").show();
        $("#addExternal").show();

        $("#btnApproval").hide();
        $("#btn_AllApproval").show();
    }
    else
    {
        if ($("#ctl00_ContentPlaceHolder1_stage").val() == "-1") {
            $("#saveProcessInfo").show();
            $("#tbSaveed").hide();
            $("#tbSaveProcessInfo").show();
            $("#btnSaveProcess").show();
            $("#auditSuggstion").hide();
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
        } else {
            $("#saveProcessInfo").show();
            $("#tbSaveProcessInfo").show();
            $("#tbSaveed").hide();
            $("#ctl00_ContentPlaceHolder1_drp_buildtype").attr("disabled", "disabled");
            $("#ctl00_ContentPlaceHolder1_stage").attr("disabled", "disabled");
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", "disabled");
            $("#auditSuggstion").show();
            $("#spanSuccessInfo").show();
            $("#addExternal").show();
        }
    }
}

//设置分配阶段
function SetProcess(process) {

    $("#file_szxs").show();
    if (process == 0) {
        $("#stagetablezero").show();
        $("#stagetableone").hide();
        $("#designProcessOneTable").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    }
    else if (process == 1) {

        $("#stagetablezero").hide();
        $("#stagetableone").show();
        $("#designProcessOneTable").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").hide();
        $("#designProcessFourTable").hide();
        $("#stagespetable").show();
    }
    else if (process == 2) {
        $("#stagetabletwo").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    }
    else if (process == 3) {
        $("#stagetablethree").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    } else if (process == 10) {
        $("#stagetableten").show();
        $("#designProcessOneTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    } else {
        $("#stagetabletfive").show();
        $("#tbOutDoor").show();
    }
}

//设置分配之后的状态
function setProcessed(process) {
    $("#file_szxs").show();
    if (process == 0) {
        $("#stageamountOne").show();
        $("#processAmountOneTable").show();
        $("#processAmountTwoTable").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    }
    else if (process == 1) {
        $("#stageamountTwo").show();
        $("#processAmountOneTable").show();
        $("#processAmountTwoTable").show();
        $("#spetamountable").show();
    }
    else if (process == 2) {
        $("#stageamountThree").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    }
    else if (process == 3) {
        $("#stageamountFour").show();
        $("#processAmountTwoTable").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    } else if (process == 10) {
        $("#stageamountTen").show();
        $("#processAmountOneTable").show();
        $("#processAmountThreeTable").show();
        $("#processAmountFourTable").show();
        $("#spetamountable").show();
    } else {
        $("#processAmountFiveTable").show();
        $("#outdooramounttable").show();
    }
}
//不是生产经营 设置金额隐藏
function scjyHide() {
    // $("#tb_project  tr[class=scjy]").hide();
}

//所长显示
function szShow() {
    $("#OneTable").show();
    $("#tb_ysfc tr:eq(0) td:eq(1)").html("");
    $("#tb_ysfc tr:eq(0) td:eq(2)").html("");
    $("#tbAllotDeatil tr:eq(10)").hide();
    $("#tbAllotDeatil tr:eq(11)").hide();
}


//查看页面转过来的
function InitViewStateDetail(auditStatus,  process, IsTrunEconomy, userUnitName, unitName) {
    var length = 0;
    switch (auditStatus) {
        case "A":
            // $("#OneTable").show();
            $("#OneTable").show();
            //如果暖通所的项目 暖通人进来审核 应该当做所长处理
            if (unitName.indexOf("暖通热力所") > -1) {
                if (userUnitName.indexOf("暖通热力所") > -1) {
                    //所长设置状态显示

                    szShow();//所长显示
                    setSzStatus();
                } else {
                    if ($.trim(userUnitName).indexOf("经济所") > -1) {
                        $("#tb_jjs").show();
                        $("#ThreeTable").show();
                    }
                }
            }
            else {
                //一次产值分配 有经济所 暖通所 所长 三种情况
                if ($.trim(userUnitName).indexOf("经济所") > -1) {
                    $("#tb_jjs").show();
                    $("#ThreeTable").show();
                } else if (userUnitName.indexOf("暖通热力所") > -1) {
                    $("#tb_nts").show();
                    $("#FourTable").show();
                } else {
                    //所长设置状态显示
                    szShow();//所长显示
                    setSzStatus();

                }
            }
            break;
           
        case "B":
            $("#OneTable").show();
            SetJJAndNtAuditStatus(userUnitName, unitName);
            break;
        case "C":
            $("#OneTable").show();
            if ($.trim($("#HiddenTwoProposal").val()) != "") {
                $("#TwoTable").show();
                $("#tbSaveed").show();
                if ($("#hidIsTAllPass").val() == "1") {
                    $("#tbSaveed tr:eq(1)").hide();
                    $("#tbSaveed tr:eq(2)").hide();
                }
                $("#saveProcessInfo").show();
                $("#auditSuggstion").show();
            }
            if ($("#HiddenThreeProposal").val() != "") {
                $("#ThreeTable").show();
            }
            if ($("#HiddenFourProposal").val() != "") {
                $("#FourTable").show();
            }
                    
            break;
        case "D":
            $("#OneTable").show();
            if ($.trim($("#HiddenTwoProposal").val()) != "") {
                $("#TwoTable").show();
                $("#tbSaveed").show();
                if ($("#hidIsTAllPass").val() == "1") {
                    $("#tbSaveed tr:eq(1)").hide();
                    $("#tbSaveed tr:eq(2)").hide();
                }
                $("#saveProcessInfo").show();
                $("#auditSuggstion").show();
            }
            if ($("#HiddenThreeProposal").val() != "") {
                $("#ThreeTable").show();
            }
            if ($("#HiddenFourProposal").val() != "") {
                $("#FourTable").show();
            }

            SetJJAndNtAuditStatus(userUnitName, unitName);
            break;
        
      
        case "F":
            $("#OneTable").show();
           
            setActul();

            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            if ($("#hidIsTAllPass").val() != "1") {
                $("#FiveTable").show();

                SetProcess(process);

                if (IsEdit == "0") {
                    // $("#btnEditALL").show();
                }
                if ($("#tbNoData").text() != "") {
                    $("#tbNoData").show();
                    DisableButton();
                }
            }
            break;

        case "H":
            $("#OneTable").show();
            setActul();

            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }
            $("#FiveTable").show();

            if ($("#lbl_NoData").text() != "") {
                $("#lbl_NoData").show();
            }
            setProcessed(process);

            //专业已经审批完毕
            if ($("#tbIsAuditedBymemberAmount").text() != '') {

                $("#SixTable").show();
                $("#tbIsAuditedBymemberAmount").show();

                var userName = TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster.GetUserName($("#HiddenSpecialityAuditUser").val());

                $("#AuditUser", $("#SixTable")).text(userName.value);
                $("#btnApproval").hide();
                $("#btnRefuse").hide();

            } else {
                $("#memberProcessTotal").show();

                $("#SixTable").show();

                $("#tbProjectValueBymember").show();
                $("#tbChooseUser").show();
            }

            break;
        

        case "J":
            $("#OneTable").show();
            setActul();

            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

        
            $("#tbProjectValueBymemberAmount").show();

            break;
        
        case "L":
            $("#OneTable").show();
            setActul();
          
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }
            $("#FiveTable").show();
            szShow();//所长显示

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();
            break;
        
        case "N":
            $("#OneTable").show();
            setActul();
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }

            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();

            break;
        
        case "P":

            $("#OneTable").show();
            setActul();
            if (IsTrunEconomy == "1") {
                $("#ThreeTable").show();
            }
            if (unitName.indexOf("暖通热力所") == -1) {
                if (IsTrunHavc == "1") {
                    $("#FourTable").show();
                }
            }
            $("#FiveTable").show();

            setProcessed(process);

            $("#SixTable").show();

            $("#tbProjectValueBymemberAmount").show();

            $("#SevenTable").show();
            $("#EightTable").show();
           

            $("#btnPrintValue").show();
            $("#ctl00_ContentPlaceHolder1_btnExportValue").show();

           
            break;

    }

    $("#btnApproval").hide();
    $("#btnRefuse").hide();

    $("input[type=text]").attr("disabled", "disabled");
    $("select").attr("disabled", "disabled");
    $("span").attr("disabled", "disabled");
    $("#btnSaveProcess").attr("disabled", "disabled");
    $("#btn_AllApproval").hide();
    if ($("#hidIsTAllPass").val() == "1") {
        $("#tbSaveed tr:eq(1)").hide();
        $("#tbSaveed tr:eq(2)").hide();
    }
}
