﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //隔行变色
    $("#gvproAlloting tr:odd").css({ background: "#f0f0f0" });
    //隔行变色
    $("#gvproAlloted tr:odd").css({ background: "#f0f0f0" });

    //隔行变色
    $("#gvTranJjsAlloting tr:odd").css({ background: "#f0f0f0" });
    //隔行变色
    $("#gvTranJjsAlloted tr:odd").css({ background: "#f0f0f0" });
    // Tabs
    $('#tabs').tabs({ cookie: { expires: 30} });

    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });

    ChangedBackgroundColor();

    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    //发起审核按钮
    $("span[id=InitiatedAudit]").live("click",function () {
        //获取合同SysNo
        var proSysNo = $(this).attr("prosysno");
        var objData =
		{
		    ProSysNo: proSysNo,
		    InUser: userSysNo,
		    SecondValue: "1"
		};
        var jsonData = Global.toJSON(objData);
        $.post("/HttpHandler/ProjectValueandAllot/ProjectValueandAllotHandler.ashx", { "Action": 0, "data": jsonData }, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                CommonControl.GetSysMsgCount("/ProjectValueandAllot/AddjjsProjectValueAllot.aspx?proid=" + proSysNo + "&ValueAllotAuditSysNo=" + jsonResult + "");
            } else {
                alert("此合同无需再次申请或者服务器错误！请联系系统管理员！");
            }
        });
    });

    //删除
    $(".cls_delteJjs").live("click",function () {
        var status = $(this).next().next().next(":hidden").val();
        if (status == 'S') {
            alert("项目分配审核完毕，不能删除");
            return false;
        }

        if (confirm("确定要删除本条分配记录吗？")) {
            var proId = $(this).next(":hidden").val();
            var allotID = $(this).next().next(":hidden").val();
            Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 8, "proID": proId, "AllotID": allotID }, null, null, function (jsonResult) {
                if (jsonResult == "0") {
                    alert("删除失败！");
                } else {
                    alert("删除成功！");
                    //查询系统新消息
                    window.location.href = "/ProjectValueandAllot/EconomyProjectValueAllotList.aspx";
                }
            });
        }
    });

    //删除
    $(".cls_delteTranJjs").live("click", function () {
        var status = $(this).parent().parent().find("td").eq(5).tex();
        //var status = $(this).next().next().next(":hidden").val();
        if (status == 'S') {
            alert("项目分配审核完毕，不能删除");
            return false;
        }

        if (confirm("确定要删除本条分配记录吗？")) {
            var proId = $(this).parent().parent().find("td").eq(2).tex();
            var allotID = $(this).attr("rel");
            // var proId = $(this).next(":hidden").val();
            //var allotID = $(this).next().next(":hidden").val();
            Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 7, "proID": proId, "AllotID": allotID }, null, null, function (jsonResult) {
                if (jsonResult == "0") {
                    alert("删除失败！");
                } else {
                    alert("删除成功！");
                    //查询系统新消息
                    window.location.href = "/ProjectValueandAllot/TranEconomyList.aspx";
                }
            });
        }
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = 0;
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_cprName"));
    autoComplete.GetAutoAJAX();
    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});