﻿$(function () {

    AllotOne();
    AllotTwo();
});


function AllotOne() {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '', '项目名称', '承接部门', '合同额(万元)', '借出金额(元)', '借入金额(元)', '分配详细'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'AuditRecordSysNo', index: 'AuditRecordSysNo', hidden: true, editable: true },
                             { name: 'AllCpr_Acount', index: 'AllCpr_Acount', hidden: true, editable: true },
                             { name: 'AllLoanValueCount', index: 'AllLoanValueCount', hidden: true, editable: true },
                             { name: 'AllBorrowValueCount', index: 'AllBorrowValueCount', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'cpr_Acount', width: 90, align: 'center' },
                             { name: 'LoanValueCount', index: 'LoanValueCount', width: 110, align: 'center' },
                             { name: 'BorrowValueCount', index: 'BorrowValueCount', width: 110, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 60, align: 'center', formatter: colStrShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "sel", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'p.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        caption: "产值分配借入、借出项目的统计表",
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });




    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//详细
function colStrShowFormatter(celvalue, options, rowData) {
    var auditString = '<a href="../ProjectValueandAllot/ProjectValueAllotDetailsBymaster.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '" class="cls_chk" style="height: 26px;">查看</a>';

    return auditString;
}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }

    var rowNum = parseInt($("#jqGrid").getGridParam('records'), 10);

    if (rowNum > 0) {

        var ret = $("#jqGrid").jqGrid("getRowData", 1);
        var allCpr_Acount = ret["AllCpr_Acount"];
        var allLoanValueCount = ret["AllLoanValueCount"];
        var allBorrowValueCount = ret["AllBorrowValueCount"];
        $("#jqGrid").footerData('set', { pro_name: "合计:", Cpr_Acount: allCpr_Acount, LoanValueCount: allLoanValueCount, BorrowValueCount: allBorrowValueCount }, false);
    }
    else {
        $("#jqGrid").footerData('set', { pro_name: "合计:", Cpr_Acount: 0, LoanValueCount: 0, BorrowValueCount: 0 }, false);

        $("#nodata").hide();
    }

}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}


function AllotTwo() {

    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '部门名称', '借出金额(元)', '借入金额(元)'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'unit_id', index: 'unit_id', hidden: true, editable: true },
                             { name: 'AllLoanValueCount', index: 'AllLoanValueCount', hidden: true, editable: true },
                             { name: 'AllBorrowValueCount', index: 'AllBorrowValueCount', hidden: true, editable: true },
                             { name: 'unit_Name', index: 'unit_Name', width: 100, align: 'center' },
                             { name: 'LoanValueCount', index: 'LoanValueCount', width: 110, align: 'center' },
                             { name: 'BorrowValueCount', index: 'BorrowValueCount', width: 110, align: 'center' }

        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where1").val()), "action": "sel2", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'unit_id',
        sortorder: 'asc',
        pager: "#gridpager2",
        viewrecords: true,
        caption: "产值分配借入、借出部门统计表",
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod2,
        loadComplete: loadCompMethod2
    });




    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where1").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'unit_id',
            sortorder: 'asc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where1").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueLendBorrowListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'unit_id',
            sortorder: 'asc'

        }).trigger("reloadGrid");
    });
}

//统计 
function completeMethod2() {
    var rowIds = $("#jqGrid2").jqGrid('getDataIDs');

    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid2 [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }

    var rowNum = parseInt($("#jqGrid").getGridParam('records'), 10);

    if (rowNum > 0) {

        var ret = $("#jqGrid2").jqGrid("getRowData", 1);
        var allLoanValueCount = ret["AllLoanValueCount"];
        var allBorrowValueCount = ret["AllBorrowValueCount"];
        $("#jqGrid2").footerData('set', { unit_Name: "合计:", LoanValueCount: allLoanValueCount, BorrowValueCount: allBorrowValueCount }, false);
    }
    else {
        $("#jqGrid2").footerData('set', { unit_Name: "合计:", LoanValueCount: 0, BorrowValueCount: 0 }, false);

        $("#nodata").hide();
    }
}
//无数据
function loadCompMethod2() {
    $("#jqGrid2").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata2").text() == '') {
            $("#jqGrid2").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata2").show(); }
    }
    else {
        $("#nodata2").hide();
    }
}