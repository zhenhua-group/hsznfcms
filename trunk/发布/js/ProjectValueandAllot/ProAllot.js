﻿$(document).ready(function () {
    //合同关联
    $("#sch_reletive").click(function () {
        var url = "../../ProjectManage/Pro_smallReletive.aspx";
        ShowDialogWin_proReletive(url);
    });
    //Grid 高度指定
    $("#gvAllot tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //得到页面的实际高度
    var h = 180;
    //表格滚动
    $("#gvAllot").chromatable({
        width: "100%",
        height: h,
        scrolling: "true"
    });
    //隔行变色
    $("#gvAllot tr:even").css({ background: "White" });

    //保存
    $("#btnSave").click(function () {
        var cprNo = $("#txtcprNo").val();
        var cprAcount = $("#txtcprAcount").val();
        var scale = $("#txtscale").val();
        var promanager = $("#txtproManager").val();
        var prostartdate = $("#txtstarttime").val();
        var profinishdate = $("#txtfinishtime").val();
        var msg = "";
        //合同编号
        if (cprNo == "") {
            msg += "合同编号不能为空！</br>";
        }
        //合同额
        if (cprAcount == "") {
            msg += "合同额不能为空！</br>";
        }
        else {
            var reg = /^\d+\.\d{0,3}$/;
            if (!reg.test(cprAcount)) {
                msg += "合同金额格式错误！</br>";
            }
        }
        //建筑规模
        if (scale == "") {
            msg += "建筑规模不能为空！</br>";
        }
        //项目经理
        if (promanager == "") {
            msg += "项目经理不能为空！</br>";
        }
        //开始时间
        if (prostartdate == "") {
            msg += "项目开始时间不能为空！</br>";
        }
        //结束时间
        if (profinishdate == "") {
            msg += "项目结束时间不能为空！</br>";
        }
        //比较时间
        var result = duibi(prostartdate, profinishdate);
        if (result == false) {
            msg += "项目开始时间大于结束时间！";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            return true;
        }
    });
    //如果已经存在了合同信息
    if ($("#hid_cprid").val() != "0") {
        $(":text").attr("disabled", "disabled");
        $(":radio").attr("disabled", "disabled");
        //隐藏查询
        $("#sch_reletive").hide();
        $("#schmanager").hide();
    }
    //分配
    $("#btn_allot").click(function () {
        if ($("#hid_iscompelete").val() == "1") {
            alert("已分配完成！");
            return false;
        }
    });
    //删除分配信息
    $(".delete").click(function () {
        delAllotData($(this));
    });
    //查看动作
    if ($("#hiddenAction").val() == "chk") {
        $("#btnSave").hide();
        $("#btn_allot").hide();
    }
});
//删除分配信息
function delAllotData(link) {
    if (confirm("确定要删除本条分配信息？")) {
        $.post("../HttpHandler/ProAllotComm.ashx", { flag: "delallot", id: "" + link.attr("rel") + "" }, function (data) {
            if (data == "ok") {
                alert("删除成功！");
                $("#hid_iscompelete").val("0");
                window.location.reload();
            }
        });
    }
}
//比较时间
function duibi(a, b) {
    var arr = a.split("-");
    var starttime = new Date(arr[0], arr[1], arr[2]);
    var starttimes = starttime.getTime();

    var arrs = b.split("-");
    var lktime = new Date(arrs[0], arrs[1], arrs[2]);
    var lktimes = lktime.getTime();

    if (starttimes >= lktimes) {
        return false;
    }
    else
        return true;

}

//合同关联
function ShowDialogWin_proReletive(url) {
    var feature = "dialogWidth:530px;dialogHeight:200px;ceter:yes";
    var result = window.showModalDialog(url, "", feature);
    //合同名称
    $("#txtcpr").val(result[0]);

    //合同金额
    $("#txtcprAcount").val(result[5]);
    //建设单位
    $("#txtbuildUnit").val(result[3]);
    //项目经理
    $("#txtproManager").val(result[6]);
    //合同id
    $("#cpr_id").val(result[8]);
    //合同编号
    $("#txtcprNo").val(result[9]);
}
