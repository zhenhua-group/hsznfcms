﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '姓名', '产值(元)', '产值工资(元)', '所管人员工资(元)', '酬金工资(元)', '税率(%)', '全年实际应交税额(元)', '应发工资(元)', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'mem_ID', index: 'mem_ID', hidden: true, editable: true },
                             { name: 'mem_Name', index: 'mem_Name', width: 100, align: 'center' },
                             { name: 'memTotalCount', index: 'memTotalCount', width: 100, align: 'right' },
                             { name: 'ValueWage', index: 'ValueWage', width: 100, align: 'right' },
                             { name: 'PossessionMemWange', index: 'PossessionMemWange', width: 120, align: 'right' },
                             { name: 'RemunerationWange', index: 'RemunerationWange', width: 100, align: 'right' },
                             { name: 'TaxPercent', index: 'TaxPercent', width: 100, align: 'right' },
                             { name: 'YearTaxCount', index: 'YearTaxCount', width: 120, align: 'right' },
                             { name: 'ShouldWange', index: 'ShouldWange', width: 100, align: 'right' },
                             { name: 'mem_ID', index: 'mem_ID', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "member", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "unit": $("#ctl00_ContentPlaceHolder1_drp_unit").val() },
        loadonce: false,
        sortname: 'me.mem_ID',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var name = $("#ctl00_ContentPlaceHolder1_txt_memberName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'year': year, "name": name },
            page: 1

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var name = $("#ctl00_ContentPlaceHolder1_txt_memberName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'year': year, "name": name },
            page: 1

        }).trigger("reloadGrid");
    });
    $(".chakan").live("click", function () {
        var memid = $(this).parent().parent().find("td").eq(2).text();
        var memname = $(this).parent().parent().find("td").eq(3).text();
        var memvalue = $(this).parent().parent().find("td").eq(4).text();
        GetProjectList(memid, memname, memvalue);
    });

    //保存
    $("#btnSave").click(function () {

        if ($.trim($("#txt_ValuePercent").val()).length == 0) {
            alert("产值工资系数不能为空！");
            return false;
        }

        if ($.trim($("#txt_PossessionMemWange").val()).length == 0) {
            alert("所管人员工资不能为空！");
            return false;
        }

        if ($.trim($("#txt_RemunerationPercent").val()).length == 0) {
            alert("资酬金工资比例不能为空！");
            return false;
        }

        if ($.trim($("#txt_TaxPercent").val()).length == 0) {
            alert("税率不能为空！");
            return false;
        }

        var ViewEntity = {
            "mem_id": $("#txt_mem_id").text(),
            "ValuePercent": $("#txt_ValuePercent").val(),
            "ValueWage": $("#txt_ValueWage").text(),
            "PossessionMemWange": $("#txt_PossessionMemWange").val(),
            "YearWange": $("#txt_YearWange").text(),
            "RemunerationPercent": $("#txt_PossessionMemWange").val(),
            "YearWange": $("#txt_PossessionMemWange").val(),
            "RemunerationPercent": $("#txt_RemunerationPercent").val(),
            "RemunerationWange": $("#txt_RemunerationWange").text(),
            "TaxPercent": $("#txt_TaxPercent").val(),
            "YearTaxCount": $("#txt_YearTaxCount").text(),
            "ShouldWange": $("#txt_ShouldWange").text(),
            "yearWang": $("#txt_year").text()
        }

        var jsonObj = Global.toJSON(ViewEntity);

        //提交数据
        $.post("/HttpHandler/DrpMemberHandler.ashx", { "action": "wangAdd", "data": jsonObj, "n": Math.random() }, function (result) {

            if (result > 0) {
                $("#ShowDiv").modal('hide');
                var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
                var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
                var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
                $("#jqGrid").jqGrid('setGridParam', {
                    url: "/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx",
                    postData: { 'strwhere': strwhere, 'unit': unit, 'year': year },
                    page: 1

                }).trigger("reloadGrid");
            }
            else {
                alert("添加失败!");
            }
        });
    });

    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    //
    //产值工资系数
    $("#txt_ValuePercent").change(function () {

        if (!reg.test($("#txt_ValuePercent").val())) {
            alert("产值工资系数请输入数字！");
            $(this).val("0");
        }

        CalculationValue();
        return false;
    });

    //所管人员工资
    $("#txt_PossessionMemWange").change(function () {

        if (!reg.test($("#txt_PossessionMemWange").val())) {
            alert("所管人员工资请输入数字！");
            $(this).val("0");
        }

        CalculationValue();
        return false;
    });

    //资酬金工资比例
    $("#txt_RemunerationPercent").change(function () {

        if (!reg.test($("#txt_RemunerationPercent").val())) {
            alert("资酬金工资比例请输入数字！");
            $(this).val("0");
        }

        CalculationValue();
        return false;
    });

    //资酬金工资比例
    $("#txt_RemunerationPercent").change(function () {

        if (!reg.test($("#txt_RemunerationPercent").val())) {
            alert("资酬金工资比例请输入数字！");
            $(this).val("0");
        }

        CalculationValue();
        return false;
    });

    //税率
    $("#txt_TaxPercent").change(function () {

        if (!reg.test($("#txt_TaxPercent").val())) {
            alert("税率请输入数字！");
            $(this).val("0");
        }

        CalculationValue();
        return false;
    });

});

//查看
function colShowFormatter(celvalue, options, rowData) {

    return '<a href="#ShowDiv" data-toggle="modal" class="chakan" >操作</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
    var summemTotalCount = parseFloat($("#jqGrid").getCol("memTotalCount", false, 'sum')).toFixed(2);
    var sumValueWage = parseFloat($("#jqGrid").getCol("ValueWage", false, 'sum')).toFixed(2);
    var sumPossessionMemWange = parseFloat($("#jqGrid").getCol("PossessionMemWange", false, 'sum')).toFixed(2);
    var sumYearWange = parseFloat($("#jqGrid").getCol("YearWange", false, 'sum')).toFixed(2);
    var sumRemunerationWange = parseFloat($("#jqGrid").getCol("RemunerationWange", false, 'sum')).toFixed(2);
    var sumYearTaxCount = parseFloat($("#jqGrid").getCol("YearTaxCount", false, 'sum')).toFixed(2);
    var sumShouldWange = parseFloat($("#jqGrid").getCol("ShouldWange", false, 'sum')).toFixed(2);
    $("#jqGrid").footerData('set', { mem_Name: "合计:", memTotalCount: summemTotalCount, ValueWage: sumValueWage, PossessionMemWange: sumPossessionMemWange, YearWange: sumYearWange, RemunerationWange: sumRemunerationWange, YearTaxCount: sumYearTaxCount, ShouldWange: sumShouldWange }, false);
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}

function GetProjectList(memid, memname, memvalue) {

    ClearPopAreaContent();

    $("#txt_mem_id").text(memid);
    if ($("#ctl00_ContentPlaceHolder1_drp_year").val() != "-1") {
        $("#btnSave").show();
        $("#txt_year").text($("#ctl00_ContentPlaceHolder1_drp_year").val());
    }
    else {
        $("#btnSave").hide();
    }
    $("#txt_name").text(memname);
    $("#txt_Value").text(memvalue);

    $.post("/HttpHandler/DrpMemberHandler.ashx", { "action": "wang", "memid": memid, "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "n": Math.random() }, function (result) {


        if (result != "null") {
            var dataObj = Global.evalJSON(result);
            $("#txt_ValuePercent").val($.trim(dataObj.ValuePercent));
            $("#txt_ValueWage").text($.trim(dataObj.ValueWage));
            $("#txt_PossessionMemWange").val($.trim(dataObj.PossessionMemWange));
            $("#txt_YearWange").text($.trim(dataObj.YearWange));
            $("#txt_RemunerationPercent").val($.trim(dataObj.RemunerationPercent));
            $("#txt_RemunerationWange").text($.trim(dataObj.RemunerationWange));
            $("#txt_TaxPercent").val($.trim(dataObj.TaxPercent));
            $("#txt_YearTaxCount").text($.trim(dataObj.YearTaxCount));
            $("#txt_ShouldWange").text($.trim(dataObj.ShouldWange));
        }

        CommonControl.SetTableStyle("resultTable", "need");
        $("h4", "#ShowDiv").text(memname + "效益工资");

        //隔行变色
        $(".cls_show_cst_jiben tr:even").css({ background: "f0f0f0" });
    });
}

function ClearPopAreaContent() {
    $("span", "#PopAreaDivMain").text("");
    $("input", "#PopAreaDivMain").val("");
}

function CalculationValue() {

    //产值
    var allotAccount = $("#txt_Value").text().length == 0 ? 0 : parseFloat($("#txt_Value").text());

    //产值系数(实)
    var valuePercent = $("#txt_ValuePercent").val().length == 0 ? 0 : parseFloat($("#txt_ValuePercent").val());
    var valueWage = parseFloat(valuePercent) * parseFloat(allotAccount);
    $("#txt_ValueWage").text(valueWage.toFixed(2));

    //所管人员工资
    var possessionMemWange = $("#txt_PossessionMemWange").val().length == 0 ? 0 : parseFloat($("#txt_PossessionMemWange").val());

    //全年工资
    var yearWange = parseFloat(valueWage) + parseFloat(possessionMemWange);
    $("#txt_YearWange").text(yearWange.toFixed(2));

    //资酬金工资比例
    var remunerationPercent = $("#txt_RemunerationPercent").val().length == 0 ? 0 : parseFloat($("#txt_RemunerationPercent").val());
    var remunerationWange = parseFloat(yearWange) * parseFloat(remunerationPercent);
    $("#txt_RemunerationWange").text(remunerationWange.toFixed(2));

    //税率
    var taxPercent = $("#txt_TaxPercent").val().length == 0 ? 0 : parseFloat($("#txt_TaxPercent").val());
    //全年实际应交税额
    var yearTaxCount = parseFloat(remunerationWange) * parseFloat(taxPercent) / 100;
    $("#txt_YearTaxCount").text(yearTaxCount.toFixed(2));

    //全年实际应交税额
    var shouldWange = parseFloat(remunerationWange) - parseFloat(yearTaxCount);
    $("#txt_ShouldWange").text(shouldWange.toFixed(2));
}