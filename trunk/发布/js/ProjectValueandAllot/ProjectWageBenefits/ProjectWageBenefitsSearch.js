﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '姓名', '产值(元)', '产值工资(元)', '所管人员工资(元)', '酬金工资(元)', '税率(%)', '全年实际应交税额(元)', '应发工资(元)'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'mem_ID', index: 'mem_ID', hidden: true, editable: true },
                             { name: 'mem_Name', index: 'mem_Name', width: 100, align: 'center' },
                             { name: 'memTotalCount', index: 'memTotalCount', width: 100, align: 'right' },
                             { name: 'ValueWage', index: 'ValueWage', width: 100, align: 'right' },
                             { name: 'PossessionMemWange', index: 'PossessionMemWange', width: 120, align: 'right' },
                             { name: 'RemunerationWange', index: 'RemunerationWange', width: 100, align: 'right' },
                             { name: 'TaxPercent', index: 'TaxPercent', width: 100, align: 'right' },
                             { name: 'YearTaxCount', index: 'YearTaxCount', width: 120, align: 'right' },
                             { name: 'ShouldWange', index: 'ShouldWange', width: 100, align: 'right' }

        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "member", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "unit": $("#ctl00_ContentPlaceHolder1_drp_unit").val() },
        loadonce: false,
        sortname: 'me.mem_ID',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var name = $("#ctl00_ContentPlaceHolder1_txt_memberName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'year': year, "name": name },
            page: 1

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var name = $("#ctl00_ContentPlaceHolder1_txt_memberName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'year': year, "name": name },
            page: 1

        }).trigger("reloadGrid");
    });
    $(".chakan").live("click", function () {
        var memid = $(this).parent().parent().find("td").eq(2).text();
        var memname = $(this).parent().parent().find("td").eq(3).text();
        var memvalue = $(this).parent().parent().find("td").eq(4).text();
        GetProjectList(memid, memname, memvalue);
    });


});



//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
    var summemTotalCount = parseFloat($("#jqGrid").getCol("memTotalCount", false, 'sum')).toFixed(2);
    var sumValueWage = parseFloat($("#jqGrid").getCol("ValueWage", false, 'sum')).toFixed(2);
    var sumPossessionMemWange = parseFloat($("#jqGrid").getCol("PossessionMemWange", false, 'sum')).toFixed(2);
    var sumYearWange = parseFloat($("#jqGrid").getCol("YearWange", false, 'sum')).toFixed(2);
    var sumRemunerationWange = parseFloat($("#jqGrid").getCol("RemunerationWange", false, 'sum')).toFixed(2);
    var sumYearTaxCount = parseFloat($("#jqGrid").getCol("YearTaxCount", false, 'sum')).toFixed(2);
    var sumShouldWange = parseFloat($("#jqGrid").getCol("ShouldWange", false, 'sum')).toFixed(2);
    $("#jqGrid").footerData('set', { mem_Name: "合计:", memTotalCount: summemTotalCount, ValueWage: sumValueWage, PossessionMemWange: sumPossessionMemWange, YearWange: sumYearWange, RemunerationWange: sumRemunerationWange, YearTaxCount: sumYearTaxCount, ShouldWange: sumShouldWange }, false);
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}





