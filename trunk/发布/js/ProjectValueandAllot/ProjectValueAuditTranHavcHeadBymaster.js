﻿
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
$(function () {
    CommonControl.SetFormWidth();


    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    setStatus();

    var type = $("#HiddenType").val();

    //转土建产值
    $("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").change(function () {

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").val())) {
            alert("转土建产值比例请输入数字！");
            $(this).val(0);
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").val()) > 100) {
            alert("转土建产值比例不能大于100%！");
            return false;
        }

        CalculationValue();

        $("input", "#gvProjectValueBymember tr ").each(function () {

            var total = $("#ctl00_ContentPlaceHolder1_txtAllotValueCount").text();

            var inputvalue = $(this).val();
            if (inputvalue == undefined || inputvalue == "") {
                inputvalue = 0;
            }
            var proresultvalue = parseFloat(inputvalue) * parseFloat(total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //分配产值
    $("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").change(function () {

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val())) {
            alert("本部门产值产值比例请输入数字！");
            $(this).val(0);
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值产值比例不能大于100%！");
            return false;
        }

        CalculationValue();

        $("input", "#gvProjectValueBymember tr ").each(function () {

            var total = $("#ctl00_ContentPlaceHolder1_txtAllotValueCount").text();

            var inputvalue = $(this).val();
            if (inputvalue == undefined || inputvalue == "") {
                inputvalue = 0;
            }
            var proresultvalue = parseFloat(inputvalue) * parseFloat(total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });


    var isRounding = $("#HiddenIsRounding").val();


    $("input", "#gvProjectValueBymember tr ").each(function () {

        $(this).change(function () {

            var total = 0;
            if ($("#ctl00_ContentPlaceHolder1_txt_TranValueCount").text() == "" || $("#ctl00_ContentPlaceHolder1_txt_TranValueCount").text() == "0.00") {
                alert("请填写转土建所比例");
                return false;
            }

            total = $("#ctl00_ContentPlaceHolder1_txtAllotValueCount").text();

            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }

            var inputvalue = $(this).val();

            var proresultvalue = parseFloat(inputvalue) * parseFloat(total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });

    });

    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

    //审核通过按钮
    $("#btnApproval").click(function () {

        if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
            alert("请选择项目分配年份！");
            return false;
        }
        //分配金额
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").val())) {
            alert("转土建产值比例请输入数字！");
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").val()) > 100) {
            alert("转土建产值比例不能大于100%！");
            return false;
        }

        var tranAccount = $("#ctl00_ContentPlaceHolder1_txt_TranValueCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_TranValueCount").val());
        if (parseFloat(tranAccount) > parseFloat($("#ctl00_ContentPlaceHolder1_lblTotalCount").text())) {
            alert("转土建金额不能超过转暖通金额！");
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            return false;
        }
        var allotAccount = $("#ctl00_ContentPlaceHolder1_txtTheDeptValueCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txtTheDeptValueCount").val());
        if (parseFloat(allotAccount) > parseFloat($("#ctl00_ContentPlaceHolder1_lblTotalCount").text())) {
            alert("本部门自留不能超过转暖通金额！");
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txtAllotValueCount").text()) < 0) {
            alert("分配产值比例不能为负数！");
            return false;
        }


        if (!ValidationMem()) {
            return false;
        }

        var type = $("#HiddenType").val();
        var finalDataObj = {
            "ProNo": $("#HiddenProSysNo").val(),
            "AllotID": $("#HiddenAllotID").val(),
            "ValueByMember": new Array(),
            "TotalCount": $("#ctl00_ContentPlaceHolder1_lblTotalCount").text(),
            "TranCount": $("#ctl00_ContentPlaceHolder1_txt_TranValueCount").text(),
            "Thedeptallotpercent": $("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val(),
            "Thedeptallotcount": $("#ctl00_ContentPlaceHolder1_txtTheDeptValueCount").text(),
            "AllotPercent": $("#ctl00_ContentPlaceHolder1_txt_AllotValuePercent").val(),
            "TranPercent": $("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").val(),
            "ItemType": "nts",
            "AllotCount": $("#ctl00_ContentPlaceHolder1_txtAllotValueCount").text(),
            "AuditUser": $("#HiddenLoginUser").val(),
            "ActualAllountTime": $("#ctl00_ContentPlaceHolder1_drp_year").val()
        };

        // 取得
        $.each($("#gvProjectValueBymember tr"), function (index, tr) {
            finalDataObj.ValueByMember[index] =
                {
                    "mem_ID": $.trim($(tr).children("td:eq(1)").text()),
                    "DesignPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "DesignCount": $.trim($(tr).children("td:eq(4)").text()),
                    "SpecialtyHeadPercent": $.trim($(tr).children("td:eq(6)").find("input").val()),
                    "SpecialtyHeadCount": $.trim($(tr).children("td:eq(7)").text()),
                    "AuditPercent": $.trim($(tr).children("td:eq(9)").find("input").val()),
                    "AuditCount": $.trim($(tr).children("td:eq(10)").text()),
                    "ProofreadPercent": $.trim($(tr).children("td:eq(12)").find("input").val()),
                    "ProofreadCount": $.trim($(tr).children("td:eq(13)").text()),
                    "ItemType": 0,
                    "IsHead": $.trim($(tr).children("td:eq(3)").find("input").attr("sz")),
                    "IsExternal": $.trim($(tr).children("td:eq(1)").attr("wp"))
                };
        });


        if (!confirm("是否保存信息?")) {
            return false;
        }

        $("#btnApproval").attr("disabled", true);

        var jsonObj = Global.toJSON(finalDataObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx", { "Action": 9, "data": jsonObj, "flag": 1, "msgID": $("#msgno").val(), "allotID": $("#HiddenTranAllotID").val(), "proType": $("#hid_proType").val(), "sys_no": $("#HiddenAuditRecordSysNo").val(), }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //消息
                    alert("所长再次审批通过，消息发给对应人员进行个人产值确认！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });

    });

    //不通过
    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {

        if (!confirm("是否不通过审核信息?")) {
            return false;
        }

        $("#btnRefuse").attr("disabled", true);
        Global.SendRequest("/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx", { "Action": 10, "flag": 1, "msgID": $("#msgno").val(), "AllotID": $("#HiddenAllotID").val(), "proID": $("#HiddenProSysNo").val(), "mem_id": $("#HiddenLoginUser").val(), "sys_no": $("#HiddenAuditRecordSysNo").val(), "proType": $("#hid_proType").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //消息
                    alert("所长再次审批不通过！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });
    });
    var chooseUserMain = new ChooseProjectValueUserControl($("#chooseUserMain"));
    //选择用户
    $("#chooseUser").click(function () {

        var parObj = {};

        parObj.UnitName = "";
        parObj.SpeName = "暖通";

        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);

        $("#btn_UserMain").click(function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
        })
    });


    //选择用户
    function ChooseUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = 0;
        var IsAdd = false;
        $.each(userArray, function (index, item) {
            $("#gvProjectValueBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {
                $("#gvProjectValueBymember tr").each(function () {
                    $("td[class=cls_Column]", this).each(function () {
                        if (item.userSpecialtyname == $(this).text()) {
                            index = parseInt($(this).parent().index());
                            lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        }
                    });
                });
                var IsExisted = true;
                //循环该人员是否存在
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                    }
                });
                //不存在添加
                if (IsExisted) {
                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $.each(userArray, function (index, items) {
                            $("#gvProjectValueBymember tr").each(function () {
                                var userSysNO = $(this).children(":eq(1)").text();
                                if (userSysNO == items.userSysNo) {
                                    _userLength = _userLength - 1;
                                }
                            });
                        });
                        $("#gvProjectValueBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + " >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    if (item.checkHead == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"fz\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    if (item.checkAudit == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    if (item.checkProof == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"jd\"  />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    trString += "<td class=\"display\">0</td>";
                    trString += "</tr>";
                    $(trString).insertAfter("#gvProjectValueBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }
            });
        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加
                var _userLength = userArray.length;
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                        _userLength = _userLength - 1;
                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"10%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }

                    trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    if (item.checkHead == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"fz\"  />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    if (item.checkAudit == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    if (item.checkProof == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + "  roles=\"jd\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += " <td class=\"display\" >0</td>";
                    trString += "<td class=\"display\"> 0</td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueBymember").append(trString);
                }
            });
        }
    }

    var chooseExternalUser = new ChooseExternalUserControl($("#chooseExternalUserDiv"));
    //选择用户
    $("#chooseExternalUser").click(function () {

        var parObj = {};

        parObj.UnitName = "暖通热力所";
        parObj.SpeName = "暖通";

        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);

        $("#btnChooseExt").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        });

    });


    //删除用户A标签点击事件
    $("#resultUserRemoveA").live("click", function () {
        var spe = $(this).attr("spe");
        var rowspan = 0;
        $($(this).parent().parent().parent().parent().children().find("td:eq(0)")).each(function () {
            if ($(this).text() == spe) {
                rowspan = $(this).attr("rowspan");
                $(this).attr("rowspan", parseInt(rowspan) - 1);
                return false;
            }
        });
        if (rowspan != 0) {
            $(this).parent().parent().parent().remove();
        }
    });
    //删除用户A标签点击事件
    $("#resultUserRemoveB").live("click", function () {
        var rowspan = $(this).parent().parent().prev().prev().attr("rowspan");
        var classDisplay = $(this).parent().parent().parent().children(":eq(0)").attr("class");
        if (rowspan != undefined && classDisplay != 'display') {
            //把这一行删掉把下一行的td换成文本框
            if (rowspan == 1) {
                $(this).parent().parent().parent().remove();
            }
            else {
                var rowspanText = $(this).parent().parent().prev().prev().text();
                $(this).parent().parent().parent().next().children(":eq(0)").attr("width", "10%");
                $(this).parent().parent().parent().next().children(":eq(0)").attr("class", "cls_Column");
                $(this).parent().parent().parent().next().children(":eq(0)").attr("rowspan", rowspan - 1);
                $(this).parent().parent().parent().next().children(":eq(0)").text(rowspanText);
                $(this).parent().parent().parent().remove();
            }
        } else {
            var speName = $(this).attr("spe");
            $("tr", $(this).parent().parent().parent().parent()).each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (speName == $(this).text()) {
                        var tempRow1 = $(this).attr("rowspan");
                        $(this).attr("rowspan", (tempRow1 - 1));
                    }
                });
            });
            $(this).parent().parent().parent().remove();
        }
    });


    //重新选择用户事情

    $("#txtChooseUser").live('change', function () {

        var total = 0;

        if ($("#ctl00_ContentPlaceHolder1_txt_TranValueCount").text() == "" || $("#ctl00_ContentPlaceHolder1_txt_TranValueCount").text() == "0.00") {
            alert("请填写转土建所比例");
            return false;
        }
        total = $("#ctl00_ContentPlaceHolder1_txtAllotValueCount").text();

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputvalue = $(this).val();

        var proresultvalue = parseFloat(inputvalue) * parseFloat(total) / 100;

        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }
    });


});
//计算产值
function CalculationValue() {

    //总金额
    var totalAccount = $("#ctl00_ContentPlaceHolder1_lblTotalCount").text();

    //转土建
    var tranPercent = $("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_TranValuePercent").val());
    var tranValueCount = parseFloat(tranPercent) * parseFloat(totalAccount) / 100;
    $("#ctl00_ContentPlaceHolder1_txt_TranValueCount").text(tranValueCount.toFixed(0));

    //本部门自留
    var theDeptValuePercent = $("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val());
    //本部门自留产值
    var theDeptValueCount = parseFloat(theDeptValuePercent) * parseFloat(totalAccount) / 100;
    $("#ctl00_ContentPlaceHolder1_txtTheDeptValueCount").text(theDeptValueCount.toFixed(0));

    //分配
    var allotPercent = 100 - parseFloat(tranPercent) - parseFloat(theDeptValuePercent);
    $("#ctl00_ContentPlaceHolder1_txt_AllotValuePercent").val(allotPercent.toFixed(2));
    var allotValueCount = parseFloat(allotPercent) * parseFloat(totalAccount) / 100;
    $("#ctl00_ContentPlaceHolder1_txtAllotValueCount").text(allotValueCount.toFixed(0));
}


//验证人员
function ValidationMem() {

    var flag = true;

    var trLength = $("#gvProjectValueBymember tr").length;
    if (trLength == 0) {
        alert("请添加人员");
        flag = false;
        return false;
    }
    var valueemputy = "0";
    $.each($("input", "#gvProjectValueBymember tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputy).length == 0) {
        alert("项目策划人员产值分配比例不能为空");
        flag = false;
        return false;
    }

    var designcount = 0;
    var isCheck = false;
    $("input", "#gvProjectValueBymember tr").each(function () {
        designcount += parseFloat($(this).val());
        isCheck = true;
    });

    if (designcount.toFixed(0) != 100 && isCheck == true) {
        alert("项目策划人员产值分配比例没有闭合！");
        flag = false;
        return false;
    }

    return flag;
}


//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}

function setStatus() {
    if ($.trim($("#hid_IsDone").val()) == "D") {
        $("input").attr("disabled", "disabled");
        $("input[type=button]").hide();
        $("#chooseUser").hide();
        $("#chooseExternalUser").hide();
    }
}