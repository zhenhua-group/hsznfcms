﻿$(document).ready(function () {
    //合同收费
    $("#partOnecprunitpricetext").blur(function () {
        var allArea = parseFloat($("#partOneSumtext").text());
        var reg = /^\d+(\.\d+)?$/;
        var count = $(this).val();
        if (!reg.test(count)) {
            $(this).val("");
            return false;
        }
        $("#partOnecprCharge").val(Round((allArea * count) / 10000));
    });
    //本次费率
    $("#partOneCurChangetext").blur(function () {
        var allcount = parseFloat($("#partOnecprCharge").val());
        var reg = /^\d+(\.\d+)?$/;
        var cout = $(this).val();
        if (!reg.test(cout)) {
            $(this).val("");
            return false;
        }
        //所剩合同额
        var suballot = parseFloat($("#HiddenSubAllot").val());
        if (suballot > 0) {
            if (cout > suballot) {
                alert("分配额已经大于合同所剩额度,可分配额度为：" + suballot + "万元");
                $(this).val(suballot);
                cout = suballot;
            }
        }
        if (allcount == 0) {
            return false;
        }

        var rlt = (parseFloat(cout) / parseFloat(allcount)) * 100;
        $("#partOneCurRatetext").val(rlt.toFixed(2));
    });
    //分成基数元
    $("#partOneLowDivUnitAreatext").blur(function () {
        var allcount = parseFloat($("#partOneSumtext").text());
        var reg = /^\d+(\.\d+)?$/;
        var cout = $(this).val();
        if (!reg.test(cout)) {
            $(this).val("");
            return false;
        }
        //不能大于合同单价
        var chae = parseFloat(cout) - parseFloat($("#partOnecprunitpricetext").val());
        if (chae > 0) {
            alert("最低收费标准分成基数每平米不能大于合同每平米单价！");
            $(this).val("");
            return false;
        }
        var rlt = parseFloat(allcount) * parseFloat(cout);
        //最低分成基数（元）
        $("#partOneLowDivUnitPricetext").val(rlt.toFixed(2));
        //税后分成基数
        if (cout > 0) {
            var b = $("#partOneTaxRatetext").val();
            if (!reg.test(b)) {
                return false;
            }
            $("#partOneLowRateUnitAreatext").val(parseFloat(LowAllotLocalYuan(cout, b)).toFixed(2));
        }
        //税后分成基数（元）
        if (rlt > 0) {
            var b = $("#partOneTaxRatetext").val();
            if (!reg.test(b)) {
                return false;
            }
            $("#partOneLowRateUnitPricetext").val(parseFloat(LowAllotLocalYuan(b, rlt)).toFixed(2));
        }
    });
    //税率
    $("#partOneTaxRatetext").blur(function () {
        if (parseInt($(this).val()) > 100) {
            alert("税率不能大于100%！");
            $(this).val("");
            return false;
        }
        var a = $("#partOneLowDivUnitAreatext").val();
        var b = $(this).val();
        var reg = /^\d+(\.\d+)?$/;
        if (!reg.test(a)) {
            $("#partOneLowDivUnitAreatext").val("");
            return false;
        }
        if (!reg.test(b)) {
            $(this).val();
            return false;
        }

        $("#partOneLowRateUnitAreatext").val(LowAllotLocal(a, b));

        if (b > 0) {
            var aa = $("#partOneLowDivUnitPricetext").val();
            if (!reg.test(aa)) {
                return false;
            }
            $("#partOneLowRateUnitPricetext").val(LowAllotLocalYuan(aa, b));
        }
    });

    //PartTwo
    $("#partTwoTaxSigneltext").blur(function () {

        if (parseInt($(this).val()) > 100) {
            alert("分配百分比不能大于100%！");
            $(this).val("");
            return false;
        }
        var persent = $(this).val();
        var count = $("#partOneLowRateUnitPricetext").val();
        var reg = /^\d+(\.\d+)?$/;
        if (!reg.test(persent)) {
            $(this).val("");
            return false;
        }
        if (!reg.test(count)) {
            return false;
        }
        $("#partTwoTaxSignelRatetext").val(LowAllotLocal(persent, count));


        //院务管理
        //D9*0.01
        var D9 = $("#partOneCurRatetext").val();
        //C13*0.01
        var C13 = $("#partTwoCollegeTaxtext").val();
        //B14
        var B14 = $("#partTwoTaxSignelRatetext").val();
        //B9
        var B9 = $("#partOnecprunitpricetext").val();
        //j7
        var J7 = $("#partOneLowDivUnitAreatext").val();
        //H6
        var H6 = $("#partOneSumtext").text();
        //H9*0.01
        var allmoney = 0;
        var H9 = $("#partOneTaxRatetext").val();
        var rlt = (D9 * 0.01 * B14 * C13 * 0.01) + (B9 - J7) * H6 * H9 * 0.01 * 0.25;
        //结果
        $("#partTwoCollegeRatetext").val(rlt);
        allmoney += rlt;
        //项目费用
        //D13*0.01
        var D13 = $("#partTwoProjectTaxtext").val();
        rlt = (D9 * 0.01 * B14 * D13 * 0.01) + (B9 - J7) * H6 * H9 * 0.01 * 0.75;
        $("#partTwoProjectRatetext").val(rlt);
        allmoney += rlt;
        //负责
        //E13*0.01
        var E13 = $("#partTwoProChgTaxtext").val();
        rlt = B14 * E13 * 0.01;
        $("#partTwoProChgRatetext").val(rlt);
        allmoney += rlt;
        //注册师
        //F13*0.01
        var F13 = $("#partTwoRegisterTaxtext").val();
        rlt = B14 * F13 * 0.01;
        $("#partTwoRegisterRatetext").val(rlt);
        allmoney += rlt;
        //设校人员
        //G13*0.01
        var G13 = $("#partTwoShexTaxtext").val();
        rlt = B14 * G13 * 0.01;
        $("#partTwoShexRatetext").val(rlt);
        allmoney += rlt;
        //小神核定
        //H13*0.01
        var H13 = $("#partTwoShedTaxtext").val();
        rlt = B14 * H13 * 0.01;
        $("#partTwoShedRatetext").val(rlt);
        allmoney += rlt;
        //总工
        //I13*0.01
        var I13 = $("#partTwoZonggongTaxtext").val();
        rlt = B14 * I13 * 0.01;
        $("#partTwoZonggongRatetext").val(rlt);
        allmoney += rlt;
        //院长
        //J13*0.01
        var J13 = $("#partTwoYuanzTaxtext").val();
        rlt = B14 * J13 * 0.01;
        $("#partTwoYuanzRatetext").val(rlt);
        allmoney += rlt;
        //合计
        //C13-J13
        var allprt = parseFloat(C13) + parseFloat(D13) + parseFloat(E13) + parseFloat(F13) + parseFloat(G13) + parseFloat(H13) + parseFloat(I13) + parseFloat(J13);
        $("#partTwoSumTaxtext").val(allprt.toFixed(2));
        $("#partTwoSumRatetext").val(allmoney.toFixed(2));
    });
});

function Round(num) {
    var result = (Math.round(num * 10000)) / 10000;
    return result;
}

//本次费率计算
function CurrentRatCost() {
    var reg = /^[0-9]+[.]?[0-9]+$/;
    var cprCount = $("#partOnecprCharge").val(); //合同收费
    var cprcharge = $("#partOneCurChangetext").val(); //本次收费
    if (!reg.test(cprCount)) {
        $("#partOnecprCharge").val("");
        return false;
    }
    if (!reg.test(cprcharge)) {
        $("#partOneCurChangetext").val("");
        return false;
    }
    cprCount = parseFloat(cprCount);
    cprcharge = parseFloat(cprcharge);
    $("#partOneCurRatetext").val(Round(cprcharge / cprCount) * 100);
}
//最低收费标准税后分成基数
function LowAllotLocal(a, b) {
    var rlt = a * b * 0.01;
    return rlt.toFixed(2);
}
//最低收费标准税后分成元
function LowAllotLocalYuan(a, b) {
    var rlt = a * b * 0.01;
    return rlt.toFixed(2);
}