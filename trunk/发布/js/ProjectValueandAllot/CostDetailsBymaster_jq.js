﻿$(function () {
    var tempRandom = Math.random() + new Date().getMilliseconds();
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/CostDetailsHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '日期', '凭单号', '明细', '金额(元)', '类别', '部门', '录入人', '', '', '', '操作', '删除'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'C.ID', index: 'C.ID', hidden: true, editable: true },
                              { name: 'costOutCharge', index: 'costOutCharge', hidden: true, editable: true },
                              { name: 'typeGroup', index: 'typeGroup', hidden: true, editable: true },
                             { name: 'costTimeS', index: 'costTime', width: 120, align: 'center' },//index是排序時所對應的字段。
                             { name: 'costNum', index: 'costNum', width: 120, align: 'center' },
                             { name: 'costSub', index: 'costSub', width: 250, align: 'left' },
                             { name: 'costCharge', index: 'costCharge', width: 100, align: 'center' },
                             { name: 'typeName', index: 'costTypeID', width: 100, align: 'center' },
                             { name: 'unitName', index: 'costUnit', width: 120, align: 'center' },
                             { name: 'userName', index: 'costUserId', width: 100, align: 'center' },
                              { name: 'costUnit', index: 'costUnit', hidden: true, editable: true, align: 'center' },
                               { name: 'costTypeID', index: 'costTypeID', hidden: true, editable: true },
                                { name: 'isexpord', index: 'isexpord', hidden: true, editable: true },
                             { name: 'ID', index: 'ID', width: 80, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'ID', index: 'ID', width: 50, align: 'center', sorttable: false, editable: false, formatter: coldelFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'C.ID',
        sortorder: 'Desc',
        postData: { 'strwhere': escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) },
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/CostDetailsHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'C.ID');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );
    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var costtypeall = $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").val();
        var year1 = $("#ctl00_ContentPlaceHolder1_txt_costdate1").val();
        var year2 = $("#ctl00_ContentPlaceHolder1_txt_costdate2").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/CostDetailsHandler.ashx?n='" + tempRandom + " '&action=sel",
            postData: { 'unit': unit, 'keyname': keyname, 'year1': year1, 'year2': year2, 'costtypeall': costtypeall },
            page: 1,
            sortname: 'C.ID',
            sortorder: 'desc'
        }).trigger("reloadGrid");
    });
    //类型
    var flag = 'add';
    $("#btn_createCost").click(function () {
        var unitid = $("#ctl00_ContentPlaceHolder1_drp_unitAdd").val();
        //$("#DivAddCost").modal();
        $("h4", "#DivAddCost").text("增加成本明细");

        var data = "action=consttype&unitid=" + unitid;
        $("#ctl00_ContentPlaceHolder1_drp_costType").empty();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
            data: data,
            success: function (result) {
                if (result != null) {
                    var obj = result.ds;
                    var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                    $.each(obj, function (i, n) {
                        gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                    });
                    $("#ctl00_ContentPlaceHolder1_drp_costType").html(gcFzr_UnitOptionHtml);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });

        flag = 'add';
    });
    //id
    var costid = 0;
    //确定按钮事件
    $("#btn_addcount").click(function () {

        var unitid = $("#ctl00_ContentPlaceHolder1_drp_unitAdd").val();
        var costtype = $("#ctl00_ContentPlaceHolder1_drp_costType").val();
        var costtime = $("#txt_costdate").val();
        var chargemoney = $("#costcharge").val();
        var outchangemoney = $("#costOutcharge").val();
        var costnum = $("#costNum").val();
        var remark = $("#remark").val();
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").val();
        var costtypeall = $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").find("option:selected").val();
        var year1 = $("#ctl00_ContentPlaceHolder1_txt_costdate1").val();
        var year2 = $("#ctl00_ContentPlaceHolder1_txt_costdate2").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var userid = $("#ctl00_ContentPlaceHolder1_hidUserId").val();
        //验证
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if (unitid == "-1") {
            $("#drp_unitSpan").text("请选择生产部门!").show();
            return false;
        } else {
            $("#drp_unitSpan").hide();
        }
        if (costtype == "-1") {
            $("#drp_costTypeSpan").text("请选择类型!").show();
            return false;
        } else {
            $("#drp_costTypeSpan").hide();
        }
        if (costtime == "") {
            $("#txt_costdateSpan").text("请选择日期!").show();
            return false;
        } else {
            $("#txt_costdateSpan").hide();
        }
        if (chargemoney == "" || chargemoney == null) {
            $("#costchargeSpan").text("请输入金额!").show();
            return false;
        } else if (!reg.test(chargemoney) || chargemoney <= 0) {
            $("#costchargeSpan").text("请输入正确的数字!").show();
            return false;
        } else {
            $("#costchargeSpan").hide();
        }
        if (outchangemoney == "" || outchangemoney == null) {
            $("#costoutchargeSpan").text("请输入金额!").show();
            return false;
        } else if (!reg.test(outchangemoney) || outchangemoney <= 0) {
            $("#costoutchargeSpan").text("请输入正确的数字!").show();
            return false;
        } else {
            $("#costoutchargeSpan").hide();
        }
        if (costnum == "" || costnum == null) {
            $("#costNumSpan").text("请输入凭证号!").show();
            return false;
        } else {
            $("#costNumSpan").hide();
        }

        if (remark == "") {
            $("#remarkSpan").text("请输入备注信息!").show();
            return false;
        } else {
            $("#remarkSpan").hide();
        }
        if (flag == "add") {
            tempRandom = Math.random() + new Date().getMilliseconds() + new Date().getSeconds();
            alert("添加成功!");
            $("#jqGrid").jqGrid('setGridParam', {
                url: "/HttpHandler/ProjectValueandAllot/CostDetailsHandler.ashx?n='" + tempRandom + " '&action=Add",
                postData: { 'unitid': unitid, 'costtype': costtype, 'costtime': costtime, 'chargemoney': chargemoney, 'outchangemoney': outchangemoney, 'costnum': costnum, 'remark': remark, 'unit': unit, 'keyname': keyname, 'year1': year1, 'year2': year2, 'userid': userid, 'costtypeall': costtypeall },
                page: 1,
                sortname: 'C.ID',
                sortorder: 'desc'
            }).trigger("reloadGrid");


        }
        else {
            tempRandom = Math.random() + new Date().getMilliseconds() + new Date().getMinutes();
            alert("修改成功!");
            $("#jqGrid").jqGrid('setGridParam', {
                url: "/HttpHandler/ProjectValueandAllot/CostDetailsHandler.ashx?n='" + tempRandom + " '&action=Edit",
                postData: { 'costid': costid, 'unitid': unitid, 'costtype': costtype, 'costtime': costtime, 'chargemoney': chargemoney, 'outchangemoney': outchangemoney, 'costnum': costnum, 'remark': remark, 'unit': unit, 'keyname': keyname, 'year1': year1, 'year2': year2, 'userid': userid, 'costtypeall': costtypeall },
                page: 1,
                sortname: 'C.ID',
                sortorder: 'desc'
            }).trigger("reloadGrid");


        }
        $("#DivAddCost").modal("hide");
    })
    //查看
    $(".cls_chk").live("click", function () {
        var costid = $(this).attr("rel");
        $("#labUnitCost").text($(this).parent().parent().find("td").eq(10).text());
        $("#labTypeCost").text($(this).parent().parent().find("td").eq(9).text());
        $("#labTime").text($(this).parent().parent().find("td").eq(5).text());
        $("#labNum").text($(this).parent().parent().find("td").eq(6).text());
        $("#labCharge").text($(this).parent().parent().find("td").eq(8).text() + "(元)");
        $("#labSub").text($(this).parent().parent().find("td").eq(7).text());
        $("#labOutCharge").text($(this).parent().parent().find("td").eq(3).text() + "(元)");
        $("#DivShowCost").modal();
    });
    //编辑
    $(".allowEdit").live("click", function () {
        costid = $(this).attr("rel");
        typegroup = $(this).attr("setrel");
        if (typegroup == "0") {
            alert("导入数据不能编辑!");
            return false;
        } else {

            flag = "edit";
            $("h4", "#DivAddCost").text("修改成本明细");
            var unitid = $(this).parent().parent().find("td").eq(12).text();
            var typeid = $(this).parent().parent().find("td").eq(13).text();
            if (typeid == "99") {
                typeid = "138";
            }
            if (typeid == "128") {
                typeid = "139";
            }
            if (typeid == "124") {
                typeid = "139";
            }
            if (typeid == "135") {
                typeid = "139";
            }
            if (typeid == "133") {
                typeid = "140";
            }
            if (typeid == "122") {
                typeid = "140";
            }
            if (typeid == "126") {
                typeid = "140";
            }



            $("#ctl00_ContentPlaceHolder1_drp_unitAdd").val(unitid);

            $("#txt_costdate").val($(this).parent().parent().find("td").eq(5).text());
            $("#costNum").val($(this).parent().parent().find("td").eq(6).text());
            $("#costcharge").val($(this).parent().parent().find("td").eq(8).text());
            $("#remark").val($(this).parent().parent().find("td").eq(7).text());
            $("#costOutcharge").val($(this).parent().parent().find("td").eq(3).text());
            var data = "action=consttype&unitid=" + unitid;
            $("#ctl00_ContentPlaceHolder1_drp_costType").empty();
            $.ajax({
                type: "Get",
                dataType: "json",
                url: "../HttpHandler/ProjectValueandAllot/CostDetailsType.ashx?n=" + tempRandom,
                data: data,
                success: function (result) {
                    if (result != null) {
                        var obj = result.ds;
                        var gcFzr_UnitOptionHtml = '<option value="-1">-----选择类型-----</option>';
                        $.each(obj, function (i, n) {
                            gcFzr_UnitOptionHtml += '<option value="' + n.ID + '">' + n.costName + '</option>';
                        });
                        $("#ctl00_ContentPlaceHolder1_drp_costType").html(gcFzr_UnitOptionHtml);

                        $("#ctl00_ContentPlaceHolder1_drp_costType").val(typeid);
                        $("#DivAddCost").modal();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("系统错误!");
                }
            });

        }


    });
    //删除
    $(".cls_delete").live("click", function () {
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").val();
        var costtype = $("#ctl00_ContentPlaceHolder1_drp_costTypeAll").find("option:selected").val();
        var year1 = $("#ctl00_ContentPlaceHolder1_txt_costdate1").val();
        var year2 = $("#ctl00_ContentPlaceHolder1_txt_costdate2").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        tempRandom = Math.random() + new Date().getMilliseconds() + new Date().getHours();
        var costid = $(this).attr("rel");
        if (window.confirm("是否要删除这条成本明细？")) {
            $("#jqGrid").jqGrid('setGridParam', {
                url: "/HttpHandler/ProjectValueandAllot/CostDetailsHandler.ashx?n='" + tempRandom + " '&action=del",
                postData: { 'unit': unit, 'keyname': keyname, 'year1': year1, 'year2': year2, 'costid': costid, 'costtype': costtype },
                page: 1,
                sortname: 'C.ID',
                sortorder: 'desc'
            }).trigger("reloadGrid");
            $("#jqGrid").trigger('reloadGrid');
            alert("删除成功!");

        }

    })

})

//查看
function colShowFormatter(celvalue, options, rowData) {

    var typeGroup = rowData["isexpord"];
    return '<a href="#DivShowCost" rel="' + celvalue + '" class="cls_chk"  data-toggle=\"modal\" alt="查看成本明细">查看</a>|<a href="#" setrel="' + typeGroup + '" rel="' + celvalue + '" class="allowEdit"   alt="编辑成本明细">编辑</a>';


}
function coldelFormatter(celvalue, options, rowData) {
    return '<a href="#" rel="' + celvalue + '" class="cls_delete" alt="删除成本明细">删除</a>';
}

//统计 
function completeMethod() {
    //
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var summem = 0, sumpro = 0, sumproA = 0, sumproB = 0, sumproC = 0, sumproD = 0;

    //二种写法
    $(rowIds).each(function () {

        sumproA = sumproA + parseInt($("#" + this).find("td").eq(8).text());

        $("#" + this).find("td").eq(1).text(this);

    });

    $("#jqGrid").footerData('set', { costTimeS: "合计:", costCharge: sumproA }, false);
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger("reloadGrid");
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        } else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}