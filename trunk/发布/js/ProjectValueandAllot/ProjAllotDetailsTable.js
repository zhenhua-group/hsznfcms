﻿$(document).ready(function() {
    //设计人员
    $(":text[class='cls_cell_dsign_bl']").keyup(function() {
        //得到专业id与个人id
        var ids = $(this).attr("id").split('_');
        var userid = ids[1];
        var speid = ids[2];
        //设计人员总费用
        var allcount = parseFloat($("#sp_disgnuserc").text());
        //专业分配比例
        var speprt = parseFloat($("#cfg_danwp_" + speid + "").attr("rel")) * 0.01;
        //个人分配比例
        var reg = /^\d+(\.\d+)?$/;
        if (!reg.test($(this).val())) {
            return false;
        }
        var ipersent = parseFloat($(this).val()) * 0.01;
        //计算出金额
        var allmoney = allcount * speprt * ipersent;
        $("#dsusercount_" + userid + "_" + speid + "").val(Math.round(allmoney, 1));
        //总和
        CalutorB();
    });

    //专业注册人员
    $(":text[class='cls_cell_reg_bl']").keyup(function() {
        var ids = $(this).attr("id").split('_');
        var userid = ids[1];
        var speid = ids[2];
        //专业注册人员总费用
        var allcount = parseFloat($("#sp_zhucec").text());
        //专业分配比例
        var speprt = parseFloat($("#cfg_danwp_" + speid + "").attr("rel")) * 0.01;
        //个人分配比例
        var ipersent = parseFloat($(this).val()) * 0.01;
        //计算金额
        var allmoney = allcount * speprt * ipersent;
        $("#regusercount_" + userid + "_" + speid + "").val(Math.round(allmoney, 1));

        CalutorA();
    })
    //校对人员
    $(":text[class='cls_cell_xh_bl']").keyup(function() {
        //得到专业id与个人id
        var ids = $(this).attr("id").split('_');
        var userid = ids[1];
        var speid = ids[2];
        //设计人员总费用
        var allcount = parseFloat($("#sp_xiaoduic").text());
        //专业分配比例
        var speprt = parseFloat($("#cfg_danwp_" + speid + "").attr("rel")) * 0.01;
        //个人分配比例
        var ipersent = parseFloat($(this).val()) * 0.01;
        //计算出金额
        var allmoney = allcount * speprt * ipersent;
        $("#xiaohecount_" + userid + "_" + speid + "").val(Math.round(allmoney, 1));
        //综合
        CalutorC();
    });

    //审核人员
    $(":text[class='cls_cell_sh_bl']").keyup(function() {
        //得到专业id与个人id
        var ids = $(this).attr("id").split('_');
        var userid = ids[1];
        var speid = ids[2];
        //设计人员总费用
        var allcount = parseFloat($("#sp_shenhc").text());
        //专业分配比例
        var speprt = parseFloat($("#cfg_danwp_" + speid + "").attr("rel")) * 0.01;
        //个人分配比例
        var ipersent = parseFloat($(this).val()) * 0.01;
        //计算出金额
        var allmoney = allcount * speprt * ipersent;
        $("#shenhecount_" + userid + "_" + speid + "").val(Math.round(allmoney, 1));
        //综合
        CalutorD();
    });

    //审定人员
    $(":text[class='cls_cell_sd_bl']").keyup(function() {
        //得到专业id与个人id
        var ids = $(this).attr("id").split('_');
        var userid = ids[1];
        var speid = ids[2];
        //设计人员总费用
        var allcount = parseFloat($("#sp_shendc").text());
        //专业分配比例
        var speprt = parseFloat($("#cfg_danwp_" + speid + "").attr("rel")) * 0.01;
        //个人分配比例
        var ipersent = parseFloat($(this).val()) * 0.01;
        //计算出金额
        var allmoney = allcount * speprt * ipersent;
        $("#shendingcount_" + userid + "_" + speid + "").val(Math.round(allmoney, 1));
        //综合
        CalutorE();
    });

    //单位综合分配比例
    var idanw = 0;
    $("div[rel='rel_danwei']").each(function(i) {
        idanw += parseFloat($(this).text());
    });
    $("#bt_danwei_all").text(idanw);
    //专业百分比综合统计
    var iprt = 0;
    $("span[class='cls_danweip']").each(function(i) {
        iprt += parseFloat($(this).attr("rel"));
    });
    $("#bt_danweip_all").text(iprt + "%");
    //保存分配数据
    $("#btn_save").click(function() {
        alert("保存数据");
    });
    $("#btn_back").click(function() {
        document.location.href = "ProValueandAlltList.aspx";
    });
});
function CalutorB() {
    //B1
    SubBCDE($("#bt_b1_all"), 'B1');
    //B2
    SubBCDE($("#bt_b2_all"), 'B2');
    //B3
    SubBCDE($("#bt_b3_all"), 'B3');
    //B4
    SubBCDE($("#bt_b4_all"), 'B4');
}
function CalutorC() {
    //C1
    SubBCDE($("#bt_c1_all"), 'C1');
    //C2
    SubBCDE($("#bt_c2_all"), 'C2');
}
function CalutorD() {
    //D
    SubBCDE($("#bt_d_all"), 'D');
}
function CalutorE() {
    //E
    SubBCDE($("#bt_e_all"), 'E');
}
function CalutorA() {
    SubBCDE($("#bt_a_all"), 'A');
}
//计算各个单元的总和
function SubBCDE(ctr, cellid) {
    var temp = 0;
    $(":text[vname='" + cellid + "']").each(function(i) {
        temp += parseFloat($(this).val());
    });
    ctr.text(temp);
}