﻿$(function () {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueRetentionStatisticalTableHadler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '',  '项目名称', '承接部门', '合同额(万元)', '实收总额(万元)', '分配金额(元)', '自留金额(元)', '审核金额(元)', '设计金额(元)', '分配详细'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'p.cpr_Acount', width: 100, align: 'center' },
                             { name: 'PayShiCount', index: 'PayShiCount', width: 100, align: 'center' },
                             { name: 'AllotCount', index: 'AllotCount', width: 100, align: 'center' },
                             { name: 'TheDeptValueCount', index: 'TheDeptValueCount', width: 100, align: 'center' },
                             { name: 'AuditCount', index: 'AuditCount', width: 100, align: 'center' },
                             { name: 'DesignCount', index: 'DesignCount', width: 100, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 60, align: 'center', formatter: colStrShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "sel", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'p.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueRetentionStatisticalTableHadler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });




    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueRetentionStatisticalTableHadler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueRetentionStatisticalTableHadler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });


});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//详细
function colStrShowFormatter(celvalue, options, rowData) {
    var auditString = '<a href="../ProjectValueandAllot/ProjectValueAllotDetailsBymaster.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '" class="cls_chk" style="height: 26px;">查看</a>';

    return auditString;
}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }

    var sumAcount = parseFloat($("#jqGrid").getCol("Cpr_Acount", false, 'sum')).toFixed(4);
    var allPayShiCount = parseFloat($("#jqGrid").getCol("PayShiCount", false, 'sum')).toFixed(4);
    var allAllotCount = parseFloat($("#jqGrid").getCol("AllotCount", false, 'sum')).toFixed(4);
    var allTheDeptValueCount = parseFloat($("#jqGrid").getCol("TheDeptValueCount", false, 'sum')).toFixed(4);
    var allAuditCount = parseFloat($("#jqGrid").getCol("AuditCount", false, 'sum')).toFixed(4);
    var allDesignCount = parseFloat($("#jqGrid").getCol("DesignCount", false, 'sum')).toFixed(4);

    $("#jqGrid").footerData('set', { pro_name: "合计:", Cpr_Acount: sumAcount, PayShiCount: allPayShiCount, AllotCount: allAllotCount, TheDeptValueCount: allTheDeptValueCount, AuditCount: allAuditCount ,DesignCount:allDesignCount}, false);


}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
