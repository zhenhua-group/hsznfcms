﻿var MainPager = function () {
    //弹出层加载
    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
              '<div class="progress progress-striped active">' +
                '<div class="progress-bar" style="width: 100%;"></div>' +
              '</div>' +
            '</div>';

    $.fn.modalmanager.defaults.resize = true;

    //点击菜单显示加载中
    $(".jztcc a").live("click", function () {     
        $('body').modalmanager('loading');
    });
   
    //定时请求数据
    var UpdateMsgConnt = function () {
        $(document).everyTime(90000, 'A', function (i) { sendRequestMsg(); });
    }
    //查询系统消息
    var sendRequestMsg = function () {

        $.get("/HttpHandler/HandlerMainPage.ashx", { "action": "getmsg", "userno": usersysno, "n": Math.random() * 100000 }, function (data) {
            if (data != null) {
                //注销右下提示消息框
                //msgSound(data.MsgCount);
                //新消息
                $("#sp_newMsgCount").text(data.MsgCount);
                $("#lbl_newMsgCount").text(data.MsgCount);
                //待办消息
                $("#sp_donemsgcount").text(data.DoneCount);
                $("#lbl_donemsgcount").text(data.DoneCount);

                if (parseInt(data.MsgCount) > 0 || parseInt(data.DoneCount)) {

                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "500",
                        "hideDuration": "500",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    //提示
                    toastr.error("您有" + data.MsgCount + "条新消息！" + data.DoneCount + "条待办事项！");

                }


                //刷新列表信息
                var donelist = data.donemsglist;
                var newlist = data.newmsglist;

                if (donelist != null) {

                    $("#ul_donemsgcontainer").find("li").remove();
                    var htmlString = "";
                    for (var i = 0; i < donelist.length; i++) {
                        var key = donelist[i];
                        htmlString += "<li><a href=\"../Coperation/cpr_SysMsgListViewBymaster.aspx?action=done&flag=A&MessageType=" + key.MsgType + "\">  <span class=\"subject\"> </span><span class=\"message\">" + key.MessageContent + " </span></a></li>";
                        //                        <span class=\"percent\">" + key.PrecentAcountModel + "%</span> </span><span class=\"progress\"><span style=\"width: " + key.PrecentAcountModel + "%;\" class=\"progress-bar progress-bar-success\"aria-valuenow=\"" + key.PrecentAcountModel + "\" aria-valuemin=\"0\" aria-valuemax=\"100\"><span class=\"sr-only\">" + key.PrecentAcountModel + "% </span></span></span><span class=\"task\"><span class=\"desc\">" + key.MsgTypeName + "</span>
                    }
                    $("#ul_donemsgcontainer").append(htmlString);
                }

                if (newlist != null) {
                    $("#ul_newmsgcontainer").find("li").remove();

                    var htmlString = "";
                    for (var i = 0; i < newlist.length; i++) {
                        var key = newlist[i];
                        htmlString += "<li><a href=\"../Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A&MessageType=" + key.MsgType + "\">  <span class=\"subject\"> </span><span class=\"message\">" + key.MessageContent + " </span></a></li>";
                    }
                    $("#ul_newmsgcontainer").append(htmlString);
                }

            }
        });

    }
    var loadLeftMap = function () {
        $.get("/HttpHandler/SetCompanyInfo.ashx?n=" + (Math.random() + new Date().getMilliseconds()), { "action": "loadmap" }, function (date) {
            $("#proj_map").attr("href", date);
        });
    }
    var msgSound = function (msgcount) {
        if (msgcount > 0) {
            ShowMsgRightBottom("系统消息提示", "", 0, msgcount, 0, "/Coperation/cpr_SysMsgListViewBymaster.aspx", "");
            //播放声音
            ShowMsgVoice();
        }
        //$("#header_inbox_bar").click(function () {
        //    ShowMsgRightBottom("系统消息提示", "", 0, msgcount, 0, "/Coperation/cpr_SysMsgListViewBymaster.aspx", "");
        //})
        $("#header_msg_bar").click(function () {
            $("#pop").show();
        })
    }
    //右下角弹出提示 2014-11-28 qpl
    function ShowMsgRightBottom(title, url, oacount, tomcount, tbmcount, tomurl, tbmurl) {
        var intro = "<div style=\"text-align:left;\">";
        //intro += "您有<span style=\"color:red;font-weight:bold;\">(" + oacount + ")</span>条[OA 办公]系统消息！";
        //intro += "<a href=\"../general/sms/receive.aspx\" target=\"iframepage\">查看</a><br />";
        intro += "您有<span style=\"color:red;font-weight:bold;\">(" + tomcount + ")</span>条[ 生产经营 ]系统消息！";
        intro += "<a href=\"" + tomurl + "\" >查看</a><br/>";
        //intro += "您有<span style=\"color:red;font-weight:bold;\">(" + tbmcount + ")</span>条[ 打印归档 ]系统消息！";
        //intro += "<a href=\"" + tbmurl + "\" target=\"_blank\">查看</a><br/>";
        var pop = new Pop(title, tomurl, intro);
    }
    //播放消息声音
    function ShowMsgVoice() {
        //cookie 判断只提示用户一次
        var key_voice = $.cookie("key_voice_tom");
        if (key_voice == null || key_voice == undefined || key_voice == 'null') {
            $.cookie("key_voice_tom", "false");
            document.getElementById("new_sms").innerHTML = "<OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codeBase='/js/callsound/swflash.cab#version=6,0,0,0' height='0' id='sms_sound' width='0'><PARAM NAME='movie' VALUE='/js/callsound/1.swf'><PARAM NAME='quality' VALUE='high'><embed src='/js/callsound/1.swf' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' width='0' height='0'></embed></OBJECT>";

        }
    }
    //加载快捷菜单
    var loadLinkMenu = function () {
        //取得顶部菜单与快捷菜单
        $.get("/HttpHandler/SetCompanyInfo.ashx", { action: "slt" }, function (data) {

            if (data != null) {

                var links = data.link;
                //顶部菜单 院主页
                if (links[0] != null) {
                    for (var key in links[0]) {
                        $("#link_gw").attr("href", links[0][key]);
                    }
                }
                //邮箱
                if (links[1] != null) {
                    for (var key in links[1]) {
                        $("#link_yx").attr("href", links[1][key]);
                    }
                }
                //FTP
                if (links[2] != null) {
                    for (var key in links[2]) {
                        $("#link_ftp").attr("href", links[2][key]);
                    }
                }
                //其他快捷菜单
                var i = 3;
                var link = '';
                for (i; i < links.length; i++) {
                    for (var key in links[i]) {
                        link += "<li><a href=\"" + links[i][key] + "\" target=\"_blank\">" + key + "</a></li>";
                    }
                }
                if (link != '') {
                    $("#left_linkcontainer").append(link);
                }
            }
        }, 'json');
    }
    var loadLeftMenu = function () {
        //客户
        LoadLeftMenu.GetLeftMenu(3);
        //合同权限
        LoadLeftMenu.GetLeftMenu(1);
        //项目权限
        LoadLeftMenu.GetLeftMenu(4);
        //领导驾驶舱
        LoadLeftMenu.GetLeftMenu(6);
        //系统设置
        LoadLeftMenu.GetLeftMenu(7);
        //技术质量
        LoadLeftMenu.GetLeftMenu(8);
        //绩效权限
        LoadLeftMenu.GetLeftMenu(9);
        //考勤权限
        LoadLeftMenu.GetLeftMenu(10);
    }
    //加载页面设置
    var loadPage = function () {
        //选中菜单样式选中
        $(".page-sidebar-menu a").each(function (i) {
            var allurl = $(this).attr("href");
           
            if (allurl.indexOf(pageurl) > -1) {
              
                var this_obj = $(this);
                //选中本节点
                this_obj.parent().attr("class", "active");
                //节点级别
                var dept = this_obj.attr("dept");
                //栏目
                var menu;
                //顶级栏目
                var topmenu;
                //二级
                if (dept == "2") {
                    //栏目
                    topmenu = this_obj.parent().parent().parent().attr("class", "active");
                }
                else if (dept == "3") {
                    menu = this_obj.parent().parent().parent().attr("class", "active open");
                    //顶级栏目
                    topmenu = menu.parent().parent().attr("class", "active");
                }
                //单独显示
                topmenu.siblings().attr("class", "");
            }
        });

        $("#btnHome").click(function () {

            location.href = "/mainpage/WelcomePage.aspx";
        });
    }
    return {
        init: function () {

            loadPage();

            loadLeftMenu();

            loadLinkMenu();

            sendRequestMsg();

            //UpdateMsgConnt();
            loadLeftMap();
        }
    };
}();