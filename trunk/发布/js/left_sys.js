﻿$(document).ready(function () {
    //部门管理
    $("#link_bumen").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/Sys_UnitList.aspx");
    });
    //角色管理
    $("#link_jiaose").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/Sys_Role.aspx");
    });
    //用户管理
    $("#link_rygl").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/Sys_Mem.aspx");
    });
    //权限管理
    $("#link_quanxian").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/RoleManager.aspx");
    });
    //字典管理
    $("#link_zidian").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/Sys_dictionary.aspx");
    });
    //配置管理
    $("#link_peizhi").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/cpr_AddCoperation.aspx");
    });
    //日志管理
    $("#link_rizhi").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/WorkLogList.aspx");
    });
    //公告管理
    $("#link_gonggao").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/PubMsgList.aspx");
    });
    //合同配置管理
    $("#coperationAuditConfig").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/CoperationAuditConfig.aspx");
    });
    //项目审核配置
    $("#projectAuditConfig").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProjectAudit/ProjectAuditConfig.aspx");
    });
    //工号申请
    $("#distributionConfig").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/DistributionJobNumConfig.aspx");
    });

    //工号生成配置
    $("#projectNum").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/ProjectNumConfig.aspx");
    });
    //生产经营配置
    $("#projectallot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/ProjectAllotConfig.aspx");
    });
    //项目策划审核配置
    $("#projectPlanAuditConfig").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectPlan/ProjectPlanAudit/ProjectPlanAuditConfig.aspx");
    });
    //项目分配表配置
    $("#projectAllotTable").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectValueandAllot/ProjectFeeAllotConfig.aspx");
    });
    //项目分配表配置
    $("#menu_config").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/LeftMenuConfig.aspx");
    });

    $("#link_editpower").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/ActionPowerManager.aspx");
    });
    $("#link_linkset").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/SetCompanyConfig.aspx");
    });

    $("#link_info").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/SetCompanyInfo.aspx");
    });

    $("#CoperationNum").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/SetCoperationConfig.aspx");
    });

    $("#setunitallot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/SetUnitAllot.aspx");
    });
    //合同目标值
    $("#setCop").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/SetUnitCopAllot.aspx");
    });
    //收费目标值
    $("#setCharge").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/SetUnitChargeAllot.aspx");
    });
    //资质  管理
    $("#quarilationComp").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/QualificationList.aspx");
    });
    //项目分部图配置
    $("#pro_map").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/PromapShow.aspx");
    });
    //项目产值分配
    $("#ProValueAllot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/SystemSet/ProValueParameterConfig.aspx");
    });
    //经济所项目各项配合是否可以编辑
    $("#JJSValueAllot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/JJSValueParameterConfig.aspx");
    });
    //工程设计出图卡
    $("#projectImaTable").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ProImageAuditConfig.aspx");
    });
    //工程设计出图卡
    $("#plotInfo").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ProjectPlotInfoAuditConfig.aspx");
    });
    //工程归档
    $("#file").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ProjectFileInfoAuditConfig.aspx");
    });
    //财务报表数据导入
    $("#cwbbsjdr").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/ImportFinancialData.aspx");
    });
    //系数是否可以编辑
    $("#ProValueIsEidt").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/ProjectParameterSeting.aspx");
    });

    //建筑职称设置
    $("#archlevel").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../SystemSet/SetArchLevel.aspx");
    });

    //显示所有菜单控制
    LoadLeftMenu.GetLeftMenu(7);
    //是否显示配置菜单
    $.post("../HttpHandler/LoadLeftMenuHandler.ashx", { action: "chk" }, function (data) {
        if (data == "true") {
            $("#menu_config").parent("td:first").show();
            $("#link_editpower").parent("td:first").show();
            $("#link_quanxian").parent("td:first").show();
        }
    });
});