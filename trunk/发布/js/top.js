﻿// JavaScript Document
$(document).ready(function () {
    //首页
    $("#btn_home2").click(function () {
        showLeft("index.html", "welcome.aspx");
    });
    //主页
    $("#btn_home").click(function () {
        showLeft("index.html", "welcome.aspx");
    });
    //前进
    $("#btn_go").click(function () {
        window.parent.frames["mainFrame"].frames["centerFrame"].frames["leftFrame"].history.forward();
        window.parent.frames["mainFrame"].frames["centerFrame"].frames["rightFrame"].history.forward();
    });
    //后退
    $("#btn_back").click(function () {
        window.parent.frames["mainFrame"].frames["centerFrame"].frames["leftFrame"].history.back();
        window.parent.frames["mainFrame"].frames["centerFrame"].frames["rightFrame"].history.back();
    });
    //刷新
    $("#btn_fresh").click(function () {
        window.parent.frames["mainFrame"].frames["centerFrame"].frames["leftFrame"].window.location.reload();
        window.parent.frames["mainFrame"].frames["centerFrame"].frames["rightFrame"].window.location.reload();
    });
    // 退出
    $("#btn_exit").click(function () {
        if (confirm("确定要退出系统吗！")) {
            $.cookie("_username", null);
            $.cookie("_password", null);
            window.parent.document.location.href = "LogOut.aspx";
        }
    });
    // 个人信息
    $("#btn_info").click(function () {
        $(window.parent.frames["mainFrame"].frames["centerFrame"].document).find("#leftFrame").attr("src", "leftmenu/left_persinal.aspx");
        $(window.parent.frames["mainFrame"].frames["centerFrame"].document).find("#rightFrame").attr("src", "SystemSet/SetPersinal.aspx");
        hideLeft();
    });
    //客户信息
    $("#btn_customer").click(function () {
        //showLeft("left.html", "m_customer.html");
        $(window.parent.frames["mainFrame"].frames["centerFrame"].document).find("#leftFrame").attr("src", "leftmenu/left.html");
        $(window.parent.frames["mainFrame"].frames["centerFrame"].document).find("#rightFrame").attr("src", "mainpage/customer.html");
        hideLeft();
    });
    //合同管理
    $("#btn_coper").click(function () {
        showLeft("left_coper.html", "../mainpage/coperation.html");
    });
    // 项目信息
    $("#btn_projinfo").click(function () {
        showLeft("left_proj.aspx", "../mainpage/packlist.html");
    });

    // iso表单库
    $("#btn_isoform").click(function () {
        showLeft("ShowISOFrom.aspx", "../mainpage/ISOlist.html");
    });
    //企业项目信息
    $("#btn_proj").click(function () {
        showLeft("left_projinfo.html", "../mainpage/project.html");
    });
    //领导
    $("#btn_leader").click(function () {
        showLeft("left_leader.html", "../mainpage/leader.html");
    });
    //系统管理
    $("#btn_sys").click(function () {
        showLeft("left_sys.html", "../mainpage/systemset.html");
    });
    //点击进入消息列表
    $("#sysMsgSpan").click(function () {
        window.parent.frames["mainFrame"].showMessageBox();
    });
});
// 框架的左右url
function showLeft(url, r_url) {
    //左侧
    $(window.parent.frames["mainFrame"].frames["centerFrame"].document).find("#leftFrame").attr("src", "leftmenu/" + url);
    //右侧
    $(window.parent.frames["mainFrame"].frames["centerFrame"].document).find("#rightFrame").attr("src", "mainpage/" + r_url);
    //隐藏
    hideLeft();
}
//隐藏二级树
function hideLeft() {
    //二级树的隐藏
    $("#frmTitle2", window.parent.frames["mainFrame"].frames["centerFrame"].document).hide();
    $("#frmTitleSub", window.parent.frames["mainFrame"].frames["centerFrame"].document).hide();
}
