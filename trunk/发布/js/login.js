﻿$(document).ready(function () {
    // 判断自动用户登录
    var cookie_uid = $.cookie("_username");
    var cookie_pwd = $.cookie("_password");
    if (cookie_uid != null && cookie_uid != 'undefined') {
        $.ajax({
            type: "POST",
            url: "HttpHandler/LoginHandler.ashx?action=login",
            data: "strlogin=" + escape(cookie_uid) + "&strpwd=" + escape(cookie_pwd),
            success: function (msg) {
                if (msg == "success") {
                    window.location.href = "../mainpage/WelcomePage.aspx"
                }
                else {
                    alert("用户名或密码错误！");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("数据库连接错误！");
            }
        });
    }
    else {
        //登录
        $("#txt_login").focus();
        $("#btn_login").click(function () {
            var login = $("#txt_login").val();
            var pwd = $("#txt_pwd").val();
            var msg = "";
            if (login == "") {
                msg += "用户名不能为空！\n";
            }
            if (login.length > 20) {
                msg += "用户名长度太长！\n";
            }
            if (pwd.length > 20) {
                msg + "密码过长！\n";
            }
            if (msg != "") {
                alert(msg);
                $("#txt_login").focus();
                return false;
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "HttpHandler/LoginHandler.ashx?action=login",
                    data: "strlogin=" + escape($("#txt_login").val()) + "&strpwd=" + escape($("#txt_pwd").val()),
                    success: function (msg) {
                        if (msg == "success") {
                            //登录成功设置cookie
                            if ($("#chk_auto").attr("checked")) {
                                $.cookie("_username", escape($("#txt_login").val()), { expires: 14 });
                                $.cookie("_password", escape($("#txt_pwd").val()), { expires: 14 });
                            }
                            window.location.href = "../mainpage/WelcomePage.aspx"
                        }
                        else {
                            alert("用户名或密码错误！");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("数据库连接错误！");
                    }
                });
            }
        });
    }
});


document.onkeydown = function (evt) {
    var evt = window.event ? window.event : evt;
    if (evt.keyCode == 13) {
        $("#btn_login").click();
    }
}