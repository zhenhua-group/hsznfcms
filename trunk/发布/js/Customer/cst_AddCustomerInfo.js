﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //添加用户信息
    $("#btn_Save").click(function () {
        //取得文化框
        var txtCst_No = $.trim($("#txtCst_No").val());
        var txtCst_Brief = $.trim($("#txtCst_Brief").val());
        var txtCst_Name = $.trim($("#txtCst_Name").val());
        var txtCpy_Address = $.trim($("#txtCpy_Address").val());
        var txtCode = $.trim($("#txtCode").val());
        var txtLinkman = $.trim($("#txtLinkman").val());
        var txtCpy_Phone = $.trim($("#txtCpy_Phone").val());
        var txtCpy_Fax = $.trim($("#txtCpy_Fax").val());
        var msg = "";
        //序号
        if (txtCst_No == "") {
            msg += "请填写编号！<br/>";
        }
        //客户简称
        if (txtCst_Brief == "") {
            msg += "请填写客户简称！<br/>";
        }
        if (txtCst_Name == "") {
            msg += "请填写客户名称!！<br/>"
        }
        if (txtCpy_Address == "") {
            msg += "请填写公司地址！<br/>";
        }
        if (txtCode != "") {
            var reg = /^[0-9][0-9]{5}$/;
            if (!reg.test(txtCode)) {
                msg += "邮编格式不正确！<br/>";
            }
        }
        if (txtCpy_Fax != "") {
            var reg = /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/;
            if (!reg.test(txtCpy_Fax)) {
                msg += "公司传真格式不正确！</br>";
            }
        }
        if (txtCpy_Phone != "") {
            var reg = /(^((0[1,2]{1}\d{1}-?\d{8})|(0[3-9]{1}\d{2}-?\d{7,8}))$)|(^0?(13[0-9]|15[0-35-9]|18[01236789]|14[57])[0-9]{8}$)/;
            if (!reg.test(txtCpy_Phone)) {
                msg += "请输入固定电话或手机号！</br>";
            }
        }
        //扩展信息
        var txtCst_EnglishName = $("#txtCst_EnglishName").val();
        var txtEmail = $("#txtEmail").val();
        var txtCpy_PrincipalSheet = $("#txtCpy_PrincipalSheet").val();
        var txtBankAccountNo = $("#txtBankAccountNo").val();
        var txtBankName = $("#txtBankName").val();

        if (txtBankAccountNo != "") {
            if (isNaN(txtBankAccountNo)) {
                msg += "银行帐号必须为数字！</br>";
            } else {
                if (txtBankName == "") {
                    msg += "需要填写银行名称！</br>";
                }
            }
        }
        if (txtEmail != "") {
            var reg = /^[a-zA-Z]([a-zA-Z0-9]*[-_.]?[a-zA-Z0-9]+)+@([\w-]+\.)+[a-zA-Z]{2,}$/;
            if (!reg.test(txtEmail)) {
                msg += "邮箱格式不正确！</br>";
            }
        }
        if (txtCpy_PrincipalSheet != "") {
            var strregex = "^((https|http|ftp|rtsp|mms)?://)"
                    + "?(([0-9a-za-z_!~*'().&=+$%-]+: )?[0-9a-za-z_!~*'().&=+$%-]+@)?"
                    + "(([0-9]{1,3}.){3}[0-9]{1,3}"
                    + "|"
                    + "([0-9a-za-z_!~*'()-]+.)*"
                    + "([0-9a-za-z][0-9a-za-z-]{0,61})?[0-9a-za-z]."
                    + "[a-za-z]{2,6})"
                    + "(:[0-9]{1,4})?"
                    + "((/?)|"
                    + "(/[0-9a-za-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
            var re = new RegExp(strregex);
            if (!re.test(txtCpy_PrincipalSheet)) {
                msg += "公司主页格式不正确！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        } else {
            //获取所有Hidden
            var hiddenArr = $(":hidden", $("#gv_Attach"));
            $.each(hiddenArr, function (index, item) {
                var value = $("#attachHidden").val() + $(item).val() + ",";
                $("#attachHidden").val(value);
            });
            $(this).hide();          
            return true;
        }
    });
    //行变色
    $(".cls_show_cst_jiben>tbody>tr:odd").attr("style", "background-color:#FFF");
    //联系人信息
    var contact = new Contact(null, $("#DivAddLinkMan"));
    //添加联系人事件
    $("#addLinkManActionLink").click(function () {
        contact.Clear();
        contact.IsEdit = false;
        contact.usetype = 1;
        ClearTipSpan();
        $("#DivAddLinkMan").dialog({
            autoOpen: false,
            modal: true,
            width: 590,
            height: 310,
            resizable: false,
            title: "添加联系人",
            buttons:
			{
			    "保存": function () {
			        contact.SaveContact(this);
			    },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        //组织浏览器默认行为
        return false;
    });


    //编辑联系人事件
    $("span[class=update]").live("click", function () {
        var cid = $(this).attr("rel");
        contact.Clear();
        contact.IsEdit = true;
        contact.GetContactInfo(cid);
        contact.usetype = 1;
        contact.SysNo = cid;
        ClearTipSpan();
        //查询原始信息并绑定
        $("#DivAddLinkMan").dialog({
            autoOpen: false,
            modal: true,
            width: 580,
            height: 310,
            resizable: false,
            zIndex: 99999,
            title: "编辑联系人",
            buttons:
			{
			    "保存": function () { contact.SaveContact(this); },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        return false;
    });



    var satisfactionInfo = new SatisfactionInfo(null, $("#DivAddSatisfaction"));

    //用户满意度事件
    $("#addSatisfactionActionLink").click(function (event) {
        satisfactionInfo.IsEdit = false;
        satisfactionInfo.Clear();
        satisfactionInfo.UseType = 1;
        ClearTipSpan();
        $("#DivAddSatisfaction").dialog({
            autoOpen: false,
            modal: true,
            width: 590,
            height: 420,
            resizable: false,
            zIndex: 99999,
            title: "添加用户满意度",
            buttons:
			{
			    "保存": function () { satisfactionInfo.SaveSatisfaction(this) },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        //组织浏览器默认行为
        return false;
    });


    //用户满意修改事件
    $("span[id=update]").live("click", function () {
        var sid = $(this).attr("rel");
        satisfactionInfo.IsEdit = true;
        satisfactionInfo.Clear();
        satisfactionInfo.UseType = 1;
        satisfactionInfo.SysNo = sid;
        satisfactionInfo.GetSatisfactionInfo(sid);
        ClearTipSpan();
        //查询原始信息并绑定
        $("#DivAddSatisfaction").dialog({
            autoOpen: false,
            modal: true,
            width: 580,
            height: 420,
            resizable: false,
            zIndex: 99999,
            title: "编辑用户满意度",
            buttons:
			{
			    "保存": function () { satisfactionInfo.SaveSatisfaction(this) },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        //组织浏览器默认行为
        return false;
    });


});
//判断汉子的实际长度
function DataLength(fData) {
    var intLength = 0;
    for (var i = 0; i < fData.length; i++) {
        if ((fData.charCodeAt(i) < 0) || (fData.charCodeAt(i) > 255))
            intLength = intLength + 2;
        else
            intLength = intLength + 1;
    }
    return intLength;
}

//附件删除事件
function AttachDetele(tr) {
    if (confirm("确认是否删除附件信息?")) {
        //获取当前系统自增号
        var sysNo = $(tr).parents("tr:first").children("td:first").children("input").val();
        $.post("/HttpHandler/ProcessUoloadFile.ashx", { "Action": "1", "SysNo": sysNo }, function (jsonResult) {
            if (jsonResult == "1") {
                //服务端成功删除的场合
                $("tr[id=" + sysNo + "]", $("#gv_Attach")).remove();
            }
        });
    }
}
//创建提示信息Span 
function CreateTipSpan(element, tipContent) {
    element.parent("td:first").append("<span id=\"TipSpan\" style=\"color:red;\">" + tipContent + "</span>");
}
//清除提示信息Span 
function ClearTipSpan(container) {
    $("span[id=TipSpan]").remove();
}
//js 加载联系人信息
function ReloadCustomerList() {
    var data = "action=getcstinfo&cstno=" + $("#HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            var data = result.ds;
            if ($("#cst_datas tr").length > 1) {
                $("#cst_datas tr:gt(0)").remove();
            }
            $.each(data, function (i, n) {
                var row = $("#cst_row").clone();

                var oper = "<span rel='" + n.CP_Id + "' class='update' style='cursor: pointer;color:blue;'>编辑</span>&nbsp;&nbsp;<span rel='" + n.CP_Id + "' class='del' style='cursor: pointer;color:blue;'>删除</span>";
                row.find("#id").text(n.ContactNo);
                row.find("#name").text(n.Name);
                row.find("#zhiwu").text(n.Duties);
                row.find("#bumen").html(n.Department);
                row.find("#phone").html(n.Phone);
                row.find("#email").html(n.Email);
                var shortMark = n.Remark;
                if (shortMark.length > 20) {
                    shortMark = shortMark.substr(0, 20) + "...";
                }
                row.find("#mark").attr("title", n.Remark);
                row.find("#mark").html(shortMark); //By fbw 新增
                row.find("#oper").html(oper);
                row.find("#oper span[class=del]").click(function () {
                    if (confirm("确定要删除联系人吗？")) {
                        //删除事件
                        delLinkMan($(this));
                    }
                });
                //添加样式
                row.addClass("cls_TableRow");

                row.appendTo("#cst_datas");
            });

            data = "";

            if (result == "0") {
                $("#linkman_nodata").show();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}

//删除联系人
function delLinkMan(link) {
    var data = "action=dellinkinfo&linkid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//加载满意度
function ReloadStaticList() {
    var data = "action=getstainfo&stano=" + $("#HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            var data = result.ds;
            if ($("#static_datas tr").length > 1) {
                $("#static_datas tr:gt(0)").remove();
            }
            $.each(data, function (i, n) {
                var row = $("#static_row").clone();
                var oper = "<span style='cursor: pointer;color:blue;' rel='" + n.Sat_Id + "'  id='update'>编辑</span>&nbsp;&nbsp;<span style='cursor: pointer;color:blue;' rel='" + n.Sat_Id + "' id='del'>删除</span>";
                row.find("#st_id").text(n.Sat_No);
                row.find("#st_content").text(n.Sch_Context);
                row.find("#st_myd").text(n.Sat_Leve);
                row.find("#st_time").html(n.Sch_Time);
                row.find("#st_type").html(n.Sch_Way);
                var shortMark = n.Remark;
                if (shortMark.length > 20) {
                    shortMark = shortMark.substr(0, 20) + "...";
                }
                row.find("#st_mark").attr("title", n.Remark);
                row.find("#st_mark").html(shortMark); //By fbw 新增
                row.find("#st_oper").html(oper);
                row.find("#st_oper span[id=del]").click(function () {
                    if (confirm("确定要删除满意度调查吗？")) {
                        //删除事件
                        delStatic($(this));
                    }
                });
                //添加样式
                row.addClass("cls_TableRow");
                row.appendTo("#static_datas");
            });
            if (result == "0") {
                $("#static_nodata").show();
            }
            data = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}



//删除满意方式
function delStatic(link) {
    var data = "action=delsta&staid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}