﻿$(function () {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Customer/CustomerInfoHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '客户编号', '客户名称 ', '客户简称 ', '公司地址', '联系人', '公司电话', '传真号', '邮政编码', '客户英文名', '是否合作', '所在国家', '所在省份', '所在城市', '客户类型', '所属行业', '分支机构', '邮箱', '公司主页', '关系建立时间', '关系部门', '信用级别', '亲密度', '开户银行名称', '企业代码', '开户银行账号', '法定代表', '纳税人识别号', '备注','录入人', '录入时间', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 50, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', hidden: true, editable: true },
                             { name: 'Cst_No', index: 'Cst_No', width: 80, align: 'center' },
                             { name: 'Cst_Name', index: 'Cst_Name', width: 200, formatter: colNameShowFormatter },
                             { name: 'Cst_Brief', index: 'Cst_Brief', width: 100 },
                             { name: 'Cpy_Address', index: 'Cpy_Address', width: 200 },
                             { name: 'Linkman', index: 'Linkman', width: 60, align: 'center' },
                             { name: 'Cpy_Phone', index: 'Cpy_Phone', width: 80, align: 'center' },
                            
                             { name: 'Cpy_Fax', index: 'Cpy_Fax', width: 80, align: 'center' },
                             { name: 'Code', index: 'Code', width: 80, align: 'center' },                            
                             { name: 'Cst_EnglishName', index: 'Cst_EnglishName', width: 80, align: 'center', hidden: true },
                             { name: 'PartnerName', index: 'IsPartner', width: 80, align: 'center', hidden: true },
                             { name: 'Country', index: 'Country', width: 80, align: 'center', hidden: true },
                             { name: 'Province', index: 'Province', width: 80, align: 'center', hidden: true },
                             { name: 'City', index: 'City', width: 80, align: 'center', hidden: true },
                             { name: 'TypeName', index: 'Type', width: 80, align: 'center', hidden: true },
                             { name: 'ProfessionName', index: 'Profession', width: 80, align: 'center', hidden: true },
                              { name: 'BranchPart', index: 'BranchPart', width: 80, align: 'center', hidden: true },
                             { name: 'Email', index: 'Email', width: 80, align: 'center', hidden: true },
                             { name: 'Cpy_PrincipalSheet', index: 'Cpy_PrincipalSheet', width: 80, align: 'center', hidden: true },
                              { name: 'RelationTime', index: 'CreateRelationTime', width: 100, align: 'center', hidden: true },
                             { name: 'RelationDepartment', index: 'RelationDepartment', width: 80, align: 'center', hidden: true },
                             { name: 'CreditLeveName', index: 'CreditLeve', width: 80, align: 'center', hidden: true },
                             { name: 'CloseLeveName', index: 'CloseLeve', width: 80, align: 'center', hidden: true },
                             { name: 'BankName', index: 'BankName', width: 100, align: 'center', hidden: true },
                             { name: 'Cpy_Code', index: 'Cpy_Code', width: 80, align: 'center', hidden: true },
                             { name: 'BankAccountNo', index: 'BankAccountNo', width: 100, align: 'center', hidden: true },
                             { name: 'LawPerson', index: 'LawPerson', width: 80, align: 'center', hidden: true },
                             { name: 'TaxAccountNo', index: 'TaxAccountNo', width: 100, align: 'center', hidden: true },
                             { name: 'Remark', index: 'Remark', width: 80, align: 'center', hidden: true },
                             { name: 'InsertUser', index: 'InsertUserID', width: 60, align: 'center' },
                              { name: 'lrsj', index: 'InsertDate', width: 80, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', width: 40, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "Search", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) },
        loadonce: false,
        sortname: 'c.cst_id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Customer/CustomerInfoHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });

    //1、客户编号
    $("#Cst_No").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Cst_No").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(0).hide();
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtCst_No").val("");
        } else {
            $("#Cst_No").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(0).show();
        }
        isShowButton();
    });

    //2、客户名称
    $("#Cst_Name").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Cst_Name").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(1).hide();
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtCst_Name").val("");
        } else {
            $("#Cst_Name").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(1).show();
        }
        isShowButton();
    });

    //3、客户简称
    $("#Cst_Brief").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Cst_Brief").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(2).hide();
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val("");
        } else {
            $("#Cst_Brief").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(2).show();
        }
        isShowButton();
    });

    //4、公司地址
    $("#Cpy_Address").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Cpy_Address").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(3).hide();
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val("");
        } else {
            $("#Cpy_Address").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(3).show();
        }
        isShowButton();
    });

    //5、建设单位负责人
    $("#Linkman").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Linkman").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(4).hide();
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtLinkman").val("");
        } else {
            $("#Linkman").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(4).show();
        }
        isShowButton();
    });

    //6、公司电话
    $("#Cpy_Phone").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Cpy_Phone").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(5).hide();
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val("");
        } else {
            $("#Cpy_Phone").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(5).show();
        }
        isShowButton();
    });

    //6、所在省份
    $("#ctl00_ContentPlaceHolder1_Province").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Province").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(6).hide();
         //   $("#jqGrid").setGridParam("").hideCol("Province").trigger('reloadGrid');
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtProvince").val("");
        } else {
            $("#Province").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(6).show();
          //  $("#jqGrid").setGridParam("").showCol("Province").trigger('reloadGrid');
        }
        isShowButton();
    });

    //7、所在城市
    $("#ctl00_ContentPlaceHolder1_City").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#City").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(7).hide();
         //   $("#jqGrid").setGridParam("").hideCol("City").trigger('reloadGrid');
            //清空数据
            $("#ctl00_ContentPlaceHolder1_txtCity").val("");
        } else {
            $("#City").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(7).show();
          //  $("#jqGrid").setGridParam("").showCol("City").trigger('reloadGrid');
        }
        isShowButton();
    });

    //8、客户类别
    $("#ctl00_ContentPlaceHolder1_Type").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Type").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(8).hide();
          //  $("#jqGrid").setGridParam("").hideCol("Type").trigger('reloadGrid');
            //清空数据
            $("#ctl00_ContentPlaceHolder1_ddType").val("-1");
        } else {
            $("#Type").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(8).show();
         //   $("#jqGrid").setGridParam("").showCol("Type").trigger('reloadGrid');
        }
        isShowButton();
    });

    //8、所属行业
    $("#ctl00_ContentPlaceHolder1_Profession").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Profession").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(9).hide();
        //    $("#jqGrid").setGridParam("").hideCol("Profession").trigger('reloadGrid');
            //清空数据
            $("#ctl00_ContentPlaceHolder1_ddProfession").val("-1");
        } else {
            $("#Profession").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(9).show();
          //  $("#jqGrid").setGridParam("").showCol("Profession").trigger('reloadGrid');
        }

        isShowButton();
    });

    //录入时间
    $("#ctl00_ContentPlaceHolder1_InsertTime").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#InsertTime").removeAttr("checked");
            $("#searchDetail").children().find("tr").eq(10).hide();

            //清空数据
            $("#ctl00_ContentPlaceHolder1_txt_start").val("");
            $("#ctl00_ContentPlaceHolder1_txt_end").val("");
        } else {
            $("#InsertTime").attr("checked", "checked");
            $("#searchDetail").show();
            $("#searchDetail").children().find("tr").eq(10).show();

        }

        isShowButton();
    });

    //声明查询方式
    var andor = "and";

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var Cst_No = $("#ctl00_ContentPlaceHolder1_txtCst_No").val();
        var Cst_Brief = $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val();
        var Cst_Name = $("#ctl00_ContentPlaceHolder1_txtCst_Name").val();
        var dType = $("#ctl00_ContentPlaceHolder1_ddType").val();
        var dProfession = $("#ctl00_ContentPlaceHolder1_ddProfession").val();
        var City = $("#ctl00_ContentPlaceHolder1_txtCity").val();
        var Linkman = $("#ctl00_ContentPlaceHolder1_txtLinkman").val();
        var Province = $("#ctl00_ContentPlaceHolder1_txtProvince").val();
        var Cpy_Address = $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val();
        var Cpy_Phone = $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();       
        $("#divCusList").show();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Customer/CustomerInfoHandler.ashx",
            postData: { 'strwhere': strwhere, 'andor': andor, 'txtCst_No': Cst_No, 'txtCst_Brief': Cst_Brief, 'txtCst_Name': Cst_Name, 'ddType': dType, 'ddProfession': dProfession, 'txtCity': City, 'txtLinkman': Linkman, 'txtProvince': Province, 'txtCpy_Address': Cpy_Address, 'Cpy_Phone': Cpy_Phone, "startTime": startTime, "endTime": endTime },
            page: 1

        }).trigger("reloadGrid");
    });

    //点击全部
    $(":checkbox:first", $("#columnsid")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_seachcustomer", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {
        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_seachcustomer", columnslist, { expires: 365, path: "/" });

    });

    //点击是否
    $("label[rel]").click(function () {      
        $(this).addClass("yellow active");
        $(this).removeClass("default");
        $(this).siblings("label").removeClass("active yellow");
        $(this).siblings("label").addClass("default");
        andor = $(this).attr("rel");
        $("#btn_search").click();
    });

    //初始化显示cookies字段列
    InitColumnsCookies();
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + rowData["Cst_Id"];
    return '<a href="' + pageurl + '" alt="查看客户">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看客户">查看</a>';
}


//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
   
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        // $("#nodata").show();
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        } else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}


function isShowButton() {
    //有条件显示查询按钮

    if ($("input[class=cls_audit]:checkbox:checked", $("#tb_check")).length > 0) {
        $("#tbSearch").show();
    }
    else {
        $("#tbSearch").hide();
    }
}
//初始化显示cookies字段列
var InitColumnsCookies = function () {
    //基本合同列表
    var columnslist = $.cookie("columnslist_seachcustomer");
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {       
        var list = columnslist.split(",");
       // $.each(list, function (i, c) {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (j, n) {
                //包含cookie中显示字段并选中
                if (list.indexOf($.trim($(n).val())) > -1) {
                    var span = $(n).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(n).attr("checked", true);
                    $("#jqGrid").showCol($(n).val());
                }
                else {
                    $("#jqGrid").hideCol($(n).val());
                }
            });

       // });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }

    }
    else {
        //cookie未保存，显示默认的几个字段
        var list = new Array("Cst_No", "Cst_Name", "Cst_Brief", "Cpy_Address", "Linkman", "Cpy_Phone", "Cpy_Fax", "Code", "InsertUser", "lrsj");//直接定义并初始化
        $.each(list, function (i, c) {
            $(":checkbox:not(:first)[value='" + c + "']", $("#columnsid")).each(function (j, n) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                // $("#jqGrid").showCol($(n).val());
            });

        });

    }
}