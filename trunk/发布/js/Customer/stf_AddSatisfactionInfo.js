﻿$(document).ready(function() {
    $("#btn_Save").click(function() {
        var txtSat_No = $("#txtSat_No").val();
        var txtSch_Time = $("#txtSch_Time").val();
        var txtSch_Context = $("#txtSch_Context").val();
        var msg = "";
        if (txtSat_No == "") {
            msg += "序号为必填项!</br>";
        } else {
            if (DataLength(txtSat_No) > 10) {

                msg += "序号长度不可超出10字符(5汉字)</br>";
            }
        }
        if (txtSch_Context == "") {
            msg += "调查内容为必填项！</br>";

        }
        if (txtSch_Time == "") {
            msg += "调查日期为必填项!</br>";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        } else {
            return true;
        }
    });
});
//判断长度
function DataLength(fData) {
    var intLength = 0;
    for (var i = 0; i < fData.length; i++) {
        if (fData.charCodeAt(i) < 0 || fData.charCodeAt(i) > 255) {
            intLength = intLength + 2;
        }
        else {
            intLength = intLength + 1;
        }
        return intLength;
    }
}