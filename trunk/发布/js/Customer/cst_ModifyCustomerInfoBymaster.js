﻿var cityId;
$(document).ready(function () {
    //加载联系人
    LoadLinkManData();
    //加载满意度
    LoadContractData();
    //加载附件
    LoadCoperationAttach();
    //new满意度
    var satisfactionInfo = new SatisfactionInfo(null, $("#MYresponsive"));
    //new 负责人
    var contact = new Contact(null, $("#JFresponsive"));

    //添加联系人信息   
    $("#addLinkManActionLink").click(function () {
        contact.Clear();
        contact.IsEdit = false;
        $("h4", "#JFresponsive").text("添加甲方负责人");
    });
    //保存联系人按钮
    $("#btnJFAdd").click(function () {
        contact.usetype = 0;
        contact.SaveContact("#addLinkManActionLink");
    })
    //编辑联系人
    $("span[class=EditContactA]").live("click", function () {

        var cid = $(this).attr("cid");
        contact.Clear();
        contact.IsEdit = true;
        contact.GetContactInfo(cid);
        contact.usetype = 0;
        contact.SysNo = cid;
        ClearTipSpan();
        $("h4", "#JFresponsive").text("编辑甲方负责人");
        //        $("#btnJFAdd").click(function () {
        //            contact.SaveContact("#addLinkManActionLink");
        //        })
    });

    //查看联系人
    $(".cls_chk").live('click', function () {
        var cstid = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/cp_ShowContactPersionInfo.ashx",
            dataType: "text",
            data: "id=" + cstid,
            success: function (msg) {
                var arrayValues = msg.split("|");
                $("#lbl_No").text(arrayValues[0]);
                $("#lbl_name").text(arrayValues[1]);
                $("#lbl_duties").text(arrayValues[2]);
                $("#lbl_department").text(arrayValues[3]);
                $("#lbl_phone").text(arrayValues[4]);
                $("#lbl_email").text(arrayValues[5]);
                $("#lbl_bphone").text(arrayValues[6]);
                $("#lbl_fphone").text(arrayValues[7]);
                $("#lbl_bfax").text(arrayValues[8]);
                $("#lbl_Ffax").text(arrayValues[9]);
                $("#lbl_bz").text(arrayValues[10]);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("错误");
            }
        });

    });
    //添加满意度
    $("#addSatisfactionActionLink").click(function () {
        satisfactionInfo.Clear();
        satisfactionInfo.IsEdit = false;
        $("h4", "#JFresponsive").text("添加用户满意度");
    });
    //保存满意度
    $("#btnSaveSatisfaction").click(function () {
        satisfactionInfo.UseType = 0;
        satisfactionInfo.SaveSatisfaction("#addSatisfactionActionLink");
    })
    //编辑满意度
    $("span[class=satisfactionEditA]").live("click", function () {

        var sid = $(this).attr("sid");
        satisfactionInfo.IsEdit = true;
        satisfactionInfo.Clear();
        satisfactionInfo.UseType = 0;
        satisfactionInfo.SysNo = sid;
        satisfactionInfo.GetSatisfactionInfo(sid);
        ClearTipSpan();
        $("h4", "#MYresponsive").text("编辑用户满意度");
        //        $("#btnSaveSatisfaction").click(function () {
        //            satisfactionInfo.SaveSatisfaction("#addSatisfactionActionLink");
        //        })
    });
    //满意度查看
    $(".sat_chk").live('click', function () {
        var sat_id = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/ShowSatisfactionInfo.ashx",
            data: "sat_id=" + sat_id,
            success: function (msg) {
                var sat_arry = msg.split("|");
                $("#lblSat_No").text(sat_arry[0]);
                $("#lblSch_Context").text(sat_arry[1]);
                $("#lblSat_Leve").text(sat_arry[5]);
                $("#lblSch_Time").text(sat_arry[6]);
                $("#lblSch_Way").text(sat_arry[7]);
                $("#lblSat_Best").text(sat_arry[3]);
                $("#lblSat_NotBest").text(sat_arry[4]);
                $("#lblRemark").text(sat_arry[2]);
            },
            eror: function (XMLHttpRequest, textStatus, errorThrowm) {
                alert("错误");
            }
        });
        //history.go(-1);
    });
    //城市
    var val = $("#ctl00_ContentPlaceHolder1_hiddenCountry").val();
    if (val != "") {
        //$("#DropDownListCountry").val(val);
        $.each($("#DropDownListCountry option"), function (index, option) {
            if ($.trim($(option).text()) == val) {
                $(option).attr("selected", "selected");
            }
        });
    }
    if ($("#ctl00_ContentPlaceHolder1_hiddenProvince").val() != "") {

        $("#DropDownListCity").children("option:gt(0)").remove();
        var countryID = $("#DropDownListCountry").children("option:selected").attr("countryid");

        if (val != "") {

            $.get("/HttpHandler/DropDownListCountryHandler.ashx", { "Action": "2", "countryID": countryID }, function (jsonResult) {
                if (jsonResult == "0") {
                    alert("城市情报取得失败！");
                } else {
                    var provincesArr = Global.evalJSON(jsonResult);
                    $("#DropDownListProvince").children("option:gt(0)").remove();
                    var optiosnString = "";
                    $.each(provincesArr, function (index, item) {
                        optiosnString += "<option value=\"" + item.ProvinceName + "\" provinceid=\"" + item.ProvinceID + "\">" + item.ProvinceName + "</option>";
                    });
                    $("#DropDownListProvince").append(optiosnString);
                    $("#DropDownListProvince option[value=" + $("#ctl00_ContentPlaceHolder1_hiddenProvince").val() + "]").attr("selected", "selected");
                    cityId = $("#DropDownListProvince option[value=" + $("#ctl00_ContentPlaceHolder1_hiddenProvince").val() + "]").attr("provinceid");
                    //获取城市
                    getcity();
                }
            });
        }
    }

    //获取城市
    function getcity() {

        if ($("#ctl00_ContentPlaceHolder1_hiddenCity").val() != "") {

            if ($("#ctl00_ContentPlaceHolder1_hiddenProvince").val() != "") {
                cityId = cityId == undefined ? 0 : cityId;
                if (cityId != 0) {
                    $.get("/HttpHandler/DropDownListCountryHandler.ashx", { "Action": "3", "provinceID": cityId }, function (jsonResult) {
                        if (jsonResult == "0") {
                            alert("城市情报取得失败！");
                        } else {
                            var cityArr = Global.evalJSON(jsonResult);
                            $("#DropDownListCity").children("option:gt(0)").remove();
                            var optiosnString = "";
                            $.each(cityArr, function (index, item) {
                                optiosnString += "<option value=\"" + item.CityName + "\" cityid=\"" + item.CityID + "\">" + item.CityName + "</option>";
                            });
                            $("#DropDownListCity").append(optiosnString);
                            $("#DropDownListCity option[value=" + $("#ctl00_ContentPlaceHolder1_hiddenCity").val() + "]").attr("selected", "selected");
                        }
                    });
                }
            }

        }
    }
    //保存修改
    //添加用户信息
    $("#ctl00_ContentPlaceHolder1_btn_Save").click(function () {
        //取得文化框
        var txtCst_No = $("#ctl00_ContentPlaceHolder1_txtCst_No").val();
        var txtCst_Brief = $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val();
        var txtCst_Name = $("#ctl00_ContentPlaceHolder1_txtCst_Name").val();
        var txtCpy_Address = $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val();
        var txtCode = $("#ctl00_ContentPlaceHolder1_txtCode").val();
        var txtLinkman = $("#ctl00_ContentPlaceHolder1_txtLinkman").val();
        var txtCpy_Phone = $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val();
        var txtCpy_Fax = $("#ctl00_ContentPlaceHolder1_txtCpy_Fax").val();
        var msg = "";
        //取得年份
        var year = new Date().getFullYear();
        //设置客户默认编码规则。部门+年份+编号
        var cstNoDefault = "编号规则：部门+年份+编号，例如：" + "IS" + year + "001";

        //序号
        if (txtCst_No == "" || txtCst_No == cstNoDefault) {
            msg += "请填写客户编号！<br/>";
        }
        //客户简称
        //if (txtCst_Brief == "") {
        //    msg += "请填写客户简称！<br/>";
        //}
        if (txtCst_Name == "") {
            msg += "请填写客户名称!！<br/>"
        }
        if (txtCpy_Address == "") {
            msg += "请填写客户联系地址！<br/>";
        }

        // 判断 甲方负责人必须填写
        var sf_rows = $("#cst_datas tr").length;

        if (sf_rows == 1) {
            msg += "请添加甲方负责人信息！</br>";
        }

        if (txtCode != "") {
            var reg = /^[0-9][0-9]{5}$/;
            if (!reg.test(txtCode)) {
                msg += "邮编格式不正确！<br/>";
            }
        }
        if (txtCpy_Fax != "") {
            var reg = /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/;
            if (!reg.test(txtCpy_Fax)) {
                msg += "公司传真格式不正确！</br>";
            }
        }
        if (txtCpy_Phone != "") {
            var reg = /(^((0[1,2]{1}\d{1}-?\d{8})|(0[3-9]{1}\d{2}-?\d{7,8}))$)|(^0?(13[0-9]|15[0-35-9]|18[01236789]|14[57])[0-9]{8}$)/;
            if (!reg.test(txtCpy_Phone)) {
                msg += "请输入固定电话或手机号！</br>";
            }
        }
        //扩展信息
        var txtCst_EnglishName = $("#ctl00_ContentPlaceHolder1_txtCst_EnglishName").val();
        var txtEmail = $("#ctl00_ContentPlaceHolder1_txtEmail").val();
        var txtCpy_PrincipalSheet = $("#ctl00_ContentPlaceHolder1_txtCpy_PrincipalSheet").val();
        var txtBankAccountNo = $("#ctl00_ContentPlaceHolder1_txtBankAccountNo").val();
        var txtBankName = $("#ctl00_ContentPlaceHolder1_txtBankName").val();

        if (txtBankAccountNo != "") {
            if (isNaN(txtBankAccountNo)) {
                msg += "银行帐号必须为数字！</br>";
            } else {
                if (txtBankName == "") {
                    msg += "需要填写银行名称！</br>";
                }
            }
        }
        if (txtEmail != "") {
            var reg = /^[a-zA-Z]([a-zA-Z0-9]*[-_.]?[a-zA-Z0-9]+)+@([\w-]+\.)+[a-zA-Z]{2,}$/;
            if (!reg.test(txtEmail)) {
                msg += "邮箱格式不正确！</br>";
            }
        }
        if (txtCpy_PrincipalSheet != "") {
            var strregex = "^((https|http|ftp|rtsp|mms)?://)"
                    + "?(([0-9a-za-z_!~*'().&=+$%-]+: )?[0-9a-za-z_!~*'().&=+$%-]+@)?"
                    + "(([0-9]{1,3}.){3}[0-9]{1,3}"
                    + "|"
                    + "([0-9a-za-z_!~*'()-]+.)*"
                    + "([0-9a-za-z][0-9a-za-z-]{0,61})?[0-9a-za-z]."
                    + "[a-za-z]{2,6})"
                    + "(:[0-9]{1,4})?"
                    + "((/?)|"
                    + "(/[0-9a-za-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
            var re = new RegExp(strregex);
            if (!re.test(txtCpy_PrincipalSheet)) {
                msg += "公司主页格式不正确！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        } else {
            //获取所有Hidden
            $(this).hide();
            return true;
        }
    });

    //取得年份
    var year = new Date().getFullYear();
    //设置客户默认编码规则。部门+年份+编号
    var cstNoDefault = "编号规则：部门+年份+编号，例如：" + "IS" + year + "001";
    //客户编号  
    $("#ctl00_ContentPlaceHolder1_txtCst_No").blur(function () { if ($.trim($(this).val()) == "") { $(this).val(cstNoDefault); $(this).css("color", "red"); } });
    $("#ctl00_ContentPlaceHolder1_txtCst_No").focus(function () { if ($.trim($(this).val()) == cstNoDefault) { $(this).val(""); } $(this).css("color", "black"); });
});
//判断汉字的实际长度
//function DataLength(fData) {
//    var intLength = 0;
//    for (var i = 0; i < fData.length; i++) {
//        if ((fData.charCodeAt(i) < 0) || (fData.charCodeAt(i) > 255))
//            intLength = intLength + 2;
//        else
//            intLength = intLength + 1;
//    }
//    return intLength;
//}
//附件删除事件
function AttachDetele(tr) {
    if (confirm("确认是否删除?")) {
        //获取当前系统自增号
        var sysNo = $(tr).parents("tr:first").children("td:first").children("input").val();
        $.post("/HttpHandler/ProcessUoloadFile.ashx", { "Action": "1", "SysNo": sysNo }, function (jsonResult) {
            if (jsonResult == "1") {
                //服务端成功删除的场合
                $(tr).parents("tr:first").remove();
            }
        });
    }
}
//创建提示信息Span 
function CreateTipSpan(element, tipContent) {
    //    element.parent("td:first").append("<span id=\"TipSpan\" style=\"color:red;\">" + tipContent + "</span>");
    alert(tipContent);
}
//清除提示信息Span 
function ClearTipSpan(container) {
    $("span[id=TipSpan]").remove();
}
//加载联系人信息
function LoadLinkManData() {
    var data = "action=getcstinfoAll&cstno=" + $("#ctl00_ContentPlaceHolder1_HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if (data != null) {
                    if ($("#cst_datas tr").length > 1) {
                        $("#cst_datas tr:gt(0)").remove();
                    }
                    $.each(data, function (i, n) {
                        var row = $("#cst_row").clone();
                        var oper = " <span rel='" + n.CP_Id + "' id='chk'style='cursor: pointer;color:blue;' class='cls_chk' href='#Show_cstInfo' data-toggle=\"modal\">查看</span>|";
                        oper += "<span cid='" + n.CP_Id + "' class='EditContactA' href=\"#JFresponsive\" data-toggle=\"modal\" style='cursor: pointer;color:blue;'>编辑|</span>";
                        oper += "<span rel='" + n.CP_Id + "' name='" + n.Name + "' class='cls_del' style='cursor: pointer;color:blue;'>删除</span>";
                        row.find("#id").text(n.ContactNo);
                        row.find("#name").text(n.Name);
                        row.find("#name").attr("align", "left");
                        row.find("#zhiwu").text(n.Duties);
                        row.find("#bumen").html(n.Department);
                        row.find("#phone").html(n.Phone);
                        row.find("#email").html(n.Email);
                        var reMark = n.Remark;
                        if (reMark.length > 20) {
                            reMark = reMark.substr(0, 20) + "...";
                        }
                        row.find("#remark").attr("title", n.Remark);
                        row.find("#remark").html(reMark); //新增,备注列
                        row.find("#oper").html(oper);
                        row.find("#oper span").click(function () {
                            //删除事件
                            delLinkManData($(this));
                        });
                        row.addClass("cls_TableRow");
                        row.appendTo("#cst_datas");
                    });
                    data = "";
                }

                if (result == "0") {
                    $("#linkman_nodata").show();
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//删除联系人信息
function delLinkManData(link) {
    if (link.attr("class") == "cls_del") {
        if (confirm("确定删除甲方负责人信息吗？")) {
            var data = "action=dellinkinfo&linkid=" + link.attr("rel") + "&name=" + link.attr("name");
            $.ajax({
                type: "GET",
                url: "../HttpHandler/CommHandler.ashx",
                dataType: "text",
                data: data,
                success: function (result) {
                    if (result == "yes") {
                        //删除本行数据
                        link.parents("tr:first").remove();
                        // alert("删除联系人成功！");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("系统错误！");
                }
            });
        }
        //history.go(-1);
    }
}
//加载满意度调查信息
function LoadContractData() {
    var data = "action=getstainfo&stano=" + $("#ctl00_ContentPlaceHolder1_HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if (data != null) {
                    if ($("#static_datas tr").length > 1) {
                        $("#static_datas tr:gt(0)").remove();
                    }
                    $.each(data, function (i, n) {
                        var row = $("#static_row").clone();
                        var oper = "<span rel='" + n.Sat_Id + "'style='cursor: pointer;color:blue;' id='sat_id' class='sat_chk' href='#Show_sfcInfo' data-toggle=\"modal\" >查看</span>|";
                        oper += "<span sid='" + n.Sat_Id + "' class='satisfactionEditA' href=\"#MYresponsive\"  data-toggle=\"modal\" style='cursor: pointer;color:blue;'>编辑</span>|";
                        oper += "<span rel='" + n.Sat_Id + "' name='" + n.Sch_Context + "' class='cls_del' style='cursor: pointer;color:blue;'>删除</span>";
                        row.find("#st_id").text(n.Sat_No);
                        row.find("#st_content").text(n.Sch_Context);
                        row.find("#st_myd").text(n.Sat_Leve);
                        row.find("#st_time").html(n.Sch_Time);
                        row.find("#st_type").html(n.Sch_Way);
                        var reMark = n.Remark;
                        if (reMark.length > 20) {
                            reMark = reMark.substr(0, 20) + "...";
                        }
                        row.find("#st_remark").attr("title", n.Remark);
                        row.find("#st_remark").html(reMark); //新增,加载满意度备注
                        row.find("#st_oper").html(oper);
                        row.find("#st_oper span").click(function () {
                            //删除事件
                            delContractData($(this));
                        });
                        row.addClass("cls_TableRow");
                        row.appendTo("#static_datas");
                    });
                }

                if (result == "0") {
                    $("#static_nodata").show();
                }
                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//删除满意度调查信息
function delContractData(link) {

    if (link.attr("class") == 'cls_del') {
        if (confirm("确定删除满意度调查信息？")) {
            var data = "action=delsta&staid=" + link.attr("rel") + "&name=" + link.attr("name");
            $.ajax({
                type: "GET",
                url: "../HttpHandler/CommHandler.ashx",
                dataType: "text",
                data: data,
                success: function (result) {
                    if (result == "yes") {
                        //删除本行数据
                        link.parents("tr:first").remove();
                        //  alert("删除联系人成功！");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("系统错误！");
                }
            });
        }
    }
}

//加载附件信息
function LoadCoperationAttach() {
    var data_att = "action=selectattachBycstId&cstid=" + $("#ctl00_ContentPlaceHolder1_HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result.ds;
                if (filedata != null) {
                    if ($("#gv_Attach tr").length > 1) {
                        $("#gv_Attach tr:gt(0)").remove();
                    }
                    $.each(filedata, function (i, n) {
                        var row = $("#att_row").clone();
                        var oper = "<span style='color:blue;cursor:pointer;' rel='" + n.ID + "'>删除</span>";
                        var oper2 = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + n.FileName + "&FileURL=" + n.FileUrl + "\" target='_blank'>下载</a>";
                        var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                        //row.find("#att_id").text(n.ID);
                        row.find("#att_filename").html(img + n.FileName);
                        row.find("#att_filename").attr("align", "left");
                        row.find("#att_uptime").text(n.UploadTime);
                        row.find("#att_filesize").text(n.FileSizeString);
                        row.find("#att_filetype").text(n.FileType);
                        row.find("#attr_updateuser").text(n.UploadUser);
                        row.find("#att_oper").html(oper);
                        row.find("#att_oper2").html(oper2);
                        row.find("#att_oper span").click(function () {
                            if (confirm("确定要删除本条附件吗？")) {
                                delCprAttach($(this));
                            }
                        });
                        row.addClass("cls_TableRow");
                        row.appendTo("#gv_Attach");
                    });
                    filedata = "";
                }


            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                link.parents("tr:first").remove();
                // alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}