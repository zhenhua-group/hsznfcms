﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //固定数据表的高度
    var h = 120;
    //附件
//    $("#gv_Attach").chromatable({
//        width: "100%",
//        height: h,
//        scrolling: "true"
//    });
    //联系人
    $("#gv_contactPerson").chromatable({
        width: "100%",
        height: h,
        scrolling: "true"
    });
    //满意度
    $("#gv_satisfaction").chromatable({
        width: "100%",
        height: h,
        scrolling: "true"
    });
    //合同
//    $("#gv_rContract").chromatable({
//        width: "100%",
//        height: h,
//        scrolling: "true"
//    });
    //表单行的颜色变化
    $(".cls_show_cst_jiben>tbody>tr:odd").attr("style", "background-color:#FFFFFF");

    //查看
    $(".cls_chk").click(function () {
        $('#Show_cstInfo').dialog('open');
        $('.ui-widget-overlay').css("display", "block");
        var cstid = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/cp_ShowContactPersionInfo.ashx",
            dataType: "text",
            data: "id=" + cstid,
            success: function (msg) {
                var arrayValues = msg.split("|");
                $("#No").text(arrayValues[0]);
                $("#name").text(arrayValues[1]);
                $("#zhiwu").text(arrayValues[2]);
                $("#department").text(arrayValues[3]);
                $("#phone").text(arrayValues[4]);
                $("#email").text(arrayValues[5]);
                $("#bphone").text(arrayValues[6]);
                $("#fphone").text(arrayValues[7]);
                $("#bfax").text(arrayValues[8]);
                $("#Ffax").text(arrayValues[9]);
                $("#bz").text(arrayValues[10]);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("错误");
            }
        });
    });
    // Dialog
    $("#Show_cstInfo").dialog({
        autoOpen: false,
        width: 640,
        height: 250
    });
    //关闭
    $("#Button1").click(function () {
        $('#Show_cstInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });

    $("#ui-dialog-title-Show_cstInfo~a").click(function () {
        $('#Show_cstInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });
    //满意度
    $(".sat_chk").click(function () {
        $('#Show_sfcInfo').dialog('open');
        $('.ui-widget-overlay').css("display", "block");
        var sat_id = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/ShowSatisfactionInfo.ashx",
            data: "sat_id=" + sat_id,
            success: function (msg) {
                var sat_arry = msg.split("|");
                $("#lblSat_No").text(sat_arry[0]);
                $("#lblSch_Context").text(sat_arry[1]);
                $("#lblSat_Leve").text(sat_arry[5]);
                $("#lblSch_Time").text(sat_arry[6]);
                $("#lblSch_Way").text(sat_arry[7]);
                $("#lblSat_Best").text(sat_arry[3]);
                $("#lblSat_NotBest").text(sat_arry[4]);
                $("#lblRemark").text(sat_arry[2]);
            },
            eror: function (XMLHttpRequest, textStatus, errorThrowm) {
                alert("错误");
            }
        });
    });

    // Dialog
    $('#Show_sfcInfo').dialog({
        autoOpen: false,
        width: 450,
        height: 280
    });

    //关闭
    $("#Button2").click(function () {
        $('#Show_sfcInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });

    $("#ui-dialog-title-Show_sfcInfo~a").click(function () {
        $('#Show_sfcInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });
    //导出
    var cstid = $("#hid_cstid").val();
    $("#ag").attr("href", "../HttpHandler/Custemer.ashx?flag=cstshow&custid=" + cstid + "");

});