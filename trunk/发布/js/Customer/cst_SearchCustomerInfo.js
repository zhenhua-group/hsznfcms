﻿$(document).ready(function() {
    //行背景
    $("#gridView tr").hover(function() {
        $(this).addClass("tr_in");
    }, function() {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gridView tr:even").css({ background: "White" });
    //返回
    $("#btn_back").click(function() {
        window.history.forward();
    });

    CommonControl.SetFormWidth();
    $(".cls_show_cst_jiben tr:odd").attr("style", "background-color:#FFF");
    $("#gridView tr:even").css({ background: "White" });
    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });
    //提示
    $(".cls_column").tipsy({ opacity: 1 })

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Customer";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = "0";
    paramEntity.currYear = "0";
    var autoComplete = new AutoComplete(paramEntity, $("#txtCst_Name"));
    autoComplete.GetAutoAJAX();
});