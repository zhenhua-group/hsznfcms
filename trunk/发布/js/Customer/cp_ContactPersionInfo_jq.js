﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Customer/ContactPersionInfoHandler.ashx?strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '姓名', '职务', '部门', '电话', 'Email', '客户编号', '客户名称', '客户简称', '公司地址', '邮政编码', '联系人', '公司电话', '传真号', '客户英文名', '是否合作', '所在国家', '所在省份', '所在城市', '客户类型', '所属行业', '分支机构', '客户邮箱', '公司主页', '关系建立时间', '关系部门', '信用级别', '亲密度', '开户银行名称', '企业代码', '开户银行账号', '法定代表', '纳税人识别号', '备注', '录入时间', '客户', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 50, align: 'center' },
                             { name: 'CP_Id', index: 'CP_Id', hidden: true, editable: true },
                             { name: 'Name', index: 'Name', width: 273, align: 'center' },
                             { name: 'Duties', index: 'Duties', width: 100, align: 'center' },
                             { name: 'Department', index: 'Department', width: 100, align: 'center' },
                             { name: 'Phone', index: 'Phone', width: 100, align: 'center' },
                             { name: 'CP.Email', index: 'CP.Email', width: 200, align: 'center' },
                             {
                                  name: 'Cst_No', index: 'Cst_No', width: 100, align: 'center', hidden: true, formatter: function (celvalue, options, rowData) {
                                      return $.trim(celvalue);
                                  }
                             },
                             { name: 'Cst_Name', index: 'Cst_Name', width: 200, hidden: true },
                             { name: 'Cst_Brief', index: 'Cst_Brief', width: 80, align: 'center', hidden: true },
                             { name: 'Cpy_Address', index: 'Cpy_Address', width: 150, hidden: true },
                             { name: 'Code', index: 'Code', width: 80, align: 'center', hidden: true },
                             { name: 'Linkman', index: 'Linkman', width: 60, align: 'center', hidden: true },
                             { name: 'Cpy_Phone', index: 'Cpy_Phone', width: 100, align: 'center', hidden: true },
                             { name: 'Cpy_Fax', index: 'Cpy_Fax', width: 80, align: 'center', hidden: true },
                             { name: 'Cst_EnglishName', index: 'Cst_EnglishName', width: 80, align: 'center', hidden: true },
                             { name: 'PartnerName', index: 'IsPartner', width: 80, align: 'center', hidden: true },
                             { name: 'Country', index: 'Country', width: 80, align: 'center', hidden: true },
                             { name: 'Province', index: 'Province', width: 80, align: 'center', hidden: true },
                             { name: 'City', index: 'City', width: 80, align: 'center', hidden: true },
                             { name: 'TypeName', index: 'Type', width: 80, align: 'center', hidden: true },
                             { name: 'ProfessionName', index: 'Profession', width: 80, align: 'center', hidden: true },
                              { name: 'BranchPart', index: 'BranchPart', width: 80, align: 'center', hidden: true },
                             { name: 'CX.Email', index: 'CX.Email', width: 80, align: 'center', hidden: true },
                             { name: 'Cpy_PrincipalSheet', index: 'Cpy_PrincipalSheet', width: 80, align: 'center', hidden: true },
                              { name: 'RelationTime', index: 'CreateRelationTime', width: 100, align: 'center', hidden: true },
                             { name: 'RelationDepartment', index: 'RelationDepartment', width: 80, align: 'center', hidden: true },
                             { name: 'CreditLeveName', index: 'CreditLeve', width: 80, align: 'center', hidden: true },
                             { name: 'CloseLeveName', index: 'CloseLeve', width: 80, align: 'center', hidden: true },
                             { name: 'BankName', index: 'BankName', width: 100, align: 'center', hidden: true },
                             { name: 'Cpy_Code', index: 'Cpy_Code', width: 80, align: 'center', hidden: true },
                             { name: 'BankAccountNo', index: 'BankAccountNo', width: 100, align: 'center', hidden: true },
                             { name: 'LawPerson', index: 'LawPerson', width: 80, align: 'center', hidden: true },
                             { name: 'TaxAccountNo', index: 'TaxAccountNo', width: 100, align: 'center', hidden: true },
                             { name: 'Remark', index: 'Remark', width: 80, align: 'center', hidden: true },
                             { name: 'lrsj', index: 'CP.LastUpdate', width: 80, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'CP_Id', index: 'CP_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'c.cst_id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Customer/ContactPersionInfoHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'Cst_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_customerNum").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Customer/ContactPersionInfoHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'keyname': keyname, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'c.cst_id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //查看联系人
    $(".cls_chk").live('click', function () {
        var cstid = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/cp_ShowContactPersionInfo.ashx",
            dataType: "text",
            data: "id=" + cstid,
            success: function (msg) {
                var arrayValues = msg.split("|");
                $("#No").text(arrayValues[0]);
                $("#SpanName").text(arrayValues[1]);
                $("#Span2").text(arrayValues[2]);
                $("#department").text(arrayValues[3]);
                $("#Span3").text(arrayValues[4]);
                $("#Span4").text(arrayValues[5]);
                $("#bphone").text(arrayValues[6]);
                $("#fphone").text(arrayValues[7]);
                $("#bfax").text(arrayValues[8]);
                $("#Ffax").text(arrayValues[9]);
                $("#bz").text(arrayValues[10]);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("错误");
            }
        });
        //history.go(-1);
    });

    //点击全部
    $(":checkbox:first", $("#columnsid")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_persion", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {
        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_persion", columnslist, { expires: 365, path: "/" });
    });

    //初始化显示cookies字段列
    InitColumnsCookies();

});


//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看客户">查看</a>';

}
//查看联系人
function colEditFormatter(celvalue, options, rowdata) {
    return '<a alt="查看联系人" rel="' + celvalue + '" class="cls_chk" href="#Show_cstInfo" data-toggle="modal">查看</a>';
}
//统计 
function completeMethod() {
    //更改序号
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    //隐藏多选框
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {

        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        } else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
//初始化显示cookies字段列
var InitColumnsCookies = function () {
    //基本合同列表
    var columnslist = $.cookie("columnslist_persion");
    //cookie已有保存 
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {
       
        var list = columnslist.split(",");
      //  $.each(list, function (i, c) {
        $(":checkbox:not(:first)", $("#columnsid")).each(function (j, n) {
               //包含cookie中显示字段并选中
                if (list.indexOf($.trim($(n).val())) > -1) {
                    var span = $(n).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(n).attr("checked", true);
                    $("#jqGrid").showCol($(n).val());
                }
                else {
                    $("#jqGrid").hideCol($(n).val());
                }
            });

      //  });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }

    }
    else {
      
        //cookie未保存，显示默认的几个字段
        var list = new Array("Name", "Duties", "Department", "Phone", "CP.Email", "lrsj");//直接定义并初始化
        $.each(list, function (i, c) {
            $(":checkbox:not(:first)[value='" + c + "']", $("#columnsid")).each(function (j, n) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                // $("#jqGrid").showCol($(n).val());
            });

        });

    }
}