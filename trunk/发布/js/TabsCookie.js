﻿$(document).ready(function () {
    var localurl = window.location.pathname;
    //tabs选中
    $("a[data-toggle='tab']").click(function () {

        // 删除 cookie
        if ($.cookie('tabs') != null && $.cookie('tabs') != undefined) {
            $.cookie('tabs', '', { expires: -1 });
        }
        if ($.cookie('fileurl') != null && $.cookie('fileurl') != undefined) {
            $.cookie('fileurl', '', { expires: -1 });
        }
        //cookie
        $.cookie('tabs', $.trim($(this).attr("href")), { path: '/' });//顶级目录下的
        $.cookie('fileurl', $.trim(localurl), { path: '/' });  //用于区分是否当前tab 
    }); 

    if ($.cookie('fileurl') != null && $.cookie('fileurl') != undefined && $.cookie('fileurl') != "") {
        if ($.trim($.cookie('fileurl')) == $.trim(localurl)) {
            if ($.cookie('tabs') != null && $.cookie('tabs') != undefined && $.cookie('tabs') != "") {
                //标题
                var $li = $("a[href='" + $.trim($.cookie('tabs')) + "']").parent();
                $li.siblings().removeClass();
                $li.addClass("active");
                //内容
                $($.trim($.cookie('tabs'))).siblings().removeClass("active in");
                $($.trim($.cookie('tabs'))).addClass("active in");

            }

        }
        else {
           // $.cookie('tabs', '', { path: '/' });
            // $.cookie('fileurl', '', { path: '/' });
            //标题
            var $li = $("a[href='#tab_1_1']").parent();
            $li.siblings().removeClass();
            $li.addClass("active");
            //内容
            $("#tab_1_1").siblings().removeClass("active");
            $("#tab_1_1").addClass("active");
        }

    }
    //点击左侧清空cookie
    $(".page-sidebar-menu a[href*='.aspx']").click(function (i) {
         $.cookie('tabs', '', { path: '/' });
         $.cookie('fileurl', '', { path: '/' });
    });
});