﻿//发送消息
function MessageCommon(md) {
    //消息框 
    this.messageDialog = md;
    var _instance = this;
    //显示角色用户列表
    this.render = function (userInfoArray, roleName) {

        var _userInfoArray = userInfoArray;
        var _roleName = roleName;
        var $div = $("<div style='overflow-y:auto;width: 400px; height: 250px;'></div>")
        var _$table = $("<table style=\"width:100%;font-size:9pt\"></table>");
        var trHead = "<tr style=\"font-size:10pt;font-weight:bold;background-color:#f0f0f0;line-height:30px;\"><td colspan=\"6\">请选择下一阶段<span style=\"color:Red;\">" + _roleName + "</span>审批人</td></tr>";
        var trString = "<tr>";
        //选择接收用户table
        for (var i = 0; i < _userInfoArray.length; i++) {
            if (i % 4 == 0 && i != 0) {
                trString += "</tr><tr>";
            }
            trString += "<td style='width:120px;'><input type=\"checkbox\" name=\"messageUser\" value=\"" + _userInfoArray[i].UserSysNo + "\" username=\"" + _userInfoArray[i].UserName + "\">&nbsp;" + _userInfoArray[i].UserName + "</td>";
        }
        trString += "</tr>"
        _$table.append(trHead + trString);
        //        $div.append(_$table)
        //        _instance.messageDialog.show($div);
        $("#auditShow").empty()
        $("#auditShow").append(_$table);
    }
    //设置消息实体
    this.setMsgTemplate = function (mt) {
        _instance.messageTemplate = mt;
    }
    //mt 消息实体，callBack 消息发送成功回调函数
    this.chooseUserForMessage = function (callBack) {
        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        }

        var userArray = new Array();
        $.each(_$mesUser, function (i, u) {
            userArray.push({
                "UserSysNo": u.value,
                "UserName": $(u).attr("username")
            });
        });

        var _jsonDataObj = {
            "MessageTemplate":
                {
                    "FromUser": _instance.messageTemplate.FromUser,
                    "InUser": _instance.messageTemplate.InUser,
                    "MessageContent": _instance.messageTemplate.MessageContent,
                    "MsgType": _instance.messageTemplate.MsgType,
                    "QueryCondition": _instance.messageTemplate.QueryCondition,
                    "ReferenceSysNo": _instance.messageTemplate.ReferenceSysNo,
                    "ToRole": _instance.messageTemplate.ToRole,
                    "IsDone": _instance.messageTemplate.IsDone
                },
            "UserList": userArray
        };

        //请求发送消息
        $.post("/HttpHandler/MessageCommonHandler.ashx", { "jsonData": Global.toJSON(_jsonDataObj) }, callBack);
    }
}
