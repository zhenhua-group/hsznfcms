﻿//检查操作权限共通类
var CheckActionPower =
{
    //检查操作权限方法,type为后台映射的枚举类型
    "CheckPower": function (type) {
        var pointerIndex = window.location.pathname.lastIndexOf("/");
        var pageName = window.location.pathname.substring(pointerIndex + 1, window.location.pathname.length);
        $.post("/HttpHandler/ActionPowerHandler.ashx", { "pageName": pageName, "type": type }, function (jsonResult) {
            if (jsonResult) {
                //操作权限集合
                var powerEntity = eval("(" + jsonResult + ")");
                if (powerEntity.AllowEdit == 0) {
                    //不允许编辑的场合，首先removeClick事件，然后bing新点击事件
                    $(".allowEdit").unbind("click").bind("click", function (e) {
                        alert("不允许编辑");
                        if (e && e.stopPropagation) {//非IE  
                            e.stopPropagation();
                        }
                        else {//IE  
                            window.event.cancelBubble = true;
                        }
                        return false;
                    });
                }
                if (powerEntity.AllowDelete == 0) {
                    //不允许删除的场合
                    $(".allowDelete").unbind("click").bind("click", function (e) {
                        alert("不允许删除");
                        if (e && e.stopPropagation) {//非IE  
                            e.stopPropagation();
                        }
                        else {//IE  
                            window.event.cancelBubble = true;
                        }
                        return false;
                    });
                }
            }
        });
    }
};