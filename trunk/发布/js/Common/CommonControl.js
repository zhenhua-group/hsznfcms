﻿//共同操作接口
function CommonControl() { }

//得到系统消息数据
CommonControl.GetSysMsgCount = function (reloadURL) {
    $.post("/HttpHandler/AuditSysMsgHandler.ashx", {}, function (jsonResult) {
        var count = parseInt(jsonResult, 10);
        window.location.href = reloadURL;
    });
}

//表格样式自定义
CommonControl.SetTableStyle = function (tableName, needChromatable) {
    if (typeof tableName != "string" || tableName.length <= 0) {
        alert("请传入正确的输入参数。TableName");
        return;
    }
    //Table取得
    var table = $("*[id=" + tableName + "]", document);

    if (table.length <= 0 || table[0].nodeName != "TABLE") {
        return;
    }
    //参数正确的场合
    if (needChromatable == undefined || needChromatable == null) {
        //设置表格置底
        table.chromatable({
            width: "100%",
            height: document.body.clientHeight - 130,
            scrolling: "true"
        });
    }
    //设置隔行变色
    table.find("tr:even").css({ "background-color": "#f0f0f0" });
    table.find("tr:odd").css({ "background-color": "White" });
}

//给文本框添加边框样式
CommonControl.SetTextBoxStyle = function () {
    $(":text").addClass("TextBoxBorder");
}

//添加一个透明的遮蔽层
CommonControl.CreateTransparentDiv = function () {
    var options = {
        "width": "100%",
        "height": "1000px",
        "position": "absolute",
        "zIndex": 10001,
        "left": "0px",
        "top": "0px",
        "background": "#C3C8CC"
    };

    if ($.browser.msie) {
        options.filter = "progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=10,finishOpacity=30";
    } else {
        options.backgroundColor = "rgba(0, 0, 0, 0.2)";
    }
    var div = $("<div id=\"transparentDiv\"></div>");
    div.css(options);
    $("body").append(div);
}

//隐藏菜单栏
CommonControl.HideLeftMenu = function () {
    //取得所有需要隐藏的table 
    var needHideMenu = $("table[id=hideMenu]");
    $.each(needHideMenu, function (i, menu) {
        var trArray = $(menu).children("tbody").children("tr");
        $(trArray[0]).bind("click", function () {
            var hasHide = $(trArray[1]).css("display");
            if (hasHide == "table-row" || hasHide == "block") {
                $(trArray[1]).hide();
            } else {
                $(trArray[1]).show();
            }
        }).css("cursor", "pointer");
    });
}
//update by  long  20130607
//绑定ASTreeView选项
CommonControl.BindASTreeView = function (asTreeViewID, strValue, container) {
    if (container == undefined || container == null || container == "" || typeof container != "object") {
        container = $("body");
    }
    //找到树
    var tree = $("#" + asTreeViewID, container);
    //清空所有树
    $.each($("li[treenodevalue]", tree), function (index, item) {
        $(item).attr("checkedstate", "2").children("img[checkedstate]").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif");
    });
    //分隔一级节点
    if (strValue.indexOf('+') > -1) {
        var strParentArray = strValue.split("+");
        $.each(strParentArray, function (index, parentValue) {
            //数组项不能为空
            if (parentValue.length > 0) {
                var firstArray = parentValue.split('^');
                var firstNodeValue = firstArray[0];
                var firstNodeLi = $("li[treenodevalue='" + firstNodeValue + "']", tree);
                firstNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
                //二级
                $.each(firstArray, function (index, firststr) {
                    if (index > 0) {
                        var scdString = firststr;
                        if (scdString.indexOf('*') > -1) {
                            var scdArray = scdString.split('*');
                            var scdNodeValue = scdArray[0];
                            var scdNodeLi = $("li[treenodevalue='" + scdNodeValue + "']", tree);
                            scdNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
                            //所有子项节点
                            $.each(scdArray, function (index, substr) {
                                if (index > 0) {
                                    var subNodeValue = substr;
                                    var subNodeLi = $("li[treenodevalue='" + subNodeValue + "']", tree);
                                    subNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
                                }
                            });
                        }
                        else //只有两级
                        {
                            var scdNodeValue = scdString;
                            var scdNodeLi = $("li[treenodevalue='" + scdNodeValue + "']", tree);
                            scdNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
                        }
                    }
                });
            }
        });
    }
    else {
        var firstArray = strValue.split('^');
        var firstNodeValue = firstArray[0];
        var firstNodeLi = $("li[treenodevalue='" + firstNodeValue + "']", tree);
        firstNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
        //二级
        $.each(firstArray, function (index, firststr) {
            if (index > 0) {
                var scdString = firststr;
                if (scdString.indexOf('*') > -1) {
                    var scdArray = scdString.split('*');
                    var scdNodeValue = scdArray[0];
                    var scdNodeLi = $("li[treenodevalue='" + scdNodeValue + "']", tree);
                    scdNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
                    //所有子项节点
                    $.each(scdArray, function (index, substr) {
                        if (index > 0) {
                            var subNodeValue = substr;
                            var subNodeLi = $("li[treenodevalue='" + subNodeValue + "']", tree);
                            subNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
                        }
                    });
                }
                else {
                    var scdNodeValue = scdString;
                    var scdNodeLi = $("li[treenodevalue='" + scdNodeValue + "']", tree);
                    scdNodeLi.attr("checkedstate", "0").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-checked.gif");
                }
            }
        });
    }
    //判断节点的状态是半选中，还是全选
    var checkedNode = $("li[treenodevalue][checkedstate=0]");
    $.each(checkedNode, function (index, item) {
        var childrenUL = $("ul:first", item);
        if (childrenUL.length > 0) {
            var flag = true;
            $.each($("li[treenodevalue]", childrenUL), function (j, li) {
                if ($(li).attr("checkedstate") == "2") {
                    flag = false;
                    return false;
                }
            });
            if (flag == false) {
                $(item).attr("checkedstate", "1").children("img[checkedstate]:first").attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-half-checked.gif");
            }
        }
    });
    //列表框显示文字
    var finalText = strValue;
    while (true) {
        finalText = finalText.replace("+", ",").replace("^", ",");
        if (finalText.indexOf("+") == -1 && finalText.indexOf("^") == -1) {
            break;
        }
    }
    $(".defaultDropdownTextContainer", container).text(finalText);
}
//计算页数
CommonControl.AccountPageCount = function (totalCount, pageSize) {
    var pageCount = 0;
    if (totalCount <= pageSize) {
        pageCount = 1;
    } else {
        if (totalCount % pageSize == 0) {
            pageCount = totalCount / pageSize;
        } else {
            pageCount = Math.floor(totalCount / pageSize) + 1;
        }
    }
    return pageCount;
}
//设置页面宽度
CommonControl.SetFormWidth = function () {
    var formwidth = $("#form1").width();
    if (formwidth < 1024) {
        $("#form1").addClass("cls_form");
    }
}
//改变审批按钮状态
CommonControl.SetApplyStatus = function (obj) {
    if (obj != null || obj != undefined) {
        obj.parent().html("<span style=\"color:black;font-size:9pt\" >审批中</span>");
    }
}
