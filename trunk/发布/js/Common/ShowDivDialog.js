﻿//权限
this.UserRolePower = {
    "previewPower": "0", //查看权限,0个人,1全部,2部门
    "userSysNum": "", //个人id
    "userUnitNum": "", //部门id
    "notShowUnitList": ""//那些不需要显示的部门id
};
//所有所需参数字典
this.ParametersDict = {
    "divDialogId": "", //弹出的层的id
    "showDataTableId": "", //用于显示数据的表的id
    "pageSize": "10", //每页容量,默认10
    "sqlStrWhere": "", //where语句
    //分页的层的按钮的id
    "prevPage": "", //上一页
    "firstPage": "", //首页
    "nextPage": "", //下一页
    "lastPage": "", //尾页
    "gotoPage": "", //跳转页
    //分页的层的显示数据的id
    "allDataCount": "", //数据总数 label控件
    "nowIndex": "", //当前页 label控件
    "allPageCount": "", //总页数 label控件
    "gotoIndex": ""//跳转页数 input控件
};
//每次使用前赋值,否则如果有多个使用者,可能会导致"串参",每次赋值后没值的都会为null或默认值,其他使用者不会用到
//为所需参数赋值-可只为所需参数赋值
function SetParameters(paramsObj) {
    ParametersDict.divDialogId = paramsObj.divDialogId == null ? null : paramsObj.divDialogId,
    ParametersDict.showDataTableId = paramsObj.showDataTableId == null ? null : paramsObj.showDataTableId,
    ParametersDict.pageSize = paramsObj.pageSize == null ? "10" : paramsObj.pageSize,
    ParametersDict.prevPage = paramsObj.prevPage == null ? null : paramsObj.prevPage,
    ParametersDict.firstPage = paramsObj.firstPage == null ? null : paramsObj.firstPage,
    ParametersDict.nextPage = paramsObj.nextPage == null ? null : paramsObj.nextPage,
    ParametersDict.lastPage = paramsObj.lastPage == null ? null : paramsObj.lastPage,
    ParametersDict.gotoPage = paramsObj.gotoPage == null ? null : paramsObj.gotoPage,
    ParametersDict.allDataCount = paramsObj.allDataCount == null ? null : paramsObj.allDataCount,
    ParametersDict.nowIndex = paramsObj.nowIndex == null ? null : paramsObj.nowIndex,
    ParametersDict.allPageCount = paramsObj.allPageCount == null ? null : paramsObj.allPageCount,
    ParametersDict.gotoIndex = paramsObj.gotoIndex == null ? null : paramsObj.gotoIndex
}
var showDivDialogClass = this;
//返回数据总数
this.AllDataCount;
//所需参数:传入参数:action,sql where语句,是否需要权限(true需要false不需要),标识符,返回成功回调函数
//需赋值参数:权限(4个)
function GetDataTotalCount(action, strWhere, isPower, mark, callBack) {
    //var resultCount;
    var data = "action=" + action + "&strWhere=" + strWhere + "&mark=" + mark + "&isPower=" + isPower;
    if (isPower) {
        data += "&previewPower=" + UserRolePower.previewPower + "&userSysNum=" +
        UserRolePower.userSysNum + "&userUnitNum=" + UserRolePower.userUnitNum + "&notShowUnitList=" + UserRolePower.notShowUnitList;
    }
    data = encodeURI(data); //对参数编码
    $.ajax({
        type: "Get",
        dataType: "text",
        url: "/HttpHandler/CommHandler.ashx",
        data: data,
        success: callBack,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
}
//获取数据-可按页数获取
//所需参数:传入参数:action,sql where语句,是否需要权限判断(true需要false不需要),哪一页数据,标识符,成功返回的回调函数
//需赋值参数:pagesize,权限(4个)
function GetDataByAJAX(action, strWhere, isPower, pageIndex, mark, callBack) {
    //var getPageIndexData = Math.abs($("#" + ParametersDict.nowIndex + "").text());
    var getPageIndexData = pageIndex;
    var data = "action=" + action + "&pageSize=" + ParametersDict.pageSize + "&pageIndex=" +
        getPageIndexData + "&strWhere=" + strWhere + "&mark=" + mark + "&isPower=" + isPower;
    if (isPower == "true") {
        data += "&previewPower=" + UserRolePower.previewPower + "&userSysNum=" +
        UserRolePower.userSysNum + "&userUnitNum=" + UserRolePower.userUnitNum + "&notShowUnitList=" + UserRolePower.notShowUnitList;
    }
    data = encodeURI(data);
   
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "/HttpHandler/CommHandler.ashx",
        data: data,
        success: callBack,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
}
//第一次加载数据后绑定分页层的值
//所需参数:传入参数:数据总数,需赋值参数:当前页id,跳转页id,总页数id,总数据id
function BindPageValueFirst(allDataCount) {
    $("#" + ParametersDict.nowIndex + "").text(1);
    $("#" + ParametersDict.gotoIndex + "").val(1);
    $("#" + ParametersDict.allDataCount + "").text(allDataCount);
    $("#" + ParametersDict.allPageCount + "").text(Math.ceil(allDataCount / ParametersDict.pageSize));
}
//判断分页逻辑
//所需参数:传入参数:点击的分页按钮id,
//需赋值参数:当前页数id,总页数id,跳转页id,分页按钮id(5个)
function IsRegex_ByPage(clickID) {

    var nowPageIndex = Math.abs($("#" + ParametersDict.nowIndex + "").text());
    var totalPageCount = Math.abs($("#" + ParametersDict.allPageCount + "").text());
    var gotoPageIndex = Math.ceil($("#" + ParametersDict.gotoIndex + "").val());
    var isRegex = false;
    if (clickID == ParametersDict.firstPage) {
        if (nowPageIndex != 1 && nowPageIndex != 0 && totalPageCount != 0) {
            $("#" + ParametersDict.nowIndex + "").text(1);
            $("#" + ParametersDict.gotoIndex + "").val(1);
            isRegex = true;
        }
    } else if (clickID == ParametersDict.prevPage) {
        if (nowPageIndex > 1 && nowPageIndex <= totalPageCount) {
            $("#" + ParametersDict.nowIndex + "").text(nowPageIndex - 1);
            $("#" + ParametersDict.gotoIndex + "").val(nowPageIndex - 1);
            isRegex = true;
        }
    } else if (clickID == ParametersDict.nextPage) {
        if (nowPageIndex < totalPageCount) {
            $("#" + ParametersDict.nowIndex + "").text(nowPageIndex + 1);
            $("#" + ParametersDict.gotoIndex + "").val(nowPageIndex + 1);
            isRegex = true;
        }
    } else if (clickID == ParametersDict.lastPage) {
        if (nowPageIndex != totalPageCount && nowPageIndex != 0 && totalPageCount != 0) {
            $("#" + ParametersDict.nowIndex + "").text(totalPageCount);
            $("#" + ParametersDict.gotoIndex + "").val(totalPageCount);
            isRegex = true;
        }
    } else if (clickID == ParametersDict.gotoPage) {
        if (gotoPageIndex && gotoPageIndex <= totalPageCount && gotoPageIndex >= 1 && gotoPageIndex != nowPageIndex) {
            $("#" + ParametersDict.nowIndex + "").text(gotoPageIndex);
            $("#" + ParametersDict.gotoIndex + "").val(gotoPageIndex);
            isRegex = true;
        }
    }
    return isRegex;
}
//过滤特殊字符
function ReplaceChars(srcStr) {
    //RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
    var pattern = new RegExp("[#]");
    var rs = "";
    for (var i = 0; i < srcStr.length; i++) {
        rs = rs + srcStr.substr(i, 1).replace(pattern, '');
    }
    return rs;
}

