﻿//自动完成
function AutoComplete(ParamEntiy,objID) {
    this.AutoCompleteParam = {};
    this.Urldata = "";
    this.Url = "/HttpHandler/AutoComplete.ashx";
    this.KeyName = objID;
    var Instance = this;
    this.GetAutoAJAX = function () {
        //参数对象
        Instance.AutoCompleteParam.action = ParamEntiy.action;
        Instance.AutoCompleteParam.previewPower = ParamEntiy.previewPower;
        Instance.AutoCompleteParam.userSysNum = ParamEntiy.userSysNum;
        Instance.AutoCompleteParam.userUnitNum = ParamEntiy.userUnitNum;
        Instance.AutoCompleteParam.unitID = ParamEntiy.unitID;
        Instance.AutoCompleteParam.currYear = ParamEntiy.currYear;

        //地址数据
        Instance.Urldata = "&action=" + Instance.AutoCompleteParam.action + "&previewPower=" + Instance.AutoCompleteParam.previewPower + "&userSysNum=" + Instance.AutoCompleteParam.userSysNum + "&userUnitNum=" + Instance.AutoCompleteParam.userUnitNum+"&unitID="+Instance.AutoCompleteParam.unitID+"&currYear="+Instance.AutoCompleteParam.currYear;


        //绑定事件
        Instance.KeyName.autocomplete({
            source: function (request, response) {

                var param = "?keywords=" + escape(request.term);
                var ajaxurl = Instance.Url + param + Instance.Urldata;

                $.get(ajaxurl, function (result) {
                    if (result != null && result != "") {
                        var data = result.ds;
                        response($.map(data, function (item) {
                            return {
                                label: item.OtherName,
                                value: item.OtherName
                            }
                        }));
                    }
                    else {
                        return {
                            label: "",
                            value: ""
                        }
                    }

                });
            },
            minLength: 1,
            delay: 2
        });
    }
}