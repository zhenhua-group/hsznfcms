﻿//显示审核轨迹
$(function () {
    //取得审核轨迹的Container
    var auditLocusContainer = $("*[id=auditLocusContainer]");
    //绑定
    auditLocusContainer.live("click", function (e) {
        //取得当前的Action
        var action = $(this).attr("action");
        //取得当前的ReferenceSysNo
        var referenceSysNo = $(this).attr("referencesysno");
        var istranEconomy = $(this).attr("istranEconomy");
        var istranhave = $(this).attr("istranhave");
        //取得鼠标位置
        var left = e.pageX;
        var top = e.pageY;
        //取得父容器
        var parentContainer = $(this).parent("td:first");
        var withoutWidth = parentContainer.width();
        $.each(parentContainer.nextAll("td"), function (i, td) {
            withoutWidth += $(td).width();
        });
        //发送请求，取得审核轨迹情报
        $.post("/HttpHandler/AuditLocusHandler.ashx", { "Action": action, "ReferenceSysNo": referenceSysNo, "IstranEconomy": istranEconomy, "ISTrunHavc": istranhave }, function (jsonResult) {
            //取得审核轨迹情报
            var auditLocusList = Global.evalJSON(jsonResult);
            if (auditLocusList != undefined && auditLocusList != null) {
                $("body").append(CreateAuditLocusDiv(auditLocusList));
                $("#auditLocusDiv").css({ "left": $(document).width() - (withoutWidth + 450), "top": top - 10 });
            }
        });
    });

    //移除页面上的审核轨迹Div，监听Document的点击事件
    $(document).click(function (e) {
        //如果当前点击对象id不为auditLocusDiv
        if (e.target.id != "auditLocusDiv") {
            //从Dom中移除审核轨迹Div
            $("div[id=auditLocusDiv]").remove();
        }
    });
});

//创建一个新的审核轨迹Div
function CreateAuditLocusDiv(auditLocusList) {
    var popDiv = "";
    popDiv += "<div class=\"mainbox\" id=\"auditLocusDiv\"><div class=\"opacity-bg-iframe\"></div>";
    var divString = "";
    divString += "<table style=\"width: 100%;background-color:#e8ffe8;border-collapse:collapse;\" id=\"AuditLocus\" cellspacing=\"0\">";
    divString += "<tr style=\"background-color: gray;\">";
    divString += "<td style=\"width: 35px;\" align=\"center\">进度</td>";
    divString += " <td style=\"width: 150px;\" align=\"center\">";
    divString += " 评审阶段";
    divString += "</td>";
    divString += "<td style=\"width: 140px;\" align=\"center\">"
    divString += "评审人";
    divString += "</td>";
    divString += "<td style=\"width: 200px;\" align=\"center\">";
    divString += "评审时间";
    divString += "</td>";
    divString += "<td style=\"width:40px;\" align=\"center\">状态</td>";
    divString += "</tr>";
    //拼接具体内容
    if (auditLocusList && auditLocusList.length > 0) {
        $.each(auditLocusList, function (index, auditLocus) {
            divString += "<tr>";
            divString += "<td style=\"width:35px;border:solid 1px #CCC;\" align=\"center\">" + auditLocus.AuditStatus + "</td>";
            divString += "<td style=\"width:150px;border:solid 1px #CCC;\" align=\"center\">" + auditLocus.ProcessDescription + "</td>";

            divString += "<td style=\"width:150px;border:solid 1px #CCC;\" align=\"center\">" + auditLocus.AuditUserName + "</td>";
            divString += "<td style=\"width:200px;border:solid 1px #CCC;\" align=\"center\">" + auditLocus.AuditDate + "</td>";
            divString += "<td style=\"width:30px;border:solid 1px #CCC;\" align=\"center\">" + auditLocus.IsPass + "</td>";

            divString += "</tr>";
        });
    }
    else {
        divString += "<tr><td colspan=\"5\" align=\"center\" style=\"color:red;\">暂无评审记录，请提交评审!</td></tr>";
    }
    divString += "</table>";
    popDiv += divString;
    popDiv += "</div>";
    return popDiv;
}

//更改Processbar的颜色。改为黄色
function ChangedBackgroundColor() {
    $.each($(".progressbar"), function () {
        $(this).css({
            "height": "13px",
            "border": "1px solid #F90",
            "margin": "3px 3px 3px 3px"
        }).children("div:first").css({
            "border": "1px solid #F90;",
            "background": "#F6A828 url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x",
            "color": " white",
            "font-weight": " bold"
        });
    });
}