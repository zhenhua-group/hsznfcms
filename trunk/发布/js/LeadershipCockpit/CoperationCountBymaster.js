﻿
var drpValue;//类型
var start;//开始时间
var end;//结束时间
var hidPower;//权限
var year;//年份
var unit;//部门
var userUnit;//个人所在的部门
var startyear;//合同年份开始时间
var endyear;//合同年份结束时间

$(document).ready(function () {

    //权限
    hidPower = $("#hiddenischeckallpower").val();

    //个人所在的部门
    var userUnit = $("#hiddenUserUnit").val();

    //加载初始化数据
    InitialiseData();

    //设置表头信息
    setTableTitleText(0);

    //查询
    $("#btn_search").click(function () {
        //查询按钮值更改
        $("#ctl00_ContentPlaceHolder1_SelectType").val("1");

        //类型
        drpValue = $("#ctl00_ContentPlaceHolder1_drp_type :selected").val();

        //创建时间查询条件
        start = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        end = $("#ctl00_ContentPlaceHolder1_txt_end").val();
             
        
        //获取部门ID
        getCheckedUnitNode();
        var unitlist = $("#hidUnitListVal").val();
        if (unitlist != "0") {
            unit = unitlist;
        }
        else {
            unit = "";
        }

        setTableTitleText(1);

        // 合同目标完成 统计
        if (drpValue == "0") {
            var msg1 = Verification1();
            if (msg1 != "") {
                jAlert(msg1, "提示");
                return false;
            }
            CountCoperationAll(1);
        } else if (drpValue == "1") {
            var msg2 = Verification2();
            if (msg2 != "") {
                jAlert(msg2, "提示");
                return false;
            }
            //合同审批情况统计
            CountCoperationAudit(1);
        }
        else if (drpValue == "2") {
            //合同等级
            var msg3 = Verification3();
            if (msg3 != "") {
                jAlert(msg3, "提示");
                return false;
            }
            CountCoperationLevel(1);
        }
        else if (drpValue == "3" || drpValue == "4" || drpValue == "5" || drpValue == "6") {
            //合同类型
            var msg4 = Verification4();
            if (msg4 != "") {
                jAlert(msg4, "提示");
                return false;
            }
            CountCoperationType(1);
        }
        else if (drpValue == "7" || drpValue == "8" || drpValue == "9" || drpValue == "10") {
            //合同性质
            var msg5 = Verification5();
            if (msg5 != "") {
                jAlert(msg4, "提示");
                return false;
            }
            CountCoperationIndustry(1);
        }
    });
    //正常查询按钮
    $("#btn_cx").click(function () {
        //查询按钮值更改
        $("#ctl00_ContentPlaceHolder1_SelectType").val("0");
        setUnitOrTimeData();
    });

    //时间事件
    $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
        setUnitOrTimeData();
    });

    //导出
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {

        //类型
        drpValue = $("#ctl00_ContentPlaceHolder1_drp_type :selected").val();


        // 合同目标完成 统计
        if (drpValue == "0") {
            var msg1 = Verification1();
            if (msg1 != "") {
                jAlert(msg1, "提示");
                return false;
            }
            var unit1 = "";
            var unitSpans = $("span[id=userSpan]", $("#divUnit0"));
            if (unitSpans.length > 0) {
                for (var i = 0; i < unitSpans.length; i++) {
                    unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
                }
            }
            $("#ctl00_ContentPlaceHolder1_hiddenUnit").val(unit1);

        } else if (drpValue == "1") {
            var msg2 = Verification2();
            if (msg2 != "") {
                jAlert(msg2, "提示");
                return false;
            }
            //合同审批情况统计
            var unit1 = "";
            var unitSpans = $("span[id=userSpan]", $("#divUnit1"));
            if (unitSpans.length > 0) {
                for (var i = 0; i < unitSpans.length; i++) {
                    unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
                }
            }
            $("#ctl00_ContentPlaceHolder1_hiddenUnit").val(unit1);
        }
        else if (drpValue == "2") {
            //合同等级
            var msg3 = Verification3();
            if (msg3 != "") {
                jAlert(msg3, "提示");
                return false;
            }
            var unit1 = "";
            var unitSpans = $("span[id=userSpan]", $("#divUnit2"));
            if (unitSpans.length > 0) {
                for (var i = 0; i < unitSpans.length; i++) {
                    unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
                }
            }
            $("#ctl00_ContentPlaceHolder1_hiddenUnit").val(unit1);
        }
        else if (drpValue == "3" || drpValue == "4" || drpValue == "5" || drpValue == "6") {
            //合同类型
            var msg4 = Verification4();
            if (msg4 != "") {
                jAlert(msg4, "提示");
                return false;
            }
            var unit1 = "";
            var unitSpans = $("span[id=userSpan]", $("#divUnit3"));
            if (unitSpans.length > 0) {
                for (var i = 0; i < unitSpans.length; i++) {
                    unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
                }
            }
            $("#ctl00_ContentPlaceHolder1_hiddenUnit").val(unit1);
        }
        else if (drpValue == "7" || drpValue == "8" || drpValue == "9" || drpValue == "10") {
            //合同性质
            var msg5 = Verification5();
            if (msg5 != "") {
                jAlert(msg4, "提示");
                return false;
            }
            var unit1 = "";
            var unitSpans = $("span[id=userSpan]", $("#divUnit4"));
            if (unitSpans.length > 0) {
                for (var i = 0; i < unitSpans.length; i++) {
                    unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
                }
            }
            $("#ctl00_ContentPlaceHolder1_hiddenUnit").val(unit1);
        }
    });

    //合同类型
    $("#ctl00_ContentPlaceHolder1_drp_type").change(function () {

        //清空所有被选择的check

        $(":text", "#selectDiv").val("");
        $("div[id^=divUnit]", "#selectDiv").empty();
        $("select", "#selectDiv").val("-1");

        var checks = $('#chkDiv :checkbox');

        //清除 已选中的checked框
        $('input[type=checkbox]', "#chkDiv").each(function () {
            if ($(this).is(":checked")) {
                $(this).parent().attr("class", "");
                $(this).attr("checked", false);
            }
        });


        var value = $(this).val();
        if (value == "0") {
            $("#tb_check0", "#chkDiv").show();
            $("#tb_check0", "#chkDiv").siblings().hide();
        }

        if (value == "1") {
            $("#tb_check1", "#chkDiv").show();
            $("#tb_check1", "#chkDiv").siblings().hide();
        }

        if (value == "2") {
            $("#tb_check2", "#chkDiv").show();
            $("#tb_check2", "#chkDiv").siblings().hide();
        }

        if (value == "3" || value == "4" || value == "5" || value == "6") {
            $("#tb_check3", "#chkDiv").show();
            $("#tb_check3", "#chkDiv").siblings().hide();

            if (value == "3") {
                $("span", "#tb_select3").text("个");
                $("#ctl00_ContentPlaceHolder1_div_util4").html("单位：个");
            } else if (value == "4") {
                $("span", "#tb_select3").text("万元");
                $("#ctl00_ContentPlaceHolder1_div_util4").html("单位：万元");
            } else if (value == "5") {
                $("span", "#tb_select3").text("㎡");
                $("#ctl00_ContentPlaceHolder1_div_util4").html("单位：㎡");
            }
            else if (value == "6") {
                $("span", "#tb_select3").text("万元");
                $("#ctl00_ContentPlaceHolder1_div_util4").html("单位：万元");
            }
        }

        if (value == "7" || value == "8" || value == "9" || value == "10") {
            $("#tb_check4", "#chkDiv").show();
            $("#tb_check4", "#chkDiv").siblings().hide();
            if (value == "7") {
                $("span", "#tb_select4").text("个");
                $("#ctl00_ContentPlaceHolder1_div_util5").html("单位：个");
            } else if (value == "8") {
                $("span", "#tb_select4").text("万元");
                $("#ctl00_ContentPlaceHolder1_div_util5").html("单位：万元");
            } else if (value == "9") {
                $("span", "#tb_select4").text("㎡");
                $("#ctl00_ContentPlaceHolder1_div_util5").html("单位：㎡");
            }
            else if (value == "10") {
                $("span", "#tb_select4").text("万元");
                $("#ctl00_ContentPlaceHolder1_div_util5").html("单位：万元");
            }
        }

        $("table", "#selectDiv").hide();
        $("#tbSearch").hide();
        $("table tr", "#selectDiv").css("display", "none");

        setUnitOrTimeData();
    });


    //点击选择条件
    $("input[name=bb]", "#tb_check0").click(function () {

        //取得当前行数
        var clo = $(this).attr("clo");

        var isChecked = $(this).attr("checked");

        //取得当前行数
        var clo = $(this).attr("clo");
        if (isChecked == undefined) {
            //清空
            $(this).parent().attr("class", "");
            $(this).attr("checked", false);
            $("#tb_select0 tr:eq(" + clo + ")").hide();
            $(":text", "#tb_select0 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select0").empty();
            $("select", "#tb_select0").val("-1");
        }
        else {
            $(this).attr("checked", "checked");
            $(this).parent().attr("class", "checked");
            $("#tb_select0 tr:eq(" + parseInt(clo) + ")").show();
        }


        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tb_check0")).length > 0) {
            $("#tb_select0").show();
            $("#tbSearch").show();
        }
        else {
            $("#tb_select0").hide();
            $("#tbSearch").hide();
            $(":text", "#tb_select0 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select0").empty();
            $("select", "#tb_select0").val("-1");
            setUnitOrTimeData();
        }

    });


    //点击选择条件
    $("input[name=bb]", "#tb_check1").click(function () {

        //取得当前行数
        var clo = $(this).attr("clo");

        var isChecked = $(this).attr("checked");

        //取得当前行数
        var clo = $(this).attr("clo");
        if (isChecked == undefined) {
            //清空
            $(this).parent().attr("class", "");
            $(this).attr("checked", false);
            $("#tb_select1 tr:eq(" + clo + ")").hide();
            $(":text", "#tb_select1 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select1").empty();
            $("select", "#tb_select1").val("-1");
        }
        else {
            $(this).attr("checked", "checked");
            $(this).parent().attr("class", "checked");
            $("#tb_select1 tr:eq(" + parseInt(clo) + ")").show();
        }

        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", "#tb_check1").length > 0) {
            $("#tb_select1").show();
            $("#tbSearch").show();
        }
        else {
            $("#tb_select1").hide();
            $("#tbSearch").hide();
            $(":text", "#tb_select1 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select1").empty();
            $("select", "#tb_select1").val("-1");
            setUnitOrTimeData();
        }

    });

    //点击选择条件
    $("input[name=bb]:checkbox", "#tb_check2").click(function () {

        //取得当前行数
        var clo = $(this).attr("clo");

        var isChecked = $(this).attr("checked");

        //取得当前行数
        var clo = $(this).attr("clo");
        if (isChecked == undefined) {
            //清空
            $(this).parent().attr("class", "");
            $(this).attr("checked", false);
            $("#tb_select2 tr:eq(" + clo + ")").hide();
            $(":text", "#tb_select2 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select2").empty();
            $("select", "#tb_select2").val("-1");
        }
        else {

            $(this).attr("checked", "checked");
            $(this).parent().attr("class", "checked");
            $("#tb_select2 tr:eq(" + parseInt(clo) + ")").show();
        }


        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tb_check2")).length > 0) {
            $("#tb_select2").show();
            $("#tbSearch").show();
        }
        else {
            $("#tb_select2").hide();
            $("#tbSearch").hide();
            $(":text", "#tb_select2 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select2").empty();
            $("select", "#tb_select2").val("-1");
            setUnitOrTimeData();
        }

    });

    //点击选择条件
    $("input[name=bb]:checkbox", "#tb_check3").click(function () {

        //取得当前行数
        var clo = $(this).attr("clo");

        var isChecked = $(this).attr("checked");

        //取得当前行数
        var clo = $(this).attr("clo");
        if (isChecked == undefined) {
            //清空
            $(this).parent().attr("class", "");
            $(this).attr("checked", false);
            $("#tb_select3 tr:eq(" + clo + ")").hide();
            $(":text", "#tb_select3 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select3").empty();
            $("select", "#tb_select3").val("-1");
        }
        else {

            $(this).attr("checked", "checked");
            $(this).parent().attr("class", "checked");
            $("#tb_select3 tr:eq(" + parseInt(clo) + ")").show();
        }


        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tb_check3")).length > 0) {
            $("#tb_select3").show();
            $("#tbSearch").show();
        }
        else {
            $("#tb_select3").hide();
            $("#tbSearch").hide();
            $(":text", "#tb_select3 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select3").empty();
            $("select", "#tb_select3").val("-1");
            setUnitOrTimeData();
        }

    });

    //点击选择条件
    $("input[name=bb]:checkbox", "#tb_check4").click(function () {

        //取得当前行数
        var clo = $(this).attr("clo");

        var isChecked = $(this).attr("checked");

        //取得当前行数
        var clo = $(this).attr("clo");
        if (isChecked == undefined) {
            //清空
            $(this).parent().attr("class", "");
            $(this).attr("checked", false);
            $("#tb_select4 tr:eq(" + clo + ")").hide();
            $(":text", "#tb_select4 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select4").empty();
            $("select", "#tb_select4").val("-1");
        }
        else {

            $(this).attr("checked", "checked");
            $(this).parent().attr("class", "checked");
            $("#tb_select4 tr:eq(" + parseInt(clo) + ")").show();
        }


        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tb_check4")).length > 0) {
            $("#tb_select4").show();
            $("#tbSearch").show();
        }
        else {
            $("#tb_select4").hide();
            $("#tbSearch").hide();
            $(":text", "#tb_select4 tr:eq(" + clo + ")").val("");
            $("div[id^=divUnit]", "#tb_select4").empty();
            $("select", "#tb_select4").val("-1");
            setUnitOrTimeData();
        }

    });

    //选择多部门信息
    $("input[choose=choose]").click(function () {

        var labtext = $(this).parent().next().text();

        var chooesUnit = $(this).parent().prev().children().find("option:selected").text();
        var chooesUnitVal = $(this).parent().prev().children().find("option:selected").val();
        var userSpans = $("span[id=userSpan]", $(this).parent().next());

        //循环判断是否有重复用户的存在
        for (var i = 0; i < userSpans.length; i++) {
            if ($(userSpans[i]).attr("usersysno") == chooesUnitVal) {
                return false;
            }
        }

        if (chooesUnitVal == "-1") {
            $(this).parent().next().empty();
            $(this).parent().next().append("<span style=\"margin-right:10px;\" id=\"userSpan\" unitAll=\"unitAll\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">全院部门<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        } else {
            $(this).parent().next().children("span[unitAll=unitAll]").remove();
            $(this).parent().next().append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">" + chooesUnit + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        }
    });


    $("img[id=deleteUserlinkButton]").live("click", function () {
        if (confirm("确认要删除这个部门吗？")) {
            //删除部门
            $(this).parent("span[id=userSpan]:first").remove();
        }
        return false;
    });


    HideTableTr($("#tbTwo tbody tr"));
    HideTableTr($("#tbThree tbody tr"));
    HideTableTr($("#tbFour tbody tr"));
    HideTableTr($("#tbFive tbody tr"));
});

//加载初始化数据
function InitialiseData() {

    //类型
    drpValue = $("#ctl00_ContentPlaceHolder1_drp_type :selected").val();

    //创建时间查询条件
    start = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    end = $("#ctl00_ContentPlaceHolder1_txt_end").val();


    //合同年份
    var tempYear = $("#ctl00_ContentPlaceHolder1_drp_year :selected").val();
    if (tempYear != "-1") {
        year = tempYear;
    }
    getCheckedUnitNode();
    var unitlist = $("#hidUnitListVal").val();
    if (unitlist != "0") {
        unit = unitlist;
    }
    else {
        unit = "";
    }

    CountCoperationAll(0);
}

//部门下拉框设置
function setUnitOrTimeData() {
    //类型
    drpValue = $("#ctl00_ContentPlaceHolder1_drp_type :selected").val();

    //创建时间查询条件
    start = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    end = $("#ctl00_ContentPlaceHolder1_txt_end").val();

    //当前年份
  //var myDate = new Date();
  //var curryear = myDate.getFullYear();

    //合同年份
    var tempYear = $("#ctl00_ContentPlaceHolder1_drp_year :selected").val();
    //选中时间段
    if ($("#ctl00_ContentPlaceHolder1_hid_time").val() == "1") {
        startyear = $("#ctl00_ContentPlaceHolder1_txt_year1").val();
        endyear = $("#ctl00_ContentPlaceHolder1_txt_year2").val();
        year = "";
    }
    else {
        if (tempYear != "-1") {
            //年
            year = tempYear;
            //季度
            var strjidu = $("#ctl00_ContentPlaceHolder1_drpJidu").val();
            //月
            var stryue = $("#ctl00_ContentPlaceHolder1_drpMonth").val();
            //年
            if (strjidu == "0" && stryue == "0") {
                startyear = year + "-01-01 00:00:00";
                endyear = year + "-12-31 23:59:59 ";
            }
            else if (strjidu != "0" && stryue == "0") //年季度
            {
                startyear = year;
                endyear = year;
                switch (strjidu) {
                    case "1":
                        startyear += "-01-01 00:00:00";
                        endyear += "-03-31 23:59:59";
                        break;
                    case "2":
                        startyear += "-04-01 00:00:00";
                        endyear += "-06-30 23:59:59";
                        break;
                    case "3":
                        startyear += "-07-01 00:00:00";
                        endyear += "-09-30 23:59:59";
                        break;
                    case "4":
                        startyear += "-10-01 00:00:00";
                        endyear += "-12-31 23:59:59";
                        break;
                }
            }
            else if (strjidu == "0" && stryue != "0")//年月份
            {
                //当月有几天
                var days = GetCurrDay(year, stryue);               
                startyear = year + "-" + stryue + "-01 00:00:00";
                endyear = year + "-" + stryue + "-" + days + " 23:59:59";
            }
        }
        else {
            //所有年
            year = "";
            startyear = "";
            endyear = "";
        }
    }

    getCheckedUnitNode();
    var unitlist = $("#hidUnitListVal").val();
    if (unitlist != "0") {
        unit = unitlist;
    }
    else {
        unit = "";
    }

    setTableTitleText(0);

    // 合同目标完成 统计
    if (drpValue == "0") {

        CountCoperationAll(0);
    } else if (drpValue == "1") {

        //合同审批情况统计
        CountCoperationAudit(0);
    }
    else if (drpValue == "2") {
        //合同等级

        CountCoperationLevel(0);
    }
    else if (drpValue == "3" || drpValue == "4" || drpValue == "5" || drpValue == "6") {
        //合同类型
        CountCoperationType(0);
    }
    else if (drpValue == "7" || drpValue == "8" || drpValue == "9" || drpValue == "10") {
        //合同性质
        CountCoperationIndustry(0);
    }
}

//合同目标完成 统计
function CountCoperationAll(status) {

    $("#ctl00_ContentPlaceHolder1_div_Container1", "#div_Table").show();;
    $("#ctl00_ContentPlaceHolder1_div_Container1", "#div_Table").siblings().hide();


    //合同目标值
    var cprTaretAcount1 = $("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount1").val();
    var cprTaretAcount2 = $("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount2").val();

    //完成合同额

    var cprAcount1 = $("#ctl00_ContentPlaceHolder1_txt_CprAcount1").val();
    var cprAcount2 = $("#ctl00_ContentPlaceHolder1_txt_CprAcount2").val();

    //合同额完成比例

    var cprAcountPercent1 = $("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent1").val();
    var cprAcountPercent2 = $("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent2").val();

    //目标产值
    var cprTarentCharge1 = $("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge1").val();
    var cprTarentCharge2 = $("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge2").val();

    //完成产值
    var cprCharge1 = $("#ctl00_ContentPlaceHolder1_txt_CprCharge1").val();
    var cprCharge2 = $("#ctl00_ContentPlaceHolder1_txt_CprCharge2").val();

    //完成产值比例
    var cprChargePercent1 = $("#ctl00_ContentPlaceHolder1_txt_CprChargePercent1").val();
    var cprChargePercent2 = $("#ctl00_ContentPlaceHolder1_txt_CprChargePercent2").val();

    var parm;

    //表示普通查询
    if (status == 0) {
        parm = {
            "action": "search",
            "year": year,
            "startyear": startyear,
            "endyear": endyear,
            "hidtime":$("#ctl00_ContentPlaceHolder1_hid_time").val(),
            "unit": unit,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status
        }
    }
    else {
        var unit1 = unit;
        //var unitSpans = $("span[id=userSpan]", $("#divUnit0"));
        //if (unitSpans.length > 0) {
        //    for (var i = 0; i < unitSpans.length; i++) {
        //        unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
        //    }
        //}
      
        parm = {
            "action": "search",
            "year": year,
            "unit": unit1,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "status": status,
            "type": drpValue,
            "cprTaretAcount1": cprTaretAcount1,
            "cprTaretAcount2": cprTaretAcount2,
            "cprAcount1": cprAcount1,
            "cprAcount2": cprAcount2,
            "cprAcountPercent1": cprAcountPercent1,
            "cprAcountPercent2": cprAcountPercent2,
            "cprTarentCharge1": cprTarentCharge1,
            "cprTarentCharge2": cprTarentCharge2,
            "cprCharge1": cprCharge1,
            "cprCharge2": cprCharge2,
            "cprChargePercent1": cprChargePercent1,
            "cprChargePercent2": cprChargePercent2
        }

    }

    $.post("/HttpHandler/LeadershipCockpit/CoperationCountBymasterHandler.ashx", parm, function (jsonResult) {
        $("#tbOne tr:gt(0)").remove();
        var sumb = 0, sumc = 0, sumd = 0, sume = 0, sumf = 0, sumg = 0;
        if (jsonResult != null || jsonResult != "") {
            var json = eval('(' + jsonResult + ')');
            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {

                $.each(data, function (i, n) {
                    var trString = "<tr><td>" + n.Name + "</td>";
                    trString += "<td>" + n.cpryear + "</td>";
                    trString += "<td>" + n.CprTarget + "</td>";
                    trString += "<td>" + n.CprAllCount + "</td>";
                    if (n.Cprprt >= 100) {
                        trString += "<td style=\"color:red;\">" + n.Cprprt + "</td>";
                    }
                    else {
                        trString += "<td>" + n.Cprprt + "</td>";
                    }
                    trString += "<td>" + n.StartTime + "</td>";
                    trString += "<td>" + n.EndTime + "</td>";
                    trString += "<td>" + n.AllotTarget + "</td>";
                    trString += "<td>" + n.AllotCount + "</td>";
                    if (n.AllotPrt >= 100) {
                        trString += "<td style=\"color:red;\">" + n.AllotPrt + "</td>";
                    }
                    else {
                        trString += "<td>" + n.AllotPrt + "</td>";
                    }
                    trString += "</tr>";

                    sumb += parseFloat(n.CprTarget);
                    sumc += parseFloat(n.CprAllCount);
                   // sumd += parseFloat(n.Cprprt);
                    sume += parseFloat(n.AllotTarget);
                    sumf += parseFloat(n.AllotCount);
                 //   sumg = sumg + parseFloat(n.AllotPrt);

                    $("#tbOne").append(trString);
                });
                //合计
                var endtrString = "<tr style='font-weight:bold;'><td>全院总计</td>";
                endtrString += "<td></td>";
                endtrString += "<td>" + sumb.toFixed("2") + "</td>";
                endtrString += "<td>" + sumc.toFixed("2") + "</td>";
                sumd = (sumc / sumb) * 100;
                endtrString += "<td>" + sumd.toFixed("2") + "</td>";                
                endtrString += "<td></td>";
                endtrString += "<td></td>";
                endtrString += "<td>" + sume.toFixed("4") + "</td>";
                endtrString += "<td>" + sumf.toFixed("4") + "</td>";
                sumg = (sumf / sume) * 100;
                endtrString += "<td>" + sumg.toFixed("2") + "</td>";
                endtrString += "</tr>";
                $("#tbOne").append(endtrString);
            }

        } else {

        }
        HideTableTr($("#tbOne tbody tr"));
    });

}

//合同审批情况统计
function CountCoperationAudit(status) {

    $("#ctl00_ContentPlaceHolder1_div_Container2", "#div_Table").show();;
    $("#ctl00_ContentPlaceHolder1_div_Container2", "#div_Table").siblings().hide();

    //已签合同数
    var cprCount1 = $("#ctl00_ContentPlaceHolder1_txtCprCount1").val();
    var cprCount2 = $("#ctl00_ContentPlaceHolder1_txtCprCount2").val();

    //签订合同额
    var cprSignAcount1 = $("#ctl00_ContentPlaceHolder1_txtCprSignAcount1").val();
    var cprSignAcount2 = $("#ctl00_ContentPlaceHolder1_txtCprSignAcount2").val();

    //建筑规模
    var bulid1 = $("#ctl00_ContentPlaceHolder1_txtBulid1").val();
    var bulid2 = $("#ctl00_ContentPlaceHolder1_txtBulid2").val();

    //收款金额
    var chargeCount1 = $("#ctl00_ContentPlaceHolder1_txtChargeCount1").val();
    var chargeCount2 = $("#ctl00_ContentPlaceHolder1_txtChargeCount2").val();

    //审批听过合同数
    var passCount1 = $("#ctl00_ContentPlaceHolder1_txtPassCount1").val();
    var passCount2 = $("#ctl00_ContentPlaceHolder1_txtPassCount2").val();

    //正在审批合同数
    var passingCount1 = $("#ctl00_ContentPlaceHolder1_txtPassingCount1").val();
    var passingCount2 = $("#ctl00_ContentPlaceHolder1_txtPassingCount2").val();

    //完成产值比例
    var notPassingCount1 = $("#ctl00_ContentPlaceHolder1_txtNotPassingCount1").val();
    var notPassingCount2 = $("#ctl00_ContentPlaceHolder1_txtNotPassingCount2").val();

    //合同年份 区间值
    var startTime = $("#ctl00_ContentPlaceHolder1_drp_year2_01").val();
    var endTime = $("#ctl00_ContentPlaceHolder1_drp_year2_02").val();

    var parm;
    //表示 
    if (status == 0) {
      
        parm = {
            "action": "search1",
            "year": year,
            "startyear": startyear,
            "endyear": endyear,
            "hidtime": $("#ctl00_ContentPlaceHolder1_hid_time").val(),
            "unit": unit,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status
        }
    }
    else {
        var unit1 = unit;
        //var unitSpans = $("span[id=userSpan]", $("#divUnit1"));
        //if (unitSpans.length > 0) {
        //    for (var i = 0; i < unitSpans.length; i++) {
        //        unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
        //    }
        //}

        parm = {
            "action": "search1",
            "year": year,
            "unit": unit1,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status,
            "cprCount1": cprCount1,
            "cprCount2": cprCount2,
            "cprSignAcount1": cprSignAcount1,
            "cprSignAcount2": cprSignAcount2,
            "bulid1": bulid1,
            "bulid2": bulid2,
            "chargeCount1": chargeCount1,
            "chargeCount2": chargeCount2,
            "passCount1": passCount1,
            "passCount2": passCount2,
            "passingCount1": passingCount1,
            "passingCount2": passingCount2,
            "notPassingCount1": notPassingCount1,
            "notPassingCount2": notPassingCount2,
            "startTime": startTime,
            "endTime": endTime
        }
    }

    $.post("/HttpHandler/LeadershipCockpit/CoperationCountBymasterHandler.ashx", parm, function (jsonResult) {
        $("#tbTwo tr:gt(0)").remove();
      
        if (jsonResult != null || jsonResult != "") {
            var json = eval('(' + jsonResult + ')');

            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {
                $.each(data, function (i, n) {
                    var trString = "<tr><td>" + n.Name + "</td>";
                    trString += "<td>" + n.A + "</td>";
                    trString += "<td>" + n.B + "</td>";
                    trString += "<td>" + n.C + "</td>";
                    trString += "<td>" + n.D + "</td>";
                    trString += "<td>" + n.E + "</td>";
                    trString += "<td>" + n.F + "</td>";
                    trString += "<td>" + n.G + "</td>";
                    trString += "</tr>";

                    $("#tbTwo").append(trString);
                });               
            }
        } else {

        }
        $("#tbTwo tr:last").css("font-weight", "bold");
        HideTableTr($("#tbTwo tbody tr"));
    });

}

//合同等级
function CountCoperationLevel(status) {


    $("#ctl00_ContentPlaceHolder1_div_Container3", "#div_Table").show();;
    $("#ctl00_ContentPlaceHolder1_div_Container3", "#div_Table").siblings().hide();

    //特级数量
    var tJCount1 = $("#ctl00_ContentPlaceHolder1_txtTJCount1").val();
    var tJCount2 = $("#ctl00_ContentPlaceHolder1_txtTJCount2").val();

    //特级合同额
    var tJCprAcount1 = $("#ctl00_ContentPlaceHolder1_txtTJCprAcount1").val();
    var tJCprAcount2 = $("#ctl00_ContentPlaceHolder1_txtTJCprAcount2").val();

    //特级收款金额
    var tJCprChargeAcount1 = $("#ctl00_ContentPlaceHolder1_txtTJCprChargeAcount1").val();
    var tJCprChargeAcount2 = $("#ctl00_ContentPlaceHolder1_txtTJCprChargeAcount2").val();

    //一级数量
    var yJCount1 = $("#ctl00_ContentPlaceHolder1_txtYJCount1").val();
    var yJCount2 = $("#ctl00_ContentPlaceHolder1_txtYJCount2").val();

    //一级合同额
    var yJCprAcount1 = $("#ctl00_ContentPlaceHolder1_txtYJCprAcount1").val();
    var yJCprAcount2 = $("#ctl00_ContentPlaceHolder1_txtYJCprAcount2").val();

    //一级收款金额
    var yJCprChargeAcount1 = $("#ctl00_ContentPlaceHolder1_txtYJCprChargeAcount1").val();
    var yJCprChargeAcount2 = $("#ctl00_ContentPlaceHolder1_txtYJCprChargeAcount2").val();

    //二级数量
    var eJCount1 = $("#ctl00_ContentPlaceHolder1_txtEJCount1").val();
    var eJCount2 = $("#ctl00_ContentPlaceHolder1_txtEJCount2").val();

    //二级合同额
    var eJCprAcount1 = $("#ctl00_ContentPlaceHolder1_txtEJCprAcount1").val();
    var eJCprAcount2 = $("#ctl00_ContentPlaceHolder1_txtEJCprAcount2").val();

    //二级收款金额
    var eJCprChargeAcount1 = $("#ctl00_ContentPlaceHolder1_txtEJCprChargeAcount1").val();
    var eJCprChargeAcount2 = $("#ctl00_ContentPlaceHolder1_txtEJCprChargeAcount2").val();

    //三级数量
    var sJCount1 = $("#ctl00_ContentPlaceHolder1_txtSJCount1").val();
    var sJCount2 = $("#ctl00_ContentPlaceHolder1_txtSJCount2").val();

    //三级合同额
    var sJCprAcount1 = $("#ctl00_ContentPlaceHolder1_txtSJCprAcount1").val();
    var sJCprAcount2 = $("#ctl00_ContentPlaceHolder1_txtSJCprAcount2").val();

    //三级收款金额
    var sJCprChargeAcount1 = $("#ctl00_ContentPlaceHolder1_txtSJCprChargeAcount1").val();
    var sJCprChargeAcount2 = $("#ctl00_ContentPlaceHolder1_txtSJCprChargeAcount2").val();

    //合同年份 区间值
    var startTime = $("#ctl00_ContentPlaceHolder1_drp_year3_01").val();
    var endTime = $("#ctl00_ContentPlaceHolder1_drp_year3_02").val();

    var parm;
    //表示 
    if (status == 0) {
        parm = {
            "action": "search2",
            "year": year,
            "startyear": startyear,
            "endyear": endyear,
            "hidtime": $("#ctl00_ContentPlaceHolder1_hid_time").val(),
            "unit": unit,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status
        }
    }
    else {
        var unit1 = unit;
        //var unitSpans = $("span[id=userSpan]", $("#divUnit2"));
        //if (unitSpans.length > 0) {
        //    for (var i = 0; i < unitSpans.length; i++) {
        //        unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
        //    }
        //}

        parm = {
            "action": "search2",
            "year": year,
            "unit": unit1,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status,
            "tJCount1": tJCount1,
            "tJCount2": tJCount2,
            "tJCprAcount1": tJCprAcount1,
            "tJCprAcount2": tJCprAcount2,
            "tJCprChargeAcount1": tJCprChargeAcount1,
            "tJCprChargeAcount2": tJCprChargeAcount2,
            "yJCount1": yJCount1,
            "yJCount2": yJCount2,
            "yJCprAcount1": yJCprAcount1,
            "yJCprAcount2": yJCprAcount2,
            "yJCprChargeAcount1": yJCprChargeAcount1,
            "yJCprChargeAcount2": yJCprChargeAcount2,
            "eJCount1": eJCount1,
            "eJCount2": eJCount2,
            "eJCprAcount1": eJCprAcount1,
            "eJCprAcount2": eJCprAcount2,
            "eJCprChargeAcount1": eJCprChargeAcount1,
            "eJCprChargeAcount2": eJCprChargeAcount2,
            "sJCount1": sJCount1,
            "sJCount2": sJCount2,
            "sJCprAcount1": sJCprAcount1,
            "sJCprAcount2": sJCprAcount2,
            "sJCprChargeAcount1": sJCprChargeAcount1,
            "sJCprChargeAcount2": sJCprChargeAcount2,
            "startTime": startTime,
            "endTime": endTime
        }

    }
    $.post("/HttpHandler/LeadershipCockpit/CoperationCountBymasterHandler.ashx", parm, function (jsonResult) {
        $("#tbThree tr:gt(1)").remove();
        if (jsonResult != null || jsonResult != "") {
            var json = eval('(' + jsonResult + ')');
            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {
                $.each(data, function (i, n) {
                    var trString = "<tr><td>" + n.Name + "</td>";
                    trString += "<td>" + n.A + "</td>";
                    trString += "<td>" + n.A1 + "</td>";
                    trString += "<td>" + n.A2 + "</td>";
                    trString += "<td>" + n.B + "</td>";
                    trString += "<td>" + n.B1 + "</td>";
                    trString += "<td>" + n.B2 + "</td>";
                    trString += "<td>" + n.C + "</td>";
                    trString += "<td>" + n.C1 + "</td>";
                    trString += "<td>" + n.C2 + "</td>";
                    trString += "<td>" + n.D + "</td>";
                    trString += "<td>" + n.D1 + "</td>";
                    trString += "<td>" + n.D2 + "</td>";
                    trString += "</tr>";
                    $("#tbThree").append(trString);
                });
            }

        } else {

        }
        $("#tbThree tr:last").css("font-weight", "bold");
        HideTableTr($("#tbThree tbody tr"));
    });

}
//合同类型
function CountCoperationType(status) {


    $("#ctl00_ContentPlaceHolder1_div_Container4", "#div_Table").show();;
    $("#ctl00_ContentPlaceHolder1_div_Container4", "#div_Table").siblings().hide();


    var type1_01 = $("#ctl00_ContentPlaceHolder1_txtType1_01").val();
    var type1_02 = $("#ctl00_ContentPlaceHolder1_txtType1_02").val();

    var type2_01 = $("#ctl00_ContentPlaceHolder1_txtType2_01").val();
    var type2_02 = $("#ctl00_ContentPlaceHolder1_txtType2_02").val();

    var type3_01 = $("#ctl00_ContentPlaceHolder1_txtType3_01").val();
    var type3_02 = $("#ctl00_ContentPlaceHolder1_txtType3_02").val();

    var type4_01 = $("#ctl00_ContentPlaceHolder1_txtType4_01").val();
    var type4_02 = $("#ctl00_ContentPlaceHolder1_txtType4_02").val();

    var type5_01 = $("#ctl00_ContentPlaceHolder1_txtType5_01").val();
    var type5_02 = $("#ctl00_ContentPlaceHolder1_txtType5_02").val();

    var type6_01 = $("#ctl00_ContentPlaceHolder1_txtType6_01").val();
    var type6_02 = $("#ctl00_ContentPlaceHolder1_txtType6_02").val();

    var type7_01 = $("#ctl00_ContentPlaceHolder1_txtType7_01").val();
    var type7_02 = $("#ctl00_ContentPlaceHolder1_txtType7_02").val();

    var type8_01 = $("#ctl00_ContentPlaceHolder1_txtType8_01").val();
    var type8_02 = $("#ctl00_ContentPlaceHolder1_txtType8_02").val();

    //合同年份 区间值
    var startTime = $("#ctl00_ContentPlaceHolder1_drp_year4_01").val();
    var endTime = $("#ctl00_ContentPlaceHolder1_drp_year4_02").val();

    var parm;
    //表示 
    if (status == 0) {
        parm = {
            "action": "search3",
            "year": year,
            "startyear": startyear,
            "endyear": endyear,
            "hidtime": $("#ctl00_ContentPlaceHolder1_hid_time").val(),
            "unit": unit,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status
        }
    }
    else {
        var unit1 = unit;
        //var unitSpans = $("span[id=userSpan]", $("#divUnit3"));
        //if (unitSpans.length > 0) {
        //    for (var i = 0; i < unitSpans.length; i++) {
        //        unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
        //    }
        //}
        var parm = {
            "action": "search3",
            "year": year,
            "unit": unit1,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status,
            "type1_01": type1_01,
            "type1_02": type1_02,
            "type2_01": type2_01,
            "type2_02": type2_02,
            "type3_01": type3_01,
            "type3_02": type3_02,
            "type4_01": type4_01,
            "type4_02": type4_02,
            "type5_01": type5_01,
            "type5_02": type5_02,
            "type6_01": type6_01,
            "type6_02": type6_02,
            "type7_01": type7_01,
            "type7_02": type7_02,
            "type8_01": type8_01,
            "type8_02": type8_02,
            "startTime": startTime,
            "endTime": endTime
        }
    }

    $.post("/HttpHandler/LeadershipCockpit/CoperationCountBymasterHandler.ashx", parm, function (jsonResult) {
        $("#tbFour tr:gt(0)").remove();
        if (jsonResult != null || jsonResult != "") {
            var json = eval('(' + jsonResult + ')');

            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {
                $.each(data, function (i, n) {
                    var trString = "<tr><td>" + n.Name + "</td>";
                    trString += "<td>" + n.A + "</td>";
                    trString += "<td>" + n.B + "</td>";
                    trString += "<td>" + n.C + "</td>";
                    trString += "<td>" + n.D + "</td>";
                    trString += "<td>" + n.E + "</td>";
                    trString += "<td>" + n.F + "</td>";
                    trString += "<td>" + n.G + "</td>";
                    trString += "<td>" + n.H + "</td>";

                    trString += "</tr>";
                    $("#tbFour").append(trString);
                });
            }

        } else {

        }
        $("#tbFour tr:last").css("font-weight", "bold");
        HideTableTr($("#tbFour tbody tr"));
    });
}

//合同性质
function CountCoperationIndustry(status) {


    $("#ctl00_ContentPlaceHolder1_div_Container5", "#div_Table").show();;
    $("#ctl00_ContentPlaceHolder1_div_Container5", "#div_Table").siblings().hide();

    var property1_01 = $("#ctl00_ContentPlaceHolder1_txtProperty1_01").val();
    var property1_02 = $("#ctl00_ContentPlaceHolder1_txtProperty1_02").val();

    var property2_01 = $("#ctl00_ContentPlaceHolder1_txtProperty2_01").val();
    var property2_02 = $("#ctl00_ContentPlaceHolder1_txtProperty2_02").val();

    var property3_01 = $("#ctl00_ContentPlaceHolder1_txtProperty3_01").val();
    var property3_02 = $("#ctl00_ContentPlaceHolder1_txtProperty3_02").val();

    var property4_01 = $("#ctl00_ContentPlaceHolder1_txtProperty4_01").val();
    var property4_02 = $("#ctl00_ContentPlaceHolder1_txtProperty4_02").val();

    var property5_01 = $("#ctl00_ContentPlaceHolder1_txtProperty5_01").val();
    var property5_02 = $("#ctl00_ContentPlaceHolder1_txtProperty5_02").val();

    var property6_01 = $("#ctl00_ContentPlaceHolder1_txtProperty6_01").val();
    var property6_02 = $("#ctl00_ContentPlaceHolder1_txtProperty6_02").val();

    var property7_01 = $("#ctl00_ContentPlaceHolder1_txtProperty7_01").val();
    var property7_02 = $("#ctl00_ContentPlaceHolder1_txtProperty7_02").val();

    var property8_01 = $("#ctl00_ContentPlaceHolder1_txtProperty8_01").val();
    var property8_02 = $("#ctl00_ContentPlaceHolder1_txtProperty8_02").val();

    var property9_01 = $("#ctl00_ContentPlaceHolder1_txtProperty9_01").val();
    var property9_02 = $("#ctl00_ContentPlaceHolder1_txtProperty9_02").val();

    //合同年份 区间值
    var startTime = $("#ctl00_ContentPlaceHolder1_drp_year5_01").val();
    var endTime = $("#ctl00_ContentPlaceHolder1_drp_year5_02").val();

    var parm;
    //表示 
    if (status == 0) {
        parm = {
            "action": "search4",
            "year": year,
            "startyear": startyear,
            "endyear": endyear,
            "hidtime": $("#ctl00_ContentPlaceHolder1_hid_time").val(),
            "unit": unit,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status
        }
    }
    else {
        var unit1 = unit;
        //var unitSpans = $("span[id=userSpan]", $("#divUnit4"));
        //if (unitSpans.length > 0) {
        //    for (var i = 0; i < unitSpans.length; i++) {
        //        unit1 += "" + $(unitSpans[i]).attr("usersysno") + ",";
        //    }
        //}
        var parm = {
            "action": "search4",
            "year": year,
            "unit": unit1,
            "userUnit": $("#hiddenUserUnit").val(),
            "hidPower": hidPower,
            "start": start,
            "end": end,
            "type": drpValue,
            "status": status,
            "property1_01": property1_01,
            "property1_02": property1_02,
            "property2_01": property2_01,
            "property2_02": property2_02,
            "property3_01": property3_01,
            "property3_02": property3_02,
            "property4_01": property4_01,
            "property4_02": property4_02,
            "property5_01": property5_01,
            "property5_02": property5_02,
            "property6_01": property6_01,
            "property6_02": property6_02,
            "property7_01": property7_01,
            "property7_02": property7_02,
            "property8_01": property8_01,
            "property8_02": property8_02,
            "property9_01": property9_01,
            "property9_02": property9_02,
            "startTime": startTime,
            "endTime": endTime
        }
    }

    $.post("/HttpHandler/LeadershipCockpit/CoperationCountBymasterHandler.ashx", parm, function (jsonResult) {
        $("#tbFive tr:gt(0)").remove();
        if (jsonResult != null || jsonResult != "") {
            var json = eval('(' + jsonResult + ')');

            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {
                $.each(data, function (i, n) {
                    var trString = "<tr><td>" + n.Name + "</td>";
                    trString += "<td>" + n.A + "</td>";
                    trString += "<td>" + n.B + "</td>";
                    trString += "<td>" + n.C + "</td>";
                    trString += "<td>" + n.D + "</td>";
                    trString += "<td>" + n.E + "</td>";
                    trString += "<td>" + n.F + "</td>";
                    trString += "<td>" + n.G + "</td>";
                    trString += "<td>" + n.H + "</td>";
                    trString += "<td>" + n.I + "</td>";
                    trString += "</tr>";
                    $("#tbFive").append(trString);
                });
            }

        } else {

        }
        $("#tbFive tr:last").css("font-weight", "bold");
        HideTableTr($("#tbFive tbody tr"));
    });

}

//设置表头信息
function setTableTitleText(status) {

    var type = $("#ctl00_ContentPlaceHolder1_drp_type :selected").val();
    var drpTypeText = $("#ctl00_ContentPlaceHolder1_drp_type :selected").text();
    var drpText = $("#ctl00_ContentPlaceHolder1_drpunit_divDropdownTreeText").text();//获取部门名称
   // var drpValue = $("#hidUnitListVal").val();
    var drpYearValue = $("#ctl00_ContentPlaceHolder1_drp_year :selected").val();
    var drpYearText = $("#ctl00_ContentPlaceHolder1_drp_year :selected").text();
    var unitname = $.trim($("#hidUnitName").val());

    var strTip = "";
    if (status == "0") {
        //权限是部门
        if (hidPower == "0") {
            strTip += unitname;
        }
        else {
            //全院部门多选框的状态0代表全选，1代表半选，2代表不选
            var allstatus = $.trim($("#ctl00_ContentPlaceHolder1_drpunit_t_l_1").attr("checkedstate"));
            if (allstatus == "0" || allstatus=="2") {
                strTip += "全院部门";
            }
            else {                
                var unitarr = drpText.split(',');
                for (var index in unitarr) {
                    if (unitarr[index] != "全院部门") {
                        strTip += "[" + $.trim(unitarr[index]) + "]";
                    }
                }
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_hid_time").val()=="1") {

          var  tempstartyear = $.trim($("#ctl00_ContentPlaceHolder1_txt_year1").val());
          var  tempendyear = $.trim($("#ctl00_ContentPlaceHolder1_txt_year2").val());
          if (tempstartyear == "" && tempendyear!="")
          {
              strTip += "合同时间小于等于" + tempendyear;
             
          }
          else if (tempstartyear != "" && tempendyear == "")
          {
             strTip += "合同时间大于等于" + tempstartyear;
          }
          else if (tempstartyear != "" && tempendyear != "")
          {
              strTip += "合同时间" + tempstartyear + "截止" + tempstartyear;
          }
        }
        else
        {        
            //年份
            if (drpYearValue == "-1") {
                strTip += "全部年份";
            }
            else {
                strTip += drpYearText + "年";
                //季度
                var strjidu = $.trim($("#ctl00_ContentPlaceHolder1_drpJidu").find("option:selected").text());
                //月
                var stryue = $.trim($("#ctl00_ContentPlaceHolder1_drpMonth").find("option:selected").text());
                if (strjidu.indexOf("全部") > -1 && stryue.indexOf("全部")==-1) {
                    strTip += stryue + "份";
                }
                else if (strjidu.indexOf("全部") == -1 && stryue.indexOf("全部") > -1) {
                    strTip += "第" + strjidu;
                }
            }
        }
    }
    else {

        var unit1 = "";
        var tempStr = "";
        var strYear = "";
        var tempYear = "";
        var year1 = "";
        var year2 = "";
        if (type == "0") {
            //var unitSpans = $("span[id=userSpan]", $("#divUnit0"));
            //if (unitSpans.length > 0) {
            //    for (var i = 0; i < unitSpans.length; i++) {
            //        tempStr += "" + $.trim($(unitSpans[i]).attr("unitname")) + ",";
            //    }
            //}
        } else if (type == "1") {
            //var unitSpans = $("span[id=userSpan]", $("#divUnit1"));
            //if (unitSpans.length > 0) {
            //    for (var i = 0; i < unitSpans.length; i++) {
            //        tempStr += "" + $.trim($(unitSpans[i]).attr("unitname")) + ",";
            //    }
            //}

            year1 = $("#ctl00_ContentPlaceHolder1_drp_year2_01").val();
            year2 = $("#ctl00_ContentPlaceHolder1_drp_year2_02").val();


        } else if (type == "2") {
            //var unitSpans = $("span[id=userSpan]", $("#divUnit2"));
            //if (unitSpans.length > 0) {
            //    for (var i = 0; i < unitSpans.length; i++) {
            //        tempStr += "" + $.trim($(unitSpans[i]).attr("unitname")) + ",";
            //    }
            //}

            year1 = $("#ctl00_ContentPlaceHolder1_drp_year3_01").val();
            year2 = $("#ctl00_ContentPlaceHolder1_drp_year3_02").val();


        } else if (type == "3" || type == "4" || type == "5" || type == "6") {
            //var unitSpans = $("span[id=userSpan]", $("#divUnit3"));
            //if (unitSpans.length > 0) {
            //    for (var i = 0; i < unitSpans.length; i++) {
            //        tempStr += "" + $.trim($(unitSpans[i]).attr("unitname")) + ",";
            //    }
            //}

            year1 = $("#ctl00_ContentPlaceHolder1_drp_year4_01").val();
            year2 = $("#ctl00_ContentPlaceHolder1_drp_year4_02").val();

        } else if (type == "5" || type == "6" || type == "7" || type == "8") {
            //var unitSpans = $("span[id=userSpan]", $("#divUnit4"));
            //if (unitSpans.length > 0) {
            //    for (var i = 0; i < unitSpans.length; i++) {
            //        tempStr += "" + $.trim($(unitSpans[i]).attr("unitname")) + ",";
            //    }
            //}

            year1 = $("#ctl00_ContentPlaceHolder1_drp_year5_01").val();
            year2 = $("#ctl00_ContentPlaceHolder1_drp_year5_02").val();

        }

        if (type != "0") {
            if (year1 != "-1" && year2 != "-1") {
                tempYear = year1 + "年至" + year2 + "年";
            } else if (year1 != "-1" && year2 == "-1") {
                tempYear = "大于" + year1 + "年";
            } else if (year1 == "-1" && year2 != "-1") {
                tempYear = "小于" + year2 + "年";
            } else {
                tempYear = "全部年份";
            }
        }
        else {
            //年份
            if (drpYearValue == "-1") {
                tempYear = "全部年份";
            }
            else {
                tempYear = drpYearText + "年";
            }
        }
        //部门权限
        if (hidPower == "0") {
            unit1 = unitname;
        }
        else {
            //if (tempStr != "") {
            //    if (tempStr != "-----全院部门-----,") {
            //        unit1 = tempStr.substring(0, tempStr.length - 1);
            //    } else {
            //        unit1 = "全院部门";
            //    }
            //}
            //全院部门多选框的状态0代表全选，1代表半选，2代表不选
            var allstatus = $.trim($("#ctl00_ContentPlaceHolder1_drpunit_t_l_1").attr("checkedstate"));
            if (allstatus == "0" || allstatus == "2") {
                unit1 = "全院部门";
            }
            else {
                var unitarr = drpText.split(',');
                for (var index in unitarr) {
                    if (unitarr[index] != "全院部门") {
                        unit1 += "[" + $.trim(unitarr[index]) + "]";
                    }
                }
            }
        }
        if (unit1 == "") {
            strTip += "全院部门";
        }
        else {
            strTip += unit1;
        }

        strTip += tempYear;

    }

    $("#spanYear").text(strTip);
    $("#spanType").text(drpTypeText);

    $("#ctl00_ContentPlaceHolder1_hiddenTitle").val("" + strTip + "合同综合统计按" + drpTypeText + "统计");

}
var bool = true;
//隐藏部门
function HideTableTr(obj) {
    var $tr = "";
    obj.each(function () {
        var tdname = $.trim($(this).find("td").eq(0).text());
        var flag = false;
        if (tdname != "全院总计") {

        }
        //把生产经营部放到最后             
        if (tdname == "生产经营部") {
            if ($(this).parent().children().length > 1) {
                $tr = $(this).clone();
                $(this).remove();
            }
        }

    });

    //if (bool) {
    obj.last().last().before($tr);
  //  }
}

//数字验证正则    
var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

//验证
function Verification1() {

    var msg = "";
    //合同值
    if ($("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount1").val())) {
            msg += "合同目标开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount2").val())) {
            msg += "合同目标结束值格式错误，请输入数字！</br>";
        }
    }

    //完成目标值
    if ($("#ctl00_ContentPlaceHolder1_txt_CprAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprAcount1").val())) {
            msg += "完成目标开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_CprAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprAcount2").val())) {
            msg += "完成目标结束值格式错误，请输入数字！</br>";
        }
    }

    //合同额比例
    if ($("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent1").val())) {
            msg += "合同额开始比例格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent2").val())) {
            msg += "合同额结束比例格式错误，请输入数字！</br>";
        }
    }

    //目标产值
    if ($("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge1").val())) {
            msg += "目标产值开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge2").val())) {
            msg += "目标产值结束值格式错误，请输入数字！</br>";
        }
    }
    //完成产值
    if ($("#ctl00_ContentPlaceHolder1_txt_CprCharge1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprCharge1").val())) {
            msg += "完成产值开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_CprCharge2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprCharge2").val())) {
            msg += "完成产值结束值格式错误，请输入数字！</br>";
        }
    }

    //完成产值比例
    if ($("#ctl00_ContentPlaceHolder1_txt_CprChargePercent1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprChargePercent1").val())) {
            msg += "完成产值比例开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_CprChargePercent2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CprChargePercent2").val())) {
            msg += "完成产值比例结束值格式错误，请输入数字！</br>";
        }
    }
    return msg;
}
//验证
function Verification2() {

    var msg = "";
    //已签合同数量
    if ($("#ctl00_ContentPlaceHolder1_txtCprCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtCprCount1").val())) {
            msg += "已签合同数量开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtCprCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtCprCount2").val())) {
            msg += "已签合同数量结束值格式错误，请输入数字！</br>";
        }
    }

    //签订合同额
    if ($("#ctl00_ContentPlaceHolder1_txtCprSignAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtCprSignAcount1").val())) {
            msg += "签订合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtCprSignAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtCprSignAcount2").val())) {
            msg += "签订合同额结束值格式错误，请输入数字！</br>";
        }
    }

    //建筑规模
    if ($("#ctl00_ContentPlaceHolder1_txtBulid1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtBulid1").val())) {
            msg += "建筑规模开始格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtBulid2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtBulid2").val())) {
            msg += "建筑规模结束格式错误，请输入数字！</br>";
        }
    }

    //收款金额
    if ($("#ctl00_ContentPlaceHolder1_txtChargeCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtChargeCount1").val())) {
            msg += "收款金额开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtChargeCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtChargeCount2").val())) {
            msg += "收款金额结束值格式错误，请输入数字！</br>";
        }
    }
    //审批通过合同数
    if ($("#ctl00_ContentPlaceHolder1_txtPassCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtPassCount1").val())) {
            msg += "审批通过合同数开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtPassCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtPassCount2").val())) {
            msg += "审批通过合同数结束值格式错误，请输入数字！</br>";
        }
    }

    //正在审批合同数
    if ($("#ctl00_ContentPlaceHolder1_txtPassingCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtPassingCount1").val())) {
            msg += "正在审批合同数开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtPassingCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtPassingCount2").val())) {
            msg += "正在审批合同数结束值格式错误，请输入数字！</br>";
        }
    }

    //未发起审批合同数
    if ($("#ctl00_ContentPlaceHolder1_txtNotPassingCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtNotPassingCount1").val())) {
            msg += "未发起审批合同数开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtNotPassingCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtNotPassingCount2").val())) {
            msg += "未发起审批合同数结束值格式错误，请输入数字！</br>";
        }
    }
    return msg;
}
//验证
function Verification3() {

    var msg = "";
    //(特级)数量
    if ($("#ctl00_ContentPlaceHolder1_txtTJCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTJCount1").val())) {
            msg += "(特级)数量开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtTJCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTJCount2").val())) {
            msg += "(特级)数量结束值格式错误，请输入数字！</br>";
        }
    }

    //(特级)合同额
    if ($("#ctl00_ContentPlaceHolder1_txtTJCprAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTJCprAcount1").val())) {
            msg += "(特级)合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtTJCprAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTJCprAcount2").val())) {
            msg += "(特级)合同额结束值格式错误，请输入数字！</br>";
        }
    }

    //(特级)收款额
    if ($("#ctl00_ContentPlaceHolder1_txtTJCprChargeAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTJCprChargeAcount1").val())) {
            msg += "(特级)收款额开始格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtTJCprChargeAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTJCprChargeAcount2").val())) {
            msg += "(特级)收款额结束格式错误，请输入数字！</br>";
        }
    }

    //(一级)数量
    if ($("#ctl00_ContentPlaceHolder1_txtYJCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtYJCount1").val())) {
            msg += "(一级)数量开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtYJCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtYJCount2").val())) {
            msg += "(一级)数量结束值格式错误，请输入数字！</br>";
        }
    }

    //(一级)合同额
    if ($("#ctl00_ContentPlaceHolder1_txtYJCprAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtYJCprAcount1").val())) {
            msg += "(一级)合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtYJCprAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtYJCprAcount2").val())) {
            msg += "(一级)合同额结束值格式错误，请输入数字！</br>";
        }
    }

    //(一级)收款额
    if ($("#ctl00_ContentPlaceHolder1_txtYJCprChargeAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtYJCprChargeAcount1").val())) {
            msg += "(一级)收款额开始格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtYJCprChargeAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtYJCprChargeAcount2").val())) {
            msg += "(一级)收款额结束格式错误，请输入数字！</br>";
        }
    }


    //(二级)数量
    if ($("#ctl00_ContentPlaceHolder1_txtEJCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtEJCount1").val())) {
            msg += "(二级)数量开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtEJCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtEJCount2").val())) {
            msg += "(二级)数量结束值格式错误，请输入数字！</br>";
        }
    }

    //(二级)合同额
    if ($("#ctl00_ContentPlaceHolder1_txtEJCprAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtEJCprAcount1").val())) {
            msg += "(二级)合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtEJCprAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtEJCprAcount2").val())) {
            msg += "(二级)合同额结束值格式错误，请输入数字！</br>";
        }
    }

    //(二级)收款额
    if ($("#ctl00_ContentPlaceHolder1_txtEJCprChargeAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtEJCprChargeAcount1").val())) {
            msg += "(二级)收款额开始格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtEJCprChargeAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtEJCprChargeAcount2").val())) {
            msg += "(二级)收款额结束格式错误，请输入数字！</br>";
        }
    }


    //(三级)数量
    if ($("#ctl00_ContentPlaceHolder1_txtSJCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtSJCount1").val())) {
            msg += "(三级)数量开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtSJCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtSJCount2").val())) {
            msg += "(三级)数量结束值格式错误，请输入数字！</br>";
        }
    }

    //(三级)合同额
    if ($("#ctl00_ContentPlaceHolder1_txtSJCprAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtSJCprAcount1").val())) {
            msg += "(三级)合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtSJCprAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtSJCprAcount2").val())) {
            msg += "(三级)合同额结束值格式错误，请输入数字！</br>";
        }
    }

    //(三级)收款额
    if ($("#ctl00_ContentPlaceHolder1_txtSJCprChargeAcount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtSJCprChargeAcount1").val())) {
            msg += "(三级)收款额开始格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtSJCprChargeAcount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtSJCprChargeAcount2").val())) {
            msg += "(三级)收款额结束格式错误，请输入数字！</br>";
        }
    }

    return msg;
}
//验证
function Verification4() {

    var msg = "";
    //民用建筑合同
    if ($("#ctl00_ContentPlaceHolder1_txtType1_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType1_01").val())) {
            msg += "民用建筑合同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType1_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType1_02").val())) {
            msg += "民用建筑合同结束值格式错误，请输入数字！</br>";
        }
    }

    //工业建筑合同
    if ($("#ctl00_ContentPlaceHolder1_txtType2_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType2_01").val())) {
            msg += "工业建筑合同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType2_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType2_02").val())) {
            msg += "工业建筑合同结束值格式错误，请输入数字！</br>";
        }
    }

    //工程勘察合同
    if ($("#ctl00_ContentPlaceHolder1_txtType3_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType3_01").val())) {
            msg += "工程勘察合同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType3_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType3_02").val())) {
            msg += "工程勘察合同结束值格式错误，请输入数字！</br>";
        }
    }

    //工程监理合同
    if ($("#ctl00_ContentPlaceHolder1_txtType4_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType4_01").val())) {
            msg += "工程监理合同同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType4_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType4_02").val())) {
            msg += "工程监理合同结束值格式错误，请输入数字！</br>";
        }
    }

    //工程施工合同
    if ($("#ctl00_ContentPlaceHolder1_txtType5_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType5_01").val())) {
            msg += "工程施工合同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType5_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType5_02").val())) {
            msg += "工程施工合同结束值格式错误，请输入数字！</br>";
        }
    }

    //工程咨询合同
    if ($("#ctl00_ContentPlaceHolder1_txtType6_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType6_01").val())) {
            msg += "工程咨询合同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType6_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType6_02").val())) {
            msg += "工程咨询合同结束值格式错误，请输入数字！</br>";
        }
    }

    //工程总承包合同
    if ($("#ctl00_ContentPlaceHolder1_txtType7_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType7_01").val())) {
            msg += "工程总承包合同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType7_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType7_02").val())) {
            msg += "工程总承包合同结束值格式错误，请输入数字！</br>";
        }
    }

    //工程代建合同
    if ($("#ctl00_ContentPlaceHolder1_txtType8_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType8_01").val())) {
            msg += "工程代建合同开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtType8_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtType8_02").val())) {
            msg += "工程代建合同结束值格式错误，请输入数字！</br>";
        }
    }
    return msg;
}
//验证
function Verification5() {

    var msg = "";
    //公建
    if ($("#ctl00_ContentPlaceHolder1_txtProperty1_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty1_01").val())) {
            msg += "公建开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty1_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty1_02").val())) {
            msg += "公建结束值格式错误，请输入数字！</br>";
        }
    }

    //房地产
    if ($("#ctl00_ContentPlaceHolder1_txtProperty2_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty2_01").val())) {
            msg += "房地产开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty2_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty2_02").val())) {
            msg += "房地产结束值格式错误，请输入数字！</br>";
        }
    }

    //市政
    if ($("#ctl00_ContentPlaceHolder1_txtProperty3_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty3_01").val())) {
            msg += "市政开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty3_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty3_02").val())) {
            msg += "市政结束值格式错误，请输入数字！</br>";
        }
    }

    //医院
    if ($("#ctl00_ContentPlaceHolder1_txtProperty4_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty4_01").val())) {
            msg += "医院开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty4_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty4_02").val())) {
            msg += "医院结束值格式错误，请输入数字！</br>";
        }
    }

    //电力
    if ($("#ctl00_ContentPlaceHolder1_txtProperty5_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty5_01").val())) {
            msg += "电力开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty5_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty5_02").val())) {
            msg += "电力结束值格式错误，请输入数字！</br>";
        }
    }


    //通信
    if ($("#ctl00_ContentPlaceHolder1_txtProperty6_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty6_01").val())) {
            msg += "通信开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty6_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty6_02").val())) {
            msg += "通信结束值格式错误，请输入数字！</br>";
        }
    }

    //银行
    if ($("#ctl00_ContentPlaceHolder1_txtProperty7_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty7_01").val())) {
            msg += "银行开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty7_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty7_02").val())) {
            msg += "银行结束值格式错误，请输入数字！</br>";
        }
    }

    //学校
    if ($("#ctl00_ContentPlaceHolder1_txtProperty8_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty8_01").val())) {
            msg += "学校开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty8_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty8_02").val())) {
            msg += "学校结束值格式错误，请输入数字！</br>";
        }
    }

    //涉外
    if ($("#ctl00_ContentPlaceHolder1_txtProperty9_01").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty9_01").val())) {
            msg += "涉外开始值格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txtProperty9_02").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtProperty9_02").val())) {
            msg += "涉外结束值格式错误，请输入数字！</br>";
        }
    }
    return msg;
}
//得到条件一
function getStrWhere1() {

    //合同目标值
    var cprTaretAcount1 = $("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount1").val();
    var cprTaretAcount2 = $("#ctl00_ContentPlaceHolder1_txt_CprTaretAcount2").val();

    //完成合同额

    var cprAcount1 = $("#ctl00_ContentPlaceHolder1_txt_CprAcount1").val();
    var cprAcount2 = $("#ctl00_ContentPlaceHolder1_txt_CprAcount2").val();

    //合同额完成比例

    var cprAcountPercent1 = $("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent1").val();
    var cprAcountPercent2 = $("#ctl00_ContentPlaceHolder1_txt_CprAcountPercent2").val();

    //目标产值
    var cprTarentCharge1 = $("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge1").val();
    var cprTarentCharge2 = $("#ctl00_ContentPlaceHolder1_txt_CprTarentCharge2").val();

    //完成产值
    var cprCharge1 = $("#ctl00_ContentPlaceHolder1_txt_CprCharge1").val();
    var cprCharge2 = $("#ctl00_ContentPlaceHolder1_txt_CprCharge2").val();

    //完成产值比例
    var cprChargePercent1 = $("#ctl00_ContentPlaceHolder1_txt_CprChargePercent1").val();
    var cprChargePercent2 = $("#ctl00_ContentPlaceHolder1_txt_CprChargePercent2").val();

    var param2 = {
        "cprTaretAcount1": cprTaretAcount1,
        "cprTaretAcount2": cprTaretAcount2,
        "cprAcount1": cprAcount1,
        "cprAcount2": cprAcount2,
        "cprAcountPercent1": cprAcountPercent1,
        "cprAcountPercent2": cprAcountPercent2,
        "cprTarentCharge1": cprTarentCharge1,
        "cprTarentCharge2": cprTarentCharge2,
        "cprCharge1": cprCharge1,
        "cprCharge2": cprCharge2,
        "cprChargePercent1": cprChargePercent1,
        "cprChargePercent2": cprChargePercent2

    }
    return param2;
}
//获取部门ID
var getCheckedUnitNode = function () {
    unitlist = "0";
    asTreeViewObjctl00_ContentPlaceHolder1_drpunit.traverseTreeNode(displayNodeFunc);
    $("#hidUnitListVal").val(unitlist);
}
//循环
var unitlist = "";
var displayNodeFunc = function (elem) {
    var checkState = elem.getAttribute("checkedState");
    var value = elem.getAttribute("treeNodeValue");
    //选中
    if (checkState == "0") {
        if (value != "0") {
            unitlist += "," + value;
        }
    }
}
//获取当前月的天数
function GetCurrDay(year,month)
{    
    var new_date = new Date(year, month, 1);                //取当年当月中的第一天         
    var date_count = (new Date(new_date.getTime() - 1000 * 60 * 60 * 24)).getDate();//获取当月的天数       
    return date_count;
   // //定义当月的天数；
  //  var days;

    //当月份为二月时，根据闰年还是非闰年判断天数
    //if (month == 2) {
    //    days = year % 4 == 0 ? 29 : 28;

    //}
    //else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
    //    //月份为：1,3,5,7,8,10,12 时，为大月.则天数为31；
    //    days = 31;
    //}
    //else {
    //    //其他月份，天数为：30.
    //    days = 30;

    //}
    //return days;
}