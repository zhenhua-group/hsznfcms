﻿$(document).ready(function () {

    CommonControl.SetFormWidth();

    //隔行变色
    // $("#GridView1 tr:even").css({ background: "White" });

    //阶段查看
    $(".chk_jd").live("click", function () {
        $("#fd_set").show();
        $("#fd_set2").hide();
        $("#fd_set3").hide();
        $("#fd_set > legend").text("项目:" + $(this).attr("prjname"));
        //加载阶段数据
        LoadJieDuanData($(this));
    });
    //专业查看
    $(".chk_spe").live('click', function () {
        $("#fd_set2").show();
        $("#fd_set3").hide();
        $("#fd_set2 > legend").text("设计阶段:" + $(this).attr("purname"));

        //加载专业
        LoadSpecialData($(this).attr("proid"), $(this).attr("rel"));
    });

    //加载人员
    $(".user_chk").live('click', function () {
        $("#fd_set3").show();
        $("#fd_set3 > legend").text("专业:" + $(this).attr("spename"));

        //加载人员
        LoadProUserData($(this).attr("proid"), $(this).attr("purid"), $(this).attr("rel"))
    });
});
//加载阶段分析表
function LoadJieDuanData(link) {
    var data = "flag=getjd&proid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/ProjectAnalysis.ashx",
        data: data,
        success: function (result) {
            if (result != null && result.ds != undefined) {


                var data = result.ds;
                if ($("#jd_data tr").length > 1) {
                    $("#jd_data tr:gt(0)").remove();
                }
                if (result != "0") {
                    $("#jd_nodata").hide();
                }

                $.each(data, function (i, n) {
                    var row = $("#jd_row").clone();
                    var oper = "<a href='###' purname='" + n.PurName + "' rel='" + n.Url + "' proid='" + link.attr("rel") + "' id='chk_id' class='chk_spe'>查看</a>";
                    row.find("#purname").text(n.PurName);
                    row.find("#purstart").text(n.EndTime);
                    row.find("#purreadystart").text(n.ReadySTime);
                    row.find("#purend").html(n.StartTime);
                    row.find("#purreadyend").html(n.ReadyETime);
                    row.find("#pur_oper").html(oper);
                    if (i % 2 == 0) {
                        row.attr("class", "tr_in");
                    }
                    //增加行
                    row.appendTo("#jd_data");
                });
            }
            data = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("没有数据！");
        }
    });
}
//加载专业的信息
function LoadSpecialData(proid, purid) {
    var data = "flag=getspe&proid=" + proid + "&purid=" + purid;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/ProjectAnalysis.ashx",
        data: data,
        success: function (result) {
            if (result != null && result.ds != undefined) {


                var data = result.ds;
                if ($("#spe_data tr").length > 1) {
                    $("#spe_data tr:gt(0)").remove();
                }
                if (result != "0") {
                    $("#spe_nodata").hide();
                }

                $.each(data, function (i, n) {
                    var row = $("#spe_row").clone();
                    var oper = "<a href='###' spename='" + n.speName + "' proid='" + proid + "' purid='" + purid + "' rel='" + n.Url + "' id='user_id' class='user_chk'>查看</a>";
                    row.find("#spename").text(n.speName);
                    row.find("#spestart").text(n.speStart);
                    row.find("#speend").text(n.speEnd);
                    row.find("#spe_oper").html(oper);
                    if (i % 2 == 0) {
                        row.attr("class", "tr_in");
                    }
                    //增加行
                    row.appendTo("#spe_data");
                });
            }
            data = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("没有数据！");
        }
    });
}
//加载人员信息
function LoadProUserData(proid, purid, classid) {
    var data = "flag=getuser&proid=" + proid + "&purid=" + purid + "&classid=" + classid;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/ProjectAnalysis.ashx",
        data: data,
        success: function (result) {
            if (result != null && result.ds != undefined) {


                var data = result.ds;
                if ($("#user_data tr").length > 1) {
                    $("#user_data tr:gt(0)").remove();
                }
                if (result != "0") {
                    $("#user_nodata").hide();
                }

                $.each(data, function (i, n) {
                    var row = $("#user_row").clone();
                    row.find("#username").text(n.userName);
                    row.find("#userstart").text(n.userStart);
                    row.find("#userend").text(n.userEnd);
                    if (i % 2 == 0) {
                        row.attr("class", "tr_in");
                    }
                    //增加行
                    row.appendTo("#user_data");
                });
            }
            data = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("没有数据！");
        }
    });
}