﻿$(function () {
    //逐年显示
    $("#YearByYearCheckbox").click(function () {
        $("#OneByOneShowResultDiv iframe").attr("src", "/Coperation/cpr_ChargeCount.aspx");
        $("#OneByOneShowResultDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 900,
            height: 500,
            resizable: false,
            buttons:
			{
			    "关闭": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
    });

    //查看合同详情
    $("a[id=previewConperation]").live("click", function () {
        var coperationSysNo = $(this).attr("coperationsysno");
        window.location.href = "/Coperation/cpr_ShowCopration.aspx?flag=cprlist&cprid=" + coperationSysNo;
    });
});

//PageSize
var pageSize = $("#HiddenPageSize").val();
var backgroundInvoke = TG.Web.LeadershipCockpit.CoperationCount;
//全局参数Array
var globalParametersArray = new Array();
function GetLength(item) {
    if (item.length > 13) {
        return item.substring(0, 13)
    }
    else {
        return item;
    }
}
//拼接Table
function CreateTable(coperatioinList, showType) {
    var tableString = "";
    tableString += "<div class=\"cls_data\"  style=\"height:90%;\">";
    tableString += "<table class=\"cls_content_head\">";
    tableString += "<tr>";
    tableString += "<td style=\"width: 300px;\" align=\"center\">";
    tableString += "合同名称";
    tableString += "</td>";
    tableString += "<td style=\"width: 100px;\" align=\"center\">";
    tableString += "合同额(万元)";
    tableString += "</td>";
    tableString += "<td style=\"width: 150px;\" align=\"center\">";
    tableString += "日期";
    tableString += "</td>";
    tableString += "<td style=\"width: 100px;\" align=\"center\">";
    tableString += "负责人";
    tableString += "</td>";

    var titleText = "";
    switch (showType) {
        case "0": titleText = "合同类型"; break;
        case "1": titleText = "合同状态"; break;
    }
    tableString += "<td style=\"width: 150px;%\" align=\"center\">";
    tableString += titleText;
    tableString += "</td>";

    tableString += "<td style=\"width: 50px;\" align=\"center\">";
    tableString += "查看";
    tableString += "</td>";
    tableString += "</tr>";
    tableString += "</table>";

    //拼接项目列表结果Table
    var resultTableString = "";
    resultTableString += "<table class=\"show_projectNumber\" id=\"resultTable\">";
    $.each(coperatioinList, function (index, coperation) {
        resultTableString += "<tr>";
        resultTableString += "<td style=\"width: 300px;\" align=\"left\" title=\"" + coperation.CoperationName + "\"> <img src=\"../Images/buttons/icon_cpr.png\" style=\"width:16px;height:16px;\">";
        resultTableString += coperation.CoperationName;
        resultTableString += "</td>";
        resultTableString += "<td style=\"width: 100px;\" align=\"center\">";
        resultTableString += coperation.CoperationAccount;
        resultTableString += "</td>";
        resultTableString += "<td style=\"width: 150px;\" align=\"center\">";
        resultTableString += coperation.RegisterTimeString;
        resultTableString += "</td>";
        resultTableString += "<td style=\"width: 100px;\" align=\"center\">";
        resultTableString += coperation.ChargeManJia;
        resultTableString += "</td>";

        var valueText = "";
        switch (showType) {
            case "0": valueText = coperation.CoperationType; break;
            case "1": valueText = coperation.ProjectStatusString; break;
        }
        resultTableString += "<td style=\"width: 150px;\" align=\"center\">";
        resultTableString += valueText;
        resultTableString += "</td>";

        resultTableString += "<td style=\"width: 50px;;\" align=\"center\">";
        resultTableString += "<a href=\"#\" id=\"previewConperation\" coperationsysno=\"" + coperation.SysNo + "\">查看</a>";
        resultTableString += "</td>";
        resultTableString += "</tr>";
    });
    resultTableString += "</table>";
    tableString += resultTableString;
    tableString += "</div>";
    tableString += "<div id=\"pager\" style=\"height:5%;\"></div>";
    return tableString;
}

//根据年份得到合同列表
function GetCoperationListByYear(year) {
    //全局数组里面保存临时数据，以便后面使用
    globalParametersArray[0] = year;

    var coperationListString = backgroundInvoke.GetCoperationListByYear(year, 1).value;

    if (coperationListString != null && coperationListString.length > 0) {
        var coperationListArray = Global.evalJSON(coperationListString);
        //createTable第二个参数 0 表示合同类型
        var tableString = CreateTable(coperationListArray[1], "0");

        //填充弹出区域
        $("#PopAreaCopertionList").html(tableString);

        CommonControl.SetTableStyle("resultTable", "need");
        //弹出层显示
        PopAreaCoperationList(year + "年合同列表");

        var currentPage = 1;
        if (coperationListArray[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }
        //计算页数
        var pageCount = 0;
        if (coperationListArray[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (coperationListArray[0] % pageSize == 0) {
                pageCount = coperationListArray[0] / pageSize;
            } else {
                pageCount = Math.floor(coperationListArray[0] / pageSize) + 1;
            }
        }
        //初始化Pager控件
        $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": coperationListArray[0], "buttonClickCallback": GetCoperationListByYearCallBack });
    }
}

//根据年份得到合同列表回调函数
function GetCoperationListByYearCallBack(clickNumber) {
    //从全局数组里取得保存的临时数据
    var result = backgroundInvoke.GetCoperationListByYear(globalParametersArray[0], clickNumber);
    if (result.value != null && result.value.length > 0) {
        var resultArray = Global.evalJSON(result.value);
        $("#PopAreaCopertionList").html(CreateTable(resultArray[1], "0"));
        CommonControl.SetTableStyle("resultTable", "need");
        var pageCount = 0;
        if (resultArray[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (resultArray[0] % pageSize == 0) {
                pageCount = resultArray[0] / pageSize;
            } else {
                pageCount = Math.floor(resultArray[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": pageSize, "datacount": resultArray[0], "buttonClickCallback": GetCoperationListByYearCallBack });
    }
}

//根据年份和合同类型得到合同列表
function GetCoperationListByTypeOfYear(year, type) {
    globalParametersArray[0] = year;
    globalParametersArray[1] = type;

    var coperationListString = backgroundInvoke.GetCoperationListByTypeOfYear(year, type, 1).value;

    if (coperationListString != null && coperationListString.length > 0) {
        var coperationListArray = Global.evalJSON(coperationListString);

        var tableString = CreateTable(coperationListArray[1], "0");

        $("#PopAreaCopertionList").html(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaCoperationList(year + "年合同列表");

        var currentPage = 1;
        if (coperationListArray[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (coperationListArray[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (coperationListArray[0] % pageSize == 0) {
                pageCount = coperationListArray[0] / pageSize;
            } else {
                pageCount = Math.floor(coperationListArray[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": coperationListArray[0], "buttonClickCallback": GetCoperationListByTypeOfYearCallBack });
    }
}
//根据年份和合同类型得到合同列表回调函数
function GetCoperationListByTypeOfYearCallBack(clickNumber) {
    var result = backgroundInvoke.GetCoperationListByTypeOfYear(globalParametersArray[0], globalParametersArray[1], clickNumber);
    if (result.value != null && result.value.length > 0) {
        var resultArray = Global.evalJSON(result.value);
        $("#PopAreaCopertionList").html(CreateTable(resultArray[1], "0"));
        CommonControl.SetTableStyle("resultTable", "need");
        var pageCount = 0;
        if (resultArray[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (resultArray[0] % pageSize == 0) {
                pageCount = resultArray[0] / pageSize;
            } else {
                pageCount = Math.floor(resultArray[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": pageSize, "datacount": resultArray[0], "buttonClickCallback": GetCoperationListByTypeOfYearCallBack });
    }
}

//根据合同关联项目进行状态。得到合同列表
function GetCoperationListByStatusOfYear(year, status) {
    globalParametersArray[0] = year;
    globalParametersArray[1] = status;

    var coperationListString = backgroundInvoke.GetCoperationListByStatusOfYear(year, status, 1).value;

    if (coperationListString != null && coperationListString.length > 0) {
        var coperationListArray = Global.evalJSON(coperationListString);

        var tableString = CreateTable(coperationListArray[1], "1");

        $("#PopAreaCopertionList").html(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaCoperationList(year + "年合同列表");

        var currentPage = 1;
        if (coperationListArray[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (coperationListArray[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (coperationListArray[0] % pageSize == 0) {
                pageCount = coperationListArray[0] / pageSize;
            } else {
                pageCount = Math.floor(coperationListArray[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": coperationListArray[0], "buttonClickCallback": GetCoperationListByTypeOfYearCallBack });
    }
}

//根据合同关联项目进行状态。得到合同列表回调函数
function GetCoperationListByStatusOfYearCallBack(clickNumber) {
    var result = backgroundInvoke.GetCoperationListByStatusOfYear(globalParametersArray[0], globalParametersArray[1], clickNumber);
    if (result.value != null && result.value.length > 0) {
        var resultArray = Global.evalJSON(result.value);
        $("#PopAreaCopertionList").html(CreateTable(resultArray[1], "1"));
        CommonControl.SetTableStyle("resultTable", "need");

        var pageCount = 0;
        if (resultArray[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (resultArray[0] % pageSize == 0) {
                pageCount = resultArray[0] / pageSize;
            } else {
                pageCount = Math.floor(resultArray[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": pageSize, "datacount": resultArray[0], "buttonClickCallback": GetCoperationListByStatusOfYearCallBack });
    }
}

//弹出层显示内容
function PopAreaCoperationList(title) {
    $("#PopAreaCopertionList").dialog({
        autoOpen: false,
        modal: true,
        width: 900,
        height: 500,
        resizable: false,
        title: title,
        buttons:
			{
			    "关闭": function () { $(this).dialog("close"); }
			}
    }).dialog("open");
}

