﻿$(document).ready(function () {

    //初始化
    $("#ctl00_ContentPlaceHolder1_mytab").append($("#ctl00_ContentPlaceHolder1_mytabhtml").val());

    var tdnumber1 = $("#ctl00_ContentPlaceHolder1_mytab > tbody > tr:eq(0) > td ").length;
    var widthsum1 = (tdnumber1 * 160);

    $("#ctl00_ContentPlaceHolder1_mytab").css({ width: widthsum1 });

    //单位
    var op = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    //年
    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();

    if (op == "-1") {
        $("#unit").html("");
    }
    else {
        $("#unit").html($.trim($("#ctl00_ContentPlaceHolder1_drp_unit option:selected").text()));
    }
    if (year == "-1") {
        $("#title").html("");
    }
    else {
        $("#title").html(year + "年");
    }

    //统计
    $("#btn_ok").click(function () {


        //判断
        if ($("#ctl00_ContentPlaceHolder1_drp_unit").get(0).selectedIndex == 0) {
            alert('请选择要统计的部门！');
            return false;
        }
        $("#ctl00_ContentPlaceHolder1_mytab tr").remove();
        //单位
        var op = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        //年
        var yearall = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        if (op == "-1") {
            $("#unit").html("");
        } else {
            $("#unit").html($.trim($("#ctl00_ContentPlaceHolder1_drp_unit option:selected").text()));
        }

        if (year == "-1") {
            $("#title").html("");
        }
        else {
            $("#title").html(year + "年");
        }

        var pp = $("#ctl00_ContentPlaceHolder1_pp").val();
        var memid = $("#ctl00_ContentPlaceHolder1_memid").val();
        var UserUnitNo = $("#ctl00_ContentPlaceHolder1_unitid").val();

        $.get("../HttpHandler/ProjectValueAllot.ashx", { "action": "1", "year": year, "unitid": op, "pp": pp, "memid": memid, "UserUnitNo": UserUnitNo, "n": Math.random() }, function (data) {

            if (data == "") {
                var $tr = $("<tr></tr>");
                var $td1 = $("<td  style='color:red;'>没有数据！</td>");
                $tr.append($td1);
                $("#mytab").append($tr);
            }
            else {
                $("#ctl00_ContentPlaceHolder1_mytab").append(data);
            }
            var tdnumber = $("#ctl00_ContentPlaceHolder1_mytab > tbody > tr:eq(0) > td ").length;
            var widthsum = ((tdnumber-1) * 120)+100;

            $("#ctl00_ContentPlaceHolder1_mytab").css({ width: widthsum });

        });
    });

    //导出
    $("#btn_report").click(function () {

        //单位
        var op = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        //年    
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var pp = $("#ctl00_ContentPlaceHolder1_pp").val();
        var memid = $("#ctl00_ContentPlaceHolder1_memid").val();
        var UserUnitNo = $("#ctl00_ContentPlaceHolder1_unitid").val();
        if (op == "-1") {
            alert("请选择生产部门！");
            return false;
        }
        window.location.href = "ExcelValueAllot.aspx?year=" + year + "&unitid=" + op + "&pp=" + pp + "&memid=" + memid + "&UserUnitNo=" + UserUnitNo;
    });

});

