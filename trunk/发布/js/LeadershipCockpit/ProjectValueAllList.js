﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //隔行变色
    $("#gv_project tr:even").css({ background: "white" });
    //全选
    $("#chk_All").click(function () {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });
    //单选
    $(".chk_id").each(function (i) {
        $(this).click(function () {
            if ($(this).get(0).checked) {
                $(this).attr("checked", "true");
            }
            else {
                $(this).attr("checked", "");
            }
        });
    });

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });
    //无合同显示
    $("#gv_project a[cprid=0]").each(function () {
        $(this).attr("href", "###").text("无合同");
    });

    $("#txt_keyname").keydown(function () {
        //输入关键字名称提示下来框
        var paramEntity = {};
        paramEntity.action = "Project";
        paramEntity.previewPower = $("#previewPower").val();
        paramEntity.userSysNum = $("#userSysNum").val();
        paramEntity.userUnitNum = $("#userUnitNum").val();
        paramEntity.unitID = $("#drp_unit").val();
        paramEntity.currYear = $("#drp_year").val();
        var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
        autoComplete.GetAutoAJAX();
    });
   

    //提示
    $(".cls_column").tipsy({ opacity: 1 });

    $("#btn_Search").click(function () {
        //判断
        if ($("#drp_unit").get(0).selectedIndex == 0) {
            alert('请选择要统计的部门！');
            return false;
        }

    });
});
