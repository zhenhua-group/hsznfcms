﻿$(document).ready(function () {

    $("#ctl00_ContentPlaceHolder1_div_Container1").show();
    $("#ctl00_ContentPlaceHolder1_div_Container1").siblings().hide();
    $("#tbl_search").hide();

    //初始化
    InitData();
    //基础查询部门选择
    $("#btn_cx").click(function () {
        //if ($("#ctl00_ContentPlaceHolder1_drp_unit").get(0).selectedIndex == 0) {
        //    if ($("#hiddenischeckallpower").val() == "0") {
        //        alert('没有查看全院部门权限，请选择本部门查看！');
        //        return false;
        //    }
        //}
        //查询按钮值更改


        $("#ctl00_ContentPlaceHolder1_SelectType").val("0");
        InitData();

    });
    //基础查询年份选择
    $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {

        InitData();

    });
    //基础查询统计类型选择
    $("#ctl00_ContentPlaceHolder1_drp_type").change(function () {
        if ($(this).val() == "0") {

            ShowHideType("tbl_ckb1", "ctl00_ContentPlaceHolder1_div_Container1");

        }
        else if ($(this).val() == "1") {

            ShowHideType("tbl_ckb2", "ctl00_ContentPlaceHolder1_div_Container2");

        }
        else if ($(this).val() == "2" || $(this).val() == "3") {

            ShowHideType("tbl_ckb3", "ctl00_ContentPlaceHolder1_div_Container3");


            if ($(this).val() == "2") {
                $("span[rel=CompanyTitle]", "#tbl_con3").each(function () {
                    $(this).text("个");
                });
            }
            else {
                $("span[rel=CompanyTitle]", "#tbl_con3").each(function () {
                    $(this).text("㎡");
                });
            }

        }
        else if ($(this).val() == "4") {

            ShowHideType("tbl_ckb4", "ctl00_ContentPlaceHolder1_div_Container4");
        }

        //绑定数据
        InitData();
    });

    //点击生产部分选择条件
    $("input[rel=btn_chooes]").click(function () {
        //显示部门字符串
        var labunit = $(this).parent().siblings("div[rel=labUnit]");

        var labtext = labunit.text();
        //选择生产部门的id
        var drpunit = $(this).parent().siblings().children("select[rel=unit]");
        var chooesUnit = drpunit.find("option:selected").text();
        var chooesUnitVal = drpunit.find("option:selected").val();
        var userSpans = $("span[id=userSpan]", labunit);
        //循环判断是否有重复用户的存在
        for (var i = 0; i < userSpans.length; i++) {
            if ($(userSpans[i]).attr("usersysno") == chooesUnitVal) {
                return false;
            }
            if ($(userSpans[i]).attr("usersysno") == "-1") {
                labunit.empty();
            }
        }

        if (chooesUnitVal == "-1") {
            labunit.empty();
            labunit.append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">全院部门<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        } else {
            labunit.append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">" + chooesUnit + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        }
        var unit = "";
        var unitID = "";
        var unitSpans = $("span[id=userSpan]", labunit);
        for (var i = 0; i < unitSpans.length; i++) {
            unit += "" + $.trim($(unitSpans[i]).attr("unitname")) + ",";
            unitID += "" + $.trim($(unitSpans[i]).attr("usersysno")) + ",";
        }
        if (unit != "") {
            unit = unit.substring(0, (unit.length - 1));
        }
        if (unitID != "") {
            unitID = unitID.substring(0, (unitID.length - 1));
        }
        $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val(unit);
        $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val(unitID);
    });
    //删除生产部门
    $("img[id=deleteUserlinkButton]").live("click", function () {
        if (confirm("确认要删除这个部门吗？")) {
            var labunit = $(this).parent().parent();
            //删除用户
            $(this).parent("span[id=userSpan]:first").remove();
            var unit = "";
            var unitID = "";
            var unitSpans = $("span[id=userSpan]", labunit);
            for (var i = 0; i < unitSpans.length; i++) {
                unit += "'" + $(unitSpans[i]).attr("unitname") + "',";
                unitID += "" + $(unitSpans[i]).attr("usersysno") + ",";
            }
            if (unit != "") {
                unit = unit.substring(0, (unit.length - 1));
            }
            if (unitID != "") {
                unitID = unitID.substring(0, (unitID.length - 1));
            }
            $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val(unit);
            $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val(unitID);
        }
        return false;
    });
    //点击checkbox事件
    $("input[name=bb]:checkbox", $("#ckb")).click(function () {

        //条件名称
        var commName = $.trim($(this).get(0).id);
        //找到table
        var tblckb = $(this).parent().parent().parent().parent().parent().parent();
        var tblcon = $("#" + tblckb.attr("rel"));
        if ($(this).is(":checked")) {

            //显示table
            tblcon.show();
            $("#con table tr[for=" + commName + "]").show();
        }
        else {

            $("#con table tr[for=" + commName + "]").hide();
            $(":text", "#con table tr[for=" + commName + "]").val("");
            $("select", "#con table tr[for=" + commName + "]").val("-1");
            if (commName == "project_Unit" || commName == "project_Unit2" || commName == "project_Unit3" || commName == "project_Unit4") {
                $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val("");
                $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val("");
            }


        }
        //隐藏table
        if (tblckb.find(":checked").length > 0) {
            $("#tbl_search").show();
        }
        else {
            $("#tbl_search").hide();
            tblcon.hide();
            InitData();
        }
    });

    //查询
    $("#btn_Search").click(function () {
        //查询按钮值更改
        $("#ctl00_ContentPlaceHolder1_SelectType").val("1");

        var drptype = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").val());

        //获取部门id 
        getCheckedUnitNode();
        //获取部门名称
        var unit = GetUnitNameNode();
        $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val(unit);

        //项目审批
        if (drptype == "0") {
            ProjectAudit();

        }
            //项目建筑等级
        else if (drptype == "1") {
            ProjectLevel();

        }
            //项目建筑性质(数量和规模)
        else if (drptype == "2" || drptype == "3") {
            ProjectIndustry();

        }
            //项目来源
        else if (drptype == "4") {
            ProjectSrc();
        }
    });

});
//显示隐藏项目分类模块
function ShowHideType(chkid, divid) {
    //高级查询字段
    $("#" + chkid).show();
    $("#" + chkid).siblings().hide();
    //显示数据
    $("#" + divid).show();
    $("#" + divid).siblings().hide();
}
//得到树选中的部门名称
function GetUnitNameNode() {
    var unitnamestr = $("#ctl00_ContentPlaceHolder1_drpunit_divDropdownTreeText").text();
    var unitstr = "";
    if ($.trim(unitnamestr) != "") {
        var unitnamelist = unitnamestr.split(",");
        for (var name in unitnamelist) {
            if ($.trim(unitnamelist[name]) != "全院部门") {
                unitstr += "'" + $.trim(unitnamelist[name]) + "',";
            }
        }
        if ($.trim(unitstr) != "") {
            unitstr = unitstr.substr(0, (unitstr.length - 1));
        }
    }
    return unitstr;
}
//获取部门ID
var getCheckedUnitNode = function () {
    unitlist = "0";
    asTreeViewObjctl00_ContentPlaceHolder1_drpunit.traverseTreeNode(displayNodeFunc);
    $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val(unitlist);
}
//循环
var unitlist = "";
var displayNodeFunc = function (elem) {
    var checkState = elem.getAttribute("checkedState");
    var value = elem.getAttribute("treeNodeValue");
    //选中
    if (checkState == "0") {
        if (value != "0") {
            unitlist += "," + value;
        }
    }
}
//初始化数据
function InitData() {

    //清空
    Clear();
    //树选中的部门
    var drpunitname = GetUnitNameNode();
    var drpunit = ""; //已经没有意义
    var titlename = "";
    //  var drpunitname = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    var drpyear = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //季度
    var drpjd = $("#ctl00_ContentPlaceHolder1_drpJidu").val();
    //月份
    var drpmth = $("#ctl00_ContentPlaceHolder1_drpMonth").val();
    //年份1
    var txt_year1 = $("#ctl00_ContentPlaceHolder1_txt_year1").val();
    //年份2
    var txt_year2 = $("#ctl00_ContentPlaceHolder1_txt_year2").val();
    //check
    var ischeck = "0";
    if ($("#ctl00_ContentPlaceHolder1_cbx_time").is(":checked")) {
        ischeck = "1";
    }
    var drptype = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").val());
    var ispower = $("#hiddenischeckallpower").val();
    var hidunitid = $("#hiddenunitid").val();
    var hiduserid = $("#hiddenuserid").val();
    var hidunitname = $("#hiddenunitName").val();

    //本部门
    if (ispower == "0") {
        drpunitname = "'" + hidunitname + "'";
        titlename = hidunitname;
    }
    else {
        if (drpunitname != "") {
            var checkedstate = $("#ctl00_ContentPlaceHolder1_drpunit_ulASTreeView li").attr("checkedstate");
            //1代表半选，0代表全选，2代表不选
            if ($.trim(checkedstate) == "0") {
                titlename = "全院";
            }
            else {
                var regstr = new RegExp("'", "g");
                titlename = drpunitname.replace(regstr, "");
            }

        }
    }

    var Param = {
        "drptype": drptype,
        "drpjd": drpjd,
        "drpmth": drpmth,
        "txt_year1": txt_year1,
        "txt_year2": txt_year2,
        "ischeck": ischeck,
        "unitnamelist": drpunitname,
        "unitidlist": drpunit,
        "drpyear": drpyear,
        "n": Math.random()
    };

    //标题
    gettitle(titlename, drpyear, drpjd, drpmth, txt_year1, txt_year2, ischeck, true);

    //项目审批
    if (drptype == "0") {
        //绑定数据
        ProjectAuditBindData(Param);

    }
        //项目建筑等级
    else if (drptype == "1") {

        //绑定数据
        ProjectLevelBindData(Param);
    }
        //项目建筑性质(数量和规模)
    else if (drptype == "2" || drptype == "3") {
        //绑定数据
        ProjectIndustryBindData(Param);
    }
        //项目来源
    else if (drptype == "4") {
        //绑定数据
        ProjectSrcBindData(Param);
    }

}
//数字验证正则    
var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
//项目审批
function ProjectAudit() {

    var msg = "";
    //项目总数
    var projectcount1 = $("#ctl00_ContentPlaceHolder1_project_count1").val();
    var projectcount2 = $("#ctl00_ContentPlaceHolder1_project_count2").val()
    if (projectcount1 != "") {
        if (!reg.test(projectcount1)) {
            msg += "项目总数开始值格式错误，请输入数字！</br>";
        }
    }
    if (projectcount2 != "") {
        if (!reg.test(projectcount2)) {
            msg += "项目总数结束值格式错误，请输入数字！</br>";
        }
    }

    //项目规模
    var projectarea1 = $("#ctl00_ContentPlaceHolder1_project_area1").val();
    var projectarea2 = $("#ctl00_ContentPlaceHolder1_project_area2").val();
    if (projectarea1 != "") {
        if (!reg.test(projectarea1)) {
            msg += "项目规模开始值格式错误，请输入数字！</br>";
        }
    }
    if (projectarea2 != "") {
        if (!reg.test(projectarea2)) {
            msg += "项目规模结束值格式错误，请输入数字！</br>";
        }
    }

    //立项通过数
    var projectauditcount1 = $("#ctl00_ContentPlaceHolder1_project_auditcount1").val();
    var projectauditcount2 = $("#ctl00_ContentPlaceHolder1_project_auditcount2").val();
    if (projectauditcount1 != "") {
        if (!reg.test(projectauditcount1)) {
            msg += "立项通过数开始值格式错误，请输入数字！</br>";
        }
    }
    if (projectauditcount2 != "") {
        if (!reg.test(projectauditcount2)) {
            msg += "立项通过数结束值格式错误，请输入数字！</br>";
        }
    }
    //进行中项目
    var projectingcount1 = $("#ctl00_ContentPlaceHolder1_project_ingcount1").val();
    var projectingcount2 = $("#ctl00_ContentPlaceHolder1_project_ingcount2").val();
    if (projectingcount1 != "") {
        if (!reg.test(projectingcount1)) {
            msg += "进行中项目开始值格式错误，请输入数字！</br>";
        }
    }
    if (projectingcount2 != "") {
        if (!reg.test(projectingcount2)) {
            msg += "进行中项目结束值格式错误，请输入数字！</br>";
        }
    }
    //暂停项目
    var projectstopcount1 = $("#ctl00_ContentPlaceHolder1_project_stopcount1").val();
    var projectstopcount2 = $("#ctl00_ContentPlaceHolder1_project_stopcount2").val();
    if (projectstopcount1 != "") {
        if (!reg.test(projectstopcount1)) {
            msg += "暂停项目开始值格式错误，请输入数字！</br>";
        }
    }
    if (projectstopcount2 != "") {
        if (!reg.test(projectstopcount2)) {
            msg += "暂停项目结束值格式错误，请输入数字！</br>";
        }
    }
    //完成项目
    var projectfinishcount1 = $("#ctl00_ContentPlaceHolder1_project_finishcount1").val();
    var projectfinishcount2 = $("#ctl00_ContentPlaceHolder1_project_finishcount2").val();
    if (projectfinishcount1 != "") {
        if (!reg.test(projectfinishcount1)) {
            msg += "完成项目开始值格式错误，请输入数字！</br>";
        }
    }
    if (projectfinishcount2 != "") {
        if (!reg.test(projectfinishcount2)) {
            msg += "完成项目结束值格式错误，请输入数字！</br>";
        }
    }

    //年份
    var drpyear1 = $("#ctl00_ContentPlaceHolder1_drp_year1").val();
    var drpyear2 = $("#ctl00_ContentPlaceHolder1_drp_year2").val();

    //部门
    var unitnamelist = "";
    var unitidlist = "";
    //标题
    var titlename = "";
    //部门权限
    if ($("#hiddenischeckallpower").val() == "0") {
        unitidlist = $("#hiddenunitid").val();
        unitnamelist = $("#hiddenunitName").val();
        titlename = $("#hiddenunitName").val();
    }
    else {
        var unitnamelist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val();
        var unitidlist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val();
        if (unitnamelist != "") {
            var checkedstate = $("#ctl00_ContentPlaceHolder1_drpunit_ulASTreeView li").attr("checkedstate");
            //1代表半选，0代表全选，2代表不选
            if ($.trim(checkedstate) == "0") {
                titlename = "全院";
            }
            else {
                var regstr = new RegExp("'", "g");
                titlename = unitnamelist.replace(regstr, "");
            }

        }
    }



    if (msg != "") {
        jAlert(msg, "提示");
        return false;
    }
    var drpyear = "";
    if (drpyear1 == "-1" && drpyear2 != "-1") {
        drpyear = "小于" + drpyear2;
    }
    else if (drpyear2 == "-1" && drpyear1 != "-1") {
        drpyear = "大于" + drpyear1;
    }
    else if (drpyear2 != "-1" && drpyear1 != "-1") {
        drpyear = drpyear1 + "年至" + drpyear2;
    }
    //标题
    gettitle(titlename, drpyear,"","","","","", false);

    var Param = {
        "action": "sel",
        "drptype": $.trim($("#ctl00_ContentPlaceHolder1_drp_type").val()),
        "unitnamelist": unitnamelist,
        "unitidlist": unitidlist,
        "drpyear1": drpyear1,
        "drpyear2": drpyear2,
        "projectcount1": projectcount1,
        "projectcount2": projectcount2,
        "projectarea1": projectarea1,
        "projectarea2": projectarea2,
        "projectauditcount1": projectauditcount1,
        "projectauditcount2": projectauditcount2,
        "projectingcount1": projectingcount1,
        "projectingcount2": projectingcount2,
        "projectstopcount1": projectstopcount1,
        "projectstopcount2": projectstopcount2,
        "projectfinishcount1": projectfinishcount1,
        "projectfinishcount2": projectfinishcount2,
        "n": Math.random()
    };
    //绑定数据
    ProjectAuditBindData(Param);
}
//项目建筑等级
function ProjectLevel() {

    var msg = "";
    //特级数量
    var levelcount_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcount_1").val();
    var levelcount_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcount_2").val()
    if (levelcount_1 != "") {
        if (!reg.test(levelcount_1)) {
            msg += "特级数量开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcount_2 != "") {
        if (!reg.test(levelcount_2)) {
            msg += "特级数量结束值格式错误，请输入数字！</br>";
        }
    }

    //特级合同额
    var levelcpr_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr_1").val();
    var levelcpr_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr_2").val();
    if (levelcpr_1 != "") {
        if (!reg.test(levelcpr_1)) {
            msg += "特级合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcpr_2 != "") {
        if (!reg.test(levelcpr_2)) {
            msg += "特级合同额结束值格式错误，请输入数字！</br>";
        }
    }

    //一级数量
    var levelcount1_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcount1_1").val();
    var levelcount1_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcount1_2").val();
    if (levelcount1_1 != "") {
        if (!reg.test(levelcount1_1)) {
            msg += "一级数量开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcount1_2 != "") {
        if (!reg.test(levelcount1_2)) {
            msg += "一级数量结束值格式错误，请输入数字！</br>";
        }
    }
    //一级合同额
    var levelcpr1_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr1_1").val();
    var levelcpr1_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr1_1").val();
    if (levelcpr1_1 != "") {
        if (!reg.test(levelcpr1_1)) {
            msg += "一级合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcpr1_2 != "") {
        if (!reg.test(levelcpr1_2)) {
            msg += "一级合同额结束值格式错误，请输入数字！</br>";
        }
    }
    //二级数量
    var levelcount2_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcount2_1").val();
    var levelcount2_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcount2_2").val();
    if (levelcount2_1 != "") {
        if (!reg.test(levelcount2_1)) {
            msg += "二级数量开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcount2_2 != "") {
        if (!reg.test(levelcount2_2)) {
            msg += "二级数量结束值格式错误，请输入数字！</br>";
        }
    }
    //二级合同额
    var levelcpr2_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr2_1").val();
    var levelcpr2_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr2_2").val();
    if (levelcpr2_1 != "") {
        if (!reg.test(levelcpr2_1)) {
            msg += "二级合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcpr2_2 != "") {
        if (!reg.test(levelcpr2_2)) {
            msg += "二级合同额结束值格式错误，请输入数字！</br>";
        }
    }
    //三级数量
    var levelcount3_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcount3_1").val();
    var levelcount3_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcount3_2").val();
    if (levelcount3_1 != "") {
        if (!reg.test(levelcount3_1)) {
            msg += "三级数量开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcount3_2 != "") {
        if (!reg.test(levelcount3_2)) {
            msg += "三级数量结束值格式错误，请输入数字！</br>";
        }
    }
    //三级合同额
    var levelcpr3_1 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr3_1").val();
    var levelcpr3_2 = $("#ctl00_ContentPlaceHolder1_txt_levelcpr3_2").val();
    if (levelcpr3_1 != "") {
        if (!reg.test(levelcpr3_1)) {
            msg += "三级合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if (levelcpr3_2 != "") {
        if (!reg.test(levelcpr3_2)) {
            msg += "三级合同额结束值格式错误，请输入数字！</br>";
        }
    }

    //年份
    var drpyear1 = $("#ctl00_ContentPlaceHolder1_drp_year2_1").val();
    var drpyear2 = $("#ctl00_ContentPlaceHolder1_drp_year2_2").val();


    //部门
    var unitnamelist = "";
    var unitidlist = "";
    //标题
    var titlename = "";
    //部门权限
    if ($("#hiddenischeckallpower").val() == "0") {
        unitidlist = $("#hiddenunitid").val();
        unitnamelist = $("#hiddenunitName").val();
        titlename = $("#hiddenunitName").val();
    }
    else {
        var unitnamelist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val();
        var unitidlist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val();
        if (unitnamelist != "") {
            var checkedstate = $("#ctl00_ContentPlaceHolder1_drpunit_ulASTreeView li").attr("checkedstate");
            //1代表半选，0代表全选，2代表不选
            if ($.trim(checkedstate) == "0") {
                titlename = "全院";
            }
            else {
                var regstr = new RegExp("'", "g");
                titlename = unitnamelist.replace(regstr, "");
            }

        }
    }

    if (msg != "") {
        jAlert(msg, "提示");
        return false;
    }

    var drpyear = "";
    if (drpyear1 == "-1" && drpyear2 != "-1") {
        drpyear = "小于" + drpyear2;
    }
    else if (drpyear2 == "-1" && drpyear1 != "-1") {
        drpyear = "大于" + drpyear1;
    }
    else if (drpyear2 != "-1" && drpyear1 != "-1") {
        drpyear = drpyear1 + "年至" + drpyear2;
    }
    //标题
    gettitle(titlename, drpyear, "", "", "", "", "", false);

    var Param = {
        "action": "sel",
        "drptype": $.trim($("#ctl00_ContentPlaceHolder1_drp_type").val()),
        "unitnamelist": unitnamelist,
        "unitidlist": unitidlist,
        "drpyear1": drpyear1,
        "drpyear2": drpyear2,
        "levelcount_1": levelcount_1,
        "levelcount_2": levelcount_2,
        "levelcpr_1": levelcpr_1,
        "levelcpr_2": levelcpr_2,
        "levelcount1_1": levelcount1_1,
        "levelcount1_2": levelcount1_2,
        "levelcpr1_1": levelcpr1_1,
        "levelcpr1_2": levelcpr1_2,
        "levelcount2_1": levelcount2_1,
        "levelcount2_2": levelcount2_2,
        "levelcpr2_1": levelcpr2_1,
        "levelcpr2_2": levelcpr2_2,
        "levelcount3_1": levelcount3_1,
        "levelcount3_2": levelcount3_2,
        "levelcpr3_1": levelcpr3_1,
        "levelcpr3_2": levelcpr3_2,
        "n": Math.random()
    };
    //绑定数据
    ProjectLevelBindData(Param);
}
//项目建筑性质(数量和规模)
function ProjectIndustry() {

    var msg = "";
    //公建
    var txt_gj1 = $("#ctl00_ContentPlaceHolder1_txt_gj1").val();
    var txt_gj2 = $("#ctl00_ContentPlaceHolder1_txt_gj2").val()
    if (txt_gj1 != "") {
        if (!reg.test(txt_gj1)) {
            msg += "项目总数开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_gj2 != "") {
        if (!reg.test(txt_gj2)) {
            msg += "项目总数结束值格式错误，请输入数字！</br>";
        }
    }

    //房地产
    var txt_fdc1 = $("#ctl00_ContentPlaceHolder1_txt_fdc1").val();
    var txt_fdc2 = $("#ctl00_ContentPlaceHolder1_txt_fdc2").val();
    if (txt_fdc1 != "") {
        if (!reg.test(txt_fdc1)) {
            msg += "房地产开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_fdc2 != "") {
        if (!reg.test(txt_fdc2)) {
            msg += "房地产结束值格式错误，请输入数字！</br>";
        }
    }

    //市政
    var txt_sz1 = $("#ctl00_ContentPlaceHolder1_txt_sz1").val();
    var txt_sz2 = $("#ctl00_ContentPlaceHolder1_txt_sz2").val();
    if (txt_sz1 != "") {
        if (!reg.test(txt_sz1)) {
            msg += "市政开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_sz2 != "") {
        if (!reg.test(txt_sz2)) {
            msg += "市政结束值格式错误，请输入数字！</br>";
        }
    }
    //医院
    var txt_yy1 = $("#ctl00_ContentPlaceHolder1_txt_yy1").val();
    var txt_yy2 = $("#ctl00_ContentPlaceHolder1_txt_yy2").val();
    if (txt_yy1 != "") {
        if (!reg.test(txt_yy1)) {
            msg += "医院开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_yy2 != "") {
        if (!reg.test(txt_yy2)) {
            msg += "医院结束值格式错误，请输入数字！</br>";
        }
    }
    //电力
    var txt_dl1 = $("#ctl00_ContentPlaceHolder1_txt_dl1").val();
    var txt_dl2 = $("#ctl00_ContentPlaceHolder1_txt_dl1").val();
    if (txt_dl1 != "") {
        if (!reg.test(txt_dl1)) {
            msg += "电力开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_dl2 != "") {
        if (!reg.test(txt_dl2)) {
            msg += "电力结束值格式错误，请输入数字！</br>";
        }
    }
    //通信
    var txt_tx1 = $("#ctl00_ContentPlaceHolder1_txt_tx1").val();
    var txt_tx2 = $("#ctl00_ContentPlaceHolder1_txt_tx2").val();
    if (txt_tx1 != "") {
        if (!reg.test(txt_tx1)) {
            msg += "通信开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_tx2 != "") {
        if (!reg.test(txt_tx2)) {
            msg += "通信结束值格式错误，请输入数字！</br>";
        }
    }
    //银行
    var txt_yh1 = $("#ctl00_ContentPlaceHolder1_txt_yh1").val();
    var txt_yh2 = $("#ctl00_ContentPlaceHolder1_txt_yh2").val();
    if (txt_yh1 != "") {
        if (!reg.test(txt_yh1)) {
            msg += "银行开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_yh2 != "") {
        if (!reg.test(txt_yh2)) {
            msg += "银行结束值格式错误，请输入数字！</br>";
        }
    }
    //学校
    var txt_xx1 = $("#ctl00_ContentPlaceHolder1_txt_xx1").val();
    var txt_xx2 = $("#ctl00_ContentPlaceHolder1_txt_xx2").val();
    if (txt_tx1 != "") {
        if (!reg.test(txt_xx1)) {
            msg += "学校开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_xx2 != "") {
        if (!reg.test(txt_xx2)) {
            msg += "学校结束值格式错误，请输入数字！</br>";
        }
    }
    //涉外
    var txt_sw1 = $("#ctl00_ContentPlaceHolder1_txt_sw1").val();
    var txt_sw2 = $("#ctl00_ContentPlaceHolder1_txt_sw2").val();
    if (txt_sw1 != "") {
        if (!reg.test(txt_sw1)) {
            msg += "涉外开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_sw2 != "") {
        if (!reg.test(txt_sw2)) {
            msg += "涉外结束值格式错误，请输入数字！</br>";
        }
    }

    //年份
    var drpyear1 = $("#ctl00_ContentPlaceHolder1_drp_year3_1").val();
    var drpyear2 = $("#ctl00_ContentPlaceHolder1_drp_year3_2").val();

    //部门
    var unitnamelist = "";
    var unitidlist = "";
    //标题
    var titlename = "";
    //部门权限
    if ($("#hiddenischeckallpower").val() == "0") {
        unitidlist = $("#hiddenunitid").val();
        unitnamelist = $("#hiddenunitName").val();
        titlename = $("#hiddenunitName").val();
    }
    else {
        var unitnamelist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val();
        var unitidlist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val();
        if (unitnamelist != "") {
            var checkedstate = $("#ctl00_ContentPlaceHolder1_drpunit_ulASTreeView li").attr("checkedstate");
            //1代表半选，0代表全选，2代表不选
            if ($.trim(checkedstate) == "0") {
                titlename = "全院";
            }
            else {
                var regstr = new RegExp("'", "g");
                titlename = unitnamelist.replace(regstr, "");
            }

        }
    }

    if (msg != "") {
        jAlert(msg, "提示");
        return false;
    }

    var drpyear = "";
    if (drpyear1 == "-1" && drpyear2 != "-1") {
        drpyear = "小于" + drpyear2;
    }
    else if (drpyear2 == "-1" && drpyear1 != "-1") {
        drpyear = "大于" + drpyear1;
    }
    else if (drpyear2 != "-1" && drpyear1 != "-1") {
        drpyear = drpyear1 + "年至" + drpyear2;
    }
    //标题
    gettitle(titlename, drpyear, "", "", "", "", "", false);

    var Param = {
        "action": "sel",
        "drptype": $.trim($("#ctl00_ContentPlaceHolder1_drp_type").val()),
        "unitnamelist": unitnamelist,
        "unitidlist": unitidlist,
        "drpyear1": drpyear1,
        "drpyear2": drpyear2,
        "txt_gj1": txt_gj1,
        "txt_gj2": txt_gj2,
        "txt_fdc1": txt_fdc1,
        "txt_fdc2": txt_fdc2,
        "txt_sz1": txt_sz1,
        "txt_sz2": txt_sz2,
        "txt_yy1": txt_yy1,
        "txt_yy2": txt_yy2,
        "txt_dl1": txt_dl1,
        "txt_dl2": txt_dl2,
        "txt_tx1": txt_tx1,
        "txt_tx2": txt_tx2,
        "txt_yh1": txt_yh1,
        "txt_yh2": txt_yh2,
        "txt_xx1": txt_xx1,
        "txt_xx2": txt_xx2,
        "txt_sw1": txt_sw1,
        "txt_sw2": txt_sw2,
        "n": Math.random()
    };
    //绑定数据
    ProjectIndustryBindData(Param);
}
//项目来源
function ProjectSrc() {

    var msg = "";
    //公开招标数量
    var txt_gkzbcount1 = $("#ctl00_ContentPlaceHolder1_txt_gkzbcount1").val();
    var txt_gkzbcount2 = $("#ctl00_ContentPlaceHolder1_txt_gkzbcount2").val()
    if (txt_gkzbcount1 != "") {
        if (!reg.test(txt_gkzbcount1)) {
            msg += "公开招标数量开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_gkzbcount2 != "") {
        if (!reg.test(txt_gkzbcount2)) {
            msg += "公开招标数量结束值格式错误，请输入数字！</br>";
        }
    }

    //公开招标规模
    var txt_gkzbscale1 = $("#ctl00_ContentPlaceHolder1_txt_gkzbscale1").val();
    var txt_gkzbscale2 = $("#ctl00_ContentPlaceHolder1_txt_gkzbscale2").val();
    if (txt_gkzbscale1 != "") {
        if (!reg.test(txt_gkzbscale1)) {
            msg += "公开招标规模开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_gkzbscale2 != "") {
        if (!reg.test(txt_gkzbscale2)) {
            msg += "公开招标规模结束值格式错误，请输入数字！</br>";
        }
    }

    //邀请招标数量
    var txt_yqzbcount1 = $("#ctl00_ContentPlaceHolder1_txt_yqzbcount1").val();
    var txt_yqzbcount2 = $("#ctl00_ContentPlaceHolder1_txt_yqzbcount2").val();
    if (txt_yqzbcount1 != "") {
        if (!reg.test(txt_yqzbcount1)) {
            msg += "邀请招标数量开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_yqzbcount2 != "") {
        if (!reg.test(txt_yqzbcount2)) {
            msg += "邀请招标数量结束值格式错误，请输入数字！</br>";
        }
    }
    //邀请招标规模
    var txt_yqzbscale1 = $("#ctl00_ContentPlaceHolder1_txt_yqzbscale1").val();
    var txt_yqzbscale2 = $("#ctl00_ContentPlaceHolder1_txt_yqzbscale2").val();
    if (txt_yqzbscale1 != "") {
        if (!reg.test(txt_yqzbscale1)) {
            msg += "邀请招标规模开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_yqzbscale2 != "") {
        if (!reg.test(txt_yqzbscale2)) {
            msg += "邀请招标规模结束值格式错误，请输入数字！</br>";
        }
    }
    //自行委托数量
    var txt_zxwtcount1 = $("#ctl00_ContentPlaceHolder1_txt_zxwtcount1").val();
    var txt_zxwtcount2 = $("#ctl00_ContentPlaceHolder1_txt_zxwtcount2").val();
    if (txt_zxwtcount1 != "") {
        if (!reg.test(txt_zxwtcount1)) {
            msg += "自行委托数量开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_zxwtcount2 != "") {
        if (!reg.test(txt_zxwtcount2)) {
            msg += "自行委托数量结束值格式错误，请输入数字！</br>";
        }
    }
    //自行委托规模
    var txt_zxwtscale1 = $("#ctl00_ContentPlaceHolder1_txt_zxwtscale1").val();
    var txt_zxwtscale2 = $("#ctl00_ContentPlaceHolder1_txt_zxwtscale2").val();
    if (txt_zxwtscale1 != "") {
        if (!reg.test(txt_zxwtscale1)) {
            msg += "自行委托规模开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_zxwtscale2 != "") {
        if (!reg.test(txt_zxwtscale2)) {
            msg += "自行委托规模结束值格式错误，请输入数字！</br>";
        }
    }


    //年份
    var drpyear1 = $("#ctl00_ContentPlaceHolder1_drp_year4_1").val();
    var drpyear2 = $("#ctl00_ContentPlaceHolder1_drp_year4_2").val();

    //部门
    var unitnamelist = "";
    var unitidlist = "";
    //标题
    var titlename = "";
    //部门权限
    if ($("#hiddenischeckallpower").val() == "0") {
        unitidlist = $("#hiddenunitid").val();
        unitnamelist = $("#hiddenunitName").val();
        titlename = $("#hiddenunitName").val();
    }
    else {
        var unitnamelist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val();
        var unitidlist = $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val();
        if (unitnamelist != "") {
            var checkedstate = $("#ctl00_ContentPlaceHolder1_drpunit_ulASTreeView li").attr("checkedstate");
            //1代表半选，0代表全选，2代表不选
            if ($.trim(checkedstate) == "0") {
                titlename = "全院";
            }
            else {
                var regstr = new RegExp("'", "g");
                titlename = unitnamelist.replace(regstr, "");
            }

        }
    }

    if (msg != "") {
        jAlert(msg, "提示");
        return false;
    }

    var drpyear = "";
    if (drpyear1 == "-1" && drpyear2 != "-1") {
        drpyear = "小于" + drpyear2;
    }
    else if (drpyear2 == "-1" && drpyear1 != "-1") {
        drpyear = "大于" + drpyear1;
    }
    else if (drpyear2 != "-1" && drpyear1 != "-1") {
        drpyear = drpyear1 + "年至" + drpyear2;
    }
    //标题
    gettitle(titlename, drpyear, "", "", "", "", "", false);

    var Param = {
        "action": "sel",
        "drptype": $.trim($("#ctl00_ContentPlaceHolder1_drp_type").val()),
        "unitnamelist": unitnamelist,
        "unitidlist": unitidlist,
        "drpyear1": drpyear1,
        "drpyear2": drpyear2,
        "txt_gkzbcount1": txt_gkzbcount1,
        "txt_gkzbcount2": txt_gkzbcount2,
        "txt_gkzbscale1": txt_gkzbscale1,
        "txt_gkzbscale2": txt_gkzbscale2,
        "txt_yqzbcount1": txt_yqzbcount1,
        "txt_yqzbcount2": txt_yqzbcount2,
        "txt_yqzbscale1": txt_yqzbscale1,
        "txt_yqzbscale2": txt_yqzbscale2,
        "txt_zxwtcount1": txt_zxwtcount1,
        "txt_zxwtcount2": txt_zxwtcount2,
        "txt_zxwtscale1": txt_zxwtscale1,
        "txt_zxwtscale2": txt_zxwtscale2,
        "n": Math.random()
    };
    //绑定数据
    ProjectSrcBindData(Param);
}

//项目审批绑定数据
function ProjectAuditBindData(param) {
    var suma = 0, sumb = 0, sumc = 0, sumd = 0, sume = 0, sumf = 0;
    $.post("/HttpHandler/LeadershipCockpit/ProjectCountBymasterHandler.ashx", param, function (data) {
        $("#tbl_one tr:gt(0)").remove();
        if (data != null && data != "") {
            $.each(data, function (i, n) {
                var trString = "<tr><td>" + $.trim(n.Name) + "</td>";
                trString += "<td>" + n.A + "</td>";
                trString += "<td>" + n.B + "</td>";;
                trString += "<td>" + n.C + "</td>";
                trString += "<td>" + n.D + "</td>";
                trString += "<td>" + n.E + "</td>";
                trString += "<td>" + n.F + "</td>";
                trString += "</tr>";

                suma = suma + parseInt(n.A);
                sumb += parseFloat(n.B);
                sumc += parseInt(n.C);
                sumd += parseInt(n.D);
                sume += parseInt(n.E);
                sumf += parseInt(n.F);

                $("#tbl_one").append(trString);
            });

        }

        $("#tbl_one tr:last").css("font-weight", "bold");
        //隐藏不显示的部门
        HideTableTr($("#tbl_one tbody tr"));
        //合计
        //        var endtrString = "<tr style='font-weight:bold;'><td>合计</td>";
        //        endtrString += "<td>" + suma + "</td>";
        //        endtrString += "<td>" + sumb.toFixed("f2") + "</td>"; ;
        //        endtrString += "<td>" + sumc + "</td>";
        //        endtrString += "<td>" + sumd + "</td>";
        //        endtrString += "<td>" + sume + "</td>";
        //        endtrString += "<td>" + sumf + "</td>";
        //        endtrString += "</tr>";
        //        $("#tbl_one tr:last").before(endtrString);
    });
}
//项目等级绑定数据
function ProjectLevelBindData(param) {

    var suma = 0, suma1 = 0, sumb = 0, sumb1 = 0, sumc = 0, sumc1 = 0, sumd = 0, sumd1 = 0;
    $.post("/HttpHandler/LeadershipCockpit/ProjectCountBymasterHandler.ashx", param, function (data) {
        $("#tbl_two tr:gt(1)").remove();
        if (data != null && data != "") {

            $.each(data, function (i, n) {
                var trString = "<tr><td>" + $.trim(n.Name) + "</td>";
                trString += "<td>" + n.A + "</td>";
                trString += "<td>" + n.A1 + "</td>";;
                trString += "<td>" + n.B + "</td>";
                trString += "<td>" + n.B1 + "</td>";
                trString += "<td>" + n.C + "</td>";
                trString += "<td>" + n.C1 + "</td>";
                trString += "<td>" + n.D + "</td>";
                trString += "<td>" + n.D1 + "</td>";
                trString += "</tr>";

                //                suma = suma + parseInt(n.A);
                //                suma1 = suma1 + parseInt(n.A1);
                //                sumb += parseFloat(n.B);
                //                sumb1 += parseFloat(n.B1);
                //                sumc += parseInt(n.C);
                //                sumc1 += parseInt(n.C1);
                //                sumd += parseInt(n.D);
                //                sumd1 += parseInt(n.D1);               

                $("#tbl_two").append(trString);
            });

        }

        $("#tbl_two tr:last").css("font-weight", "bold");
        //隐藏不显示的部门
        HideTableTr($("#tbl_two tbody tr"));

    });
}
//项目建筑性质绑定数据
function ProjectIndustryBindData(param) {
    var suma = 0, sumb = 0, sumc = 0, sumd = 0, sume = 0, sumf = 0;
    $.post("/HttpHandler/LeadershipCockpit/ProjectCountBymasterHandler.ashx", param, function (data) {
        $("#tbl_three tr:gt(0)").remove();
        if (data != null && data != "") {
            $.each(data, function (i, n) {
                var trString = "<tr><td>" + $.trim(n.Name) + "</td>";
                trString += "<td>" + n.A + "</td>";
                trString += "<td>" + n.B + "</td>";;
                trString += "<td>" + n.C + "</td>";
                trString += "<td>" + n.D + "</td>";
                trString += "<td>" + n.E + "</td>";
                trString += "<td>" + n.F + "</td>";
                trString += "<td>" + n.G + "</td>";
                trString += "<td>" + n.H + "</td>";
                trString += "<td>" + n.I + "</td>";
                trString += "</tr>";

                $("#tbl_three").append(trString);
            });

        }

        $("#tbl_three tr:last").css("font-weight", "bold");
        //隐藏不显示的部门
        HideTableTr($("#tbl_three tbody tr"));
    });
}
//项目来源绑定数据
function ProjectSrcBindData(param) {
    var suma = 0, sumb = 0, sumc = 0, sumd = 0, sume = 0, sumf = 0;
    $.post("/HttpHandler/LeadershipCockpit/ProjectCountBymasterHandler.ashx", param, function (data) {
        $("#tbl_four tr:gt(1)").remove();
        if (data != null && data != "") {
            $.each(data, function (i, n) {
                var trString = "<tr><td>" + $.trim(n.Name) + "</td>";
                trString += "<td>" + n.A + "</td>";
                trString += "<td>" + n.A1 + "</td>";;
                trString += "<td>" + n.B + "</td>";
                trString += "<td>" + n.B1 + "</td>";
                trString += "<td>" + n.C + "</td>";
                trString += "<td>" + n.C1 + "</td>";
                trString += "</tr>";

                $("#tbl_four").append(trString);
            });

        }

        $("#tbl_four tr:last").css("font-weight", "bold");
        //隐藏不显示的部门
        HideTableTr($("#tbl_four tbody tr"));
    });
}
//标题    gettitle(titlename, drpyear,drpjd,drpmth, true);
function gettitle(unitname, yearname, drpjd, drpmth, txt_year1, txt_year2, ischeck, flage) {
    //    var unitname = $.trim($("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text());
    //    var yearname = $.trim($("#ctl00_ContentPlaceHolder1_drp_year").val());
    var typename = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());


    if (typename.indexOf("数量") > -1) {
        $("#ctl00_ContentPlaceHolder1_div_util3").text("单位:个");
    }
    else if (typename.indexOf("规模") > -1) {
        $("#ctl00_ContentPlaceHolder1_div_util3").text("单位:㎡");
    }
    else if (typename.indexOf("额") > -1) {
        $("#ctl00_ContentPlaceHolder1_div_util3").text("单位:万元");
    }


    if (unitname == "-1" || unitname.indexOf("全院部门") > -1 || unitname == "") {
        unitname = "全院";
    }
    else {
        unitname = unitname;
    }
    //年份
    if (flage) {
        if (ischeck == "1") {
            unitname += txt_year1 + "至" + txt_year2;
        } else {
            if (yearname == "-1" || yearname == "") {
                unitname += "全部年份";
            }
            else {
                if (drpjd != "0") {
                    unitname += yearname + "年" + $("#ctl00_ContentPlaceHolder1_drpJidu :selected").text();
                } if (drpmth != "0") {
                    unitname += yearname + "年" + $("#ctl00_ContentPlaceHolder1_drpMonth :selected").text();
                }
            }
        }
    }
     
    $("#ctl00_ContentPlaceHolder1_drpunitname").text(unitname);
    $("#ctl00_ContentPlaceHolder1_drptypename").text(typename);
    //  if (!flage) {
    $("#ctl00_ContentPlaceHolder1_titlecount").val(unitname);
    // }    

}
//清空数据
function Clear() {
    //隐藏查询按钮
    $("#tbl_search").hide();
    //隐藏查询条件
    $("#tbl_con1").hide();
    $("#tbl_con2").hide();
    $("#tbl_con3").hide();
    $("#tbl_con4").hide();

    $("#tbl_con1").children("tbody").children("tr").hide();
    $("#tbl_con2").children("tbody").children("tr").hide();
    $("#tbl_con3").children("tbody").children("tr").hide();
    $("#tbl_con4").children("tbody").children("tr").hide();

    //清空数据   
    $('input[type=checkbox]', $("#ckb")).each(function () {
        $(this).parent().attr("class", "");
    });

    $(":text", "#con").val("");
    $("select", "#con").val("-1");
    $("div[rel=labUnit]", "#con").text("");
    $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val("");
    $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val("");
}
//初始化显示当前人部门，隐藏其他部门
function HideTableTr(obj) {
    var $tr = "";
    obj.each(function () {
        var tdname = $.trim($(this).find("td").eq(0).text());
        var flag = false;
        //if (tdname != "全院总计") {
        //    $("#ctl00_ContentPlaceHolder1_drp_unit option").each(function () {

        //        if ($.trim($(this).text()) == tdname) {
        //            flag = true;
        //            return false;
        //        }
        //    });
        //    if (!flag) {
        //        $(this).css("display", "none");
        //    }
        //}
        //把生产经营部放到最后             
        if (tdname == "生产经营部") {
            $tr = $(this).clone();
            $(this).remove();
        }
    });

    obj.last().last().before($tr);

}