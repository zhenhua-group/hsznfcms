﻿$(function () {

    //隐藏查询条件
    $("#tbl_id2").children("tbody").children("tr").hide();
    //点击选择条件
    $("input[name=bb]:checkbox", $("#tbl_id")).click(function () {
        //条件名称
        var commName = $.trim($(this).get(0).id);
        //获取数据库字段名      
        var columnsname = $.trim($(this).parent().parent().siblings("span").attr("for"));
        //获取表头字段名
        var columnsChinaName = $.trim($(this).parent().parent().siblings("span").text());
        if ($(this).is(":checked")) {
            $("#tbl_id2 tr[for=" + commName + "]").show();
            //if (commName == "cpr_Unit") {
            //    $("#AuditUnit").modal();
            //}
        }
        else {
            //判断执行设总和甲方负责人 
            $("#tbl_id2 tr[for=" + commName + "]").hide();
            $("#tbl_id2 tr[for=" + commName + "]").removeData();
            $(":text", "#tbl_id2 tr[for=" + commName + "]").val("");
            $("select", "#tbl_id2 tr[for=" + commName + "]").val("-1");
            if (commName == "cpr_Unit") {
                $("#labUnit").empty();
            }
        }
        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tbl_id")).length > 0) {
            $("#tbl_id2 tr:last").show();
        }
        else {
            $("#tbl_id2 tr:last").hide();
            $(":text", "#tbl_id2 tr[for=" + commName + "]").val("");
            $("select", "#tbl_id2 tr[for=" + commName + "]").val("-1");
            if (commName == "cpr_Unit") {
                $("#labUnit").empty();
            }           
        }
    });
    //暂时没用开始
    $("#btn_chooes").click(function () {
        var labtext = $("#labUnit").text();
        var chooesUnit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var chooesUnitVal = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").val();
        var userSpans = $("span[id=userSpan]", $("#labUnit"));
        //循环判断是否有重复用户的存在
        for (var i = 0; i < userSpans.length; i++) {
            if ($(userSpans[i]).attr("usersysno") == chooesUnitVal) {
                return false;
            }
            if ($(userSpans[i]).attr("usersysno") == "-1") {
                $("#labUnit").empty();
            }
        }

        if (chooesUnitVal == "-1") {
            $("#labUnit").empty();
            $("#labUnit").append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">全院部门<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        } else {
            $("#labUnit").append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">" + chooesUnit + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        }
    });
    $("img[id=deleteUserlinkButton]").live("click", function () {
        if (confirm("确认要删除这个部门吗？")) {
            //删除用户
            $(this).parent("span[id=userSpan]:first").remove();
        }
        return false;
    });
    //暂时没用结束
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/ProjectChargeHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '合同编号', '合同名称', '合同额(万元)', '实收总额(万元)', '收款进度', '状态', '', '', '查看'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 120, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 350, formatter: colNameShowFormatter },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 100, align: 'center' },
                             { name: 'ssze', index: 'ssze', width: 100, align: 'center' },
                             { name: 'sfjd', index: 'sfjd', width: 100, align: 'center', formatter: colSfjdFormatter },
                              { name: 'sfjd', index: 'sfjd', width: 100, align: 'center', sorttable: false, editable: false, formatter: colImgFormatter },
                              { name: 'sfze', index: 'sfze', hidden: true, editable: true },
                              { name: 'htze', index: 'htze', hidden: true, editable: true },
                            { name: 'cpr_Id', index: 'cpr_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "cprfinalist", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year1": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "chargeStartDate": $("#ctl00_ContentPlaceHolder1_txt_start").val(), "chargeEndDate": $("#ctl00_ContentPlaceHolder1_txt_end").val() },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/ProjectChargeHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );
    //导出
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = "";
        var unitSpans = $("span[id=userSpan]", $("#labUnit"));
        for (var i = 0; i < unitSpans.length; i++) {
            unit += "'" + $(unitSpans[i]).attr("unitname") + "',";
        }
        var year = $("#ctl00_ContentPlaceHolder1_drp_year1").val();
        var year2 = $("#ctl00_ContentPlaceHolder1_drp_year2").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        var txtproAcount1 = $("#ctl00_ContentPlaceHolder1_txtproAcount").val();
        var txtproAcount2 = $("#ctl00_ContentPlaceHolder1_txtproAcount2").val();
        var projcharge1 = $("#ctl00_ContentPlaceHolder1_projcharge1").val();
        var projcharge2 = $("#ctl00_ContentPlaceHolder1_projcharge2").val();
        var txtprogress = $("#ctl00_ContentPlaceHolder1_txtprogress").val();
        var txtprogress2 = $("#ctl00_ContentPlaceHolder1_txtprogress2").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txtproAcount1 != "") {
            if (!reg.test(txtproAcount1)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtproAcount2 != "") {
            if (!reg.test(txtproAcount2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge1 != "") {
            if (!reg.test(projcharge1)) {
                msg += "合同收费开始值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge2 != "") {
            if (!reg.test(projcharge2)) {
                msg += "合同收费结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtprogress != "") {
            if (!reg.test(txtprogress)) {
                msg += "收款进度开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtprogress2 != "") {
            if (!reg.test(txtprogress2)) {
                msg += "收款进度结束值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }

    });
    //高级查询
    $("#btn_Search").click(function () {
        //查询按钮值更改
        $("#ctl00_ContentPlaceHolder1_SelectType").val("1");
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = getCheckedUnitNodes();
        //  var unitSpans = $("span[id=userSpan]", $("#labUnit"));       
        //for (var i = 0; i < unitSpans.length; i++) {
        //    unit += "'" + $(unitSpans[i]).attr("unitname") + "',";
        //}
       
        $("#ctl00_ContentPlaceHolder1_HiddenUnit").val(unit);
        var year1 = $("#ctl00_ContentPlaceHolder1_drp_year1").val();
        var year2 = $("#ctl00_ContentPlaceHolder1_drp_year2").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        var txtproAcount1 = $("#ctl00_ContentPlaceHolder1_txtproAcount").val();
        var txtproAcount2 = $("#ctl00_ContentPlaceHolder1_txtproAcount2").val();
        var projcharge1 = $("#ctl00_ContentPlaceHolder1_projcharge1").val();
        var projcharge2 = $("#ctl00_ContentPlaceHolder1_projcharge2").val();
        var txtprogress = $("#ctl00_ContentPlaceHolder1_txtprogress").val();
        var txtprogress2 = $("#ctl00_ContentPlaceHolder1_txtprogress2").val();
        //收费开始日期
        var chargeStartDate = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        //收费结束日期 
        var chargeEndDate = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txtproAcount1 != "") {
            if (!reg.test(txtproAcount1)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtproAcount2 != "") {
            if (!reg.test(txtproAcount2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge1 != "") {
            if (!reg.test(projcharge1)) {
                msg += "合同收费开始值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge2 != "") {
            if (!reg.test(projcharge2)) {
                msg += "合同收费结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtprogress != "") {
            if (!reg.test(txtprogress)) {
                msg += "收款进度开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtprogress2 != "") {
            if (!reg.test(txtprogress2)) {
                msg += "收款进度结束值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "unit": unit,
            "keyname": keyname,
            "year1": year1,
            "year2": year2,
            "txtproAcount1": txtproAcount1,
            "txtproAcount2": txtproAcount2,
            "projcharge1": projcharge1,
            "projcharge2": projcharge2,
            "txtprogress": txtprogress,
            "txtprogress2": txtprogress2,
            "chargeStartDate": chargeStartDate,
            "chargeEndDate": chargeEndDate
        };
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx?cxtype=gj&n=" + (Math.random() + new Date().getMilliseconds()),
            postData: params,
            page: 1

        }).trigger("reloadGrid");
    });
    //正常查询按钮
    $("#btn_cx").click(function () {
        //查询按钮记录
        $("#ctl00_ContentPlaceHolder1_SelectType").val("0");
        //获取where条件
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //获取部门
        var unit = getCheckedUnitNodes();
        //年份
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        //季度
        var jidu = $("#ctl00_ContentPlaceHolder1_drpJidu").val();
        //月份
        var month = $("#ctl00_ContentPlaceHolder1_drpMonth").val();
        //合同时间段
        var startyear = $("#ctl00_ContentPlaceHolder1_txt_year1").val();
        var endyear = $("#ctl00_ContentPlaceHolder1_txt_year2").val();
        //时间段选中的隐藏域值
        var cbxtimetype=$("#ctl00_ContentPlaceHolder1_hid_time").val();
        //收费开始日期
        var chargeStartDate = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        //收费结束日期 
        var chargeEndDate = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        var params = {
            "strwhere": strwhere,
            "unit": unit,
            "year": year,
            "jidu": jidu,
            "month": month,
            "startyear": startyear,
            "endyear": endyear,
            "cbxtimetype":cbxtimetype,
            "keyname": "",
            "txtproAcount1": "",
            "txtproAcount2": "",
            "projcharge1": "",
            "projcharge2": "",
            "txtprogress": "",
            "txtprogress2": "",
            "year1":"",
            "year2": "",
            "chargeStartDate": chargeStartDate,
            "chargeEndDate": chargeEndDate

        };
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx?n=" + (Math.random() + new Date().getMilliseconds()),
            postData: params,
            page: 1

        }).trigger("reloadGrid");
    });

    //年份
    //$("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
    //    $("#btn_cx").click();
    //});
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/Coperation/cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
    return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

}
function colSfjdFormatter(celvalue, options, rowData) {
    return '<div class="progressbar" style="height:20px;margin:1px 1px;" title="' + celvalue + '%" rel="' + celvalue + '"></div>';
}
function colImgFormatter(celvalue, options, rowData) {
    var str = "";
    if (celvalue == "100") {
        str = '<img src="/Images/status/green.gif" border="0" alt="完成收款"/>';
    }
    else {
        str = '<img src="/Images/status/red.gif" border="0" alt="未完成收款"/>';
    }
    return str;
}
//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "CoperationChargeReportViewBymaster.aspx?coperationSysNo=" + celvalue + "&coperationName=" + rowData["cpr_Name"] + "&coperationAccount=" + rowData["cpr_Acount"] + "&coperationRealReceivables=" + rowData["ssze"];
    return '<a href="' + pageurl + '" alt="查看">查看</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var sumSfjd = 0;
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));

        var sfjd = parseFloat($("#" + rowIds[i]).find("td").eq(7).text());
        sumSfjd = sumSfjd + sfjd;
    }

    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("rel"));
        $(item).progressbar({
            value: percent
        });
    });

    //var sumBuildArea = parseFloat($("#jqGrid").getCol("BuildArea", false, 'sum')).toFixed(4);BuildArea: sumBuildArea,
    var sumcpr_Acount = parseFloat($("#jqGrid ").jqGrid('getCol', 'htze', false));//获取列名为name的列
    // var sumcpr_Acount = parseFloat($("#jqGrid").getCol("cpr_Acount", false, 'sum')).toFixed(4);
    var sumssze = parseFloat($("#jqGrid ").jqGrid('getCol', 'sfze', false));//获取列名为name的列
    //var sumssze = parseFloat($("#jqGrid").getCol("ssze", false, 'sum')).toFixed(4);
    //var sumwsk = parseFloat($("#jqGrid").getCol("wsk", false, 'sum')).toFixed(4);
    $("#jqGrid").footerData('set', { cpr_Name: "合计:", cpr_Acount: sumcpr_Acount, ssze: sumssze }, false);

}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
//获取选中的部门
var getCheckedUnitNodes = function () {

    var unitlist = $("#ctl00_ContentPlaceHolder1_drpunit_divDropdownTreeText").text();
    //temp var
    var unit = "";
    if ($.trim(unitlist) != "") {
        //部门数组
        var unitarr = unitlist.split(',');
        //得到部门
        for(var index in unitarr) {
            if ($.trim(unitarr[index]) != "全院部门") {
                unit += "'" + $.trim(unitarr[index]) + "',";
            }
        }
    }
    return unit;
}
