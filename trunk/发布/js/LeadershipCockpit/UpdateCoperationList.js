﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/CoperationListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&action=updatecprlist&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val())+ '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year").val(),
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '', '', '', '', '', '', '', '', '', '', '','', '合同编号', '工程名称', '合同分类', '建筑规模(㎡)', '合同额(万元)', '承接部门', '签订日期', '完成日期', '统计年份', '工程负责人', '合同类型', '建设单位', '建筑类别', '录入人', '录入时间', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'sumarea', index: 'sumarea', hidden: true, editable: true },
                             { name: 'sumaccount', index: 'sumaccount', hidden: true, editable: true },
                             { name: 'sumsjaccount', index: 'sumsjaccount', hidden: true, editable: true },
                             { name: 'sumtzaccount', index: 'sumtzaccount', hidden: true, editable: true },
                             { name: 'sumsjtzaccount', index: 'sumsjtzaccount', hidden: true, editable: true },
                             { name: 'sumysaccount', index: 'sumysaccount', hidden: true, editable: true },
                             { name: 'AuditSysNo', index: 'AuditSysNo', hidden: true, editable: true },
                             { name: 'AuditStatus', index: 'AuditStatus', hidden: true, editable: true },
                             { name: 'ManageLevel', index: 'ManageLevel', hidden: true, editable: true },
                             { name: 'NeedLegalAdviser', index: 'NeedLegalAdviser', hidden: true, editable: true },
                             { name: 'cpr_SignDate', index: 'cpr_SignDate', hidden: true, editable: true },
                             { name: 'cpr_subID', index: 'cpr_subID', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 80, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 200, formatter: colNameShowFormatter },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 80, align: 'center' },
                             { name: 'BuildArea', index: 'BuildArea', width: 80, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 70, align: 'center' },
                             { name: 'qdrq', index: 'cpr_SignDate2', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'cpr_DoneDate', width: 80, align: 'center' },
                             { name: 'tjrq', index: 'cpr_SignDate', width: 80, align: 'center', hidden: true },
                             {
                                 name: 'PMUserName', index: 'ChgPeople', width: 80, align: 'center', formatter: function colShowName(celvalue, options, rowData) {
                                     return $.trim(celvalue);
                                 }
                             },
                              { name: 'cpr_Type2', index: 'cpr_Type2', width: 80, align: 'center' },
                             { name: 'BuildUnit', index: 'BuildUnit', width: 80, align: 'center', hidden: true },
                              { name: 'BuildType', index: 'BuildType', width: 60, align: 'center', hidden: true },
                             { name: 'InsertUser', index: 'InsertUserID', width: 70, align: 'center'},
                             { name: 'lrsj', index: 'InsertDate', width: 80, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/CoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'cpr_Id') + ",";
                            }
                        }
                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        // var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );



    //查询
    $("#btn_basesearch").click(function () {
        ClearjqGridParam("jqGrid");
        //数字验证正则    
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if ($("#ctl00_ContentPlaceHolder1_txt_account1").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account1").val())) {
            alert("合同额格式不正确！");
            $("#ctl00_ContentPlaceHolder1_txt_account1").val("");
            return false;
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_account2").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account2").val())) {
            alert("合同额格式不正确！");
            $("#ctl00_ContentPlaceHolder1_txt_account2").val("");
            return false;
        }      

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        // var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var unit = getCheckedUnitNodes();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_startdate").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_enddate").val();
        var cprtype = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());
        var buildunit = $("#ctl00_ContentPlaceHolder1_txt_cprbuildunit").val();
        var account1 = $("#ctl00_ContentPlaceHolder1_txt_account1").val();
        var account2 = $("#ctl00_ContentPlaceHolder1_txt_account2").val();

        $(".norecords").hide();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/CoperationListHandler.ashx?n='" + (Math.random() + new Date().getMilliseconds()) + " '&action=updatecprlist",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime, "cprtype": cprtype, "buildunit": buildunit, "account1": account1, "account2": account2 },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'
        }).trigger("reloadGrid");
    });

    //批量删除
    $("#btn_del").click(function () {
        var selrowlist = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
        var len = selrowlist.length;
        if (len == 0) {           
            alert("请选择删除的合同！");
            return false;
        }
        //if (confirm("确定要删除吗？")) {
        //    var cpridlist = "";
        //    for (i = 0; i < len; i++) {
        //        var cprid = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'cpr_Id');
        //        cpridlist = cpridlist + cprid + ",";
        //    }
           
        //    if (cpridlist.length > 0)
        //    {
        //        cpridlist = cpridlist.substr(0, (cpridlist.length - 1));
        //        var num = TG.Web.LeadershipCockpit.UpdateCoperationList.DelCoperation(cpridlist).value;
        //        if (num!="") {
        //            if (num > 0) {
        //                alert("删除成功，消息已发送合同所在部门的所长！");
        //                $("#jqGrid").trigger("reloadGrid");
        //            }
        //            else {
        //                alert("删除失败！");
        //            }
        //        }
        //        else
        //        {
        //            alert("删除成功，合同所在部门无所长消息无法发送！");
        //        }                
        //    }
            
        //}
    });
    //弹出层原因
    $("#btn_save").live("click", function () {
       
        if ($.trim($("#txt_content").val()) == "")
        {
            alert("删除原因不能为空！");
            
            return false;
        }
        var selrowlist = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
        var len = selrowlist.length;
        var cpridlist = "";
        for (i = 0; i < len; i++) {
            var cprid = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'cpr_Id');
            cpridlist = cpridlist + cprid + ",";
        }

        if (cpridlist.length > 0) {
            cpridlist = cpridlist.substr(0, (cpridlist.length - 1));
            var num = TG.Web.LeadershipCockpit.UpdateCoperationList.DelCoperation(cpridlist, $.trim($("#txt_content").val())).value;
            if (num != "") {
                if (num > 0) {
                    alert("删除成功，消息已发送合同所在部门的所长！");
                    $("#jqGrid").trigger("reloadGrid");
                }
                else {
                    alert("删除失败！");
                }
            }
            else {
                alert("删除成功，合同所在部门无所长消息无法发送！");
            }
            $("#btn_close").click();
        }
    });
});
//
//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "cpr_ShowCoprationBymaster.aspx";
    if (rowData["cpr_Type2"] != null && rowData["cpr_Type2"]!="") {
        switch ($.trim(rowData["cpr_Type2"])) {           
            case "岩土工程勘察合同":
                pageurl = "cpr_ShowReconnCoprationBymaster.aspx";
                break;
            case "岩土工程设计合同":
                pageurl = "cpr_ShowDesignCoprationBymaster.aspx";
                break;
            case "基坑及边坡监测合同":
                pageurl = "cpr_ShowPitCoperationBymaster.aspx";
                break;
            case "测量及沉降观测合同":
                pageurl = "cpr_ShowTestCoperationBymaster.aspx";
                break;
            case "岩土工程检测合同":
                pageurl = "cpr_ShowMeasureCoperationBymaster.aspx";
                break;
            case "岩土工程施工合同":
                pageurl = "cpr_ShowConstruCoperartionBymaster.aspx";
                break;
            case "规划工程合同":
                pageurl = "cpr_ShowPlanningCoperationBymaster.aspx";
                break;
            case "景观规划合同":
                pageurl = "cpr_ShowSceneryCoperationBymaster.aspx";
                break;
            case "工程监理":
            case "市政工程监理合同":
            case "景观绿化工程监理合同":
            case "天然气输配工程监理合同":
            case "管网、热力站工程监理合同":
            case "土方工程监理合同":
                pageurl = "cpr_ShowSuperCoperationBymaster.aspx";
                break;
            default:
                pageurl = "cpr_ShowCoprationBymaster.aspx";
                break;

        }
    }
    var cprid=rowData["cpr_subID"];
    if(cprid==null||cprid==""||cprid=="0")
    {
        cprid = rowData["cpr_Id"];
        pageurl = "cpr_ShowCoprationBymaster.aspx";
    }
   
   
    return '<a href="../coperation/' + pageurl + '?flag=cprlist&cprid=' + cprid + '" alt="查看合同">' + celvalue + '</a>';

}

//合同阶段
function colProcess(celvalue, options, rowData) {
    var temp = "";
    var arr = $.trim(celvalue).split(",");
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == "27") {
            temp = "方案,";
        }
        if (arr[i] == "28") {
            temp += "初设,";
        }
        if (arr[i] == "29") {
            temp += "施工图,";
        }
        if (arr[i] == "30") {
            temp += "其他,";
        }
    }
    return temp;
}

//查看
function colShowFormatter(celvalue, options, rowData) {

    var pageurl = "cpr_ShowCoprationBymaster.aspx";
    if (rowData["cpr_Type2"] != null && rowData["cpr_Type2"] != "") {
        switch ($.trim(rowData["cpr_Type2"])) {
            case "岩土工程勘察合同":
                pageurl = "cpr_ShowReconnCoprationBymaster.aspx";
                break;
            case "岩土工程设计合同":
                pageurl = "cpr_ShowDesignCoprationBymaster.aspx";
                break;
            case "基坑及边坡监测合同":
                pageurl = "cpr_ShowPitCoperationBymaster.aspx";
                break;
            case "测量及沉降观测合同":
                pageurl = "cpr_ShowTestCoperationBymaster.aspx";
                break;
            case "岩土工程检测合同":
                pageurl = "cpr_ShowMeasureCoperationBymaster.aspx";
                break;
            case "岩土工程施工合同":
                pageurl = "cpr_ShowConstruCoperartionBymaster.aspx";
                break;
            case "规划工程合同":
                pageurl = "cpr_ShowPlanningCoperationBymaster.aspx";
                break;
            case "景观规划合同":
                pageurl = "cpr_ShowSceneryCoperationBymaster.aspx";
                break;
            case "市政工程合同":
                pageurl = "cpr_ShowEngineerCoperationBymaster.aspx";
                break;
            case "工程监理":
            case "市政工程监理合同":
            case "景观绿化工程监理合同":
            case "天然气输配工程监理合同":
            case "管网、热力站工程监理合同":
            case "土方工程监理合同":
                pageurl = "cpr_ShowSuperCoperationBymaster.aspx";
                break;
            default:
                pageurl = "cpr_ShowCoprationBymaster.aspx";
                break;

        }
    }
    var cprid = rowData["cpr_subID"];
    if (cprid == null || cprid == "" || cprid == "0") {
        cprid = rowData["cpr_Id"];
        pageurl = "cpr_ShowCoprationBymaster.aspx";
    }


    return '<a href="../coperation/' + pageurl + '?flag=cprlist&cprid=' + cprid + '" alt="查看合同">查看</a>';


}
//编辑
function colEditFormatter(celvalue, options, rowdata) {
    if (celvalue != "") {
        var pageurl = "UpdateCoperationAcount.aspx?cprid=" + celvalue;
       
        return '<a href="' + pageurl + '" alt="编辑合同" class="allowEdit">编辑</a>';
    } //class="allowEdit"
}
//统计 
function completeMethod() {

    var sumarea = 0, sumaccount = 0, sumsjaccount = 0, sumtzaccount = 0, sumsjtzaccount = 0, sumyfaccount = 0;
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(1).text((i + 1));
        if (i == 0) {
            sumarea = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(3).text());
            sumaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(4).text());
            sumsjaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(5).text());
            sumtzaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(6).text());
            sumsjtzaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(7).text());
            sumyfaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(8).text());
        }
        //$("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
    $("#jqGrid").footerData('set', { cpr_No: "合计:", BuildArea: sumarea, cpr_Acount: sumaccount });
   
}
//无数据
function loadCompMethod() {
   // $("#jqGrid").setGridParam("").hideCol("cb").trigger("reloadGrid");
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }

    }
    else {
        $("#nodata").hide();
    }
}
//获取建筑分类值
function GetTreeValues(obj) {
    var allstr = "";
    //一级   
    $("#" + obj).children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate!="2") {          
        var onestr = ckvalue;
        var tempstr = subGetTreeValues($(this), onestr);
        allstr = allstr + tempstr + "+";
        // }
    });
    if (allstr != "") {
        allstr = allstr.substr(0, (allstr.length - 1));
    }
    return allstr;
}
//二级li里选中的值(递归)
function subGetTreeValues(obj, str) {
    //二级   
    var currallstr = "";
    obj.children("ul").children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate != "2") {
        var substr = "^" + ckvalue;
        var tempstr = childGetTreeValues($(this), substr);
        currallstr = currallstr + tempstr;

        //}
    });
    return str + currallstr;
}
//三级li
function childGetTreeValues(obj, str) {
    var currallstr = "";
    obj.children("ul").children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate != "2") {
        var childstr = str + "*" + ckvalue;
        var tempstr = childGetTreeValues($(this), childstr);
        currallstr = currallstr + tempstr;
        //}
    });
    if (currallstr == "") {
        currallstr = str;
    }
    return currallstr;
}
//清空jqgrid postdata参数
var ClearjqGridParam = function (jqgrid) {
    var postData = $("#" + jqgrid).getGridParam("postData");

    $.each(postData, function (k, v) {
        delete postData[k];
    });
}
//获取选中的部门
var getCheckedUnitNodes = function () {

    var unitlist = $("#ctl00_ContentPlaceHolder1_drp_unit_divDropdownTreeText").text();
    //temp var
    var unit = "";
    if ($.trim(unitlist) != "") {
        //选择的全部部门，部门值为空
        var allcheckedstate = $.trim($("#ctl00_ContentPlaceHolder1_drp_unit_t_l_1").attr("checkedstate"));
        if (allcheckedstate != "0") {
            //部门数组
            var unitarr = unitlist.split(',');
            //得到部门
            for (var index in unitarr) {
                if ($.trim(unitarr[index]) != "全院部门") {
                    unit += "'" + $.trim(unitarr[index]) + "',";
                }
            }
        }
    }
    return unit;
}

