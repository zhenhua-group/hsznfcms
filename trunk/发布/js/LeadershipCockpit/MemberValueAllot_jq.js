﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '姓名', '部门', '本所项目总产值(元)', '其他所项目总产值(元)', '项目总产值(元)', '方案补贴(元)', '总计(元)', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'mem_ID', index: 'mem_ID', hidden: true, editable: true },
                             { name: 'mem_Name', index: 'mem_Name', width: 100, align: 'center' },
                             { name: 'unit_name', index: 'unit_name', width: 100, align: 'center' },
                             { name: 'theUnitCount', index: 'theUnitCount', width: 100, align: 'right' },
                              {
                                  name: 'qttotalcount', index: 'qttotalcount', width: 100, align: 'right', formatter: function (celvalue, options, rowData) {
                                      var d_zj = parseFloat(rowData["totalcount"]) - parseFloat(rowData["theUnitCount"]);
                                      return d_zj.toFixed(2);
                                  }
                              },
                             { name: 'totalcount', index: 'totalcount', width: 100, align: 'right' },
                             { name: 'sbcount', index: 'sbcount', width: 100, align: 'right' },
                             {
                                 name: 'zj', index: 'zj', width: 100, align: 'right', formatter: function (celvalue, options, rowData) {
                                     var d_zj = parseFloat(rowData["totalcount"]) + parseFloat(rowData["sbcount"]);
                                     return d_zj.toFixed(2);
                                 }
                             },
                             { name: 'mem_ID', index: 'mem_ID', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "member", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "unit": $("#ctl00_ContentPlaceHolder1_drp_unit").val() },
        loadonce: false,
        sortname: 'me.mem_ID',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'mem_ID');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var name = $("#ctl00_ContentPlaceHolder1_txt_memberName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'year': year, "name": name },
            page: 1

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var name = $("#ctl00_ContentPlaceHolder1_txt_memberName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'year': year, "name": name },
            page: 1

        }).trigger("reloadGrid");
    });
    $(".chakan").live("click", function () {
        var memid = $(this).parent().parent().find("td").eq(2).text();
        var memname = $(this).parent().parent().find("td").eq(3).text();
        GetProjectList(memid, memname);
    });
});

//名称连接
//function colNameShowFormatter(celvalue, options, rowData) {
//    var pageurl = "/ProjectManage/ShowProject.aspx?flag=list&pro_id=" + rowData["pro_ID"];
//    return '<img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px; border: none;" /><a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

//}

//查看
function colShowFormatter(celvalue, options, rowData) {

    return '<a href="#ShowDiv" data-toggle="modal" class="chakan" >查看</a>';

}
function ClearPopAreaContent() {
    $("#PopAreaDivMain").html("");
}
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
    var sumtheUnitCount = parseFloat($("#jqGrid").getCol("theUnitCount", false, 'sum')).toFixed(4);
    var sumqtUnitCount =parseFloat($("#jqGrid").getCol("totalcount", false, 'sum'))- parseFloat($("#jqGrid").getCol("theUnitCount", false, 'sum')).toFixed(4);
    var sumtotalcount = parseFloat($("#jqGrid").getCol("totalcount", false, 'sum')).toFixed(4);
    var sumsbcount = parseFloat($("#jqGrid").getCol("sbcount", false, 'sum')).toFixed(4);
    var sumzj = parseFloat($("#jqGrid").getCol("totalcount", false, 'sum')) + parseFloat($("#jqGrid").getCol("sbcount", false, 'sum'));
    $("#jqGrid").footerData('set', { mem_Name: "合计:", theUnitCount: sumtheUnitCount, qttotalcount: sumqtUnitCount, totalcount: sumtotalcount, sbcount: sumsbcount, zj: sumzj.toFixed(4) }, false);
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
function GetProjectList(memid, memname) {

    // var result = BackgroundInvoke.GetProjectCountByQuarter(quarter + "", year + "");



    $.post("/HttpHandler/DrpMemberHandler.ashx", { "action": "2", "memid": memid, "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "n": Math.random() }, function (result) {

        var tableString = "";
        tableString += "<table class=\"table table-bordered table-hover\">";
        tableString += "<tr>";
        tableString += "<td style=\"width: 10%\" align=\"center\">";
        tableString += "序号";
        tableString += "</td>";
        tableString += "<td style=\"width: 60%;\" align=\"center\">";
        tableString += "项目名称";
        tableString += "</td>"
        tableString += "<td style=\"width: 15%;\" align=\"center\">";
        tableString += "承接部门";
        tableString += "</td>"
        tableString += "<td style=\"width: 15%;\" align=\"center\">";
        tableString += "产值(元)";
        tableString += "</td>";
        tableString += "</tr>";


        if (result.length > 0) {
            tableString += result;
        }
        tableString += "</table>";
        ClearPopAreaContent();
        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        $("h4", "#ShowDiv").text(memname + "相关项目产值详情");

        //隔行变色
        $(".cls_show_cst_jiben tr:even").css({ background: "f0f0f0" });
    });
}