﻿$(document).ready(function () {
    //行背景
    $("#gv_project tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //得到实际的现实高度
    var h = document.body.clientHeight - 130;
    //表格滚动
    //    $("#gv_project").chromatable({
    //        width: "100%",
    //        height: h,
    //        scrolling: "true"
    //    });
    //隔行变色
    $("#gv_project tr:odd").css({ background: "white" });
    //全选
    $("#chk_All").click(function () {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });
    //单选
    $(".chk_id").each(function (i) {
        $(this).click(function () {
            if ($(this).get(0).checked) {
                $(this).attr("checked", "true");
            }
            else {
                $(this).attr("checked", "");
            }
        });
    });

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });
    //无合同显示
    $("#gv_project a[cprid=0]").each(function () {
        $(this).attr("href", "###").text("无合同");
    });
    $("#btn_Search").click(function () {
        if ($("#drp_unit").val() == "-1") {
            alert("请选择生产部门!");
            return false;
        }

    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();



});

function GetProjectList(memid,memname) {

   // var result = BackgroundInvoke.GetProjectCountByQuarter(quarter + "", year + "");



    $.post("/HttpHandler/DrpMemberHandler.ashx", { "action": "4", "memid": memid, "year": $("#drp_year").val() }, function (result) {

        var tableString = "";
        tableString += "<table class=\"cls_show_cst_jiben\">";
        tableString += "<tr>";
        tableString += "<td style=\"width: 10%\" align=\"center\">";
        tableString += "序号";
        tableString += "</td>";
        tableString += "<td style=\"width: 70%;\" align=\"center\">";
        tableString += "项目名称";
        tableString += "</td>"
        tableString += "<td style=\"width: 20%;\" align=\"center\">";
        tableString += "产值(元)";
        tableString += "</td>";
        tableString += "</tr>";
       
        
        if (result.length > 0) {
            tableString+=result;
        }
        tableString += "</table>";
        ClearPopAreaContent();
        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv(memname + "相关项目产值详情");
        //隔行变色
        $(".cls_show_cst_jiben tr:even").css({ background: "f0f0f0" });

    });
}
function PopAreaDiv(title) {
    $("#PopAreaDivMain").dialog({
        autoOpen: false,
        modal: true,
        width: 750,
        height: 300,
        resizable: false,
        title: title,
        buttons:
            {
                "关闭": function () { $(this).dialog("close"); }
            }
    }).dialog("open");
}
function ClearPopAreaContent() {
    $("#PopAreaDivMain").html("");
}

