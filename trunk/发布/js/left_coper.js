﻿$(document).ready(function () {
    //合同录入
    $("#link_htlr").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_AddCoperation.aspx");
    });
    //电子合同
    $("#link_cprlist").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_CorperationList.aspx");
    });
    //    //客户关联
    $("#link_htgl").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_ShowRelativeContractInfo.aspx");
    });
    //合同评审
    $("#coperationAudit").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_CoperationAuditList.aspx");
    });
    //合同评审
    $("#coperationAuditEdit").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_CoperationAuditEditList.aspx");
    });
    //项目收费管理 by long 20130513
    $("#link_proj_sfgl").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/ProjectCharge.aspx");
    });
    //收费管理
    //    $("#link_sfgl").click(function () {
    //        $("#rightFrame", window.parent.document).attr("src", "../Coperation/CoperationCharge.aspx");
    //    });
    //合同收费统计
    $("#link_sftj").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_ChargeCountList.aspx");
    });
    //部门目标完成统计
    $("#link_unit_allot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_ChargeCount.aspx");
    });
    //查询
    $("#link_search").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_SearchCoperation.aspx");
    });
    //合同审核流程配置
    $("#auditConfig").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/CoperationAuditConfig.aspx");
    });
    //显示报备信息
    $("#coprepertShow").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/CoperationForReportList.aspx");
    });
    //报备信息录入
    $("#copreportInsert").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/AddCoperationForReport.aspx");
    });
    //报备信息高级查询
    $("#coprepertSelect").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/SeacherCoperationForReport.aspx");
    });

    //显示所有菜单控制
    LoadLeftMenu.GetLeftMenu(1);
});