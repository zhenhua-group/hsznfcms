﻿
var SysUserInfoSch = function () {

    //默认加载
    var pageLoad = function () {

        //绑定显示查询字段事件
        $("#chkSchColums :checkbox").bind('click', function () {

            var obj = $(this);
            //选中名称
            var columnName = obj.attr('id');

            if (obj.is(":checked")) {
                if (columnName == "chkAll") {
                    //还原到字段容器
                    $("#tbTableColumns>tbody>tr:gt(0) td[for]").each(function (i, n) {
                        var $td = $(n);
                        var $td2 = $(n).next();
                        var forname = $(n).attr("for");
                        $("#tbTableColumnItems tr[for=" + forname + "]").append($td);
                        $("#tbTableColumnItems tr[for=" + forname + "]").append($td2);
                    });
                    //移除原来选中的
                    $("#tbTableColumns>tbody>tr:gt(0)").remove();
                    //行
                    var $tr = $("<tr></tr>");
                    //根据选中的字段添加到查询条件
                    $("#chkSchColums :checkbox[for!='chkAll']").each(function (i, n) {

                        var forname = $(n).attr('id');
                        var $temp = $("#tbTableColumnItems>tbody>tr[for=" + forname + "]").children();

                        if ($temp.length > 0) {

                            $tr.append($temp);

                            if ((i % 3) == 0) {

                                $("#tbTableColumns>tbody").append($tr);
                                $tr = $("<tr></tr>");
                            }

                            $("#tbTableColumns>tbody").append($tr);

                            $(n).attr("checked", true);
                            $(n).parent().addClass('checked');
                        }
                    });
                }
                else //选中单个 
                {
                    var $td = $("#tbTableColumnItems>tbody>tr[for=" + columnName + "]").children();
                    //行
                    var $tr = $("<tr></tr>");

                    $("#tbTableColumns>tbody>tr:gt(0)>td").each(function (i, n) {

                        $tr.append($(n));
                        if (((i + 1) % 6) == 0) {
                            $("#tbTableColumns>tbody").append($tr);
                            $tr = $("<tr></tr>");
                        }
                    });

                    if ($td.length > 0) {

                        $tr.append($td);
                        $("#tbTableColumns>tbody").append($tr);
                    }
                }
            }
            else { //取消选中
                if (columnName == "chkAll") {
                    //选过的字段放回原来的容器
                    $("#tbTableColumns>tbody>tr:gt(0) td[for]").each(function (i, n) {

                        var $td = $(n);
                        var $td2 = $(n).next();
                        var forname = $(n).attr("for");

                        $("#tbTableColumnItems tr[for=" + forname + "]").append($td);
                        $("#tbTableColumnItems tr[for=" + forname + "]").append($td2);
                    });

                    //移除
                    $("#tbTableColumns>tbody>tr:gt(0)").remove();

                    //全部选不中
                    $("#chkSchColums :checkbox").attr("checked", false);
                    $("#chkSchColums :checkbox").parent().removeClass("checked");

                }
                else {
                    //找到选中的字段添加到原来的容器
                    var $td = $("#tbTableColumns>tbody>tr:gt(0)").children("td[for=" + columnName + "]");
                    var $td2 = $("#tbTableColumns>tbody>tr:gt(0)").children("td[for=" + columnName + "]").next();
                    $("#tbTableColumnItems tr[for=" + columnName + "]").append($td);
                    $("#tbTableColumnItems tr[for=" + columnName + "]").append($td2);

                    var $tr = $("<tr></tr>");
                    var $tempTable = $("<table></table>");
                    $("#tbTableColumns>tbody>tr:gt(0)>td").each(function (i, n) {
                        $tr.append($(n));
                        if (((i + 1) % 6) == 0) {
                            $tempTable.append($tr);
                            $tr = $("<tr></tr>");
                        }
                    });

                    $tempTable.append($tr);
                    //移除
                    $("#tbTableColumns>tbody>tr:gt(0)").remove();
                    //重新添加到容器
                    $("#tbTableColumns>tbody").append($tempTable.children("tbody").children());
                }
            }
        });

        //查询
        $("#btnSearch").click(function () {

            var val1 = $("#txt_mem_Login").val();
            var val2 = $("#txt_mem_Name").val();
            var val3 = $("#txt_mem_Birthday").val();
            var val4 = $("#txt_mem_Sex").val();
            //var val5 = $("#txt_memUnitName").val();
            var val5 = $.trim(IsStructCheckNode('structtype')); //承接部门
            if (val5.length > 0) {
                val5 = val5.substr(0, val5.length - 1);
            }
            var val6 = $("#txt_memSpeName").val();
            var val7 = $("#txt_isFiredName").val();
            var val8 = $("#txt_RoleTecName").val();
            var val9 = $("#txt_RoleMagName").val();
            var val10 = $("#txt_mem_ArchReg").val();
            var val11 = $("#txt_ArchLevelName").val();
            var val12 = $("#txt_mem_Code").val();
            var val13 = $("#txt_mem_Age").val();
            var val14 = $("#txt_mem_CoperationDate").val();

            var val15 = $.trim(IsStructCheckNodeMem('structtype')); //人员选择
            if (val15.length > 0) {
                val15 = val15.substr(0, val15.length - 1);
            }

            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var msg = "";
            if (val1 != "") {
                if (val1.length > 16) {
                    msg += "登录名输入过长！</br>";
                }
            }
            if (val2 != "") {
                if (val2.length > 6) {
                    msg += "姓名名输入过长！</br>";
                }
            }
            if (val12 != "") {
                if (val12.length > 18) {
                    msg += "身份证号输入过长！</br>";
                }
            }
            if (val13 != "") {
                if (!reg.test(val13)) {
                    msg += "年龄请输入数字！</br>";
                }
            }

            if (msg != "") {
                jAlert(msg, "提示");
                return false;
            }

            var strwhere = "";

            var params = {
                "strwhere": strwhere,
                "mem_Login": val1,
                "mem_Name": val2,
                "mem_Birthday": val3,
                "mem_Sex": val4,
                "memUnitName": val5,
                "memSpeName": val6,
                "isFiredName": val7,
                "RoleTecName": val8,
                "RoleMagName": val9,
                "mem_ArchReg": val10,
                "ArchLevelName": val11,
                "mem_Code": val12,
                "mem_Age": val13,
                "mem_CoperationDate": val14,
                "mem_List": val15
            };

            $("#jqGrid").jqGrid('setGridParam', {
                url: "/HttpHandler/SystemSet/SysUserInfoHandler.ashx?n='" + Math.random() + " '&action=search",
                postData: params,
                mytype: 'POST',
                page: 1,
                sortname: 'mem_Order',
                sortorder: 'ASC'

            }).trigger("reloadGrid");

        });

        //绑定列选择
        InitColumnsCookies("columnslist", "jqGrid", "columnsuserid");

        //显示列选中
        InitColumsCheck();

        //导出
        $("#btnExportExcel").click(function () {

            var val1 = $("#txt_mem_Login").val();
            var val2 = $("#txt_mem_Name").val();
            var val3 = $("#txt_mem_Birthday").val();
            var val4 = $("#txt_mem_Sex").val();
            //var val5 = $("#txt_memUnitName").val();
            var val5 = $.trim(IsStructCheckNode('structtype')); //承接部门
            if (val5.length > 0) {
                val5 = val5.substr(0, val5.length - 1);
            }
            var val6 = $("#txt_memSpeName").val();
            var val7 = $("#txt_isFiredName").val();
            var val8 = $("#txt_RoleTecName").val();
            var val9 = $("#txt_RoleMagName").val();
            var val10 = $("#txt_mem_ArchReg").val();
            var val11 = $("#txt_ArchLevelName").val();
            var val12 = $("#txt_mem_Code").val();
            var val13 = $("#txt_mem_Age").val();
            var val14 = $("#txt_memCode").val();

            var val15 = $.trim(IsStructCheckNodeMem('structtype')); //人员选择
            if (val15.length > 0) {
                val15 = val15.substr(0, val15.length - 1);
            }


            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var msg = "";
            if (val1 != "") {
                if (val1.length > 16) {
                    msg += "登录名输入过长！</br>";
                }
            }
            if (val2 != "") {
                if (val2.length > 6) {
                    msg += "姓名名输入过长！</br>";
                }
            }
            if (val12 != "") {
                if (val12.length > 18) {
                    msg += "身份证号输入过长！</br>";
                }
            }
            if (val13 != "") {
                if (!reg.test(val13)) {
                    msg += "年龄请输入数字！</br>";
                }
            }

            if (msg != "") {
                jAlert(msg, "提示");
                return false;
            }

            var strwhere = "";

            var params = {
                "strwhere": strwhere,
                "mem_Login": val1,
                "mem_Name": val2,
                "mem_Birthday": val3,
                "mem_Sex": val4,
                "memUnitName": val5,
                "memSpeName": val6,
                "isFiredName": val7,
                "RoleTecName": val8,
                "RoleMagName": val9,
                "mem_ArchReg": val10,
                "ArchLevelName": val11,
                "mem_Code": val12,
                "mem_Age": val13,
                "mem_CoperationDate": val14,
                "mem_List": val15
            };

            var urlparams = "strwhere=" + strwhere + "&mem_Login=" + val1 + "&mem_Name=" + val2 + "&mem_Birthday=" + val3 + "&mem_Sex=" + val4 + "&memUnitName=" + val5 + "&memSpeName=" + val6 + "&isFiredName=" + val7 + "&RoleTecName=" + val8 + "&RoleMagName=" + val9 + "&mem_ArchReg=" + val10 + "&ArchLevelName=" + val11 + "&mem_Code=" + val12 + "&mem_Age=" + val13 + "&mem_CoperationDate=" + val14 + "&mem_List=" + val15;

            window.location.href = '/SystemSet/ExportUserInfoListToExcel.aspx?' + urlparams;


            return false;
        });
    }
    //清空jqgrid postdata参数
    var ClearjqGridParam = function (jqgrid) {
        var postData = $("#" + jqgrid).getGridParam("postData");

        $.each(postData, function (k, v) {
            delete postData[k];
        });
    }
    //绑定列选择 
    var InitColumsCheck = function () {

        //选择显示列
        $(":checkbox:first", $("#columnsuserid")).click(function () {

            //获得显示的列，保存到cookies
            var columnslist = "";
            var checkedstate = $(this).parent().attr("class");
            if (checkedstate != "checked")//代表选中
            {
                $(":checkbox:not(:first)", $("#columnsuserid")).each(function (i, value) {
                    var span = $(value).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(value).attr("checked", true);
                    $("#jqGrid").showCol($(value).val());
                    //保存cookies值
                    columnslist = columnslist + $(value).val() + ",";
                });
                columnslist = columnslist.substr(0, (columnslist.length - 1));
            }
            else {
                $(":checkbox:not(:first)", $("#columnsuserid")).each(function (i, value) {
                    var span = $(value).parent();
                    span.removeClass("checked");
                    $(value).attr("checked", false);
                    $("#jqGrid").hideCol($(value).val());
                });
                columnslist = "";
            }
            //保存到cookies
            $.cookie("columnslist", columnslist, { expires: 365, path: "/" });

            // $("#jqGrid").trigger('reloadGrid');
        });


        //点击基本合同字段
        $(":checkbox:not(:first)", $("#columnsuserid")).click(function () {

            var checkedstate = $(this).parent().attr("class");
            var columnsname = $(this).val();

            if (($(":checkbox:not(:first):checked", $("#columnsuserid")).length) == ($(":checkbox:not(:first)", $("#columnsuserid")).length)) {
                $(":checkbox:first", $("#columnsuserid")).parent().addClass("checked");
                $(":checkbox:first", $("#columnsuserid")).attr("checked", true);
            }
            else {
                $(":checkbox:first", $("#columnsuserid")).parent().removeClass("checked");
                $(":checkbox:first", $("#columnsuserid")).attr("checked", false);
            }

            if (checkedstate != "checked")//代表选中
            {
                $("#jqGrid").showCol(columnsname);
            }
            else {
                $(this).attr("checked", false);
                $("#jqGrid").hideCol(columnsname);
            }

            //获得显示的列，保存到cookies
            var columnslist = "";
            $(":checkbox:not(:first):checked", $("#columnsuserid")).each(function () {
                columnslist = columnslist + $(this).val() + ",";
            });
            if (columnslist != "") {
                columnslist = columnslist.substr(0, (columnslist.length - 1));
            }
            //保存到cookies
            $.cookie("columnslist", columnslist, { expires: 365, path: "/" });

        });
    }
    //存储Cookies
    var InitColumnsCookies = function (cookiename, jqgrid, colid) {
        //基本合同列表
        var columnslist = $.cookie(cookiename);

        if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {
            var list = columnslist.split(",");
            // $.each(list, function (i, c) {
            $(":checkbox:not(:first)[value]", $("#" + colid)).each(function (j, n) {
                if (list.indexOf($.trim($(n).val())) > -1) {
                    var span = $(n).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(n).attr("checked", true);
                    $("#" + jqgrid).showCol($(n).val());
                }
                else {
                    $("#" + jqgrid).hideCol($(n).val());
                }

            });

            //  });
            //是否需要选中 全选框
            if ((list.length) == ($(":checkbox:not(:first)", $("#" + colid)).length)) {
                $(":checkbox:first", $("#" + colid)).parent().addClass("checked");
                $(":checkbox:first", $("#" + colid)).attr("checked", true);
            }

        }
        else {

            //cookie未保存，显示默认的几个字段
            //生产经营合同和监理合同是11个多了已收费字段，岩石工程检测合同是9个少建筑面积，其他都是10个字段
            var list = new Array("mem_Login", "mem_Name", "isFiredName", "memUnitName", "mem_Sex", "memSpeName", "RoleMagName", "RoleTecName", "mem_Code", "mem_CodeAddr", "mem_Birthday", "mem_Mobile", "mem_Telephone", "mem_ArchLevelName2", "mem_ArchLevelTime", "mem_Age", "ArchLevelName", "mem_ArchRegTime", "mem_ArchRegAddrName");//直接定义并初始化


            $.each(list, function (i, c) {

                $(":checkbox:not(:first)[value='" + c + "']", $("#" + colid)).each(function (j, n) {
                    var span = $(n).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(n).attr("checked", true);
                    // $("#jqGrid").showCol($(n).val());
                });

            });
        }
    }
    //拼接地址栏参数
    var parseParam = function (param, key) {

        var paramStr = "";
        if (param instanceof String || param instanceof Number || param instanceof Boolean) {
            paramStr += "&" + key + "=" + encodeURIComponent(param);
        } else {
            $.each(param, function (i) {
                var k = key == null ? i : key + (param instanceof Array ? "[" + i + "]" : "." + i);
                paramStr += '&' + parseParam(this, k);
            });
        }
        return paramStr.substr(1);
    }



    return {
        init: function () {
            pageLoad();
        }
    };
}();