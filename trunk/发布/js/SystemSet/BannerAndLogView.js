﻿$(document).ready(function () {
    
});
//加载数据
function GetAttachData() {
    loadCoperationAttach();
}
//加载附件信息
function loadCoperationAttach() {
    var data = "action=getbanerattach&attachid=" + $("#hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            var filedata = result.ds;
            if ($("#datas_att tr").length > 1) {
                $("#datas_att tr:gt(0)").remove();
            }
            $.each(filedata, function (i, n) {
                var row = $("#att_row").clone();
                var oper = "<a href='#' rel='" + n.ID + "'>删除</a>";
                var oper2 = "<a href='../Attach_User/filedata/pubfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                row.find("#att_id").text(n.ID);
                row.find("#att_filename").html(img + n.FileName);
                row.find("#att_filesize").text(n.FileSizeString);
                row.find("#att_filetype").text(n.FileType);
                row.find("#att_uptime").text(n.UploadTime);
                row.find("#att_oper").html(oper);
                row.find("#att_oper2").html(oper2);
                row.find("#att_oper a").click(function () {
                    delCprAttach($(this));
                });
                row.appendTo("#datas_att");
            });
            filedata = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delbanerattach&attachid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                //加载附件
                link.parents("tr:first").remove();
                alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}