﻿function ActionPower(container) {
    this.IsEdit = false;
    this.Container = container;
    this.BackgroundInvoke = TG.Web.SystemSet.ActionPowerManager;
    this.ActionPowerSysNo = 0;
    var Instance = this;

    this.AddButton = $("#AddActionPower");
    this.AddButton.click(function () {
        Instance.IsEdit = false;
        Instance.Clear();
        Instance.PopAreaMenuDetail("添加操作权限");
        return false;
    });
    this.EditButton = $("#editActionPowerLinkButton");
    this.EditButton.live("click", function () {
        Instance.IsEdit = true;
        var sysNo = $(this).attr("actionpowersysno");
        Instance.ActionPowerSysNo = sysNo;
        Instance.Clear();
        Instance.InitActionPowerDetail(sysNo);
        Instance.PopAreaMenuDetail("修改操作权限");
    });
    this.DeleteButton = $("a[id=deleteActionPowerLinkButton]");
    this.DeleteButton.live("click", function () {
        if (confirm("您确认要删除此条记录吗？")) {
            var sysNo = $(this).attr("actionpowersysno");
            var result = Instance.BackgroundInvoke.DeleteActionPower(sysNo).value;
            if (parseInt(result, 10) > 0) {
                alert("删除成功");
                document.location.reload();
            } else {
                alert("删除失败");
            }
        }
    });
    this.InitActionPowerDetail = function (sysNo) {
        var result = Instance.BackgroundInvoke.GetActionPower(sysNo).value;
        if (result != null && result.length > 0) {
            var actionPowerViewEntity = Global.evalJSON(result);
            $("#DescriptionTextBox", container).val(actionPowerViewEntity.Description);
            $("#PageNameTextBox", container).val(actionPowerViewEntity.PageName);
            $("#typeDropDownList option[value=" + actionPowerViewEntity.Type + "]", container).attr("selected", "selected");
        }
    }

    this.PopAreaMenuDetail = function (title) {
        $("#PopAreaActionPowerDetail").dialog({
            autoOpen: false,
            modal: true,
            width: 530,
            height: "auto",
            resizable: false,
            title: title,
            buttons:
	                        {
	                            "保存": Instance.SaveLeftMenu,
	                            "关闭": function () { $(this).dialog("close"); }
	                        }
        }).dialog("open");
    }

    //保存左侧菜单项
    this.SaveLeftMenu = function () {
        var myDate = new Date();
        var actionPowerViewEntity = {
            "SysNo": Instance.ActionPowerSysNo,
            "Description": $("#DescriptionTextBox", container).val(),
            "PageName": $("#PageNameTextBox", container).val(),
            "RoleNameString": "",
            "Type": $("#typeDropDownList", container).val(),
            "TypeString": "",
            "InUser": 1,
            "InUserName": "",
            "InDate": myDate.getYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate()
        };
        actionPowerViewEntity.Role = "1";

        actionPowerViewEntity.Power = "1";

        actionPowerViewEntity.PreviewPower = "1";

        //是否是编缉模式
        if (Instance.IsEdit == true) {
            var result = Instance.BackgroundInvoke.UpdateActionPower(Global.toJSON(actionPowerViewEntity));
            if (parseInt(result.value, 10) > 0) {
                alert("修改模块成功！");
                $(this).dialog("close");
                document.location.reload();
            } else {
                alert("修改模块失败！");
            }
        } else {
            var result = Instance.BackgroundInvoke.InsertActionPower(Global.toJSON(actionPowerViewEntity));

            if (parseInt(result.value, 10) > 0) {
                alert("添加模块成功！");
                $(this).dialog("close");
                document.location.reload();
            } else {
                alert("添加模块失败！");
            }
        }
    }

    //清空弹出层填写内容方法
    this.Clear = function () {
        $(":text", container).val("");
        $(":checkbox", container).removeAttr("checked");
    }
}