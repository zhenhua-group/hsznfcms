﻿var swfu;
var projectNameRepeat = false;
var projectNameRepeat1 = false;
$(document).ready(function () {
    var flag = true;

    $("#btn_showadd").click(function () {
        $('#div_add').show();
        $("#div_edit").hide();
        Clear();
        window.scrollTo(0, document.body.scrollHeight);

    });
    $("#btn_Cancle1").click(function () {
        $('#div_add').hide("slow");
        window.scrollTo(0, 0);
    });

    $("#btn_Cancle2").click(function () {

        $("#div_edit").hide("slow");
        window.scrollTo(0, 0);
    });
  

    $(".cls_select").live("click", function () {
        $("#div_add").hide();
        $("#div_edit").show();
        $("#ctl00_ContentPlaceHolder1_drp_unit1").attr("disabled", "disabled");
        $("#ctl00_ContentPlaceHolder1_drp_operationType1").attr("disabled", "disabled");
        $("#ctl00_ContentPlaceHolder1_hid_id").val($(this).attr("typeid"));
        $("#ctl00_ContentPlaceHolder1_txt_name1").val($(this).parent().parent().find("TD").eq(1).text());

        var costGroup = $.trim($(this).attr("costGroup"));

        if (costGroup == 0) {
            $("#ctl00_ContentPlaceHolder1_drp_unit1 option[value=0]").attr("selected", "selected");
        } else if (costGroup == 1) {
            $("#ctl00_ContentPlaceHolder1_drp_unit1 option[value=250]").attr("selected", "selected");
        } else if (costGroup == 2) {
            $("#ctl00_ContentPlaceHolder1_drp_unit1 option[value=250]").attr("selected", "selected");
        } else if (costGroup == 3) {
            $("#ctl00_ContentPlaceHolder1_drp_unit1 option[value=252]").attr("selected", "selected");
        } else {
            $("#ctl00_ContentPlaceHolder1_drp_unit1 option[value=239]").attr("selected", "selected");
        }

        var isInput = $.trim($(this).attr("isInput"));
        $.each($("#ctl00_ContentPlaceHolder1_drp_operationType1 option"), function (index, option) {
            if ($.trim($(option).val()) == isInput) {
                $(option).attr("selected", "selected");
            }
        });

    });


    $("#ctl00_ContentPlaceHolder1_txt_name").blur(function () {

        if ($("#ctl00_ContentPlaceHolder1_drp_unit").val() == "-1") {
            alert("请选择成本类型！");
            projectNameRepeat = true;
            return false;
        }

        //名称
        if ($("#ctl00_ContentPlaceHolder1_txt_name").val() == "") {
            alert("请填写成本名称！");
            projectNameRepeat = true;
            return false;
        }

        if ($("#ctl00_ContentPlaceHolder1_drp_operationType").val() == "-1") {
            alert("请选择操作类型！");
            projectNameRepeat = true;
            return false;
        }

        if ($("#ctl00_ContentPlaceHolder1_txt_name").length > 0) {
            $.get("../HttpHandler/CommHandler.ashx", { "action": "costtypeRepeat", "costunit": $("#ctl00_ContentPlaceHolder1_drp_unit option:selected").val(), "costName": $("#ctl00_ContentPlaceHolder1_txt_name").val(), "id": "", "isInput": $("#ctl00_ContentPlaceHolder1_drp_operationType option:selected").val() }, function (result) {
                if (result == "1") {
                    projectNameRepeat = true;
                } else {
                    projectNameRepeat = false;
                }
            });
        }
    });


    $("#ctl00_ContentPlaceHolder1_txt_name1").blur(function () {

        if ($("#ctl00_ContentPlaceHolder1_drp_unit1").val() == "-1") {
            alert("请选择成本类型！");
            projectNameRepeat1 = true;
            return false;
        }

        if ($("#ctl00_ContentPlaceHolder1_drp_operationType1").val() == "-1") {
            alert("请选择操作类型！");
            projectNameRepeat1 = true;
            return false;
        }

        //名称
        if ($("#ctl00_ContentPlaceHolder1_txt_name1").val() == "") {
            alert("请填写成本名称！");
            projectNameRepeat1 = true;
            return false;
        }

        if ($("#ctl00_ContentPlaceHolder1_txt_name1").length > 0) {
            $.get("../HttpHandler/CommHandler.ashx", { "action": "costtypeRepeat", "costunit": $("#ctl00_ContentPlaceHolder1_drp_unit1 option:selected").val(), "costName": $("#ctl00_ContentPlaceHolder1_txt_name1").val(), "id": $("#ctl00_ContentPlaceHolder1_hid_id").val(), "isInput": $("#ctl00_ContentPlaceHolder1_drp_operationType1 option:selected").val() }, function (result) {
                if (result == "1") {
                    projectNameRepeat1 = true;
                } else {
                    projectNameRepeat1 = false;
                }
            });
        }
    });

    //添加
    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {

        var msg = "";
        if ($("#ctl00_ContentPlaceHolder1_drp_unit").val() == "-1") {
            msg += "请选择成本类型！<br/>";
        }

        if ($("#ctl00_ContentPlaceHolder1_drp_operationType").val() == "-1") {
            msg += "请选择操作类型！<br/>";
        }

        //名称
        if ($("#ctl00_ContentPlaceHolder1_txt_name").val() == "") {
            msg += "请填写成本名称！<br/>";
        }

        if (projectNameRepeat) {
            msg += "名称已存在，请重新添加！<br/>";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //修改
    $("#ctl00_ContentPlaceHolder1_btn_edit").live("click", function () {

        var msg = "";

        if ($("#ctl00_ContentPlaceHolder1_drp_unit1").val() == "-1") {
            msg += "请选择成本类型！<br/>";
        }

        //名称
        if ($("#ctl00_ContentPlaceHolder1_txt_name1").val() == "") {
            msg += "成本名称不能为空！<br/>";
        }

        if ($("#ctl00_ContentPlaceHolder1_drp_operationType1").val() == "-1") {
            msg += "请选择操作类型！<br/>";
        }


        if (projectNameRepeat1) {
            msg += "名称已存在，请重新添加！<br/>";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }

    });


});

function cls_select_Edit() {
    window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
}

function Clear() {
    $("#ctl00_ContentPlaceHolder1_drp_unit").val("-1");
    $("#ctl00_ContentPlaceHolder1_drp_operationType").val("-1");
    $("#ctl00_ContentPlaceHolder1_txt_name").val("");

}

