﻿function LeftMenuConfigBymaster(container) {
    this.IsEdit = false;
    this.Container = container;
    this.BackgroundInvoke = TG.Web.SystemSet.LeftMenuConfigBymaster;
    this.LeftMenuSysNo = 0;
    var Instance = this;

    this.AddButton = $("#AddLeftMenu");
    this.AddButton.click(function () {
        Instance.IsEdit = false;
        Instance.Clear();
        //Instance.PopAreaMenuDetail("添加左侧菜单栏");
        $("h4").text("添加左侧菜单栏");
    });
    this.EditButton = $("#editLeftMenuLinkButton");
    this.EditButton.live("click", function () {
        Instance.IsEdit = true;
        var sysNo = $(this).attr("leftmenusysno");
        Instance.LeftMenuSysNo = sysNo;
        Instance.Clear();
        Instance.InitLeftMenuDetail(sysNo);
        //Instance.PopAreaMenuDetail("修改左侧菜单栏");
        $("h4").text("修改左侧菜单栏");
    });
    this.DeleteButton = $("a[id=deleteLeftMetnuLinkButton]");
    this.DeleteButton.live("click", function () {
        if (confirm("确认删除此条功能权限？")) {
            var sysNo = $(this).attr("leftmenusysno");
            var result = Instance.BackgroundInvoke.DeleteLeftMenu(sysNo).value;
            if (parseInt(result, 10) > 0) {
                alert("删除成功");
                document.location.reload();
            } else {
                alert("删除失败");
            }
        }
    });
    this.BtnsaveActionPower = $("#btn_saveActionPower");
    this.BtnsaveActionPower.click(function () {
        Instance.SaveLeftMenu();

    })
    this.InitLeftMenuDetail = function (sysNo) {
        var result = Instance.BackgroundInvoke.GetLeftMenuViewEntity(sysNo).value;
        if (result != null && result.length > 0) {
            var leftMenuViewEntity = Global.evalJSON(result);
            $("#DescriptionTextBox", container).val(leftMenuViewEntity.Description);
            $("#ElementIDTextBox", container).val(leftMenuViewEntity.ElementID);
            $("#typeDropDownList option[value=" + leftMenuViewEntity.Type + "]", container).attr("selected", "selected");

            var roleSysNoArray = leftMenuViewEntity.Role.split(",");
            $.each(roleSysNoArray, function (index, item) {
                $(":checkbox[name=roleCheckBox][value=" + item + "]").parent().attr("class", "checked");
            });
        }
    }

    //    this.PopAreaMenuDetail = function (title) {
    //        $("#PopAreaLeftMenuDetail").dialog({
    //            autoOpen: false,
    //            modal: true,
    //            width: 530,
    //            height: 200,
    //            resizable: false,
    //            title: title,
    //            buttons:
    //	                        {
    //	                            "保存": Instance.SaveLeftMenu,
    //	                            "关闭": function () { $(this).dialog("close"); }
    //	                        }
    //        }).dialog("open");
    //    }

    this.SaveLeftMenu = function () {
        var myDate = new Date();
        var leftMenuViewEntity = {
            "SysNo": Instance.LeftMenuSysNo,
            "Description": $("#DescriptionTextBox", container).val(),
            "ElementID": $("#ElementIDTextBox", container).val(),
            "RoleNameString": "",
            "Type": $("#typeDropDownList", container).val(),
            "TypeString": "",
            "InUser": 1,
            "InUserName": "",
            "InDate": myDate.getYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate()
        };
        var roleSysNoString = "";
        $.each($(":checkbox[name=roleCheckBox]", "span[class=checked]", container), function (index, chexkBox) {
            roleSysNoString += chexkBox.value + ",";
        });


        roleSysNoString = roleSysNoString.substring(0, roleSysNoString.length - 1);

        leftMenuViewEntity.Role = roleSysNoString;

        if (Instance.IsEdit == true) {
            var result = Instance.BackgroundInvoke.UpdateLeftMenu(Global.toJSON(leftMenuViewEntity));
            if (parseInt(result.value, 10) > 0) {
                alert("修改成功！");

                var href1 = $.trim(document.location.href.replace('#', ''));
                document.location.href = href1 + "?d=" + new Date();
            } else {
                alert("修改失败！");
            }
        } else {
            var result = Instance.BackgroundInvoke.InsertLeftMenu(Global.toJSON(leftMenuViewEntity));
            if (parseInt(result.value, 10) > 0) {
                alert("添加成功！");

                var href = $.trim(document.location.href.replace('#', ''));
                document.location.href = href + "?d=" + new Date();
            } else {
                alert("添加失败！");
            }
        }
    }

    this.Clear = function () {
        $(":text", container).val("");
        $(":checkbox", container).removeAttr("checked");
    }
}