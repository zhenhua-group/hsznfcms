﻿$(document).ready(function () {


    //前缀
    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
        if ($(":text[title=prefix][value='']").length > 0) {
            alert("编号前缀不能为空！");
            return false;
        }
        //开始编号
        if ($(":text[title=start][value='']").length > 0) {
            alert("开始编号不能为空！");
            return false;
        }
        else {
            var cout = 0;
            $(":text[title=start][value!='']").each(function (i) {
                var reg = /^[1-9]\d*$/;

                if (!reg.test($(this).val())) {
                    cout++;
                }
            });
            if (cout > 0) {
                alert("开始编号请输入数字！");
                return false;
            }
        }
        //结束编号
        if ($(":text[title=end][value='']").length > 0) {
            alert("结束编号不能为空！");
            return false;
        }
        else {
            var cout = 0;
            $(":text[title=end][value!='']").each(function (i) {
                var reg = /^[1-9]\d*$/;

                if (!reg.test($(this).val())) {
                    cout++;
                }
            });
            if (cout > 0) {
                alert("结束编号请输入数字！");
                return false;
            }
        }
    });
    //    //加载默认项
    //    if ($("#hid_flag").val() == "1") {
    //        $(".cls_type").show();
    //        $(".cls_notype").hide();
    //    }
    //    else {
    //        $(".cls_type").hide();
    //        $(".cls_notype").show();
    //    }
    //    //按项目
    //    $("#rbtn_type").click(function() {
    //        $(".cls_type").show();
    //        $(".cls_notype").hide();
    //        $("#hid_flag").val("1");
    //    });
    //    //不按项目
    //    $("#rbtn_notype").click(function() {
    //        $(".cls_type").hide();
    //        $(".cls_notype").show();
    //        $("#hid_flag").val("0");
    //    });
});