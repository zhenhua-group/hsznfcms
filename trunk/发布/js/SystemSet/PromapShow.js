﻿
$(document).ready(function () {
    CommonControl.SetFormWidth();
    //行背景
    $("#grid_unit tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#grid_unit tr:even").css({ background: "White" });
    //添加
    $("#btn_showadd").click(function () {
        $("#div_add").show("slow");
        $("#div_edit").hide("slow");
    });
    $(".cls_select").click(function () {
        $("#div_add").hide("slow");
        $("#div_edit").show("slow");
    });
    //添加
    $("#btn_save").click(function () {
        var msg = "";
        if ($("#txt_cityName").val() == "") {
            msg += "城市名不能为空！<br/>";
        }
        else {
            if ($("#txt_toaspx").val().length > 25) {
                msg += "城市页面太长！<br/>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //绑定值
    $("#drp_typelist").change(function () {
        $("#txt_cityName").val($(this).val());
    })
    //修改
    $("#btn_edit").click(function () {
        var msg = "";
        if ($("#txt_dicName0").val() == "") {
            msg += "城市名不能为空！<br/>";
        }
        else {
            if ($("#txt_dicType0").val().length > 25) {
                msg += "城市对应页面太长！<br/>";
            }
        }
        if ($("#txt_dicDescript").val().length > 50) {
            msg += "字典介绍过长！";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //选择信息
    $(".cls_select").click(function () {
        $("#lbl_dicid").text($(this).parent().parent().find("TD").eq(1).text());
        $("#hid_dicid").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txt_dicName0").val($(this).parent().parent().find("TD").eq(2).text());
        $("#txt_dicType0").val($(this).parent().parent().find("TD").eq(3).text());
        //        $("#txt_dicType0").val($(this).parent().parent.find("TD").eq(3).text());
        $("#txt_dicDescript").val($(this).parent().parent().find("TD").eq(4).text());
    });
    //删除是判断有无记录选中
    $("#btn_DelCst").click(function () {
        if ($(".cls_chk :checkbox[checked='checked']").length == 0) {
            jAlert("请选择要删除的字典！", "提示");
            return false;
        }
        //判断是否要删除
        return confirm("是否要删除字典信息？");
    });
    //选择城市显示
    $(".choose").click(function () {
        var cityname = $(this).parent().parent().find("TD").eq(2).text();
        var linkaspx = $(this).parent().parent().find("TD").eq(3).text();
        $.get("../HttpHandler/ProMapshow.ashx", { "name": cityname, "aspx": linkaspx }, function (date) {
            if (date == "ok") {
                alert("修改成功地图显示为" + cityname + "地图");
                $("#lab").html(cityname);
            }
        });
    });
    //全选
    $("#chk_All").click(function () {
        if ($(this).get(0).checked) {
            $(".cls_chk :checkbox").attr("checked", "checked");
        }
        else {
            $(".cls_chk  :checkbox").attr("checked", false);
        }
    });
});
  