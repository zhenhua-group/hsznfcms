﻿
$(document).ready(function () {

    //绑定设置
    $("a[data-action]", "#tbData").click(function () { 
        SaveMemLevel($(this),"");
    });
    //全部设置
    $("#btn_save").click(function () {
        SaveMemLevel($("#tbData>tbody>tr"), "all");
    });
});

//保存用户多部门
function SaveMemLevel(obj, ftype) {
    // 获取多部门
    var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
    var action = "savememlevel";
    var arr = new Array();
    if (ftype == "all") {
        //tr循环插入
        obj.each(function (i, k) {
            var entityData = {
                ID: '0',
                mem_ID: $.trim($(k).children().eq(1).text()),
                mem_Year: $.trim($(k).children().eq(0).text()) == '无' ? $("#ctl00_ContentPlaceHolder1_drp_year").val() : $.trim($(k).children().eq(0).text()),
                mem_Level: $(k).children().eq(5).find('select option:selected').val()
            };
            arr.push(entityData);
        });
    }
    else
    {
       var entityData = {
            ID: '0',
            mem_ID: $.trim(obj.parent().parent().children().eq(1).text()),
            mem_Year: $.trim(obj.parent().parent().children().eq(0).text()) == '无' ? $("#ctl00_ContentPlaceHolder1_drp_year").val() : $.trim(obj.parent().parent().children().eq(0).text()),
            mem_Level: obj.parent().parent().children().eq(5).find('select option:selected').val()
        };
        arr.push(entityData);
    }
   


    var data = { "action": action, "data": JSON.stringify(arr) };

    $.post(url, data, function (result) {

        if (result) {
            if (result == "1") {
                alert('人员级别设置成功！');
                $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
            }
            else if (result == "2") {
                alert('部分人员级别设置成功！');
                $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
            }
            else {
                alert('人员级别设置失败！');
            }
        }
    });
}