﻿
$(document).ready(function () {

    //绑定设置
    $("a[data-action]", "#tbData").click(function () {
        SaveMemLevel($(this),"");
    });
    //全部设置
    $("#btn_save").click(function () {
        SaveMemLevel($("#tbData>tbody>tr"),"all");
    });

    $(":text", "#tbData").click(function () {

        $(this).select();
    });
});

//保存用户多部门
function SaveMemLevel(obj,ftype) {
    // 获取多部门
    var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
    var action = "savememout";
    var arr = new Array();
    if (ftype == "all")
    {
        var message = "";
        //tr循环插入
        obj.each(function (i, k) {
            var memname = $.trim($(k).children().eq(0).text());
            var setcount = $(k).children().eq(7).find(':text').val();
            if (setcount == '') {
                setcount = 0;
            }
            else {
                var reg = /^[+-]?([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$/;
                if (!reg.test(setcount)) {
                    message = message + memname + '请输入数字！\n';                  
                   // return false;
                }
            }
            if (message=="") {
                var entityData = {
                    ID: '0',
                    Mem_ID: $.trim($(k).children().eq(0).attr("data-memid")),
                    Mem_Name: $.trim($(k).children().eq(0).text()),
                    Yue: $.trim($(k).children().eq(5).text()),
                    SetCount: setcount,
                    SetYear: $("#ctl00_ContentPlaceHolder1_drp_year").val()
                };

                arr.push(entityData);
            }
            
        });

        if (message != "")
        {
            alert(message);
            return false;
        }
    }
    else
    {
        var setcount = obj.parent().parent().children().eq(7).find(':text').val();
        if (setcount == '') {
            setcount = 0;
        }
        else {
            var reg = /^[+-]?([0-9]*\.?[0-9]+|[0-9]+\.?[0-9]*)([eE][+-]?[0-9]+)?$/;
            if (!reg.test(setcount)) {
                alert('请输入数字！');
                return false;
            }
        }

      var entityData = {
            ID: '0',
            Mem_ID: $.trim(obj.parent().parent().children().eq(0).attr("data-memid")),
            Mem_Name: $.trim(obj.parent().parent().children().eq(0).text()),
            Yue: $.trim(obj.parent().parent().children().eq(5).text()),
            SetCount: setcount,
            SetYear: $("#ctl00_ContentPlaceHolder1_drp_year").val()
        };
        arr.push(entityData);
    }   


    var data = { "action": action, "data": JSON.stringify(arr) };

    $.post(url, data, function (result) {

        if (result) {
            if (result == "1") {
                alert('设置成功！');
                $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
            }
            else if (result == "2") {
                alert('部分设置成功！');
                $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
            }            
            else {
                alert('设置失败！');
            }
        }
    });
}