﻿$(document).ready(function () {
    $("#btn_save").click(function () {
        var arr = new Array();
        var year=$("#ctl00_ContentPlaceHolder1_drpYear").val();
        $("#ctl00_ContentPlaceHolder1_grid_mem>tbody>tr:gt(0)").each(function (j, r) {
           
            //人员id
            var mid = $.trim($(r).children(":eq(0)").text());
            $(r).find("table").each(function (i,k) {
                var shang = $(k).find("tr:eq(0)").find("input").val();
                var xia = $(k).find("tr:eq(1)").find("input").val();
                var obj = {
                    "mem_ID": mid,
                    "attend_month": (i + 1),
                    "ToWork": shang,
                    "OffWork": xia,
                    "attend_year": year
                };
                arr.push(obj);
            });
        });
        //
        var url = "/HttpHandler/SystemSet/PersonAttendSetHandler.ashx";
        var jsonObj = Global.toJSON(arr);
       
      
        $.post(url, { "data": jsonObj }, function (result) {
            if (result == "1")
            {
                alert("保存成功！");
            }
            else {
                alert("保存失败！");
            }
        });
    });

});