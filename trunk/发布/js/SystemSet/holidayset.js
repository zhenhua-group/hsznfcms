﻿$(document).ready(function () {
    $("#btn_save").click(function () {
        var arr = new Array();
        $("#ctl00_ContentPlaceHolder1_grid_mem>tbody>tr:gt(0)").each(function (j, r) {
           
            //人员id
            var mid = $.trim($(r).children(":eq(0)").text());
            var obj = {
                "mem_ID": mid,
                "attend_month": "0",
                "ToWork": $.trim($(r).children(":eq(2)").find("input").val()),
                "OffWork": $("#ctl00_ContentPlaceHolder1_drp_year3").val(),
                "attend_year":"0"
            };
            arr.push(obj);
        });
        //
        var url = "/HttpHandler/SystemSet/PersonAttendSetHandler.ashx";
        var jsonObj = Global.toJSON(arr);
       
      
        $.post(url, { "data": jsonObj }, function (result) {
            if (result == "1")
            {
                alert("保存成功！");
            }
            else {
                alert("保存失败！");
            }
        });
    });

});