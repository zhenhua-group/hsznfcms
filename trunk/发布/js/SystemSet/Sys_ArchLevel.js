﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //隔行变色
    $("#grid_level tr:even").css({ background: "White" });
    //添加
    $("#btn_showadd").click(function () {
        $("#div_add").show("slow");
        $("#div_edit").hide("slow");
    });
    $(".cls_select").click(function () {
        $("#div_add").hide("slow");
        $("#div_edit").show("slow");
    });
    //添加
    $("#btn_save").click(function () {
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        //职称名称
        if ($("#txt_archName").val() == "") {
            msg += "职称名不能为空！<br/>";
        }
        else {
            if ($("#txt_archName").val().length > 15) {
                msg += "职称名太长！<br/>";
            }
        }
        //分配比例
        if ($("#txt_allotprt").val() == "") {
            msg += "分配比例不能为空！<br/>";
        }
        else {
            if (!reg_math.test($("#txt_allotprt").val())) {
                msg += "请输入数字！<br/>";
            }
        }
        //排序
        if ($("#drp_order").val() == "0") {
            msg += "请选择职位排序！<br/>";
        }
        //描述
        if ($("#txt_archInfo").val().length > 30) {
            msg += "部门简介输入过长！";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //修改
    $("#btn_edit").click(function () {
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        //职称名称
        if ($("#txt_archName0").val() == "") {
            msg += "职称名不能为空！<br/>";
        }
        else {
            if ($("#txt_archName0").val().length > 15) {
                msg += "职称名太长！<br/>";
            }
        }
        //分配比例
        if ($("#txt_allotprt0").val() == "") {
            msg += "分配比例不能为空！<br/>";
        }
        else {
            if (!reg_math.test($("#txt_allotprt0").val())) {
                msg += "请输入数字！<br/>";
            }
        }
        //排序
        if ($("#drp_order0").val() == "0") {
            msg += "请选择职位排序！<br/>";
        }
        //描述
        if ($("#txt_archInfo0").val().length > 30) {
            msg += "部门简介输入过长！";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //选择信息
    $(".cls_select").click(function () {
        $("#lbl_archid").text($.trim($(this).parent().parent().find("TD").eq(1).text()));
        $("#hid_archid").val($.trim($(this).parent().parent().find("TD").eq(1).text()));
        $("#txt_archName0").val($.trim($(this).parent().parent().find("TD").eq(2).text()));
        $("#txt_allotprt0").val($.trim($(this).parent().parent().find("TD").eq(3).text()));
        $("#drp_order0").val($.trim($(this).parent().parent().find("TD").eq(4).text()));
        $("#txt_archInfo0").val($.trim($(this).parent().parent().find("TD").eq(5).text()));
        var showname = $.trim($(this).parent().parent().find("TD").eq(4).text());
        if (showname == "显示") {
            $("#raBtnshow").attr("checked", "checked");
        }
        else {
            $("#raBtnHidd").attr("checked", "checked");
        }
    });
    //删除是判断有无记录选中
    $("#btn_DelCst").click(function () {
        if ($(".cls_chk :checkbox[checked]").length == 0) {
            jAlert("请选择要删除的部门！", "提示");
            return false;
        }
        //判断是否要删除
        return confirm("是否要删除部门信息？");
    });
    //全选
    $("#chk_All").click(function () {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });

});