﻿$(document).ready(function () {

    CommonControl.SetFormWidth();
    if (flag == "1") {
        $("#tb_edit").show();
        $("#tb_show").hide();
    }
    $("#btn_pwd").click(function () {
        var msg = "";
        if ($("#ctl00_ContentPlaceHolder1_txt_old").val() == "") {
            msg += "旧密码不能为空！<br/>";
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_new").val() == "") {
            msg += "新密码不能为空！<br/>";
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_new1").val() != $("#ctl00_ContentPlaceHolder1_txt_new").val()) {
            msg += "两次密码输入不一致！";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            $.ajax({
                type: "POST",
                url: "../HttpHandler/LoginHandler.ashx?action=chgpwd",
                data: "oldpwd=" + escape($("#ctl00_ContentPlaceHolder1_txt_old").val()) + "&newpwd=" + escape($("#ctl00_ContentPlaceHolder1_txt_new").val()),
                success: function (rlt) {
                    if (rlt == "yes") {
                        jAlert("密码修改成功！", "提示");
                        $("#ctl00_ContentPlaceHolder1_txt_old").val("");
                        $("#ctl00_ContentPlaceHolder1_txt_new").val("");
                        $("#ctl00_ContentPlaceHolder1_txt_new1").val("");
                    }
                    else if (rlt == "no") {
                        jAlert("原始密码不正确！", "提示");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    jAlert("数据库连接错误！", "提示");
                }
            });
        }

    });
    //保存验证
    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {

        var msg = "";
        if ($("#ctl00_ContentPlaceHolder1_lbl_Name0").val() == "") {
            msg += "用户名不能为空！<br/>";
        }
        if ($("#ctl00_ContentPlaceHolder1_lbl_Name0").val().length > 5) {
            msg += "用户名太长<br/>";
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_memLogin0").val() == "") {
            msg += "登录名不能为空！<br/>";
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_memLogin0").val().length > 10) {
            msg += "用户登录名太长！";
        }
        if (isCn($("#ctl00_ContentPlaceHolder1_txt_memLogin0").val())) {
            msg += "登录名中不能含有中文！<br/>";
        }
        if ($("#ctl00_ContentPlaceHolder1_lbl_mobile0").val() != "") {
            if ($("#ctl00_ContentPlaceHolder1_lbl_mobile0").val().length > 11) {
                msg += "手机号输入过长！";
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_lbl_phone0").val() != "") {
            if ($("#ctl00_ContentPlaceHolder1_lbl_phone0").val().length > 11) {
                msg += "固定电话输入过长！";
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_lbl_email0").val() != "") {
            var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            if (!reg.test($("#lbl_email0").val())) {
                msg += "邮箱格式不正确";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    // 返回
    $("#btn_back").click(function () {
        window.document.location.href = "../SystemSet/SetPersinalBymaster.aspx";
    });
    //表单行变色
    $(".cls_show_cst_jiben tr:odd").attr("style", "background-color:#FFF");
    
});
//判断是否是中文
function isCn(fData) {
    var reg = /[\u4E00-\u9FA5]/;
    if (reg.test(fData)) {
        return true;
    }
    else {
        return false;
    }
}