﻿
// 系统用户信息编辑
var SysUserInfo = function () {


    var pageLoad = function () {

        //查看
        if ($("#hidAction").val() == "chk") {
            $(":input", "#tbUserForm").attr("disabled", true).attr("style", "border:none;");
            $(":selected", "#tbUserForm").attr("disabled", true).attr("style", "border:none;");

            $("a[data-toggle]").hide();
            $("a[data-type]").hide();

            $("#btnSave").hide();
            $("#btnSave2").hide();

            $("#txt_mem_Name").hide();
            $("#txt_mem_Login").hide();

            return false;
        }

        //输入身份证号，获取信息
        $("#txt_mem_Code").blur(function () {

            var obj = $(this);

            //cardid
            var cardid = obj.val();

            if ($.trim(cardid) != '') {
                //身份证合法
                if (isCard(cardid)) {
                    //性别
                    $("#txt_mem_Sex").text(getSex(cardid));
                    //出生日期
                    $("#txt_mem_Birthday").text(getBirthDay(cardid));
                    //年龄
                    $("#txt_mem_Age").text(getAge(cardid));
                    //获取户籍所在地
                    getAddress(cardid);

                    //正确
                    obj.next('span').css("color", "#000");
                }
                else {
                    obj.next('span').css("color", "red");
                }
            }
            else {

                //性别
                $("#txt_mem_Sex").text('');
                //出生日期
                $("#txt_mem_Birthday").text('');
                //年龄
                $("#txt_mem_Age").text('');
                //户籍地址
                $("#txt_mem_CodeAddr").val('');

                obj.next('span').css("color", "red");
            }
        });

        //工作年限  工作间隔时间
        $("#txt_mem_WorkDiff").blur(function () {

            var reg = /^[1-9]d*|0$/;
            if (!reg.test($(this).val())) {
                $(this).val('0');
            }
            getWorkDiff();
            getHolidayBase();
        });
        //工作时间
        $("#txt_mem_WorkTime").blur(function () {
            getWorkDiff();
            getHolidayBase();
        });

        //入司年限  入司间隔时间
        $("#txt_mem_InCompanyDiff").blur(function () {

            var reg = /^[1-9]d*|0$/;
            if (!reg.test($(this).val())) {
                $(this).val('0');
            }
            getWorkCompanyDiff();
            getHolidayBase();
        });
        //入司年限
        $("#txt_mem_InCompanyTime").blur(function () {
            getWorkCompanyDiff();
            getHolidayBase();
        });
        //岗位工龄
        $("#txt_mem_InCompanyTemp").change(function () {
            getHolidayBase();
        });

        $("#txt_mem_GongZi").blur(function () {

            var reg = /^[1-9]d*|0$/;
            if (!reg.test($(this).val())) {
                $(this).val('0');
            }
            if (!reg.test($("#txt_mem_Yufa").val())) {
                $("#txt_mem_Yufa").val('0');
            }
            getYingFa();
        });
        $("#txt_mem_Jiaotong").change(function () {

            var reg = /^[1-9]d*|0$/;
            if (!reg.test($("#txt_mem_GongZi").val())) {
                $("#txt_mem_GongZi").val('0');
            }
            getYingFa();
        });
        $("#txt_mem_Tongxun").change(function () {

            var reg = /^[1-9]d*|0$/;
            if (!reg.test($("#txt_mem_GongZi").val())) {
                $(this).val('0');
            }
            getYingFa();
        });
        //预发奖金
        $("#txt_mem_Yufa").blur(function () {

            var reg = /^[1-9]d*|0$/;
            if (!reg.test($(this).val())) {
                $(this).val('0');
            }

            if (!reg.test($("#txt_mem_GongZi").val())) {
                $("#txt_mem_GongZi").val('0');
            }
            getYingFa();
        });

        //合同性质
        $("#txt_mem_CoperationType").change(function () {
            getCoperationEndDate($(this).val());
        });

        //选择合同日期
        $("#txt_mem_CoperationDate").blur(function () {
            getCoperationEndDate($("#txt_mem_CoperationType").val());
        });
        //标示是否为编辑
        if ($("#hidEditUserSysNo").val() != "0") {
            $("#txt_mem_Login").hide();
            $("#txt_mem_Name").hide();

            $("#sp_mem_Login").show();
            $("#sp_mem_Name").show();
        }

        //保存
        $("#btnSave").click(function () {
            //添加
            if ($("#hidEditUserSysNo").val() == "0") {
                saveUserinfo(0);
            }
            else {
                saveUserinfo(1);
            }
        });
        $("#btnSave2").click(function () {
            if ($("#hidEditUserSysNo").val() == "0") {
                saveUserinfo(0);
            }
            else {
                saveUserinfo(1);
            }
        });

        //职务调整
        $("#btn_AddChange").click(function () {
            saveRoleChange();
        });
        //工资
        $("#btn_GongZiChange").click(function () {
            saveGongZiChange();
        });
        //公积金
        $("#btn_SheBaoChange").click(function () {
            saveSheBaoChange();
        });
        //奖励
        $("#btn_Jiangli").click(function () {
            saveJiangli();
        });
        //惩罚
        $("#btn_Chengfa").click(function () {
            saveChengfa();
        });

        //签订情况
        $("#btn_SignCpr").click(function () {
            saveSignCpr($("#hidSignCprID").val());
        });

        //合同签订编辑绑定
        $("#tbSignCprList>tbody a[data-type=edit]").live('click', function () {

            var obj = $(this);
            var id = obj.attr("data-id");
            var $tds = obj.parent().parent().find("td");

            $("#txt_mem_CoperationDate2", "#tbSignCprAdd").val($tds.eq(0).text());
            var type = $tds.eq(1).text();
            if (type == "签订") {
                type = "0";
            }
            else if (type == "续签") {
                type = "1";
            }
            else if (type == "无固定") {
                type = "2";
            }
            $("#txt_mem_CoperationType2", "#tbSignCprAdd").val(type);
            $("#txt_mem_CoperationDateEnd2", "#tbSignCprAdd").val($tds.eq(2).text());
            $("#txt_mem_CoperationSub2", "#tbSignCprAdd").val($tds.eq(3).text());
            //编辑id
            $("#hidSignCprID").val(id);
        });

        //合同删除绑定
        $("#tbSignCprList>tbody a[data-type=del]").live('click', function () {


            if (confirm('确定要删除合同签订情况吗？')) {

                var obj = $(this);

                var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
                var action = "delsigncpr";
                var data = { "action": action, "id": obj.attr("data-id") };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result.result == "success") {

                            obj.parent().parent().remove();
                        }
                    }
                }, 'json');
            }
        });

        //转正时间
        $("#txt_mem_ZhuanZheng").blur(function () {

            if ($(this).val() != "") {
                getHolidayBase();
            }
        });


    }

    //根据身份证判断男女
    var getSex = function (cardId) {
        if (cardId.length == 18) {
            return parseInt(cardId.substr(14, 3)) % 2 == 0 ? "女" : "男";
        }
        else {
            return parseInt(cardId.substr(12, 1)) % 2 == 0 ? "女" : "男";
        }
    }
    //获取出生日期
    var getBirthDay = function (cardId) {
        return cardId.substring(6, 10) + "-" + cardId.substring(10, 12) + "-" + cardId.substring(12, 14);
    }
    //获取年龄
    var getAge = function (cardId) {

        var curDate = new Date();
        var month = curDate.getMonth() + 1;
        var day = curDate.getDate();
        var age = curDate.getFullYear() - cardId.substring(6, 10) - 1;
        if (cardId.substring(10, 12) < month || cardId.substring(10, 12) == month && cardId.substring(12, 14) <= day) {
            age++;
        }

        return age;
    }
    //获取户籍所在地
    var getAddress = function (cardId) {

        var code = cardId.substr(0, 6);

        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "getaddress";

        var data = { "action": action, "cardcode": code };

        $.get(url, data, function (result) {

            if (result) {
                //设置成功！
                $("#txt_mem_CodeAddr").val(result.result);
            }

        });
    }
    //计算工作年限
    var getWorkDiff = function () {

        var startwork = $("#txt_mem_WorkTime").val();
        var workdiff = $("#txt_mem_WorkDiff").val();

        if (startwork != '') {
            var curDate = new Date();
            //计算月
            var year = curDate.getFullYear();
            var month = curDate.getMonth();
            var curMonthCount = parseInt(year * 12) + parseInt(month + 1);

            //入职年月
            var inDate = new Date(startwork);
            var inyear = inDate.getFullYear();
            var inmonth = inDate.getMonth();

            var inMonthCount = parseInt(inyear * 12) + parseInt(inmonth + 1);

            var monthDiff = curMonthCount - inMonthCount;

            var result = 0;
            if (!(monthDiff % 12 < 6)) {
                result = Math.floor(monthDiff / 12) + 1;
            }
            else {
                result = Math.floor(monthDiff / 12);
            }

            if (workdiff != '') {
                result = result - parseInt(workdiff);
            }

            $("#txt_mem_WorkYear").text(result);
        }
        else {
            $("#txt_mem_WorkYear").text('0');
        }
    }
    //计算入司工作年限
    var getWorkCompanyDiff = function () {
        var startwork = $("#txt_mem_InCompanyTime").val();
        var workdiff = $("#txt_mem_InCompanyDiff").val();

        if (startwork != '') {
            var curDate = new Date();
            //计算月
            var year = curDate.getFullYear();
            var month = curDate.getMonth();
            var curMonthCount = parseInt(year * 12) + parseInt(month + 1);

            //入职年月
            var inDate = new Date(startwork);
            var inyear = inDate.getFullYear();
            var inmonth = inDate.getMonth();

            var inMonthCount = parseInt(inyear * 12) + parseInt(inmonth + 1);

            var monthDiff = curMonthCount - inMonthCount;

            var result = 0;
            if (!(monthDiff % 12 < 6)) {
                result = Math.floor(monthDiff / 12) + 1;
            }
            else {
                result = Math.floor(monthDiff / 12);
            }

            if (workdiff != '') {
                result = result - parseInt(workdiff);
            }

            $("#txt_mem_InCompanyYear").text(result);
        }
        else {
            $("#txt_mem_InCompanyYear").text('0');
        }
    }
    //计算休假基数
    var getHolidayBase = function () {

        //工作年限
        var workYear = parseInt($("#txt_mem_WorkYear").text());
        //入司年限
        var inYear = parseInt($("#txt_mem_InCompanyYear").text());
        //工位工龄
        var gongling = parseInt($("#txt_mem_InCompanyTemp").val());

        var count = workYear + inYear + gongling;
        $("#txt_mem_Holidaybase").text(count);
        //转正年份
        if ($("#txt_mem_ZhuanZheng").val() != "") {
            var zhuanZDate = new Date($("#txt_mem_ZhuanZheng").val());
            var nowDate = new Date();

            var zhuanYear = zhuanZDate.getFullYear();
            var zhuanMonth = zhuanZDate.getMonth() + 1;

            var nowYear = nowDate.getFullYear();
            if (zhuanYear == nowYear) {
                if (zhuanMonth >= 1 && zhuanMonth < 4) {
                    $("#txt_mem_HolidayYear").text('5');
                }
                else if (zhuanMonth >= 4 && zhuanMonth < 7) {
                    $("#txt_mem_HolidayYear").text('4');
                } else if (zhuanMonth >= 7 && zhuanMonth < 10) {
                    $("#txt_mem_HolidayYear").text('3');
                } else if (zhuanMonth >= 10 && zhuanMonth <= 12) {
                    $("#txt_mem_HolidayYear").text('2');
                }
            }
            else {
                if (count > 1 && count < 4) {
                    $("#txt_mem_HolidayYear").text('7');
                }
                else if (count >= 5 && count <= 10) {
                    $("#txt_mem_HolidayYear").text('10');
                }
                else if (count >= 11 && count <= 17) {
                    $("#txt_mem_HolidayYear").text('13');
                }
                else if (count >= 18 && count <= 30) {
                    $("#txt_mem_HolidayYear").text('16');
                }
                else if (count >= 31) {
                    $("#txt_mem_HolidayYear").text('20');
                }
            }
        }
    }
    //计算应发工资
    var getYingFa = function () {

        var gongzi = parseFloat($("#txt_mem_GongZi").val());
        var jiaotong = parseFloat($("#txt_mem_Jiaotong").val());
        var tongxun = parseFloat($("#txt_mem_Tongxun").val());
        var yufa = parseFloat($("#txt_mem_Yufa").val());

        var allCount = gongzi + jiaotong + tongxun + yufa;
        $("#txt_mem_YingFa").text(allCount);
    }
    //显示合同截止日期
    var getCoperationEndDate = function (status) {
        if (status == "0" || status == "1") {

            if ($("#txt_mem_CoperationDate").val() != "") {
                var date = new Date($("#txt_mem_CoperationDate").val());

                var year = date.getFullYear();
                var month = date.getMonth() + 1;
                var day = date.getDay() - 1;



                var result = (year + 1) + '-' + month + '-' + day
                $("#txt_mem_CoperationDateEnd").text(result);
            }
        }
        else {
            $("#txt_mem_CoperationDateEnd").text('');
        }
    }
    //保存信息
    var saveUserinfo = function (isedit) {
        //包含零的正整数
        var reg = /^[1-9]d*|0$/;
        var msg = "";
        var val1 = $("#txt_mem_Login").val();
        if (val1.length == 0) {
            msg += "用户名不能为空<br/>";
        }
        else {
            var reg2 = /^[a-zA-Z]\w{3,15}$/;
            if (!reg2.test(val1)) {
                msg += "用户名只能是字母数字下划线，并且以字母开头！<br/>";
            }
        }
        var val2 = $("#txt_mem_Name").val();
        if (val2.length == 0) {
            msg += "姓名不能为空<br/>";
        }
        var val3 = $("#txt_mem_Password").val();
        if (val3.length == 0) {
            msg += "密码不能为空<br/>";
        }
        else {
            if (val3.length < 3) {
                msg += "密码太简单！";
            }
        }
        var val4 = $("#txt_mem_isFired").val();
        var val5 = $("#txt_mem_Unit_ID").val();
        var val6 = $("#txt_mem_Sex").text();
        var val7 = $("#txt_mem_Speciality_ID").val();
        var val8 = $("#txt_RoleIdMag").val();
        var val9 = $("#txt_RoleIdTec").val();
        var val10 = $("#txt_mem_CodeAddr").val();
        var val12 = $("#txt_mem_Code").val();
        //if (val12.length == 0) {
        //    msg += "身份证号不能为空<br/>";
        //}
        var val13 = $("#txt_mem_Birthday").text();
        if (val13.length == 0) {
            msg += "出生年月不能为空<br/>";
        }
        var val14 = $("#txt_mem_Mobile").val();
        if (val14.length == 0) {
            //msg += "联系电话不能为空<br/>";
        }
        else {
            if (!reg.test($("#txt_mem_Mobile").val())) {
                msg += "联系电话格式错误<br/>";
            }
        }
        var val15 = $("#txt_mem_Telephone").val();
        if (val15.length > 0) {
            if (!reg.test($("#txt_mem_Telephone").val())) {
                msg += "分机号输入格式错误不能为空<br/>";
            }
        }
        var val16 = $("#txt_mem_Age").text();
        var val17 = $("#txt_mem_School").val();
        var val18 = $("#txt_mem_SchLevel").val();
        var val19 = $("#txt_mem_SchSpec").val();
        var val20 = $("#txt_mem_SchOutDate").val();
        var val21 = $("#txt_mem_School2").val();
        var val22 = $("#txt_mem_SchLevel2").val();
        var val23 = $("#txt_mem_SchSpec2").val();
        var val24 = $("#txt_mem_SchOutDate2").val();
        var val25 = $("#txt_mem_ArchLevel").val();
        var val26 = $("#txt_mem_ArchLevelTime").val();
        var val27 = $("#txt_mem_ArchReg").val();
        var val28 = $("#txt_mem_ArchRegTime").val();
        var val29 = $("#txt_mem_ArchRegAddr").val();
        var val30 = $("#txt_mem_WorkTime").val();
        if (val30.length == 0) {
            //msg += "参加工作时间不能为空<br/>";
        }
        var val31 = $("#txt_mem_InCompanyTime").val();
        if (val31.length == 0) {
            //msg += "入司时间不能为空<br/>";
        }
        var val32 = $("#txt_mem_WorkDiff").val();
        if (val32.length == 0) {
            //msg += "工作间隔时间不能为空<br/>";
        }
        var val33 = $("#txt_mem_InCompanyDiff").val();
        if (val33.length == 0) {
            //msg += "入司间隔时间不能为空<br/>";
        }
        var val34 = $("#txt_mem_WorkDiffSub").val();
        var val35 = $("#txt_mem_InCompanyDiffSub").val();
        var val36 = $("#txt_mem_WorkYear").text();
        var val37 = $("#txt_mem_InCompanyYear").val();
        var val38 = $("#txt_mem_WorkTemp").val();
        var val39 = $("#txt_mem_InCompanyTemp").val();
        var val40 = $("#txt_mem_FileAddr").val();
        if (val40.length == 0) {
            //msg += "档案所在地不能为空<br/>";
        }
        var val41 = $("#txt_mem_Holidaybase").text();
        var val42 = $("#txt_mem_HolidayYear").text();
        var val43 = $("#txt_mem_MagPishi").val();
        var val44 = $("#txt_mem_ZhuanZheng").val();
        var val45 = $("#txt_mem_GongZi").val();
        if (val45.length == 0) {
            // msg += "工资不能为空<br/>";
        }
        var val46 = $("#txt_mem_Jiaotong").val();
        var val47 = $("#txt_mem_Tongxun").val();
        var val48 = $("#txt_mem_SheBaobase").val();
        if (val48.length == 0) {
            //msg += "社保基数不能为空<br/>";
        }
        var val49 = $("#txt_mem_Gongjijin").val();
        if (val49.length == 0) {
            //msg += "公积金不能为空<br/>";
        }
        var val50 = $("#txt_mem_GongjijinSub").val();
        var val51 = $("#txt_mem_Yufa").val();
        if (val51.length == 0) {
            //msg += "预发奖金不能为空<br/>";
        }
        var val52 = $("#txt_mem_CoperationDate").val();
        var val53 = $("#txt_mem_CoperationType").val();
        var val54 = $("#txt_mem_CoperationDateEnd").val();
        var val55 = $("#txt_mem_CoperationSub").val();
        var val56 = $("#txt_mem_OutCompany").val();
        var val57 = $("#txt_mem_OutCompanyDate").val();
        var val58 = $("#txt_mem_OutCompanyType").val();
        var val59 = $("#txt_mem_OutCompanySub").val();
        var val60 = $("#txt_mem_YearCharge").val();

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }

        //tgMember对象
        var tgMember = {
            "mem_ID": $("#hidEditUserSysNo").val(),
            "mem_Login": isedit == 0 ? $("#txt_mem_Login").val() : $("#sp_mem_Login").text(),
            "mem_Name": isedit == 0 ? $("#txt_mem_Name").val() : $("#sp_mem_Name").text(),
            "mem_Password": $("#txt_mem_Password").val(),
            "mem_Birthday": $("#txt_mem_Birthday").text(),
            "mem_Sex": $("#txt_mem_Sex").text(),
            "mem_Unit_ID": $("#txt_mem_Unit_ID").val(),
            "mem_Speciality_ID": $("#txt_mem_Speciality_ID").val(),
            "mem_Principalship_ID": $("#txt_mem_Principalship_ID").val(),
            "mem_Telephone": $("#txt_mem_Telephone").val(),
            "mem_Mobile": $("#txt_mem_Mobile").val(),
            "mem_isFired": $("#txt_mem_isFired").val()
        };

        //管理角色
        var tgMemRole = {
            "ID": 0,
            "mem_Id": $("#hidEditUserSysNo").val(),
            "RoleIdTec": $("#txt_RoleIdTec").val(),
            "RoleIdMag": $("#txt_RoleIdMag").val(),
            "Weights": 0,
            "mem_Unit_ID": $("#txt_mem_Unit_ID").val()
        };

        //进入公司信息
        var tgMemExt = {
            "ID": 0,
            "mem_ID": $("#hidEditUserSysNo").val(),
            "mem_Name": $("#txt_mem_Name").val(),
            "mem_InTime": $("#txt_mem_InCompanyTime").val(),
            "mem_OutTime": $("#txt_mem_OutCompanyDate").val()
        };

        //职称信息
        var tgMemRelation = {
            "ID": 0,
            "Mem_ID": $("#hidEditUserSysNo").val(),
            "ArchLevel_ID": $("#txt_mem_ArchReg").val()
        };

        //扩展信息
        var tgMemExtInfo = {
            "ID": 0,
            "mem_ID": $("#hidEditUserSysNo").val(),
            "mem_Code": $("#txt_mem_Code").val(),
            "mem_CodeAddr": $("#txt_mem_CodeAddr").val(),
            "mem_Age": $("#txt_mem_Age").text() != '' ? $("#txt_mem_Age").text() : '0',
            "mem_School": $("#txt_mem_School").val(),
            "mem_SchLevel": $("#txt_mem_SchLevel").val(),
            "mem_SchSpec": $("#txt_mem_SchSpec").val(),
            "mem_SchOutDate": $("#txt_mem_SchOutDate").val(),
            "mem_School2": $("#txt_mem_School2").val(),
            "mem_SchLevel2": $("#txt_mem_SchLevel2").val(),
            "mem_SchSpec2": $("#txt_mem_SchSpec2").val(),
            "mem_SchOutDate2": $("#txt_mem_SchOutDate2").val(),
            "mem_ArchLevel": $("#txt_mem_ArchLevel").val(),
            "mem_ArchLevelTime": $("#txt_mem_ArchLevelTime").val(),
            "mem_ArchReg": $("#txt_mem_ArchReg").val(),
            "mem_ArchRegTime": $("#txt_mem_ArchRegTime").val(),
            "mem_ArchRegAddr": $("#txt_mem_ArchRegAddr").val(),
            "mem_WorkTime": $("#txt_mem_WorkTime").val(),
            "mem_WorkDiff": $("#txt_mem_WorkDiff").val(),
            "mem_WorkDiffSub": $("#txt_mem_WorkDiffSub").val(),
            "mem_WorkYear": $("#txt_mem_WorkYear").text(),
            "mem_WorkTemp": $("#txt_mem_WorkTemp").val(),
            "mem_InCompanyTime": $("#txt_mem_InCompanyTime").val(),
            "mem_InCompanyDiff": $("#txt_mem_InCompanyDiff").val(),
            "mem_InCompanyDiffSub": $("#txt_mem_InCompanyDiffSub").val(),
            "mem_InCompanyYear": $("#txt_mem_InCompanyYear").text(),
            "mem_InCompanyTemp": $("#txt_mem_InCompanyTemp").val(),
            "mem_FileAddr": $("#txt_mem_FileAddr").val(),
            "mem_Holidaybase": $("#txt_mem_Holidaybase").text(),
            "mem_HolidayYear": $("#txt_mem_HolidayYear").text(),
            "mem_MagPishi": $("#txt_mem_MagPishi").val(),
            "mem_ZhuanZheng": $("#txt_mem_ZhuanZheng").val(),
            "mem_GongZi": $("#txt_mem_GongZi").val() != '' ? $("#txt_mem_GongZi").val() : '0',
            "mem_Jiaotong": $("#txt_mem_Jiaotong").val(),
            "mem_Tongxun": $("#txt_mem_Tongxun").val(),
            "mem_YingFa": $("#txt_mem_YingFa").text() != '' ? $("#txt_mem_YingFa").text() : '0',
            "mem_SheBaobase": $("#txt_mem_SheBaobase").val(),
            "mem_Gongjijin": $("#txt_mem_Gongjijin").val() != '' ? $("#txt_mem_Gongjijin").val() : '0',
            "mem_GongjijinSub": $("#txt_mem_GongjijinSub").val(),
            "mem_Yufa": $("#txt_mem_Yufa").val() != '' ? $("#txt_mem_Yufa").val() : '0',
            "mem_CoperationDate": $("#txt_mem_CoperationDate").val(),
            "mem_CoperationType": $("#txt_mem_CoperationType").val(),
            "mem_CoperationDateEnd": $("#txt_mem_CoperationDateEnd").text(),
            "mem_CoperationSub": $("#txt_mem_CoperationSub").val(),
            "mem_OutCompany": $("#txt_mem_OutCompany").val(),
            "mem_OutCompanyDate": $("#txt_mem_OutCompanyDate").val(),
            "mem_OutCompanyType": $("#txt_mem_OutCompanyType").val(),
            "mem_OutCompanySub": $("#txt_mem_OutCompanySub").val(),
            "mem_YearCharge": $("#txt_mem_YearCharge").val() != '' ? $("#txt_mem_YearCharge").val() : '0'
        };

        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "saveData";
        if (isedit == 1) {
            action = "editData";
        }
        var data = { "action": action, "tgmember": JSON.stringify(tgMember), "tgmemrole": JSON.stringify(tgMemRole), "tgmemext": JSON.stringify(tgMemExt), "tgmemrelation": JSON.stringify(tgMemRelation), "tgmemextinfo": JSON.stringify(tgMemExtInfo) };

        $.post(url, data, function (result) {

            if (result) {
                if (result.result == "exsit") {
                    jAlert('用户账户已经存在！', "提示");
                    return false;
                }
                else if (result.result == "success") {
                    jAlert('用户账户已经创建成功！', "提示");
                    $("#btnSave2").attr("disabled", true);
                    $("#btnSave").attr("disabled", true);
                }
                else if (result.result == "edit") {
                    jAlert('用户信息更新成功！', "提示");
                }
            }
        }, 'json');
    }
    //职务调整
    var saveRoleChange = function () {

        var msg = "";
        var chgCount = 0;
        var val1 = $("#txt_ChangeDate1").val();
        var val2 = $("#txt_RoleIdMag2").val();
        var val3 = $("#txt_RoleIdMag3").val();

        if (val2 == val3) {
            chgCount++;
        }
        var val4 = $("#txt_RoleIdTec2").val();
        var val5 = $("#txt_RoleIdTec3").val();
        if (val4 == val5) {
            chgCount++;
        }
        var val6 = $("#txt_mem_Jiaotong2").val();
        var val7 = $("#txt_mem_Jiaotong3").val();
        if (val6 == val7) {
            chgCount++;
        }
        var val8 = $("#txt_mem_Tongxun2").val();
        var val9 = $("#txt_mem_Tongxun3").val();
        if (val8 == val9) {
            chgCount++;
        }

        if (chgCount == 4) {
            msg += "没有做出任何项的调整<br/>";
            jAlert(msg, '提示');
            return false;
        }
        //memID
        var val10 = $("#hidEditUserSysNo").val();
        var val11 = $("#hidUserSysNo").val();

        var jsonModel =
        {
            "ID": 0,
            "mem_ID": val10,
            "MagRole": val2,
            "MagRoleName": $("#txt_RoleIdMag2").val() == "-1" ? "" : $("#txt_RoleIdMag2 option:selected").text(),
            "MagRole2": val3,
            "MagRoleName2": $("#txt_RoleIdMag3").val() == "-1" ? "" : $("#txt_RoleIdMag3 option:selected").text(),
            "TecRole": val4,
            "TecRoleName": $("#txt_RoleIdTec2").val() == "-1" ? "" : $("#txt_RoleIdTec2 option:selected").text(),
            "TecRole2": val5,
            "TecRoleName2": $("#txt_RoleIdTec3").val() == "-1" ? "" : $("#txt_RoleIdTec3 option:selected").text(),
            "JiaoTong": val6,
            "JiaoTong2": val7,
            "Tongxun": val8,
            "Tongxun2": val9,
            "ChangeDate": $("#txt_ChangeDate1").val(),
            "InsertUserID": val11
        };


        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "savepositionchg";
        var data = { "action": action, "data": JSON.stringify(jsonModel) };

        $.post(url, data, function (result) {

            if (result) {
                if (result.result == "success") {

                    //$("#tbPositionChangeList>tbody>tr").remove();
                    //表容器
                    var $tr = $("<tr></tr>");
                    var $td = "";
                    $td += "<td>" + jsonModel.MagRoleName + "</td>";
                    $td += "<td>" + jsonModel.MagRoleName2 + "</td>";
                    $td += "<td>" + jsonModel.TecRoleName + "</td>";
                    $td += "<td>" + jsonModel.TecRoleName2 + "</td>";
                    $td += "<td>" + jsonModel.JiaoTong + "</td>";
                    $td += "<td>" + jsonModel.JiaoTong2 + "</td>";
                    $td += "<td>" + jsonModel.Tongxun + "</td>";
                    $td += "<td>" + jsonModel.Tongxun2 + "</td>";
                    $td += "<td>" + jsonModel.ChangeDate + "</td>";

                    $tr.append($td);
                    $("#tbPositionChangeList>tbody").append($tr);
                    //关闭
                    $("#btn_AddChangeClose").trigger('click');

                    //改变现在表单
                    $("#txt_RoleIdMag").val(jsonModel.MagRole2);
                    $("#txt_RoleIdTec").val(jsonModel.TecRole2);
                    $("#txt_mem_Jiaotong").val(jsonModel.JiaoTong2);
                    $("#txt_mem_Tongxun").val(jsonModel.Tongxun2);
                }
            }
        }, 'json');
    }
    //工资调整
    var saveGongZiChange = function () {

        var reg = /^[1-9]d*|0$/;
        var msg = "";
        var chgCount = 0;
        var val1 = $("#txt_mem_GongZi2").val();
        var val2 = $("#txt_mem_GongZi3").val();
        if (val1 == val2) {
            chgCount++;
        }
        if (val2.length == 0) {
            msg += "调整工资不能为空！<br/>";
        }
        else {
            if (!reg.test(val2)) {
                msg += "调整工资请输入数字！<br/>";
            }
        }
        var val3 = $("#txt_mem_Yufa2").val();
        var val4 = $("#txt_mem_Yufa3").val();

        if (val3 == val4) {
            chgCount++;
        }
        if (val4.length == 0) {
            msg += "预发奖金不能为空！<br/>";
        }
        else {
            if (!reg.test(val4)) {
                msg += "预发奖金请输入数字！<br/>";
            }
        }

        if (chgCount == 2) {
            msg = "没有做出任何调整<br/>";
        }

        if (msg != "") {
            jAlert(msg, '提示');
            return false;
        }

        //memID
        var val10 = $("#hidEditUserSysNo").val();
        var val11 = $("#hidUserSysNo").val();

        var jsonModel =
        {
            "ID": 0,
            "mem_ID": val10,
            "mem_GongZi": parseInt(val1),
            "mem_GongZi2": parseInt(val2),
            "mem_Yufa": parseInt(val3),
            "mem_Yufa2": parseInt(val4),
            "ChangeDate": $("#txt_ChangeDate2").val(),
            "InsertUserID": val11
        };


        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "savegongzichg";
        var data = { "action": action, "data": JSON.stringify(jsonModel) };

        $.post(url, data, function (result) {

            if (result) {
                if (result.result == "success") {

                    //$("#tbPositionChangeList>tbody>tr").remove();
                    //表容器
                    var $tr = $("<tr></tr>");
                    var $td = "";
                    $td += "<td>" + jsonModel.mem_GongZi + "</td>";
                    $td += "<td>" + jsonModel.mem_GongZi2 + "</td>";
                    $td += "<td>" + jsonModel.mem_Yufa + "</td>";
                    $td += "<td>" + jsonModel.mem_Yufa2 + "</td>";
                    $td += "<td>" + jsonModel.ChangeDate + "</td>";

                    $tr.append($td);
                    $("#tbChargeChangeList>tbody").append($tr);
                    //关闭
                    $("#btn_GongZiChangeClose").trigger('click');
                    
                    //改变现有值
                    $("#txt_mem_GongZi").val(jsonModel.mem_GongZi2);
                    $("#txt_mem_Yufa").val(jsonModel.mem_Yufa2);

                    //更新应发 2018年1月2日 
                    getYingFa();
                }
            }
        }, 'json');
    }
    //社保调整
    var saveSheBaoChange = function () {

        var reg = /^[1-9]d*|0$/;
        var msg = "";
        var chgCount = 0;
        var val1 = $("#txt_mem_SheBaobase2").val();
        var val2 = $("#txt_mem_SheBaobase3").val();
        if (val1 == val2) {
            chgCount++;
        }
        if (val2.length == 0) {
            msg += "社保基数不能为空！<br/>";
        }
        else {
            if (!reg.test(val2)) {
                msg += "社保基数请输入数字！<br/>";
            }
        }
        var val3 = $("#txt_mem_Gongjijin2").val();
        var val4 = $("#txt_mem_Gongjijin3").val();

        if (val3 == val4) {
            chgCount++;
        }
        if (val4.length == 0) {
            msg += "公积金不能为空！<br/>";
        }
        else {
            if (!reg.test(val4)) {
                msg += "公积金请输入数字！<br/>";
            }
        }

        if (chgCount == 2) {
            msg = "没有做出任何调整<br/>";
        }

        if (msg != "") {
            jAlert(msg, '提示');
            return false;
        }

        //memID
        var val10 = $("#hidEditUserSysNo").val();
        var val11 = $("#hidUserSysNo").val();

        var jsonModel =
        {
            "ID": 0,
            "mem_ID": val10,
            "mem_SheBaobase": parseInt(val1),
            "mem_SheBaobase2": parseInt(val2),
            "mem_Gongjijin": parseInt(val3),
            "mem_Gongjijin2": parseInt(val4),
            "ChangeDate": $("#txt_ChangeDate3").val(),
            "InsertUserID": val11
        };


        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "saveshebaochg";
        var data = { "action": action, "data": JSON.stringify(jsonModel) };

        $.post(url, data, function (result) {

            if (result) {
                if (result.result == "success") {

                    //$("#tbPositionChangeList>tbody>tr").remove();
                    //表容器
                    var $tr = $("<tr></tr>");
                    var $td = "";
                    $td += "<td>" + jsonModel.mem_SheBaobase + "</td>";
                    $td += "<td>" + jsonModel.mem_SheBaobase2 + "</td>";
                    $td += "<td>" + jsonModel.mem_Gongjijin + "</td>";
                    $td += "<td>" + jsonModel.mem_Gongjijin2 + "</td>";
                    $td += "<td>" + jsonModel.ChangeDate + "</td>";

                    $tr.append($td);
                    $("#tbGongjijinChangeList>tbody").append($tr);
                    //关闭
                    $("#btn_SheBaoChangeClose").trigger('click');

                    //改变现有值
                    $("#txt_mem_SheBaobase").val(jsonModel.mem_SheBaobase2);
                    $("#txt_mem_Gongjijin").val(jsonModel.mem_Gongjijin2);
                }
            }
        }, 'json');
    }

    //录入奖励
    var saveJiangli = function () {
        var reg = /^[1-9]d*|0$/;
        var msg = "";
        var val1 = $("#txt_mem_JiangliWhy").val();
        if (val1.length == 0) {
            msg += "奖励原因不能为空！<br/>";
        }
        var val2 = $("#txt_mem_JiangliSub").val();
        if (val2.length == 0) {
            msg += "奖励备注不能为空！<br/>";
        }
        var val3 = $("#txt_ChangeDate4").val();
        if (val3.length == 0) {
            msg += "奖励时间不能为空！<br/>";
        }

        if (msg != "") {
            jAlert(msg, '提示');
            return false;
        }
        //memID
        var val10 = $("#hidEditUserSysNo").val();
        var val11 = $("#hidUserSysNo").val();

        var jsonModel =
        {
            "ID": 0,
            "mem_ID": val10,
            "mem_JiangliDate": $("#txt_ChangeDate4").val(),
            "mem_JiangliWhy": val1,
            "mem_JiangliSub": val2,
            "InsertUserID": val11
        };


        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "savejiangli";
        var data = { "action": action, "data": JSON.stringify(jsonModel) };

        $.post(url, data, function (result) {

            if (result) {
                if (result.result == "success") {

                    //$("#tbPositionChangeList>tbody>tr").remove();
                    //表容器
                    var $tr = $("<tr></tr>");
                    var $td = "";
                    $td += "<td>" + jsonModel.mem_JiangliDate + "</td>";
                    $td += "<td>" + jsonModel.mem_JiangliWhy + "</td>";
                    $td += "<td>" + jsonModel.mem_JiangliSub + "</td>";

                    $tr.append($td);
                    $("#tbJiangliChangeList>tbody").append($tr);
                    //关闭
                    $("#btn_JiangliClose").trigger('click');
                }
            }
        }, 'json');
    }

    //录入惩罚
    var saveChengfa = function () {

        var msg = "";
        var val1 = $("#txt_mem_ChengfaWhy").val();
        if (val1.length == 0) {
            msg += "惩罚原因不能为空！<br/>";
        }
        var val2 = $("#txt_mem_ChengfaSub").val();
        if (val2.length == 0) {
            msg += "惩罚备注不能为空！<br/>";
        }

        var val3 = $("#txt_ChangeDate5").val();
        if (val3.length == 0) {
            msg += "惩罚时间不能为空！<br/>";
        }

        if (msg != "") {
            jAlert(msg, '提示');
            return false;
        }
        //memID
        var val10 = $("#hidEditUserSysNo").val();
        var val11 = $("#hidUserSysNo").val();

        var jsonModel =
        {
            "ID": 0,
            "mem_ID": val10,
            "mem_ChengfaDate": $("#txt_ChangeDate5").val(),
            "mem_ChengfaWhy": val1,
            "mem_ChengfaSub": val2,
            "InsertUserID": val11
        };


        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "savechengfa";
        var data = { "action": action, "data": JSON.stringify(jsonModel) };

        $.post(url, data, function (result) {

            if (result) {
                if (result.result == "success") {

                    //$("#tbPositionChangeList>tbody>tr").remove();
                    //表容器
                    var $tr = $("<tr></tr>");
                    var $td = "";
                    $td += "<td>" + jsonModel.mem_ChengfaDate + "</td>";
                    $td += "<td>" + jsonModel.mem_ChengfaWhy + "</td>";
                    $td += "<td>" + jsonModel.mem_ChengfaSub + "</td>";

                    $tr.append($td);
                    $("#tbChengfaChangeList>tbody").append($tr);
                    //关闭
                    $("#btn_ChengfaClose").trigger('click');
                }
            }
        }, 'json');

    }

    //录入合同签订情况
    var saveSignCpr = function (id) {

        var msg = "";
        var val1 = $("#txt_mem_CoperationDate2").val();
        if (val1.length == 0) {
            msg += "合同签订时间不能为空！<br/>";
        }
        var val2 = $("#txt_mem_CoperationDateEnd2").val();
        if (val2.length == 0) {
            msg += "合同到期时间不能为空！<br/>";
        }

        if (msg != "") {
            jAlert(msg, '提示');
            return false;
        }
        //memID
        var val10 = $("#hidEditUserSysNo").val();
        var val11 = $("#hidUserSysNo").val();

        var jsonModel =
        {
            "ID": parseInt(id) == 0 ? 0 : parseInt(id),
            "mem_ID": val10,
            "mem_CoperationDate": val1,
            "mem_CoperationType": $("#txt_mem_CoperationType2").val(),
            "mem_CoperationDateEnd": val2,
            "mem_CoperationSub": $("#txt_mem_CoperationSub2").val(),
            "insertUserID": val11
        };


        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "savesigncpr";
        var data = { "action": action, "data": JSON.stringify(jsonModel) };

        $.post(url, data, function (result) {

            if (result) {
                if (result.result == "success") {

                    //$("#tbPositionChangeList>tbody>tr").remove();
                    //表容器
                    if (parseInt(id) == 0) {//新建
                        var $tr = $("<tr></tr>");
                        var $td = "";
                        $td += "<td>" + jsonModel.mem_CoperationDate + "</td>";
                        var type = jsonModel.mem_CoperationType;
                        if (type == "0") {
                            type = "签订";
                        }
                        else if (type == "1") {
                            type = "续签";
                        }
                        else if (type == "2") {
                            type = "无固定";
                        }

                        $td += "<td>" + type + "</td>";
                        $td += "<td>" + jsonModel.mem_CoperationDateEnd + "</td>";
                        $td += "<td>" + jsonModel.mem_CoperationSub + "</td>";
                        $td += "<td style=\"display:none\"><a href=\"#addSignCpr\" data-toggle=\"modal\" data-id=\"" + result.id + "\" >编辑</a></td>";
                        $td += "<td><a href=\"###\" data-id=\"" + result.id + "\">删除</a></td>";

                        $tr.append($td);
                        $("#tbSignCprList>tbody").append($tr);
                        //关闭
                        $("#btn_SignCprClose").trigger('click');
                    }
                    else {
                        var $tr = $("<tr></tr>");
                        var $td = "";
                        $td += "<td>" + jsonModel.mem_CoperationDate + "</td>";
                        var type = jsonModel.mem_CoperationType;
                        if (type == "0") {
                            type = "签订";
                        }
                        else if (type == "1") {
                            type = "续签";
                        }
                        else if (type == "2") {
                            type = "无固定";
                        }

                        $td += "<td>" + type + "</td>";
                        $td += "<td>" + jsonModel.mem_CoperationDateEnd + "</td>";
                        $td += "<td>" + jsonModel.mem_CoperationSub + "</td>";
                        $td += "<td style=\"display:none\"><a href=\"#addSignCpr\" data-toggle=\"modal\" data-id=\"" + result.id + "\">编辑</a></td>";
                        $td += "<td><a href=\"###\" data-id=\"" + result.id + "\">删除</a></td>";

                        $tr.append($td);
                        $("#tbSignCprList>tbody a[data-id=" + result.id + "]").parent().parent().remove();
                        $("#tbSignCprList>tbody").append($tr);
                        //关闭
                        $("#btn_SignCprClose").trigger('click');
                    }

                    //清空
                    $("#hidSignCprID").val("0");
                }
            }
        }, 'json');
    }

    //最严判断身份证合法
    var isCard = function (idCard) {
        //15位和18位身份证号码的正则表达式
        var regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;

        //如果通过该验证，说明身份证格式正确，但准确性还需计算
        if (regIdCard.test(idCard)) {
            if (idCard.length == 18) {
                var idCardWi = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //将前17位加权因子保存在数组里
                var idCardY = new Array(1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
                var idCardWiSum = 0; //用来保存前17位各自乖以加权因子后的总和
                for (var i = 0; i < 17; i++) {
                    idCardWiSum += idCard.substring(i, i + 1) * idCardWi[i];
                }

                var idCardMod = idCardWiSum % 11;//计算出校验码所在数组的位置
                var idCardLast = idCard.substring(17);//得到最后一位身份证号码

                //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
                if (idCardMod == 2) {
                    if (idCardLast == "X" || idCardLast == "x") {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                    if (idCardLast == idCardY[idCardMod]) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
    }

    return {
        Init: function () {
            pageLoad();
        }
    };
}();