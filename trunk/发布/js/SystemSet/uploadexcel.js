﻿$(function () {
    var img;
    //添加经理
    $("#addUserBtn").live("click", function () {
        img = $(this);
        selectRoleUser.Clear();
        // selectRoleUser.Init();
        selectRoleUser.Dom.SearchButton.click();
    });
    //同步初始按钮
    if ($("#ctl00_ContentPlaceHolder1_DropDownList2").val() == "0") {
        $("#ctl00_ContentPlaceHolder1_btn_Save").hide();
    }
    else
    {
        $("#ctl00_ContentPlaceHolder1_btn_Save").show();
    }
   
    //绑定人员数据
    var selectRoleUser = new SelectRoleUser($("#chooseUserMain", $("#chooseUserMain")));
    $("#btn_SaveUser").click(function () {
        selectRoleUser.SelectedUserDone(chooseUserCallback, img);

    });
    //返回方法
    var chooseUserCallback = function (selectedUserCheckboxs, img) {
        //取得选择的用户
        var userSysNoString = "";
        //取得该td下所有的用户
        var userSpanli = $(img).parent().siblings("td:first");
        //判断是否有重复项存在
        $.each(selectedUserCheckboxs, function (index, checkBox) {
            var flag = true;
            $.each(userSpanli.children("span[id=userSpan]"), function (index1, item) {
                if ($(item).attr("usersysno") == $(checkBox).val()) {
                    flag = false;
                    return false;
                }
            });
            //判断是否有重复的人员
            if (flag == true) {
                var spanString = "<span id=\"userSpan\" usersysno=\"" + $(checkBox).val() + "\"  style=\"margin-right: 1px;\">" + $(checkBox).attr("username") + "<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
                $(img).parent().siblings("td:first").append(spanString);
                userSysNoString = userSysNoString + $(checkBox).val() + ",";
            }
        });
        //保存数据到数据库
        //取得所有用户		
        //var userSysNoString = "";
        //$.each(userSpanli.children("span[id=userSpan]"), function (index, item) {
        //    userSysNoString += $(item).attr("usersysno") + ",";
        //});
        if (userSysNoString!="") {
            userSysNoString = userSysNoString.substring(0, userSysNoString.length - 1);
        }
        var year = $("#ctl00_ContentPlaceHolder1_DropDownList1").val();
        var month = $("#ctl00_ContentPlaceHolder1_DropDownList2").val();
        //权限人员id
        var roleSysNo = $(img).attr("sysno");
        var roleName = $(img).attr("rel");
        var result = TG.Web.SystemSet.UploadExcel.UpdateRoleUsers(userSysNoString, roleSysNo, roleName,year,month, "add");
        //成功的场合
        if (result.value != null && result.value.length > 0) {
            if (parseInt(result.value, 10) <= 0) {
                alert("添加用户失败，请联系管理员！");
            }
        }
        $("#user_close").click();
    };

    //删除用户
    $("img[name=deleteUserImgActionBtn]").live("click", function () {
      //  if ($(this).parents("td:first").children("span").length > 1) {
            if (confirm("您确认是否删除该用户？")) {
               // //取得该Img父级同辈所有usreSpan
               // var userSpans = $(this).parents("span[id=userSpan]:first").siblings("span[id=userSpan]");
                var userSysNoString = $(this).parents("span[id=userSpan]:first").attr("usersysno");
              //  $.each(userSpans, function (index, item) {
               //     userSysNoString += $(item).attr("usersysno") + ",";
              //  });
              //  userSysNoString = userSysNoString.substring(0, userSysNoString.length - 1);

                //取得RoleSysNo
                var roleSysNo = $(this).parents("td").next().children("img").attr("sysno");
                var rolename = $(this).parents("td").next().children("img").attr("rel");
                var year = $("#ctl00_ContentPlaceHolder1_DropDownList1").val();
                var month = $("#ctl00_ContentPlaceHolder1_DropDownList2").val();
                //调用后台方法
                var result = TG.Web.SystemSet.UploadExcel.UpdateRoleUsers(userSysNoString, roleSysNo, rolename, year, month, "del");
                if (parseInt(result.value, 10) <= 0) {
                    alert("删除用户失败！");
                } else {
                    $(this).parents("span[id=userSpan]:first").remove();
                }
            }
       // }
    });
    //删除权限人员
    $("a[id=delete]").live("click", function () {
        if (confirm("您确认是否删除权限人员？")) {
            var managerid = $(this).attr("rel");
            var year = $("#ctl00_ContentPlaceHolder1_DropDownList1").val();
            var month = $("#ctl00_ContentPlaceHolder1_DropDownList2").val();
            var result = TG.Web.SystemSet.UploadExcel.UpdateRoleUsers("", managerid, "", year, month, "del");
            if (parseInt(result.value, 10) <= 0) {
                alert("删除用户失败！");
            } else {
                $(this).parent().parent().remove();
                window.location.reload();
            }
        }
    });
});