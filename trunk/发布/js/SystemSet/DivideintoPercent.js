﻿
var projectNameRepeat = false;
var projectNameRepeat1 = false;
$(function () {

    $('#ctl00_ContentPlaceHolder1_grid_cost_ctl01_chkAll').click(function () {

        var checks = $('#ctl00_ContentPlaceHolder1_grid_cost :checkbox');

        if ($('#ctl00_ContentPlaceHolder1_grid_cost_ctl01_chkAll').attr("checked") == "checked") {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.setAttribute("class", "checked");
                checks[i].setAttribute("checked", "checked");
            }
        }
        else {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.removeAttribute("class");
                checks[i].removeAttribute("checked");
            }
        }
    });


    $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {

        if ($("#ctl00_ContentPlaceHolder1_grid_cost :checkbox:checked").length == 0) {
            alert("请选择要删除的院所分成比例！");
            return false;
        }
        //判断是否要删除
        return confirm("是否要删除选中的院所分成比例吗？");
    });

    $("#btn_showadd").click(function () {
        $('#div_add').show();
        $("#div_edit").hide();
        Clear();
    });
    $("#btn_Cancle1").click(function () {
        $('#div_add').hide("slow");
    });

    $("#btn_Cancle11").click(function () {
        $("#div_edit").hide("slow");
    });

    $(".cls_select").live("click", function () {
        $("#div_add").hide();
        $("#div_edit").show();
        // $("#ctl00_ContentPlaceHolder1_drp_unit1").attr("disabled", "disabled");
        //$("#ctl00_ContentPlaceHolder1_drp_allotyear0").attr("disabled", "disabled");
        $("#ctl00_ContentPlaceHolder1_hid_id").val($(this).attr("typeid"));
        $("#ctl00_ContentPlaceHolder1_txt_allot0").val($(this).parent().parent().find("TD").eq(4).text());

        var year = $.trim($(this).parent().parent().find("TD").eq(3).text());
        $.each($("#ctl00_ContentPlaceHolder1_drp_allotyear0 option"), function (index, option) {
            if ($.trim($(option).val()) == year) {
                $(option).attr("selected", "selected");
            }
        });
        var unit = $.trim($(this).attr("unitid"));
        $.each($("#ctl00_ContentPlaceHolder1_drp_unit1 option"), function (index, option) {
            if ($.trim($(option).val()) == unit) {
                $(option).attr("selected", "selected");
            }
        });

    });

    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
        var msg = "";
        //生产部门
        if ($("#ctl00_ContentPlaceHolder1_drp_unit0").val() == "-1") {
            msg += "请选择要生产部门！<br/>";
        }
        //年份
        if ($("#ctl00_ContentPlaceHolder1_drp_allotyear").val() == "-1") {
            msg += "请选择年份！<br/>";
        }

        if ($("#ctl00_ContentPlaceHolder1_txt_allot").val() == "") {
            msg += "请填写分成系数！<br/>";
        }
        else {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_allot").val())) {
                msg += "分成系数格式不正确！</br>";
            }
        }
        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_allot").val()) <= 0) {
            msg += "分成系数必须大于0！";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

    $("#ctl00_ContentPlaceHolder1_btn_edit").click(function () {
        var msg = "";

        //生产部门
        if ($("#ctl00_ContentPlaceHolder1_drp_unit1").val() == "-1") {
            msg += "请选择要生产部门！<br/>";
        }
        //年份
        if ($("#ctl00_ContentPlaceHolder1_drp_allotyear0").val() == "-1") {
            msg += "请选择年份！<br/>";
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_allot0").val() == "") {
            msg += "请填写分成系数！<br/>";
        }
        else {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_allot0").val())) {
                msg += "分成系数格式不正确！</br>";
            }
        }
        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_allot0").val()) <= 0) {
            msg += "分成系数必须大于0！";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
});


function Clear() {
    $("#ctl00_ContentPlaceHolder1_drp_unit0").val("-1");
    $("#ctl00_ContentPlaceHolder1_drp_allotyear").val("-1");
    $("#ctl00_ContentPlaceHolder1_txt_allot").val("");

}