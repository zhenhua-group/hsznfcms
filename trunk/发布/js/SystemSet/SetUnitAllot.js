﻿$(document).ready(function () {
    $('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').click(function () {
        var checks = $('#ctl00_ContentPlaceHolder1_grid_mem :checkbox');

        if ($('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').attr("checked") == "checked") {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.setAttribute("class", "checked");
                checks[i].setAttribute("checked", "checked");
            }
        }
        else {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.removeAttribute("class");
                checks[i].removeAttribute("checked");
            }
        }
    });

    //编辑
    $(".cls_select").click(function () {
        $("#div_add").hide("slow");
        $("#div_edit").show("slow");
    });

    $(".cls_select").click(function () {
        window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
        $("#ctl00_ContentPlaceHolder1_hid_memid").val($(this).parent().parent().find("TD").eq(1).find(":hidden").val());
        $("#ctl00_ContentPlaceHolder1_drp_unit1").val($(this).parent().parent().find("TD").eq(2).find(":hidden").val());
        $("#ctl00_ContentPlaceHolder1_hid_unitid").val($(this).parent().parent().find("TD").eq(2).find(":hidden").val());
        $("#ctl00_ContentPlaceHolder1_drp_unit1").get(0).disabled = true;
        $("#ctl00_ContentPlaceHolder1_drp_allotyear0").val($.trim($(this).parent().parent().find("TD").eq(3).text()));
        $("#ctl00_ContentPlaceHolder1_hid_allotyear").val($.trim($(this).parent().parent().find("TD").eq(3).text()));
        $("#ctl00_ContentPlaceHolder1_drp_allotyear0").get(0).disabled = true;
        $("#ctl00_ContentPlaceHolder1_txt_allot0").val($(this).parent().parent().find("TD").eq(4).text());
    });

    $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {
        if ($("#ctl00_ContentPlaceHolder1_grid_mem :checkbox:checked").length == 0) {
            jAlert("请选择要删除的目标值！", "提示");
            $('#popup_ok').attr("class", "btn green");
            return false;
        }
        //判断是否要删除
        return confirm("是否要删除此条目标值？");
    });

    //菜单展开
    $("#show8_3_7").parent().attr("class", "active");
    $("#show8").parent().attr("class", " arrow open");
    $("#show8_3").parent().attr("class", " arrow open");
    $("#show8_3").parent().parent().css("display", "block");
    $("#show8_3_7").parent().parent().css("display", "block");

    //添加
    $("#btn_showadd").click(function () {
        window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
        $("#div_add").show("slow");
        $("#div_edit").hide("slow");
    });

    //隐藏
    $(" .btn_cancel").click(function () {
        $("#div_add").hide("slow");
        $("#div_edit").hide("slow");
    });


    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
        var msg = "";
        //生产部门
        if ($("#ctl00_ContentPlaceHolder1_drp_unit0").val() == "-1") {
            msg += "请选择要生产部门！<br/>";
        }
        //年份
        if ($("#ctl00_ContentPlaceHolder1_drp_allotyear").val() == "-1") {
            msg += "请选择年份！<br/>";
        }

        if ($("#ctl00_ContentPlaceHolder1_txt_allot").val() == "") {
            msg += "请填写目标产值！<br/>";
        }
        else {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_allot").val())) {
                msg += "目标产值格式不正确！</br>";
            }
        }
        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_allot").val()) <= 0) {
            msg += "目标产值必须大于0！";
        }
        if (msg != "") {
            jAlert(msg, "提示");

            return false;
        }
    });

    //修改
    $("#ctl00_ContentPlaceHolder1_btn_edit").click(function () {
        var msg = "";
        //生产部门
        if ($("#ctl00_ContentPlaceHolder1_drp_unit1").val() == "-1") {
            msg += "请选择要修改的生产部门！<br/>";
        }
        //年份
        if ($("#ctl00_ContentPlaceHolder1_drp_allotyear0").val() == "-1") {
            msg += "请选择年份！<br/>";
        }
        //产值额度
        if ($("#ctl00_ContentPlaceHolder1_txt_allot0").val() == "") {
            msg += "请填写目标产值！<br/>";
        }
        else {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_allot0").val())) {
                msg += "目标产值格式不正确！</br>";
            }
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

});

function Clear() {
    $("input", $("#div_add")).val("");
    window.document.getElementsByTagName("select").selectedIndex = 0;
}

function scrollToBottom() {
    window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
}