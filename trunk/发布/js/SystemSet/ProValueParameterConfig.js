﻿$(document).ready(function () {

    CommonControl.SetFormWidth();
    //行背景
    $("#gvOne tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvOne tr:even").css({ background: "White" });

    //行背景
    $("#gvTwo tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvTwo tr:even").css({ background: "White" });

    //行背景
    $("#gvThree tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvThree tr:even").css({ background: "White" });

    //行背景
    $("#gvFour tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvFour tr:even").css({ background: "White" });

    //行背景
    $("#gvEleven tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvEleven tr:even").css({ background: "White" });

    //行背景
    $("#gvProjectStageSpe tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvProjectStageSpe tr:even").css({ background: "White" });

    //行背景
    $("#gvProjectStageIsHaveSpe tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvProjectStageIsHaveSpe tr:even").css({ background: "White" });

    //行背景
    $("#gvDesignProcess tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gvDesignProcess tr:even").css({ background: "White" });


    $("#gvFive tr:even").css({ background: "White" });
    $("#gvSix tr:even").css({ background: "White" });
    $("#gvSeven tr:even").css({ background: "White" });
    $("#gvEight tr:even").css({ background: "White" });
    $("#gvNine tr:even").css({ background: "White" });
    $("#gvTen tr:even").css({ background: "White" });
    $("#gvIsHaveFive tr:even").css({ background: "White" });
    $("#gvIsHaveSix tr:even").css({ background: "White" });
    $("#gvIsHaveSeven tr:even").css({ background: "White" });
    $("#gvIsHaveEight tr:even").css({ background: "White" });
    $("#gvIsHaveNine tr:even").css({ background: "White" });
    $("#gvIsHaveTen tr:even").css({ background: "White" });
    $("#gvDesignProcessTwo tr:even").css({ background: "White" });
    $("#gvDesignProcessThree tr:even").css({ background: "White" });
    $("#gvDesignProcessFour tr:even").css({ background: "White" });
    $("#gvDesignProcessFive tr:even").css({ background: "White" });

    var map = { "0": "divone", "1": "divTwo", "2": "divThree", "3": "divFour", "4": "divFive", "5": "divSix", "6": "divSeven", "7": "divEight", "8": "divNine", "10": "divEleven" };

    //设计阶段
    $("#stage").change(function () {
        var divId = map[this.value];
        $("#" + divId).show().siblings().hide();
        $("#divOneEdit").hide();
        $("#divTwoEdit").hide();
        $("#divThreeEdit").hide();
        $("#divFourEdit").hide();
        $("#divFiveEdit").hide();
        $("#dixSixEdit").hide();
        $("#dixIsHaveSixEdit").hide();
        $("#divServenEdit").hide();
        $("#divIsHaveServenEdit").hide();
        $("#divEightEidt").hide();
        $("#divIsHaveEightEidt").hide();
        $("#divNineEidt").hide();
        $("#divIsHaveNineEidt").hide();
        $("#divTenEdit").hide();
        $("#divElevenEdit").hide();
    });

    var mapProcess = { "0": "processOne", "1": "processTwo", "2": "processThree", "3": "processFour", "4": "processFive" };

    //设计阶段--工序
    $("#processStage").change(function () {
        var divId = mapProcess[this.value];
        $("#" + divId).show().siblings().hide();
        $("#divDesignEdit").hide();
    });

    //各设计阶段
    $(".cls_select").click(function () {
        $("#divOneEdit").show("slow");
    });


    $(".cls_select").click(function () {
        $("#ctl00_ContentPlaceHolder1_lblOneType").text($(this).parent().parent().find("TD").eq(0).text());
        $("#txtOneProgram").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txtOnepreliminary").val($(this).parent().parent().find("TD").eq(2).text());
        $("#txtOneWorkDraw").val($(this).parent().parent().find("TD").eq(3).text());
        $("#txtLateStage").val($(this).parent().parent().find("TD").eq(4).text());
        $("#hidOneStatus").val($(this).parent().parent().find("TD").eq(7).text());
        $("#hidOneID").val($(this).parent().parent().find("TD").eq(6).text());
    });


    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    //项目各设计阶段保存
    $("#btnOneSave").click(function () {

        var programPercent = $("#txtOneProgram").val();
        if (!reg.test(programPercent)) {
            $("#spanOneProgram").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanOneProgram").hide();
        }

        var preliminaryPercent = $("#txtOnepreliminary").val();
        if (!reg.test(preliminaryPercent)) {
            $("#spanpreliminary").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanpreliminary").hide();
        }

        var WorkDrawPercent = $("#txtOneWorkDraw").val();
        if (!reg.test(WorkDrawPercent)) {
            $("#spanWorkDraw").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanWorkDraw").hide();
        }

        var LateStagePercent = $("#txtLateStage").val();
        if (!reg.test(LateStagePercent)) {
            $("#spanLateStage").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanLateStage").hide();
        }

        var total = parseFloat(programPercent) + parseFloat(preliminaryPercent) + parseFloat(WorkDrawPercent) + parseFloat(LateStagePercent);

        if (parseInt(total).toFixed(1) != 100.0) {
            alert('没有闭合，请确认');
            return false;
        }

        var id = $("#hidOneID").val();
        var itemType = $("#ctl00_ContentPlaceHolder1_lblOneType").text();
        var ProjectStatues = $("#hidOneStatus").val();

        var data = { "action": "0", "id": "" + id + "", "itemType": "" + itemType + "", "programPercent": "" + programPercent + "", "preliminaryPercent": "" + preliminaryPercent + "", "WorkDrawPercent": "" + WorkDrawPercent + "", "LateStagePercent": "" + LateStagePercent + "", "ProjectStatues": "" + ProjectStatues + "" };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //方案+设计阶段处理
    $(".cls_Twoselect").click(function () {
        $("#divTwoEdit").show("slow");
    });

    $(".cls_Twoselect ").click(function () {
        $("#ctl00_ContentPlaceHolder1_lblTwoType").text($(this).parent().parent().find("TD").eq(0).text());
        $("#txtTwoProgram").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txtTwopreliminary").val($(this).parent().parent().find("TD").eq(2).text());
        $("#hidTwoID").val($(this).parent().parent().find("TD").eq(4).text());
        $("#hidTwoStatus").val($(this).parent().parent().find("TD").eq(5).text());
    });

    $("#btnTwoSave").click(function () {

        var twoprogramPercent = $("#txtTwoProgram").val();
        if (!reg.test(twoprogramPercent)) {
            $("#spanTwoProgram").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanTwoProgram").hide();
        }

        var twopreliminaryPercent = $("#txtTwopreliminary").val();
        if (!reg.test(twopreliminaryPercent)) {
            $("#spanTwopreliminary").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanTwopreliminary").hide();
        }

        var total = parseFloat(twoprogramPercent) + parseFloat(twopreliminaryPercent);

        if (parseInt(total).toFixed(1) != 100.0) {
            alert('没有闭合，请确认');
            return false;
        }

        var twoid = $("#hidTwoID").val();
        var twoitemType = $("#ctl00_ContentPlaceHolder1_lblTwoType").text();
        var twoProjectStatues = $("#hidTwoStatus").val();

        var data = { "action": "1", "id": "" + twoid + "", "itemType": "" + twoitemType + "", "programPercent": "" + twoprogramPercent + "", "preliminaryPercent": "" + twopreliminaryPercent + "", "ProjectStatues": "" + twoProjectStatues + "" };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //施工图+后期 阶段处理
    $(".cls_Threeselect").click(function () {
        $("#divThreeEdit").show("slow");
    });

    $(".cls_Threeselect ").click(function () {
        $("#ctl00_ContentPlaceHolder1_lblThreeType").text($(this).parent().parent().find("TD").eq(0).text());
        $("#txtThreeWorkDrawPercent").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txtThreeLateStagePercent").val($(this).parent().parent().find("TD").eq(2).text());
        $("#hidThreeID").val($(this).parent().parent().find("TD").eq(4).text());
        $("#hidThreeStatus").val($(this).parent().parent().find("TD").eq(5).text());
    });

    $("#btnThreeSave").click(function () {

        var threeWorkDrawPercent = $("#txtThreeWorkDrawPercent").val();
        if (!reg.test(threeWorkDrawPercent)) {
            $("#spanThreeWorkDrawPercent").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanTwoProgram").hide();
        }

        var threeLateStagePercent = $("#txtThreeLateStagePercent").val();
        if (!reg.test(threeLateStagePercent)) {
            $("#spanThreeLateStagePercent").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanTwopreliminary").hide();
        }

        var total = parseFloat(threeWorkDrawPercent) + parseFloat(threeLateStagePercent);

        if (parseInt(total).toFixed(1) != 100.0) {
            alert('没有闭合，请确认');
            return false;
        }

        var threeid = $("#hidThreeID").val();
        var threeitemType = $("#ctl00_ContentPlaceHolder1_lblThreeType").text();
        var threeProjectStatues = $("#hidThreeStatus").val();

        var data = { "action": "2", "id": "" + threeid + "", "itemType": "" + threeitemType + "", "WorkDrawPercent": "" + threeWorkDrawPercent + "", "LateStagePercent": "" + threeLateStagePercent + "", "ProjectStatues": "" + threeProjectStatues + "" };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    // 初步设计+施工图设计+后期服务
    $(".cls_fourselect").click(function () {
        $("#divFourEdit").show("slow");
    });

    $(".cls_fourselect").click(function () {
        $("#ctl00_ContentPlaceHolder1_lblFourType").text($(this).parent().parent().find("TD").eq(0).text());
        $("#txtFourpreliminary").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txtFourWorkDraw").val($(this).parent().parent().find("TD").eq(2).text());
        $("#txtFourLateStage").val($(this).parent().parent().find("TD").eq(3).text());;
        $("#hidFourStatus").val($(this).parent().parent().find("TD").eq(6).text());
        $("#hidFourID").val($(this).parent().parent().find("TD").eq(5).text());
    });
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnFourSave").click(function () {


        var fourpreliminaryPercent = $("#txtFourpreliminary").val();
        if (!reg.test(fourpreliminaryPercent)) {
            $("#spanFourpreliminary").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanFourpreliminary").hide();
        }

        var fourWorkDrawPercent = $("#txtFourWorkDraw").val();
        if (!reg.test(fourWorkDrawPercent)) {
            $("#spanFourWorkDraw").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanFourWorkDraw").hide();
        }

        var fourLateStagePercent = $("#txtFourLateStage").val();
        if (!reg.test(fourLateStagePercent)) {
            $("#spanFourLateStage").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanFourLateStage").hide();
        }

        var total = parseFloat(fourpreliminaryPercent) + parseFloat(fourWorkDrawPercent) + parseFloat(fourLateStagePercent);

        if (parseInt(total).toFixed(1) != 100.0) {
            alert('没有闭合，请确认');
            return false;
        }

        var id = $("#hidFourID").val();
        var itemType = $("#ctl00_ContentPlaceHolder1_lblFourType").text();
        var ProjectStatues = $("#hidFourStatus").val();

        var data = { "action": "3", "id": "" + id + "", "itemType": "" + itemType + "", "preliminaryPercent": "" + fourpreliminaryPercent + "", "WorkDrawPercent": "" + fourWorkDrawPercent + "", "LateStagePercent": "" + fourLateStagePercent + "", "ProjectStatues": "" + ProjectStatues + "" };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //方案+施工图+后期
    $(".cls_elevenselect").click(function () {
        $("#divElevenEdit").show("slow");
    });

    $(".cls_elevenselect").click(function () {
        $("#ctl00_ContentPlaceHolder1_lblElevenType").text($(this).parent().parent().find("TD").eq(0).text());
        $("#txtElevenProgramPercent").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txtElevenWorkDraw").val($(this).parent().parent().find("TD").eq(2).text());
        $("#txtElevenLateStage").val($(this).parent().parent().find("TD").eq(3).text());;
        $("#hidElevenStatus").val($(this).parent().parent().find("TD").eq(6).text());
        $("#hidElevenID").val($(this).parent().parent().find("TD").eq(5).text());
    });

    $("#btnElevenSave").click(function () {


        var fourpreliminaryPercent = $.trim($("#txtElevenProgramPercent").val());
        if (!reg.test(fourpreliminaryPercent)) {
            $("#spanElevenProgramPercent").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanElevenProgramPercent").hide();
        }

        var fourWorkDrawPercent = $.trim($("#txtElevenWorkDraw").val());
        if (!reg.test(fourWorkDrawPercent)) {
            $("#spanElevenWorkDraw").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanElevenWorkDraw").hide();
        }

        var fourLateStagePercent = $.trim($("#txtElevenLateStage").val());
        if (!reg.test(fourLateStagePercent)) {
            $("#spanElevenLateStage").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanElevenLateStage").hide();
        }

        var total = parseFloat(fourpreliminaryPercent) + parseFloat(fourWorkDrawPercent) + parseFloat(fourLateStagePercent);

        if (parseInt(total).toFixed(1) != 100.0) {
            alert('没有闭合，请确认');
            return false;
        }

        var id = $("#hidElevenID").val();
        var itemType = $("#ctl00_ContentPlaceHolder1_lblElevenType").text();
        var ProjectStatues = $("#hidElevenStatus").val();

        var data = { "action": "10", "id": "" + id + "", "itemType": "" + itemType + "", "programPercent": "" + fourpreliminaryPercent + "", "WorkDrawPercent": "" + fourWorkDrawPercent + "", "LateStagePercent": "" + fourLateStagePercent + "", "ProjectStatues": "" + ProjectStatues + "" };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //各设计介绍与专业 无暖通专业
    $(".cls_Speselect").click(function () {
        $("#divSpa").show("slow");

    });

    //查询显示
    $(".cls_Speselect").each(function () {

        $(this).click(function () {
            $("#lblProjectStage").text($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divSpa tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });


    //保存个专业
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnSpeSave").click(function () {
        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "ProjectStage": $("#lblProjectStage").text(),
            "Specialty": new Array(),
            "IsHaved": 0
        };

        $(":text", $("td:eq(1)", "#divSpa tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanbulidingPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanbulidingPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divSpa tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "spe", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });


    //各设计介绍与专业 有暖通专业
    $(".cls_IsHaveSpeselect").click(function () {
        $("#divIsHaveSpa").show("slow");

    });

    //查询显示
    $(".cls_IsHaveSpeselect").each(function () {

        $(this).click(function () {
            $("#lblProjectStage").text($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divIsHaveSpa tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });


    //保存个专业
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnIsHaveSpeSave").click(function () {
        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "ProjectStage": $("#lblProjectStage").text(),
            "Specialty": new Array(),
            "IsHaved": 1
        };

        $(":text", $("td:eq(1)", "#divIsHaveSpa tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanbulidingPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanbulidingPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divIsHaveSpa tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "spe", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });
    //设计工序
    $(".cls_Designselect").click(function () {
        $("#divDesignEdit").show("slow");
    });

    $(".cls_Designselect").click(function () {
        $("#ctl00_ContentPlaceHolder1_lblSpecialty").text($(this).parent().parent().find("TD").eq(0).text());
        $("#txtAuditPercent").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txtSpecialtyHeadPercent").val($(this).parent().parent().find("TD").eq(2).text());
        $("#txtProofreadPercent").val($(this).parent().parent().find("TD").eq(3).text());
        $("#txtDesignPercent").val($(this).parent().parent().find("TD").eq(4).text());
        $("#hidDesignProcessID").val($(this).parent().parent().find("TD").eq(6).text());
    });
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnDesignSave").click(function () {

        var AuditPercent = $("#txtAuditPercent").val();
        if (!reg.test(AuditPercent)) {
            $("#spanAuditPercent").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanAuditPercent").hide();
        }

        var SpecialtyHeadPercent = $("#txtSpecialtyHeadPercent").val();
        if (!reg.test(SpecialtyHeadPercent)) {
            $("#spanSpecialtyHeadPercent").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanSpecialtyHeadPercent").hide();
        }

        var ProofreadPercent = $("#txtProofreadPercent").val();
        if (!reg.test(ProofreadPercent)) {
            $("#spanProofreadPercent").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanProofreadPercent").hide();
        }

        var DesignPercent = $("#txtDesignPercent").val();
        if (!reg.test(DesignPercent)) {
            $("#spanDesignPercent").text("请输入数字！").show();
            return false;
        }
        else {
            $("#spanDesignPercent").hide();
        }

        var total = parseFloat(AuditPercent) + parseFloat(SpecialtyHeadPercent) + parseFloat(ProofreadPercent) + parseFloat(DesignPercent);

        if (parseInt(total).toFixed(1) != 100.0) {
            alert('没有闭合，请确认');
            return false;
        }

        var id = $("#hidDesignProcessID").val();
        var Specialty = $("#ctl00_ContentPlaceHolder1_lblSpecialty").text();

        var data = { "action": "design", "id": "" + id + "", "Specialty": "" + Specialty + "", "AuditPercent": "" + AuditPercent + "", "SpecialtyHeadPercent": "" + SpecialtyHeadPercent + "", "ProofreadPercent": "" + ProofreadPercent + "", "DesignPercent": "" + DesignPercent + "" };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });
    $("#btnCancelDesign").click(function () {
        $("#divDesignEdit").hide();
    });
    //室外工程 -无暖通专业
    $(".cls_Fiveselect").click(function () {
        $("#divFiveEdit").show("slow");
    });

    //查询显示
    $(".cls_Fiveselect").each(function () {

        $(this).click(function () {

            $("#lblspetialty").html($(this).parent().parent().find("TD").eq(0).text());
            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })

            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divFiveEdit tr:eq(' + i + ')')).val(array[i]);
            };
        });

    });

    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnFive").click(function () {


        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 4,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 0
        };

        $(":text", $("td:eq(1)", "#divFiveEdit tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanSpetytyPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanSpetytyPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divFiveEdit tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });


    //室外工程 -有暖通专业
    $(".cls_IsHaveFiveselect").click(function () {
        $("#divIsHaveFiveEdit").show("slow");
    });

    //查询显示
    $(".cls_IsHaveFiveselect").each(function () {

        $(this).click(function () {

            $("#lblspetialty").html($(this).parent().parent().find("TD").eq(0).text());
            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })

            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divIsHaveFiveEdit tr:eq(' + i + ')')).val(array[i]);
            };
        });

    });

    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnIsHaveFive").click(function () {


        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 4,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 1
        };

        $(":text", $("td:eq(1)", "#divIsHaveFiveEdit tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanSpetytyPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanSpetytyPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divIsHaveFiveEdit tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //锅炉房 --无暖通
    $(".cls_Sixselect").click(function () {
        $("#dixSixEdit").show("slow");
    });

    //查询显示
    $(".cls_Sixselect").each(function () {

        $(this).click(function () {
            var opeString = $(this).parent().parent().find("TD").eq(0).text();
           
            $("#drp_select").text(opeString);
            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#dixSixEdit tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnBoiler").click(function () {


        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 5,
            "type": $("#drp_select").text(),
            "Specialty": new Array(),
            "IsHaved": 0
        };


        $(":text", $("td:eq(1)", "#dixSixEdit tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanSixPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanSixPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#dixSixEdit tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //锅炉房 --有暖通专业
    $(".cls_IsHaveSixselect").click(function () {
        $("#dixIsHaveSixEdit").show("slow");
    });

    //查询显示
    $(".cls_IsHaveSixselect").each(function () {

        $(this).click(function () {
            var opeString = $(this).parent().parent().find("TD").eq(0).text();
           
            $("#drp_IsHavcSelect").text(opeString);
            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#dixIsHaveSixEdit tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnIsHaveBoiler").click(function () {


        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 5,
            "type": $("#drp_IsHavcSelect").text(),
            "Specialty": new Array(),
            "IsHaved": 1
        };


        $(":text", $("td:eq(1)", "#dixIsHaveSixEdit tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanSixPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanSixPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#dixIsHaveSixEdit tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //地上单建水泵房 --无暖通
    $(".cls_Servenselect").click(function () {
        $("#divServenEdit").show("slow");
    });
    //查询显示
    $(".cls_Servenselect").each(function () {

        $(this).click(function () {

            $("#lblServenType").html($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divServenEdit tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnSeven").click(function () {

        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 6,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 0
        };

        $(":text", $("td:eq(1)", "#divServenEdit tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanSevenPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanSevenPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divServenEdit tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });

    });

    //地上单建水泵房 --有暖通
    $(".cls_IsHaveServenselect").click(function () {
        $("#divIsHaveServenEdit").show("slow");
    });
    //查询显示
    $(".cls_IsHaveServenselect").each(function () {

        $(this).click(function () {

            $("#lblServenType").html($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divIsHaveServenEdit tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    $("#btnIsHaveSeven").click(function () {

        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 6,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 1
        };

        $(":text", $("td:eq(1)", "#divIsHaveServenEdit tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanSevenPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanSevenPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divIsHaveServenEdit tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });

    });

    //地上单建变配电所（室）--无暖通专业
    $(".cls_Eightselect").click(function () {
        $("#divEightEidt").show("slow");
    });
    //查询显示
    $(".cls_Eightselect").each(function () {

        $(this).click(function () {
            $("#lblEightType").html($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divEightEidt tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });

    $("#btnEight").click(function () {

        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 7,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 0
        };

        $(":text", $("td:eq(1)", "#divEightEidt tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanEightPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanEightPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divEightEidt tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //地上单建变配电所（室）--you暖通专业
    $(".cls_IsHaveEightselect").click(function () {
        $("#divIsHaveEightEidt").show("slow");
    });
    //查询显示
    $(".cls_IsHaveEightselect").each(function () {

        $(this).click(function () {
            $("#lblEightType").html($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divIsHaveEightEidt tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });

    $("#btnIsHaveEight").click(function () {

        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 7,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 1
        };

        $(":text", $("td:eq(1)", "#divIsHaveEightEidt tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanEightPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanEightPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divIsHaveEightEidt tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };

        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //地下室--无暖通所
    $(".cls_Nineselect").click(function () {
        $("#divNineEidt").show("slow");
    });

    //查询显示
    $(".cls_Nineselect").each(function () {

        $(this).click(function () {
            $("#lblNineType").html($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divNineEidt tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });


    $("#btnNine").click(function () {

        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 8,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 0
        };

        $(":text", $("td:eq(1)", "#divNineEidt tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanNinePercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanNinePercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divNineEidt tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };
        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //地下室--有暖通所
    $(".cls_IsHaveNineselect").click(function () {
        $("#divIsHaveNineEidt").show("slow");
    });

    //查询显示
    $(".cls_IsHaveNineselect").each(function () {

        $(this).click(function () {
            $("#lblNineType").html($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divIsHaveNineEidt tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });


    $("#btnIsHaveNine").click(function () {

        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 8,
            "type": "",
            "Specialty": new Array(),
            "IsHaved": 1
        };

        $(":text", $("td:eq(1)", "#divIsHaveNineEidt tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanNinePercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanNinePercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divIsHaveNineEidt tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };
        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });

    //市政道路工程

    $(".cls_Tenselect").click(function () {
        $("#divTenEdit").show("slow");
    });

    //查询显示
    $(".cls_Tenselect").each(function () {

        $(this).click(function () {
            $("#lblTenType").html($(this).parent().parent().find("TD").eq(0).text());

            var array = new Array();
            var $Obj = $(this).parent().parent().find("TD").not(":last");
            $Obj.each(function () {
                array.push($(this).text());
            })
            for (var i = 1; i < 15; i++) {
                $("input", $('td:eq(1)', '#divTenEdit tr:eq(' + i + ')')).val(array[i]);
            }

        });
    });

    $("#btnTen").click(function () {

        var isTrue = true;
        var AllInputValue = 0;
        var objectAdd = {
            "status": 9,
            "type": "",
            "Specialty": new Array()
        };

        $(":text", $("td:eq(1)", "#divSpa tr:gt(0)")).each(function () {

            if (!reg.test($(this).val())) {
                $("#spanTenPercent", $(this).parent()).show();
                isTrue = false;
                return false;
            }
            else {
                $("#spanTenPercent", $(this).parent()).hide();
                isTrue = true;
            }
            AllInputValue += parseFloat($(this).val());

        });

        if (isTrue) {
            if (parseFloat(AllInputValue).toFixed(2) != 100.00) {
                alert('没有闭合，请确认');
                return false;
            }
        }

        $.each($("#divTenEdit tr:gt(0)").not(":last"), function (index, val) {
            objectAdd.Specialty[index] = { "SpecialtyName": $(this).find("TD").eq(0).text(), "SpecialtyValue": $(this).find("TD").eq(1).children("input").val() };
        });


        var data = { "action": "out", "data": Global.toJSON(objectAdd) };
        $.post("/HttpHandler/ProjectValueandAllot/ProValueParameterConfigHandler.ashx", data, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                alert("修改成功！");
                window.location.href = "/SystemSet/ProValueParameterConfigBymaster.aspx";
            }
            else {
                alert("修改失败！");
            }
        });
    });
});