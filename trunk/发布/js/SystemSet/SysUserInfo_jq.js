﻿
var sysUserInfoList = function () {

    //默认加载

    var pageLoad = function () {
        //数字验证正则    
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

        var tempRandom = Math.random() + new Date().getMilliseconds();

        $("#jqGrid").jqGrid({
            url: '/HttpHandler/SystemSet/SysUserInfoHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#hidWhere").val()),
            datatype: 'json',
            height: "auto",
            rowNum: 20,
            rowList: [20, 25, 30],
            colNames: ['编辑', '查看', '序号', '员工账号', '姓名', '在职状态', '所属部门', '性别', '从事专业', '管理职位', '技术职位', '身份证号', '户籍所在地', '出生日期', '年龄', '联系方式', '分机号', '职称评定', '评定时间', '注册', '注册时间', '注册地点', '毕业学校', '学位(历)', '毕业专业', '毕业时间', '毕业学校', '学位(历)', '毕业专业', '毕业学校', '参加工作时间', '工作间隔时间', '工作间隔时间(备注)', '工作年限', '试用周期', '入司时间', '入司间隔时间', '入司间隔时间(备注)', '入司年限', '岗位工龄', '档案所在地', '休假基数', '年假时间', '总经理特批', '转正时间', '工资', '交通补贴', '通讯补贴', '应发工资', '社保基数', '公积金', '备注', '预发奖金', '签订时间', '合同性质', '到期时间', '备注', '离职情况', '离职时间', '离职性质', '原因', '年薪(万)','离职情况','离职时间','离职性质','离职原因'],
            colModel: [
                                 { name: 'memID', index: 'memID', width: 40, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter },
                                 { name: 'memID', index: 'memID', width: 40, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                                 { name: 'memID', index: 'memID', width: 40, align: 'center' },
                                 { name: 'mem_Login', index: 'mem_Login', width: 80, editable: true },
                                 { name: 'mem_Name', index: 'mem_Name', width: 60, editable: true },
                                 { name: 'isFiredName', index: 'isFiredName', width: 60, editable: true },
                                 { name: 'memUnitName', index: 'memUnitName', width: 80, editable: true },
                                 { name: 'mem_Sex', index: 'mem_Sex', width: 40, editable: true },
                                 { name: 'memSpeName', index: 'memSpeName', width: 40, editable: true },
                                 { name: 'RoleMagName', index: 'RoleMagName', width: 80, align: 'center' },
                                 { name: 'RoleTecName', index: 'RoleTecName', width: 80, align: 'center', editable: true },

                                 { name: 'mem_Code', index: 'mem_Code', width: 120, align: 'center' },
                                 { name: 'mem_CodeAddr', index: 'mem_CodeAddr', width: 200 },
                                 { name: 'mem_Birthday', index: 'mem_Birthday', width: 80, align: 'center' },
                                 { name: 'mem_Age', index: 'mem_Age', width: 80, align: 'center' },
                                 { name: 'mem_Mobile', index: 'mem_Mobile', width: 100, align: 'center' },
                                 { name: 'mem_Telephone', index: 'mem_Telephone', width: 80, align: 'center' },
                                 { name: 'mem_ArchLevelName2', index: 'mem_ArchLevelName2', width: 80, align: 'center' },
                                 { name: 'mem_ArchLevelTime', index: 'mem_ArchLevelTime', width: 80, align: 'center' },
                                 { name: 'ArchLevelName', index: 'ArchLevelName', width: 80, align: 'center' },
                                 { name: 'mem_ArchRegTime', index: 'mem_ArchRegTime', width: 80, align: 'center', editable: true },
                                 { name: 'mem_ArchRegAddrName', index: 'mem_ArchRegAddrName', width: 80, align: 'center' },

                                 { name: 'mem_School', index: 'mem_School', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_SchLevel', index: 'mem_SchLevel', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_SchSpec', index: 'mem_SchSpec', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_SchOutDate', index: 'mem_SchOutDate', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_School2', index: 'mem_School2', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_SchLevel2', index: 'mem_SchLevel2', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_SchSpec2', index: 'mem_SchSpec2', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_SchOutDate2', index: 'mem_SchOutDate2', width: 80, align: 'center', hidden: true },

                                 { name: 'mem_WorkTime', index: 'mem_WorkTime', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_WorkDiff', index: 'mem_WorkDiff', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_WorkDiffSub', index: 'mem_WorkDiffSub', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_WorkYear', index: 'mem_WorkYear', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_WorkTemp', index: 'mem_WorkTemp', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_InCompanyTime', index: 'mem_InCompanyTime', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_InCompanyDiff', index: 'mem_InCompanyDiff', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_InCompanyDiffSub', index: 'mem_InCompanyDiffSub', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_InCompanyYear', index: 'mem_InCompanyYear', width: 80, align: 'center', hidden: true },
                                 { name: 'mem_InCompanyTemp', index: 'mem_InCompanyTemp', width: 80, align: 'center', hidden: true },

                                 { name: 'mem_FileAddr', index: 'mem_FileAddr', width: 80, hidden: true },
                                 { name: 'mem_Holidaybase', index: 'mem_Holidaybase', width: 80, hidden: true },
                                 { name: 'mem_HolidayYear', index: 'mem_HolidayYear', width: 80, hidden: true },
                                 { name: 'mem_MagPishi', index: 'mem_MagPishi', width: 80, hidden: true },
                                 { name: 'mem_ZhuanZheng', index: 'mem_ZhuanZheng', width: 80, hidden: true },
                                 { name: 'mem_GongZi', index: 'mem_GongZi', width: 80, hidden: true },
                                 { name: 'mem_Jiaotong', index: 'mem_Jiaotong', width: 80, hidden: true },
                                 { name: 'mem_Tongxun', index: 'mem_Tongxun', width: 80, hidden: true },
                                 { name: 'mem_YingFa', index: 'mem_YingFa', width: 80, hidden: true },
                                 { name: 'mem_SheBaobase', index: 'mem_SheBaobase', width: 80, hidden: true },
                                 { name: 'mem_Gongjijin', index: 'mem_Gongjijin', width: 80, hidden: true },
                                 { name: 'mem_GongjijinSub', index: 'mem_GongjijinSub', width: 80, hidden: true },
                                 { name: 'mem_Yufa', index: 'mem_Yufa', width: 80, hidden: true },

                                 { name: 'mem_CoperationDate2', index: 'mem_CoperationDate2', width: 80, hidden: true },
                                 { name: 'mem_CoperationTypeName2', index: 'mem_CoperationTypeName2', width: 80, hidden: true },
                                 { name: 'mem_CoperationDateEnd2', index: 'mem_CoperationDateEnd2', width: 80, hidden: true },
                                 { name: 'mem_CoperationSub', index: 'mem_CoperationSub', width: 80, hidden: true },
                                 { name: 'mem_OutCompany', index: 'mem_OutCompany', width: 80, hidden: true },
                                 { name: 'mem_OutCompanyDate', index: 'mem_OutCompanyDate', width: 80, hidden: true },
                                 { name: 'mem_OutCompanyType', index: 'mem_OutCompanyType', width: 80, hidden: true },
                                 { name: 'mem_OutCompanySub', index: 'mem_OutCompanySub', width: 80, hidden: true },
                                 { name: 'mem_YearCharge', index: 'mem_YearCharge', width: 80, hidden: true },

                                 { name: 'mem_OutCompany', index: 'mem_OutCompany', width: 80, hidden: true },
                                 { name: 'mem_OutCompanyDate', index: 'mem_OutCompanyDate', width: 80, hidden: true },
                                 { name: 'mem_OutCompanyTypeName', index: 'mem_OutCompanyTypeName', width: 80, hidden: true },
                                 { name: 'mem_OutCompanySub', index: 'mem_OutCompanySub', width: 80, hidden: true },


            ],
            jsonReader: {
                repeatitems: false,
                root: function (obj) { return obj.rows; },
                page: function (obj) { return obj.pageindex; },
                total: function (obj) { return obj.pagecount; },
                records: function (obj) { return obj.total; }
            },
            prmNames: {
                page: 'PageIndex',
                rows: 'PageSize',
                sort: 'OrderBy',
                order: 'Sort'
            },
            loadonce: false,
            sortname: 'mem_Order',
            sortorder: 'ASC',
            pager: "#gridpager",
            viewrecords: true,
            shrinkToFit: false,
            autowidth: true,
            editurl: "/HttpHandler/SystemSet/SysUserInfoHandler.ashx",
            multiselect: true,
            multiselectWidth: 25,
            footerrow: true,
            gridComplete: completeMethod,
            loadComplete: loadCompMethod
        });
    }

    //编辑
    function colEditFormatter(celvalue, options, rowdata) {
        var pageurl = "EditSysUserInfo.aspx?id=" + celvalue;
        return '<a target="_blank" href="' + pageurl + '" alt="编辑" class="btn default btn-xs blue-stripe" >编辑</a>';
    }
    //查看
    function colShowFormatter(celvalue, options, rowData) {
        var pageurl = "EditSysUserInfo.aspx?id=" + celvalue + "&action=chk";
        return '<a target="_blank" href="' + pageurl + '" alt="查看" class="btn default btn-xs red-stripe">查看</a>';

    }
    //统计 
    function completeMethod() {

    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata").text() == '') {
                $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
            }
            else { $("#nodata").show(); }

        }
        else {
            $("#nodata").hide();
        }
    }


    return {
        init: function () {

            pageLoad();
        }
    };
}();

