﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //全选
    $("#chk_all").click(function () {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });
    //删除
    $("#btn_DelCst").click(function () {
        var icount = $(":checked").length;
        if (icount == 0) {
            alert("请选择要删除的记录");
            return false;
        }

        if (confirm("确实要删除消息吗？")) {
            return true;
        }
        else {
            return false;
        }

    });

    //行背景
    $("#gv_Coperation tr:odd").css("background-color", "#F0F0F0");
    $("#gv_Coperation tr:odd").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //得到实际的现实高度
    var h = document.body.clientHeight - 129;
    //表格滚动
    $("#GridView1").chromatable({
        width: "100%",
        height: h,
        scrolling: "true"
    });
    //隔行变色
    $("#gv_Coperation tr:even").css({ background: "White" });

});