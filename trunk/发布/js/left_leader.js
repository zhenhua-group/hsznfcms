﻿$(document).ready(function () {
    //项目分析
    $("#pro_analysis").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/ProjectAnalysis.aspx");
    });
    //项目统计
    $("#proj_count").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/ProjectCount.aspx");
    });
    //合同统计
    $("#cpr_count").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/CoperationCount.aspx");
    });
    //项目分布
    $("#proj_map").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/MapOfChina.aspx");
    });
    //项目分配
    $("#proj_allot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/ProjectAllot.aspx");
    });
    //合同收款
    $("#cpr_chk").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/CoperationChargeList.aspx");
    });
    //合同计划收费查看
    $("#cpr_cprplanchk").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/CoperationFinancialList.aspx");
    });
    //合同收款统计
    $("#cpr_sktj").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_ChargeCountList.aspx");
    });
    //部门产值完成统计
    $("#cpr_allotcount").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/CoperationChargeYearDetails.aspx");
    });
    //资质管理
    $("#zizhi_info").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/QualManagement.aspx");
    });
    //信息查询
    $("#copj_select").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../LeadershipCockpit/CoperationProjectList.aspx");
    });
    //合同收费对比新增
    $("#cop_acontrast").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_ContrastList.aspx");
    });
    //合同收费同期同比新增
    $("#cop_commonsize").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_CommonsizeList.aspx");
    });
    //合同收费环比表
    $("#ChargeAndCoperation").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_ChargeAndCoperationRatioList.aspx");
    });
    //合同及合同收费详情表
    $("#StandBook").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_StandBook.aspx");
    });
    //产值汇总
    $("#OutSummary").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/OutSummary.aspx");
    });
    //收费与年度对比图
    $("#cop_chargecontrast").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_ChargeContrastList.aspx");
    });
    //收费日期汇总表
    $("#ChargeDate").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_ChargeDate.aspx");
    });
    //项目分类对比表
    $("#Industry").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_AcountAndIndustryList.aspx");
    });
    //财务报表分析
    $("#CollectionAndUnit").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/Coperation/cpr_CollectionAndUnit.aspx");
    });
    
    //个人产值统计分配表
    $("#MemProjValueAllot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/LeadershipCockpit/MemberProjectValueAllot.aspx");
    });
    
});