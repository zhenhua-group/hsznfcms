﻿var projectNameRepeat = false;
$(document).ready(function () {
    $("#ctl00_ContentPlaceHolder1_hid_cprid").val(hid_cprid);
    $("#ctl00_ContentPlaceHolder1_CompanyType_dpr_company").val("2");
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#previewPower").val(),
        "userSysNum": $("#userSysNum").val(),
        "userUnitNum": $("#userUnitNum").val(),
        "notShowUnitList": ""
    };
    var regOne = /[\\/:\*\?\""<>|]/;
    var regTwo = /^[^.]/;
    //检查合同名称是否重复
    $("#ctl00_ContentPlaceHolder1_txt_cprName").blur(function () {
        var projectName = $.trim($(this).val());
        if (regOne.test(projectName)) {
            alert("合同名称不能包括以下任何字符！\/:\*\?\"<>|");
            projectNameRepeat = true;
            return false;
        }
        if (!regTwo.test(projectName)) {
            alert("合同名称不能为空!");
            projectNameRepeat = true;
            return false;
        }
        if (projectName.length > 0) {
            $.post("/HttpHandler/ProjectMgr/ExistsInTGProjectHandler.ashx", { "cprname": projectName,"sysno":"0", "action": "Construcpr" }, function (result) {
                if (result == "1") {
                    projectNameRepeat = true;
                    alert("合同名称已存在，请重新输入新合同名称！");
                } else {
                    projectNameRepeat = false;
                }
            });
        }
    });
    //保存合同信息
    $("#ctl00_ContentPlaceHolder1_btn_Save").click(function () {


        //取得文本值
        //合同编号
        var txtcpr_No = $("#ctl00_ContentPlaceHolder1_txtcpr_No").val();
        //合同名称
        var txtcCprName = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        //承接部门
        var txtCprUnit = $("#ctl00_ContentPlaceHolder1_txt_cprBuildUnit").val();
        //建筑类型
        var drpBuildType = $("#ctl00_ContentPlaceHolder1_drp_buildtype").val();
        //承接部门
        var txt_cjbm = $("#ctl00_ContentPlaceHolder1_txt_cjbm").val();
        //开始日期
        var txtRegisterDate = $("#ctl00_ContentPlaceHolder1_txtSingnDate").val();
        //完成日期
        var txtCompleteDate = $("#ctl00_ContentPlaceHolder1_txtCompleteDate").val();
        //备注
        var txtcpr_Remark = $("#ctl00_ContentPlaceHolder1_txtcpr_Remark").val();
        //甲方负责
        var txtFParty = $("#ctl00_ContentPlaceHolder1_txtFParty").val();


        //合同额
        $("#ctl00_ContentPlaceHolder1_hidtxtcpr_Account").val($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val());

        //数字验证正则    
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        //联系人
        if ($("#ctl00_ContentPlaceHolder1_hid_cstid").val() == "") {
            msg += "请为合同选择客户信息！</br>";
        }
        //合同编号
        if (txtcpr_No == "") {
            msg += "请输入合同编号！</br>";
        }
        //合同分类
        if ($("#ctl00_ContentPlaceHolder1_ddcpr_Type").val() == "-1") {
            msg += "请选择合同分类！</br>";
        }
        //合同名称
        if (txtcCprName == "") {
            msg += "请输入合同名称！</br>";
        } else {
            //合同名称重复
            if (projectNameRepeat) {
                msg += "合同名称已存在，请重新输入新合同名称！</br>";
            }
        }
        //建筑类型
        if (drpBuildType == "-1") {
            msg += "请选择建筑类型！</br>";
        }

        //建设单位
        if (txtCprUnit == "") {
            msg += "请选择建设单位！</br>";
        }

        //项目经理
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_proFuze").val()) == "") {
            msg += "请填写工程负责人！</br>";
        }
        //甲方负责人
        if ($.trim(txtFParty) == "") {
            msg += "请填写甲方负责人！</br>";
        }
        //承接部门
        if (txt_cjbm == "") {
            msg += "请选择承接部门！</br>";
        }

        //建设地点
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_ProjectPosition").val()) == "") {
            msg += "请填写工程地点！</br>";
        }

        //合同金额
        if ($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val() == "") {
            msg += "请输入合同额！</br>";
        }
        else {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                msg += "合同额格式不正确！</br>";
            }
        }
        //实际合同额
        if ($("#ctl00_ContentPlaceHolder1_txtcpr_Account0").val() != "") {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account0").val())) {
                msg += "实际合同额请输入数字！</br>";
            }
        }
        //投资额
        if ($("#ctl00_ContentPlaceHolder1_txtInvestAccount").val() != "") {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtInvestAccount").val())) {
                msg += "投资额请输入数字！</br>";
            }
        }
        //行业性质
        if ($.trim($("#ctl00_ContentPlaceHolder1_ddProfessionType").val()) == "") {
            msg += "请选择行业性质！</br>";
        }

        //工程来源
        if ($.trim($("#ctl00_ContentPlaceHolder1_ddSourceWay").val()) == "") {
            msg += "请选择工程来源！</br>";
        }

        var regInteger = /^[1-9]\d*$/;



        //占地面积
        if ($("#ctl00_ContentPlaceHolder1_txt_AreaUnit").val() != "") {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txt_AreaUnit").val())) {
                msg += "输入占地面积格式不正确！</br>";
            }
        }

        //合同签订日期
        if ($("#ctl00_ContentPlaceHolder1_txtSingnDate").val() == "") {
            msg += "请输入合同统计年份！</br>";
        }
        if ($("#ctl00_ContentPlaceHolder1_txtSingnDate2").val() == "") {
            msg += "请输入合同开工日期！</br>";
        }
        if (txtCompleteDate == "") {
            msg += "请输入合同完成日期！</br>";
        }

        //工期
        if ($("#ctl00_ContentPlaceHolder1_txt_ProjectDate").val() != "") {

            if (!regInteger.test($("#ctl00_ContentPlaceHolder1_txt_ProjectDate").val())) {
                msg += "输入工期格式错误，请输入数字！</br>";
            }
        }

        //楼层数判断
        if ($("#ctl00_ContentPlaceHolder1_txt_upfloor").val() != "") {

            if (!regInteger.test($("#ctl00_ContentPlaceHolder1_txt_upfloor").val())) {
                msg += "输入地上层数格式错误，请输入数字！</br>";
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_downfloor").val() != "") {

            if (!regInteger.test($("#ctl00_ContentPlaceHolder1_txt_downfloor").val())) {
                msg += "输入地下层数格式错误，请输入数字！</br>";
            }
        }

        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_fzphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[01236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_fzphone").val())) {
                msg += "手机号码格式输入不正确！<br/>";
            }
        }
        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_jiafphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[01236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_jiafphone").val())) {
                msg += "手机号码输入格式不正确！<br/>";
            }
        }


        //收费计划
        //        var sf_rows = $("#sf_datas tr").length;
        //        if (sf_rows == 1) {
        //            msg += "请添加收费计划！</br>";
        //        }

        //备注
        if (txtcpr_Remark == "") {
            $("#ctl00_ContentPlaceHolder1_txtcpr_Remark").val("");
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            $(this).hide();
        }
    });
    //首先加载客户列表
    var chooseCustomer = new ChooseCustomer($("#chooseCustomerContainer"), chooseCustomerCallBack);
    //查询 bianhao 按钮
    $("#btn_getcprnum").click(function () {
        //赋值
        showDivDialogClass.SetParameters({
            "pageSize": "0"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "0", "cprNumUnit", CprNumUnitCallBack);
    })

    //点击单选控件重新加载
    $(":radio[name=cprType]").click(function () {
        var typeID = $("#sele_cprNumUnit option:selected").val();
        if (typeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", typeID, "false", "1", "cprNumUnit", BindCprNumDataCallBack);
        }
    });
    //选择合同编号确定按钮
    $("#btn_seleCprNum").click(function () {
        var cpr_Num = $("#sele_cprNum option:selected").text();
        var cpr_ID = $("#sele_cprNum option:selected").val();
        if (cpr_ID > 0) {
            $("#ctl00_ContentPlaceHolder1_txtcpr_No").val(cpr_Num);
            $("#ctl00_ContentPlaceHolder1_hid_cprno").val(cpr_Num);
            //$("#cprNumDialogDiv").dialog().dialog("close");
        } else {
            $("#cprNo_noSelect").show();
            return false;
        }
    });

    //选择项目经理
    $("#btn_gcfz").click(function () {
        //先赋值
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        var unit_ID = $("#select_gcFzr_Unit").val();
        if (unit_ID < 0 || unit_ID == null) {
            //绑定工程负责部门
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
        }
    });
    //选择承接部门
    //承接部门
    $("#btn_cjbm").click(function () {

        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "prevPage": "cjbm_prevPage",
            "firstPage": "cjbm_firstPage",
            "nextPage": "cjbm_nextPage",
            "lastPage": "cjbm_lastPage",
            "gotoPage": "cjbm_gotoPageIndex",
            "allDataCount": "cjbm_allDataCount",
            "nowIndex": "cjbm_nowPageIndex",
            "allPageCount": "cjbm_allPageCount",
            "gotoIndex": "cjbm_pageIndex",
            "pageSize": "10"
        });
        var isValue = $("#ctl00_ContentPlaceHolder1_txt_cjbm").val().length;
        if (isValue == null || isValue == undefined || isValue <= 0) {

            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
            BindAllDataCount(); //绑定总数据
        }
    });
    //合同类型
    $("#btn_cprType").click(function () {
        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "cprType", CproTypeCallBack);

    });
    //甲方负责人
    $("#btn_jffz").click(function () {

        $("#txtName").val("");
        $("#txtPhone").val("");
        $("#txtCompName").val("");
        jffzr_strWhere_Serch = "";
        $("#jffzr_table tr:gt(0)").remove();
        //先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "5"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "1", "jffzrCpr", JffzrCallBack);
        BindAllDataCountJffzr(); //绑定总数据
    });
    $("#btn_serch").click(function () {
        var str_name = $("#txtName").val();
        var str_phone = $("#txtPhone").val();
        var str_cpyName = $("#txtCompName").val();
        var strWhere = "";
        strWhere += "&Name=" + str_name + "&Phone=" + str_phone + "&Department=" + str_cpyName;
        jffzr_strWhere_Serch = strWhere;
        jffzr_strWhere_Serch = showDivDialogClass.ReplaceChars(jffzr_strWhere_Serch);
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", jffzr_strWhere_Serch, "true", "1", "jffzrCpr", JffzrCallBack);
        BindAllDataCountJffzr();//绑定总数据
    });
    //增加计划收费
    $("#btn_AddSf").click(function () {
        $("h4", "#TJSF").text("添加计划收费");
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val();
        chargeid = "0";
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else {
                AddChargePlan();
                $("#chargeType_notselect").hide();
                $("#jine_notnull").hide();
                $("#jine_notint").hide();
                $("#jine_xiaoyu").hide();
                $("#date_notnull").hide();
                $("#span_Percent").hide();
                $("#span_PercentNotInt").hide();
            }
        } else {
            alert("请填写合同额！");
            return false;
        }

    });
    //增加计划收费确定按钮
    $("#btn_addPlanCharge").click(function () {
        var tempId = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
        var txtPlanChargeNum = $("#txt_planChargeNum").val();
        var lblCprMoney = $("#lbl_copMoney").text();
        var txtDatePic = $("#txt_datePicker").val();
        var txtSkr = $("#ctl00_ContentPlaceHolder1_userShortName").val();
        var txtRemark = $("#txt_chargeRemark").val();
        var data = "action=addcprsktype&flag=add&cpr_type=construcharge";
        //金额
        if (txtPlanChargeNum == "") {
            $("#jine_xiaoyu").hide();
            $("#jine_notint").hide();
            $("#jine_notnull").show();
            return false;
        } else {
            $("#jine_notnull").hide();
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
            if (!reg.test(txtPlanChargeNum)) {
                $("#jine_xiaoyu").hide();
                $("#jine_notint").show();
                return false;
            } else {
                $("#jine_notint").hide();
            }
            //判断合同额
            if (parseFloat(lblCprMoney) < parseFloat(txtPlanChargeNum)) {
                $("#jine_xiaoyu").show();
                return false;
            } else {
                $("#jine_xiaoyu").hide();
            }
            data += "&jine=" + escape(txtPlanChargeNum);
        }
        //合同总金额
        data += "&allcount=" + escape($("#lbl_copMoney").text());
        //时间
        if (txtDatePic == "") {
            $("#date_notnull").show();
            return false;
        } else {
            $("#date_notnull").hide();
            data += "&date=" + escape($("#txt_datePicker").val());
        }
        //收款人
        if (txtSkr == "") {
            $("#skr_notnull").show();
            return false;
        } else {
            $("#skr_notnull").hide();
            data += "&skr=" + escape(txtSkr);
        }

        data += "&mark=" + escape(txtRemark) + "&cprid=" + tempId + "&chargeid=" + chargeid + "&payType=" + escape($("#chargeTypeSelect option:selected").text());
        //data = encodeURI(data);
        //添加收款信息
        $.ajax({
            type: "GET",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            dataType: "text",
            success: function (result) {
                if (result == "yes") {
                    //alert("添加收款信息成功!");
                    loadCprCharge();
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").attr("readonly", "readonly");
                }
                else {
                    alert("计划收款金额超过合同总额！合同余额：" + result + "万元。");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误！");
            }
        });
    });
    //修改计划收费
    $("span[class=update]").live("click", function () {
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val();
        chargeid = $(this).attr("rel");
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else {
                $("h4", "#TJSF").text("编辑计划收费");
                $("#lbl_copMoney").text(cprAllcount);
                //初始化信息
                var trtd = $(this).parent().parent().find("TD");
                $("#txt_planChargeNum").val(trtd.eq(2).text());
                $("#txt_datePicker").val(trtd.eq(3).text());
                $("#txt_chargeRemark").val(trtd.eq(4).text());
                var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val(); //总金额
                $("#lbl_copMoney").text(cprAllcount);
                //收费百分比
                var persent = trtd.eq(1).text();
                persent = persent.substring(0, (persent.length - 1));
                $("#planChargeNumPercent").val(persent);
                //收费金额
                $("#txt_planChargeNum").unbind('change').change(function () {
                    var chargeNum = $(this).val();
                    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                    if (!reg.test(chargeNum)) {
                        $("#jine_notint").show();
                        return false;
                    } else {
                        $("#jine_notint").hide();
                    }
                    var lblCprMoney = $("#lbl_copMoney").text();
                    $("#planChargeNumPercent").val((parseFloat(chargeNum) / parseFloat(lblCprMoney) * 100).toFixed(2));
                });

                $("#chargeType_notselect").hide();
                $("#jine_notnull").hide();
                $("#jine_notint").hide();
                $("#jine_xiaoyu").hide();
                $("#date_notnull").hide();
            }
        } else {
            alert("请填写合同额！");
            return false;
        }

    });
    //添加子项
    $("#btn_addSub").click(function () {
        subid = "0";
        var timespan = new Date();
        $("h4", "#ZXObject").text("添加承包范围及工程量");
        //清空
        $("input ", "#addsubDivDialog").val("");
        $("#txt_sub_Construction_Name").val("");
        $("span", "#addsubDivDialog").hide();

    });

    //修改子项
    $("span[id=update]").live("click", function () {
        subid = $(this).attr("rel");
        var timespan = new Date();
        $("h4", "#ZXObject").text("编辑承包范围及工程量");
        //查询信息
        var trtd = $(this).parent().parent().find("TD");
        $("#txt_sub_Construction_Name").val(trtd.eq(1).text());
        $("#txt_sub_Project_Diameter").val(trtd.eq(2).text());
        $("#txt_sub_Quantity").val(trtd.eq(3).text());
        $("#txt_sub_Pile_Length").val(trtd.eq(4).text());
        $("#txt_sub_Pit_Area").val(trtd.eq(5).text());
        $("#txt_sub_Depth").val(trtd.eq(6).text());
        $("#txt_Support_Form").val(trtd.eq(7).text());
        $("#txt_sub_Well_Amount").val(trtd.eq(8).text());
        $("#txt_sub_Concrete_Amount").val(trtd.eq(9).text());
        $("#txt_sub_Reinforced_Amount").val(trtd.eq(10).text());
    });
    //工程子项-按钮事件
    $("#btn_addsubChild").click(function () {

        //施工内容
        var sub_Construction_Name = $("#txt_sub_Construction_Name").val();
        //工程桩径
        var sub_Project_Diameter = $("#txt_sub_Project_Diameter").val();
        //数量（根）
        var sub_Quantity = $("#txt_sub_Quantity").val();

        var cstno = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();


        var tableTrCount = $("#datas tr:gt(0)").length;


        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
        var regInt = /^[0-9]\d*$/;
        var data = "action=cpraddsubConstru&flag=add&cstno=" + cstno + "&subid=" + subid;

        //判断输入是否合法
        //施工内容
        if (sub_Construction_Name == "" || sub_Construction_Name == null || sub_Construction_Name == NaN) {
            $("#sub_Construction_NameNull").show();
            return false;
        } else {
            $("#sub_Construction_NameNull").hide();
            data += "&Construction_Name=" + sub_Construction_Name;
        }

        //工程桩径
        if (sub_Project_Diameter == "" || sub_Project_Diameter == null || sub_Project_Diameter == NaN) {
            $("#sub_Project_DiameterNull").show();
            $("#sub_Project_DiameterNoInt").hide();
            return false;
        } else {
            $("#sub_Project_DiameterNull").hide();
            if (!reg.test(sub_Project_Diameter)) {
                $("#sub_Project_DiameterNoInt").show();;
                return false;
            } else {
                $("#sub_Project_DiameterNoInt").hide();
            }

            data += "&Project_Diameter=" + sub_Project_Diameter;
        }

        //数量（根）
        if (sub_Quantity == "" || sub_Quantity == null || sub_Quantity == NaN) {
            $("#sub_QuantityNull").show();
            $("#sub_Project_DiameterNoInt").hide();
            return false;
        } else {
            $("#sub_QuantityNull").hide();
            if (!regInt.test(sub_Quantity)) {
                $("#sub_QuantityNoInt").show();;
                return false;
            } else {
                $("#sub_QuantityNoInt").hide();
            }

            data += "&Quantity=" + sub_Quantity;
        }

        //桩长
        var sub_Pile_Length = $("#txt_sub_Pile_Length").val();
        if (sub_Pile_Length == "" || sub_Pile_Length == null || sub_Pile_Length == NaN) {
            $("#sub_Pile_LengthNull").show();
            $("#sub_Pile_LengthNoInt").hide();
            return false;
        } else {
            $("#sub_Pile_LengthNull").hide();
            if (!reg.test(sub_Pile_Length)) {
                $("#sub_Pile_LengthNoInt").show();;
                return false;
            } else {
                $("#sub_Pile_LengthNoInt").hide();
            }

            data += "&Pile_Length=" + sub_Pile_Length;
        }

        //基坑面积(㎡)
        var sub_Pit_Area = $("#txt_sub_Pit_Area").val();
        if (sub_Pit_Area == "" || sub_Pit_Area == null || sub_Pit_Area == NaN) {
            $("#sub_Pit_AreaNull").show();
            $("#sub_Pit_AreaNoInt").hide();
            return false;
        } else {
            $("#sub_Pit_AreaNull").hide();
            if (!reg.test(sub_Pit_Area)) {
                $("#sub_Pit_AreaNoInt").show();;
                return false;
            } else {
                $("#sub_Pit_AreaNoInt").hide();
            }

            data += "&Pit_Area=" + sub_Pit_Area;
        }

        //深度
        var sub_Depth = $("#txt_sub_Depth").val();
        if (sub_Depth == "" || sub_Depth == null || sub_Depth == NaN) {
            $("#sub_DepthNull").show();
            $("#sub_DepthNoInt").hide();
            return false;
        } else {
            $("#sub_DepthNull").hide();
            if (!reg.test(sub_Depth)) {
                $("#sub_DepthNoInt").show();;
                return false;
            } else {
                $("#sub_DepthNoInt").hide();
            }

            data += "&Depth=" + sub_Depth;
        }

        //降水井数量
        var sub_Well_Amount = $("#txt_sub_Well_Amount").val();
        if (sub_Well_Amount == "" || sub_Well_Amount == null || sub_Well_Amount == NaN) {

        } else {

            if (!reg.test(sub_Well_Amount)) {
                $("#sub_Well_AmountNoInt").show();;
                return false;
            } else {
                $("#sub_Well_AmountNoInt").hide();
            }

            data += "&Well_Amount=" + sub_Well_Amount;
        }

        //混凝土计划用量
        var sub_Concrete_Amount = $("#txt_sub_Concrete_Amount").val();
        if (sub_Concrete_Amount == "" || sub_Concrete_Amount == null || sub_Concrete_Amount == NaN) {

        } else {

            if (!reg.test(sub_Concrete_Amount)) {
                $("#sub_Concrete_AmountNoInt").show();;
                return false;
            } else {
                $("#sub_Concrete_AmountNoInt").hide();
            }

            data += "&Concrete_Amount=" + sub_Concrete_Amount;
        }

        //钢筋计划用量
        var sub_Reinforced_Amount = $("#txt_sub_Reinforced_Amount").val();
        if (sub_Reinforced_Amount == "" || sub_Reinforced_Amount == null || sub_Reinforced_Amount == NaN) {

        } else {

            if (!reg.test(sub_Reinforced_Amount)) {
                $("#sub_Reinforced_AmountNoInt").show();;
                return false;
            } else {
                $("#sub_Reinforced_AmountNoInt").hide();
            }

            data += "&Reinforced_Amount=" + sub_Reinforced_Amount;
        }
        data += "&Support_Form=" + $("#txt_Support_Form").val();

        $.ajax({
            type: "POST",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            success: function (result) {
                if (result == "yes") {
                    //alert("添加子项成功！"); 
                    loadSubCopration();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误！");
            }
        });
    });
});
//选择用户callback
function chooseCustomerCallBack(recordObj) {

    $("#ctl00_ContentPlaceHolder1_txtCst_No").val(recordObj.CustomerNo);
    $("#ctl00_ContentPlaceHolder1_txtCst_Name").val(recordObj.CustomerName);
    $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val(recordObj.Address);
    $("#ctl00_ContentPlaceHolder1_txtCode").val(recordObj.ZipCode == "0" ? "" : recordObj.ZipCode);
    $("#ctl00_ContentPlaceHolder1_txtLinkman").val(recordObj.LinkMan);
    $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val(recordObj.Phone);
    $("#ctl00_ContentPlaceHolder1_txtCpy_Fax").val(recordObj.Fax);
    $("#ctl00_ContentPlaceHolder1_hid_cstid").val(recordObj.SysNo);
    $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val(recordObj.CustomerShortName);
}
//===========合同编号所属院CallBack
//合同编号
function CprNumUnitCallBack(result) {
    if ($("#ctl00_ContentPlaceHolder1_txtcpr_No").val() == "" || $("#ctl00_ContentPlaceHolder1_txtcpr_No").val() == "(例：BJ2012-09)") {
        var data = result == null ? "" : result.ds;
        var cpr_typeSelect_htm = '<option value="-1">--------请选择--------</option>';
        $.each(data, function (i, n) {
            cpr_typeSelect_htm += '<option value="' + n.ID + '">' + n.CprType + '</option>';
        });
        $("#cpr_typeSelect").html(cpr_typeSelect_htm);
        $('#cpr_numSelect').empty();
    }
    //注册合同类型选项改变的事件
    $("#cpr_typeSelect").unbind('change').change(function () {
        $("#noselectMsg").hide();
        var CprTypeID = $(this).val();
        if (CprTypeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", CprTypeID, "false", "0", "cprNumUnit", CprNumCallBack);
        } else {
            $('#cpr_numSelect').empty();
        }

    });
    //注册确定按钮点击事件
    $("#btn_cprNum_close").unbind('click').click(function () {
        var CprNum = $("#cpr_numSelect").find("option:selected").text();
        if (CprNum != NaN && CprNum != "" && CprNum != undefined) {
            $("#noselectMsg").hide();
            $("#ctl00_ContentPlaceHolder1_txtcpr_No").val(CprNum);
            $("#ctl00_ContentPlaceHolder1_hid_cprno").val(CprNum);

        } else {
            $("#noselectMsg").show();
        }
    });
}
function CprNumCallBack(result) {
    var obj = result == null ? "" : result.ds;
    var cpr_TypeSelectChangeHtml;
    var prevTxt;
    $.each(obj, function (i, n) {
        var startNums = n.CprNumStart.split('-');
        var endNums = n.CprNumEnd.split('-');
        prevTxt = startNums[0] + "-";
        var startNum = Math.abs(GetCprNumFix(startNums[1])); //开始编号
        var endNum = Math.abs(GetCprNumFix(endNums[1])); //结束编号
        for (var i = startNum; i <= endNum; i++) {
            var cprNumOk = prevTxt + GetCprNumFix(i);
            cpr_TypeSelectChangeHtml += '<option cprNum="' + cprNumOk + '" value="' + GetCprNumFix(i) + '">' + cprNumOk + '</option>';
        }
    });
    $('#cpr_numSelect').empty();
    $('#cpr_numSelect').html(cpr_TypeSelectChangeHtml);
    //去除已使用的编号
    BindCprNumber(prevTxt);
}
//处理合同编号数字
function GetCprNumFix(cprNo) {
    if (cprNo < 10) {
        return "00" + cprNo;
    } else if (10 <= cprNo && cprNo < 100) {
        return "0" + cprNo;
    } else { return cprNo; }
}
//去除已使用的合同编号
function BindCprNumber(fixstr) {
    var data = "action=removeUsedCprNum&fixStr=" + fixstr;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                $.each(obj, function (i, n) {
                    $("#cpr_numSelect option[cprNum=" + n.cpr_No + "]").remove();
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!未能去除已使用的合同编号!");
        }
    });
}
//日期
function datemethod() {
    var gq = 0;
    var startdate = $("#ctl00_ContentPlaceHolder1_txtSingnDate2").val();
    var enddate = $("#ctl00_ContentPlaceHolder1_txtCompleteDate").val();
    if (enddate != "" && startdate != "") {
        gq = DateDiff(enddate, startdate);
    }

    $("#ctl00_ContentPlaceHolder1_txt_ProjectDate").val(gq);

}
//两个日期的差值(d1 - d2). 
function DateDiff(d1, d2) {
    var day = 24 * 60 * 60 * 1000;
    try {
        var dateArr = d1.split("-");
        var checkDate = new Date();
        checkDate.setFullYear(dateArr[0], dateArr[1] - 1, dateArr[2]);
        var checkTime = checkDate.getTime();

        var dateArr2 = d2.split("-");
        var checkDate2 = new Date();
        checkDate2.setFullYear(dateArr2[0], dateArr2[1] - 1, dateArr2[2]);
        var checkTime2 = checkDate2.getTime();

        var cha = (checkTime - checkTime2) / day;
        return parseInt(cha);
    } catch (e) {
        return false;
    }
}//end fun 
//=====合同编号
//合同类型绑定表格CallBack
function CproTypeCallBack(result) {
    $("#customerCompactTable tr:gt(0)").remove();
    if (result != null) {
        var data = result.ds;
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.ID + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var trHtml = "<tr><td>" + (i + 1) + "</td><td>" + n.dic_Name + "</td><td>" + oper + "</td></tr>";
            $("#customerCompactTable").append(trHtml);
            $("#customerCompactTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_cprType").val(n.dic_Name);

            });
        });
        ControlTableCss("customerCompactTable");
    }
}

//////---------项目经理工程负责人
//工程负责人绑定部门CallBack
function CprTypeUnitCallBack(result) {
    if (result != null) {

        var data = result.ds;
        var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
        $.each(data, function (i, n) {
            gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
        });
        $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
        //注册部门选项改变事件
        showDivDialogClass.SetParameters({ "pageSize": "10" });
        $("#select_gcFzr_Unit").unbind('change').change(function () {
            var unit_ID = $("#select_gcFzr_Unit").val();
            if (Math.abs(unit_ID) > 0) {
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
                BindAllDataCountGcfzr(unit_ID);
            }
        });
    }
}
//绑定工程负责人表格数据CallBack
function BindGcfzrDataCallBack(result) {
    if (result != null) {

        var obj = result.ds;
        var gcFzrMemTableHtml;
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $.each(obj, function (i, n) {
            var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\" >选择</span>";
            gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + oper + "</td></tr>";
            $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
            $("#gcFzr_MemTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_proFuze").val(n.mem_Name);
                $("#ctl00_ContentPlaceHolder1_txt_fzphone").val(n.mem_Mobile);
                //增加项目经理ID qpl 20131225
                $("#ctl00_ContentPlaceHolder1_HiddenPMUserID").val(n.mem_ID);
            });
        });
        for (var trBland = 0; trBland < ParametersDict.pageSize - obj.length; trBland++) {

        }
        ControlTableCss("gcFzr_MemTable");
    }
}
//绑定工程负责人数据总数
function BindAllDataCountGcfzr(unit_ID) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "gcfzr_prevPage",
        "firstPage": "gcfzr_firstPage",
        "nextPage": "gcfzr_nextPage",
        "lastPage": "gcfzr_lastPage",
        "gotoPage": "gcfzr_gotoPageIndex",
        "allDataCount": "gcfzr_allDataCount",
        "nowIndex": "gcfzr_nowPageIndex",
        "allPageCount": "gcfzr_allPageCount",
        "gotoIndex": "gcfzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
    //注册事件,先注销,再注册
    $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#gcfzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
        }
    });
}
//项目经理数据总数CallBack
function GetGcfzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $("#gcfzr_allDataCount").text(0);
        $("#gcfzr_nowPageIndex").text(0);
        $("#gcfzr_allPageCount").text(0);
        NoDataMessageOnTable("gcFzr_MemTable", 3);
    }
}
////---------项目经理工程负责人
///-----------------承接部门
//承接部门表格数据绑定-CallBack函数
function ProCjbmCallBack(result) {
    if (result != null) {

        var data = result.ds;
        $("#pro_cjbmTable tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
            $("#pro_cjbmTable").append(trHtml);
            $("#pro_cjbmTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_cjbm").val($.trim(n.unit_Name));
                $("#ctl00_ContentPlaceHolder1_hid_cjbm").val($.trim(n.unit_Name));

            });
        });
        for (var trBland = 0; trBland < ParametersDict.pageSize - data.length; trBland++) {

        }
        ControlTableCss("pro_cjbmTable");
    }
}
//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
        }
    });
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}
///---------------承接部门
////////////----------甲方负责人
//获取甲方负责人总数据
function BindAllDataCountJffzr() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "jffzr_prevPage",
        "firstPage": "jffzr_firstPage",
        "nextPage": "jffzr_nextPage",
        "lastPage": "jffzr_lastPage",
        "gotoPage": "jffzr_gotoPage",
        "allDataCount": "jffzr_totalCount",
        "nowIndex": "jffzr_nowPageIndex",
        "allPageCount": "jffzr_PagesCount",
        "gotoIndex": "jffzr_gotoPageNum",
        "pageSize": "5"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", jffzr_strWhere_Serch, "true", "jffzrCpr", GetJffzrAllDataCount);

    //注册分页事件
    $("#jffzr_forPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", jffzr_strWhere_Serch, "true", pageIndex, "jffzrCpr", JffzrCallBack);
        }
    });
}
//甲方负责人数据总数CallBack函数
function GetJffzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#jffzr_table tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_table", 4);
    }
}
//甲方负责人绑定表格数据CallBack
function JffzrCallBack(result) {
    if (result != null) {
        var data = result.ds;
        var jffzrHtml;
        $("#jffzr_table tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.Cst_Id + "'style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            jffzrHtml = '<tr><td>' + n.Name + '</td><td>' + n.Phone + '</td><td>' + n.Department + '</td><td>' + oper + '</td></tr>';
            $("#jffzr_table").append(jffzrHtml);
            $("#jffzr_table span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txtFParty").val($.trim(n.Name));
                $("#ctl00_ContentPlaceHolder1_txt_jiafphone").val($.trim(n.Phone));

            });
        });
        ControlTableCss("jffzr_table");
    }
}
//////*-----甲方负责人3
////////////------------添加计划收费
//添加收款计划 by fbw
function AddChargePlan() {
    $("#lbl_copMoney").text(cprAllcount);
    //每次初始化信息
    $("#ctl00_ContentPlaceHolder1_chargeTypeSelect").val(-1);
    $("#txt_planChargeNum").val("");
    $("#txt_datePicker").val("");
    $("#txt_chargeRemark").val("");
    $("#planChargeNumPercent").val("");
    var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val(); //总金额
    $("#lbl_copMoney").text(cprAllcount);

    $("#txt_planChargeNum").change(function () {
        var chargeNum = $(this).val();
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if (!reg.test(chargeNum)) {
            $("#jine_notint").show();
            return false;
        } else {
            $("#jine_notint").hide();
        }
        var lblCprMoney = $("#lbl_copMoney").text();
        $("#planChargeNumPercent").val((parseFloat(chargeNum) / parseFloat(lblCprMoney) * 100).toFixed(2));
    });

    //比例
    $("#planChargeNumPercent").change(function () {
        var chargeNum = $(this).val();
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if (!reg.test(chargeNum)) {
            $("#span_PercentNotInt").show();
            return false;
        } else {
            $("#span_PercentNotInt").hide();
        }
        var lblCprMoney = $("#lbl_copMoney").text();
        $("#txt_planChargeNum").val(parseFloat(chargeNum) * parseFloat(lblCprMoney) / 100);
    });
}

//////////-------------- 计划收费
//加载收款信息
function loadCprCharge() {
    var data = "action=getplanchargetype&cpr_type=construcharge&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if ($("#sf_datas tr").length > 1) {
                    $("#sf_datas tr:gt(0)").remove();
                }
                $.each(data, function (i, n) {
                    var row = $("#sf_row").clone();
                    var oper = "<span class='update' rel='" + n.ID + "' style='cursor: pointer;color:blue;' data-toggle=\"modal\" href=\"#TJSF\">编辑</span>&nbsp;<span class='del' rel='" + n.ID + "' style='cursor: pointer;color:blue;'>删除</span>";
                    row.find("#sf_id").text(i + 1);
                    row.find("#sf_bfb").text(n.persent + "%");
                    row.find("#sf_edu").text(n.payCount);
                    row.find("#sf_time").html(n.paytime);
                    var markShow = n.mark;
                    if (n.mark.length > 20) {
                        markShow = markShow.substr(0, 20) + "...";
                    }
                    row.find("#sf_mark").attr("title", n.mark);
                    row.find("#sf_mark").html(markShow); //新增,备注
                    row.find("#sf_oper").html(oper);
                    row.find("#sf_oper span[class=del]").click(function () {
                        if (confirm("确定要删除此条计划收费吗？")) {
                            //删除事件
                            delChargeItem($(this));
                        }
                    });
                    //添加样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#sf_datas");
                });

                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误3！");
        }
    });
}
//删除计划收费
function delChargeItem(link) {
    var data = "action=delchargeitemtype&cpr_type=construcharge&chrgid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                //  alert("计划收费删除成功！");
                if (($("#sf_datas tr").length <= 1) && ($("#datas tr").length <=
 1)) {
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").removeAttr("readonly");
                }

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误4！");
        }
    });
}
/////////--------------- 计划收费
//------------------------------子项
//加载分项信息
function loadSubCopration() {
    var data = "action=getsubitemdataConstru&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if ($("#datas tr").length > 1) {
                    $("#datas tr:gt(0)").remove();
                }
                var i_rowindex = 0;
                //总建筑面积
                var totalArea = 0;
                var totalMoney = 0;
                var totalLiving_Area = 0;
                $.each(data, function (i, n) {
                    var row = $("#sub_row").clone();
                    var oper = "<span id='update' rel='" + n.ID + "' style='cursor: pointer;color:blue;' data-toggle=\"modal\" href=\"#ZXObject\">编辑</span>&nbsp;<span id='del' rel='" + n.ID + "' style='cursor: pointer;color:blue;'>删除</span>";
                    //增加行
                    i_rowindex++
                    row.find("#sub_id").text(i_rowindex);

                    row.find("#sub_Diameter").text(n.Project_Diameter);
                    row.find("#sub_Quantity").text(n.Quantity);
                    row.find("#sub_Length").text(n.Pile_Length);
                    row.find("#sub_PitArea").text(n.Pit_Area);
                    row.find("#sub_Depth").text(n.Depth);
                    row.find("#sub_Form").text(n.Support_Form);
                    row.find("#sub_Well").text(n.Well_Amount);
                    row.find("#sub_Concrete").text(n.Concrete_Amount);
                    row.find("#sub_Reinforced").text(n.Reinforced_Amount);
                    row.find("#sub_oper").html(oper);

                    var cprRemark = n.Construction_Name;
                    if (cprRemark.length > 20) {
                        cprRemark = cprRemark.substr(0, 20) + "...";
                    }

                    row.find("#sub_name").attr("title", n.Remark);
                    row.find("#sub_name").html(cprRemark);
                    row.find("#sub_oper span[id=del]").click(function () {
                        if (confirm("确定要删除此条分项信息吗？")) {
                            //删除事件
                            delSubItem($(this));
                        }
                    });
                    //添加样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas");
                });

                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除子项
function delSubItem(link) {
    var data = "action=delsubitemConstru&subid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                //  alert("删除子项成功！");
                if (($("#datas tr").length <= 1)) {
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").removeAttr("readonly");
                    $("#ctl00_ContentPlaceHolder1_txt_buildArea").removeAttr("readonly");

                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误6！");
        }
    });
}
///------子项
//删除附件
//加载数据
function GetAttachData() {
    loadCoperationAttach();
}
//加载附件信息
function loadCoperationAttach() {
    var data = "action=getcprfilestype&cpr_type=construtype&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {

                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href='#' rel='" + n.ID + "'>删除</a>";
                    var oper2 = "<a href='../Attach_User/filedata/cprfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filename").attr("align", "left");
                    row.find("#att_filesize").text(n.FileSizeString + 'KB');
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper a").click(function () {
                        if (confirm("确定要删除本条附件吗？")) {
                            delCprAttach($(this));
                        }
                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误7！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                //加载附件
                link.parents("tr:first").remove();
                // alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//无数据提示                     

function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}

//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}
