﻿var PitCorperation = function () {

    var tempRandom = Math.random() + new Date().getMilliseconds();

    $("#jqGrid4").jqGrid({
        url: '/HttpHandler/Coperation/PitCoperationListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '','','', '合同编号', '工程名称', '合同分类', '占地面积(亩)', '合同额(万元)', '承接部门', '签订日期', '完成日期','统计年份', '工程负责人', '基坑等级', '建设单位', '建筑类型', '层数', '工程负责人电话', '甲方负责人', '甲方负责人电话', '工程地点', '行业性质', '实际合同额(万元)', '投资额(万元)', '实际投资额(万元)', '基坑深度(m)', '边坡高度(m)', '位移监测点数量(个)', '沉降观测点数量(个)', '监测次数(次)', '累计监测点次(次)', '工期(天)', '制表人', '工程来源', '多栋楼', '合同备注', '录入人', '录入时间', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                              { name: 'sumarea', index: 'sumarea', hidden: true, editable: true },
                             { name: 'sumaccount', index: 'sumaccount', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 80, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 200, formatter: colNameShowFormatter4 },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 80, align: 'center' },                             
                             { name: 'BuildArea', index: 'BuildArea', width: 80, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 70, align: 'center' },
                             { name: 'qdrq', index: 'cpr_SignDate2', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'cpr_DoneDate', width: 80, align: 'center' },
                             { name: 'tjrq', index: 'cpr_SignDate', width: 80, align: 'center' },
                             {
                                 name: 'PMUserName', index: 'ChgPeople', width: 60, align: 'center', formatter: function colShowName(celvalue, options, rowData) {
                                     return $.trim(celvalue);
                                 }
                             },
                              { name: 'PitLevel', index: 'PitLevel', width: 80, align: 'center', hidden: true },
                               { name: 'BuildUnit', index: 'BuildUnit', width: 80, align: 'center', hidden: true },
                               { name: 'BuildType', index: 'BuildType', width: 60, align: 'center', hidden: true },
                               {
                                   name: 'Floor', index: 'Floor', width: 80, align: 'center', hidden: true, formatter: function colShowName(celvalue, options, rowData) {
                                       if (celvalue != null && celvalue != "null" && celvalue != "") {
                                           var arr = $.trim(celvalue).split("|");
                                           return "地上:" + $.trim(arr[0]) + " 地下:" + $.trim(arr[1]);
                                       }
                                       return "";
                                   }
                               },
                             { name: 'ChgPhone', index: 'ChgPhone', width: 80, align: 'center', hidden: true },
                               { name: 'ChgJia', index: 'ChgJia', width: 80, align: 'center', hidden: true },
                              { name: 'ChgJiaPhone', index: 'ChgJiaPhone', width: 80, align: 'center', hidden: true },
                             { name: 'BuildPosition', index: 'BuildPosition', width: 80, align: 'center', hidden: true },
                             { name: 'Industry', index: 'Industry', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', width: 80, align: 'center', hidden: true },
                              { name: 'cpr_Touzi', index: 'cpr_Touzi', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiTouzi', index: 'cpr_ShijiTouzi', width: 80, align: 'center', hidden: true },
                               { name: 'PitHeight', index: 'PitHeight', width: 80, align: 'center', hidden: true },
                             { name: 'SlopeHeight', index: 'SlopeHeight', width: 80, align: 'center', hidden: true },
                             { name: 'PointNumber', index: 'PointNumber', width: 80, align: 'center', hidden: true },
                             { name: 'TestNumber', index: 'TestNumber', width: 80, align: 'center', hidden: true },
                              { name: 'TestCount', index: 'TestCount', width: 80, align: 'center', hidden: true },
                             { name: 'PointSum', index: 'PointSum', width: 80, align: 'center', hidden: true },
                               { name: 'ProjectDate', index: 'ProjectDate', width: 100, align: 'center', hidden: true },
                               { name: 'TableMaker', index: 'TableMaker', width: 80, align: 'center', hidden: true },
                                    { name: 'BuildSrc', index: 'BuildSrc', width: 80, align: 'center', hidden: true },
                                { name: 'MultiBuild', index: 'MultiBuild', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Mark', index: 'cpr_Mark', width: 80, align: 'center', hidden: true },
                             { name: 'InsertUser', index: 'InsertUserID', width: 100, align: 'center', hidden: true },
                              { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter4 },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter4 }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager4",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/PitCoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod4,
        loadComplete: loadCompMethod4
    });


    //显示查询
    $("#jqGrid4").jqGrid("navGrid", "#gridpager4", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid4").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid4').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid4').jqGrid('getCell', sel_id[i], 'cpr_Id') + ",";
                            }
                        }
                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        // var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_basesearch").click(function () {
        ClearjqGridParam("jqGrid4");
        //隐藏高级查询       
        //$("#div_cbx4").hide();
        //$("#div_sch4").hide();


        if ($("#ctl00_ContentPlaceHolder1_txt_account1").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account1").val())) {
            $("#ctl00_ContentPlaceHolder1_txt_account1").val("");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_account2").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account2").val())) {
            $("#ctl00_ContentPlaceHolder1_txt_account2").val("");
        }


        //设置值为0基本查询
        $("#hid_cxtype").val("0");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = getCheckedUnitNodes();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_startdate").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_enddate").val();
        var cprtype = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());
        var buildunit = $("#ctl00_ContentPlaceHolder1_txt_cprbuildunit").val();
        var account1 = $("#ctl00_ContentPlaceHolder1_txt_account1").val();
        var account2 = $("#ctl00_ContentPlaceHolder1_txt_account2").val();
        $(".norecords").hide();
        $("#jqGrid4").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/PitCoperationListHandler.ashx?n='" + tempRandom + " '&action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime, "cprtype": cprtype, "buildunit": buildunit, "account1": account1, "account2": account2 },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'
        }).trigger("reloadGrid");
    });


    
    //选择生产部门
    //$("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
    //    var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
    //    var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    //    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //    var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
    //    var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    //    var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
    //    $(".norecords").hide();
    //    $("#jqGrid4").jqGrid('setGridParam', {
    //        url: "/HttpHandler/Coperation/PitCoperationListHandler.ashx?action=sel",
    //        postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
    //        page: 1,
    //        sortname: 'cpr_Id',
    //        sortorder: 'desc'

    //    }).trigger("reloadGrid");
    //});

    //导出excel

    //
    //名称连接
    function colNameShowFormatter4(celvalue, options, rowData) {
        var pageurl = "cpr_ShowPitCoperationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
        return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

    }

    //查看cpr_ShowPitCoperationBymaster
    function colShowFormatter4(celvalue, options, rowData) {
        var pageurl = "cpr_ShowPitCoperationBymaster.aspx?flag=cprlist&cprid=" + celvalue;
        return '<a href="' + pageurl + '" alt="查看合同">查看</a>';

    }
    //编辑
    function colEditFormatter4(celvalue, options, rowdata) {
        if (celvalue != "") {
            var pageurl = "cpr_ModifyPitCoperationBymaster.aspx?cprid=" + celvalue;
            return '<a href="' + pageurl + '" alt="编辑合同" class="allowEdit">编辑</a>';
        } //class="allowEdit"
    }
    //统计 
    function completeMethod4() {
        var sumarea = 0, sumaccount = 0;
        var rowIds = $("#jqGrid4").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#jqGrid4 tr[id=" + rowIds[i] + "]").children("td").eq(1).text((i + 1));
            if (i == 0) {
                sumarea = $.trim($("#jqGrid4 tr[id=" + rowIds[i] + "]").children("td").eq(3).text());
                sumaccount = $.trim($("#jqGrid4 tr[id=" + rowIds[i] + "]").children("td").eq(4).text());
            }
        }
        $("#jqGrid4").footerData('set', { cpr_No: "合计:", BuildArea: sumarea, cpr_Acount: sumaccount });
    }
    //无数据
    function loadCompMethod4() {
        var rowcount = parseInt($("#jqGrid4").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata4").text() == '') {
                $("#jqGrid4").parent().append("<div id='nodata4'>没有查询到数据!</div>");
            }
            else { $("#nodata4").show(); }

        }
        else {
            $("#nodata4").hide();
        }
    }
}
