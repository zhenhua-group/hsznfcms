﻿var SuperCoperation = function () {   
    var tempRandom = Math.random() + new Date().getMilliseconds();

    $("#jqGrid3").jqGrid({
        url: '/HttpHandler/Coperation/SuperCoperationListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '','','', '','','', '合同编号', '工程名称', '合同分类', '建筑规模', '合同额(万元)', '承接部门', '签订日期', '完成日期','统计年份', '工程负责人','已收费(万元)', '建筑类别', '合同类型', '建设单位', '结构形式', '建筑分类', '层数', '工程负责人电话', '外聘人员信息', '甲方负责人', '甲方负责人电话', '工程地点', '行业性质', '实际合同额(万元)', '投资额(万元)', '实际投资额(万元)', '工程来源', '合同阶段', '制表人', '多栋楼', '合同备注', '录入人', '录入时间', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },                             
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                              { name: 'sumarea', index: 'sumarea', hidden: true, editable: true },
                             { name: 'sumaccount', index: 'sumaccount', hidden: true, editable: true },
                             { name: 'sumssze', index: 'sumssze', hidden: true, editable: true },                            
                              { name: 'AreaUnit', index: 'AreaUnit', hidden: true, editable: true },
                              { name: 'AuditSysNo', index: 'AuditSysNo', hidden: true, editable: true },
                              { name: 'AuditStatus', index: 'AuditStatus', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 80, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 150, formatter: colNameShowFormatter3 },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 80, align: 'center' },                            
                             { name: 'BuildArea', index: 'BuildArea', width: 80, align: 'center', formatter:colBuildArea },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 70, align: 'center' },
                             { name: 'qdrq', index: 'cpr_SignDate2', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'cpr_DoneDate', width: 80, align: 'center' },
                             { name: 'tjrq', index: 'cpr_SignDate', width: 80, align: 'center' },
                             {
                                 name: 'PMUserName', index: 'ChgPeople', width: 60, align: 'center', formatter: function colShowName3(celvalue, options, rowData) {
                                     return $.trim(celvalue);
                                 }
                             },
                              { name: 'ssze', index: 'ssze', width: 80, align: 'center' },
                              { name: 'BuildType', index: 'BuildType', width: 60, align: 'center', hidden: true },
                              { name: 'cpr_Type2', index: 'cpr_Type2', width: 80, align: 'center', hidden: true },
                             { name: 'BuildUnit', index: 'BuildUnit', width: 80, align: 'center', hidden: true },
                             { name: 'StructType', index: 'StructType', width: 80, align: 'center', hidden: true },
                             { name: 'BuildStructType', index: 'BuildStructType', width: 80, align: 'center', hidden: true },
                             {
                                 name: 'Floor', index: 'Floor', width: 80, align: 'center', hidden: true, formatter: function colShowName(celvalue, options, rowData) {
                                     if (celvalue != null && celvalue != "null" && celvalue != "") {
                                         var arr = $.trim(celvalue).split("|");
                                         return "地上:" + $.trim(arr[0]) + " 地下:" + $.trim(arr[1]);
                                     }
                                     return "";
                                 }
                             },
                             { name: 'ChgPhone', index: 'ChgPhone', width: 80, align: 'center', hidden: true },
                             { name: 'ExternalMember', index: 'ExternalMember', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJiaPhone', index: 'ChgJiaPhone', width: 80, align: 'center', hidden: true },
                             { name: 'BuildPosition', index: 'BuildPosition', width: 80, align: 'center', hidden: true },
                             { name: 'Industry', index: 'Industry', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Touzi', index: 'cpr_Touzi', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiTouzi', index: 'cpr_ShijiTouzi', width: 80, align: 'center', hidden: true },
                             { name: 'BuildSrc', index: 'BuildSrc', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_Process', index: 'cpr_Process', width: 100, align: 'center', hidden: true, formatter: colProcess },
                             { name: 'TableMaker', index: 'TableMaker', width: 80, align: 'center', hidden: true },
                             { name: 'MultiBuild', index: 'MultiBuild', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Mark', index: 'cpr_Mark', width: 80, align: 'center', hidden: true },
                             { name: 'InsertUser', index: 'InsertUser', width: 100, align: 'center', hidden: true },
                             { name: 'lrsj', index: 'InsertDate', width: 80, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter3 },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter3 }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager3",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/SuperCoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod3,
        loadComplete: loadCompMethod3
    });


    //显示查询
    $("#jqGrid3").jqGrid("navGrid", "#gridpager3", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid3").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid3').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid3').jqGrid('getCell', sel_id[i], 'cpr_Id') + ",";
                            }
                        }
                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        // var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_basesearch").click(function () {
       
        ClearjqGridParam("jqGrid3");
        //隐藏高级查询       
        //$("#div_cbx3").hide();
        //$("#div_sch3").hide();

        //设置值为0基本查询
        $("#hid_cxtype").val("0");

        if ($("#ctl00_ContentPlaceHolder1_txt_account1").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account1").val())) {
            $("#ctl00_ContentPlaceHolder1_txt_account1").val("");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_account2").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account2").val())) {
            $("#ctl00_ContentPlaceHolder1_txt_account2").val("");
        }

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        // var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var unit = getCheckedUnitNodes();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_startdate").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_enddate").val();
        var cprtype = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());
        var buildunit = $("#ctl00_ContentPlaceHolder1_txt_cprbuildunit").val();
        var account1 = $("#ctl00_ContentPlaceHolder1_txt_account1").val();
        var account2 = $("#ctl00_ContentPlaceHolder1_txt_account2").val();
        $(".norecords").hide();
        $("#jqGrid3").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/SuperCoperationListHandler.ashx?n='" + tempRandom + " '&action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime, "cprtype": cprtype, "buildunit": buildunit, "account1": account1, "account2": account2 },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'
        }).trigger("reloadGrid");
    });
   
    ////选择生产部门
    //$("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
    //    var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
    //    var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    //    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //    var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
    //    var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    //    var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
    //    $(".norecords").hide();
    //    $("#jqGrid3").jqGrid('setGridParam', {
    //        url: "/HttpHandler/Coperation/SuperCoperationListHandler.ashx?action=sel",
    //        postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
    //        page: 1,
    //        sortname: 'cpr_Id',
    //        sortorder: 'desc'

    //    }).trigger("reloadGrid");
    //});

//建设规模
function colBuildArea(celvalue, options, rowData)
{
    if (celvalue != "")
    {
       // if ($.trim(rowData["cpr_Type2"]) == "市政工程监理合同" && $.trim(rowData["AreaUnit"]) == "k㎡") {
       //     return parseFloat(celvalue) * 1000000;
       // }
       // else
       // {
        return $.trim(celvalue) +"("+$.trim(rowData["AreaUnit"])+")";
       // }
    }

    return "0.00";
    
}

//
//名称连接
function colNameShowFormatter3(celvalue, options, rowData) {
    var pageurl = "cpr_ShowSuperCoperationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
    return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

}

//查看
function colShowFormatter3(celvalue, options, rowData) {
    var pageurl = "cpr_ShowSuperCoperationBymaster.aspx?flag=cprlist&cprid=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看合同">查看</a>';

}
//编辑
function colEditFormatter3(celvalue, options, rowdata) { 

    if (celvalue != "") {
        var pageurl = "cpr_ModifySuperCoperationBymaster.aspx?cprid=" + celvalue;
        if ($.trim(rowdata["cpr_Type"]) != "项目协议") {

            if (rowdata["AuditSysNo"] != null && $.trim(rowdata["AuditSysNo"]) != "" && $.trim(rowdata["AuditStatus"]) != "C" && $.trim(rowdata["AuditStatus"]) != "E") {
                var pageurl = "javascript:alert('该合同已提交审批，不能被修改！');"
            }
        }
        return '<a href="' + pageurl + '" alt="编辑合同" class="allowEdit">编辑</a>';
    } //class="allowEdit"
}
//统计 
function completeMethod3() {
    var sumarea = 0, sumaccount = 0, sumssze=0;
    var rowIds = $("#jqGrid3").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid3 tr[id=" + rowIds[i] + "]").children("td").eq(1).text((i + 1));
        if (i == 0) {
            sumarea = $.trim($("#jqGrid3 tr[id=" + rowIds[i] + "]").children("td").eq(3).text());
            sumaccount = $.trim($("#jqGrid3 tr[id=" + rowIds[i] + "]").children("td").eq(4).text());
            sumssze = $.trim($("#jqGrid3 tr[id=" + rowIds[i] + "]").children("td").eq(5).text());
        }
    }
    $("#jqGrid3").footerData('set', { cpr_No: "合计:", cpr_Acount: sumaccount, ssze: sumssze });

  
}
//无数据
function loadCompMethod3() {
    var rowcount = parseInt($("#jqGrid3").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata3").text() == '') {
            $("#jqGrid3").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata3").show(); }

    }
    else {
        $("#nodata3").hide();
    }
}
    //合同阶段
function colProcess(celvalue, options, rowData) {
    var temp = "";
    var arr = $.trim(celvalue).split(",");
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == "27") {
            temp = "方案,";
        }
        if (arr[i] == "28") {
            temp += "初设,";
        }
        if (arr[i] == "29") {
            temp += "施工图,";
        }
        if (arr[i] == "30") {
            temp += "其他,";
        }
        if (arr[i] == "103") {
            temp += "施工阶段监理,";
        }
        if (arr[i] == "104") {
            temp += "保修阶段监理,";
        }
    }
    return temp;
}


}

