﻿//数字验证正则    
var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
$(function () {

    //加载点击列表字段的全部事件
    InitColumnsClick();

    //基本合同查询判断
    //$("#btn_basesearch").click(function () {
    //    if ($("#ctl00_ContentPlaceHolder1_txt_account1").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account1").val())) {
    //        alert("合同额格式不正确！");
    //        $("#ctl00_ContentPlaceHolder1_txt_account1").val("");
    //        return false;
    //    }
    //    if ($("#ctl00_ContentPlaceHolder1_txt_account2").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account2").val())) {
    //        alert("合同额格式不正确！");
    //        $("#ctl00_ContentPlaceHolder1_txt_account2").val("");
    //        return false;
    //    }

    //});
    //$(":checkbox").each(function () {
    //    //$(this).parent().attr("class", "");
    //    if ($(this).parent().attr("class") == "checked") {
    //        $(this).click();
    //    }
    //});

    //基本合同查询
    $("#btn_search").click(function () {
        //声明查询方式
        var andor = $("#hid_cxtype").val();

        ClearjqGridParam("jqGrid");

        //权限条件
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同名称
        var txt_cprname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        //合同分类
        var drp_type = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());
        //建筑单位
        var txt_cprBuildUnit = $("#ctl00_ContentPlaceHolder1_txt_cprbuildunit").val();
        //合同额区间值
        var txtcpr_Account = $("#ctl00_ContentPlaceHolder1_txt_account1").val();
        var txtcpr_Account2 = $("#ctl00_ContentPlaceHolder1_txt_account2").val();
        //合同年份
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        //合同编号
        var txtcpr_No = $("#txt_proName").val();
        //合同类型
        var cprtype = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type").find("option:selected").text();

        //建筑规模
        var txt_Area = $("#txt_buildArea").val();
        var txt_Area2 = $("#txt_buildArea2").val();
        //建设单位
        //  var txt_cprBuildUnit = $("#txt_BuildUnit").val();
        //结构形式
        var asTreeviewStruct = GetTreeValues("ctl00_ContentPlaceHolder1_asTreeviewStruct_ulASTreeView");

        //建筑分类
        var asTreeviewStructType = GetTreeValues("ctl00_ContentPlaceHolder1_asTreeviewStructType_ulASTreeView");

        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze").val();
        //甲方负责人
        var txtFParty = $("#txtFParty").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        // var txtcpr_Account = $("#txtproAcount").val();
        // var txtcpr_Account2 = $("#txtproAcount2").val();
        //工程地点
        var txt_Address = $("#txtaddress").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text();
        //合同阶段
        var chk_cprjd = "";
        var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;

        for (var i = 0; i < parseInt(see) ; i++) {
            var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class");
            if (isChecked == undefined || isChecked == "") {

            } else {
                chk_cprjd += $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().parent().next().text() + ",";

            }
        }
        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource").find("option:selected").text();
        //合同统计年份
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
        //合同签订日期
        var txt_signdate_1 = $("#ctl00_ContentPlaceHolder1_txt_signdate_1").val();
        var txt_signdate2_1 = $("#ctl00_ContentPlaceHolder1_txt_signdate2_1").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //合同收费
        var projcharge1 = $("#projcharge1").val();
        var projcharge2 = $("#projcharge2").val();
        //按照客户
        var txt_cst = $("#txt_cstname").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "建筑规模开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "建筑规模结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge1 != "") {
            if (!reg.test(projcharge1)) {
                msg += "合同收费开始值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge2 != "") {
            if (!reg.test(projcharge2)) {
                msg += "合同收费结束值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }

        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "drp_year": year,           //合同年份
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "cprtype": cprtype,         //合同类型
            "txt_cprname": txt_cprname,     //合同名称
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位
            "asTreeviewStruct": asTreeviewStruct,     //结构形式
            "asTreeviewStructType": asTreeviewStructType,     //建筑分类
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质
            "chk_cprjd": chk_cprjd,     //合同阶段
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate_1": txt_signdate_1,     //合同签订日期1
            "txt_signdate2_1": txt_signdate2_1,     //合同签订日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate2,     //合同完成日期2
            "txt_signdate": txt_signdate,     //合同统计年份
            "txt_signdate2": txt_signdate2,     //合同统计年份
            "projcharge1": projcharge1,     //合同收费1
            "projcharge2": projcharge2,     //合同收费2
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };
       
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            mytype: 'POST',
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

});
//获取建筑分类值
function GetTreeValues(obj) {
    var allstr = "";
    //一级   
    $("#" + obj).children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate!="2") {          
        var onestr = ckvalue;
        var tempstr = subGetTreeValues($(this), onestr);
        allstr = allstr + tempstr + "+";
        // }
    });
    if (allstr != "") {
        allstr = allstr.substr(0, (allstr.length - 1));
    }
    return allstr;
}
//二级li里选中的值(递归)
function subGetTreeValues(obj, str) {
    //二级   
    var currallstr = "";
    obj.children("ul").children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate != "2") {
        var substr = "^" + ckvalue;
        var tempstr = childGetTreeValues($(this), substr);
        currallstr = currallstr + tempstr;

        //}
    });
    return str + currallstr;
}
//三级li
function childGetTreeValues(obj, str) {
    var currallstr = "";
    obj.children("ul").children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate != "2") {
        var childstr = str + "*" + ckvalue;
        var tempstr = childGetTreeValues($(this), childstr);
        currallstr = currallstr + tempstr;
        //}
    });
    if (currallstr == "") {
        currallstr = str;
    }
    return currallstr;
}
//高级查询选择多选框字段
function CheckBoxComm(tblid) {

    //绑定事件
    $("input[name=bb]:checkbox", $("#" + tblid)).bind("click", function () {
        var tblschid = $("#" + tblid).attr("for");
        var commName = $(this).parent().parent().siblings("span").attr("for");
        $("#TempTbl").html("");
        //选中
        if ($(this).is(":checked")) {
            //全选点击事件
            if (commName == "chk_All") {


                //显示的所有的字段填写值，还原回去，因为顺序
                $("#tbl_columns>tbody>tr:gt(1) td[for]").each(function (i, n) {
                    var $td = $(n);
                    var $td2 = $(n).next();
                    var forname = $(n).attr("for");
                    $("#" + tblschid + " tr[for=" + forname + "]").append($td);
                    $("#" + tblschid + " tr[for=" + forname + "]").append($td2);
                });
                //移除tr标签
                $("#tbl_columns>tbody>tr:gt(1)").remove();
                //取消选中
                $("input[name=bb]:checkbox:not(:first)", $("#" + tblid)).attr("checked", false);
                $("input[name=bb]:checkbox:not(:first)", $("#" + tblid)).parent().removeClass("checked");

                //重新添加，选中
                var $tr = $("<tr></tr>");
                //循环所有的列
                $(":checkbox:not(:first)", $("#" + tblid)).each(function (i, n) {
                    var forname = $(n).parent().parent().siblings("span").attr("for");
                    var $tempTd = $("#" + tblschid + " tr[for=" + forname + "]").children();
                    $tr.append($tempTd);
                    if (((i + 1) % 3) == 0) {
                        $("#TempTbl").append($tr);
                        $tr = $("<tr></tr>");
                    }
                    //多选框都选中
                    $(n).attr("checked", true);
                    $(n).parent().addClass("checked");
                });
                $("#TempTbl").append($tr);
                //移除tr标签
                $("#tbl_columns>tbody>tr:gt(1)").remove();
                //重新添加进去
                $("#tbl_columns>tbody").append($("#TempTbl>tbody").children());
            }
            else {
                var $td = $("#" + tblschid + " tr[for=" + commName + "]").children();
                var $tr = $("<tr></tr>");

                $("#tbl_columns>tbody>tr:gt(1)>td").each(function (i, n) {
                    var $tempTd = $(n);
                    $tr.append($tempTd);
                    if (((i + 1) % 6) == 0) {
                        $("#TempTbl").append($tr);
                        $tr = $("<tr></tr>");
                    }
                });
                $tr = $tr.append($td);
                $("#TempTbl").append($tr);
                //移除tr标签
                $("#tbl_columns>tbody>tr:gt(1)").remove();
                //重新添加进去
                $("#tbl_columns>tbody").append($("#TempTbl>tbody").children());

                //如果选中的和总共个数相同，全选多选框就被选中
                if (($("input[name=bb]:checkbox:not(:first):checked", $("#" + tblid)).length) == ($("input[name=bb]:checkbox:not(:first)", $("#" + tblid)).length)) {
                    $("input[name=bb]:checkbox:first", $("#" + tblid)).attr("checked", true);
                    $("input[name=bb]:checkbox:first", $("#" + tblid)).parent().addClass("checked");
                }

            }


        }//没有选中
        else {
            //全不选中事件
            if (commName == "chk_All") {
                //循环所有的字段填写值
                $("#tbl_columns>tbody>tr:gt(1) td[for]").each(function (i, n) {
                    var $td = $(n);
                    var $td2 = $(n).next();
                    var forname = $(n).attr("for");
                    $("#" + tblschid + " tr[for=" + forname + "]").append($td);
                    $("#" + tblschid + " tr[for=" + forname + "]").append($td2);
                });
                //移除tr标签
                $("#tbl_columns>tbody>tr:gt(1)").remove();
                //取消选中
                $("input[name=bb]:checkbox", $("#" + tblid)).attr("checked", false);
                $("input[name=bb]:checkbox", $("#" + tblid)).parent().removeClass("checked");


                //清空数据
                $(":text", $("#tbl_columns>tbody>tr:gt(1)")).val("");
                $("select", $("#tbl_columns>tbody>tr:gt(1)")).val("-1");
                if (tblschid == "tbl_id2") {
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text("");
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text("");
                }
                else if (tblschid == "tbl_id1_2") {
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStructType1_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStructType1")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                }
                else if (tblschid == "tbl_id3_2") {
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStruct3_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStruct3")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStructType3_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStructType3")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                }
                else if (tblschid == "tbl_id9_2") {
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStruct9_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStruct9")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStructType9_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStructType9")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                }
                else if (tblschid == "tbl_id10_2") {
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStruct10_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStruct10")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStructType10_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStructType10")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                }
                else if (tblschid == "tbl_id11_2") {
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStruct11_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStruct11")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                    $("#ctl00_ContentPlaceHolder1_asTreeviewStructType11_divDropdownTreeText").text("");
                    $("li", $("#ctl00_ContentPlaceHolder1_asTreeviewStructType11")).each(function () {
                        $(this).attr("checkedstate", "2");
                        $(".astreeview-checkbox", $(this)).attr("src", "/js/astreeview/astreeview/themes/macOS/images/astreeview-checkbox-unchecked.gif")
                    });
                }

                var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;
                for (var i = 0; i < parseInt(see) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class", "");
                }
                //勘察分院合同1
                var see1 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd1").length;
                for (var i = 0; i < parseInt(see1) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd1_" + i).parent().attr("class", "");
                }

                //监理1
                var see3 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd3").length;

                for (var i = 0; i < parseInt(see3) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd3_" + i).parent().attr("class", "");
                }
                //市政
                var see9 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd9").length;

                for (var i = 0; i < parseInt(see9) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd9_" + i).parent().attr("class", "");
                }
                //市政
                var see10 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd10").length;

                for (var i = 0; i < parseInt(see10) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd10_" + i).parent().attr("class", "");
                }

                //市政
                var see11 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd11").length;

                for (var i = 0; i < parseInt(see11) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd11_" + i).parent().attr("class", "");
                }

            }
            else {
                var $td = $("#tbl_columns>tbody>tr:gt(1)").children("td[for=" + commName + "]");
                var $td2 = $("#tbl_columns>tbody>tr:gt(1)").children("td[for=" + commName + "]").next();
                $("#" + tblschid + " tr[for=" + commName + "]").append($td);
                $("#" + tblschid + " tr[for=" + commName + "]").append($td2);

                var $tr = $("<tr></tr>");
                $("#tbl_columns>tbody>tr:gt(1)>td").each(function (i, n) {
                    var $tempTd = $(n);
                    $tr.append($tempTd);
                    if (((i + 1) % 6) == 0) {
                        $("#TempTbl").append($tr);
                        $tr = $("<tr></tr>");
                    }
                });
                $("#TempTbl").append($tr);
                //移除tr标签
                $("#tbl_columns>tbody>tr:gt(1)").remove();
                //重新添加进去
                $("#tbl_columns>tbody").append($("#TempTbl>tbody").children());

                //如果选中的和总共个数相同，全选多选框取消选中
                if (($("input[name=bb]:checkbox:not(:first):checked", $("#" + tblid)).length) != ($("input[name=bb]:checkbox:not(:first)", $("#" + tblid)).length)) {
                    $("input[name=bb]:checkbox:first", $("#" + tblid)).attr("checked", false);
                    $("input[name=bb]:checkbox:first", $("#" + tblid)).parent().removeClass("checked");
                }

            }
            //清空数据
            $(":text", "#tbl_columns td[for=" + commName + "]").next().val("");
            $("select", "#tbl_columns td[for=" + commName + "]").next().val("-1");
            if (commName == "StructType") {
                switch (tblschid) {
                    case "tbl_id2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text("");
                        break;
                    case "tbl_id3_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct3_divDropdownTreeText").text("");
                        break;
                    case "tbl_id9_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct9_divDropdownTreeText").text("");
                        break;
                    case "tbl_id10_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct10_divDropdownTreeText").text("");
                        break;
                    case "tbl_id11_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct11_divDropdownTreeText").text("");
                        break;
                }


            }
            if (commName == "BuildStructType") {
                switch (tblschid) {
                    case "tbl_id2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text("");
                        break;
                    case "tbl_id1_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType1_divDropdownTreeText").text("");
                        break;
                    case "tbl_id3_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType3_divDropdownTreeText").text("");
                        break;
                    case "tbl_id9_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType9_divDropdownTreeText").text("");
                        break;
                    case "tbl_id10_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType10_divDropdownTreeText").text("");
                        break;
                    case "tbl_id11_2":
                        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType11_divDropdownTreeText").text("");
                        break;
                }

            }
            if (commName == "cpr_Process") {
                var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;
                for (var i = 0; i < parseInt(see) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class", "");
                }
                //勘察分院合同1
                var see1 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd1").length;
                for (var i = 0; i < parseInt(see1) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd1_" + i).parent().attr("class", "");
                }

                //监理1
                var see3 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd3").length;

                for (var i = 0; i < parseInt(see3) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd3_" + i).parent().attr("class", "");
                }
                //市政
                var see9 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd9").length;

                for (var i = 0; i < parseInt(see9) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd9_" + i).parent().attr("class", "");
                }

                //规划
                var see10 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd10").length;

                for (var i = 0; i < parseInt(see10) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd10_" + i).parent().attr("class", "");
                }

                //景观
                var see11 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd11").length;

                for (var i = 0; i < parseInt(see11) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd11_" + i).parent().attr("class", "");
                }
            }
        }
    });

    //以前的高级查询方法
    //点击选择条件
    //$("input[name=bb]:checkbox", $("#" + tblid)).click(function () {
    //    //条件名称
    //    // var commName = $.trim($(this).get(0).id);
    //    var commName = $.trim($(this).parent().parent().siblings("span").attr("for"));
    //    //获取数据库字段名      
    //    var columnsname = $.trim($(this).parent().parent().siblings("span").attr("for"));
    //    //获取表头字段名
    //    var columnsChinaName = $.trim($(this).parent().parent().siblings("span").text());
    //    if ($(this).is(":checked")) {
    //        $("#" + tblid2 + " tr[for=" + commName + "]").show();
    //        //添加到数组里
    //        columnslist.push(columnsname);
    //        columnsChinaNameList.push(columnsChinaName);

    //        //显示字段
    //        //  $("#jqGrid").showCol(columnsname).trigger('reloadGrid');
    //    }
    //    else {

    //        $("#" + tblid2 + " tr[for=" + commName + "]").hide();
    //        $("#" + tblid2 + " tr[for=" + commName + "]").removeData();
    //        $(":text", "#" + tblid2 + " tr[for=" + commName + "]").val("");
    //        $("select", "#" + tblid2 + " tr[for=" + commName + "]").val("-1");
    //        //隐藏字段
    //        if (columnsname != "cpr_Name" && columnsname != "ChgPeople" && columnsname != "cpr_Unit" && columnsname != "cpr_Acount" && columnsname != "cpr_Process" && columnsname != "cpr_SignDate" && columnsname != "cpr_DoneDate" && columnsname != "ssze") {
    //            // $("#jqGrid").hideCol(columnsname).trigger('reloadGrid');
    //            $(":text", "#" + tblid2 + " tr[for=" + commName + "]").val("");
    //        }
    //        //删除数组
    //        columnslist.splice($.inArray(columnsname, columnslist), 1);
    //        columnsChinaNameList.splice($.inArray(columnsChinaName, columnsChinaNameList), 1);
    //    }

    //    //有条件显示查询按钮
    //    if ($("input[name=bb]:checkbox:checked", $("#" + tblid)).length > 0) {
    //        $("#" + tblid2 + " tr:last").show();

    //    }
    //    else {
    //        $("#" + tblid2 + " tr:last").hide();
    //        $(":text", "#" + tblid2).val("");

    //    }
    //});   

}

//高级导出
var SearchExport = function () {

    //声明查询方式
    var andor = $("#hid_cxtype").val();

    //显示字段
    var colsname = $("#ctl00_ContentPlaceHolder1_hid_cols").val();
    var colsvalue = $("#ctl00_ContentPlaceHolder1_hid_colsvalue").val();
    //权限条件
    var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
    //合同名称
    var txt_cprname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
    //合同分类
    var drp_type = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());
    //建筑单位
    var txt_cprBuildUnit = $("#ctl00_ContentPlaceHolder1_txt_cprbuildunit").val();
    //合同额区间值
    var txtcpr_Account = $("#ctl00_ContentPlaceHolder1_txt_account1").val();
    var txtcpr_Account2 = $("#ctl00_ContentPlaceHolder1_txt_account2").val();
    //合同年份
    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //合同编号
    var txtcpr_No = $("#txt_proName").val();
    //合同类型
    var cprtype = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type").find("option:selected").text();
    //合同名称
    //  var txt_cprname = $("#txt_cprName").val();
    //建筑规模
    var txt_Area = $("#txt_buildArea").val();
    var txt_Area2 = $("#txt_buildArea2").val();
    //规模单位
    // var drp_AreaUnit = $("#drp_AreaUnit").val();
    //建设单位
    // var txt_cprBuildUnit = $("#txt_BuildUnit").val();
    //结构形式
    var asTreeviewStruct = $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text();
    //建筑分类
    var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text();
    //建筑类别
    var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype").find("option:selected").text();
    //工程负责人
    var txt_proFuze = $("#txt_proFuze").val();
    //甲方负责人
    var txtFParty = $("#txtFParty").val();
    //承接部门
    //var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    var drp_unit = getCheckedUnitNodes();
    //合同额
    //  var txtcpr_Account = $("#txtproAcount").val();
    //  var txtcpr_Account2 = $("#txtproAcount2").val();
    //工程地点
    var txt_Address = $("#txtaddress").val();
    //行业性质
    var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text();
    //合同阶段
    var chk_cprjd = "";
    var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;

    for (var i = 0; i < parseInt(see) ; i++) {
        var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class");
        if (isChecked == undefined || isChecked == "") {

        } else {
            chk_cprjd += $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().parent().next().text() + ",";

        }
    }
    //工程来源
    var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource").find("option:selected").text();
    //合同统计合同
    var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val();
    var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
    //合同签订日期
    var txt_signdate_1 = $("#ctl00_ContentPlaceHolder1_txt_signdate_1").val();
    var txt_signdate2_1 = $("#ctl00_ContentPlaceHolder1_txt_signdate2_1").val();
    //合同完成日期
    var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
    var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
    //录入时间
    var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
    //合同收费
    var projcharge1 = $("#projcharge1").val();
    var projcharge2 = $("#projcharge2").val();
    //按照客户
    var txt_cst = $("#txt_cstname").val();

    //数字验证正则    
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    var msg = "";
    if (txt_Area != "") {
        if (!reg.test(txt_Area)) {
            msg += "建筑规模开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_Area2 != "") {
        if (!reg.test(txt_Area2)) {
            msg += "建筑规模结束值格式错误，请输入数字！</br>";
        }
    }
    if (txtcpr_Account != "") {
        if (!reg.test(txtcpr_Account)) {
            msg += "合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if (txtcpr_Account2 != "") {
        if (!reg.test(txtcpr_Account2)) {
            msg += "合同额结束值格式错误，请输入数字！</br>";
        }
    }
    if (projcharge1 != "") {
        if (!reg.test(projcharge1)) {
            msg += "合同收费开始值格式错误，请输入数字！</br>";
        }
    }
    if (projcharge2 != "") {
        if (!reg.test(projcharge2)) {
            msg += "合同收费结束值格式错误，请输入数字！</br>";
        }
    }
    if (msg != "") {
        jAlert(msg, "提示");
        return false;
    }
    //没有用到
    var params = {
        "strwhere": strwhere,
        "drp_year": year,//合同年份
        "txtcpr_No": txtcpr_No,     //合同编号
        "drp_type": drp_type,     //合同分类
        "txt_cprname": txt_cprname,     //合同名称
        "txt_Area": txt_Area,     //建筑规模1
        "txt_Area2": txt_Area2,     //建筑规模2
        "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位
        "asTreeviewStruct": asTreeviewStruct,     //结构形式
        "asTreeviewStructType": asTreeviewStructType,     //建筑分类
        "drp_buildtype": drp_buildtype,     //建筑类别
        "txt_proFuze": txt_proFuze,     //工程负责人
        "txtFParty": txtFParty,     //甲方负责人
        "drp_unit": drp_unit,     //承接部门
        "txtcpr_Account": txtcpr_Account,     //合同额1
        "txtcpr_Account2": txtcpr_Account2,     //合同额2
        "txt_Address": txt_Address,     //工程地点
        "ddType": ddType,     //行业性质
        "chk_cprjd": chk_cprjd,     //合同阶段
        "ddSourceWay": ddSourceWay,     //工程来源
        "txt_signdate_1": txt_signdate_1,     //合同签订日期1
        "txt_signdate2_1": txt_signdate2_1,     //合同签订日期2
        "txt_finishdate": txt_finishdate,     //合同完成日期1
        "txt_finishdate2": txt_finishdate,     //合同完成日期2
        "txt_signdate": txt_signdate,     //合同统计年份
        "txt_signdate2": txt_signdate2,     //合同统计年份2
        "projcharge1": projcharge1,     //合同收费1
        "projcharge2": projcharge2,     //合同收费2
        "txt_cst": txt_cst,//按照客户
        "startTime": startTime,//录入时间
        "endTime": endTime,
        "str_columnsname": colsvalue,
        "str_columnschinaname": colsname,
        "andor": andor
    };
  
    window.location.href = "ExportWord.aspx?action=search&strwhere=" + strwhere + "&drp_year=" + year + "&txtcpr_No=" + txtcpr_No + "&drp_type=" + drp_type + "&cprtype=" + cprtype + "&txt_cprname=" + txt_cprname + "&txt_Area=" + txt_Area + "&txt_Area2=" + txt_Area2 + "&txt_cprBuildUnit=" + txt_cprBuildUnit + "&asTreeviewStruct=" + asTreeviewStruct + "&asTreeviewStructType=" + asTreeviewStructType + "&drp_buildtype=" + drp_buildtype + "&txt_proFuze=" + txt_proFuze + "&txtFParty=" + txtFParty + "&drp_unit=" + drp_unit + "&txtcpr_Account=" + txtcpr_Account + "&txtcpr_Account2=" + txtcpr_Account2 + "&txt_Address=" + txt_Address + "&ddType=" + ddType + "&chk_cprjd=" + chk_cprjd + "&ddSourceWay=" + ddSourceWay + "&txt_signdate_1=" + txt_signdate_1 + "&txt_signdate2_1=" + txt_signdate2_1 + "&txt_signdate=" + txt_signdate + "&txt_signdate2=" + txt_signdate2 + "&txt_finishdate=" + txt_finishdate + "&txt_finishdate2=" + txt_finishdate2 + "&projcharge1=" + projcharge1 + "&projcharge2=" + projcharge2 + "&txt_cst=" + txt_cst + "&str_columnsname=" + colsvalue + "&str_columnschinaname=" + colsname + "&andor=" + andor + "&startTime=" + startTime + "&endTime=" + endTime + "";

};
//获取选中的部门
var getCheckedUnitNodes = function () {

    var unitlist = $("#ctl00_ContentPlaceHolder1_drp_unit_divDropdownTreeText").text();
    //temp var
    var unit = "";
    if ($.trim(unitlist) != "") {
        //选择的全部部门，部门值为空
        var allcheckedstate = $.trim($("#ctl00_ContentPlaceHolder1_drp_unit_t_l_1").attr("checkedstate"));
        if (allcheckedstate != "0") {
            //部门数组
            var unitarr = unitlist.split(',');
            //得到部门
            for (var index in unitarr) {
                if ($.trim(unitarr[index]) != "全院部门") {
                    unit += "'" + $.trim(unitarr[index]) + "',";
                }
            }
        }
    }
    return unit;
}
//清空jqgrid postdata参数
var ClearjqGridParam = function (jqgrid) {
    var postData = $("#" + jqgrid).getGridParam("postData");

    $.each(postData, function (k, v) {
        delete postData[k];
    });
}
//数据恢复还原初始
var BackCheckBoxSelectColumns = function (divcbx, divsch) {
    //选择查询字段全部隐藏
    $("div[id*='tbl_cbx']", "#selectChoose").css("display", "none");
    $("table[id*='tbl_sch']", "#selectColumnsParam").hide();

    //取消选中
    $("input[name=bb]:checkbox", "#selectChoose").attr("checked", false);
    $("input[name=bb]:checkbox", "#selectChoose").parent().removeClass("checked");

    //查询方式恢复"必须"
    $("label[rel='and']", "#CommSelectWay").addClass("yellow active");
    $("label[rel='and']", "#CommSelectWay").removeClass("default");
    $("label[rel='and']", "#CommSelectWay").siblings("label").removeClass("active yellow");
    $("label[rel='and']", "#CommSelectWay").siblings("label").addClass("default");
    $("#hid_cxtype").val("and");
    //////////////////////////////////////////查询字段选择框全部归位，与下面第一种方法共同存在 begin
    //var relid = $("#" + divcbx).attr("rel");
    //$("label", $("#" + divcbx)).each(function (i, k) {           
    //        if ($.trim($(k).text()) != "") {
    //            var $td = $("<td></td>");
    //            $td.append($(k).children());
    //            $("#" + relid).append($td);
    //        }
    //});
    ////赋值后清空
    //$("#" + divcbx).html("");
    //取消绑定事件
    //$("input[name=bb]:checkbox", $("#" + divcbx)).unbind("click");
    //////////////////////////////////////////////////////////////// end

    //点击列显示字段归位查询数据,循环td，td是字段名称
    $("#tbl_columns>tbody>tr:gt(1)>td[for]").each(function () {
        var commName = $(this).attr("for");
        //文本框td
        var $td = $(this);
        var $td2 = $(this).next();
        $("#" + divsch + " tr[for=" + commName + "]").append($td);
        $("#" + divsch + " tr[for=" + commName + "]").append($td2);
    });
    //清空   
    $("#tbl_columns>tbody>tr:gt(1)").remove();

    //清空数据
    $(":text", $("#tbl_columns>tbody>tr")).val("");
    //$("select", $("#tbl_columns>tbody>tr")).val("-1");

    if (divsch == "tbl_id2") {
        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text("");
        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text("");
    }
    else if (divsch == "tbl_id1_2") {
        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType1_divDropdownTreeText").text("");
    }
    else if (divsch == "tbl_id3_2") {
        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct3_divDropdownTreeText").text("");
        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType3_divDropdownTreeText").text("");
    }
    else if (divsch == "tbl_id9_2") {
        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct9_divDropdownTreeText").text("");
        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType9_divDropdownTreeText").text("");
    }
    else if (divsch == "tbl_id10_2") {
        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct10_divDropdownTreeText").text("");
        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType10_divDropdownTreeText").text("");
    } else if (divsch == "tbl_id11_2") {
        $("#ctl00_ContentPlaceHolder1_asTreeviewStruct11_divDropdownTreeText").text("");
        $("#ctl00_ContentPlaceHolder1_asTreeviewStructType11_divDropdownTreeText").text("");
    }
    var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;
    for (var i = 0; i < parseInt(see) ; i++) {
        $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class", "");
    }
    //勘察分院合同1
    var see1 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd1").length;
    for (var i = 0; i < parseInt(see1) ; i++) {
        $("#ctl00_ContentPlaceHolder1_chk_cprjd1_" + i).parent().attr("class", "");
    }

    //监理1
    var see3 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd3").length;

    for (var i = 0; i < parseInt(see3) ; i++) {
        $("#ctl00_ContentPlaceHolder1_chk_cprjd3_" + i).parent().attr("class", "");
    }
    //市政
    var see9 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd9").length;
    for (var i = 0; i < parseInt(see9) ; i++) {
        $("#ctl00_ContentPlaceHolder1_chk_cprjd9_" + i).parent().attr("class", "");
    }

    //规划
    var see10 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd10").length;
    for (var i = 0; i < parseInt(see10) ; i++) {
        $("#ctl00_ContentPlaceHolder1_chk_cprjd10_" + i).parent().attr("class", "");
    }

    //景观
    var see11 = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd11").length;
    for (var i = 0; i < parseInt(see11) ; i++) {
        $("#ctl00_ContentPlaceHolder1_chk_cprjd11_" + i).parent().attr("class", "");
    }
}
//初始化选择查询字段的列
var InitCheckBoxSelectColumns = function () {
    /////////////////////////////////////////////////////////////第一种方法：每次点击tbl，需重新绑定查询字段的列,暂不需要，因为click事件取消不掉 begin
    ////绑定查询字段列和点击事件
    //var n = $("#" + divcbx, "#selectChoose");
    //var tblcbxid = n.attr("rel"); //隐藏选择列字段名称的table的ID
    //var tblschid = n.attr("for");//隐藏选择列字段填写值的table的ID

    //$("#" + tblcbxid + " td").each(function (i, k) {
    //    if ($.trim($(k).text()) != "") {           
    //        var $label = $("<label></label>");
    //        $label.append($(k).children());
    //        n.append($label);
    //    }              
    //});
    ////赋值后清空td
    //$("#" + tblcbxid + " td").remove();

    //绑定事件
    //$("input[name=bb]:checkbox", $("#" + divcbx)).bind("click", function () {
    //    alert($(this).val());
    //var commName = $(this).parent().parent().siblings("span").attr("for");
    //if ($(this).is(":checked")) {
    //    var $td = $("#" + tblschid + " tr[for=" + commName + "]").children();
    //    var $tr = $("<tr></tr>");
    //    $tr = $tr.append($td);
    //    $("#tbl_columns").append($tr);
    //}
    //else {
    //    var $td = $("#tbl_columns>tbody>tr:gt(1)").children("td[for=" + commName + "]");
    //    var $td2 = $("#tbl_columns>tbody>tr:gt(1)").children("td[for=" + commName + "]").next();
    //    $("#tbl_columns td[for=" + commName + "]").parent().remove();
    //    $("#" + tblschid + " tr[for=" + commName + "]").append($td);
    //    $("#" + tblschid + " tr[for=" + commName + "]").append($td2);

    //}

    //});
    ///////////////////////////////////////////////////////////////////////////////////////////// end
    ////////////////////////////////////////////////////////////////////////////////////////////第二种方法：查询列字段全部绑定
    $("div[id*='tbl_cbx']", "#selectChoose").each(function (j, n) {
        var tblcbxid = $(n).attr("rel"); //隐藏选择列字段名称的table的ID      
        $("#" + tblcbxid + " td").each(function (i, k) {
            if ($.trim($(k).text()) != "") {
                var $label = $("<label></label>");
                $label.append($(k).children());
                $(n).append($label);
            }
        });
        $("#" + tblcbxid + " td").remove();
    });
    //绑定事件     
    CheckBoxComm("tbl_cbx");
    //CheckBoxComm("tbl_cbx3");
    //CheckBoxComm("tbl_cbx1");
    //CheckBoxComm("tbl_cbx2");
    //CheckBoxComm("tbl_cbx4");
    //CheckBoxComm("tbl_cbx7");
    //CheckBoxComm("tbl_cbx6");
    //CheckBoxComm("tbl_cbx8");
    //CheckBoxComm("tbl_cbx9");
    //CheckBoxComm("tbl_cbx10");
    //CheckBoxComm("tbl_cbx11");

    //点击查询方式
    $("label[rel]", "#CommSelectWay").click(function () {
        $(this).addClass("yellow active");
        $(this).removeClass("default");
        $(this).siblings("label").removeClass("active yellow");
        $(this).siblings("label").addClass("default");
        $("#hid_cxtype").val($(this).attr("rel"));
        var btnid = $("table[id*='tbl_sch']:visible", "#selectColumnsParam").find("input").attr("id");
        $("#" + btnid).click();
    });


}
//加载点击列表字段的全部事件
var InitColumnsClick = function () {
    //点击全部基本合同
    $(":checkbox:first", $("#columnsid")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist", columnslist, { expires: 365, path: "/" });

        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击基本合同字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist", columnslist, { expires: 365, path: "/" });

    });

    //点击全部监理合同
    $(":checkbox:first", $("#columnsid_gcjl")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_gcjl")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid3").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_gcjl")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid3").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_gcjl", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_gcjl")).click(function () {

        var checkedstate = $(this, $("#columnsid_gcjl")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_gcjl")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_gcjl")).length) == ($(":checkbox:not(:first)", $("#columnsid_gcjl")).length)) {
            $(":checkbox:first", $("#columnsid_gcjl")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_gcjl")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_gcjl")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_gcjl")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid3").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid3").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_gcjl")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_gcjl", columnslist, { expires: 365, path: "/" });

    });

    //点击全部岩土工程勘察合同
    $(":checkbox:first", $("#columnsid_gckc")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_gckc")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid1").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_gckc")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid1").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_gckc", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_gckc")).click(function () {

        var checkedstate = $(this, $("#columnsid_gckc")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_gckc")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_gckc")).length) == ($(":checkbox:not(:first)", $("#columnsid_gckc")).length)) {
            $(":checkbox:first", $("#columnsid_gckc")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_gckc")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_gckc")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_gckc")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid1").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid1").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_gckc")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_gckc", columnslist, { expires: 365, path: "/" });

    });

    //点击全部岩土工程设计合同
    $(":checkbox:first", $("#columnsid_gcsj")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_gcsj")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid2").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_gcsj")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid2").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_gcsj", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_gcsj")).click(function () {

        var checkedstate = $(this, $("#columnsid_gcsj")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_gcsj")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_gcsj")).length) == ($(":checkbox:not(:first)", $("#columnsid_gcsj")).length)) {
            $(":checkbox:first", $("#columnsid_gcsj")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_gcsj")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_gcsj")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_gcsj")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid2").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid2").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_gcsj")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_gcsj", columnslist, { expires: 365, path: "/" });

    });

    //点击全部基坑及边坡监测合同
    $(":checkbox:first", $("#columnsid_jkjc")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_jkjc")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid4").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_jkjc")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid4").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_jkjc", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_jkjc")).click(function () {

        var checkedstate = $(this, $("#columnsid_jkjc")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_jkjc")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_jkjc")).length) == ($(":checkbox:not(:first)", $("#columnsid_jkjc")).length)) {
            $(":checkbox:first", $("#columnsid_jkjc")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_jkjc")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_jkjc")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_jkjc")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid4").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid4").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_jkjc")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_jkjc", columnslist, { expires: 365, path: "/" });

    });

    //点击全部测量及沉降观测合同
    $(":checkbox:first", $("#columnsid_clgc")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_clgc")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid7").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_clgc")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid7").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_clgc", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_clgc")).click(function () {

        var checkedstate = $(this, $("#columnsid_clgc")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_clgc")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_clgc")).length) == ($(":checkbox:not(:first)", $("#columnsid_clgc")).length)) {
            $(":checkbox:first", $("#columnsid_clgc")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_clgc")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_clgc")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_clgc")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid7").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid7").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_clgc")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_clgc", columnslist, { expires: 365, path: "/" });

    });

    //点击全部岩土工程检测合同
    $(":checkbox:first", $("#columnsid_gcjc")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_gcjc")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid6").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_gcjc")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid6").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_gcjc", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_gcjc")).click(function () {

        var checkedstate = $(this, $("#columnsid_gcjc")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_gcjc")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_gcjc")).length) == ($(":checkbox:not(:first)", $("#columnsid_gcjc")).length)) {
            $(":checkbox:first", $("#columnsid_gcjc")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_gcjc")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_gcjc")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_gcjc")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid6").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid6").hideCol(columnsname);
        }
        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_gcjc")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_gcjc", columnslist, { expires: 365, path: "/" });

    });

    //点击全部岩土工程施工合同
    $(":checkbox:first", $("#columnsid_gcsg")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_gcsg")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid8").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_gcsg")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid8").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_gcsg", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_gcsg")).click(function () {

        var checkedstate = $(this, $("#columnsid_gcsg")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_gcsg")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_gcsg")).length) == ($(":checkbox:not(:first)", $("#columnsid_gcsg")).length)) {
            $(":checkbox:first", $("#columnsid_gcsg")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_gcsg")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_gcsg")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_gcsg")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid8").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid8").hideCol(columnsname);
        }
        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_gcsg")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_gcsg", columnslist, { expires: 365, path: "/" });
    });

    $(":checkbox:first", $("#columnsid_szht")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_szht")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid9").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_szht")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid9").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_szht", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid_szht")).click(function () {

        var checkedstate = $(this, $("#columnsid_szht")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_szht")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_szht")).length) == ($(":checkbox:not(:first)", $("#columnsid_szht")).length)) {
            $(":checkbox:first", $("#columnsid_szht")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_szht")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_szht")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_szht")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid9").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid9").hideCol(columnsname);
        }
        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_szht")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_szht", columnslist, { expires: 365, path: "/" });
    });

    //规划合同-全选
    $(":checkbox:first", $("#columnsid_ghht")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_ghht")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid10").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_ghht")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid10").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_ghht", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //规划合同-单选
    $(":checkbox:not(:first)", $("#columnsid_ghht")).click(function () {

        var checkedstate = $(this, $("#columnsid_ghht")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_ghht")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_ghht")).length) == ($(":checkbox:not(:first)", $("#columnsid_ghht")).length)) {
            $(":checkbox:first", $("#columnsid_ghht")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_ghht")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_ghht")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_ghht")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid10").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid10").hideCol(columnsname);
        }
        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_ghht")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_ghht", columnslist, { expires: 365, path: "/" });
    });


    //景观合同-全选
    $(":checkbox:first", $("#columnsid_jght")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_jght")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid11").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid_jght")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid11").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnsid_jght", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //景观合同-单选
    $(":checkbox:not(:first)", $("#columnsid_jght")).click(function () {

        var checkedstate = $(this, $("#columnsid_jght")).parent().attr("class");
        var columnsname = $(this, $("#columnsid_jght")).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_jght")).length) == ($(":checkbox:not(:first)", $("#columnsid_jght")).length)) {
            $(":checkbox:first", $("#columnsid_jght")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_jght")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_jght")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_jght")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid11").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid11").hideCol(columnsname);
        }
        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid_jght")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnsid_jght", columnslist, { expires: 365, path: "/" });
    });
}
//初始化显示cookies列表字段列
var InitColumnsCookies = function (cookiename, jqgrid, colid) {
    //基本合同列表
    var columnslist = $.cookie(cookiename);
   
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {
        var list = columnslist.split(",");
        // $.each(list, function (i, c) {
        $(":checkbox:not(:first)[value]", $("#" + colid)).each(function (j, n) {
            if (list.indexOf($.trim($(n).val())) > -1) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                $("#" + jqgrid).showCol($(n).val());
            }
            else {
                $("#" + jqgrid).hideCol($(n).val());
            }

        });

        //  });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#" + colid)).length)) {
            $(":checkbox:first", $("#" + colid)).parent().addClass("checked");
            $(":checkbox:first", $("#" + colid)).attr("checked", true);
        }

    }
    else {
       
        //cookie未保存，显示默认的几个字段
        //生产经营合同和监理合同是11个多了已收费字段，岩石工程检测合同是9个少建筑面积，其他都是10个字段
        var list = new Array("cpr_No", "cpr_Name", "BuildArea", "cpr_Acount", "cpr_ShijiAcount", "cpr_Unit", "tjrq", "PMUserName", "ssze");//直接定义并初始化


        $.each(list, function (i, c) {
          
                $(":checkbox:not(:first)[value='" + c + "']", $("#" + colid)).each(function (j, n) {
                    var span = $(n).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(n).attr("checked", true);
                    // $("#jqGrid").showCol($(n).val());
                });
            
        });
    }
}