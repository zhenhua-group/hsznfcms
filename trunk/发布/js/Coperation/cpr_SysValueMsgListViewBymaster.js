﻿$(function () {
    //定义变量
    var MessageType = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageType").val()); //判断消息类型
    var MessageDone = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageDone").val()); //判断是否已办
    var cpruserSysNum = escape($("#ctl00_ContentPlaceHolder1_userSysNum").val()); //系统自动登录号
    var MessageIsDoneStatus = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageIsDoneStatus").val()); //判断已办的状态
    var MessageIsRead = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageIsRead").val()); //判断是否已读
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/cpr_SysValueMsgListViewBymaster.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['消息状态', '', '', '', '消息内容', '消息类型', '发送时间', '', '发送人', '操作'],
        colModel: [
                                 { name: 'Status', index: 'Status', width: 80, formatter: colISReadFormatter },
                                 { name: 'SysNo', index: 'SysNo', hidden: true, editable: true },
                                  { name: 'FromUser', index: 'FromUser', hidden: true, editable: true },
                                 { name: 'ReferenceSysNo', index: 'ReferenceSysNo', hidden: true, editable: true },
                                 { name: 'MessageContent', index: 'MessageContent', width: 600, formatter: colHrefFormatter },
                                 { name: 'MsgType', index: 'MsgType', width: 150, formatter: colTypeFormatter },
                                  { name: 'InDate', index: 'InDate', width: 100, formatter: 'date', formatoptions: { srcformat: 'Y-m-d', newformat: 'Y-m-d' } },
                                 { name: 'InUser', index: 'InUser', hidden: true },
                                 { name: 'FromUserName', index: 'FromUserName', width: 80 },
                                 { name: 'cz', index: 'cz', width: 30, align: 'center', sorttable: false, editable: false, formatter: colHrefFormatter }

        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { 'cpruserSysNum': cpruserSysNum, 'MessageType': MessageType, 'MessageDone': MessageDone, 'MessageIsDoneStatus': MessageIsDoneStatus, 'MessageIsRead': MessageIsRead },
        loadonce: false,
        sortname: 'S.SysNo',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/cpr_SysValueMsgListViewBymaster.ashx",

        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });

    //multiselect: true,
    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refreshtext: "刷新",
        deltext: "删除"
    },
                {//添加
                },
                {//编辑
                },
                {//删除            
                },
                {//搜索
                    top: 200,
                    left: 400
                }
                );
    //查询
    $("#btn_Search").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysValueMsgListViewBymaster.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });
    //未读消息
    $("#ButtonUnread").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysValueMsgListViewBymaster.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': '', 'MessageIsDoneStatus': '', 'MessageIsRead': 'A' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });


    //已读消息 
    $("#ButtonRead").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysValueMsgListViewBymaster.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': '', 'MessageIsDoneStatus': '', 'MessageIsRead': 'D' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });
    //未办事项
    $("#ButtonDone").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysValueMsgListViewBymaster.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': '', 'MessageIsDoneStatus': 'A', 'MessageIsRead': '' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });
    //已办事项
    $("#ButtonUnDone").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysValueMsgListViewBymaster.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': 'done', 'MessageIsDoneStatus': 'D', 'MessageIsRead': '' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });

    $("a[id=redirectLink]").live("click", function () {
        var msgSysNo = $(this).attr("msgsysno");
        var result = TG.Web.Coperation.cpr_SysValueMsgListViewBymaster.UpdateMsgStatus(msgSysNo);
        if (result != null && result.value != null && result.value.length > 0) {
            if (parseInt(result.value, 10) <= 0) {
                alert("修改消息状态失败，请联系系统管理员！");
            }
            else {
                $("#jqGrid").trigger("reloadGrid");
            }

        }
    })
});



//查看合同
function colShowFormatter(celvalue, options, rowData) {
    if (celvalue != "") {
        var pageurl = "cpr_ShowCoprationBymaster.aspx?cprid=" + celvalue;
        return '<a href="' + pageurl + '" title="查看合同">查看</a>';
    }
}
//已读和未读
function colISReadFormatter(celvalue, options, rowdata) {
    if (celvalue != "") {
        if (celvalue == "A") {
            return "<img src=\"/Images/unread.gif\" align=\"center\"> 未读"
        } else {
            return "<img src=\"/Images/read.gif\" align=\"center\">已读"
        }
    }
}
//绑定链接
function colHrefFormatter(celvalue, options, rowData) {
    var MsgType = rowData["MsgType"];
    var SysNo = rowData["SysNo"];
    var ReferenceSysNo = rowData["ReferenceSysNo"];
    var FromUser = rowData["FromUser"];
    var redirectString = "";
    switch (MsgType) {

        //产值分配                              
        case 5:
            // xiugai
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            else {
                redirectString = "/ProjectValueandAllot/ProjectIsEditBymaster.aspx?ValueAllotAuditSysNo=" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            break;
        case 10:
            redirectString = "../ProjectValueandAllot/ShowUserValueDatailBymaster.aspx?" + ReferenceSysNo;
            break;
        case 14://二次产值
            // xiugai
            redirectString = "/ProjectValueandAllot/ProSecondValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 15://暖通所
            // xiugai
            redirectString = "/ProjectValueandAllot/HavcProjectValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;

        case 16:
            // 转经济所
            redirectString = "/ProjectValueandAllot/TranjjsProjectValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 17://系数确认
            redirectString = "../ProjectValueandAllot/ShowMsgValueEditBymaster.aspx?" + ReferenceSysNo;
            break;
        case 18:
            // 经济所审批
            redirectString = "/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 19:
            // 转暖通
            redirectString = "/ProjectValueandAllot/ShowTranProjectValueandAllotBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 20:
            // 转建筑
            redirectString = "/ProjectValueandAllot/TranBulidingProjectValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;

        case 25:
            //个人产值确认
            redirectString = "/ProjectValueandAllot/ProjectValueAuditMemberBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 26:
            //专业负责人再次确认
            var inUserId = rowData["InUser"];
            redirectString = "/ProjectValueandAllot/ProjectValueAuditSpeHeadBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&inUserId=" + inUserId;
            break;
        case 27:
            //经济所所长再次确认
            var inUserId = rowData["InUser"];
            redirectString = "/ProjectValueandAllot/ProjectValueAuditHeadBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&inUserId=" + inUserId;
            break;
        case 28:
            //转经济所所长再次确认
            var inUserId = rowData["InUser"];
            redirectString = "/ProjectValueandAllot/ProjectValueAuditTranHeadBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&inUserId=" + inUserId;
            break;
        case 29:
            //转暖通所所长再次确认
            var inUserId = rowData["InUser"];
            redirectString = "/ProjectValueandAllot/ProjectValueAuditTranHavcHeadBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&inUserId=" + inUserId;
            break;
        case 30:
            //转土建所所长再次确认
            var inUserId = rowData["InUser"];
            redirectString = "/ProjectValueandAllot/ProjectValueAuditTranTjsHeadBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&inUserId=" + inUserId;
            break;
        case 31:
            //三所长全部审批完毕
            redirectString = "/ProjectValueandAllot/ProjectValueAllotExportBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo ;
            break;
    }
    return '<a href="' + redirectString + '" id="redirectLink" msgsysno="' + SysNo + '" >' + celvalue + '</a>';
}
//绑定类型
function colTypeFormatter(celvalue, options, rowdata) {
    var msgTypeString = "";
    if (celvalue != "") {
        switch (celvalue) {


            case 5: msgTypeString = "土建所产值分配"; break;
            case 10: msgTypeString = "个人分配明细"; break;
            case 14: msgTypeString = "二次产值分配审核"; break;
            case 15: msgTypeString = "暖通所分配"; break;
            case 16: msgTypeString = "转经济所分配"; break;
            case 17: msgTypeString = "分配系数审核"; break;
            case 18: msgTypeString = "经济所产值分配"; break;
            case 19: msgTypeString = "转暖通所产值分配"; break;
            case 20: msgTypeString = "转土建所产值分配"; break;
            case 25: msgTypeString = "个人产值确认"; break;
            case 26: msgTypeString = "专业负责人重新分配"; break;
            case 27: msgTypeString = "经济所长重新分配"; break;
            case 28: msgTypeString = "转经济所长重新分配"; break;
            case 29: msgTypeString = "转暖通所长重新分配"; break;
            case 30: msgTypeString = "转土建所长重新分配"; break;
            case 31: msgTypeString = "三所长全部审批完毕"; break;
        }
    }
    return msgTypeString;
}
//统计 
function completeMethod() {

}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        $("#nodata").show();
    }
    else {
        $("#nodata").hide();
    }
}