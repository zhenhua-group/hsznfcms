﻿$(document).ready(function () {
    //保存合同信息
    $("#btn_save").click(function () {
        //取得文本值
        var txtcpr_No = $("#txtcpr_No").val();
        var txtcCprName = $("#txt_cprName").val();
        var txtCprUnit = $("#txt_cprBuildUnit").val();
        var drpBuildType = $("#drp_buildtype").val();
        var txtAddress = $("#txt_cprAddress").val();
        var txt_cjbm = $("#txt_cjbm").val();
        var txtRegisterDate = $("#txtRegisterDate").val();
        var txtcpr_Remark = $("#txtcpr_Remark").val();
        var txtcjbm = $("#txt_cjbm").val();
        var txtFParty = $("#txtFParty").val();
        var txtBuildArea = $("#txt_buildArea").val();
        var txtCompleteDate = $("#txtCompleteDate").val();
        var txtFParty = $("#txtFParty").val();
        var txt_jiafphone = $("#txt_jiafphone").val();
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        //用户
        if ($("#hid_cstid").val() == "") {
            msg += "请选择合同报备关联的客户信息！</br>";
        }
        //请输入报备合同名称
        if ($.trim(txtcCprName).length == 0) {
            msg += "请输入报备合同名称！</br>";
        }
        //合同分类
        if ($("#ddcpr_Type").val() == "-1") {
            msg += "请选择合同分类！</br>";
        }
        //项目建设地点
        if ($.trim(txtAddress).length == 0) {
            msg += "请输入项目建设地点！</br>";
        }
        //建设类别
        if (drpBuildType == "-1") {
            msg += "请选择建设类别！</br>";
        }
        //建设规模
        if (txtBuildArea == "") {
            msg += "请输入建设规模！</br>";
        }
        else {
            if (!reg_math.test(txtBuildArea)) {
                msg += "输入规模格式不正确！</br>";
            }
        }
        //结构形式
        if (!IsStructCheckNode('struct')) {
            msg += "请选择结构样式！</br>";
        }
        //建筑分类
        if (!IsStructCheckNode('structtype')) {
            msg += "请选择建筑分类！</br>";
        }
        //楼层数判断
        if ($("#txt_upfloor").val() != "") {
            var reg = /^[1-9]\d*$/;
            if (!reg.test($("#txt_upfloor").val())) {
                msg += "输入地上层数格式错误，请输入数字！</br>";
            }
        }
        if ($("#txt_downfloor").val() != "") {
            var reg = /^[1-9]\d*$/;
            if (!reg.test($("#txt_downfloor").val())) {
                msg += "输入地下层数格式错误，请输入数字！</br>";
            }
        }
        //承接部门
        if (txt_cjbm == "") {
            msg += "请选择承接部门！</br>";
        }
        //甲方负责人
        if (txtFParty == "") {
            msg += "请输入甲方负责人！</br>";
        }
        //甲方电话
        if (txt_jiafphone == "") {
            msg += "请输入甲方负责人电话！</br>";
        }
        //工程地点
        if ($.trim($("#txt_ProjectPosition").val()) == "") {
            msg += "请选择工程地点！</br>";
        }
        //合同金额
        if ($("#txtcpr_Account").val() == "") {
            msg += "请输入合同金额！</br>";
        }
        else {
            if (!reg_math.test($("#txtcpr_Account").val())) {
                msg += "合同金额格式不正确！</br>";
            }
        }
        if ($.trim($("#ddProfessionType").val()) == "") {
            msg += "请选择行业性质！</br>";
        }
        if ($.trim($("#ddSourceWay").val()) == "") {
            msg += "请选择工程来源！</br>";
        }
        //电话
        if ($("#txt_fzphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#txt_fzphone").val())) {
                msg += "手机号码格式输入不正确！<br/>";
            }
        }
        //电话
        if ($("#txt_jiafphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#txt_jiafphone").val())) {
                msg += "手机号码输入格式不正确！<br/>";
            }
        }

        //投资额
        if ($("#txtInvestAccount").val() != "") {
            if (!reg_math.test($("#txtInvestAccount").val())) {
                msg += "投资额请输入数字！</br>";
            }
        }
        //实际合同
        if ($("#txtcpr_Account0").val() != "") {
            if (!reg_math.test($("#txtcpr_Account0").val())) {
                msg += "实际合同额请输入数字！</br>";
            }
        }
        //实际投资
        if ($("#txtInvestAccount0").val() != "") {
            if (!reg_math.test($("#txtInvestAccount0").val())) {
                msg += "实际投资请输入数字！</br>";
            }
        }
        //是否立项
        if ($("#drp_proapproval").val() == "-1") {
            msg += "请选择是否立项！";
        }
        if (txtcpr_Remark == "") {
            $("#txtcpr_Remark").val("");
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    var chooseCustomer = new ChooseCustomer($("#chooseCustomerContainer"), chooseCustomerCallBack);
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#previewPower").val(),
        "userSysNum": $("#userSysNum").val(),
        "userUnitNum": $("#userUnitNum").val(),
        "notShowUnitList": ""
    };
    //行变色
    $(".cls_show_cst_jiben>tbody>tr:odd").attr("style", "background-color:#FFF");
    //查询按钮
    $("#btn_search").click(function () {
        chooseCustomer.ClearQueryCondition();
        $("#chooseCustomerContainer").dialog({
            autoOpen: false,
            modal: true,
            width: 740,
            resizable: false,
            title: "查询客户"
        }).dialog("open");
    });
    //合同类型
    $("#btn_cprType").click(function () {
        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "cprType", CproTypeCallBack);
        $("#chooseCustomerCompact").dialog({
            autoOpen: false,
            modal: true,
            width: 500,
            top: 100,
            resizable: false,
            title: "合同类型"
        }).dialog("open");
    });
    //工程负责人
    $("#btn_gcfz").click(function () {
        //        //var unit_ID = $("#select_gcFzr_Unit").val();
        //        //if (unit_ID < 0 || unit_ID == null) {
        //            showDivDialogClass.SetParameters({
        //                "pageSize": "10"
        //            });
        //            //绑定工程负责部门
        //            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
        //        //}
        //先设置该层上的ID
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        //如果没有选择过,则加载,选择过则不用再加载,不能手填的情况下使用
        var unit_ID = $("#select_gcFzr_Unit").val();
        if (unit_ID < 0 || unit_ID == null) {
            //绑定工程负责部门
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
        }
        $("#gcFzr_Dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 460,
            height: 380,
            resizable: false,
            title: "工程负责人"
        }).dialog("open");
    });
    //承接部门
    $("#btn_cjbm").click(function () {
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
        BindAllDataCount(); //绑定总数据

        $("#cpr_cjbmDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 340,
            resizable: false,
            title: "承接部门"
        }).dialog("open");
    });
    //甲方负责人
    $("#btn_jffz").click(function () {
        // var selectVal = $("#select_jffzrMem").val();
        //if (selectVal == null || selectVal == undefined) {
        //        showDivDialogClass.SetParameters({
        //            "pageSize": "10"
        //        });
        //        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "jffzrCop", JffzrUnitCallBack);
        //}
        //先设置该层上的ID
        showDivDialogClass.SetParameters({
            "prevPage": "jffzr_prevPage",
            "firstPage": "jffzr_firstPage",
            "nextPage": "jffzr_nextPage",
            "lastPage": "jffzr_lastPage",
            "gotoPage": "jffzr_gotoPageIndex",
            "allDataCount": "jffzr_allDataCount",
            "nowIndex": "jffzr_nowPageIndex",
            "allPageCount": "jffzr_allPageCount",
            "gotoIndex": "jffzr_pageIndex",
            "pageSize": "10"
        });
        //如果没有选择过,则加载,选择过则不用再加载,不能手填的情况下使用
        var selectVal = $("#select_jffzrMem").val();
        if (selectVal == null || selectVal == undefined) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "jffzrCop", JffzrUnitCallBack);
        }
        $("#jffzr_dialogDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 560,
            height: 400,
            resizable: false,
            title: "甲方负责人"
        }).dialog("open");
    });
    //甲方搜索
    $("#btn_SearchByCusName").click(function () {
        var custName = $("#txt_custNameSearch").val();
        if (custName != null && custName != undefined) {
            custName = showDivDialogClass.ReplaceChars(custName);
            //            showDivDialogClass.SetParameters({
            //                "pageSize": "10"
            //            });
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", custName, "true", "0", "jffzrCop", JffzrUnitCallBack);
        }
    });
    //添加子项
    $("#btn_addSub").click(function () {
        var cprAllcount = $("#txtcpr_Account").val();
        var cprbuildArea = $("#txt_buildArea").val();


        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else if (!reg.test(cprbuildArea)) {
                alert("建筑规模格式不正确！");
                return false;
            }
            else {
                var timespan = new Date();
                var url = "cpr_AddSubItem.aspx?area=" + cprbuildArea + "&money=" + cprAllcount + "&tempid=" + $("#hid_cprid").val() + "&tspan=" + timespan.getSeconds();
                ShowDialogWin_CprSubItem(url);
            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //添加附件
    $("#btn_upload").click(function () {
        var timespan = new Date();
        var url = "cpr_SmallUploadFile.aspx?cprid=" + $("#txtcpr_No").val() + "&tspan=" + timespan.getSeconds();
        ShowDialog_UploadFile(url);
    });
    //添加计划收费
    $("#btn_AddSf").click(function () {
        var cprAllcount = $("#txtcpr_Account").val();
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            }
            else {
                var url = "cpr_SmallCharge.aspx?flag=add&tempid=" + $("#hid_cprid").val() + "&allcount=" + cprAllcount + "&tspan=" + Math.random();
                ShowDialogWin_CprCharge(url);
            }
        }
        else {
            alert("请填写合同额！");
        }

    });

    $("#btn_getcprnum").click(function () {
        LoadAjaxData("cpr_Number");
        $("#cpr_Number").dialog({
            autoOpen: false,
            modal: true,
            width: 350,
            top: 100,
            resizable: false,
            title: "选择合同编号"
        }).dialog("open");
    });

    //不可编辑 
    $(".cls_input_text_onlyslt").focus(function () {
        $(this).blur();
    });
});
//返回长度
function DataLength(fdata) {
    var tlength = 0;
    for (var i = 0; i < fdata.length; i++) {
        if (fdata.charCodeAt(i) < 0 || fdata.charCodeAt(i) > 255) {
            tlength = tlength + 2;
        } else {
            tlength = tlength + 1;
        }
    }
    return tlength;
}

function chooseCustomerCallBack(recordObj) {
    $("#txtCst_No").val(recordObj.CustomerNo);
    $("#txtCst_Name").val(recordObj.CustomerName);
    $("#txtCpy_Address").val(recordObj.Address);
    $("#txtCode").val(recordObj.ZipCode == "0" ? "" : recordObj.ZipCode);
    $("#txtLinkman").val(recordObj.LinkMan);
    $("#txtCpy_Phone").val($.trim(recordObj.Phone));
    $("#txtCpy_Fax").val(recordObj.Fax);
    $("#hid_cstid").val(recordObj.SysNo);
    $("#txtCst_Brief").val(recordObj.CustomerShortName);
}
//***************************************************************************************************
//By  fbw 20130917/18 修改
//合同类型绑定表格CallBack
function CproTypeCallBack(result) {
    $("#customerCompactTable tr:gt(0)").remove();
    var data = result.ds;
    $.each(data, function (i, n) {
        var oper = "<span rel='" + n.ID + "' style=\"color:blue;cursor: pointer;\">选择</span>";
        var trHtml = "<tr><td>" + (i + 1) + "</td><td>" + n.dic_Name + "</td><td>" + oper + "</td></tr>";
        $("#customerCompactTable").append(trHtml);
        $("#customerCompactTable span:last").click(function () {
            $("#txt_cprType").val(n.dic_Name);
            $("#chooseCustomerCompact").dialog().dialog("close");
        });
    });
    ControlTableCss("customerCompactTable");
}

//承接部门表格数据绑定-CallBack函数
function ProCjbmCallBack(result) {
    var data = result.ds;
    $("#pro_cjbmTable tr:gt(0)").remove();
    $.each(data, function (i, n) {
        //        var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor: pointer;\">选择</span>";
        var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer\">选择</span>";
        var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
        $("#pro_cjbmTable").append(trHtml);
        $("#pro_cjbmTable span:last").click(function () {
            $("#txt_cjbm").val($.trim(n.unit_Name));
            $("#hid_cjbm").val($.trim(n.unit_Name));
            $("#cpr_cjbmDiv").dialog().dialog("close");
        });
    });
    ControlTableCss("pro_cjbmTable");
}
//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
        }
    });
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}


//工程负责人绑定部门CallBack
function CprTypeUnitCallBack(result) {
    //    $("#gcFzr_MemTable tr:gt(0)").remove();
    //    BindAllDataCountGcfzr(-1);
    if (result == null) {
        return false;
    }
    var data = result.ds;
    var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
    $.each(data, function (i, n) {
        gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
    });
    $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
    //注册部门选项改变事件
    //    showDivDialogClass.SetParameters({ "pageSize": "10" });
    $("#select_gcFzr_Unit").unbind('change').change(function () {
        var unit_ID = $("#select_gcFzr_Unit").val();
        if (Math.abs(unit_ID) > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
            BindAllDataCountGcfzr(unit_ID);
        }
    });
}
//绑定工程负责人表格数据CallBack
function BindGcfzrDataCallBack(result) {
    if (result == null) {
        return false;
    }
    var obj = result.ds;
    var gcFzrMemTableHtml;
    $("#gcFzr_MemTable tr:gt(0)").remove();
    $.each(obj, function (i, n) {
        var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue;cursor: pointer;\">选择</span>";
        gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + oper + "</td></tr>";
        $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
        $("#gcFzr_MemTable span:last").click(function () {
            $("#txt_proFuze").val(n.mem_Name);
            //            $("#txt_fzphone").val(n.mem_Mobile);
            $("#txt_fzphone").val(n.mem_Mobile == "" ? n.mem_Telephone : n.mem_Mobile);
            $("#gcFzr_Dialog").dialog().dialog("close");
        });
    });
    ControlTableCss("gcFzr_MemTable");
}
//绑定工程负责人数据总数
function BindAllDataCountGcfzr(unit_ID) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "gcfzr_prevPage",
        "firstPage": "gcfzr_firstPage",
        "nextPage": "gcfzr_nextPage",
        "lastPage": "gcfzr_lastPage",
        "gotoPage": "gcfzr_gotoPageIndex",
        "allDataCount": "gcfzr_allDataCount",
        "nowIndex": "gcfzr_nowPageIndex",
        "allPageCount": "gcfzr_allPageCount",
        "gotoIndex": "gcfzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
    //注册事件,先注销,再注册
    $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#gcfzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
        }
    });
}
//工程负责人数据总数CallBack
function GetGcfzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $("#gcfzr_allDataCount").text(0);
        $("#gcfzr_nowPageIndex").text(0);
        $("#gcfzr_allPageCount").text(0);
        NoDataMessageOnTable("gcFzr_MemTable", 3);
    }
}


//甲方负责人部门CallBack
function JffzrUnitCallBack(result) {

    if (result == null || result == undefined) {
        $("#select_jffzrMem").html('<option value="-1">---------------------请选择---------------------</option>');
        $("#jffzr_dataTable tr:gt(0)").remove();
        NoDataMessageOnTable("jffzr_dataTable", 6);
        return false;
    }
    var data = result.ds;
    var optionHtml = '<option value="-1">---------------------请选择---------------------</option>';
    //客户数量
    var data_count = 0;
    $.each(data, function (i, n) {
        optionHtml += '<option value="' + n.Cst_Id + '">' + n.Cst_Name + '</option>';
        data_count++;
    });
    $("#select_jffzrMem").html(optionHtml);
    $("#span_count").text(data_count + "项结果");
    //注册部门选项改变事件
    showDivDialogClass.SetParameters({ "pageSize": "10" });
    $("#select_jffzrMem").unbind('change').change(function () {
        var Cst_Id = $("#select_jffzrMem").val();
        if (Math.abs(Cst_Id) > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", "1", "jffzrMem", BindJffzrDataCallBack);
            BindAllDataCountJffzr(Cst_Id);
        }
    });
}
//绑定表格数据CallBack
function BindJffzrDataCallBack(result) {
    if (result == null) {
        return false;
    }
    var obj = result.ds;
    var jffzrTableHtml;
    $("#jffzr_dataTable tr:gt(0)").remove();
    $.each(obj, function (i, n) {
        var oper = "<span rel='" + n.Cst_Id + "' style=\"color:blue;cursor: pointer;\">选择</span>";
        jffzrTableHtml = "<tr style='text-align:center'><td>" + n.Cst_Id + "</td><td>"
        + n.Name + "</td><td>" + n.Duties + "</td><td>" + n.Department + "</td><td>" + n.Phone + "</td><td>" + oper + "</td></tr>";
        $("#jffzr_dataTable").append(jffzrTableHtml);
        $("#jffzr_dataTable span:last").click(function () {
            $("#txtFParty").val($.trim(n.Name));
            $("#txt_jiafphone").val($.trim(n.Phone));
            $("#jffzr_dialogDiv").dialog().dialog("close");
        });
    });
    ControlTableCss("jffzr_dataTable");
}
//甲方负责人数据总数
function BindAllDataCountJffzr(Cst_Id) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "jffzr_prevPage",
        "firstPage": "jffzr_firstPage",
        "nextPage": "jffzr_nextPage",
        "lastPage": "jffzr_lastPage",
        "gotoPage": "jffzr_gotoPageIndex",
        "allDataCount": "jffzr_allDataCount",
        "nowIndex": "jffzr_nowPageIndex",
        "allPageCount": "jffzr_allPageCount",
        "gotoIndex": "jffzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", Cst_Id, "true", "jffzrMem", GetJffzrAllDataCount);
    //注册事件,先注销,再注册
    $("#jffzrByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", pageIndex, "jffzrMem", BindJffzrDataCallBack);
        }
    });
}
//获取甲方负责人总数CallBack
function GetJffzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#jffzr_dataTable tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_dataTable", 6);
    }
}

//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}