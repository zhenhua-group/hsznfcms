﻿//选择用户UserControl JS类
function SelectRoleUser(parent, container) {
    this.Dom = {};
    this.Container = container;
    this.Parent = parent;
    this.BackgroundInvoke = TG.Web.Coperation.SelectUserControl;
    var Instance = this;

    //输入名字文本框
    Instance.Dom.RoleUserShortNameTextBox = $("#UserShortName", container);

    //选择部门下拉框
    Instance.Dom.DepartmentDropdownList = $("#UserDepartment", container);

    //默认选中部门ID
    Instance.Dom.DefaultSelectedDepartmentSysNo = $("#defaultDepartmentSysNo", container).val();

    //查询按钮
    Instance.Dom.SearchButton = $("#btnSearch", container);

    //页面内容清除事件
    Instance.Clear = function () {
        $(":checkbox", container).attr("checked", false);
        $(":text", container).val("");
    }

    Instance.Dom.SearchButton.click(function () {
        //取得文本框内容
        var userShortName = Instance.Dom.RoleUserShortNameTextBox.val();
        //取得部门选择框的内容
        var departmentSysNo = Instance.Dom.DepartmentDropdownList.val()

        var result = Instance.BackgroundInvoke.GetUserList(userShortName, departmentSysNo);

        if (result != null && result != undefined && result.value != null && result.value.length > 0 && result.value != undefined) {
            var userList = Global.evalJSON(result.value);
            $("#divDataTable").html("");
            var html = "<tr>";
            $.each(userList, function (index, item) {
                if (index % 6 == 0 && index != 0) {
                    html += "</tr><tr>";
                }
                html += "<td><input type=\"checkbox\" name=\"roleUsers\" username=\"" + item.mem_Name + "\" value=\"" + item.mem_ID + "\"  /><span>" + item.mem_Name + "<span></td>";
            });
            html += "</tr>";
            $("#divDataTable").html(html);
        }
    });

    //初始化设定加载项事件
    Instance.Init = function () {
        $("select option[value=" + Instance.Dom.DefaultSelectedDepartmentSysNo + "]", container).attr("selected", true);
        Instance.Dom.SearchButton.click();
    }

    //点击确定按钮处理事件，可在实例化后重写此方法
    this.SelectedUserDone = function (callback, img) {
        var userCheckboxs = $(":checkbox:checked", Instance.Container);       
        callback(userCheckboxs, img);
    }
}