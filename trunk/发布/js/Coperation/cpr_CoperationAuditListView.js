﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var startAppControl; //发起审批对象
var PmId;//区分基本合同和监理公司
//合同审核列表
$(function () {
    CommonControl.SetFormWidth();
    //提示
    //    $(".cls_column").tipsy({ opacity: 1 });
    //    //隔行变色
    //    $("#gv_Coperation tr:even").css({ background: "White" });

    //设置文本框样式
    CommonControl.SetTextBoxStyle();
    //改变背景颜色
    ChangedBackgroundColor();
    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();


    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('0', '1', jsonDataEntity);
        }
    });
    //发起审核按钮
    $("span[id=InitiatedAudit]").live("click", function () {
        //        //获取合同SysNo
        //        var coperationSysNo = $(this).attr("coperationSysNo");
        //        var objData =
        //		{
        //		    CoperationSysNo: coperationSysNo,
        //		    InUser: userSysNo
        //		};
        //        var jsonData = Global.toJSON(objData);
        //        jsonDataEntity = jsonData;
        //        startAppControl = $(this);
        //        getUserAndUpdateAudit('0', '0', jsonDataEntity);
        //实例化类容
        messageDialog = $("#auditShow").messageDialog;
        sendMessageClass = new MessageCommon(messageDialog);
        //获取合同SysNo
        var coperationSysNo = $(this).attr("coperationSysNo");
        PmId = $(this).attr("action");
        var objData =
		{
		    CoperationSysNo: coperationSysNo,
		    InUser: userSysNo
		};
        var jsonData = Global.toJSON(objData);
        jsonDataEntity = jsonData;
        startAppControl = $(this);
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });
    //绑定审批进度
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });

    //预览按钮
    $("span[id=PreviewAudit]").live("click", function () {
        //获取合同系统自增号
        var coperationSysNo = $(this).attr("coperationSysNo");
        window.location.href = "/Coperation/cpr_ShowCoprationBymaster.aspx?cprid=" + coperationSysNo;
    });

    //审核按钮
    $("span[id=GoAudit]").live("click", function () {
        //获取合同系统自增号
        var coperationSysNo = $(this).attr("coperationSysNo");
        //获取合同审核记录自增号
        var coperationAdutiSysNo = $(this).attr("coperationAuditSysNo");

        if ($.trim($(this).attr("action")) == "super") {
            window.location.href = "/Coperation/cpr_SuperCoperationAuditBymaster.aspx?CoperationAuditSysNo=" + coperationAdutiSysNo + "&CoperationSysNo=" + coperationSysNo;
        }
        else {
            window.location.href = "/Coperation/cpr_CoperationAuditBymaster.aspx?CoperationAuditSysNo=" + coperationAdutiSysNo + "&CoperationSysNo=" + coperationSysNo;
        }

    });

    /*
    //实例化类容
    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                getUserAndUpdateAudit('0', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                messageDialog.hide();
    //            }
    //        }

    //    });
    //    sendMessageClass = new MessageCommon(messageDialog);
    */

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Corperation";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();

});

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //默认基本合同地址
    var url = "/HttpHandler/CoperationAuditHandler.ashx";
    //监理公司
    if (PmId == "super") {
        url = "/HttpHandler/SuperCoperationAuditHandler.ashx";
    }

    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        //发起审批返回数据
        if (jsonResult == "0") {
            alert("流程评审错误，请联系管理员！");
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        //提示消息
        alert("发起合同评审已成功！\n消息已成功发送到评审人等待审批！");
        //改变流程提示状态
        CommonControl.SetApplyStatus(startAppControl);
    } else {
        alert("消息发送失败！");
    }
}
