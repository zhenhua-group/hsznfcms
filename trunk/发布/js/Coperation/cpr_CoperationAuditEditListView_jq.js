﻿$(function () {
        //点击全部
        $(":checkbox:first", $("#columnsid")).click(function () {

            //获得显示的列，保存到cookies
            var columnslist = "";
            var checkedstate = $(this).parent().attr("class");
            if (checkedstate != "checked")//代表选中
            {
                $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                    var span = $(value).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(value).attr("checked", true);
                    $("#jqGrid").showCol($(value).val());
                    //保存cookies值
                    columnslist = columnslist + $(value).val() + ",";
                });
                columnslist = columnslist.substr(0, (columnslist.length - 1));
            }
            else {
                $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                    var span = $(value).parent();
                    span.removeClass("checked");
                    $(value).attr("checked", false);
                    $("#jqGrid").hideCol($(value).val());
                });
                columnslist = "";
            }
            //保存到cookies
            $.cookie("columnslist_copeditaudit", columnslist, { expires: 365, path: "/" });
            // $("#jqGrid").trigger('reloadGrid');
        });

        //点击客户字段
        $(":checkbox:not(:first)", $("#columnsid")).click(function () {
            var checkedstate = $(this).parent().attr("class");
            var columnsname = $(this).val();

            if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
                $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
                $(":checkbox:first", $("#columnsid")).attr("checked", true);
            }
            else {
                $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
                $(":checkbox:first", $("#columnsid")).attr("checked", false);
            }

            if (checkedstate != "checked")//代表选中
            {
                $("#jqGrid").showCol(columnsname);
            }
            else {
                $(this).attr("checked", false);
                $("#jqGrid").hideCol(columnsname);
            }

            //获得显示的列，保存到cookies
            var columnslist = "";
            $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
                columnslist = columnslist + $(this).val() + ",";
            });
            if (columnslist != "") {
                columnslist = columnslist.substr(0, (columnslist.length - 1));
            }
            //保存到cookies
            $.cookie("columnslist_copeditaudit", columnslist, { expires: 365, path: "/" });

        });

    //点击全部监理合同
        $(":checkbox:first", $("#columnsid_gcjl")).click(function () {

            //获得显示的列，保存到cookies
            var columnslist = "";
            var checkedstate = $(this).parent().attr("class");
            if (checkedstate != "checked")//代表选中
            {
                $(":checkbox:not(:first)", $("#columnsid_gcjl")).each(function (i, value) {
                    var span = $(value).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(value).attr("checked", true);
                    $("#jqGrid3").showCol($(value).val());
                    //保存cookies值
                    columnslist = columnslist + $(value).val() + ",";
                });
                columnslist = columnslist.substr(0, (columnslist.length - 1));
            }
            else {
                $(":checkbox:not(:first)", $("#columnsid_gcjl")).each(function (i, value) {
                    var span = $(value).parent();
                    span.removeClass("checked");
                    $(value).attr("checked", false);
                    $("#jqGrid3").hideCol($(value).val());
                });
                columnslist = "";
            }
            //保存到cookies
            $.cookie("columnslist_gcjlcopeditaudit", columnslist, { expires: 365, path: "/" });
            // $("#jqGrid").trigger('reloadGrid');
        });

    //点击客户字段
        $(":checkbox:not(:first)", $("#columnsid_gcjl")).click(function () {

            var checkedstate = $(this, $("#columnsid_gcjl")).parent().attr("class");
            var columnsname = $(this, $("#columnsid_gcjl")).val();

            if (($(":checkbox:not(:first):checked", $("#columnsid_gcjl")).length) == ($(":checkbox:not(:first)", $("#columnsid_gcjl")).length)) {
                $(":checkbox:first", $("#columnsid_gcjl")).parent().addClass("checked");
                $(":checkbox:first", $("#columnsid_gcjl")).attr("checked", true);
            }
            else {
                $(":checkbox:first", $("#columnsid_gcjl")).parent().removeClass("checked");
                $(":checkbox:first", $("#columnsid_gcjl")).attr("checked", false);
            }

            if (checkedstate != "checked")//代表选中
            {
                $("#jqGrid3").showCol(columnsname);
            }
            else {
                $(this).attr("checked", false);
                $("#jqGrid3").hideCol(columnsname);
            }

            //获得显示的列，保存到cookies
            var columnslist = "";
            $(":checkbox:not(:first):checked", $("#columnsid_gcjl")).each(function () {
                columnslist = columnslist + $(this).val() + ",";
            });
            if (columnslist != "") {
                columnslist = columnslist.substr(0, (columnslist.length - 1));
            }
            //保存到cookies
            $.cookie("columnslist_gcjlcopeditaudit", columnslist, { expires: 365, path: "/" });

        });

     

    //发起审核按钮
        $("span[id=InitiatedAudit]").live("click", function () {
            //获取合同SysNo
            var coperationSysNo = $(this).attr("coperationSysNo");
            if ($.trim($(this).attr("action")) == "super") {
                window.location.href = "/Coperation/cpr_SuperCoperationEditChooseBymaster.aspx?coperationSysNo=" + coperationSysNo;
            }
            else
            {
                window.location.href = "/Coperation/cpr_CoperationEditChooseBymaster.aspx?coperationSysNo=" + coperationSysNo;
            }
            
        });
    //预览按钮
        $("span[id=PreviewAudit]").live("click", function () {
            //获取合同系统自增号
            var coperationSysNo = $(this).attr("coperationSysNo");
            window.location.href = "/Coperation/cpr_CoperationPreviewBymaster.aspx?coperationSysNo=" + coperationSysNo;
        });

    });

var coperation = function () {
    var tempRandom = Math.random();
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/CoperationListHandler.ashx?n=' + tempRandom + ' ',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '', '合同编号', '合同名称', '甲方', '工程地点', '承接部门', '签订日期', '完成日期', '统计年份', '合同分类', '合同类型', '建筑类别', '建设单位', '建筑规模(㎡)', '结构形式', '建筑分类', '层数', '工程负责人', '工程负责人电话', '甲方负责人电话', '合同额(万元)', '实际合同额(万元)', '投资额(万元)', '实际投资额(万元)', '行业性质', '工程来源', '合同阶段', '制表人', '多栋楼', '合同备注', '录入人', '录入时间', '评审状态', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'AuditSysNo', index: 'AuditSysNo', hidden: true, editable: true },
                             { name: 'statusE', index: 'statusE', hidden: true, editable: true },
                             { name: 'ManageLevel', index: 'ManageLevel', hidden: true, editable: true },
                             { name: 'NeedLegalAdviser', index: 'NeedLegalAdviser', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 100, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 300, formatter: colNameShowFormatter },
                             { name: 'ChgJia', index: 'ChgJia', width: 60, align: 'center' },
                             { name: 'BuildPosition', index: 'BuildPosition', width: 80, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 100, align: 'center' },
                             { name: 'qdrq', index: 'cpr_SignDate2', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'cpr_DoneDate', width: 80, align: 'center' },
                                { name: 'tjrq', index: 'cpr_SignDate', width: 80, align: 'center' },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 80, align: 'center', hidden: true },
                              { name: 'cpr_Type2', index: 'cpr_Type2', width: 80, align: 'center', hidden: true },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center', hidden: true },
                             { name: 'BuildUnit', index: 'BuildUnit', width: 80, align: 'center', hidden: true },
                             { name: 'BuildArea', index: 'BuildArea', width: 80, align: 'center', hidden: true },
                              { name: 'StructType', index: 'StructType', width: 80, align: 'center', hidden: true },
                             { name: 'BuildStructType', index: 'BuildStructType', width: 80, align: 'center', hidden: true },
                             {
                                 name: 'Floor', index: 'Floor', width: 80, align: 'center', hidden: true, formatter: function colShowName(celvalue, options, rowData) {
                                     if (celvalue != null && celvalue != "null" && celvalue != "") {
                                         var arr = $.trim(celvalue).split("|");
                                         return "地上:" + $.trim(arr[0]) + " 地下:" + $.trim(arr[1]);
                                     }
                                     return "";
                                 }
                             },
                              { name: 'ChgPeople', index: 'ChgPeople', width: 80, align: 'center', hidden: true },
                             { name: 'ChgPhone', index: 'ChgPhone', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJiaPhone', index: 'ChgJiaPhone', width: 80, align: 'center', hidden: true },
                              { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Touzi', index: 'cpr_Touzi', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiTouzi', index: 'cpr_ShijiTouzi', width: 80, align: 'center', hidden: true },
                             { name: 'Industry', index: 'Industry', width: 80, align: 'center', hidden: true },
                             { name: 'BuildSrc', index: 'BuildSrc', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_Process', index: 'cpr_Process', width: 100, align: 'center', hidden: true, formatter: colProcess },
                             { name: 'TableMaker', index: 'TableMaker', width: 80, align: 'center', hidden: true },
                             { name: 'MultiBuild', index: 'MultiBuild', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Mark', index: 'cpr_Mark', width: 80, align: 'center', hidden: true },
                             { name: 'InsertUser', index: 'InsertUser', width: 100, align: 'center', hidden: true },
                              { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 150, align: 'center', formatter: colEditFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 80, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "AuditEdit", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/CoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });

    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/CoperationListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //合同阶段
    function colProcess(celvalue, options, rowData) {
        var temp = "";
        var arr = $.trim(celvalue).split(",");
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == "27") {
                temp = "方案,";
            }
            if (arr[i] == "28") {
                temp += "初设,";
            }
            if (arr[i] == "29") {
                temp += "施工图,";
            }
            if (arr[i] == "30") {
                temp += "其他,";
            }
        }
        return temp;
    }


    //名称连接
    function colNameShowFormatter(celvalue, options, rowData) {
        var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
        return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

    }
    //查看
    function colShowFormatter(celvalue, options, rowData) {
        var actionLinkString = "";
        switch (rowData["statusE"]) {
            case "":
            case " ":
            case null:
            case "C":
            case "E":
            case "G":
            case "K":
            case "I":
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" coperationSysNo=\"" + celvalue + "\">修改申请</span>";
                break;
            case "A":
            case "B":
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" coperationSysNo=\"" + celvalue + "\">查看</span>";
                break;
            case "D":
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" coperationSysNo=\"" + celvalue + "\" coperationAuditSysNo=\"" + rowData["AuditSysNo"] + "\">修改申请</span>";
                break;
        }
        return actionLinkString;

    }
    //评审进度
    function colEditFormatter(celvalue, options, rowdata) {
        var length = 2, perc = 0;
        if (rowdata["AuditSysNo"] != null && rowdata["AuditSysNo"] != "") {
            switch (rowdata["statusE"]) {
                case "A":
                    perc = 0;
                    break;
                case "B":
                case "C":
                    perc = (100 / length) * 1;
                    break;
                case "D":
                case "E":
                    perc = (100 / length) * 2;
                    break;
                case "F":
                case "G":
                    perc = (100 / length) * 3;
                    break;
                case "H":
                case "I":
                    if (rowdata["ManageLevel"] == "1") {
                        perc = 100;
                    }
                    else {
                        if (rowdata["NeedLegalAdviser"] == "0") {
                            perc = (100 / length) * 3;
                        }
                        else {
                            perc = (100 / length) * 4;
                        }
                    }
                    break;
                case "J":
                case "K":
                    perc = 100;
                    break;
                default:
                    perc = 0;
                    break;
            }
        }
        perc = parseFloat(perc).toFixed(2);
        var pageurl = '<div class="progressbar" percent="' + perc + '" id="auditLocusContainer" title="' + perc + '%" action="EC" referencesysno="' + celvalue + '" style="cursor: pointer;height:20px; margin: 1px 1px 1px 1px;"></div>'
        return pageurl;
    }
    //统计 
    function completeMethod() {
        var rowIds = $("#jqGrid").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
        }

        //进度条
        $.each($(".progressbar"), function (index, item) {
            var percent = parseFloat($(item).attr("percent"));
            $(item).progressbar({
                value: percent
            });
        });


    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid").getGridParam("records"));
        $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
        if (rowcount <= 0) {
            if ($("#nodata").text() == '') {
                $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
            }
            else { $("#nodata").show(); }
        }
        else {
            $("#nodata").hide();
        }
    }
    //导出
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {

        //显示列信息
        var colsName = "";
        var colsValue = "";
        //循环选中的字段列
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            colsName = colsName + $.trim($(this).parent().parent().parent().text()) + ",";
            colsValue = colsValue + $(this).val() + ",";
        });
        if (colsName != "" && colsValue != "") {
            colsName = colsName.substr(0, (colsName.length - 1));
            colsValue = colsValue.substr(0, (colsValue.length - 1));
            $("#ctl00_ContentPlaceHolder1_hid_cols").val(colsName);
            $("#ctl00_ContentPlaceHolder1_hid_colsvalue").val(colsValue);
        }
       
    });
}

//初始化显示cookies字段列
var InitColumnsCookies = function (cookiename, jqgrid, colid) {
    //基本合同列表
    var columnslist = $.cookie(cookiename);
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {
        var list = columnslist.split(",");
      //  $.each(list, function (i, c) {
            $(":checkbox:not(:first)[value]", $("#" + colid)).each(function (j, n) {
                if (list.indexOf($.trim($(n).val())) > -1) {
                    var span = $(n).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(n).attr("checked", true);
                    $("#" + jqgrid).showCol($(n).val());
                }
                else {
                    $("#" + jqgrid).hideCol($(n).val());
                }

            });

      //  });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#" + colid)).length)) {
            $(":checkbox:first", $("#" + colid)).parent().addClass("checked");
            $(":checkbox:first", $("#" + colid)).attr("checked", true);
        }

    }
    else {
        //cookie未保存，显示默认的几个字段
        var list = new Array("cpr_No", "cpr_Name", "ChgJia", "BuildPosition", "cpr_Unit", "qdrq", "wcrq","tjrq", "lrsj");//直接定义并初始化

        $.each(list, function (i, c) {

            $(":checkbox:not(:first)[value='" + c + "']", $("#" + colid)).each(function (j, n) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                // $("#jqGrid").showCol($(n).val());
            });

        });
    }
}