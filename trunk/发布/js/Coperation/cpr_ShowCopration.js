﻿//加载合同分项和附件信息
$(document).ready(function () {
    CommonControl.SetFormWidth();

    //加载分项信息
    var data = "action=selectsubcpr&cprid=" + $("#hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {

            if (result != null) {
                var rlt_data = result.ds;
                if ($("#datas tr").length > 1) {
                    $("#datas tr:gt(0)").empty();
                }
                $.each(rlt_data, function (i, n) {
                    var row = $("#sub_row").clone();

                    row.find("#sub_id").text(i + 1);
                    row.find("#sub_name").text(n.Item_Name);
                    row.find("#sub_area").text(n.Item_Area);

                    row.find("#Money").text(n.Money);
                    row.find("#Remark").text(n.Remark);
                    //添加样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas");
                });
                rlt_data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("加载分项信息失败！");
        }
    });
    //表格变色
    $(".cls_show_cst_jiben>tbody>tr:odd").attr("style", "background-color:#FFF");
    //加载附件信息
    var data_att = "action=selectattach&cprid=" + $("#hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").empty();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + escape(n.FileName) + "&FileURL=" + escape(n.FileUrl) + "\" target='_blank'>下载</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").attr("align", "left").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    //添加样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("加载附件信息错误！");
        }
    });
    //加载计划
    var data = "action=getplancharge&cstno=" + $("#hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if ($("#sf_datas tr").length > 1) {
                    $("#sf_datas tr:gt(0)").remove();
                }
                $.each(data, function (i, n) {
                    var row = $("#sf_row").clone();

                    row.find("#sf_id").text(n.Times);
                    row.find("#sf_bfb").text(n.persent + "%");
                    row.find("#sf_edu").text(n.payCount);
                    row.find("#sf_time").text(n.paytime);
                    row.find("#sf_mark").text(n.mark);

                    //添加样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#sf_datas");
                });
                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });

    //导出
    var cprname = $("#hid_cprid").val();
    $("#ag").attr("href", "../HttpHandler/CustemerToWord.ashx?flag=cstshow&cprname=" + cprname + "");

});