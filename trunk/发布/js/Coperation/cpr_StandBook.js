﻿$(document).ready(function () {
    //单位
    var op = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    //年
    var yearall = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    if (yearall != "-1") {
        $("#title").html(yearall + '年');
    } else {
        $("#title").html('全部年份');
    }
    if (op == "-1") {
        $("#unit").html("");
    }
    else {
        $("#unit").html(op);
    }
    //统计
    $("#btn_ok").click(function () {
        //判断
        if ($("#ctl00_ContentPlaceHolder1_drp_unit").get(0).selectedIndex == 0) {
            alert('请选择要统计的生产部门！');
            return false;
        }
        $("#AddTable tr").remove();
        //单位
        var op = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        //年
        var yearall = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var cprName = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        if (yearall != "-1") {
            $("#title").html(yearall + '年');
        } else {
            $("#title").html('全部年份');
        }
        if (op == "-1") {
            $("#unit").html("");
        }
        else {
            $("#unit").html(op);
        }
        var userShortName = $("#ctl00_ContentPlaceHolder1_userShortName").val();
        var previewPower = $("#ctl00_ContentPlaceHolder1_previewPower").val();
        var userSysNum = $("#ctl00_ContentPlaceHolder1_userSysNum").val();
        var userUnitNum = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
        $.get("../HttpHandler/HandlerContrast.ashx", { "action": "5", "year": yearall, "unitid": op, "previewPower": previewPower, "userUnitNum": userUnitNum, "cprname": cprName }, function (data) {
            if (data == "[]") {
                var $tr = $("<tr></tr>");
                var $td1 = $("<td colspan='13' style='color:red;'>没有数据！</td>");
                $tr.append($td1);
                $("#AddTable").append($tr);
            }
            var json = eval('(' + data + ')');
            var v = 1;
            $.each(json, function (key, valObj) {

                var $tr = $("<tr></tr>");
                var $td1 = $("<td style=\"width: 3%\">" + v + "</td>");
                var $td2 = $("<td style=\"width:9%\" title=\"" + valObj.CprNo + "\">" + valObj.CprNo + "</td>");
                var $td3 = $("<td style=\"width: 21%;text-align:left;\"><div class=\"cls_Column_Short\" title=\"" + valObj.CprName + "\" style=\"width:16em;\">" + valObj.CprName + "</div></td>");
                var $td5 = $("<td style=\"width: 6%\">" + valObj.BuildArea + "</td>");
                var $td6 = $("<td style=\"width: 9%\">" + valObj.CprAcount + "</td>");
                var $td7 = $("<td style=\"width: 9%\">" + valObj.Acount + "</td>");
                var $td10 = $("<td style=\"width: 9%\">" + valObj.Owe + "</td>");
                //var $td19 = $("<input type='hidden' class='hiddenDetail' value=\"" + Getstring(valObj.Chargedetail) + "\">");
                //var $td11 = $("<td style=\"width:14%;\"><a href='###' class='showTD' style=\"color:blue;\">查看Ｖ</a></td>");
                var $td19 = $("<input type='hidden' class='hiddenDetail' value=\"" + Getstring(valObj.Chargedetail) + "\">");
                var $td11 = $("<td style=\"width:16%;\"> " + Getstring(valObj.Chargedetail) + "</td>");
                var $td12 = $("<td style=\"width: 6%\">" + Getstring(valObj.ProStage) + "</td>");
                var $td13 = $("<td style=\"width: 6%\">" + Getstring(valObj.CopStage) + "</td>");
                var $td14 = $("<td style=\"width: 6%\">" + Getstring(valObj.Mark) + "</td>");
                //Getstring(valObj.Chargedetail)
                $tr.append($td1);
                $tr.append($td2);
                $tr.append($td3);
                $tr.append($td5);
                $tr.append($td6);
                $tr.append($td7);
                $tr.append($td10);
                $tr.append($td11);
                $tr.append($td12);
                $tr.append($td13);
                $tr.append($td19);
                $tr.append($td14);
                $("#AddTable").append($tr);
                v++;
            });
        });

    });
    $(".showTD").live("click", function () {
        var str = $(this).parent().parent().find('input').val();
        var hascharge = $(this).parent().parent().find('td').eq(5).text();
        if (hascharge == "0") {
            alert("没有收费！");
        }
        else {
            $(this).parent().html(str + "<a href='###' class='hideTD' style=\"color:blue; float:right \" >收起Λ</a>")
        }
        $(".hideTD").live("click", function () {
            $(this).parent().html("<a href='###' class='showTD' style=\"color:blue;\">查看Ｖ</a>");
        });
    });
    //提示
    $(".cls_Column_Short").tipsy({ opacity: 1, live: true });

});


function Getstring(item) {
    if (item == "" || item == null) {
        return "";
    } else {
        return item;
    }
}