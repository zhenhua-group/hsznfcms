﻿
//create by long 20130605

$(document).ready(function () {
    //统计
    $("#btn_ok").click(function () {

        //清除上一次统计的数据
        $("#mytab tr:gt(1)").remove();
        //统计
        var op = $("#drp_unit").val();
        var year = $("#drp_year").val();
        var month = $("#drp_month").val();

        $.get("../HttpHandler/GetTable.ashx", { "action": "1", "year": year, "unitid": op}, function (data) {
            var json = eval('(' + data + ')');
            $.each(json, function (key, valObj) {
                //单位SConpany 合同目标值SMubiaozhi 面积 Smianji 已签合同数目Syiqianhet 合同额 SHeTongE 已完成合同额SyiwanchengHeTongE 合同欠费SqianfeiHetong 完成目标值 SwanchengMubiaoZhi 完成图纸张数 SwanchengTuzhi 以前年度欠款 SbeforeQianKuan 本年度收费 SNowShouFei 尚欠 SOweMoney 备注 Smark
                $.each(valObj, function (k, val) {
                    var $tr = $("<tr></tr>");
                    var $td1 = $("<td>" + val["SConpany"] + "</td>");
                    var $td2 = $("<td>" + val["SMubiaozhi"] + "</td>");
                    var $td3 = $("<td>" + val["Syiqianhet"] + "</td>");
                    var $td4 = $("<td>" + Getstring(val["Smianji"]) + "</td>");
                    var $td5 = $("<td>" + Getstring(val["SHeTongE"]) + "</td>");
                    var $td6 = $("<td>" + Getstring(val["SyiwanchengHeTongE"]) + "</td>");
                    var $td7 = $("<td>" + Getstring(val["SqianfeiHetong"]) + "</td>");
                    var $td8 = $("<td>" + Getstring(val["SwanchengMubiaoZhi"]) + "</td>");
                    var $td9 = $("<td>" + Getstring(val["SwanchengTuzhi"]) + "</td>");
                    var $td10 = $("<td>" + val["SbeforeQianKuan"] + "</td>");
                    var $td11 = $("<td>" + Getstring(val["SNowShouFei"]) + "</td>");
                    var $td12 = $("<td>" + Getstring(val["SOweMoney"]) + "</td>");
                    var $td13 = $("<td>" + val["Smark"] + "</td>");
                    $tr.append($td1);
                    $tr.append($td2);
                    $tr.append($td3);
                    $tr.append($td4);
                    $tr.append($td5);
                    $tr.append($td6);
                    $tr.append($td7);
                    $tr.append($td8);
                    $tr.append($td9);
                    $tr.append($td10);
                    $tr.append($td11);
                    $tr.append($td12);
                    $tr.append($td13);
                    $("#mytab").append($tr);
                })
            });
        });
    });
});

function Getstring(item) {
    if (item == 0.00) {
        return 0;
    } else {
        return item;
    }
}