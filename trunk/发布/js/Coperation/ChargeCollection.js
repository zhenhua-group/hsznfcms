﻿var TableAdvanced = function () {

    var initTable1 = function () {

        /* Formatting function for row details */
        //function fnFormatDetails ( oTable, nTr )
        //{
        //    var aData = oTable.fnGetData( nTr );
        //    var sOut = '<table>';
        //    sOut += '<tr><td>Platform(s):</td><td>'+aData[2]+'</td></tr>';
        //    sOut += '<tr><td>Engine version:</td><td>'+aData[3]+'</td></tr>';
        //    sOut += '<tr><td>CSS grade:</td><td>'+aData[4]+'</td></tr>';
        //    sOut += '<tr><td>Others:</td><td>Could provide a link here</td></tr>';
        //    sOut += '</table>';

        //    return sOut;
        //}

        /*
         * Insert a 'details' column to the table
         */
        //var nCloneTh = document.createElement( 'th' );
        //var nCloneTd = document.createElement( 'td' );
        //nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';

        //$('#tablecontent thead tr').each(function () {
        //    this.insertBefore( nCloneTh, this.childNodes[0] );
        //} );

        //$('#tablecontent tbody tr').each(function () {
        //    this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
        //} );

        /*
         * Initialize DataTables, with no sorting on the 'details' column
         */
        var oTable = $('#tablecontent').dataTable({
            "bFilter": false,
            "bLengthChange": false,
            "iDisplayLength":20,
            "oLanguage": {
                "sProcessing": "正在加载中......",
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "对不起，查询不到相关数据！",
                "sEmptyTable": "表中无数据存在！",
                "sInfo": "当前显示 _START_ 到 _END_ 条，共 _TOTAL_ 条记录",
                "sInfoFiltered": "数据表中共为 _MAX_ 条记录",
                "sSearch": "搜索",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "上一页",
                    "sNext": "下一页",
                    "sLast": "末页"
                }
            }
        });

        //jQuery('#tablecontent .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        //jQuery('#tablecontent .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
        //jQuery('#tablecontent .dataTables_length select').select2(); // initialize select2 dropdown

        /* Add event listener for opening and closing details
         * Note that the indicator for showing which row is open is not controlled by DataTables,
         * rather it is done here
         */
        //$('#tablecontent').on('click', ' tbody td .row-details', function () {
        //    var nTr = $(this).parents('tr')[0];
        //    if ( oTable.fnIsOpen(nTr) )
        //    {
        //        /* This row is already open - close it */
        //        $(this).addClass("row-details-close").removeClass("row-details-open");
        //        oTable.fnClose( nTr );
        //    }
        //    else
        //    {
        //        /* Open this row */                
        //        $(this).addClass("row-details-open").removeClass("row-details-close");
        //        oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
        //    }
        //});
    }

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();