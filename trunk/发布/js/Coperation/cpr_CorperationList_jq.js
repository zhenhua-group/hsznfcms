﻿//数字验证正则    
var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

var coperation = function () {

    var tempRandom = Math.random() + new Date().getMilliseconds();

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/CoperationListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '', '', '', '', '', '合同编号', '合同文本编号', '工程名称', '合同分类', '建筑规模(㎡)', '合同额(万元)', '实际合同额(万元)', '子公司', '签约日期', '工程负责人', '建设单位', '甲方类型', '结构形式', '建筑分类', '工程负责人电话', '甲方负责人', '甲方负责人电话', '工程地点', '合同备注', '已收费(万元)', '客户名称', '录入人', '录入时间', '', '', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'sumarea', index: 'sumarea', hidden: true, editable: true },
                             { name: 'sumaccount', index: 'sumaccount', hidden: true, editable: true },
                             { name: 'sumsjaccount', index: 'sumsjaccount', hidden: true, editable: true },
                             { name: 'sumysaccount', index: 'sumysaccount', hidden: true, editable: true },
                             { name: 'cpr_SignDate', index: 'cpr_SignDate', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 80, align: 'center' },
                             { name: 'cpr_Type2', index: 'cpr_Type2', width: 80, align: 'center', hidden: true, editable: true },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 200, formatter: colNameShowFormatter },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 80, align: 'center' },
                             { name: 'BuildArea', index: 'BuildArea', width: 80, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                              { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', width: 100, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 70, align: 'center' },
                             { name: 'tjrq', index: 'cpr_SignDate', width: 80, align: 'center' },
                             {
                                 name: 'PMUserName', index: 'ChgPeople', width: 60, align: 'center', formatter: function colShowName(celvalue, options, rowData) {
                                     return $.trim(celvalue);
                                 }
                             },
                             { name: 'BuildUnit', index: 'BuildUnit', width: 80, align: 'center', hidden: true },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center', hidden: true, editable: true },
                             { name: 'StructType', index: 'StructType', width: 80, align: 'center', hidden: true },
                             { name: 'BuildStructType', index: 'BuildStructType', width: 80, align: 'center', hidden: true },
                             { name: 'ChgPhone', index: 'ChgPhone', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJiaPhone', index: 'ChgJiaPhone', width: 80, align: 'center', hidden: true },
                             { name: 'BuildPosition', index: 'BuildPosition', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_Mark', index: 'cpr_Mark', width: 80, align: 'center', hidden: true },
                              { name: 'ssze', index: 'ssze', width: 80, align: 'center' },
                              { name: 'cstName', index: 'cstName', width: 80, hidden: true, align: 'center' },
                             { name: 'InsertUser', index: 'InsertUserID', width: 100, align: 'center', hidden: true },
                             { name: 'lrsj', index: 'InsertDate', width: 80, align: 'center' },
                             { name: 'subcprname', index: 'subcprname', hidden: true },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 40, align: 'center', sorttable: false, editable: false, formatter: colMoneyFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/CoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'cpr_Id') + ",";
                            }
                        }
                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        // var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );



    ////查询
    //$("#btn_basesearch").click(function () {
    //    ClearjqGridParam("jqGrid");
    //    //隐藏高级查询       
    //    //$("#div_cbx").hide();
    //    //$("#div_sch").hide();


    //    if ($("#ctl00_ContentPlaceHolder1_txt_account1").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account1").val())) {
    //        $("#ctl00_ContentPlaceHolder1_txt_account1").val("");
    //    }
    //    if ($("#ctl00_ContentPlaceHolder1_txt_account2").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account2").val())) {
    //        $("#ctl00_ContentPlaceHolder1_txt_account2").val("");
    //    }

    //    var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
    //    // var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    //    var unit = getCheckedUnitNodes();
    //    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //    var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
    //    var startTime = $("#ctl00_ContentPlaceHolder1_txt_startdate").val();
    //    var endTime = $("#ctl00_ContentPlaceHolder1_txt_enddate").val();
    //    var cprtype = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());
    //    var buildunit = $("#ctl00_ContentPlaceHolder1_txt_cprbuildunit").val();
    //    var account1 = $("#ctl00_ContentPlaceHolder1_txt_account1").val();
    //    var account2 = $("#ctl00_ContentPlaceHolder1_txt_account2").val();

    //    $(".norecords").hide();
    //    $("#jqGrid").jqGrid('setGridParam', {
    //        url: "/HttpHandler/Coperation/CoperationListHandler.ashx?n='" + tempRandom + " '&action=sel",
    //        postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime, "cprtype": cprtype, "buildunit": buildunit, "account1": account1, "account2": account2 },
    //        page: 1,
    //        sortname: 'cpr_Id',
    //        sortorder: 'desc'
    //    }).trigger("reloadGrid");
    //});


    //导出
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {

        //显示列信息
        var colsName = "";
        var colsValue = "";
        //循环选中的字段列
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            colsName = colsName + $.trim($(this).parent().parent().parent().text()) + ",";
            colsValue = colsValue + $(this).val() + ",";
        });
        if (colsName != "" && colsValue != "") {
            colsName = colsName.substr(0, (colsName.length - 1));
            colsValue = colsValue.substr(0, (colsValue.length - 1));
            $("#ctl00_ContentPlaceHolder1_hid_cols").val(colsName);
            $("#ctl00_ContentPlaceHolder1_hid_colsvalue").val(colsValue);
        }

        SearchExport();
        return false;

    });
    //选择生产部门
    //$("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
    //    var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
    //    var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    //    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //    var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
    //    var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    //    var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
    //    $(".norecords").hide();
    //    $("#jqGrid").jqGrid('setGridParam', {
    //        url: "/HttpHandler/Coperation/CoperationListHandler.ashx?action=sel",
    //        postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
    //        page: 1,
    //        sortname: 'cpr_Id',
    //        sortorder: 'desc'

    //    }).trigger("reloadGrid");
    //});
    //导出excel

    //
    //名称连接
    function colNameShowFormatter(celvalue, options, rowData) {
        var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
        return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

    }

 

    //查看
    function colShowFormatter(celvalue, options, rowData) {
        var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + celvalue;
        return '<a href="' + pageurl + '" alt="查看合同">查看</a>';

    }
    //编辑
    function colEditFormatter(celvalue, options, rowdata) {
       // if (celvalue != "") {
            var pageurl = "cpr_ModifyCoperationBymaster.aspx?cprid=" + celvalue;
            //if ($.trim(rowdata["cpr_Type"]) != "项目协议") {
            //    if (rowdata["AuditSysNo"] != null && $.trim(rowdata["AuditSysNo"]) != "") {
            //        if ($.trim(rowdata["AuditStatus"]) != "C" && $.trim(rowdata["AuditStatus"]) != "E" && $.trim(rowdata["AuditStatus"]) != "G" && (($.trim(rowdata["AuditStatus"]) != "K" && $.trim(rowdata["ManageLevel"]) == "0") || ($.trim(rowdata["AuditStatus"]) != "I" && $.trim(rowdata["ManageLevel"]) == "1"))) {
            //            pageurl = "javascript:alert('该合同已提交审批，不能被修改！');"
            //        }
            //    }
            //}
            return '<a href="' + pageurl + '" alt="编辑合同" class="allowEdit">编辑</a>';
       // }
    }
    //合同收费
    function colMoneyFormatter(celvalue, options, rowdata) {
        var url = "";
        if (celvalue != "") {
            url = '<a href="#addCharge" rel="' + celvalue + '" class="cls_add" data-cprname=\"' + rowdata["cpr_Name"] + '\" data-cpracount=\"' + rowdata["cpr_Acount"] + '\" data-cprpmname=\"' + rowdata["PMUserName"] + '\" data-subcprname=\"' + rowdata["subcprname"] + '\" data-toggle=\"modal\"  alt="添加收费" class="allowEdit">收费</a>';
        }
        return url;
    }
    //统计 
    function completeMethod() {

        var sumarea = 0, sumaccount = 0, sumsjaccount = 0, sumtzaccount = 0, sumsjtzaccount = 0, sumyfaccount = 0;
        var rowIds = $("#jqGrid").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(1).text((i + 1));
            if (i == 0) {
                sumarea = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(3).text());
                sumaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(4).text());
                sumsjaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(5).text());               
                sumyfaccount = $.trim($("#jqGrid tr[id=" + rowIds[i] + "]").children("td").eq(6).text());
            }
            //$("#" + rowIds[i]).find("td").eq(1).text((i + 1));
        }
        $("#jqGrid").footerData('set', { cpr_No: "合计:", BuildArea: sumarea, cpr_Acount: sumaccount, cpr_ShijiAcount: sumsjaccount, ssze: sumyfaccount });
      
    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata").text() == '') {
                $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
            }
            else { $("#nodata").show(); }

        }
        else {
            $("#nodata").hide();
        }
    }

}


