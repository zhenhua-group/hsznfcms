﻿//合同审核配置页
$(function(){
	//角色选择下拉框String
	var roleDropDownList = $("#roleDropDownList");
	//删除按钮显示
	$("span[id=userSpan]",$("#sortTable")).live("mouseover",function(){
		$(this).find("img:first").show();
	}).live("mouseout",function(){
		$(this).find("img:first").hide();
	});
	
	//删除按钮点击事件
	$("img[name=deleteUserImgActionBtn]").live("click",function(){
		var img = $(this);
		if($(this).parents("li:first").children("span").length >1){
			if(confirm("确认删除此用户？")){
				//取得其他的所有Span下的UserSysNo
				var imgs = $(this).parent("span:first").siblings("span").children("img");
				//循环取得Img中的UserSysNo，拼接字符串
				var userSysNoString = "";
				$.each(imgs,function(index,item){
					var userSysNo = $(item).attr("userSysNo");
					userSysNoString += userSysNo+",";
				});				
				userSysNoString = userSysNoString.substring(0,userSysNoString.length -1);
				//取得角色SysNo
				var roleSysNo = $(this).parents("span:first").attr("rolesysno");
				
				//发送请求。删除该用户角色		
				$.post("/HttpHandler/CoperationAuditConfigHandlerFolder/CoperationAuditConfigCommonHandler.ashx",{"Action":"3","RoleSysNo":roleSysNo,"Users":userSysNoString},function(jsonResult){
					if(parseInt(jsonResult) < 0){
						alert("修改角色对应用户失败，请联系系统管理员！");
					}else{
						img.parent("span:first").remove();
					}
				});		
			}	
		}
	});
	
	//工作流程双击事件
//	$("li[id=workflowliAction]").dblclick(function(){
//		CreateTextBoxAndFill(this)
//	});
	
	//选择角色的双击事件
	$("li[id=roleSysNoliAction]").dblclick(function(){
		if($("select[id=roleDropDownList]:visible").length <1){
			//取得当前的SysNo
			var roleSysNo = $(this).children("select:first").attr("roleSysNo");
			//删除Span
			$(this).find("span:first").hide();
			//显示选择Select
			$(this).find("select").show().val(roleSysNo);
		}
	});
	
	var selectRoleUser = new SelectRoleUser(null,$("#selectUserMain"));
        
        //选择用户完成事件
        ProcessSelectedUserDone = function(userCheckboxs,img){
		//取得RoleSysNo
		var roleSysNo = img.parent("li:first").siblings("li[userli=true]").children("span[id=userSpan]:first").attr("rolesysno");
		//获取Img同行的UserSpanLi
		var userLiImg = img.parent("li:first").siblings("li[userli=true]").children("span[id=userSpan]").children("img");
		
		$.each(userCheckboxs,function(index,item){
			var flag = true;
			//判断是否有重复的人员
			$.each(userLiImg,function(index1,item1){
				if($(item1).attr("usersysno") == $(item).val()){
					flag = false;
					return false;
				}
			});
			if(flag == true){
				var spanString = "<span id=\"userSpan\" usersysno=\""+$(item).val()+"\" style=\"margin-right:5px;\">"+$(item).attr("username")+"<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer; display: none;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
				img.parent("li:first").siblings("li[userli=true]").append(spanString);
			}
		});
		//保存数据到数据库
		//取得所有的用户Span
		var allUserSpan = img.parent("li:first").siblings("li[userli=true]").children("span[id=userSpan]");
		//取得角色SysNo
		var roleSysNo = $(allUserSpan[0]).attr("rolesysno");
		var userSysNoString ="";
		$.each(allUserSpan.children("img"),function(index,item){
			userSysNoString +=$(item).parent("span[id=userSpan]:first").attr("userSysNo")+",";
		});
		userSysNoString = userSysNoString.substring(0,userSysNoString.length -1);
		//发送请求
		$.post("/HttpHandler/CoperationAuditConfigHandlerFolder/CoperationAuditConfigCommonHandler.ashx",{"Action":"3","RoleSysNo":roleSysNo,"Users":userSysNoString},function(jsonResult){
			if(parseInt(jsonResult) < 0){
				alert("修改角色对应用户失败，请联系系统管理员！");
			}
		});								
        }
        
	//添加用户按钮点击事件
	$("img[id=addRoleUserBtn]").click(function(){
		var img = $(this);
		
		selectRoleUser.Clear();
		
		$("#DivAddRoleUser").dialog({
			autoOpen: false,
			modal: true,
			width:550,
			height:400,
			resizable :false,
			buttons:
			{
				"确定":function(){
					//调用处理事件
					selectRoleUser.SelectedUserDone(ProcessSelectedUserDone,img);
					$(this).dialog("close");
				},
				"取消" : function(){$(this).dialog("close");}
			}
		}).dialog("open");
		
		selectRoleUser.Init();
	});
});


//拼接用户Span字符串
function JoinUserSpans (result){
	var userList = Global.evalJSON(result.value);
	var userSpansString ="";
	$.each(userList,function(index,user){
		userSpansString+="<span id=\"userSpan\" usersysno=\""+user.mem_ID+"\" style=\"margin-right: 5px;\">" +user.mem_Name;
		userSpansString+="<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer; display: none; margin-left: 5px;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
	});
	return userSpansString;
}

//取得内容并且填充到文本框
function CreateTextBoxAndFill(container){
	var textString = $.trim($(container).text());
	$(container).html("").append("<input type=\"text\" onblur=\"TextBoxOnblurEvent(this,'1')\" value=\""+textString+"\" class=\"inputTextBox\" />");
	
}
//保存文本框内容到容器内
function TextBoxOnblurEvent(textBox,flag){
	var value = $(textBox).val();
	var parentContainer = $(textBox).parent("li:first");
	$(textBox).remove();
	parentContainer.text(value);
	
	switch(flag)
	{
		//表示是工作流程文本框
		case"1":
			UpdateWorkflow(parentContainer);
			break;
		case"2":
			//是角色名文本框
			break;
	}
}

//修改工作流名称
function UpdateWorkflow(li){
	//取得工作流程SysNo
	var sysNo = li.attr("coperationauditconfigSysNo");
	//取得工作流字符
	var workflow = $.trim(li.text());
	$.post("/HttpHandler/CoperationAuditConfigHandlerFolder/CoperationAuditConfigCommonHandler.ashx",{"Action":"1","SysNo":sysNo,"Workflow":workflow},function(jsonResult){
		if(parseInt(jsonResult,10) < 0){
			alert("修改工作流失败，请联系系统管理员！");
		}
	});
}

//选择角色下拉框失去焦点绑定事件
function RoleDropDownListBlur(select){
	//取得当前未改变之前的RoleSysNo
	var instanceRoleSysNo = $(select).attr("RoleSysNo");
	//获取当前选中后的RoleSysNo
	var changedRoleSysNo = $(select).val();
	
	//获取当前对应改变后的select对象
	var otherSelect = $("select[rolesysno="+changedRoleSysNo+"]",$("#sortTable"));
	
	//修改本身的RoleSysNo属性和互斥的Select的RoleSysNo
	$(select).attr("rolesysno",changedRoleSysNo).hide().siblings("span[id=roleNameSpan]").text($(select).children("option[value="+changedRoleSysNo+"]").text()).show();
	otherSelect.attr("rolesysno",instanceRoleSysNo).siblings("span[id=roleNameSpan]").text(otherSelect.children("option[value="+instanceRoleSysNo+"]").text());
	
	//改变角色所对应的用户
	//取得当前Select对应的UserSpan
	var instanceUserSpan = JoinUserSpans(TG.Web.Coperation.CoperationAuditConfig.GetRoleUsersList(instanceRoleSysNo));
	//取得另外改变的UserSpan
	var otherUserSpan = JoinUserSpans(TG.Web.Coperation.CoperationAuditConfig.GetRoleUsersList(changedRoleSysNo));
	
	$(select).parent("li:first").siblings("li[userli=true]").html(otherUserSpan);
	otherSelect.parent("li:first").siblings("li[userli=true]").html(instanceUserSpan);
	
	//发送请求修改工作流程与角色的对应关系。由于这里的限制，每次只能修改两条记录
	//当前选择的和原来的没有发生改变的场合。不发送修改请求
	if(parseInt(instanceRoleSysNo,10) != parseInt(changedRoleSysNo,10)){
		var dataArr = new Array();
		//取得改变的两条记录的ConfigSysNo和RoleSysNo，封装数据
		dataArr[0] =
		{
			AuditConfigSysNo : $(select).attr("coperationauditconfigsysno"),
			RoleSysNo : $(select).attr("RoleSysNo")
		};
		if(otherSelect.length > 0){
			dataArr[1] = {
				AuditConfigSysNo : otherSelect.attr("coperationauditconfigsysno"),
				RoleSysNo : otherSelect.attr("RoleSysNo")
			};
		}
		var jsonData = Global.toJSON(dataArr);
		$.post("/HttpHandler/CoperationAuditConfigHandlerFolder/CoperationAuditConfigCommonHandler.ashx",{"Action":"2","data":jsonData},function(jsonResult){
			if(parseInt(jsonResult,10) < 0){
				alert("对应角色修改失败，请联系系统管理员！");
			}
		});
	}
}

