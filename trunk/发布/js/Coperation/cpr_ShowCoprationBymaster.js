﻿//加载合同分项和附件信息
$(document).ready(function () {

    //加载子分项信息
    var data = "action=selectsubcpr&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var rlt_data = result.ds;
                if (rlt_data != null) {
                    if ($("#datas tr").length > 1) {
                        $("#datas tr:gt(0)").empty();
                    }
                    //总建筑面积
                    var totalArea = 0;
                    var totalMoney = 0;
                 
                    $.each(rlt_data, function (i, n) {
                        var row = $("#sub_row").clone();
                        row.find("#sub_id").text(i + 1);
                        row.find("#sub_name").text(n.Item_Name);
                       
                        row.find("#sub_area").text(n.Item_Area);
                        row.find("#Money").text(n.Money);
                      
                        row.find("#sub_moneystatus").html(n.MoneyStatus == null ? "" : n.MoneyStatus);
                        row.find("#Remark").text(n.Remark);

                        //totalArea = totalArea + parseFloat(n.Item_Area);
                        //totalMoney = totalMoney + parseFloat(n.Money);
                     
                        row.addClass("cls_TableRow");
                        row.appendTo("#datas");
                    });
                }

                //            //添加合计
                //            var totalTr = $("<tr></tr>");
                //            var totalTd0 = $("<td></td>");
                //            var totalTd1 = $("<td style=\"font-weight: bold;\" align=\"center\">合计</td>");
                //            var totalTd2 = $("<td></td><td></td>");
                //            var totalTd3 = $("<td style=\"font-weight: bold;\" align=\"center\">" + totalArea.toFixed(2) + "</td>");
                //            var totalTd4 = $("<td style=\"font-weight: bold;\" align=\"center\">" + totalMoney + "</td>");
                //            var totalTd5 = $("<td style=\"font-weight: bold;\" align=\"center\">" + totalLiving_Area + "</td>");
                //            var totalTd6 = $("<td></td><td></td>");
                //            totalTr.append(totalTd0);
                //            totalTr.append(totalTd1);
                //            totalTr.append(totalTd2);
                //            totalTr.append(totalTd3);
                //            totalTr.append(totalTd4);
                //            totalTr.append(totalTd5);
                //            totalTr.append(totalTd6);
                //            $("#datas").append(totalTr);

                rlt_data = "";
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("加载分项信息失败！");
        }
    });

    //表格变色
    $(".table table-striped table-bordered table-hover>tr:odd").attr("style", "background-color:#FFF");

    //加载附件信息
    var data_att = "action=selectattach&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").empty();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + escape(n.FileName) + "&FileURL=" + escape(n.FileUrl) + "\" target='_blank'>下载</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").attr("align", "left").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);

                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("加载附件信息错误！");
        }
    });

    //加载计划
    var data = "action=getplancharge&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if ($("#sf_datas tr").length > 1) {
                    $("#sf_datas tr:gt(0)").remove();
                }
                $.each(data, function (i, n) {
                    var row = $("#sf_row").clone();

                    row.find("#sf_id").text(i + 1);
                    row.find("#sf_bfb").text(n.persent + "%");
                    row.find("#sf_edu").text(n.payCount);
                    //                    row.find("#sf_type").text(n.payType == null ? "" : n.payType); //新增,收费类型
                    row.find("#sf_time").html(n.paytime);
                    var markShow = n.mark;
                    if (n.mark.length > 20) {
                        markShow = markShow.substr(0, 20) + "...";
                    }
                    row.find("#sf_mark").attr("title", n.mark);
                    row.find("#sf_mark").html(markShow); //新增,备注

                    row.addClass("cls_TableRow"); //样式
                    row.appendTo("#sf_datas");

                });
                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });

    //导出
    var cprname = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $("#btn_output").attr("href", "../HttpHandler/CustemerToWord.ashx?flag=cstshow&cprname=" + cprname + "");

    //合同收费信息 
        var cprid = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();       
        var allcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").text();
        var data = "action=getChargeDataList&proSysNo=" + cprid;
        var tableID="chargeDataTableSee";
        $("#"+tableID+" tr:gt(0)").remove();
        $.ajax({
            type: "Get",
            dataType: "json",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            success: function (result) {
                if (result != null && result != NaN) {
                    var dataObj = result.ds;
                    var sumcount = 0, sumnumber = 0;
                    $.each(dataObj, function (i, n) {
                        var trHtml = "";
                        var presnumber=Math.abs((n.Acount / Math.abs(allcount)) * 100).toFixed(2)
                        var pres = presnumber + "%";
                        var chargeStatu = "";
                        var status = $.trim(n.Status);
                        var dateShort = GetShortDate(n.InAcountTime);
                        var remarkSub = n.ProcessMark;
                        //if (remarkSub.length > 10) {
                        //    remarkSub = remarkSub.substr(0, 10) + "...";
                        //}
                        var fromUser = n.FromUser;
                        //if (fromUser.length > 10) {
                        //    fromUser = fromUser.substr(0, 10) + "...";
                        //}
                        if (status == "A") {
                            chargeStatu = "财务待确认";
                        } else if (status == "C") {
                            chargeStatu = "所长待确认";
                        } else if (status == "B") {
                            chargeStatu = "财务不通过";
                        } else if (status == "E") {
                            chargeStatu = "完成入账";
                        }
                        else if (status == "D") {
                            chargeStatu = "所长不通过";
                        }
                        trHtml = '<tr><td style="background-color:#ffffff;">' + (i + 1) + '</td><td style="background-color:#ffffff;">' + n.Acount + '</td><td style="background-color:#ffffff;">' + pres + '</td><td style="background-color:#ffffff;" title="' + n.FromUser + '">' + fromUser + '</td><td style="background-color:#ffffff;">' + n.mem_Name + '</td><td style="background-color:#ffffff;">' + dateShort + '</td><td style="background-color:#ffffff;">' + chargeStatu + '</td>';
                      
                        trHtml += '<td style="background-color:#ffffff;" title="' + n.ProcessMark + '">' + remarkSub + '</td>';
                       
                        trHtml += '</tr>';
                        $("#" + tableID + "").append(trHtml);

                        sumcount = sumcount + parseFloat(n.Acount);
                        sumnumber = sumnumber + parseFloat(presnumber);
                    });
                    var hj = "<tr><td style='font-weight:bold;'>合计:</td><td>" + sumcount.toFixed(4) + "</td><td>" + sumnumber.toFixed(2) + "%</td><td></td><td></td><td></td><td></td><td></td></tr>"
                    $("#" + tableID + "").append(hj);

                } else {
                    NoDataMessageOnTable(tableID, 8);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误!");
            }
        });

});
//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td style='background-color:#ffffff;' colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//转换为短日期格式
function GetShortDate(dateString) {
    if (dateString != "" && dateString != null) {
        var index = 10;
        if (dateString.lastIndexOf("-") > 0) {
            index = dateString.lastIndexOf("-") + 3;
        } else if (dateString.lastIndexOf("/") > 0) {
            index = dateString.lastIndexOf("/") + 3;
        }
        return dateString.substr(0, index);
    } else {
        return "无";
    }
}