﻿$(document).ready(function () {
    $("#gv_Coperation tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //关闭
    $("#btn_close").click(function () {
        if (window.opener != undefined) {
            //for  chrom
            window.opener.returnValue = $("#hid_isinput").val();
        }
        window.returnValue = $("#hid_isinput").val();
        window.close();
    });

    //入账
    $(".cls_inputcount").click(function () {
        $("#td_input").show();
        //计划收款总额
        var sumOfAccount = parseFloat($(this).siblings(":hidden[id$=HiddenSumOfAccount]").val());
        //实际收款总额
        var partialAcceptance = $(this).siblings(":hidden[id$=HiddenPartialAcceptance]").val().length == 0 ? 0 : parseFloat($(this).siblings(":hidden[id$=HiddenPartialAcceptance]").val());

        $("#txt_partialAcceptance").val(sumOfAccount - partialAcceptance);

        $("#txt_inputtimes").text($(this).parent().parent().find("td:first").text());
        $("#hid_plantime").val($(this).parent().parent().find("td:eq(3)").text());
        $("#hid_chgid").val($(this).next(":hidden").val());
    });
    //收款
    $("#btn_addcount").click(function () {
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var paycount = $("#txt_payCount").val();
        if (parseFloat($("#txt_partialAcceptance").val()) == 0) {
            alert("已完成入账，无需入账！");
            return false;
        }
        if (!reg.test(paycount)) {
            $("#txt_paycount_valide").text("请输入数字！").show();
            return false;
        } else if (parseFloat(paycount) > parseFloat($("#txt_partialAcceptance").val())) {
            $("#txt_paycount_valide").text("请确认支付金额不超过未支付金额！").show();
            return false;
        }
        else {
            $("#txt_paycount_valide").hide();
        }
        if ($("#txt_times").val() == "") {
            $("#txt_time_valide").show();
            return false;
        }
        else {
            $("#txt_time_valide").hide();
        }
        if ($("#txt_user").val() == "") {
            $("#txt_user_valide").show();
            return false;
        }
        else {
            $("#txt_user_valide").hide();
        }
        //汇款人
        if ($("#txt_remitter").val().length == 0) {
            $("#txt_remitter_valide").show();
            return false;
        } else {
            $("#txt_remitter_valide").hide();
        }
        //入账单据号
        if ($("#txt_BillNo").val().length == 0) {
            $("#txt_BillNo_valide").show();
        } else {
            $("#txt_BillNo_valide").hide();
        }

        //添加收款信息
        var chgid = $("#hid_chgid").val();
        var chgcount = $("#txt_payCount").val();
        var time = $("#txt_times").val();
        var plantime = $("#hid_plantime").val();
        var user = $("#txt_user").val();
        var mark = $("#txt_mark").val();
        var billNo = $("#txt_BillNo").val();
        var remitter = $("#txt_remitter").val();
        var url = "../HttpHandler/CoperationChargeHandler.ashx";
        var data = { "action": "add", "id": "" + chgid + "", "chgcount": "" + chgcount + "", "time": "" + time + "", "user": "" + user + "", "mark": "" + mark + "", "ptime": "" + plantime + "", "billNo": billNo, "remitter": remitter };

        $.post(url, data, function (rlt) {
            if (rlt == "ok") {
                alert("入账成功！");
                $("#td_input").hide();
                var url = document.location.href;
                if (url == "") {
                    url = "AddCoperationCharge.aspx?cprid=" + $("#hid_cprid").val() + "&math=" + Math.random();
                }
                else {
                    url = url.replace(/###/, '') + "&math=" + Math.random();
                }
                $("#reload").attr("href", url);
                $("#reload").get(0).click();
            }
            else {
                alert("入账失败！");
            }
        });
    });
});