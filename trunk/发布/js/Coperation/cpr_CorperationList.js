﻿$(document).ready(function () {
    //宽度
    CommonControl.SetFormWidth();
    //隔行变色
    $("#gv_Coperation tr:even").css({ background: "White" });
    //全选
    $("#chk_All").click(function() {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });

    //单选
    $(".chk_id").each(function(i) {
        $(this).click(function() {
            if ($(this).get(0).checked) {
                $(this).attr("checked", "true");
            }
            else {
                $(this).attr("checked", "");
            }
        });
    });
    //删除判断
    $("#btn_DelCpr").click(function() {
        if ($(":checked", $("#gv_Coperation")).length == 0) {
            alert("请选择要删除的记录！");
            return false;
        }
        return confirm("确认要删除选中的记录吗！");
    });

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Corperation";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});