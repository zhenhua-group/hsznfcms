﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
//获取消息列表参数
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;
$(function () {

    $(":radio[name=BuildNature][value=" + $("#ctl00_ContentPlaceHolder1_HiddenBuildNature").val() + "]").attr("checked", "checked");
    $(":radio[name=BuildType][value=" + $("#ctl00_ContentPlaceHolder1_HiddenCoperationType").val() + "]").attr("checked", "checked");
    $("input", $("#CoperationBaseInfo")).attr("disabled", true);
    // 设置文本框样式
    CommonControl.SetTextBoxStyle();
    CommonControl.SetFormWidth();

    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();

    //当前合同审核记录
    var auditRecordStatus = $("#AuditRecordStatus").val();
    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();
    

    //审核通过按钮
    $("#AgreeButton").click(function () {
        messageDialog = $("#auditShow").messageDialog;
        sendMessageClass = new MessageCommon(messageDialog);
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            CoperationSysNo: $("#HiddenCoperationSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Suggestion: $("#suggestionTextArea").val(),
            Status: auditRecordStatus
        };
        if (Vilidation(auditObj) == false) {
            jAlert("请填写意见！", "提示");
            return false;
        }

        //按钮变为不可用
        $(this).attr("disabled", "disabled");

        var jsonObj = Global.toJSON(auditObj);
        jsonDataEntity = jsonObj;
        //合同最后完成阶段
        if (auditRecordStatus == "B") {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
            return false;
        }      
        else {
            getUserAndUpdateAudit('1', '0', jsonDataEntity);
        }
    });

    //审核不通过按钮
    $("#DisAgreeButton").click(function () {
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            CoperationSysNo: $("#HiddenCoperationSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Suggestion: $("#suggestionTextArea").val(),
            Status: auditRecordStatus
        };
        if (Vilidation(auditObj) == false) {
            jAlert("请填写意见！", "提示");
            return false;
        }

        //按钮变为不可用
        $(this).attr("disabled", "disabled");

        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/SuperCoperationAuditHandler.ashx", { "Action": 2, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("评审失败！");
            } else {
                if (jsonResult == "1") {

                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("评审不通过成功，消息已发送给申请人！");
                }

                window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });
    });

    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
        }
    });
    //实例化类容
    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程评审人！");
    //                    return false;
    //                }

    //                getUserAndUpdateAudit('1', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    //    sendMessageClass = new MessageCommon(messageDialog);


    $("#btnReport").click(function () {
        var data = "coperationSysNo=" + $.trim($("#HiddenCoperationSysNo").val()) + "&level=" + $.trim($("#HiddenManageLevel").val()) + "&auditNo=" + $.trim($("#HiddenAuditRecordSysNo").val()) + "&option=0";
        window.location.href = "ExportWord.aspx?" + data;
    });

});

//验证
function Vilidation(auditObj) {
    var flag = true;
    if (auditObj.Suggestion == null || auditObj.Suggestion == undefined || auditObj.Suggestion.length == 0) {
        flag = false;
    }
    return flag;
}

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/SuperCoperationAuditHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    Global.SendRequest(url, data, null, null, function (jsonResult) {
        //改变消息状态
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }

        if (jsonResult == "0") {
            alert("流程评审错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("监理合同评审流程完成，已全部通过！");
            //查询系统新消息
            // window.history.back();
            window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
        $("#AuditUserList").modal();
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        alert("监理合同评审已成功！\n消息已成功发送到评审人等待审批！");

        //查询系统新消息
        //window.history.back();

        window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
    } else {
        alert("消息发送失败！");
    }
}