﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var isEditApp;
var projectNameRepeat = false;//判断合同名称是否重复
$(document).ready(function () {

    //建设规模单位显示  不是市政工程监理合同就只显示平方米
    //if ($.trim($("#ctl00_ContentPlaceHolder1_txt_cprType").text()) != "市政工程监理合同") {

    //    $("#ctl00_ContentPlaceHolder1_drp_AreaUnit").find("option[value=k㎡]").remove();
    //}
    //合同类型
    $("#ctl00_ContentPlaceHolder1_txt_cprTypeEdit").change(function () {
        var $tr = $("#ctl00_ContentPlaceHolder1_drp_AreaUnit").parent().parent().parent();
        //判断建筑面积是否显示
        if ($tr.css("display") != "none") {
            //判断建筑面积
            if ($.trim($(this).val()) != "市政工程监理合同" && $.trim($(this).val()) != "-1") {
                $("#ctl00_ContentPlaceHolder1_drp_AreaUnit option[value=k㎡]").remove();
            }
            else {
                if ($("#ctl00_ContentPlaceHolder1_drp_AreaUnit option").length < 2) {
                    $("#ctl00_ContentPlaceHolder1_drp_AreaUnit").append("<option value='k㎡'>k㎡</option>");
                }
            }
        }

    });
    //加载收费计划func
    loadCprCharge();
    $("#EditDetail tr:even").css({ background: "White" });
    $("#EditDetail tbody>tr").hide();
    //    alert($("#EditDetail").children().find("tr").length)
    //监理合同ID
    // $("#hid_cprid").val(hid_cprid);
    $("#EditDetail").children().find("tr").eq(36).show();
    var chargeid = "0";
    var subid = "0";
    $("#ctl00_ContentPlaceHolder1_ddcpr_Type").change(function () {
        if ($(":selected", this).text().trim() == "规划合同") {
            $("#ctl00_ContentPlaceHolder1_txt_buildAreaEdit").css("background-color", "white");

        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_buildAreaEdit").css("background-color", "#FFC");

        }
    });
    var regOne = /[\\/:\*\?\""<>|]/;
    var regTwo = /^[^.]/;
    //检查合同名称是否重复
    $("#ctl00_ContentPlaceHolder1_txt_cprNameEdit").blur(function () {
        var projectName = $.trim($(this).val());
        if (regOne.test(projectName)) {
            alert("合同名称不能包括以下任何字符！\/:\*\?\"<>|");
            projectNameRepeat = true;
            return false;
        }
        if (!regTwo.test(projectName)) {
            alert("合同名称不能为空!");
            projectNameRepeat = true;
            return false;
        }
        if (projectName.length > 0) {
            $.post("/HttpHandler/ProjectMgr/ExistsInTGProjectHandler.ashx", { "cprname": projectName, "sysno": $("#ctl00_ContentPlaceHolder1_hid_cprid").val(), "action": "Supercpr" }, function (result) {
                if (result == "1") {
                    projectNameRepeat = true;
                    alert("合同名称已存在，请重新输入新合同名称！");
                } else {
                    projectNameRepeat = false;
                }
            });
        }
    });
    //隐藏和显示
    //1、合同编号
    $("#cpr_No").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_No").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(0).hide();
        } else {
            $("#cpr_No").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(0).show();
        }
    });
    //1、合同分类

    $("#ddcpr_Type").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddcpr_Type").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(1).hide();
        } else {
            $("#ddcpr_Type").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(1).show();
        }
    });
    //1、合同名称

    $("#cprName").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cprName").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(2).hide();
        } else {
            $("#cprName").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(2).show();
        }
    });
    //1、建筑面积
    $("#buildArea").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#buildArea").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(3).hide();
        } else {
            $("#buildArea").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(3).show();
        }
    });
    //1、建筑单位
    $("#cprBuildUnit").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cprBuildUnit").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(4).hide();
        } else {
            $("#cprBuildUnit").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(4).show();
        }
    });
    //1、规划面积
    $("#cprType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cprType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(5).hide();
        } else {
            $("#cprType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(5).show();
        }
    });
    //1、结构形式
    $("#StructType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#StructType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(6).hide();
            $("#EditDetail").children().find("tr").eq(7).hide(); $("#EditDetail").children().find("tr").eq(8).hide(); $("#EditDetail").children().find("tr").eq(9).hide();


        } else {
            $("#StructType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(6).show();
            $("#EditDetail").children().find("tr").eq(7).show(); $("#EditDetail").children().find("tr").eq(8).show(); $("#EditDetail").children().find("tr").eq(9).show();

        }
    });
    //1、建筑分类
    $("#BuildStructType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#BuildStructType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(10).hide(); $("#EditDetail").children().find("tr").eq(11).hide(); $("#EditDetail").children().find("tr").eq(12).hide(); $("#EditDetail").children().find("tr").eq(13).hide();
        } else {
            $("#BuildStructType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(10).show(); $("#EditDetail").children().find("tr").eq(11).show(); $("#EditDetail").children().find("tr").eq(12).show(); $("#EditDetail").children().find("tr").eq(13).show();
        }
    });
    //1、设计等级
    $("#buildType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#buildType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(14).hide();
        } else {
            $("#buildType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(14).show();
        }
    });
    //1、项目经理
    $("#proFuze").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#proFuze").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(15).hide();
            $("#EditDetail").children().find("tr").eq(16).hide();
        } else {
            $("#proFuze").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(15).show();
            $("#EditDetail").children().find("tr").eq(16).show();
        }
    });
    //1、层数
    $("#floor").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#floor").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(17).hide();
        } else {
            $("#floor").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(17).show();
        }
    });
    //1、甲方负责人
    $("#FParty").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#FParty").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(18).hide();
            $("#EditDetail").children().find("tr").eq(19).hide();
        } else {
            $("#FParty").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(18).show();
            $("#EditDetail").children().find("tr").eq(19).show();
        }
    });
    //1、承接部门
    $("#cjbm").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cjbm").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(20).hide();
        } else {
            $("#cjbm").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(20).show();
        }
    });
    //1、合同额
    $("#cpr_Account").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_Account").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(21).hide();
        } else {
            $("#cpr_Account").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(21).show();
        }
    });
    //1、投资额
    $("#InvestAccount").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#InvestAccount").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(22).hide();
        } else {
            $("#InvestAccount").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(22).show();
        }
    });
    //1、工程地点
    $("#ddProjectPosition").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddProjectPosition").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(23).hide();
        } else {
            $("#ddProjectPosition").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(23).show();
        }
    });
    //-------
    //1、实际合同额
    $("#cpr_Account0").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_Account0").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(24).hide();
        } else {
            $("#cpr_Account0").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(24).show();
        }
    });
    //1、实际投资额
    $("#InvestAccount0").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#InvestAccount0").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(25).hide();
        } else {
            $("#InvestAccount0").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(25).show();
        }
    });
    //1、行业性质
    $("#ddProfessionType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddProfessionType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(26).hide();
        } else {
            $("#ddProfessionType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(26).show();
        }
    });
    //1、合同阶段
    $("#ddcpr_Stage").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddcpr_Stage").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(27).hide();
        } else {
            $("#ddcpr_Stage").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(27).show();
        }
    });
    //1、制表人
    $("#tbcreate").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#tbcreate").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(28).hide();
        } else {
            $("#tbcreate").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(28).show();
        }
    });
    //1、工程来源
    $("#ddSourceWay").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddSourceWay").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(29).hide();
        } else {
            $("#ddSourceWay").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(29).show();
        }
    });
    //1、合同签订日期
    $("#SingnDate").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#SingnDate").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(32).hide();
        } else {
            $("#SingnDate").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、合同完成日期
    $("#CompleteDate").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#CompleteDate").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(31).hide();
        } else {
            $("#CompleteDate").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(31).show();
        }
    });
    $("#SingnDate2").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#SingnDate2").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(30).hide();
        } else {
            $("#SingnDate2").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(30).show();


        }
    });
    //1、多栋楼
    $("#multibuild").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#multibuild").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(33).hide();
        } else {
            $("#multibuild").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(33).show();

        }
    });
    //1、合同备注
    $("#cpr_Remark").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_Remark").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(34).hide();
        } else {
            $("#cpr_Remark").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(34).show();

        }
    });
    $("#cpr_chgplan").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_chgplan").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(35).hide();
        } else {
            $("#cpr_chgplan").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(35).show();


        }
    });

    //    //实例化类容
    messageDialog = $("#auditShow").messageDialog;


    //        "button": {
    //            "发送消息": function () {

    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                if (isEditApp == "0") {
    //                    getUserAndUpdateAuditForEdit('0', '1', jsonDataEntity);
    //                }
    //                else {
    //                    getUserAndUpdateAudit('1', '1', jsonDataEntity);
    //                }
    //            },
    //            "关闭": function () {
    //                $("#btnsub").show();
    //                messageDialog.hide();

    //            }
    //        }
    //    });
    sendMessageClass = new MessageCommon(messageDialog);
    $("#btn_Send").click(function () {
        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        }

        if (isEditApp == "0") {
            getUserAndUpdateAuditForEdit('0', '1', jsonDataEntity);
        }
        else {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
        }
    });
    //保存合同信息
    $("#btnsub").click(function () {
        var sd = $('input[type="checkbox"][name="bb"]:checked').length;
        var sdfs = $('input[type="checkbox"][name="aa"]:checked').length;
        if (sd == "0" && sdfs == "0") {
            alert("请选择要修改的项！");
            return false;
        }
        if (sd == "0" && parseInt(sdfs) > 0) {
            alert("不需要提交申请，已修改保存！");
            return false;
        }
        var msg = "";
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        //电话验证
        var reg_phone = /^(1[3,5,8,7]{1}[\d]{9})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{3,7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$/;



        var optionsArray = "";
        $('input[type="checkbox"][name="bb"]:checked').each(function () {
            var option = $(this).attr("id");
            switch (option) {
                case "cpr_No":
                    optionsArray += "cpr_No:" + $("#ctl00_ContentPlaceHolder1_txtcpr_NoEdit").val() + "|*|";
                    //合同编号
                    if ($("#ctl00_ContentPlaceHolder1_txtcpr_NoEdit").val() == "") {
                        msg += "请输入合同编号！</br>";
                    }
                    break;
                case "ddcpr_Type":
                    optionsArray += "cpr_Type:" + $("#ctl00_ContentPlaceHolder1_txt_ddcpr_TypeEdit").val() + "|*|";
                    //合同分类
                    if ($("#ctl00_ContentPlaceHolder1_txt_ddcpr_TypeEdit").val() == "-1") {
                        msg += "请选择合同分类！</br>";
                    }
                    break;
                case "cprName":
                    optionsArray += "cpr_Name:" + $("#ctl00_ContentPlaceHolder1_txt_cprNameEdit").val() + "|*|";
                    //合同名称
                    if ($("#ctl00_ContentPlaceHolder1_txt_cprNameEdit").val() == "") {
                        msg += "输入合同名称！</br>";
                    }
                    else {
                        //合同名称重复
                        if (projectNameRepeat) {
                            msg += "合同名称已存在，请重新输入新合同名称！</br>";
                        }
                    }
                    break;
                case "buildArea":
                    optionsArray += "BuildArea:" + $("#ctl00_ContentPlaceHolder1_txt_buildAreaEdit").val() + "|*|";
                    optionsArray += "AreaUnit:" + $("#ctl00_ContentPlaceHolder1_drp_AreaUnit").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txt_buildAreaEdit").val() != "") {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txt_buildAreaEdit").val())) {
                            msg += "建筑规模面积输入格式不正确！</br>";

                        }
                    } else {
                        msg += "请输入建筑规模！</br>";
                    }

                    break;
                case "cprBuildUnit":
                    optionsArray += "BuildUnit:" + $("#ctl00_ContentPlaceHolder1_txt_cprBuildUnitEdit").val() + "|*|";
                    //建设单位
                    if ($("#ctl00_ContentPlaceHolder1_txt_cprBuildUnitEdit").val() == "") {
                        msg += "请选择建设单位！</br>";
                    }
                    break;
                case "cprType":
                    optionsArray += "cpr_Type2:" + $("#ctl00_ContentPlaceHolder1_txt_cprTypeEdit").val() + "|*|";
                    //规划面积
                    if ($("#ctl00_ContentPlaceHolder1_txt_cprTypeEdit").val() != "-1") {
                        //
                    }
                    else {
                        msg += "请选择合同类型！</br>";
                    }
                    break;
                    /////                                                                                                                                                                                                                                                                                              
                case "StructType":
                    optionsArray += "StructType:" + $("#ctl00_ContentPlaceHolder1_asTreeviewStructEdit_divDropdownTreeText").text() + "|*|";
                    //                    var seerere = TG.Web.Coperation.cpr_CoperationEditChoose.output_Click();
                    //                    alert(seerere.value);
                    //结构形式
                    //判读是否是规划合同
                    if ($(":selected", "#ctl00_ContentPlaceHolder1_txt_ddcpr_TypeEdit").text().trim() != "规划合同") {
                        //结构形式
                        if (!IsStructCheckNode('struct')) {
                            msg += "请选择结构形式(结构样式太长或未选择)！</br>";
                        }

                    }
                    break;
                case "BuildStructType":
                    optionsArray += "BuildStructType:" + $("#ctl00_ContentPlaceHolder1_asTreeviewStructTypeEdit_divDropdownTreeText").text() + "|*|";
                    //判读是否是规划合同
                    if ($(":selected", "#ctl00_ContentPlaceHolder1_txt_ddcpr_TypeEdit").text().trim() != "规划合同") {


                        //建筑分类
                        if (!IsStructCheckNode('structtype')) {
                            msg += "请选择建筑分类(建筑分类太长或未选择)！</br>";
                        }
                    }
                    break;
                case "buildType":
                    optionsArray += "BuildTypelevel:" + $("#ctl00_ContentPlaceHolder1_drp_buildtype").val() + "|*|";
                    //设计等级
                    if ($("#ctl00_ContentPlaceHolder1_drp_buildtype").val() == "-1") {
                        msg += "请选择建筑类别！</br>";
                    }
                    break;
                    ///                                                                                                                                                                                                                                                                                              
                case "proFuze":
                    optionsArray += "ChgPeople:" + $("#ctl00_ContentPlaceHolder1_txt_proFuzeEdit").val() + "|*|";
                    optionsArray += "ChgPhone:" + $("#ctl00_ContentPlaceHolder1_txt_fzphoneEdit").val() + "|*|";
                    optionsArray += "HiddenPMUserID:" + $("#ctl00_ContentPlaceHolder1_HiddenPMUserID").val() + "|*|";
                    //项目经理
                    if ($.trim($("#ctl00_ContentPlaceHolder1_txt_proFuzeEdit").val()) == "") {
                        msg += "请填写工程负责人！</br>";
                    }

                    if ($("#ctl00_ContentPlaceHolder1_txt_fzphoneEdit").val() != "") {
                        if (!reg_phone.test($("#ctl00_ContentPlaceHolder1_txt_fzphoneEdit").val())) {
                            msg += "工程负责人电话输入格式不正确！</br>";

                        }
                    }

                    break;
                case "floor":
                    optionsArray += "floor:" + $("#ctl00_ContentPlaceHolder1_txt_upfloor").val() + "|" + $("#ctl00_ContentPlaceHolder1_txt_downfloor").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txt_upfloor").val() != "") {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txt_upfloor").val())) {
                            msg += "楼层输入格式不正确！</br>";
                        }
                    }
                    if ($("#ctl00_ContentPlaceHolder1_txt_downfloor").val() != "") {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txt_downfloor").val())) {
                            msg += "楼层输入格式不正确！</br>";

                        }
                    }
                    break;
                case "FParty":
                    optionsArray += "ChgJia:" + $("#ctl00_ContentPlaceHolder1_txtFPartyEdit").val() + "|*|";
                    optionsArray += "ChgJiaPhone:" + $("#ctl00_ContentPlaceHolder1_txt_jiafphoneEdit").val() + "|*|";
                    //甲方负责人
                    if ($.trim($("#ctl00_ContentPlaceHolder1_txtFPartyEdit").val()) == "") {
                        msg += "请填写甲方负责人！</br>";
                    }

                    if ($("#ctl00_ContentPlaceHolder1_txt_jiafphoneEdit").val() != "") {
                        if (!reg_phone.test($("#ctl00_ContentPlaceHolder1_txt_jiafphoneEdit").val())) {
                            msg += "甲方负责人电话输入格式不正确！</br>";

                        }
                    }
                    break;
                case "cjbm":
                    optionsArray += "cpr_Unit:" + $("#ctl00_ContentPlaceHolder1_txt_cjbmEdit").val() + "|*|";
                    //承接部门
                    if ($("#ctl00_ContentPlaceHolder1_txt_cjbmEdit").val() == "") {
                        msg += "请选择承接部门！</br>";
                    }

                    break;
                case "cpr_Account":
                    optionsArray += "cpr_Acount:" + $("#ctl00_ContentPlaceHolder1_txtcpr_AccountEdit").val() + "|*|";
                    //合同金额
                    if ($("#ctl00_ContentPlaceHolder1_txtcpr_AccountEdit").val() == "") {
                        msg += "请输入合同额！</br>";
                    }
                    else {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtcpr_AccountEdit").val())) {
                            msg += "合同额格式不正确！</br>";
                        }
                    }
                    break;
                case "InvestAccount":
                    optionsArray += "cpr_Touzi:" + $("#ctl00_ContentPlaceHolder1_txtInvestAccountEdit").val() + "|*|";
                    //投资额
                    if ($("#ctl00_ContentPlaceHolder1_txtInvestAccountEdit").val() != "") {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtInvestAccountEdit").val())) {
                            msg += "投资额请输入数字！</br>";
                        }
                    }
                    else {
                        msg += "请输入投资额！</br>";
                    }
                    break;
                case "ddProjectPosition":
                    optionsArray += "BuildPosition:" + $("#ctl00_ContentPlaceHolder1_txt_ProjectPosition").val() + "|*|";
                    //建设地点
                    if ($.trim($("#ctl00_ContentPlaceHolder1_txt_ProjectPosition").val()) == "") {
                        msg += "请填写工程地点！</br>";
                    }
                    break;
                case "cpr_Account0":
                    optionsArray += "cpr_ShijiAcount:" + $("#ctl00_ContentPlaceHolder1_txtcpr_AccountEdit0").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txtcpr_AccountEdit0").val() != "") {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtcpr_AccountEdit0").val())) {
                            msg += "实际合同额输入格式不正确！</br>";

                        }
                    }

                    break;
                case "InvestAccount0":
                    optionsArray += "cpr_ShijiTouzi:" + $("#ctl00_ContentPlaceHolder1_txtInvestAccountEdit0").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txtInvestAccountEdit0").val() != "") {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtInvestAccountEdit0").val())) {
                            msg += "实际投资额输入格式不正确！</br>";

                        }
                    }
                    break;
                case "ddProfessionType":
                    optionsArray += "Industry:" + $("#ctl00_ContentPlaceHolder1_txt_ddProfessionTypeEdit").val() + "|*|";

                    if ($.trim($("#ctl00_ContentPlaceHolder1_txt_ddProfessionTypeEdit").val()) == "") {
                        msg += "请选择行业性质！</br>";
                    }
                    break;
                    ////                                                                                                                                                                                                                                                                 
                case "ddcpr_Stage":
                    var processLength = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;
                    if (processLength == 0) {
                        msg += "请至少选择一项合同阶段！</br>";
                    }

                    var Stage = "";
                    var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;

                    for (var i = 0; i < parseInt(see) ; i++) {
                        var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class");
                        if (isChecked == undefined) {

                        } else {
                            Stage += $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().parent().next().text() + ",";

                        }
                    }
                    if (Stage == "") {
                        msg += "请选择合同阶段</br>";
                    }
                    optionsArray += "cpr_Process:" + Stage + "|*|";
                    break;
                    ////                                                                                                                                                                                                                                                                 
                case "tbcreate":
                    optionsArray += "TableMaker:" + $("#ctl00_ContentPlaceHolder1_txt_tbcreateEdit").val() + "|*|";
                    break;
                case "ddSourceWay":
                    optionsArray += "BuildSrc:" + $("#ctl00_ContentPlaceHolder1_txt_ddSourceWayEdit").val() + "|*|";
                    if ($.trim($("#ctl00_ContentPlaceHolder1_txt_ddSourceWayEdit").val()) == "") {
                        msg += "请选择工程来源！</br>";
                    }
                    break;
                case "SingnDate":
                    optionsArray += "cpr_SignDate:" + $("#ctl00_ContentPlaceHolder1_txtSingnDateEdit").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txtSingnDateEdit").val() == "") {
                        msg += "请选择合同统计年份！</br>";
                    }
                    break;
                case "SingnDate2":
                    optionsArray += "cpr_SignDate2:" + $("#ctl00_ContentPlaceHolder1_txtSingnDateEdit2").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txtSingnDateEdit2").val() == "") {
                        msg += "请选择合同签订日期！</br>";
                    }
                    break;
                case "CompleteDate":
                    optionsArray += "cpr_DoneDate:" + $("#ctl00_ContentPlaceHolder1_txtCompleteDateEdit").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txtCompleteDateEdit").val() == "") {
                        msg += "请选择合同完成日期！</br>";
                    }
                    break;
                case "multibuild":
                    optionsArray += "MultiBuild:" + $("#ctl00_ContentPlaceHolder1_txt_multibuildEdit").val() + "|*|";
                    break;
                case "cpr_Remark":
                    optionsArray += "cpr_Mark:" + $("#ctl00_ContentPlaceHolder1_txtcpr_RemarkEdit").val() + "|*|";
                    break;

            }

        });
        // 
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            $(this).hide();
        }


        var auditObj = {
            CoperationSysNo: $("#HiddenCoperationSysNo").val(),
            InUser: $("#HiddenLoginUser").val(),
            Status: "A",
            options: optionsArray

        };
        jsonDataEntity = Global.toJSON(auditObj);
        isEditApp = "0";
        //xianshi

        //弹出审批
        getUserAndUpdateAuditForEdit('0', '0', jsonDataEntity);
    });
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#previewPower").val(),
        "userSysNum": $("#userSysNum").val(),
        "userUnitNum": $("#userUnitNum").val(),
        "notShowUnitList": ""
    };
    $("#btncancl").click(function () {
        window.history.back()
    });
    //行变色
    //  $(".cls_show_cst_jiben>tbody>tr:odd").attr("style", "background-color:#FFF");
    //查询按钮
    $("#btn_search").click(function () {
        chooseCustomer.ClearQueryCondition();
        //        $("#chooseCustomerContainer").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 740,
        //            resizable: false,
        //            title: "查询客户"
        //        }).dialog("open");
    });
    //合同类型
    $("#btn_cprType").click(function () {
        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "cprType", CproTypeCallBack);
        //        $("#chooseCustomerCompact").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 460,
        //            top: 200,
        //            resizable: false,
        //            title: "选择合同类型"
        //        }).dialog("open");
    });
    //工程负责人
    $("#btn_gcfz").click(function () {
        //先赋值
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        var unit_ID = $("#select_gcFzr_Unit").val();
        if (unit_ID < 0 || unit_ID == null) {
            //绑定工程负责部门
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
        }
        //        $("#gcFzr_Dialog").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 460,
        //            height: 380,
        //            top: 200,
        //            resizable: false,
        //            title: "项目经理"
        //        }).dialog("open");
    });
    //承接部门
    $("#btn_cjbm").click(function () {

        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "prevPage": "cjbm_prevPage",
            "firstPage": "cjbm_firstPage",
            "nextPage": "cjbm_nextPage",
            "lastPage": "cjbm_lastPage",
            "gotoPage": "cjbm_gotoPageIndex",
            "allDataCount": "cjbm_allDataCount",
            "nowIndex": "cjbm_nowPageIndex",
            "allPageCount": "cjbm_allPageCount",
            "gotoIndex": "cjbm_pageIndex",
            "pageSize": "10"
        });

        var isValue = $("#ctl00_ContentPlaceHolder1_txt_cjbm").val().length;
        if (isValue == null || isValue == undefined || isValue <= 0) {
            //$("#cpr_cjbmTable tr:gt(0)").remove();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
            BindAllDataCount(); //绑定总数据
        }
        //        $("#pro_cjbmDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 460,
        //            height: 340,
        //            resizable: false,
        //            title: "承接部门"
        //        }).dialog("open");
    });
    //甲方负责人
    $("#btn_jffz").click(function () {

        showDivDialogClass.SetParameters({
            "prevPage": "jffzr_prevPage",
            "firstPage": "jffzr_firstPage",
            "nextPage": "jffzr_nextPage",
            "lastPage": "jffzr_lastPage",
            "gotoPage": "jffzr_gotoPageIndex",
            "allDataCount": "jffzr_allDataCount",
            "nowIndex": "jffzr_nowPageIndex",
            "allPageCount": "jffzr_allPageCount",
            "gotoIndex": "jffzr_pageIndex",
            "pageSize": "10"
        });
        var selectVal = $("#select_jffzrMem").val();
        if (selectVal == null || selectVal == undefined) {
            //$("#jffzr_table tr:gt(0)").remove();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "jffzrCop", JffzrUnitCallBack);
        }
        //        $("#jffzr_dialogDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 560,
        //            top: 200,
        //            height: 400,
        //            resizable: false,
        //            title: "甲方负责人"
        //        }).dialog("open");
    });

    //合同编号
    //选择合同编号
    $("#btn_getcprnum").click(function () {
        //赋值
        showDivDialogClass.SetParameters({
            "pageSize": "0"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "0", "cprNumUnit", CprNumUnitCallBack);
        //        $("#cpr_Number").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 340,
        //            resizable: false,
        //            title: "选择合同编号"
        //        }).dialog("open");
    });
    //点击单选控件重新加载
    //添加收费

    //添加计划收费
    $("#btn_AddSf").live("click", function () {
        $("h4", "#TJSF").text("添加计划收费");
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").text();
        chargeid = "0";
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").text())) {
                alert("合同金额格式不正确！");
                return false;
            }
            else {
                $("#lbl_copMoney").text(cprAllcount);
                //每次初始化信息
                $("#chargeTypeSelect").val(-1);
                $("#txt_planChargeNum").val("");
                $("#txt_datePicker").val("");
                $("#txt_chargeRemark").val("");
                $("#planChargeNumPercent").val("");
                var cprAllcount = $("#txtcpr_Account").val(); //总金额
                $("#lbl_copMoney").text(cprAllcount);

                $("#txt_planChargeNum").unbind('change').change(function () {
                    var chargeNum = $(this).val();
                    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
                    if (!reg.test(chargeNum)) {
                        $("#jine_notint").show();
                        return false;
                    } else {
                        $("#jine_notint").hide();
                    }
                    var lblCprMoney = $("#lbl_copMoney").text();
                    $("#planChargeNumPercent").val((parseFloat(chargeNum) / parseFloat(lblCprMoney) * 100).toFixed(2));
                });
            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //修改计划收费
    $("span[class='update']").live("click", function () {
        alert("sss");
        $("#TJSF").modal();
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").text();
        chargeid = $(this).attr("rel");
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").text())) {
                alert("合同金额格式不正确！");
                return false;
            }
            else {
                $("h4", "#TJSF").text("编辑计划收费");
                $("#lbl_copMoney").text(cprAllcount);
                //初始化信息
                var trtd = $(this).parent().parent().find("TD");
                $("#txt_planChargeNum").val(trtd.eq(2).text());
                $("#txt_datePicker").val(trtd.eq(3).text());
                $("#planChargeNumPercent").val(trtd.eq(1).text());
                $("#txt_chargeRemark").val(trtd.eq(4).text());
                var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").text(); //总合同金额
                $("#lbl_copMoney").text(cprAllcount);

                $("#txt_planChargeNum").unbind('change').change(function () {
                    var chargeNum = $(this).val();
                    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                    if (!reg.test(chargeNum)) {
                        $("#jine_notint").show();
                        return false;
                    } else {
                        $("#jine_notint").hide();
                    }
                    var lblCprMoney = $("#lbl_copMoney").text();
                    $("#planChargeNumPercent").val((parseFloat(chargeNum) / parseFloat(lblCprMoney) * 100).toFixed(2));
                });

                //比例
                $("#planChargeNumPercent").unbind('change').change(function () {
                    var chargeNum = $(this).val();
                    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                    if (!reg.test(chargeNum)) {
                        $("#span_PercentNotInt").show();
                        return false;
                    } else {
                        $("#span_PercentNotInt").hide();
                    }
                    var lblCprMoney = $("#lbl_copMoney").text();
                    $("#txt_planChargeNum").val(parseFloat(chargeNum) * parseFloat(lblCprMoney) / 100);
                });
                $("#chargeType_notselect").hide();
                $("#jine_notnull").hide();
                $("#jine_notint").hide();
                $("#jine_xiaoyu").hide();
                $("#date_notnull").hide();
            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //添加计划收费信息-确定按钮
    $("#btn_addPlanCharge").click(function () {
        var tempId = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
        var txtPlanChargeNum = $("#txt_planChargeNum").val();
        var lblCprMoney = $("#lbl_copMoney").text();
        var txtDatePic = $("#txt_datePicker").val();
        var txtSkr = $("#ctl00_ContentPlaceHolder1_userShortName").val();
        var txtRemark = $("#txt_chargeRemark").val();
        var data = "action=addcprsktype&cpr_type=supercharge&flag=edit";
        //金额
        if (txtPlanChargeNum == "") {
            $("#jine_xiaoyu").hide();
            $("#jine_notint").hide();
            $("#jine_notnull").show();
            return false;
        } else {
            $("#jine_notnull").hide();
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
            if (!reg.test(txtPlanChargeNum)) {
                $("#jine_xiaoyu").hide();
                $("#jine_notint").show();
                return false;
            } else {
                $("#jine_notint").hide();
            }
            //判断合同额
            if (parseFloat(lblCprMoney) < parseFloat(txtPlanChargeNum)) {
                $("#jine_xiaoyu").show();
                return false;
            } else {
                $("#jine_xiaoyu").hide();
            }
            data += "&jine=" + escape(txtPlanChargeNum);
        }
        //合同总金额
        data += "&allcount=" + escape($("#lbl_copMoney").text());
        //时间
        if (txtDatePic == "") {
            $("#date_notnull").show();
            return false;
        } else {
            $("#date_notnull").hide();
            data += "&date=" + escape($("#txt_datePicker").val());
        }
        //收款人
        if (txtSkr == "") {
            $("#skr_notnull").show();
            return false;
        } else {
            $("#skr_notnull").hide();
            data += "&skr=" + escape(txtSkr);
        }
        data += "&mark=" + escape(txtRemark) + "&cprid=" + tempId + "&chargeid=" + chargeid + "&payType=" + escape($("#chargeTypeSelect option:selected").text());
        //data = encodeURI(data);
        //添加收款信息
        $.ajax({
            type: "GET",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            dataType: "text",
            success: function (result) {
                if (result == "yes") {
                    //alert("添加收款信息成功!");
                    loadCprCharge();
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").attr("readonly", "readonly");
                }
                else {
                    alert("计划收款金额超过合同总额！合同余额：" + result + "万元。");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误！");
            }
        });
    });
    var chooseCustomer = new ChooseCustomer($("#chooseCustomerContainer"), chooseCustomerCallBack);

    CommonControl.SetFormWidth();

    //只能选择控制
    $(".cls_input_text_onlyslt").focus(function () {
        $(this).blur();
    });

});
//返回长度
function DataLength(fdata) {
    var tlength = 0;
    for (var i = 0; i < fdata.length; i++) {
        if (fdata.charCodeAt(i) < 0 || fdata.charCodeAt(i) > 255) {
            tlength = tlength + 2;
        } else {
            tlength = tlength + 1;
        }
    }
    return tlength;
}
//客户选择回调
function chooseCustomerCallBack(recordObj) {
    $("#txtCst_No").val(recordObj.CustomerNo);
    $("#txtCst_Name").val(recordObj.CustomerName);
    $("#txtCpy_Address").val(recordObj.Address);
    $("#txtCode").val(recordObj.ZipCode == "0" ? "" : recordObj.ZipCode);
    $("#txtLinkman").val(recordObj.LinkMan);
    $("#txtCpy_Phone").val(recordObj.Phone);
    $("#txtCpy_Fax").val(recordObj.Fax);
    $("#hid_cstid").val(recordObj.SysNo);
    $("#txtCst_Brief").val(recordObj.CustomerShortName);
}

//加载收款信息
function loadCprCharge() {
    var data = "action=getplanchargetype&cpr_type=supercharge&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {

                var data = result.ds;
                if ($("#sf_datas tr").length > 1) {
                    $("#sf_datas tr:gt(0)").remove();
                }

                $.each(data, function (i, n) {
                    var row = $("#sf_row").clone();
                    var oper = "<span class='update' rel='" + n.ID + "' data-toggle=\"modal\" style='cursor: pointer;color:blue;'>编辑</span>&nbsp;<span class='del' rel='" + n.ID + "' style='cursor: pointer;color:blue;'>删除</span>";

                    row.find("#sf_id").text(i + 1);
                    row.find("#sf_bfb").text(n.persent + "%");
                    row.find("#sf_edu").text(n.payCount);
                    row.find("#sf_type").html(n.payType == null ? "" : n.payType); //新增,收费类型
                    row.find("#sf_time").html(n.paytime);
                    var markShow = n.mark;
                    if (n.mark.length > 20) {
                        markShow = markShow.substr(0, 20) + "...";
                    }
                    row.find("#sf_mark").attr("title", n.mark);
                    row.find("#sf_mark").html(markShow); //新增,备注
                    row.find("#sf_oper").html(oper);
                    row.find("#sf_oper span[class=del]").click(function () {
                        if (confirm("确定要删除此条计划收费吗？")) {
                            //删除事件
                            delChargeItem($(this));
                        }
                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#sf_datas");
                });
                $("#sf_datas").append("<tr><td colspan='6' align='center'>   <button type=\"button\" class=\"btn default\" data-toggle=\"modal\" href=\"#TJSF\" id=\"btn_AddSf\">添加</button></td></tr>")
                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//加载分项信息
function loadSubCopration() {
    var data = "action=getsubitemdata&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {

                var data = result.ds;
                if ($("#datas tr").length > 1) {
                    $("#datas tr:gt(0)").remove();
                }
                var i_rowindex = 0;
                $.each(data, function (i, n) {
                    var row = $("#sub_row").clone();
                    var oper = "<span id='update' rel='" + n.ID + "' style='cursor: pointer;color:blue;'>编辑</span>&nbsp;<span id='del' rel='" + n.ID + "' style='cursor: pointer;color:blue;'>删除</span>";
                    var cprRemark = n.Remark;
                    if (cprRemark.length > 20) {
                        cprRemark = cprRemark.substr(0, 20) + "...";
                    }
                    //增加行
                    i_rowindex++
                    row.find("#sub_id").text(i_rowindex);
                    row.find("#sub_name").text(n.Item_Name);
                    row.find("#sub_area").text(n.Item_Area);
                    row.find("#sub_oper").html(oper);
                    row.find("#sub_money").html(n.Money);
                    row.find("#sub_livearea").html(n.Living_Area);
                    row.find("#sub_moneyunit").html(n.Money_Unit);
                    row.find("#sub_remark").attr("title", n.Remark);
                    row.find("#sub_remark").html(cprRemark);
                    row.find("#sub_oper span[id=del]").click(function () {
                        if (confirm("确定要删除此条分项信息吗？")) {
                            //删除事件
                            delSubItem($(this));
                        }
                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas");
                });

                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除计划收费
function delChargeItem(link) {
    var data = "action=delchargeitem&chrgid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                //  alert("计划收费删除成功！");
                if (($("#sf_datas tr").length <= 1) && ($("#datas tr").length <=
 1)) {
                    $("#txtcpr_Account").removeAttr("readonly");
                }

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除子项
function delSubItem(link) {
    var data = "action=delsubitem&subid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                //  alert("删除子项成功！");
                if (($("#datas tr").length <= 1)) {
                    $("#txtcpr_Account").removeAttr("readonly");
                    $("#txt_buildArea").removeAttr("readonly");
                    $("#txt_area").removeAttr("readonly");
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}

//加载数据
function GetAttachData() {
    loadCoperationAttach();
}
//加载附件信息
function loadCoperationAttach() {
    var data = "action=getcprfiles&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {


                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href='#' rel='" + n.ID + "'>删除</a>";
                    var oper2 = "<a href='../Attach_User/filedata/cprfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString + 'KB');
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper a").click(function () {
                        delCprAttach($(this));
                    });
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                //加载附件
                link.parents("tr:first").remove();
                // alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//合同类型绑定表格CallBack
function CproTypeCallBack(result) {
    $("#customerCompactTable tr:gt(0)").remove();
    var data = result.ds;
    $.each(data, function (i, n) {
        var oper = "<span rel='" + n.ID + "' style=\"color:blue;cursor:pointer\"data-dismiss=\"modal\" >选择</span>";
        var trHtml = "<tr><td>" + (i + 1) + "</td><td>" + n.dic_Name + "</td><td>" + oper + "</td></tr>";
        $("#customerCompactTable").append(trHtml);
        $("#customerCompactTable span:last").click(function () {
            $("#ctl00_ContentPlaceHolder1_txt_cprTypeEdit").val(n.dic_Name);
            //            $("#chooseCustomerCompact").dialog().dialog("close");
        });
    });
    ControlTableCss("customerCompactTable");
}

//承接部门表格数据绑定-CallBack函数
function ProCjbmCallBack(result) {
    var data = result.ds;
    $("#pro_cjbmTable tr:gt(0)").remove();
    $.each(data, function (i, n) {
        var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer\"data-dismiss=\"modal\" >选择</span>";
        var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
        $("#pro_cjbmTable").append(trHtml);
        $("#pro_cjbmTable span:last").click(function () {
            $("#ctl00_ContentPlaceHolder1_txt_cjbmEdit").val($.trim(n.unit_Name));
            $("#ctl00_ContentPlaceHolder1_hid_cjbm").val($.trim(n.unit_Name));
            //            $("#pro_cjbmDiv").dialog().dialog("close");
        });
    });
    for (var trBland = 0; trBland < ParametersDict.pageSize - data.length; trBland++) {

    }
    ControlTableCss("pro_cjbmTable");
}
//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
        }
    });
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}


//工程负责人绑定部门CallBack
function CprTypeUnitCallBack(result) {
    var data = result.ds;
    var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
    $.each(data, function (i, n) {
        gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
    });
    $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
    //注册部门选项改变事件
    showDivDialogClass.SetParameters({ "pageSize": "10" });
    $("#select_gcFzr_Unit").unbind('change').change(function () {
        var unit_ID = $("#select_gcFzr_Unit").val();
        if (Math.abs(unit_ID) > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
            BindAllDataCountGcfzr(unit_ID);
        }
    });
}
//绑定工程负责人表格数据CallBack
function BindGcfzrDataCallBack(result) {
    var obj = result.ds;
    var gcFzrMemTableHtml;
    $("#gcFzr_MemTable tr:gt(0)").remove();
    $.each(obj, function (i, n) {
        var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue;cursor:pointer\"data-dismiss=\"modal\" >选择</span>";
        gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + oper + "</td></tr>";
        $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
        $("#gcFzr_MemTable span:last").click(function () {
            $("#ctl00_ContentPlaceHolder1_txt_proFuzeEdit").val(n.mem_Name);
            $("#ctl00_ContentPlaceHolder1_txt_fzphoneEdit").val(n.mem_Mobile);
            //增加项目经理ID qpl 20131225
            $("#ctl00_ContentPlaceHolder1_HiddenPMUserID").val(n.mem_ID);

            //            $("#gcFzr_Dialog").dialog().dialog("close");
        });
    });
    for (var trBland = 0; trBland < ParametersDict.pageSize - obj.length; trBland++) {

    }
    ControlTableCss("gcFzr_MemTable");
}
//绑定工程负责人数据总数
function BindAllDataCountGcfzr(unit_ID) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "gcfzr_prevPage",
        "firstPage": "gcfzr_firstPage",
        "nextPage": "gcfzr_nextPage",
        "lastPage": "gcfzr_lastPage",
        "gotoPage": "gcfzr_gotoPageIndex",
        "allDataCount": "gcfzr_allDataCount",
        "nowIndex": "gcfzr_nowPageIndex",
        "allPageCount": "gcfzr_allPageCount",
        "gotoIndex": "gcfzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
    //注册事件,先注销,再注册
    $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#gcfzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
        }
    });
}
//项目经理数据总数CallBack
function GetGcfzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $("#gcfzr_allDataCount").text(0);
        $("#gcfzr_nowPageIndex").text(0);
        $("#gcfzr_allPageCount").text(0);
        NoDataMessageOnTable("gcFzr_MemTable", 3);
    }
}

//甲方负责人部门CallBack
function JffzrUnitCallBack(result) {
    if (result == null || result == undefined) {
        $("#select_jffzrMem").html('<option value="-1">--------------请选择--------------</option>');
        $("#noCustMsg").show();
        return false;
    }
    $("#noCustMsg").hide();
    var data = result.ds;
    var optionHtml = '<option value="-1">--------------请选择--------------</option>';
    $.each(data, function (i, n) {
        optionHtml += '<option value="' + n.Cst_Id + '">' + n.Cst_Name + '</option>';
    });
    $("#select_jffzrMem").html(optionHtml);
    //注册部门选项改变事件
    showDivDialogClass.SetParameters({ "pageSize": "10" });
    $("#select_jffzrMem").unbind('change').change(function () {
        var Cst_Id = $("#select_jffzrMem").val();
        if (Math.abs(Cst_Id) > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", "1", "jffzrMem", BindJffzrDataCallBack);
            BindAllDataCountJffzr(Cst_Id);
        }
    });
}
//绑定表格数据CallBack
function BindJffzrDataCallBack(result) {
    var obj = result.ds;
    var jffzrTableHtml;
    $("#jffzr_dataTable tr:gt(0)").remove();
    $.each(obj, function (i, n) {
        var oper = "<span rel='" + n.Cst_Id + "' style=\"color:blue;cursor:pointer\"data-dismiss=\"modal\" >选择</span>";
        jffzrTableHtml = "<tr style='text-align:center'><td>" + n.Cst_Id + "</td><td>"
        + n.Name + "</td><td>" + n.Duties + "</td><td>" + n.Department + "</td><td>" + n.Phone + "</td><td>" + oper + "</td></tr>";
        $("#jffzr_dataTable").append(jffzrTableHtml);
        $("#jffzr_dataTable span:last").click(function () {
            $("#ctl00_ContentPlaceHolder1_txtFPartyEdit").val($.trim(n.Name));
            $("#ctl00_ContentPlaceHolder1_txt_jiafphoneEdit").val($.trim(n.Phone));
            //            $("#jffzr_dialogDiv").dialog().dialog("close");
        });
    });
    for (var trBland = 0; trBland < ParametersDict.pageSize - obj.length; trBland++) {

    }
    ControlTableCss("jffzr_dataTable");
}
//甲方负责人数据总数
function BindAllDataCountJffzr(Cst_Id) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "jffzr_prevPage",
        "firstPage": "jffzr_firstPage",
        "nextPage": "jffzr_nextPage",
        "lastPage": "jffzr_lastPage",
        "gotoPage": "jffzr_gotoPageIndex",
        "allDataCount": "jffzr_allDataCount",
        "nowIndex": "jffzr_nowPageIndex",
        "allPageCount": "jffzr_allPageCount",
        "gotoIndex": "jffzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", Cst_Id, "true", "jffzrMem", GetJffzrAllDataCount);
    //注册事件,先注销,再注册
    $("#jffzrByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", pageIndex, "jffzrMem", BindJffzrDataCallBack);
        }
    });
}
//获取甲方负责人总数CallBack
function GetJffzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#jffzr_dataTable tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_dataTable", 6);
    }
}


function CprNumUnitCallBack(result) {
    if ($("#ctl00_ContentPlaceHolder1_txtcpr_NoEdit").val() == "" || $("#ctl00_ContentPlaceHolder1_txtcpr_NoEdit").val() == "(例：BJ2012-09)") {
        var data = result == null ? "" : result.ds;
        var cpr_typeSelect_htm = '<option value="-1">--------请选择--------</option>';
        $.each(data, function (i, n) {
            cpr_typeSelect_htm += '<option value="' + n.ID + '">' + n.CprType + '</option>';
        });
        $("#cpr_typeSelect").html(cpr_typeSelect_htm);
        $('#cpr_numSelect').empty();
    }
    //注册合同类型选项改变的事件
    $("#cpr_typeSelect").unbind('change').change(function () {
        $("#noselectMsg").hide();
        var CprTypeID = $(this).val();
        if (CprTypeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", CprTypeID, "false", "0", "cprNumUnit", CprNumCallBack);
        } else {
            $('#cpr_numSelect').empty();
        }

    });
    //注册确定按钮点击事件
    $("#btn_cprNum_close").unbind('click').click(function () {
        var CprNum = $("#cpr_numSelect").find("option:selected").text();
        if (CprNum != NaN && CprNum != "" && CprNum != undefined) {
            $("#noselectMsg").hide();
            $("#ctl00_ContentPlaceHolder1_txtcpr_NoEdit").val(CprNum);
            $("#ctl00_ContentPlaceHolder1_hid_cprno").val(CprNum);

            //            $("#cpr_Number").dialog().dialog("close");
        } else {
            $("#noselectMsg").show();
        }
    });
}
function CprNumCallBack(result) {
    var obj = result == null ? "" : result.ds;
    var cpr_TypeSelectChangeHtml;
    var prevTxt;
    $.each(obj, function (i, n) {
        var startNums = n.CprNumStart.split('-');
        var endNums = n.CprNumEnd.split('-');
        prevTxt = startNums[0] + "-";
        var startNum = Math.abs(GetCprNumFix(startNums[1])); //开始编号
        var endNum = Math.abs(GetCprNumFix(endNums[1])); //结束编号
        for (var i = startNum; i <= endNum; i++) {
            var cprNumOk = prevTxt + GetCprNumFix(i);
            cpr_TypeSelectChangeHtml += '<option cprNum="' + cprNumOk + '" value="' + GetCprNumFix(i) + '">' + cprNumOk + '</option>';
        }
    });
    $('#cpr_numSelect').empty();
    $('#cpr_numSelect').html(cpr_TypeSelectChangeHtml);
    //去除已使用的编号
    BindCprNumber(prevTxt);
}
//处理合同编号数字
function GetCprNumFix(cprNo) {
    if (cprNo < 10) {
        return "00" + cprNo;
    } else if (10 <= cprNo && cprNo < 100) {
        return "0" + cprNo;
    } else { return cprNo; }
}
//去除已使用的合同编号
function BindCprNumber(fixstr) {
    var data = "action=removeUsedCprNum&fixStr=" + fixstr;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                $.each(obj, function (i, n) {
                    $("#cpr_numSelect option[cprNum=" + n.cpr_No + "]").remove();
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!未能去除已使用的合同编号!");
        }
    });
}

//添加收款计划 by fbw
function AddChargePlan() {
    $("#tjsfPlan_dialogDiv").dialog({
        autoOpen: false,
        modal: true,
        width: 540,
        top: 100,
        resizable: false,
        title: "添加计划收费信息"
    }).dialog("open");

    $("#lbl_copMoney").text(cprAllcount);
    //每次初始化信息
    $("#chargeTypeSelect").val(-1);
    $("#txt_planChargeNum").val("");
    $("#txt_datePicker").val("");
    $("#txt_chargeRemark").val("");
    $("#planChargeNumPercent").val("");
    var cprAllcount = $("#txtcpr_Account").val(); //总金额
    $("#lbl_copMoney").text(cprAllcount);

    $("#txt_planChargeNum").unbind('change').change(function () {
        var chargeNum = $(this).val();
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
        if (!reg.test(chargeNum)) {
            $("#jine_notint").show();
            return false;
        } else {
            $("#jine_notint").hide();
        }
        var lblCprMoney = $("#lbl_copMoney").text();
        $("#planChargeNumPercent").val((parseFloat(chargeNum) / parseFloat(lblCprMoney) * 100).toFixed(2));
    });
}

//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程

function getUserAndUpdateAuditForEdit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/SuperCoperationAuditEditHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (jsonResult == "0") {
            alert("审批角色没有对应人员！");
            return false;
        }
        else if (jsonResult == "1") {
            alert("已发起合同修改审批，不能重复发起！");
            return false;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        alert("合同修改审批已成功！\n消息已成功发送到审批人等待审批！");
        //查询系统新消息
        window.location.href = "/Coperation/cpr_CoperationAuditListEditBymaster.aspx";
        //返回消息列表
        //window.history.back();
    } else {
        alert("消息发送失败！");
    }
}

