﻿$(document).ready(function () {

    //单位
    var op = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    //年
    var yearall = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    if (yearall != "-1") {
        $("#title").html(yearall + '年');
    } else {
        $("#title").html('全部年份');
    }
    if (op == "-1") {
        $("#unit").html("");
    }
    else {
        $("#unit").html(op);
    }
    //统计
    $("#btn_ok").click(function () {
        //判断
        if ($("#ctl00_ContentPlaceHolder1_drp_unit").get(0).selectedIndex == 0) {
            alert('请选择要统计的生产部门！');
            return false;
        }
        $("#AddTable tr").remove();
        //单位
        var op = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        //年
        var yearall = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var cprName = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        if (yearall != "-1") {
            $("#title").html(yearall + '年');
        } else {
            $("#title").html('全部年份');
        }
        if (op == "-1") {
            $("#unit").html("");
        }
        else {
            $("#unit").html(op);
        }
        var userShortName = $("#ctl00_ContentPlaceHolder1_userShortName").val();
        var previewPower = $("#ctl00_ContentPlaceHolder1_previewPower").val();
        var userSysNum = $("#ctl00_ContentPlaceHolder1_userSysNum").val();
        var userUnitNum = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
        $.get("../HttpHandler/HandlerContrast.ashx", { "action": "6", "year": yearall, "unitid": op, "previewPower": previewPower, "userUnitNum": userUnitNum, "cprName": cprName }, function (data) {
            if (data == "[]") {
                var $tr = $("<tr></tr>");
                var $td1 = $("<td colspan='13' style='color:red;'>没有数据！</td>");
                $tr.append($td1);
                $("#AddTable").append($tr);
            }
            var json = eval('(' + data + ')');
            var v = 1;
            $.each(json, function (key, valObj) {
                var $tr = $("<tr></tr>");
                var $td1 = $("<td style=\"width: 5%\">" + v + "</td>");
                var $td2 = $("<td style=\"width: 30%;text-align:left;\" ><div class=\"cls_Column_Short\" title=\"" + valObj.CprName + "\" style=\"width:20em;\">" + valObj.CprName + "</div></td>");
                var $char = $("<input type='hidden' class='hiddencharge' value=\"" + valObj.Charge + "\">");
                var $td3 = $("<td style=\"width: 15%;text-align:left;\">" + valObj.Charge + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='###'  title='查看详细收费' class='showTD' style=\"color:blue; float:right;\">详情Ｖ</a></td>");
                var $td5 = $("<td style=\"width: 9%\">" + valObj.WeiFenValue + "</td>");
                var $charlist = $("<input type='hidden' class='hiddenDetail' value=\"" + getlist(valObj.Chargelist) + "\">");
                var $td51 = $("<td style=\"width: 9%\">" + valObj.TranBuild + "</td>");
                var $td6 = $("<td style=\"width:9%\">" + valObj.ToOtherValue + "</td>");
                var $td11 = $("<td style=\"width: 9%\">" + valObj.HavcCount + "</td>");
                var $td7 = $("<td style=\"width: 9%\">" + valObj.ActualValue + "</td>");
                // Charge - ToOtherValue - HavcCount - TranBuild
                var $td10 = $("<td style=\"width: 5%\">" + valObj.Mark + "</td>");
                $tr.append($td1);
                $tr.append($td2);
                $tr.append($td3);
                $tr.append($td5);
                $tr.append($td51);
                $tr.append($td6);
                $tr.append($td11);
                $tr.append($td7);

                $tr.append($td10);
                $tr.append($char);
                $tr.append($charlist);
                $("#AddTable").append($tr);
                v++;

            })
        });


    });
    $(".showTD").live("click", function () {
        var str = $(this).parent().parent().find('.hiddenDetail').val();
        var strcharge = $(this).parent().parent().find('.hiddencharge').val();
        if (strcharge == "0") {
            alert("没有收费！");
        }
        else {
            $(this).parent().html(str + "<a href='###' class='hideTD' style=\"color:blue; float:right; \" >收起Λ</a>")
        }
        $(".hideTD").live("click", function () {
            var charge = $(this).parent().parent().find(".hiddencharge").val();
            $(this).parent().html(charge + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='###'  title='查看详细收费' class='showTD' style=\"color:blue;float:right;\">详情Ｖ</a>");
        });
    });
    //提示
    //$(".cls_Column_Short").tipsy({ opacity: 1, live: true });
});


function Getstring(item) {
    if (item == 0.00) {
        return 0;
    } else {
        return item;
    }
}
function getlist(temp) {
    var str = "";
    if (temp != null) {
        var array = null;
        try {
            array = temp.split('+');
        } catch (e) {
            //
        }
        for (var i = 0; i < array.length; i++) {
            str += "第" + (i + 1) + "次收费<br/>收款"
            str += array[i] == null ? 0 : array[i] + "万元<br/>"
        }
    }
    return str;
}