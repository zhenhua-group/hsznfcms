﻿$(document).ready(function () {
    $("#gv_Coperation tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //关闭
    $("#btn_close").click(function () {
        if (window.opener != undefined) {
            //for  chrom
            window.opener.returnValue = $("#hid_isinput").val();
        }
        window.returnValue = $("#hid_isinput").val();
        window.close();
    });

    //收款
    $("#btn_addcount").click(function () {
        //验证
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var paycount = $("#txt_payCount").val();
//        if ($("#hid_iscomplete").val() == "1") {
//            alert("已完成入账，无需入账！");
//            return false;
//        }
        if (!reg.test(paycount)) {
            $("#txt_paycount_valide").text("请输入数字！").show();
            return false;
        } 
//        else if (parseFloat(paycount) > parseFloat($("#lbl_nopaycount").text())) {
//            $("#txt_paycount_valide").text("请确认支付金额不超过未支付金额！").show();
//            return false;
//        }
        else {
            $("#txt_paycount_valide").hide();
        }
        //时间
        if ($("#txt_times").val() == "") {
            $("#txt_time_valide").show();
            return false;
        }
        else {
            $("#txt_time_valide").hide();
        }

        //汇款人
        if ($("#txt_remitter").val().length == 0) {
            $("#txt_remitter_valide").show();
            return false;
        } else {
            $("#txt_remitter_valide").hide();
        }

        //添加收款信息
        var projid = $("#hid_projID").val();
        var paycount = $("#txt_payCount").val();
        var time = $("#txt_times").val();
        var mark = $("#txt_mark").val();
        var remitter = $("#txt_remitter").val();
        var curuser = $("#hid_curuser").val();
        var url = "../../HttpHandler/ProjectChargeHandler.ashx";
        var data = { "action": "add", "projid": "" + projid + "", "paycount": "" + paycount + "", "time": "" + time + "", "mark": "" + mark + "", "remitter": remitter, "curuser": curuser };

        $.post(url, data, function (rlt) {
            if (rlt == "0") {
                alert("添加项目收费成功！");
                var url = document.location.href;
                if (url == "") {
                    url = "AddProjCharge.aspx.aspx?cprid=" + $("#hid_projID").val() + "&math=" + Math.random();
                }
                else {
                    url = url.replace(/###/, '') + "&math=" + Math.random();
                }
                $("#reload").attr("href", url);
                $("#reload").get(0).click();
            }
            else {
                alert("添加项目收费失败！");
            }
        });
    });
    //修改收款纪录
    $(".cls_edit").click(function () {
        //判断是否完成入账
        var isallowedit = $(this).attr("allowedit");
        if ($.trim(isallowedit) == "E") {
            alert("已经完成入账确认，不允许修改！");
            return false;
        }
        //隐藏添加，显示修改
        $("#ft_add").hide();
        $("#ft_edit").show();
        $("#btn_showadd").show();
        var chg_id = $(this).attr("alt");
        //保存收款ID
        $("#hid_chgid").val(chg_id);
        //入账金额
        var curpaycount = $(this).parent().parent().find("td").eq(1).text();
        $("#hid_tempnopay").val(curpaycount);
        //入账人
        var curpayuser = $(this).parent().parent().find("td").eq(3).text();
        //入账时间
        var curpaytime = $(this).parent().parent().find("td").eq(5).text();
        //备注
        var url = "../../HttpHandler/ProjectChargeHandler.ashx";
        var data = { 'action': 'edit_show', 'id': chg_id };
        $.get(url, data, function (rlt) {

            $("#txt_mark2").val(rlt);
        }, 'text');

        //赋值
        $("#txt_curpaycount").val(curpaycount);
        $("#txt_remitter2").val(curpayuser);
        $("#txt_paytime2").val(curpaytime);
    });
    //显示添加添加按钮后隐藏
    $("#btn_showadd").click(function () {
        $('#ft_add').show();
        $("#ft_edit").hide();
        $(this).hide();
    });
    //保存修改内容
    $("#btn_addcount0").click(function () {

        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var curpaycount = $("#txt_curpaycount").val();
        //未支付金额
        var curnopaycount = parseFloat($("#lbl_nopaycount").text()) + parseFloat($("#hid_tempnopay").val());
        if (!reg.test(curpaycount)) {
            $("#txt_paycount_valide").text("请输入数字！").show();
            return false;
        } 
//        else if (parseFloat(curpaycount) > curnopaycount) {
//            $("#sp_curpaycount").text("请确认支付金额不超过未支付金额！").show();
//            return false;
//        }
        else {
            $("#sp_curpaycount").hide();
        }
        //时间
        if ($("#txt_paytime2").val() == "") {
            $("#sp_paytime2").show();
            return false;
        }
        else {
            $("#sp_paytime2").hide();
        }

        //汇款人
        if ($("#txt_remitter2").val().length == 0) {
            $("#sp_remitter2").show();
            return false;
        } else {
            $("#sp_remitter2").hide();
        }

        //添加收款信息
        var projid = $("#hid_projID").val();
        var chgid = $("#hid_chgid").val();
        var paycount = $("#txt_curpaycount").val();
        var time = $("#txt_paytime2").val();
        var mark = $("#txt_mark2").val();
        var remitter = $("#txt_remitter2").val();
        var url = "../../HttpHandler/ProjectChargeHandler.ashx";
        var data = { "action": "edit", "projid": "" + projid + "", "paycount": "" + paycount + "", "time": "" + time + "", "mark": "" + mark + "", "remitter": remitter, "chgid": chgid };

        $.post(url, data, function (rlt) {
            if (rlt == "0") {
                alert("项目收费修改成功！");
                var url = document.location.href;
                if (url == "") {
                    url = "../AddProjCharge.aspx.aspx?cprid=" + $("#hid_projID").val() + "&math=" + Math.random();
                }
                else {
                    url = url.replace(/###/, '') + "&math=" + Math.random();
                }
                $("#reload").attr("href", url);
                $("#reload").get(0).click();
            }
            else {
                alert("项目收费修改失败！");
            }
        });
    });
    //删除
    $(".cls_del").click(function () {
        //判断是否完成入账
        var isallowdel = $(this).attr("allowdel");
        if ($.trim(isallowdel) == "E") {
            alert("已经完成入账确认，不允许删除！");
            return false;
        }
        if (!confirm("确认删除本条收款信息？")) {
            return false;
        }
        var chg_id = $(this).attr("alt");
        var row = $(this).parent().parent();
        var url = "../../HttpHandler/ProjectChargeHandler.ashx";
        var data = { 'action': 'del', 'chgid': chg_id };
        $.post(url, data, function (rlt) {
            if (rlt == "0") {
                alert('删除成功！');
                row.remove();
            }
            else {
                alert('删除失败！');
            }
        }, 'text');
    });
});