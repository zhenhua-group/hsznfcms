﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    
    //隔行变色
    $("#GridView1 tr:even").css({ background: "White" });
    //进度条
    $(".progressbar").each(function () {
        var prst = parseInt($(this).next(":hidden").val());
        $(this).progressbar({
            value: prst
        });
        $(this).attr("title", prst+"%");
    });
    //添加
    $(".cls_add").click(function () {
        var cprid = $(this).next(":hidden").val();
        var url = "AddCoperationCharge.aspx?cprid=" + cprid + "&tspan=" + Math.random();
        ShowDialogWin_CprCharge(url);
    });
    //查看
    $(".cls_chk").click(function () {
        var cprid = $(this).nextAll(":hidden").val();
        var url = "SmallShowCharge.aspx?cprid=" + cprid;
        ShowDialogWin_ChargeCheck(url);
    });
    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});
//添加计划收费弹出窗口
function ShowDialogWin_CprCharge(url) {
    var feature = "dialogWidth:600px;dialogHeight:400px;center:yes";
    var result = window.showModalDialog(url, "", feature);
    if (!result) {
        result = window.returnValue;
    }
    if (result == "1") {
        window.document.location.reload();
    }
}
//收费情况
function ShowDialogWin_ChargeCheck(url) {
    var feature = "dialogWidth:700px;dialogHeight:350px;center:yes";
    var result = window.showModalDialog(url, "", feature);
    if (!result) {
        result = window.returnValue;
    }
    if (result == "1") {
        window.document.location.reload();
    }
}