﻿
var projAllot = function () {

    var pageLoad = function () {
        //比例变化
        $(":text", $("#tbDataThree")).change(function () {

            var obj = $(this);
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //同步改变值
            changeAllotVal(obj);
        }).focus(function () {
            $(this).select();
        });
        //发送消息进行
        $("#btnSave").click(function () {

            //判断比例是否为100%
            var bli = 0;
            $(":text", $("#tbDataThree")).each(function (i, item) {
                bli += parseFloat($(item).val());
            });

            if (bli.toFixed(2) != '100.00') {
                alert("调整比例不闭合，请重新调整！");
                return false;
            }
            //保存数据
            saveProjAllot("0");
        });

        //提交数据
        $("#btnSubmit").click(function () {

            if (confirm("确定提交后，项目奖金计算数据不可修改！")) {
                //判断比例是否为100%
                var bli = 0;
                $(":text", $("#tbDataThree")).each(function (i, item) {
                    bli += parseFloat($(item).val());
                });

                if (bli.toFixed(2) != '100.00') {
                    alert("调整比例不闭合，请重新调整！");
                    return false;
                }

                //发送消息
                sendMsg();
                //保存数据
                saveProjAllot("1");
            }
        });
        //撤回
        $("#btnSaveCh").click(function () {
          
            var memArray = new Array() 
            var index = 0;
            var isAll = 0;
            $("#tbDataFour tbody tr").each(function (i, item) {

                //专业
              
               var spename = $(this).find("td").eq(0).find("a").attr("spename");
               var speid = $(this).find("td").eq(0).find("a").attr("speid");
    
                //对象
                var obj = $(this);

                var container = obj.find("td").eq(0).find("a");
             
                    container.each(function (j, item2) {
                        memArray[index] = {
                            ID: "0",
                            ProId: $("#hidProID").val(),//项目ID
                            ProKHId: $("#hidProKHID").val(), //项目考核ID
                            ProKHNameId: $("#hidKaoHeNameID").val(),// ID
                            //KHTypeId: $("#hidKaoHeTypeID").val(),
                            KHTypeId: $("#hidKaoHeNameID").val(),
                            MemID: $(this).attr("memid"),
                            MemName: $(this).attr("memname"),
                            SpeName: spename,
                            SpeID: speid
                        };

                        index++;
                    });
                 
            });

           var jsonEntity = JSON.stringify(memArray);
           var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
           //var action = "savespecmems";
           var action = "yang";
            var senderid = $("#hidUserSysNo").val();//用户系统登录id
            var khtypeid = $("#hidKaoHeTypeID").val(); //考核类型ID
            var data = { "action": action, "data": jsonEntity, "senderid": senderid, "KHtypeid": khtypeid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置部门经理人成功！");

                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
       
        $("#btnSaveChs").click(function () {

            var memArray = new Array()
            var index = 0;
            var isAll = 0;
            $("#tbDataFour tbody tr").each(function (i, item) {

                //专业

                var spename = $(this).find("td").eq(0).find("a").attr("spename");
                var speid = $(this).find("td").eq(0).find("a").attr("speid");

                //对象
                var obj = $(this);

                var container = obj.find("td").eq(0).find("a");

                container.each(function (j, item2) {
                    memArray[index] = {
                        ID: "0",
                        ProId: $("#hidProID").val(),//项目ID
                        ProKHId: $("#hidProKHID").val(), //项目考核ID
                        ProKHNameId: $("#hidKaoHeNameID").val(),// ID
                        //KHTypeId: $("#hidKaoHeTypeID").val(),
                        KHTypeId: $("#hidKaoHeNameID").val(),
                        MemID: $(this).attr("memid"),
                        MemName: $(this).attr("memname"),
                        SpeName: spename,
                        SpeID: speid
                    };

                    index--;
                });

            });

            var jsonEntity = JSON.stringify(memArray);
            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            //var action = "savespecmems";
            var action = "yang";
            var senderid = $("#hidUserSysNo").val();//用户系统登录id
            var khtypeid = $("#hidKaoHeTypeID").val(); //考核类型ID
            var data = { "action": action, "data": jsonEntity, "senderid": senderid, "KHtypeid": khtypeid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置部门经理人成功！");

                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });


        if ($("#hidAction").val() == "check") {
            $("#btnSave").attr("class", "btn btn-sm red disabled");
            $("#btnSubmit").attr("class", "btn btn-sm red disabled");
            $("#tbDataThree input").attr("disabled", "disabled");
        }

        if ($("#hidIsSubmit").val() == "True") {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();
        }
        else {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.ReadMsg();
        }

        //--------------2017年5月25日
        //修改人员比例判断  
        //$("#btnEditMemsPrt").click(function () {

            //if ($("#hidIsSubmit").val() == "True")
            //{
            //    alert("项目调整已经提交不能修改人员比例！");
            //    return false;
            //}
        //});

        //--- end----
    }
    //计算值
    var changeAllotVal = function (obj) {
        //得到专业
        var txtobj = obj;
        var speid = txtobj.attr("speid");
        //当前专业比例
        var spebl = parseFloat(txtobj.val());
        //计算主持人绩效系数
        var zcrjxjj = 0;
        $("#tbDataThree tbody tr").each(function (i, item) {
            var trobj = $(this);
            //专业主持人绩效
            var zcrjx = parseFloat(trobj.find("td").eq(2).find("span").text());
            if (trobj.find("input").val() != '') {

                var speprt = parseFloat(trobj.find("input").val());
                zcrjxjj += zcrjx * speprt * 0.01;
            }
        });
        //主持人绩效
        $("#tbDataThree tbody tr").eq(0).find("td").eq(3).find("span").text(zcrjxjj.toFixed(2));
        $("#tbDataThree tbody tr").eq(0).find("td").eq(4).find("span").text(zcrjxjj.toFixed(2));
        $("#spZcjx").text(zcrjxjj.toFixed(2));

        //实际总奖金
        var speprojsjcount = parseFloat($("#spSjjjzs").text());
        //主持人资金计提比例
        var spZczjjtbl = parseFloat($("#spZczjjtbl").text());
        //方案奖金
        var spFashjj = parseFloat($("#spFashjj").text());
        //主持人奖金计算
        var spzcrjx = speprojsjcount * spZczjjtbl * 0.01 * zcrjxjj;
        $("#spZcjj").text(spzcrjx.toFixed(2));
        $("#spZcjj2").text(spzcrjx.toFixed(2));
        //项目组奖金
        var spXmjj = speprojsjcount - spFashjj - spzcrjx;
        $("#spXmjj").text(spXmjj.toFixed(2));
        $("#spXmjj2").text(spXmjj.toFixed(0));
        //项目总奖金
        var projcount = parseFloat($("#spXmjj").text());
        //专业总奖金
        var speprojcount = projcount * spebl * 0.01;
        //方案深化计提比例
        var spFashjtbl = parseFloat($("#spFashjtbl").text());
        //主持人绩效
        var spZcjx = parseFloat($("#spZcjx").text());
        //实际项目总奖金
        var sjProjCount = parseFloat($("#spSjjjzs").text());

        //专业个人比例划分
        var table = $("tr[speid='" + speid + "']", $("#tbDataFour")).find("td").eq(5).children("table");
        $(table).find("tbody").children("tr").each(function (i, item) {
            var td = $(item).find("td");
            //制图
            var ztbl = $(item).find("td").eq(1).find("span").attr("colbl");
            var ztcelval = $(item).find("td").eq(1).find("span").attr("celval");
            var ztrlt = parseFloat(ztbl) * 0.01 * parseFloat(ztcelval) * speprojcount * 0.01;
            $(item).find("td").eq(1).find("span").text(Math.round(ztrlt));
            //校对
            var xdbl = $(item).find("td").eq(2).find("span").attr("colbl");
            var xdcelval = $(item).find("td").eq(2).find("span").attr("celval");
            var xdrlt = parseFloat(xdbl) * 0.01 * parseFloat(xdcelval) * speprojcount * 0.01;
            $(item).find("td").eq(2).find("span").text(Math.round(xdrlt));
            //审核
            var shbl = $(item).find("td").eq(3).find("span").attr("colbl");
            var shcelval = $(item).find("td").eq(3).find("span").attr("celval");
            var shrlt = parseFloat(shbl) * 0.01 * parseFloat(shcelval) * speprojcount * 0.01;
            $(item).find("td").eq(3).find("span").text(Math.round(shrlt));
            //方案
            var fabl = $(item).find("td").eq(4).find("span").attr("celval");
            var farlt = parseFloat(fabl) * 0.01 * spFashjtbl * sjProjCount * 0.01;
            $(item).find("td").eq(4).find("span").text(Math.round(farlt));
            //主持
            var zcbl = $(item).find("td").eq(5).find("span").attr("celval");
            var zcrlt = parseFloat(zcbl) * 0.01 * spZczjjtbl * spZcjx * sjProjCount * 0.01;
            $(item).find("td").eq(5).find("span").text(Math.round(zcrlt));
            //总奖金
            var zong = ztrlt + xdrlt + shrlt + farlt + zcrlt;
            $(item).find("td").eq(6).find("span").text(Math.round(zong));
        });

        //建筑专业每次都需要计算
        if (speid != "1") {
            var table = $("tr[speid='1']", $("#tbDataFour")).find("td").eq(5).children("table");
            $(table).find("tbody").children("tr").each(function (i, item) {
                var td = $(item).find("td");
                //制图
                var ztrlt = parseFloat($(item).find("td").eq(1).find("span").text());
                //校对
                var xdrlt = parseFloat($(item).find("td").eq(2).find("span").text());
                //审核
                var shrlt = parseFloat($(item).find("td").eq(3).find("span").text());
                //方案
                var fabl = $(item).find("td").eq(4).find("span").attr("celval");
                var farlt = parseFloat(fabl) * 0.01 * spFashjtbl * sjProjCount * 0.01;
                $(item).find("td").eq(4).find("span").text(Math.round(farlt));
                //主持
                var zcbl = $(item).find("td").eq(5).find("span").attr("celval");
                var zcrlt = parseFloat(zcbl) * 0.01 * spZczjjtbl * spZcjx * sjProjCount * 0.01;
                $(item).find("td").eq(5).find("span").text(Math.round(zcrlt));
                //总奖金
                var zong = ztrlt + xdrlt + shrlt + farlt + zcrlt;
                $(item).find("td").eq(6).find("span").text(Math.round(zong));
            });
        }
    }
    //保存项目分配基本数据
    var saveProjAllot = function (stat) {
        //实体
        var entityData = {
            ID: "0",
            ProId: $("#hidProID").val(),
            ProKHId: $("#hidProKHID").val(),
            ProKHNameId: $("#hidKaoHeNameID").val(),
            Xnzcz: $("#spXnzcz").text(),
            Xnzcz2: $("#spXnzcz2").text(),
            WcBl: $("#spWcBl").text(),
            Xmjjjs: $("#spXmjjjs").text(),
            Xmjjjs2: $("#spXmjjjs2").text(),
            Jjbl: $("#spJjbl").text(),
            Jsjjzs: $("#spJsjjzs").text(),
            Jsjjzs2: $("#spJsjjzs2").text(),
            Xmztjx: $("#spXmztjx").text(),
            Sjjjzs: $("#spSjjjzs").text(),
            Sjjjzs2: $("#spSjjjzs2").text(),
            Fashjtbl: $("#spFashjtbl").text(),
            Fashjj: $("#spFashjj").text(),
            Fashjj2: $("#spFashjj2").text(),
            Zczjjtbl: $("#spZczjjtbl").text(),
            Zcjx: $("#spZcjx").text(),
            Zcjj: $("#spZcjj").text(),
            Zcjj2: $("#spZcjj2").text(),
            Xmjj: $("#spXmjj").text(),
            Xmjj2: $("#spXmjj2").text(),
            Stat: stat
        };

        var jsonDataProj = JSON.stringify(entityData);
        //专业信息
        var speArr = new Array()

        $("#tbDataTwo tbody tr").each(function (i, item) {

            speArr[i] = {
                ID: "0",
                ProKHNameId: $("#hidKaoHeNameID").val(),
                AllotID: "0",
                SpeID: $(item).find("td").eq(0).find("span").attr("speid"),
                SpeName: $(item).find("td").eq(0).find("span").text(),
                Persentold: $(item).find("td").eq(1).find("span").text(),
                Persent1: $(item).find("td").eq(2).find("span").text(),
                Persent2: $(item).find("td").eq(3).find("span").text(),
                AllotCount: $(item).find("td").eq(4).find("span").text(),
                AllotCountEnd: $(item).find("td").eq(5).find("span").text(),
                ZcStr: $(item).find("td").eq(6).find("span").text(),
                XmjyStr: $(item).find("td").eq(7).find("span").text()
            };
        });

        var jsonDataSpe = JSON.stringify(speArr);

        //主持人
        var zcrArr = new Array()

        var jxcount1 = "0";
        var jxcount2 = "0";
        var index = 5;
        $("#tbDataThree tbody tr").each(function (i, item) {

            if (jxcount1 == "0") {
                jxcount1 = $(item).find("td").eq(3).find("span").text();
            }
            if (jxcount2 == "0") {
                jxcount2 = $(item).find("td").eq(4).find("span").text();
            }
            zcrArr[i] = {
                ID: "0",
                ProKHNameId: $("#hidKaoHeNameID").val(),
                AllotID: "0",
                SpeID: $(item).find("td").eq(0).find("span").attr("speid"),
                SpeName: $(item).find("td").eq(0).find("span").text(),
                Persent2: $(item).find("td").eq(1).find("span").text(),
                Jixiao: $(item).find("td").eq(2).find("span").text(),
                JixiaoCount: jxcount1,
                JixiaoCountEnd: jxcount2,
                PersentEnd: $(item).find("td").eq(index).find(":text").val()
            };
            index = 3;
        });

        var jsonDataZcr = JSON.stringify(zcrArr);

        //员工

        var memArr = new Array()
        var index = 0;
        $("#tbDataFour tbody tr").each(function (i, item) {
            var speid = $(this).attr("speid");
            var spename = $(this).attr("spename")
            var table = $("tr[speid='" + speid + "']", $("#tbDataFour")).find("td").eq(5).children("table");
            $(table).find("tbody").children("tr").each(function (j, item2) {

                memArr[index] = {
                    ID: "0",
                    ProKHNameId: $("#hidKaoHeNameID").val(),
                    AllotID: "0",
                    SpeID: speid,
                    SpeName: spename,
                    MemID: $(item2).find("td").eq(0).find("span").attr("memid"),
                    MemName: $(item2).find("td").eq(0).find("span").text(),
                    Zt: $(item2).find("td").eq(1).find("span").text(),
                    Xd: $(item2).find("td").eq(2).find("span").text(),
                    Sh: $(item2).find("td").eq(3).find("span").text(),
                    Fa: $(item2).find("td").eq(4).find("span").text(),
                    Zc: $(item2).find("td").eq(5).find("span").text(),
                    AllotCount: $(item2).find("td").eq(6).find("span").text()
                };

                index++;
            });
        });

        var jsonDataMem = JSON.stringify(memArr);

        var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
        var action = "saveprojallot";
        var data = { "action": action, "data": jsonDataProj, "data2": jsonDataSpe, "data3": jsonDataZcr, "data4": jsonDataMem };

        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    //设置成功！
                    if (stat == "0") {
                        alert("项目奖金计算数据保存成功!");
                        //阅读消息
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        alert("项目奖金计算数据提交成功!");
                        //阅读消息
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }

                    location.reload();
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }

   


    //发送消息
    var sendMsg = function () {
        //发消息
        var nameid = $("#hidKaoHeNameID").val();
        var proid = $("#hidProID").val();
        var prohkid = $("#hidProKHID").val();

        var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
        var action = "startdept";
        var data = { "action": action, "id": nameid, "projid": proid, "khproid": prohkid, "senderid": $("#hidUserSysNo").val() };

        $.post(url, data, function (result) {
            if (result) {
                if (result == "1") {
                    //alert("项目奖金调整成功，部门奖金划分消息已发送！");
                    $("#btnSave").attr("class", "btn btn-sm default disabled");
                }
            }
        });
    }
 
    return {
        init: function () {
            pageLoad();
        }
    }
}();