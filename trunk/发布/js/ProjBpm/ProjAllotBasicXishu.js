﻿
var xiShu = function () {

    var pageLoad = function () {

        //保存更新
        $("#btnSave").click(function () {

            var isnull = false;
            $(":text", $("#tbData")).each(function (i, item) {
                var val = $(item).val();
                //计算绩效
                if ($.trim(val) == "") {
                    isnull = true;
                }
            });

            if (isnull) {
                alert("请完整填写加权系数，总建筑师调整，优秀员工！");
                return false;
            }


            var arrDetail = new Array();
            var index = 0;
            $("#tbData tbody tr").each(function (i) {

                var obj = $(this);

                arrDetail[index++] = {
                    ID: obj.find("td").eq(1).children("span").attr("id"),
                    jianc: obj.find("td").eq(1).children("span").attr("jc"),
                    jiancName: obj.find("td").eq(1).children("span").text(),
                    bili: obj.find("td").eq(0).children("input").val()
                };
            });

            var jsonEntity = JSON.stringify(arrDetail);

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "savexishu";
            var data = { "action": action, "data": jsonEntity };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("保存成功！");
                        //按钮不可用
                        $("#btnSave").attr("class", "btn btn-sm default disabled");
                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
    }

    return {
        init: function () {
            pageLoad();
        }
    }
}();