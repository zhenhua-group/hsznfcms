﻿
var SetMems = function () {
    //人员列表选项
    var chooseUserOfTheDepartmentControl = new UserOfTheDepartmentTree($("#ChooseUserOfTherDepartmentContainer"));

    var pageLoad = function () {

        $("#btn_OK").click(function () {
            if (chooseUserOfTheDepartmentControl) {
                chooseUserOfTheDepartmentControl.SaveUser(ChooseUserOfTheDepartmentCallBack);
            }
        });

        //计算比例
        $("#tbData tbody :text").live("keyup", function () {

            var col1 = 0;
            var col2 = 0;
            var col3 = 0;
            var col4 = 0;
            var col5 = 0;
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            $(":text", $("#tbData tbody")).each(function (i, item) {

                var index = $(item).attr("colindex");
                if (index == "1") {
                    if (reg_math.test($(item).val())) {
                        col1 += parseFloat($(item).val());
                    }
                }
                else if (index == "2") {
                    if (reg_math.test($(item).val())) {
                        col2 += parseFloat($(item).val());
                    }
                }
                else if (index == "3") {
                    if (reg_math.test($(item).val())) {
                        col3 += parseFloat($(item).val());
                    }
                } else if (index == "4") {
                    if (reg_math.test($(item).val())) {
                        col4 += parseFloat($(item).val());
                    }
                } else if (index == "5") {
                    if (reg_math.test($(item).val())) {
                        col5 += parseFloat($(item).val());
                    }
                }
            });

            $("#spcount1").text(col1);
            $("#spcount2").text(col2);
            $("#spcount3").text(col3);
            $("#spcount4").text(col4);
            $("#spcount5").text(col5);

        }).live('click', function () {
            $(this).select();
        });

        //清空
        $("#btnClear").click(function () {
            $("#tbData tbody tr").remove();
        });
        //单个删除
        $("#tbData tbody a").live('click', function () {
            $(this).parent().parent().remove();
        });

        //保存
        $("#btnSave").click(function () {
            //判断是否闭合
            if ($("#hidSpeID").val() == "1") {
                if (parseFloat($("#spcount1").text()) > 100 || parseFloat($("#spcount2").text()) > 100 || parseFloat($("#spcount3").text()) > 100 || parseFloat($("#spcount4").text()) > 100 || parseFloat($("#spcount5").text()) > 100) {

                    //return confirm("人员分配比例已超过100%，是否继续保存？");
                    alert("比例超过100%，不能保存，请重新调整！");
                    return false;
                }
            }
            else {
                if (parseFloat($("#spcount1").text()) > 100 || parseFloat($("#spcount2").text()) > 100 || parseFloat($("#spcount3").text()) > 100) {
                    //return confirm("人员分配比例已超过100%，是否继续保存？");
                    alert("比例超过100%，不能保存，请重新调整！");
                    return false;
                }
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);
                //建筑专业
                if ($("#hidSpeID").val() == "1") {
                    //创建对象
                    unitArr[index] = {
                        "ID": "0",
                        "ProID": $("#hidProId").val(),
                        "ProKHID": $("#hidProKHID").val(),
                        "KaoHeNameID": $("#hidKHNameID").val(),
                        "MemID": obj.find("td").eq(0).find("span").attr("usersysno"),
                        "MemName": obj.find("td").eq(0).find("span").text(),
                        "SpeID": $("#hidSpeID").val(),
                        "SpeName": $("#hidSpeName").val(),
                        "Zt": obj.find("td").eq(1).find("input").val(),
                        "Xd": obj.find("td").eq(2).find("input").val(),
                        "Sh": obj.find("td").eq(3).find("input").val(),
                        "Fa": obj.find("td").eq(4).find("input").val(),
                        "Zc": obj.find("td").eq(5).find("input").val(),
                        "InsertUser1": $("#hidUserSysNo").val()
                    };
                }
                else {
                    //创建对象
                    unitArr[index] = {
                        "ID": "0",
                        "ProID": $("#hidProId").val(),
                        "ProKHID": $("#hidProKHID").val(),
                        "KaoHeNameID": $("#hidKHNameID").val(),
                        "MemID": obj.find("td").eq(0).find("span").attr("usersysno"),
                        "MemName": obj.find("td").eq(0).find("span").text(),
                        "SpeID": $("#hidSpeID").val(),
                        "SpeName": $("#hidSpeName").val(),
                        "Zt": obj.find("td").eq(1).find("input").val(),
                        "Xd": obj.find("td").eq(2).find("input").val(),
                        "Sh": obj.find("td").eq(3).find("input").val(),
                        "Fa": "0",
                        "Zc": "0",
                        "InsertUser1": $("#hidUserSysNo").val()
                    };
                }
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savememsprt";
            var setup = $("#hidSetup").val();
            //jixiao
            var jixiao = 0;
            if ($("#hidSpeID").val() != "1") {
                jixiao = $("#selJixiao").val();
            }
            //部门ID 
            var unitid = $("#hidUnitSysNo").val();

            var data = { "action": action, "data": jsonData, "setup": setup, "jixiao": jixiao, "unitid": unitid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("人员比例划分保存成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });

        //确认提交
        $("#btnSubmit").click(function () {
            //判断是否闭合
            if ($("#hidSpeID").val() == "1") {
                if (parseFloat($("#spcount1").text()) > 100 || parseFloat($("#spcount2").text()) > 100 || parseFloat($("#spcount3").text()) > 100 || parseFloat($("#spcount4").text()) > 100 || parseFloat($("#spcount5").text()) > 100) {
                    //return confirm("人员分配比例已超过100%，是否继续保存？");
                    alert("比例超过100%，不能保存，请重新调整！");
                    return false;
                }
            }
            else {
                if (parseFloat($("#spcount1").text()) > 100 || parseFloat($("#spcount2").text()) > 100 || parseFloat($("#spcount3").text()) > 100) {
                    //return confirm("人员分配比例已超过100%，是否继续保存？");
                    alert("比例超过100%，不能保存，请重新调整！");
                    return false;
                }
            }
            //确认提交
            if (!confirm('确认要提交？')) {
                return false;
            }
            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);

                //建筑专业
                if ($("#hidSpeID").val() == "1") {
                    //创建对象
                    unitArr[index] = {
                        "ID": "0",
                        "ProID": $("#hidProId").val(),
                        "ProKHID": $("#hidProKHID").val(),
                        "KaoHeNameID": $("#hidKHNameID").val(),
                        "MemID": obj.find("td").eq(0).find("span").attr("usersysno"),
                        "MemName": obj.find("td").eq(0).find("span").text(),
                        "SpeID": $("#hidSpeID").val(),
                        "SpeName": $("#hidSpeName").val(),
                        "Zt": obj.find("td").eq(1).find("input").val(),
                        "Xd": obj.find("td").eq(2).find("input").val(),
                        "Sh": obj.find("td").eq(3).find("input").val(),
                        "Fa": obj.find("td").eq(4).find("input").val(),
                        "Zc": obj.find("td").eq(5).find("input").val(),
                        "InsertUser1": $("#hidUserSysNo").val(),
                        "Stat": "1"
                    };
                }
                else {
                    unitArr[index] = {
                        "ID": "0",
                        "ProID": $("#hidProId").val(),
                        "ProKHID": $("#hidProKHID").val(),
                        "KaoHeNameID": $("#hidKHNameID").val(),
                        "MemID": obj.find("td").eq(0).find("span").attr("usersysno"),
                        "MemName": obj.find("td").eq(0).find("span").text(),
                        "SpeID": $("#hidSpeID").val(),
                        "SpeName": $("#hidSpeName").val(),
                        "Zt": obj.find("td").eq(1).find("input").val(),
                        "Xd": obj.find("td").eq(2).find("input").val(),
                        "Sh": obj.find("td").eq(3).find("input").val(),
                        "Fa": "0",
                        "Zc": "0",
                        "InsertUser1": $("#hidUserSysNo").val(),
                        "Stat": "1"
                    };

                }
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savememsprt";
            var senderid = $("#hidUserSysNo").val();
            var setup = $("#hidSetup").val();
            //jixiao
            var jixiao = 0;
            if ($("#hidSpeID").val() != "1") {
                jixiao = $("#selJixiao").val();
            }
            //部门ID 
            var unitid = $("#hidUnitSysNo").val();

            var data = { "action": action, "data": jsonData, "senderid": senderid, "setup": setup, "jixiao": jixiao, "unitid": unitid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        if ($("#hidSpeID").val() == "1") {
                            if ($("#hidSetup").val() == "1") {
                                alert("人员比例设置提交成功，请部门经理审批！");
                            }
                            else if ($("#hidSetup").val() == "2") {
                                alert("人员比例设置提交成功，请总建筑师审批！");
                            }
                            else {
                                alert("建筑专业内人员、比例审核流程完成！");
                            }
                        } else if ($("#hidSpeID").val() == "2") {
                            if ($("#hidSetup").val() == "1") {
                                alert("人员比例设置提交成功，请部门经理审批！");
                            }
                            else {
                                alert("结构专业内人员、比例审核流程完成！");
                            }

                        } else if ($("#hidSpeID").val() == "3") {
                            if ($("#hidSetup").val() == "1") {
                                alert("人员比例设置提交成功，请部门经理审批！");
                            }
                            else {
                                alert("暖通专业内人员、比例审核流程完成！");
                            }
                        } else if ($("#hidSpeID").val() == "4") {
                            if ($("#hidSetup").val() == "1") {
                                alert("人员比例设置提交成功，请部门经理审批！");
                            }
                            else {
                                alert("给排水专业内人员、比例审核流程完成！");
                            }
                        } else if ($("#hidSpeID").val() == "5") {
                            if ($("#hidSetup").val() == "1") {
                                alert("人员比例设置提交成功，请部门经理审批！");
                            }
                            else {
                                alert("电气专业内人员、比例审核流程完成！");
                            }
                        }
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();

                     //   location.reload();
                        location.replace(location.href);
                    }
                    else if (result == "2") {
                        alert("部门未设置部门经理不能提交！");
                        return false;
                    }
                    else if (result == "3") {
                        alert("部门未设置总建筑师不能提交！");
                        return false;
                    }
                }
                else {
                    //设置成功！
                    alert("提交失败请重试！");
                }
            });
        });

        //点击添加绑定一个事件
        $("a[action]").bind('click', function () {
            //全部隐藏

            $("#t_u_弘石产值分配系统 li").hide();

            var speid = $("#hidSpeID").val();
            var unitid = $("#hidUnitSysNo").val();
            //建筑
            if (speid == "1") {
                //建筑一部
                $("#t_u_231", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_231 li", "#ChooseUserOfTheDepartmentMain").show();
                //建筑二部
                $("#t_u_232", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_232 li", "#ChooseUserOfTheDepartmentMain").show();
                //特务一部
                $("#t_u_233", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_233 li", "#ChooseUserOfTheDepartmentMain").show();
                //特务二部
                $("#t_u_234", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_234 li", "#ChooseUserOfTheDepartmentMain").show();
                //研发
                $("#t_u_235", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_235 li", "#ChooseUserOfTheDepartmentMain").show();
                //室内
                $("#t_u_236", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_236 li", "#ChooseUserOfTheDepartmentMain").show();
                //经理办公室
                $("#t_u_230", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_230 li", "#ChooseUserOfTheDepartmentMain").show();
                //总工办
                $("#t_u_242", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_242 li", "#ChooseUserOfTheDepartmentMain").show();
                //特务三部
                $("#t_u_281", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_281 li", "#ChooseUserOfTheDepartmentMain").show();
                //西安分公司
                $("#t_u_282", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_282 li", "#ChooseUserOfTheDepartmentMain").show();
            }
            else if(unitid=="282")//西安分公司
            {
                //建筑一部
                $("#t_u_231", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_231 li", "#ChooseUserOfTheDepartmentMain").show();
                //建筑二部
                $("#t_u_232", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_232 li", "#ChooseUserOfTheDepartmentMain").show();
                //特务一部
                $("#t_u_233", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_233 li", "#ChooseUserOfTheDepartmentMain").show();
                //特务二部
                $("#t_u_234", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_234 li", "#ChooseUserOfTheDepartmentMain").show();
                //研发
                $("#t_u_235", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_235 li", "#ChooseUserOfTheDepartmentMain").show();
                //室内
                $("#t_u_236", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_236 li", "#ChooseUserOfTheDepartmentMain").show();
                //结构
                $("#t_u_237", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_237 li", "#ChooseUserOfTheDepartmentMain").show();
                //设备
                $("#t_u_238", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_238 li", "#ChooseUserOfTheDepartmentMain").show();
                //电气
                $("#t_u_239", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_239 li", "#ChooseUserOfTheDepartmentMain").show();
                //经理办公室
                $("#t_u_230", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_230 li", "#ChooseUserOfTheDepartmentMain").show();
                //总工办
                $("#t_u_242", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_242 li", "#ChooseUserOfTheDepartmentMain").show();
                //特务三部
                $("#t_u_281", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_281 li", "#ChooseUserOfTheDepartmentMain").show();
                //西安分公司
                $("#t_u_282", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_282 li", "#ChooseUserOfTheDepartmentMain").show();
            }
            else {
                //总工办
                $("#t_u_242", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_242 li", "#ChooseUserOfTheDepartmentMain").show();

                $("#t_u_" + unitid + "", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_" + unitid + " li", "#ChooseUserOfTheDepartmentMain").show();
                //经理办公室
                $("#t_u_230", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_230 li", "#ChooseUserOfTheDepartmentMain").show();

                //西安分公司
                $("#t_u_282", "#ChooseUserOfTheDepartmentMain").show().parent().show();
                $("#t_u_282 li", "#ChooseUserOfTheDepartmentMain").show();
            }

        });

        //如果考核名称已经删除
        if ($("#hidKaoHeNameID").val() == "0") {

            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.ReadMsg();

            $("#btnSave").hide()
            $("#btnSubmit").hide();
        }

        if ($("#hidIsSubmit").val() == "True") {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();
        }
        else {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.ReadMsg();
        }
    }

    //选择用户回掉
    var ChooseUserOfTheDepartmentCallBack = function (userArray) {
        //结果表
        var showResultTable = $("#tbData tbody");

        var rowString = "";
        $.each(userArray, function (index, item) {

            var trArr = showResultTable.find("tr");
            //屏蔽已存在的人员 2016-8-24
            var isExsit = true;
            $.each(trArr, function (i, item2) {
                var userID = $(item2).find("td").eq(0).find("span").attr("usersysno");
                //如果已经存在
                if (item.userSysNo == userID) {
                    isExsit = false;
                    return false;
                }
            });

            if (isExsit) {
                if ($("#hidSpeID").val() == "1") {//建筑
                    rowString += "<tr>";
                    rowString += "<td><span class=\"badge badge-info\" userspecialtyno=\"" + item.specialtyno + "\" userSysNo=\"" + item.userSysNo + "\"  userSpecialtyName=\"" + item.specialtyName + "\">" + item.userName + "</span></td>";
                    rowString += "<td><input colindex=\"1\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><input colindex=\"2\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><input colindex=\"3\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><input colindex=\"4\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><input colindex=\"5\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><a href=\"#\" class=\"btn default btn-xs red-stripe\" action=\"del\">删除</a></td></tr>";
                }
                else {
                    rowString += "<tr>";
                    rowString += "<td><span class=\"badge badge-info\" userspecialtyno=\"" + item.specialtyno + "\" userSysNo=\"" + item.userSysNo + "\"  userSpecialtyName=\"" + item.specialtyName + "\">" + item.userName + "</span></td>";
                    rowString += "<td><input colindex=\"1\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><input colindex=\"2\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><input colindex=\"3\" style=\"width:80px;\" name=\"name\" type=\"text\" value=\"0\"/>%</td>";
                    rowString += "<td><a href=\"#\" class=\"btn default btn-xs red-stripe\" action=\"del\">删除</a></td></tr>";
                }
            }
        });
        showResultTable.append(rowString);
    }

    return {
        init: function () {
            pageLoad();
        }
    }
}();