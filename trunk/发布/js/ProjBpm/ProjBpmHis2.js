﻿
var ProjHis = function () {

    var pageLoad = function () {

        $("#tbNameList tbody a[action='check']").bind('click', function () {

            var id = $(this).attr("pronameid");
            loadsepcmems(id);
        });
        //发起审批
        $("#tbNameList tbody a[action='start']").click(function () {

            var obj = $(this);
            //不可用
            obj.attr("class", "btn default btn-xs red-stripe disabled");

            var colcount = obj.attr("colcount");
            if (parseInt(colcount) != 11) {
                alert("部门经理或负责人没有设置完成，不能发起审批！");
                return false;
            }
            //取得信息
            var nameid = $(this).attr("pronameid");
            var proid = $(this).attr("proid");
            var prohkid = $(this).attr("prokhid");

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "startpromsg";
            var data = { "action": action, "id": nameid, "projid": proid, "khproid": prohkid, "senderid": $("#hidUserSysNo").val() };

            $.post(url, data, function (result) {
                if (result) {
                    if (result == "1") {
                        alert("主持人，专业负责人消息发送成功！");
                        //状态
                        obj.parent().prev("td").find("span").attr("class", "badge badge-success").text("已发起");
                        obj.attr("class", "btn default btn-xs red-stripe disabled");
                    }
                    else if (result == "2") {
                        alert("请设置人力资源主管！消息发送失败！");
                        return false;
                    }
                }
            });
        });

        //全选
        $("#chekALL").click(function () {
            var obj = $(this);

            if (!obj.get(0).checked) {
                $(":checkbox").not(this).attr("checked", !obj.checked).parent().attr("class", "");
            }
            else {
                $(":checkbox").not(this).attr("checked", true).parent().attr("class", "checked");
            }
        });

        //批量发起
        $("#btnSendMulti").click(function () {

            var chekList = $("#tbNameList tbody :checkbox").parent("span[class]");

            if (chekList.length == 0) {
                alert("请选择要发起的项！");
                return false;
            }
            //批量发起
            chekList.each(function (i) {
                //找到发送按钮
                var obj = $(this).parents("td").siblings().eq(14).find("a");

                //批量发送
                var sendcount = 0;
                if (!(obj.attr("class").indexOf("disabled") > -1)) {
                    var colcount = obj.attr("colcount");
                    if (parseInt(colcount) != 11) {
                        //alert("部门经理或负责人没有设置完成，不能发起审批！");
                        return true;
                    }
                    //取得信息
                    var nameid = obj.attr("pronameid");
                    var proid = obj.attr("proid");
                    var prohkid = obj.attr("prokhid");

                    var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
                    var action = "startpromsg";
                    var data = { "action": action, "id": nameid, "projid": proid, "khproid": prohkid, "senderid": $("#hidUserSysNo").val() };

                    $.post(url, data, function (result) {
                        if (result) {
                            if (result == "1") {
                                //alert("主持人，专业负责人消息发送成功！");
                                //状态
                                obj.parent().prev("td").find("span").attr("class", "badge badge-success").text("已发起");
                                obj.attr("class", "btn default btn-xs red-stripe disabled");

                                sendcount++;


                            }
                            else if (result == "2") {
                                //alert("请设置人力资源主管！消息发送失败！");
                                return true;
                            }
                        }
                    });
                }

                //alert("主持人，专业负责人消息批量发送成功" + sendcount + "条");

            });

            //发送成功
            alert("主持人，专业负责人消息批量发送成功!");
            $("#ctl00_ContentPlaceHolder1_btn_Search").get(0).click();

        });
    }
    //加载设置的人员
    var loadsepcmems = function (id) {
        //考核类型ID
        var typeid = id;
        //
        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "getspecmems3";

        var data = { "action": action, "id": typeid };

        $.get(url, data, function (result) {

            if (result) {
                //专业人员
                var mems = eval("(" + result + ")");
                $("#tbSetMems tbody tr").each(function (i) {

                    var obj = $(this);
                    var speid = obj.find("td").eq(0).find("span").attr("speid");
                    //如果是同一专业
                    if (obj.find("td").eq(1).find("a").length > 0) {
                        obj.find("td").eq(1).find("a").remove();
                    }
                    $.each(mems, function (i, item) {
                        if (speid == item.SpeID) {
                            var html = "<a href=\"#\" title=\"点击删除\" class=\"btn btn-xs blue\" action=\"deluser\" id=\"" + item.ID + "\" memname=\"" + item.MemName + "\" memid=\"" + item.MemID + "\">" + item.MemName + "<i class=\"fa fa-times\"></i></a>";
                            obj.find("td").eq(1).append(html);
                        }
                    });
                });

            }
        });
    }

    //var initOrderTable = function () {

    //    $("#tbNameList").dataTable({
    //        "aoColumnDefs": [
    //            { "bSortable": false, "aTargets": [0] },
    //            { "bSortable": true, "aTargets": [1] },
    //            { "bSortable": false, "aTargets": [2] },
    //            { "bSortable": false, "aTargets": [3] },
    //            { "bSortable": false, "aTargets": [4] },
    //            { "bSortable": false, "aTargets": [5] },
    //            { "bSortable": false, "aTargets": [6] },
    //            { "bSortable": false, "aTargets": [7] },
    //            { "bSortable": false, "aTargets": [8] },
    //            { "bSortable": false, "aTargets": [9] },
    //            { "bSortable": false, "aTargets": [10] },
    //            { "bSortable": false, "aTargets": [11] },
    //            { "bSortable": false, "aTargets": [12] },
    //            { "bSortable": false, "aTargets": [13] },
    //            { "bSortable": false, "aTargets": [14] },
    //            { "bSortable": false, "aTargets": [15] },
    //            { "bSortable": false, "aTargets": [16] }
    //        ],
    //        "iDisplayLength": 15
    //    });


    //    jQuery('#tbNameList_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
    //    jQuery('#tbNameList_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
    //    //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    //}


    return {
        init: function () {
            pageLoad();

            //initOrderTable();
        }
    }
}();