﻿
$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectStatistics/ProjectScheduleListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '项目名称', '承接部门', '执行设总', '项目状态', '开始时间', '结束时间', '项目进行阶段', '完成图纸数量'],
        colModel: [
                             { name: 'id', index: 'id', width: 45, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_Name', index: 'a.pro_Name', width: 320 },
                             { name: 'Unit', index: 'Unit', width: 80 },
                             { name: 'PMName', index: 'PMName', width: 80 },
                             { name: 'pro_Status', index: 'pro_Status', width: 80 },
                             { name: 'pro_StartTime', index: 'pro_StartTime', width: 80, align: 'center' },
                             { name: 'pro_FinishTime', index: 'pro_FinishTime', width: 80, align: 'center' },
                             { name: 'ProjectCurrentStatus', index: 'ProjectCurrentStatus', width: 100, align: 'center' },
                             { name: 'DrawingCount', index: 'DrawingCount', width: 150 }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "sel", 'unit': $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text(), 'year': $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'a.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        loadComplete: loadCompMethod
    });

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectStatistics/ProjectScheduleListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'a.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectStatistics/ProjectScheduleListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'a.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<img src="../Images/proj.png" style="width: 16px; height: 16px; border: none;" /><a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}

//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}