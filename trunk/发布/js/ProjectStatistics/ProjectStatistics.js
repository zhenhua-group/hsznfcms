﻿var BackgroundInvoke = TG.Web.ProjectStatistics.ProjectCountList;

function GetProjectCountByQuarter(quarter, year) {
    var result = BackgroundInvoke.GetProjectCountByQuarter(quarter + "", year + "");

    if (result.value.length > 0) {
        var tableString = "";
        tableString += "<table class=\"cls_content_head\">";
        tableString += "<tr>";
        tableString += "<td style=\"width: 70%\" align=\"center\">";
        tableString += "部门";
        tableString += "</td>";
        tableString += "<td style=\"width: 30%;\" align=\"center\">";
        tableString += "项目数量";
        tableString += "</td>";
        tableString += "</tr>";
        tableString += "</table>";
        tableString += "<table class=\"show_projectNumber\" id=\"resultTable\">" + result.value + "</table>"

        ClearPopAreaContent();
        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv("项目数量季度详情");
    }
}


function GetProjectCountByMonth(year, month) {
    var result = BackgroundInvoke.GetProjectCountByMonth(year, month);

    if (result.value.length > 0) {
        var tableString = "";
        tableString += "<table class=\"cls_content_head\">";
        tableString += "<tr>";
        tableString += "<td style=\"width: 70%\" align=\"center\">";
        tableString += "部门";
        tableString += "</td>";
        tableString += "<td style=\"width: 30%;\" align=\"center\">";
        tableString += "项目数量";
        tableString += "</td>";
        tableString += "</tr>";
        tableString += "</table>";
        tableString += "<table class=\"show_projectNumber\" id=\"resultTable\">" + result.value + "</table>"

        ClearPopAreaContent();
        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv("项目数量" + month + "月详情");
    }
}

function GetProjectTypeDetailByYearAndType(year, projectTypeInt) {
    var projectTypeString = projectTypeInt.toString();
    var result = BackgroundInvoke.GetProjectTypeDetailByYearAndType(year, projectTypeString);
    if (result.value.length > 0) {
        var tableString = "";
        tableString += "<table class=\"cls_content_head\">";
        tableString += "<tr>";
        tableString += "<td style=\"width: 70%\" align=\"center\">";
        tableString += "部门";
        tableString += "</td>";
        tableString += "<td style=\"width: 30%;\" align=\"center\">";
        tableString += "项目数量";
        tableString += "</td>";
        tableString += "</tr>";
        tableString += "</table>";
        tableString += "<table class=\"show_projectNumber\" id=\"resultTable\">" + result.value + "</table>";

        ClearPopAreaContent();

        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv(projectTypeString + "项目");
    }
}

function GetProjectTypeDetailByMonthAndYear(year, month) {
    var result = BackgroundInvoke.GetProjectTypeDetailByMonthAndYear(year, month);

    if (result.value.length > 0) {
        var tableString = "";
        tableString += "<table class=\"cls_content_head\">";
        tableString += "<tr>";
        tableString += "<td align=\"center\" style=\"width:10%;\">部门</td>";
        tableString += "<td style=\"width:15%;\" align=\"center\">建筑工程</td>";
        tableString += "<td style=\"width:15%;\" align=\"center\">城乡规划</td>";
        tableString += "<td style=\"width:15%;\" align=\"center\">市政工程</td>";
        tableString += "<td style=\"width:15%;\" align=\"center\">工程勘察</td>";
        tableString += "<td style=\"width:15%;\" align=\"center\">咨询</td>";
        tableString += "<td style=\"width:15%;\" align=\"center\">其他</td>";
        tableString += "</tr>";
        tableString += "</table>";
        tableString += "<table class=\"show_projectNumber\" id=\"resultTable\">" + result.value + "</table>";

        ClearPopAreaContent();

        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv(month + "月份项目分布");
    }
}

function GetProjectDetailSquareMetersByQuarter(year, quarter) {
    var result = BackgroundInvoke.GetProjectDetailSquareMetersByQuarter(year, quarter);

    if (result.value.length > 0) {
        var tableString = "";
        tableString += "<table class=\"cls_content_head\">";
        tableString += "<tr>";
        tableString += "<td style=\"width: 270px\" align=\"center\">";
        tableString += "项目名称";
        tableString += "</td>";
        tableString += "<td style=\"width: 30%;\" align=\"center\">";
        tableString += "平米数";
        tableString += "</td>";
        tableString += "<td style=\"width:20%;\" align=\"center\">所属部门</td>";
        tableString += "</tr>";
        tableString += "</table>";
        tableString += "<table class=\"show_projectNumber\" id=\"resultTable\">" + result.value + "</table>";
        ClearPopAreaContent();
        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv("第" + quarter + "季度");
    }
}

function GetProjectDetailSquareMetersByMonth(year, month) {
    var result = BackgroundInvoke.GetProjectDetailSquareMetersByMonth(year, month);

    if (result.value.length > 0) {
        var tableString = "";
        tableString += "<table class=\"cls_content_head\" style=\"width:100%;\">";
        tableString += "<tr>";
        tableString += "<td style=\"width:270px\" align=\"center\">";
        tableString += "项目名称";
        tableString += "</td>";
        tableString += "<td style=\"width: 30%;\" align=\"center\">";
        tableString += "平米数";
        tableString += "</td>";
        tableString += "<td style=\"width:20%;\" align=\"center\">所属部门</td>";
        tableString += "</tr>";
        tableString += "</table>";
        tableString += "<table class=\"show_projectNumber\" style=\"width:100%;\" id=\"resultTable\">" + result.value + "</table>";

        ClearPopAreaContent();

        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv("第" + month + "月份");
    }
}

function GetProjectDetailStatus(year, statusInt) {
    var stautsString = "";
    var timeString = "";
    switch (statusInt) {
        case 1:
            stautsString = "进行中";
            timeString = "开始时间";
            break;
        case 2:
            stautsString = "暂停";
            break;
        case 3:
            stautsString = "完成";
            timeString = "结束时间";
            break;
    }

    var result = BackgroundInvoke.GetProjectDetailByStatus(year, statusInt);

    if (result.value.length > 0) {
        var tableString = "";
        tableString += "<table class=\"cls_content_head\">";
        tableString += "<tr>";
        tableString += "<td style=\"width: 270px\" align=\"center\">";
        tableString += "项目名称";
        tableString += "</td>";
        if (statusInt != 2) {
            tableString += "<td style=\"width:270px;\" align=\"center\">" + timeString + "</td>";
        }
        tableString += "<td style=\"width: 30%;\" align=\"center\">";
        tableString += "平米数";
        tableString += "</td>";
        tableString += "<td style=\"width:20%;\" align=\"center\">所属部门</td>";
        tableString += "</tr>";
        tableString += "</table>";
        tableString += "<table class=\"show_projectNumber\" id=\"resultTable\">" + result.value + "</table>";
        ClearPopAreaContent();

        $("#PopAreaDivMain").append(tableString);
        CommonControl.SetTableStyle("resultTable", "need");
        PopAreaDiv(stautsString + "项目");
    }
}

function PopAreaDiv(title) {
    $("#PopAreaDivMain").dialog({
        autoOpen: false,
        modal: true,
        width: 750,
        height: 300,
        resizable: false,
        title: title,
        buttons:
            {
                "关闭": function () { $(this).dialog("close"); }
            }
    }).dialog("open");
}

function ClearPopAreaContent() {
    $("#PopAreaDivMain").html("");
}