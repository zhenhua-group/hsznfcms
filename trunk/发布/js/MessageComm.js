﻿//create by long 20130527
//修改流程待办状态
function MessageComm(msgID) {
    this.MsgID = msgID;
    this.ReadMsg = function () {
        //改变消息状态
        var data = { "action": "readmsg", "msgID": msgID };
        var url = "../HttpHandler/CommHandler.ashx";
        $.post(url,data);
    }

    this.DoneMsg = function () {
        //改变消息状态
        var data = { "action": "donemsg", "msgID": msgID };
        var url = "../HttpHandler/CommHandler.ashx";
        $.post(url, data);
    }
}
//项目审核
function MessageCommProj(msgID) {
    this.MsgID = msgID;
    this.ReadMsg = function () {
        //改变消息状态
        var data = { "action": "readmsg", "msgID": msgID };
        var url = "../../HttpHandler/CommHandler.ashx";
        $.post(url, data);
    }
}
//项目策划
function MessageCommProjPlan(msgID) {
    this.MsgID = msgID;
    this.ReadMsg = function () {
        //改变消息状态
        var data = { "action": "readmsg", "msgID": msgID };
        var url = "../../HttpHandler/CommHandler.ashx";
        $.post(url, data);
    }
}
//产值分配
function MessageCommProjAllot(msgID) {
    this.MsgID = msgID;
    this.ReadMsg = function () {
        //改变消息状态
        var data = { "action": "readmsg", "msgID": msgID };
        var url = "../../HttpHandler/CommHandler.ashx";
        $.post(url, data);
    }
}

//产值分配
function MessageCommProjAllot1(msgID) {
    this.MsgID = msgID;
    this.ReadMsg = function () {
        //改变消息状态
        var data = { "action": "readmsg1", "msgID": msgID };
        var url = "../../HttpHandler/CommHandler.ashx";
        $.post(url, data);
    }
}