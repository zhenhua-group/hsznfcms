﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProNumber/ProPassListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '项目名称', '提交日期', '提交人', '承接部门', '项目工程号', '工程号分配', '查看'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                              { name: 'ID', index: 'ID', hidden: true, editable: true },
                             { name: 'Pro_id', index: 'Pro_id', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 350, formatter: colNameShowFormatter },
                              { name: 'SubmitDate', index: 'SubmitDate', width: 150, align: 'center' },
                             { name: 'submituser', index: 'submituser', width: 80, align: 'center' },
                             { name: 'Unit', index: 'Unit', width: 100, align: 'center' },
                             { name: 'number', index: 'ProNumber', width: 80, align: 'center' },
                             { name: 'Pro_id', index: 'Pro_id', width: 100, align: 'center', formatter: colStatusShowFormatter },
                             { name: 'Pro_id', index: 'Pro_id', width: 50, align: 'center', formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "pronumber", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'PN.[ID]',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProNumber/ProPassListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $(".norecords").hide();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProNumber/ProPassListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'PN.[ID]',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $(".norecords").hide();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProNumber/ProPassListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'PN.[ID]',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["Pro_id"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
}

//状态
function colStatusShowFormatter(celvalue, options, rowData) {
    var pageurl = "ProNumberAllotBymaster.aspx?pro_id=" + celvalue + "&pronum_id=" + rowData["ID"];
    return '<a href="' + pageurl + '" alt="设计编号分配">设计编号分配</a>';
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看">查看</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}