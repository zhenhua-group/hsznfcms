﻿
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var proID;
var userObj;
var startAppControl; //发起审批对象

$(document).ready(function () {
    ProPassList();
    ProPassList2();
    //提交申请
    $(".apply").live("click", function () {
        messageDialog = $("#auditShow").messageDialog;
        sendMessageClass = new MessageCommon(messageDialog);
        var pro_id = $(this).attr("rel");
        var user = $(this).attr("SubUser");
        proID = pro_id;
        userObj = user;
        startAppControl = $(this);
        getUserAndUpdateAudit('0', '0', pro_id, user);
    });
    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('0', '1', proID, userObj);
        }
    });
    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#ctl00_ContentPlaceHolder1_previewPower").val();
    paramEntity.userSysNum = $("#ctl00_ContentPlaceHolder1_userSysNum").val();
    paramEntity.userUnitNum = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    paramEntity.unitID = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    paramEntity.currYear = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();
});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, proid, user) {
    //地址
    var url = "../../HttpHandler/ProNumber/NumApplyHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "pro_id": proid, "user": user };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (jsonResult == "0") {
            alert("流程审批错误，请联系管理员！");
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        //提示消息
        alert("发起工程号分配申请已成功！\n消息已成功发送到审批人等待审批！");
        //改变流程提示状态
        CommonControl.SetApplyStatus(startAppControl);
    } else {
        alert("消息发送失败！");
    }
}