﻿function ProPassList2() {
    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/ProNumber/ProPassListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '项目名称', '建设单位', '承接部门', '提交日期', '项目工号', '查看'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                              { name: 'ID', index: 'ID', hidden: true, editable: true },
                             { name: 'Pro_id', index: 'Pro_id', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 350, formatter: colNameShowFormatter2 },
                             { name: 'pro_buildUnit', index: 'pro_buildUnit', width: 300 },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'SubmitDate', index: 'SubmitDate', width: 150, align: 'center' },
                             { name: 'ProNumber', index: 'ProNumber', width: 80, align: 'center' },
                             { name: 'Pro_id', index: 'Pro_id', width: 50, align: 'center', formatter: colShowFormatter2 }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "sel2", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where2").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'PN.[ID]',
        sortorder: 'desc',
        pager: "#gridpager2",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProNumber/ProPassListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod2,
        loadComplete: loadCompMethod2
    });


    //显示查询
    $("#jqGrid2").jqGrid("navGrid", "#gridpager2", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid2").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid2').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid2').jqGrid('getCell', sel_id, 'ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where2").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProNumber/ProPassListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'PN.[ID]',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where2").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#txt_keyname").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProNumber/ProPassListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'PN.[ID]',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter2(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["Pro_id"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
}
////静态状态
//function colStrShowFormatter(celvalue, options, rowData) {
//    var str = "";
//    if (celvalue == "1" || celvalue == "2") {
//        str = '已提交申请';
//    }   
//    else {
//        str = '未提交申请';
//    }
//    return str;
//}
////状态
//function colStatusShowFormatter(celvalue, options, rowData) {
//    var str = "";
//    if (celvalue == "1") {
//        str = '<a href="###"  class="process">分配中</a>';
//    }
//    else if (celvalue == "2") {
//    str = '<a href="###" class="done">已分配</a>';
//    }
//    else {
//        str = '<a href="###" rel=' + celvalue + ' class="apply" runat="server">提交申请</a>';
//    }
//    return str;
//}

//查看
function colShowFormatter2(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看">查看</a>';

}

//统计 
function completeMethod2() {
    var rowIds = $("#jqGrid2").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid2 [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod2() {
    $("#jqGrid2").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid2").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata2").text() == '') {
            $("#jqGrid2").parent().append("<div id='nodata2'>没有符合条件数据！</div>")
        }
        else { $("#nodata2").show(); }

    }
    else {
        $("#nodata2").hide();
    }
}