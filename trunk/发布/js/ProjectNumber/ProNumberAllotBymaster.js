﻿$(document).ready(function () {
    //加载数据
    loadCoperationAttach();
    $("#btn_Save").click(function () {
        var pronumber = $("#ctl00_ContentPlaceHolder1_proNumber").val();
        //填写项目工号
        var msg = "";
        if (pronumber == "") {
            msg += "项目工号不能为空！</br >";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            return true;

        }

    });
    $("#btnfygh").click(function () {
        /*
        var url = "../../ProjectNumber/SelectProjectNum.aspx";
        var feature = "dialogWidth:310px;dialogHeight:130px;center:yes";
        var result = window.showModalDialog(url, "", feature);
        if (!result) {
        result = window.returnValue;
        }
        if ($.trim(result) != "") {
        $("#proNumber").val($.trim(result));
        }*/
        //赋值
        showDivDialogClass.SetParameters({
            "pageSize": "0"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "0", "proNum", CprNumUnitCallBack);


    });
    //分配框状态判断
    if ($.trim($("#ctl00_ContentPlaceHolder1_proNumber").val()) == "") {
        $("#btn_modify").hide();
        $("#btnfygh").show();
    }
    else {
        $("#btnfygh").hide();
        $("#btn_modify").show();
        $("#ctl00_ContentPlaceHolder1_HiddenNum").val($.trim($("#ctl00_ContentPlaceHolder1_proNumber").val()));
        //        $("#ctl00_ContentPlaceHolder1_proNumber").removeClass();
        //        $("#ctl00_ContentPlaceHolder1_proNumber").css({ "border": "none", "background-color": "#f0f0f0", "text-align": "center" });
    }
    //事件
    $("#btn_modify").toggle(function () {
        $(this).text("放弃");
        $("#btnfygh").show();
        //        $("#ctl00_ContentPlaceHolder1_proNumber").addClass("TextBoxBorder");
        //        $("#ctl00_ContentPlaceHolder1_proNumber").css({ "border": "solid #cccccc 1px", "background-color": "", "text-align": "" });
    }, function () {
        $(this).text("修改");
        $("#btnfygh").hide();
        $("#ctl00_ContentPlaceHolder1_proNumber").val($.trim($("#ctl00_ContentPlaceHolder1_HiddenNum").val()));
        //        $("#ctl00_ContentPlaceHolder1_proNumber").removeClass();
        //        $("#ctl00_ContentPlaceHolder1_proNumber").css({ "border": "none", "background-color": "#f0f0f0", "text-align": "center" });hid_projid
    });
});
function loadCoperationAttach() {
    var data = "action=getprojfiles&type=proj&projid=" + $("#ctl00_ContentPlaceHolder1_hid_projid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            var filedata = result == null ? "" : result.ds;
            if ($("#datas_att tr").length > 1) {
                $("#datas_att tr:gt(0)").remove();
            }
            //缩略图列表
            if ($("#img_container img").length > 1) {
                $("#img_container img:gt(0)").remove();
            }
            $.each(filedata, function (i, n) {
                var row = $("#att_row").clone();
                //显示
                $("#img_small").show();
                //克隆
                var thumd = $("#img_small").clone();
                //隐藏
                $("#img_small").hide();

                var oper2 = "<a href='../Attach_User/filedata/projfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";

                //显示缩略图
                var img_path = n.FileUrl.split('/');
                thumd.attr("src", "../Attach_User/filedata/projfile/" + img_path[0] + "/min/" + img_path[1]);

                row.find("#att_id").text(n.ID);
                row.find("#att_filename").attr("align", "left").html(img + n.FileName);
                row.find("#att_filesize").text(n.FileSizeString + 'KB');
                row.find("#att_filetype").text(n.FileType);
                row.find("#att_uptime").text(n.UploadTime);
                row.find("#att_oper2").html(oper2);

                //添加样式
                row.addClass("cls_TableRow");
                row.appendTo("#datas_att");
                //缩略图
                thumd.appendTo("#img_container");
                if (i % 3 == 0) {
                    $("img_container").append("<br/>")
                }
                //隐藏原始图
            });
            filedata = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//合同编号
function CprNumUnitCallBack(result) {
    var data = result == null ? "" : result.ds;
    var cpr_typeSelect_htm = '<option value="-1">--------请选择--------</option>';
    $.each(data, function (i, n) {
        cpr_typeSelect_htm += '<option value="' + n.ID + '">' + n.ProType + '</option>';
    });
    $("#cpr_typeSelect").html(cpr_typeSelect_htm);
    $('#cpr_numSelect').empty();
    //注册合同类型选项改变的事件
    $("#cpr_typeSelect").unbind('change').change(function () {
        $("#noselectMsg").hide();
        var proTypeID = $(this).val();
        if (proTypeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", proTypeID, "false", "0", "proNum", CprNumCallBack);
        } else {
            $('#cpr_numSelect').empty();
        }

    });
    //注册确定按钮点击事件
    $("#btn_cprNum_close").unbind('click').click(function () {
        var CprNum = $("#cpr_numSelect").find("option:selected").text();
        if (CprNum != NaN && CprNum != "" && CprNum != undefined) {
            $("#noselectMsg").hide();
            $("#ctl00_ContentPlaceHolder1_proNumber").val(CprNum);

        } else {
            $("#noselectMsg").show();
        }
    });
}
function CprNumCallBack(result) {
    var obj = result == null ? "" : result.ds;
    var cpr_TypeSelectChangeHtml;
    var prevTxt; //前缀
    var mark; //连接符
    var startNum; //开始编号
    var endNum; //结束编号
    $.each(obj, function (i, n) {
        startNum = n.StartNum;
        endNum = n.EndNum;
        prevTxt = n.PreFix;
        mark = n.Mark;
        for (var i = startNum; i <= endNum; i++) {
            var cprNumOk = prevTxt + mark + GetCprNumFix(i);
            cpr_TypeSelectChangeHtml += '<option cprNum="' + cprNumOk + '" value="' + GetCprNumFix(i) + '">' + cprNumOk + '</option>';
        }
    });
    $('#cpr_numSelect').empty();
    $('#cpr_numSelect').html(cpr_TypeSelectChangeHtml);
    //去除已使用的编号
    BindCprNumber(prevTxt + mark);
}
//处理合同编号数字
function GetCprNumFix(cprNo) {
    if (cprNo < 10) {
        return "00" + cprNo;
    } else if (10 <= cprNo && cprNo < 100) {
        return "0" + cprNo;
    } else { return cprNo; }
}
//去除已使用的合同编号
function BindCprNumber(fixstr) {
    var data = "action=removeUsedCprNum&type=1&fixStr=" + fixstr;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                $.each(obj, function (i, n) {
                    $("#cpr_numSelect option[cprNum=" + n.ProNumber + "]").remove();
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!未能去除已使用编号!");
        }
    });
}