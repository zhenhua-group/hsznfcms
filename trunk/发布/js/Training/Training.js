﻿/// <reference path="../../Training/TrainingStatics.aspx" />
var swfu;
var action;
$(document).ready(function () {

    $(".delete").click(function () {
        if (confirm("确定要删除该活动吗"))
        {
            var id = $(this).parents("tr").find(".cs").text();
            var url = "../../HttpHandler/TrainingHandler.ashx";
            var data = { "action": "delete", "id": id };
            $.post(url, data, function (result) {
                if (result == "0") {
                    alert("删除培训成功！");
                    window.location.reload();

                } else {
                    alert("删除培训失败！");

                }
            });
        }

      




    });

    $("#btn_add").click(function () {
        $("#addTrainingDiv .p").remove();
        $("#txt_title_valide").hide();
        $("#txt_BeginDate_valide").hide();
        $("#txt_EndDate_valide").hide();
        $("#txt_addperson_valide").hide();
        $(".txt_addfee_valide").hide();
        $("#title").val("");
        $("#BeginDate").val("");
        $("#EndDate").val("");
    });

    $(".cls_select").live("click", function () {

        


        $("#editTrainingDiv .p").remove();
        $("#edittxt_title_valide").hide();
        $("#edittxt_BeginDate_valide").hide();
        $("#edittxt_EndDate_valide").hide();
        $("#edittxt_addperson_valide").hide();
        $(".edittxt_addfee_valide").hide();
        $("#edittitle").val("");
        $("#editBeginDate").val("");
        $("#editEndDate").val("");

        var count = ($(this).parent().attr("rowspan"));
        if (!count) {
            count = 1;}
        var tr = $(this).parents("tr");
        $("#selectedid").val($(tr).find(".cs").text());

        var property = $(tr).find(".xz").text();
        var title = $(tr).find(".nr").text();
        var beginDate = $(tr).find(".ks").text();
        var endDate = $(tr).find(".js").text();
        $("#editselect").val(property);
        $("#edittitle").val(title);
        $("#editBeginDate").val(beginDate);
        $("#editEndDate").val(endDate);

        for (var i = 0; i < count; i++) {
            var baoMingFee = $(tr).find(".bm").text();
            var jiPiaoFee = $(tr).find(".jp").text();
            var zhuShuFee = $(tr).find(".zs").text();
            var otherFee = $(tr).find(".qt").text();
            var canYuRen = $(tr).find(".cy").text();
            var unitName = $(tr).find(".ss").text();
            var total = $(tr).find(".hj").text();
            var detailId = $(tr).find(".detail").text();
        
            $("#editTrainingDiv table tbody").append
   ("<tr class='p'><td><label class='CanYuRen'>" + canYuRen + "</label></td><td><label class='UnitName'>" + unitName + "</label></td>" +
       "<td><input  style='width: 50px;' class='BaoMingFee Fee' value=" + baoMingFee + "></td>" +
       "<td><input  style='width: 50px;' class='ZhuShuFee Fee' value=" + zhuShuFee + "></td>" +
       "<td><input style='width: 50px;' class='JiPiaoFee Fee' value=" + jiPiaoFee + "></td>" +
       "<td><input style='width: 50px;' class='OtherFee Fee' value=" + otherFee + "></td>" +
       "<td><input style='width: 50px;' class='Total' value=" + total + " disabled='true'/> <label class='detailId' style='display:none'>" + detailId + "</label></td>" +
        "<td><a href='#' class='editdelete'>删除</a></td>" +
                "</tr>");
            tr = $(tr).next();
        }
        $(".editdelete").click(function() { $(this).parents("tr").remove(); });
        $(".Fee").blur(function () {
            //var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            //var fee = $(this).val();
            //if (!reg.test(fee) || fee <= 0) {

            //} else {
            //    var total = $(this).parents("tr").find(".Total");
            //    var total1 = 0;
            //    if ($(total).val() != "")
            //        total1 = parseInt($(total).val());
            //    total1 += parseInt($(this).val());
            //    $(total).val(total1);


            //}
            var sum = 0;
            var total = $(this).parents("tr").find(".Total");
            var tr = $(this).parents("tr");
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            $(tr).find(".Fee").each(function () {
                var fee = $(this).val();
                if (!reg.test(fee) || fee <= 0) {

                } else {
                    sum += parseInt(fee);
                }
            });
            $(total).val(sum);

        });

    });

    var chooseUserOfTheDepartmentControl = new UserOfTheDepartmentTree($("#ChooseUserOfTherDepartmentContainer"));
    $("#btn_OK").click(function () {

        if ($("#editTraining").is(":hidden")) {
            action = "add";
        } else
            action = "edit";
        chooseUserOfTheDepartmentControl.SaveUser(ChooseUserOfTheDepartmentCallBack);


    });

    $("#btn_addsure").click(BtnAddCount);

    $("#btn_editsure").click(BtnEditCount);
    


    var flag = true;

    $("#btn_showadd").click(function () {
        $('#div_add').show();
        $("#div_edit").hide();
        window.scrollTo(0, document.body.scrollHeight);

    });
    $("#btn_Cancl").click(function () {
        $('#div_add').hide("slow");

        window.scrollTo(0, 0);

    });
    $("#btn_Cancle").click(function () {

        $("#div_edit").hide("slow");
        window.scrollTo(0, 0);

    });
    //删除是判断有无记录选中
    $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {
        if ($("#ctl00_ContentPlaceHolder1_grid_mem :checkbox:checked").length == 0) {
            jAlert("请选择要删除的人员！", "提示");
            return false;
        }
        else {
            $('#popup_ok').attr("class", "btn green");
            return confirm("是否要删除人员信息？");
        }
    });
    $('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').click(function () {

        var checks = $('#ctl00_ContentPlaceHolder1_grid_mem :checkbox');


        if ($('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').attr("checked") == "checked") {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.setAttribute("class", "checked");
                checks[i].setAttribute("checked", "checked");
            }
        }
        else {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.removeAttribute("class");
                checks[i].removeAttribute("checked");
            }
        }
    });



    $("#ctl00_ContentPlaceHolder1_txt_memLogin").blur(function () {
        ActionPic($(this).val(), "");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/LoginHandler.ashx?action=exsitlogin",
            data: "memlogin=" + escape($(this).val()),
            success: function (rlt) {
                if (rlt == "yes") {
                    $("#ctl00_ContentPlaceHolder1_hid_exsit").val("1");
                    jAlert("用户名已存在！", "提示");
                    $("#ctl00_ContentPlaceHolder1_txt_memLogin").addClass("cls_input_red");
                }
                else {
                    $("#ctl00_ContentPlaceHolder1_hid_exsit").val("");
                    $("#ctl00_ContentPlaceHolder1_txt_memLogin").removeClass("cls_input_red");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#ctl00_ContentPlaceHolder1_hid_exsit").val("1");
            }
        });
    });

    //添加
    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {

        var msg = "";
        //姓名
        if ($("#ctl00_ContentPlaceHolder1_txt_memName").val() == "") {
            msg += "用户姓名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_memName").val().length > 10) {
                msg += "用户姓名太长！<br/>";
            }
        }
        //登录名
        if ($("#ctl00_ContentPlaceHolder1_txt_memLogin").val() == "") {
            msg += "登录名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_memLogin").val().length > 15) {
                msg += "登录名过长！<br/>";
            }
        }
        if ($("#hid_exsit").val() == "1") {
            msg += "登录名已经存在！<br/>";
        }
        //密码
        if ($("#ctl00_ContentPlaceHolder1_txt_pwd").val().length > 16) {
            msg += "密码输入过长！<br/>";
        }
        //专业
        if ($("#ctl00_ContentPlaceHolder1_drp_spec").val() == "-1") {
            msg += "请选择专业！<br/>";
        }
        //单位
        if ($("#ctl00_ContentPlaceHolder1_drp_unit").val() == "-1") {
            msg += "请选择单位！<br/>";
        }
        //角色
        if ($("#ctl00_ContentPlaceHolder1_drp_roles").val() == "-1") {
            msg += "请选择角色！<br/>";
        }
        //邮箱
        if ($("#ctl00_ContentPlaceHolder1_txt_mail").val() != "") {
            var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            if (!reg.test($("#txt_mail").val())) {
                msg += "邮件格式不正确！";
            }
        }
        //手机
        if ($("#ctl00_ContentPlaceHolder1_txt_mobile").val() != "") {
            if ($("#txt_mobile").val().length > 11) {
                msg += "手机号码输入过长！<br/>";
            }
        }
        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_phone").val() != "") {
            if ($("#txt_phone").val().length > 16) {
                msg += "电话输入长度过长！<br/>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //修改
    $("#ctl00_ContentPlaceHolder1_btn_edit").live("click", function () {
        var msg = "";
        //姓名
        if ($("#ctl00_ContentPlaceHolder1_txt_memName0").val() == "") {
            msg += "用户姓名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_memName0").val().length > 10) {
                msg += "用户姓名太长！<br/>";
            }
        }
        //密码
        if ($("#ctl00_ContentPlaceHolder1_txt_pwd0").val().length > 16) {
            msg += "密码输入过长！<br/>";
        }
        //专业
        if ($("#ctl00_ContentPlaceHolder1_drp_spec0").val() == "-1") {
            msg += "请选择专业！<br/>";
        }
        //单位
        if ($("#ctl00_ContentPlaceHolder1_drp_unit0").val() == "-1") {
            msg += "请选择单位！<br/>";
        }
        //角色
        if ($("#ctl00_ContentPlaceHolder1_drp_roles0").val() == "-1") {
            msg += "请选择角色！<br/>";
        }
        //邮箱
        if ($("#ctl00_ContentPlaceHolder1_txt_mail0").val() != "") {
            var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            if (!reg.test($("#txt_mail0").val())) {
                msg += "邮件格式不正确！";
            }
        }
        //手机
        if ($("#ctl00_ContentPlaceHolder1_txt_mobile0").val() != "") {
            if ($("#txt_mobile").val().length > 11) {
                msg += "手机号码输入过长！<br/>";
            }
        }
        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_phone0").val() != "") {
            if ($("#txt_phone").val().length > 16) {
                msg += "电话输入长度过长！<br/>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

});


//点击保存
function BtnAddCount() {

    //性质
    var property = $("#select").val();
    //标题
    var title = $("#title").val();
    if (title == "") {
        $("#txt_title_valide").show();
        return false;
    } else {
        $("#txt_title_valide").hide();
    }

    //开始时间
    var beginDate = $("#BeginDate").val();
    if (beginDate == "") {
        $("#txt_BeginDate_valide").show();
        return false;
    } else {
        $("#txt_BeginDate_valide").hide();
    }

    //结束时间
    var endDate = $("#EndDate").val();
    if (endDate == "") {
        $("#txt_EndDate_valide").show();
        return false;
    } else {
        $("#txt_EndDate_valide").hide();
    }

    if ($("#addTrainingDiv tr").length == 3) {

        $("#txt_addperson_valide").text("请添加人员!").show();
        return false;
    } else {
        $("#txt_addperson_valide").text("请添加人员!").hide();
    }

    var err = 0;
    $(".Fee").each(function () {

        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var fee = $(this).val();
        if ((!reg.test(fee) || fee <= 0) && $(this).val() != "") {
            err += 1;
            $(".txt_addfee_valide").text("请输入正确的正整数!").show();
            return false;
        }
        else if ($(this).val() == "") {
            err += 1;
            $(".txt_addfee_valide").text("请输入费用!").show();
            return false;
        } else {
            $(".txt_addfee_valide").hide();
        }
    });
    if (err == 1) {
        return false;}

    var peoples = [];
    $(".p").each(function () {

        var d = {};
        var canYuRen = $(this).find(".CanYuRen").text();
        var unitName = $(this).find(".UnitName").text();
        var baoMingFee = $(this).find(".BaoMingFee ").val();
        var zhuShuFee = $(this).find(".ZhuShuFee ").val();
        var jiPiaoFee = $(this).find(".JiPiaoFee ").val();
        var otherFee = $(this).find(".OtherFee ").val();
        var total = $(this).find(".Total ").val();
        d.CanYuRen = canYuRen;
        d.UnitName = unitName;
        d.BaoMingFee = baoMingFee;
        d.ZhuShuFee = zhuShuFee;
        d.JiPiaoFee = jiPiaoFee;
        d.OtherFee = otherFee;
        d.Total = total;
        peoples.push(d);

    });
    var url = "../../HttpHandler/TrainingHandler.ashx";
    var data = { "action": "add", "property": "" + property + "", "title": "" + title + "", "beginDate": "" + beginDate + "", "endDate": "" + endDate + "", "peoples": "" + JSON.stringify(peoples) + "" };
    $.post(url, data, function (result) {
        if (result == "0") {
            alert("添加培训成功！");
            window.location.reload();

        } else {
            alert("添加培训失败！");

        }
    });

}


//点击编辑保存
function BtnEditCount() {

    var id = $("#selectedid").val();
   
    //性质
    var property = $("#editselect").val();
    //标题
    var title = $("#edittitle").val();
    if (title == "") {
        $("#edittxt_title_valide").show();
        return false;
    } else {
        $("#edittxt_title_valide").hide();
    }

    //开始时间
    var beginDate = $("#editBeginDate").val();
    if (beginDate == "") {
        $("#edittxt_BeginDate_valide").show();
        return false;
    } else {
        $("#edittxt_BeginDate_valide").hide();
    }

    //结束时间
    var endDate = $("#editEndDate").val();
    if (endDate == "") {
        $("#edittxt_EndDate_valide").show();
        return false;
    } else {
        $("#edittxt_EndDate_valide").hide();
    }

    if ($("#editTrainingDiv tr").length == 3) {

        $("#edittxt_addperson_valide").text("请添加人员!").show();
        return false;
    } else {
        $("#edittxt_addperson_valide").text("请添加人员!").hide();
    }

    var err = 0;
    $("#editTrainingDiv .Fee").each(function () {

        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var fee = $(this).val();
        if ((!reg.test(fee) || fee <= 0) && $(this).val() != "") {
            err += 1;
            $(".edittxt_addfee_valide").text("请输入正确的正整数!").show();
            return false;
        }
        else if ($(this).val() == "") {
            err += 1;
            $(".edittxt_addfee_valide").text("请输入费用!").show();
            return false;
        } else {
            $(".edittxt_addfee_valide").hide();
        }
    });
    if (err == 1) {
        return false;}

    var peoples = [];
    $("#editTrainingDiv .p").each(function () {

        var d = {};
        var canYuRen = $(this).find(".CanYuRen").text();
        var unitName = $(this).find(".UnitName").text();
        var baoMingFee = $(this).find(".BaoMingFee ").val();
        var zhuShuFee = $(this).find(".ZhuShuFee ").val();
        var jiPiaoFee = $(this).find(".JiPiaoFee ").val();
        var otherFee = $(this).find(".OtherFee ").val();
        var total = $(this).find(".Total ").val();
        //var id = $(this).find(".detailId").text();
        d.CanYuRen = canYuRen;
        d.UnitName = unitName;
        d.BaoMingFee = baoMingFee;
        d.ZhuShuFee = zhuShuFee;
        d.JiPiaoFee = jiPiaoFee;
        d.OtherFee = otherFee;
        d.Total = total;
        //d.id = id;
        peoples.push(d);

    });
    var url = "../../HttpHandler/TrainingHandler.ashx";
    var data = { "action": "edit", "id": "" + id + "", "property": "" + property + "", "title": "" + title + "", "beginDate": "" + beginDate + "", "endDate": "" + endDate + "", "peoples": "" + JSON.stringify(peoples) + "" };
    $.post(url, data, function (result) {
        if (result == "0") {
            alert("编辑培训成功！");
            window.location.reload();

        } else {
            alert("编辑培训失败！");

        }
    });

}




function ChooseUserOfTheDepartmentCallBack(userArray) {

    if (action == "add") {
        $.each(userArray, function (index, item) {
            $("#addTrainingDiv table tbody").append
            ("<tr class='p'><td><label class='CanYuRen'>" + item.userName + "</label></td><td><label class='UnitName'>" + item.departmentName + "</label></td><td><input  style='width: 50px;' class='BaoMingFee Fee'/></td>" +
                "<td><input  style='width: 50px;' class='ZhuShuFee Fee'/></td>" +
                "<td><input style='width: 50px;' class='JiPiaoFee Fee'/></td>" +
                "<td><input style='width: 50px;' class='OtherFee Fee'/></td>" +
                "<td><input style='width: 50px;' class='Total' disabled='true'/> </td>" +
                "<td><a href='#' class='adddelete'>删除</a></td>"+
                "</tr>");
        });
        $(".adddelete").click(function () { $(this).parents("tr").remove(); });
    } else {
        $.each(userArray, function (index, item) {
            $("#editTrainingDiv table tbody").append
            ("<tr class='p'><td><label class='CanYuRen'>" + item.userName + "</label></td><td><label class='UnitName'>" + item.departmentName + "</label></td><td><input  style='width: 50px;' class='BaoMingFee Fee'/></td>" +
                "<td><input  style='width: 50px;' class='ZhuShuFee Fee'/></td>" +
                "<td><input style='width: 50px;' class='JiPiaoFee Fee'/></td>" +
                "<td><input style='width: 50px;' class='OtherFee Fee'/></td>" +
                "<td><input style='width: 50px;' class='Total' disabled='true'/> </td>" +
                "<td><a href='#' class='editdelete'>删除</a></td>" +
                "</tr>");
        });
        $(".editdelete").click(function () { $(this).parents("tr").remove(); });

    }


    $(".Fee").blur(function () {
        var sum = 0;
        var total = $(this).parents("tr").find(".Total");
        var tr = $(this).parents("tr");
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        $(tr).find(".Fee").each(function () {
            var fee = $(this).val();
            if (!reg.test(fee) || fee <= 0) {

            } else {
                sum += parseInt(fee);
            }
        });
        $(total).val(sum);
    });
}

function cls_select_Edit() {
    window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
}

function Clear() {
    $("input", $("#div_add")).val("");
    window.document.getElementsByTagName("select").selectedIndex = 0;
}
function ActionPic(memid, param) {

    var divid = "divFileProgressContainer" + param;
    var spanid = "spanButtonPlaceholder" + param;

    //附件高清图
    swfu = new SWFUpload({

        upload_url: "../ProcessUpload/upload_sign.aspx?type=sign&id=" + memid + "&userid=" + userid,
        flash_url: "../js/swfupload/swfupload.swf",
        post_params: {
            "ASPSESSID": "<%=Session.SessionID %>"
        },
        file_size_limit: "10 MB",
        file_types: "*.jpg;*.gif;*.bmp;.png",
        file_types_description: "文件资料上传",
        file_upload_limit: "0",
        file_queue_limit: "1",

        //Events
        file_queued_handler: fileQueued,
        file_queue_error_handler: fileQueueError,
        file_dialog_complete_handler: fileDialogComplete,
        upload_progress_handler: uploadProgress,
        upload_error_handler: uploadError,
        upload_success_handler: uploadSuccess,
        upload_complete_handler: uploadComplete,

        // Button
        button_placeholder_id: spanid,
        button_image_url: "../images/swfupload/XPButtonNoText_61x22.png",
        button_width: 61,
        button_height: 22,
        button_text: '<span class="btnFile">选择文件</span>',
        button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 11pt;background-color:Black; } ',
        button_text_top_padding: 1,
        button_text_left_padding: 5,

        custom_settings: {
            upload_target: divid
        },
        debug: false
    });

}