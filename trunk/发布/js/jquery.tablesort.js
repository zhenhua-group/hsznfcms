﻿/*
*	Create By Long 2013-05-08
*/
(function ($) {
    $.fn.extend({
        "tablesort": function (options) {
            options = $.extend({
                sortcolumn: "ID",
                sortvalue: "ASC",
                schbtn: $("#btn_search"),
                hidclm: $("#hid_column"),
                hidord: $("#hid_order")
            }, options);
            //实现方式
            $("tr>td[sortc]", this).each(function (i) {
                //列属性
                options.sortcolumn = $(this).attr("sortc");
                var text = $(this).text();

                if (options.sortcolumn != undefined) {
                    $(this).html("<a title='排序' href='###' class='cls_sorthead' sortc='" + options.sortcolumn + "' sortv='" + options.sortvalue + "'>" + text + "</a>▼");
                }

            });
            //点击事件
            $("tr>td[sortc]", this).click(function () {
                //字段
                var column = $(this).find('a').attr("sortc");
                options.hidclm.val(column);
                //排序
                var order = $(this).find('a').attr("sortv");
  
                if (options.hidord.val()=='') {
                    options.hidord.val(order);                    
                }
                if (options.hidord.val() == "ASC") {
                    options.hidord.val("DESC");
                }
                else {
                    options.hidord.val("ASC");
                }

                //查询
                if (column != undefined) {
                    options.schbtn.get(0).click();
                }

                return this;
            });
        }
    });
})(jQuery);


