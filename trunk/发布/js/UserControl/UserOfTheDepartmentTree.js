﻿function UserOfTheDepartmentTree(container) {
    this.Dom = {};
    this.Container = confirm;
    this.Dom = {};
    var Instance = this;

    Instance.IsRadio = $("#IsRadio", container).val();

    this.SaveUser = function (callBack) {
        //获取所有选中的人物
        var alluserlist = $("li[additional-attributes][checkedstate=0]");

        var resultArray = new Array();
        $.each(alluserlist, function (index, item) {
            resultArray[index] = eval("(" + $(item).attr("additional-attributes") + ")");;
        });

        callBack(resultArray);
    }
    //清除所有的CheckBox
    this.Clear = function () {
        $(":checkbox", container).removeAttr("checked");
    }
    //单选模式的场合互斥
    $("li[additional-attributes] img[class=astreeview-checkbox]").click(function () {
        if (Instance.IsRadio == "True") {
            $("li[additional-attributes]").attr("checkedstate", "1").find("img[class=astreeview-checkbox]").attr("src", "/js/astreeview/astreeview/images/astreeview-checkbox-unchecked.gif");

            $(this).attr("src", "/js/astreeview/astreeview/images/astreeview-checkbox-checked.gif").parent("li[additional-attributes]").attr("checkedstate", "0");
        }
    });
}