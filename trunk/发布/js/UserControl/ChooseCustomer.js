﻿//选择客户信息JS类
function ChooseCustomer(container, callback) {
    this.PageSize = $("#hiddenPageSize", container).val();
    this.ResultTable = $("#customerInfoListTable", container);
    this.Pager = $("#pager", container);
    this.BackgroundInvoke = TG.Web.UserControl.ChooseCustomer;
    this.TempArray = null;
    var Instance = this;

    this.SearchButton = $("#serachCustomerButton", container);
    this.SearchButton.bind("click", function () {
        Instance.SearchCustomserRecord();
    });

    //查询客户信息方法
    this.SearchCustomserRecord = function (queryObj) {
        var queryObjEntity = queryObj;
        if (queryObj == undefined || typeof queryObj != "object") {
            queryObjEntity = Instance.InitQueryObj();
        }

        var jsonObj = Global.toJSON(queryObjEntity);
        var result = Instance.BackgroundInvoke.SearchCustomerRecord(jsonObj).value;
        var resultList = Global.evalJSON(result);

        Instance.CreateTable(resultList.Body);

        var pageCount = CommonControl.AccountPageCount(resultList.TotalCount, Instance.PageSize);

        //初始化分页控件
        Instance.Pager.pager({ "pagenumber": 1, "pagecount": pageCount, "pagesize": Instance.PageSize, "datacount": resultList.TotalCount, "buttonClickCallback": Instance.PageCallBack });
    }

    //分页回调函数
    this.PageCallBack = function (clickNumber) {
        var queryObject = Instance.InitQueryObj(clickNumber);
        var jsonObj = Global.toJSON(queryObject);
        var result = Instance.BackgroundInvoke.SearchCustomerRecord(jsonObj).value;
        var resultList = Global.evalJSON(result);

        Instance.CreateTable(resultList.Body);
        //调用公共方法，计算总页数
        var pageCount = CommonControl.AccountPageCount(resultList.TotalCount, Instance.PageSize);

        //初始化分页控件
        Instance.Pager.pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": Instance.PageSize, "datacount": resultList.TotalCount, "buttonClickCallback": Instance.PageCallBack });
    }

    //创建查询对象
    this.InitQueryObj = function (pagecurrent) {
        var obj = {
            "PageSize": Instance.PageSize,
            "PageCurrent": pagecurrent == undefined ? 1 : pagecurrent,
            "CustomerName": $("#customerName", container).val(),
            "CustomerLevel": $("#ctl00_ContentPlaceHolder1_ChooseCustomer1_customerLevel", container).val(),
            "ProvinceName": $("#provinceName", container).val(),
            "CityName": $("#cityName", container).val(),
            "LinkMan": $("#linkMan", container).val(),
            "UserSysNo": $("#hid_usersysno", container).val(),
            "PreviewPower": $("#hid_previewpower", container).val()
        };
        return obj;
    }

    //拼接新表格内容
    this.CreateTable = function (customerRecordList) {
        Instance.TempArray = customerRecordList;
        Instance.Clear();
        $.each(customerRecordList, function (i, c) {
            var trString = "<tr>";
            trString += "<td style=\"text-align:left;width:100px;\">" + $.trim(c.CustomerNo) + "</td>";
            trString += "<td style=\"text-align:left;width:230px;\">" + $.trim(c.CustomerName) + "</td>";
            trString += "<td style=\"text-align:left;width:60px;\">" + $.trim(c.LinkMan) + "</td>";
            trString += "<td style=\"text-align:left;width:160px;\">" + $.trim(c.Address) + "</td>";
            trString += "<td style=\"text-align:left;width:100px;\">" + $.trim(c.Phone) + "</td>";
            trString += "<td style=\"text-align:center;width:50px;\">";
            trString += "<input type=\"hidden\" value=\"" + i + "\" />";
            trString += " <a href=\"#\" id=\"chooseCustomer\">选择</a></td>";
            trString += "</tr>";
            Instance.ResultTable.append(trString);
        });
        //隔行变色
        CommonControl.SetTableStyle("customerInfoListTable", "need");
    }

    //选择用户确认事件
    $("a[id=chooseCustomer]", container).live("click", function () {
        var recordObj = Instance.TempArray[$(this).siblings(":hidden:first").val()];
        callback(recordObj);
        $("#btn_customer_close").trigger('click');
        return false;
    });

    //清空查询结果
    this.Clear = function () {
        Instance.ResultTable.find("tr:gt(0)").remove();
        Instance.ResultTable.find("tr:eq(0)").remove();
    }

    //清空已入力的查询条件
    this.ClearQueryCondition = function () {
        $(":text", container).val("");
        $("select", container)[0].selectedIndex = 0;
    }

    //调用查询方法
    Instance.SearchCustomserRecord();
}