﻿$(document).ready(function () {

    //项目信息
    $("#xmxx").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectManage/ProjectList.aspx");
    });
    //项目立项
    $("#xmlx").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectManage/AddProject.aspx");
    });
    //项目审批
    $("#xmsp").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectManage/ApplyProject.aspx");
    });
    $("#xmspEdit").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectManage/ApplyEditProject.aspx");
    });
    //合同关联
    $("#link_proj_coper").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../ProjectManage/Project_RelationCoperation.aspx");
    });
    //工号申请
    $("#pronumberapply").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../ProjectNumber/ProPassList.aspx");
    });
    //工号分配
    $("#pronumallot").click(function () {

        $("#rightFrame", window.parent.document).attr("src", "../ProjectNumber/ProNumber.aspx");
    });
    //项目策划
    $("#projectPlanButton").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectPlan/ProPlanList.aspx");
    });

    //项目进度计划
    $("#xmjd").click(function () {
        $("#gh").hide("slow");
    });
    //项目产值分配
    $("#czfp").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectValueandAllot/ProValueandAlltList.aspx");
    });
    //经济所
    $("#jjsAllot").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectValueandAllot/EconomyProjectValueAllotList.aspx");
    });
    //暖通所产值分配
    $("#ntsAllot").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectValueandAllot/HaveProjectValueAllotList.aspx");
    });

    //二次产值分配
    $("#secondValue").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectValueandAllot/ProjectSecondValueList.aspx");
    });
    //转暖通所

    $("#tranNts").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectValueandAllot/TrunHavcList.aspx");
    });

    $("#tranjjs").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectValueandAllot/TranEconomyList.aspx");
    });

    //转土建所
    $("#tranTjs").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectValueandAllot/TranBulidingList.aspx");
    });

    //项目统计
    $("#xmtj").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectStatistics/ProjectCountList.aspx");
    });

    //查询功能
    $("#pro_check").click(function () {
        $("#gh").hide("slow");
        $("#rightFrame", window.parent.document).attr("src", "../ProjectManage/Sch_Project.aspx");
    });

    //图纸录入
    $("#pro_imge").click(function () {

        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/Pro_imageList.aspx");
    });
    //图纸审批
    $("#pro_imageapp").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ApplyProImage.aspx");
    });
    //所补产值配置
    $("#sbczsz").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../ProjectManage/CompensationSet.aspx");
    });
    //产值分配表
    $("#ValueAllot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/LeadershipCockpit/ProjectValueAllList.aspx");
    });
    //个人产值分配表
    $("#MemValueAllot").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/LeadershipCockpit/MemberValueAllot.aspx");
    });
    //个人产值分配表(二)
    $("#MemValueAllot2").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/LeadershipCockpit/MemberValueAllot2.aspx");
    });
    //个人收入明细表
    $("#MemDetailed").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/LeadershipCockpit/MemberDetailed.aspx");
    });

    //工程设计出图卡
    $("#pro_plotInfo").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ProjectDesignCardsList.aspx");
    });
    //工程出图卡审批
    $("#pro_plotInfoAudit").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ProjectDesignCardsAuditList.aspx");
    });

    //归档资料申请

    $("#pro_FileAdd").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ProjectFileList.aspx");
    });
    //归档资料审核

    $("#pro_FileAudit").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "/ProjectManage/ProImageAudit/ProjectFileAuditList.aspx");
    });
    //控制菜单显示
    LoadLeftMenu.GetLeftMenu(4);
    //菜单折叠
    CommonControl.HideLeftMenu();
})
