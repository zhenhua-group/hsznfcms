﻿var swfu;
$(document).ready(function () {
   
    $("#ctl00_ContentPlaceHolder1_hid_cprid").val(cprid);
    var cprinterviewids = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $("#btn_add").click(function () {
        var id = $("#selectedid").val();
        var count = ($(this).parent().attr("rowspan"));
        if (!count) {
            count = 1;
        }
        var tr = $(this).parents("tr");
        $("#selectedid").val($(tr).find(".cs").text());
        var ids = $(tr).find(".cs").text();
        $("#addTrainingDiv .p").remove();
        $("#txt_title_valide").hide();
        $("#txt_BeginDate_valide").hide();
        $("#txt_EndDate_valide").hide();
        $("#txt_addperson_valide").hide();
        $(".txt_addfee_valide").hide();
        $("#title").val("");
        $("#BeginDate").val("");
        $("#EndDate").val("");
    });
    $(".cls_selectedit").live("click", function () {
        $("#div_edit").show("slow");
    
        var id = $("#selectedid").val();
        var count = ($(this).parent().attr("rowspan"));
        if (!count) {
            count = 1;
        }
        var tr = $(this).parents("tr");
        $("#selectedid").val($(tr).find(".cs").text());
        var ids = $(tr).find(".cs").text();
        var interview_name = $(tr).find(".xm").text();
        var interview_unit = $(tr).find(".bmmc").text();
        var beginDate = $(tr).find(".mssj").text();
        var msjg = $(tr).find(".msjg").text();
        var bz = $(tr).find(".bz").text();
        var fj = $(tr).find(".fj").text();

        $("#ctl00_ContentPlaceHolder1_drp_Interviewunitedit").val(interview_unit);
        $("#txt_internameedit").val(interview_name);
        $("#txt_interdateedit").val(beginDate);
        $("#txt_interresultsedit").val(msjg);
        $("#txt_interremarkedit").val(bz);
        $("#ctl00_ContentPlaceHolder1_txt_pic0").val(fj);
        ActionPic(cprinterviewids, "0");
    });
   
    $("#btn_addsure").click(BtnAddCount);

   // $("#btn_editsure").click(BtnEditCount);
    $("#btn_Interviewedit").click(BtnEditCount);

    $(".cls_selectDelete").live("click", function () {

        var count = ($(this).parent().attr("rowspan"));
        if (!count) {
            count = 1;
        }
        var tr = $(this).parents("tr");
        $("#selectedid").val($(tr).find(".cs").text());
        var ids = $(tr).find(".cs").text();
        if (confirm("是否确认删除")) {
            var url = "../../HttpHandler/InterviewManageHandler.ashx";
            var data = { "action": "del", "ID": "" + ids + "" };
            $.post(url, data, function (result) {
                if (result == "0") {
                    alert("删除成功！");
                    window.location.reload();

                } else {
                    alert("删除失败！");

                }
            });
        }
        else {
            return false;
        }
    });


    var flag = true;

    $("#btn_showadd").click(function () {
        $('#div_add').show();
        $("#div_edit").hide();
        window.scrollTo(0, document.body.scrollHeight);
    });
    $("#btn_Cancl").click(function () {
        $('#div_add').hide("slow");
        window.scrollTo(0, 0);
    });
    $("#btn_Cancle").click(function () {

        $("#div_edit").hide("slow");
        window.scrollTo(0, 0);
    });

    $("#title").blur(function () {
        ActionPic(cprinterviewids, "");
    });
});


//点击保存
function BtnAddCount() {
    //部门名称
    var msg = "";
    //姓名
    var Interview_unit = $("#ctl00_ContentPlaceHolder1_drp_Interviewunit").val();
    if (Interview_unit == "-1")
    {
        msg += "请选择部门！<br/>";
  
    }
    //面试结果
    var Inter_Jieguo = $("#select1").val();
    //姓名
    var Inter_Name = $("#title").val();
    if (Inter_Name == "") {
        $("#txt_title_valide").show();
        msg += "姓名不能为空！<br/>";
    } else {
        $("#txt_title_valide").hide();
    }

    //面试时间
    var beginDate = $("#BeginDate").val();
    if (beginDate == "") {
        $("#txt_BeginDate_valide").show();
        msg += "面试时间不能为空！<br/>";
    } else {
        $("#txt_BeginDate_valide").hide();
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_pic").val() == "")
    {
        msg += "附件不能为空！<br/>";
    }
    var TempNo = cprid;
    var Interview_Remark = $("#Remark").val();

    if (msg != "") {
        jAlert(msg, "提示");
        return false;
    }
    var url = "../../HttpHandler/InterviewManageHandler.ashx";
    var data = { "action": "add", "Interview_unit": "" + Interview_unit + "", "Inter_Jieguo": "" + Inter_Jieguo + "", "Inter_Name": "" + Inter_Name + "", "beginDate": "" + beginDate + "", "Interview_Remark": "" + Interview_Remark + "", "TempNo": "" + TempNo + "" };
    $.post(url, data, function (result) {
        if (result == "0") {
            alert("添加面试成功！");
            window.location.reload();

        } else {
            alert("添加面试失败！");

        }
    });

}


//点击编辑保存
function BtnEditCount() {
    var id = $("#selectedid").val();
    //部门名称
    var Interview_unit = $("#ctl00_ContentPlaceHolder1_drp_Interviewunitedit").val();
    //面试结果
    var Inter_Jieguo = $("#txt_interresultsedit").val();
    //姓名
    var Inter_Name = $("#txt_internameedit").val();
    if (Inter_Name == "") {
        $("#Span1").show();
        return false;
    } else {
        $("#Span1").hide();
    }

    //面试时间
    var beginDate = $("#txt_interdateedit").val();
    if (beginDate == "") {
        $("#Span2").show();
        return false;
    } else {
        $("#Span2").hide();
    }
    var TempNo = cprid;
    var Interview_Remark = $("#txt_interremarkedit").val();

    var filename = $("#ctl00_ContentPlaceHolder1_txt_pic0").val();

    var url = "../../HttpHandler/InterviewManageHandler.ashx";
    var data = { "action": "edit", "Interview_unit": "" + Interview_unit + "", "Inter_Jieguo": "" + Inter_Jieguo + "", "Inter_Name": "" + Inter_Name + "", "beginDate": "" + beginDate + "", "Interview_Remark": "" + Interview_Remark + "", "ids": "" + id + "", "TempNo": "" + TempNo + "", "filename": "" + filename + "" };
    $.post(url, data, function (result) {
        if (result == "0") {
            alert("编辑成功！");
            window.location.reload();

        } else {
            alert("编辑失败！");

        }
    });


}







function Clear() {
    $("input", $("#div_add")).val("");
    window.document.getElementsByTagName("select").selectedIndex = 0;
}


function GetAttachData() {
    loadCoperationAttach();
   
}
//加载附件信息
function loadCoperationAttach() {
    var data = "action=getcprfilestypeinterview&cpr_type=interview&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/InterviewManageHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {

                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href='#' rel='" + n.ID + "'>删除</a>";
                    var oper2 = "<a href='../Attach_User/filedata/cprfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    $("#ctl00_ContentPlaceHolder1_txt_pic").val(n.FileName);
                    $("#ctl00_ContentPlaceHolder1_txt_pic0").val(n.FileName);
                    
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filename").attr("align", "left");
                    row.find("#att_filesize").text(n.FileSizeString + 'KB');
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper a").click(function () {
                        if (confirm("确定要删除本条附件吗？")) {
                            delCprAttach($(this));
                        }

                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误7！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                //加载附件
                link.parents("tr:first").remove();
                // alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误8！");
        }
    });
}





function ActionPic(memid, param) {

    var divid = "divFileProgressContainer" + param;
    var spanid = "spanButtonPlaceholder" + param;

    //附件高清图
    swfu = new SWFUpload({


        upload_url: "../ProcessUpload/upload_Interview.aspx?type=interview&id=" + memid + "&userid=" + userid,
        flash_url: "../js/swfupload/swfupload.swf",
        post_params: {
            "ASPSESSID": "<%=Session.SessionID %>"
        },
        file_size_limit: "10 MB",
        file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
        file_types_description: "上传",
        file_upload_limit: "0",
        file_queue_limit: "1",

        //Events
        file_queued_handler: fileQueued,
        file_queue_error_handler: fileQueueError,
        file_dialog_complete_handler: fileDialogComplete,
        upload_progress_handler: uploadProgress,
        upload_error_handler: uploadError,
        upload_success_handler: uploadSuccess,
        upload_complete_handler: uploadComplete,

        // Button
        button_placeholder_id: spanid,
        button_image_url: "../images/swfupload/XPButtonNoText_61x22.png",
        button_width: 61,
        button_height: 22,
        button_text: '<span class="btnFile">选择文件</span>',
        button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 11pt;background-color:Black; } ',
        button_text_top_padding: 1,
        button_text_left_padding: 5,

        custom_settings: {
            upload_target: divid
        },
        debug: false
    });

}

