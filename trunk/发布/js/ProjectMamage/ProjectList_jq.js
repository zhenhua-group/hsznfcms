﻿ 
var projectList = function () {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectMamage/ProjectListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        mytype: 'POST',
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '', '', '', '甲方负责人', '项目名称', '建筑类别', '合同信息', '合同额(万元)', '开始日期', '结束日期', '执行设总','项目工程号', '管理级别', '审核级别', '建设单位', '建设地点', '建设规模(㎡)', '承接部门', '结构形式', '建筑分类', '设计阶段', '项目来源', '行业性质', '甲方负责人电话', '执行设总电话', '参与部门', '', '', '项目概况', '项目备注', '录入人', '录入时间', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'CoperationSysNo', index: 'CoperationSysNo', hidden: true, editable: true },
                             { name: 'allAcount', index: 'allAcount', hidden: true, editable: true },
                             { name: 'allProjectScale', index: 'allProjectScale', hidden: true, editable: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 80, align: 'center' },
                             { name: 'pro_name', index: 'pro_name', width: 200, formatter: colNameShowFormatter },
                             { name: 'BuildType', index: 'BuildType', width: 60, align: 'center' },
                             { name: 'Project_reletive', index: 'Project_reletive', width: 200, formatter: colNameCprFormatter },
                             { name: 'Cpr_Acount', index: 'Cpr_Acount', width: 80, align: 'center' },
                             { name: 'qdrq', index: 'pro_startTime', width: 100, align: 'center' },
                             { name: 'wcrq', index: 'pro_finishTime', width: 100, align: 'center' },
                             { name: 'PMUserName', index: 'PMName', width: 60, align: 'center' },
                             { name: 'Pro_number', index: 'Pro_number', width: 80, align: 'center', hidden: true },
                              { name: 'pro_jb', index: 'pro_level', width: 80, align: 'center', hidden: true },
                               {
                                   name: 'AuditLevel', index: 'AuditLevel', width: 80, hidden: true, formatter: function colAuditName(celvalue, options, rowData) {
                                       var str = "";
                                       if ($.trim(celvalue) == "1,0") {
                                           str = "院审";
                                       }
                                       if ($.trim(celvalue) == "0,1") {
                                           str = "所审";
                                       }
                                       if ($.trim(celvalue) == "1,1") {
                                           str = "院审,所审";
                                       }
                                       return str;
                                   }
                               },
                                { name: 'pro_buildUnit', index: 'pro_buildUnit', width: 80, hidden: true },
                                 { name: 'BuildAddress', index: 'BuildAddress', width: 80, hidden: true },
                                 { name: 'ProjectScale', index: 'ProjectScale', width: 80, align: 'center', hidden: true },
                               { name: 'Unit', index: 'Unit', width: 80, align: 'center', hidden: true },
                                { name: 'pro_StruType', index: 'pro_StruType', width: 80, hidden: true },
                                 { name: 'pro_kinds', index: 'pro_kinds', width: 80, hidden: true },
                                 { name: 'pro_status', index: 'pro_status', width: 80, hidden: true },
                               { name: 'pro_from', index: 'Pro_src', width: 80, align: 'center', hidden: true },
                                { name: 'Industry', index: 'Industry', width: 80, align: 'center', hidden: true },
                                 { name: 'Phone', index: 'Phone', width: 80, align: 'center', hidden: true },
                               { name: 'PMPhone', index: 'PMPhone', width: 80, align: 'center', hidden: true },
                                { name: 'ISTrunEconomy', index: 'ISTrunEconomy', width: 80, hidden: true, formatter: colISFormatter },
                                 { name: 'ISHvac', index: 'ISHvac', hidden: true },
                                 { name: 'ISArch', index: 'ISArch', hidden: true },
                                 { name: 'ProjSub', index: 'ProjSub', width: 80, hidden: true },
                                 { name: 'pro_Intro', index: 'pro_Intro', width: 80, hidden: true },
                               { name: 'InsertUser', index: 'InsertUserID', width: 80, align: 'center', hidden: true },
                             { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        rowTotals: true,
        colTotals: true,
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {

                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'pro_ID') + ",";
                            }
                        }

                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );
}
//其他部门参与
function colISFormatter(celvalue, options, rowData) {
    var str = "";
    if ($.trim(celvalue) == "1") {
        str = "经济所,"
    }
    if ($.trim(rowData["ISHvac"]) == "1") {
        str += "暖通所,";
    }
    if ($.trim(rowData["ISArch"]) == "1") {
        str += "土建所,";
    }
    return str;
}
//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//合同名称连接
function colNameCprFormatter(celvalue, options, rowData) {
    var pageurl = "../Coperation/cpr_ShowCoprationBymaster.aspx?flag=projlist1&cprid=" + rowData["CoperationSysNo"];
    if (celvalue == null) {
        celvalue = "";
    }
    return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowProjectBymaster.aspx?flag=list&pro_id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看项目">查看</a>';

}
//编辑
function colEditFormatter(celvalue, options, rowdata) {
    if (celvalue != "") {
        var pageurl = "ModifyProjectBymaster.aspx?bs_project_Id=" + celvalue;
        return '<a href="' + pageurl + '" alt="编辑项目" class="allowEdit">编辑</a>';
    } // class="allowEdit"
}
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
    var rowNum = parseInt($("#jqGrid").getGridParam('records'), 10);
    if (rowNum > 0) {
        $(".ui-jqgrid-sdiv").show();
        var ret = $("#jqGrid").jqGrid("getRowData", 1);
        var allcount = ret["allAcount"];
        var allarea = ret["allProjectScale"];
        $("#jqGrid").footerData('set', { pro_name: "合计:", Cpr_Acount: allcount, ProjectScale: allarea }, false);
    }
    else {
        $("#nodata").hide();
    }
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
var clearGridPostData = function () {
    var postData = $("#jqGrid").jqGrid("getGridParam", "postData");
    $.each(postData, function (k, v) {
        delete postData[k];
    });
}