﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    $.each($(":text"), function () {
        var value = $(this).val();
        $(this).focus(function () {
            $(this).val("");
        });
        $(this).blur(function () {
            var value1 = $(this).val();
            var str = /^\+?[0-9][0-9]*$/; //正整数
            if (value1 == "") {
                $(this).val(value);
            }
            else {
                if (!str.test(value1)) {
                    alert("图纸数量应该为正整数！");
                    $(this).val("0");
                } else {
                    value = value1;
                    var hj0 = parseInt($("#jz0").val()) + parseInt($("#jg0").val()) + parseInt($("#gps0").val()) + parseInt($("#nt0").val()) + parseInt($("#dq0").val())
                    var hj1 = parseInt($("#jz1").val()) + parseInt($("#jg1").val()) + parseInt($("#gps1").val()) + parseInt($("#nt1").val()) + parseInt($("#dq1").val())
                    var hj2 = parseInt($("#jz2").val()) + parseInt($("#jg2").val()) + parseInt($("#gps2").val()) + parseInt($("#nt2").val()) + parseInt($("#dq2").val())
                    var hj2_1 = parseInt($("#jz2_1").val()) + parseInt($("#jg2_1").val()) + parseInt($("#gps2_1").val()) + parseInt($("#nt2_1").val()) + parseInt($("#dq2_1").val())
                    var hj3 = parseInt($("#jz3").val()) + parseInt($("#jg3").val()) + parseInt($("#gps3").val()) + parseInt($("#nt3").val()) + parseInt($("#dq3").val())
                    var hj4 = parseInt($("#jz4").val()) + parseInt($("#jg4").val()) + parseInt($("#gps4").val()) + parseInt($("#nt4").val()) + parseInt($("#dq4").val())
                    var hj1_1 = parseInt($("#jz1_1").val()) + parseInt($("#jg1_1").val()) + parseInt($("#gps1_1").val()) + parseInt($("#nt1_1").val()) + parseInt($("#dq1_1").val())
                    var cadhj0 = parseInt($("#cadjz0").val()) + parseInt($("#cadjg0").val()) + parseInt($("#cadgps0").val()) + parseInt($("#cadnt0").val()) + parseInt($("#caddq0").val())
                    var cadhj1 = parseInt($("#cadjz1").val()) + parseInt($("#cadjg1").val()) + parseInt($("#cadgps1").val()) + parseInt($("#cadnt1").val()) + parseInt($("#caddq1").val())
                    var cadhj2 = parseInt($("#cadjz2").val()) + parseInt($("#cadjg2").val()) + parseInt($("#cadgps2").val()) + parseInt($("#cadnt2").val()) + parseInt($("#caddq2").val())
                    var cadhj2_1 = parseInt($("#cadjz2_1").val()) + parseInt($("#cadjg2_1").val()) + parseInt($("#cadgps2_1").val()) + parseInt($("#cadnt2_1").val()) + parseInt($("#caddq2_1").val())
                    var cadhj3 = parseInt($("#cadjz3").val()) + parseInt($("#cadjg3").val()) + parseInt($("#cadgps3").val()) + parseInt($("#cadnt3").val()) + parseInt($("#caddq3").val())
                    var cadhj4 = parseInt($("#cadjz4").val()) + parseInt($("#cadjg4").val()) + parseInt($("#cadgps4").val()) + parseInt($("#cadnt4").val()) + parseInt($("#caddq4").val())
                    var cadhj1_1 = parseInt($("#cadjz1_1").val()) + parseInt($("#cadjg1_1").val()) + parseInt($("#cadgps1_1").val()) + parseInt($("#cadnt1_1").val()) + parseInt($("#caddq1_1").val())
                    $("#hj0").text(hj0.toString())
                    $("#hj1").text(hj1.toString())
                    $("#hj2").text(hj2.toString())
                    $("#hj2_1").text(hj2_1.toString())
                    $("#hj3").text(hj3.toString())
                    $("#hj4").text(hj4.toString())
                    $("#hj1_1").text(hj1_1.toString())
                    $("#cadhj0").text(cadhj0.toString())
                    $("#cadhj1").text(cadhj1.toString())
                    $("#cadhj2").text(cadhj2.toString())
                    $("#cadhj2_1").text(cadhj2_1.toString())
                    $("#cadhj3").text(cadhj3.toString())
                    $("#cadhj4").text(cadhj4.toString())
                    $("#cadhj1_1").text(cadhj1_1.toString())
                    $("#Sum_image").text((hj0 + hj1 + hj2 + hj2_1 + hj3 + hj4 + hj1_1 + cadhj0 + cadhj1 + cadhj2 + cadhj2_1 + cadhj3 + cadhj4 + cadhj1_1).toString())
                }
            }
        })
    });
});
