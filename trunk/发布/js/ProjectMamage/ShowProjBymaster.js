﻿$(document).ready(function () {


    //加载数据
    loadCoperationAttach();
    //导出
    var cprname = $("#ctl00_ContentPlaceHolder1_hid_projid").val();
    $("#ag").attr("href", "../HttpHandler/Process.ashx?flag=cstshow&cprname=" + cprname + "");
});

//加载附件信息ctl00_ContentPlaceHolder1_hid_projid
function loadCoperationAttach() {
    var data = "action=getprojfiles&type=proj&projid=" + $("#ctl00_ContentPlaceHolder1_hid_projid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            var filedata = result == null ? "" : result.ds;
            if ($("#datas_att tr").length > 1) {
                $("#datas_att tr:gt(0)").remove();
            }
            //缩略图列表
            if ($("#img_container img").length > 1) {
                $("#img_container img:gt(0)").remove();
            }
            $.each(filedata, function (i, n) {
                var row = $("#att_row").clone();
                //显示
                $("#img_small").show();
                //克隆
                var thumd = $("#img_small").clone();
                //隐藏
                $("#img_small").hide();

                var oper2 = "<a href='../Attach_User/filedata/projfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";

                //显示缩略图
                var img_path = n.FileUrl.split('/');
                thumd.attr("src", "../Attach_User/filedata/projfile/" + img_path[0] + "/min/" + img_path[1]);

                row.find("#att_id").text(n.ID);
                row.find("#att_filename").attr("align", "left").html(img + n.FileName);
                row.find("#att_filesize").text(n.FileSizeString+'KB');
                row.find("#att_filetype").text(n.FileType);
                row.find("#att_uptime").text(n.UploadTime);
                row.find("#att_oper2").html(oper2);

                //添加样式
                row.addClass("cls_TableRow");
                row.appendTo("#datas_att");
                //缩略图
                thumd.appendTo("#img_container");
                if (i % 3 == 0) {
                    $("img_container").append("<br/>")
                }
                //隐藏原始图
            });
            filedata = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
           // alert("系统错误！");
        }
    });
}
