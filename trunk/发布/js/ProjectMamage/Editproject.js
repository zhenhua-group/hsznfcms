﻿var projectNameRepeat = false;
var jffzr_strWhere_Serch="";
$(document).ready(function () {

    var regOne =/[\\/:\*\?\""<>|]/;
    var regTwo=/^[^.]/;
    //检查项目名称是否重复
    $("#txt_name").blur(function () {
        var projectName = $.trim($(this).val());
         if (regOne.test(projectName)) {
            alert("项目名称不能包括以下任何字符！\/:\*\?\"<>|");
            projectNameRepeat=true;
            return false;
        }
         if (!regTwo.test(projectName)) {
            alert("项目名称不能为空!");
            projectNameRepeat=true;
            return false;
        }
        if (projectName.length > 0) {
            $.post("/HttpHandler/ProjectMgr/ExistsInTGProjectHandler.ashx", { "projectname": projectName }, function (result) {
                if (result == "1") {
                    projectNameRepeat = true;
                    alert("项目名称已存在，请重新输入新项目名称！");
                } else {
                    projectNameRepeat = false;
                }
            });
        }
    });
    //绑定权限
    showDivDialogClass.UserRolePower={
        "previewPower":$("#previewPower").val(),
        "userSysNum" :$("#userSysNum").val(),
        "userUnitNum":$("#userUnitNum").val(),
        "notShowUnitList": ""
    };
    //项目ID
    $("#hid_projid").val(hid_projid);
    //甲方负责人
    $("#sch_Aperson").click(function () {
         $("#txtName").val("");
        $("#txtPhone").val("");
        $("#txtCompName").val("");
        jffzr_strWhere_Serch="";
        $("#jffzr_table tr:gt(0)").remove();
        //先赋值
        showDivDialogClass.SetParameters({
            "pageSize":"5"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog","","true","1","jffzrCpr",JffzrCallBack);
        BindAllDataCountJffzr();//绑定总数据
        $("#jffzr_DialogDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 550,
            height:310,
            resizable: false,
            title: "甲方负责人"
        }).dialog("open");
    });
    //甲方搜索
     $("#btn_serch").click(function () {
       var str_name = $("#txtName").val();
        var str_phone = $("#txtPhone").val();
        var str_cpyName = $("#txtCompName").val();
        var strWhere = "";
        strWhere += "&Name=" + str_name+"&Phone=" + str_phone+"&Department=" + str_cpyName;
        jffzr_strWhere_Serch=strWhere;
        jffzr_strWhere_Serch=showDivDialogClass.ReplaceChars(jffzr_strWhere_Serch);
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog",jffzr_strWhere_Serch,"true","1","jffzrCpr",JffzrCallBack);
        BindAllDataCountJffzr();//绑定总数据
    });
    //合同关联
    $("#sch_reletive").click(function () {
        //加载数据
       $("#TxtCprName").val("");
        //先赋值
        showDivDialogClass.SetParameters({
            "nowIndex":"reletiveCpr_nowPageIndex",//当前页id
            "pageSize":"10"
        });
        //绑定生产部门数据
        GetReleCprUnit();
        //绑定项目数据
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog","","true","1","reletiveCpr",ReletiveCprCallBack);
        BindAllDataCountReleCpr();//绑定总数据
        $("#cprReletiveDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 580,
            height:400,
            resizable: false,
            title: "合同关联"
        }).dialog("open");

    });
     //合同关联-搜索
    $("#btn_SearchByCprName").click(function () {
        var strNameUnit="";
        var cprName=$("#TxtCprName").val();//当做where语句
        cprName=showDivDialogClass.ReplaceChars(cprName);
        strNameUnit+=cprName+"|";
        var unitID=$("#select_releCprUnit option:selected").val();
         var unitName = $("#select_releCprUnit option:selected").text();

    if (unitID > 0) {
            strNameUnit += unitID + "|";
            strNameUnit += unitName;
        }
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog",strNameUnit,"true","1","reletiveCpr",ReletiveCprCallBack);
        BindAllDataCountReleCpr();//绑定总数据
    });
    //承接部门
    $("#sch_unit").click(function () {

        //设置当前层的所需ID
         showDivDialogClass.SetParameters({
        "prevPage":"cjbm_prevPage",
        "firstPage":"cjbm_firstPage",
        "nextPage":"cjbm_nextPage",
        "lastPage":"cjbm_lastPage",
        "gotoPage":"cjbm_gotoPageIndex",
        "allDataCount":"cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex", 
        "allPageCount": "cjbm_allPageCount", 
        "gotoIndex":"cjbm_pageIndex",
            "pageSize":"10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog","","false","1","proCjbm",ProCjbmCallBack);
        BindAllDataCount();//绑定总数据
        $("#cpr_cjbmDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height:320,
            resizable: false,
            title: "承接部门"
        }).dialog("open");
    });

    $("#sch_PM").click(function () {
        $("#chooseUserMain").dialog({
            autoOpen: false,
            modal: true,
            width: 600,
            height: "auto",
            resizable: false,
            title: "选择执行设总"
        }).dialog("open");
        return false;
    });

    //行变色
    $(".show_project>tbody>tr:odd").attr("style", "background-color:#FFF");
    //保存
    $("#btn_imgsave").click(function () {
        //项目名称
        var name = $("#txt_name").val();
        //建筑类别
        var structtype = $("#drp_buildtype").val();
        //项目来源
        var source = $("#ddsource").val();
        //合同额
        var acount = $("#txtproAcount").val();
        //建设单位
        var txtbuildUnit = $("#txtbuildUnit").val();
        //承接部门
        var txt_unit = $("#txt_unit").val();
        //建设地点
        var txtbuildAddress = $("#txtbuildAddress").val();
        //建设规模
        var txt_scale = $("#txt_scale").val();
        //行业性质
        var professionType=$("#ddProfessionType").val();
        //甲方负责人
        var txt_Aperson = $("#txt_Aperson").val();
        //甲方负责人电话
        var txt_phone = $("#txt_phone").val();
        //开始时间
        var txt_startdate = $("#txt_startdate").val();
        //结束时间
        var txt_finishdate = $("#txt_finishdate").val();
        //项目特征概况
        var txt_sub = $("#txt_sub").val();
        //项目备注
        var txt_remark = $("#txt_remark").val();
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

        var msg = "";
        if (name == "") {
            msg += "请输入项目名称！</br>";
        }
        //管理级别
        if ($("#radio_yuan").get(0).checked == false && $("#radio_suo").get(0).checked == false) {
            msg += "请选择项目的管理级别！</br>";
        }
        //审核级别
        if ($(".cls_audit :checkbox[checked=checked]").length == 0) {
            msg += "请至少选择一项审核级别！</br>";
        }
        //建设单位
        if (txtbuildUnit == "") {
            msg += "请输入建设单位！</br>";
        }
       
        //建设地点
        if (txtbuildAddress == "") {
            msg += "请输入建设地点！</br>";
        }
        //建设规模
        if (txt_scale == "") {
            msg += "请输入建设规模！</br>";
        }
        else {
            if (!reg_math.test(txt_scale)) {
                msg += "建设规模请输入数字！</br>";
            }
        }
         //建筑类别
        if (structtype == "-1") {
            msg += "请选择建筑类别！</br>";
        }
         //承接部门
        if (txt_unit == "") {
            msg += "请选择承接部门！</br>";
        }
        //结构形式

        if (!IsStructCheckNode('struct')) {
            msg += "请选择结构形式！</br>";
        }
        //建筑分类
        if (!IsStructCheckNode('structtype')) {
            msg += "请选择建筑分类！</br>";
        }
        //设计阶段
        if ($("span[class=cls_jd]> :checkbox[checked=checked]").length == 0) {
            msg += "请至少选择一项设计阶段！</br>";
        }
        //项目来源
        if (source == "0") {
            msg += "请选择项目来源！</br>";
        }
        //合同额
        if (acount != "") {
            if (!reg_math.test(acount)) {
                msg += "合同额请输入数字！</br>";
            }
        }
        else {
            $("#txtproAcount").val("0");
        }

        //行业性质
        if(professionType=="0")
        {
          msg += "请选择行业性质！</br>";
        }

        //甲方负责人
        if (txt_Aperson == "") {
            msg += "请输入甲方负责人！</br>";
        }
        //电话
        if (txt_phone == "") {
            msg += "请输入甲方负责人电话！</br>";
        }
        if ($.trim($("#txt_PMName").val()).length == 0) {
            msg += "执行设总不能为空！<br/>";
        }
        //项目开始日期
        if (txt_startdate == "") {
            msg += "项目开始日期不能为空！</br>";
        }
        //项目结束日期
        if (txt_finishdate == "") {
            msg += "项目完成日期不能为空！</br>";
        }

        //比较时间
        var result = duibi(txt_startdate, txt_finishdate);
        if (result == false) {
            msg += "项目开始时间大于结束时间！";
        }
        
        if (projectNameRepeat) {
            msg += "项目名称不符合填写规范,不能创建项目！<br/>";
        }
        if ($.trim($("#txt_sub").val()).length == 0) {
            msg += "项目特征概况不能为空！<br/>";
        }
        else if ($("#txt_sub").val().length > 500) {
            msg += "项目特征概况不能超过500字！<br/>";
        }
        //提示
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        } else {
            return true;
        }
    });
    //加载附件信息
    LoadCoperationAttach();
    //管理级别(院)
    $("#radio_yuan").click(function () {
        $("#audit_yuan").attr("checked","checked");
        $("#audit_suo").attr("checked",false).hide().next('label').hide();
    });
    //管理级别(所)
    $("#radio_suo").click(function () {
        $("#audit_yuan").attr("checked",false);
        $("#audit_suo").attr("checked","checked").show().next('label').show();
    });
    //选择所
    if ($("#radio_yuan").get(0).checked) {
        $("#audit_suo").attr("checked",false).hide().next('label').hide();
    }

    //只能选择控制
    $(".cls_input_text_onlyslt").focus(function () {
        $(this).blur();
    });
    //判断当前部门
    var curunitid=$("#userUnitNum").val();
    //经济所
    if (curunitid=='248') {
        $("#chk_ISTrunEconomy").hide().next('label').hide();
    }
    else if (curunitid=='247') {//暖通所
        $("#chk_ISHvac").hide().next('label').hide();
    }
    else{//土建所
        $("#chk_ISArch").hide().next('label').hide();
    }

    //结构样式
    CommonControl.BindASTreeView("asTreeviewStruct_ulASTreeView", $("#structstring").val(), $("#StrructContainer"));
    //建筑分类
    CommonControl.BindASTreeView("asTreeviewStructType_ulASTreeView", $("#structtypestring").val(), $("#buildTypeContainer"));

});
//比较时间
function duibi(a, b) {
    var arr = a.split("-");
    var starttime = new Date(arr[0], arr[1], arr[2]);
    var starttimes = starttime.getTime();

    var arrs = b.split("-");
    var lktime = new Date(arrs[0], arrs[1], arrs[2]);
    var lktimes = lktime.getTime();

    if (starttimes >= lktimes) {
        return false;
    }
    else
        return true;

}

function GetCoperationLevel(id) {
    //地址
    var url = "../HttpHandler/SimpleHandler.ashx";
    $.get(url, { flag: "getcprlevel", id: id }, function (data) {
        if (data == "0") {
            $("#radio_yuan").atrr("checked", "checked");
        }
        else if (data == "1") {
            $("#radio_suo").atrr("checked", "checked");
        }
    });
}
//加载数据
function GetAttachData() {
    LoadCoperationAttach();
}
//加载附件信息
function LoadCoperationAttach() {
    var data_att = "action=getprojfiles&type=proj&projid=" + $("#hidproId").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result == null ? "" :result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                //缩略图列表
                if ($("#img_container img").length > 1) {
                    $("#img_container img:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    //显示
                    $("#img_small").show();
                    //克隆
                    var thumd = $("#img_small").clone();
                    //隐藏
                    $("#img_small").hide();
                  var oper = "<span style='color:blue;cursor:pointer;' rel='" + n.ID + "'>删除</span>";
                    var oper2 = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + n.FileName + "&FileURL=" + n.FileUrl + "\" target='_blank'>下载</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    //显示缩略图
                    var img_path = n.FileUrl.split('/');
                    thumd.attr("src", "../Attach_User/filedata/projfile/" + img_path[0] + "/min/" + img_path[1]);
                    thumd.attr("alt", n.ID);

                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").attr("align","left").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper span").click(function () {
                        if (confirm("确定要删除本条附件吗？")) {
                            delCprAttach($(this));
                        }
                    });
                    //改变样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                    //缩略图
                    thumd.appendTo("#img_container");
                    if (i % 3 == 0) {
                        $("img_container").append("<br/>")
                    }
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("加载附件信息错误！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                link.parents("tr:first").remove();
                //删除缩略图
                $("#img_container img[alt=" + link.attr("rel") + "]").remove();
                alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//绑定页面上的关联值
function BindCprReletive(n) {
    //合同名称
    $("#txt_reletive").val(n.cpr_Name);
    //弱关联合同名称
    $("#txt_name").val(n.cpr_Name);
    //关联合同ID
    $("#txtcpr_id").val($.trim(n.cpr_Id));
    //触发onBlur事件，检查项目名称是否存在
    $("#txt_name").focus();
}

//2013-09-17 修改
//承接部门表格数据绑定-CallBack函数
function ProCjbmCallBack(result){
    if(result!=null){
     var data=result.ds;
     $("#cpr_cjbmTable tr:gt(0)").remove();
     $.each(data, function (i, n) {
        var oper = "<span  rel='" + n.unit_ID + "' style=\"color:blue;cursor: pointer;\">选择</span>";
        var trHtml = "<tr style='text-align:center'><td>" + (i+1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
        $("#cpr_cjbmTable").append(trHtml);
        $("#cpr_cjbmTable span:last").click(function () {
            $("#txt_unit").val($.trim(n.unit_Name));
            $("#hid_unit").val($.trim(n.unit_Name));
            $("#cpr_cjbmDiv").dialog().dialog("close");
        });
    });
    ControlTableCss("cpr_cjbmTable");
    }
}
//获得承接部门数据总数
function BindAllDataCount(){
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage":"cjbm_prevPage",
        "firstPage":"cjbm_firstPage",
        "nextPage":"cjbm_nextPage",
        "lastPage":"cjbm_lastPage",
        "gotoPage":"cjbm_gotoPageIndex",
        "allDataCount":"cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex", 
        "allPageCount": "cjbm_allPageCount", 
        "gotoIndex":"cjbm_pageIndex",
        "pageSize":"10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm",GetCjbmAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex=$("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog","","false",pageIndex,"proCjbm", ProCjbmCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
    });
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result){
    if(result>0){
        showDivDialogClass.BindPageValueFirst(result);
    }else{
        NoDataMessageOnTable("pro_cjbmTable",3);
    }
}

//获取生产部门
function GetReleCprUnit(){
    var  previewPower=showDivDialogClass.UserRolePower.previewPower;
    var  userSysNum=showDivDialogClass.UserRolePower.userSysNum;
    var  userUnitNum=showDivDialogClass.UserRolePower.userUnitNum;
    var data="action=getReleCprUnit&previewPower="+previewPower+"&userSysNum="+userSysNum+"&userUnitNum="+userUnitNum;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success:function(result){
        if(result!=null){
            var data=result.ds;
            var optionHtml='<option value="-1">-----全院部门-----</option>';
            $.each(data, function (i, n) {
                 optionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
            });
            $("#select_releCprUnit").html(optionHtml);

            $("#select_releCprUnit").unbind('change').change(function(){
                 var strNameUnit="";
                 var cprName=$("#TxtCprName").val();//当做where语句
                 cprName=cprName.replace("#","");
                 strNameUnit+=cprName+"|";
                 var unitID=$("#select_releCprUnit option:selected").val();
                  var unitName = $("#select_releCprUnit option:selected").text();
//                 if(unitID>0){
//                    strNameUnit+=unitID;
//                 }
//                 strNameUnit += "|" + unitName;
  if (unitID > 0) {
            strNameUnit += unitID + "|";
            strNameUnit += unitName;
        }
                 showDivDialogClass.GetDataByAJAX("getDataToDivDialog",strNameUnit,"true","1","reletiveCpr",ReletiveCprCallBack);
                 BindAllDataCountReleCpr();//绑定总数据
            });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误!");
        }
    });
}
//合同关联绑定表格数据-CallBack
function ReletiveCprCallBack(result){
if(result!=null){
    var cprName = $("#TxtCprName").val();
    var data = result.ds;
    var releviteCprHtml;
    $("#cprReletiveTable tr:gt(0)").remove();
    $.each(data, function (i, n) {
        var signdate=GetShortDate(n.cpr_SignDate);
        var oper = "<span rel='" + n.cpr_Id + "' style=\"color:blue;cursor: pointer;\">选择</span>";
        var cprNameResult = n.cpr_Name;
        if (cprNameResult.length > 20) {
            cprNameResult = cprNameResult.substr(0, 20) + "...";
        }
        cprNameResult=cprNameResult.replace(cprName,"<span style='color:red'>"+cprName+"</span>");
        releviteCprHtml = '<tr><td title="'+n.cpr_Name+'">' + cprNameResult + '</td><td align=\"center\">' + signdate + '</td><td align=\"center\">' + oper + '</td></tr>';
        $("#cprReletiveTable").append(releviteCprHtml);
        $("#cprReletiveTable span:last").click(function () {
            //绑定关联值
            BindCprReletive(n);
            $("#cprReletiveDiv").dialog().dialog("close");
        });
    });
    ControlTableCss("cprReletiveTable");
    }
}
//获得合同关联数据总数
function BindAllDataCountReleCpr(){
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage":"reletiveCpr_prevPage",
        "firstPage":"reletiveCpr_firstPage",
        "nextPage":"reletiveCpr_nextPage",
        "lastPage":"reletiveCpr_lastPage",
        "gotoPage":"reletiveCpr_gotoPage",
        "allDataCount":"reletiveCpr_totalCount",
        "nowIndex": "reletiveCpr_nowPageIndex", 
        "allPageCount": "reletiveCpr_PagesCount", 
        "gotoIndex":"reletiveCpr_gotoPageNum",
        "pageSize":"10"
    });
    var strNameUnit=$("#TxtCprName").val()+"|";
    var unitID=$("#select_releCprUnit option:selected").val();
     var unitName = $("#select_releCprUnit option:selected").text();

//        if(unitID>0){
//        strNameUnit+=unitID;
//    }
//       strNameUnit += "|" + unitName;
  if (unitID > 0) {
            strNameUnit += unitID + "|";
            strNameUnit += unitName;
        }
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", strNameUnit, "true", "reletiveCpr",GetReleCprAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cprReletivePage span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex=$("#reletiveCpr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog",strNameUnit,"true",pageIndex,"reletiveCpr", ReletiveCprCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
    });
}
//合同关联数据总数CallBack函数
function GetReleCprAllDataCount(result){
    if(result>0){
        showDivDialogClass.BindPageValueFirst(result);
    }else{
        $("#cprReletiveTable tr:gt(0)").remove();
        $("#reletiveCpr_totalCount").text(0);
        $("#reletiveCpr_nowPageIndex").text(0);
        $("#reletiveCpr_PagesCount").text(0);
        NoDataMessageOnTable("cprReletiveTable",3);
    }
}
//甲方负责人绑定表格数据CallBack
function JffzrCallBack(result){
if(result!=null){
    var data = result.ds;
    var jffzrHtml;
    $("#jffzr_table tr:gt(0)").remove();
    $.each(data, function (i, n) {
        var oper = "<span rel='" + n.Cst_Id + "'style=\"color:blue;cursor: pointer;\">选择</span>";
        jffzrHtml = '<tr><td>' + n.Name + '</td><td>' + n.Phone + '</td><td>' + n.Department + '</td><td>' + oper + '</td></tr>';
        $("#jffzr_table").append(jffzrHtml);
        $("#jffzr_table span:last").click(function () {
            $("#txt_Aperson").val($.trim(n.Name));
            $("#txt_phone").val($.trim(n.Phone));
            $("#jffzr_DialogDiv").dialog().dialog("close");
        });
    });
    ControlTableCss("jffzr_table");
    }
}
//获取甲方负责人总数据
function BindAllDataCountJffzr(){
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage":"jffzr_prevPage",
        "firstPage":"jffzr_firstPage",
        "nextPage":"jffzr_nextPage",
        "lastPage":"jffzr_lastPage",
        "gotoPage":"jffzr_gotoPage",
        "allDataCount":"jffzr_totalCount",
        "nowIndex": "jffzr_nowPageIndex", 
        "allPageCount": "jffzr_PagesCount", 
        "gotoIndex":"jffzr_gotoPageNum",
        "pageSize":"5"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", jffzr_strWhere_Serch, "true", "jffzrCpr",GetJffzrAllDataCount);

     //注册分页事件
    $("#jffzr_forPageDiv span").unbind('click').click(function () {
        var isRegex =  showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex=$("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog",jffzr_strWhere_Serch,"true",pageIndex,"jffzrCpr",JffzrCallBack);
        }
    });
}
//甲方负责人数据总数CallBack函数
function GetJffzrAllDataCount(result){
    if(result>0){
        showDivDialogClass.BindPageValueFirst(result);
    }else{
        $("#jffzr_table tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_table",4);
    }
}

//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//转换为短日期格式
function GetShortDate(dateString){
    //var pattern = new RegExp("[ ]");
  if (dateString != "" && dateString != null) {
        var index=10;
        if(dateString.lastIndexOf("-")>0){
            index=dateString.lastIndexOf("-")+3;
        }else if(dateString.lastIndexOf("/")>0){
            index=dateString.lastIndexOf("/")+3;
        }
        return dateString.substr(0,index);
    }else {
        return "无";
    }
     /*
if (dateString != "" || dateString != null) {
        var date = new Date(dateString);
        return date.getFullYear().toString() + "-" + (date.getMonth() + 1).toString() + "-" + date.getDate().toString();
    } else {
        return "无";
    } */
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId){
    $("#"+tableId+" tr:gt(0):odd").attr("class", "trOddColor");
    $("#"+tableId+" tr:gt(0)").hover(function () {
         $(this).addClass("mouseOverColor");}, function () {
         $(this).removeClass("mouseOverColor");
   });
}


