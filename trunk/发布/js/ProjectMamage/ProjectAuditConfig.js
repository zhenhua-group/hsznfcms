﻿function ProjectAuditConfig (container) {
	this.Container = container;
	this.Dom ={};
	this.BackgroundInvoke = TG.Web.ProjectManage.ProjectAudit.ProjectAuditConfig;
	var Instance = this;
	
	this.Dom.UserSpan =$("span[id=userSpan]",container);
	
	this.Dom.UserSpan.DeleteActionLink =$("img[name=deleteUserImgActionBtn]",container);
	
	//bind事件
	this.Dom.UserSpan.live("mousemove",function(){
		$(this).find("img:first").show();
	}).live("mouseout",function(){
		$(this).find("img:first").hide();
	});
	
	//删除用户
	this.Dom.UserSpan.DeleteActionLink.live("click",function(){
		if($(this).parents("li:first").children("span").length >1){
			if(confirm("您确认是否删除该用户？")){
				//取得该Img父级同辈所有usreSpan
				var userSpans = $(this).parents("span[id=userSpan]:first").siblings("span[id=userSpan]");
				var userSysNoString = "";
				$.each(userSpans,function(index,item){
					userSysNoString+=$(item).attr("usersysno")+",";			
				});
				userSysNoString = userSysNoString.substring(0,userSysNoString.length -1);
				
				//取得RoleSysNo
				var roleSysNo =userSpans.parents("li:first").siblings("li[id=roleSysNoliAction]").attr("rolesysno");
				
				//调用后台方法
				var result = Instance.BackgroundInvoke.UpdateRoleUsers(userSysNoString,roleSysNo);
				if(parseInt( result.value,10)<= 0 ){
					alert("修改用户所对应角色失败！");
				}else{
					$(this).parents("span[id=userSpan]:first").remove();
				}
			}
		}
	});
	
	var selectRoleUser = new SelectRoleUser($("#DivAddRoleUser"));
	
	//选择完用户处理事件
	this.ProcessSelectedUserDone = function(selectedUserCheckboxs,img){
		//取得该Li下所有的用户
		var userSpanli = $(img).parents("li:first").siblings("li[userli=true]");
		//判断是否有重复项存在
		$.each(selectedUserCheckboxs,function(index,checkBox){
			var flag = true;
			$.each(userSpanli.children("span[id=userSpan]"),function(index1,item){
				if($(item).attr("usersysno") == $(checkBox).val()){
					flag = false;
					return false;
				}
			});
			//判断是否有重复的人员
			if(flag == true){
				var spanString = "<span id=\"userSpan\" usersysno=\""+$(checkBox).val()+"\"  style=\"margin-right: 5px;\">"+$(checkBox).attr("username")+"<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer; display: none;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
				$(img).parents("li:first").siblings("li[userli=true]").append(spanString);
			}
		});	
		//保存数据到数据库
		//取得所有用户		
		var userSysNoString = "";
		$.each(userSpanli.children("span[id=userSpan]"),function(index,item){
			userSysNoString += $(item).attr("usersysno") +",";
		});
		
		userSysNoString = userSysNoString.substring(0,userSysNoString.length -1);
		
		var roleSysNo = userSpanli.siblings("li[id=roleSysNoliAction]").attr("rolesysno");
		var result = Instance.BackgroundInvoke.UpdateRoleUsers(userSysNoString,roleSysNo);
		//成功的场合
		if(result.value !=null && result.value.length > 0)
		{
			if(parseInt(result.value,10) <= 0){
				alert("添加用户失败，请联系管理员！");
			}
		}
	}
	
	//添加用户ImageButton
	this.Dom.AddUserButton = $("img[id=addRoleUserBtn]");
	
	this.Dom.AddUserButton.live("click",function(){
		var img = $(this);
		
		selectRoleUser.Clear();
		
		$("#DivAddRoleUser").dialog({
			autoOpen: false,
			modal: true,
			width:550,
			height:400,
			resizable :false,
			buttons:
			{
				"确定":function(){
					//调用处理事件
					selectRoleUser.SelectedUserDone(Instance.ProcessSelectedUserDone,img);
					$(this).dialog("close");
				},
				"取消" : function(){$(this).dialog("close");}
			}
		}).dialog("open");
		
		selectRoleUser.Init();
	});
	//流程对应角色Span
	this.RoleNameSpan =$("span[id=roleNameSpan]",container);
	
	//拼接用户Span字符串
	this.JoinUserSpans = function(result){
		var userList = Global.evalJSON(result.value);
		var userSpansString ="";
		$.each(userList,function(index,user){
			userSpansString+="<span id=\"userSpan\" usersysno=\""+user.mem_ID+"\" style=\"margin-right: 5px;\">" +user.mem_Name;
			userSpansString+="<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer; display: none; margin-left: 5px;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
		});
		return userSpansString;
	}
	
	//选择角色下拉框失去焦点处理事件
	this.RoleSelectEvent = function(){
		var instanceSelect = $(this);
		
		var instanceRoleSysNo = instanceSelect.parent("li:first").attr("rolesysno");
		//取得改变之后的RoleSysNo
		var otherRoleSysNo =instanceSelect.val();
		
		instanceSelect.hide().siblings("span[id=roleNameSpan]").text(instanceSelect.children("option[value="+otherRoleSysNo+"]").text()).show();
		var otherLi =$("li[id=roleSysNoliAction][rolesysno="+otherRoleSysNo+"]");
		otherLi.children("span[id=roleNameSpan]").text(instanceSelect.children("option[value="+instanceRoleSysNo+"]").text());
		
		//取得当前的UserSpan和互斥的UserSpan
		var instanceUserSpanString = Instance.JoinUserSpans(Instance.BackgroundInvoke.GetRoleUsersList(instanceRoleSysNo));
		var otherUserSpanString = Instance.JoinUserSpans(Instance.BackgroundInvoke.GetRoleUsersList(otherRoleSysNo));
		
		//交换UserSpanString
		instanceSelect.parent("li:first").siblings("li[userli=true]").html(otherUserSpanString);
		$("li[id=roleSysNoliAction][rolesysno="+otherRoleSysNo+"]").siblings("li[userli=true]").html(instanceUserSpanString);
		
		//改变Li的RoleSysNo
		instanceSelect.parent("li[id=roleSysNoliAction]:first").attr("rolesysno",otherRoleSysNo);
		otherLi.attr("rolesysno",instanceRoleSysNo);
		
		//封装数据，修改
		//当前选择的和原来的没有发生改变的场合。不发送修改请求
		if(parseInt(instanceRoleSysNo,10) != parseInt(otherRoleSysNo,10)){
			var dataArray = new Array();
			dataArray[0]=
			{
				AuditConfigSysNo : instanceSelect.attr("projectauditconfigsysno"),
				RoleSysNo : otherRoleSysNo
			};
			
			if(otherLi.length >0){
				dataArray[1]=
				{
					AuditConfigSysNo : otherLi.children("select[id=roleDropDownList]:first").attr("projectauditconfigsysno"),
					RoleSysNo : instanceRoleSysNo
				};
			}
			
			var jsonData = Global.toJSON(dataArray);
			
			var result = Instance.BackgroundInvoke.UpdateCoperationAuditConfigRoleRelationship(jsonData);
			
			if(result.value !=null && result.value.length > 0){
				if(parseInt(result.value,10)<=0){
					alert("修改流程所对应的角色失败，请联系系统管理员！");
				}
			}
		}
	}
	this.RoleNameSpan.live("click",function(){
		var roleSysNo = $(this).parent("li:first").attr("rolesysno");
		//修改显示状态
		$(this).hide().siblings("select:first").show().val(roleSysNo).bind("blur",Instance.RoleSelectEvent);
	});
}