﻿var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;
//项目审核类
function ProjectAuditClass(container) {
    this.Container = container;
    this.Dom = {};
    this.BackgroundInvoke = TG.Web.ProjectManage.ProjectAudit.ProjectAudit;
    this.JsonDataEntity = "";
    this.messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程评审人！");
                    return false;
                }

                Instance.getUserAndUpdateAudit('1', '1', Instance.JsonDataEntity);
            }
             ,
            "关闭": function () {
                Instance.messageDialog.hide();
            }
        }
    });
    this.sendMessageClass = new MessageCommon(this.messageDialog);
    var Instance = this;
    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();
    this.ProjectAuditSysNo = $("#ProjectAuditSysNoHidden").val();
    this.ProjectStatus = "";
    this.UserSysNo = $("#UserSysNoHidden", container).val();
    this.MessageStatus = $("#hiddenMessageStatus").val();

    //同意审核按钮
    this.Dom.AgreeButton = $("#agreeButton");
    this.Dom.DisAgreeButton = $("#disAgreeButton");

    //禁用所有的HTML控件
    this.DisableHTMLControl = function () {
        $(":text,:checkbox,textarea,select", container).attr("disabled", true);
        $("textarea[id=txt_view]").attr("disabled", false);
    }

    //动态创建审核TR
    this.CreateAuditTR = function (disabled, auditContent, hasPower, processDescription, auditDate) {
        var disabledString = disabled == true ? "disabled=\"disabled\"" : "";
        var trString = "";
        trString += "<table class=\"show_project\">";
        trString += "<tr>";
        trString += "<td style=\"width:200px;\" align=\"center\">"
        trString += "<br/>" + processDescription + "&nbsp;意见：";
        trString += "</td>";
        trString += "<td colspan=\"2\">";
        trString += "<textarea id=\"txt_view\" " + disabledString + " style=\"height: 68px; width: 800px;border: solid 1px #CCC;\">" + auditContent + "</textarea>";
        trString += "&nbsp;";
        trString += "</td>";
        trString += "<td>";
        trString += auditDate;
        trString += "</td>";
        trString += "</tr>";
        //需要Button的场合
        if (disabled == false) {
            trString += "<tr>";
            trString += "<td style=\"height: 50px; background-color: #f0f0f0;\" colspan=\"4\" align=\"center\">";
            trString += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            if (hasPower == false) {
                trString += "您没有权限评审或已经评审完成！";
            } else {
                trString += "<input type=\"button\" id=\"agreeButton\" class=\"cls_btn_comm_w\" value=\"通过\" />";
                trString += "&nbsp;<input type=\"button\" id=\"disAgreeButton\" class=\"cls_btn_comm_w\" value=\"不通过\" />";
                trString += " &nbsp;<input type=\"button\" class=\"cls_btn_comm_w\" id=\"btnback\" name=\"controlBtn\" value=\"返回\" onclick=\"javascript:history.back();\" />"
            }

            trString += "</td>";
            trString += "</tr>";
        }
        trString += "</table>";
        return trString;
    }
    //当前时间
    this.getNowDateString = function () {
        var date = new Date();
        var now = "";
        now = date.getFullYear() + "-";
        now = now + (date.getMonth() + 1) + "-";
        now = now + date.getDate() + " ";
        now = now + date.getHours() + ":";
        now = now + date.getMinutes() + ":";
        now = now + date.getSeconds() + "";
        return now;
    }
    //初始化绑定页面下拉框。
    this.BindDataForControl = function () {
        //取得项目类型情报
        var projectKind = $("#projectKindHidden", container).val();
        //取得管理级别情报
        $("#projectLevel option[value=" + $("#projectLevel", container).attr("projectlevel") + "]", container).attr("selected", true);
        //取得项目来源信息
        $("#ddsource option[value=" + $("#ddsource", container).attr("projectSrc") + "]", container).attr("selected", true);
    }


    //初始化方法
    this.Init = function () {
        //取得审核角色名称数组
        var roleNameArrayString = Instance.BackgroundInvoke.GetAuditProcessRoleName();
        var roleNameArray = new Array();
        if (roleNameArrayString.value != null && roleNameArrayString.value.length > 0) {
            roleNameArray = Global.evalJSON(roleNameArrayString.value);
        }

        Instance.DisableHTMLControl();
        //查询项目审核信息
        var result = Instance.BackgroundInvoke.GetProjectAuditInfo(Instance.ProjectAuditSysNo);
        var projectAuditObj = Global.evalJSON(result.value);

        //得到审核用户名称数组
        var userNameString = Instance.BackgroundInvoke.GetAuditUserNameArrayString(projectAuditObj.AuditUser);
        var userNameArray = new Array();
        if (userNameString.value != null && userNameString.value.length > 0) {
            userNameArray = Global.evalJSON(userNameString.value);
        }
        //得到审批时间
        var userAuditDate = projectAuditObj.AudtiDate;
        if (userAuditDate == null) {
            userAuditDate = Instance.getNowDateString() + ",";
        }
        userAuditDate = userAuditDate.split(',');
        //创建表格
        Instance.ProjectStatus = projectAuditObj.Status;
        var fdString = "";

        //审批fieldset

        if (projectAuditObj.Suggestion != null && projectAuditObj.Suggestion.length > 0) {
            var fdString = "<fieldset style=\"font-size:12px;\"><legend>评审意见</legend>";
            //取得审核内容
            var arrAuditContent = projectAuditObj.Suggestion.substring(0, projectAuditObj.Suggestion.length - 1).split("|");

            if ((this.MessageStatus == "C" || this.MessageStatus == "A") && (this.MessageStatus != projectAuditObj.Status)) {
                var processDescriptionString = roleNameArray[0] + ":<br/>" + userNameArray[0];
                var auditDate = userAuditDate;
                fdString += Instance.CreateAuditTR(true, arrAuditContent[0], true, processDescriptionString, auditDate);
            }
            else if ((this.MessageStatus == "B" || this.MessageStatus == "E") && (this.MessageStatus != projectAuditObj.Status)) {
                var processDescriptionString = roleNameArray[1] + ":<br/>" + userNameArray[1];
                var auditDate = userAuditDate[1];
                fdString += Instance.CreateAuditTR(true, arrAuditContent[1], true, processDescriptionString, auditDate);
            }
            else {
                $.each(arrAuditContent, function (index, item) {
                    var processDescriptionString = roleNameArray[index] + ":<br/>" + userNameArray[index];
                    var auditDate = userAuditDate[index];
                    fdString += Instance.CreateAuditTR(true, item, true, processDescriptionString, auditDate);
                });
            }
            fdString += "</fieldset>";
            $("#auditHtml").append(fdString);
        }
        if (projectAuditObj.Status == this.MessageStatus) {

            if (projectAuditObj.Status != "D" && projectAuditObj.Status != "E" && projectAuditObj.Status != "C") {

                //取得是否有权限审核该项目
                var hasPower = parseInt(Instance.BackgroundInvoke.CheckPower(Instance.UserSysNo, Instance.ProjectAuditSysNo).value, 10);
                //当前时间
                var auditNowDate = Instance.getNowDateString();
                var fdString = "<fieldset style=\"font-size:12px;\"><legend>评审意见</legend>";
                fdString += Instance.CreateAuditTR(false, "", hasPower, "", auditNowDate);

                fdString += "</fieldset>";
                $("#auditHtml").append(fdString);
            }
        }


        //绑定数据
        Instance.BindDataForControl();
    }

    Instance.Init();

    //验证
    this.Vaildation = function (container) {
        var remark = $("textarea[id=txt_view]", container).val();
        var msg = "";
        if (remark == "") {
            msg += "请填写意见！";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    }

    this.Dom.AgreeButton.live("click", function () {

        var textareeParentPanel = $(this).parents("table:first");
        //调用验证方法
        Instance.Vaildation(textareeParentPanel);
        var objData =
		{
		    SysNo: Instance.ProjectAuditSysNo,
		    Suggestion: $.trim($("textarea[id=txt_view]", textareeParentPanel).val()),
		    Status: Instance.ProjectStatus
		};

        if ($.trim($("textarea[id=txt_view]", textareeParentPanel).val()).length > 0) {
            var queryString = Global.toJSON(objData);
            Instance.JsonDataEntity = queryString;

            if (Instance.ProjectStatus == "B") {
                Instance.getUserAndUpdateAudit('1', '1', Instance.JsonDataEntity);
            }
            else {
                Instance.getUserAndUpdateAudit('1', '0', Instance.JsonDataEntity);
            }
            Instance.setAuditBtnDisable(true);
        }
    });
    //查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
    //显示用户列表发送消息更新流程
    this.getUserAndUpdateAudit = function (action, flag, jsonData) {
        //提交地址
        var result = Instance.BackgroundInvoke.DoAgree(jsonData, flag);
        //改变消息状态
        if (flag == "1") {
            var msg = new MessageCommProj($("#msgno").val());
            msg.ReadMsg();
        }
        //提交数据
        if (result.value == "0") {
            alert("发起项目评审失败，请联系系统管理员！");
        }
        else if (result.value == "1") {
            alert("项目评审完成，项目信息已经同步到TCD中！");
            //window.history.back();
            //window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=A&pageIndex=" + index;
            window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            Instance.renderUserOrSendMsg(flag, result.value);
        }
    }
    //显示用户OR更新审批状态
    this.renderUserOrSendMsg = function (flag, jsonResult) {
        var obj = eval("(" + jsonResult + ")");
        if (flag === "0") {
            Instance.sendMessageClass.render(obj.UserList, obj.RoleName);
        }
        else {
            Instance.sendMessageClass.setMsgTemplate(obj);
            Instance.sendMessageClass.chooseUserForMessage(Instance.sendMessage);
        }
    }
    //设置审批按钮
    this.setAuditBtnDisable = function (flag) {
        Instance.Dom.AgreeButton.attr("disabled", flag);
        Instance.Dom.DisAgreeButton.attr("disabled", flag);
    }
    this.sendMessage = function (jsonResult) {
        if (jsonResult == "1") {
            Instance.messageDialog.hide();
            alert("项目评审成功，消息发送成功！");
            //查询系统新消息
            //window.history.back();
            // window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=A&pageIndex=" + index;
            window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        } else {
            alert("消息发送失败！");
        }
    }

    this.Dom.DisAgreeButton.live("click", function () {
        //按钮变为不可用
        //        $(this).attr("disabled", "disabled");
        //        //验证方法
        //        Instance.Vaildation();
        //        var textareeParentPanel = $(this).parents("table:first");

        var textareeParentPanel = $(this).parents("table:first");
        //调用验证方法
        Instance.Vaildation(textareeParentPanel);
        var objData =
        {
            SysNo: Instance.ProjectAuditSysNo,
            Suggestion: $.trim($("textarea[id=txt_view]", textareeParentPanel).val()),
            Status: Instance.ProjectStatus
        };
        if ($.trim($("textarea[id=txt_view]", textareeParentPanel).val()).length > 0) {
            var queryString = Global.toJSON(objData);
            //反射调用方法
            var result = Instance.BackgroundInvoke.DoDisAgree(queryString);
            if (result.value == "1") {
                //改变消息状态
                var msg = new MessageCommProj($("#msgno").val());
                msg.ReadMsg();
                //提示
                alert("评审拒绝操作成功！");
                // window.history.back();
                // window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=A&pageIndex=" + index;
                window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            } else {
                alert("评审拒结操作失败！");
            }

            Instance.setAuditBtnDisable(true);
        }
    });

}