﻿//高级查询
$(function () {
    //初始化项目list
    projectList();
    //初始化显示cookies字段列
    InitColumnsCookies("columnslist_project", "jqGrid", "columnsid");
    //判断当前用户所在部门--用于高级查询（其他参与部门）
    var curunitid = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    if (curunitid == '248') {//经济所
        $("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").hide().parent().parent().hide();
    }
    else if (curunitid == '247') {//暖通所
        $("#ctl00_ContentPlaceHolder1_chk_ISHvac").hide().parent().parent().hide();
    }
    else {//土建所
        $("#ctl00_ContentPlaceHolder1_chk_ISArch").hide().parent().parent().hide();
    }
    //选择生产部门事件
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    $("#ctl00_ContentPlaceHolder1_hid_IsSeach").val("1");
    //cookie存储显示列--全部事件
    $(":checkbox:first", $("#columnsid")).click(function () {
        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_project", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });
    //cookie存储显示列--单一事件
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_project", columnslist, { expires: 365, path: "/" });

    });
    //普通查询
    $("#btn_search").live("click", function () {
        var allcheckedstate = $.trim($("#ctl00_ContentPlaceHolder1_drp_unit_t_l_1").attr("checkedstate"));
        $("#ctl00_ContentPlaceHolder1_hid_IsSeach").val("1");
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var txt_proName = $("#ctl00_ContentPlaceHolder1_txt_keyname").val(); //项目名称
        var txt_buildUnit = $("#txt_buildUnit").val(); //建设单位
        var txtaddress = $("#txtaddress").val(); //建设地点
        var drp_unit = $.trim(IsStructCheckNode('structtype')); //承接部门
        if (allcheckedstate == "0") {
            drp_unit = "";
        };
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text(); //行业性质
        var ddsource = $("#ctl00_ContentPlaceHolder1_ddsource").val(); //工程来源    
        var txt_PMName = $("#txt_PMName").val(); //执行设总
        var txt_PMPhone = $("#txt_PMPhone").val(); //电话
        var txt_Aperson = $("#txt_Aperson").val(); //甲方负责人
        var txt_phone = $("#txt_phone").val(); //电话
        var txtproAcount = $("#txtproAcount").val(); //合同额
        var txtproAcount2 = $("#txtproAcount2").val();
        var txt_buildArea = $("#ctl00_ContentPlaceHolder1_txt_buildArea").val(); //建筑规模
        var txt_buildArea2 = $("#ctl00_ContentPlaceHolder1_txt_buildArea2").val();
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val(); //开始时间
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val(); //完成时间
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
        var ddrank = $("#ctl00_ContentPlaceHolder1_drp_buildtype").find("option:selected").text(); //建筑类别

        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //管理级别
        var managerLevelDropDownList = $("#ctl00_ContentPlaceHolder1_managerLevelDropDownList").val();
        //设计阶段
        var str_jd = "";
        $('input[type="checkbox"]:not(.can):checked', "#Schtable").each(function () {
            str_jd = str_jd + $(this).parent().parent().next("label").first().text() + ",";
        });
        //参与部门
        var cybm = "";
        if ($("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").is(":checked")) {
            cybm = "1,";
        }
        else { cybm = "0,"; }

        if ($("#ctl00_ContentPlaceHolder1_chk_ISHvac").is(":checked")) {
            cybm = cybm + "1,";
        }
        else { cybm = cybm + "0,"; }

        if ($("#ctl00_ContentPlaceHolder1_chk_ISArch").is(":checked")) {
            cybm = cybm + "1";
        }
        else {
            cybm = cybm + "0";
        }
        if ($("#cpr_join").is(":checked")) {
            cybm = cybm;
        }
        else {
            cybm = "";
        }

        //结构形式
        var asTreeviewStruct = GetTreeValues("ctl00_ContentPlaceHolder1_asTreeviewStruct_ulASTreeView");

        //建筑分类
        var asTreeviewStructType = GetTreeValues("ctl00_ContentPlaceHolder1_asTreeviewStructType_ulASTreeView");
        var paramsH = {
            "strwhere": strwhere,
            "andor": andor,
            "year": year,
            "txt_proName": txt_proName,
            "txt_buildUnit": txt_buildUnit,
            "txtaddress": txtaddress,
            "drp_unit": drp_unit,
            "ddType": ddType,
            "ddsource": ddsource,
            "txt_Aperson": txt_Aperson,
            //"txt_phone": txt_phone,
            "txt_PMName": txt_PMName,
            //"txt_PMPhone": txt_PMPhone,
            "txtproAcount": txtproAcount,
            "txtproAcount2": txtproAcount2,
            "txt_buildArea": txt_buildArea,
            "txt_buildArea2": txt_buildArea2,
            "txt_signdate": txt_signdate,
            "txt_signdate2": txt_signdate2,
            "txt_finishdate": txt_finishdate,
            "txt_finishdate2": txt_finishdate2,
            "ddrank": ddrank,
            "managerLevelDropDownList": managerLevelDropDownList,
            "str_jd": str_jd,
            "cybm": cybm,
            "asTreeviewStruct": asTreeviewStruct,
            "asTreeviewStructType": asTreeviewStructType,
            "startTime": startTime,//录入时间
            "endTime": endTime
        };
        clearGridPostData();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?n=" + (Math.random() + new Date().getMilliseconds()) + "&action=select",
            postData: paramsH,
            mytype: 'POST',
            page: 1
        }).trigger("reloadGrid");
    });

    //字段导出记录
    var columnslist = new Array();
    var columnsChinaNameList = new Array();
    //查询条件记录
    var schcolumnslist = new Array();
    var schcolumnsChinaNameList = new Array();
    //声明查询方式
    var andor = "and";
    //点击是否--条件
    $("label[rel]").click(function () {
        $(this).addClass("yellow active");
        $(this).removeClass("default");
        $(this).siblings("label").removeClass("active yellow");
        $(this).siblings("label").addClass("default");
        andor = $(this).attr("rel");
        $("#btn_search").click();
    });
   
    //新增选择项全选按钮
    $(":checkbox:first", $("#Colsdrpboxs")).click(function () {

        if ($(this).is(":checked"))//代表选中
        {
            var colsnuma = new Array();
            $(":checkbox:not(:first)", $("#Colsdrpboxs")).each(function (i, value) {
                var commName = $.trim($(this).get(0).id);
                var span = $(value).parent();
                //获取数据库字段名      
                var columnsname = $.trim(span.parent().siblings("span").attr("rel"));
                //获取表头字段名
                var columnsChinaName = $.trim(span.parent().siblings("span").text());
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                colsnuma.push(commName);
                columnslist.push(columnsname);
                columnsChinaNameList.push(columnsChinaName);
            });            
            BindSchCols(colsnuma, "Schtable");
            
        }
        else {
            var colsnuma = new Array();
            $(":checkbox:not(:first)", $("#Colsdrpboxs")).each(function (i, value) {
                var commName = $.trim($(this).get(0).id);
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                //colsnuma.push(commName)
                //append到table
                 columnslist = new Array();
                 columnsChinaNameList = new Array();
            });            
            BindSchCols(colsnuma, "Schtable");
            
        }

    });
    //查询条件单独按钮     
    $(":checkbox:not(:first)", $("#Colsdrpboxs")).click(function () {

        //var checkedstate = $(this).parent().attr("class");
        //alert(checkedstate);
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#Colsdrpboxs")).length) == ($(":checkbox:not(:first)", $("#Colsdrpboxs")).length)) {
            $(":checkbox:first", $("#Colsdrpboxs")).parent().addClass("checked");
            $(":checkbox:first", $("#Colsdrpboxs")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#Colsdrpboxs")).parent().removeClass("checked");
            $(":checkbox:first", $("#Colsdrpboxs")).attr("checked", false);
        }
        //条件名称
        var commName = $.trim($(this).get(0).id);
        //获取数据库字段名      
        var columnsname = $.trim($(this).parent().parent().siblings("span").attr("rel"));
        //获取表头字段名
        var columnsChinaName = $.trim($(this).parent().parent().siblings("span").text());
        if ($(this).is(":checked"))//代表选中
        {
            //$("#tbl_id2").show();
            //$("#tbl_id2 tr[for=" + commName + "]").show();
            //判断执行设总和甲方负责人

            //添加到数组里
            columnslist.push(columnsname);
            columnsChinaNameList.push(columnsChinaName);
            BindSchCols(columnslist, "Schtable");
        }
        else {
            $(this).attr("checked", false);
            if (commName == "StructType") {
                $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text("");
            }
            if (commName == "BuildStructType") {
                var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text("");
            }
            if (commName == "ddcpr_Stage" || commName == "cpr_join") {

                $('input[type=checkbox]', "#tbl_id2 tr[for=" + commName + "]").each(function () {
                    $(this).parent().attr("class", "");
                });
            }

            ////隐藏字段
            //if (columnsname != "pro_name" && columnsname != "PMUserName" && columnsname != "pro_status" && columnsname != "Unit" && columnsname != "Cpr_Acount" && columnsname != "qdrq" && columnsname != "wcrq") {

            //}
            //删除数组           
            columnslist.splice($.inArray(columnsname, columnslist), 1);
            columnsChinaNameList.splice($.inArray(columnsChinaName, columnsChinaNameList), 1);
            
            BindSchCols(columnslist, "Schtable");
        }

        //获得显示的列，保存到cookies
        //var columnslist = "";
        //$(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
        //    columnslist = columnslist + $(this).val() + ",";
        //});
        //if (columnslist != "") {
        //    columnslist = columnslist.substr(0, (columnslist.length - 1));
        //}
        ////保存到cookies
        //$.cookie("columnslist", columnslist, { expires: 365, path: "/" });

    });
    //点击选择条件





    //导出
    $("#ctl00_ContentPlaceHolder1_btn_Output").click(function () {

        var columnslist = new Array();
        var columnsChinaNameList = new Array();
        var isseach = $("#ctl00_ContentPlaceHolder1_hid_IsSeach").val();
        $(":checkbox", $("#columnsid")).each(function () {
            if ($(this).parent().attr("class") == "checked") {
                var name = $(this).attr("rel");
                var val = $(this).val();
                if (name != undefined) {
                    columnsChinaNameList.push(name);
                }
                if (val != "all") {
                    columnslist.push(val);
                }
            }
        });
        //导出显示的字段
        var str_columnsname = "";
        var str_columnschinaname = "";
        if (columnslist != null && columnslist.length > 0) {
            for (var i = 0; i < columnslist.length; i++) {
                var arr = $.trim(columnslist[i]);
                var arr_china = $.trim(columnsChinaNameList[i]);
                if (arr != "pro_name" && arr != "PMUserName" && arr != "pro_status" && arr != "Unit" && arr != "Cpr_Acount" && arr != "qdrq" && arr != "wcrq") {
                    str_columnsname = str_columnsname + arr + ",";
                    str_columnschinaname = str_columnschinaname + arr_china + ",";
                }
            }
        }
        if (str_columnsname != "") {
            str_columnsname = str_columnsname.substring(str_columnsname, (str_columnsname.length - 1));
        }
        if (str_columnschinaname != "") {
            str_columnschinaname = str_columnschinaname.substring(str_columnschinaname, (str_columnschinaname.length - 1));
        }



        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var txt_proName = $("#ctl00_ContentPlaceHolder1_txt_keyname").val(); //项目名称
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var txt_buildUnit = $("#txt_buildUnit").val(); //建设单位
        var txtaddress = $("#txtaddress").val(); //建设地点
        var drp_unit = $.trim(IsStructCheckNode('structtype')); //承接部门
        var allcheckedstate = $.trim($("#ctl00_ContentPlaceHolder1_drp_unit_t_l_1").attr("checkedstate"));
        if (allcheckedstate == "0") {
            drp_unit = "";
        };
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text(); //行业性质
        var ddsource = $("#ctl00_ContentPlaceHolder1_ddsource").val(); //工程来源    
        var txt_PMName = $("#txt_PMName").val(); //执行设总
        //var txt_PMPhone = $("#txt_PMPhone").val(); //电话
        var txt_Aperson = $("#txt_Aperson").val(); //甲方负责人
        //var txt_phone = $("#txt_phone").val(); //电话
        var txtproAcount = $("#txtproAcount").val(); //合同额
        var txtproAcount2 = $("#txtproAcount2").val();
        var txt_buildArea = $("#ctl00_ContentPlaceHolder1_txt_buildArea").val(); //建筑规模
        var txt_buildArea2 = $("#ctl00_ContentPlaceHolder1_txt_buildArea2").val();
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val(); //开始时间
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val(); //完成时间
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
        var ddrank = $("#ctl00_ContentPlaceHolder1_drp_buildtype").find("option:selected").text(); //建筑类别
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //管理级别
        var managerLevelDropDownList = $("#ctl00_ContentPlaceHolder1_managerLevelDropDownList").val();
        //设计阶段
        var str_jd = "";
        $('input[type="checkbox"]:not(.can):checked', "#Schtable").each(function () {
            str_jd = str_jd + $(this).parent().parent().next("label").first().text() + ",";
        });
        //参与部门
        var cybm = "";
        if ($("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").is(":checked")) {
            cybm = "1,";
        }
        else { cybm = "0,"; }

        if ($("#ctl00_ContentPlaceHolder1_chk_ISHvac").is(":checked")) {
            cybm = cybm + "1,";
        }
        else { cybm = cybm + "0,"; }

        if ($("#ctl00_ContentPlaceHolder1_chk_ISArch").is(":checked")) {
            cybm = cybm + "1";
        }
        else {
            cybm = cybm + "0";
        }
        if ($("#cpr_join").is(":checked")) {
            cybm = cybm;
        }
        else {
            cybm = "";
        }
        //结构形式
        var asTreeviewStruct = GetTreeValues("ctl00_ContentPlaceHolder1_asTreeviewStruct_ulASTreeView");

        //建筑分类
        var asTreeviewStructType = GetTreeValues("ctl00_ContentPlaceHolder1_asTreeviewStructType_ulASTreeView");
        var params = {
            "strwhere": strwhere,
            "txt_proName": txt_proName,
            "year": year,
            "txt_buildUnit": txt_buildUnit,
            "txtaddress": txtaddress,
            "drp_unit": drp_unit,
            "ddType": ddType,
            "ddsource": ddsource,
            "txt_Aperson": txt_Aperson,
            //"txt_phone": txt_phone,
            "txt_PMName": txt_PMName,
            //"txt_PMPhone": txt_PMPhone,
            "txtproAcount": txtproAcount,
            "txtproAcount2": txtproAcount2,
            "txt_buildArea": txt_buildArea,
            "txt_buildArea2": txt_buildArea2,
            "txt_signdate": txt_signdate,
            "txt_signdate2": txt_signdate2,
            "txt_finishdate": txt_finishdate,
            "txt_finishdate2": txt_finishdate2,
            "ddrank": ddrank,
            "managerLevelDropDownList": managerLevelDropDownList,
            "str_jd": str_jd,
            "cybm": cybm,
            "asTreeviewStruct": asTreeviewStruct,
            "asTreeviewStructType": asTreeviewStructType,
            "startTime": startTime,//录入时间
            "endTime": endTime,
            "str_columnsname": str_columnsname,
            "str_columnschinaname": str_columnschinaname
        };
        window.location.href = "Export_SearchProject.aspx?strwhere=" + strwhere + "&year=" + year + "&andor=" + andor + "&txt_proName=" + txt_proName + "&txt_buildUnit=" + txt_buildUnit + "&txtaddress=" + txtaddress + "&drp_unit=" + drp_unit + "&ddType=" + ddType + "&ddsource=" + ddsource + "&txt_Aperson=" + txt_Aperson + "&txt_PMName=" + txt_PMName + "&txtproAcount=" + txtproAcount + "&txtproAcount2=" + txtproAcount2 + "&txt_buildArea=" + txt_buildArea + "&txt_buildArea2=" + txt_buildArea2 + "&txt_signdate=" + txt_signdate + "&txt_signdate2=" + txt_signdate2 + "&txt_finishdate=" + txt_finishdate + "&txt_finishdate2=" + txt_finishdate2 + "&ddrank=" + ddrank + "&managerLevelDropDownList=" + managerLevelDropDownList + "&str_jd=" + str_jd + "&cybm=" + cybm + "&asTreeviewStruct=" + asTreeviewStruct + "&asTreeviewStructType=" + asTreeviewStructType + "&str_columnsname=" + str_columnsname + "&str_columnschinaname=" + str_columnschinaname + "&startTime=" + startTime + "&endTime=" + endTime + "";

        return false;

    });

});

//全局集合
var arrayTRList = new Array();
var arrayoldList = new Array();
//获取建筑分类值
function GetTreeValues(obj) {
    var allstr = "";
    //一级   
    $("#" + obj).children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate!="2") {          
        var onestr = ckvalue;
        var tempstr = subGetTreeValues($(this), onestr);
        allstr = allstr + tempstr + ",";
        // }
    });
    if (allstr != "") {
        allstr = allstr.substr(0, (allstr.length - 1));
    }
    return allstr;
}
//二级li里选中的值(递归)
function subGetTreeValues(obj, str) {
    //二级   
    var currallstr = "";
    obj.children("ul").children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate != "2") {
        var substr = "," + ckvalue;
        var tempstr = childGetTreeValues($(this), substr);
        currallstr = currallstr + tempstr;

        //}
    });
    return str + currallstr;
}
//三级li
function childGetTreeValues(obj, str) {
    var currallstr = "";
    obj.children("ul").children("li[checkedstate!=2]").each(function () {
        var ckstate = $(this).attr("checkedstate");
        var ckvalue = $.trim($(this).attr("treenodevalue"));
        //0代表全选，1代表半选，2代表无选中
        // if (ckstate != "2") {
        var childstr = str + "," + ckvalue;
        var tempstr = childGetTreeValues($(this), childstr);
        currallstr = currallstr + tempstr;
        //}
    });
    if (currallstr == "") {
        currallstr = str;
    }
    return currallstr;
}

//初始化显示cookies字段列
var InitColumnsCookies = function (cookiename, jqgrid, colid) {
    //基本合同列表
    var columnslist = $.cookie(cookiename);
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {

        var list = columnslist.split(",");
        // $.each(list, function (i, c) {
        $(":checkbox:not(:first)[value]", $("#" + colid)).each(function (j, n) {
            if (list.indexOf($.trim($(n).val())) > -1) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                $("#" + jqgrid).showCol($(n).val());
            }
            else {
                $("#" + jqgrid).hideCol($(n).val());
            }

        });

        //  });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#" + colid)).length)) {
            $(":checkbox:first", $("#" + colid)).parent().addClass("checked");
            $(":checkbox:first", $("#" + colid)).attr("checked", true);
        }

    }
    else {
        //cookie未保存，显示默认的几个字段
        //生产经营合同和监理合同是11个多了已收费字段，岩石工程检测合同是9个少建筑面积，其他都是10个字段
        var list = new Array("ChgJia", "pro_name", "BuildType", "Project_reletive", "Cpr_Acount", "qdrq", "wcrq", "PMUserName", "lrsj");//直接定义并初始化


        $.each(list, function (i, c) {
            if ((jqgrid == "jqGrid6" && (c == "BuildArea" || c == "ssze")) || (jqgrid != "jqGrid" && jqgrid != "jqGrid3" && c == "ssze")) {
            }
            else {
                $(":checkbox:not(:first)[value='" + c + "']", $("#" + colid)).each(function (j, n) {
                    var span = $(n).parent();
                    if (!span.hasClass("checked")) {
                        span.addClass("checked");
                    }
                    $(n).attr("checked", true);
                    // $("#jqGrid").showCol($(n).val());
                });
            }
        });
    }
}
//绑定到table
var BindSchCols = function (arrayList, tableid) {
    //定义$对象新集合

    var arrayTRListNew = new Array();
    //还原上次参数并清除

    for (var i = 0; i < arrayTRList.length; i++) {

        $("#hideChoose div[for=" + arrayoldList[i] + "]").append($("div[for=" + arrayoldList[i] + "_1]"))
        $("#hideChoose div[for=" + arrayoldList[i] + "]").append($("div[for=" + arrayoldList[i] + "_2]"))
    }
    if ($(":checkbox:first", $("#Colsdrpboxs")).is(":checked")) {
        //
    }
    else {
        //全部选
        $("tr:gt(6)", $("#Schtable")).remove();
    }
    //删除添加行重新增加排列
    if ($("#Schtable tr").length > 6) {
        $("tr:gt(6)", $("#Schtable")).remove();
    }
    //$tr对象添加到array集合
    for (var i = 0; i < arrayList.length ; i++) {
        arrayTRListNew.push($("#hideChoose div[for=" + arrayList[i] + "]"));
        arrayTRList.push($("#hideChoose div[for=" + arrayList[i] + "]"));
        arrayoldList.push(arrayList[i]);
    }
    var $tr; var $td1; var $td2;
    var arrtrlist = new Array();
    for (var i = 0; i < arrayTRListNew.length; i++) {
        $td1 = $("<td></td>");
        $td2 = $("<td></td>");
        if (parseInt(i / 3) == i / 3 || i / 3 == 0) {
            $tr = $("<tr></tr>");
            $td1 = $td1.append(arrayTRListNew[i].children().first());
            $td2 = $td2.append(arrayTRListNew[i].children().last());
        } else {
            $td1 = $td1.append(arrayTRListNew[i].children().first());
            $td2 = $td2.append(arrayTRListNew[i].children().last());
        }
        $tr = $tr.append($td1).append($td2);
        if (parseInt(i / 3) == i / 3 || i == arrayTRListNew.length - 1 || i / 3 == 0) {
            arrtrlist.push($tr);
        }
    }
    for (var i = 0; i < arrtrlist.length; i++) {
        $("#Schtable").append(arrtrlist[i]);
    }

}
