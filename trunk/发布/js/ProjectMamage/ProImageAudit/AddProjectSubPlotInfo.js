﻿//添加工程出图卡子项
function AddProjectSubPlotInfo(container) {

    this.Dom = {};
    this.Container = container;
    var Instance = this;
    this.IsEdit = false;

    this.BindData = function (dataObj) {

        $.each($("#drop_Mapsheet option", container), function (index, option) {
            if ($.trim($(option).text()) == dataObj.Mapsheet) {
                $(option).attr("selected", "selected");
            }
        });

        $.each($("#drop_LengthenType option", container), function (index, item) {
            if ($.trim($(item).text()) == dataObj.LengthenType) {
                $(item).attr("selected", "selected");
            }
        });

        $.each($("#drop_Specialty option", container), function (index, item) {
            if ($.trim($(item).text()) == dataObj.Specialty) {
                $(item).attr("selected", "selected");
            }
        });

        $("#txt_Count", container).val(dataObj.Counts);
    }

    this.SaveProjectPlotInfo = function (callBack, dialogWindow) {
        var mapsheet = $("#drop_Mapsheet", container).val();
        if (mapsheet == "-1") {
            $("#spanSubMapsheet", container).show();
            return false;
        } else {
            $("#spanSubMapsheet", container).hide();
        }
        var lengthenType = $("#drop_LengthenType", container).val();
        if (lengthenType == "-1") {
            $("#spanLengthenType", container).show();
            return false;
        } else {
            $("#spanLengthenType", container).hide();
        }

        var specialty = $("#drop_Specialty", container).val();
        if (specialty == "-1") {
            $("#spanSpecialty", container).show();
            return false;
        } else {
            $("#spanSpecialty", container).hide();
        }

        var counts = $("#txt_Count", container).val();
        if (counts == "") {
            $("#countsNull", container).show();
            return false;
        } else {
            $("#countsNull", container).hide();
            var reg = /^\+?[0-9][0-9]*$/;
            if (!reg.test(counts)) {
                $("#countsNoInt", container).show();
                return false;
            } else {
                $("#countsNoInt", container).hide();
            }
        }

        var dataObj =
        {
            "Specialty": $("#drop_Specialty :selected", container).text(),
            "Mapsheet": $("#drop_Mapsheet :selected", container).text(),
            "LengthenType": $("#drop_LengthenType :selected", container).text(),         
            "Counts": $("#txt_Count", container).val()
        };
        callBack(dataObj);
        $(dialogWindow).dialog("close");
    };

    this.Clear = function () {

        $("#spanSubMapsheet", container).hide();
        $("#spanLengthenType", container).hide();
        $("#spanSpecialty", container).hide();
        $("#countsNull", container).hide();
        $("#countsNoInt", container).hide();
        $(":text", container).val("");
        $("#drop_LengthenType", container).val("-1");
        $("#drop_Mapsheet", container).val("-1");
        $("#drop_Specialty", container).val("-1");
    }

}
