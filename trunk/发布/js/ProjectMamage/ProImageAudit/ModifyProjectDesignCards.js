﻿$(document).ready(function () {

    CommonControl.SetFormWidth();

    $("#projectInfo tr:odd").css({ background: "White" });

    LoadProtSubInfo();

    var chooseUser = new ChooseUserControl($("#chooseUserMain"), PMCallBack);

    function PMCallBack(userObj) {
        $("#txt_PMName").val(userObj.username);
        $("#hid_pmname").val(userObj.username);
        $("#hid_pmuserid").val(userObj.usersysno);
    }

    $("#sch_PM").click(function () {
        $("#chooseUserMain").dialog({
            autoOpen: false,
            modal: true,
            width: 600,
            height: "auto",
            resizable: false,
            title: "选择执行设总"
        }).dialog("open");
        return false;
    });

    //添加子项类
    var addProjectSubPlotInfo = new AddProjectSubPlotInfo($("#addsubPlotInfo"));

    //添加出图卡子项信息
    $("#btn_addSub").click(function () {
        addProjectSubPlotInfo.Clear();
        $("#addsubPlotInfo").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 200,
            resizable: false,
            title: "添加工程出图卡子项信息",
            buttons:
                    {
                        "确定": function () {
                            //调用处理事件
                            addProjectSubPlotInfo.SaveProjectPlotInfo(AddProjectPlotInfoCallback, this);
                        },
                        "取消": function () { $(this).dialog("close"); }
                    }
        }).dialog("open");
        return false;
    });

    //编辑
    $("#EditSubItemLinkButton").live("click", function () {
        var tr = $(this).parents("tr:first");
        var parObj = {};
        parObj.Specialty = $.trim(tr.children("td:eq(0)").text());
        parObj.Mapsheet = $.trim(tr.children("td:eq(1)").text());
        parObj.LengthenType = $.trim(tr.children("td:eq(2)").text());
        parObj.Counts = $.trim(tr.children("td:eq(3)").text());

        //修改状态IsEdit等于True。有些地方需要判断是否是编辑状态
        addProjectSubPlotInfo.IsEdit = true;
        addProjectSubPlotInfo.Clear();
        addProjectSubPlotInfo.BindData(parObj);
        $("#addsubPlotInfo").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 200,
            resizable: false,
            title: "编辑工程出图卡子项信息",
            buttons:
                    {
                        "确定": function () {
                            //调用处理事件
                            addProjectSubPlotInfo.SaveProjectPlotInfo(AddProjectPlotInfoCallback, this);
                            tr.remove();
                            $(this).dialog("close");
                        },
                        "取消": function () { $(this).dialog("close"); addProjectSubPlotInfo.IsEdit = false; }
                    }
        }).dialog("open");
        return false;
    });
    function AddProjectPlotInfoCallback(dataObj) {
        var trString = "<tr>";
        trString += "<td align=\"center\" style=\"width:150px\">" + dataObj.Specialty + "</td>";
        trString += "<td align=\"center\" style=\"width:150px\">" + dataObj.Mapsheet + "</td>";
        trString += "<td  align=\"center\" style=\"width:150px\">" + dataObj.LengthenType + "</td>";
        trString += "<td align=\"center\" style=\"align:center;width:200px\">" + dataObj.Counts + "</td>";
        trString += "<td align=\"center\" style=\";width:150px\"><a id=\"DeleteSubItemLinkButton\" sysno=\"0\" style='cursor: pointer;color:blue;'>删除</a>&nbsp;<a href=\"###\" id=\"EditSubItemLinkButton\" style='cursor: pointer;color:blue;'>编辑</a></td>";
        trString += "</tr>";
        $("#tb_sub").append(trString);
        $("#tb_sub tr:gt(0)").addClass("cls_TableRow");
    }

    //删除
    $("#DeleteSubItemLinkButton").live("click", function () {
        if (confirm("是否要删除此条信息？")) {
            var sysNo = $(this).attr("sysno");
            $(this).parents("tr:first").remove();
            return false;
        }
    });

    //保存
    $("#btn_save").click(function () {

        var reg = /^\+?[0-9][0-9]*$/;

        if ($("#drop_PlotType").val() == "-1") {
            alert("请选择出图类型");
            return false;
        }

        if ($("#txt_BluePrintCounts").val() == "") {
            alert("请填写晒图份数");
            return false;
        }

        if (!reg.test($("#txt_BluePrintCounts").val())) {
            alert("晒图份数请输入整数");
            return false;
        }

        if ($.trim($("#txt_PMName").val()).length == 0) {
            alert("请选择出图人");
            return false;
        }

        //出图子项信息
        var sub = $("#tb_sub tr").length;
        if (sub == 1) {
            alert("请添加工程设计出图卡子项信息");
            return false;
        }

        var finalDataObj = {
            "ProNo": $("#HidProID").val(),
            "PlotType": $("#drop_PlotType :selected").text(),
            "BlueprintCounts": $("#txt_BluePrintCounts").val(),
            "PlotUserID": $("#hid_pmuserid").val(),
            "PlotUserName": $("#hid_pmname").val(),
            "PlotSub": new Array()
        };


        $.each($("#tb_sub tr:gt(0)"), function (index, tr) {
            finalDataObj.PlotSub[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "Mapsheet": $.trim($(tr).children("td:eq(1)").text()),
                    "LengthenType": $.trim($(tr).children("td:eq(2)").text()),
                    "Counts": $.trim($(tr).children("td:eq(3)").text())
                };
        });
        var jsonObj = Global.toJSON(finalDataObj);
        var url = "/HttpHandler/ProjectIma/ProjectPlotInfo.ashx";

        //数据         
        var data = { "Action": "0", "data": jsonObj };
        //提交数据
        $.post(url, data, function (jsonResult) {
            if (jsonResult == "1") {
                alert("保存成功!");
                window.location.href = "/ProjectManage/ProImageAudit/ProjectDesignCardsList.aspx";
            }
            else {
                alert("保存失败");
            }
        });
    });

});

function LoadProtSubInfo() {
    $.post("/HttpHandler/ProjectIma/ProjectPlotInfo.ashx", { "Action": 1, "proID": $("#HidProID").val() }, function (jsonResult) {
        var json = eval('(' + jsonResult + ')');
        var data = json == null ? "" : json.ds;
        if (data != null && data != "") {
            $("#tb_sub tr:gt(0)").remove();
            $.each(data, function (i, n) {
                var trString = "<tr>";
                trString += "<td align=\"center\" style=\"width:200px\">" + n.Specialty + "</td>";
                trString += "<td align=\"center\" style=\"width:200px\">" + n.Mapsheet + "</td>";
                trString += "<td  align=\"center\" style=\"width:200px\">" + n.LengthenType + "</td>";
                trString += "<td align=\"center\" style=\"align:center;width:200px\">" + n.Counts + "</td>";
                trString += "<td align=\"center\" style=\";width:150px\"><a id=\"DeleteSubItemLinkButton\" sysno=\"0\" style='cursor: pointer;color:blue;'>删除</a>&nbsp;<a href=\"###\" id=\"EditSubItemLinkButton\" style='cursor: pointer;color:blue;'>编辑</a></td>";
                trString += "</tr>";
                $("#tb_sub").append(trString);
                $("#tb_sub tr:gt(0)").addClass("cls_TableRow");
            });
        }
    });
}