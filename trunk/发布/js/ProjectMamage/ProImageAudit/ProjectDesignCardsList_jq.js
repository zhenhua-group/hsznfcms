﻿//未分配
function AllotList() {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '','项目名称', '工程号', '承接部门', '设计阶段', '执行设总', '管理级别', '审核级别', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 300, formatter: colNameShowFormatter },
                             { name: 'Pro_number', index: 'Pro_number', width: 100, align: 'center' },
                             { name: 'Unit', index: 'Unit', width: 200, align: 'center' },
                             { name: 'pro_status', index: 'pro_status', width: 120, align: 'center' },
                             { name: 'PMName', index: 'PMName', width: 80, align: 'center' },
                              { name: 'pro_level', index: 'pro_level', width: 80, align: 'center' },
                             { name: 'AuditLevel', index: 'AuditLevel', width: 80, align: 'center'},
                             { name: 'pro_ID', index: 'pro_ID', width: 80, align: 'center', formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#hid_where").val()) },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#hid_where").val());
        var unit = $("#drp_unit").find("option:selected").text();
        var year = $("#drp_year").val();
        var keyname = $("#txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year},
            page: 1

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#drp_unit").change(function () {
        var strwhere = escape($("#hid_where").val());
        var unit = $("#drp_unit").find("option:selected").text();
        var year = $("#drp_year").val();
        var keyname = $("#txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProject.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<img src="/Images/proj.png" style="width: 16px; height: 16px; border: none;" /><a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var linkAction = "";
    linkAction = "<a href='AddProjectDesignCards.aspx?project_Id="+celvalue+"' target='_self'>出图申请</a>";
    return linkAction;
}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1)); 

    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
          $("#nodata").hide();
    }
}


//已分配完成
function AllotList2() {
    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '项目名称', '工程号', '承接部门', '设计阶段', '执行设总', '管理级别', '审核级别', '审批进度', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'Plot_Type', index: 'Plot_Type', hidden: true, editable: true },
                             { name: 'AuditStatus', index: 'AuditStatus', hidden: true, editable: true },
                             { name: 'AuditRecordSysNo', index: 'AuditRecordSysNo', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Pro_number', index: 'Pro_number', width: 100, align: 'center' },
                             { name: 'Unit', index: 'Unit', width: 150, align: 'center' },
                             { name: 'pro_status', index: 'pro_status', width: 150, align: 'center' },
                             { name: 'PMName', index: 'PMName', width: 80, align: 'center' },
                             { name: 'pro_level', index: 'pro_level', width: 50, align: 'center' },
                             { name: 'AuditLevel', index: 'AuditLevel', width: 50, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 100, align: 'center', formatter: colStrShowFormatter2 },
                             { name: 'pro_ID', index: 'pro_ID', width: 100, align: 'center', formatter: colShowFormatter2 }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "sel2", "strwhere": escape($("#hid_where2").val()),"year":$("#drp_year").val()},
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager2",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod2,
        loadComplete: loadCompMethod2
    });


    //显示查询
    $("#jqGrid2").jqGrid("navGrid", "#gridpager2", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid2").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid2').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid2').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#hid_where2").val());
        var unit = $("#drp_unit").find("option:selected").text();
        var year = $("#drp_year").val();
        var keyname = $("#txt_keyname").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1

        }).trigger("reloadGrid");
    });
    //查询
    $("#drp_unit").change(function () {
        var strwhere = escape($("#hid_where2").val());
        var unit = $("#drp_unit").find("option:selected").text();
        var year = $("#drp_year").val();
        var keyname = $("#txt_keyname").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMaMage/ProImageAudit/ProjectDesignHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter2(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProject.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<img src="/Images/proj.png" style="width: 16px; height: 16px; border: none;" /><a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
}
//静态状态
function colStrShowFormatter2(celvalue, options, rowData) {
    var percent = 0;
    var count = 8;
    if (rowData["AuditRecordSysNo"] != null && rowData["AuditRecordSysNo"] != "") {
        if (rowData["Plot_Type"] == "正常输出" || rowData["Plot_Type"] == "补充图" || rowData["Plot_Type"] == "变更图") {
            switch (rowData["AuditStatus"]) {
                case "A":
                    percent = 0;
                    break;
                case "B":
                case "C":
                    percent = (100 / 3) * 1;
                    break;
                case "D":
                case "E":
                    percent = (100 / 3) * 2;
                    break;
                case "F":
                case "G":
                    percent = 100;
                    break;
            }
        }
        else {
            switch (rowData["AuditStatus"]) {
                case "A":
                    percent = 0;
                    break;
                case "B":
                case "C":
                    percent = (100 / count) * 1;
                    break;
                case "D":
                case "E":
                    percent = (100 / count) * 2;
                    break;
                case "F":
                case "G":
                    percent = (100 / count) * 3;
                    break;
                case "H":
                case "I":
                    percent = (100 / count) * 4;
                    break;
                case "J":
                case "K":
                    percent = (100 / count) * 5;
                    break;
                case "L":
                case "M":
                    percent = (100 / count) * 6;
                    break;
                case "N":
                case "O":
                    percent = (100 / count) * 7;
                    break;
                case "P":
                case "Q":
                    percent = 100;
                    break;
            }
        }
    }
    percent = parseFloat(percent).toFixed(2);
    var pageurl = '<div class="progressbar" percent="' + percent + '" title="' + percent + '%" id="auditLocusContainer" action="PLOT" referencesysno="' + rowData["AuditRecordSysNo"] + '" style="cursor: pointer;height:20px;margin:1px 1px;"></div>';

    return pageurl;
}
//查看
function colShowFormatter2(celvalue, options, rowData) {
    var actionLinkString="";
    if (rowData["Plot_Type"] == "正常输出" || rowData["Plot_Type"] == "补充图" || rowData["Plot_Type"] == "变更图") {
        switch (rowData["AuditStatus"]) {
            case "":
            case null:
            case "C":
            case "E":
            case "G":
            case "K":
            case "I":
            case "M":
            case "O":
            case "Q":
                actionLinkString = " <a href='ModifyProjectDesignCards.aspx?project_Id=" + celvalue + " '  target=\"_self\">编辑</a>|<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + celvalue + "\">发起审批</span>";
                break;
            case "A":
            case "B":
            case "D":
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" proSysNo=\"" + celvalue + "\">审批中</span>";
                break;
            case "F":
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" proSysNo=\"" + celvalue + "\" ProjectPlotAuditSysNo=\"" + rowData["AuditRecordSysNo"] + "\">审核结果</span>";
                break;
        }
    }
    else {

        switch (rowData["AuditStatus"]) {
            case "":
            case null:
            case "C":
            case "E":
            case "G":
            case "K":
            case "I":
            case "M":
            case "O":
            case "Q":
                actionLinkString = " <a href='ModifyProjectDesignCards.aspx?project_Id=" + celvalue + " '   target=\"_self\">编辑</a>|<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + celvalue + "\">发起审批</span>";
                break;
            case "A":
            case "B":
            case "D":
            case "F":
            case "H":
            case "J":
            case "L":
            case "N":
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"PreviewAudit\" proSysNo=\"" + celvalue + "\">审批中</span>";
                break;
            case "P":
                actionLinkString = "<span style=\"cursor:pointer;color:blue;\" id=\"GoAudit\" proSysNo=\"" + celvalue + "\" ProjectPlotAuditSysNo=\"" + rowData["AuditRecordSysNo"] + "\">审核结果</span>";
                break;
        }
    }
    return actionLinkString;
}




//统计 
function completeMethod2() {
    var rowIds = $("#jqGrid2").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid2 [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }
    //绑定审批进度
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));      
        $(item).progressbar({
            value: percent
        });
    });
}
//无数据
function loadCompMethod2() {
    $("#jqGrid2").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid2").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata2").text() == '') {
            $("#jqGrid2").parent().append("<div id='nodata2'>没有符合条件数据！</div>")
        }
        else { $("#nodata2").show(); }
    }
    else {
        $("#nodata2").hide();
    }
}