﻿
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var FileNum;
$(document).ready(function () {

    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();

    $("#projectInfo tr:odd").css({ background: "White" });

    //状态
    var Status = $("#HiddenAuditStatus").val();
    var plotType = $("#HiddenPlotType").val();
    CommonControl.SetFormWidth();

    $("input", "#rbtlist").attr("disabled", "disabled");
    //图幅改变
    $("#drop_Mapsheet").change(function () {

        var mapSheet = "0#图";
        if ($("#drop_Mapsheet").val() != "") {
            mapSheet = $("#drop_Mapsheet :selected").text();
        }
        var proID = $("#ProjectSysNo").val();
        var subInfo = TG.Web.ProjectManage.ProImageAudit.ProjectDesignCardsAudit.bindDropProjectPlotSunInfo(mapSheet, proID);
        $("#lblSubInfo").html("");
        $("#lblSubInfo").html(subInfo.value);
    });

    //同意
    $("#AgreeButton").live("click", function () {

        var dataObj = {
            "SysNo": $("#HiddenProjectPlotAuditSysNo").val(),
            "Suggestion": $("#suggestionTextArea").val(),
            "ProjectSysNo": $("#ProjectSysNo").val(),
            "Status": $("#HiddenAuditStatus").val()
        };

        if (plotType.indexOf('特殊输出') > -1 || plotType.indexOf('结构延迟校审') > -1) {

            if ($("#HiddenAuditStatus").val() == "N") {
                if ($("#FileNum").val() == "") {
                    alert("备案号不能为空！");
                    return false;
                }
                FileNum = $("#FileNum").val();
            }
        }
        else {
            if ($("#HiddenAuditStatus").val() == "D") {
                if ($("#FileNum").val() == "") {
                    alert("备案号不能为空！");
                    return false;
                }
                FileNum = $("#FileNum").val();
            }
        }

        if (!Vilidation(dataObj)) {
            alert("建议不能为空！");
            return false;
        }


        //按钮变为不可用
        $(this).attr("disabled", "disabled");

        var jsonData = Global.toJSON(dataObj);
        jsonDataEntity = jsonData;
        if (plotType.indexOf('特殊输出') > -1 || plotType.indexOf('结构延迟校审') > -1) {
            //开始审批
            if (Status == "N") {
                getUserAndUpdateAudit('1', '1', jsonDataEntity);
            }
            else {
                getUserAndUpdateAudit('1', '0', jsonDataEntity);
            }
        }
        else {
            //开始审批
            if (Status == "D") {
                getUserAndUpdateAudit('1', '1', jsonDataEntity);
            }
            else {
                getUserAndUpdateAudit('1', '0', jsonDataEntity);
            }
        }

    });

    //不同意
    $("#DisAgreeButton").live("click", function () {

        var dataObj = {
            "SysNo": $("#HiddenProjectPlotAuditSysNo").val(),
            "Suggestion": $("#suggestionTextArea").val(),
            "ProjectSysNo": $("#ProjectSysNo").val(),
            "Status": $("#HiddenAuditStatus").val()
        };

        if (plotType.indexOf('特殊输出') > -1 || plotType.indexOf('结构延迟校审') > -1) {

            if ($("#HiddenAuditStatus").val() == "N") {
                FileNum = $("#FileNum").val();
            }
        }
        else {
            if ($("#HiddenAuditStatus").val() == "D") {
                FileNum = $("#FileNum").val();
            }
        }

        if (!Vilidation(dataObj)) {
            alert("建议不能为空！");
            return false;
        }

        //按钮变为不可用
        $(this).attr("disabled", "disabled");

        var jsonObj = Global.toJSON(dataObj);

        Global.SendRequest("/HttpHandler/ProjectIma/ProjectPlotAuditInfo.ashx", { "Action": 2, "data": jsonObj, "flag": 1, "FileNum": FileNum }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {

                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("审核不通过成功，消息已发送给申请人！");
                }

                window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });
    });
    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('1', '1', jsonDataEntity);
            },
            "关闭": function () {
                $("#AgreeButton").attr("disabled", false);
                $("#DisAgreeButton").attr("disabled", false);
                messageDialog.hide();
            }
        }

    });
    sendMessageClass = new MessageCommon(messageDialog);

});
//验证方法
function Vilidation(auditObj) {
    var flag = true;
    if (auditObj.Suggestion == null || auditObj.Suggestion == undefined || auditObj.Suggestion.length == 0) {
        flag = false;
    }
    return flag;
}

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程

function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectIma/ProjectPlotAuditInfo.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData, "FileNum": FileNum };
    //提交数据
    Global.SendRequest(url, data, null, null, function (jsonResult) {
        //改变消息状态
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }

        if (jsonResult == "0") {
            alert("流程审批错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("工程设计出图卡审批流程完成，已全部通过！");
            window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

//消息发送成功回调
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        alert("工程设计出图卡审批通过，消息发送成功！");
        //查询系统新消息
        window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
    } else {
        alert("消息发送失败！");
    }
}

function LoadProtSubInfo() {
    $.post("/HttpHandler/ProjectIma/ProjectPlotInfo.ashx", { "Action": 1, "proID": $("#ProjectSysNo").val() }, function (jsonResult) {
        var json = eval('(' + jsonResult + ')');
        var data = json == null ? "" : json.ds;
        if (data != null && data != "") {
            $("#tb_sub tr:gt(0)").remove();
            $.each(data, function (i, n) {
                var trString = "<tr>";
                trString += "<td align=\"center\" style=\"width:150px\">" + n.Specialty + "</td>";
                trString += "<td align=\"center\" style=\"width:150px\">" + n.Mapsheet + "</td>";
                trString += "<td  align=\"center\" style=\"width:150px\">" + n.LengthenType + "</td>";
                trString += "<td align=\"center\" style=\"align:center;width:200px\">" + n.Counts + "</td>";
                //                trString += "<td align=\"center\" style=\";width:150px\"></td>";
                trString += "</tr>";
                $("#tb_sub").append(trString);
                $("#tb_sub tr:gt(0)").addClass("cls_TableRow");
            });
        }
    });
}