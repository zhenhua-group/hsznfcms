﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=select&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '项目名称', '合同名称', '执行设总', '项目阶段', '承接部门', '合同额(万元)', '开始日期', '结束日期', '管理级别', '审核级别', '建设规模(平米)', '建设单位', '结构形式', '建筑分类', '建筑类别', '甲方负责人', '甲方负责人电话', '执行设总电话', '建设地点', '行业性质', '工程来源', '参与部门', '项目概况', '项目备注', '录入人', '录入时间', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'CoperationSysNo', index: 'CoperationSysNo', hidden: true, editable: true },
                             { name: 'ISTrunEconomy', index: 'ISTrunEconomy', hidden: true, editable: true },
                             { name: 'ISHvac', index: 'ISHvac', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Project_reletive', index: 'Project_reletive', width: 200, align: 'center' },
                             { name: 'PMUserName', index: 'PMName', width: 80, align: 'center' },
                             { name: 'pro_status', index: 'pro_status', width: 150, align: 'center' },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'Cpr_Acount', width: 100, align: 'center' },
                             { name: 'qdrq', index: 'pro_startTime', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'pro_finishTime', width: 80, align: 'center' },
                             { name: 'pro_jb', index: 'pro_level', width: 60, align: 'center', hidden: true, editable: true },
                             {
                                 name: 'AuditLevel', index: 'AuditLevel', width: 80, align: 'center', hidden: true, formatter: function colAuditName(celvalue, options, rowData) {
                                     var str = "";
                                     if ($.trim(celvalue) == "1,0") {
                                         str = "所审";
                                     }
                                     else if ($.trim(celvalue) == "0,1") {
                                         str = "院审";
                                     }
                                     else if ($.trim(celvalue) == "1,1") {
                                         str = "院审,所审";
                                     }
                                     return str;
                                 }
                             },
                             { name: 'ProjectScale', index: 'ProjectScale', width: 100, align: 'center', hidden: true, editable: true },
                             { name: 'pro_buildUnit', index: 'pro_buildUnit', width: 100, hidden: true, editable: true },
                             { name: 'pro_StruType', index: 'pro_StruType', width: 200, hidden: true, editable: true },
                             { name: 'pro_kinds', index: 'pro_kinds', width: 200, hidden: true, editable: true },
                             { name: 'BuildType', index: 'BuildType', width: 60, align: 'center', hidden: true, editable: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 60, align: 'center', hidden: true, editable: true },
                                 { name: 'Phone', index: 'Phone', width: 80, align: 'center', hidden: true },
                               { name: 'PMPhone', index: 'PMPhone', align: 'center', width: 80, hidden: true },
                             { name: 'BuildAddress', index: 'BuildAddress', width: 100, align: 'center', hidden: true, editable: true },
                             { name: 'Industry', index: 'Industry', width: 100, align: 'center', hidden: true, editable: true },
                              { name: 'pro_from', index: 'Pro_src', width: 100, align: 'center', hidden: true, editable: true },
                             { name: 'ISArch', index: 'ISArch', width: 100, align: 'center', sorttable: false, editable: false, hidden: true, editable: true, formatter: colCybmFormatter },
                               { name: 'ProjSub', index: 'ProjSub', width: 80, hidden: true },
                                 { name: 'pro_Intro', index: 'pro_Intro', width: 80, hidden: true },
                               { name: 'InsertUser', index: 'InsertUserID', align: 'center', width: 80, hidden: true },
                              { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {

                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            });

    //声明查询方式
    var andor = "and";

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var txt_proName = $("#txt_proName").val(); //项目名称
        var txt_buildUnit = $("#txt_buildUnit").val(); //建设单位
        var txtaddress = $("#txtaddress").val(); //建设地点
        var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text(); //承接部门
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text(); //行业性质
        var ddsource = $("#ctl00_ContentPlaceHolder1_ddsource").val(); //工程来源    
        var txt_PMName = $("#txt_PMName").val(); //执行设总
        var txt_PMPhone = $("#txt_PMPhone").val(); //电话
        var txt_Aperson = $("#txt_Aperson").val(); //甲方负责人
        var txt_phone = $("#txt_phone").val(); //电话
        var txtproAcount = $("#txtproAcount").val(); //合同额
        var txtproAcount2 = $("#txtproAcount2").val();
        var txt_buildArea = $("#txt_buildArea").val(); //建筑规模
        var txt_buildArea2 = $("#txt_buildArea2").val();
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val(); //开始时间
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val(); //完成时间
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
        var ddrank = $("#ctl00_ContentPlaceHolder1_ddrank").find("option:selected").text(); //建筑类别

        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //管理级别
        var managerLevelDropDownList = $("#ctl00_ContentPlaceHolder1_managerLevelDropDownList").val();
        //设计阶段
        var str_jd = "";
        $('input[type="checkbox"]:not(.can):checked', "#tbl_id2").each(function () {
            str_jd = str_jd + $(this).parent().parent().next("label").first().text() + ",";
        });
        //参与部门
        var cybm = "";
        if ($("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").is(":checked")) {
            cybm = "1,";
        }
        else { cybm = "0,"; }

        if ($("#ctl00_ContentPlaceHolder1_chk_ISHvac").is(":checked")) {
            cybm = cybm + "1,";
        }
        else { cybm = cybm + "0,"; }

        if ($("#ctl00_ContentPlaceHolder1_chk_ISArch").is(":checked")) {
            cybm = cybm + "1";
        }
        else {
            cybm = cybm + "0";
        }
        if ($("#cpr_join").is(":checked")) {
            cybm = cybm;
        }
        else {
            cybm = "";
        }

        //结构形式
        var asTreeviewStruct = $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text();
        //建筑分类
        var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text();
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txt_proName": txt_proName,
            "txt_buildUnit": txt_buildUnit,
            "txtaddress": txtaddress,
            "drp_unit": drp_unit,
            "ddType": ddType,
            "ddsource": ddsource,
            "txt_Aperson": txt_Aperson,
            "txt_phone": txt_phone,
            "txt_PMName": txt_PMName,
            "txt_PMPhone": txt_PMPhone,
            "txtproAcount": txtproAcount,
            "txtproAcount2": txtproAcount2,
            "txt_buildArea": txt_buildArea,
            "txt_buildArea2": txt_buildArea2,
            "txt_signdate": txt_signdate,
            "txt_signdate2": txt_signdate2,
            "txt_finishdate": txt_finishdate,
            "txt_finishdate2": txt_finishdate2,
            "ddrank": ddrank,
            "managerLevelDropDownList": managerLevelDropDownList,
            "str_jd": str_jd,
            "cybm": cybm,
            "asTreeviewStruct": asTreeviewStruct,
            "asTreeviewStructType": asTreeviewStructType,
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=select",
            postData: params,
            page: 1
        }).trigger("reloadGrid");
    });
    //点击全部基本合同
    $(":checkbox:first", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
            });
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
        }
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

    });

    //点击是否
    $("label[rel]").click(function () {
        $(this).addClass("yellow active");
        $(this).removeClass("default");
        $(this).siblings("label").removeClass("active yellow");
        $(this).siblings("label").addClass("default");
        andor = $(this).attr("rel");
        $("#btn_Search").click();
    });

});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
}
////合同名称连接
//function colNameCprFormatter(celvalue, options, rowData) {
//    var pageurl = "../Coperation/cpr_ShowCopration.aspx?flag=projlist1&cprid=" + rowData["CoperationSysNo"];
//    return '<img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px; border: none;" /><a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

//}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowProjectBymaster.aspx?flag=list&pro_id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看项目">查看</a>';

}
//参与部门
function colCybmFormatter(celvalue, options, rowdata) {
    var cybm = "";
    if ($.trim(rowdata["ISTrunEconomy"]) == "1") {
        cybm = "经济所";
    }
    if ($.trim(rowdata["ISHvac"]) == "1") {
        cybm = cybm + ",暖通热力所";
    }
    if ($.trim(celvalue) == "1") {
        cybm = cybm + ",土建所";
    }
    return cybm;
}
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}