﻿function ProjectAuditListView(container) {
    this.Dom = {};
    this.Container = container;
    this.TabControl = $("#tabsLi", container);
    this.BackgroundInvoke = TG.Web.UserControl.ProjectAudit.ProjectAuditListView;
    this.UserSysNo = $("#UserSysNoHidden", container).val();
    this.CurrentSelectedIndex = 0;
    var Instance = this;

    //清除数据
    this.Clear = function () {
        $.each($("table[id^=tableResult]"), function (index, item) {
            $(item).find("tr:gt(0)").remove();
        });
    }

    //创建查询参数
    this.CreateProcessObj = function (pageCurrent) {
        var processObj =
		{
		    "UserSysNo": Instance.UserSysNo,
		    "MsgType": 2,
		    "ProjectAuditStatus": Instance.CurrentSelectedIndex == "0" ? "A" : "D",
		    "PageSize": 15,
		    "PageCurrent": pageCurrent
		};
        return processObj;
    }

    //创建分页参数
    this.CreatePageObj = function (pagenumber, pagecount, pagesize, datacount, buttonClickCallback) {
        pagerObj =
		{
		    "pagenumber": pagenumber,
		    "pagecount": pagecount,
		    "pagesize": pagesize,
		    "datacount": datacount,
		    "buttonClickCallback": buttonClickCallback
		};
        return pagerObj;
    }

    //实例化分页控件
    this.InitPager = function (pagerObj) {
        if (Instance.CurrentSelectedIndex == 0) {
            $("#pager", $("#tabs0-1")).pager(pagerObj);
        } else {
            $("#pager", $("#tabs0-2")).pager(pagerObj);
        }
    }

    //创建分页处理函数
    this.PagerCallBack = function (pageNum) {
        var processObj = Instance.CreateProcessObj(pageNum);
        //调用后台查询方法
        var resultObj = Instance.ChangTabDoPostBack(processObj, false);

        var pagesize = resultObj == null ? 0 : resultObj.PageCount;

        var totalcount = resultObj == null ? 0 : resultObj.TotalCount

        var pagerObj = Instance.CreatePageObj(pageNum, pagesize, 15, totalcount, Instance.PagerCallBack);

        Instance.InitPager(pagerObj);
    }

    //切换Tab页事件
    this.ChangTabDoPostBack = function (processObj, isInitPager) {
        if (processObj.ProjectAuditStatus != "A" && processObj.ProjectAuditStatus != "D") {
            alert("传入参数不合法！");
            return false;
        }
        //序列化参数
        var jsonObj = Global.toJSON(processObj);

        var result = Instance.BackgroundInvoke.GetProjectAuditList(jsonObj);

        var resultObj = Global.evalJSON(result.value);

        //绑定数据
        if (resultObj) {
            Instance.Clear();
            $.each(resultObj.Body, function (index, item) {
                var tr = "<tr><td align=\"left\" style=\"height:18px;\"><img src=\"../Images/proj.png\" style=\"width:16px;height:16px;margin-left:1px;\" />" + item.ProjectName + "</td>";
                if (index % 2 == 0) {
                    tr = "<tr style=\"background-color:#f0f0f0;\"><td align=\"left\"><img src=\"../Images/proj.png\" style=\"width:16px;height:16px;margin-left:1px;\" />" + item.ProjectName + "</td>";
                }

                tr += "<td>" + item.InDateString + "</td>";
                tr += "<td>" + item.InUserName + "</td>";
                if (processObj.ProjectAuditStatus != "A") {
                    tr += "<td><div class=\"progressbar\" percent=\"" + item.Percent + "\" id=\"auditLocusContainer\" action=\"P\" referencesysno=\"" + item.ProjectAuditSysNo + "\" style=\"cursor: pointer;\"></div></td>";
                } else {
                    tr += "<td>未提交审核</td>"
                }
                tr += "<td><a href=\"ProjectAudit/ProjectAudit.aspx?projectSysNo=" + item.ProjectSysNo + "&projectAuditSysNo=" + item.ProjectAuditSysNo + "\">点击审核</a></td>";
                tr += "</tr>";
                if (processObj.ProjectAuditStatus == "A") {
                    $("#tableResult_01").append(tr);
                } else {
                    $("#tableResult_02").append(tr);
                }
            });
            $.each($(".progressbar"), function () {
                var percent = parseFloat($(this).attr("percent"));
                $(this).progressbar({
                    value: percent
                });
            });

            ChangedBackgroundColor();
        }

        //是否需要初始化分页控件
        if (isInitPager) {
            var pagerObj = Instance.CreatePageObj(processObj.PageCurrent, resultObj.PageCount, processObj.PageSize, resultObj.TotalCount, Instance.PagerCallBack);
            //分别处理未审核，和已审核的分页控件
            Instance.InitPager(pagerObj);
        }
        return resultObj;
    }

    //初始化调用分页函数
    Instance.ChangTabDoPostBack(Instance.CreateProcessObj(1), true);

    //绑定Tabs切换事件
    this.TabControl.find("a[href ^=#tabs]").bind("click", function () {
        var panelIndex = $(this).attr("index");
        Instance.CurrentSelectedIndex = panelIndex;
        $(this).css({ "color": "black", "textDecoration": "none" }).parents("li:first").siblings("li:first").find("a:first").css({ "color": "blue", "textDecoration": "inline" });
        $("div[id=" + $(this).attr("href").replace("#", "") + "]", container).show().siblings("div[id^=tabs]:first").hide();
        //调用后台方法
        Instance.ChangTabDoPostBack(Instance.CreateProcessObj(1), true);
        return false;
    });
}