﻿$(function () {
    CommonControl.SetFormWidth();
    //行背景
    $("#gv_project tr").hover(function() {
        $(this).addClass("tr_in");
    }, function() {
        $(this).removeClass("tr_in");
    });
    //得到实际的现实高度
    var h = 300;
    //表格滚动
//    $("#gv_project").chromatable({
//        width: "100%",
//        height: h,
//        scrolling: "true"
//    });
    //表单表格
    $(".cls_show_cst_jiben tr:odd").attr("style", "background-color:#FFF");
    //隔行变色
    $("#gv_project tr:odd").css({ background: "White" });
    //查询
    $("#btn_search").click(function() {
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if ($("#txtproAcount").val() != "") {
            if (!reg_math.test($("#txtproAcount").val())) {
                alert("合同额请输入数字！");
                return false;
            }
        }
        if ($("#txtproAcount2").val() != "") {
            if (!reg_math.test($("#txtproAcount2").val())) {
                alert("合同额请输入数字！");
                return false;
            }
        }
        if ($("#txt_buildArea").val() != "") {
            if (!reg_math.test($("#txt_buildArea").val())) {
                alert("建筑规模请输入数字！");
                return false;
            }
        }
        if ($("#txt_buildArea2").val() != "") {
            if (!reg_math.test($("#txt_buildArea2").val())) {
                alert("建筑规模请输入数字！");
                return false;
            }
        }
    });
    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });


    //提示
    $(".cls_column").tipsy({ opacity: 1 });


    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = "0";
    var autoComplete = new AutoComplete(paramEntity, $("#txt_proName"));
    autoComplete.GetAutoAJAX();

})