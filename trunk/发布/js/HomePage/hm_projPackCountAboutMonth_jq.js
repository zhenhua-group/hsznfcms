﻿$(function () {
    var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").val();
    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    var month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/hm_projCountHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '部门名称', '文件数量(张)', '待提交(张)', '待打印(张)', '已打印(张)', '已删除(张)', '已备档(张)'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 45, align: 'center' },
                             { name: 'unit_ID', index: 'unit_ID', hidden: true, editable: true },
                             { name: 'unit_Name', index: 'unit_Name', width: 200 },
                             { name: 'wjsl', index: 'wjsl', width: 100, align: 'center' },
                              { name: 'DTJ', index: 'DTJ', width: 100, align: 'center' },
                              { name: 'DDY', index: 'DDY', width: 100, align: 'center' },
                              { name: 'YDY', index: 'YDY', width: 100, align: 'center' },
                              { name: 'YSC', index: 'YSC', width: 100, align: 'center' },
                              { name: 'YGD', index: 'YGD', width: 100, align: 'center' }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "projPackCount11", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "unit": unit, "year": year, "month": month },
        loadonce: false,
        sortname: 'unit_ID',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,

        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/hm_projCountHandler.ashx?action=projPackCount11&strwhere=" + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                closeOnEscape: true,
                closeAfterDelete: true,
                reloadAfterSubmit: true,
                afterSubmit: function (response, postdata) {
                    if (response.responseText == "") {

                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'unit_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );
    //-----     
    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").val();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/hm_projCountHandler.ashx",
            postData: { "action": "projPackCount11", "strwhere": strwhere, "unit": unit, "year": year, "month": month },
            page: 1
        }).trigger("reloadGrid");
    });
});
//查看人员图标
function colShowMemFormatter(celvalue, options, rowData) {

    var pageurl = "<img src='/images/" + rowData["mem_Sex"] + ".png' style=\"width: 16px; height: 16px;\" />";
    return pageurl + celvalue;
}

//查看项目
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "hm_showProject.aspx?unitid=" + rowData["unit_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}

//统计 
function completeMethod() {

    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var sumproE = 0, sumpro = 0, sumproA = 0, sumproB = 0, sumproC = 0, sumproD = 0;
    //二种写法
    $(rowIds).each(function () {
        sumproE = sumproE + parseInt($("#" + this).find("td").eq(4).text());
        sumpro = sumpro + parseInt($("#" + this).find("td").eq(5).text());
        sumproA = sumproA + parseInt($("#" + this).find("td").eq(6).text());
        sumproB = sumproB + parseInt($("#" + this).find("td").eq(7).text());
        sumproC = sumproC + parseInt($("#" + this).find("td").eq(8).text());
        sumproD = sumproD + parseInt($("#" + this).find("td").eq(9).text());
        $("#" + this).find("td").eq(1).text(this);
    });

    $("#jqGrid").footerData('set', { unit_Name: "合计:", wjsl: sumproE, DTJ: sumpro, DDY: sumproA, YDY: sumproB, YSC: sumproC, YGD: sumproD }, false);

}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        //$("#nodata").show();
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
    }
    else {
        $("#nodata").hide();
    }
}