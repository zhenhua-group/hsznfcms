﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/hm_projCountHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '项目工号', '项目名称', '建设单位 ', '开始时间', '结束时间', '项目状态', '方案设计(图纸数)', '初步设计(图纸数)', '施工图设计(图纸数)'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 45, align: 'center' },
                             { name: 'pro_ID', index: 'AT.pro_ID', hidden: true, editable: true },
                             { name: 'pro_Order', index: 'AT.pro_Order', width: 100, align: 'center' },
                             { name: 'pro_Name', index: 'AT.pro_Name', width: 200, formatter: colShowMemFormatter },
                             { name: 'pro_BuildUnit', index: 'AT.pro_BuildUnit', width: 150},
                             { name: 'kssj', index: 'AT.pro_StartTime', width: 80, align: 'center' },
                             { name: 'jssj', index: 'AT.pro_FinishTime', width: 80, align: 'center' },
                             { name: 'pro_Status', index: 'AT.pro_Status', width: 80, align: 'center', formatter: colShowStatusFormatter },
                             { name: 'FASJ', index: 'FASJ', width: 120, align: 'center' },
                             { name: 'CBSJ', index: 'CBSJ', width: 120, align: 'center' },
                             { name: 'SGTSJ', index: 'SGTSJ', width: 120, align: 'center' }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },//	--方案设计FASJ 初步设计CBSJ 施工图设计SGTSJ
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "showprojectAboutStatus" },
        loadonce: false,
        sortname: 'AT.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/hm_projCountHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },

            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                closeOnEscape: true,
                closeAfterDelete: true,
                reloadAfterSubmit: true,
                afterSubmit: function (response, postdata) {
                    if (response.responseText == "") {

                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'unit_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/hm_projCountHandler.ashx?action=showprojectAboutStatus",
            postData: { 'strwhere': strwhere },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
});
//查看人员
function colShowMemFormatter(celvalue, options, rowData) {

    return celvalue;
}
function colShowStatusFormatter(celvalue, options, rowData) {
    if ($.trim(celvalue) == "") {
        return "进行中";
    } else {
        return celvalue;
    }


}
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var summem = 0, sumpro = 0, sumproA = 0, sumproB = 0, sumproC = 0, sumproD = 0;
    $(rowIds).each(function () {
        sumproA = sumproA + parseInt($("#" + this).find("td").eq(9).text());
        sumproB = sumproB + parseInt($("#" + this).find("td").eq(10).text());
        sumproC = sumproC + parseInt($("#" + this).find("td").eq(11).text());
        //sumproD = sumproD + parseInt($("#" + this).find("td").eq(10).text());
        $("#" + this).find("td").eq(1).text(this);
    });
    $("#jqGrid").footerData('set', { pro_Name: "合计:", FASJ: sumproA, CBSJ: sumproB, SGTSJ: sumproC }, false);
}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        // $("#nodata").show();
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
    }
    else {
        $("#nodata").hide();
    }
}