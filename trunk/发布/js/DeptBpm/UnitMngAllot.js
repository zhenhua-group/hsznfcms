﻿
var mngAllot = function () {
  
    var pageLoad = function () {
        ////施工图部门
        //var archArr = ['231', '232', '237', '238', '239'];
        //if (archArr.indexOf($("#hidUnitID").val()) > -1) {
        //    $("#tbDataOne").show();
        //    $("#tbDataTwo").hide();
        //    $("#tbDataThree").hide();
        //}
        ////方案部门
        //var fanArr = ['233', '234', '236'];
        //if (fanArr.indexOf($("#hidUnitID").val()) > -1) {
        //    $("#tbDataOne").hide();
        //    $("#tbDataTwo").hide();
        //    $("#tbDataThree").show();
        //}
        ////特殊部门
        //var teshuArr = ['240', '241', '242', '243', '244', '235'];
        //if (teshuArr.indexOf($("#hidUnitID").val()) > -1) {
        //    $("#tbDataOne").hide();
        //    $("#tbDataTwo").show();
        //    $("#tbDataThree").hide();
        //}

        //施工图部门输入
        $(":text", $("#tbDataOne tbody")).change(function () {

            var obj = $(this);
            var reg_math = /^[-+]?[0-9]+(\.[0-9]+)?$/;

            //输入.
            if ($(this).val().indexOf('.') > -1) {
                if ($(this).val().indexOf('.') <= 0 || $(this).val().split('.').length > 2) {
                    $(this).val("");
                    return false;
                }
            }
            //小数点后没有数不计算
            if ($(this).val().indexOf('.') == $(this).val().length - 1) {
                return false;
            }


            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算占比
            changeAllotVal(obj);
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });

        //方案部门输入
        $(":text", $("#tbDataThree tbody")).change(function () {

            var obj = $(this);
            var reg_math = /^[-+]?[0-9]+(\.[0-9]+)?$/;

            //输入.
            if ($(this).val().indexOf('.') > -1) {
                if ($(this).val().indexOf('.') <= 0 || $(this).val().split('.').length > 2) {
                    $(this).val("");
                    return false;
                }
            }
            //小数点后没有数不计算
            if ($(this).val().indexOf('.') == $(this).val().length - 1) {
                return false;
            }


            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算占比
            changeAllotValFangAn(obj);
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });;

        //特殊部门输入
        $(":text", $("#tbDataTwo tbody")).change(function () {

            var obj = $(this);
            var reg_math = /^[-+]?[0-9]+(\.[0-9]+)?$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算占比
            changeAllotValSpecial(obj);
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });

        //特殊部门2输入
        $(":text", $("#tbDataTwo2 tbody")).change(function () {

            var obj = $(this);
            var reg_math = /^[-+]?[0-9]+(\.[0-9]+)?$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算占比
            changeAllotValSpecial2(obj);
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });

        //离职人员输入
        $(":text", $("#tbDataOne2 tbody")).change(function () {

            var obj = $(this);
            var reg_math = /^[-+]?[0-9]+(\.[0-9]+)?$/;

            //输入.
            if ($(this).val().indexOf('.') > -1) {
                if ($(this).val().indexOf('.') <= 0 || $(this).val().split('.').length > 2) {
                    $(this).val("");
                    return false;
                }
            }
            //小数点后没有数不计算
            if ($(this).val().indexOf('.') == $(this).val().length - 1) {
                return false;
            }

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算占比
            changeOutAllotVal(obj);
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });

        //施工图部门差值系数填写计算加权系数
        $("#txtChazhiXishu").change(function () {
            var obj = $(this);
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算加权系数
            changeAllotWeights();
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });
        //默认加载
        if ($("#hidIsSaved").val() == "False") {
         changeAllotWeights();
        }

        //方案部门差值系数填写计算加权系数
        $("#txtChazhiXishu2").change(function () {
            var obj = $(this);
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算加权系数
            changeAllotWeightsFangAn();
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });;

        //默认加载
        if ($("#hidIsSaved").val() == "False") {
            changeAllotWeightsFangAn();
        }
        //所有按钮不可用
        if ($("#hidIsCheck").val() == "chk") {
            $(":text").attr("readonly", true);
            $(".btn.btn-sm.red").attr("class", "btn btn-sm red disabled");
            $("#ctl00_ContentPlaceHolder1_btn_Export").attr("class", "btn btn-sm red");
            $("#ctl00_ContentPlaceHolder1_btn_Export2").attr("class", "btn btn-sm red");
            $("#ctl00_ContentPlaceHolder1_btn_Export3").attr("class", "btn btn-sm red");
            $("#ctl00_ContentPlaceHolder1_btn_Export4").attr("class", "btn btn-sm red");
            $("#ctl00_ContentPlaceHolder1_Button1").attr("class", "btn btn-sm red");
            //$("#ctl00_ContentPlaceHolder1_Button8").attr("class", "btn btn-sm red");

            //如果是查看就排序  2016年12月13日   查看发现如果是查看界面就给表格排序

            //initOrderTableAll();
        }

        //无论查看与编辑都给表格排序
        //initOrderTableAll();

        $("#btnOrder").click(function () {
            initOrderTableAll();
        }).attr("class", "btn btn-sm red");

        $("#btnDefault").click(function () {
            window.location.reload();
        });

        //保存
        $("#btnSave").click(function () {
            saveAllotDetail('save');
            saveOutAllotDetail('save');
        });
        //提交保存
        $("#btnSubmit").click(function () {
            saveAllotDetail('submit');
            saveOutAllotDetail('submit');
        });
        //方案部门
        $("#btnSave3").click(function () {
            saveAllotDetailFangAn('save');
            saveOutAllotDetail('save');
        });
        $("#btnSubmit3").click(function () {
            saveAllotDetailFangAn('submit');
            saveOutAllotDetail('submit');
        });
        //特殊部门
        $("#btnSave2").click(function () {
            saveAllotDetailSpecial('save');
            saveOutAllotDetail('save');
        });
        $("#btnSubmit2").click(function () {
            saveAllotDetailSpecial('submit');
            saveOutAllotDetail('submit');
        });

        
        //特殊部门2
        $("#btnSave4").click(function () {
            saveAllotDetailSpecial2('save');
            saveOutAllotDetail('save');
        });
        $("#btnSubmit4").click(function () {
            saveAllotDetailSpecial2('submit');
            saveOutAllotDetail('submit');
        });
        
 
        //离职人员
        //$("#btnSaveOut").click(function () {
        //    saveOutAllotDetail('save');
        //});
        //$("#btnSubmitOut").click(function () {
        //    saveOutAllotDetail('submit');
        //});


        if ($("#hidIsSubmit").val() == "True") {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();
        }
        else {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.ReadMsg();
        }

        //建筑部门归档
        $("#btnBak").click(function () {
            archBak();
        });
        //方案部门归档
        $("#btnFanganBak").click(function () {
            fanganBak();
        });
        //特殊一
        $("#btnTeshu1").click(function () {
            teshuoneBak();
        });
        //特殊二
        $("#btnTeshu2").click(function () {
            teshutwoBak();
        });
    }

    //计算值
    var changeAllotVal = function (obj) {

        //项目经理调整
        //var jltz = obj.parent().parent().find("td").eq(12).children("input").val();
        var jltz = obj.parent().parent().find("td").eq(11).children("input").val();
        //优秀员工
        //var yxyg = obj.parent().parent().find("td").eq(14).children("input").val();
        var yxyg = obj.parent().parent().find("td").eq(13).children("input").val();
        //预发奖金
        var yfjj = obj.parent().parent().find("td").eq(4).children("span").text();
        var yfjj = obj.parent().parent().find("td").eq(15).children("span").text();

        if (jltz != "" && yxyg != "") {

            //计算项目奖金调整值
            //var xmjj = parseFloat(obj.parent().parent().find("td").eq(11).children("span").text());
            //var xmjj = parseFloat(obj.parent().parent().find("td").eq(11).text());
            var xmjj = parseFloat(obj.parent().parent().find("td").eq(10).text());
            var xmtz = xmjj + parseFloat(jltz);
            //项目奖金调整值
            //obj.parent().parent().find("td").eq(13).children("span").text(xmtz.toFixed(0))
            //obj.parent().parent().find("td").eq(13).text(xmtz.toFixed(0));
            obj.parent().parent().find("td").eq(12).text(xmtz.toFixed(0));

            //部门内奖金
            //var unitjiang = parseFloat(obj.parent().parent().find("td").eq(10).children("span").text());
            //var unitjiang = parseFloat(obj.parent().parent().find("td").eq(10).text());
            var unitjiang = parseFloat(obj.parent().parent().find("td").eq(9).text());
            var yxygval = parseFloat(yxyg);
            //合计
            var hj = unitjiang + yxygval + xmtz - parseFloat(yfjj);
            //obj.parent().parent().find("td").eq(15).children("span").text(hj.toFixed(2));
            //obj.parent().parent().find("td").eq(15).text(hj.toFixed(2));
            obj.parent().parent().find("td").eq(16).text(hj.toFixed(2));

            //合计 预发
            var hjyf = unitjiang + yxygval + xmtz;
            //obj.parent().parent().find("td").eq(15).children("span").text(hj.toFixed(2));
            //obj.parent().parent().find("td").eq(16).text(hjyf.toFixed(2));
            obj.parent().parent().find("td").eq(14).text(hjyf.toFixed(2));

            //倍数
            //var jbgz = obj.parent().parent().find("td").eq(3).children("span").text();
            var jbgz = obj.parent().parent().find("td").eq(3).text();
            var beishu = 0;
            if (jbgz != 0) {
                //beishu = hj / jbgz;
                beishu = hjyf / jbgz;
            }
            //obj.parent().parent().find("td").eq(16).children("span").text(beishu.toFixed(2));
            obj.parent().parent().find("td").eq(17).children("span").text(beishu.toFixed(2));

            //算差额
            //总额
            var sbn = parseFloat(obj.parent().parent().find("td").eq(18).children("span").text());
            var xbn = parseFloat(obj.parent().parent().find("td").eq(20).children("span").text());
            var chazhi = hjyf - sbn;
            var chazhi2 = hjyf - xbn;
            obj.parent().parent().find("td").eq(19).children("span").text(chazhi.toFixed(2));
            obj.parent().parent().find("td").eq(21).children("span").text(chazhi2.toFixed(2));
        }
    }
    //方案部门
    var changeAllotValFangAn = function (obj) {

        //部门内奖金经理调整
        //var jltz = obj.parent().parent().find("td").eq(11).children("input").val();
        var jltz = obj.parent().parent().find("td").eq(10).children("input").val();
        //优秀员工
        //var yxyg = obj.parent().parent().find("td").eq(13).children("input").val();
        var yxyg = obj.parent().parent().find("td").eq(12).children("input").val();
        //预发奖金
        //var yfjj = obj.parent().parent().find("td").eq(4).children("span").text();
        var yfjj = obj.parent().parent().find("td").eq(14).children("span").text();

        if (jltz != "" && yxyg != "") {

            //部门内奖金计算值
            //var bmnjjjs = parseFloat(obj.parent().parent().find("td").eq(10).children("span").text());
            //var bmnjjjs = parseFloat(obj.parent().parent().find("td").eq(10).text());
            var bmnjjjs = parseFloat(obj.parent().parent().find("td").eq(9).text());
            var xmtz = bmnjjjs + parseFloat(jltz);
            //部门内奖金经理调整值
            //obj.parent().parent().find("td").eq(12).children("span").text(xmtz.toFixed(0));
            //obj.parent().parent().find("td").eq(12).text(xmtz.toFixed(0));
            obj.parent().parent().find("td").eq(11).text(xmtz.toFixed(0));

            //部门内奖金
            //var unitjiang = parseFloat(obj.parent().parent().find("td").eq(10).children("span").text());
            //var unitjiang = parseFloat(obj.parent().parent().find("td").eq(10).text());
            var unitjiang = parseFloat(obj.parent().parent().find("td").eq(9).text());
            var yxygval = parseFloat(yxyg);
            //合计
            var hj = yxygval + xmtz - parseFloat(yfjj);
            //obj.parent().parent().find("td").eq(14).children("span").text(hj.toFixed(2));
            //obj.parent().parent().find("td").eq(14).text(hj.toFixed(2));
            obj.parent().parent().find("td").eq(15).text(hj.toFixed(2));

            //合计 预发
            var hjyf = yxygval + xmtz;
            //obj.parent().parent().find("td").eq(14).children("span").text(hj.toFixed(2));
            //obj.parent().parent().find("td").eq(15).text(hjyf.toFixed(2));
            obj.parent().parent().find("td").eq(13).text(hjyf.toFixed(2));

            //倍数
            //var jbgz = obj.parent().parent().find("td").eq(3).children("span").text();
            var jbgz = obj.parent().parent().find("td").eq(3).text();
            var beishu = 0;
            if (jbgz != 0) {
                beishu = hjyf / jbgz;
            }
            obj.parent().parent().find("td").eq(16).children("span").text(beishu.toFixed(2));

            //算差额
            //总额
            var sbn = parseFloat(obj.parent().parent().find("td").eq(17).children("span").text())
            var xbn = parseFloat(obj.parent().parent().find("td").eq(19).children("span").text())
            var chazhi = hjyf - sbn;
            var chazhi2 = hjyf - xbn;
            obj.parent().parent().find("td").eq(18).children("span").text(chazhi.toFixed(2));
            obj.parent().parent().find("td").eq(20).children("span").text(chazhi2.toFixed(2));
        }
    }
    //特殊部门
    var changeAllotValSpecial = function (obj) {
        //部门经理调整
        //var jltz = obj.parent().parent().find("td").eq(9).children("input").val();
        var jltz = obj.parent().parent().find("td").eq(8).children("input").val();
        //优秀员工
        //var yxyg = obj.parent().parent().find("td").eq(10).children("input").val();
        var yxyg = obj.parent().parent().find("td").eq(9).children("input").val();
        //预发奖金
        //var yfjj = obj.parent().parent().find("td").eq(4).children("span").text();
        var yfjj = obj.parent().parent().find("td").eq(11).children("span").text();

        if (jltz != "" && yxyg != "") {

            var jltzval = parseFloat(jltz);
            var yxygval = parseFloat(yxyg);
            //合计
            var hj = jltzval + yxygval - parseFloat(yfjj);
            //obj.parent().parent().find("td").eq(11).children("span").text(hj.toFixed(2));
            //obj.parent().parent().find("td").eq(11).text(hj.toFixed(2));
            obj.parent().parent().find("td").eq(12).text(hj.toFixed(2));

            //合计 预发
            var hjyf = jltzval + yxygval;
            //obj.parent().parent().find("td").eq(11).children("span").text(hj.toFixed(2));
            obj.parent().parent().find("td").eq(10).text(hjyf.toFixed(2));

            //倍数
            //var jbgz = obj.parent().parent().find("td").eq(3).children("span").text();
            var jbgz = obj.parent().parent().find("td").eq(3).text();
            var beishu = 0;
            if (jbgz != 0) {
                //beishu = hj / jbgz;
                beishu = hjyf / jbgz;
            }
            obj.parent().parent().find("td").eq(13).children("span").text(beishu.toFixed(2));

            //算差额
            //总额
            var sbn = parseFloat(obj.parent().parent().find("td").eq(14).children("span").text())
            var xbn = parseFloat(obj.parent().parent().find("td").eq(16).children("span").text())
            var chazhi = hjyf - sbn;
            var chazhi2 = hjyf - xbn;
            obj.parent().parent().find("td").eq(15).children("span").text(chazhi.toFixed(2));
            obj.parent().parent().find("td").eq(17).children("span").text(chazhi2.toFixed(2));

        }
    }
    //特殊部门2
    var changeAllotValSpecial2 = function (obj) {
        //部门经理调整
        //var jltz = obj.parent().parent().find("td").eq(5).children("input").val();
        var jltz = obj.parent().parent().find("td").eq(4).children("input").val();
        //优秀员工
        //var yxyg = obj.parent().parent().find("td").eq(6).children("input").val();
        var yxyg = obj.parent().parent().find("td").eq(5).children("input").val();
        //预发奖金
        //var yfjj = obj.parent().parent().find("td").eq(4).children("span").text();
        var yfjj = obj.parent().parent().find("td").eq(7).children("span").text();

        if (jltz != "" && yxyg != "") {

            var jltzval = parseFloat(jltz);
            var yxygval = parseFloat(yxyg);
            //合计
            var hj = jltzval + yxygval - parseFloat(yfjj);
            //obj.parent().parent().find("td").eq(7).children("span").text(hj.toFixed(2));
            //obj.parent().parent().find("td").eq(7).text(hj.toFixed(2));
            obj.parent().parent().find("td").eq(8).text(hj.toFixed(2));

            //合计
            var hjyf = jltzval + yxygval;
            //obj.parent().parent().find("td").eq(7).children("span").text(hj.toFixed(2));
            obj.parent().parent().find("td").eq(6).text(hjyf.toFixed(2));

            //倍数
            //var jbgz = obj.parent().parent().find("td").eq(3).children("span").text();
            var jbgz = obj.parent().parent().find("td").eq(3).text();
            var beishu = 0;
            if (jbgz != 0) {
                //beishu = hj / jbgz;
                beishu = hjyf / jbgz;
            }
            obj.parent().parent().find("td").eq(9).children("span").text(beishu.toFixed(2));

            //算差额
            //总额
            var sbn = parseFloat(obj.parent().parent().find("td").eq(10).children("span").text())
            var xbn = parseFloat(obj.parent().parent().find("td").eq(12).children("span").text())
            var chazhi = hjyf - sbn;
            var chazhi2 = hjyf - xbn;
            obj.parent().parent().find("td").eq(11).children("span").text(chazhi.toFixed(2));
            obj.parent().parent().find("td").eq(13).children("span").text(chazhi2.toFixed(2));

        }
    }
    //离职员工
    var changeOutAllotVal = function (obj) {
        //优秀员工
        var jbgz = obj.parent().parent().find("td").eq(5).children("span").text();
        //项目经理调整
        var bmtz = obj.parent().parent().find("td").eq(6).children("input").val();

        var beizhu = 0;
        if (parseFloat(jbgz) != 0) {
            beizhu = bmtz / jbgz;
        }

        obj.parent().parent().find("td").eq(7).children("span").text(beizhu.toFixed(2));
    }
    //特殊部门
    var saveAllotDetailSpecial = function (action) {

        //对象
        var arrDetail = new Array();
        var index = 0;
        $("#tbDataTwo tbody tr").each(function (i) {

            var obj = $(this);
            if (obj.find("td").eq(4).children("input").val() != "" && obj.find("td").eq(5).children("input").val() != "") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuno").val(),
                    MemID: obj.find("td").eq(0).children("span").attr("memid"),
                    MemName: obj.find("td").eq(0).children("span").text(),
                    UnitID: $("#hidUnitID").val(),
                    UnitName: $("#spunitName").text(),
                    jbgz: obj.find("td").eq(1).text(),//obj.find("td").eq(1).children("span").text(),
                    gzsj: obj.find("td").eq(2).children("input").val(),//obj.find("td").eq(2).children("span").text(),
                    pjgz: obj.find("td").eq(3).text(),//obj.find("td").eq(3).children("span").text(),
                    zphu: obj.find("td").eq(4).children("span").text(),
                    orderid: obj.find("td").eq(5).text() == "" ? "0" : obj.find("td").eq(6).text(),///
                    bmjlpj: obj.find("td").eq(6).children("span").text(),
                    orderid2: obj.find("td").eq(7).text(),//obj.find("td").eq(8).children("span").text(),
                    bmnjs: obj.find("td").eq(8).children("input").val(),
                    yxyg: obj.find("td").eq(9).children("input").val(),
                    hj: obj.find("td").eq(12).text(),//obj.find("td").eq(11).children("span").text(),
                    hjyf: obj.find("td").eq(10).text(),
                    beizhu: obj.find("td").eq(13).children("span").text(),
                    beforyear: obj.find("td").eq(16).children("span").text(),
                    chazhi: obj.find("td").eq(17).children("span").text(),
                    beforyear2: obj.find("td").eq(14).children("span").text(),
                    chazhi2: obj.find("td").eq(15).children("span").text(),
                    insertUserID: $("#hidUserSysNo").val(),
                    Stat: action == "submit" ? "1" : "0"
                };
            }
        });

        var jsonEntity = JSON.stringify(arrDetail);

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var postaction = "savememsmngallot";
        var data = { "action": postaction, "data": jsonEntity };

        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    if (action == "save") {
                        //设置成功！
                        alert("部门绩效调整数据保存成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        //提交成功
                        alert("部门绩效调整数据提交成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }
                    //按钮不可用
                    $("#btnSave2").attr("class", "btn btn-sm default disabled");
                    //location.reload();
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }
    //特殊部门2
    var saveAllotDetailSpecial2 = function (action) {

        //对象
        var arrDetail = new Array();
        var index = 0;
        $("#tbDataTwo2 tbody tr").each(function (i) {

            var obj = $(this);
            if (obj.find("td").eq(4).children("input").val() != "" && obj.find("td").eq(5).children("input").val() != "") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuno").val(),
                    MemID: obj.find("td").eq(0).children("span").attr("memid"),
                    MemName: obj.find("td").eq(0).children("span").text(),
                    UnitID: $("#hidUnitID").val(),
                    UnitName: $("#spunitName").text(),
                    jbgz: obj.find("td").eq(1).text(),//obj.find("td").eq(1).children("span").text(),
                    gzsj: obj.find("td").eq(2).children("input").val(),//obj.find("td").eq(2).children("span").text(),
                    pjgz: obj.find("td").eq(3).text(),//obj.find("td").eq(3).children("span").text(),
                    zphu: "0",
                    orderid: "0",
                    bmjlpj: "0",
                    orderid2: "0",
                    bmnjs: obj.find("td").eq(4).children("input").val(),
                    yxyg: obj.find("td").eq(5).children("input").val(),
                    hj: obj.find("td").eq(8).text(),//obj.find("td").eq(7).children("span").text(),
                    hjyf: obj.find("td").eq(6).text(),
                    beizhu: obj.find("td").eq(9).children("span").text(),
                    beforyear: obj.find("td").eq(12).children("span").text(),
                    chazhi: obj.find("td").eq(13).children("span").text(),
                    beforyear2: obj.find("td").eq(10).children("span").text(),
                    chazhi2: obj.find("td").eq(11).children("span").text(),
                    insertUserID: $("#hidUserSysNo").val(),
                    Stat: action == "submit" ? "1" : "0"
                };
            }
        });

        var jsonEntity = JSON.stringify(arrDetail);

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var postaction = "savememsmngallot";
        var data = { "action": postaction, "data": jsonEntity };
       
        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    if (action == "save") {
                        //设置成功！
                        alert("部门绩效调整数据保存成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        ////确认提交
                        if (!confirm('确认要提交？')) {
                            return false;
                        }

                        //提交成功
                        alert("部门绩效调整数据提交成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                        location.reload();
                    }
                    //按钮不可用
                    $("#btnSave2").attr("class", "btn btn-sm default disabled");
                    //location.reload();
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }
    //保存
    var saveAllotDetail = function (action) {

        if ($.trim($("#txtChazhiXishu").val()) == "") {
            alert("请完整填写整体差距调整系数，总建筑师调整，优秀员工！");
            return false;
        }

        //对象
        var arrDetail = new Array();
        var index = 0;
        $("#tbDataOne tbody tr").each(function (i) {

            var obj = $(this);
            //if (obj.find("td").eq(11).children("input").val() != "" && obj.find("td").eq(13).children("input").val() != "") {
                if (obj.find("td").eq(10).children("input").val() != "" && obj.find("td").eq(12).children("input").val() != "") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuno").val(),
                    MemID: obj.find("td").eq(0).children("span").attr("memid"),
                    MemName: obj.find("td").eq(0).children("span").text(),
                    UnitID: $("#hidUnitID").val(),
                    UnitName: $.trim($("#spunitName").text()),
                    jbgz: $.trim(obj.find("td").eq(1).text()),//obj.find("td").eq(1).children("span").text(),//基本工资
                    gzsj: $.trim(obj.find("td").eq(2).children("input").val()),//$.trim(obj.find("td").eq(2).children("span").text()),
                    pjgz: $.trim(obj.find("td").eq(3).text()),//obj.find("td").eq(3).children("span").text(),//平均月工资
                    zphu: $.trim(obj.find("td").eq(4).text()),//$.trim(obj.find("td").eq(5).text()),obj.find("td").eq(5).children("span").text(),//自评互评
                    orderid: $.trim(obj.find("td").eq(5).text()),//$.trim(obj.find("td").eq(6).text())obj.find("td").eq(6).children("span").text(),
                    bmjlpj: $.trim(obj.find("td").eq(6).text()),//$.trim(obj.find("td").eq(7).text()),obj.find("td").eq(7).children("span").text(),//部门经理评价
                    orderid2: $.trim(obj.find("td").eq(7).text()),//$.trim(obj.find("td").eq(8).text()),obj.find("td").eq(8).children("span").text(),
                    jqxs: $.trim(obj.find("td").eq(8).children("span").text()),//$.trim(obj.find("td").eq(9).children("span").text()),
                    bmnjs: $.trim(obj.find("td").eq(9).text()),//$.trim(obj.find("td").eq(10).text()),部门内奖金计算
                    xmjjjs: $.trim(obj.find("td").eq(10).text()),//$.trim(obj.find("td").eq(11).text()),obj.find("td").eq(11).children("span").text(),
                    xmjjmng: $.trim(obj.find("td").eq(11).children("input").val()),//$.trim(obj.find("td").eq(12).children("input").val()),
                    xmjjtz: $.trim(obj.find("td").eq(12).text()),//$.trim(obj.find("td").eq(13).text()),项目奖金调整值
                    yxyg: $.trim(obj.find("td").eq(13).children("input").val()),//$.trim(obj.find("td").eq(14).children("input").val()),
                    hj: $.trim(obj.find("td").eq(16).text()),//$.trim(obj.find("td").eq(15).text()),合计
                    hjyf: $.trim(obj.find("td").eq(14).text()),//$.trim(obj.find("td").eq(16).text()),
                    beizhu: $.trim(obj.find("td").eq(17).children("span").text()),
                    beforyear: $.trim(obj.find("td").eq(20).children("span").text()),
                    chazhi: $.trim(obj.find("td").eq(21).children("span").text()),
                    beforyear2: $.trim(obj.find("td").eq(18).children("span").text()),
                    chazhi2: $.trim(obj.find("td").eq(19).children("span").text()),
                    insertUserID: $("#hidUserSysNo").val(),
                    weights: $("#txtChazhiXishu").val(),
                    Stat: action == "submit" ? "1" : "0"
                };
            }
        });



        if (arrDetail.length == 0) {
            alert('未填写任何相关数据！');
            return false;
        }

        var jsonEntity = JSON.stringify(arrDetail);

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var postaction = "savememsmngallot";
        var data = { "action": postaction, "data": jsonEntity };

        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    if (action == "save") {
                        //设置成功！
                        alert("部门绩效调整数据保存成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        //提交成功
                        alert("部门绩效调整数据提交成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }
                    //按钮不可用
                    $("#btnSave").attr("class", "btn btn-sm default disabled");
                    //location.reload();
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }
    //保存方案部门
    var saveAllotDetailFangAn = function (action) {

        if ($.trim($("#txtChazhiXishu2").val()) == "") {
            alert("请完整填写整体差距调整系数，总建筑师调整，优秀员工！");
            return false;
        }

        //对象
        var arrDetail = new Array();
        var index = 0;
        $("#tbDataThree tbody tr").each(function (i) {

            var obj = $(this);
            //if (obj.find("td").eq(10).children("input").val() != "" && obj.find("td").eq(12).children("input").val() != "") {
                if (obj.find("td").eq(9).children("input").val() != "" && obj.find("td").eq(11).children("input").val() != "") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuno").val(),
                    MemID: obj.find("td").eq(0).children("span").attr("memid"),
                    MemName: obj.find("td").eq(0).children("span").text(),
                    UnitID: $("#hidUnitID").val(),
                    UnitName: $.trim($("#spunitName").text()),
                    jbgz: obj.find("td").eq(1).text(),//obj.find("td").eq(1).children("span").text(),
                    gzsj: obj.find("td").eq(2).children("input").val(),//obj.find("td").eq(2).children("span").text(),
                    pjgz: obj.find("td").eq(3).text(),//obj.find("td").eq(3).children("span").text(),
                    zphu: obj.find("td").eq(4).children("span").text(),//obj.find("td").eq(5).children("span").text(),
                    orderid: obj.find("td").eq(5).text() == "" ? "0" : obj.find("td").eq(5).text(),//
                    bmjlpj: obj.find("td").eq(6).children("span").text(),
                    orderid2: obj.find("td").eq(7).text(),//obj.find("td").eq(8).children("span").text(),
                    jqxs: obj.find("td").eq(8).children("span").text(),
                    bmnjs: obj.find("td").eq(9).text(),//obj.find("td").eq(10).children("span").text(),
                    xmjjjs: "0",//没用
                    xmjjmng: obj.find("td").eq(10).children("input").val(),
                    xmjjtz: obj.find("td").eq(11).text(),//obj.find("td").eq(12).children("span").text(),
                    yxyg: obj.find("td").eq(12).children("input").val(),
                    hj: obj.find("td").eq(15).text(),//obj.find("td").eq(14).children("span").text(),
                    hjyf: obj.find("td").eq(13).text(),
                    beizhu: obj.find("td").eq(16).children("span").text(),
                    beforyear: obj.find("td").eq(19).children("span").text(),
                    chazhi: obj.find("td").eq(20).children("span").text(),
                    beforyear2: obj.find("td").eq(17).children("span").text(),
                    chazhi2: obj.find("td").eq(18).children("span").text(),
                    insertUserID: $("#hidUserSysNo").val(),
                    weights: $("#txtChazhiXishu2").val(),
                    Stat: action == "submit" ? "1" : "0"
                };
            }
        });



        if (arrDetail.length == 0) {
            alert('未填写任何数据！');
            return false;
        }

        var jsonEntity = JSON.stringify(arrDetail);

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var postaction = "savememsmngallot";
        var data = { "action": postaction, "data": jsonEntity };

        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    if (action == "save") {
                        //设置成功！
                        alert("部门绩效调整数据保存成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        //提交成功
                        alert("部门绩效调整数据提交成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }
                    //按钮不可用
                    $("#btnSave").attr("class", "btn btn-sm default disabled");
                    //location.reload();
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }
    //保存离职人员
    var saveOutAllotDetail = function (action) {
        var isnull = false;
        $(":text", $("#tbDataOne2")).each(function (i, item) {
            var val = $(item).val();
            //计算绩效
            if ($.trim(val) == "") {
                isnull = true;
            }
        });

        if (isnull) {
            alert("请完整部门经理调整！");
            return false;
        }


        //对象
        var arrDetail = new Array();
        var index = 0;
        $("#tbDataOne2 tbody tr").each(function (i) {

            var obj = $(this);
            if (obj.find("td").eq(6).children("input").val() != "") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuno").val(),
                    MemID: obj.find("td").eq(0).children("span").attr("memid"),
                    MemName: obj.find("td").eq(0).children("span").text(),
                    UnitID: $("#hidUnitID").val(),
                    UnitName: $.trim($("#spunitName").text()),
                    bmjltz: obj.find("td").eq(6).children("input").val(),
                    beizhu: obj.find("td").eq(7).children("span").text(),
                    Stat: action == "submit" ? "1" : "0"
                };
            }
        });

        var jsonEntity = JSON.stringify(arrDetail);

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var postaction = "saveoutmemsmngallot";
        var data = { "action": postaction, "data": jsonEntity };

        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    if (action == "save") {
                        //设置成功！
                        alert("部门绩效调整数据保存成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        //提交成功
                        alert("部门绩效调整数据提交成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }
                    //按钮不可用
                    //$("#btnSaveOut").attr("class", "btn btn-sm default disabled");
                    location.reload();
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }
    //计算加权系数
    var changeAllotWeights = function () {

        //计算加权系数
        $("#tbDataOne tbody tr").each(function (i) {

            var obj = $(this);

            var weights = parseFloat($("#txtChazhiXishu").val());
            //自评互评
            //var maxval = parseFloat(obj.find("td").eq(9).children("span").attr("maxval"));
            var maxval = parseFloat(obj.find("td").eq(8).children("span").attr("maxval"));
            //var minval = parseFloat(obj.find("td").eq(9).children("span").attr("minval"));
            var minval = parseFloat(obj.find("td").eq(8).children("span").attr("minval"));
            //var val = parseFloat(obj.find("td").eq(9).children("span").attr("val"));
            var val = parseFloat(obj.find("td").eq(8).children("span").attr("val"));

            var jqxs = 0;
            if ((maxval - minval) != 0) {
                jqxs = 1 + [val - minval] / [maxval - minval] * (weights - 1);
            }

            if ((maxval - minval) == 0) {
                jqxs = 1;
            }

            //obj.find("td").eq(9).children("span").text(jqxs.toFixed(2));
            obj.find("td").eq(8).children("span").text(jqxs.toFixed(2));
        });

        //计算基本工资总和
        var jbgzall = 0;
        $("#tbDataOne tbody tr").each(function (i) {
            var obj = $(this);
            //计算工资
            //这里要求改为 平均月工资  qpl 2016-9-7
            //var val = parseFloat(obj.find("td").eq(3).children("span").text());
            var val = parseFloat(obj.find("td").eq(3).text());
            //var val2 = parseFloat(obj.find("td").eq(9).children("span").text());
            var val2 = parseFloat(obj.find("td").eq(8).children("span").text());
            jbgzall += (val * val2);
        });
        //部门内奖金
        $("#tbDataOne tbody tr").each(function (i) {
            var obj = $(this);
            //部门内奖金计算值
            //这里要求改为 平均月工资  qpl 2016-9-7
            //var jbgz = parseFloat(obj.find("td").eq(3).children("span").text());
            var jbgz = parseFloat(obj.find("td").eq(3).text());
            var zjj = parseFloat($("#spunitAllCount").text());
            //var jqxs = parseFloat(obj.find("td").eq(9).children("span").text());
            var jqxs = parseFloat(obj.find("td").eq(8).children("span").text());

            var result = 0;
            if (jbgzall != 0) {
                result = (jbgz * jqxs * zjj) / (jbgzall);
            }

            //obj.find("td").eq(10).children("span").text(Math.round((result * 0.01)) * 100);
            //obj.find("td").eq(10).text(Math.round((result * 0.01)) * 100);
            obj.find("td").eq(9).text(Math.round((result * 0.01)) * 100);

        });

    }
    //计算加权系数
    var changeAllotWeightsFangAn = function () {


        //计算加权系数
        $("#tbDataThree tbody tr").each(function (i) {

            var obj = $(this);

            var weights = parseFloat($("#txtChazhiXishu2").val());
            //自评互评
            //var maxval = parseFloat(obj.find("td").eq(9).children("span").attr("maxval"));
            //var minval = parseFloat(obj.find("td").eq(9).children("span").attr("minval"));
            //var val = parseFloat(obj.find("td").eq(9).children("span").attr("val"));
            var maxval = parseFloat(obj.find("td").eq(8).children("span").attr("maxval"));
            var minval = parseFloat(obj.find("td").eq(8).children("span").attr("minval"));
            var val = parseFloat(obj.find("td").eq(8).children("span").attr("val"));

            var jqxs = 0;
            if ((maxval - minval) != 0) {
                jqxs = 1 + [val - minval] / [maxval - minval] * (weights - 1);
            }

            if ((maxval - minval) == 0) {
                jqxs = 1;
            }

            //obj.find("td").eq(9).children("span").text(jqxs.toFixed(2));
            obj.find("td").eq(8).children("span").text(jqxs.toFixed(2));
        });

        //计算基本工资总和
        //这里要求改为 平均月工资  qpl 2016-9-7
        var jbgzall = 0;
        $("#tbDataThree tbody tr").each(function (i) {
            var obj = $(this);
            //计算工资
            //var val = parseFloat(obj.find("td").eq(3).children("span").text());
            var val = parseFloat(obj.find("td").eq(3).text());
            //var val2 = parseFloat(obj.find("td").eq(9).children("span").text());
            var val2 = parseFloat(obj.find("td").eq(8).children("span").text());
            jbgzall += (val * val2);
        });
        //部门内奖金计算值
        $("#tbDataThree tbody tr").each(function (i) {
            var obj = $(this);
            //部门内奖金计算值
            //这里要求改为 平均月工资  qpl 2016-9-7
            //var jbgz = parseFloat(obj.find("td").eq(3).children("span").text());
            var jbgz = parseFloat(obj.find("td").eq(3).text());
            var zjj = parseFloat($("#spunitAllCount3").text());
            //var jqxs = parseFloat(obj.find("td").eq(9).children("span").text());
            var jqxs = parseFloat(obj.find("td").eq(8).children("span").text());

            var result = 0;
            if (jbgzall != 0) {
                result = (jbgz * jqxs * zjj) / (jbgzall);
            }

            //obj.find("td").eq(10).children("span").text(Math.round((result * 0.01)) * 100);
            //obj.find("td").eq(10).text(Math.round((result * 0.01)) * 100);
            obj.find("td").eq(9).text(Math.round((result * 0.01)) * 100);

        });

    }

    //给表格排序
    var initOrderTableAll = function () {

        if ($("#hidIsArch").val() == "True") {
            initOrderTableIsArch();
        }

        if ($("#hidIsFangAn").val() == "True") {
            initOrderTableIsFangAn();
        }

        if ($("#hidIsTeShu").val() == "True") {
            initOrderTableIsTeShu();
        }

        if ($("#hidIsTeShu2").val() == "True") {
            initOrderTableIsTeShu2();
        }
    }

    //给table排序
    var initOrderTableIsArch = function () {

        $("#tbDataOne").dataTable({
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "bSortable": true, "aTargets": [1] },
                { "bSortable": false, "aTargets": [2] },
                { "bSortable": true, "aTargets": [3] },
                { "bSortable": false, "aTargets": [4] },
                { "bSortable": true, "aTargets": [5] },
                { "bSortable": false, "aTargets": [6] },
                { "bSortable": true, "aTargets": [7] },
                { "bSortable": false, "aTargets": [8] },
                { "bSortable": true, "aTargets": [9] },
                { "bSortable": true, "aTargets": [10] },
                { "bSortable": false, "aTargets": [11] },
                { "bSortable": true, "aTargets": [12] },
                { "bSortable": true, "aTargets": [13] },
                { "bSortable": true, "aTargets": [14] },
                { "bSortable": true, "aTargets": [15] },
                { "bSortable": true, "aTargets": [16] },
                { "bSortable": false, "aTargets": [17] },
                { "bSortable": false, "aTargets": [18] },
                { "bSortable": false, "aTargets": [19] },
                { "bSortable": false, "aTargets": [20] },
                { "bSortable": false, "aTargets": [21] },
                { "bSortable": false, "aTargets": [22] },
            ],
            "iDisplayLength": 50
        });


        jQuery('#tbDataOne_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#tbDataOne_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
        //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }

    //给table排序
    var initOrderTableIsFangAn = function () {

        $("#tbDataThree").dataTable({
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "bSortable": true, "aTargets": [1] },
                { "bSortable": false, "aTargets": [2] },
                { "bSortable": true, "aTargets": [3] },
                { "bSortable": false, "aTargets": [4] },
                { "bSortable": true, "aTargets": [5] },
                { "bSortable": false, "aTargets": [6] },
                { "bSortable": true, "aTargets": [7] },
                { "bSortable": false, "aTargets": [8] },
                { "bSortable": true, "aTargets": [9] },
                { "bSortable": false, "aTargets": [10] },
                { "bSortable": true, "aTargets": [11] },
                { "bSortable": false, "aTargets": [12] },
                { "bSortable": true, "aTargets": [13] },
                { "bSortable": true, "aTargets": [14] },
                { "bSortable": true, "aTargets": [15] },
                { "bSortable": true, "aTargets": [16] },
                { "bSortable": false, "aTargets": [17] },
                { "bSortable": false, "aTargets": [18] },
                { "bSortable": false, "aTargets": [19] },
                { "bSortable": false, "aTargets": [20] },
                { "bSortable": false, "aTargets": [21] }
            ],
            "iDisplayLength": 50
        });


        jQuery('#tbDataThree_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#tbDataThree_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
        //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }

    //给table排序
    var initOrderTableIsTeShu = function () {

        $("#tbDataTwo").dataTable({
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "bSortable": true, "aTargets": [1] },
                { "bSortable": false, "aTargets": [2] },
                { "bSortable": true, "aTargets": [3] },
                { "bSortable": false, "aTargets": [4] },
                { "bSortable": false, "aTargets": [5] },
                { "bSortable": true, "aTargets": [6] },
                { "bSortable": false, "aTargets": [7] },
                { "bSortable": true, "aTargets": [8] },
                { "bSortable": true, "aTargets": [9] },
                { "bSortable": false, "aTargets": [10] },
                { "bSortable": true, "aTargets": [11] },
                { "bSortable": false, "aTargets": [12] },
                { "bSortable": false, "aTargets": [13] },
                { "bSortable": false, "aTargets": [14] },
                { "bSortable": false, "aTargets": [15] },
                { "bSortable": false, "aTargets": [16] },
                { "bSortable": false, "aTargets": [17] },
                { "bSortable": false, "aTargets": [18] }
            ],
            "iDisplayLength": 50
        });


        jQuery('#tbDataTwo_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#tbDataTwo_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
        //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }

    //给table排序
    var initOrderTableIsTeShu2 = function () {

        $("#tbDataTwo2").dataTable({
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "bSortable": true, "aTargets": [1] },
                { "bSortable": false, "aTargets": [2] },
                { "bSortable": true, "aTargets": [3] },
                { "bSortable": false, "aTargets": [4] },
                { "bSortable": true, "aTargets": [5] },
                { "bSortable": false, "aTargets": [6] },
                { "bSortable": true, "aTargets": [7] },
                { "bSortable": false, "aTargets": [8] },
                { "bSortable": false, "aTargets": [9] },
                { "bSortable": false, "aTargets": [10] },
                { "bSortable": false, "aTargets": [11] },
                { "bSortable": false, "aTargets": [12] },
                { "bSortable": false, "aTargets": [13] },
                { "bSortable": false, "aTargets": [14] }
            ],
            "iDisplayLength": 50
        });


        jQuery('#tbDataTwo2_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#tbDataTwo2_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
        //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }

    //建筑归档
    var archBak = function () {
        if (!confirm("确定要发起部门内奖金计算归档吗？")) {
            return false;
        }
        if ($("#tbDataOne tbody tr").length == 0) {
            alert("没有要归档的部门信息！");
            return false;
        }

        var renwuid = $("#hidRenwuno").val();
        var renwuName = "";
        var unitid = $("#hidUnitID").val();
        var unitName = $("#spunitName").text();
        var unitallCount = $("#spunitAllCount").text();
        var ZTCJXS = $("#spChazhiXishu").text();
        //获取归档统计记录
        var arryData = new Array();
        $("#tbDataOne tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UnitAllCount: unitallCount,
                ZTCJXS: ZTCJXS,
                UserName: $.trim(rowdata.children().eq(0).text()),
                JBGZ: $.trim(rowdata.children().eq(1).text()),
                GZSJ: $.trim(rowdata.children().eq(2).text()),
                PJYGZ: $.trim(rowdata.children().eq(3).text()),
                YFJJ: $.trim(rowdata.children().eq(15).text()),
                ZPHP: $.trim(rowdata.children().eq(4).text()),
                PM: $.trim(rowdata.children().eq(5).text()),
                BMJLPJ: $.trim(rowdata.children().eq(6).text()),
                PM2: $.trim(rowdata.children().eq(7).text()),
                JQXS: $.trim(rowdata.children().eq(8).text()),
                BMNJJJS: $.trim(rowdata.children().eq(9).text()),
                XMJJJS: $.trim(rowdata.children().eq(10).text()),
                XMJJJLTZ: $.trim(rowdata.children().eq(11).text()),
                XMJJTZ: $.trim(rowdata.children().eq(12).text()),
                YXYG: $.trim(rowdata.children().eq(13).text()),
                HJ: $.trim(rowdata.children().eq(16).text()),
                HJYF: $.trim(rowdata.children().eq(14).text()),
                GZBS: $.trim(rowdata.children().eq(17).text()),
                SBN: $.trim(rowdata.children().eq(18).text()),
                SBN2: $.trim(rowdata.children().eq(19).text()),
                XBN: $.trim(rowdata.children().eq(20).text()),
                XBN2: $.trim(rowdata.children().eq(21).text())
            }
        });
        //离职
        var arryData2 = new Array();
        $("#tbDataOne2 tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData2[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UserName: $.trim(rowdata.children().eq(0).text()),
                RZSJ: $.trim(rowdata.children().eq(1).text()),
                LZSJ: $.trim(rowdata.children().eq(2).text()),
                JBGZ: $.trim(rowdata.children().eq(3).text()),
                YFJJ: $.trim(rowdata.children().eq(4).text()),
                PJGZ: $.trim(rowdata.children().eq(5).text()),
                BMJJTZ: $.trim(rowdata.children().eq(6).text()),
                GZBS: $.trim(rowdata.children().eq(7).text())
            }
        });

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var action = "archdetailbak";

        var jsonData = JSON.stringify(arryData);
        var jsonData2 = JSON.stringify(arryData2);

        var data = { "action": action, "isNew": 0, "data": jsonData, "data2": jsonData2 };

        $.post(url, data, function (result) {

            if (result) {
                if (result == 1) {
                    alert("部门内人员奖金归档成功!");

                    $("#btnBak").attr("class", "btn btn-sm default disabled").text("已归档");
                }
                else if (result == 2) {
                    alert('已归档，无需重复归档！');
                }
                else if (result == 3) {
                    alert("考核任务未归档，请先归档考核任务！");
                }

            }
            else {
                alert("错误！");
            }
        });
    }
    //方案归档
    var fanganBak = function () {
        if (!confirm("确定要发起部门内奖金计算归档吗？")) {
            return false;
        }
        if ($("#tbDataThree tbody tr").length == 0) {
            alert("没有要归档的部门信息！");
            return false;
        }

        var renwuid = $("#hidRenwuno").val();
        var renwuName = "";
        var unitid = $("#hidUnitID").val();
        var unitName = $("#spunitName3").text();
        var unitallCount = $("#spunitAllCount3").text();
        var ZTCJXS = $("#spChazhiXishu3").text();
        //获取归档统计记录
        var arryData = new Array();
        $("#tbDataThree tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UnitAllCount: unitallCount,
                ZTCJXS: ZTCJXS,
                UserName: $.trim(rowdata.children().eq(0).text()),
                JBGZ: $.trim(rowdata.children().eq(1).text()),
                GZSJ: $.trim(rowdata.children().eq(2).text()),
                PJYGZ: $.trim(rowdata.children().eq(3).text()),
                YFJJ: $.trim(rowdata.children().eq(14).text()),
                ZPHP: $.trim(rowdata.children().eq(4).text()),
                PM: $.trim(rowdata.children().eq(5).text()),
                BMJLPJ: $.trim(rowdata.children().eq(6).text()),
                PM2: $.trim(rowdata.children().eq(7).text()),
                JQXS: $.trim(rowdata.children().eq(8).text()),
                BMNJJJS: $.trim(rowdata.children().eq(9).text()),
                XMJJJS: $.trim(rowdata.children().eq(10).text()),
                XMJJJLTZ: 0,//rowdata.children().eq(13).text(), 
                XMJJTZ: $.trim(rowdata.children().eq(11).text()),
                YXYG: $.trim(rowdata.children().eq(12).text()),
                HJ: $.trim(rowdata.children().eq(15).text()),
                HJYF: $.trim(rowdata.children().eq(13).text()),
                GZBS: $.trim(rowdata.children().eq(16).text()),
                SBN: $.trim(rowdata.children().eq(17).text()),
                SBN2: $.trim(rowdata.children().eq(18).text()),
                XBN: $.trim(rowdata.children().eq(19).text()),
                XBN2: $.trim(rowdata.children().eq(20).text())
            }
        });
        //离职
        var arryData2 = new Array();
        $("#tbDataOne2 tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData2[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UserName: rowdata.children().eq(0).text(),
                RZSJ: rowdata.children().eq(1).text(),
                LZSJ: rowdata.children().eq(2).text(),
                JBGZ: rowdata.children().eq(3).text(),
                YFJJ: rowdata.children().eq(4).text(),
                PJGZ: rowdata.children().eq(5).text(),
                BMJJTZ: rowdata.children().eq(6).text(),
                GZBS: rowdata.children().eq(7).text()
            }
        });

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var action = "archdetailbak";

        var jsonData = JSON.stringify(arryData);
        var jsonData2 = JSON.stringify(arryData2);

        var data = { "action": action, "isNew": 0, "data": jsonData, "data2": jsonData2 };

        $.post(url, data, function (result) {

            if (result) {
                if (result == 1) {
                    alert("部门内人员奖金归档成功!");

                    $("#btnFanganBak").attr("class", "btn btn-sm default disabled").text("已归档");
                }
                else if (result == 2) {
                    alert('已归档，无需重复归档！');
                }
                else if (result == 3) {
                    alert("考核任务未归档，请先归档考核任务！");
                }

            }
            else {
                alert("错误！");
            }
        });
    }
    //特殊部门1
    var teshuoneBak = function () {
        if (!confirm("确定要发起部门内奖金计算归档吗？")) {
            return false;
        }
        if ($("#tbDataTwo tbody tr").length == 0) {
            alert("没有要归档的部门信息！");
            return false;
        }

        var renwuid = $("#hidRenwuno").val();
        var renwuName = "";
        var unitid = $("#hidUnitID").val();
        var unitName = $("#spunitName2").text();
        var unitallCount = 0;
        var ZTCJXS = 0;
        //获取归档统计记录
        var arryData = new Array();
        $("#tbDataTwo tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UnitAllCount: unitallCount,
                ZTCJXS: ZTCJXS,
                UserName: $.trim(rowdata.children().eq(0).text()),
                JBGZ: $.trim(rowdata.children().eq(1).text()),
                GZSJ: $.trim(rowdata.children().eq(2).text()),
                PJYGZ: $.trim(rowdata.children().eq(3).text()),
                YFJJ: $.trim(rowdata.children().eq(11).text()),
                ZPHP: $.trim(rowdata.children().eq(4).text()),
                PM: $.trim(rowdata.children().eq(5).text()),
                BMJLPJ: $.trim(rowdata.children().eq(6).text()),
                PM2: $.trim(rowdata.children().eq(7).text()),
                JQXS: 0,//rowdata.children().eq(9).text(),
                BMNJJJS: $.trim(rowdata.children().eq(8).text()),
                XMJJJS: 0,//rowdata.children().eq(11).text(),
                XMJJJLTZ: 0,//rowdata.children().eq(12).text(),
                XMJJTZ: 0,//rowdata.children().eq(13).text(),
                YXYG: $.trim(rowdata.children().eq(9).text()),
                HJ: $.trim(rowdata.children().eq(12).text()),
                HJYF: $.trim(rowdata.children().eq(10).text()),
                GZBS: $.trim(rowdata.children().eq(13).text()),
                SBN: $.trim(rowdata.children().eq(14).text()),
                SBN2: $.trim(rowdata.children().eq(15).text()),
                XBN: $.trim(rowdata.children().eq(16).text()),
                XBN2: $.trim(rowdata.children().eq(17).text())
            }
        });
        //离职
        var arryData2 = new Array();
        $("#tbDataOne2 tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData2[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UserName: $.trim(rowdata.children().eq(0).text()),
                RZSJ: $.trim(rowdata.children().eq(1).text()),
                LZSJ: $.trim(rowdata.children().eq(2).text()),
                JBGZ: $.trim(rowdata.children().eq(3).text()),
                YFJJ: $.trim(rowdata.children().eq(4).text()),
                PJGZ: $.trim(rowdata.children().eq(5).text()),
                BMJJTZ: $.trim(rowdata.children().eq(6).text()),
                GZBS: $.trim(rowdata.children().eq(7).text())
            }
        });

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var action = "archdetailbak";

        var jsonData = JSON.stringify(arryData);
        var jsonData2 = JSON.stringify(arryData2);

        var data = { "action": action, "isNew": 0, "data": jsonData, "data2": jsonData2 };

        $.post(url, data, function (result) {

            if (result) {
                if (result == 1) {
                    alert("部门内人员奖金归档成功!");

                    $("#btnTeshu1").attr("class", "btn btn-sm default disabled").text("已归档");
                }
                else if (result == 2) {
                    alert('已归档，无需重复归档！');
                }
                else if (result == 3) {
                    alert("考核任务未归档，请先归档考核任务！");
                }

            }
            else {
                alert("错误！");
            }
        });
    }
    var teshutwoBak = function () {
        if (!confirm("确定要发起部门内奖金计算归档吗？")) {
            return false;
        }
        if ($("#tbDataTwo2 tbody tr").length == 0) {
            alert("没有要归档的部门信息！");
            return false;
        }

        var renwuid = $("#hidRenwuno").val();
        var renwuName = "";
        var unitid = $("#hidUnitID").val();
        var unitName = $("#spunitName2").text();
        var unitallCount = 0;
        var ZTCJXS = 0;
        //获取归档统计记录
        var arryData = new Array();
        $("#tbDataTwo2 tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UnitAllCount: unitallCount,
                ZTCJXS: ZTCJXS,
                UserName: $.trim(rowdata.children().eq(0).text()),
                JBGZ: $.trim(rowdata.children().eq(1).text()),
                GZSJ: $.trim(rowdata.children().eq(2).text()),
                PJYGZ: $.trim(rowdata.children().eq(3).text()),
                YFJJ: $.trim(rowdata.children().eq(7).text()),
                ZPHP: 0,//rowdata.children().eq(5).text(),
                PM: 0,//rowdata.children().eq(6).text(),
                BMJLPJ: 0,//rowdata.children().eq(5).text(),
                PM2: 0,//rowdata.children().eq(8).text(),
                JQXS: 0,//rowdata.children().eq(9).text(),
                BMNJJJS: $.trim(rowdata.children().eq(4).text()),
                XMJJJS: 0,//rowdata.children().eq(11).text(),
                XMJJJLTZ: 0,// rowdata.children().eq(12).text(),
                XMJJTZ: 0,//rowdata.children().eq(13).text(),
                YXYG: $.trim(rowdata.children().eq(5).text()),
                HJ: $.trim(rowdata.children().eq(8).text()),
                HJYF: $.trim(rowdata.children().eq(6).text()),
                GZBS: $.trim(rowdata.children().eq(9).text()),
                SBN: $.trim(rowdata.children().eq(10).text()),
                SBN2: $.trim(rowdata.children().eq(11).text()),
                XBN: $.trim(rowdata.children().eq(12).text()),
                XBN2: $.trim(rowdata.children().eq(13).text())
            }
        });
        //离职
        var arryData2 = new Array();
        $("#tbDataOne2 tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData2[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitID: unitid,
                UnitName: unitName,
                UserName: $.trim(rowdata.children().eq(0).text()),
                RZSJ: $.trim(rowdata.children().eq(1).text()),
                LZSJ: $.trim(rowdata.children().eq(2).text()),
                JBGZ: $.trim(rowdata.children().eq(3).text()),
                YFJJ: $.trim(rowdata.children().eq(4).text()),
                PJGZ: $.trim(rowdata.children().eq(5).text()),
                BMJJTZ: $.trim(rowdata.children().eq(6).text()),
                GZBS: $.trim(rowdata.children().eq(7).text())
            }
        });

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var action = "archdetailbak";

        var jsonData = JSON.stringify(arryData);
        var jsonData2 = JSON.stringify(arryData2);

        var data = { "action": action, "isNew": 0, "data": jsonData, "data2": jsonData2 };

        $.post(url, data, function (result) {

            if (result) {
                if (result == 1) {
                    alert("部门内人员奖金归档成功!");

                    $("#btnTeshu2").attr("class", "btn btn-sm default disabled").text("已归档");
                }
                else if (result == 2) {
                    alert('已归档，无需重复归档！');
                }
                else if (result == 3) {
                    alert("考核任务未归档，请先归档考核任务！");
                }

            }
            else {
                alert("错误！");
            }
        });
    }
    //特殊部门2
    return {

        init: function () {
            pageLoad();
        
        }
    };
}();

