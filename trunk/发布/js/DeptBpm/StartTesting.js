﻿//开始考核
var StartTest = function () {

    var pageLoad = function () {
        //全选按钮
        $("#chkAll").click(function () {
            var obj = $(this);

            if (!obj.get(0).checked) {
                $(":checkbox").not(this).attr("checked", !obj.checked).parent().attr("class", "");
            }
            else {
                $(":checkbox").not(this).attr("checked", true).parent().attr("class", "checked");
            }
        });
        //考核任务选择
        $("#ctl00_ContentPlaceHolder1_selRenwu").change(function () {

            var obj = $(this);
            loadList(obj);
            //设置考核任务ID
            $("#hidRenwuId").val(obj.val());

        });

        // 注释 2016年10月14日
        //$(":checkbox[action='renwu']").click(function () {
        //    var obj = $(this);
        //    if (obj.is(":checked")) {
        //        $(":checkbox[action='renwu']").attr('checked', false);
        //        $(":checkbox[action='renwu']").parent().removeAttr("class");
        //        obj.attr('checked', true);

        //        loadList(obj);
        //        //设置考核任务ID
        //        $("#hidRenwuId").val(obj.val());
        //    }
        //});

        //设置考核部门
        $("#btnSetUnit").click(function () {

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);

                if (obj.find("td").eq(0).find(":checkbox").attr("checked") == "checked") {
                    //创建对象
                    unitArr[index] = {
                        "ID": "0",
                        "RenWuNo": $("#hidRenwuId").val(),
                        "UnitID": obj.find("td").eq(0).find(":checkbox").attr("uid"),
                        "UnitName": obj.find("td").eq(0).find(":checkbox").attr("uname"),
                        "IsIn": obj.find("td").eq(3).find(":selected").val(),
                        "Statu": "0"
                    };
                }
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "saveunit";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置考核单位成功！");
                        //重新加载
                        //loadList($(":checkbox[checked='checked']"));
                        loadList($("#ctl00_ContentPlaceHolder1_selRenwu"));
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });

        //初始加载
        //if ($("#selRenwu").val() != "0") {
        //    loadList($("#selRenwu").val());
        //}

        //初始加载
        if ($("#hidRenwuId").val() == "0") {
            loadList($("#hidProcessRenwuID"));
            $("#hidRenwuId").val($("#hidProcessRenwuID").val());
        } else {
            loadList($("#hidRenwuId"));
        }

        //提交考核
        $("a[action='sendmsg']").bind('click', function () {

            if (confirm("确定要提交部门考核？")) {
                var obj = $(this);
                //给需要考核的用户发消息
                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "sendusermsg";
                var unitid = $(this).attr("uid");
                var ukaoheid = $(this).attr("ukaoheid");
                var urenwuno = $(this).attr("urenwuno");
                var senduserid = $(this).attr("senduserid");
                var data = { "action": action, "uid": unitid, "ukaoheid": ukaoheid, "urenwuno": urenwuno, "senduserid": senduserid };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result != "error") {
                            //设置成功！
                            alert("发起部门内考核成功！");
                            //重新加载
                            //loadList($(":checkbox[checked='checked']"));
                            loadList($("#hidRenwuId"));
                        }
                    }
                    else {
                        //设置成功！
                        alert("设置失败请重试！");
                    }
                });
            }
        });
        //重新提交考核
        $("a[action='resendmsg']").bind('click', function () {

            if (confirm("确定要重新提交部门考核？")) {
                var obj = $(this);
                //给需要考核的用户发消息
                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "resendusermsg";
                var unitid = $(this).attr("uid");
                var ukaoheid = $(this).attr("ukaoheid");
                var urenwuno = $(this).attr("urenwuno");
                var senduserid = $(this).attr("senduserid");
                var data = { "action": action, "uid": unitid, "ukaoheid": ukaoheid, "urenwuno": urenwuno, "senduserid": senduserid };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result != "error") {
                            //设置成功！
                            alert("重新发起部门内考核成功！");
                            //重新加载
                            //loadList($(":checkbox[checked='checked']"));
                            loadList($("#hidRenwuId"));
                        }
                    }
                    else {
                        //设置成功！
                        alert("设置失败请重试！");
                    }
                });
            }
        });
        //不参加考核部门直接提交
        $("a[action='sendallot']").bind('click', function () {

            if (confirm("确定要提交部门内部奖金调整审批？")) {
                var obj = $(this);
                if ($(this).attr("ukaoheid") == "0") {
                    alert('请选择考核任务计划！');
                    return false;
                }
                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "sendmsgtomng";
                //发送消息
                var senduserid = $(this).attr("senduserid");
                //部门ID
                var unitid = $(this).attr("uid");
                //任务ID
                var renwuid = $(this).attr("urenwuno");
                //部门考核ID
                var kaoheunitid = $(this).attr("ukaoheid");

                var data = { "action": action, "uid": unitid, "urenwuno": renwuid, "ukaoheid": kaoheunitid, "senduserid": senduserid };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result == "1") {
                            //设置成功！
                            alert("部门经理调整消息发送成功！");
                            loadList($("#hidRenwuId"));
                        }
                        else {
                            //设置成功！
                            alert("发送失败，未设置部门经理！");
                            return false;
                        }
                    }
                    else {
                        //设置成功！
                        alert("设置失败请重试！");
                    }
                });
            }
        });

        //默认加载进行中的的任务
        if ($("#hidProcessRenwuID").val() != "0") {
            $("#ctl00_ContentPlaceHolder1_selRenwu").val($("#hidProcessRenwuID").val());
            loadList($("#hidProcessRenwuID"));
        }

        //一键提交 2017年11月16日
        $("a[action='sendonce']").bind('click', function () {

            if (confirm("确定要一键提交部门考核数据？")) {
                var obj = $(this);
                //给需要考核的用户发消息
                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "sendonce";
                var unitid = $(this).attr("uid");
                var ukaoheid = $(this).attr("ukaoheid");
                var urenwuno = $(this).attr("urenwuno");
                var senduserid = $(this).attr("senduserid");
                var data = { "action": action, "uid": unitid, "ukaoheid": ukaoheid, "urenwuno": urenwuno, "senduserid": senduserid };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result != "error") {

                            if (result == "0") {
                                alert("未设置考核人员！");
                            }
                            else if (result == "-1") {
                                alert("提交打分数据失败！");
                            }
                            else if (result == "-2") {
                                alert("更新打分数据失败！");
                            }
                            else {
                                //设置成功！
                                alert("一键提交考核数据成功！");
                            }
                            //重新加载
                            //loadList($(":checkbox[checked='checked']"));
                            loadList($("#hidRenwuId"));
                        }
                    }
                    else {
                        //设置成功！
                        alert("设置失败请重试！");
                    }
                });
            }
        });

    }
    //加载部门考核状态
    var loadList = function (obj) {
        var obj = obj;
        if (obj.val() != "0") {
            $("#btnSetUnit").attr("class", "btn btn-sm red");
        }
        else {
            $("#btnSetUnit").attr("class", "btn btn-sm default disabled");
            return false;
        }
        //获取设置的考核部门
        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var id = obj.val();
        var action = "getkhunit";
        var data = { "action": action, "id": id };

        $.get(url, data, function (result) {
            if (result) {
                //不参加考核部门ID
                //var unitArr = [230, 242, 243, 244];
                var unitArr = $("#hidSpecialUnitListStr").val();
                //未保存考核
                if (result == "1") {
                    //设置成功！
                    //alert("未设置考核单位！");
                    //location.href = "/DeptBpm/StartTesting.aspx";
                    $("#tbData tbody tr").each(function (index) {
                        var obj = $(this);
                        //打钩
                        obj.find("td").eq(0).find(":checkbox").attr("checked", "checked").parent().attr("class", "checked");
                        //部门id
                        var unitid = parseInt(obj.find("td").eq(0).find(":checkbox").attr("uid"));
                        //是否参与
                        if (unitArr.indexOf(unitid) > -1) {
                            obj.find("td").eq(3).find("select").val("1");
                        }
                        else {
                            obj.find("td").eq(3).find("select").val("0");
                        }
                        //设置设置用户ID
                        var href = "#";
                        obj.find("td").eq(4).find("a").attr("href", href).text("人员设置(0)").attr("class", "btn default btn-xs green-stripe disabled");
                        //设置提交按钮
                        if (!(unitArr.indexOf(unitid) > -1)) {
                            var href2 = "#";
                            obj.find("td").eq(6).find("a").attr("href", href2).attr("class", "btn default btn-xs red-stripe disabled");
                        }
                    });
                }
                else {
                    var jsonResult = eval("(" + result + ")");

                    for (i = 0; i < jsonResult.length; i++) {
                        var uid = jsonResult[i].UnitID;
                        $("#tbData tbody tr").each(function (index) {
                            var obj = $(this);
                            //当前部门
                            if (obj.find("td").eq(0).find(":checkbox").attr("uid") == uid) {
                                //打钩
                                obj.find("td").eq(0).find(":checkbox").attr("checked", "checked").parent().attr("class", "checked");
                                //是否参与
                                if (jsonResult[i].IsIn == 1) {
                                    //改变78任务时特务三部的部门类型
                                    if (jsonResult[i].RenWuNo == '78' && uid == 281) {
                                        uid = 242;
                                    }

                                    //一般部门
                                    if (!(unitArr.indexOf(uid) > -1)) {
                                        obj.find("td").eq(3).find("select").val(jsonResult[i].IsIn);
                                        obj.find("td").eq(4).find("a").attr("class", "btn default btn-xs green-stripe disabled");
                                        obj.find("td").eq(6).find("a").attr("class", "btn default btn-xs green-stripe disabled");

                                        obj.find("td").eq(7).find("a").hide();
                                    }
                                    else//行政部门
                                    {
                                        //设置设置用户ID
                                        var href = "/DeptBpm/SetUnitUsers.aspx?uid=" + jsonResult[i].UnitID + "&renwuno=" + jsonResult[i].RenWuNo + "&id=" + jsonResult[i].ID + "";
                                        obj.find("td").eq(4).find("a").attr("href", href).text("人员设置(" + jsonResult[i].RenYuanCount + ")").attr("class", "btn default btn-xs green-stripe");

                                        obj.find("td").eq(3).find("select").val(jsonResult[i].IsIn);
                                        obj.find("td").eq(6).find("a").attr("href", "#").attr("class", "btn default btn-xs blue-stripe");

                                        obj.find("td").eq(7).find("a").hide();
                                        //设置KaoheUnitId
                                        obj.find("td").eq(6).find("a").attr("ukaoheid", jsonResult[i].ID);
                                        //设置RenWuNo
                                        obj.find("td").eq(6).find("a").attr("urenwuno", jsonResult[i].RenWuNo);

                                        //设置KaoheUnitId
                                        obj.find("td").eq(7).find("a").attr("ukaoheid", jsonResult[i].ID);
                                        //设置RenWuNo
                                        obj.find("td").eq(7).find("a").attr("urenwuno", jsonResult[i].RenWuNo);

                                        //不参加部门考核部门
                                        if (jsonResult[i].Statu != 0) {
                                            obj.find("td").eq(5).children("span").attr("class", "badge badge-success").text("已提交考核");
                                            obj.find("td").eq(6).find("a").attr("href", "#").attr("class", "btn default btn-xs blue-stripe disabled");

                                            obj.find("td").eq(7).find("a").hide();
                                        }
                                    }
                                }
                                else {
                                    //设置设置用户ID
                                    var href = "/DeptBpm/SetUnitUsers.aspx?uid=" + jsonResult[i].UnitID + "&renwuno=" + jsonResult[i].RenWuNo + "&id=" + jsonResult[i].ID + "";
                                    obj.find("td").eq(4).find("a").attr("href", href).text("人员设置(" + jsonResult[i].RenYuanCount + ")").attr("class", "btn default btn-xs green-stripe");
                                    //设置提交按钮
                                    var href2 = "/DeptBpm/ShowUserPress.aspx?uid=" + jsonResult[i].UnitID + "&renwuno=" + jsonResult[i].RenWuNo + "&id=" + jsonResult[i].ID + "";
                                    //没有设置人不允许提交
                                    if (jsonResult[i].RenYuanCount > 0) {
                                        //判断提交考核状态
                                        //0为未提交 1为已提交
                                        if (jsonResult[i].Statu != 0) {
                                            if (!(unitArr.indexOf(uid) > -1)) {
                                                obj.find("td").eq(6).find("a").attr("href", href2).text("查看用户打分进度").attr("class", "btn default btn-xs red-stripe").removeAttr("action").unbind("click").attr("target", "_blank");
                                                obj.find("td").eq(5).children("span").attr("class", "badge badge-success").text("已提交考核");
                                                //显示重新提交
                                                obj.find("td").eq(7).find("a").attr("class", "btn default btn-xs yellow-stripe ").show();

                                                //设置KaoheUnitId
                                                obj.find("td").eq(7).find("a").attr("ukaoheid", jsonResult[i].ID);
                                                //设置RenWuNo
                                                obj.find("td").eq(7).find("a").attr("urenwuno", jsonResult[i].RenWuNo);
                                            }
                                            else {
                                                obj.find("td").eq(3).find("select").val(jsonResult[i].IsIn);
                                                obj.find("td").eq(5).children("span").attr("class", "badge badge-success").text("已提交考核");
                                                obj.find("td").eq(6).find("a").attr("href", "#").attr("class", "btn default btn-xs blue-stripe");

                                                obj.find("td").eq(7).find("a").hide();
                                                //设置KaoheUnitId
                                                obj.find("td").eq(6).find("a").attr("ukaoheid", jsonResult[i].ID);
                                                //设置RenWuNo
                                                obj.find("td").eq(6).find("a").attr("urenwuno", jsonResult[i].RenWuNo);

                                                //设置KaoheUnitId
                                                obj.find("td").eq(7).find("a").attr("ukaoheid", jsonResult[i].ID);
                                                //设置RenWuNo
                                                obj.find("td").eq(7).find("a").attr("urenwuno", jsonResult[i].RenWuNo);
                                            }
                                        }
                                        else {
                                            if (!(unitArr.indexOf(uid) > -1)) {
                                                obj.find("td").eq(6).find("a").attr("href", "#").attr("class", "btn default btn-xs red-stripe");

                                                obj.find("td").eq(7).find("a").hide();
                                            }
                                            else {
                                                obj.find("td").eq(6).find("a").attr("href", "#").attr("class", "btn default btn-xs blue-stripe");

                                                obj.find("td").eq(7).find("a").hide();
                                            }
                                            //设置KaoheUnitId
                                            obj.find("td").eq(6).find("a").attr("ukaoheid", jsonResult[i].ID);
                                            //设置RenWuNo
                                            obj.find("td").eq(6).find("a").attr("urenwuno", jsonResult[i].RenWuNo);

                                            //设置KaoheUnitId
                                            obj.find("td").eq(7).find("a").attr("ukaoheid", jsonResult[i].ID);
                                            //设置RenWuNo
                                            obj.find("td").eq(7).find("a").attr("urenwuno", jsonResult[i].RenWuNo);
                                        }
                                    }
                                    else {
                                        href2 = "#";
                                        obj.find("td").eq(6).find("a").attr("href", href2).attr("class", "btn default btn-xs green-stripe disabled");

                                        obj.find("td").eq(7).find("a").hide();
                                    }
                                }
                                return false;
                            }
                        });
                    }
                }


            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }

    

    return {
        init: function () {
            pageLoad();
        }
    }
}();