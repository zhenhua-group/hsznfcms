﻿
var DeptBpm = function () {

    var pageInit = function () {
        //添加
        $("#btnSave").click(function () {
            if (isValide()) {
                //数据实体
                var entityData = {
                    ID: $("#hidRecordID").val() == "0" ? "0" : $("#hidRecordID").val(),
                    RenWuNo: $("#txtRenWuNo").val(),
                    StartDate: $("#txtStartDate").val(),
                    EndDate: $("#txtEndDate").val(),
                    Sub: $("#txtSub").val(),
                    ProjStatus: $("#projStatus").val(),
                    DeptStatus: $("#deptStatus").val(),
                    InsertDate: $("#hidCurDate").val(),
                    InsertUser: $("#hidUserSysNo").val()
                };
                var jsonData = JSON.stringify(entityData);

                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = $("#hidIsNew").val() == "0" ? "add" : "edit";

                var data = { "action": action, "isNew": $("#hidIsNew").val(), "data": jsonData };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result == 3) {
                            alert("已存在此名称任务，不允许重复添加！");
                            return false;
                        }

                        if ($("#hidIsNew").val() == "0") {
                            alert("任务添加成功！");
                        }
                        else {
                            alert("任务修改成功！");
                        }

                        location.href = "/DeptBpm/DeptBpmList.aspx";
                    }
                    else {
                        alert("错误！");
                    }


                });
                //隐藏
                $(this).hide();
            }

        }).hide();

        $("#btnIsNew").click(function () {
            $("#txtRenWuNo").val("");
            $("#txtStartDate").val("");
            $("#txtEndDate").val("");
            $("#txtSub").val("");
            $("#projStatus").val("进行中");
            $("#deptStatus").val("进行中");

            $("#btnSave").show();
            $("#hidIsNew").val("0");
            $("#tbRenwu").show();
            $("#tbJiangJin").hide();
        });
        //修改
        $("table a[caction='edit']").click(function () {
            var obj = $(this);
            $("#txtRenWuNo").val(obj.parent().parent().find("td").eq(0).text());
            $("#txtStartDate").val(obj.attr("cstart"));
            $("#txtEndDate").val(obj.attr("cend"));
            $("#txtSub").val(obj.attr("csub"));
            $("#projStatus").val(obj.parent().parent().find("td").eq(3).text());
            $("#deptStatus").val(obj.parent().parent().find("td").eq(4).text());
            $("#hidRecordID").val(obj.attr("cid"));
            //修改
            $("#btnSave").show();
            $("#hidIsNew").val("1");

            $("#tbRenwu").show();
            $("#tbJiangJin").hide();
        });
        //删除
        $("table a[caction='del']").click(function () {

            $("#hidIsNew").val("3");
            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "del";
            var id = $(this).attr("cid");
            var data = { "action": action, "isNew": $("#hidIsNew").val(), "data": "", "id": id };

            if (confirm("确定要删除此任务？")) {
                $.post(url, data, function (result) {

                    if (result) {
                        if (result == 1) {
                            alert("任务删除成功");
                        }

                        location.href = "/DeptBpm/DeptBpmList.aspx";
                    }
                    else {
                        alert("错误！");
                    }
                });
            }
        });

        //设定奖金数
        //$("table a[caction='setcount']").click(function () {
        //    var obj = $(this);
        //    //保存记录id
        //    $("#hidRecordID").val(obj.attr("cid"));

        //    var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        //    var action = "setcount";
        //    var id = $(this).attr("cid");
        //    var data = { "action": action, "isNew": $("#hidIsNew").val(), "data": "", "id": id };

        //    $.get(url, data, function (result) {

        //        if (result) {
        //            if (parseFloat(result).toFixed(4) > 0) {
        //                $("#btnSaveJJ").hide();
        //                $("#btnSaveJJedit").show();
        //                $("#txtAllCount").val(parseFloat(result).toFixed(2)).css({ border: "none" }).attr("disabled", true);
        //            }
        //            else {
        //                $("#txtAllCount").val("0");
        //                $("#txtAllCount").val(parseFloat(result).toFixed(2)).css({ border: "2px inset" }).attr("disabled", false);

        //            }
        //        }
        //        else {
        //            $("#txtAllCount").val(0);
        //        }
        //    });

        //    $("#tbRenwu").hide();
        //    $("#tbJiangJin").show();
        //    $("#btnSaveJJ").show();
        //    $("#btnSaveJJedit").hide();
        //});

        //按钮保存当前设置金额
        $("#btnSaveLuru").click(function () {
            var obj = $("#txtLuru");
            //任务id
            var renwuid = obj.attr("cid");
            var oldval = obj.next(":hidden").val();
            var curval = obj.val();

            if (oldval != curval) {
                if (confirm("初始录入已经改变是否要保存当前值？")) {
                    var recordid = renwuid;
           
                    var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                    if ($.trim(curval) == "") {
                        alert("奖金输入不能为空！");
                        return false;
                    }
                    else {
                        if (!reg_math.test(curval)) {
                            alert("请输入数字！");
                            $("#txtAllCount").val(oldval);
                            return false;
                        }
                    }

                    var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                    var action = "savejj";
                 var id = recordid;
               
                    var jjcount = curval;
                    var data = { "action": action, "isNew": $("#hidIsNew").val(), "jjcount": jjcount, "id": id };

                    $.get(url, data, function (result) {

                        if (result) {
                            if (result == "1") {
                                //设置成功！
                                alert("设置总奖金数成功！");
                            }
                        }
                        else {
                            //设置成功！
                            alert("设置失败请重试！");
                            return false;
                        }
                    });
                }
            }
            else {
                alert('金额未改变！');
            }
        });



        //确认提交
        $("#btnSaveLuru2").click(function () {
            var obj = $("#txtLuru");
            //任务id
            var renwuid = obj.attr("cid");
            var oldval = obj.next(":hidden").val();
            var curval = obj.val();       
                if (confirm("确认要提交？")) {
                    var recordid = renwuid;

                    var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                    if ($.trim(curval) == "") {
                        alert("奖金输入不能为空！");
                        return false;
                    }
                    else {
                        if (!reg_math.test(curval)) {
                            alert("请输入数字！");
                            $("#txtAllCount").val(oldval);
                            return false;
                        }
                    }

                    var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                    var action = "savejj";
                    var id = recordid;
                    var jjcount = curval;
                    var data = { "action": action, "isNew": $("#hidIsNew").val(), "jjcount": jjcount, "id": id };

                    $.get(url, data, function (result) {

                        if (result) {
                            if (result == "1") {
                                //设置成功！
                                alert("设置总奖金数成功！");
                                $("#btnSaveLuru2").hide()
                            }
                        }
                        else {
                            //设置成功！
                            alert("设置失败请重试！");
                            return false;
                        }
                    });
                }
         
           
        });






       
        //文本框编辑
        //$("table input[cid]").blur(function () {


        //});

        //保存奖金数
        $("#btnSaveJJ").click(function () {

            var recordid = $("#hidRecordID").val();

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if ($.trim($("#txtAllCount").val()) == "") {
                alert("奖金输入不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($("#txtAllCount").val())) {
                    alert("请输入数字！");
                    $("#txtAllCount").val("");
                    return false;
                }
            }

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "savejj";
            var id = $("#hidRecordID").val();
            var jjcount = $("#txtAllCount").val();
            var data = { "action": action, "isNew": $("#hidIsNew").val(), "jjcount": jjcount, "id": id };

            $.get(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置总奖金数成功！");
                        $("#btnSaveJJ").hide();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //修改
        $("#btnSaveJJedit").click(function () {
            $("#txtAllCount").css({ border: "2px inset" }).attr("disabled", false);
            $(this).hide();
            $("#btnSaveJJ").show();
        });
        //发起归档申请
        $("table a[caction='bak']").click(function () {

            if (confirm("确定要归档考核数据？")) {

                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "bak";
                var id = $(this).attr("cid");

                //获取归档统计记录
                var rowdata = $("table tr[data-rid=" + id + "]", "#divKaoHeSum");

                var entityData = {
                    ID: 0,
                    RenwuID: id,
                    RenwuName: rowdata.children().eq(0).text(),
                    ShijiCount: rowdata.children().eq(1).text(),
                    InitCount: rowdata.children().eq(2).text(),
                    JishuCount: rowdata.children().eq(3).text(),
                    JiangjinCount: rowdata.children().eq(4).text()
                };

                var jsonData = JSON.stringify(entityData);
                var data = { "action": action, "isNew": 0, "data": jsonData, "id": id };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result == 1) {
                            alert("任务归档成功");
                        }
                        else if (result == 2) {
                            alert('任务已归档，无需重复归档');
                        }

                        location.href = "/DeptBpm/DeptBpmList.aspx";
                    }
                    else {
                        alert("错误！");
                    }
                });
            }

        });

    }

    var isValide = function () {
        var msg = "";
        //任务编号
        if ($.trim($("#txtRenWuNo").val()) == "") {
            msg += "任务编号不能为空\n";
        }
        else {
            if ($.trim($("#txtRenWuNo").val()).length > 10) {
                msg += "任务编号输入过长\n";
            }
        }
        //开始时间
        if ($.trim($("#txtStartDate").val()) == "") {
            msg += "起始时间不能为空\n";
        }
        //结束时间
        if ($.trim($("#txtEndDate").val()) == "") {
            msg += "截止时间不能为空\n";
        }
        //备注信息
        if ($.trim($("#txtSub").val()) != "") {
            if ($("#txtSub").val().length > 200) {
                msg += "备注输入过长！\n";
            }
        }

        if (msg != "") {
            alert(msg);
            return false;
        }
        return true;
    }

    return {
        Init: function () {
            pageInit();
        }
    }
}();

function DeptBpmListBtnSaveLuruOnClick(renwuid) {
    //var renwuid = $(this).attr("cid");
    var obj = $("#txtLuru_" + renwuid);
    //任务id
    // var renwuid = obj.attr("cid");
    var oldval = obj.next(":hidden").val();
    var curval = obj.val();

    if (oldval != curval) {
        if (confirm("初始录入已经改变是否要保存当前值？")) {
            var recordid = renwuid;

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if ($.trim(curval) == "") {
                alert("奖金输入不能为空！");
                return false;
            }
            else {
                if (!reg_math.test(curval)) {
                    alert("请输入数字！");
                    $("#txtAllCount").val(oldval);
                    return false;
                }
            }

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "savejj";
            var id = recordid;

            var jjcount = curval;
            var data = { "action": action, "isNew": $("#hidIsNew").val(), "jjcount": jjcount, "id": id };

            $.get(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置总奖金数成功！");
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                    return false;
                }
            });
        }
    }
    else {
        alert('金额未改变！');
    }
}


//确认提交
function  DeptBpmListBtnSaveLuruOnClicktijiao(renwuid) {
    //var obj = $("#txtLuru");
    var obj = $("#txtLuru_" + renwuid);
    //任务id
  //  var renwuid = obj.attr("cid");
    var oldval = obj.next(":hidden").val();
    var curval = obj.val();
    
        if (confirm("确定要提交？")) {
            var recordid = renwuid;
            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "savejj";
            var id = recordid;

            var jjcount = curval;
            var data = { "action": action, "isNew": $("#hidIsNew").val(), "jjcount": jjcount, "id": id };

            $.get(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置总奖金数成功！");
                  }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                    return false;
                }
            });
        }
    

};