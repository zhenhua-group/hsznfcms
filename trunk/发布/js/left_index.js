﻿$(document).ready(function() {
    //院项目统计
    $("#link_proj").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../HomePage/hm_projCount.aspx");
    });
    //院图纸统计
    $("#link_package").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../HomePage/hm_projPackCount.aspx");
    });
    //院基本情况
    $("#link_info").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../HomePage/hm_Info.aspx");
    });
    //院系结构
    $("#link_const").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../HomePage/hm_Const.aspx");
    });
    //院资质
    $("#link_qua").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../HomePage/hm_qualification.aspx");
    });
});
//隐藏缓冲
function showBody() {
    window.parent.hideWait();
    $("body").show();
}
window.onload = showBody;