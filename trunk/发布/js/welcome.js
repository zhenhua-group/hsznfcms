﻿$(document).ready(function () {

    //综合统计
    var data = "flag=getall";
    $.ajax({
        type: "GET",
        url: "../HttpHandler/ProjectAnalysis.ashx",
        data: data,
        success: function (msg) {
            var message = msg.split("$");
            $("#item_all").html(message[0]);
            $("#item_zhi").html(message[1]);
            $("#item_bumen").html(message[2]);
            $("#item_per").html(message[3]);
        }
    });

    //加载系统公告页面
    //ajax demo:
    // general settings
    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
      '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
        '<div class="progress progress-striped active">' +
          '<div class="progress-bar" style="width: 100%;"></div>' +
        '</div>' +
      '</div>';

    //$.fn.modalmanager.defaults.resize = false;

    var $model = $("#ajax-modal");

    $("a[msgtitle]").live('click', function () {

        var msgsysno = $.trim($(this).attr("msgid"));

        $('body').modalmanager('loading');
        var url = "ShowContent.aspx?type=0&sysno=" + msgsysno+"&n="+Math.random();
        $model.load(url, "", function () {
            $model.modal();
        });
        //  }, 1000);

    });




});