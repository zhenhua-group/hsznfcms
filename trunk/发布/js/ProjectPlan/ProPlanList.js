﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var startAppControl; //发起审批对象

//项目策划列表
$(document).ready(function () {

    CommonControl.SetFormWidth();
    //Tab页
    $('#tabs').tabs({ cookie: { expires: 30} });

    //隔行变色
    $("#gvproPlan tr:even").css({ background: "#f0f0f0" });

    //隔行变色
    $("#gvAuditstatus tr:even").css({ background: "#f0f0f0" });

    //审批进度
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });
    ChangedBackgroundColor();
    $(".cls_flag").each(function () {
        if ($(this).attr("id") == projsysno) {
            $(this).parent().parent().css({ "background-color": "#999999" });
        }
        //标示是否返回类型
        var obj = $(this).parent().parent().find("td .cls_addplan");
        obj.attr("href", obj.attr("href") + "&flag=" + $("#hid_flag").val());
    });
    //编辑
    $(".cls_flag2").each(function () {
        if ($(this).attr("id") == projsysno) {
            $(this).parent().parent().css({ "background-color": "#999999" });
        }
        //标示是否返回类型
        var obj = $(this).parent().parent().find("td .cls_editplan");
        obj.attr("href", obj.attr("href") + "&flag=" + $("#hid_flag").val());
    });

    //添加策划
    $(".cls_addplan").live("click", function () {
        //是否当前用户为项目总负责
        // var curusersysno = $(this).next(":hidden").val();
        var curusersysno = $(this).parent().parent().find("td").eq(5).text();
        if (usersysno != curusersysno) {
            $(this).attr("href", "###");
            alert("您不是本项目的执行设总，不能添加策划信息！");
            return false;
        }
    });
    //编辑策划
    $(".cls_editplan").live("click", function () {
        //是否当前用户为项目总负责
        //var curusersysno = $(this).nextAll(":hidden").val();
        var curusersysno = $(this).parent().parent().find("td").eq(5).text();
        if (usersysno != curusersysno) {
            $(this).attr("href", "###");
            alert("您不是本项目的执行设总，不能编辑策划信息！");
            return false;
        }
    });
    //提交审核按钮点击事件
    $("a[id=applyAuditImgButton]").live("click", function () {
        //实体数据
        var objData = {
            ProjectSysNo: $(this).attr("projectsysno"),
            CoperationName: $(this).attr("coperationname")
        };
        jsonDataEntity = Global.toJSON(objData);
        startAppControl = $(this);
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });

    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('0', '1', jsonDataEntity);
            },
            "关闭": function () {
                messageDialog.hide();
            }
        }
    });
    sendMessageClass = new MessageCommon(messageDialog);

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //提交地址
    var result = TG.Web.ProjectPlan.ProPlanList.SubmitApply(jsonData, flag);
    //提交数据
    if (result.value == "0") {
        alert("发起项目策划评审失败，请联系系统管理员！");
    }
    else if (result.value == "1") {
        alert("项目策划已提交评审，不能重复提交！");
    }
    else {
        renderUserOrSendMsg(flag, result.value);
    }
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag == "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        //提示消息
        alert("发起项目策划评审已成功！\n消息已成功发送到评审人等待审批！");
        //改变流程提示状态
        CommonControl.SetApplyStatus(startAppControl);
    } else {
        alert("消息发送失败！");
    }
}