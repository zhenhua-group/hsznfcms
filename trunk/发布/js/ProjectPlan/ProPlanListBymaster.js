﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var startAppControl; //发起审批对象

//项目策划列表
$(document).ready(function () {


    AllotList();
    AllotList2();
    //提交审核按钮点击事件
    $("a[id=applyAuditImgButton]").live("click", function () {
        messageDialog = $("#auditShow").messageDialog;
        sendMessageClass = new MessageCommon(messageDialog);
        //实体数据
        var objData = {
            ProjectSysNo: $(this).attr("projectsysno"),
            CoperationName: $(this).attr("coperationname")
        };
        jsonDataEntity = Global.toJSON(objData);
        startAppControl = $(this);
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });
    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('0', '1', jsonDataEntity);
        }
    });
    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#ctl00_ContentPlaceHolder1_previewPower").val();
    paramEntity.userSysNum = $("#ctl00_ContentPlaceHolder1_userSysNum").val();
    paramEntity.userUnitNum = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    paramEntity.unitID = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    paramEntity.currYear = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();
});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //提交地址
    var result = TG.Web.ProjectPlan.ProPlanListBymaster.SubmitApply(jsonData, flag);
    //提交数据
    if (result.value == "0") {
        alert("发起项目策划评审失败，请联系系统管理员！");
    }
    else if (result.value == "1") {
        alert("项目策划已提交评审，不能重复提交！");
    }
    else {
        renderUserOrSendMsg(flag, result.value);
    }
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag == "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        //提示消息
        alert("发起项目策划评审已成功！\n消息已成功发送到评审人等待审批！");
        //改变流程提示状态
        CommonControl.SetApplyStatus(startAppControl);
    } else {
        alert("消息发送失败！");
    }
}