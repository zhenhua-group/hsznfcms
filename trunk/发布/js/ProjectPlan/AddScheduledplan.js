﻿function AddScheduledplan(container) {
    this.Dom = {};
    this.Container = container;
    var Instance = this;
    this.IsEdit = false;

    this.BindData = function (dataObj) {
        $("#designLevel").val(dataObj.DesignLevel);
        $("#txtStartTime").val(dataObj.StartDate);
        $("#txtFinishtime").val(dataObj.EndDate);

        $("#txtjz").val(dataObj.BuildingCount);
        $("#txtjg").val(dataObj.StructCount);
        $("#txtdq").val(dataObj.ElectricCount);
        $("#txtnt").val(dataObj.HVACCount);
        $("#txtgps").val(dataObj.DrainCount);
        $("#txtsz").val(dataObj.MunicipalAdministrationCount);
    }

    this.SaveScheduledplan = function (callBack, dialogWindow) {
        var dataObj =
        {
            "designLevel": $("#designLevel").val(),
            "txtchildName": $("#txtchildName").val(),
            "txtStartTime": $("#txtStartTime").val(),
            "txtFinishtime": $("#txtFinishtime").val(),
            "txtds": $("#txtds").val(),
            "txtcs": $("#txtcs").val(),
            "txtArea": $("#txtArea").val(),
            "txtjz": $("#txtjz").val(),
            "txtjg": $("#txtjg").val(),
            "txtdq": $("#txtdq").val(),
            "txtnt": $("#txtnt").val(),
            "txtgps": $("#txtgps").val(),
            "txtsz": $("#txtsz").val()
        };

        //验证通过的场合
        if (Instance.Validation() == false) {

            return false;
        } else {
            var sum = parseInt(dataObj.txtjz, 10) + parseInt(dataObj.txtjg, 10) + parseInt(dataObj.txtdq, 10) + parseInt(dataObj.txtnt, 10) + parseInt(dataObj.txtgps, 10) + parseInt(dataObj.txtsz, 10);
            dataObj.PaperString = "{\"BuildingCount\" : " + dataObj.txtjz + " ,\"StructCount\" : " + dataObj.txtjg + " ,\"ElectricCount\" : " + dataObj.txtdq + " ,\"HVACCount\" : " + dataObj.txtnt + ",\"DrainCount\" : " + dataObj.txtgps + ",\"MunicipalAdministrationCount\":" + dataObj.txtsz + "}";
            dataObj.SumPaper = sum;
            callBack(dataObj);
//            $(dialogWindow).dialog("close");
        }
    }

    this.Clear = function () {
        $(":text", container).val("");
        $(":text[name=number]", container).val("0")
    }

    $(":text[name=number]").blur(function () {
        if ($.trim(this.value) == "") {
            this.value = "0";
        }
    });

    //验证方法
    this.Validation = function () {
        var numberTextBox = $(":text[name=number]", container);
        var flag = true;
        for (var i = 0; i < numberTextBox.length; i++) {
            if (/^\d+$/.test(numberTextBox[i].value) == false) {
                flag = false;
                alert("请确认填写内容为正整数！");
                $(numberTextBox[i]).focus();
                break;
            }
        }

        if ($("#txtStartTime").val().length == 0) {
            alert("请选择开始时间！");
            flag = false;
        }
        if ($("#txtFinishtime").val().length == 0) {
            alert("请选择结束时间！");
            flag = false;
        }
        if ($("#txtStartTime").val() >= $("#txtFinishtime").val()) {
            alert("开始时间大于结束时间！");
            flag = false;
        }
        return flag;
    }

}