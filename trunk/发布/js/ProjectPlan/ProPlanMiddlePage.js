﻿/// <reference path="../../Coperation/cpr_CoperationAuditListBymaster.aspx" />
//接受消息
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
$(function () {
    //发起审批
    $("#btn_startApply").live("click", function () {
      
        messageDialog = $("#auditShow");
        sendMessageClass = new MessageCommon(messageDialog);      
        var objData =
         {
             ProjectSysNo: $(this).attr("proid"),
             CoperationName: $(this).attr("cprname")
         };
        var query = Global.toJSON(objData);
        jsonDataEntity = query;
        startAppControl = $(this);
        //发起审批
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });
    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('0', '1', jsonDataEntity);
        }
    });
})
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        //提示消息
        alert("发起项目策划评审已成功！\n消息已成功发送到评审人等待审批！");
        window.loction.href = "ProPlanListBymaster.aspx";
       
    } else {
        alert("消息发送失败！");
    }
}

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //提交地址
    var result = TG.Web.ProjectPlan.ProPlanMiddlePage.SubmitApply(jsonData, flag);
    //提交数据
    if (result.value == "0") {
        alert("发起项目策划评审失败，请联系系统管理员！");
    }
    else if (result.value == "1") {
        alert("项目策划已提交评审，不能重复提交！");
    }
    else {
        renderUserOrSendMsg(flag, result.value);
    }
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag == "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

