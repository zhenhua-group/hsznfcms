﻿var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;
function ProjectPlanAuditBymaster(container) {

    this.Container = container;
    this.Dom = {};
    this.BackgroundInvoke = TG.Web.ProjectPlan.ProPlanAuditBymaster;
    this.JsonDataEntity = "";
    this.messageDialog = $("#auditShow").messageDialog;
    this.sendMessageClass = new MessageCommon(this.messageDialog);
    //    this.messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                Instance.getUserAndUpdateAudit('1', '1', Instance.JsonDataEntity);
    //            },
    //            "关闭": function () {
    //                Instance.messageDialog.hide();
    //            }
    //        }
    //    });
    this.Dom.BtnSend = $("#btn_Send", container);
    this.Dom.BtnSend.live("click", function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            Instance.getUserAndUpdateAudit('1', '1', Instance.JsonDataEntity);
        }
        $("#btn_CanceAudit").trigger('click');
    });
    //    this.sendMessageClass = new MessageCommon(this.messageDialog);
    var Instance = this;
    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();
    this.ProjectSysNo = $("#ProjectSysNo", container).val();
    this.CoperationName = $("#HiddenCoperationName", container).val();
    this.ProjectPlanAuditSysNo = $("#HiddenProjectPlanAuditSysNo", container).val();
    this.Status = $("#HiddenAuditStatus", container).val();

    //设置文本框样式
    CommonControl.SetTextBoxStyle();
    //人员表格样式
    CommonControl.SetTableStyle("chooseUserResultTable", "")
    CommonControl.SetTableStyle("AddSubItemResultTable", "");
    CommonControl.SetTableStyle("infomationTable", "need");

    this.RedirectToList = function () {
        //window.history.back();
        //window.location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=A&pageIndex=" + index;
        window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
    }

    this.BindDataForControl = function () {
        //取得管理级别情报
        $(":radio[name=projectLevel][value=" + $("#ProjectLevelHidden", container).val() + "]").attr("checked", true);

        //取得项目状态情报
        var projectStatus = $("#ProjectStatusHidden", container).val();
        $.each(projectStatus.split(","), function (index, item) {
            $(":checkbox[name=DesignLevelCheckBox][value=" + item + "]", container).attr("checked", true);
        });
    }
    Instance.BindDataForControl();

    //审核通过
    this.Dom.AgreeButton = $("#AgreeButton", container);
//    this.Dom.AgreeButton.live("focus", function () {
//        var dataObj = {
//            "ProjectPlanAuditSysNo": Instance.ProjectPlanAuditSysNo,
//            "Suggestion": $("#suggestionTextArea", container).val(),
//            "Status": $("#HiddenAuditStatus", container).val(),
//            "CoperationName": $("#HiddenCoperationName", container).val()
//        };
//        if (Instance.Vilidation(dataObj) != false && Instance.Status != "D") {
//            $(this).attr({ "data-toggle": "modal", "href": "#AuditUserDiv" });
//        }
//    })
    this.Dom.AgreeButton.live("click", function () {
        var dataObj = {
            "ProjectPlanAuditSysNo": Instance.ProjectPlanAuditSysNo,
            "Suggestion": $("#suggestionTextArea", container).val(),
            "Status": $("#HiddenAuditStatus", container).val(),
            "CoperationName": $("#HiddenCoperationName", container).val()
        };

        if (Instance.Vilidation(dataObj) == false) {
            alert("建议不能为空！");
            return false;

        }
        //按钮变为不可用
        // $(this).attr("disabled", "disabled");

        var jsonData = Global.toJSON(dataObj);
        Instance.JsonDataEntity = jsonData;
        //开始审批
        if (Instance.Status == "D") {
            Instance.getUserAndUpdateAudit('1', '1', Instance.JsonDataEntity);
        }
        else {
            Instance.getUserAndUpdateAudit('1', '0', Instance.JsonDataEntity);
        }
    });



    //审核不通过
    this.Dom.DisAgreeButton = $("#DisAgreeButton", container);
    this.Dom.DisAgreeButton.live("click", function () {


        var dataObj = {
            "ProjectPlanAuditSysNo": Instance.ProjectPlanAuditSysNo,
            "Suggestion": $("#suggestionTextArea", container).val(),
            "Status": $("#HiddenAuditStatus", container).val(),
            "CoperationName": $("#HiddenCoperationName", container).val()
        };

        if (Instance.Vilidation(dataObj) == false) {
            alert("建议不能为空！");
            return false;
        }

        //按钮变为不可用
        $(this).attr("disabled", "disabled");

        var result = Instance.BackgroundInvoke.DisAgree(Global.toJSON(dataObj));

        if (parseInt(result.value, 10) > 0) {
            //更新待办状态
            var msg = new MessageCommProjPlan($("#msgno").val());
            msg.ReadMsg();
            //提示
            alert("评审不通过，消息已发送给策划申请人！");
            Instance.RedirectToList();
        } else {
            alert("评审不通过失败！");
        }
    });

    //验证方法
    this.Vilidation = function (auditObj) {
        var flag = true;
        if (auditObj.Suggestion == null || auditObj.Suggestion == undefined || auditObj.Suggestion.length == 0) {
            flag = false;
        }
        return flag;
    }

    //查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
    //显示用户列表发送消息更新流程
    this.getUserAndUpdateAudit = function (action, flag, jsonData) {
        //提交地址
        var result = Instance.BackgroundInvoke.Agree(jsonData, flag);
        //改变消息状态
        if (flag == "1") {
            var msg = new MessageCommProjPlan($("#msgno").val());
            msg.ReadMsg();
        }
        //提交数据
        if (result.value == "0") {
            alert("发起项目策划评审失败，请联系系统管理员！");
        }
        else if (result.value == "1") {
            alert("项目策划评审完成，项目信息已经同步到TCD中！");
            //window.history.back();
            //location.href = "/Coperation/cpr_SysMsgListView.aspx?flag=A&pageIndex=" + index;
            window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            Instance.renderUserOrSendMsg(flag, result.value);
        }
    }
    //显示用户OR更新审批状态
    this.renderUserOrSendMsg = function (flag, jsonResult) {
        var obj = eval("(" + jsonResult + ")");
        if (flag === "0") {
            Instance.sendMessageClass.render(obj.UserList, obj.RoleName);
            $("#AuditUserDiv").modal();
        }
        else {
            Instance.sendMessageClass.setMsgTemplate(obj);
            Instance.sendMessageClass.chooseUserForMessage(Instance.sendMessage);
        }
    }
    //消息发送成功回调
    this.sendMessage = function (jsonResult) {
        if (jsonResult == "1") {
            $("#AuditUserDiv").modal('hide');
            alert("项目策划评审通过，消息发送成功！");
            //查询系统新消息
            Instance.RedirectToList();
        } else {
            alert("消息发送失败！");
        }
    }
}