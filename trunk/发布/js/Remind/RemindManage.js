﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/RemindHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '部门名称', '姓名', '入司日期', '转正日期', '到期时间', '合同提醒', '转正提醒'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30 },
                             { name: 'Interview_Unit', index: 'Interview_Unit', width: 120},
                             { name: 'mem_Name', index: 'Interview_Name', width: 100},
                             { name: 'rztime_date', index: 'rztime', width: 100 },
                             { name: 'Positivetime_date', index: 'Positivetime', width: 100 },
                             { name: 'Duetime_date', index: 'Duetime', width: 100 },
                             {
                                 name: 'httx', index: 'httx', width: 100, formatter: function InterviewDate(celvalue, options, rowData) {
                                     var str = "";
                                     if (rowData.httx > 30) {
                                         return str;
                                     }
                                     else if (rowData.httx == null) {
                                         return str;
                                     }
                                     else {
                                         return '<span style="color:red">' + celvalue + '</span>';
                                      
                                     }
                                 }
                             },

                             {
                                 name: 'zztx', index: 'zztx', width: 100, formatter: function InterviewDate(celvalue, options, rowData) {
                                     var str = "";
                                     if (rowData.zztx > 30) {
                                         return str;
                                     }
                                     else if (rowData.zztx == "0") {
                                         return str;
                                     }
                                     else if (rowData.zztx == null) {
                                         return str;
                                     }
                                     else {
                                         return '<span style="color:red">' + celvalue + '</span>';
                                     }


                                 }
                             }



        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },

        loadonce: false,
        sortname: 'A.mem_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/RemindHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'ID') + ",";
                            }
                        }
                        return values;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $(".norecords").hide();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/RemindHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname },
            page: 1,
            sortname: 'Interview_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    //$("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
    //    var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
    //    var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    //    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //    var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
    //    $(".norecords").hide();
    //    $("#jqGrid").jqGrid('setGridParam', {
    //        url: "/HttpHandler/ProNumber/ProPassListHandler.ashx?action=sel",
    //        postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
    //        page: 1,
    //        sortname: 'P.[ID]',
    //        sortorder: 'desc'

    //    }).trigger("reloadGrid");
    //});
});
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    //  $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}