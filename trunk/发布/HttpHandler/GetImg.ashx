﻿<%@ WebHandler Language="C#" Class="GetImg" %>

using System;
using System.Web;
using TG.DBUtility;
using TG.Model;
using TG.DAL;

public class GetImg : IHttpHandler
{

    TblAreaDal dal = new TblAreaDal();
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string city = context.Request.QueryString["city"];
        city = context.Server.UrlDecode(city);
        string strwhere = "AreaName ='" + city + "'";
        string src = dal.GetAreaSrc(strwhere);
        context.Response.Write(src);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}