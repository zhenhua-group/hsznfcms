﻿<%@ WebHandler Language="C#" Class="GetquaNum" %>

using System;
using System.Web;


public class GetquaNum : IHttpHandler
{

    TG.BLL.cm_Qualification quabll = new TG.BLL.cm_Qualification();
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string data = quabll.GetquaNum();
        context.Response.Write(data);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}