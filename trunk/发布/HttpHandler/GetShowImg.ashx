﻿<%@ WebHandler Language="C#" Class="GetShowImg" %>

using System;
using System.Web;
using TG.Model;
using System.Text;
using System.Collections.Generic;
using TG.BLL;

public class GetShowImg : IHttpHandler
{
    TG.BLL.cm_AttachInfo attbll = new TG.BLL.cm_AttachInfo();
    TG.BLL.cm_Qualification quabll = new TG.BLL.cm_Qualification();
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        string Getsrc = string.Empty;
        string quaid = context.Request.QueryString["quaid"];
        string attinfoid = quabll.GetattInfoid(quaid);
        List<TG.Model.cm_AttachInfo> list = attbll.GetModelList("Temp_No='" + attinfoid + "'");
        StringBuilder builder = new StringBuilder();
        if (list.Count > 0)
        {
            //../Attach_User/filedata/qualfile/204248/header-bg(31).jpg
            foreach (TG.Model.cm_AttachInfo item in list)
            {
                builder.Append("<img src='../Attach_User/filedata/qualfile/" + item.FileUrl + "' class='selectimg' >");
            }
            Getsrc = builder.ToString();
        }
        else
        {
            Getsrc = "<img src='../Attach_User/filedata/tempimg/tempimg.jpg' class='selectimg' />";
        }
        context.Response.Write(Getsrc);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}