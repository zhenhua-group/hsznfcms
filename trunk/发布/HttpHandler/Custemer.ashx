﻿<%@ WebHandler Language="C#" Class="ConstemerToWord" %>

using System;
using System.Web;
using TG.BLL;
using TG.Model;
using TG.Common;

public class ConstemerToWord : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        TG.BLL.cm_Coperation_back bll = new TG.BLL.cm_Coperation_back();
        TG.BLL.cm_CustomerInfo bllinfo = new TG.BLL.cm_CustomerInfo();
        TG.BLL.cm_CustomerExtendInfo custbll = new TG.BLL.cm_CustomerExtendInfo();
        string custId = context.Request.QueryString["custid"];
        //得到所要导出的对象
        TG.Model.cm_CustomerInfo model = bllinfo.GetModel(Convert.ToInt32(custId));
        TG.Model.cm_CustomerExtendInfo modelcust = custbll.GetModel(Convert.ToInt32(custId));
        //公共类。给模板的书签增加值
        ReportWord report = new ReportWord();
        string path = context.Server.MapPath(@"~\TemplateWord\ReportCustomer.doc");
        if (System.IO.File.Exists(path))
        {
            report.CreateNewDocument(path);
        }
        report.InsertValue("Cst_No", model.Cst_No);
        report.InsertValue("Cst_Name", model.Cst_Name);
        report.InsertValue("Cst_Brief", model.Cst_Brief);
        report.InsertValue("Cpy_Fax", model.Cpy_Fax);
        report.InsertValue("Cpy_Address", model.Cpy_Address);
        report.InsertValue("Code", model.Code == null ? "" : model.Code.ToString());
        report.InsertValue("Cpy_Phone", model.Cpy_Phone);
        report.InsertValue("Linkman", model.Linkman);

        report.InsertValue("BankAccountNo", modelcust.BankAccountNo);
        report.InsertValue("BankName", modelcust.BankName);
        report.InsertValue("BranchPart", modelcust.BranchPart);
        report.InsertValue("City", modelcust.City);
        report.InsertValue("Country", modelcust.Country);
        report.InsertValue("Cpy_Email", modelcust.Email);
        report.InsertValue("Cst_EnglishName", modelcust.Cst_EnglishName);
        report.InsertValue("FadingDaibiao", modelcust.LawPerson);
        report.InsertValue("IsPartner", modelcust.IsPartner.ToString() == "1" ? "合作" : "不合作");
        report.InsertValue("Profession", GetProfession(modelcust.Profession.ToString()));
        report.InsertValue("Province", modelcust.Province);
        report.InsertValue("Qinmidu", GetProfession(modelcust.CloseLeve.ToString()));
        report.InsertValue("QiyeDaima", modelcust.Cpy_Code);
        report.InsertValue("Remark", modelcust.Remark);
        report.InsertValue("RelationDepartment", modelcust.RelationDepartment);
        report.InsertValue("TaxAccountNo", modelcust.TaxAccountNo);
        report.InsertValue("Tyoe", GetProfession(modelcust.Type.ToString()));
        report.InsertValue("Xinyongjibie", GetProfession(modelcust.CreditLeve.ToString()));
        report.InsertValue("UpdateBy", CommCoperation.GetEasyTime(Convert.ToDateTime(modelcust.LastUpdate)));
        string wordPath = context.Server.MapPath(@"~\TemplateWord\repordWord1.doc");
        //下载
        if (System.IO.File.Exists(wordPath))
        {
            report.SaveDocument(wordPath);
            context.Response.ContentType = "application/x-zip-compressed";
            context.Response.AddHeader("Content-Disposition", "attachment;filename= " + model.Cst_Name + ".doc");
            context.Response.TransmitFile(wordPath);
        }
    }
    protected string GetProfession(string num)
    {
        string result = "";
        switch (num)
        {
            case "-1":
                result = "";
                break;
            case "36":
                result = "计算机行业";
                break;
            case "37":
                result = "教育行业";
                break;
            case "38":
                result = "建筑行业";
                break;
            case "47":
                result = "科教行业";
                break;
            case "34":
                result = "普通客户";
                break;
            case "35":
                result = "VIP客户";
                break;
            case "39":
                result = "一般";
                break;
            case "40":
                result = "密切";
                break;
            case "41":
                result = "很密切";
                break;
            case "43":
                result = "一级";
                break;
            case "44":
                result = "二级";
                break;
            case "45":
                result = "三级";
                break;
            case "46":
                result = "四级";
                break;
            default:
                result = "";
                break;
        }
        return result;
    }
    public bool IsReusable
    {

        get
        {
            return false;
        }
    }

}