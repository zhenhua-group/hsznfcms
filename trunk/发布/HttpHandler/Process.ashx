﻿<%@ WebHandler Language="C#" Class="Process" %>

using System;
using System.Web;
using TG.Model;
using TG.BLL;
using TG.Common;

public class Process : IHttpHandler
{


    public void ProcessRequest(HttpContext context)
    {
        TG.BLL.cm_Project bll = new TG.BLL.cm_Project();
        string cprid = context.Request.QueryString["cprname"];
        TG.Model.cm_Project model = bll.GetModel(Convert.ToInt32(cprid));

        ReportWord report = new ReportWord();
        string path = context.Server.MapPath(@"~\TemplateWord\process.doc");
        if (System.IO.File.Exists(path))
        {
            report.CreateNewDocument(path);
        }
        report.InsertValue("BuildAddress", model.BuildAddress);
        report.InsertValue("BuildType", model.BuildType);
        report.InsertValue("ChgJia", model.ChgJia);
        report.InsertValue("Cpr_Acount", model.Cpr_Acount.ToString() + "万元");

        report.InsertValue("Industry", model.Industry);
        report.InsertValue("Phone", model.Phone.ToString());
        report.InsertValue("PMName", model.PMName);
        report.InsertValue("PMPhone", model.PMPhone);
        report.InsertValue("pro_buildUnit", model.pro_buildUnit);
        report.InsertValue("pro_finishTime", CommCoperation.GetEasyTime(Convert.ToDateTime(model.pro_finishTime)));
        report.InsertValue("pro_Intro", model.pro_Intro);
        report.InsertValue("pro_kinds", model.pro_kinds);
        report.InsertValue("pro_level", model.pro_level.ToString() == "0" ? "院管" : "所管");
        report.InsertValue("pro_name", model.pro_name);
        report.InsertValue("Pro_src", GetProfession(model.Pro_src.ToString()));
        report.InsertValue("pro_startTime", CommCoperation.GetEasyTime(Convert.ToDateTime(model.pro_startTime)));
        report.InsertValue("pro_status", model.pro_status.ToString());
        report.InsertValue("pro_StruType", model.pro_StruType);
        report.InsertValue("Project_reletive", model.Project_reletive.ToString());
        report.InsertValue("ProjectScale", model.ProjectScale.ToString() + "平米");
        report.InsertValue("ProjSub", model.ProjSub);
        report.InsertValue("Unit", model.Unit);

        string wordPath = context.Server.MapPath(@"~\TemplateWord\repordWord1.doc");
        if (System.IO.File.Exists(wordPath))
        {
            report.SaveDocument(wordPath);
            context.Response.ContentType = "application/x-zip-compressed";
            context.Response.AddHeader("Content-Disposition", "attachment;filename= " + model.pro_name + ".doc");
            context.Response.TransmitFile(wordPath);
        }
    }
    protected string GetProfession(string num)
    {
        string result = "";
        switch (num)
        {
            case "-1":
                result = "";
                break;
            case "36":
                result = "计算机行业";
                break;
            case "33":
                result = "自行委托";
                break;
            case "37":
                result = "教育行业";
                break;
            case "38":
                result = "建筑行业";
                break;
            case "47":
                result = "科教行业";
                break;
            case "34":
                result = "普通客户";
                break;
            case "35":
                result = "VIP客户";
                break;
            case "39":
                result = "一般";
                break;
            case "40":
                result = "密切";
                break;
            case "41":
                result = "很密切";
                break;
            case "43":
                result = "一级";
                break;
            case "44":
                result = "二级";
                break;
            case "45":
                result = "三级";
                break;
            case "46":
                result = "四级";
                break;
            default:
                result = "";
                break;
        }
        return result;
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}