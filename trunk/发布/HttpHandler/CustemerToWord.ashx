﻿<%@ WebHandler Language="C#" Class="CustemerToWord" %>

using System;
using System.Web;
using TG.BLL;
using TG.Model;
using TG.Common;

public class CustemerToWord : IHttpHandler
{

    //  转换地上和地下楼层数
    protected string GetFloor(string floor)
    {
        string[] floors = floor.Split('|');
        if (floors.Length > 0)
        {
            return "地上:" + floors[0] + "地下:" + floors[1];
        }
        else
        {
            return floor;
        }
    }
    //===============New Mthod
    //根据合同id获得用户id。
    public int GetCstId(int cpr_Id)
    {
        string sql = "select  cst_Id from cm_Coperation where cpr_Id=" + cpr_Id;
        return Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(sql));
    }
    //根据用户id获得用户model
    public TG.Model.cm_CustomerInfo GetConst(int cstid)
    {
        TG.BLL.cm_CustomerInfo dalcust = new TG.BLL.cm_CustomerInfo();
        return dalcust.GetModel(cstid);
    }
    public void ProcessRequest(HttpContext context)
    {
        TG.BLL.cm_Coperation bllcop = new TG.BLL.cm_Coperation();
        string cprid = context.Request.QueryString["cprname"];
        TG.Model.cm_Coperation model = bllcop.GetModel(Convert.ToInt32(cprid));
        int cstid = this.GetCstId(Convert.ToInt32(cprid));
        TG.Model.cm_CustomerInfo conmodel = this.GetConst(cstid);
        ReportWord report = new ReportWord();
        string path = context.Server.MapPath(@"~\TemplateWord\ReportCopration2.doc");
        if (System.IO.File.Exists(path))
        {
            report.CreateNewDocument(path);
        }
        report.InsertValue("Cst_No", conmodel.Cst_No);
        report.InsertValue("Cst_Name", conmodel.Cst_Name);
        report.InsertValue("Cst_Brief", conmodel.Cst_Brief);
        report.InsertValue("Cpy_Fax", conmodel.Cpy_Fax);
        report.InsertValue("Cpy_Address", conmodel.Cpy_Address);
        report.InsertValue("Code", conmodel.Code == null ? "" : conmodel.Code.ToString());
        report.InsertValue("Cpy_Phone", conmodel.Cpy_Phone);
        report.InsertValue("Cst_Name", conmodel.Cst_Name);
        report.InsertValue("BuildArea", model.BuildArea + "平米");
        report.InsertValue("BuildPosition", model.BuildPosition);
        report.InsertValue("BuildSrc", model.BuildSrc);
        report.InsertValue("BuildStructType", model.BuildStructType);
        report.InsertValue("BuildType", model.BuildType);
        report.InsertValue("BuildUnit", model.BuildUnit);
        report.InsertValue("ChgJia", model.ChgJia);
        report.InsertValue("ChgJiaPhone", model.ChgJiaPhone);
        report.InsertValue("ChgPeople", model.ChgPeople.ToString());
        report.InsertValue("ChgPhone", model.ChgPhone);
        report.InsertValue("cpr_Acount", model.cpr_Acount.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元");
        report.InsertValue("cpr_DoneDate", CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_DoneDate)));
        report.InsertValue("cpr_Mark", model.cpr_Mark);
        report.InsertValue("cpr_Name", model.cpr_Name);
        report.InsertValue("cpr_No", model.cpr_No);
        report.InsertValue("cpr_Process", model.cpr_Process);
        report.InsertValue("cpr_ShijiAcount", model.cpr_ShijiAcount.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元");
        report.InsertValue("cpr_ShijiTouzi", model.cpr_ShijiTouzi.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元");
        report.InsertValue("cpr_SignDate", CommCoperation.GetEasyTime(Convert.ToDateTime(model.cpr_SignDate)));
        report.InsertValue("cpr_Touzi", model.cpr_Touzi.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元");
        report.InsertValue("cpr_Type", model.cpr_Type);
        report.InsertValue("cpr_Type2", model.cpr_Type2);
        report.InsertValue("cpr_Unit", model.cpr_Unit.ToString());
        report.InsertValue("TableMaker", model.TableMaker);
        report.InsertValue("Floor", GetFloor(model.Floor));
        report.InsertValue("Industry", model.Industry.ToString());
        report.InsertValue("MultBuild", model.MultiBuild);
        report.InsertValue("StructType", model.StructType.ToString());
        string wordPath = context.Server.MapPath(@"~\TemplateWord\repordWord1.doc");
        if (System.IO.File.Exists(wordPath))
        {
            report.SaveDocument(wordPath);
            context.Response.ContentType = "application/x-zip-compressed";
            context.Response.AddHeader("Content-Disposition", "attachment;filename= " + model.cpr_Name + ".doc");
            context.Response.TransmitFile(wordPath);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}